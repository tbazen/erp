
using INTAPS;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.Evaluator;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;
using INTAPS.RDBMS;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using BIZNET.iERP;
namespace INTAPS.SubscriberManagment.BDE
{

    public partial class SubscriberManagmentBDE : BDEBase
    {
        private void ValidatePaymentCenter(SQLHelper dsp, PaymentCenter center, PaymentCenter old)
        {
            BIZNET.iERP.CashAccount cashAccount = bdeBERP.GetCashAccount(center.casherAccountID);
            if (cashAccount == null)
                throw new ServerUserMessage("Invalid cash account.");
            BIZNET.iERP.CashAccount summaryCashAccount = bdeBERP.GetCashAccount(center.summaryAccountID);
            if (summaryCashAccount == null)
                throw new ServerUserMessage("Invalid summary cash account.");
            else if (this.bdeAccounting.GetCostCenterAccountWithDescription(center.summaryAccountID).account.Code.IndexOf("CUST-") != -1)
                throw new ServerUserMessage("Summary account can't be under CUST");

            BIZNET.iERP.CashAccount detailCashAccount= bdeBERP.GetCashAccount(center.casherAccountID);
            if (detailCashAccount != null)
                if (this.bdeAccounting.GetCostCenterAccountWithDescription(center.casherAccountID).account.Code.IndexOf("CUST-") == -1)
                    throw new ServerUserMessage("Detail account should be under CUST");


            if (cashAccount.employeeID != summaryCashAccount.employeeID)
                throw new ServerUserMessage("Both cash accounts should belong to the same employee");
            if (cashAccount.employeeID == -1)
                throw new ServerUserMessage("Employee ID should be set to the cash accounts");
            if (center.casherAccountID == center.summaryAccountID)
                throw new ServerUserMessage("Summary and detail account can't be the same");

            Employee emp = this.bdePayroll.GetEmployee(cashAccount.employeeID);
            if (emp == null)
                throw new ServerUserMessage("Invalid employee id set to the cash accounts");
            if (string.IsNullOrEmpty(emp.loginName))
                throw new ServerUserMessage("The casher employee doesn't have login name set");

            center.casheir = emp.id;
            center.userName = emp.loginName;

            if ((old == null) || (old.casherAccountID != center.casherAccountID))
            {
                if (old == null)
                {
                    if (((int)dsp.ExecuteScalar(string.Concat(new object[] { "Select count(*) from ", base.DBName, ".dbo.PaymentCenter where casherAccountID='", center.casherAccountID, "'" }))) > 0)
                    {
                        throw new Exception("Account allready assigned to another payment center.");
                    }
                }
                else if (((int)dsp.ExecuteScalar(string.Concat(new object[] { "Select count(*) from ", base.DBName, ".dbo.PaymentCenter where id<>", old.id, " and casherAccountID='", center.casherAccountID, "'" }))) > 0)
                {
                    throw new Exception("Account allready assigned to another payment center.");
                }
            }
            //if ((old == null) || (old.userName != center.userName))
            //{
            //    try
            //    {
            //        ApplicationServer.SecurityBDE.User(center.userName);
            //    }
            //    catch
            //    {
            //        throw new Exception("Invalid for center user name.");
            //    }
            //    if (old == null)
            //    {
            //        if (((int)dsp.ExecuteScalar("Select count(*) from " + base.DBName + ".dbo.PaymentCenter where userName='" + center.userName + "'")) > 0)
            //        {
            //            throw new Exception("User assinged to another payment center.");
            //        }
            //    }
            //    else if (((int)dsp.ExecuteScalar(string.Concat(new object[] { "Select count(*) from ", base.DBName, ".dbo.PaymentCenter where id<>", old.id, " and userName='", center.userName, "'" }))) > 0)
            //    {
            //        throw new Exception("User assinged to another payment center.");
            //    }
            //}
        }

        // BDE exposed
        public void UpdatePaymentCenter(int AID,PaymentCenter center)
        {
            lock (base.dspWriter)
            {
                PaymentCenter paymentCenter = this.GetPaymentCenter(center.id);
                this.ValidatePaymentCenter(base.dspWriter, center, paymentCenter);
                base.dspWriter.setReadDB(base.DBName);
                try
                { 
                    fixSummerizationCodes(AID, center);
                    base.dspWriter.logDeletedData(AID, this.DBName + ".dbo.PaymentCenter", "id=" + center.id);
                    base.dspWriter.UpdateSingleTableRecord<PaymentCenter>(base.m_DBName, center, new string[] { "__AID" }, new object[] { AID });
                    foreach (PaymentCenterSerialBatch batch in paymentCenter.serialBatches)
                    {
                        base.dspWriter.DeleteSingleTableRecrod<PaymentCenterSerialBatch>(base.m_DBName, new object[] { center.id, this.Accounting.GetDocumentTypeByType(typeof(CustomerPaymentReceipt)).id, batch.batchID });
                    }
                    foreach (PaymentCenterSerialBatch batch in center.serialBatches)
                    {   
                        batch.paymentCenterID = center.id;
                        base.dspWriter.InsertSingleTableRecord<PaymentCenterSerialBatch>(base.m_DBName, batch);
                    }
                }finally
                {
                    base.dspWriter.restoreReadDB();
                }
            }
        }

        private void fixSummerizationCodes(int AID, PaymentCenter center)
        {
            if (center.casherAccountID > 0 && center.summaryAccountID > 0)
            {
                Account daily = this.bdeAccounting.GetAccount<Account>(this.bdeAccounting.GetCostCenterAccount(center.casherAccountID).accountID);
                Account sum = this.bdeAccounting.GetAccount<Account>(this.bdeAccounting.GetCostCenterAccount(center.summaryAccountID).accountID);
                if (!daily.Code.Equals("CUST-" + sum.Code))
                {
                    daily.Code = "CUST-" + sum.Code;
                    this.bdeAccounting.UpdateAccount<Account>(AID, daily);
                }
            }
        }
        // BDE exposed
        public int CreatePaymentCenter(int AID,PaymentCenter center)
        {
            int id;
            lock (base.dspWriter)
            {
                this.ValidatePaymentCenter(base.dspWriter, center, null);
                
                center.id = AutoIncrement.GetKey("CustomerManagment.CenterID");
                
                base.dspWriter.BeginTransaction();
                base.dspWriter.setReadDB(base.DBName);
                try
                {
                    fixSummerizationCodes(AID, center);
                    base.dspWriter.InsertSingleTableRecord<PaymentCenter>(base.m_DBName, center, new string[]{"__AID"},new object[]{AID});
                    foreach (PaymentCenterSerialBatch batch in center.serialBatches)
                    {
                        batch.paymentCenterID = center.id;
                        batch.documentTypeID = this.Accounting.GetDocumentTypeByType(typeof(CustomerPaymentReceipt)).id;
                        base.dspWriter.InsertSingleTableRecord<PaymentCenterSerialBatch>(base.m_DBName, batch);
                    }
                    base.dspWriter.CommitTransaction();
                    id = center.id;
                }
                catch (Exception exception)
                {
                    base.dspWriter.RollBackTransaction();
                    throw exception;
                }
                finally
                {
                    base.dspWriter.restoreReadDB();
                }
            }
            return id;
        }

        public bool IsPaymentSerialBatchOverlapped(SerialBatch batch)
        {
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                bool overlapped = false;
                PaymentCenterSerialBatch[] batches = readerHelper.GetSTRArrayByFilter<PaymentCenterSerialBatch>(null);
                foreach (PaymentCenterSerialBatch pbatch in batches)
                {
                    SerialBatch sBatch = bdeAccounting.GetSerialBatch(pbatch.batchID);
                    if (sBatch != null)
                    {
                        if (batch.serialFrom >= sBatch.serialFrom && batch.serialTo <= sBatch.serialTo)
                        {
                            overlapped = true;
                            break;
                        }
                        if (batch.serialTo >= sBatch.serialFrom && batch.serialTo <= sBatch.serialTo)
                        {
                            overlapped = true;
                            break;
                        }
                    }
                }
                return overlapped;
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
        }
        // BDE exposed
        public void DeletePaymentCenter(int AID, int centerID)
        {
            dspWriter.Delete(this.DBName, "PaymentCenterSerialBatch", "paymentCenterID=" + centerID);
            base.dspWriter.logDeletedData(AID, this.DBName + ".dbo.PaymentCenter", "id=" + centerID);
            base.dspWriter.DeleteSingleTableRecrod<PaymentCenter>(base.m_DBName, centerID);
            dspWriter.CommitTransaction();
        }

        // BDE exposed
        public PaymentCenter[] GetAllPaymentCenters()
        {
            PaymentCenter[] centerArray2;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                PaymentCenter[] sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<PaymentCenter>(null);
                foreach (PaymentCenter center in sTRArrayByFilter)
                {
                    center.serialBatches = readerHelper.GetSTRArrayByFilter<PaymentCenterSerialBatch>("paymentCenterID=" + center.id);
                }
                centerArray2 = sTRArrayByFilter;
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return centerArray2;
        }

        private void VerifyUniqueRefNo(AccountDocument doc)
        {
            if (bdeAccounting.FindFirstDocument(doc.PaperRef, bdeAccounting.GetDocumentTypeByType(doc.GetType()).id) != null)
            {
                throw new Exception("Reference number " + doc.PaperRef + " is allready used");
            }
        }
        // BDE exposed
        public PaymentCenter GetPaymentCenter(string userID)
        {
            PaymentCenter center;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                PaymentCenter[] sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<PaymentCenter>("userName='" + userID + "'");
                if (sTRArrayByFilter.Length == 0)
                {
                    return null;
                }
                sTRArrayByFilter[0].serialBatches = readerHelper.GetSTRArrayByFilter<PaymentCenterSerialBatch>("paymentCenterID=" + sTRArrayByFilter[0].id);
                center = sTRArrayByFilter[0];
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return center;
        }
        // BDE exposed
        public PaymentCenter GetPaymentCenterByCashAccount(int account)
        {
            PaymentCenter center;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                PaymentCenter[] sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<PaymentCenter>("casherAccountID=" + account + " or summaryAccountID="+account);
                if (sTRArrayByFilter.Length == 0)
                {
                    return null;
                }
                sTRArrayByFilter[0].serialBatches = readerHelper.GetSTRArrayByFilter<PaymentCenterSerialBatch>("paymentCenterID=" + sTRArrayByFilter[0].id);
                center = sTRArrayByFilter[0];
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return center;
        }
        // BDE exposed
        public PaymentCenter GetPaymentCenter(int centerID)
        {
            PaymentCenter center;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                PaymentCenter[] sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<PaymentCenter>("id=" + centerID);
                if (sTRArrayByFilter.Length == 0)
                {
                    throw new Exception("Payment center not found in the datbase centerID:" + centerID);
                }
                sTRArrayByFilter[0].serialBatches = readerHelper.GetSTRArrayByFilter<PaymentCenterSerialBatch>("paymentCenterID=" + centerID);
                center = sTRArrayByFilter[0];
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return center;
        }
    }
}