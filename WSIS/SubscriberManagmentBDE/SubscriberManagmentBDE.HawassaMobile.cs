using INTAPS;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.Evaluator;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;
using INTAPS.RDBMS;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using BIZNET.iERP;
using System.Net;
using System.Data.SqlClient;
namespace INTAPS.SubscriberManagment.BDE
{

    public partial class SubscriberManagmentBDE : BDEBase
    {
        IReadingAdapter _readingAdapter;
        bool _adaptorLoaded = false;
      
        void initializeReadingAdapter()
        {
            //load the assembly from configuration
            //instantiate the IReadingAdapter
            //_readingAdapter.initialize();
            loadReadingAdapter();
            if (_adaptorLoaded)
            {
                _readingAdapter.initialize();
              //  generateReadingPeriod();
            }
        }

        private void generateReadingPeriod()
        {
            _readingAdapter.generateReadingPeriod();
        }
        void loadReadingAdapter()
        {

            string an = System.Configuration.ConfigurationManager.AppSettings["readingAdapterAssembly"];
            if (an != null)
            {
                string d = new System.IO.FileInfo(Assembly.GetCallingAssembly().Location).DirectoryName;
                if (System.IO.File.Exists(d + "\\" + an+".dll"))
                {
                    Assembly a = System.Reflection.Assembly.Load(an);
                    try
                    {
                        foreach (Type t in a.GetTypes())
                        {
                            if (t.GetInterface(typeof(IReadingAdapter).Name) != null)
                            {
                                ConstructorInfo ci = t.GetConstructor(new Type[] { typeof(SubscriberManagmentBDE) });
                                if (ci == null)
                                {
                                    ApplicationServer.EventLoger.Log(EventLogType.Errors, "Constructor not found for type ");
                                    continue;
                                }
                                try
                                {
                                    IReadingAdapter adapter = ci.Invoke(new object[] { this }) as IReadingAdapter;
                                    if (adapter == null)
                                    {
                                        ApplicationServer.EventLoger.Log(EventLogType.Errors, "Reading adapter " + t + " couldn't be loaded");
                                        continue;
                                    }
                                    _readingAdapter = adapter;
                                    ApplicationServer.EventLoger.Log(EventLogType.ServerActions, "Reading adapter " + t + " loaded succesfully.");
                                    _adaptorLoaded = true;
                                    return;
                                }
                                catch (Exception tex)
                                {
                                    ApplicationServer.EventLoger.LogException("Error trying to instantiate reading adapter " + t, tex);
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        ApplicationServer.EventLoger.LogException("Failed to load reading adapter", ex);
                    }
                }
            }
            ApplicationServer.EventLoger.Log(EventLogType.Errors, "No reading adapter found");
        }

        // BDE exposed
        public void transferCustomerData(int AID, int wsisPeriodID)
        {
            generateReadingPeriod();
            _readingAdapter.transferCustomerData(AID, wsisPeriodID);
        }
        public void UploadReading(int wsisPeriodID)
        {
            _readingAdapter.UploadReading(wsisPeriodID);
        }
      
        public void registerMobileReader(int AID, Employee emp,string password, int periodID)
        {
            lock(dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    if (emp.id == -1)
                    {
                        ApplicationServer.SecurityBDE.CreateUser(null, emp.loginName, password);
                        int empID = bdeBERP.RegisterEmployee(AID, emp, null, false, null, -1, null, null, null, null);

                        MeterReaderEmployee readerEmp = new MeterReaderEmployee();
                        readerEmp.employeeID = empID;
                        readerEmp.periodID = periodID;
                        CreateMeterReaderEmployee(readerEmp);

                        ReadingEntryClerk clerk = new ReadingEntryClerk();
                        clerk.employeeID = empID;
                        clerk.userID = emp.loginName;
                        CreateReadingEntryClerk(clerk);
                    }
                    else
                    {
                        bdePayroll.Update(AID, emp, false);
                    }

                    dspWriter.CommitTransaction();
                }
                catch(Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public void changePassword(int AID, string userName, string password)
        {
            ApplicationServer.SecurityBDE.ChangePassword(userName, password);
        }
        public DataTable getReadingStatistics(int periodID)
        {
            SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                string sqlPeriod = string.Format("Select toDate from {0}.dbo.BillPeriod where id=" + periodID, base.DBName);
                long toTicks = ((DateTime)dspReader.ExecuteScalar(sqlPeriod)).Ticks;

                string sqlMain = @"SELECT     Subscription.kebele, COUNT(*) AS c,Sum(case when BWFMeterReading.reading is null then 0 else 1 end),Sum(case when BWFMeterReading.reading is null then 0 when BWFMeterReading.bwfStatus=0 then 0 else 1 end) as readMeter
FROM         Subscription LEFT OUTER JOIN
                      BWFMeterReading ON Subscription.id = BWFMeterReading.subscriptionID AND BWFMeterReading.periodID = {0}
where subscriptionStatus=2 and ticksFrom<={1} and (ticksTo=-1 or ticksTo>{1}) and subscriptionStatus=2
GROUP BY Subscription.kebele order by Kebele";
                string sql = string.Format(sqlMain, periodID, toTicks);
                DataTable tbl = dspReader.GetDataTable(sql);
                return tbl;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        public int getReaderCount(int periodID, int readerID)
        {
            SQLHelper dsp = base.GetReaderHelper();
            try
            {
                int systemBlockID = int.Parse((string)dsp.ExecuteScalar("Select ParValue from SystemParameter where ParName='systemReadingBlockID'"));
                string sql = @"select count(*) from BWFMeterReading where 
bwfStatus=1 and method in (2,3) 
and periodID={0} and readingBlockID in (Select id from BWFReadingBlock where readerID={1} and id<>{2})";
                string readsql = string.Format(sql, periodID, readerID, systemBlockID);
                int count = (int)dsp.ExecuteScalar(readsql);
                return count;
            }
            finally
            {
                base.ReleaseHelper(dsp);
            }

        }
        public ReadingCycle getReadingCycle(int periodID, int status)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                ReadingCycle[] ret = dspReader.GetSTRArrayByFilter<ReadingCycle>("status=" + status + " and periodID=" + periodID);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        public BWFDescribedMeterReading[] BWFGetDescribedReadings(int blockID,int periodID, string contractNo, string name, int kebele, int customerType, int index, int pageSize, out int NRecords)
        {
            if (pageSize < 2)
                throw new Exception("Invalid query");
            BWFDescribedMeterReading[] resultArray;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                SQLHelper helper2 = readerHelper.Clone();
                SQLHelper helper3 = readerHelper.Clone();
                IDataReader reader = null;
                if (pageSize < 1)
                {
                    throw new Exception("Invalid page size.");
                }
                try
                {
                    string sql = "";

                    if (blockID == -1)
                    {
                        sql = @"Select Subscription.* from Subscription  inner join Subscriber on Subscriber.id=Subscription.SubscriberID
               where timeBinding=1 and Subscription.SubscriptionStatus=" + (int)SubscriptionStatus.Active + " and Subscription.id not in (select subscriptionID from BWFMeterReading where periodID=" + periodID + ")";
                    }
                    else if (blockID == -2)
                    {
                        sql = @"SELECT     BWFMeterReading.readingBlockID, Subscription.id, BWFMeterReading.bwfStatus, BWFMeterReading.readingType, BWFMeterReading.averageMonths, 
                      BWFMeterReading.reading, BWFMeterReading.consumption, BWFMeterReading.entryDate, BWFMeterReading.orderN, BWFMeterReading.billDocumentID, 
                      BWFMeterReading.periodID, BWFMeterReading.readingProblem, BWFMeterReading.extraInfo, BWFMeterReading.readingRemark, BWFMeterReading.readingTime, 
                      BWFMeterReading.readingX, BWFMeterReading.readingY, BWFMeterReading.method, BWFMeterReading.__AID
FROM         Subscription LEFT OUTER JOIN
                      BWFMeterReading ON BWFMeterReading.subscriptionID = Subscription.id AND BWFMeterReading.periodID = {0} INNER JOIN
                      Subscriber ON Subscriber.id = Subscription.subscriberID
WHERE     (Subscription.timeBinding = 1) and Subscription.SubscriptionStatus={1}".format(periodID,(int)SubscriptionStatus.Active);
                    }
                    else
                    {
                        sql = @"Select BWFMeterReading.* from BWFMeterReading inner join Subscription 
                  on BWFMeterReading.subscriptionID=Subscription.id inner join Subscriber on Subscriber.id=Subscription.SubscriberID
               where timeBinding=1 and readingBlockID=" + blockID;
                    }

                    if (!string.IsNullOrEmpty(contractNo))
                        sql += " and contractNo like '%" + contractNo + "%'";

                    if (!string.IsNullOrEmpty(name))
                        sql += " and name like '%" + name + "%'";

                    if (kebele != -1)
                        sql += " and Subscriber.Kebele=" + kebele;

                    if (customerType != -1)
                        sql += " and SubscriberType=" + customerType;

                    reader = helper2.ExecuteReader(sql);
                    List<BWFDescribedMeterReading> list = new List<BWFDescribedMeterReading>();

                    NRecords = 0;
                    while (reader.Read())
                    {
                        
                        BWFMeterReading reading = new BWFMeterReading();
                        reading.readingBlockID = blockID != -1 ? INTAPS.TypeTools.castWithDefault<int>(reader[0]) : -1;
                        reading.subscriptionID = blockID != -1 ? INTAPS.TypeTools.castWithDefault<int>(reader[1]) : (int)reader[0];
                        reading.bwfStatus = blockID != -1 ? (BWFStatus)INTAPS.TypeTools.castWithDefault<int>(reader[2]) : BWFStatus.Unread;
                        reading.readingType = blockID != -1 ? (MeterReadingType)INTAPS.TypeTools.castWithDefault<int>(reader[3]) : MeterReadingType.Normal;
                        reading.averageMonths = blockID != -1 ? INTAPS.TypeTools.castWithDefault<int>(reader[4]) : 0;
                        reading.reading = blockID != -1 ? INTAPS.TypeTools.castWithDefault<double>(reader[5]) : 0;
                        reading.consumption = blockID != -1 ? INTAPS.TypeTools.castWithDefault<double>(reader[6]) : 0;
                        reading.entryDate = blockID != -1 ? INTAPS.TypeTools.castWithDefault<DateTime>(reader[7]) : DateTime.Now;
                        reading.orderN = blockID != -1 ? INTAPS.TypeTools.castWithDefault<int>(reader[8]) : reading.subscriptionID;
                        reading.billDocumentID = blockID != -1 ? INTAPS.TypeTools.castWithDefault<int>(reader[9]) : -1;
                        //reading.periodID = blockID != -1 ? INTAPS.TypeTools.castWithDefault<int>(reader[10]) : periodID;
                        reading.periodID = periodID;
                        reading.readingProblem = blockID != -1 ? (MeterReadingProblem)INTAPS.TypeTools.castWithDefault<int>(reader[11]) : MeterReadingProblem.Other;
                        reading.extraInfo = blockID != -1 ? (ReadingExtraInfo)INTAPS.TypeTools.castWithDefault<int>(reader[12]) : ReadingExtraInfo.None;
                        reading.readingRemark = blockID != -1 ? (string)INTAPS.TypeTools.castWithDefault<string>(reader[13]) : "";
                        reading.readingTime = blockID != -1 ? INTAPS.TypeTools.castWithDefault<DateTime>(reader[14]) : DateTime.Now;
                        reading.readingX = blockID != -1 ? INTAPS.TypeTools.castWithDefault<double>(reader[15]) : (double)reader[19];
                        reading.readingY = blockID != -1 ? INTAPS.TypeTools.castWithDefault<double>(reader[16]) : (double)reader[20];
                        reading.method = blockID != -1 ? (ReadingMethod)INTAPS.TypeTools.castWithDefault<int>(reader[17]) : ReadingMethod.ReaderMobileOnline;

                        NRecords++;
                        if (((NRecords - 1) >= index) && (NRecords <= (index + pageSize)))
                        {
                            Subscription subsc = GetSubscriptionInternal(helper3, reading.subscriptionID, blockID != -1 ? GetBillPeriod(reading.periodID).toDate.Ticks : GetBillPeriod(periodID).toDate.Ticks, true);
                            BWFDescribedMeterReading desc = new BWFDescribedMeterReading(subsc, reading);
                            BWFMeterReading r = this.BWFGetMeterPreviousReading(reading.subscriptionID, reading.periodID);
                            if (r != null)
                            {
                                desc.previousPeriod = this.GetBillPeriod(r.periodID);
                                desc.previousReading = r;
                            }

                            list.Add(desc);
                        }
                    }
                    resultArray = list.ToArray();
                }
                finally
                {
                    if (!((reader == null) || reader.IsClosed))
                    {
                        reader.Close();
                    }
                    helper2.CloseConnection();
                    helper3.CloseConnection();
                }
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return resultArray;

        }

        public void transferSubscriptionsToReader(int AID, int[]subscriptions, int readerID, int periodID)
        {
            //check if the reader has a block and transfer all the subscriptions to that block
            //if the reader has no block, create new block and transfer the subscriptions to that block
            //only one block per reader is allowed
            lock (dspWriter)
            {
                try
                {
                    dspWriter.BeginTransaction();
                    dspWriter.setReadDB(this.DBName);
                    BWFReadingBlock[] block = dspWriter.GetSTRArrayByFilter<BWFReadingBlock>("readerID=" + readerID + " and periodID=" + periodID);
                    int readingBlockID = -1;
                    if (block.Length == 0)
                    {
                        //create new block for reader
                        BWFReadingBlock newBlock = new BWFReadingBlock();
                        newBlock.readerID = readerID;
                        newBlock.periodID = periodID;
                        newBlock.blockName = "Mobile-" + bdePayroll.GetEmployee(readerID).employeeID;
                        newBlock.entryClerkID = -1;
                        newBlock.readingStart = DateTime.Now.Date;
                        newBlock.readingEnd = DateTime.Now.Date;
                        readingBlockID = BWFCreateReadingBlock(AID, null, newBlock);
                    }
                    else
                        readingBlockID = block[0].id;
                    bdeAccounting.PushProgress(String.Format("Transferring subscriptions to {0}", GetBlock(readingBlockID).blockName));
                    int step = 0;
                    foreach (var sub in subscriptions)
                    {
                        //Consider transferring subscriptions in the case of unallocated blocks too (If the subscription is not found in BWFMeterReading)
                        string sql = "Update Subscriber.dbo.BWFMeterReading set readingBlockID=" +readingBlockID + " where subscriptionID=" + sub + " and periodID=" + periodID;
                        dspWriter.ExecuteScalar(sql);
                        transferUnallocatedBlockReading(AID, readingBlockID, periodID, sub, null, dspWriter);
                        step++;
                        bdeAccounting.SetProgress((double)step / (double)subscriptions.Length);
                    }
                    bdeAccounting.PopProgress();
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }

        public void deleteMeterReaderProfile(int AID, int empID)
        {
            //check if the reader has not joined reading block and if not delete all reader's profile(employee,username,reading clerk,meter reader)
            //Block if the reader has joined reading block
            lock(dspWriter)
            {
                try
                {

                }
                catch(Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw new ServerUserMessage(ex.Message);
                }
            }
        }

        private void transferUnallocatedBlockReading(int AID, int block, int period, int subscriptionID, ReadingEntryClerk clerk, SQLHelper helper)
        {

            int count = (int)helper.ExecuteScalar("select count(*) from Subscriber.dbo.BWFMeterReading where subscriptionID=" + subscriptionID + " and periodID=" + period);

            if (count == 0)
            {
                object order = helper.ExecuteScalar("select max(orderN) from Subscriber.dbo.BWFMeterReading where periodID=" + period);
                int lastOrderN = order is DBNull ? 0 : (int)order;
                Subscription sub = helper.GetSTRArrayByFilter<Subscription>("id=" + subscriptionID)[0];

                BWFMeterReading reading = new BWFMeterReading();
                reading.readingBlockID = block;
                reading.subscriptionID = subscriptionID;
                reading.bwfStatus = BWFStatus.Unread;
                reading.readingType = MeterReadingType.Normal;
                reading.averageMonths = 0;
                reading.reading = 0;
                reading.consumption = 0;
                reading.entryDate = DateTime.Now;
                reading.orderN = lastOrderN + 1;
                reading.billDocumentID = -1;
                reading.periodID = period;
                reading.readingProblem = MeterReadingProblem.NoProblem;
                reading.extraInfo = ReadingExtraInfo.None;
                reading.readingRemark = "Reading imported WSS System Employee";
                reading.readingTime = reading.entryDate;
                reading.readingX = sub.waterMeterX;
                reading.readingY = sub.waterMeterY;
                reading.method = ReadingMethod.Unknown;
                BWFAddMeterReading(AID, clerk, reading, false);
            }
        }


    }
}