using INTAPS;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.Evaluator;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;
using INTAPS.RDBMS;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using BIZNET.iERP;
using System.Net;
using System.Data.SqlClient;
namespace INTAPS.SubscriberManagment.BDE
{
    
    public partial class SubscriberManagmentBDE : BDEBase
    {
        const string REP_ID_CUSTOMER_BILL = "System.Customer.BillData";
        const string REP_ID_CUSTOMER_READING = "System.Customer.Reading";
        const string REP_ID_CUSTOMER_LIST = "System.Customer.CustList";
        // BDE exposed
        public string htmlDocGetCustomerBillData(int customerID, int connectionID, out string headers)
        {
            int repID;
            repID= checkAndGetReportDefination(REP_ID_CUSTOMER_BILL);
            List<object> billsList = new List<object>();
            
            foreach (CustomerBillRecord record in getBills(-1,customerID,connectionID,-1))
            {
                CustomerBillDocument billDoc = bdeAccounting.GetAccountDocument(record.id, true) as CustomerBillDocument;

                
                string periodName;
                if (record.periodID == -1)
                    periodName = "";
                else
                    periodName = this.GetBillPeriod(record.periodID).name;
                string typeText = bdeAccounting.GetDocumentTypeByID(record.billDocumentTypeID).name;
                string statusText;
                if (record.draft)
                {
                    statusText= "Draft";
                }
                else if (record.paymentDocumentID == -1 && !record.paymentDiffered)
                {
                    statusText= "Not paid";
                }
                else if (record.paymentDiffered)
                {
                    statusText= "Payment Differed";
                }
                else
                {
                    statusText="Paid";
                }

                //Bill data fields: Period, PeriodID,Type, TypeID, Status, Amount,Remark,ConnectionID,draft
                billsList.Add(new
                {
                    Period = periodName,
                    PeriodID = record.periodID,
                    Type = typeText,
                    TypeID = record.billDocumentTypeID,
                    Status = statusText,
                    Amount = billDoc == null ? 0 : billDoc.total,
                    Remark = billDoc == null ? "Error" : billDoc.ShortDescription,
                    ConnectionID = record.connectionID,
                    draft = record.draft
                });
            }
            
            Subscriber customer = GetSubscriber(customerID);
            return evaluateReport(repID, new object[] {
                "bills",new EData(billsList),
                "customerID",new EData(customerID), 
                "customer",new EData(customer),
            }, out headers);

            
        }

        public string htmlDocGetReadings(int kebele, int subscriptionID, int periodID, int employeeID, out string headers)
        {
            int repID;
            repID = checkAndGetReportDefination(REP_ID_CUSTOMER_READING);
            List<object> billsList = new List<object>();

            int N;
            BWFMeterReading[] readingArray = this.BWFGetMeterReadings(kebele, subscriptionID, periodID, employeeID, 0, 1000000, out N);
            List<object> readings = new List<object>();
            Dictionary<int, BWFReadingBlock> blocks = new Dictionary<int, BWFReadingBlock>();
            foreach (BWFMeterReading reading in readingArray)
            {
                try
                {
                    Subscription subscription = this.GetSubscription(reading.subscriptionID, this.GetBillPeriod(reading.periodID).toDate.Ticks);
                    string contractNo = subscription.contractNo;
                    string name = subscription.subscriber.name;
                    string period = this.GetBillPeriod(reading.periodID).name;
                    BWFMeterReading reading1 = this.BWFGetMeterPreviousReading(subscription.id, reading.periodID);
                    string prevPeriod;
                    string prevReading;
                    string currentReading;
                    string consumption;
                    string readingDate;
                    string readingType;
                    if (reading1 == null)
                    {
                        prevPeriod = "";
                        prevReading = "";
                    }
                    else
                    {
                        prevPeriod = this.GetBillPeriod(reading1.periodID).name;
                        prevReading = reading1.reading.ToString("0.0");
                    }
                    if (reading.bwfStatus == BWFStatus.Unread)
                    {
                        currentReading = "";
                        consumption = "";
                        readingDate = "";
                        readingType = "Not Read";
                    }
                    else
                    {
                        currentReading = reading.reading.ToString("0.0");
                        consumption = reading.consumption.ToString("0.0");
                        readingDate = INTAPS.Ethiopic.EtGrDate.ToEth(reading.entryDate).ToString();
                        readingType = reading.readingType.ToString();
                    }

                    if (!blocks.ContainsKey(reading.readingBlockID))
                        blocks.Add(reading.readingBlockID, this.GetBlock(reading.readingBlockID));
                    string block = blocks[reading.readingBlockID].blockName;
                    readings.Add(new
                    {
                        contractNo = contractNo,
                        name = name,
                        period = period,
                        prevPeriod = prevPeriod,
                        prevReading = prevReading,
                        currentReading = currentReading,
                        consumption = consumption,
                        readingDate = readingDate,
                        readingType = readingType,
                        block = block,
                    });

                }
                catch (Exception ex)
                {
                    readings.Add(new
                    {
                        contractNo = "error",
                        name = ex.Message,
                        period = "",
                        prevPeriod = "",
                        prevReading="",
                        currentReading = "",
                        consumption = "",
                        readingDate = "",
                        readingType = "",
                        block = "",
                    });
                }
            }
            Subscription connection = null;
            if (subscriptionID != -1)
                connection = GetSubscription(subscriptionID, DateTime.Now.Ticks);
            return evaluateReport(repID, new object[] {
                "readings",new EData(readings),
                "connectionID",new EData(subscriptionID), 
                "customer",new EData(connection.subscriber),
            }, out headers);
        }

        public string htmlDocGetCustomerList(SubscriberSearchResult[] result, out string headers)
        {
            int repID;
            repID = checkAndGetReportDefination(REP_ID_CUSTOMER_LIST);
            List<object> custList = new List<object>();
            int i = 0;
            foreach (SubscriberSearchResult res in result)
            {
                DistrictMeteringZone dma = getDMA(res.subscription.dma);
                PressureZone zone = getPressureZone(res.subscription.pressureZone);
                custList.Add(new
                {
                    no = i + 1,
                    customerCode = res.subscriber.customerCode,
                    contractNo = res.subscription == null ? "N/A" : res.subscription.contractNo,
                    meterNo = res.subscription == null ? "N/A" : res.subscription.serialNo,
                    customerName = res.subscriber.name,
                    subscriptionType = res.subscription == null ? "N/A" : res.subscription.subscriptionType.ToString(),
                    status = res.subscription == null ? "N/A" : res.subscription.subscriptionStatus.ToString(),
                    kebele = GetKebele(res.subscription.Kebele).name,
                    dma=dma==null?"":dma.name,
                    zone=zone==null ? "" : zone.name
                });
                i++;   
            }

            return evaluateReport(repID, new object[] {
                "cust",new EData(custList)
            }, out headers);


        }

        string evaluateReport(int repID,object[] pars,out string headers)
        {
            string body= this.bdeAccounting.EvaluateEHTML(repID, pars,out headers);
            string httpEndPoint = this.bdeAccounting.webServerClientAddress;
            if (!string.IsNullOrEmpty(httpEndPoint))
            {
                headers = headers + string.Format("<link href='{0}/css/htmlreport/{1}' rel='stylesheet' type='text/css'/>", httpEndPoint, this.bdeAccounting.GetEHTMLDAta(repID).styleSheet);
            }
            return body;

        }
        private int checkAndGetReportDefination(string defCode)
        {
            int ret;
            ret= bdeAccounting.GetReportDefID(defCode);
            if (ret==-1)
                throw new ServerUserMessage("Report defination code: {0} not defined", defCode);
            return ret;
        }
        
    }
}