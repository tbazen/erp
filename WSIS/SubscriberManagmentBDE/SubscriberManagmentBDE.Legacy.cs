﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;

namespace INTAPS.SubscriberManagment.BDE
{
    partial class SubscriberManagmentBDE
    {
        public const string OB_BLOCK = "Outstanding Bill";
        // BDE exposed
        public void lbDeleteSet(int AID, int setID)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.LegacyBillItem", string.Format("legacyBillID in (Select id from {0}.dbo.LegacyBill where  setID={1})", this.DBName, setID));
                    dspWriter.Delete(this.DBName, "LegacyBillItem", string.Format("legacyBillID in (Select id from {0}.dbo.LegacyBill where  setID={1})", this.DBName, setID));
                    
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.LegacyBill", "setID=" + setID);
                    dspWriter.Delete(this.DBName, "LegacyBill", "setID=" + setID);
                    
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.LegacyBillSet", "id=" + setID);
                    dspWriter.DeleteSingleTableRecrod<LegacyBillSet>(this.DBName, setID);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        // BDE exposed
        public int lbSaveLegacySet(int AID, LegacyBillSet set, LegacyBill[] bills)
        {
            int userID = ApplicationServer.SecurityBDE.GetUIDForName(set.encoderUserID);
            if (userID == -1)
                throw new ServerUserMessage("Invalid user ID:" + set.encoderUserID);
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    if (set.id != -1)
                    {
                        /*dspWriter.Delete(this.DBName, "LegacyBillItem", string.Format("legacyBillID in (Select id from {0}.dbo.LegacyBill where  setID={1})",this.DBName,set.id));
                        dspWriter.Delete(this.DBName, "LegacyBill", "setID=" + set.id);
                        dspWriter.DeleteSingleTableRecrod<LegacyBillSet>(this.DBName, set.id);*/
                        lbDeleteSet(AID, set.id);
                    }
                    else
                        set.id = AutoIncrement.GetKey("LegacyBillSetID");
                    dspWriter.InsertSingleTableRecord(this.DBName, set, new string[] { "__AID" }, new object[] { AID });
                    int orderN = 1;
                    foreach (LegacyBill b in bills)
                    {
                        BillPeriod bp = this.GetBillPeriod(b.periodID);
                        if (bp == null)
                            throw new ServerUserMessage("Invalid period");
                        Subscription subsc = this.GetSubscription(b.customerID, bp.toDate.Ticks);
                        if (subsc == null)
                            throw new ServerUserMessage("Invalid customer");
                        b.id = AutoIncrement.GetKey("LegacyBillID");
                        b.setOrderN = orderN;
                        b.setID = set.id;
                        foreach (LegacyBillItem i in b.items)
                        {
                            LegacyBillItemDefination def = this.lbGetLegacyBillItemDefinationInternal(dspWriter, i.itemID);
                            if (def == null)
                                throw new ServerUserMessage("Invalid item ID:" + i.itemID);
                            i.legacyBillID = b.id;
                            dspWriter.InsertSingleTableRecord(this.DBName, i);
                        }
                        dspWriter.InsertSingleTableRecord(this.DBName, b,new string[]{"__AID"},new object[]{AID});
                        orderN++;
                    }
                    dspWriter.CommitTransaction();
                    return set.id;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        // BDE exposed
        public bool lbIsAlreadyVerified(int ownSetID, VerificationEntry entry)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                int c= (int)dspReader.ExecuteScalar(string.Format(
                    @"Select count(*) from {0}.dbo.LegacyBill b inner join {0}.dbo.LegacyBillSet s
                on b.setID=s.ID
                where customerID={1} and periodID={2} and setID<>{3} and verfied=1"
                    , this.DBName, entry.bill.customerID, entry.bill.periodID,ownSetID));
                return c > 0;

            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        // BDE exposed
        public LegacyBillSet lbGetVerifiedSetByCustomerAndPeriod(int customerID, int periodID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                object id = dspReader.ExecuteScalar(string.Format(
                        @"Select setID from {0}.dbo.LegacyBill b inner join {0}.dbo.LegacyBillSet s
                        on b.setID=s.ID where customerID={1} and periodID={2} and verfied=1",this.DBName,customerID,periodID));
                if (id is int)
                    return lbGetLegacyBillSet((int)id);
                return null;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        // BDE exposed
        public int lbSaveLegacyVerificationSet(int AID, ReadingEntryClerk clerk, LegacyBillSet set, VerificationEntry[] bills)
        {
            if(clerk==null)
                throw new ServerUserMessage("Please login with user account registered as reading encoder");
            int userID = ApplicationServer.SecurityBDE.GetUIDForName(set.encoderUserID);
            if (userID == -1)
                throw new ServerUserMessage("Invalid user ID:" + set.encoderUserID);
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    if (set.id != -1)
                    {
                        lbDeleteSet(AID, set.id);
                    }
                    else
                        set.id = AutoIncrement.GetKey("LegacyBillSetID");
                    set.verfied = true;
                    dspWriter.InsertSingleTableRecord(this.DBName, set, new string[] { "__AID" }, new object[] { AID });
                    int orderN = 1;
                    foreach (VerificationEntry b in bills)
                    {
                        if (b.subsc != null)
                        {
                            string conNo = b.subsc.Kebele.ToString("00") + "-" + b.subsc.contractNo;
                            Subscription ex = GetSubscription(conNo, GetBillPeriod(b.bill.periodID).toDate.Ticks);
                            if (ex == null)
                            {
                                b.subsc.contractNo = conNo;
                                b.subsc.subscriber.customerCode = "OC" + b.subsc.contractNo;
                                b.subsc.subscriber.id = RegisterSubscriber(AID, b.subsc.subscriber);
                                b.subsc.subscriberID = b.subsc.subscriber.id;
                                b.subsc.subscriptionStatus = SubscriptionStatus.Active;
                                b.subsc.ticksFrom= new DateTime(1980,1,1,0,0,0).Ticks;
                                b.subsc.id = AddSubscription(AID, b.subsc);
                                b.bill.customerID = b.subsc.id;
                            }
                            else
                            {
                                ex.subscriber.name = b.subsc.subscriber.name;
                                UpdateSubscriber(AID, ex.subscriber, new Subscription[0]);
                                b.bill.customerID = ex.id;
                            }
                        }               

                        
                        BillPeriod bp = this.GetBillPeriod(b.bill.periodID);
                        if (bp == null)
                            throw new ServerUserMessage("Invalid period");
                        Subscription subsc = this.GetSubscription(b.bill.customerID, bp.toDate.Ticks);
                        if (subsc == null)
                            throw new ServerUserMessage($"Invalid customer: {b.bill.customerID}");
                        int [] existing=dspWriter.GetColumnArray<int>(string.Format(
                            "Select id from {0}.dbo.LegacyBill where customerID={1} and periodID={2}"
                            ,this.DBName,b.bill.customerID,b.bill.periodID),0);
                        
                        foreach (int e in existing)
                        {
                            LegacyBill lb = lbGetLegacyBill(e);
                            LegacyBillSet thisset = lbGetLegacyBillSet(lb.setID);
                            if(thisset.verfied)
                                throw new ServerUserMessage("This bill is already inlucded in verified set "+thisset.description);

                            dspWriter.logDeletedData(AID, this.DBName + ".dbo.LegacyBillItem", "legacyBillID=" + e);
                            dspWriter.Delete(this.DBName, "LegacyBillItem", "legacyBillID=" + e);
                            dspWriter.logDeletedData(AID, this.DBName + ".dbo.LegacyBill", "id=" + e);
                            dspWriter.Delete(this.DBName, "LegacyBill", "id=" + e);
                        }
                        foreach (LegacyBill lb in lbFindByBillNo(b.bill.reference))
                        {
                            LegacyBillSet thisset = lbGetLegacyBillSet(lb.setID);
                            if (thisset.verfied)
                                throw new ServerUserMessage("This bill no is already used in verified set " + thisset.description);
                        }

                        b.bill.id = AutoIncrement.GetKey("LegacyBillID");
                        b.bill.setOrderN = orderN;
                        b.bill.setID = set.id;
                        foreach (LegacyBillItem i in b.bill.items)
                        {
                            LegacyBillItemDefination def = this.lbGetLegacyBillItemDefinationInternal(dspWriter, i.itemID);
                            if (def == null)
                                throw new ServerUserMessage("Invalid item ID:" + i.itemID);
                            i.legacyBillID = b.bill.id;
                            dspWriter.InsertSingleTableRecord(this.DBName, i, new string[] { "__AID" }, new object[] { AID });
                        }
                        dspWriter.InsertSingleTableRecord(this.DBName, b.bill, new string[] { "__AID" }, new object[] { AID });
                        int blockID=BWFGetReadingBlockID(b.bill.customerID, b.bill.periodID);
                        if (blockID == -1)
                        {
                            BWFReadingBlock[] block = GetBlock(b.bill.periodID, OB_BLOCK);
                            if (block.Length == 0)
                            {
                                BWFReadingBlock blk = new BWFReadingBlock();
                                blk.blockName = OB_BLOCK;
                                blk.periodID = b.bill.periodID;
                                MeterReaderEmployee emp = GetMeterReadingEmployee(clerk.employeeID);
                                if (emp == null)
                                {
                                    emp = new MeterReaderEmployee();
                                    emp.employeeID = clerk.employeeID;
                                    emp.periodID = b.bill.periodID;
                                    CreateMeterReaderEmployee(emp);
                                }
                                blk.readerID = emp.employeeID;
                                blk.readingStart = blk.readingEnd = DateTime.Now;
                                blockID = BWFCreateReadingBlock(AID, clerk, blk);
                            }
                            else if (block.Length == 1)
                            {
                                blockID = block[0].id;
                            }
                            else 
                                throw new ServerUserMessage("Multiple " + OB_BLOCK + " blocks founds");
                        }
                        BWFMeterReading reading = BWFGetMeterReading(b.bill.customerID,blockID);
                        if (reading == null)
                        {
                            reading = new BWFMeterReading();
                            reading.readingBlockID = blockID;
                            reading.readingType = MeterReadingType.MeterReset;
                            reading.bwfStatus = BWFStatus.Read;
                            reading.subscriptionID = b.bill.customerID;
                            reading.periodID = b.bill.periodID;
                            reading.reading = b.bill.reading;
                            reading.consumption = b.bill.consumption;
                            BWFAddMeterReading(AID, clerk, reading,false);
                        }
                        else
                        {
                            if (!AccountBase.AmountEqual(reading.reading, b.bill.reading) ||
                                !AccountBase.AmountEqual(reading.consumption, b.bill.consumption)
                                )
                            {
                                reading.readingType = MeterReadingType.MeterReset;
                                reading.bwfStatus = BWFStatus.Read;
                                reading.reading = b.bill.reading;
                                reading.consumption = b.bill.consumption;
                                BWFUpdateMeterReading(AID, clerk, reading);
                            }
                        }
                        
                        orderN++;
                    }
                    dspWriter.CommitTransaction();
                    return set.id;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        // BDE exposed
        public LegacyBill[] lbGetLegacyBills(int setID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                LegacyBill[] ret = dspReader.GetSTRArrayByFilter<LegacyBill>("setID=" + setID);
                foreach (LegacyBill lb in ret)
                {
                    lb.items = dspReader.GetSTRArrayByFilter<LegacyBillItem>("legacyBillID=" + lb.id);
                }
                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        public LegacyBill lbGetLegacyBill(int lbID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                LegacyBill[] ret = dspReader.GetSTRArrayByFilter<LegacyBill>("id=" + lbID);
                if (ret.Length == 0)
                    return null;
                ret[0].items = dspReader.GetSTRArrayByFilter<LegacyBillItem>("legacyBillID=" + ret[0].id);
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public LegacyBillSet lbGetLegacyBillSet(int setID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                LegacyBillSet[] ret = dspReader.GetSTRArrayByFilter<LegacyBillSet>("id=" + setID);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public LegacyBill[] lbFindByBillNo(string billNo)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                LegacyBill[] ret= dspReader.GetSTRArrayByFilter<LegacyBill>("reference='" + billNo + "'");
                foreach (LegacyBill lb in ret)
                {
                    lb.items = dspReader.GetSTRArrayByFilter<LegacyBillItem>("legacyBillID=" + lb.id);
                }
                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public LegacyBill[] lbFindBySubscriberAndPeriod(int conID, int periodID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                LegacyBill[] ret= dspReader.GetSTRArrayByFilter<LegacyBill>(string.Format("customerID={0} and periodID={1}", conID, periodID));
                foreach (LegacyBill lb in ret)
                {
                    lb.items = dspReader.GetSTRArrayByFilter<LegacyBillItem>("legacyBillID=" + lb.id);
                }
                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public LegacyBillSet[] lbGetSet(string userID, int pageIndex, int pageSize, out int N)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                string cr = string.IsNullOrEmpty(userID) ? null : "encoderUserID='" + userID + "'";
                return dspReader.GetSTRArrayByFilter<LegacyBillSet>(cr, pageIndex, pageSize, out N);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public LegacyBillItemDefination[] lbGetAllItemDefinations()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<LegacyBillItemDefination>(null);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        private LegacyBillItemDefination lbGetLegacyBillItemDefinationInternal(RDBMS.SQLHelper dsp, int itemDefinationID)
        {
            LegacyBillItemDefination[] ret = dsp.GetSTRArrayByFilter<LegacyBillItemDefination>("id=" + itemDefinationID);
            if (ret.Length == 0)
                return null;
            return ret[0];
        }
        // BDE exposed
        public void generateOutstandingBills(int AID, int customerID)
        {
            Subscription[] ss = GetSubscriptions(customerID, DateTime.Now.Ticks);
            if (ss.Length == 0)
                return;
            string list = "";
            foreach (Subscription s in ss)
            {
                list = INTAPS.StringExtensions.AppendOperand(list, ",", s.id.ToString());
            }

            this.WriterHelper.BeginTransaction();
            try
            {
                this.WriterHelper.setReadDB(this.DBName);
                Console.WriteLine("Retreiving bills list");

                int[] billIDs = this.WriterHelper.GetColumnArray<int>(@"SELECT     LegacyBill.id
                                FROM         Subscriber.dbo.LegacyBill INNER JOIN
                                Subscriber.dbo.LegacyBillSet ON LegacyBill.setID = LegacyBillSet.id
                                WHERE     (LegacyBillSet.verfied = 1) and not exists(Select * from 
                                Subscriber.dbo.CustomerBill where periodID=[LegacyBill].periodID and connectionID=[LegacyBill].customerID ) and Subscriber.dbo.LegacyBill.customerID in (" + list + ")", 0);
                int c = billIDs.Length;
                Console.WriteLine();
                Dictionary<int, LegacyBillItemDefination> defs = new Dictionary<int, LegacyBillItemDefination>();
                foreach (LegacyBillItemDefination def in this.WriterHelper.GetSTRArrayByFilter<LegacyBillItemDefination>(null))
                    defs.Add(def.id, def);
                
                foreach (int b in billIDs)
                {
                    LegacyBill bill = this.lbGetLegacyBill(b);
                    Subscription subsc = this.GetSubscription(bill.customerID, DateTime.Now.Ticks);
                    BWFMeterReading read = new BWFMeterReading();
                    read.periodID = bill.periodID;
                    read.readingBlockID = -1;
                    read.subscriptionID = bill.customerID;
                    read.reading = bill.reading;
                    read.consumption = bill.consumption;

                    int typeID = this.Accounting.GetDocumentTypeByType(typeof(WaterBillDocument)).id;
                    foreach (CustomerBillRecord existing in this.getBills(typeID, subsc.subscriberID, subsc.id, bill.periodID))
                    {
                        throw new ServerUserMessage("Outstanding bill already exists");
                    }
                    CustomerBillRecord rec = new CustomerBillRecord();
                    rec.billDate = DateTime.Now;
                    rec.billDocumentTypeID = typeID;
                    rec.customerID = subsc.subscriberID;
                    rec.connectionID = subsc.id;
                    rec.periodID = bill.periodID;
                    rec.postDate = DateTime.Now;
                    rec.draft = false;
                    WaterBillDocument billDoc = new WaterBillDocument();
                    billDoc.period = this.GetBillPeriod(rec.periodID);
                    billDoc.subscription = subsc;
                    billDoc.reading = read;

                    List<BillItem> items = new List<BillItem>();
                    foreach (LegacyBillItem item in bill.items)
                    {
                        if (!AccountBase.AmountEqual(item.amount, 0))
                        {
                            BillItem billItem = new BillItem();
                            billItem.description = defs[item.itemID].description + " " + billDoc.period.name;
                            billItem.accounting = BillItemAccounting.Invoice;
                            billItem.hasUnitPrice = false;
                            billItem.price = item.amount;
                            switch (item.itemID)
                            {
                                case 1:
                                    billItem.itemTypeID = 0;
                                    billItem.incomeAccountID = this.SysPars.incomeAccount;
                                    break;
                                case 2:
                                    billItem.itemTypeID = 1;
                                    billItem.incomeAccountID = this.SysPars.rentIncomeAccount;
                                    break;
                                case 4:
                                    billItem.itemTypeID = 2;
                                    billItem.incomeAccountID = this.SysPars.additionalFeeIncomeAcount;
                                    break;
                                case 3:
                                    billItem.itemTypeID = 3;
                                    billItem.incomeAccountID = this.SysPars.otherBillIncomeAccount;
                                    break;
                            }
                            items.Add(billItem);
                        }
                    }
                    billDoc.waterBillItems = items.ToArray();
                    billDoc.ShortDescription = "Outstanding Bill";
                    billDoc.customer = subsc.subscriber;
                    billDoc.draft = false;
                    billDoc.summerizeTransaction = SysPars.summerizeTransactions;
                    billDoc.billBatchID = -1;
                    this.addBill(-1, billDoc, rec, items.ToArray());
                }
                this.WriterHelper.CommitTransaction();

            }
            catch (Exception bex)
            {
                Console.WriteLine(bex.Message);
                Console.WriteLine();
            }

        }
    }
}
