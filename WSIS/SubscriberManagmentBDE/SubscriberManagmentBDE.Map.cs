    using INTAPS;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.Evaluator;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;
using INTAPS.RDBMS;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using BIZNET.iERP;
using System.Net;
using System.Data.SqlClient;
namespace INTAPS.SubscriberManagment.BDE
{
    public delegate bool mapChekerDelegate(Subscription subsc);
    public partial class SubscriberManagmentBDE : BDEBase
    {
        public class MapServer
        {
            SubscriberManagmentBDE _bde;
            HttpListener listner = new HttpListener();
            public MapServer(SubscriberManagmentBDE bde)
            {
                _bde = bde;
                listner.Prefixes.Add("http://localhost:8080/");
                ApplicationServer.EventLoger.Log("Map Server:Listening..");
                listner.Start();
                new System.Threading.Thread(new ThreadStart(run)).Start();
            }
            ~MapServer()
            {
                listner.Stop();
            }
            void run()
            {
                while (true)
                {

                    HttpListenerContext context = listner.GetContext();
                    ApplicationServer.EventLoger.Log(EventLogType.Debug, context.ToString());
                    HttpListenerResponse response = context.Response;

                    const string responseString = "<html><body>Hello world</body></html>";

                    byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
                    response.ContentLength64 = buffer.Length;

                    System.IO.Stream output = response.OutputStream;

                    output.Write(buffer, 0, buffer.Length);
                    ApplicationServer.EventLoger.Log(EventLogType.Debug, "Map server: wrote output");
                    output.Close();
                }
            }
        }

        void initMapServer()
        {
            //new MapServer(this);
        }
        // BDE exposed
        public void setSubscriptionCoordinate(int AID, int subcriptionID, long version, double x, double y)
        {
            Subscription subsc = GetSubscription(subcriptionID, version);
            if (subsc == null)
                throw new ServerUserMessage("Connection doesn't exist");
            subsc.waterMeterX = x;
            subsc.waterMeterY = y;
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.Subscription", SQLHelper.getVersionDataFilter(version, "id=" + subcriptionID));
                    dspWriter.UpdateSingleTableRecord(this.DBName, subsc, new string[] { "__AID" }, new object[] { AID });
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        public void generateMap(mapChekerDelegate checker, long version)
        {
            checkAndBuildSubscriptionCache();
            List<MeterMarker> ret = new List<MeterMarker>();
            foreach(Subscription s in cache_subsc)
            {
                if (s.ticksFrom > version
                    || (s.timeBinding == DataTimeBinding.LowerAndUpperBound && s.ticksTo<= version))
                    continue;
                if (!s.hasCoordinate)
                    continue;
                if (!checker(s))
                    break;
            }

        }
        public void generateUnreadMetersMap(mapChekerDelegate checker, long version)
        {
            //checkAndCasheSubscriptionCache();
            Subscription[] subscription = getUnAllocatedConnections(version);
            List<MeterMarker> ret = new List<MeterMarker>();
            foreach (Subscription s in subscription)
            {
                if (s.ticksFrom > version
                    || (s.timeBinding == DataTimeBinding.LowerAndUpperBound && s.ticksTo <= version))
                    continue;
                if (!s.hasCoordinate)
                    continue;
                if (!checker(s))
                    break;
            }

        }

        private Subscription[] getUnAllocatedConnections(long version)
        {
                INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
                try
                {


                    string sql = @"SELECT     Subscription.id
FROM         BWFReadingBlock INNER JOIN
                      BWFMeterReading ON BWFReadingBlock.id = BWFMeterReading.readingBlockID RIGHT OUTER JOIN
                      Subscriber INNER JOIN
                      Subscription ON Subscriber.id = Subscription.subscriberID ON BWFMeterReading.subscriptionID = Subscription.id 
                      AND BWFMeterReading.periodID = {0}                      
WHERE     (Subscription.ticksFrom <= {1}) AND (Subscription.ticksTo = - 1 OR
                      Subscription.ticksTo > {1}) AND (Subscription.subscriptionStatus = 2) 
                      AND (BWFMeterReading.bwfStatus is null or  BWFReadingBlock.blockName='Unallocted-Estimated')
ORDER BY Subscription.kebele, Subscription.subscriptionType";
                    int periodID=SysPars.readingPeriod;
                    DataTable dt = dspReader.GetDataTable(string.Format(sql, periodID, version));
                    List<Subscription> subscription = new List<Subscription>();
                    foreach (DataRow row in dt.Rows)
                    {
                        Subscription s = dspReader.GetSTRArrayByFilter<Subscription>("id=" + row[0])[0];
                        subscription.Add(s);
                    }
                    return subscription.ToArray();
                }
            finally
                {
                    base.ReleaseHelper(dspReader);
                }
        }
        // BDE exposed
        public MeterMap getMetersNearBy(int readerID, double lat0, double lng0, double radius, long version)
        {
             List<MeterMarker> ret = new List<MeterMarker>();

            generateMap(delegate(Subscription subsc)
            {
                    double lat = subsc.waterMeterX;
                    double lng = subsc.waterMeterY;
                    double dist = Subscription.sphericalDistance(lat0, lng0, lat, lng);
                    if (dist <= radius)
                    {
                       
                        MeterMarker m = new MeterMarker();
                        m.symbol = MeterMarkerSymbol.Default;
                        m.lat = lat;
                        m.lng = lng;
                        m.connectionCode = subsc.contractNo;
                        m.label = m.connectionCode;
                        m.color = System.Drawing.Color.Orange.ToArgb();
                        ret.Add(m);
                    }
                return true;
            },version);
                MeterMap map = new MeterMap();
                map.layerName = "Meters Nearby";
                map.markers = ret.ToArray();
                return map;
        }

        // BDE exposed
        public MeterMap getAllMetersMap(long version)
        {
            List<MeterMarker> ret = new List<MeterMarker>();

            generateMap(delegate(Subscription subsc)
            {
                if (subsc.hasCoordinate)
                {
                    MeterMarker m = new MeterMarker();
                    m.symbol = MeterMarkerSymbol.Default;
                    m.lat = subsc.waterMeterX;
                    m.lng = subsc.waterMeterY;
                    m.connectionCode = subsc.contractNo;
                    m.label = m.connectionCode;
                    m.color = System.Drawing.Color.Orange.ToArgb();
                    ret.Add(m);
                }
                return true;
            }, version);
            MeterMap map = new MeterMap();
            map.layerName = "Meters Nearby";
            map.markers = ret.ToArray();
            return map;

        }
        public MeterMap getUnreadMetersMap(long version)
        {
            List<MeterMarker> ret = new List<MeterMarker>();

            generateUnreadMetersMap(delegate(Subscription subsc)
            {
                if (subsc.hasCoordinate)
                {
                    MeterMarker m = new MeterMarker();
                    m.symbol = MeterMarkerSymbol.Default;
                    m.lat = subsc.waterMeterX;
                    m.lng = subsc.waterMeterY;
                    m.connectionCode = subsc.contractNo;
                    m.label = m.connectionCode;
                    m.color = System.Drawing.Color.Orange.ToArgb();
                    ret.Add(m);
                }
                return true;
            }, version);
            MeterMap map = new MeterMap();
            map.layerName = "Meters Nearby";
            map.markers = ret.ToArray();
            return map;
        }
        // BDE exposed
        public MeterMap getReadingMap(int periodID)
        {
            List<MeterMarker> ret = new List<MeterMarker>();
            BillPeriod prd = GetBillPeriod(periodID);
            Dictionary<int, BWFMeterReading> reading = new Dictionary<int, BWFMeterReading>();

            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                SQLObjectReader<BWFMeterReading> reader = dspReader.GetObjectReader<BWFMeterReading>("periodID=" + periodID);
                while (reader.Read())
                {
                    reading.Add(reader.Current.subscriptionID, reader.Current);
                }
                reader.Close();
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
            generateMap(delegate(Subscription subsc)
            {
                MeterMarker m = new MeterMarker();
                m.symbol = MeterMarkerSymbol.Default;
                m.lat = subsc.waterMeterX;
                m.lng = subsc.waterMeterY;
                m.connectionCode = subsc.contractNo;
                m.label = m.connectionCode;
                m.color = System.Drawing.Color.Orange.ToArgb();
                ret.Add(m);

                //BWFMeterReading mr = BWFGetMeterReadingByPeriodID(subsc.id, prd.id);
                BWFMeterReading mr;
                if (!reading.TryGetValue(subsc.id, out mr))
                    mr = null;
                if (mr != null)
                {

                    MeterMarker m2 = new MeterMarker();
                    if (mr.bwfStatus == BWFStatus.Read)
                        m2.symbol = MeterMarkerSymbol.Read;
                    else
                        switch (mr.readingProblem)
                        {
                            case MeterReadingProblem.NoProblem:
                                m2.symbol = MeterMarkerSymbol.Unread;
                                break;
                            default:
                                m2.symbol = MeterMarkerSymbol.ProblemReading1;
                                break;
                        }
                    m2.lat = mr.readingX;
                    m2.lng = mr.readingY;
                    m2.connectionCode = subsc.contractNo;
                    m2.label = m.connectionCode;
                    m2.color = System.Drawing.Color.Orange.ToArgb();
                    if (mr.hasCoordinate)
                    {
                        ret.Remove(m);
                        ret.Add(m2);
                    }
                    else
                        m.symbol = m2.symbol;

                }
                return true;
            }, prd.toDate.Ticks);
            MeterMap map = new MeterMap();
            map.layerName = "Meters Nearby";
            map.markers = ret.ToArray();
            return map;
        }
        // BDE exposed
        public MeterMap getMineOthers(int periodID, int readerID)
        {
            List<MeterMarker> ret = new List<MeterMarker>();

            BillPeriod prd = GetBillPeriod(periodID);
            Dictionary<int, int> blocks = new Dictionary<int, int>();
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                string sql = @"SELECT     r.subscriptionID, ISNULL(b.readerID, - 1) AS readerID
FROM         BWFReadingBlock AS b RIGHT OUTER JOIN
                      BWFMeterReading AS r ON b.id = r.readingBlockID 
                      and b.periodID=r.periodID
                      where b.periodID = " +periodID;
                foreach(DataRow row in reader.GetDataTable(sql).Rows)
                {
                    blocks.Add((int)row[0], (int)row[1]);
                }
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
            generateMap(delegate(Subscription subsc)
            {
                //int blockID = BWFGetReadingBlockID(subsc.id, prd.id);
                int thisReaderID = blocks.ContainsKey(subsc.id) ? blocks[subsc.id] : -1;
                //if (blockID != -1)
                {
                    if (thisReaderID== readerID)
                    {
                        MeterMarker m = new MeterMarker();
                        m.symbol = MeterMarkerSymbol.SelfMeter;
                        m.lat = subsc.waterMeterX;
                        m.lng = subsc.waterMeterY;
                        m.connectionCode = subsc.contractNo;
                        m.label = m.connectionCode;
                        m.color = System.Drawing.Color.Orange.ToArgb();
                        ret.Add(m);
                        return true;
                    }
                }
                MeterMarker m2 = new MeterMarker();
                m2.symbol = MeterMarkerSymbol.OtherMeter;
                m2.lat = subsc.waterMeterX;
                m2.lng = subsc.waterMeterY;
                m2.connectionCode = subsc.contractNo;
                m2.label = m2.connectionCode;
                m2.color = System.Drawing.Color.Orange.ToArgb();
                ret.Add(m2);
                return true;
            }, prd.toDate.Ticks);
            MeterMap map = new MeterMap();
            map.layerName = "Meters Allocation";
            map.markers = ret.ToArray();
            return map;
        }

        class ReadingPathComparer : IComparer<BWFMeterReading>
        {
            public int Compare(BWFMeterReading x, BWFMeterReading y)
            {
                if (x == null && y == null)
                    return 0;
                if (x == null)
                    return -1;
                if (y == null)
                    return -1;
                bool hasTime1=(x.extraInfo & ReadingExtraInfo.ReadingDate) == ReadingExtraInfo.ReadingDate;
                bool hasTime2=(y.extraInfo & ReadingExtraInfo.ReadingDate) == ReadingExtraInfo.ReadingDate;
                if (hasTime1 && hasTime2)
                {
                    return x.readingTime.CompareTo(y.readingTime);
                }
                if (hasTime1)
                    return 1;
                if (hasTime2)
                    return -1;
                return x.orderN.CompareTo(y.orderN);
            }
        }
        // BDE exposed
        public ReadingPath getReadingPath(int periodID, int readerID, out int notMapped)
        {
            List<MeterMarker> ret = new List<MeterMarker>();

            BillPeriod prd = GetBillPeriod(periodID);
            INTAPS.RDBMS.SQLHelper reader=GetReaderHelper();
            try
            {
                notMapped = 0;
                List<BWFMeterReading> readings = new List<BWFMeterReading>();
                foreach (BWFMeterReading r in reader.GetSTRArrayByFilter<BWFMeterReading>(string.Format("readingBlockID in (Select id from BWFReadingBlock where readerID={0} and periodID={1}) and periodID={1} and (bwfStatus=1 or extraInfo<>0)", readerID, periodID)))
                {
                    if (r.hasCoordinate)
                    {
                        readings.Add(r);
                    }
                }
                readings.Sort(new ReadingPathComparer());
                foreach (BWFMeterReading r in readings)
                {
                    MeterMarker marker = new MeterMarker();
                    marker.color = System.Drawing.Color.Green.ToArgb();
                    marker.connectID = r.subscriptionID;
                    marker.connectionCode = GetSubscription(r.subscriptionID, DateTime.Now.Ticks).contractNo;
                    if ((r.extraInfo & ReadingExtraInfo.Remark) == ReadingExtraInfo.Remark)
                        marker.label = r.readingRemark;
                    if ((r.extraInfo & ReadingExtraInfo.ReadingDate) == ReadingExtraInfo.ReadingDate)
                    {
                        if (!string.IsNullOrEmpty(marker.label))
                            marker.label += "</br>";
                        marker.label += System.Web.HttpUtility.HtmlEncode(r.readingTime.ToString());
                    }
                    marker.lat = r.readingX;
                    marker.lng = r.readingY;
                    marker.symbol = r.bwfStatus == BWFStatus.Read ? MeterMarkerSymbol.PathRead : MeterMarkerSymbol.PathUnread;
                    ret.Add(marker);
                }
                
                ReadingPath map = new ReadingPath();
                map.layerName = "Reading Path";
                map.markers = ret.ToArray();
                return map;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
           
        }

        // BDE exposed
        public ReadingPath getReadingPath(int periodID, int readerID, DateTime d1, DateTime d2, out int notMapped)
        {
            List<MeterMarker> ret = new List<MeterMarker>();

            BillPeriod prd = GetBillPeriod(periodID);
            INTAPS.RDBMS.SQLHelper reader = GetReaderHelper();
            try
            {
                notMapped = 0;
                List<BWFMeterReading> readings = new List<BWFMeterReading>();
                foreach (BWFMeterReading r in reader.GetSTRArrayByFilter<BWFMeterReading>(string.Format("readingBlockID in (Select id from BWFReadingBlock where readerID={0} and periodID={1}) and periodID={1} and (bwfStatus=1 or (extraInfo%2=1 and (extraInfo/2)%2=1)) and readingTime>='{2}' and readingTime<'{3}'", readerID, periodID,d1,d2)))
                {
                    if (r.hasCoordinate)
                    {
                        readings.Add(r);
                    }
                }
                readings.Sort(new ReadingPathComparer());
                foreach (BWFMeterReading r in readings)
                {
                    MeterMarker marker = new MeterMarker();
                    marker.color = System.Drawing.Color.Green.ToArgb();
                    marker.connectID = r.subscriptionID;
                    marker.connectionCode = GetSubscription(r.subscriptionID, DateTime.Now.Ticks).contractNo;
                    if ((r.extraInfo & ReadingExtraInfo.Remark) == ReadingExtraInfo.Remark)
                        marker.label = r.readingRemark;
                    if ((r.extraInfo & ReadingExtraInfo.ReadingDate) == ReadingExtraInfo.ReadingDate)
                    {
                        if (!string.IsNullOrEmpty(marker.label))
                            marker.label += "</br>";
                        marker.label += System.Web.HttpUtility.HtmlEncode(r.readingTime.ToString());
                    }
                    marker.lat = r.readingX;
                    marker.lng = r.readingY;
                    marker.symbol = r.bwfStatus == BWFStatus.Read ? MeterMarkerSymbol.PathRead : MeterMarkerSymbol.PathUnread;
                    ret.Add(marker);
                }

                ReadingPath map = new ReadingPath();
                map.layerName = "Reading Path";
                map.markers = ret.ToArray();
                return map;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }

        }

        // BDE exposed
        public DateTime[] getReadingDates(int periodID, int readerID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                DataTable table = reader.GetDataTable(string.Format(@"Select distinct datepart(year,readingTime) as y,datepart(month,readingTime) as m,datepart(day,readingTime) as d from {0}.dbo.BWFMeterReading r inner join {0}.dbo.BWFReadingBlock b
on r.readingBlockID=b.id
where readerID={2} and r.periodID={1} and extraInfo%2=1 and (extraInfo/2)%2=1", this.DBName, periodID, readerID));
                List<DateTime> ret = new List<DateTime>();
                foreach (DataRow row in table.Rows)
                {
                    int y = (int)row[0];
                    int m = (int)row[1];
                    int d = (int)row[2];
                    ret.Add(new DateTime(y, m, d));
                }
                ret.Sort();
                return ret.ToArray();
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        List<Subscription> cache_subsc = null;
        void clearSubscriptionCache()
        {
            cache_subsc = null;
        }
        void checkAndBuildSubscriptionCache()
        {
            if (cache_subsc != null)
                return;
            Console.WriteLine("Building subscription cashe");
             INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                int c = 0;
                Console.WriteLine();
                cache_subsc = new List<Subscription>();
                Dictionary<int, Subscriber> allSusbc = new Dictionary<int, Subscriber>();
                foreach (Subscriber sr in dspReader.GetSTRArrayByFilter<Subscriber>(null))
                    allSusbc.Add(sr.id, sr);
                foreach (Subscription subsc in dspReader.GetSTRArrayByFilter<Subscription>(null))
                {
                    //Console.CursorTop--;
                    Console.WriteLine((c++) + "                  ");
                    //subsc.subscriber=GetSubscriber(subsc.subscriberID);
                    subsc.subscriber = allSusbc.ContainsKey(subsc.subscriberID) ? allSusbc[subsc.subscriberID] : null;
                    cache_subsc.Add(subsc);
                }

            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
            
        }

        // BDE exposed
        public MeterMap disconnectCandidate(int readingPeriodID)
        {
            return null;
        }
                 //BDE exposed
        public MeterMap locateWaterMeter(int connectionID, long version)
        {
            //checkAndCasheSubscriptionCache();
            Subscription test = GetSubscription(connectionID, version);
            if (!test.hasCoordinate)
                return null;
            List<MeterMarker> ret = new List<MeterMarker>();

            generateMap(delegate(Subscription subsc)
            {

                if (subsc.id == connectionID)
                {
                    MeterMarker m = new MeterMarker();
                    m.symbol = MeterMarkerSymbol.LocatedMeter;
                    m.lat = subsc.waterMeterX;
                    m.lng = subsc.waterMeterY;
                    m.connectionCode = subsc.contractNo;
                    m.label = m.connectionCode;
                    m.color = System.Drawing.Color.Orange.ToArgb();
                    ret.Add(m);
                }
                else
                {
                    MeterMarker m2 = new MeterMarker();
                    m2.symbol = MeterMarkerSymbol.Default;
                    m2.lat = subsc.waterMeterX;
                    m2.lng = subsc.waterMeterY;
                    m2.connectionCode = subsc.contractNo;
                    m2.label = m2.connectionCode;
                    m2.color = System.Drawing.Color.Orange.ToArgb();
                    ret.Add(m2);
                }

                return true;

            }, version);
            MeterMap map = new MeterMap();
            map.layerName = "Meters Allocation";
            map.markers = ret.ToArray();
            return map;

        }

        // BDE exposed
        public MeterMap singleMeterHistoricalReadingMap(int connectionID)
        {
            //checkAndCasheSubscriptionCache();
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                DataTable data = reader.GetDataTable(string.Format("Select periodID,[readingX],[readingY] from {0}.dbo.[BWFMeterReading] where method={1} and subscriptionID={2} and extraInfo%2=1"
                    , this.DBName, (int)ReadingMethod.ReaderMobileOffline,connectionID));
                MeterMap map = new MeterMap();
                int i=0;
                Subscription subsc = GetSubscription(connectionID, DateTime.Now.Ticks);
                if(subsc.hasCoordinate)
                {
                    map.markers = new MeterMarker[data.Rows.Count+1];
                }
                else
                    map.markers = new MeterMarker[data.Rows.Count];
                map.layerName = "Meter Reading Locations";
                
                foreach (DataRow row in data.Rows)
                {
                    int periodID = (int)row[0];
                    double x = (double)row[1];
                    double y = (double)row[2];
                    map.markers[i] = new MeterMarker();
                    map.markers[i].connectID = connectionID;
                    map.markers[i].color = System.Drawing.Color.Red.ToArgb();
                    map.markers[i].connectionCode = subsc.contractNo;
                    map.markers[i].label = map.markers[i].connectionCode;
                    map.markers[i].lat = x;
                    map.markers[i].lng = y;
                    map.markers[i].symbol = MeterMarkerSymbol.Read;
                    i++;
                }
                if (subsc.hasCoordinate)
                {
                    map.markers[i] = new MeterMarker();
                    map.markers[i].connectID = connectionID;
                    map.markers[i].color = System.Drawing.Color.Red.ToArgb();
                    map.markers[i].connectionCode = subsc.contractNo;
                    map.markers[i].label = map.markers[i].connectionCode;
                    map.markers[i].lat = subsc.waterMeterX;
                    map.markers[i].lng = subsc.waterMeterY;
                    map.markers[i].symbol = MeterMarkerSymbol.Default;
                }
                i++;
                return map;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public void analyzeReading(int AID, int periodID, bool updateConnectionLocations)
        {
            ReadingAnalysisMethod method = m_sysPars.analysisMethod;
            if(method==null)
            {
                throw new ServerUserMessage("Analysis method not set.");
            }
            if(method.nPeriod<1)
                throw new ServerUserMessage("Number of last periods should be at least 1. Please fix the method setting");
            if (method.minLat < -90 || method.maxlat > 90 || method.minLng < -180 || method.maxLng > 189)
                throw new ServerUserMessage("Invalid geographic bound");

            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                BillPeriod[] prds = new BillPeriod[method.nPeriod];
                int i = method.nPeriod-1;
                BillPeriod prd=GetBillPeriod(periodID);
                prds[i] =prd;
                i--;
                while(i>=0)
                {
                    prd=GetPreviousBillPeriod(prd.id);
                    prds[i] = prd;
                    i--;
                }
                this.bdeAccounting.clearProgress();
                this.bdeAccounting.PushProgress("Loading connection");
                Console.WriteLine("Loading connections");
                Subscription[] connections = reader.GetSTRArrayByFilter<Subscription>("timeBinding=1");
                Console.WriteLine("{0} connections loaded", connections.Length);
                this.bdeAccounting.SetProgress(0.2);
                int c = connections.Length;
                Console.WriteLine();
                List<ReadingAnalysisRecord> records = new List<ReadingAnalysisRecord>();
                ReadingAnalysisHeader header=new ReadingAnalysisHeader();
                header.periodID=periodID;
                header.method=m_sysPars.analysisMethod;
                bool firstLocation=true;
                this.bdeAccounting.SetChildWeight(0.7);
                this.bdeAccounting.PushProgress("Analhyzing readings");
                foreach(Subscription con in connections)
                {
                    //Console.CursorTop--;
                    Console.WriteLine(c-- + "              ");
                    ReadingAnalysisRecord rec = new ReadingAnalysisRecord();
                    rec.connectionID = con.id;
                    rec.periodID = periodID;
                    List<BWFMeterReading> rsample = new List<BWFMeterReading>();
                    List<BWFMeterReading> lsamples = new List<BWFMeterReading>();
                    
                    for(int j=0;j<prds.Length;j++)
                    {
                        BWFMeterReading reading = BWFGetMeterReadingByPeriod(con.id, prds[j].id);
                        if (reading == null || reading.bwfStatus != BWFStatus.Read)
                            continue;
                        rec.nReading++;
                        rec.averageConsumption += reading.consumption;
                        rsample.Add(reading);
                        if((reading.extraInfo&ReadingExtraInfo.Coordinate)==ReadingExtraInfo.Coordinate)
                        {
                            if(firstLocation)
                            {
                                header.nLocation = 1;
                                header.minLat = header.maxLat = reading.readingX;
                                header.minLng = header.maxLng = reading.readingY;
                            }
                            rec.nLocation++;
                            rec.medianX = reading.readingX;
                            rec.medianY = reading.readingY;
                            lsamples.Add(reading);
                        }
                        
                    }
                    rec.readingSamples = rsample.ToArray();
                    rec.locationSamples = lsamples.ToArray();

                    if (rec.nLocation > 0)
                    {
                        double[] x = new double[rec.locationSamples.Length];
                        double[] y = new double[rec.locationSamples.Length];
                        //x[0] = 0;
                        //y[0] = 0;
                        
                        for (int j = 0; j < rec.locationSamples.Length; j++)
                        {
                            /*double cx = Math.Sign(rec.locationSamples[j].readingX - rec.locationSamples[0].readingX) * 
                                Subscription.sphericalDistance(rec.locationSamples[0].readingX, rec.locationSamples[0].readingY
                                , rec.locationSamples[j].readingX, rec.locationSamples[0].readingY) * 1000;
                            double cy = Math.Sign(rec.locationSamples[j].readingY - rec.locationSamples[0].readingY) * 
                                Subscription.sphericalDistance(rec.locationSamples[0].readingX, rec.locationSamples[0].readingY
                                , rec.locationSamples[0].readingX, rec.locationSamples[j].readingY) * 1000;
                            x[j] = cx;
                            y[j] = cy;*/
                            x[j] = rec.locationSamples[j].readingX;
                            y[j] = rec.locationSamples[j].readingY;
                        }
                        
                        double mx, my;
                        
                        estimateGeometricMedian(x, y, 1e-6,1e-6, out mx, out my,out rec.confidence);
                        rec.medianX = mx;
                        rec.medianY = my;
                        header.nLocation += rec.nLocation;
                    }
                    
                    if(rec.nReading>0)
                    {
                        header.totalVolume +=rec.averageConsumption;
                        rec.averageConsumption = rec.averageConsumption / rec.nReading;
                        records.Add(rec);
                        header.nReading+=rec.nReading;                        
                    }
                    this.bdeAccounting.SetProgress( (double)(connections.Length - c) / (double)connections.Length);
                }
                this.bdeAccounting.PopProgress();
                if (header.nReading == 0)
                    throw new ServerUserMessage("No reading to analyse.");
                
                this.bdeAccounting.SetChildWeight(0.1);
                this.bdeAccounting.PushProgress("Saving data");
                c = 0;
                lock(dspWriter)
                {
                    ReadingAnalysisHeader existing = getReadingAnalysisHeader(periodID);
                    if (existing != null)
                    {
                        deleteReadingAnalysis(AID, existing.periodID);
                    }
                    
                    dspWriter.InsertSingleTableRecord(this.DBName, header,new string[]{"__AID"},new object[]{AID});
                    
                    foreach(ReadingAnalysisRecord arec in records)
                    {
                        dspWriter.InsertSingleTableRecord(this.DBName, arec, new string[] { "__AID" }, new object[] { AID });
                        c++;
                        this.bdeAccounting.SetProgress((double)c/(double)(records.Count));
                    }
                }
                this.bdeAccounting.PopProgress();
                this.bdeAccounting.SetProgress("Updating meters coordinate");
                string sql = @"UPDATE    Subscription
SET              waterMeterX = ReadingAnalysisRecord.medianX, waterMeterY = ReadingAnalysisRecord.medianY
FROM         {0}.dbo.ReadingAnalysisRecord INNER JOIN
                      {0}.dbo.Subscription ON ReadingAnalysisRecord.connectionID = Subscription.id
WHERE     (Subscription.timeBinding = 1) AND (ReadingAnalysisRecord.medianX > 0) and PeriodID={1}";
                this.bdeAccounting.SetProgress(1);
                this.bdeAccounting.PopProgress();
                dspWriter.ExecuteNonQuery(string.Format(sql, this.DBName, periodID));
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        private void estimateGeometricMedian(double[] x, double[] y, double tolx, double toly, out double medianX, out double medianY,out double confidence)
        {
            if(x.Length==0)
            {
                throw new ServerUserMessage("At least one point needed to calculated geometric median");
            }
            
            if(x.Length==1)
            {
                medianX = x[0];
                medianY = y[0];
                confidence = 1-Math.Exp(-1);
                return;
            }
            

            //Find the median in the set
            bool first = true;
            List<int> mindIndex=new List<int>();
            double minDistance = 0;
            for (int i = 0; i < x.Length; i++)
            {
                double total = 0;
                for (int j = 0; j < x.Length; j++)
                {
                    if (i == j)
                        continue;
                    //total += Math.Sqrt((x[i] - x[j]) * (x[i] - x[j]) + (y[i] - y[j]) * (y[i] - y[j]));
                    total += Subscription.sphericalDistance(x[i],y[i],x[j],y[j]);
                }
                if (first)
                {
                    first = false;
                    minDistance = total;
                    mindIndex.Add(i);
                }
                else
                {
                    if (minDistance == total)
                    {
                        mindIndex.Add(i);
                    }
                    else if (minDistance > total)
                    {
                        mindIndex = new List<int>(new int[] { i });
                        minDistance = total;
                    }
                }
            }
            double mx1 = 0;
            double my1 = 0;
            foreach (int index in mindIndex)
            {
                mx1 += x[index];
                my1 += y[index];
            }
            mx1 = mx1 / mindIndex.Count;
            my1 = my1 / mindIndex.Count;
            
            //search local minima
            double x0 = mx1;
            double y0 = my1;
            
            do
            {
                double[,] samples = new double[4,3];
                samples[0, 0] = x0 - tolx;
                samples[0, 1] = y0;

                samples[1, 0] = x0 ;
                samples[1, 1] = y0+toly;

                samples[2, 0] = x0+tolx;
                samples[2, 1] = y0;
                
                samples[3, 0] = x0;
                samples[3, 1] = y0-toly;
                
                int minSide = -1;
                
                for(int m=0;m<4;m++)
                {
                    double x1 = samples[m, 0];
                    double y1 = samples[m, 1];
                    double dist = 0;
                    for(int k=0;k<x.Length;k++)
                    {
                        dist += Subscription.sphericalDistance(x[k] ,y[k],x1,y1);
                    }
                    samples[m, 2] = dist;
                    if(dist<minDistance)
                    {
                        minSide = m;
                        minDistance = dist;
                    }
                }
                if(minSide==-1)
                {
                    break;
                }
                x0 = samples[minSide, 0];
                y0 = samples[minSide, 1];
            }
            while(true);
            
            medianX = x0;
            medianY = y0;

            confidence = 0;
            for (int k = 0; k < x.Length; k++)
            {
                confidence += Math.Exp(-0.1*Subscription.sphericalDistance(x[k] ,y[k],medianX,medianY));
            }
            confidence *= (1 - Math.Exp(-x.Length))/x.Length;
        }
        public ReadingAnalysisHeader getReadingAnalysisHeader(int periodID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                ReadingAnalysisHeader[] existing=dspWriter.GetSTRArrayByFilter<ReadingAnalysisHeader>("periodID="+periodID);
                    if(existing.Length==0)
                        return null;
                return existing[0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public void deleteReadingAnalysis(int AID,int periodID)
        {
            dspWriter.Delete(this.DBName,"ReadingAnalysisRecord", "periodID=" + periodID);
            dspWriter.Delete(this.DBName, "ReadingAnalysisHeader", "periodID=" + periodID);
        }
    }
}