using INTAPS;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.Evaluator;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;
using INTAPS.RDBMS;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using BIZNET.iERP;
namespace INTAPS.SubscriberManagment.BDE
{

    


    public partial class SubscriberManagmentBDE : BDEBase
    {
      
        // BDE exposed
        public int getLastMessageNumber()
        {
            return ApplicationServer.MsgRepo.getLastMessageNumber();
        }
        // BDE exposed
        public int getFirstMessageNumber()
        {
            return ApplicationServer.MsgRepo.getFirstMessageNumber();
        }
        // BDE exposed
        public int countMessages(int after)
        {
            return ApplicationServer.MsgRepo.countMessages(after);
        }
        // BDE exposed
        public MessageList getMessageList(int from, int to)
        {
            return ApplicationServer.MsgRepo.getMessageList(from, to);
        }
        // BDE exposed
        public List<int> postOfflineMessages(int AID, PaymentCenterMessageList msgList, bool assertSequence)
        {
            //if (!msgList.list.Verify())
            //{
            //    throw new ServerUserMessage("Corrupt update.");
            //}
            ApplicationServer.EventLoger.Log($"Processing {msgList.list.data.Length} messages from center ID: {msgList.paymentCenterID}");

            object _maxUpdateID = this.WriterHelper.ExecuteScalar(string.Format("Select max(updateID) from {0}.dbo.UpdateLog where centerID={1}", this.DBName, msgList.paymentCenterID));
            int lastUpdateID = (_maxUpdateID is int) ? ((int)_maxUpdateID) : -1;
            List<string> list = new List<string>();
            BillSettlementHandler handler = Accounting.GetDocumentHandler(Accounting.GetDocumentTypeByType(typeof(CustomerPaymentReceipt)).id) as BillSettlementHandler;
            List<int> ret = new List<int>();
            foreach (OfflineMessage msg in msgList.list.data)
            {
                Console.WriteLine($"Processing msg {msg.id}");
                if (assertSequence)
                {
                    if (msg.id <= lastUpdateID)
                    { 
                        ret.Add(-1);
                        Console.WriteLine($"Processing msg {msg.id} lest than last upate ID:{lastUpdateID}, skipped");
                        continue;
                    }
                    if (msg.previousMessageID != lastUpdateID)
                        throw new Exception("Unexpected update :" + msg.id);
                }
                if (msg.messageData is PaymentReceivedMessage)
                {
                    PaymentReceivedMessage payment = msg.messageData as PaymentReceivedMessage;
                    DocumentSerialType st = Accounting.getDocumentSerialType("BSN");
                    if (st == null)
                        throw new ServerUserMessage("Configure BSN document reference type");
                    if (payment.payment.receiptNumber != null)
                        payment.payment.receiptNumber.typeID = st.id;

                    payment.payment.ShortDescription = "Payment from offline center";
                    payment.payment.offline = true;

                    int id = postOfflineReceipt(AID, payment.payment);

                    OfflineMessage ack = new OfflineMessage();
                    ack.sentDate = DateTime.Now;
                    MessageAcknowledgeMessage ackMsg = new MessageAcknowledgeMessage();
                    ackMsg.messageID = msg.id;
                    ackMsg.paymentCenterID = msgList.paymentCenterID;
                    ack.messageData = ackMsg;
                    ApplicationServer.MsgRepo.addMessage(AID, ack);
                    ret.Add(id);
                }
                else
                {
                    Console.WriteLine($"Processing msg {msg.id} not PaymentReceivedMessage, skipped");
                    ret.Add(-1);
                }
                dspWriter.Insert(this.DBName, "UpdateLog", new string[] { "centerID", "updateID", "applyDate" }, new object[] { msgList.paymentCenterID, msg.id, DateTime.Now });
                lastUpdateID = msg.id;
            }
            return ret;
        }

        public int postOfflineReceipt(int AID, CustomerPaymentReceipt payment)
        {
            double nonExitantBillTotal = 0;
            double totalDoublePayments = 0;
            double totalBill = 0;
            List<int> exitingBill = new List<int>();
            
            for (int i = 0; i < payment.settledBills.Length; i++)
            {
                if (payment.receiptNumber == null)
                    Console.WriteLine(i);
                else
                    Console.WriteLine(payment.receiptNumber.reference + "-" + i);
                CustomerBillRecord doc = getCustomerBillRecord(payment.settledBills[i]);
                if (doc == null)
                {
                    continue;
                }
                double thisBillTotal = 0;
                foreach (BillItem item in getCustomerBillItems(payment.settledBills[i]))
                    thisBillTotal += item.netPayment;
                totalBill += thisBillTotal;


                if (doc.isPayedOrDiffered)
                {
                    totalDoublePayments += thisBillTotal;
                    continue;
                }
                exitingBill.Add(doc.id);
            }

            double totalPayment = 0;
            foreach (PaymentInstrumentItem it in payment.paymentInstruments)
                totalPayment += it.amount;

            if (AccountBase.AmountGreater(totalDoublePayments, 0))
            {
                //verify if it is a repeat attempt to post (this shouldn't happen unless there is a bug in the payment client application)
                try
                {
                    int paymentDocumentID = this.getCustomerBillRecord(payment.settledBills[0]).paymentDocumentID;
                    if (paymentDocumentID != -1)
                    {
                        CustomerPaymentReceipt existingReceipt = this.bdeAccounting.GetAccountDocument<CustomerPaymentReceipt>(paymentDocumentID);
                        if (existingReceipt == null)
                            throw new ServerUserMessage("Receipt id {0} not in accounting database.", paymentDocumentID);
                        if (AccountBase.AmountEqual(existingReceipt.totalInstrument, payment.totalInstrument)
                            && existingReceipt.assetAccountID == payment.assetAccountID
                            && existingReceipt.settledBills.Length>=payment.settledBills.Length
                            )
                        {
                            bool equal = true;
                            for (int k = 0; k < payment.settledBills.Length; k++)
                                if (existingReceipt.settledBills[k] != payment.settledBills[k])
                                {
                                    equal = false;
                                    break;
                                }
                            //if the existing receipt contains exactly one more bill than the receipt just sent, 
                            //and if that bill is a mobile payment service fee,
                            //then we can consider the two payments as equal
                            if (existingReceipt.settledBills.Length > payment.settledBills.Length)
                            {
                                if (existingReceipt.settledBills.Length != payment.settledBills.Length+1)
                                    equal = false;
                                else
                                {
                                    equal = bdeAccounting.GetDocumentTypeByType(typeof(MobilePaymentServiceCharge)).id ==
                                        this.getCustomerBillRecord(existingReceipt.settledBills[existingReceipt.settledBills.Length - 1]).billDocumentTypeID
                                        ;
                                }
                            }
                            if (equal)
                            {
                                ApplicationServer.EventLoger.Log(EventLogType.Errors,
    @"BUG:A client sent repeated payment request. This indicates a clear bug in the client application.
The repeat request is ignored.
client asset account ID:{0}
".format(payment.assetAccountID)
                                    );
                                return existingReceipt.AccountDocumentID;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ApplicationServer.EventLoger.LogException("Critical system inconsistency. Exception while trying to verify double payment attempt.", ex);
                }
            }
            if (payment.offline && payment.mobile && AccountBase.AmountGreater(m_sysPars.commissionAmount_Utility_Share, 0))
            {
                BillItem i = new BillItem();
                i.itemTypeID = 0;
                i.incomeAccountID = m_sysPars.mobilePaymentServiceChargeIncome;
                if (m_sysPars.mobilePaymentServiceChargeIncome == -1)
                    throw new ServerUserMessage("Please configure mobilePaymentServiceChargeIncome account");
                i.description = "Mobile Payment Service Charge";
                i.hasUnitPrice = false;
                i.price = m_sysPars.commissionAmount_Utility_Share;
                i.accounting = BillItemAccounting.Cash;
                MobilePaymentServiceCharge b = new MobilePaymentServiceCharge();
                b.customer = GetSubscriber(payment.customer.id);
                b.ShortDescription = "Mobile Payment Service Charge";
                b.billItems = new BillItem[] { i };
                b.draft = false;
                exitingBill.Add(addBill(AID, b, b.itemsLessExemption.ToArray()));
                totalBill += i.netPayment;

            }
            nonExitantBillTotal = totalPayment - totalBill;

            if (AccountBase.AmountGreater(totalDoublePayments, 0))
            {
                BillItem i = new BillItem();
                i.itemTypeID = 0;
                i.incomeAccountID = -1;
                i.description = "Double payment";
                i.hasUnitPrice = false;
                i.price = totalDoublePayments;
                i.accounting = BillItemAccounting.Advance;
                UnclassifiedPayment b = new UnclassifiedPayment();
                b.customer = GetSubscriber(payment.customer.id);
                b.ShortDescription = "Double settlement of bills";
                b.billItems = new BillItem[] { i };
                b.draft = false;
                exitingBill.Add(addBill(AID, b, b.itemsLessExemption.ToArray()));
            }
            if (AccountBase.AmountGreater(nonExitantBillTotal, 0))
            {
                BillItem i = new BillItem();
                i.itemTypeID = 0;
                i.incomeAccountID = -1;
                i.description = "Settlement of none existent bills";
                i.hasUnitPrice = false;
                i.price = nonExitantBillTotal;
                i.accounting = BillItemAccounting.Advance;
                UnclassifiedPayment b = new UnclassifiedPayment();
                b.customer = GetSubscriber(payment.customer.id);
                b.ShortDescription = "Settlement of none existent bills";
                b.billItems = new BillItem[] { i };
                b.draft = false;
                exitingBill.Add(addBill(AID, b, b.itemsLessExemption.ToArray()));
            }
            
            payment.offlineBills = payment.settledBills;
            payment.settledBills = exitingBill.ToArray();
            return Accounting.PostGenericDocument(AID, payment);
        }
        // BDE exposed
        public ReadingCycle getReadingCycle(int periodID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                ReadingCycle[] ret = dspReader.GetSTRArrayByFilter<ReadingCycle>("periodID=" + periodID);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public ReadingCycle getCurrentReadingCycle()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                ReadingCycle[] ret = dspReader.GetSTRArrayByFilter<ReadingCycle>("status=" + (int)ReadingCycleStatus.Open);
                if (ret.Length == 0)
                    return null;
                if (ret.Length > 1)
                    throw new ServerUserMessage("Multiple open reading cycles");
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public ReadingSheetRow getReadingShowRow(int periodID, int rowIndex)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                ReadingSheetRow[] ret = dspReader.GetSTRArrayByFilter<ReadingSheetRow>("periodID=" + periodID + " and orderN=" + rowIndex);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public ReadingSheetRow[] getReadingShowRows(int periodID, int rowIndex,int n)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                ReadingSheetRow[] ret = dspReader.GetSTRArrayByFilter<ReadingSheetRow>("periodID={0} and orderN>={1} and orderN<{2}".format(periodID,rowIndex,rowIndex+n));
                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public ReadingSheetRow getReadingShowRowByConnectionID(int periodID, int connectionID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                ReadingSheetRow[] ret = dspReader.GetSTRArrayByFilter<ReadingSheetRow>("periodID=" + periodID + " and connectionID=" + connectionID);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public int readSheetSize(int periodID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return (int)dspReader.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.ReadingSheetRow where periodID={1}", this.DBName, periodID));
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        // BDE exposed
        public void openReadingCycle(int AID, int periodID)
        {

            ReadingCycle test = getReadingCycle(periodID);
            if (test != null)
                throw new ServerUserMessage("Reading cyle for period " + GetBillPeriod(periodID).name + " already opened.");
            BillPeriod prd = GetBillPeriod(periodID);
            if (prd == null)
                throw new ServerUserMessage("Invalid period ID:" + periodID);
            if (getCurrentReadingCycle() != null)
                throw new ServerUserMessage("The current reading cycle is not closed");
            lock (dspWriter)
            {
                Accounting.clearProgress();
                Accounting.PushProgress("Creating reading cycle");
                Accounting.SetProgress(0);
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();

                try
                {
                    ReadingCycle cycle = new ReadingCycle();
                    cycle.periodID = periodID;
                    cycle.status = ReadingCycleStatus.Open;
                    cycle.openTime = DateTime.Now;
                    cycle.closeTime = DateTime.Now;
                    cycle.hashCode = Guid.NewGuid().ToByteArray();
                    cycle.syncNo = ApplicationServer.MsgRepo.getLastMessageNumber();
                    dspWriter.InsertSingleTableRecord(this.DBName, cycle, new string[] { "__AID" }, new object[] { AID });
                    Accounting.SetProgress("Retrieving reading list", 0.2);
                    Subscription[] ss = dspWriter.GetSTRArrayByFilter<Subscription>(SQLHelper.getVersionDataFilter(prd.toDate.Ticks, null));
                    Accounting.SetProgress("Preparing reading sheet", 0.3);
                    Accounting.SetChildWeight(0.5);
                    Accounting.PushProgress("Preparing reading sheet");
                    int i = 0;
                    List<int> ids = new List<int>();
                    if (_readingAdapter != null)
                        _readingAdapter.prepareReadingCycleData();
                    foreach (Subscription s in ss)
                    {
                        if (ids.Contains(s.id))
                            continue;
                        else
                            ids.Add(s.id);
                        ReadingSheetRecord row = new ReadingSheetRecord(); ;
                        row.orderN = i;
                        row.connectionID = s.id;
                        row.contractNo = s.contractNo;
                        Subscriber customer=GetSubscriber(s.subscriberID);
                        row.customerName = customer.name;
                        row.meterNo = string.IsNullOrEmpty(s.serialNo) ? "" : s.serialNo;
                        TransactionItems item = bERP.GetTransactionItems(s.itemCode);
                        row.meterType = item == null ? "" : item.Name;
                        row.meterTypeID = item == null ? "" : item.Code;
                        row.phoneNo = customer.phoneNo;
                        row.readerID = -1;
                        row.periodID = periodID;
                        row.status = ReadingSheetRow.STATUS_UNREAD;
                        row.actualReaderID = -1;
                        row.reading = 0.0;
                        row.extraInfo = 0;
                        row.problem = 0;
                        row.remark = "";
                        row.readTime_date = DateTime.Now;
                        row.lat = s.waterMeterX;
                        row.lng = s.waterMeterY;
                        row.readingLat = 0;
                        row.readingLng = 0;
                        row.connectionStatus = (int)s.subscriptionStatus;
                        BWFMeterReading prev= this.BWFGetSimpleMeterPreviousReadingInternal(dspWriter,row.connectionID,row.periodID);
                        row.minReadAllowed =prev==null?0:prev.reading;
                        row.maxReadAllowed = row.minReadAllowed + getExageratedConsumption(s.subscriberID);
                        BWFReadingBlock blk = GetBlock(BWFGetReadingBlockID(s.id, periodID));
                        if (blk != null)
                        {
                            BWFMeterReading r = BWFGetMeterReading(s.id, blk.id);
                            if (r != null)
                            {
                                row.readerID = blk.readerID;
                                row.status = r.bwfStatus == BWFStatus.Read ? ReadingSheetRow.STATUS_READ_UPLOADED : ReadingSheetRow.STATUS_UNREAD;
                                row.reading = r.reading;
                                row.extraInfo = (int)r.extraInfo;
                                row.problem = (int)r.readingProblem;
                                row.remark = r.readingRemark;
                                row.readTime_date = r.readingTime;
                                row.readingLat = r.readingX;
                                row.readingLng = r.readingY;
                            }
                        }
                        if (_readingAdapter == null)
                        {
                            double totalBill=0;
                            HashSet<int> periods = new HashSet<int>();
                            row.bill = new ReadingSheetRecord.MRBill();
                            row.bill.items = new List<ReadingSheetRecord.MRBillItem>();
                            foreach (CustomerBillRecord bill in getBills(-1, customer.id, -1, -1))
                            {
                                if (bill.periodID == -1)
                                    continue;
                                if (bill.isPayedOrDiffered)
                                    continue;

                                if (!periods.Contains(bill.periodID))
                                    periods.Add(bill.periodID);
                                double singleBillTotal = 0;
                                String billDesc = null;

                                foreach (BillItem bi in getCustomerBillItems(bill.id))
                                {
                                    singleBillTotal += bi.price - bi.settledFromDepositAmount;
                                    string desc = bi.description + " (" + bi.netPayment.ToString("#,#0.00") + ")";
                                    if (billDesc == null)
                                        billDesc = desc;
                                    else
                                        billDesc += "\n" + desc;
                                }
                                if (bill.periodID != -1)
                                {
                                    row.bill.items.Add(new ReadingSheetRecord.MRBillItem()
                                    {
                                        billID = bill.id,
                                        amount = singleBillTotal,
                                        description = billDesc
                                    });
                                }
                                totalBill += singleBillTotal;
                            }
                            row.bill_json = Newtonsoft.Json.JsonConvert.SerializeObject(row.bill);
                            
                            row.billText = "{0} Birr from {1} Bill{2}".format(
                                AccountBase.FormatAmount(totalBill)
                                ,periods.Count
                                ,periods.Count>1?"s":""
                                );
                        }
                        else
                        {
                            row.billText = _readingAdapter.getOverdue(periodID,DateTime.Now, customer.id, s.id);
                            row.setCustomFields(_readingAdapter.getcustomFieldValues(periodID, customer.id, s.id));
                        }
                        row.rowKey = generateOctaID();
                        row.customFields_json = row.customFields == null ? null : Newtonsoft.Json.JsonConvert.SerializeObject(row.customFields);
                        row.customerType = (int)customer.subscriberType;
                        row.connectionType = (int)s.connectionType;
                        dspWriter.InsertSingleTableRecord(this.DBName, row, new string[] { "__AID" }, new object[] { AID });
                        i++;
                        Console.WriteLine(i + "                ");
                        //Console.CursorTop--;
                        Accounting.SetProgress((double)i / (double)ss.Length);
                    }
                    Accounting.PopProgress();
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                    Accounting.PopProgress(0);
                }
            }
        }
        // BDE exposed
        public void deleteReadingCycle(int AID, int periodID)
        {
            ReadingCycle test = getReadingCycle(periodID);
            if (test == null)
                throw new ServerUserMessage("Reading cycle for period ID" + periodID + " doesn't exist.");

            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                Accounting.PushProgress("Deleting reading cycle");
                try
                {
                    Accounting.SetProgress("Checking data", 0);
                    int c = (int)dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.BWFMeterReading where periodID={1} and bwfStatus={2}", this.DBName, periodID, (int)BWFStatus.Read));
                    if (c > 0)
                        throw new ServerUserMessage("Reading cycle can't be deleted some readings are already recorded.");
                    c = (int)dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.ReadingSheetRow where periodID={1}", this.DBName, periodID));
                    Accounting.SetProgress("Deleteing reading sheet entries", 0.2);
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.ReadingSheetRow", "periodID=" + periodID);
                    dspWriter.Delete(this.DBName, "ReadingSheetRow", "periodID=" + periodID);
                    Accounting.SetProgress("Delete reading cycle", 0.9);
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.ReadingCycle", "periodID=" + periodID);
                    dspWriter.DeleteSingleTableRecrod<ReadingCycle>(this.DBName, periodID);
                    dspWriter.CommitTransaction();
                    Accounting.PopProgress();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    Accounting.PopProgress(0);
                }
            }
        }
        // BDE exposed
        public ReadingSheetRow[] getReadingSheet(int periodID)
        {
            ReadingCycle cycle = getReadingCycle(periodID);
            if (cycle == null)
                return null;
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<ReadingSheetRow>("periodID=" + periodID);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public void closeReadingCycle(int AID, int periodID)
        {
            ReadingCycle cycle = getReadingCycle(periodID);
            if (cycle == null)
                throw new ServerUserMessage("Invalid reading periodID:" + periodID);
            if (cycle.status == ReadingCycleStatus.Closed)
                throw new ServerUserMessage("Reading cycle already closed");
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    cycle.status = ReadingCycleStatus.Closed;
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.ReadingCycle", "periodID=" + periodID);
                    dspWriter.UpdateSingleTableRecord(this.DBName, cycle, new string[] { "__AID" }, new object[] { AID });
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        // BDE exposed
        public void setReadingSheetRow(int AID, ReadingSheetRow row)
        {

            if (row.status != ReadingSheetRow.STATUS_READ && row.problem == (int)MeterReadingProblem.NoProblem)
            {
                ApplicationServer.EventLoger.Log("Reading already set, reading skipped.");
                return;
                //throw new ServerUserMessage("Only read status is permited");
            }
            BillPeriod period = GetBillPeriod(row.periodID);
            if (period == null)
                throw new ServerUserMessage("Invalid period ID:" + row.periodID);
            ReadingCycle cycle = getCurrentReadingCycle();
            if (cycle == null)
                throw new ServerUserMessage("There is no open reading cycle");
            if (cycle.periodID != row.periodID)
                throw new ServerUserMessage(string.Format("The current reading cycle is {0} but the reading that is submited is for {1}", GetBillPeriod(cycle.periodID).name, period.name));
            Subscription subsc = this.GetSubscription(row.connectionID, period.toDate.Ticks);
            if (subsc == null)
                throw new ServerUserMessage("Invalid connection ID:" + row.connectionID);
            ReadingSheetRow testRow = getReadingShowRowByConnectionID(row.periodID, row.connectionID);
            if (testRow == null)
                throw new ServerUserMessage("Connection " + subsc.contractNo + " is not included in the current reading plan");
            if (testRow.readerID != -1 && testRow.readerID != row.readerID)
                throw new ServerUserMessage(subsc.contractNo + " is allocated to another reader");

            BWFMeterReading existing = this.BWFGetMeterReadingByPeriod(row.connectionID, row.periodID);
            if (existing != null)
            {
                /*if (existing.bwfStatus == BWFStatus.Read)
                    throw new ServerUserMessage("Aready read:" + this.GetSubscription(row.connectionID, period.toDate.Ticks).contractNo);*/
            }
            BWFMeterReading r = new BWFMeterReading();
            r.extraInfo = (ReadingExtraInfo)row.extraInfo;
            r.subscriptionID = row.connectionID;
            r.averageMonths = 0;
            r.billDocumentID = -1;
            r.bwfStatus = (row.status & ReadingSheetRow.STATUS_READ) == ReadingSheetRow.STATUS_READ ? BWFStatus.Read : BWFStatus.Unread;
            r.consumption = 0;
            r.periodID = row.periodID;
            r.reading = row.reading;
            r.readingType = MeterReadingType.Normal;
            r.readingProblem = (MeterReadingProblem)row.problem;

            r.extraInfo = (ReadingExtraInfo)row.extraInfo;
            r.readingRemark = row.remark;
            r.entryDate=r.readingTime =TimeZone.CurrentTimeZone.ToLocalTime(row.time);
            r.readingX = row.readingLat;
            r.readingY = row.readingLng;
            r.method = ReadingMethod.ReaderMobileOffline;
            r.readingBlockID = BWFGetReadingBlockID(r.subscriptionID, r.periodID);
            ReadingEntryClerk clerk = BWFGetClerk(row.readerID);

            if (clerk == null)
                throw new ServerUserMessage("Please register the meter reader as reading entry clerk to allow him to submit reading on his/her own");

            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    string name = "Mobile-" + bdePayroll.GetEmployee(row.readerID).employeeID;
                    if (r.readingBlockID == -1)
                    {
                        BWFReadingBlock[] test = GetBlock(row.periodID, name);
                        if (test.Length > 0)
                        {
                            if (test[0].readerID == row.readerID)
                            {
                                r.readingBlockID = test[0].id;
                            }
                            else
                                throw new ServerUserMessage("Reading block name " + name + " is already used.");
                        }
                        else
                        {
                            BWFReadingBlock block = new BWFReadingBlock();
                            block.periodID = row.periodID;
                            block.readerID = row.readerID;
                            block.readingEnd = block.readingStart = row.time;
                            block.blockName = name;
                            r.readingBlockID = BWFCreateReadingBlock(AID, clerk, block);
                        }
                    }
                    //if (r.hasCoordinate && !subsc.hasCoordinate)
                    //{
                    //    subsc.waterMeterX = r.readingX;
                    //    subsc.waterMeterY = r.readingY;
                    //    UpdateSubscription(AID, subsc);
                    //}
                    if (existing == null)
                        BWFAddMeterReading(AID, clerk, r, true);
                    else
                        BWFUpdateMeterReading(AID, clerk, r);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        public void setReadingSheetRow2(int AID, ReadingSheetRecord row)
        {

            if (row.status != ReadingSheetRow.STATUS_READ && row.problem == (int)MeterReadingProblem.NoProblem)
            {
                ApplicationServer.EventLoger.Log("Reading already set, reading skipped.");
                return;
                //throw new ServerUserMessage("Only read status is permited");
            }
            BillPeriod period = GetBillPeriod(row.periodID);
            if (period == null)
                throw new ServerUserMessage("Invalid period ID:" + row.periodID);
            ReadingCycle cycle = getCurrentReadingCycle();
            if (cycle == null)
                throw new ServerUserMessage("There is no open reading cycle");
            if (cycle.periodID != row.periodID)
                throw new ServerUserMessage(string.Format("The current reading cycle is {0} but the reading that is submited is for {1}", GetBillPeriod(cycle.periodID).name, period.name));
            Subscription subsc = this.GetSubscription(row.connectionID, period.toDate.Ticks);
            if (subsc == null)
                throw new ServerUserMessage("Invalid connection ID:" + row.connectionID);
            ReadingSheetRow testRow = getReadingShowRowByConnectionID(row.periodID, row.connectionID);
            if (testRow == null)
                throw new ServerUserMessage("Connection " + subsc.contractNo + " is not included in the current reading plan");
            if (testRow.readerID != -1 && testRow.readerID != row.readerID)
                throw new ServerUserMessage(subsc.contractNo + " is allocated to another reader");

            BWFMeterReading existing = this.BWFGetMeterReadingByPeriod(row.connectionID, row.periodID);
            if (existing != null)
            {
                /*if (existing.bwfStatus == BWFStatus.Read)
                    throw new ServerUserMessage("Aready read:" + this.GetSubscription(row.connectionID, period.toDate.Ticks).contractNo);*/
            }
            BWFMeterReading r = new BWFMeterReading();
            r.extraInfo = (ReadingExtraInfo)row.extraInfo;
            r.subscriptionID = row.connectionID;
            r.averageMonths = 0;
            r.billDocumentID = -1;
            r.consumption = 0;
            r.periodID = row.periodID;
            
            
            r.reading = row.reading;
            r.bwfStatus = ((row.status & ReadingSheetRow.STATUS_READ) == ReadingSheetRow.STATUS_READ && r.reading>=0) ? BWFStatus.Read : BWFStatus.Unread;

            
            r.readingType = MeterReadingType.Normal;
            r.readingProblem = (MeterReadingProblem)row.problem;

            r.extraInfo = (ReadingExtraInfo)row.extraInfo;
            r.readingRemark = row.remark;
            r.entryDate = r.readingTime = ReadingSheetRecord.fromJavaTicks(row.readTime);
            r.readingX = row.readingLat;
            r.readingY = row.readingLng;
            r.method = ReadingMethod.ReaderMobileOffline;
            r.readingBlockID = BWFGetReadingBlockID(r.subscriptionID, r.periodID);
            ReadingEntryClerk clerk = BWFGetClerk(row.readerID);

            if (clerk == null)
                throw new ServerUserMessage("Please register the meter reader as reading entry clerk to allow him to submit reading on his/her own");

            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    string name = "Mobile-" + bdePayroll.GetEmployee(row.readerID).employeeID;
                    if (r.readingBlockID == -1)
                    {
                        BWFReadingBlock[] test = GetBlock(row.periodID, name);
                        if (test.Length > 0)
                        {
                            if (test[0].readerID == row.readerID)
                            {
                                r.readingBlockID = test[0].id;
                            }
                            else
                                throw new ServerUserMessage("Reading block name " + name + " is already used.");
                        }
                        else
                        {
                            BWFReadingBlock block = new BWFReadingBlock();
                            block.periodID = row.periodID;
                            block.readerID = row.readerID;
                            block.readingEnd = block.readingStart = r.entryDate;
                            block.blockName = name;
                            r.readingBlockID = BWFCreateReadingBlock(AID, clerk, block);
                        }
                    }
                    //if (r.hasCoordinate && !subsc.hasCoordinate)
                    //{
                    //    subsc.waterMeterX = r.readingX;
                    //    subsc.waterMeterY = r.readingY;
                    //    UpdateSubscription(AID, subsc);
                    //}
                    if (existing == null)
                        BWFAddMeterReading(AID, clerk, r, true);
                    else
                        BWFUpdateMeterReading(AID, clerk, r);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        
        
        public static string generateOctaID()
        {
            byte[] guid=Guid.NewGuid().ToByteArray();
            
            char[] ret = new char[8];
            for(int i=0;i<8;i++)
            {
                int val = (guid[2 * i] * 256 + guid[2 * i + 1]) % 36;
                if (val < 10)
                    ret[i] = (char)('0' + val);
                else
                    ret[i] = (char)('A' + (val - 10));
            }
            return new string(ret);
        }
        public void addSystemFeeDeposit(int AID,SystemFeeDeposit deposit)
        {
            dspWriter.InsertSingleTableRecord(this.DBName, deposit, new string[] { "__AID" }, new object[] { AID });
            deposit.returnDate = deposit.returnDate.Date;
            ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(deposit.periodID+"-"+deposit.readerID+"-"+deposit.returnDate.Ticks, deposit));

        }
        public void updateSystemFeeDeposit(int AID,SystemFeeDeposit deposit)
        {
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.SystemFeeDeposit", "periodID={0} and readerID={1} and [returnDate]='{1}'".format(deposit.periodID, deposit.readerID, deposit.returnDate));
            dspWriter.UpdateSingleTableRecord(this.DBName, deposit, new string[] { "__AID" }, new object[] { AID });
            ApplicationServer.sendDiffMsg(AID, GenericDiff.getUpdateDiff(deposit.periodID + "-" + deposit.readerID + "-" + deposit.returnDate.Ticks, deposit));
        }
        public void deleteSystemFeeDeposit(int AID,int periodID,int readerID,DateTime returnDate)
        {
            String cr = "periodID={0} and readerID={1} and [returnDate]='{2}'".format(periodID, readerID, returnDate);
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.SystemFeeDeposit",cr);
            dspWriter.Delete(this.DBName, "SystemFeeDeposit",cr);
            ApplicationServer.sendDiffMsg(AID, GenericDiff.getDeleteDiff(typeof(SystemFeeDeposit), periodID + "-" + readerID + "-" + returnDate.Ticks));
        }
        public double getSystemDepositFeeBalance(int readerID, DateTime date)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                object _lastDate = dspReader.ExecuteScalar("Select max(returnDate) from {0}.dbo.SystemFeeDeposit where returnDate<'{1}'".format(this.DBName, date));
                
                PaymentCenter pc=this.GetPaymentCenter(this.bdePayroll.GetEmployee(readerID,DateTime.Now.Ticks).loginName);
                if (pc == null)
                    return 0;
                String sql="Select count(distinct batchID) from {0}.dbo.AccountTransaction where accountID={1} and tranTicks<{2}";
                int payCount = (int)dspReader.ExecuteScalar(sql.format(bdeAccounting.DBName,pc.casherAccountID,date.Ticks));

                sql = "Select sum(amount) from {0}.dbo.SystemFeeDeposit where readerID={1} and returnDate<'{2}'".format(this.DBName, readerID, date);
                object _sum = dspReader.ExecuteScalar(sql);
                double sum = _sum is double ? (double)_sum : 0;
                return payCount * (m_sysPars.commissionAmount_System_Share) - sum;

            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public SystemFeeDeposit getDeposit(int periodID, int readerID, DateTime returnDate)
        {
            SQLHelper reader = this.GetReaderHelper();
            try
            {
                String cr = "periodID={0} and readerID={1} and [returnDate]='{1}'".format(periodID, readerID, returnDate);
                SystemFeeDeposit[] ret = reader.GetSTRArrayByFilter<SystemFeeDeposit>(cr);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                this.ReleaseHelper(reader);
            }
        }
        public SystemFeeDeposit[] getDeposits(int periodID,int readerID,DateTime from,DateTime to)
        {
            SQLHelper reader = this.GetReaderHelper();
            try
            {
                String cr=null;
                if (readerID != -1)
                    cr = INTAPS.StringExtensions.AppendOperand(cr, " and ", "readerID=" + readerID);
                
                if (periodID!= -1)
                    cr = INTAPS.StringExtensions.AppendOperand(cr, " and ", "periodID=" + readerID);

                return reader.GetSTRArrayByFilter<SystemFeeDeposit>(cr);
            }
            finally
            {
                this.ReleaseHelper(reader);
            }
        }
    }   

    
}