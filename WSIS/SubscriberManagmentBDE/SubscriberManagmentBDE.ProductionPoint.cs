using INTAPS;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.Evaluator;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;
using INTAPS.RDBMS;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using BIZNET.iERP;
using System.Net;
using System.Data.SqlClient;
namespace INTAPS.SubscriberManagment.BDE
{

    public partial class SubscriberManagmentBDE : BDEBase
    {
        // BDE exposed
        public int registerProductionPoint(int AID, ProductionPoint productionPoint)
        {
            lock(dspWriter)
            {
                try
                {
                    productionPoint.id = AutoIncrement.GetKey("SubscriberManagment.productionPointID");
                    dspWriter.InsertSingleTableRecord<ProductionPoint>(this.DBName, productionPoint, new string[] { "__AID" }, new object[] { AID });
                    return productionPoint.id;
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }
        }
        // BDE exposed
        public ProductionPoint[] getProductionPoint(string name, int kebele, int index, int type, int pageSize, out int NRecords)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                string criteria = "";
                if (string.IsNullOrEmpty(name) && kebele == -1)
                    criteria = null;
                if (!string.IsNullOrEmpty(name) && kebele != -1)
                    criteria = "name like '%" + name + "%' and kebele=" + kebele;
                else if (string.IsNullOrEmpty(name) && kebele != -1)
                    criteria = "kebele=" + kebele;
                else if (!string.IsNullOrEmpty(name) && kebele == -1)
                    criteria = "name like '%" + name + "%'";
                if (type != -1)
                    criteria += criteria == null ? "type=" + type : " and type=" + type;
                return reader.GetSTRArrayByFilter<ProductionPoint>(criteria, index, pageSize, out NRecords);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public ProductionReadingData[] getProductionReadingsData(int productionPointID, int index, int pageSize, out int NRecords)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            List<ProductionReadingData> readingData = new List<ProductionReadingData>();
            try
            {
                
                ProductionPointReading[] readings = reader.GetSTRArrayByFilter<ProductionPointReading>("productionPointID=" + productionPointID, index, pageSize, out NRecords);
                foreach (var r in readings)
                {
                    ProductionReadingData data = new ProductionReadingData();
                    data.consumption = r.consumption;
                    data.reading = r.reading;
                    data.previousReading = getProductionPreviousReading(r.id,productionPointID, reader);
                    data.readingDate = r.readingDate;
                    data.readingType = r.readingType;
                    readingData.Add(data);
                }
                return readingData.ToArray();
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        public ProductionPointReading getProductionPointReading(int productionPointReadingID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                ProductionPointReading[] readings = reader.GetSTRArrayByFilter<ProductionPointReading>("id=" + productionPointReadingID);
                if (readings.Length > 0)
                    return readings[0];
                else
                    return null;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        private double getProductionPreviousReading(int productionReadingID,int productionPointID, SQLHelper helper)
        {
            string sql = "select readingDate from {0}.dbo.ProductionPointReading where id=" + productionReadingID;
            DateTime readingDate = (DateTime)helper.ExecuteScalar(string.Format(sql,this.DBName));
            string sqlPrev = "select max(readingDate) from {0}.dbo.ProductionPointReading where productionPointID=" + productionPointID + " and readingDate<'" + readingDate + "'";
            object obj = helper.ExecuteScalar(string.Format(sqlPrev,this.DBName));
            if (obj == null)
                return 0.0;
            else
            {
                string sqlPrevReading = "select reading from {0}.dbo.ProductionPointReading where readingDate='" + (DateTime)obj + "' and productionPointID=" + productionPointID;
                return (double)helper.ExecuteScalar(string.Format(sqlPrevReading, this.DBName));
            }
        }
        // BDE exposed
        public ProductionPoint getProductionPoint(int id)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                ProductionPoint[]productions= reader.GetSTRArrayByFilter<ProductionPoint>("id=" + id);
                if (productions.Length > 0)
                    return productions[0];
                else
                    return null;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public int setProductionPointReading(int AID, DateTime readingDate, double reading,MeterReadingType readingType, int productionPointID)
        {
            //Prevent setting reading for an existing datetime
            //check if the last reading has been reset and if it is use the reading as consumption
            //What happens when setting a reading back dated? (update all readings forward?)
            //What happens when resetting a reading back dated?
           lock(dspWriter)
           {
               dspWriter.setReadDB(this.DBName);
               try
               {
                   string sql = "select count(*) from {0}.dbo.ProductionPointReading where readingDate='" + readingDate + "' and productionPointID=" + productionPointID;
                   if ((int)dspWriter.ExecuteScalar(string.Format(sql, this.DBName)) > 0)
                   {
                       throw new ServerUserMessage("Reading already exists for date " + readingDate.ToShortDateString());
                   }
                   if (readingType == MeterReadingType.MeterReset)
                   {
                      int id= resetProductionPointReading(AID, readingDate, productionPointID);
                      return id;
                   }
                   else
                   {
                       double previousReading = getProductionPreviousReading(readingDate, productionPointID, dspWriter);
                       if (reading < previousReading)
                           throw new ServerUserMessage("Reading will cause negative consumption");
                       ProductionPointReading pReading = new ProductionPointReading();
                       pReading.id = AutoIncrement.GetKey("SubscriberManagment.ProductionPointReadingID");
                       pReading.consumption = reading - previousReading;
                       pReading.reading = reading;
                       pReading.readingDate = readingDate;
                       pReading.productionPointID = productionPointID;
                       pReading.readingType = MeterReadingType.Normal;
                       dspWriter.InsertSingleTableRecord<ProductionPointReading>(this.DBName, pReading, new string[] { "__AID" }, new object[] { AID });
                       return pReading.id;
                   }

               }
               catch (Exception ex)
               {
                   throw ex;
               }
               finally
               {
                   dspWriter.restoreReadDB();
               }
           }

        }

        // BDE exposed
        private int resetProductionPointReading(int AID, DateTime readingDate, int productionPointID)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    string sql = "select count(*) from {0}.dbo.ProductionPointReading where readingDate='" + readingDate + "' and productionPointID=" + productionPointID;
                    if ((int)dspWriter.ExecuteScalar(string.Format(sql, this.DBName)) > 0)
                    {
                        throw new ServerUserMessage("Reading already exists for date " + readingDate.ToShortDateString());
                    }

                    ProductionPointReading pReading = new ProductionPointReading();
                    pReading.id = AutoIncrement.GetKey("SubscriberManagment.ProductionPointReadingID");
                    pReading.consumption = 0.0;
                    pReading.reading = 0.0;
                    pReading.readingDate = readingDate;
                    pReading.productionPointID = productionPointID;
                    pReading.readingType = MeterReadingType.MeterReset;
                    dspWriter.InsertSingleTableRecord<ProductionPointReading>(this.DBName, pReading, new string[] { "__AID" }, new object[] { AID });
                    return pReading.id;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        // BDE exposed
        public void updateProductionPointReading(int AID, ProductionPointReading reading, bool reset)
        {
            lock(dspWriter)
            {
                try
                {
                    dspWriter.setReadDB(this.DBName);
                    if (reset)
                    {
                        reading.readingType = MeterReadingType.MeterReset;
                        reading.consumption = 0.0;
                        reading.reading = 0.0;
                    }
                    dspWriter.logDeletedData(AID, string.Format("{0}.dbo.ProductionPointReading", this.DBName), "id=" + reading.id);
                    dspWriter.UpdateSingleTableRecord<ProductionPointReading>(this.DBName, reading, new string[] { "__AID" }, new object[] { AID });
                    propagateConsequentReadings(AID, reading.id, reading.productionPointID, reading.reading, reset, dspWriter);
                }
                catch(Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }

        private void propagateConsequentReadings(int AID, int productionPointReadingID,int productionPointID,double reading,bool reset, SQLHelper helper)
        {
           //check if reading is the first reading(no previous)
            lock (helper)
            {
                helper.BeginTransaction();
                try
                {
                    int nextReadingID = getImmediateNextReadingID(productionPointReadingID, productionPointID, helper);
                    double previousReading = getProductionPreviousReading(productionPointReadingID, productionPointID, helper);

                    double newConsumption = reading - previousReading;
                    if (!reset && reading < previousReading)
                        throw new ServerUserMessage("Reading will cause negative consumption.");
                    if (reset)
                    {
                        newConsumption = 0.0;
                        reading = 0.0;
                    }
                    ProductionPointReading r = getProductionPointReading(productionPointReadingID);
                    r.consumption = newConsumption;
                    r.reading = reading;
                    dspWriter.logDeletedData(AID, string.Format("{0}.dbo.ProductionPointReading", this.DBName), "id=" + productionPointReadingID);
                    dspWriter.UpdateSingleTableRecord<ProductionPointReading>(this.DBName, r, new string[] { "__AID" }, new object[] { AID });
                    if (nextReadingID != -1)
                    {
                        ProductionPointReading nextReading = getProductionPointReading(nextReadingID);
                        nextReading.consumption = nextReading.reading - reading;
                        dspWriter.logDeletedData(AID, string.Format("{0}.dbo.ProductionPointReading", this.DBName), "id=" + nextReadingID);
                        dspWriter.UpdateSingleTableRecord<ProductionPointReading>(this.DBName, nextReading, new string[] { "__AID" }, new object[] { AID });
                    }

                    helper.CommitTransaction();
                }
                catch(Exception ex)
                {
                    helper.RollBackTransaction();
                    throw ex;
                }
            }
        }

        private int getImmediateNextReadingID(int productionPointReadingID, int productionPointID, SQLHelper helper)
        {
            string sql = "select readingDate from {0}.dbo.ProductionPointReading where id=" + productionPointReadingID;
            DateTime readingDate = (DateTime)helper.ExecuteScalar(string.Format(sql, this.DBName));
            string sqlNext = "select min(readingDate) from {0}.dbo.ProductionPointReading where productionPointID=" + productionPointID + " and readingDate>'" + readingDate + "'";
            object obj = helper.ExecuteScalar(string.Format(sqlNext, this.DBName));
            if (obj == null || obj is DBNull)
                return -1;
            else
            {
                string sqlNextReading = "select id from {0}.dbo.ProductionPointReading where readingDate='" + (DateTime)obj + "' and productionPointID=" + productionPointID;
                return (int)helper.ExecuteScalar(string.Format(sqlNextReading, this.DBName));
            }
        }

        private double getProductionPreviousReading(DateTime readingDate, int productionPointID, SQLHelper helper)
        {
            string sqlPrev = "select max(readingDate) from {0}.dbo.ProductionPointReading where productionPointID=" + productionPointID + " and readingDate<'" + readingDate + "'";
            object obj = helper.ExecuteScalar(string.Format(sqlPrev, this.DBName));
            if (obj == null || obj is DBNull)
                return 0.0;
            else
            {
                string sqlPrevReading = "select reading from {0}.dbo.ProductionPointReading where readingDate='" + (DateTime)obj + "' and productionPointID=" + productionPointID;
                return (double)helper.ExecuteScalar(string.Format(sqlPrevReading, this.DBName));
            }
        }
    }
}