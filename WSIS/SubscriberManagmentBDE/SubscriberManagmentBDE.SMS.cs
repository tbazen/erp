using INTAPS;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.Evaluator;
using INTAPS.Payroll;
using INTAPS.RDBMS;
using System;
using System.Collections.Generic;
using System.Data;
using INTAPS.SMS;
namespace INTAPS.SubscriberManagment.BDE
{
    public partial class SubscriberManagmentBDE : BDEBase
    {
        SMSInterface _sms = null;
        class SMSReceiver : ISMSReceiver
        {

            const int RECEIVE_FILTER_TIME_OUT = 300;//seconds
            public SubscriberManagmentBDE bde;
            public bool enableReceive = true;
            Dictionary<string, DateTime> filterPhoneNo = new Dictionary<string, DateTime>();
            public SMSReceiver()
            {
                new System.Threading.Thread(
                    delegate()
                    {
                        System.Threading.Thread.Sleep(RECEIVE_FILTER_TIME_OUT * 1000);
                        lock (filterPhoneNo)
                        {
                            List<string> rem = new List<string>();
                            DateTime now=DateTime.Now;
                            foreach (KeyValuePair<string, DateTime> kv in filterPhoneNo)
                            {
                                if (now.Subtract(kv.Value).TotalSeconds > RECEIVE_FILTER_TIME_OUT)
                                    rem.Add(kv.Key);
                            }
                            foreach (string pn in rem)
                                filterPhoneNo.Remove(pn);
                        }
                    }
                ).Start();
            }

            public void addToFilter(string phoneNo)
            {
                if (phoneNo.Length < 9 )
                    return;
                phoneNo = "0" + phoneNo.Substring(phoneNo.Length-9, 9);
                lock (filterPhoneNo)
                {
                    if (filterPhoneNo.ContainsKey(phoneNo))
                        filterPhoneNo[phoneNo] = DateTime.Now;
                    else
                        filterPhoneNo.Add(phoneNo, DateTime.Now);
                }
            }
            public void removeFromFilter(string phoneNo)
            {
                if (phoneNo.Length < 9)
                    return;
                phoneNo = "0" + phoneNo.Substring(phoneNo.Length - 9, 9);
                lock (filterPhoneNo)
                {
                    if (filterPhoneNo.ContainsKey(phoneNo))
                        filterPhoneNo.Remove(phoneNo);
                }
            }
            public bool isFiltered(string phoneNo)
            {
                 if (phoneNo.Length < 9)
                    return false;
                phoneNo = "0" + phoneNo.Substring(phoneNo.Length - 9, 9);
                lock (filterPhoneNo)
                {
                    return filterPhoneNo.ContainsKey(phoneNo);
                }
            }
            bool sendReply(string phoneNo, string msg)
            {
                bde._sms.logSMS(-1,phoneNo, msg);
                return true;
            }
            public bool onSMSReceived(string phoneNo, string message)
            {
                if (phoneNo.Length < 9)
                    return false;
                if (isFiltered(phoneNo))
                    return true;
                if (!enableReceive)
                    return sendReply(phoneNo, "SMS Reading not avialable now. Thanks!");
                ReadingCycle cycle=bde.getCurrentReadingCycle();
                if(cycle==null)
                    return sendReply(phoneNo, "SMS Reading not avialable now. Thanks!");

                phoneNo = "0"+phoneNo.Substring(phoneNo.Length - 9, 9);
                Subscriber[] customer = bde.getCustomersByPhoneNo(phoneNo);
                if (customer.Length == 0)
                    return sendReply(phoneNo, "Sorry your phone number is not in the database. Please contact {0}".format(bde.bERP.SysPars.companyProfile.Name));

                if (customer.Length > 1)
                    return sendReply(phoneNo, "Another customer is also registered with your phone no. Please contact {0}".format(bde.bERP.SysPars.companyProfile.Name));

                Subscription[] subsc = bde.GetSubscriptions(customer[0].id, DateTime.Now.Ticks);
                if (subsc.Length == 0)
                    return sendReply(phoneNo, "There is connection in the database associated with your customer code. Please contact {0}".format(bde.bERP.SysPars.companyProfile.Name));
                string[] parts = message.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length == 0)
                    return sendReply(phoneNo, "This is not a valid message");
                Employee emp = bde.bdePayroll.GetEmployee(bde.SysPars.smsReader);
                if(string.IsNullOrEmpty(emp.loginName))
                    return sendReply(phoneNo, "SMS Reading not avialable now. Thanks!");

                if (subsc.Length == 1)
                {
                    int reading;
                    if (!int.TryParse(parts[parts.Length - 1], out reading))
                        return sendReply(phoneNo, "Thank you for the message.\n"+bde.bERP.SysPars.companyProfile.Name);
                    BWFMeterReading r = new BWFMeterReading();
                    r.averageMonths = 0;
                    r.billDocumentID = -1;
                    r.bwfStatus = BWFStatus.Proposed;
                    r.consumption = 0;
                    r.entryDate = DateTime.Now;
                    r.extraInfo = ReadingExtraInfo.ReadingDate;
                    r.method = ReadingMethod.CustomerProvidedSMS;
                    r.periodID = cycle.periodID;
                    r.reading = reading;
                    r.readingBlockID = bde.BWFGetReadingBlockID(subsc[0].id, r.periodID);
                    r.readingProblem = MeterReadingProblem.NoProblem;
                    r.readingRemark = "";
                    r.readingTime = DateTime.Now;
                    r.readingType = MeterReadingType.Normal;
                    r.readingX = 0;
                    r.readingY = 0;
                    r.subscriptionID = subsc[0].id;
                    BWFMeterReading existing = bde.BWFGetMeterReadingByPeriod(r.subscriptionID, r.periodID);
                    ReadingEntryClerk clerk = bde.BWFGetClerk(bde.SysPars.smsReader);
                    string submitReply;
                    lock (bde.dspWriter)
                    {
                        bde.dspWriter.BeginTransaction();
                        try
                        {
                            int AID = ApplicationServer.SecurityBDE.WriteAddAudit(emp.loginName, ApplicationServer.SecurityBDE.GetUIDForName(emp.loginName), "SMS Reading Attempt", "", -1);

                            if (r.readingBlockID == -1)
                            {
                                string name = "SMS Readings";
                                BWFReadingBlock[] test = bde.GetBlock(r.periodID, name);
                                if (test.Length > 0)
                                {
                                    r.readingBlockID = test[0].id;
                                }
                                else
                                {
                                    BWFReadingBlock block = new BWFReadingBlock();
                                    block.periodID = r.periodID;
                                    block.readerID = bde.SysPars.smsReader;
                                    block.readingEnd = block.readingStart = r.readingTime;
                                    block.blockName = name;
                                    r.readingBlockID = bde.BWFCreateReadingBlock(AID, clerk, block);
                                }
                            }
                            if (existing == null)
                                bde.BWFAddMeterReading(AID, clerk, r, true);
                            else
                                bde.BWFUpdateMeterReading(AID, clerk, r);
                            submitReply = "Reading submited. Thanks!";
                            bde.dspWriter.CommitTransaction();
                        }
                        catch (Exception ex)
                        {
                            bde.dspWriter.RollBackTransaction();
                            ApplicationServer.EventLoger.LogException("Problem submiting sms reading", ex);
                            submitReply = "There was problem submitting your reading. Sorry for the inconvinence!";
                        }
                    }
                    return sendReply(phoneNo, submitReply);
                }
                else
                    return sendReply(phoneNo, "Because you have multiple connections you can't submit reading with SMS");
            }
        }
        void initializeSMS()
        {
            if (System.Configuration.ConfigurationManager.AppSettings["MaintainanceJob"].Equals("true", StringComparison.CurrentCultureIgnoreCase))
                return;
            if (!("true".Equals(System.Configuration.ConfigurationManager.AppSettings["sms_start"])))
                return;
            _sms = new SMSInterface();
            if (SysPars.smsReader == -1 || this.GetMeterReadingEmployee(SysPars.smsReader) == null)
            {
                ApplicationServer.EventLoger.Log("SMS Reader not set, SMS reading functionality willl not be avialable");
            }
            else
                _sms.addReciever(new SMSReceiver() {bde=this});
            try
            {
                _sms.start();
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException("Error trying to initialize sms interface", ex);
                _sms = null;
            }
        }

        internal Subscriber[] getCustomersByPhoneNo(string phoneNo)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<Subscriber>("phoneNo='{0}'".format(phoneNo));
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public string getSMSInterfaceStatus()
        {
            if (_sms == null)
                return "SMS Interface not started";
            return _sms.StatusData.statusMessage;
        }
        public CustomerSMS[] previewSMS(CustomerSMSSendParameter send)
        {
            switch(send.sendType)
            {
                case CustomerSMSSendType.Bills:
                    return previewBillSMS(send);
                case CustomerSMSSendType.General:
                    return previewGeneralSMS(send);
                case CustomerSMSSendType.UnreadNotification:
                    return previewReadingSMS(send);
                default:
                    throw new ServerUserMessage("Not implemented");

            }
        }
        public CustomerSMS[] previewGeneralSMS(CustomerSMSSendParameter send)
        {
            Console.WriteLine("Generating sms bill notififcation");
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                if (send.manualSMS != null)
                {
                    return send.manualSMS;
                }
                if (!string.IsNullOrEmpty(send.phoneNo))
                {
                    return new CustomerSMS[]{
                        new CustomerSMS()
                        {
                            customerCode=null,
                            customerID=-1,
                            message=send.messageFormat,
                            name=null,
                            phoneNo=send.phoneNo
                            }};
                }
                string cr = null;
                if (send.singleCustomer != -1)
                    cr = INTAPS.StringExtensions.AppendOperand(cr, " AND ", "Subscriber.id=" + send.singleCustomer);

                if (cr != null)
                    cr = " AND " + cr;
                string sql = @"SELECT     phoneNo, name, customerCode FROM         {0}.dbo.Subscriber
                    WHERE     (phoneNo <> '') {1} order by name";
                sql = string.Format(sql, this.DBName,cr);
                DataTable table = helper.GetDataTable(sql);
                int c = table.Rows.Count;
                Console.WriteLine("{0} sms will be generated", c);
                Console.WriteLine();
                CustomerSMS[] ret = new CustomerSMS[table.Rows.Count];
                int i = 0;
                string format = send.messageFormat;
                foreach (DataRow row in table.Rows)
                {
                    //Console.CursorTop--;
                    Console.WriteLine((c--) + "                ");
                    string phoneNo = row["phoneNo"] as string;
                    string name = row["name"] as string;
                    string customerCode = row["customerCode"] as string;
                    string sms = format.Replace("\\n", "\n");
                    sms = sms.Replace("$name", name);
                    ret[i++] = new CustomerSMS() { customerCode = customerCode,  message = sms, phoneNo = phoneNo, name = name };
                }
                return ret;
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public CustomerSMS[] previewBillSMS(CustomerSMSSendParameter send)
        {

            string format = SysPars.sms_bill_format;
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                int currentPeriod = SysPars.currentPeriod;
                string cr=null;
                if (send.singleCustomer != -1)
                    cr = INTAPS.StringExtensions.AppendOperand(cr, " AND ", "Subscriber.id=" + send.singleCustomer);
                if (cr != null)
                    cr = " AND " + cr;
                string sql = @"SELECT  CustomerBill.connectionID,  Subscriber.phoneNo, Subscriber.name, ISNULL(SUM(CASE WHEN periodID = {0} THEN CustomerBillItem.price END), 0) AS Cur, 
                      ISNULL(SUM(CASE WHEN periodID <> {0} THEN CustomerBillItem.price END), 0) AS Overd, Subscriber.customerCode
FROM         {2}.dbo.Subscriber INNER JOIN
                      {2}.dbo.CustomerBill INNER JOIN
                      {2}.dbo.CustomerBillItem ON CustomerBill.id = CustomerBillItem.customerBillID ON Subscriber.id = CustomerBill.customerID
WHERE     (Subscriber.phoneNo <> '') AND (CustomerBill.paymentDocumentID = - 1) AND (CustomerBill.paymentDiffered = 0)
{1}
and CustomerBill.billDocumentTypeID in (4,5,12)
GROUP BY CustomerBill.connectionID, Subscriber.name, Subscriber.phoneNo, Subscriber.customerCode
HAVING      (ISNULL(SUM(CASE WHEN periodID = {0} THEN CustomerBillItem.price END), 0) > 0)";

                sql = string.Format(sql, currentPeriod,cr,this.DBName);
                DataTable table = helper.GetDataTable(sql);
                int c = table.Rows.Count;
                BillPeriod period = GetBillPeriod(currentPeriod);
                BillPeriod nextPeriod = INTAPS.Accounting.BDE.AccountingPeriodHelper.GetNextPeriod<BillPeriod>(helper, "BillPeriod", period);
                Console.WriteLine("{0} sms will be generated", c);
                Console.WriteLine();
                CustomerSMS[] ret = new CustomerSMS[table.Rows.Count];
                int i = 0;
                foreach (DataRow row in table.Rows)
                {
                    //Console.CursorTop--;
                    Console.WriteLine((c--) + "                ");
                    string phoneNo = row["phoneNo"] as string;
                    string name = row["name"] as string;
                    string customerCode=row["customerCode"] as string;
                    double current = (double)row["Cur"];
                    double overd = (double)row["Overd"];
                    string sms = format.Replace("\\n", "\n");
                    sms = sms.Replace("$name", CustomerSMSSendParameter.truncateName(name));
                    sms = sms.Replace("$bill", (current + overd).ToString("#,#0.00"));
                    sms = sms.Replace("$period", period.name);
                    
                    string before = INTAPS.Ethiopic.EtGrDate.GetEtMonthNameEng(INTAPS.Ethiopic.EtGrDate.ToEth(nextPeriod.fromDate).Month, ApplicationServer.languageID);
                    sms = sms.Replace("$before", before);
                    if (sms.Contains("$cont"))
                    {
                        int connectionID=(int)row["connectionID"];
                        Subscription subsc=GetSubscription(connectionID,period.toDate.Ticks);
                        sms = sms.Replace("$cont", subsc == null ? "" : subsc.contractNo);
                    }
                    ret[i++] = new CustomerSMS() { customerCode=customerCode,message=sms,phoneNo=phoneNo,name=name};
                }
                return ret;
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }

        public CustomerSMS[] previewReadingSMS(CustomerSMSSendParameter send)
        {

            string format = SysPars.sms_unread_format;
           INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                BillPeriod period = GetBillPeriod(SysPars.readingPeriod);
                string cr = null;
                string sql = @"Select s.name,ss.contractNo,s.phoneNo,s.customerCode from 
{0}.dbo.Subscription ss  inner join {0}.dbo.Subscriber s on ss.subscriberID=s.id and ss.ticksTo=-1
where
ss.subscriptionStatus=2 and phoneNo is not null and len(phoneNo)>9
and ss.id not in (Select subscriptionID from {0}.dbo.BWFMeterReading where periodID={1} and bwfStatus=1)";
                if (send.singleCustomer != -1)
                    sql+= " AND s.id=" + send.singleCustomer;

                sql = string.Format(sql, this.DBName, SysPars.readingPeriod);
                DataTable table = helper.GetDataTable(sql);
                int c = table.Rows.Count;
                
                Console.WriteLine("{0} sms will be generated", c);
                Console.WriteLine();
                CustomerSMS[] ret = new CustomerSMS[table.Rows.Count];
                int i = 0;
                foreach (DataRow row in table.Rows)
                {
                    //Console.CursorTop--;
                    Console.WriteLine((c--) + "                ");
                    string phoneNo = row["phoneNo"] as string;
                    string name = row["name"] as string;
                    String customerCode = row["customerCode"] as string;

                    string sms = format.Replace("\\n", "\n");
                    sms = sms.Replace("$name", CustomerSMSSendParameter.truncateName(name));
                    sms = sms.Replace("$date", period.toDate.ToString("MMM dd,yyy"));

                    ret[i++] = new CustomerSMS() { customerCode = customerCode, message = sms, phoneNo = phoneNo, name = name };
                }
                return ret;
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }

        // BDE exposed
        public string getSMSPreviewHTML(CustomerSMSSendParameter send)
        {
            CustomerSMS[] sms = previewSMS(send);
            string h;
            return Accounting.EvaluateEHTML("System.Billing.SMSPreview",new object[]{"data",new EData(sms)},out h);
        }
        public void sendSMS(int AID, bool validatePassCode, CustomerSMSSendParameter send)
        {
            if (_sms == null)
                throw new ServerUserMessage("SMS mobule not activated");
            CustomerSMS[] smss = previewSMS(send);
            if(validatePassCode)
                validateSMSPasscode(send.passCode, "Invalid passcode");
            foreach (CustomerSMS s in smss)
            {
                _sms.logSMS(AID,s.phoneNo, s.message);
            }
        }
        public InstantMessageData[] getSMSThread(string phoneNo)
        {
            return null;
        }
        public int sendSingleSMS(int AID, InstantMessageData sms)
        {
            if (_sms == null)
                throw new ServerUserMessage("SMS mobule not activated");
            if (_sms.Status != SMSInterfaceStatus.Ready)
                throw new ServerUserMessage("SMS module not ready");
            return _sms.logSMS(AID, sms.to, sms.messages);
        }
        public int getSMSStatus(int smsID)
        {
            if (_sms == null)
                throw new ServerUserMessage("SMS mobule not activated");
            return _sms.getStatusBySMSID(smsID);
        }
        static string hashSMSPassCode(string passCode)
        {
            byte[] hash=System.Security.Cryptography.MD5.Create().ComputeHash(System.Text.Encoding.Unicode.GetBytes(passCode));
            return Convert.ToBase64String(hash);
        }

        // BDE exposed
        public void changeSMSPassCode(int AID, string oldPassCode, string newPassCode)
        {
            validateSMSPasscode(oldPassCode, "Invalid old passcode");

            SetSystemParameters(AID, new string[] { "sms_passs_code" }, new object[] { hashSMSPassCode( newPassCode) });
        }

        private void validateSMSPasscode(string oldPassCode, string msg)
        {
            if (string.IsNullOrEmpty(m_sysPars.sms_passs_code))
            {
                if (!oldPassCode.Equals("smsintapspass@2691"))
                    throw new ServerUserMessage(msg);
            }
            else
            {
                if (!m_sysPars.sms_passs_code.Equals(hashSMSPassCode(oldPassCode)))
                    throw new ServerUserMessage(msg);
            }
        }
        // BDE exposed
        public SMSLicenseInformation getSMSLicense()
        {
            return new SMSLicenseInformation()
            {
               allowBillSend=_sms!=null && !string.IsNullOrEmpty(SysPars.sms_bill_format),
               allowReceive = _sms != null && SysPars.smsReader > 0
            };
        }
        // BDE exposed
        public CustomerSMS[] getSMSLogByPhoneNo(int sendID, int receivedID, string phoneNo)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                string sql = @"Select * from
(
Select id,phoneNo,[message],receivedTicks as ticks,2 as [Status], 0 as smsOut from {0}.dbo.SMSIn
where id>{1} and phoneNo='{3}'
union
Select id,phoneNo,[message],statusTicks as ticks,status,1 as smsOut from {0}.dbo.SMSOut
where id>{2} and phoneNo='{3}'
)
s
order by s.ticks";
                foreach(DataRow row in reader.GetDataTable(sql.format(this.DBName,receivedID,sendID,phoneNo)).Rows)
                {
                    CustomerSMS sms = new CustomerSMS();
                    sms.smsID = (int)row[0];
                    sms.customerID = -1;
                    sms.phoneNo = (string)row[1];
                    sms.smsOut = (int)row["smsOut"] == 1;
                    switch ((int)row["status"])
                    {
                        case 1:
                            sms.status = CustomerSMS.CustmerSMSStatus.Sending;
                            break;
                        case 2:
                            sms.status = CustomerSMS.CustmerSMSStatus.Sent;
                            break;
                        case 4:
                            sms.status = CustomerSMS.CustmerSMSStatus.Delivered;
                            break;
                        default:
                            sms.status = CustomerSMS.CustmerSMSStatus.Failed;
                            break;
                    }
                    sms.ticks = (long)row["ticks"];
                }
                return null;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
    }
}