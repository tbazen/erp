using INTAPS;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.Evaluator;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;
using INTAPS.RDBMS;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using BIZNET.iERP;
namespace INTAPS.SubscriberManagment.BDE
{
    public partial class SubscriberManagmentBDE : BDEBase
    {
        SubscriberManagmentUpdateNumber _updateNumber = new SubscriberManagmentUpdateNumber();
        private AccountingBDE bdeAccounting;

        public AccountingBDE Accounting
        {
            get { return bdeAccounting; }
            set { bdeAccounting = value; }
        }
        private BIZNET.iERP.Server.iERPTransactionBDE bdeBERP;
        public BIZNET.iERP.Server.iERPTransactionBDE bERP
        {
            get { return bdeBERP; }

        }
        private PayrollBDE m_payroll;
        internal SubscriberManagmentSystemParameters m_sysPars;
        private const int MAX_PAGE_SIZE = 0x3e8;
        public int Progress;
        public int ProgressMax;
        public BatchJobStage ProgressStage;
        public bool StopProgress;
        public string SystemBusyMessage;
        private const int UPDATE_PAGE_SIZE = 500;
        public SubscriberManagmentSystemParameters SysPars
        {
            get
            {
                return m_sysPars;
            }
        }

        public SubscriberManagmentBDE(string bdeName, string DB, SQLHelper writer, string ReadOnlyConnectionString)
            : base(bdeName, DB, writer, ReadOnlyConnectionString)
        {
            this.ProgressMax = -1;
            this.SystemBusyMessage = null;
            this.m_payroll = null;
            this.bdeAccounting = (AccountingBDE)ApplicationServer.GetBDE("Accounting");
            this.bdeBERP = (BIZNET.iERP.Server.iERPTransactionBDE)ApplicationServer.GetBDE("iERP");
            this.LoadSystemParameters();
            this.InitializeReporting();
            this.initializeBillingSubsystem();
            this.initMapServer();
            this.initializeReadingAdapter();
            this.initializeSMS();
            this.initializeEPS();
        }

        // BDE exposed
        public SubscriberManagmentUpdateNumber getUpdateNumber()
        {
            return _updateNumber;
        }
        public int AddSubscription(int AID, Subscription subscription)
        {
            _billingRule.fixSubscriptionDataForSave(subscription);
            int id;
            lock (base.dspWriter)
            {
                Exception exception;
                try
                {
                    if (this.m_sysPars.receivableAccount < 1)
                    {
                        throw new ServerUserMessage("Please configure the receivable account.");
                    }
                    Account account = this.bdeAccounting.GetAccount<Account>(this.m_sysPars.receivableAccount);
                }
                catch (Exception exception1)
                {
                    exception = exception1;
                    throw new ServerUserMessage("Invalid subscriber receivable account. " + exception.Message);
                }
                base.dspWriter.setReadDB(base.DBName);
                try
                {
                    Console.WriteLine($"Adding subscription with subscriber id{subscription.subscriberID}");
                    subscription.subscriber = GetSubscriber(subscription.subscriberID);
                    this.ValidateSubscription(base.dspWriter, subscription, false);
                    try
                    {
                        base.dspWriter.BeginTransaction();
                        subscription.id = AutoIncrement.GetKey("SubscriberManagment.SubscriptionID");
                        subscription.timeBinding = DataTimeBinding.LowerBound;
                        subscription.ticksTo = -1;
                        base.dspWriter.InsertSingleTableRecord<Subscription>(base.m_DBName, subscription, new string[] { "__AID" }, new object[] { AID });
                        ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(subscription.id, subscription.ticksFrom, subscription));

                        clearSubscriptionCache();
                        _updateNumber.contractUpdateNumber++;

                        base.dspWriter.CommitTransaction();
                        id = subscription.id;
                    }
                    catch (Exception exception2)
                    {
                        exception = exception2;
                        base.dspWriter.RollBackTransaction();
                        throw;
                    }
                }
                finally
                {
                    base.dspWriter.restoreReadDB();
                }
            }
            return id;
        }

        public void AddSurveyReading(int AID, ReadingEntryClerk clerk, MeterSurveyData data)
        {
            throw new NotImplementedException("Depreciated");
        }

        private static void AddToActivePeriods(SQLHelper dspReader, DateTime lastActiveDate, DateTime endActivePeriod, List<BillPeriod> activePeriods)
        {
            BillPeriod[] periodArray = AccountingPeriodHelper.GetTouchedPeriods<BillPeriod>(dspReader, "BillPeriod", lastActiveDate, endActivePeriod);
            foreach (BillPeriod period in periodArray)
            {
                bool flag = false;
                foreach (BillPeriod period2 in activePeriods)
                {
                    if (period2.id == period.id)
                    {
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                {
                    activePeriods.Add(period);
                }
            }
        }

        private void AddUpdate(int AID, GenericDiff diff)
        {
            base.dspWriter.InsertSingleTableRecord<GenericDiff>(base.m_DBName, diff, new string[] { "__AID" }, new object[] { AID });
        }
        

        public void BWFAddMeterReading(int AID, ReadingEntryClerk clerk, BWFMeterReading m, bool allowInCycle)
        {
            BWFReadingBlock block;

            if ((clerk == null) && (m.bwfStatus != BWFStatus.Unread))
            {
                throw new Exception("Only clerks can enter readings");
            }
            SQLHelper reader = base.GetReaderHelper();
            try
            {
                BWFMeterReading reading = BWFGetMeterReadingInternal(reader, m.subscriptionID, m.readingBlockID);
                if (reading != null)
                {
                    if (Account.AmountEqual(reading.reading, m.reading)
                        && Account.AmountEqual(reading.consumption, m.consumption))
                        throw new ServerUserMessage("Reading already added-same reading");
                    throw new ServerUserMessage("Reading already added");
                }
                if (m.bwfStatus == BWFStatus.Read || m.bwfStatus == BWFStatus.Proposed)
                {
                    BillPeriod billPeriod = this.GetBillPeriod(this.BWFGetReadingPeriodIDInternal(reader, m.readingBlockID));
                    this.BWFBlockInActiveAtDate(m.subscriptionID, billPeriod.toDate);
                }
                block = this.BWFGetReadingBlockInternal(reader, m.readingBlockID);
                assertNoBill(m.subscriptionID, m.periodID);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
            if (block == null)
            {
                throw new Exception("Invalid reading block");
            }
            if ((block.entryClerkID != -1) && (block.entryClerkID != clerk.employeeID))
            {
            }
            lock (base.dspWriter)
            {
                if ((clerk == null) && (m.bwfStatus != BWFStatus.Unread))
                {
                    throw new Exception("Only clerks can enter readings");
                }
                base.dspWriter.setReadDB(base.DBName);
                try
                {
                    if (m.readingBlockID == -1)
                    {
                        throw new Exception("Invalid reading block ID");
                    }
                    this.BWFFixReading(base.dspWriter, m);
                    object obj2 = base.dspWriter.ExecuteScalar(string.Concat(new object[] { "Select max(orderN) from ", base.DBName, ".dbo.BWFMeterReading where readingBlockID=", m.readingBlockID }));
                    m.orderN = (obj2 is int) ? (((int)obj2) + 1) : 1;
                    try
                    {
                        base.dspWriter.BeginTransaction();
                        if ((block.entryClerkID == -1) && (clerk != null))
                        {
                            block.entryClerkID = clerk.employeeID;
                            this.logOldReadingBlock(AID, block.id);
                            base.dspWriter.UpdateSingleTableRecord<BWFReadingBlock>(base.DBName, block, new string[] { "__AID" }, new object[] { AID });
                        }
                        this.BWFUpdateOrderN(base.dspWriter, m, false);
                        base.dspWriter.InsertSingleTableRecord<BWFMeterReading>(base.m_DBName, m, new string[] { "__AID" }, new object[] { AID });
                        addAction(AID, ORActionType.setReading, m);
                        _updateNumber.readingUpdateNumber++;
                        base.dspWriter.CommitTransaction();
                    }
                    catch
                    {
                        base.dspWriter.RollBackTransaction();
                        throw;
                    }
                }
                finally
                {
                    base.dspWriter.restoreReadDB();
                }
            }
        }

        public void BWFBatchCorrectReadings(int AID, ReadingEntryClerk clerk, int periodID, BWFReadingProblemType problem, SubscriberType[] types, double consumption, out List<Exception> errors)
        {
            errors = new List<Exception>();
            if (clerk == null)
                throw new ServerUserMessage("The loged in user is not a data entry clerk");
            int[] subscIDs;
            this.bdeAccounting.PushProgress("Collecting information");
            BillPeriod billPeriodInternal = this.GetBillPeriod(periodID);
            string str = "";
            try
            {
                SQLHelper readerHelper = base.GetReaderHelper();
                try
                {
                    string typeList = "";
                    foreach (SubscriberType type in types)
                    {
                        if (string.IsNullOrEmpty(typeList))
                        {
                            typeList = "(" + ((int)type);
                            str = type.ToString();
                        }
                        else
                        {
                            typeList = typeList + "," + ((int)type);
                            str = str + ", " + type.ToString();
                        }
                    }
                    typeList = typeList + ")";
                    string selectSql = null;
                    switch (problem)
                    {
                        case BWFReadingProblemType.NotRead:
                            selectSql = @"SELECT Subscription.[id] FROM {0}.[dbo].[Subscription] 
                                    inner join {0}.[dbo].Subscriber on Subscriber.id=Subscription.subscriberID  where subscriberType in {1} 
                                    and not exists(
                                        Select * from {0}.dbo.BWFMeterReading 
                                            where subscriptionID=Subscription.id and periodID={2} and bwfStatus<>0
                                           )  and {3}";
                            selectSql = string.Format(selectSql, base.DBName, typeList, periodID, SQLHelper.getVersionDataFilter(billPeriodInternal.toDate.Ticks, null));
                            break;

                        case BWFReadingProblemType.Exagerated:
                            selectSql = @"SELECT Subscription.id, BWFMeterReading.consumption FROM {0}.dbo.Subscription 
                                            inner join {0}.[dbo].Subscriber on Subscriber.id=Subscription.subscriberID  
                                            INNER JOIN {0}.dbo.ExaggeratedRange ON subscriberType = ExaggeratedRange.ID INNER JOIN {0}.dbo.BWFMeterReading ON Subscription.id = BWFMeterReading.subscriptionID AND ExaggeratedRange.Value <= BWFMeterReading.consumption 
                                WHERE     (Subscriber.subscriberType in {1}) AND (BWFMeterReading.periodID = {2}) and {3}";
                            selectSql = string.Format(selectSql, base.DBName, typeList, periodID, SQLHelper.getVersionDataFilter(billPeriodInternal.toDate.Ticks, null));
                            break;

                        case BWFReadingProblemType.Negative:
                            selectSql = @"SELECT BWFMeterReading.subscriptionID, BWFMeterReading.consumption 
                                        FROM {0}.dbo.Subscription inner join {0}.[dbo].Subscriber on Subscriber.id=Subscription.subscriberID  
                                        INNER JOIN {0}.dbo.BWFMeterReading ON Subscription.id = BWFMeterReading.subscriptionID 
                                        WHERE (Subscriber.subscriberType in {1}) AND (BWFMeterReading.periodID = {2}) AND (BWFMeterReading.consumption <{3}) and {4}";
                            selectSql = string.Format(selectSql, base.DBName, typeList, periodID, 0.0001, SQLHelper.getVersionDataFilter(billPeriodInternal.toDate.Ticks, null));
                            break;
                    }
                    subscIDs = readerHelper.GetColumnArray<int>(selectSql, 0);
                }
                finally
                {
                    base.ReleaseHelper(readerHelper);
                }
            }
            finally
            {
                this.bdeAccounting.PopProgress();
            }
            this.bdeAccounting.PushProgress("Setting readings");
            try
            {
                Employee employee = this.bdePayroll.GetEmployee(this.m_sysPars.correctionReader);
                if (employee == null)
                {
                    throw new Exception("Correction reader employee not found");
                }
                double averageScaleFactor;
                bool scaledAverage = false;
                String scaleConfig = System.Configuration.ConfigurationManager.AppSettings["subsc_avg_scale"];
                if (!String.IsNullOrEmpty(scaleConfig) && double.TryParse(scaleConfig, out averageScaleFactor))
                    scaledAverage = true;


                lock (base.dspWriter)
                {
                    base.dspWriter.setReadDB(base.DBName);
                    try
                    {

                        BWFReadingBlock currentBlock = null;
                        try
                        {
                            base.dspWriter.BeginTransaction();
                            int count = 1;
                            foreach (int subscID in subscIDs)
                            {
                                try
                                {
                                    Subscription subscr = this.GetSubscription(subscID, billPeriodInternal.toDate.Ticks);
                                    if (subscr != null)
                                    {
                                        if (subscr.subscriptionStatus == SubscriptionStatus.Active)
                                        {
                                            BWFMeterReading reading2;
                                            BWFReadingBlock readingBlock = this.BWFGetReadingBlockInternal(base.dspWriter, this.BWFGetReadingBlockID(base.dspWriter, subscID, periodID));
                                            bool updateReading = true;
                                            if (readingBlock == null)
                                            {
                                                updateReading = false;
                                                if (currentBlock == null)
                                                {
                                                    BWFReadingBlock[] bs = this.GetBlock(periodID, "Unallocted-Estimated");
                                                    if (bs.Length == 0)
                                                    {
                                                        BWFReadingBlock correctionBlock;
                                                        correctionBlock = new BWFReadingBlock();
                                                        correctionBlock.blockName = "Unallocted-Estimated";
                                                        correctionBlock.entryClerkID = clerk.employeeID;
                                                        correctionBlock.id = -1;
                                                        correctionBlock.periodID = periodID;
                                                        correctionBlock.readerID = employee.id;
                                                        correctionBlock.readingEnd = DateTime.Now;
                                                        correctionBlock.readingStart = DateTime.Now;
                                                        correctionBlock.id = this.BWFCreateReadingBlock(AID, clerk, correctionBlock);
                                                        currentBlock = correctionBlock;
                                                    }
                                                    else
                                                        currentBlock = bs[0];

                                                }
                                                readingBlock = currentBlock;
                                            }
                                            BWFMeterReading reading = this.BWFGetMeterPreviousNoneNegativeReadingInternal(base.dspWriter, subscID, periodID);
                                            if (updateReading)
                                            {
                                                reading2 = this.BWFGetMeterReadingInternal(base.dspWriter, subscID, readingBlock.id);
                                                if (consumption < 0.0)
                                                {
                                                    reading2.averageMonths = 6;
                                                    reading2.reading = 0.0;
                                                    reading2.readingType = MeterReadingType.Average;
                                                    if (scaledAverage)
                                                        reading2.method = ReadingMethod.EstimationMethodScaledAverage;
                                                    else
                                                        reading2.method = ReadingMethod.EstimationMethodAverage;
                                                }
                                                else
                                                {
                                                    reading2.averageMonths = 0;
                                                    reading2.reading = ((reading == null) ? 0.0 : reading.reading) + consumption;
                                                    reading2.readingType = MeterReadingType.Normal;
                                                    reading2.method = ReadingMethod.EstimationMethodManual;
                                                }
                                                reading2.bwfStatus = BWFStatus.Read;
                                                this.BWFUpdateMeterReading(AID, clerk, reading2);
                                            }
                                            else
                                            {
                                                reading2 = new BWFMeterReading();
                                                reading2.bwfStatus = BWFStatus.Read;
                                                reading2.entryDate = DateTime.Now;
                                                reading2.orderN = count;
                                                reading2.periodID = periodID;
                                                if (consumption < 0.0)
                                                {
                                                    reading2.averageMonths = 6;
                                                    reading2.reading = 0.0;
                                                    reading2.readingType = MeterReadingType.Average;
                                                    reading2.method = ReadingMethod.EstimationMethodAverage;
                                                }
                                                else
                                                {
                                                    reading2.averageMonths = 0;
                                                    reading2.reading = ((reading == null) ? 0.0 : reading.reading) + consumption;
                                                    reading2.readingType = MeterReadingType.Normal;
                                                    reading2.method = ReadingMethod.EstimationMethodManual;
                                                }
                                                reading2.subscriptionID = subscID;
                                                reading2.readingBlockID = readingBlock.id;
                                                this.BWFAddMeterReading(AID, clerk, reading2, true);
                                            }
                                            this.bdeAccounting.SetProgress((double)(((double)count) / ((double)subscIDs.Length)));
                                            count++;
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Null subscription found for " + subscID);
                                    }
                                }
                                catch (Exception estimateEx)
                                {
                                    errors.Add(estimateEx);
                                }
                            }
                            _updateNumber.readingUpdateNumber++;
                            base.dspWriter.CommitTransaction();
                        }
                        catch
                        {
                            base.dspWriter.RollBackTransaction();
                            throw;
                        }
                    }
                    finally
                    {
                        base.dspWriter.restoreReadDB();
                    }
                }
            }
            finally
            {
                this.bdeAccounting.PopProgress();
            }
        }

        private void BWFBlockInActive(Subscription subsc)
        {
            if (subsc.subscriptionStatus != SubscriptionStatus.Active)
            {
                throw new Exception("Subscription " + subsc.contractNo + " is not active");
            }
        }
        private void BWFBlockDiscontinued(Subscription subsc)
        {
            if (subsc.subscriptionStatus == SubscriptionStatus.Discontinued)
            {
                throw new Exception("Subscription " + subsc.contractNo + " is not active");
            }
        }

        private void BWFBlockInActiveAtDate(int subscriptionID, DateTime date)
        {
            if (this.GetSubscription(subscriptionID, date.Ticks).subscriptionStatus != SubscriptionStatus.Active)
            {
                throw new Exception("Subscription not active on " + date);
            }
        }

        private int BWFCountReadings(DSP dspReader, int blockID)
        {
            string format = "Select count(*) from {0}.dbo.BWFMeterReading where readingBlockID={1}";
            return (int)dspReader.ExecuteScalar(string.Format(format, base.DBName, blockID));
        }

        public int BWFCreateMeterReading(int AID, ReadingEntryClerk clerk, BWFReadingBlock readingBlock, BWFMeterReading[] readings)
        {
            int id;
            lock (base.dspWriter)
            {
                base.dspWriter.setReadDB(base.DBName);
                try
                {
                    base.dspWriter.BeginTransaction();
                    if (this.BWFGetReadingPeriodIDInternal(base.dspWriter, readingBlock.id) != this.m_sysPars.readingPeriod)
                    {
                        throw new Exception("You can enter reading only for the current period");
                    }
                    int num2 = 1;
                    foreach (BWFMeterReading reading in readings)
                    {
                        reading.orderN = num2++;
                        this.BWFFixReading(base.dspWriter, reading);
                    }
                    readingBlock.id = AutoIncrement.GetKey("SubscriberManagment.ReadingBlockID");
                    base.dspWriter.InsertSingleTableRecord<BWFReadingBlock>(base.m_DBName, readingBlock);
                    int num3 = 1;
                    foreach (BWFMeterReading reading in readings)
                    {
                        reading.readingBlockID = readingBlock.id;
                        if (this.GetSubscription(reading.subscriptionID, GetBillPeriod(reading.periodID).toDate.Ticks).subscriptionStatus != SubscriptionStatus.Active)
                        {
                            throw new Exception("Subscription not active");
                        }
                        reading.orderN = num3;
                        base.dspWriter.InsertSingleTableRecord<BWFMeterReading>(base.m_DBName, reading, new string[] { "__AID" }, new object[] { AID });
                        num3++;
                    }
                    _updateNumber.readingUpdateNumber++;
                    base.dspWriter.CommitTransaction();
                    id = readingBlock.id;
                }
                catch (Exception exception)
                {
                    base.dspWriter.RollBackTransaction();
                    throw new Exception("Error on creating BWFMeterReading", exception);
                }
                finally
                {
                    base.dspWriter.restoreReadDB();
                }
            }
            return id;
        }

        public int BWFCreateReadingBlock(int AID, ReadingEntryClerk clerk, BWFReadingBlock block)
        {
            int id;
            lock (base.dspWriter)
            {
                this.BWFValidateReaderEmployee(base.dspWriter, block.readerID);
                if (this.GetBillPeriod(block.periodID) == null)
                {
                    throw new Exception("Invalid period");
                }
                try
                {
                    base.dspWriter.BeginTransaction();
                    if (((int)base.dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.BWFReadingBlock where blockName='{1}' and periodID={2}", base.DBName, block.blockName, block.periodID))) > 0)
                    {
                        throw new Exception("Reading block name already used");
                    }
                    block.entryClerkID = -1;
                    block.id = AutoIncrement.GetKey("SubscriberManagment.BWFReadingBlockID");
                    base.dspWriter.InsertSingleTableRecord<BWFReadingBlock>(base.DBName, block, new string[] { "__AID" }, new object[] { AID });
                    _updateNumber.readingUpdateNumber++;
                    base.dspWriter.CommitTransaction();
                    id = block.id;
                }
                catch
                {
                    base.dspWriter.RollBackTransaction();
                    throw;
                }
            }
            return id;
        }

        public void BWFDeleteMeterReading(int AID, ReadingEntryClerk clerk, int subscriptionID, int periodID)
        {
            lock (base.dspWriter)
            {
                //this.BWFBlockInActiveAtDate(subscriptionID,GetBillPeriod(periodID).midTime);
                base.dspWriter.setReadDB(base.DBName);
                try
                {
                    int blockID = this.BWFGetReadingBlockID(base.dspWriter, subscriptionID, periodID);
                    BWFMeterReading m = this.BWFGetMeterReadingInternal(base.dspWriter, subscriptionID, blockID);
                    if (m.bwfStatus == BWFStatus.Read)
                    {
                        throw new Exception("It is not possible to delete an item with reading");
                    }

                    try
                    {
                        base.dspWriter.BeginTransaction();
                        this.logOldMeterReading(AID, subscriptionID, blockID);
                        base.dspWriter.DeleteSingleTableRecrod<BWFMeterReading>(base.m_DBName, new object[] { blockID, subscriptionID });
                        addAction(AID, ORActionType.deleteReading, new SubscriptionPeriod(subscriptionID, periodID));
                        this.BWFUpdateOrderN(base.dspWriter, m, true);
                        _updateNumber.readingUpdateNumber++;
                        base.dspWriter.CommitTransaction();
                    }
                    catch (Exception exception)
                    {
                        base.dspWriter.RollBackTransaction();
                        throw new Exception("Error on deleting meter reading", exception);
                    }
                }
                finally
                {
                    base.dspWriter.restoreReadDB();
                }
            }
        }

        public void BWFDeleteReadingBlock(int AID, ReadingEntryClerk clerk, int blockID)
        {
            lock (base.dspWriter)
            {
                base.dspWriter.setReadDB(base.DBName);
                try
                {
                    base.dspWriter.BeginTransaction();
                    if (((int)base.dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.BWFMeterReading where readingBlockID={1} and bwfStatus<>{2}", base.DBName, blockID, 0))) > 0)
                    {
                        throw new Exception("The reading sheet can't be deleted because it contains readings");
                    }
                    this.logOldReadingBlock(AID, blockID);
                    foreach (int num in base.dspWriter.GetColumnArray<int>(string.Format("Select subscriptionID from {0}.dbo.BWFMeterReading where readingBlockID={1}", base.DBName, blockID), 0))
                    {
                        this.logOldMeterReading(AID, num, blockID);
                    }
                    base.dspWriter.DeleteSingleTableRecrod<BWFReadingBlock>(base.m_DBName, new object[] { blockID });
                    _updateNumber.readingUpdateNumber++;
                    base.dspWriter.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base.dspWriter.RollBackTransaction();
                    throw new Exception("Error on deleting reading block", exception);
                }
                finally
                {
                    base.dspWriter.restoreReadDB();
                }
            }
        }

        // BDE exposed
        public BWFMeterReading getAverageReading(int subscriptionID, int periodID, int nmonths)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                BWFMeterReading ret = new BWFMeterReading();
                ret.periodID = periodID;
                ret.subscriptionID = subscriptionID;
                BWFMeterReading reading = ret;
                double totalConsumption = 0.0;
                int numberOfMonths = 0;
                double prevReading = 0.0;
                for (int i = 0; i < nmonths; i++)
                {
                    reading = this.BWFGetSimpleMeterPreviousReadingInternal(dspReader, reading.subscriptionID, periodID);
                    if (reading == null)
                    {
                        break;
                    }
                    totalConsumption += reading.consumption;
                    numberOfMonths++;
                    if (i == 0)
                    {
                        prevReading = reading.reading;
                    }
                }
                if (numberOfMonths == 0)
                    return null;
                ret.consumption = Math.Round(totalConsumption / (double)numberOfMonths, 0);
                ret.reading = prevReading + ret.consumption;
                ret.readingType = MeterReadingType.Average;
                ret.averageMonths = numberOfMonths;
                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }

        }
        public void BWFFixReading(SQLHelper dspReader, BWFMeterReading r)
        {
            r.periodID = this.BWFGetReadingBlockInternal(dspReader, r.readingBlockID).periodID;
            if (r.bwfStatus == BWFStatus.Unread)
            {
                r.consumption = 0.0;
                r.reading = 0.0;
                r.readingType = MeterReadingType.Normal;
            }
            else
            {
                BWFMeterReading reading;
                ReadingReset reset = getReadingReset(r.subscriptionID, r.periodID);
                if (r.readingType == MeterReadingType.Normal)
                {
                    if (reset == null)
                    {
                        reading = this.BWFGetMeterPreviousNoneNegativeReadingInternal(dspReader, r.subscriptionID, this.BWFGetReadingPeriodIDInternal(dspReader, r.readingBlockID));
                        if (reading == null)
                        {
                            r.consumption = r.reading;
                        }
                        else
                        {
                            r.consumption = r.reading - reading.reading;
                        }
                    }
                    else
                    {
                        r.readingType = MeterReadingType.MeterReset;
                        r.consumption = r.reading - reset.reading;
                    }
                }
                else if (r.readingType == MeterReadingType.Average)
                {
                    if (reset != null)
                        throw new ServerUserMessage("Average estimation can't be made because the meter is reset");

                    double totalConsumption = 0.0;
                    double nMonths = 0.0;
                    reading = r;

                    int previousPeriodID = r.periodID;
                    List<int> readPeriod = new List<int>();
                    List<BWFMeterReading> readings = new List<BWFMeterReading>();
                    BWFMeterReading thisReading;
                    for (int i = 0; i < r.averageMonths; i++)
                    {
                        previousPeriodID = GetPreviousBillPeriod(previousPeriodID).id;
                        thisReading = this.BWFGetMeterReadingByPeriod(r.subscriptionID, previousPeriodID);
                        if (thisReading != null && (thisReading.bwfStatus != BWFStatus.Read || thisReading.consumption < 0))
                            thisReading = null;
                        readings.Add(thisReading);
                        readPeriod.Add(previousPeriodID);
                        if (thisReading != null)
                            totalConsumption += thisReading.consumption;
                        /*reading = this.BWFGetSimpleMeterPreviousReadingInternal(dspReader, reading.subscriptionID, this.BWFGetReadingPeriodIDInternal(dspReader, reading.readingBlockID));
                        if (reading == null)
                        {
                            break;
                        }
                        totalConsumption += reading.consumption;
                        nMonths++;
                        if (i == 0)
                        {
                            previosReading = reading.reading;
                        }*/
                    }
                    int startIndex = -1;
                    for (int i = 0; i < readings.Count; i++)
                    {
                        if (readings[i] == null)
                            continue;
                        startIndex = i;
                        break;
                    }
                    int endEndex = -1;
                    if (startIndex != -1)
                    {
                        for (int i = readings.Count - 1; i >= 0; i--)
                        {
                            if (readings[i] == null)
                                continue;
                            endEndex = i;
                            break;
                        }
                    }
                    if (startIndex == -1)
                        nMonths = 0;
                    else
                        nMonths = endEndex - startIndex + 1;
                    if (nMonths == 0.0)
                    {
                        r.consumption = 0.0;
                    }
                    else
                    {
                        r.consumption = Math.Round((double)(totalConsumption / nMonths), 0);
                    }
                    if (reset == null)
                    {
                        BWFMeterReading prevReading = this.BWFGetMeterPreviousNoneNegativeReadingInternal(dspReader, r.subscriptionID, this.BWFGetReadingPeriodIDInternal(dspReader, r.readingBlockID));
                        r.reading = (prevReading == null ? 0 : prevReading.reading) + r.consumption;
                    }
                    else
                        r.reading = reset.reading + r.consumption;
                }
                else if (r.readingType == MeterReadingType.MeterReset)
                {
                    if (reset != null)
                    {
                        r.consumption = r.reading - reset.reading;
                    }
                }
            }

            var maxConsumptionConfig = System.Configuration.ConfigurationManager.AppSettings["max_consumption"];
            var maxConsumption = 10000.0;
            if (!String.IsNullOrEmpty(maxConsumptionConfig))
                if (!double.TryParse(maxConsumptionConfig, out maxConsumption))
                    maxConsumption = 10000.0;
            if (r.consumption > maxConsumption)
                throw new InvalidOperationException($"Consumption of more than {maxConsumption} is not allowed. Contract no:{GetSubscription(r.subscriptionID,DateTime.Now.Ticks).contractNo}");
        }

        public ReadingEntryClerk BWFGetClerk(int employeeID)
        {
            ReadingEntryClerk clerk;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                ReadingEntryClerk[] sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<ReadingEntryClerk>("employeeID=" + employeeID);
                if (sTRArrayByFilter.Length == 0)
                {
                    return null;
                }
                clerk = sTRArrayByFilter[0];
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return clerk;
        }

        public ReadingEntryClerk BWFGetClerkByUserID(string p)
        {
            ReadingEntryClerk clerk;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                ReadingEntryClerk[] sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<ReadingEntryClerk>("userID='" + p + "'");
                if (sTRArrayByFilter.Length == 0)
                {
                    return null;
                }
                clerk = sTRArrayByFilter[0];
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return clerk;
        }

        private List<BWFMeterReading> BWFGetMeterLaterReadings(SQLHelper dspReader, int subcriptionID, int periodID)
        {
            BillPeriod billPeriodInternal = this.GetBillPeriodInternal(dspReader, periodID);
            List<BWFMeterReading> list = new List<BWFMeterReading>();
            string sql = string.Concat(new object[] { "SELECT     readingBlockID\r\n        FROM         ", base.DBName, ".dbo.vwBWFMeterReading m INNER JOIN\r\n                      ", base.DBName, ".dbo.BillPeriod b ON m.periodID = b.id\r\n        WHERE     subscriptionID =", subcriptionID, " AND fromDate > '", billPeriodInternal.fromDate, "' AND periodID <>", periodID, " order by fromDate" });
            int[] columnArray = dspReader.GetColumnArray<int>(sql, 0);
            foreach (int num in columnArray)
            {
                list.Add(this.BWFGetMeterReadingInternal(dspReader, subcriptionID, num));
            }
            return list;
        }

        private BWFMeterReading BWFGetMeterPreviousNoneNegativeReadingInternal(SQLHelper dsp, int subcriptionID, int periodID)
        {
            BillPeriod billPeriodInternal = this.GetBillPeriodInternal(dsp, periodID);
            string format = "SELECT TOP (1) {0}.dbo.BWFMeterReading.readingBlockID FROM {0}.dbo.BillPeriod INNER JOIN {0}.dbo.BWFMeterReading ON {0}.dbo.BillPeriod.id = {0}.dbo.BWFMeterReading.periodID WHERE  bwfStatus=1 and consumption>=0 and   ({0}.dbo.BWFMeterReading.subscriptionID = {1}) AND ({0}.dbo.BillPeriod.fromDate < '{2}') AND ({0}.dbo.BWFMeterReading.periodID <> {3}) ORDER BY {0}.dbo.BillPeriod.fromDate DESC";
            format = string.Format(format, new object[] { base.DBName, subcriptionID, billPeriodInternal.fromDate, periodID });
            object obj2 = dsp.ExecuteScalar(format);
            if (obj2 is int)
            {
                return this.BWFGetMeterReadingInternal(dsp, subcriptionID, (int)obj2);
            }
            return null;
        }

        public BWFMeterReading BWFGetMeterPreviousReading(int subcriptionID, int periodID)
        {
            BWFMeterReading reading;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                reading = this.BWFGetMeterPreviousNoneNegativeReadingInternal(readerHelper, subcriptionID, periodID);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return reading;
        }

        public BWFMeterReading BWFGetMeterReading(int subcriptionID, int blockID)
        {
            BWFMeterReading reading;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                reading = this.BWFGetMeterReadingInternal(readerHelper, subcriptionID, blockID);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return reading;
        }
        // BDE exposed
        public BWFMeterReading BWFGetMeterReadingByPeriodID(int subcriptionID, int periodID)
        {
            BWFMeterReading reading;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                reading = this.BWFGetMeterReadingInternal(readerHelper, subcriptionID, BWFGetReadingBlockID(readerHelper, subcriptionID, periodID));
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return reading;
        }
        // BDE exposed
        public BWFMeterReading BWFGetMeterReadingByPeriod(int subcriptionID, int peridID)
        {
            BWFMeterReading reading;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                reading = this.BWFGetMeterReadingInternal(readerHelper, subcriptionID, BWFGetReadingBlockID(readerHelper, subcriptionID, peridID));
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return reading;
        }

        private BWFMeterReading BWFGetMeterReadingInternal(SQLHelper dspReader, int subcriptionID, int blockID)
        {
            string filter = string.Concat(new object[] { "readingBlockID=", blockID, " and subscriptionID=", subcriptionID });
            BWFMeterReading[] sTRArrayByFilter = dspReader.GetSTRArrayByFilter<BWFMeterReading>(filter);
            if (sTRArrayByFilter.Length == 0)
            {
                return null;
            }
            return sTRArrayByFilter[0];
        }

        public BWFMeterReading[] BWFGetMeterReadings(int subscriptionID)
        {
            BWFMeterReading[] sTRArrayByFilter;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<BWFMeterReading>("subscriptionID=" + subscriptionID);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return sTRArrayByFilter;
        }

        public BWFMeterReading[] BWFGetMeterReadings(int kebele, int subscriptionID, int periodID, int employeeID, int index, int pageSize, out int NRecords)
        {
            return BWFGetMeterReadings(
                new MeterReadingFilter()
                {
                    kebeles = kebele == -1 ? null : new int[] { kebele },
                    customerTypes = null,
                    meterReaders = employeeID == -1 ? null : new int[] { employeeID },
                    periods = periodID == -1 ? null : new int[] { periodID },
                    readingStatus = null,
                    subscriptions = subscriptionID == -1 ? null : new int[] { subscriptionID },
                }
                , index, pageSize, out NRecords);
        }
        // BDE exposed
        public BWFMeterReading[] BWFGetMeterReadings(MeterReadingFilter filter, int index, int pageSize, out int NRecords)
        {
            BWFMeterReading[] readingArray;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                SQLHelper helper2 = readerHelper.Clone();
                IDataReader reader = null;
                try
                {
                    string criteria = null;

                    if (!ArrayExtension.isNullOrEmpty(filter.subscriptions))
                    {
                        string cr = null;
                        foreach (int ival in filter.subscriptions)
                        {
                            cr = INTAPS.StringExtensions.AppendOperand(cr, ",", ival.ToString());
                        }
                        criteria = INTAPS.StringExtensions.AppendOperand(criteria, " AND ", "subscriptionID in ({0})".format(cr));
                    }
                    if (!ArrayExtension.isNullOrEmpty(filter.meterReaders))
                    {
                        string cr = null;
                        foreach (int ival in filter.meterReaders)
                        {
                            cr = INTAPS.StringExtensions.AppendOperand(cr, ",", ival.ToString());
                        }
                        criteria = INTAPS.StringExtensions.AppendOperand(criteria, " AND ", "readerID in ({0})".format(cr));
                    }
                    if (!ArrayExtension.isNullOrEmpty(filter.periods))
                    {
                        string cr = null;
                        foreach (int ival in filter.periods)
                        {
                            cr = INTAPS.StringExtensions.AppendOperand(cr, ",", ival.ToString());
                        }
                        criteria = INTAPS.StringExtensions.AppendOperand(criteria, " AND ", "periodID in ({0})".format(cr));
                    }
                    if (!ArrayExtension.isNullOrEmpty(filter.readingStatus))
                    {
                        string cr = null;
                        foreach (int ival in filter.readingStatus)
                        {
                            cr = INTAPS.StringExtensions.AppendOperand(cr, ",", ival.ToString());
                        }
                        criteria = INTAPS.StringExtensions.AppendOperand(criteria, " AND ", "bwfStatus in ({0})".format(cr));
                    }

                    string sql = "SELECT     BWFMeterReading.subscriptionID, readingBlockID,periodID FROM {0}.dbo.BWFMeterReading inner join {0}.dbo.BillPeriod on BWFMeterReading.periodID=BillPeriod.id".format(this.DBName);
                    if (criteria != null)
                    {
                        sql = sql + " WHERE " + criteria;
                    }
                    sql = sql + " order by BillPeriod.fromDate desc";
                    reader = helper2.ExecuteReader(sql);
                    List<BWFMeterReading> list = new List<BWFMeterReading>();
                    NRecords = 0;
                    while (reader.Read())
                    {
                        int id = (reader[0] is int) ? ((int)reader[0]) : -1;
                        int blockID = (reader[1] is int) ? ((int)reader[1]) : -1;
                        int thisPeriodID = (reader[2] is int) ? ((int)reader[2]) : -1;

                        BillPeriod prd = this.GetBillPeriod(thisPeriodID);
                        if ((filter.kebeles == null) || ArrayExtension.indexOf(filter.kebeles, this.GetSubscriptionInternal(readerHelper, id, prd.toDate.Ticks, false).Kebele) != -1)
                        {
                            NRecords++;
                            if ((NRecords - 1 >= index && NRecords <= index + pageSize) || pageSize == -1)
                            {
                                BWFMeterReading item = this.BWFGetMeterReading(id, blockID);
                                list.Add(item);
                            }
                        }
                    }
                    readingArray = list.ToArray();
                }
                finally
                {
                    if (!((reader == null) || reader.IsClosed))
                    {
                        reader.Close();
                    }
                    helper2.CloseConnection();
                }
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return readingArray;
        }

        public int BWFGetReadingBlockID(int subcriptionID, int periodID)
        {
            int num;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                num = this.BWFGetReadingBlockID(readerHelper, subcriptionID, periodID);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return num;
        }

        private int BWFGetReadingBlockID(SQLHelper dspReader, int subcriptionID, int periodID)
        {
            string format = "SELECT     readingBlockID from {0}.dbo.BWFMeterReading WHERE     ({0}.dbo.BWFMeterReading.subscriptionID = {1}) AND ({0}.dbo.BWFMeterReading.periodID = {2})";
            object obj2 = dspReader.ExecuteScalar(string.Format(format, base.DBName, subcriptionID, periodID));
            if (obj2 is int)
            {
                return (int)obj2;
            }
            return -1;
        }

        public BWFReadingBlock BWFGetReadingBlockInternal(SQLHelper dspReader, int blockID)
        {
            BWFReadingBlock[] sTRArrayByFilter = dspReader.GetSTRArrayByFilter<BWFReadingBlock>("id=" + blockID);
            if (sTRArrayByFilter.Length == 0)
            {
                return null;
            }
            return sTRArrayByFilter[0];
        }

        public int BWFGetReadingPeriodID(int blockID)
        {
            int num;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                num = this.BWFGetReadingPeriodIDInternal(readerHelper, blockID);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return num;
        }

        private int BWFGetReadingPeriodIDInternal(SQLHelper dspReader, int blockID)
        {
            return this.BWFGetReadingBlockInternal(dspReader, blockID).periodID;
        }

        private BWFMeterReading BWFGetSimpleMeterPreviousReadingInternal(SQLHelper dspReader, int subcriptionID, int periodID)
        {
            BillPeriod billPeriodInternal = this.GetBillPeriodInternal(dspReader, periodID);
            string format = @"SELECT     TOP (1) {0}.dbo.BWFMeterReading.readingBlockID
                                FROM         {0}.dbo.BillPeriod INNER JOIN
                                {0}.dbo.BWFMeterReading ON {0}.dbo.BillPeriod.id = {0}.dbo.BWFMeterReading.periodID
                            WHERE   ({0}.dbo.BWFMeterReading.subscriptionID = {1}) 
                                    AND ({0}.dbo.BillPeriod.fromDate < '{2}') 
                                    AND ({0}.dbo.BWFMeterReading.periodID <> {3})
                        ORDER BY {0}.dbo.BillPeriod.fromDate DESC";
            format = string.Format(format, new object[] { base.DBName, subcriptionID, billPeriodInternal.fromDate, periodID });
            object obj2 = dspReader.ExecuteScalar(format);
            if (obj2 is int)
            {
                return this.BWFGetMeterReadingInternal(dspReader, subcriptionID, (int)obj2);
            }
            return null;
        }

        public void BWFImportReadingBlocks(int AID, int destPeriodID, int[] blockID)
        {
            lock (base.dspWriter)
            {
                base.dspWriter.setReadDB(base.DBName);
                try
                {
                    base.dspWriter.BeginTransaction();
                    this.bdeAccounting.PushProgress("Imporiting " + blockID.Length + " blocks");
                    int num = 0;
                    BillPeriod period = GetBillPeriod(destPeriodID);
                    foreach (int num2 in blockID)
                    {
                        int num3;
                        BWFReadingBlock block = this.BWFGetReadingBlockInternal(base.dspWriter, num2);
                        BWFMeterReading[] readingArray = this.GetReadings(num2, 0, -1, out num3);
                        block.id = -1;
                        block.periodID = destPeriodID;
                        block.id = this.BWFCreateReadingBlock(AID, null, block);
                        this.bdeAccounting.SetChildWeight(1.0 / ((double)blockID.Length));
                        this.bdeAccounting.PushProgress(string.Concat(new object[] { "Importing ", readingArray.Length, " readings for blockID:", num2 }));
                        int num4 = 0;
                        foreach (BWFMeterReading reading in readingArray)
                        {
                            if (GetSubscriptionStatusAtDate(reading.subscriptionID, period.toDate) != SubscriptionStatus.Active)
                                continue;
                            reading.readingBlockID = block.id;
                            reading.periodID = block.periodID;
                            reading.bwfStatus = BWFStatus.Unread;
                            reading.reading = 0.0;
                            reading.readingType = MeterReadingType.Normal;
                            this.BWFAddMeterReading(AID, null, reading, false);
                            num4++;
                            this.bdeAccounting.SetProgress((double)(((double)num4) / ((double)readingArray.Length)));
                        }
                        this.bdeAccounting.PopProgress();
                        num++;
                        this.bdeAccounting.SetProgress((double)(((double)num) / ((double)blockID.Length)));
                    }
                    _updateNumber.readingUpdateNumber++;
                    base.dspWriter.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base.dspWriter.RollBackTransaction();
                    throw new Exception("Error trying to import block", exception);
                }
                finally
                {
                    base.dspWriter.restoreReadDB();
                }
            }
        }

        public void BWFImportReadings(int AID, int dest, int src, int[] subscriptionID)
        {
            lock (base.dspWriter)
            {
                base.dspWriter.setReadDB(base.DBName);
                try
                {
                    base.dspWriter.BeginTransaction();
                    BWFReadingBlock block = this.GetBlock(dest);
                    if (block == null)
                    {
                        throw new Exception("Destination block doesn't exist");
                    }
                    object obj2 = base.dspWriter.ExecuteScalar(string.Format("Select max(orderN) from {0}.dbo.BWFMeterReading where readingBlockID={1}", base.DBName, dest));
                    int num = (obj2 is int) ? (((int)obj2) + 1) : 1;
                    BWFReadingBlock block2 = this.GetBlock(src);
                    if (block2 == null)
                    {
                        throw new Exception("Invalid source block");
                    }
                    if (block.periodID != block2.periodID)
                    {
                        throw new Exception("Only blocks from the same period can be merged");
                    }
                    foreach (int num3 in subscriptionID)
                    {
                        BWFMeterReading reading = this.BWFGetMeterReading(num3, src);
                        if (reading == null)
                        {
                            throw new Exception("Reading doesn't existin in the source block");
                        }
                        if (reading.readingBlockID != src)
                        {
                            throw new Exception("BUG: destination reading from another block");
                        }
                        this.logOldMeterReading(AID, reading.subscriptionID, reading.readingBlockID);
                        base.dspWriter.Update(base.DBName, "BWFMeterReading", new string[] { "readingBlockID", "orderN", "__AID" }, new object[] { dest, num++, AID }, string.Concat(new object[] { "readingBlockID=", reading.readingBlockID, " and subscriptionID=", reading.subscriptionID }));
                    }
                    _updateNumber.readingUpdateNumber++;
                    base.dspWriter.CommitTransaction();
                }
                catch
                {
                    base.dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    base.dspWriter.restoreReadDB();
                }
            }
        }

        private int BWFMaxOrderN(SQLHelper dspReader, int readingBlockID)
        {
            string format = "Select Max(orderN) from {0}.dbo.BWFMeterReading where readingBlockID={1}";
            object obj2 = dspReader.ExecuteScalar(string.Format(format, base.DBName, readingBlockID));
            if (obj2 is int)
            {
                return (int)obj2;
            }
            return 0;
        }

        public void BWFMergeBlock(int AID, int dest, int[] sources)
        {
            lock (base.dspWriter)
            {
                if (sources.Length != 0)
                {
                    base.dspWriter.setReadDB(base.DBName);
                    try
                    {
                        base.dspWriter.BeginTransaction();
                        BWFReadingBlock block = this.GetBlock(dest);
                        if (block == null)
                        {
                            throw new Exception("Destination block doesn't exist");
                        }
                        object obj2 = base.dspWriter.ExecuteScalar(string.Format("Select max(orderN) from {0}.dbo.BWFMeterReading where readingBlockID={1}", base.DBName, dest));
                        int num = (obj2 is int) ? (((int)obj2) + 1) : 1;
                        foreach (int num2 in sources)
                        {
                            int num3;
                            BWFReadingBlock block2 = this.GetBlock(num2);
                            if (block2 == null)
                            {
                                throw new Exception("Invalid source block");
                            }
                            if (block.periodID != block2.periodID)
                            {
                                throw new Exception("Only blocks from the same period can be merged");
                            }
                            BWFMeterReading[] readingArray = this.GetReadings(num2, 0, -1, out num3);
                            foreach (BWFMeterReading reading in readingArray)
                            {
                                if (reading.readingBlockID != num2)
                                {
                                    throw new Exception("BUG: destination reading from another block");
                                }
                                this.logOldMeterReading(AID, reading.subscriptionID, reading.readingBlockID);
                                base.dspWriter.Update(base.DBName, "BWFMeterReading", new string[] { "readingBlockID", "orderN", "__AID" }, new object[] { dest, num++, AID }, string.Concat(new object[] { "readingBlockID=", reading.readingBlockID, " and subscriptionID=", reading.subscriptionID }));
                            }
                            this.BWFDeleteReadingBlock(AID, null, num2);
                        }
                        _updateNumber.readingUpdateNumber++;
                        base.dspWriter.CommitTransaction();
                    }
                    catch (Exception exception)
                    {
                        base.dspWriter.RollBackTransaction();
                        throw new Exception("Error on merging block", exception);
                    }
                    finally
                    {
                        base.dspWriter.restoreReadDB();
                    }
                }
            }
        }

        public void BWFUpdateBlock(int AID, BWFReadingBlock block)
        {
            lock (base.dspWriter)
            {
                if (((int)base.dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.BWFReadingBlock where blockName='{1}' and id<>{2} and periodID={3}", new object[] { base.DBName, block.blockName, block.id, block.periodID }))) > 0)
                {
                    throw new Exception("Reading block name already used");
                }
                logOldReadingBlock(AID, block.id);
                base.dspWriter.UpdateSingleTableRecord<BWFReadingBlock>(base.DBName, block, new string[] { "__AID" }, new object[] { AID });
            }
        }

        public void BWFUpdateMeterReading(int AID, ReadingEntryClerk clerk, BWFMeterReading reading)
        {
            BWFReadingBlock block;
            List<BWFMeterReading> later;
            BillPeriod billPeriod;
            if (clerk == null)
            {
                throw new Exception("Only clerks can enter readings");
            }
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                if (reading.bwfStatus == BWFStatus.Read || reading.bwfStatus == BWFStatus.Proposed)
                {
                    billPeriod = this.GetBillPeriod(this.BWFGetReadingPeriodIDInternal(readerHelper, reading.readingBlockID));
                    this.BWFBlockInActiveAtDate(reading.subscriptionID, billPeriod.toDate);
                }
                later = this.BWFGetMeterLaterReadings(readerHelper, reading.subscriptionID, this.BWFGetReadingPeriodID(reading.readingBlockID));
                block = this.BWFGetReadingBlockInternal(readerHelper, reading.readingBlockID);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            if (block == null)
            {
                throw new Exception("Invalid reading block");
            }
            if ((block.entryClerkID != -1) && (block.entryClerkID != clerk.employeeID))
            {
            }
            lock (base.dspWriter)
            {
                base.dspWriter.setReadDB(base.DBName);
                try
                {
                    base.dspWriter.BeginTransaction();
                    if (block.entryClerkID == -1)
                    {
                        block.entryClerkID = clerk.employeeID;
                        this.logOldReadingBlock(AID, block.id);
                        base.dspWriter.UpdateSingleTableRecord<BWFReadingBlock>(base.DBName, block, new string[] { "__AID" }, new object[] { AID });
                    }
                    billPeriod = this.GetBillPeriod(this.BWFGetReadingPeriodIDInternal(base.dspWriter, reading.readingBlockID));
                    reading.periodID = billPeriod.id;
                    this.BWFBlockInActiveAtDate(reading.subscriptionID, billPeriod.toDate);
                    this.BWFUpdateMeterReadingInternal(base.dspWriter, AID, reading);
                    foreach (BWFMeterReading reading2 in later)
                    {
                        this.BWFUpdateMeterReadingInternal(base.dspWriter, AID, reading2);
                    }
                    this.BWFUpdateOrderN(base.dspWriter, reading, false);
                    _updateNumber.readingUpdateNumber++;
                    base.dspWriter.CommitTransaction();
                }
                catch
                {
                    base.dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    base.dspWriter.restoreReadDB();
                }
            }
        }
        // BDE exposed
        public void BWFAcceptRejectReading(int AID, bool accept, ReadingEntryClerk clerk, int periodID, MeterReadingFilter filter, int[] specific)
        {
            IList<BWFMeterReading> readings;
            if (filter == null)
            {
                readings = new List<BWFMeterReading>();
                foreach (int subscriptionID in specific)
                {
                    BWFMeterReading r = BWFGetMeterReadingByPeriodID(subscriptionID, periodID);
                    if (r != null && r.bwfStatus == BWFStatus.Proposed)
                        readings.Add(r);
                }
            }
            else
            {
                filter.periods = new int[] { periodID };
                filter.readingStatus = new BWFStatus[] { BWFStatus.Proposed };
                int N;
                readings = BWFGetMeterReadings(filter, 0, -1, out N);
            }
            foreach (BWFMeterReading r in readings)
            {
                if (r.bwfStatus == BWFStatus.Proposed)
                {
                    if (accept)
                        r.bwfStatus = BWFStatus.Read;
                    else
                        r.bwfStatus = BWFStatus.Unread;
                    BWFUpdateMeterReading(AID, clerk, r);
                }
            }
        }
        void assertNoBill(int connectionID, int periodID)
        {
            CustomerBillRecord[] existing = getBills(Accounting.GetDocumentTypeByType(typeof(WaterBillDocument)).id, -1, connectionID, periodID);
            if (existing.Length > 0)
                throw new ServerUserMessage("This reading can't be updated because bill is allready generated based on it for connection id=" + connectionID);

        }
        void assertNoBill(int connectionID, long time)
        {
            BillPeriod p = AccountingPeriodHelper.GetPeriodForDate<BillPeriod>(dspWriter, "BillPeriod", new DateTime(time));
            if (p != null)
            {
                assertNoBill(connectionID, p.id);
            }
        }
        private void BWFUpdateMeterReadingInternal(SQLHelper dspReader, int AID, BWFMeterReading reading)
        {
            if ((reading.readingBlockID == -1) || (reading.subscriptionID == -1))
            {
                throw new Exception("Invalid reading");
            }
            assertNoBill(reading.subscriptionID, reading.periodID);
            BWFMeterReading r = this.BWFGetMeterReading(reading.subscriptionID, reading.readingBlockID);
            r.reading = reading.reading;
            r.consumption = reading.consumption;
            r.averageMonths = reading.averageMonths;
            r.readingType = reading.readingType;
            r.bwfStatus = reading.bwfStatus;
            r.readingX = reading.readingX;
            r.readingY = reading.readingY;
            r.extraInfo = reading.extraInfo;
            r.readingTime = reading.readingTime;
            r.readingRemark = reading.readingRemark;
            r.readingProblem = reading.readingProblem;
            r.method = reading.method;
            this.BWFFixReading(dspReader, r);
            this.logOldMeterReading(AID, r.subscriptionID, r.readingBlockID);
            base.dspWriter.UpdateSingleTableRecord<BWFMeterReading>(base.m_DBName, r, new string[] { "__AID" }, new object[] { AID });
            addAction(AID, ORActionType.setReading, reading);
        }

        private void BWFUpdateOrderN(SQLHelper dspReader, BWFMeterReading m, bool delete)
        {
            if (((int)dspReader.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.BWFMeterReading where readingBlockID={1} and orderN={2} and subscriptionID<>{3}", new object[] { base.DBName, m.readingBlockID, m.orderN, m.subscriptionID }))) > 0)
            {
                if (delete)
                {
                    dspReader.ExecuteNonQuery(string.Format("Update {0}.dbo.BWFMeterReading set orderN=orderN-1 where readingBlockID={1} and orderN>{2} and subscriptionID<>{3}", new object[] { base.DBName, m.readingBlockID, m.orderN, m.subscriptionID }));
                }
                else
                {
                    dspReader.ExecuteNonQuery(string.Format("Update {0}.dbo.BWFMeterReading set orderN=orderN+1 where readingBlockID={1} and orderN>={2} and subscriptionID<>{3}", new object[] { base.DBName, m.readingBlockID, m.orderN, m.subscriptionID }));
                }
            }
        }

        private void BWFValidateClerkEmployee(SQLHelper dspWriter, int employeeID)
        {
            if (((int)dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.ReadingEntryClerk where employeeID={1}", base.DBName, employeeID))) == 0)
            {
                throw new Exception("Invalid meter reader");
            }
        }

        private void BWFValidateReaderEmployee(SQLHelper dspWriter, int employeeID)
        {
            if (((int)dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.MeterReaderEmployee where employeeID={1}", base.DBName, employeeID))) == 0)
            {
                throw new Exception("Invalid meter reader");
            }
        }
        public void changeSubscription(int AID, Subscription data)
        {
            _billingRule.fixSubscriptionDataForSave(data);
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    Subscription oldData;
                    Subscription[] test = dspWriter.GetSTRArrayByFilterVersioned<Subscription>("id=" + data.id, data.ticksFrom);
                    if (test.Length == 0)
                    {
                        long test2 = dspWriter.GetSTRNextVersionDate<Subscription>("id=" + data.id, data.ticksFrom);
                        if (test2 < 1)
                            throw new ServerUserMessage("No existing data to change");
                        test = dspWriter.GetSTRArrayByFilterVersioned<Subscription>("id=" + data.id, test2);
                    }
                    oldData = test[0];
                    switch (VersionedID.compareVersion(oldData.ticksFrom, data.ticksFrom))
                    {
                        case 1:
                            data.ticksTo = oldData.ticksFrom;
                            data.timeBinding = DataTimeBinding.LowerAndUpperBound;
                            dspWriter.InsertSingleTableRecord(this.DBName, data, new string[] { "__AID" }, new object[] { AID });
                            ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(data.id, data.ticksFrom, data));
                            break;
                        case -1:
                            data.ticksTo = oldData.ticksTo;
                            data.timeBinding = oldData.timeBinding;
                            dspWriter.InsertSingleTableRecord(this.DBName, data, new string[] { "__AID" }, new object[] { AID });
                            oldData.ticksTo = data.ticksFrom;
                            oldData.timeBinding = DataTimeBinding.LowerAndUpperBound;
                            dspWriter.logDeletedData(AID, this.DBName + ".dbo.Subscription", SQLHelper.getVersionDataFilter(oldData.ticksFrom, "id=" + data.id));
                            dspWriter.UpdateSingleTableRecord(this.DBName, oldData, new string[] { "__AID" }, new object[] { AID });
                            ApplicationServer.sendDiffMsg(AID, GenericDiff.getUpdateDiff(oldData.id, oldData.ticksFrom, oldData));
                            break;
                        default:
                            data.ticksTo = oldData.ticksTo;
                            data.timeBinding = oldData.timeBinding;
                            dspWriter.logDeletedData(AID, this.DBName + ".dbo.Subscription", SQLHelper.getVersionDataFilter(data.ticksFrom, "id=" + data.id));
                            dspWriter.UpdateSingleTableRecord(this.DBName, data, new string[] { "__AID" }, new object[] { AID });
                            ApplicationServer.sendDiffMsg(AID, GenericDiff.getUpdateDiff(data.id, data.ticksFrom, data));
                            break;
                    }
                    clearSubscriptionCache();
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        void deleteSubscription(int AID, int subscriptionID, long version)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    Subscription oldData;
                    Subscription[] test = dspWriter.GetSTRArrayByFilterVersioned<Subscription>("id=" + subscriptionID, version);
                    if (test.Length == 0)
                    {
                        throw new ServerUserMessage("No existing data to delete");
                    }
                    oldData = test[0];
                    long prevVersionDate = dspWriter.GetSTRPreviousVersionDate<Subscription>("id=" + subscriptionID, oldData.ticksFrom);
                    if (prevVersionDate > 0)
                    {
                        dspWriter.logDeletedData(AID, this.DBName + ".dbo.Subscription", SQLHelper.getVersionDataFilter(prevVersionDate, "id=" + oldData.id));
                        dspWriter.Update(this.DBName, "Subscription", new string[] { "dateTo", "timeBinding", "__AID" }, new object[] { oldData.ticksTo, oldData.timeBinding, AID }, SQLHelper.getVersionDataFilter(prevVersionDate, "id=" + oldData.id));
                    }
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.Subscription", SQLHelper.getVersionDataFilter(oldData.ticksFrom, "id=" + oldData.id));
                    dspWriter.DeleteSingleTableRecrod<Subscription>(this.DBName, subscriptionID, version);
                    clearSubscriptionCache();
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        public void ChangeSubcriberMeter(int AID, int subscriptionID, DateTime changeDate, MeterData meter)
        {
            lock (base.dspWriter)
            {
                changeDate = DateTime.Now;
                base.dspWriter.setReadDB(base.DBName);
                try
                {

                    Subscription subsc = GetSubscriptionInternal(dspWriter, subscriptionID, changeDate.Ticks, false);
                    if (subsc == null)
                        throw new ServerUserMessage("No subscription found");
                    this.BWFBlockDiscontinued(subsc);
                    try
                    {
                        base.dspWriter.BeginTransaction();
                        subsc.meterData = meter;
                        subsc.ticksFrom = changeDate.Ticks;
                        changeSubscription(AID, subsc);
                        base.dspWriter.CommitTransaction();
                    }
                    catch (Exception exception)
                    {
                        base.dspWriter.RollBackTransaction();
                        throw new Exception("Error on changing subcriber meter", exception);
                    }
                }
                finally
                {
                    base.dspWriter.restoreReadDB();
                }
            }
        }

        public void ChangeSubcriptionStatus(int AID, int subscriptionID, DateTime changeDate, SubscriptionStatus newStatus, bool overrideChangeDate)
        {
            lock (base.dspWriter)
            {
                if (!overrideChangeDate)
                    changeDate = DateTime.Now;
                base.dspWriter.setReadDB(base.DBName);
                try
                {

                    Subscription rec = GetSubscriptionInternal(dspWriter, subscriptionID, changeDate.Ticks, false);
                    if (rec == null)
                        throw new ServerUserMessage("No subscription found");
                    switch (rec.subscriptionStatus)
                    {
                        case SubscriptionStatus.Active:
                            switch (newStatus)
                            {
                                case SubscriptionStatus.Pending:
                                case SubscriptionStatus.Active:
                                    throw new Exception("Invalid status change.");
                            }
                            break;

                        case SubscriptionStatus.Diconnected:
                            switch (newStatus)
                            {
                                case SubscriptionStatus.Pending:
                                case SubscriptionStatus.Diconnected:
                                    throw new Exception("Invalid status change.");
                            }
                            break;

                        case SubscriptionStatus.Discontinued:
                            throw new Exception("The subscription is dicontinued. No change is allowed.");
                    }
                    try
                    {
                        base.dspWriter.BeginTransaction();
                        rec.subscriptionStatus = newStatus;
                        rec.ticksFrom = changeDate.Ticks;
                        changeSubscription(AID, rec);
                        base.dspWriter.CommitTransaction();
                    }
                    catch (Exception exception)
                    {
                        base.dspWriter.RollBackTransaction();
                        throw new Exception("Error on changing subcriber meter", exception);
                    }
                }
                finally
                {
                    base.dspWriter.restoreReadDB();
                }
            }
        }

        public int ChangeSubcriptionType(int AID, int subscriptionID, DateTime changeDate, SubscriptionType newType)
        {
            throw new Exception("Changing sucbription type not supported.");
        }

        public void CreateMeterReaderEmployee(MeterReaderEmployee employee)
        {
            lock (base.dspWriter)
            {
                base.dspWriter.InsertSingleTableRecord<MeterReaderEmployee>(base.m_DBName, employee);
            }
        }

        public void CreateReadingEntryClerk(ReadingEntryClerk employee)
        {
            lock (base.dspWriter)
            {
                Employee emp = bdePayroll.GetEmployee(employee.employeeID);
                if (emp == null)
                    throw new ServerUserMessage("Invalid employee ID");
                if (string.IsNullOrEmpty(emp.loginName))
                    throw new ServerUserMessage("Employee {0} doesn't have login name", emp.employeeID);
                employee.userID = emp.loginName;
                base.dspWriter.InsertSingleTableRecord<ReadingEntryClerk>(base.m_DBName, employee);
            }
        }

        public void DeleteEntryClerk(int employeeID)
        {
            lock (base.dspWriter)
            {
                base.dspWriter.DeleteSingleTableRecrod<ReadingEntryClerk>(base.m_DBName, new object[] { employeeID });
            }
        }

        private void logOldHistoryEntry(int AID, int id)
        {
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.SubscriptionHistoryItem", "id=" + id);
        }

        public void DeleteMeterReaderEmployee(int employeeID, int periodID)
        {
            lock (base.dspWriter)
            {
                base.dspWriter.DeleteSingleTableRecrod<MeterReaderEmployee>(base.m_DBName, new object[] { employeeID, periodID });
            }
        }

        public void DeleteSubscriber(int AID, int subscriberID)
        {
            Subscriber customer = GetSubscriber(subscriberID);
            lock (base.dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    this.logOldSubscriberData(AID, subscriberID);
                    if (bdeAccounting.AccountHasTransaction<Account>(customer.accountID)
                        || bdeAccounting.AccountHasTransaction<Account>(customer.depositAccountID)
                        )
                        throw new ServerUserMessage("The customer has transaction, it can't be deleted");
                    if (((int)dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.CustomerBill where customerID={1}", this.DBName, subscriberID))) > 0)
                        throw new ServerUserMessage("The customer has bills, it can't be deleted");
                    foreach (int acID in new int[] { customer.accountID, customer.depositAccountID })
                    {
                        if (bdeAccounting.GetAccount<Account>(acID) != null)
                            bdeAccounting.DeleteAccount<Account>(AID, acID, true);
                    }
                    foreach (Subscription subsc in dspWriter.GetSTRArrayByFilter<Subscription>("timeBinding=1 and subscriberID=" + subscriberID))
                        DeleteSubscription(AID, subsc.id);

                    base.dspWriter.DeleteSingleTableRecrod<Subscriber>(base.m_DBName, new object[] { subscriberID });

                    _updateNumber.customerUpdateNumber++;
                    clearSubscriptionCache();
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }

        public void DeleteSubscription(int AID, int subscriptionID)
        {

            Subscription[] subscriptionHistory = this.GetSubscriptionHistory(subscriptionID);
            if (subscriptionHistory.Length == 0)
                throw new ServerUserMessage("Subscription not found id " + subscriptionID);
            if (BWFGetMeterReadings(subscriptionID).Length > 0)
                throw new ServerUserMessage("Susbcription with reading can't be deleted.");
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.Subscription", "id=" + subscriptionID);
            base.dspWriter.Delete(base.m_DBName, "Subscription", "id=" + subscriptionID);
            clearSubscriptionCache();
            _updateNumber.contractUpdateNumber++;
        }

        public Kebele[] GetAllKebeles()
        {
            Kebele[] sTRArrayByFilter;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<Kebele>(null);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return sTRArrayByFilter;
        }

        public MeterReaderEmployee[] GetAllMeterReaderEmployees(int periodID)
        {
            MeterReaderEmployee[] employeeArray2;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                MeterReaderEmployee[] sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<MeterReaderEmployee>("periodID=" + periodID);
                foreach (MeterReaderEmployee employee in sTRArrayByFilter)
                {
                    try
                    {
                        employee.emp = this.bdePayroll.GetEmployee(employee.employeeID);
                    }
                    catch
                    {
                        employee.emp = null;
                    }
                }
                employeeArray2 = sTRArrayByFilter;
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return employeeArray2;
        }
        // BDE exposed
        public MeterReaderEmployee GetMeterReaderEmployees(int periodID, int employeeID)
        {
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                MeterReaderEmployee[] ret = readerHelper.GetSTRArrayByFilter<MeterReaderEmployee>("periodID=" + periodID + " and employeeID=" + employeeID);
                foreach (MeterReaderEmployee employee in ret)
                {
                    try
                    {
                        employee.emp = this.bdePayroll.GetEmployee(employee.employeeID);
                    }
                    catch
                    {
                        employee.emp = null;
                    }
                }
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
        }

        public ReadingEntryClerk[] GetAllReadingEntryClerk()
        {
            ReadingEntryClerk[] clerkArray2;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                ReadingEntryClerk[] sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<ReadingEntryClerk>(null);
                foreach (ReadingEntryClerk clerk in sTRArrayByFilter)
                {
                    try
                    {
                        clerk.emp = this.bdePayroll.GetEmployee(clerk.employeeID);
                    }
                    catch
                    {
                        clerk.emp = null;
                    }
                }
                clerkArray2 = sTRArrayByFilter;
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return clerkArray2;
        }

        public BillPeriod GetBillPeriod(int periodID)
        {
            BillPeriod period;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                period = AccountingPeriodHelper.GetPayPeriod<BillPeriod>(readerHelper, "BillPeriod", periodID);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return period;
        }

        public BillPeriod GetBillPeriodInternal(SQLHelper dspReader, int periodID)
        {
            return AccountingPeriodHelper.GetPayPeriod<BillPeriod>(dspReader, "BillPeriod", periodID);
        }

        public BillPeriod[] GetBillPeriods(int Year, bool EthiopianYear)
        {
            BillPeriod[] periodArray2;
            lock (base.dspWriter)
            {
                base.dspWriter.setReadDB(base.DBName);
                try
                {
                    base.dspWriter.BeginTransaction();
                    BillPeriod[] periodArray = AccountingPeriodHelper.GetPayPeriods<BillPeriod>(base.m_DBName, base.dspWriter, base.dspWriter, "BillPeriod", Year, EthiopianYear);
                    base.dspWriter.CommitTransaction();
                    periodArray2 = periodArray;
                }
                catch
                {
                    base.dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    base.dspWriter.restoreReadDB();
                }
            }
            return periodArray2;
        }

        public BWFReadingBlock GetBlock(int blockID)
        {
            BWFReadingBlock block;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                BWFReadingBlock[] sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<BWFReadingBlock>("id=" + blockID);
                if (sTRArrayByFilter.Length == 0)
                {
                    return null;
                }
                block = sTRArrayByFilter[0];
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return block;
        }

        public BWFReadingBlock[] GetBlock(int period, string name)
        {
            BWFReadingBlock[] sTRArrayByFilter;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<BWFReadingBlock>(string.Concat(new object[] { "blockName='", name, "' and periodID=", period }));
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return sTRArrayByFilter;
        }

        public BWFReadingBlock[] GetBlocks(int periodID, int readerID)
        {
            BWFReadingBlock[] sTRArrayByFilter;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                if (readerID == -1)
                {
                    return readerHelper.GetSTRArrayByFilter<BWFReadingBlock>("periodID=" + periodID);
                }
                sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<BWFReadingBlock>(string.Concat(new object[] { "periodID=", periodID, " and readerID=", readerID }));
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return sTRArrayByFilter;
        }


        public MeterReaderEmployee GetMeterReadingEmployee(int id)
        {
            MeterReaderEmployee employee;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                MeterReaderEmployee[] sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<MeterReaderEmployee>("employeeID=" + id);
                if (sTRArrayByFilter.Length == 0)
                {
                    return null;
                }
                try
                {
                    sTRArrayByFilter[0].emp = this.bdePayroll.GetEmployee(id);
                }
                catch
                {
                    sTRArrayByFilter[0].emp = null;
                }
                employee = sTRArrayByFilter[0];
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return employee;
        }
        // BDE exposed
        public MeterReaderEmployee GetMeterReadingEmployee(string loginName)
        {
            Employee emp = bdePayroll.GetEmployeeByLoginName(loginName);
            if (emp == null)
                return null;
            return GetMeterReadingEmployee(emp.id);
        }

        public PrePaymentBill[] GetPrepaymentData(int subscriptionID)
        {
            PrePaymentBill[] sTRArrayByFilter;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<PrePaymentBill>("subscriptionID=" + subscriptionID);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return sTRArrayByFilter;
        }

        public BillPeriod GetPreviousBillPeriod(int periodID)
        {
            BillPeriod period;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                period = AccountingPeriodHelper.GetPreviousPeriod<BillPeriod>(readerHelper, "BillPeriod", this.GetBillPeriodInternal(readerHelper, periodID));
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return period;
        }

        public SingeRateSetting[] GetRates(Subscription sub, BillPeriod p)
        {
            SingeRateSetting[] settingArray;
            List<SingeRateSetting> list = new List<SingeRateSetting>();
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                foreach (SingeRateSetting setting in this.m_sysPars.rateSetting.setting)
                {
                    if (!AccountingPeriodHelper.WithinValidPariod<BillPeriod>(readerHelper, "BillPeriod", setting.period.PeriodFrom, setting.period.PeriodTo, p.id))
                    {
                        continue;
                    }
                    bool flag = false;
                    foreach (SubscriberType type in setting.subType)
                    {
                        if (type == sub.subscriber.subscriberType)
                        {
                            flag = true;
                            break;
                        }
                    }
                    if (flag)
                    {
                        flag = false;
                        foreach (SubscriptionType type2 in setting.subscType)
                        {
                            if (type2 == sub.subscriptionType)
                            {
                                flag = true;
                                break;
                            }
                        }
                        if (flag)
                        {
                            if (!setting.allKebeles)
                            {
                                flag = false;
                                foreach (int num in setting.kebele)
                                {
                                    if (num == sub.Kebele)
                                    {
                                        flag = true;
                                        break;
                                    }
                                }
                                if (!flag)
                                {
                                    continue;
                                }
                            }
                            list.Add(setting);
                        }
                    }
                }
                settingArray = list.ToArray();
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return settingArray;
        }

        public BWFMeterReading[] GetReadings(int blockID, int index, int pageSize, out int NRecords)
        {
            BWFMeterReading[] readingArray;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                string str = "Select BWFMeterReading.* from BWFMeterReading \r\n                                inner join Subscription on BWFMeterReading.subscriptionID=Subscription.id\r\n                                where readingBlockID=" + blockID;
                readingArray = readerHelper.GetSTRArrayByFilter<BWFMeterReading>("readingBlockID=" + blockID, index, pageSize, out NRecords);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return readingArray;
        }
        // BDE exposed
        public BWFDescribedMeterReading[] BWFGetDescribedReadings(int blockID, int index, int pageSize, out int NRecords)
        {
            SQLHelper helper = base.GetReaderHelper();
            try
            {
                string sql = "Select BWFMeterReading.* from BWFMeterReading inner join Subscription on BWFMeterReading.subscriptionID=Subscription.id where timeBinding=1 and readingBlockID="
                    + blockID;
                BWFMeterReading[] readings = helper.GetSTRArrayByFilter<BWFMeterReading>("readingBlockID=" + blockID, index, pageSize, out NRecords);
                BWFDescribedMeterReading[] ret = new BWFDescribedMeterReading[readings.Length];
                int i = 0;
                foreach (BWFMeterReading r in readings)
                {
                    Subscription subsc = GetSubscriptionInternal(helper, r.subscriptionID, GetBillPeriod(r.periodID).toDate.Ticks, true);
                    ret[i] = new BWFDescribedMeterReading(subsc, r);
                    BWFMeterReading reading = this.BWFGetMeterPreviousReading(r.subscriptionID, r.periodID);
                    if (reading != null)
                    {
                        ret[i].previousPeriod = this.GetBillPeriod(reading.periodID);
                        ret[i].previousReading = reading;
                    }
                    i++;
                }
                return ret;

            }
            finally
            {
                base.ReleaseHelper(helper);
            }

        }

        internal SingeRateSetting GetSingleRate(Subscription sub, BillPeriod p)
        {
            SingeRateSetting[] rates = this.GetRates(sub, p);
            if ((rates.Length == 0) || (rates.Length > 1))
            {
                return null;
            }
            return rates[0];
        }

        public Subscriber GetSubscriber(int id)
        {
            Subscriber subscriber;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                Subscriber[] ret = readerHelper.GetSTRArrayByFilter<Subscriber>("id=" + id);
                if (ret.Length == 0)
                {
                    return null;
                }
                subscriber = ret[0];
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return subscriber;
        }
        // BDE exposed
        public Subscriber GetSubscriber(string code)
        {
            Subscriber subscriber;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                Subscriber[] ret = readerHelper.GetSTRArrayByFilter<Subscriber>("customerCode='" + code + "'");
                if (ret.Length == 0)
                {
                    return null;
                }
                subscriber = ret[0];
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return subscriber;
        }


        public Subscription GetSubscription(int id, long verion)
        {

            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                return GetSubscriptionInternal(readerHelper, id, verion, true);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }

        }



        public Subscription GetSubscription(string contractNO, long version)
        {
            Subscription subscription;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                Subscription[] ret = readerHelper.GetSTRArrayByFilter<Subscription>(SQLHelper.getVersionDataFilter(version, "contractNo='" + contractNO + "'"));
                if (ret.Length == 0)
                {
                    return null;
                }
                ret[0].subscriber = this.GetSubscriber(ret[0].subscriberID);
                subscription = ret[0];
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return subscription;
        }
        public Subscription[] GetSubscriptionHistory(int subscriptionID)
        {
            Subscription[] ret;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                ret = readerHelper.GetSTRArrayByFilter<Subscription>("id=" + subscriptionID);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return ret;
        }





        public Subscription GetSubscriptionInternal(SQLHelper dspReader, int id, long version, bool includeSubscriber)
        {
            Subscription[] ret = dspReader.GetSTRArrayByFilter<Subscription>(SQLHelper.getVersionDataFilter(version, "id=" + id));
            if (ret.Length == 0)
            {
                return null;
            }
            if (includeSubscriber)
            {
                ret[0].subscriber = this.GetSubscriber(ret[0].subscriberID);
            }
            return ret[0];
        }

        public Subscription GetSubscriptionNoException(string contractNO)
        {
            Subscription subscription;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                Subscription[] sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<Subscription>("contractNo='" + contractNO + "'");
                if (sTRArrayByFilter.Length == 0)
                {
                    return null;
                }
                sTRArrayByFilter[0].subscriber = this.GetSubscriber(sTRArrayByFilter[0].subscriberID);
                subscription = sTRArrayByFilter[0];
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return subscription;
        }

        public Subscription[] GetSubscriptions(int subscriberID, long version)
        {
            Subscription[] sTRArrayByFilter;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<Subscription>(SQLHelper.getVersionDataFilter(version, "subscriberID=" + subscriberID));
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return sTRArrayByFilter;
        }

        public SubscriberSearchResult[] GetSubscriptions(string query, SubscriberSearchField fields, bool multipleVersion, int kebele,
            int zone, int dma,
            int index, int pageSize, out int NRecords)
        {
            if (pageSize < 2)
                throw new Exception("Invalid query");
            SubscriberSearchResult[] resultArray;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                SQLHelper helper2 = readerHelper.Clone();
                IDataReader reader = null;
                if ((pageSize < 1) || (pageSize > 1000000))
                {
                    throw new Exception("Invalid page size.");
                }
                try
                {
                    bool textSearch = query != null && !query.Trim().Equals("");
                    string conQuery = null;
                    string uppperQuery = null;
                    if (textSearch)
                    {
                        query = AmharicText.NormalizeSpace(query);
                        conQuery = AmharicText.GetLatinConsonant(query).ToUpper();
                        uppperQuery = query.ToUpper();
                    }
                    List<SubscriberSearchResult> res = new List<SubscriberSearchResult>();
                    string sql = "Select Subscription.id,name,contractNo,Subscriber.id,customerCode,serialNo,phoneNo from  Subscriber LEFT OUTER JOIN Subscription ON Subscription.subscriberID=Subscriber.ID";
                    if (!multipleVersion)
                        sql += " where (timeBinding=1 or (timeBinding is Null))";
                    if (kebele != -1)
                    {
                        sql += " and (Subscription.kebele=" + kebele + ")";
                    }
                    if (dma != -1)
                    {
                        sql += " and (Subscription.dma=" + dma + ")";
                    }
                    if (zone != -1)
                    {
                        sql += " and (Subscription.pressureZone=" + zone + ")";
                    }

                    reader = helper2.ExecuteReader(sql);
                    List<SubscriberSearchResult> list = new List<SubscriberSearchResult>();

                    NRecords = 0;
                    while (reader.Read())
                    {
                        string name = reader[1] as string;
                        if (name != null)
                            name = AmharicText.NormalizeSpace(name);
                        string contractNo = reader[2] as string;
                        string upperName = name != null ? name.ToUpper() : null;
                        string conName = name != null ? AmharicText.GetLatinConsonant(name) : null;
                        string customerCode = reader[4] as string;
                        string meterNo = reader[5] as string;
                        string phoneNo = reader[6] as string;
                        SubscriberSearchResult sr;
                        int phoneIndex;
                        if (textSearch)
                        {
                            if (((fields & SubscriberSearchField.CustomerCode) == SubscriberSearchField.CustomerCode) && customerCode != null && customerCode.Equals(query))
                                sr = new SubscriberSearchResult(-11);
                            else if (((fields & SubscriberSearchField.CustomerCode) == SubscriberSearchField.CustomerCode) && customerCode != null && customerCode.IndexOf(query, StringComparison.CurrentCultureIgnoreCase) == 0)
                                sr = new SubscriberSearchResult(-10);
                            else if (((fields & SubscriberSearchField.ConstactNo) == SubscriberSearchField.ConstactNo) && contractNo != null && contractNo.Equals(query))
                                sr = new SubscriberSearchResult(-9);
                            else if (((fields & SubscriberSearchField.MeterNo) == SubscriberSearchField.MeterNo) && meterNo != null && meterNo.Equals(query))
                                sr = new SubscriberSearchResult(-8);
                            else if (((fields & SubscriberSearchField.MeterNo) == SubscriberSearchField.MeterNo) && meterNo != null && meterNo.IndexOf(query, StringComparison.CurrentCultureIgnoreCase) == 0)
                                sr = new SubscriberSearchResult(-7);
                            else if (((fields & SubscriberSearchField.CustomerName) == SubscriberSearchField.CustomerName) && name != null && name.Equals(query))
                                sr = new SubscriberSearchResult(-6);
                            else if (((fields & SubscriberSearchField.CustomerName) == SubscriberSearchField.CustomerName) && name != null && upperName.Equals(uppperQuery))
                                sr = new SubscriberSearchResult(-5);
                            else if (((fields & SubscriberSearchField.CustomerName) == SubscriberSearchField.CustomerName) && name != null && conName.Equals(conQuery))
                                sr = new SubscriberSearchResult(-4);
                            else if (((fields & SubscriberSearchField.ConstactNo) == SubscriberSearchField.ConstactNo) && contractNo != null && contractNo.IndexOf(query) == 0)
                                sr = new SubscriberSearchResult(-3);
                            else if (((fields & SubscriberSearchField.CustomerName) == SubscriberSearchField.CustomerName) && name != null && upperName.IndexOf(uppperQuery, StringComparison.CurrentCultureIgnoreCase) == 0)
                                sr = new SubscriberSearchResult(-2);
                            else if (((fields & SubscriberSearchField.CustomerName) == SubscriberSearchField.CustomerName) && name != null && conName.IndexOf(conQuery, StringComparison.CurrentCultureIgnoreCase) == 0)
                                sr = new SubscriberSearchResult(-1);
                            else if (((fields & SubscriberSearchField.PhoneNo) == SubscriberSearchField.PhoneNo) && phoneNo != null && (phoneIndex = phoneNo.IndexOf(query, StringComparison.CurrentCultureIgnoreCase)) == (phoneNo.Length - query.Length) && phoneIndex != -1)
                                sr = new SubscriberSearchResult(-1);
                            else
                                continue;
                        }
                        else
                        {
                            sr = new SubscriberSearchResult(-7);
                        }
                        int id = (reader[3] is int) ? ((int)reader[3]) : -1;
                        int subscriptionID = (reader[0] is int) ? ((int)reader[0]) : -1;

                        NRecords++;
                        if (((NRecords - 1) >= index) && (NRecords <= (index + pageSize)))
                        {
                            if (subscriptionID != -1)
                            {
                                Subscription[] aSubsc = readerHelper.GetSTRArrayByFilter<Subscription>("ticksTo=-1 and id=" + subscriptionID);
                                if (aSubsc.Length == 0)
                                {
                                    sr.subscription = null;
                                }
                                else
                                {
                                    sr.subscription = aSubsc[0];
                                }
                            }
                            sr.subscriber = this.GetSubscriber(id);
                            list.Add(sr);
                        }
                    }
                    resultArray = list.ToArray();
                }
                finally
                {
                    if (!((reader == null) || reader.IsClosed))
                    {
                        reader.Close();
                    }
                    helper2.CloseConnection();
                }
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return resultArray;
        }
        // BDE exposed
        public SubscriberSearchResult[] GetCustomers(string query, int kebele, int index, int pageSize, out int NRecords)
        {
            if (pageSize < 2)
                throw new Exception("Invalid query");
            SubscriberSearchResult[] resultArray;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                SQLHelper helper2 = readerHelper.Clone();
                IDataReader reader = null;
                if ((pageSize < 1) || (pageSize > 1000))
                {
                    throw new Exception("Invalid page size.");
                }
                try
                {
                    bool textSearch = query != null && !query.Trim().Equals("");
                    string conQuery = null;
                    string uppperQuery = null;
                    if (textSearch)
                    {
                        query = AmharicText.NormalizeSpace(query);
                        conQuery = AmharicText.GetLatinConsonant(query).ToUpper();
                        uppperQuery = query.ToUpper();
                    }
                    List<SubscriberSearchResult> res = new List<SubscriberSearchResult>();
                    string sql = "Select id,name,customerCode from Subscriber";// where ((name like '%" + str + "%') or (contractNo like '%" + str + "%'))";
                    if (kebele != -1)
                    {
                        sql += " and (kebele=" + kebele + ") order by name";
                    }
                    reader = helper2.ExecuteReader(sql);
                    List<SubscriberSearchResult> list = new List<SubscriberSearchResult>();

                    NRecords = 0;
                    while (reader.Read())
                    {
                        string name = reader[1] as string;
                        if (name != null)
                            name = AmharicText.NormalizeSpace(name);
                        string contractNo = reader[2] as string;
                        string upperName = name != null ? name.ToUpper() : null;
                        string conName = name != null ? AmharicText.GetLatinConsonant(name) : null;

                        SubscriberSearchResult sr;
                        if (textSearch)
                        {
                            if (contractNo != null && contractNo.Equals(query))
                                sr = new SubscriberSearchResult(-7);
                            else if (name != null && name.Equals(query))
                                sr = new SubscriberSearchResult(-6);
                            else if (name != null && upperName.Equals(uppperQuery))
                                sr = new SubscriberSearchResult(-5);
                            else if (name != null && conName.Equals(conQuery))
                                sr = new SubscriberSearchResult(-4);
                            if (contractNo != null && contractNo.IndexOf(query, StringComparison.CurrentCultureIgnoreCase) == 0)
                                sr = new SubscriberSearchResult(-3);
                            else if (name != null && upperName.IndexOf(uppperQuery, StringComparison.CurrentCultureIgnoreCase) == 0)
                                sr = new SubscriberSearchResult(-2);
                            else if (name != null && conName.IndexOf(conQuery, StringComparison.CurrentCultureIgnoreCase) == 0)
                                sr = new SubscriberSearchResult(-1);
                            else
                                continue;
                        }
                        else
                        {
                            sr = new SubscriberSearchResult(-7);
                        }
                        int id = (int)reader[0];

                        NRecords++;
                        if (((NRecords - 1) >= index) && (NRecords <= (index + pageSize)))
                        {
                            sr.subscriber = this.GetSubscriber(id);
                            list.Add(sr);
                        }
                    }
                    resultArray = list.ToArray();
                }
                finally
                {
                    if (!((reader == null) || reader.IsClosed))
                    {
                        reader.Close();
                    }
                    helper2.CloseConnection();
                }
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return resultArray;
        }
        public MeterSurveyData GetSurveyData(int blockID, string contractNo)
        {
            throw new NotImplementedException("Depricated");
        }

        public object[] GetSystemParameters(string[] fields)
        {
            object[] systemParameters;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                systemParameters = readerHelper.GetSystemParameters<SubscriberManagmentSystemParameters>(this.m_sysPars, fields);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return systemParameters;
        }


        private void InitializeReporting()
        {
            foreach (KeyValuePair<int, IReportProcessor> pair in this.bdeAccounting.ReportProcessors)
            {
                ((ReportProcessorBase)pair.Value).vars.Add("conSM", ReportProcessorBase.CreateConnection("Subscriber"));
                ((ReportProcessorBase)pair.Value).funcs.Add(new PCSalesReport(this, this.bdeAccounting));
                ((ReportProcessorBase)pair.Value).funcs.Add(new PCSalesSummaryReport(this, this.bdeAccounting));
                ((ReportProcessorBase)pair.Value).funcs.Add(new PCSalesVolume(this, this.bdeAccounting));
                ((ReportProcessorBase)pair.Value).funcs.Add(new PCGeoDistance());
                ((ReportProcessorBase)pair.Value).funcs.Add(new PCIsSubscriptionActive(this));
                ((ReportProcessorBase)pair.Value).funcs.Add(new PCChargeList(this, this.bdeAccounting));
                ((ReportProcessorBase)pair.Value).funcs.Add(new PCReaderWorkData(this, this.bdeAccounting));
                ((ReportProcessorBase)pair.Value).funcs.Add(new PCReceiptList(this, this.bdeAccounting));
                ((ReportProcessorBase)pair.Value).funcs.Add(new PCGetReaderAllocation(this));
                ((ReportProcessorBase)pair.Value).funcs.Add(new SMSettingFunction(this));
                ((ReportProcessorBase)pair.Value).funcs.Add(new PCGetDetailedBillingReport(this, this.bdeAccounting));
                ((ReportProcessorBase)pair.Value).funcs.Add(new PCGetCreditSchemeReport(this, this.bdeAccounting));
                ((ReportProcessorBase)pair.Value).funcs.Add(new PCGetCollectionSummary(this));
                ((ReportProcessorBase)pair.Value).funcs.Add(new PCBillLedgerEntry(this));
            }
        }

        public int IssueMeter(MeterIssuanceDocument doc)
        {
            lock (base.dspWriter)
            {
                return -1;
            }
        }

        private void LoadSystemParameters()
        {
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                this.m_sysPars = readerHelper.LoadSystemParameters<SubscriberManagmentSystemParameters>();
            }
            catch (Exception exception)
            {
                ApplicationServer.EventLoger.LogException("Error loading system parameters.", exception);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
        }
        void logOldBill(int AID, string table, int subscriptionID, int periodID)
        {
            base.dspWriter.logDeletedData(AID, this.DBName + ".dbo." + table, string.Format("subscriptionID={0} and periodID={1}", subscriptionID, periodID));
        }
        private void logOldMeterReading(int AID, int subscriptionID, int blockID)
        {
            base.dspWriter.logDeletedData(AID, this.DBName + ".dbo.BWFMeterReading", string.Format("subscriptionID={0} and readingBlockID={1}", subscriptionID, blockID));
        }

        private void logOldReadingBlock(int AID, int blockID)
        {
            base.dspWriter.logDeletedData(AID, this.DBName + ".dbo.BWFReadingBlock", "id=" + blockID);
        }

        private void logOldSubscriberData(int AID, int subId)
        {
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.Subscriber", "id=" + subId);
        }





        private TransactionOfBatch[] fixTransactionBatchCostCenterAccounts(int costCenterID, TransactionOfBatch[] tran)
        {
            foreach (TransactionOfBatch t in tran)
            {
                t.AccountID = getCostCenterAccountID(costCenterID, t.AccountID);
            }
            return tran;
        }

        public int getCostCenterAccountID(int costCenterID, int templateAccountID)
        {
            if (costCenterID == -1)
                return bdeAccounting.GetCostCenterAccount(bdeBERP.SysPars.mainCostCenterID, templateAccountID).id;
            return bdeAccounting.GetOrCreateCostCenterAccount(costCenterID, bdeBERP.SysPars.mainCostCenterID, templateAccountID).id;
        }


        public string useNextCustomerCode(int AID, Subscriber subsciber)
        {

            this.verifyBillingSubsystem();

            int sn = Accounting.GetNextSerial(m_sysPars.customerCodeBatchID);
            string customerCode = _billingRule.formatCustomerCode(subsciber, sn);
            if (customerCode == null)
            {
                Evaluator.Symbolic symbolic = new Evaluator.Symbolic();
                symbolic.SetSymbolProvider(new DefaultProvider());
                symbolic.Expression = m_sysPars.customerCodeFormat;

                if (symbolic.ContainsVariable("sn")) //serial number
                    symbolic["sn"] = new EData(Evaluator.DataType.Int, sn);

                if (symbolic.ContainsVariable("ct")) //customer type
                    symbolic["ct"] = new EData(Evaluator.DataType.Int, (int)subsciber.subscriberType);

                if (symbolic.ContainsVariable("k")) //kebele
                    symbolic["k"] = new EData(Evaluator.DataType.Int, (int)subsciber.Kebele);
                customerCode= symbolic.Evaluate().Value.ToString();
            }
            UsedSerial us = new UsedSerial();
            us.batchID = m_sysPars.customerCodeBatchID;
            us.date = DateTime.Now;
            us.isVoid = false;
            us.note = "Customer code " + customerCode;
            us.val = sn;
            bdeAccounting.UseSerial(AID, us);
            return customerCode;
        }
        public void mergeCustomers(int AID, ReadingEntryClerk clerk, int sid1, int sid2)
        {
            Subscriber c1, c2;
            c1 = this.GetSubscriber(sid1);
            c2 = this.GetSubscriber(sid2);
            if (c1 == null || c2 == null)
                throw new ServerUserMessage("Invalid subscriber ID.");

            if (!c1.name.Trim().Equals(c2.name.Trim()))
                throw new ServerUserMessage("{0} and {1} don't have the same name", c1.customerCode, c2.customerCode);
            Subscription[] ss = this.GetSubscriptions(c1.id, DateTime.Now.Ticks);
            if (ss.Length == 0)
                throw new ServerUserMessage("Subscriptions not found for customers {0}.", c1.id);
            if (ss.Length > 1)
                throw new ServerUserMessage("Multiple subscriptions found for customers {0}.", c1.id);
            Subscription s1 = ss[0];

            ss = this.GetSubscriptions(c2.id, DateTime.Now.Ticks);
            if (ss.Length == 0)
                throw new ServerUserMessage("Subscriptions not found for customers {0}.", c2.id);
            if (ss.Length > 1)
                throw new ServerUserMessage("Multiple subscriptions found for customers {0}.", c2.id);
            Subscription s2 = ss[0];
            BWFMeterReading[] r1 = this.BWFGetMeterReadings(s1.id);
            BWFMeterReading[] r2 = this.BWFGetMeterReadings(s2.id);
            Dictionary<int, BWFMeterReading> ps = new Dictionary<int, BWFMeterReading>();
            foreach (BWFMeterReading r in r2)
                ps.Add(r.periodID, r);
            foreach (BWFMeterReading r in r1)
                if (ps.ContainsKey(r.periodID))
                {
                    BWFMeterReading reading2 = ps[r.periodID];
                    if (r.reading != reading2.reading || r.consumption != reading2.consumption)
                    {
                        throw new ServerUserMessage("Readings for subscription {0} and {1} overlap at period {2}", s1.id, s2.id, r.periodID);
                    }
                    ps.Remove(r.periodID);
                }

            foreach (BWFMeterReading r in r2)
            {
                if (ps.ContainsKey(r.periodID))
                {
                    r.subscriptionID = s1.id;
                    this.BWFAddMeterReading(AID, clerk, r, true);
                }
                else
                {
                    throw new ServerUserMessage("BUG: update reading case not expected.");
                    r.bwfStatus = 0;
                    //this.BWFUpdateMeterReading(AID, clerk, r);
                    //this.BWFDeleteMeterReading(AID, clerk, r.subscriptionID, r.periodID);
                }
            }
            //this.DeleteSubscription(AID, s2.id);
            //this.DeleteSubscriber(AID, c2.id);
        }
        public int RegisterSubscriber(int AID, Subscriber subscriber)
        {
            int id;
            subscriber.id = AutoIncrement.GetKey("SubscriberManagment.SubcriberID");
            if (string.IsNullOrEmpty(subscriber.customerCode))
                subscriber.customerCode = useNextCustomerCode(AID, subscriber);
            else
            {
                if (GetSubscriber(subscriber.customerCode) != null)
                    throw new ServerUserMessage($"Customer code {subscriber.customerCode} is already used");
            }
            base.dspWriter.InsertSingleTableRecord<Subscriber>(base.m_DBName, subscriber, new string[] { "__AID" }, new object[] { AID });
            ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(subscriber.id, subscriber));
            _updateNumber.customerUpdateNumber++;
            id = subscriber.id;
            return id;
        }

        public void ResetUpdateChain()
        {
            throw new NotImplementedException("Method ResetUpdateChain is not upgraded");
            //lock (base.dspWriter)
            //{
            //    Exception exception;
            //    Console.WriteLine("Resetting updates");
            //    Console.WriteLine("Loadding unpaid bill ids");
            //    int[] columnArray = base.dspWriter.GetColumnArray<int>("SELECT accountDocumentID   FROM " + base.DBName + ".dbo.vwBill where accountDocumentID!=-1 and payDocumentID=-1", 0);
            //    Console.WriteLine(columnArray.Length + " bills found");
            //    Console.WriteLine();
            //    int length = columnArray.Length;
            //    List<GenericDiff> list = new List<GenericDiff>();
            //    for (int i = 0; i < columnArray.Length; i++)
            //    {
            //        Console.CursorTop--;
            //        Console.WriteLine(length + "               ");
            //        try
            //        {
            //            int documentID = columnArray[i];
            //            GenericDiff item = new GenericDiff();
            //            item.billDoc = (BillDocument)this.bdeAccounting.GetAccountDocument(documentID, true);
            //            item.objectID = documentID;
            //            item.changeDate = item.billDoc.DocumentDate;
            //            item.changeType = GenericDiffType.Add;
            //            list.Add(item);
            //        }
            //        catch (Exception exception1)
            //        {
            //            exception = exception1;
            //            Console.WriteLine(string.Concat(new object[] { "Error DocID:", columnArray[i], "\n", exception.Message }));
            //        }
            //        length--;
            //    }
            //    Console.WriteLine("Deleting existing data");
            //    Console.WriteLine();
            //    base.dspWriter.ExecuteNonQuery("Delete from " + base.m_DBName + ".dbo.IssuedUpdate");
            //    base.dspWriter.ExecuteNonQuery("Delete from " + base.m_DBName + ".dbo.BillDiff");
            //    base.dspWriter.ExecuteNonQuery("Delete from " + base.m_DBName + ".dbo.SerialDiff");

            //    Console.WriteLine("Saving BillDiffs");
            //    Console.WriteLine();
            //    int count = list.Count;
            //    foreach (GenericDiff bd in list)
            //    {
            //        Console.CursorTop--;
            //        dspWriter.InsertSingleTableRecord<GenericDiff>(this.DBName, bd);
            //        count--;
            //        Console.WriteLine(count + "               ");
            //    }
            //    Console.WriteLine("Done");
            //}
        }

        public void SetSystemParameters(int AID, string[] fields, object[] vals)
        {
            lock (base.dspWriter)
            {
                base.dspWriter.setReadDB(base.DBName);
                try
                {
                    base.dspWriter.SetSystemParameters<SubscriberManagmentSystemParameters>(base.m_DBName, this.m_sysPars, fields, vals, true, AID);
                    _updateNumber.billingConfiguration++;
                }
                finally
                {
                    base.dspWriter.restoreReadDB();
                }
            }
        }



        public void UpdateMeterReaderEmployee(MeterReaderEmployee employee)
        {
            lock (base.dspWriter)
            {
                base.dspWriter.UpdateSingleTableRecord<MeterReaderEmployee>(base.m_DBName, employee);
            }
        }

        public void UpdateSubscriber(int AID, Subscriber subscriber, Subscription[] subsc)
        {
            Subscriber old = GetSubscriber(subscriber.id);
            subscriber.status = old.status;
            subscriber.statusDate = old.statusDate;
            subscriber.customerCode = old.customerCode;
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    UpdateCustomerInternal(AID, subscriber);
                    if (subsc != null)
                    {
                        foreach (Subscription s in subsc)
                        {
                            UpdateSubscription(AID, s);
                        }
                    }
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }

        }

        private void UpdateCustomerInternal(int AID, Subscriber subscriber)
        {
            lock (base.dspWriter)
            {
                try
                {
                    base.dspWriter.BeginTransaction();
                    this.logOldSubscriberData(AID, subscriber.id);
                    base.dspWriter.UpdateSingleTableRecord<Subscriber>(base.m_DBName, subscriber, new string[] { "__AID" }, new object[] { AID });
                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getUpdateDiff(subscriber.id, subscriber));
                    _updateNumber.customerUpdateNumber++;
                    base.dspWriter.CommitTransaction();
                }
                catch
                {
                    base.dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        public void updateSubscriptionCoordinates(int AID, int subscription, int ticksFrom, double meterX, double meterY)
        {
            Subscription s = GetSubscription(subscription, ticksFrom);
            s.waterMeterX = meterX;
            s.waterMeterY = meterY;
            UpdateSubscription(AID, s);
        }
        public void UpdateSubscription(int AID, Subscription subscription)
        {
            _billingRule.fixSubscriptionDataForSave(subscription);
            lock (base.dspWriter)
            {
                base.dspWriter.setReadDB(base.DBName);
                try
                {
                    base.dspWriter.BeginTransaction();
                    Subscription oldData = this.GetSubscription(subscription.id, subscription.ticksFrom);
                    if (oldData.subscriptionStatus == SubscriptionStatus.Discontinued)
                        throw new ServerUserMessage("It is not allowed to change the data of a discontinued connection");
                    this.BWFBlockInActive(oldData);
                    this.ValidateSubscription(base.dspWriter, subscription, true);

                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.Subscription", "id=" + subscription.id);

                    if (subscription.subscriberID < 1)
                        subscription.subscriberID = oldData.subscriberID;
                    base.dspWriter.UpdateSingleTableRecord<Subscription>(base.m_DBName, subscription, new string[] { "__AID" }, new object[] { AID });
                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getUpdateDiff(subscription.id, subscription.ticksFrom, subscription));
                    this.ValidateStatusHistoryEntry(base.dspWriter, subscription.id);
                    clearSubscriptionCache();
                    _updateNumber.contractUpdateNumber++;
                    base.dspWriter.CommitTransaction();
                }
                catch
                {
                    base.dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    base.dspWriter.restoreReadDB();
                }
            }
        }

        private Subscription UpdateSubscriptionInformation(int AID, MeterSurveyData data)
        {
            throw new ServerUserMessage("Depricated");
        }

        public void UpdateSurveyReading(int AID, ReadingEntryClerk clerk, MeterSurveyData data)
        {
            SQLHelper helper;
            Monitor.Enter(helper = base.dspWriter);
            try
            {
                base.dspWriter.BeginTransaction();
                Subscription subscription = this.UpdateSubscriptionInformation(AID, data);
                BWFMeterReading reading = new BWFMeterReading();
                reading.readingType = MeterReadingType.Normal;
                reading.readingBlockID = data.blockID;
                reading.subscriptionID = subscription.id;
                reading.reading = data.reading;
                reading.entryDate = data.date;
                reading.bwfStatus = BWFStatus.Read;
                this.BWFUpdateMeterReading(AID, clerk, reading);
                _updateNumber.readingUpdateNumber++;
                base.dspWriter.CommitTransaction();
            }
            catch
            {
                base.dspWriter.RollBackTransaction();
                throw;
            }
            finally
            {
                Monitor.Exit(helper);
            }
        }

        private void ValidateStatusHistoryEntry(SQLHelper dspReader, int subscriptionID)
        {

        }

        private void ValidateSubscription(SQLHelper dspReader, Subscription subscription, bool forUpdate)
        {
            string str;
            if (subscription == null)
                throw new ServerUserMessage("Subscription null");
            if (subscription.subscriber == null)
                throw new ServerUserMessage("Subscriber null");
            if (GetKebele(subscription.Kebele) == null || GetKebele(subscription.subscriber.Kebele) == null)
                throw new ServerUserMessage("Invalid kebele");
            if (subscription.subscriptionType == SubscriptionType.Unknown)
                throw new ServerUserMessage("Unknown connection type not supported");
            if (subscription.subscriber.subscriberType == SubscriberType.Unknown)
                throw new ServerUserMessage("Unknown customer type not supported");

            if ((subscription.contractNo == null) || ((subscription.contractNo = subscription.contractNo.Trim()) == ""))
            {
                throw new ServerUserMessage("Contract no must be specified.");
            }
            if (forUpdate)
            {
                str = string.Concat(new object[] { "id<>", subscription.id, " and contractNo='", subscription.contractNo, "'" });
            }
            else
            {
                str = "contractNo='" + subscription.contractNo + "'";
            }
            Subscription[] otherSubsc = dspReader.GetSTRArrayByFilter<Subscription>(str);
            if (otherSubsc.Length > 0)
            {
                Subscriber subscriber = this.GetSubscriber(otherSubsc[0].subscriberID);
                throw new Exception("Contract number " + subscription.contractNo + " already assigned to another subscriber " + subscriber.name);
            }
            if (subscription.initialMeterReading < 0.0)
            {
                throw new Exception("Invalid initial meter readgin");
            }



            string sql = string.Format("Select  top 1 contractNo from {0}.dbo.Subscription where ", this.DBName);
            if (!string.IsNullOrEmpty(subscription.serialNo))
            {
                if (forUpdate)
                {
                    sql += " id<>" + subscription.id + " and serialNo='" + subscription.serialNo + "'  and timeBinding=1 and subscriptionStatus<>" + ((int)SubscriptionStatus.Discontinued);
                }
                else
                {
                    sql += "serialNo='" + subscription.serialNo + "'  and timeBinding=1 and subscriptionStatus<>" + ((int)SubscriptionStatus.Discontinued);
                }

                string cn = dspReader.ExecuteScalar(sql) as string;
                if (!string.IsNullOrEmpty(cn))
                {
                    throw new Exception("Meter number proposed for contract no " + subscription.contractNo + " assigned to another subscription with  contract No:" + cn);
                }
            }

        }
        internal void VerifyMeter(string itemCode)
        {
            BIZNET.iERP.TransactionItems material = this.bdeBERP.GetTransactionItems(itemCode);
            if (material == null)
            {
                throw new Exception("Invalid water meter item code " + itemCode);
            }
            if (!this.bdeBERP.isCatageoryCategoryOf(this.m_sysPars.meterMaterialCategory, material.categoryID))
            {
                throw new Exception("Item code provided is not water meter:" + itemCode);
            }
        }

        public PayrollBDE bdePayroll
        {
            get
            {
                if (this.m_payroll == null)
                {
                    this.m_payroll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
                }
                return this.m_payroll;
            }
        }
        internal bool isPrePaidInternal(SQLHelper reader, int subscriptionID)
        {
            return (bool)reader.ExecuteScalar("Select prePaid from " + this.DBName + ".dbo.Subscription where id=" + subscriptionID);
        }
        void addAction(int AID, ORActionType type, object data)
        {
            lock (dspWriter)
            {

                ORAction ac = new ORAction();
                dspWriter.BeginTransaction();
                try
                {
                    ac.id = AutoIncrement.GetKey("SubscriberManagment.ORActionID");
                    ac.actionTime = DateTime.Now;
                    ac.type = type;
                    ac.data = data;
                    _updateNumber.readingUpdateNumber++;
                    //dspWriter.InsertSingleTableRecord(this.DBName, ac, new string[] { "__AID" }, new object[] { AID });
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }

            }
        }
        int getMaxORActionID(INTAPS.RDBMS.SQLHelper reader)
        {
            return (int)reader.ExecuteScalar(string.Format("Select max(id) from {0}.dbo.ORAction", this.DBName));
        }

        ORActionPackage getORActionPackage(int lowerExclusiveBound, int upperInclusiveBound)
        {

            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                return new ORActionPackage(helper.GetSTRArrayByFilter<ORAction>(string.Format("id>{0} and id<={1}", lowerExclusiveBound, upperInclusiveBound)));
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }

        void runORActions(ORAction action)
        {
            SubscriptionPeriod subscPeriod = null;
            lock (dspWriter)
            {
                switch (action.type)
                {
                    case ORActionType.setReading:
                        subscPeriod = (SubscriptionPeriod)action.data;
                        BWFMeterReading reading = BWFGetMeterReadingInternal(dspWriter, subscPeriod.subscriptionID, subscPeriod.periodID);
                        if (reading == null || reading.bwfStatus != BWFStatus.Read)
                        {
                            if (reading == null && !m_sysPars.offlineReadingDB)
                            {
                                throw new ServerUserMessage(string.Format("Block not set for Contract no {0} period {1}"
                                    , GetSubscriptionInternal(dspWriter, subscPeriod.subscriptionID, GetBillPeriod(reading.periodID).toDate.Ticks, false).contractNo
                                    , GetBillPeriodInternal(dspWriter, subscPeriod.periodID).name));
                            }
                            if (reading == null)
                            {

                            }
                        }
                        break;
                    case ORActionType.deleteReading:
                        break;
                    case ORActionType.setBill:
                        if (!m_sysPars.offlineReadingDB)
                            throw new ServerUserMessage("Main database bills can't be updated from offline reading system");
                        break;
                    case ORActionType.deleteBill:
                        if (!m_sysPars.offlineReadingDB)
                            throw new ServerUserMessage("Main database bills can't be updated from offline reading system");
                        break;
                    case ORActionType.createPeriod:
                        if (!m_sysPars.offlineReadingDB)
                            throw new ServerUserMessage("Main database settings can be changed from offline reading system");
                        break;
                    case ORActionType.setCurrentPeriod:
                        break;
                    default:
                        break;
                }
            }
        }
        // BDE exposed
        public Subscription getSubscriptionByMeterNo(string meterNo)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                Subscription[] ret = helper.GetSTRArrayByFilter<Subscription>("serialNo='" + meterNo + "'");
                if (ret.Length > 0)
                {
                    return ret[0];
                }
                return null;
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        // BDE exposed
        public BillPeriod getPeriodForDate(DateTime date)
        {
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                return AccountingPeriodHelper.GetPeriodForDate<BillPeriod>(readerHelper, "BillPeriod", date);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
        }
        // BDE exposed
        public double getExageratedConsumption(int subscriberID)
        {
            string sql = @"SELECT  ExaggeratedRange.Value FROM {0}.dbo.Subscriber INNER JOIN
                                {0}.dbo.ExaggeratedRange ON Subscriber.subscriberType= ExaggeratedRange.ID where Subscriber.id={1}";
            sql = string.Format(sql, base.DBName, subscriberID);
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                object val = helper.ExecuteScalar(sql);
                if (val is double)
                    return (double)val;
                if (val is int)
                    return (double)(int)val;
                return 0;
            }
            finally
            {
                base.ReleaseHelper(helper);
            }

        }

        public Kebele GetKebele(int kebele)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                Kebele[] ret = dspReader.GetSTRArrayByFilter<Kebele>("id=" + kebele);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public TranslatorEntry[] getTranslationTable()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<TranslatorEntry>(null);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        class TranslateEntryFilter : IObjectFilter<TranslatorEntry>
        {
            string key;
            public TranslateEntryFilter(string str)
            {
                key = str.ToUpper();
            }
            public bool Include(TranslatorEntry obj, object[] additional)
            {
                return obj.str.ToUpper().Equals(key);
            }
        }

        // BDE exposed
        public void saveTranslation(TranslatorEntry[] newEntries)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    foreach (TranslatorEntry e in newEntries)
                    {
                        int N;
                        TranslatorEntry[] test = dspWriter.GetSTRArrayByFilter<TranslatorEntry>(null, 0, -1, out N, new TranslateEntryFilter(e.str));
                        if (test.Length > 0)
                            dspWriter.UpdateSingleTableRecord(this.DBName, e);
                        else
                            dspWriter.InsertSingleTableRecord(this.DBName, e);
                    }
                    _updateNumber.billingConfiguration++;
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }

            }
        }

        public void changeCustomerStatus(int AID, int customerID, DateTime changeDate, CustomerStatus newSatatus)
        {
            Subscriber customer = GetSubscriber(customerID);
            if (customer == null)
                throw new ServerUserMessage("Invalid customer id-" + customerID);
            if (customer.status == newSatatus)
                throw new ServerUserMessage("Invalid request");
            if (customer.statusDate >= changeDate)
                throw new ServerUserMessage("Invalid status change date");
            switch (newSatatus)
            {
                case CustomerStatus.Inactive:
                    if (customer.status != CustomerStatus.Active)
                        throw new ServerUserMessage("Invalid request. Customer not active");
                    Subscription[] subsc = GetSubscriptions(customerID, changeDate.Ticks);
                    foreach (Subscription s in subsc)
                    {
                        if (s.subscriptionStatus == SubscriptionStatus.Active)
                        {
                            throw new ServerUserMessage("Customer can't be deactivated because it has active connections.");
                        }
                    }
                    break;
                default:
                    throw new ServerUserMessage("Invalid request");
            }
            CustomerStatusHistoryItem item = new CustomerStatusHistoryItem();
            item.customerID = customerID;
            item.oldStatus = customer.status;
            item.newStatus = newSatatus;
            item.changeDate = changeDate;

            customer.statusDate = changeDate;
            customer.status = newSatatus;

            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    dspWriter.InsertSingleTableRecord(this.DBName, item, new string[] { "__AID" }, new object[] { AID });
                    UpdateCustomerInternal(AID, customer);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        // BDE exposed
        public CustomerSubtype[] getCustomerSubtypes(SubscriberType type)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<CustomerSubtype>("type=" + (int)type);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }

        }
        // BDE exposed
        public CustomerSubtype getCustomerSubtype(SubscriberType type, int subTypeID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                CustomerSubtype[] ret = dspReader.GetSTRArrayByFilter<CustomerSubtype>("type=" + (int)type + " and id=" + subTypeID);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public ReadingReset getReadingReset(int connectionID, int periodID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                ReadingReset[] ret = dspReader.GetSTRArrayByFilter<ReadingReset>("connectionID=" + connectionID + " and periodID=" + periodID);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        public void setReadingReset(int AID, ReadingEntryClerk clerk, ReadingReset r)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    assertNoBill(r.connectionID, r.ticks);
                    BWFMeterReading current = BWFGetMeterReadingByPeriod(r.connectionID, r.periodID);
                    if (current != null && current.bwfStatus == BWFStatus.Read)
                    {
                        BWFUpdateMeterReading(AID, clerk, current);
                    }
                    if (getReadingReset(r.connectionID, r.periodID) == null)
                    {
                        dspWriter.InsertSingleTableRecord(this.DBName, r, new string[] { "__AID" }, new object[] { AID });
                    }
                    else
                    {
                        dspWriter.logDeletedData(AID, this.DBName + ".dbo.MeterReading", "connectionID=" + r.connectionID + " and periodID=" + r.periodID);
                        dspWriter.UpdateSingleTableRecord(this.DBName, r, new string[] { "__AID" }, new object[] { AID });
                    }
                    _updateNumber.billingConfiguration++;
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }

        public void deleteReadingReset(int AID, int connectionID, int periodID)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    if (getReadingReset(connectionID, periodID) == null)
                        throw new ServerUserMessage("Meter reading doesn't exist");
                    assertNoBill(connectionID, periodID);
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.MeterReading", "connectionID=" + connectionID + " and periodID=" + periodID);
                    dspWriter.Delete(this.DBName, "MeterReading", "connectionID=" + connectionID + " and periodID=" + periodID);
                    _updateNumber.billingConfiguration++;
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }


    }
}