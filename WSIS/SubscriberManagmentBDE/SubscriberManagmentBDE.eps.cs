using INTAPS;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.Evaluator;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;
using INTAPS.RDBMS;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using BIZNET.iERP;
namespace INTAPS.SubscriberManagment.BDE
{

    public partial class SubscriberManagmentBDE : BDEBase
    {
        Dictionary<int, IEPSInterface> _epsInterfaces;
        void loadTypeIDS()
        {
            string alist = System.Configuration.ConfigurationManager.AppSettings["typeIDAssemblies"];
            if (string.IsNullOrEmpty(alist))
                return;
            string[] a = alist.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string aa in a)
                ObjectTable.loadTypeIDs(System.Reflection.Assembly.Load(aa));

        }
        public void initializeEPS()
        {
            _epsInterfaces = new Dictionary<int, IEPSInterface>();
            string assemblies = System.Configuration.ConfigurationManager.AppSettings["EPSPlugins"];
            if (assemblies == null)
                return;
            this.loadTypeIDS();
            foreach (string an in assemblies.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                string[] pars = an.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                if (pars.Length != 2)
                {
                    ApplicationServer.EventLoger.Log(EventLogType.Errors, "Invalid EPSPlugins config syntax: " + an);
                    continue;
                }
                try
                {
                    PaymentCenter pc=this.GetPaymentCenter(pars[0]);
                    if(pc==null)
                    {
                        ApplicationServer.EventLoger.Log(EventLogType.Errors, "Invalid EPSPlugins.Invalid payment center user name: " + an);
                        continue;
                    }
                    Assembly a = System.Reflection.Assembly.Load(pars[1]);
                    int count = 0;
                    foreach (Type t in a.GetTypes())
                    {
                        try
                        {
                            if (t.GetInterface("IEPSInterface") != null)
                            {
                                _epsInterfaces.Add(pc.id, Activator.CreateInstance(t,pc) as IEPSInterface);
                                count++;
                            }

                        }
                        catch (Exception tex)
                        {
                            ApplicationServer.EventLoger.LogException("Error trying to instantiate EPS interface " + t, tex);
                        }
                    }
                    if (count > 0)
                    {
                        ApplicationServer.EventLoger.Log(string.Format("{0} job EPS interface (s) loaded from {1}", count, an));
                    }
                }
                catch (Exception ex)
                {
                    ApplicationServer.EventLoger.LogException("Error trying to load EPS interface assembly " + an, ex);
                }
            }
        }

        public IEPSInterface getEPSInterface(int paymentCenterID)
        {
            if (_epsInterfaces.ContainsKey(paymentCenterID))
                return _epsInterfaces[paymentCenterID];
            return null;
        }
        public int getSyncCount(int pcID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                object max = reader.ExecuteScalar("Select max(messageID) from {0}.dbo.EPSStatus where paymentCenterID={1}".format(this.DBName, pcID));
                int last=ApplicationServer.MsgRepo.getLastMessageNumber();
                return last - (max is int ? (int)max : 0);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }   
        }
        public EPSBill epsGetBill(int billID)
        {
            PeriodicBill bill = Accounting.GetAccountDocument(billID,true) as PeriodicBill;
            if (bill == null)
                return null;
            int pcid;
            string status=epsGetPaymentStatus(bill,out pcid);
            return new EPSBill(bill,status,pcid);
        }
        void epsSyncOneMessage(int AID,IEPSInterface eps,int paymentCenterID, int messageID)
        {
            MessageList list = ApplicationServer.MsgRepo.getMessageList(messageID, messageID);
            bool ignore = true;
            if (list.data.Length != 0)
            {

                OfflineMessage msg = list.data[0];
                if (msg.messageData is GenericDiff)
                {
                    GenericDiff bill = (GenericDiff)msg.messageData;
                    switch (bill.changeType)
                    {
                        case GenericDiffType.Add:
                            if (bill.diffData is PeriodicBill)
                            {
                                ignore = false;
                                int pcid;
                                string status = epsGetPaymentStatus((PeriodicBill)bill.diffData, out pcid);
                                eps.addBill(new EPSBill((PeriodicBill)bill.diffData,status,pcid));
                            }
                            break;
                        case GenericDiffType.Delete:
                            if(bill==null)
                            {
                                ApplicationServer.EventLoger.Log("Bill is null sync skipped. msgID:{0}".format(msg.id));
                                break;
                            }
                            Type billType = INTAPS.RDBMS.ObjectTable.getTypeByID(bill.typeID);
                            if(billType==null)
                            {
                                throw new ServerUserMessage("Bill type ID {0} not found", bill.typeID);
                            }
                            if (INTAPS.RDBMS.ObjectTable.getTypeByID(bill.typeID).IsSubclassOf(typeof(PeriodicBill)))
                            {
                                ignore = false;
                                eps.deleteBill(int.Parse(bill.objectID));
                            }
                            break;
                        case GenericDiffType.Replace:
                            /*if (bill.diffData is PeriodicBill)
                            {
                                ignore = false;
                                eps.deleteBill(int.Parse(bill.objectID));
                                int pcid;
                                string status = epsGetPaymentStatus((PeriodicBill)bill.diffData, out pcid);
                                eps.addBill(new EPSBill((PeriodicBill)bill.diffData,status,pcid));
                            }
                            else*/
                            if(bill.diffData is CustomerBillRecord)
                            {
                                CustomerBillRecord rec = bill.diffData as CustomerBillRecord;
                                PeriodicBill pb = this.Accounting.GetAccountDocument(rec.id, true) as PeriodicBill;
                                if (pb == null)
                                    ignore = true;
                                else
                                {
                                    int pcID;
                                    string status = epsGetPaymentStatus(pb, out pcID);
                                    eps.replaceBill(new EPSBill(pb, status, pcID));
                                    ignore = false;
                                }
                            }
                            break;
                        case GenericDiffType.NoChange:
                            break;
                        default:
                            break;
                    }
                }
            }
            dspWriter.Insert(this.DBName, "EPSStatus", new string[] { "paymentCenterID", "messageID", "na", "__AID" }
    , new object[] { paymentCenterID, messageID, ignore, AID });

        }

        private string epsGetPaymentStatus(PeriodicBill periodicBill,out int pcID)
        {
            pcID = -1;
            string status = "unpaid";
            CustomerBillRecord rec = getCustomerBillRecord(periodicBill.AccountDocumentID);
            if (rec != null)
            {
                if (rec.paymentDiffered)
                    status = "deferred";
                else if (rec.paymentDocumentID != -1)
                {
                    CustomerPaymentReceipt receipt = this.Accounting.GetAccountDocument<CustomerPaymentReceipt>(rec.paymentDocumentID);
                    if (receipt == null)
                        status = "paid";
                    else
                    {
                        PaymentCenter pc = GetPaymentCenterByCashAccount(receipt.assetAccountID);
                        if (pc == null)
                            status = "paid";
                        else
                        {
                            status = "paid at payment center " + pc.id;
                            pcID = pc.id;
                        }
                    }
                }
            }
            return status;
        }
        public void epsSyncOne(int AID,int pcID)
        {
            IEPSInterface eps= getEPSInterface(pcID);
            if (eps == null)
                throw new ServerUserMessage("No EPS assocatied with payment center ID:{0}", pcID);

            object _last = dspWriter.ExecuteScalar("Select max(messageID) from {0}.dbo.EPSStatus where paymentCenterID={1}".format(this.DBName, pcID));
            int from;
            if (_last is int)
                from = (int)(_last) + 1;
            else
                from = 1;
            epsSyncOneMessage(AID, eps, pcID, from);
        }   
        public int epsGetLastPaymentSeqNo(int pcid)
        {
            object _last = dspWriter.ExecuteScalar("Select max(seqNo) from {0}.dbo.EPRReceipt where paymentCenterID={1}".format(this.DBName, pcid));
            if (_last is int)
                return (int)_last;
            return -1;
        }
        public int epsGetPaymentSeqNo(int pcid, string paymentID)
        {
            object _last = dspWriter.ExecuteScalar("Select seqNo from {0}.dbo.EPRReceipt where paymentID='{1}'".format(this.DBName, paymentID));
            if (_last is int)
                return (int)_last;
            return -1;
        }
        public int epsLogPaymentID(int AID,int pcid,string paymentID)
        {
            int test = epsGetPaymentSeqNo(pcid, paymentID);
            if (test != -1)
                return test;
            int seqNo = epsGetLastPaymentSeqNo(pcid);
            if (seqNo == -1)
                seqNo = 1;
            else
                seqNo++;
            dspWriter.Insert(this.DBName, "EPRReceipt", new string[] { "paymentCenterID", "paymentID", "seqNo", "receiveDate", "__AID" }
                , new object[] { pcid, paymentID, seqNo, DateTime.Now, AID }
                );
            return seqNo;
        }
    }
}