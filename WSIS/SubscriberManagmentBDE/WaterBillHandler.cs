
    using INTAPS.Accounting;
    using INTAPS.ClientServer;
    using INTAPS.SubscriberManagment;
    using System;
using INTAPS.Accounting.BDE;
namespace INTAPS.SubscriberManagment.BDE
{

    public class WaterBillHandler : DeleteReverseDocumentHandler, IDocumentServerHandler
    {
        public WaterBillHandler(AccountingBDE ac) : base(ac)
        {
        }

        public bool CheckPostPermission(int docID, AccountDocument _doc, UserSessionData userSession)
        {
            return true;
        }

        public override string GetHTML(AccountDocument _doc)
        {
            WaterBillDocument document = (WaterBillDocument) _doc;
            return document.BuildHTML();
        }

        public int Post(int AID, AccountDocument _doc)
        {
            WaterBillDocument document = (WaterBillDocument) _doc;
            SubscriberManagmentBDE bDE = (SubscriberManagmentBDE) ApplicationServer.GetBDE("Subscriber");
            throw new Exception("Not implemented");
        }
    }
}

