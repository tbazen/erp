
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Accounting.BDE;
using INTAPS.RDBMS;
using INTAPS.SubscriberManagment;
using System;
using System.Threading;
using System.Web;
namespace INTAPS.SubscriberManagment.BDE
{

    internal class WaterMeterIssuanceHandler : IDocumentServerHandler
    {
        AccountingBDE bdeAccounting;
        protected SubscriberManagmentBDE bdeSubscriber;

        public WaterMeterIssuanceHandler(AccountingBDE bde)
        {
            this.bdeAccounting = bde;
            this.bdeSubscriber = (SubscriberManagmentBDE)ApplicationServer.GetBDE("Subscriber");
        }

        public bool CheckPostPermission(int docID, AccountDocument _doc, UserSessionData userSession)
        {
            return true;
        }

        public void DeleteDocument(int AID, int docID)
        {
            MeterIssuanceDocument accountDocument = (MeterIssuanceDocument)this.bdeAccounting.GetAccountDocument(docID, true);
            if (accountDocument == null)
            {
                throw new Exception("Water meter issuance not found ID:" + docID);
            }
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    this.bdeAccounting.DeleteAccountDocument(AID, docID,false);
                    bdeAccounting.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw new Exception("Error deleting meter issuance transaction", exception);
                }
            }
        }

        public string GetHTML(AccountDocument _doc)
        {
            MeterIssuanceDocument document = (MeterIssuanceDocument)_doc;
            return ("<b>Meter Issued to :</b>" + HttpUtility.HtmlEncode(document.subscription.contractNo));
        }

        public int Post(int AID, AccountDocument _doc)
        {
            MeterIssuanceDocument document = (MeterIssuanceDocument)_doc;
            int accountDocumentID = document.AccountDocumentID;
            lock (this.bdeAccounting.WriterHelper)
            {
                if (accountDocumentID != -1)
                {
                    this.DeleteDocument(AID, accountDocumentID);
                }
                //document.subscription = this.bdeSubscriber.GetSubscription(document.subscriptionID);
                
                return -1;
            }
        }

        public void ReverseDocument(int AID, int docID)
        {
            throw new Exception("Transaction reversal not support for water meter issuance please delete the document.");
        }
    }
}

