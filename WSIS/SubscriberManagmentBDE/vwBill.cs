
    using INTAPS.RDBMS;
    using System;
namespace INTAPS.SubscriberManagment.BDE
{

    [SingleTableObject]
    public class vwBill
    {
        [DataField]
        public int accountDocumentID;
        [DataField]
        public string contractNo;
        [DataField]
        public int Kebele;
        [DataField]
        public string name;
        [DataField]
        public int payDocumentID;
        [DataField]
        public int periodID;
        [DataField]
        public int subscriptionID;
        [DataField]
        public int WBill;
    }
}

