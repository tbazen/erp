namespace INTAPS.SubscriberManagment.Client
{
    using INTAPS.ClientServer.Client;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    internal partial class ChangePassword : Form
    {
        
        
        private Container components = null;
        
        private string m_User;
        

        public ChangePassword(string User)
        {
            this.InitializeComponent();
            this.m_User = User;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.txtPW1.Text != this.txtPW2.Text)
            {
                MessageBox.Show("Password confirmed incorrectly.", "Error!");
                this.txtPW1.Focus();
                this.txtPW1.Text = "";
                this.txtPW2.Text = "";
            }
            else
            {
                try
                {
                    SecurityClient.ChangePassword(this.m_User, this.txtPW1.Text);
                    base.Close();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Can't change password.");
                }
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void NewUser_Load(object sender, EventArgs e)
        {
        }
    }
}

