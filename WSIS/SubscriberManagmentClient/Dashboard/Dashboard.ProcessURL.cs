﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.UI.HTML;

namespace INTAPS.SubscriberManagment.Client
{
    partial class MainForm : IHTMLBuilder
    {
        public bool ProcessUrl(string path, System.Collections.Hashtable query)
        {
            try
            {
                
                //PostBills pb;
                switch (path)
                {
                    case "custMap":
                        SubscriberManagmentClient.showAllCustomersMap();
                        return true;
                    case "evalPrd1":
                        string repID = (string)query["repID"];
                        int prd1=int.Parse( (string)query["prd1"]);
                        PaymentCenter currentPaymentCenter = SubscriberManagmentClient.GetCurrentPaymentCenter();
                        int pcID = (currentPaymentCenter == null) ? -1 : currentPaymentCenter.id;
                        string htmlHeaders;
                        string html = Accounting.Client.AccountingClient.EvaluateEHTML(repID, new object[] { pcID, -1, prd1, prd1 }, out htmlHeaders);

                        new SimpleHTMLViewer("", html, Accounting.Client.AccountingClient.GetReportDefinationInfo(repID).styleSheet).Show(this);
                        return true;
                    case "editRS":
                        int prd = int.Parse((string)query["prd"]);
                        BWF.ReadingSheetBrowser rsb = new BWF.ReadingSheetBrowser(prd,false);
                        rsb.Show(this);
                        return true;
                    case "custList":
                        SubscriptionList list = new SubscriptionList();
                        list.Show(this);
                        return true;
                    case "genMain":
                        GenerateBillsForm gb = new GenerateBillsForm();
                        gb.Show(this);
                        return true;
                    case "delBill":
                        DeleteBillsForm dellBill = new DeleteBillsForm();
                        dellBill.Show(this);
                        return true;
                    case "postMain":
                        PostBillsForm pb = new PostBillsForm();
                        pb.Show(this);
                        return true;
                    case "postPenality":
                        throw new NotImplementedException("Not upgraded");
                        //pb = new PostBills(typeof(LatePaymentBill));
                        //pb.Show(this);
                        //return true;
                    case "postCredit":
                        throw new NotImplementedException("Not upgraded");
                        //pb = new PostBills(typeof(CreditBill));
                        //pb.Show(this);
                        //return true;

                }
                return false;
            }
            catch (Exception ex)
            {
                this.BeginInvoke(new ShowErrorMesssageDelegate(showErrorMessage), ex);
                return true;
            }

        }
        void showErrorMessage(Exception ex)
        {
            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error", ex);
        }
    }
}
