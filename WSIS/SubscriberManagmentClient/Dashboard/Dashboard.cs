﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.UI.HTML;
using INTAPS.Accounting;

namespace INTAPS.SubscriberManagment.Client
{
    partial class MainForm : IHTMLBuilder
    {
        SubscriberManagmentUpdateNumber _updateNumber=null;
        void startUpdatePolling()
        {
            new System.Threading.Thread(new System.Threading.ThreadStart(pollChanges)).Start();
        }
        void updateBrowser(string styleSheet)
        {
            browser.StyleSheetFile = styleSheet;
            browser.LoadControl(this);
        }
        void pollChanges()
        {
            do
            {

                SubscriberManagmentUpdateNumber n=null;
                do
                {
                    try
                    {
                        n = SubscriberManagmentClient.getUpdateNumber();
                        break;
                    }
                    catch
                    {
                        System.Threading.Thread.Sleep(10000);
                    }
                }
                while (true);
                if (_updateNumber == null || !n.Equals(_updateNumber))
                {
                    _updateNumber = n;
                    try
                    {
                        ReportDefination def;
                        def= INTAPS.Accounting.Client.AccountingClient.GetReportDefinationInfo("Billing.DashBoard");
                        string htmlHeaders;
                        _html = INTAPS.Accounting.Client.AccountingClient.EvaluateEHTML(def.id, new object[] { }, out htmlHeaders);
                        this.BeginInvoke(new INTAPS.UI.HTML.ProcessSingleParameter<string>(updateBrowser),def.styleSheet);
                    }
                    catch (Exception ex)
                    {
                        this.BeginInvoke(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                        {
                            _html = ex.Message+"\n"+ex.StackTrace;
                        }));
                        return;
                    }
                }
                System.Threading.Thread.Sleep(1000);
            }
            while (true);
        }
        public void SetHost(IHTMLDocumentHost host)
        {
            
        }

        public string WindowCaption
        {
            get 
            {
                return "Dashboard";
            }
        }
    }
}
