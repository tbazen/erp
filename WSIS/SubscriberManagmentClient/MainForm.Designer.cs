﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class MainForm : System.Windows.Forms.Form
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.dropDownShow = new System.Windows.Forms.ToolStripDropDownButton();
            this.subscribersListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.readingCycleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miShowMeterReaders = new System.Windows.Forms.ToolStripMenuItem();
            this.menReadingSheets = new System.Windows.Forms.ToolStripMenuItem();
            this.miShowClerks = new System.Windows.Forms.ToolStripMenuItem();
            this.readingAnalysisMethodToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.billingCyleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miBillConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.miShowRateConfiguration = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.paymentInstrumentTypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentCentersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.itemBank = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.miShowConfiguration = new System.Windows.Forms.ToolStripMenuItem();
            this.ddSalesPoint = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.viewOfflineDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.testReceiptPrint = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton3 = new System.Windows.Forms.ToolStripDropDownButton();
            this.registerProductionPointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productionPointReadingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ddExport = new System.Windows.Forms.ToolStripDropDownButton();
            this.workToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.excelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.browserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.allMetersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.miAllocation = new System.Windows.Forms.ToolStripMenuItem();
            this.readingProgressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownReport = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.fixReadingProblemsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analyzeReadingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendInstanantMessageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonChangePassword = new System.Windows.Forms.ToolStripMenuItem();
            this.miSMSTools = new System.Windows.Forms.ToolStripMenuItem();
            this.miSendBillSMS = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewProposedReading = new System.Windows.Forms.ToolStripMenuItem();
            this.miSendGeneralSMS = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.changeSMSPasscodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commissionPaymentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.browser = new INTAPS.UI.HTML.ControlBrowser();
            this.miSendUnreadSMS = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip
            // 
            this.toolStrip.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.toolStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dropDownShow,
            this.ddSalesPoint,
            this.toolStripDropDownButton3,
            this.ddExport,
            this.toolStripDropDownButton1,
            this.dropDownReport,
            this.toolStripDropDownButton2,
            this.toolStripSeparator3});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(871, 32);
            this.toolStrip.TabIndex = 9;
            this.toolStrip.Text = "toolStrip1";
            // 
            // dropDownShow
            // 
            this.dropDownShow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.dropDownShow.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subscribersListToolStripMenuItem,
            this.toolStripSeparator1,
            this.readingCycleToolStripMenuItem,
            this.miShowMeterReaders,
            this.menReadingSheets,
            this.miShowClerks,
            this.readingAnalysisMethodToolStripMenuItem,
            this.toolStripSeparator2,
            this.billingCyleToolStripMenuItem,
            this.miBillConfig,
            this.toolStripSeparator4,
            this.paymentInstrumentTypesToolStripMenuItem,
            this.paymentCentersToolStripMenuItem,
            this.toolStripSeparator5,
            this.itemBank,
            this.toolStripSeparator6,
            this.miShowConfiguration});
            this.dropDownShow.Image = ((System.Drawing.Image)(resources.GetObject("dropDownShow.Image")));
            this.dropDownShow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.dropDownShow.Name = "dropDownShow";
            this.dropDownShow.Size = new System.Drawing.Size(74, 29);
            this.dropDownShow.Text = "&Show";
            // 
            // subscribersListToolStripMenuItem
            // 
            this.subscribersListToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("subscribersListToolStripMenuItem.Image")));
            this.subscribersListToolStripMenuItem.Name = "subscribersListToolStripMenuItem";
            this.subscribersListToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.subscribersListToolStripMenuItem.Size = new System.Drawing.Size(400, 30);
            this.subscribersListToolStripMenuItem.Text = "Customers List";
            this.subscribersListToolStripMenuItem.Click += new System.EventHandler(this.subscribersListToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(397, 6);
            // 
            // readingCycleToolStripMenuItem
            // 
            this.readingCycleToolStripMenuItem.Name = "readingCycleToolStripMenuItem";
            this.readingCycleToolStripMenuItem.Size = new System.Drawing.Size(400, 30);
            this.readingCycleToolStripMenuItem.Text = "Reading Cycle";
            this.readingCycleToolStripMenuItem.Click += new System.EventHandler(this.readingCycleToolStripMenuItem_Click);
            // 
            // miShowMeterReaders
            // 
            this.miShowMeterReaders.Image = ((System.Drawing.Image)(resources.GetObject("miShowMeterReaders.Image")));
            this.miShowMeterReaders.Name = "miShowMeterReaders";
            this.miShowMeterReaders.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.M)));
            this.miShowMeterReaders.Size = new System.Drawing.Size(400, 30);
            this.miShowMeterReaders.Text = "Meter Readers";
            this.miShowMeterReaders.Click += new System.EventHandler(this.miShowMeterReaders_Click);
            // 
            // menReadingSheets
            // 
            this.menReadingSheets.Name = "menReadingSheets";
            this.menReadingSheets.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.menReadingSheets.Size = new System.Drawing.Size(400, 30);
            this.menReadingSheets.Text = "Reading Sheets";
            this.menReadingSheets.Click += new System.EventHandler(this.menReadingSheets_Click);
            // 
            // miShowClerks
            // 
            this.miShowClerks.Name = "miShowClerks";
            this.miShowClerks.Size = new System.Drawing.Size(400, 30);
            this.miShowClerks.Text = "Reading Clerks";
            this.miShowClerks.Click += new System.EventHandler(this.miShowClerks_Click);
            // 
            // readingAnalysisMethodToolStripMenuItem
            // 
            this.readingAnalysisMethodToolStripMenuItem.Image = global::INTAPS.SubscriberManagment.Client.Properties.Resources.ShowTestReport_32x32;
            this.readingAnalysisMethodToolStripMenuItem.Name = "readingAnalysisMethodToolStripMenuItem";
            this.readingAnalysisMethodToolStripMenuItem.Size = new System.Drawing.Size(400, 30);
            this.readingAnalysisMethodToolStripMenuItem.Text = "Reading Analysis Setting";
            this.readingAnalysisMethodToolStripMenuItem.Click += new System.EventHandler(this.readingAnalysisMethodToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(397, 6);
            // 
            // billingCyleToolStripMenuItem
            // 
            this.billingCyleToolStripMenuItem.Name = "billingCyleToolStripMenuItem";
            this.billingCyleToolStripMenuItem.Size = new System.Drawing.Size(400, 30);
            this.billingCyleToolStripMenuItem.Text = "Billing Cycle";
            this.billingCyleToolStripMenuItem.Click += new System.EventHandler(this.billingCyleToolStripMenuItem_Click);
            // 
            // miBillConfig
            // 
            this.miBillConfig.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miShowRateConfiguration});
            this.miBillConfig.Name = "miBillConfig";
            this.miBillConfig.Size = new System.Drawing.Size(400, 30);
            this.miBillConfig.Text = "Billing Configuration";
            // 
            // miShowRateConfiguration
            // 
            this.miShowRateConfiguration.Image = ((System.Drawing.Image)(resources.GetObject("miShowRateConfiguration.Image")));
            this.miShowRateConfiguration.Name = "miShowRateConfiguration";
            this.miShowRateConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.R)));
            this.miShowRateConfiguration.Size = new System.Drawing.Size(341, 30);
            this.miShowRateConfiguration.Text = "Rate Configuration";
            this.miShowRateConfiguration.Click += new System.EventHandler(this.miShowRateConfiguration_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(397, 6);
            // 
            // paymentInstrumentTypesToolStripMenuItem
            // 
            this.paymentInstrumentTypesToolStripMenuItem.Name = "paymentInstrumentTypesToolStripMenuItem";
            this.paymentInstrumentTypesToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.P)));
            this.paymentInstrumentTypesToolStripMenuItem.Size = new System.Drawing.Size(400, 30);
            this.paymentInstrumentTypesToolStripMenuItem.Text = "Payment Instrument Types";
            this.paymentInstrumentTypesToolStripMenuItem.Click += new System.EventHandler(this.paymentInstrumentTypesToolStripMenuItem_Click);
            // 
            // paymentCentersToolStripMenuItem
            // 
            this.paymentCentersToolStripMenuItem.Name = "paymentCentersToolStripMenuItem";
            this.paymentCentersToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.paymentCentersToolStripMenuItem.Size = new System.Drawing.Size(400, 30);
            this.paymentCentersToolStripMenuItem.Text = "Payment Centers";
            this.paymentCentersToolStripMenuItem.Click += new System.EventHandler(this.paymentCentersToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(397, 6);
            // 
            // itemBank
            // 
            this.itemBank.Name = "itemBank";
            this.itemBank.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
            this.itemBank.Size = new System.Drawing.Size(400, 30);
            this.itemBank.Text = "Banks";
            this.itemBank.Click += new System.EventHandler(this.itemBank_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(397, 6);
            // 
            // miShowConfiguration
            // 
            this.miShowConfiguration.Image = ((System.Drawing.Image)(resources.GetObject("miShowConfiguration.Image")));
            this.miShowConfiguration.Name = "miShowConfiguration";
            this.miShowConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.G)));
            this.miShowConfiguration.Size = new System.Drawing.Size(400, 30);
            this.miShowConfiguration.Text = "General Configuration";
            this.miShowConfiguration.Click += new System.EventHandler(this.miShowConfiguration_Click);
            // 
            // ddSalesPoint
            // 
            this.ddSalesPoint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddSalesPoint.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.viewOfflineDataToolStripMenuItem,
            this.toolStripSeparator7,
            this.testReceiptPrint});
            this.ddSalesPoint.Image = ((System.Drawing.Image)(resources.GetObject("ddSalesPoint.Image")));
            this.ddSalesPoint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddSalesPoint.Name = "ddSalesPoint";
            this.ddSalesPoint.Size = new System.Drawing.Size(211, 29);
            this.ddSalesPoint.Text = "&Offline Collection Point";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.toolStripMenuItem1.Size = new System.Drawing.Size(388, 30);
            this.toolStripMenuItem1.Text = "Download Update";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Image = global::INTAPS.SubscriberManagment.Client.Properties.Resources.Up_32x32;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.U)));
            this.toolStripMenuItem2.Size = new System.Drawing.Size(388, 30);
            this.toolStripMenuItem2.Text = "Upload Payment Center Data";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // viewOfflineDataToolStripMenuItem
            // 
            this.viewOfflineDataToolStripMenuItem.Name = "viewOfflineDataToolStripMenuItem";
            this.viewOfflineDataToolStripMenuItem.Size = new System.Drawing.Size(388, 30);
            this.viewOfflineDataToolStripMenuItem.Text = "View Offline Data";
            this.viewOfflineDataToolStripMenuItem.Click += new System.EventHandler(this.viewOfflineDataToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(385, 6);
            // 
            // testReceiptPrint
            // 
            this.testReceiptPrint.Name = "testReceiptPrint";
            this.testReceiptPrint.Size = new System.Drawing.Size(388, 30);
            this.testReceiptPrint.Text = "Test Printout";
            this.testReceiptPrint.Click += new System.EventHandler(this.testReceiptPrint_Click);
            // 
            // toolStripDropDownButton3
            // 
            this.toolStripDropDownButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registerProductionPointToolStripMenuItem,
            this.productionPointReadingsToolStripMenuItem});
            this.toolStripDropDownButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton3.Image")));
            this.toolStripDropDownButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton3.Name = "toolStripDropDownButton3";
            this.toolStripDropDownButton3.Size = new System.Drawing.Size(162, 29);
            this.toolStripDropDownButton3.Text = "Production Point";
            // 
            // registerProductionPointToolStripMenuItem
            // 
            this.registerProductionPointToolStripMenuItem.Name = "registerProductionPointToolStripMenuItem";
            this.registerProductionPointToolStripMenuItem.Size = new System.Drawing.Size(305, 30);
            this.registerProductionPointToolStripMenuItem.Text = "Manage Production Point";
            this.registerProductionPointToolStripMenuItem.Click += new System.EventHandler(this.registerProductionPointToolStripMenuItem_Click);
            // 
            // productionPointReadingsToolStripMenuItem
            // 
            this.productionPointReadingsToolStripMenuItem.Name = "productionPointReadingsToolStripMenuItem";
            this.productionPointReadingsToolStripMenuItem.Size = new System.Drawing.Size(305, 30);
            this.productionPointReadingsToolStripMenuItem.Text = "Production Point Readings";
            // 
            // ddExport
            // 
            this.ddExport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddExport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.workToolStripMenuItem,
            this.excelToolStripMenuItem,
            this.browserToolStripMenuItem,
            this.toolStripSeparator8,
            this.refreshToolStripMenuItem});
            this.ddExport.Image = ((System.Drawing.Image)(resources.GetObject("ddExport.Image")));
            this.ddExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddExport.Name = "ddExport";
            this.ddExport.Size = new System.Drawing.Size(118, 29);
            this.ddExport.Text = "&Dashboard";
            // 
            // workToolStripMenuItem
            // 
            this.workToolStripMenuItem.Name = "workToolStripMenuItem";
            this.workToolStripMenuItem.Size = new System.Drawing.Size(220, 30);
            this.workToolStripMenuItem.Text = "Export to Word";
            this.workToolStripMenuItem.Click += new System.EventHandler(this.workToolStripMenuItem_Click);
            // 
            // excelToolStripMenuItem
            // 
            this.excelToolStripMenuItem.Name = "excelToolStripMenuItem";
            this.excelToolStripMenuItem.Size = new System.Drawing.Size(220, 30);
            this.excelToolStripMenuItem.Text = "Export to Excel";
            this.excelToolStripMenuItem.Click += new System.EventHandler(this.excelToolStripMenuItem_Click);
            // 
            // browserToolStripMenuItem
            // 
            this.browserToolStripMenuItem.Name = "browserToolStripMenuItem";
            this.browserToolStripMenuItem.Size = new System.Drawing.Size(220, 30);
            this.browserToolStripMenuItem.Text = "View in Browser";
            this.browserToolStripMenuItem.Click += new System.EventHandler(this.browserToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(217, 6);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(220, 30);
            this.refreshToolStripMenuItem.Text = "Refresh";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.allMetersToolStripMenuItem,
            this.toolStripMenuItem3,
            this.miAllocation,
            this.readingProgressToolStripMenuItem});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(66, 29);
            this.toolStripDropDownButton1.Text = "&Map";
            this.toolStripDropDownButton1.DropDownOpening += new System.EventHandler(this.toolStripDropDownButton1_DropDownOpening);
            // 
            // allMetersToolStripMenuItem
            // 
            this.allMetersToolStripMenuItem.Name = "allMetersToolStripMenuItem";
            this.allMetersToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this.allMetersToolStripMenuItem.Size = new System.Drawing.Size(295, 30);
            this.allMetersToolStripMenuItem.Text = "All Meters";
            this.allMetersToolStripMenuItem.Click += new System.EventHandler(this.metersMapToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(295, 30);
            this.toolStripMenuItem3.Text = "Unallocated Meters";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // miAllocation
            // 
            this.miAllocation.Name = "miAllocation";
            this.miAllocation.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.miAllocation.Size = new System.Drawing.Size(295, 30);
            this.miAllocation.Text = "Reading Route";
            // 
            // readingProgressToolStripMenuItem
            // 
            this.readingProgressToolStripMenuItem.Name = "readingProgressToolStripMenuItem";
            this.readingProgressToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.readingProgressToolStripMenuItem.Size = new System.Drawing.Size(295, 30);
            this.readingProgressToolStripMenuItem.Text = "Reading Progress";
            this.readingProgressToolStripMenuItem.Click += new System.EventHandler(this.currentReadingMapToolStripMenuItem_Click);
            // 
            // dropDownReport
            // 
            this.dropDownReport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.dropDownReport.Image = ((System.Drawing.Image)(resources.GetObject("dropDownReport.Image")));
            this.dropDownReport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.dropDownReport.Name = "dropDownReport";
            this.dropDownReport.Size = new System.Drawing.Size(83, 29);
            this.dropDownReport.Text = "&Report";
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fixReadingProblemsToolStripMenuItem,
            this.analyzeReadingToolStripMenuItem,
            this.sendInstanantMessageToolStripMenuItem,
            this.buttonChangePassword,
            this.miSMSTools,
            this.commissionPaymentsToolStripMenuItem});
            this.toolStripDropDownButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton2.Image")));
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(71, 29);
            this.toolStripDropDownButton2.Text = "&Tools";
            this.toolStripDropDownButton2.Click += new System.EventHandler(this.toolStripDropDownButton2_Click);
            // 
            // fixReadingProblemsToolStripMenuItem
            // 
            this.fixReadingProblemsToolStripMenuItem.Name = "fixReadingProblemsToolStripMenuItem";
            this.fixReadingProblemsToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F12;
            this.fixReadingProblemsToolStripMenuItem.Size = new System.Drawing.Size(390, 30);
            this.fixReadingProblemsToolStripMenuItem.Text = "Estimate Readings";
            this.fixReadingProblemsToolStripMenuItem.Click += new System.EventHandler(this.fixReadingProblemsToolStripMenuItem_Click);
            // 
            // analyzeReadingToolStripMenuItem
            // 
            this.analyzeReadingToolStripMenuItem.Image = global::INTAPS.SubscriberManagment.Client.Properties.Resources.EditTask_32x32;
            this.analyzeReadingToolStripMenuItem.Name = "analyzeReadingToolStripMenuItem";
            this.analyzeReadingToolStripMenuItem.Size = new System.Drawing.Size(390, 30);
            this.analyzeReadingToolStripMenuItem.Text = "Analyze Reading";
            this.analyzeReadingToolStripMenuItem.Click += new System.EventHandler(this.analyzeReadingToolStripMenuItem_Click);
            // 
            // sendInstanantMessageToolStripMenuItem
            // 
            this.sendInstanantMessageToolStripMenuItem.Name = "sendInstanantMessageToolStripMenuItem";
            this.sendInstanantMessageToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.M)));
            this.sendInstanantMessageToolStripMenuItem.Size = new System.Drawing.Size(390, 30);
            this.sendInstanantMessageToolStripMenuItem.Text = "Send Instanant Message";
            this.sendInstanantMessageToolStripMenuItem.Visible = false;
            this.sendInstanantMessageToolStripMenuItem.Click += new System.EventHandler(this.sendInstanantMessageToolStripMenuItem_Click);
            // 
            // buttonChangePassword
            // 
            this.buttonChangePassword.Name = "buttonChangePassword";
            this.buttonChangePassword.Size = new System.Drawing.Size(390, 30);
            this.buttonChangePassword.Text = "Change Password";
            this.buttonChangePassword.Click += new System.EventHandler(this.buttonChangePassword_Click);
            // 
            // miSMSTools
            // 
            this.miSMSTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSendBillSMS,
            this.miSendUnreadSMS,
            this.miViewProposedReading,
            this.miSendGeneralSMS,
            this.toolStripSeparator9,
            this.changeSMSPasscodeToolStripMenuItem});
            this.miSMSTools.Name = "miSMSTools";
            this.miSMSTools.Size = new System.Drawing.Size(390, 30);
            this.miSMSTools.Text = "SMS";
            this.miSMSTools.Visible = false;
            // 
            // miSendBillSMS
            // 
            this.miSendBillSMS.Name = "miSendBillSMS";
            this.miSendBillSMS.Size = new System.Drawing.Size(354, 30);
            this.miSendBillSMS.Text = "Send Bill SMS";
            this.miSendBillSMS.Click += new System.EventHandler(this.miSendBillSMS_Click);
            // 
            // miViewProposedReading
            // 
            this.miViewProposedReading.Name = "miViewProposedReading";
            this.miViewProposedReading.Size = new System.Drawing.Size(354, 30);
            this.miViewProposedReading.Text = "View Proposed Readings";
            this.miViewProposedReading.Click += new System.EventHandler(this.viewProposedReadingsToolStripMenuItem_Click);
            // 
            // miSendGeneralSMS
            // 
            this.miSendGeneralSMS.Name = "miSendGeneralSMS";
            this.miSendGeneralSMS.Size = new System.Drawing.Size(354, 30);
            this.miSendGeneralSMS.Text = "Send General SMS";
            this.miSendGeneralSMS.Click += new System.EventHandler(this.sendGeneralSMSToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(351, 6);
            // 
            // changeSMSPasscodeToolStripMenuItem
            // 
            this.changeSMSPasscodeToolStripMenuItem.Name = "changeSMSPasscodeToolStripMenuItem";
            this.changeSMSPasscodeToolStripMenuItem.Size = new System.Drawing.Size(354, 30);
            this.changeSMSPasscodeToolStripMenuItem.Text = "Change SMS Passcode";
            this.changeSMSPasscodeToolStripMenuItem.Click += new System.EventHandler(this.changeSMSPasscodeToolStripMenuItem_Click);
            // 
            // commissionPaymentsToolStripMenuItem
            // 
            this.commissionPaymentsToolStripMenuItem.Name = "commissionPaymentsToolStripMenuItem";
            this.commissionPaymentsToolStripMenuItem.Size = new System.Drawing.Size(390, 30);
            this.commissionPaymentsToolStripMenuItem.Text = "Commission Payments";
            this.commissionPaymentsToolStripMenuItem.Click += new System.EventHandler(this.commissionPaymentsToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 32);
            // 
            // browser
            // 
            this.browser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browser.Location = new System.Drawing.Point(0, 32);
            this.browser.MinimumSize = new System.Drawing.Size(20, 20);
            this.browser.Name = "browser";
            this.browser.Size = new System.Drawing.Size(871, 319);
            this.browser.StyleSheetFile = null;
            this.browser.TabIndex = 10;
            // 
            // miSendUnreadSMS
            // 
            this.miSendUnreadSMS.Name = "miSendUnreadSMS";
            this.miSendUnreadSMS.Size = new System.Drawing.Size(354, 30);
            this.miSendUnreadSMS.Text = "Send Unread Meters Notification";
            this.miSendUnreadSMS.Click += new System.EventHandler(this.miSendUnreadSMS_Click);
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(871, 351);
            this.Controls.Add(this.browser);
            this.Controls.Add(this.toolStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Reading and Billing Manager";
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripDropDownButton dropDownShow;
        private System.Windows.Forms.ToolStripMenuItem subscribersListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menReadingSheets;
        private System.Windows.Forms.ToolStripMenuItem miShowMeterReaders;
        private System.Windows.Forms.ToolStripMenuItem miShowClerks;
        private System.Windows.Forms.ToolStripMenuItem miShowConfiguration;
        private System.Windows.Forms.ToolStripDropDownButton dropDownReport;
        private System.Windows.Forms.ToolStripMenuItem paymentCentersToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton ddSalesPoint;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private UI.HTML.ControlBrowser browser;
        private System.Windows.Forms.ToolStripDropDownButton ddExport;
        private System.Windows.Forms.ToolStripMenuItem workToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem excelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem browserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemBank;
        private System.Windows.Forms.ToolStripMenuItem paymentInstrumentTypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem allMetersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miAllocation;
        private System.Windows.Forms.ToolStripMenuItem readingProgressToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem viewOfflineDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readingCycleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miBillConfig;
        private System.Windows.Forms.ToolStripMenuItem miShowRateConfiguration;
        private System.Windows.Forms.ToolStripMenuItem billingCyleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testReceiptPrint;
        private System.Windows.Forms.ToolStripMenuItem readingAnalysisMethodToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripMenuItem fixReadingProblemsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analyzeReadingToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton3;
        private System.Windows.Forms.ToolStripMenuItem registerProductionPointToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productionPointReadingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem sendInstanantMessageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buttonChangePassword;
        private System.Windows.Forms.ToolStripMenuItem miSMSTools;
        private System.Windows.Forms.ToolStripMenuItem miSendBillSMS;
        private System.Windows.Forms.ToolStripMenuItem miViewProposedReading;
        private System.Windows.Forms.ToolStripMenuItem miSendGeneralSMS;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem changeSMSPasscodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commissionPaymentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miSendUnreadSMS;
    }
}