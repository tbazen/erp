
using INTAPS.Accounting.Client;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client.BWF;
using INTAPS.UI;
using INTAPS.UI.Windows;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using INTAPS.Evaluator;
using System.Collections.Generic;
using INTAPS.ClientServer;
using INTAPS.ClientServer.Client;
using INTAPS.SubscriberManagment.Client.MobilePayment;
namespace INTAPS.SubscriberManagment.Client
{
    public partial class MainForm : Form
    {
        public static MainForm TheInstance;

        public MainForm()
        {
            this.InitializeComponent();
            this.toolStrip.SendToBack();
            TheInstance = this;
            this.Text = this.Text + " - " + INTAPS.ClientServer.Client.ApplicationClient.UserName;
            AccountingClient.PopulateReportSelector(-1, new ToolStripDrowDownReportSelector(this.dropDownReport));
            //intAllocationMapMen();
            initBillingSettingConfigMenu();
            initSMS();
        }

        private void initSMS()
        {
            try
            {
                SMSLicenseInformation license = SubscriberManagmentClient.getSMSLicense();
                miSMSTools.Visible = license.hasLicense();
                miSendBillSMS.Visible = miSendGeneralSMS.Visible = license.allowBillSend;
                miViewProposedReading.Visible = license.allowReceive;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error getting sms initializion information\n"+ex.Message);
            }
        }

        private void initBillingSettingConfigMenu()
        {
            foreach (BillingRuleSettingType st in SubscriberManagmentClient.getSettingTypes())
            {
                ToolStripMenuItem mi = new ToolStripMenuItem(st.name);
                miBillConfig.DropDown.Items.Add(mi);
                mi.Click += new EventHandler(billingSettingMenuItemClicked);
                mi.Tag = st;
            }
        }

        void billingSettingMenuItemClicked(object sender, EventArgs e)
        {
            try
            {
                BillingRuleSettingType st = ((ToolStripMenuItem)sender).Tag as BillingRuleSettingType;
                SubscriberManagmentClient.billingRuleClient.showSettingEditor(st.type(), SubscriberManagmentClient.getBillingRuleSetting(st.type()));
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
        class EmployeDate
        {
            public DateTime date;
            public int employeeID;
        }
        void initAllocationMapMen()
        {
            miAllocation.DropDownItems.Clear();
            miAllocation.DropDownItems.Add("Loading readers data.. wait");
            new System.Threading.Thread(delegate()
                {
                    MeterReaderEmployee[] emps=null;
                    List<DateTime[]> datesList;
                    do
                    {
                        datesList = new List<DateTime[]>();
                        try
                        {
                            emps = SubscriberManagmentClient.GetAllMeterReaderEmployees(SubscriberManagmentClient.ReadingPeriod.id);

                            foreach (MeterReaderEmployee e in emps)
                            {
                                DateTime[] dates = null;

                                dates = SubscriberManagmentClient.getReadingDates(SubscriberManagmentClient.ReadingPeriod.id, e.employeeID);
                                datesList.Add(dates);
                            }
                            break;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"Error loading reader employees.\n{ex.Message}\n{ex.StackTrace}");
                            System.Threading.Thread.Sleep(10000);
                        }
                    }
                    while (true);

                    this.Invoke(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                        {
                            miAllocation.DropDownItems.Clear();
                            int i=0;
                            foreach (DateTime[] dates in datesList)
                            {
                                MeterReaderEmployee e = emps[i++];
                                ToolStripMenuItem mi = (ToolStripMenuItem)miAllocation.DropDownItems.Add(INTAPS.Payroll.Client.PayrollClient.GetEmployee(e.employeeID).employeeName);
                                mi.Tag = e.employeeID;
                                foreach (DateTime d in dates)
                                {
                                    ToolStripItem miDate = mi.DropDownItems.Add(d.ToString("MMM dd/yyyy"));
                                    miDate.Click += new EventHandler(mi_Click);
                                    miDate.Tag = new EmployeDate { date = d, employeeID = e.employeeID };
                                }
                            }

                        }
                    ));

                }).Start();
        }

        void mi_Click(object sender, EventArgs e)
        {
            EmployeDate emp=(EmployeDate)( ((ToolStripItem)sender).Tag);
            SubscriberManagmentClient.showReadingPath(SubscriberManagmentClient.ReadingPeriod.id, emp.employeeID,emp.date);
        }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            //SubscriberManagmentClient.saveTranslation();
            base.OnFormClosing(e);
        }
        private void btnGenCredit_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException("Not upgraded");
            //GenerateBills f = new GenerateBills(false, true);
            //this.ShowOwnedForm(f);
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException("Not upgraded");
            //GenerateBills f = new GenerateBills(false, false);
            //this.ShowOwnedForm(f);
        }

        private void btnGenPenality_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException("Not upgraded");
            //GenerateBills f = new GenerateBills(true, false);
            //this.ShowOwnedForm(f);
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException("Not upgraded");
            //PostReceipts f = new PostReceipts();
            //this.ShowOwnedForm(f);
        }


        private void btnPostFTP_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException("Not upgraded");
            //new UpdateChecker().ShowDialog(this);
        }

        private void buttonExport_Click(object sender, EventArgs e)
        {
            DownloadUpdate f = new DownloadUpdate();
            this.ShowOwnedForm(f);
        }

        private void buttonUploadToFTP_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException("Not upgraded");
            //new FTPUploader().ShowDialog(this);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

       


        private void itemPostBill_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException("Not upgraded");
            //PostBills f = new PostBills(typeof(WaterBill));
            //this.ShowOwnedForm(f);
        }

        private void itemPostCredit_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException("Not upgraded");
            //PostBills f = new PostBills(typeof(CreditBill));
            //this.ShowOwnedForm(f);
        }
        public void ShowOwnedForm(System.Windows.Forms.Form f)
        {
            f.Owner = this;
            if (f.StartPosition == FormStartPosition.Manual)
            {
                f.Location = base.PointToScreen(new Point((base.Right - f.Width) - 30, (base.Bottom - f.Height) - 30));
            }
            f.Show();
        }
        private void itemPostPenality_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException("Not upgraded");
            //PostBills f = new PostBills(typeof(LatePaymentBill));
            //this.ShowOwnedForm(f);
        }

        private void menReadingSheets_Click(object sender, EventArgs e)
        {
            this.ShowOwnedForm(new ReadingSheetBrowser(false));
        }

        private void miLegacy_Click(object sender, EventArgs e)
        {
        }

        private void miPostAll_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException("Not upgraded");
            //PostBills f = new PostBills(null);
            //this.ShowOwnedForm(f);
        }

        private void miShowClerks_Click(object sender, EventArgs e)
        {
            new EntryClerkList().ShowDialog(UIFormApplicationBase.MainForm);
        }

        private void miShowConfiguration_Click(object sender, EventArgs e)
        {
            new SubscriberManagmentConfigurationForm().Show(UIFormApplicationBase.MainForm);
        }

        private void miShowMeterReaders_Click(object sender, EventArgs e)
        {
            new MeterReadersList().ShowDialog(UIFormApplicationBase.MainForm);
        }

        private void miShowMeterReading_Click(object sender, EventArgs e)
        {
            throw new Exception("Not working");
        }

        private void miShowPaymentCenters_Click(object sender, EventArgs e)
        {
        }

        private void miShowRateConfiguration_Click(object sender, EventArgs e)
        {
            new RateSettingEditor().Show(UIFormApplicationBase.MainForm);
        }

        private void miShowReadingAnalysis_Click(object sender, EventArgs e)
        {
            this.ShowOwnedForm(new ReadingAnalysisForm());
        }

        private void miShowReadingBrowser_Click(object sender, EventArgs e)
        {
            throw new Exception("Not working");
        }

        private void miShowReadingSheet_Click(object sender, EventArgs e)
        {
            throw new Exception("Not working");
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Process.GetCurrentProcess().Kill();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            UIFormApplicationBase.CurrentAppliation.SetSetting("MainForm.Form", UIStateHandler.GetFormState(this));
            UIFormApplicationBase.CurrentAppliation.SaveSettings();
           // ApplicationClient.Disconnect();
            base.OnClosing(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            UIStateHandler.LoadFormState(UIFormApplicationBase.CurrentAppliation.GetSetting("MainForm.Form", null), this);
            base.OnLoad(e);
            startUpdatePolling();
        }

        private void resetSalePointUpdateChainOnlyAdminToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to reset update chains?\nAfter you have done this you will have to clear the sale point database on each offline sites"))
                {
                    SubscriberManagmentClient.ResetUpdateChain();
                }
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error reseting update chain", exception);
            }
        }



        private void subscribersListToolStripMenuItem_Click(object sender, EventArgs e)
        {

            this.ShowOwnedForm(new SubscriptionList());
        }

        private void paymentCentersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ShowOwnedForm(new PaymentCenterList());
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DownloadUpdate f = new DownloadUpdate();
            this.ShowOwnedForm(f);
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            ImportPayment f = new ImportPayment();
            this.ShowOwnedForm(f);
        }
        private void updateUpdateToFTPFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException("Not upgraded");
            //new FTPUploader().ShowDialog(this);
        }
        private void postCollectionFromFTPFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException("Not upgraded");
            //new UpdateChecker().ShowDialog(this);
        }
        private void workToolStripMenuItem_Click(object sender, EventArgs e)
        {
            INTAPS.UI.HTML.HTMLBuilder.ViewInApplication(UI.HTML.ApplicationType.Word, "Billing Dashboard", browser.DocumentText);
        }
        private void excelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            INTAPS.UI.HTML.HTMLBuilder.ViewInApplication(UI.HTML.ApplicationType.Excel, "Billing Dashboard", browser.DocumentText);
        }
        private void browserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            INTAPS.UI.HTML.HTMLBuilder.ViewInApplication(UI.HTML.ApplicationType.IExplorer, "Billing Dashboard", browser.DocumentText);
        }
        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            INTAPS.Accounting.ReportDefination def = INTAPS.Accounting.Client.AccountingClient.GetReportDefinationInfo("Billing.DashBoard");
            string htmlHeaders;
            _html = INTAPS.Accounting.Client.AccountingClient.EvaluateEHTML(def.id, new object[] { }, out htmlHeaders);
            updateBrowser(def.styleSheet);
        }
        private void itemBank_Click(object sender, EventArgs e)
        {
            BankList list = new BankList();
            list.Show(this);
        }
        private void paymentInstrumentTypesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PaymentInstrumentTypeList list = new PaymentInstrumentTypeList();
            list.Show(this);
        }
        private void metersMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SubscriberManagmentClient.showAllCustomersMap();
        }

        private void currentReadingMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SubscriberManagmentClient.showReadingMap(SubscriberManagmentClient.ReadingPeriod.id);
        }
        private void viewOfflineDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string html;
            OpenFileDialog of = new OpenFileDialog();
            of.Multiselect = false;
            of.Filter = "*.posmessage|*.posmessage";
            if (of.ShowDialog(this) != DialogResult.OK)
                return;

            System.IO.FileStream serializationStream = null;
            try
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                serializationStream = System.IO.File.OpenRead(of.FileName);
                PaymentCenterMessageList update = (PaymentCenterMessageList)formatter.Deserialize(serializationStream);
                serializationStream.Close();
                List<EData> receipts = new List<EData>();
                foreach (OfflineMessage msg in update.list.data)
                {
                    if (msg.messageData is PaymentReceivedMessage)
                    {
                        PaymentReceivedMessage payment = (PaymentReceivedMessage)msg.messageData;
                        ListData row = new ListData(9);
                        row[0] = new EData(DataType.Text, payment.payment.customer.customerCode);
                        row[1] = new EData(DataType.Text, payment.payment.DocumentDate.ToString());
                        row[2] = new EData(DataType.Text, payment.payment.receiptNumber == null ? "" : payment.payment.receiptNumber.reference);
                        row[3] = new EData(DataType.Text, payment.payment.receiptNumber == null ? "" : payment.paidAmount.ToString());
                        PaymentCenter pc = SubscriberManagmentClient.GetPaymentCenterByCashAccount(payment.paymentCenterID);
                        row[4] = new EData(DataType.Text, pc==null?"[Null]":pc.userName);
                        row[5] = new EData(DataType.Text, pc == null ? "[Null]" : pc.id.ToString());
                        row[6] = new EData(DataType.Text, msg.id.ToString());
                        string bills="";
                        foreach (int b in payment.payment.settledBills)
                        {
                            double pay= 0;
                            double fromDep = 0;
                            foreach(BillItem it in payment.payment.billItems)
                            {
                                if (it.customerBillID == b)
                                {
                                    pay += it.netPayment;
                                    fromDep += it.settledFromDepositAmount;
                                }
                            }
                            //bills = bills.Equals("") ? b + ", " + pay + ", " + fromDep : (bills + " ; " + (b + ", " + pay + ", " + fromDep));
                            bills = bills + "," + b;
                        }
                        row[7] = new EData(DataType.Text, bills);
                        row[8] = new EData(DataType.Text, payment.payment.assetAccountID.ToString());

                        receipts.Add(new EData(DataType.ListData, row));
                    }
                }
                EData data = new EData(DataType.ListData, new ListData(receipts.ToArray()));
                string htmlHeaders;
                html = AccountingClient.EvaluateEHTML("SalesPointData", new object[] { "data", data }, out htmlHeaders);
                
            }
            catch (Exception ex)
            {
                html = ex.Message;
                if (serializationStream != null)
                {
                    serializationStream.Close();
                }
            }
            SimpleHTMLViewer v = new SimpleHTMLViewer("Offline Data", html, "jobstyle.css");
            v.Show(this);
        }
        private void readingCycleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReadingCycles rs = new ReadingCycles();
            rs.Show(this);
        }
        private void billingCyleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BillingCycleList list = new BillingCycleList();
            this.ShowOwnedForm(list);
        }
        private void testReceiptPrint_Click(object sender, EventArgs e)
        {
            try
            {
                
                OpenFileDialog fd = new OpenFileDialog();
                fd.Multiselect = true;
                if (fd.ShowDialog(this) == DialogResult.OK)
                {
                    PrintLogData data;
                    System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(PrintLogData));
                    System.IO.StreamReader stream=System.IO.File.OpenText(fd.FileName);
                    try
                    {
                        data = s.Deserialize(stream) as PrintLogData;
                        data.paymentReceipt.billItems = new List<BillItem>();
                        foreach (int billID in data.paymentReceipt.settledBills)
                            data.paymentReceipt.billItems.AddRange((AccountingClient.GetAccountDocument(billID, true) as CustomerBillDocument).itemsLessExemption);
                        SubscriberManagmentClient.billingRuleClient.printReceipt(data.paymentReceipt, data.receiptParameters,data.rePrint, data.nCopies);
                    }
                    finally
                    {
                        if (stream != null)
                            stream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }

        }
        private void toolStripDropDownButton1_DropDownOpening(object sender, EventArgs e)
        {
            initAllocationMapMen();
        }
        private void readingAnalysisMethodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowOwnedForm(new ReadingAnalysisSetting());
        }
        private void fixReadingProblemsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BatchReading f = new BatchReading();
            this.ShowOwnedForm(f);
        }
        private void analyzeReadingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowOwnedForm(new AnalyzeReading());
        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripDropDownButton2_Click(object sender, EventArgs e)
        {

        }

        private void registerProductionPointToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProductionPointList p = new ProductionPointList();
            p.Show();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            SubscriberManagmentClient.showUnreadCustomersMap();
        }

        private void sendInstanantMessageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ApplicationClient.showMessageSender();
        }

        private void buttonChangePassword_Click(object sender, EventArgs e)
        {
            new ChangePassword(ApplicationClient.UserName).ShowDialog(this);
        }

        private void miSendBillSMS_Click(object sender, EventArgs e)
        {
            SendSMSProgresForm send = new SendSMSProgresForm(CustomerSMSSendType.Bills);
            send.Show(this);
        }
        private void miSendUnreadSMS_Click(object sender, EventArgs e)
        {
            SendSMSProgresForm send = new SendSMSProgresForm(CustomerSMSSendType.UnreadNotification);
            send.Show(this);
        }

        private void viewProposedReadingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProposedReadingList p = new ProposedReadingList();
            p.Show(this);
        }

        private void minSendDisconnectionNotifcation_Click(object sender, EventArgs e)
        {

        }

        private void sendGeneralSMSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SendSMSProgresForm send = new SendSMSProgresForm(CustomerSMSSendType.General);
            send.Show(this);
        }

        private void changeSMSPasscodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ChangeSMSPassCode().ShowDialog(this);
        }

        private void commissionPaymentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CommisionPaymentList paymentList = new CommisionPaymentList();
            ShowOwnedForm(paymentList);
        }        
    }
}

