
    using INTAPS.UI;
    using System;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            try
            {
                Login login = new Login();
                login.TryLogin();
                if (login.logedin)
                {
                    if (!INTAPS.ClientServer.Client.SecurityClient.IsPermited("root/Subscriber/login"))
                    {
                        System.Windows.Forms.MessageBox.Show("You are not authorized to login to billing application");
                        return;
                    }
                    MainForm main = new MainForm();
                    new UIFormApplicationBase(main);
                    //INTAPS.ClientServer.Client.ApplicationClient.startInstantMessageClient();
                    Application.Run(main);
                    INTAPS.ClientServer.Client.ApplicationClient.CloseSession();
                }
            }
            catch(Exception ex)
            {
                string msg = ex.Message + "\n" + ex.StackTrace;
                while(ex.InnerException!=null)
                {
                    ex = ex.InnerException;
                    msg +="\n"+ ex.Message + "\n" + ex.StackTrace;
                }
                MessageBox.Show(msg);
            }
        }
    }
}

