﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class BillingCycleEditor : System.Windows.Forms.Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BillingCycleEditor));
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonCloseReadingCycle = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.billPeriod = new INTAPS.SubscriberManagment.Client.BillPeriodSelector();
            this.label1 = new System.Windows.Forms.Label();
            this.date1 = new INTAPS.Ethiopic.DualCalendar();
            this.label2 = new System.Windows.Forms.Label();
            this.date2 = new INTAPS.Ethiopic.DualCalendar();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnOk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(379, 6);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(65, 28);
            this.btnOk.TabIndex = 29;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(450, 6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(65, 28);
            this.btnCancel.TabIndex = 28;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Controls.Add(this.buttonCloseReadingCycle);
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 194);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(527, 39);
            this.panel1.TabIndex = 32;
            // 
            // buttonCloseReadingCycle
            // 
            this.buttonCloseReadingCycle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCloseReadingCycle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonCloseReadingCycle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCloseReadingCycle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCloseReadingCycle.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCloseReadingCycle.ForeColor = System.Drawing.Color.White;
            this.buttonCloseReadingCycle.Location = new System.Drawing.Point(12, 6);
            this.buttonCloseReadingCycle.Name = "buttonCloseReadingCycle";
            this.buttonCloseReadingCycle.Size = new System.Drawing.Size(152, 28);
            this.buttonCloseReadingCycle.TabIndex = 29;
            this.buttonCloseReadingCycle.Text = "&Close Reading Cycle";
            this.buttonCloseReadingCycle.UseVisualStyleBackColor = false;
            this.buttonCloseReadingCycle.Click += new System.EventHandler(this.buttonCloseReadingCycle_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(4, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 17);
            this.label4.TabIndex = 34;
            this.label4.Text = "Period:";
            // 
            // billPeriod
            // 
            this.billPeriod.Location = new System.Drawing.Point(161, 12);
            this.billPeriod.Name = "billPeriod";
            this.billPeriod.NullSelection = null;
            this.billPeriod.SelectedPeriod = null;
            this.billPeriod.Size = new System.Drawing.Size(327, 24);
            this.billPeriod.TabIndex = 35;
            this.billPeriod.SelectionChanged += new System.EventHandler(this.billPeriod_SelectionChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(4, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 17);
            this.label1.TabIndex = 34;
            this.label1.Text = "Collection Start Date:";
            // 
            // date1
            // 
            this.date1.BackColor = System.Drawing.Color.White;
            this.date1.Location = new System.Drawing.Point(161, 49);
            this.date1.Name = "date1";
            this.date1.ShowEthiopian = true;
            this.date1.ShowGregorian = true;
            this.date1.ShowTime = false;
            this.date1.Size = new System.Drawing.Size(351, 40);
            this.date1.TabIndex = 36;
            this.date1.VerticalLayout = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(4, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 17);
            this.label2.TabIndex = 34;
            this.label2.Text = "Collection End Date:";
            // 
            // date2
            // 
            this.date2.BackColor = System.Drawing.Color.White;
            this.date2.Location = new System.Drawing.Point(161, 123);
            this.date2.Name = "date2";
            this.date2.ShowEthiopian = true;
            this.date2.ShowGregorian = true;
            this.date2.ShowTime = false;
            this.date2.Size = new System.Drawing.Size(351, 40);
            this.date2.TabIndex = 36;
            this.date2.VerticalLayout = true;
            // 
            // BillingCycleEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(527, 233);
            this.Controls.Add(this.date2);
            this.Controls.Add(this.date1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.billPeriod);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BillingCycleEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Billing Cycle Editor";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private BillPeriodSelector billPeriod;
        private System.Windows.Forms.Label label1;
        private Ethiopic.DualCalendar date1;
        private System.Windows.Forms.Label label2;
        private Ethiopic.DualCalendar date2;
        private System.Windows.Forms.Button buttonCloseReadingCycle;
 
    }
}