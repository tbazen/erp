
    using INTAPS.SubscriberManagment;
    using INTAPS.UI;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class BillingCycleEditor : Form
    {
        private bool m_changed = false;

        public BillingCycle _cycle;
        public BillingCycleEditor(BillingCycle cycle)
        {
            this.InitializeComponent();
            billPeriod.LoadData(SubscriberManagmentClient.CurrentYear);
            _cycle = cycle;
            if (_cycle == null)
            {
                billPeriod.SelectedPeriod = SubscriberManagmentClient.CurrentPeriod;
                date1.DateTime = billPeriod.SelectedPeriod.fromDate;
                date2.DateTime = billPeriod.SelectedPeriod.toDate.AddDays(-1);
                buttonCloseReadingCycle.Visible = false;
            }
            else
            {
                BillingCycle current = SubscriberManagmentClient.getCurrentBillingCycle();
                billPeriod.SelectedPeriod = SubscriberManagmentClient.GetBillPeriod(_cycle.periodID);
                date1.DateTime = _cycle.collectionStartTime;
                date2.DateTime = _cycle.collectionEndTime.AddDays(-1);
                
                if (_cycle.status == BillingCycleStatus.Open)
                {
                    buttonCloseReadingCycle.Visible = true;
                    buttonCloseReadingCycle.Text = "Close Reading Cycle";
                }
                else
                {
                    if (current == null)
                    {
                        buttonCloseReadingCycle.Visible = true;
                        buttonCloseReadingCycle.Text = "Open Reading Cycle";
                    }
                    else
                    {
                        buttonCloseReadingCycle.Visible = false;
                    }
                }
            }
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            BillingCycle data = new BillingCycle();
            data.periodID = billPeriod.SelectedPeriod.id;
            data.collectionStartTime = date1.DateTime.Date;
            data.collectionEndTime = date2.DateTime.Date.AddDays(1);
            try
            {
                if (_cycle == null)
                {
                    BillingCycle current = SubscriberManagmentClient.getCurrentBillingCycle();
                    data.status = current == null ? BillingCycleStatus.Open : BillingCycleStatus.Closed;
                    SubscriberManagmentClient.createBillingCycle(data);
                }
                else
                {
                    data.status = _cycle.status;
                    SubscriberManagmentClient.updateBillingCycle(data);
                }
                _cycle = data;
                base.Close();
                base.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
            
            
        }

        private void billPeriod_SelectionChanged(object sender, EventArgs e)
        {
            date1.DateTime = billPeriod.SelectedPeriod.fromDate;
            date2.DateTime = billPeriod.SelectedPeriod.toDate.AddDays(-1);
        }

        private void buttonCloseReadingCycle_Click(object sender, EventArgs e)
        {
            BillingCycle data = new BillingCycle();
            data.periodID = billPeriod.SelectedPeriod.id;
            data.collectionStartTime = date1.DateTime.Date;
            data.collectionEndTime = date2.DateTime.Date.AddDays(1);
            try
            {
                data.status = _cycle.status == BillingCycleStatus.Open ? BillingCycleStatus.Closed : BillingCycleStatus.Open;
                SubscriberManagmentClient.updateBillingCycle(data);
                _cycle = data;
                base.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
            base.DialogResult = DialogResult.OK;
        }

    }
}

