﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{
    public class BillingCycleList:INTAPS.UI.SimpleObjectList<BillingCycle>
    {
        protected override BillingCycle CreateObject()
        {
            BillingCycleEditor e = new BillingCycleEditor(null);
            if (e.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm) == DialogResult.OK)
            {
                return e._cycle;
            }
            return null;
        }

        protected override void DeleteObject(BillingCycle obj)
        {
            SubscriberManagmentClient.deleteBillingCycle(obj.periodID);
        }

        protected override BillingCycle[] GetObjects()
        {
            return SubscriberManagmentClient.getAllBillingCycles();
        }

        protected override string GetObjectString(BillingCycle obj)
        {
            return SubscriberManagmentClient.GetBillPeriod(obj.periodID).name + "  " + obj.status.ToString();
        }

        protected override string ObjectTypeName
        {
            get { return "Billing Cycle"; }
        }
        protected override bool AllowEdit
        {
            get
            {
                return true;
            }
        }
        protected override BillingCycle EditObject(BillingCycle obj)
        {
            BillingCycleEditor e = new BillingCycleEditor(obj);
            if (e.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm) == DialogResult.OK)
            {
                return e._cycle;
            }
            return null;
        }
    }
}
