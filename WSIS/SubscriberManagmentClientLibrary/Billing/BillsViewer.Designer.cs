﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class BillsViewer : System.Windows.Forms.Form
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BillsViewer));
            this.listView = new System.Windows.Forms.ListView();
            this.colPeriod = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colAmount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDescription = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.controlBrowser = new INTAPS.UI.HTML.ControlBrowser();
            this.bowserController = new INTAPS.UI.HTML.BowserController();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.exportTool = new INTAPS.UI.HTML.HTMLExportTool();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonGenBill = new System.Windows.Forms.Button();
            this.buttonRefresh = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView
            // 
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colPeriod,
            this.colType,
            this.colStatus,
            this.colAmount,
            this.colDescription});
            this.listView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView.FullRowSelect = true;
            this.listView.HideSelection = false;
            this.listView.Location = new System.Drawing.Point(0, 0);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(744, 164);
            this.listView.TabIndex = 0;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.Details;
            this.listView.SelectedIndexChanged += new System.EventHandler(this.listView_SelectedIndexChanged);
            this.listView.DoubleClick += new System.EventHandler(this.listView_DoubleClick);
            // 
            // colPeriod
            // 
            this.colPeriod.Text = "Period";
            this.colPeriod.Width = 143;
            // 
            // colType
            // 
            this.colType.Text = "Type";
            this.colType.Width = 152;
            // 
            // colStatus
            // 
            this.colStatus.Text = "Status";
            this.colStatus.Width = 120;
            // 
            // colAmount
            // 
            this.colAmount.Text = "Amount";
            this.colAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colAmount.Width = 173;
            // 
            // colDescription
            // 
            this.colDescription.Text = "Remark";
            this.colDescription.Width = 200;
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 41);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.listView);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.controlBrowser);
            this.splitContainer.Panel2.Controls.Add(this.bowserController);
            this.splitContainer.Size = new System.Drawing.Size(744, 359);
            this.splitContainer.SplitterDistance = 164;
            this.splitContainer.TabIndex = 1;
            // 
            // controlBrowser
            // 
            this.controlBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlBrowser.Location = new System.Drawing.Point(0, 25);
            this.controlBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.controlBrowser.Name = "controlBrowser";
            this.controlBrowser.Size = new System.Drawing.Size(744, 166);
            this.controlBrowser.StyleSheetFile = "jobstyle.css";
            this.controlBrowser.TabIndex = 0;
            // 
            // bowserController
            // 
            this.bowserController.Location = new System.Drawing.Point(0, 0);
            this.bowserController.Name = "bowserController";
            this.bowserController.ShowBackForward = false;
            this.bowserController.ShowRefresh = false;
            this.bowserController.Size = new System.Drawing.Size(744, 25);
            this.bowserController.TabIndex = 1;
            this.bowserController.Text = "bowserController1";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(744, 41);
            this.panel1.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkOrange;
            this.panel2.Controls.Add(this.exportTool);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.buttonGenBill);
            this.panel2.Controls.Add(this.buttonRefresh);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(744, 38);
            this.panel2.TabIndex = 8;
            // 
            // exportTool
            // 
            this.exportTool.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exportTool.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.exportTool.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exportTool.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.exportTool.ForeColor = System.Drawing.Color.White;
            this.exportTool.Location = new System.Drawing.Point(560, 4);
            this.exportTool.Name = "exportTool";
            this.exportTool.Size = new System.Drawing.Size(93, 28);
            this.exportTool.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(145, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(247, 28);
            this.button1.TabIndex = 7;
            this.button1.Text = "&Generate Old System Outstanding Bills";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonGenBill
            // 
            this.buttonGenBill.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonGenBill.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonGenBill.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGenBill.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGenBill.ForeColor = System.Drawing.Color.White;
            this.buttonGenBill.Location = new System.Drawing.Point(3, 4);
            this.buttonGenBill.Name = "buttonGenBill";
            this.buttonGenBill.Size = new System.Drawing.Size(117, 28);
            this.buttonGenBill.TabIndex = 7;
            this.buttonGenBill.Text = "&Generate Bill";
            this.buttonGenBill.UseVisualStyleBackColor = false;
            this.buttonGenBill.Click += new System.EventHandler(this.buttonGenBill_Click);
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRefresh.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRefresh.ForeColor = System.Drawing.Color.White;
            this.buttonRefresh.Location = new System.Drawing.Point(659, 4);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(75, 28);
            this.buttonRefresh.TabIndex = 7;
            this.buttonRefresh.Text = "&Refresh";
            this.buttonRefresh.UseVisualStyleBackColor = false;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // BillsViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.ClientSize = new System.Drawing.Size(744, 400);
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BillsViewer";
            this.Text = "Bills Viewer";
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.ListView listView;
        private System.Windows.Forms.ColumnHeader colPeriod;
        private System.Windows.Forms.ColumnHeader colStatus;
        private System.Windows.Forms.SplitContainer splitContainer;
        private INTAPS.UI.HTML.ControlBrowser controlBrowser;
        private INTAPS.UI.HTML.BowserController bowserController;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonGenBill;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ColumnHeader colAmount;
        private System.Windows.Forms.ColumnHeader colDescription;
        private System.Windows.Forms.ColumnHeader colType;
        private UI.HTML.HTMLExportTool exportTool;

    }
}