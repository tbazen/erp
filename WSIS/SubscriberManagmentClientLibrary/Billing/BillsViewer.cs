
using INTAPS.Accounting.Client;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
using INTAPS.UI.HTML;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using INTAPS.Accounting;
using INTAPS.ClientServer.Client;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class BillsViewer : Form
    {
        private Subscriber _customer;

        public BillsViewer()
        {
            this.components = null;
            this.InitializeComponent();
            this.bowserController.SetBrowser(this.controlBrowser);
            this.exportTool.setDelegate(new INTAPS.UI.HTML.GenerateHTMLDelegate(getBillsHtml));
        }
        string getBillsHtml(out string headers)
        {
            string html=SubscriberManagmentClient.htmlDocGetCustomerBillData(_customer.id, -1, out headers);
            return html;
            
        }
        public BillsViewer(int customerID)
            : this()
        {
            this._customer = SubscriberManagmentClient.GetSubscriber(customerID);
            this.Text = "Bills for " + _customer.customerCode;
            this.ReloadData();
        }

        private void buttonGenBill_Click(object sender, EventArgs e)
        {
            new GenerateBillsForm(this._customer.id).ShowDialog(UIFormApplicationBase.MainForm);
            this.ReloadData();
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            this.ReloadData();
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void listView_DoubleClick(object sender, EventArgs e)
        {
        }

        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listView.SelectedItems.Count != 0)
            {
                try
                {
                    if (this.listView.SelectedItems[0].Tag is CustomerBillRecord)
                    {
                        CustomerBillRecord billRecord = (CustomerBillRecord)this.listView.SelectedItems[0].Tag;
                        this.controlBrowser.LoadTextPage("Bill", AccountingClient.GetDocumentHTML(billRecord.id));
                    }
                }
                catch (Exception exception)
                {
                    this.controlBrowser.LoadTextPage("Bill", exception.Message);
                }
            }
        }

        private void LoadList(CustomerBillRecord[] billRecord)
        {
            this.listView.Items.Clear();
            foreach (CustomerBillRecord record in billRecord)
            {
                CustomerBillDocument billDoc = AccountingClient.GetAccountDocument(record.id, true) as CustomerBillDocument;

                ListViewItem item;
                
                string name;
                if (record.periodID == -1)
                    name = "";
                else
                    name = SubscriberManagmentClient.GetBillPeriod(record.periodID).name;
                string typeText = AccountingClient.GetDocumentTypeByID(record.billDocumentTypeID).name;
                    
                item = new ListViewItem(name);
                item.Tag = record;
                item.SubItems.Add(typeText);
                
                if (record.draft)
                {
                    item.SubItems.Add("Draft");
                }
                else if (record.paymentDocumentID == -1 && !record.paymentDiffered)
                {
                    item.SubItems.Add("Not paid");
                }
                else if (record.paymentDiffered)
                {
                    item.SubItems.Add("Payment Differed");
                }
                else
                {
                    item.SubItems.Add("Paid");
                }
                if (billDoc != null)
                {
                    item.SubItems.Add(AccountBase.FormatAmount(billDoc.total));
                    item.SubItems.Add(billDoc.ShortDescription);
                }
                else
                {
                    item.SubItems.Add("Error!");
                    item.SubItems.Add("Error!");
                }
                this.listView.Items.Add(item);
            }
        }

        
        int compareBill(CustomerBillRecord bill1, CustomerBillRecord bill2)
        {
            if (bill1.periodID == -1 && bill2.periodID == -1)
                return 0;
            if (bill1.periodID == -1)
                return -1;
            if (bill2.periodID == -1)
                return 1;
            if (bill1.periodID != bill2.periodID)
                return SubscriberManagmentClient.GetBillPeriod(bill1.periodID).fromDate.CompareTo(SubscriberManagmentClient.GetBillPeriod(bill2.periodID).fromDate);
            return bill1.billDocumentTypeID.CompareTo(bill2.billDocumentTypeID);
        }
        private void ReloadData()
        {
            CustomerBillRecord[] bills = SubscriberManagmentClient.getBills(-1, this._customer.id, -1, -1);
            Array.Sort<CustomerBillRecord>(bills, new Comparison<CustomerBillRecord>(compareBill));
            this.LoadList(bills);
            this.controlBrowser.LoadTextPage("Bill", "");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to generate outstanding bills from old system?"))
                    return;
                SubscriberManagmentClient.generateOutstandingBills(_customer.id);
                ReloadData();
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Outstanding bills generated without error");
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }

    }
}

