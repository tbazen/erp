using INTAPS.Ethiopic;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
using INTAPS.UI.Windows;
using System;
using System.Drawing;
using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{
    public partial class GenerateBillsForm : AsyncForm
    {
        int _customer = -1;

        public GenerateBillsForm()
        {
            this.InitializeComponent();
            this.Text = "Generate Bills";
            this.dtpDate.Value = DateTime.Now;
            this.kebelePicker.LoadData();
            this.kebelePicker.SetData(null);
            this.billPeriodSelector.LoadData(SubscriberManagmentClient.CurrentYear);
        }


        public GenerateBillsForm(int customers)
            : this()
        {
            this._customer = customers;
            this.kebelePicker.Visible = false;
        }


        GenerateBillJob _gj = null;
        protected virtual GenerateBillJob createJob(DateTime time, int[] ks, int customer, BillPeriod prd)
        {
            return new GenerateBillJob(time, ks, customer, prd);
        }
        protected virtual void StartJob()
        {
            listResult.Items.Clear();
            _gj = createJob(dtpDate.Value, _customer == -1 ? kebelePicker.GetData() : null, _customer, billPeriodSelector.SelectedPeriod);
            buttonGo.Text = "Stop";
            base.WaitFor(_gj);

        }
        protected override void WaitIsOver(ServerDataDownloader d)
        {
            buttonGo.Text = "Start";
            if (_gj._ex == null)
            {
                int nErrors = 0;
                int nWarning = 0;
                foreach (BatchJobResult res in ((GenerateBillJob)d).res)
                {
                    ListViewItem li = new ListViewItem(res.item);
                    li.SubItems.Add(res.statusDescription);
                    li.Tag = res.ex;
                    listResult.Items.Add(li);
                    switch (res.resultType)
                    {
                        case BatchJobResultType.Ok:
                            break;
                        case BatchJobResultType.Error:
                            nErrors++;
                            break;
                        case BatchJobResultType.Warning:
                            nWarning++;
                            break;
                        default:
                            break;
                    }
                }
                if (nErrors == 0 && nWarning == 0)
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage(_gj.Description + " completed with no problem");
                else if (nErrors == 0)
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserWarning(string.Format("{0} completed with {1} warning", _gj.Description, nWarning));
                else if (nWarning == 0)
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserWarning(string.Format("{0} completed with {1} error", _gj.Description, nErrors));
                else
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserWarning(string.Format("{0} completed with {1} error and {2} warning", _gj.Description, nErrors, nWarning));
                progressBar1.Value = 0;
            }
            else
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to generat bills", _gj._ex);
            labelStatus.Text = "Ready";
        }
        protected virtual void StopJob()
        {
            SubscriberManagmentClient.cancelBillGeneration();
        }
        protected override void CheckProgress()
        {
            string msg;
            int v = (int)Math.Round(Accounting.Client.AccountingClient.GetProccessProgress(out msg) * 100, 0);
            progressBar1.Value = v > 100 ? 100 : (v < 0 ? 0 : v);
            labelStatus.Text = string.IsNullOrEmpty(msg) ? "" : msg;
            base.CheckProgress();
        }
        private void billPeriodSelector_SelectionChanged(object sender, EventArgs e)
        {
            //dtpDate.Value = billPeriodSelector.SelectedPeriod.toDate;
        }

        private void listResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listResult.SelectedItems.Count == 0)
                return;
            Exception ex = listResult.SelectedItems[0].Tag as Exception;
            if (ex == null)
                return;
            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(listResult.SelectedItems[0].SubItems[1].Text, ex);
        }

        private void buttonGo_Click(object sender, EventArgs e)
        {
            if (_gj == null || _gj.State == ServerDownloaderState.Idle || _gj.State == ServerDownloaderState.Complete)
                StartJob();
            else if (_gj.State == ServerDownloaderState.Busy)
                StopJob();
            else
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Unexpected job status " + _gj.State);
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }
    }
    public class GenerateBillJob : ServerDataDownloader
    {
        protected int _customer;
        protected int[] _ks;
        protected BillPeriod _prd;
        protected DateTime _time;
        public BatchJobResult[] res = null;
        public Exception _ex = null;

        public GenerateBillJob(DateTime time, int[] ks, int cust, BillPeriod prd)
        {
            this._customer = cust;
            this._prd = prd;
            _ks = ks;
            _time = time;
        }
        public override void RunDownloader()
        {
            try
            {
                if (_customer != -1)
                {
                    res = SubscriberManagmentClient.generatCustomerPeriodicBills(_time, _customer, _prd.id);
                }
                else
                {
                    SubscriberManagmentClient.startBillGenerationByKebele(_time, _prd.id, _ks);
                    do
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                    while (SubscriberManagmentClient.IsBillGenerating());
                    res = SubscriberManagmentClient.getBillGenerationResult();
                }
            }
            catch (Exception ex)
            {
                this._ex = ex;
            }
        }

        public override string Description
        {
            get { return "Generating Bills"; }
        }
    }
    public class DeleteBillJob : GenerateBillJob
    {
        public DeleteBillJob(DateTime time, int[] ks, int cust, BillPeriod prd)
            : base(time, ks, cust, prd)
        {
        }
        public override void RunDownloader()
        {
            try
            {
                res = SubscriberManagmentClient.deleteUnpaidPeriodicBillByKebele(_prd.id, _ks);
            }
            catch (Exception ex)
            {
                this._ex = ex;
            }

        }

        public override string Description
        {
            get { return "Deleting Bills"; }
        }
    }
    public class PostBillJob : GenerateBillJob
    {
        int[] _billRecordID = null;
        public PostBillJob(int[] billRecordID, DateTime time, int[] ks, int cust, BillPeriod prd)
            : base(time, ks, cust, prd)
        {
            _billRecordID = billRecordID;
        }
        public override void RunDownloader()
        {
            try
            {
                if (_billRecordID == null)
                    res = SubscriberManagmentClient.postBillsByKebele(_time, _prd.id, _ks);
                else
                    res = SubscriberManagmentClient.postBills(_time, -1, _billRecordID);
            }
            catch (Exception ex)
            {
                this._ex = ex;
            }

        }

        public override string Description
        {
            get { return "Posting Bills"; }
        }
    }
    public class DeleteBillsForm : GenerateBillsForm
    {
        public DeleteBillsForm()
        {
            this.Text = "Delete bills";
        }
        protected override GenerateBillJob createJob(DateTime time, int[] ks, int customer, BillPeriod prd)
        {
            return new DeleteBillJob(time, ks, -1, prd);
        }
    }
    public class PostBillsForm : GenerateBillsForm
    {
        int[] _billRecordID = null;
        public PostBillsForm()
        {
            this.Text = "Post bills";
        }
        public PostBillsForm(int[] billRecordID) : this()
        {
            _billRecordID = billRecordID;
        }
        protected override GenerateBillJob createJob(DateTime time, int[] ks, int customer, BillPeriod prd)
        {
            return new PostBillJob(_billRecordID, time, ks, -1, prd);
        }
    }
}