
    using INTAPS.SubscriberManagment;
    using INTAPS.UI;
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Drawing;
    using System.Runtime.InteropServices;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class MatrixEditor : UserControl
    {
         
        private IContainer components = null;
         
        public event EventHandler DataChanged;

        public MatrixEditor()
        {
            this.InitializeComponent();
            this.dataGridView.Columns[1].ValueType = typeof(double);
            this.dataGridView.Columns[2].ValueType = typeof(double);
            this.dataGridView.DataError += new DataGridViewDataErrorEventHandler(this.dataGridView_DataError);
        }

        private void dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.RowIndex != -1) && (e.ColumnIndex == 3))
            {
                if (this.dataGridView.Rows.Count == 1)
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserMessage("The setting table needs at least one row.");
                }
                else
                {
                    int rowIndex = e.RowIndex;
                    this.dataGridView.Rows.RemoveAt(rowIndex);
                    if (this.dataGridView.Rows.Count == 1)
                    {
                        this.dataGridView[0, 0].Value = ">0";
                        this.dataGridView[1, 0].Value = null;
                    }
                    else if (rowIndex == this.dataGridView.Rows.Count)
                    {
                        this.dataGridView_CellEndEdit(sender, new DataGridViewCellEventArgs(1, rowIndex - 2));
                    }
                    else if (rowIndex == 0)
                    {
                        this.dataGridView[0, 0].Value = "0";
                    }
                    else
                    {
                        this.dataGridView_CellEndEdit(sender, new DataGridViewCellEventArgs(1, rowIndex - 1));
                    }
                    if (this.DataChanged != null)
                    {
                        this.DataChanged(this, null);
                    }
                }
            }
        }

        private void dataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                DataGridViewRow row = this.dataGridView.Rows[e.RowIndex];
                if (row.Index == (this.dataGridView.Rows.Count - 1))
                {
                    if (row.Index == 0)
                    {
                        row.Cells[0].Value = "0";
                    }
                    else
                    {
                        double num = ((double) this.dataGridView[1, row.Index - 1].Value) + 1.0;
                        row.Cells[0].Value = num.ToString();
                    }
                    object[] values = new object[2];
                    values[0] = ">" + row.Cells[1].Value;
                    this.dataGridView.Rows.Add(values);
                    //this.dataGridView.CurrentCell = this.dataGridView[1, this.dataGridView.Rows.Count - 1];
                }
                else if (row.Index == (this.dataGridView.Rows.Count - 2))
                {
                    this.dataGridView[0, row.Index + 1].Value = ">" + row.Cells[1].Value;
                    this.dataGridView[1, row.Index + 1].Value = null;
                }
                else
                {
                    this.dataGridView[0, row.Index + 1].Value = (((double) row.Cells[1].Value) + 1.0).ToString();
                }
                if (this.DataChanged != null)
                {
                    this.DataChanged(this, null);
                }
            }
        }

        private void dataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        public bool GetData(out RateMatrix mat)
        {
            mat = null;
            mat = new RateMatrix();
            mat.ranges = new double[this.dataGridView.Rows.Count - 1];
            mat.rates = new double[mat.ranges.Length];
            double lowerBound = 0.0;
            foreach (DataGridViewRow row in (IEnumerable) this.dataGridView.Rows)
            {
                if (!((row.Cells[1].Value is double) || (row.Index >= (this.dataGridView.Rows.Count - 1))))
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please enter a range value.");
                    this.dataGridView.CurrentCell = row.Cells[1];
                    return false;
                }
                if (!(row.Cells[2].Value is double))
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please enter a rate.");
                    this.dataGridView.CurrentCell = row.Cells[2];
                    return false;
                }
                if (row.Index < (this.dataGridView.Rows.Count - 1))
                {
                    mat.ranges[row.Index] = (double) row.Cells[1].Value;
                    if (lowerBound >= mat.ranges[row.Index])
                    {
                        UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Ranges must be entered in increasing order.");
                        this.dataGridView.CurrentCell = row.Cells[1];
                        return false;
                    }
                    mat.rates[row.Index] = (double) row.Cells[2].Value;
                    lowerBound = mat.ranges[row.Index];
                }
                else
                {
                    mat.toInf = (double) row.Cells[2].Value;
                }
            }
            return true;
        }


        public void SetData(RateMatrix mat)
        {
            this.dataGridView.Rows.Clear();
            double lowerBound = 0.0;
            int index = 0;
            while (index < mat.ranges.Length)
            {
                this.dataGridView.Rows.Add(new object[] { lowerBound==0?"0": (lowerBound+1).ToString(), mat.ranges[index], mat.rates[index] });
                this.dataGridView[0, index].ReadOnly = true;
                lowerBound = mat.ranges[index];
                index++;
            }
            this.dataGridView.Rows.Add(new object[] { ">" + lowerBound.ToString(), "", mat.toInf });
            this.dataGridView[0, index].ReadOnly = true;
        }
    }
}

