
    using INTAPS.SubscriberManagment;
    using INTAPS.UI;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class RateSettingEditor : Form
    {
        
        
        
        private IContainer components = null;
        private bool m_changed = false;
        

        public RateSettingEditor()
        {
            this.InitializeComponent();
            this.LoadData();
        }

        private void AddSetting(SingeRateSetting setting)
        {
            SingleRateSettingEditor editor = new SingleRateSettingEditor();
            editor.DataChanged += new EventHandler(this.re_DataChanged);
            editor.DeleteThis += new EventHandler(this.re_DeleteThis);
            editor.SetData(setting);
            TabPage page = new TabPage();
            page.Tag = editor;
            page.Text = setting.name;
            page.Controls.Add(editor);
            editor.Dock = DockStyle.Fill;
            this.tabControl.TabPages.Add(page);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (this.m_changed)
            {
                try
                {
                    RateSetting setting = new RateSetting();
                    setting.setting = new SingeRateSetting[this.tabControl.TabPages.Count];
                    for (int i = 0; i < this.tabControl.TabPages.Count; i++)
                    {
                        SingleRateSettingEditor tag = this.tabControl.TabPages[i].Tag as SingleRateSettingEditor;
                        if ((setting.setting[i] = tag.GetData()) == null)
                        {
                            return;
                        }
                    }
                    SubscriberManagmentClient.SetSystemParameters(new string[] { "rateSetting" }, new object[] { setting });
                }
                catch (Exception exception)
                {
                    UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't save settting", exception);
                    return;
                }
            }
            base.DialogResult = DialogResult.OK;
            base.Close();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            string str = "New Rate";
            int num = 0;
            for (int i = 0; i < this.tabControl.TabPages.Count; i++)
            {
                TabPage page = this.tabControl.TabPages[i];
                if (page.Text == str)
                {
                    num++;
                    str = "New Rate " + num;
                    i = 0;
                }
            }
            SingeRateSetting setting = new SingeRateSetting();
            setting.name = str;
            this.AddSetting(setting);
            this.tabControl.SelectedTab = this.tabControl.TabPages[this.tabControl.TabPages.Count - 1];
            this.DataChanged();
        }

        public void DataChanged()
        {
            this.m_changed = true;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void LoadData()
        {
            RateSetting setting = SubscriberManagmentClient.GetSystemParameters(new string[] { "rateSetting" })[0] as RateSetting;
            if (setting != null)
            {
                for (int i = 0; i < setting.setting.Length; i++)
                {
                    this.AddSetting(setting.setting[i]);
                }
            }
        }

        private void re_DataChanged(object sender, EventArgs e)
        {
            this.DataChanged();
        }

        private void re_DeleteThis(object sender, EventArgs e)
        {
            if (UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete this?"))
            {
                SingleRateSettingEditor editor = sender as SingleRateSettingEditor;
                foreach (TabPage page in this.tabControl.TabPages)
                {
                    if (page.Tag == editor)
                    {
                        this.tabControl.TabPages.Remove(page);
                        m_changed = true;
                        break;
                    }
                }
            }
        }

        private void RateSettingEditor_Load(object sender, EventArgs e)
        {

        }
    }
}

