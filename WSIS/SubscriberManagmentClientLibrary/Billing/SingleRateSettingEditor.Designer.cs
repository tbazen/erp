﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class SingleRateSettingEditor : System.Windows.Forms.UserControl
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SingleRateSettingEditor));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.pageRate = new System.Windows.Forms.TabPage();
            this.matrixEditor = new INTAPS.SubscriberManagment.Client.MatrixEditor();
            this.pageFilter = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.listSubscType = new System.Windows.Forms.ListView();
            this.listKebele = new System.Windows.Forms.ListView();
            this.listSubTypes = new System.Windows.Forms.ListView();
            this.billPeriodRangeSelector = new INTAPS.SubscriberManagment.Client.BillPeriodRangeSelector();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textPenalityFormula = new System.Windows.Forms.TextBox();
            this.lblFormula = new System.Windows.Forms.Label();
            this.pageRent = new System.Windows.Forms.TabPage();
            this.gridPenalityRate = new System.Windows.Forms.DataGridView();
            this.colMat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.textServiceCharge = new System.Windows.Forms.TextBox();
            this.labelAdditionalFormula = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textName = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.acIncomeWaterUse = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.acIncomeRent = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.acIncomePenality = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.acIncomeOther = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.tabControl.SuspendLayout();
            this.pageRate.SuspendLayout();
            this.pageFilter.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.pageRent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPenalityRate)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.pageRate);
            this.tabControl.Controls.Add(this.pageFilter);
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.pageRent);
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Location = new System.Drawing.Point(4, 38);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(518, 439);
            this.tabControl.TabIndex = 0;
            // 
            // pageRate
            // 
            this.pageRate.Controls.Add(this.matrixEditor);
            this.pageRate.Controls.Add(this.panel1);
            this.pageRate.Location = new System.Drawing.Point(4, 22);
            this.pageRate.Name = "pageRate";
            this.pageRate.Padding = new System.Windows.Forms.Padding(3);
            this.pageRate.Size = new System.Drawing.Size(510, 413);
            this.pageRate.TabIndex = 1;
            this.pageRate.Text = "Rates";
            this.pageRate.UseVisualStyleBackColor = true;
            // 
            // matrixEditor
            // 
            this.matrixEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.matrixEditor.Location = new System.Drawing.Point(3, 62);
            this.matrixEditor.Name = "matrixEditor";
            this.matrixEditor.Size = new System.Drawing.Size(504, 348);
            this.matrixEditor.TabIndex = 0;
            this.matrixEditor.DataChanged += new System.EventHandler(this.matrixEditor_DataChanged);
            // 
            // pageFilter
            // 
            this.pageFilter.BackColor = System.Drawing.Color.Gray;
            this.pageFilter.Controls.Add(this.label3);
            this.pageFilter.Controls.Add(this.label4);
            this.pageFilter.Controls.Add(this.label2);
            this.pageFilter.Controls.Add(this.listSubscType);
            this.pageFilter.Controls.Add(this.listKebele);
            this.pageFilter.Controls.Add(this.listSubTypes);
            this.pageFilter.Controls.Add(this.billPeriodRangeSelector);
            this.pageFilter.Location = new System.Drawing.Point(4, 22);
            this.pageFilter.Name = "pageFilter";
            this.pageFilter.Padding = new System.Windows.Forms.Padding(3);
            this.pageFilter.Size = new System.Drawing.Size(510, 413);
            this.pageFilter.TabIndex = 0;
            this.pageFilter.Text = "Domain";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(7, 242);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 35;
            this.label3.Text = "Subscription Type";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(290, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 35;
            this.label4.Text = "Kebele";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(7, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "Subscriber Type";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // listSubscType
            // 
            this.listSubscType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listSubscType.CheckBoxes = true;
            this.listSubscType.Location = new System.Drawing.Point(6, 258);
            this.listSubscType.Name = "listSubscType";
            this.listSubscType.Size = new System.Drawing.Size(248, 149);
            this.listSubscType.TabIndex = 34;
            this.listSubscType.UseCompatibleStateImageBehavior = false;
            this.listSubscType.View = System.Windows.Forms.View.List;
            this.listSubscType.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listSubscType_ItemChecked);
            // 
            // listKebele
            // 
            this.listKebele.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listKebele.CheckBoxes = true;
            this.listKebele.Location = new System.Drawing.Point(294, 113);
            this.listKebele.Name = "listKebele";
            this.listKebele.Size = new System.Drawing.Size(210, 294);
            this.listKebele.TabIndex = 34;
            this.listKebele.UseCompatibleStateImageBehavior = false;
            this.listKebele.View = System.Windows.Forms.View.List;
            this.listKebele.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listKebele_ItemChecked);
            // 
            // listSubTypes
            // 
            this.listSubTypes.CheckBoxes = true;
            this.listSubTypes.Location = new System.Drawing.Point(6, 113);
            this.listSubTypes.Name = "listSubTypes";
            this.listSubTypes.Size = new System.Drawing.Size(248, 126);
            this.listSubTypes.TabIndex = 34;
            this.listSubTypes.UseCompatibleStateImageBehavior = false;
            this.listSubTypes.View = System.Windows.Forms.View.List;
            this.listSubTypes.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listSubTypes_ItemChecked);
            // 
            // billPeriodRangeSelector
            // 
            this.billPeriodRangeSelector.ForeColor = System.Drawing.Color.White;
            this.billPeriodRangeSelector.Location = new System.Drawing.Point(6, 3);
            this.billPeriodRangeSelector.Name = "billPeriodRangeSelector";
            this.billPeriodRangeSelector.Size = new System.Drawing.Size(428, 90);
            this.billPeriodRangeSelector.TabIndex = 12;
            this.billPeriodRangeSelector.PeriodRangeChanged += new System.EventHandler(this.billPeriodRangeSelector_PeriodRangeChanged);
            this.billPeriodRangeSelector.Load += new System.EventHandler(this.billPeriodRangeSelector_Load_1);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textPenalityFormula);
            this.tabPage1.Controls.Add(this.lblFormula);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(510, 413);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Penality Formula";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // textPenalityFormula
            // 
            this.textPenalityFormula.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textPenalityFormula.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPenalityFormula.Location = new System.Drawing.Point(3, 150);
            this.textPenalityFormula.Multiline = true;
            this.textPenalityFormula.Name = "textPenalityFormula";
            this.textPenalityFormula.Size = new System.Drawing.Size(504, 260);
            this.textPenalityFormula.TabIndex = 1;
            this.textPenalityFormula.TextChanged += new System.EventHandler(this.textFormula_TextChanged);
            // 
            // lblFormula
            // 
            this.lblFormula.BackColor = System.Drawing.Color.Gray;
            this.lblFormula.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblFormula.ForeColor = System.Drawing.Color.White;
            this.lblFormula.Location = new System.Drawing.Point(3, 62);
            this.lblFormula.Name = "lblFormula";
            this.lblFormula.Size = new System.Drawing.Size(504, 88);
            this.lblFormula.TabIndex = 0;
            this.lblFormula.Text = "Penality Formula";
            // 
            // pageRent
            // 
            this.pageRent.Controls.Add(this.gridPenalityRate);
            this.pageRent.Controls.Add(this.panel2);
            this.pageRent.Location = new System.Drawing.Point(4, 22);
            this.pageRent.Name = "pageRent";
            this.pageRent.Padding = new System.Windows.Forms.Padding(3);
            this.pageRent.Size = new System.Drawing.Size(510, 413);
            this.pageRent.TabIndex = 3;
            this.pageRent.Text = "Rent Rates";
            this.pageRent.UseVisualStyleBackColor = true;
            // 
            // gridPenalityRate
            // 
            this.gridPenalityRate.AllowUserToAddRows = false;
            this.gridPenalityRate.AllowUserToDeleteRows = false;
            this.gridPenalityRate.AllowUserToOrderColumns = true;
            this.gridPenalityRate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPenalityRate.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colMat,
            this.colRent});
            this.gridPenalityRate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPenalityRate.Location = new System.Drawing.Point(3, 62);
            this.gridPenalityRate.Name = "gridPenalityRate";
            this.gridPenalityRate.Size = new System.Drawing.Size(504, 348);
            this.gridPenalityRate.TabIndex = 0;
            this.gridPenalityRate.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridPenalityRate_CellEndEdit);
            // 
            // colMat
            // 
            this.colMat.HeaderText = "Meter Type";
            this.colMat.Name = "colMat";
            this.colMat.ReadOnly = true;
            this.colMat.Width = 200;
            // 
            // colRent
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colRent.DefaultCellStyle = dataGridViewCellStyle1;
            this.colRent.HeaderText = "Rent Rate";
            this.colRent.Name = "colRent";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.textServiceCharge);
            this.tabPage3.Controls.Add(this.labelAdditionalFormula);
            this.tabPage3.Controls.Add(this.panel4);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(510, 413);
            this.tabPage3.TabIndex = 5;
            this.tabPage3.Text = "Service Charge";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // textServiceCharge
            // 
            this.textServiceCharge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textServiceCharge.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textServiceCharge.Location = new System.Drawing.Point(3, 150);
            this.textServiceCharge.Multiline = true;
            this.textServiceCharge.Name = "textServiceCharge";
            this.textServiceCharge.Size = new System.Drawing.Size(504, 260);
            this.textServiceCharge.TabIndex = 3;
            this.textServiceCharge.TextChanged += new System.EventHandler(this.textServiceCharge_TextChanged);
            // 
            // labelAdditionalFormula
            // 
            this.labelAdditionalFormula.BackColor = System.Drawing.Color.Gray;
            this.labelAdditionalFormula.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelAdditionalFormula.ForeColor = System.Drawing.Color.White;
            this.labelAdditionalFormula.Location = new System.Drawing.Point(3, 62);
            this.labelAdditionalFormula.Name = "labelAdditionalFormula";
            this.labelAdditionalFormula.Size = new System.Drawing.Size(504, 88);
            this.labelAdditionalFormula.TabIndex = 2;
            this.labelAdditionalFormula.Text = "Service Charge Formula";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(5, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Rate Name:";
            this.label1.Click += new System.EventHandler(this.label8_Click);
            // 
            // textName
            // 
            this.textName.Location = new System.Drawing.Point(94, 11);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(297, 20);
            this.textName.TabIndex = 11;
            this.textName.TextChanged += new System.EventHandler(this.textName_TextChangecd);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(397, 7);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(118, 27);
            this.btnClose.TabIndex = 33;
            this.btnClose.Text = "&Remove Rate";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Type";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 200;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn2.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.acIncomeWaterUse);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(504, 59);
            this.panel1.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(252, 17);
            this.label5.TabIndex = 20;
            this.label5.Text = "Account from Income from Water Use";
            // 
            // acIncomeWaterUse
            // 
            this.acIncomeWaterUse.account = null;
            this.acIncomeWaterUse.AllowAdd = false;
            this.acIncomeWaterUse.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.acIncomeWaterUse.Location = new System.Drawing.Point(7, 33);
            this.acIncomeWaterUse.Name = "acIncomeWaterUse";
            this.acIncomeWaterUse.OnlyLeafAccount = false;
            this.acIncomeWaterUse.Size = new System.Drawing.Size(373, 20);
            this.acIncomeWaterUse.TabIndex = 21;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.acIncomeRent);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(504, 59);
            this.panel2.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(257, 17);
            this.label6.TabIndex = 20;
            this.label6.Text = "Account from Income from Meter Rent";
            // 
            // acIncomeRent
            // 
            this.acIncomeRent.account = null;
            this.acIncomeRent.AllowAdd = false;
            this.acIncomeRent.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.acIncomeRent.Location = new System.Drawing.Point(7, 33);
            this.acIncomeRent.Name = "acIncomeRent";
            this.acIncomeRent.OnlyLeafAccount = false;
            this.acIncomeRent.Size = new System.Drawing.Size(373, 20);
            this.acIncomeRent.TabIndex = 21;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.acIncomePenality);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(504, 59);
            this.panel3.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Location = new System.Drawing.Point(4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(238, 17);
            this.label7.TabIndex = 20;
            this.label7.Text = "Account from Income from Penality";
            // 
            // acIncomePenality
            // 
            this.acIncomePenality.account = null;
            this.acIncomePenality.AllowAdd = false;
            this.acIncomePenality.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.acIncomePenality.Location = new System.Drawing.Point(3, 36);
            this.acIncomePenality.Name = "acIncomePenality";
            this.acIncomePenality.OnlyLeafAccount = false;
            this.acIncomePenality.Size = new System.Drawing.Size(373, 20);
            this.acIncomePenality.TabIndex = 21;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.acIncomeOther);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(504, 59);
            this.panel4.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(286, 17);
            this.label8.TabIndex = 20;
            this.label8.Text = "Account from Income from Service Charge";
            // 
            // acIncomeServiceCharge
            // 
            this.acIncomeOther.account = null;
            this.acIncomeOther.AllowAdd = false;
            this.acIncomeOther.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.acIncomeOther.Location = new System.Drawing.Point(7, 33);
            this.acIncomeOther.Name = "acIncomeServiceCharge";
            this.acIncomeOther.OnlyLeafAccount = false;
            this.acIncomeOther.Size = new System.Drawing.Size(373, 20);
            this.acIncomeOther.TabIndex = 21;
            // 
            // SingleRateSettingEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.textName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl);
            this.Name = "SingleRateSettingEditor";
            this.Size = new System.Drawing.Size(525, 480);
            this.tabControl.ResumeLayout(false);
            this.pageRate.ResumeLayout(false);
            this.pageFilter.ResumeLayout(false);
            this.pageFilter.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.pageRent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridPenalityRate)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage pageRate;
        private System.Windows.Forms.TabPage pageFilter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView listSubscType;
        private System.Windows.Forms.ListView listKebele;
        private System.Windows.Forms.ListView listSubTypes;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox textPenalityFormula;
        private System.Windows.Forms.Label lblFormula;
        private System.Windows.Forms.TabPage pageRent;
        private System.Windows.Forms.DataGridView gridPenalityRate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textName;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private MatrixEditor matrixEditor;
        private BillPeriodRangeSelector billPeriodRangeSelector;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMat;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRent;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox textServiceCharge;
        private System.Windows.Forms.Label labelAdditionalFormula;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private Accounting.Client.AccountPlaceHolder acIncomeWaterUse;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label6;
        private Accounting.Client.AccountPlaceHolder acIncomeRent;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label7;
        private Accounting.Client.AccountPlaceHolder acIncomePenality;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label8;
        private Accounting.Client.AccountPlaceHolder acIncomeOther;
    }
}