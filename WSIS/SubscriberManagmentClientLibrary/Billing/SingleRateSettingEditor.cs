using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using BIZNET.iERP;
using BIZNET.iERP.Client;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class SingleRateSettingEditor : UserControl
    {
        private IContainer components;
        private bool m_IgnoreEvent;
        public event EventHandler DataChanged;

        public event EventHandler DeleteThis;

        public SingleRateSettingEditor()
        {
            ListViewItem item;
            this.components = null;
            this.m_IgnoreEvent = false;
            this.InitializeComponent();
            this.lblFormula.Text = "Penality Formula\r\nFormula should evaluate the penality for a single month of deyals\r\nD: the number of peried that passed since the bill produced;\r\nA: the billl amount; R: the consumption amount SStype: subscriber type;\r\nSCType: subscription type MType: meter type (uses material code)\r\n";
            this.billPeriodRangeSelector.LoadData(SubscriberManagmentClient.CurrentYear);
            foreach (SubscriberType type in Enum.GetValues(typeof(SubscriberType)))
            {
                if (type != SubscriberType.Unknown)
                {
                    item = new ListViewItem(Subscriber.EnumToName(type));
                    item.Tag = type;
                    this.listSubTypes.Items.Add(item);
                }
            }
            foreach (SubscriptionType type2 in Enum.GetValues(typeof(SubscriptionType)))
            {
                if (type2 != SubscriptionType.Unknown)
                {
                    item = new ListViewItem(Subscriber.EnumToName(type2));
                    item.Tag = type2;
                    this.listSubscType.Items.Add(item);
                }
            }
            Kebele kebele = new Kebele();
            kebele.id = -1;
            kebele.name = "All Kebele";
            this.listKebele.Items.Add(kebele.ToString()).Tag = -1;
            foreach (Kebele kebele2 in SubscriberManagmentClient.GetAllKebeles())
            {
                this.listKebele.Items.Add(kebele2.ToString()).Tag = kebele2.id;
            }
            this.LoadRentGrid();
            this.GetExaggeratedRange();


            this.labelAdditionalFormula.Text = "Serivice Charge Formula\r\nB: the bill amoung;\nR: the rent amount SStype: customer type;\r\nSCType: line type MType: meter type (uses material code)";

        }

        private void AddMatrix(SubscriberType[] subTypes, RateMatrix mat)
        {
            MatrixEditor editor = new MatrixEditor();
            editor.DataChanged += new EventHandler(this.me_DataChanged);
            editor.SetData(mat);
            TabPage page = new TabPage();
            page.Tag = editor;
            page.Text = "Rate " + this.tabControl.TabCount;
            page.Controls.Add(editor);
            editor.Dock = DockStyle.Fill;
            this.tabControl.TabPages.Add(page);
        }

        private void billPeriodRangeSelector_Load(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvent)
            {
                if (this.DataChanged != null)
                {
                    this.DataChanged(this, null);
                }
                if (base.Parent is TabPage)
                {
                    ((TabPage)base.Parent).Text = this.textName.Text;
                }
            }
        }

        private void billPeriodRangeSelector_PeriodRangeChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvent && (this.DataChanged != null))
            {
                this.DataChanged(this, null);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (this.DeleteThis != null)
            {
                this.DeleteThis(this, null);
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            this.AddMatrix(new SubscriberType[0], new RateMatrix());
            this.tabControl.SelectedTab = this.tabControl.TabPages[this.tabControl.TabPages.Count - 1];
            if (this.DataChanged != null)
            {
                this.DataChanged(this, null);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        public SingeRateSetting GetData()
        {
            SingeRateSetting rate = new SingeRateSetting();
            rate.name = this.textName.Text.Trim();
            if (rate.name == "")
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please enter a name.");
                return null;
            }
            rate.period = this.billPeriodRangeSelector.GetData();
            rate.subType = new SubscriberType[this.listSubTypes.CheckedItems.Count];
            int num = 0;
            foreach (ListViewItem item in this.listSubTypes.CheckedItems)
            {
                rate.subType[num++] = (SubscriberType)item.Tag;
            }
            num = 0;
            rate.subscType = new SubscriptionType[this.listSubscType.CheckedItems.Count];
            foreach (ListViewItem item in this.listSubscType.CheckedItems)
            {
                rate.subscType[num++] = (SubscriptionType)item.Tag;
            }
            if (this.listKebele.Items[0].Checked)
            {
                rate.allKebeles = true;
            }
            else
            {
                rate.allKebeles = false;
                rate.kebele = new int[this.listKebele.CheckedItems.Count];
                num = 0;
                foreach (ListViewItem item in this.listKebele.CheckedItems)
                {
                    rate.kebele[num++] = (int)item.Tag;
                }
            }
            this.GetRentData(rate);
            rate.penalityFormula = this.textPenalityFormula.Text;
            rate.additionalFormula = this.textServiceCharge.Text;
            if (!this.matrixEditor.GetData(out rate.matrixes))
            {
                return null;
            }

            rate.mainAccountID = acIncomeWaterUse.GetAccountID();
            rate.rentAccountID = acIncomeRent.GetAccountID();
            rate.penalityAccountID = acIncomePenality.GetAccountID();
            rate.additionalAccountID = acIncomeOther.GetAccountID();
            return rate;
        }

        private void GetExaggeratedRange()
        {
        }

        private void GetRentData(SingeRateSetting rate)
        {
            List<double> list = new List<double>();
            List<string> list2 = new List<string>();
            foreach (DataGridViewRow row in (IEnumerable)this.gridPenalityRate.Rows)
            {
                if (row.Cells[1].Value is double)
                {
                    list.Add((double)row.Cells[1].Value);
                    list2.Add((string)row.Tag);
                }
            }
            rate.rent = list.ToArray();
            rate.meterType = list2.ToArray();
        }

        private void gridPenalityRate_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (this.DataChanged != null)
            {
                this.DataChanged(this, null);
            }
        }


        private void label8_Click(object sender, EventArgs e)
        {
        }

        private void listKebele_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (!this.m_IgnoreEvent)
            {
                this.m_IgnoreEvent = true;
                try
                {
                    if (e.Item == this.listKebele.Items[0])
                    {
                        foreach (ListViewItem item in this.listKebele.Items)
                        {
                            if (item != e.Item)
                            {
                                item.Checked = e.Item.Checked;
                            }
                        }
                    }
                    if (this.DataChanged != null)
                    {
                        this.DataChanged(this, null);
                    }
                }
                finally
                {
                    this.m_IgnoreEvent = false;
                }
            }
        }

        private void listSubscType_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (!this.m_IgnoreEvent && (this.DataChanged != null))
            {
                this.DataChanged(this, null);
            }
        }

        private void listSubTypes_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (!this.m_IgnoreEvent && (this.DataChanged != null))
            {
                this.DataChanged(this, null);
            }
        }

        private void LoadRentGrid()
        {
            int systemParameter = Convert.ToInt32(SubscriberManagmentClient.GetSystemParameter("meterMaterialCategory"));
            this.gridPenalityRate.Columns[1].ValueType = typeof(double);
            if (systemParameter > 1)
            {
                try
                {
                    TransactionItems[] descriptionArray = iERPTransactionClient.GetItemsInCategory(systemParameter);
                    if (descriptionArray.Length == 0)
                    {
                        UIFormApplicationBase.CurrentAppliation.ShowUserMessage("No meter types found.");
                    }
                    foreach (TransactionItems description in descriptionArray)
                    {
                        object[] values = new object[2];
                        values[0] = description.Name;
                        this.gridPenalityRate.Rows[this.gridPenalityRate.Rows.Add(values)].Tag = description.Code;
                    }
                }
                catch (Exception exception)
                {
                    UIFormApplicationBase.CurrentAppliation.HandleException("Error loading material types.", exception);
                }
            }
        }

        private void matrixEditor_DataChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvent && (this.DataChanged != null))
            {
                this.DataChanged(this, null);
            }
        }

        private void me_DataChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvent && (this.DataChanged != null))
            {
                this.DataChanged(this, null);
            }
        }

        internal void SetData(SingeRateSetting setting)
        {
            this.m_IgnoreEvent = true;
            try
            {
                this.textServiceCharge.Text = setting.additionalFormula;

                this.textPenalityFormula.Text = setting.penalityFormula;
                this.textName.Text = setting.name;
                try
                {
                    this.billPeriodRangeSelector.SetData(setting.period);
                }
                catch
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserError("Period error for " + setting.name);
                }
                foreach (ListViewItem item in this.listSubTypes.Items)
                {
                    SubscriberType tag = (SubscriberType)item.Tag;
                    foreach (SubscriberType type2 in setting.subType)
                    {
                        if (tag == type2)
                        {
                            item.Checked = true;
                            break;
                        }
                    }
                }
                foreach (ListViewItem item in this.listSubscType.Items)
                {
                    SubscriptionType type3 = (SubscriptionType)item.Tag;
                    foreach (SubscriptionType type4 in setting.subscType)
                    {
                        if (type3 == type4)
                        {
                            item.Checked = true;
                            break;
                        }
                    }
                }
                foreach (ListViewItem item in this.listKebele.Items)
                {
                    if (setting.allKebeles)
                    {
                        item.Checked = true;
                    }
                    else
                    {
                        foreach (int num in setting.kebele)
                        {
                            if (((int)item.Tag) == num)
                            {
                                item.Checked = true;
                                break;
                            }
                        }
                    }
                }
                acIncomeWaterUse.SetByID(setting.mainAccountID);
                acIncomeRent.SetByID(setting.rentAccountID);
                acIncomePenality.SetByID(setting.penalityAccountID);
                acIncomeOther.SetByID(setting.additionalAccountID);

                this.SetRentData(setting);
                this.matrixEditor.SetData(setting.matrixes);
            }
            finally
            {
                this.m_IgnoreEvent = false;
            }
        }

        private void SetRentData(SingeRateSetting rate)
        {
            foreach (DataGridViewRow row in (IEnumerable)this.gridPenalityRate.Rows)
            {
                string code = (string)row.Tag;
                for (int i = 0; i < rate.meterType.Length; i++)
                {
                    if (code.Equals(rate.meterType[i]))
                    {
                        row.Cells[1].Value = rate.rent[i];
                        break;
                    }
                }
            }
        }

        private void textFormula_TextChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvent && (this.DataChanged != null))
            {
                this.DataChanged(this, null);
            }
        }

        private void textName_TextChangecd(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvent)
            {
                if (this.DataChanged != null)
                {
                    this.DataChanged(this, null);
                }
                if (base.Parent is TabPage)
                {
                    ((TabPage)base.Parent).Text = this.textName.Text;
                }
            }
        }

        private void textPublicRate_TextChanged(object sender, EventArgs e)
        {
        }

        private void UpdateEx()
        {
            /*int[] numArray = new int[this.grdExaggeratedRange.Rows.Count];
            string[] strArray = new string[this.grdExaggeratedRange.Rows.Count];
            int index = 0;
            foreach (DataGridViewRow row in (IEnumerable)this.grdExaggeratedRange.Rows)
            {
                numArray[index] = 0;
                numArray[index] = int.Parse(row.Cells[1].Value.ToString());
                strArray[index] = row.Cells[0].Value.ToString();
                index++;
            }*/
        }

        private void textServiceCharge_TextChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvent && (this.DataChanged != null))
            {
                this.DataChanged(this, null);
            }

        }

        private void billPeriodRangeSelector_Load_1(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}

