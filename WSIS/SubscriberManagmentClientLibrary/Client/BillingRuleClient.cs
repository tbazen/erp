﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace INTAPS.SubscriberManagment.Client
{
    public class PrintReceiptParameters
    {
        public string mainTitle;
        public string casheirName;
    }
    public class PrintLogData
    {
        public DateTime date=DateTime.Now;
        public CustomerPaymentReceipt paymentReceipt;
        public PrintReceiptParameters receiptParameters;
        public bool rePrint;
        public int nCopies;
        public bool success = true;
        public string[] exceptionMessage; //0=exception, >=1=inner exceptions
        public string[] stackTrace;

        public static void WritePrintLog(CustomerPaymentReceipt payment, PrintReceiptParameters printParameters, int nCopies, Exception exception,bool rePrint)
        {
            PrintLogData log = new PrintLogData();
            string extension = rePrint ? ".RePrint" : ".Print";
            if (!System.IO.Directory.Exists(Application.StartupPath + "\\log"))
            {
                Directory.CreateDirectory(Application.StartupPath + "\\log");
            }
            if (exception == null)
            {
                log.paymentReceipt = payment;
                log.receiptParameters = printParameters;
                log.rePrint = rePrint;
                log.nCopies = nCopies;
                string xmlString = ToXML(log);
                //if (!System.IO.Directory.Exists(Application.StartupPath + "\\log"))
                //{
                //    Directory.CreateDirectory(Application.StartupPath + "\\log");
                //}
                File.WriteAllText(String.Format("{0}\\{1}", Application.StartupPath + "\\log", Guid.NewGuid() + extension), xmlString);
            }
            else
            {
                log.success = false;
                List<string> exMessages = new List<string>();
                List<string> stackTraceMessages = new List<string>();
                exMessages.Add(exception.Message);
                stackTraceMessages.Add(exception.StackTrace);
                AddInnerExceptions(exception, exMessages, stackTraceMessages);
                log.exceptionMessage = exMessages.ToArray();
                log.stackTrace = stackTraceMessages.ToArray();
                File.WriteAllText(String.Format("{0}\\{1}", Application.StartupPath + "\\log", Guid.NewGuid() + ".Error"), ToXML(log));
            }

        }
        private static string ToXML(object typeObject)
        {
            System.Xml.Serialization.XmlSerializer s = new XmlSerializer(typeObject.GetType());
            StringWriter sw = new StringWriter();
            s.Serialize(sw, typeObject);
            return sw.ToString();
        }
        private static void AddInnerExceptions(Exception exception, List<string> exceptions, List<string> stackTraceExceptions)
        {
            if (exception.InnerException != null)
            {
                exceptions.Add(exception.InnerException.Message);
                stackTraceExceptions.Add(exception.InnerException.StackTrace);
                AddInnerExceptions(exception.InnerException, exceptions, stackTraceExceptions);
            }
        }
    }
    public interface IBillingRuleClient
    {
        void showSettingEditor(Type type,object setting);

        System.Resources.ResourceManager getStringTable(int languageID);

        void printReceipt(CustomerPaymentReceipt receipt, PrintReceiptParameters pars, bool reprint, int nCopies);

    }
}
