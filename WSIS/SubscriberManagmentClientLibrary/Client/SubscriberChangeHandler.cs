namespace INTAPS.SubscriberManagment.Client
{
    using INTAPS.SubscriberManagment;
    using System;
    using System.Runtime.CompilerServices;

    public delegate void SubscriberChangeHandler(INTAPS.ClientServer.Client.DataChange change, Subscriber obj, int objID);
}

