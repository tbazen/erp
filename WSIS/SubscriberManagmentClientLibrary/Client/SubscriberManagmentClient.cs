namespace INTAPS.SubscriberManagment.Client
{
    using INTAPS.ClientServer;
    using INTAPS.ClientServer.Client;
    using INTAPS.Ethiopic;
    using INTAPS.Payroll;
    using INTAPS.SubscriberManagment;
    using INTAPS.UI;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Reflection;

    public class SubscriberManagmentClient
    {
        // public static ISubscriberManagmentService subscriberManagmentServer;
        static string path;
        private static Kebele[] allKebeles = null;
        public static IBillingRuleClient billingRuleClient;
        public static string smsPassCode = null;
        public static BillPeriod CurrentPeriod
        {
            get
            {
                return GetBillPeriod(Convert.ToInt32(GetSystemParameter("currentPeriod")));
            }
        }
        public static int CurrentYear;
        public static bool Ethiopian;
        public static StringTranslator translator;
        public static BillPeriod ReadingPeriod
        {
            get
            {
                return GetBillPeriod(Convert.ToInt32(GetSystemParameter("readingPeriod")));
            }
        }

        public static void Connect(string url)
        {
            path = url + "/api/erp/subscribermanagment/";
            //subscriberManagmentServer = (ISubscriberManagmentService)RemotingServices.Connect(typeof(ISubscriberManagmentService), url + "/SubscriberServer");
            InitCalendar();
            loadBillingRuleEngineClient();
        }
        public static void loadBillingRuleEngineClient()
        {

            string an = System.Configuration.ConfigurationManager.AppSettings["billingRuleAssembly"];
            if (an != null)
            {
                Assembly a = System.Reflection.Assembly.Load(an);
                try
                {
                    foreach (Type t in a.GetTypes())
                    {
                        if (t.GetInterface(typeof(IBillingRuleClient).Name) != null)
                        {
                            ConstructorInfo ci = t.GetConstructor(new Type[] { });
                            if (ci == null)
                            {
                                Console.WriteLine("Constructor not found for billing rule client " + t);
                                continue;
                            }
                            try
                            {
                                IBillingRuleClient o = ci.Invoke(new object[] { }) as IBillingRuleClient;
                                if (o == null)
                                {
                                    Console.WriteLine("Billing rule client " + t + " couldn't be loaded");
                                    continue;
                                }
                                billingRuleClient = o;
                                Console.WriteLine("Billing rule client " + t + " loaded succesfully.");
                                return;
                            }
                            catch (Exception tex)
                            {
                                Console.WriteLine("Error trying to instantiate billing rule clinet " + t + "\n" + tex.Message);
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed to load billing rule handler\n" + ex.Message);
                }
            }
            Console.WriteLine("No billing rule handler found");
        }

        public static void loadBillingRuleEngineClient(byte[] rawAssembly)
        {
            Assembly a = Assembly.Load(rawAssembly);
            try
            {
                foreach (Type t in a.GetTypes())
                {
                    if (t.GetInterface(typeof(IBillingRuleClient).Name) != null)
                    {
                        ConstructorInfo ci = t.GetConstructor(new Type[] { });
                        if (ci == null)
                        {
                            Console.WriteLine("Constructor not found for billing rule client " + t);
                            continue;
                        }
                        try
                        {
                            IBillingRuleClient o = ci.Invoke(new object[] { }) as IBillingRuleClient;
                            if (o == null)
                            {
                                Console.WriteLine("Billing rule client " + t + " couldn't be loaded");
                                continue;
                            }
                            billingRuleClient = o;
                            Console.WriteLine("Billing rule client " + t + " loaded succesfully.");
                            return;
                        }
                        catch (Exception tex)
                        {
                            Console.WriteLine("Error trying to instantiate billing rule clinet " + t + "\n" + tex.Message);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to load billing rule handler\n" + ex.Message);
            }
        }





        public static event SubscriberChangeHandler SubscriberChanged;

        public static event SubscriptionChangeHandler SubscriptionChanged;

        public static int AddSubscription(Subscription subscription)
        {
            //subscription.id = subscriberManagmentServer.AddSubscription(ApplicationClient.SessionID, subscription);
            subscription.id = RESTAPIClient.Call<int>(path + "AddSubscription", new { sessionID = ApplicationClient.SessionID, subscription = subscription });
            OnSubscriptionChanged(DataChange.Create, subscription, subscription.id);
            return subscription.id;
        }

        public static void AddSurveyReading(MeterSurveyData data)
        {
            // subscriberManagmentServer.AddSurveyReading(ApplicationClient.SessionID, data);
            RESTAPIClient.Call<VoidRet>(path + "AddSurveyReading", new { sessionID = ApplicationClient.SessionID, data = data });
        }

        public static void BWFAddMeterReading(BWFMeterReading m)
        {
            //subscriberManagmentServer.BWFAddMeterReading(ApplicationClient.SessionID, m);
            RESTAPIClient.Call<VoidRet>(path + "BWFAddMeterReading", new { sessionID = ApplicationClient.SessionID, m = m });
        }
        public class BWFBatchCorrectReadingsOut
        {
            public System.Collections.Generic.List<Exception> errors;

        }
        public static void BWFBatchCorrectReadings(int periodID, BWFReadingProblemType problem, SubscriberType[] types, double consumption, out System.Collections.Generic.List<Exception> errors)
        {
            var _ret = new BWFBatchCorrectReadingsOut();
            // subscriberManagmentServer.BWFBatchCorrectReadings(ApplicationClient.SessionID, periodID, problem, types, consumption, out errors);
            RESTAPIClient.Call<VoidRet>(path + "BWFBatchCorrectReadings", new { sessionID = ApplicationClient.SessionID, periodID = periodID, problem = problem, types = types, consumption = consumption });
            errors = _ret.errors;
        }

        public static int BWFCreateMeterReading(BWFReadingBlock readingBlock, BWFMeterReading[] readings)
        {
            // return subscriberManagmentServer.BWFCreateMeterReading(ApplicationClient.SessionID, readingBlock, readings);
            return RESTAPIClient.Call<int>(path + "BWFCreateMeterReading", new { sessionID = ApplicationClient.SessionID, readingBlock = readingBlock, readings = readings });

        }

        public static int BWFCreateReadingBlock(BWFReadingBlock block)
        {
            //return subscriberManagmentServer.BWFCreateReadingBlock(ApplicationClient.SessionID, block);
            return RESTAPIClient.Call<int>(path + "BWFCreateReadingBlock", new { sessionID = ApplicationClient.SessionID, block = block });
        }

        public static void BWFDeleteMeterReading(int subscriptionID, int periodID)
        {
            // subscriberManagmentServer.BWFDeleteMeterReading(ApplicationClient.SessionID, subscriptionID, periodID);
            RESTAPIClient.Call<VoidRet>(path + "BWFDeleteMeterReading", new { sessionID = ApplicationClient.SessionID, subscriptionID = subscriptionID, periodID = periodID });
        }

        public static void BWFDeleteReadingBlock(int blockID)
        {
            //subscriberManagmentServer.BWFDeleteReadingBlock(ApplicationClient.SessionID, blockID);
            RESTAPIClient.Call<VoidRet>(path + "BWFDeleteReadingBlock", new { sessionID = ApplicationClient.SessionID, ReadingSheetID = blockID });
        }

        public static BWFMeterReading BWFGetMeterPreviousReading(int subcriptionID, int periodID)
        {
            //  return subscriberManagmentServer.BWFGetMeterPreviousReading(ApplicationClient.SessionID, subcriptionID, periodID);
            return RESTAPIClient.Call<BWFMeterReading>(path + "BWFGetMeterPreviousReading", new { sessionID = ApplicationClient.SessionID, subcriptionID = subcriptionID, periodID = periodID });
        }

        public static BWFMeterReading BWFGetMeterReading(int subcriptionID, int blockID)
        {
            // return subscriberManagmentServer.BWFGetMeterReading(ApplicationClient.SessionID, subcriptionID, blockID);
            return RESTAPIClient.Call<BWFMeterReading>(path + "BWFGetMeterReading", new { sessionID = ApplicationClient.SessionID, subcriptionID = subcriptionID, blockID = blockID });
        }
        public class BWFGetMeterReadingsOut
        {
            public int NRecords;
            public BWFMeterReading[] _ret;
        }
        public static BWFMeterReading[] BWFGetMeterReadings(int kebele, int subscriptionID, int periodID, int employeeID, int index, int pageSize, out int NRecords)
        {
            //return subscriberManagmentServer.BWFGetMeterReadings(ApplicationClient.SessionID, kebele, subscriptionID, periodID, employeeID, index, pageSize, out NRecords);
            var _ret = RESTAPIClient.Call<BWFGetMeterReadingsOut>(path + "BWFGetMeterReadings", new { sessionID = ApplicationClient.SessionID, kebele = kebele, subscriptionID = subscriptionID, periodID = periodID, employeeID = employeeID, index = index, pageSize = pageSize });
            NRecords = _ret.NRecords;
            return _ret._ret;
        }

        public static int BWFGetReadingBlockID(int subcriptionID, int periodID)
        {
            // return subscriberManagmentServer.BWFGetReadingBlockID(ApplicationClient.SessionID, subcriptionID, periodID);
            return RESTAPIClient.Call<int>(path + "BWFGetReadingBlockID", new { sessionID = ApplicationClient.SessionID, subcriptionID = subcriptionID, periodID = periodID });
        }

        public static int BWFGetReadingPeriodID(int blockID)
        {
            //return subscriberManagmentServer.BWFGetReadingPeriodID(ApplicationClient.SessionID, blockID);
            return RESTAPIClient.Call<int>(path + "BWFGetReadingPeriodID", new { sessionID = ApplicationClient.SessionID, blockID = blockID });
        }

        public static void BWFImportReadingBlocks(int destPeriodID, int[] blockID)
        {
            //subscriberManagmentServer.BWFImportReadingBlocks(ApplicationClient.SessionID, destPeriodID, blockID);
            RESTAPIClient.Call<VoidRet>(path + "BWFImportReadingBlocks", new { sessionID = ApplicationClient.SessionID, destPeriodID = destPeriodID, blockID = blockID });
        }

        public static void BWFImportReadings(int dest, int src, int[] subscriptionID)
        {
            //subscriberManagmentServer.BWFImportReadings(ApplicationClient.SessionID, dest, src, subscriptionID);
            RESTAPIClient.Call<VoidRet>(path + "BWFImportReadings", new { sessionID = ApplicationClient.SessionID, dest = dest, src = src, subscriptionID = subscriptionID });
        }

        public static void BWFMergeBlock(int dest, int[] sources)
        {
            //subscriberManagmentServer.BWFMergeBlock(ApplicationClient.SessionID, dest, sources);
            RESTAPIClient.Call<VoidRet>(path + "BWFMergeBlock", new { sessionID = ApplicationClient.SessionID, dest = dest, sources = sources });
        }

        public static void BWFUpdateBlock(BWFReadingBlock block)
        {
            // subscriberManagmentServer.BWFUpdateBlock(ApplicationClient.SessionID, block);
            RESTAPIClient.Call<VoidRet>(path + "BWFUpdateBlock", new { sessionID = ApplicationClient.SessionID, block = block });
        }

        public static void BWFUpdateMeterReading(BWFMeterReading reading)
        {
            //subscriberManagmentServer.BWFUpdateMeterReading(ApplicationClient.SessionID, reading);
            RESTAPIClient.Call<VoidRet>(path + "BWFUpdateMeterReading", new { sessionID = ApplicationClient.SessionID, reading = reading });
        }

        public static void ChangeSubcriberMeter(Subscription subscription, DateTime changeDate, MeterData meter)
        {
            //subscriberManagmentServer.ChangeSubcriberMeter(ApplicationClient.SessionID, subscription.id, changeDate, meter);
            RESTAPIClient.Call<VoidRet>(path + "ChangeSubcriberMeter", new { sessionID = ApplicationClient.SessionID, subscriptionID = subscription.id, changeDate = changeDate, meterID = meter });
            subscription.meterData = meter;
            OnSubscriptionChanged(DataChange.Update, subscription, subscription.id);
        }

        public static void ChangeSubcriptionStatus(Subscription subscription, DateTime changeDate, SubscriptionStatus newStatus)
        {
            //subscriberManagmentServer.ChangeSubcriptionStatus(ApplicationClient.SessionID, subscription.id, changeDate, newStatus);
            RESTAPIClient.Call<VoidRet>(path + "ChangeSubcriptionStatus", new { sessionID = ApplicationClient.SessionID, subscriptionID = subscription.id, changeDate = changeDate, newStatus = newStatus });
            subscription.subscriptionStatus = newStatus;
            subscription.ticksFrom = changeDate.Ticks;
            OnSubscriptionChanged(DataChange.Update, subscription, subscription.id);
        }



        private static PaymentCenter[] _paymentCenters = null;
        private static PaymentCenter[] paymentCenters
        {
            get
            {
                if (_paymentCenters == null)
                    //  _paymentCenters = subscriberManagmentServer.GetAllPaymentCenters(ApplicationClient.SessionID);
                    _paymentCenters = RESTAPIClient.Call<INTAPS.SubscriberManagment.PaymentCenter[]>(path + "GetAllPaymentCenters", new { sessionID = ApplicationClient.SessionID });
                return _paymentCenters;
            }
        }

        //public static void initTranlsationTable(TranslatorEntry[] table)
        //{
        //    translator = new StringTranslator(table);
        //}
        //public static void saveTranslation()
        //{
        //    try
        //    {
        //        subscriberManagmentServer.saveTranslation(ApplicationClient.SessionID, translator.newTranslations.ToArray());
        //    }
        //    catch (Exception ex)
        //    {
        //        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to save neew translation entries", ex);
        //    }
        //}
        public static PaymentCenter[] AllPaymentCenters
        {
            get
            {
                return paymentCenters;
            }
        }
        public static void CreateMeterReaderEmployee(MeterReaderEmployee employee)
        {
            // subscriberManagmentServer.CreateMeterReaderEmployee(ApplicationClient.SessionID, employee);
            RESTAPIClient.Call<VoidRet>(path + "CreateMeterReaderEmployee", new { sessionID = ApplicationClient.SessionID, employee = employee });
        }

        public static void CreateReadingEntryClerk(ReadingEntryClerk employee)
        {
            // subscriberManagmentServer.CreateReadingEntryClerk(ApplicationClient.SessionID, employee);
            RESTAPIClient.Call<VoidRet>(path + "CreateReadingEntryClerk", new { sessionID = ApplicationClient.SessionID, employee = employee });
        }

        public static void DeleteEntryClerk(int employeeID)
        {
            // subscriberManagmentServer.DeleteEntryClerk(ApplicationClient.SessionID, employeeID);
            RESTAPIClient.Call<VoidRet>(path + "DeleteEntryClerk", new { sessionID = ApplicationClient.SessionID, employeeID = employeeID });
        }


        public static void DeleteMeterReaderEmployee(int employeeID, int periodID)
        {
            //subscriberManagmentServer.DeleteMeterReaderEmployee(ApplicationClient.SessionID, employeeID, periodID);
            RESTAPIClient.Call<VoidRet>(path + "DeleteMeterReaderEmployee", new { sessionID = ApplicationClient.SessionID, employeeID = employeeID, periodID = periodID });
        }

        public static void DeleteSubscriber(int subscriberID)
        {
            //subscriberManagmentServer.DeleteSubscriber(ApplicationClient.SessionID, subscriberID);
            RESTAPIClient.Call<VoidRet>(path + "DeleteSubscriber", new { sessionID = ApplicationClient.SessionID, subscriberID = subscriberID });
            OnSubscriberChanged(DataChange.Delete, null, subscriberID);
        }

        public static void DeleteSubscription(int subscriptionID)
        {
            //subscriberManagmentServer.DeleteSubscription(ApplicationClient.SessionID, subscriptionID);
            RESTAPIClient.Call<VoidRet>(path + "DeleteSubscription", new { sessionID = ApplicationClient.SessionID, subscriptionID = subscriptionID });
            OnSubscriptionChanged(DataChange.Delete, null, subscriptionID);
        }




        public static Kebele[] GetAllKebeles()
        {
            if (allKebeles == null)
            {
                // allKebeles = subscriberManagmentServer.GetAllKebeles(ApplicationClient.SessionID);
                allKebeles = RESTAPIClient.Call<Kebele[]>(path + "GetAllKebeles", new { sessionID = ApplicationClient.SessionID });
            }
            return allKebeles;
        }

        public static MeterReaderEmployee[] GetAllMeterReaderEmployees(int periodID)
        {
            // return subscriberManagmentServer.GetAllMeterReaderEmployees(ApplicationClient.SessionID, periodID);
            return RESTAPIClient.Call<MeterReaderEmployee[]>(path + "GetAllMeterReaderEmployees", new { sessionID = ApplicationClient.SessionID, periodID = periodID });
        }

        public static ReadingEntryClerk[] GetAllReadingEntryClerk()
        {
            // return subscriberManagmentServer.GetAllReadingEntryClerk(ApplicationClient.SessionID);
            return RESTAPIClient.Call<ReadingEntryClerk[]>(path + "GetAllReadingEntryClerk", new { sessionID = ApplicationClient.SessionID });
        }
        static Dictionary<int, BillPeriod> getBillPeriodCashe = new Dictionary<int, BillPeriod>();
        public static BillPeriod GetBillPeriod(int periodID)
        {
            lock (getBillPeriodCashe)
            {
                if (getBillPeriodCashe.ContainsKey(periodID))
                    return getBillPeriodCashe[periodID];
                //BillPeriod bp = subscriberManagmentServer.GetBillPeriod(ApplicationClient.SessionID, periodID);
                BillPeriod bp = RESTAPIClient.Call<BillPeriod>(path + "GetBillPeriod", new { sessionID = ApplicationClient.SessionID, periodID = periodID });
                if (bp != null)
                    getBillPeriodCashe.Add(bp.id, bp);
                return bp;
            }
        }

        public static BillPeriod[] GetBillPeriods(int Year, bool EthiopianYear)
        {
            // return subscriberManagmentServer.GetBillPeriods(ApplicationClient.SessionID, Year, EthiopianYear);
            return RESTAPIClient.Call<BillPeriod[]>(path + "GetBillPeriods", new { sessionID = ApplicationClient.SessionID, Year = Year, EthiopianYear = EthiopianYear });
        }

        public static BWFReadingBlock GetBlock(int blockID)
        {
            //return subscriberManagmentServer.GetBlock(ApplicationClient.SessionID, blockID);
            return RESTAPIClient.Call<BWFReadingBlock>(path + "GetBlock", new { sessionID = ApplicationClient.SessionID, blockID = blockID });
        }

        public static BWFReadingBlock[] GetBlock(int period, string name)
        {
            //return subscriberManagmentServer.GetBlock(ApplicationClient.SessionID, period, name);
            return RESTAPIClient.Call<BWFReadingBlock[]>(path + "GetBlock2", new { sessionID = ApplicationClient.SessionID, period = period, name = name });
        }

        public static BWFReadingBlock[] GetBlocks(int periodID, int readerID)
        {
            // return subscriberManagmentServer.GetBlocks(ApplicationClient.SessionID, periodID, readerID);
            return RESTAPIClient.Call<BWFReadingBlock[]>(path + "GetBlocks", new { sessionID = ApplicationClient.SessionID, periodID = periodID, readerID = readerID });
        }

        public static BatchJobStatus GetGeneratorStatus()
        {
            // return subscriberManagmentServer.GetGeneratorStatus(ApplicationClient.SessionID);
            return RESTAPIClient.Call<BatchJobStatus>(path + "GetGeneratorStatus", new { sessionID = ApplicationClient.SessionID });
        }

        public static Kebele GetKebele(int kebele)
        {
            foreach (Kebele kebele2 in GetAllKebeles())
            {
                if (kebele2.id == kebele)
                {
                    return kebele2;
                }
            }
            return null;
        }
        public static string GetKebeleName(int kebele)
        {
            return GetKebeleName(kebele, false);
        }
        public static string GetKebeleName(int kebele, bool emptyStringIfInvalidKebele)
        {
            foreach (Kebele kebele2 in GetAllKebeles())
            {
                if (kebele2.id == kebele)
                {
                    return kebele2.name;
                }
            }
            if (emptyStringIfInvalidKebele)
                return "";
            return null;
        }

        public static BWFMeterReading[] GetMeterReadings(int subscriptionID)
        {
            //return subscriberManagmentServer.GetMeterReadings(ApplicationClient.SessionID, subscriptionID);
            return RESTAPIClient.Call<BWFMeterReading[]>(path + "GetMeterReadings", new { sessionID = ApplicationClient.SessionID, subscriptionID = subscriptionID });
        }

        public static PrePaymentBill[] GetPrepaymentData(int subscriptionID)
        {
            // return subscriberManagmentServer.GetPrepaymentData(ApplicationClient.SessionID, subscriptionID);
            return RESTAPIClient.Call<PrePaymentBill[]>(path + "GetPrepaymentData", new { sessionID = ApplicationClient.SessionID, subscriptionID = subscriptionID });
        }

        public static BillPeriod GetPreviousBillPeriod(int periodID)
        {
            //return subscriberManagmentServer.GetPreviousBillPeriod(ApplicationClient.SessionID, periodID);
            return RESTAPIClient.Call<BillPeriod>(path + "GetPreviousBillPeriod", new { sessionID = ApplicationClient.SessionID, periodID = periodID });
        }
        public class GetReadingsOut
        {
            public int NRecords;
            public BWFMeterReading[] _ret;
        }
        public static BWFMeterReading[] GetReadings(int blockID, int index, int pageSize, out int NRecords)
        {
            //  return subscriberManagmentServer.GetReadings(ApplicationClient.SessionID, blockID, index, pageSize, out NRecords);
            var _ret = RESTAPIClient.Call<GetReadingsOut>(path + "GetReadings", new { sessionID = ApplicationClient.SessionID, blockID = blockID, index = index, pageSize = pageSize });
            NRecords = _ret.NRecords;
            return _ret._ret;
        }

        public static Subscriber GetSubscriber(int subscriberID)
        {
            //return subscriberManagmentServer.GetSubscriber(ApplicationClient.SessionID, subscriberID);
            return RESTAPIClient.Call<Subscriber>(path + "GetSubscriber2", new { sessionID = ApplicationClient.SessionID, id = subscriberID });
        }



        public static Subscription GetSubscription(int id, long version)
        {
            // return subscriberManagmentServer.GetSubscription(ApplicationClient.SessionID, id, version);
            return RESTAPIClient.Call<Subscription>(path + "GetSubscription", new { sessionID = ApplicationClient.SessionID, id = id, version = version });
        }

        public static Subscription GetSubscription(string contractNO, long version)
        {
            // return subscriberManagmentServer.GetSubscription(ApplicationClient.SessionID, contractNO, version);
            return RESTAPIClient.Call<Subscription>(path + "GetSubscription2", new { sessionID = ApplicationClient.SessionID, contractNo = contractNO, version = version });
        }

        public static Subscription[] GetSubscriptionHistory(int subscriptionID)
        {
            //return subscriberManagmentServer.GetSubscriptionHistory(ApplicationClient.SessionID, subscriptionID);
            return RESTAPIClient.Call<Subscription[]>(path + "GetSubscriptionHistory", new { sessionID = ApplicationClient.SessionID, subscriptionID = subscriptionID });
        }

        public static Subscription[] GetSubscriptions(int subscriberID, long version)
        {
            //return subscriberManagmentServer.GetSubscriptions(ApplicationClient.SessionID, subscriberID, version);
            return RESTAPIClient.Call<Subscription[]>(path + "GetSubscriptions", new { sessionID = ApplicationClient.SessionID, subscriberID = subscriberID, version = version });
        }
        public class GetSubscriptions2Out
        {
            public int NRecord;
            public SubscriberSearchResult[] _ret;
        }
        public static SubscriberSearchResult[] GetSubscriptions(string query, SubscriberSearchField fields, bool multipleVersion, int kebele, int zone, int dma, int index, int pageSize, out int NRecords)
        {
            // return subscriberManagmentServer.GetSubscriptions(ApplicationClient.SessionID, query, fields, multipleVersion, kebele, zone, dma, index, pageSize, out NRecords);
            var _ret = RESTAPIClient.Call<GetSubscriptions2Out>(path + "GetSubscriptions2", new { sessionID = ApplicationClient.SessionID, query = query, fields = fields, multipleVersion = multipleVersion, kebele = kebele, zone = zone, dma = dma, index = index, page = pageSize });
            NRecords = _ret.NRecord;
            return _ret._ret;
        }

        public static SubscriptionStatus GetSubscriptionStatusAtDate(int subscriptionID, DateTime dateTime)
        {
            // return subscriberManagmentServer.GetSubscriptionStatusAtDate(ApplicationClient.SessionID, subscriptionID, dateTime);
            return RESTAPIClient.Call<SubscriptionStatus>(path + "GetSubscriptionStatusAtDate", new { sessionID = ApplicationClient.SessionID, subscriptionID = subscriptionID, dateTime = dateTime });
        }

        public static MeterSurveyData GetSurveyData(int blockID, string contractNo)
        {
            // return subscriberManagmentServer.GetSurveyData(ApplicationClient.SessionID, blockID, contractNo);
            return RESTAPIClient.Call<MeterSurveyData>(path + "GetSurveyData", new { sessionID = ApplicationClient.SessionID, blockID = blockID, contractNo = contractNo });
        }

        public static object GetSystemParameter(string name)
        {
            return ApplicationClient.GetSystemParameters(path, new string[] { name })[0];
        }

        public static object[] GetSystemParameters(params string[] fields)
        {
            return ApplicationClient.GetSystemParameters(path, fields);
        }


        private static void InitCalendar()
        {
            CurrentYear = EtGrDate.ToEth(DateTime.Now).Year;
            Ethiopian = true;
            //BillPeriod[] billPeriods = GetBillPeriods(CurrentYear, Ethiopian);
        }


        public static void OnSubscriberChanged(DataChange change, Subscriber obj, int objID)
        {
            if (SubscriberChanged != null)
            {
                SubscriberChanged(change, obj, objID);
            }
        }

        public static void OnSubscriptionChanged(DataChange change, Subscription obj, int objID)
        {
            if (SubscriptionChanged != null)
            {
                SubscriptionChanged(change, obj, objID);
            }
        }



        public static int RegisterSubscriber(Subscriber subscriber)
        {
            //subscriber.id = subscriberManagmentServer.RegisterSubscriber(ApplicationClient.SessionID, subscriber);
            subscriber.id = RESTAPIClient.Call<int>(path + "RegisterSubscriber", new { sessionID = ApplicationClient.SessionID, subscriber = subscriber });
            OnSubscriberChanged(DataChange.Create, subscriber, subscriber.id);
            return subscriber.id;
        }

        public static void ResetUpdateChain()
        {
            //subscriberManagmentServer.ResetUpdateChain(ApplicationClient.SessionID);
            RESTAPIClient.Call<VoidRet>(path + "ResetUpdateChain", new { sessionID = ApplicationClient.SessionID });
        }

        public static void SetSystemParameters(string[] fields, object[] vals)
        {
            ApplicationClient.SetSystemParameters(path, fields, vals);
        }

        public static void UpdateMeterReaderEmployee(MeterReaderEmployee employee)
        {
            //subscriberManagmentServer.UpdateMeterReaderEmployee(ApplicationClient.SessionID, employee);
            RESTAPIClient.Call<VoidRet>(path + "UpdateMeterReaderEmployee", new { sessionID = ApplicationClient.SessionID, employee = employee });
        }

        public static void UpdateSubscriber(Subscriber subscriber, Subscription[] subsc)
        {
            //subscriberManagmentServer.UpdateSubscriber(ApplicationClient.SessionID, subscriber, subsc);
            RESTAPIClient.Call<VoidRet>(path + "UpdateSubscriber", new { sessionID = ApplicationClient.SessionID, subscriber = subscriber, subsc = subsc });
            OnSubscriberChanged(DataChange.Update, subscriber, subscriber.id);
            if (subsc != null)
                foreach (Subscription s in subsc)
                    OnSubscriptionChanged(DataChange.Update, s, s.id);
        }

        public static void UpdateSubscription(Subscription subscription)
        {
            //subscriberManagmentServer.UpdateSubscription(ApplicationClient.SessionID, subscription);
            RESTAPIClient.Call<VoidRet>(path + "UpdateSubscription", new { sessionID = ApplicationClient.SessionID, });
            OnSubscriptionChanged(DataChange.Update, subscription, subscription.id);
        }

        public static void UpdateSurveyReading(MeterSurveyData data)
        {
            //subscriberManagmentServer.UpdateSurveyReading(ApplicationClient.SessionID, data);
            RESTAPIClient.Call<VoidRet>(path + "UpdateSurveyReading", new { sessionID = ApplicationClient.SessionID, data = data });
        }

        public static INTAPS.SubscriberManagment.Subscription getSubscriptionByMeterNo(string meterNo)
        {
            // return subscriberManagmentServer.getSubscriptionByMeterNo(ApplicationClient.SessionID, meterNo);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.Subscription>(path + "getSubscriptionByMeterNo", new { sessionID = ApplicationClient.SessionID, meterNo = meterNo });
        }

        public static INTAPS.SubscriberManagment.BillPeriod getPeriodForDate(System.DateTime date)
        {
            //return subscriberManagmentServer.getPeriodForDate(ApplicationClient.SessionID, date);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.BillPeriod>(path + "getPeriodForDate", new { sessionID = ApplicationClient.SessionID, date = date });
        }

        public static double getExageratedConsumption(int customerID)
        {
            // return subscriberManagmentServer.getExageratedConsumption(ApplicationClient.SessionID, customerID);
            return RESTAPIClient.Call<double>(path + "getExageratedConsumption", new { sessionID = ApplicationClient.SessionID, subscriptionID = customerID });
        }


        public static int lbSaveLegacySet(INTAPS.SubscriberManagment.LegacyBillSet set, INTAPS.SubscriberManagment.LegacyBill[] bills)
        {
            // return subscriberManagmentServer.lbSaveLegacySet(ApplicationClient.SessionID, set, bills);
            return RESTAPIClient.Call<int>(path + "lbSaveLegacySet", new { sessionID = ApplicationClient.SessionID, set = set, bills = bills });
        }

        public static INTAPS.SubscriberManagment.LegacyBill[] lbGetLegacyBills(int setID)
        {
            // return subscriberManagmentServer.lbGetLegacyBills(ApplicationClient.SessionID, setID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.LegacyBill[]>(path + "lbGetLegacyBills", new { sessionID = ApplicationClient.SessionID, setID = setID });
        }
        public class lbGetSetOut
        {
            public int N;
            public INTAPS.SubscriberManagment.LegacyBillSet[] _ret;
        }
        public static INTAPS.SubscriberManagment.LegacyBillSet[] lbGetSet(string userID, int pageIndex, int pageSize, out int N)
        {
            // return subscriberManagmentServer.lbGetSet(ApplicationClient.SessionID, userID, pageIndex, pageSize, out N);
            var _ret = RESTAPIClient.Call<lbGetSetOut>(path + "lbGetSet", new { sessionID = ApplicationClient.SessionID, userID = userID, pageIndex = pageIndex, pageSize = pageSize });
            N = _ret.N;
            return _ret._ret;
        }

        public static INTAPS.SubscriberManagment.LegacyBillItemDefination[] lbGetAllItemDefinations()
        {
            // return subscriberManagmentServer.lbGetAllItemDefinations(ApplicationClient.SessionID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.LegacyBillItemDefination[]>(path + "lbGetAllItemDefinations", new { sessionID = ApplicationClient.SessionID });
        }

        public static INTAPS.SubscriberManagment.BWFMeterReading BWFGetMeterReadingByPeriod(int subcriptionID, int peridID)
        {
            // return subscriberManagmentServer.BWFGetMeterReadingByPeriod(ApplicationClient.SessionID, subcriptionID, peridID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.BWFMeterReading>(path + "BWFGetMeterReadingByPeriod", new { sessionID = ApplicationClient.SessionID, subcriptionID = subcriptionID, peridID = peridID });
        }

        public static void lbDeleteSet(int setID)
        {
            //subscriberManagmentServer.lbDeleteSet(ApplicationClient.SessionID, setID);
            RESTAPIClient.Call<VoidRet>(path + "lbDeleteSet", new { sessionID = ApplicationClient.SessionID, setID = setID });
        }
        public class GetCustomersOut
        {
            public int NRecords;
            public INTAPS.SubscriberManagment.SubscriberSearchResult[] _ret;
        }
        public static INTAPS.SubscriberManagment.SubscriberSearchResult[] GetCustomers(string query, int kebele, int index, int pageSize, out int NRecords)
        {
            // return subscriberManagmentServer.GetCustomers(ApplicationClient.SessionID, query, kebele, index, pageSize, out NRecords);
            var _ret = RESTAPIClient.Call<GetCustomersOut>(path + "GetCustomers", new { sessionID = ApplicationClient.SessionID, query = query, kebele = kebele, index = index, pageSize = pageSize });
            NRecords = _ret.NRecords;
            return _ret._ret;
        }

        public static INTAPS.SubscriberManagment.PaymentCenter GetCurrentPaymentCenter()
        {
            //return subscriberManagmentServer.GetCurrentPaymentCenter(ApplicationClient.SessionID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.PaymentCenter>(path + "GetCurrentPaymentCenter", new { sessionID = ApplicationClient.SessionID });
        }

        public static INTAPS.SubscriberManagment.PaymentCenter[] GetAllPaymentCenters()
        {
            //return subscriberManagmentServer.GetAllPaymentCenters(ApplicationClient.SessionID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.PaymentCenter[]>(path + "GetAllPaymentCenters", new { sessionID = ApplicationClient.SessionID });
        }

        public static void DeletePaymentCenter(int centerID)
        {
            // subscriberManagmentServer.DeletePaymentCenter(ApplicationClient.SessionID, centerID);
            RESTAPIClient.Call<VoidRet>(path + "DeletePaymentCenter", new { sessionID = ApplicationClient.SessionID, centerID = centerID });
        }

        public static void UpdatePaymentCenter(INTAPS.SubscriberManagment.PaymentCenter center)
        {
            // subscriberManagmentServer.UpdatePaymentCenter(ApplicationClient.SessionID, center);
            RESTAPIClient.Call<VoidRet>(path + "UpdatePaymentCenter", new { sessionID = ApplicationClient.SessionID, center = center });
        }

        public static int CreatePaymentCenter(INTAPS.SubscriberManagment.PaymentCenter center)
        {
            //  return subscriberManagmentServer.CreatePaymentCenter(ApplicationClient.SessionID, center);
            return RESTAPIClient.Call<int>(path + "CreatePaymentCenter", new { sessionID = ApplicationClient.SessionID, center = center });
        }
        public static bool IsPaymentSerialBatchOverlapped(INTAPS.Accounting.SerialBatch batch)
        {
            //return subscriberManagmentServer.IsPaymentSerialBatchOverlapped(ApplicationClient.SessionID, batch);
            return RESTAPIClient.Call<bool>(path + "IsPaymentSerialBatchOverlapped", new { sessionID = ApplicationClient.SessionID, batch = batch });
        }
        public static INTAPS.SubscriberManagment.TranslatorEntry[] getTranslationTable()
        {
            //return subscriberManagmentServer.getTranslationTable(ApplicationClient.SessionID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.TranslatorEntry[]>(path + "getTranslationTable", new { sessionID = ApplicationClient.SessionID });
        }

        //public static void saveTranslation(INTAPS.SubscriberManagment.TranslatorEntry[] newEntries)
        //{
        //    subscriberManagmentServer.saveTranslation(ApplicationClient.SessionID, newEntries);
        //}

        public static INTAPS.SubscriberManagment.SubscriberManagmentUpdateNumber getUpdateNumber()
        {
            // return subscriberManagmentServer.getUpdateNumber(ApplicationClient.SessionID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.SubscriberManagmentUpdateNumber>(path + "getUpdateNumber", new { sessionID = ApplicationClient.SessionID });
        }
        public class BWFGetDescribedReadingsOut
        {
            public int NRecords;
            public INTAPS.SubscriberManagment.BWFDescribedMeterReading[] _ret;
        }
        public static INTAPS.SubscriberManagment.BWFDescribedMeterReading[] BWFGetDescribedReadings(int blockID, int index, int pageSize, out int NRecords)
        {
            //return subscriberManagmentServer.BWFGetDescribedReadings(ApplicationClient.SessionID, blockID, index, pageSize, out NRecords);
            var _ret = RESTAPIClient.Call<BWFGetDescribedReadingsOut>(path + "BWFGetDescribedReadings", new { sessionID = ApplicationClient.SessionID, blockID = blockID, index = index, pageSize = pageSize });
            NRecords = _ret.NRecords;
            return _ret._ret;
        }

        public static int lbSaveLegacyVerificationSet(INTAPS.SubscriberManagment.LegacyBillSet set, INTAPS.SubscriberManagment.VerificationEntry[] bills)
        {
            // return subscriberManagmentServer.lbSaveLegacyVerificationSet(ApplicationClient.SessionID, set, bills);
            return RESTAPIClient.Call<int>(path + "lbSaveLegacyVerificationSet", new { sessionID = ApplicationClient.SessionID, set = set, bills = bills });
        }

        public static INTAPS.SubscriberManagment.LegacyBill[] lbFindBySubscriberAndPeriod(int conID, int periodID)
        {
            //return subscriberManagmentServer.lbFindBySubscriberAndPeriod(ApplicationClient.SessionID, conID, periodID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.LegacyBill[]>(path + "lbFindBySubscriberAndPeriod", new { sessionID = ApplicationClient.SessionID, conID = conID, periodID = periodID });
        }

        public static INTAPS.SubscriberManagment.LegacyBill[] lbFindByBillNo(string billNo)
        {
            //return subscriberManagmentServer.lbFindByBillNo(ApplicationClient.SessionID, billNo);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.LegacyBill[]>(path + "lbFindByBillNo", new { sessionID = ApplicationClient.SessionID, billNo = billNo });
        }

        public static bool lbIsAlreadyVerified(int ownSetID, INTAPS.SubscriberManagment.VerificationEntry entry)
        {
            //return subscriberManagmentServer.lbIsAlreadyVerified(ApplicationClient.SessionID, ownSetID, entry);
            return RESTAPIClient.Call<bool>(path + "lbIsAlreadyVerified", new { sessionID = ApplicationClient.SessionID, ownSetID = ownSetID, entry = entry });
        }

        public static INTAPS.SubscriberManagment.LegacyBillSet lbGetVerifiedSetByCustomerAndPeriod(int customerID, int periodID)
        {
            //return subscriberManagmentServer.lbGetVerifiedSetByCustomerAndPeriod(ApplicationClient.SessionID, customerID, periodID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.LegacyBillSet>(path + "lbGetVerifiedSetByCustomerAndPeriod", new { sessionID = ApplicationClient.SessionID, customerID = customerID, periodID = periodID });
        }

        public static INTAPS.SubscriberManagment.BWFMeterReading BWFGetMeterReadingByPeriodID(int subcriptionID, int periodID)
        {
            //return subscriberManagmentServer.BWFGetMeterReadingByPeriodID(ApplicationClient.SessionID, subcriptionID, periodID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.BWFMeterReading>(path + "BWFGetMeterReadingByPeriodID", new { sessionID = ApplicationClient.SessionID, subcriptionID = subcriptionID, periodID = periodID });
        }

        public static INTAPS.UI.BatchJobResult[] generatCustomerPeriodicBills(System.DateTime time, int customerID, int periodID)
        {
            // return subscriberManagmentServer.generatCustomerPeriodicBills(ApplicationClient.SessionID, time, customerID, periodID);
            return RESTAPIClient.Call<INTAPS.UI.BatchJobResult[]>(path + "generatCustomerPeriodicBills", new { sessionID = ApplicationClient.SessionID, time = time, customerID = customerID, periodID = periodID });
        }

        public static INTAPS.UI.BatchJobResult[] generateBillsByKebele(System.DateTime time, int periodID, int[] kebeles)
        {
            //  return subscriberManagmentServer.generateBillsByKebele(ApplicationClient.SessionID, time, periodID, kebeles);
            return RESTAPIClient.Call<INTAPS.UI.BatchJobResult[]>(path + "generateBillsByKebele", new { sessionID = ApplicationClient.SessionID, time = time, periodID = periodID, kebeles = kebeles });
        }

        public static void cancelBillGeneration()
        {
            // subscriberManagmentServer.cancelBillGeneration(ApplicationClient.SessionID);
            RESTAPIClient.Call<VoidRet>(path + "cancelBillGeneration", new { sessionID = ApplicationClient.SessionID });
        }

        public static void deleteCustomerBill(int billRecordID)
        {
            // subscriberManagmentServer.deleteCustomerBill(ApplicationClient.SessionID, billRecordID);
            RESTAPIClient.Call<VoidRet>(path + "deleteCustomerBill", new { sessionID = ApplicationClient.SessionID, billRecordID = billRecordID });
        }

        public static INTAPS.SubscriberManagment.CustomerBillRecord[] getBills(int mainTypeID, int customerID, int connectionID, int periodID)
        {
            // return subscriberManagmentServer.getBills(ApplicationClient.SessionID, mainTypeID, customerID, connectionID, periodID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.CustomerBillRecord[]>(path + "getBills", new { sessionID = ApplicationClient.SessionID, mainTypeID = mainTypeID, customerID = customerID, connectionID = connectionID, periodID = periodID });
        }

        public static BatchJobResult[] deleteUnpaidPeriodicBillByKebele(int periodID, int[] kebeles)
        {
            // return subscriberManagmentServer.deleteUnpaidPeriodicBillByKebele(ApplicationClient.SessionID, periodID, kebeles);
            return RESTAPIClient.Call<BatchJobResult[]>(path + "deleteUnpaidPeriodicBillByKebele", new { sessionID = ApplicationClient.SessionID, periodID = periodID, kebeles = kebeles });
        }

        public static void postBill(System.DateTime time, int billRecordID)
        {
            //subscriberManagmentServer.postBill(ApplicationClient.SessionID, time, billRecordID);
            RESTAPIClient.Call<VoidRet>(path + "postBill", new { sessionID = ApplicationClient.SessionID, time = time, billRecordID = billRecordID });
        }

        public static INTAPS.UI.BatchJobResult[] postBills(System.DateTime time, int periodID, int[] billIDs)
        {
            //return subscriberManagmentServer.postBills(ApplicationClient.SessionID, time, periodID, billIDs);
            return RESTAPIClient.Call<INTAPS.UI.BatchJobResult[]>(path + "postBills", new { sessionID = ApplicationClient.SessionID, time = time, periodID = periodID, billIDs = billIDs });
        }

        public static INTAPS.UI.BatchJobResult[] postBillsByKebele(System.DateTime time, int periodID, int[] kebeles)
        {
            // return subscriberManagmentServer.postBillsByKebele(ApplicationClient.SessionID, time, periodID, kebeles);
            return RESTAPIClient.Call<INTAPS.UI.BatchJobResult[]>(path + "postBillsByKebele", new { sessionID = ApplicationClient.SessionID, time = time, periodID = periodID, kebeles = kebeles });
        }

        public static INTAPS.SubscriberManagment.CustomerBillDocument[] getBillDocuments(int mainTypeID, int customerID, int connectionID, int periodID, bool exculdePaid)
        {
            //  return subscriberManagmentServer.getBillDocuments(ApplicationClient.SessionID, mainTypeID, customerID, connectionID, periodID, exculdePaid);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.CustomerBillDocument[]>(path + "getBillDocuments", new { sessionID = ApplicationClient.SessionID, mainTypeID = mainTypeID, customerID = customerID, connectionID = connectionID, periodID = periodID, excludePaid = exculdePaid }, Accounting.Client.AccountingClient.documentConverter);
        }

        public static INTAPS.SubscriberManagment.Subscriber GetSubscriber(string code)
        {
            //return subscriberManagmentServer.GetSubscriber(ApplicationClient.SessionID, code);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.Subscriber>(path + "GetSubscriber", new { sessionID = ApplicationClient.SessionID, code = code });
        }

        public static INTAPS.SubscriberManagment.MeterReaderEmployee GetMeterReaderEmployees(int periodID, int employeeID)
        {
            //return subscriberManagmentServer.GetMeterReaderEmployees(ApplicationClient.SessionID, periodID, employeeID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.MeterReaderEmployee>(path + "GetMeterReaderEmployees", new { sessionID = ApplicationClient.SessionID, periodID = periodID, employeeID = employeeID });
        }

        public static INTAPS.SubscriberManagment.CustomerBillRecord getCustomerBillRecord(int billRecordID)
        {
            //return subscriberManagmentServer.getCustomerBillRecord(ApplicationClient.SessionID, billRecordID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.CustomerBillRecord>(path + "getCustomerBillRecord", new { sessionID = ApplicationClient.SessionID, billRecordID = billRecordID });
        }

        public static INTAPS.SubscriberManagment.PaymentCenter GetPaymentCenter(string userID)
        {
            //return subscriberManagmentServer.GetPaymentCenter(ApplicationClient.SessionID, userID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.PaymentCenter>(path + "GetPaymentCenter", new { sessionID = ApplicationClient.SessionID, userID = userID });
        }

        public static INTAPS.SubscriberManagment.PaymentCenter GetPaymentCenterByCashAccount(int account)
        {
            // return subscriberManagmentServer.GetPaymentCenterByCashAccount(ApplicationClient.SessionID, account);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.PaymentCenter>(path + "GetPaymentCenterByCashAccount", new { sessionID = ApplicationClient.SessionID, account = account });
        }

        public static INTAPS.SubscriberManagment.PaymentCenter GetPaymentCenter(int centerID)
        {
            //return subscriberManagmentServer.GetPaymentCenter(ApplicationClient.SessionID, centerID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.PaymentCenter>(path + "GetPaymentCenter2", new { sessionID = ApplicationClient.SessionID, centerID = centerID });
        }

        public static bool isBillPaid(int invoiceNo)
        {
            //return subscriberManagmentServer.isBillPaid(ApplicationClient.SessionID, invoiceNo);
            return RESTAPIClient.Call<bool>(path + "isBillPaid", new { sessionID = ApplicationClient.SessionID, invoiceNo = invoiceNo });
        }

        public static INTAPS.SubscriberManagment.BillItem[] getCustomerBillItems(int billRecordID)
        {
            //return subscriberManagmentServer.getCustomerBillItems(ApplicationClient.SessionID, billRecordID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.BillItem[]>(path + "getCustomerBillItems", new { sessionID = ApplicationClient.SessionID, billRecordID = billRecordID });
        }

        public static int getLastMessageNumber()
        {
            //return subscriberManagmentServer.getLastMessageNumber(ApplicationClient.SessionID);
            return RESTAPIClient.Call<int>(path + "getLastMessageNumber", new { sessionID = ApplicationClient.SessionID });
        }

        public static int countMessages(int after)
        {
            // return subscriberManagmentServer.countMessages(ApplicationClient.SessionID, after);
            return RESTAPIClient.Call<int>(path + "countMessages", new { sessionID = ApplicationClient.SessionID, after = after });
        }



        public static INTAPS.ClientServer.MessageList getMessageList(int from, int to)
        {
            // return subscriberManagmentServer.getMessageList(ApplicationClient.SessionID, from, to);
            return RESTAPIClient.Call<BinObject>(path + "getMessageList", new { sessionID = ApplicationClient.SessionID, from = from, to = to }).Deserialized() as INTAPS.ClientServer.MessageList;
        }

        public static int getFirstMessageNumber()
        {
            //return subscriberManagmentServer.getFirstMessageNumber(ApplicationClient.SessionID);
            return RESTAPIClient.Call<int>(path + "getFirstMessageNumber", new { sessionID = ApplicationClient.SessionID });
        }

        public static void postOfflineMessages(INTAPS.SubscriberManagment.PaymentCenterMessageList msgList)
        {
            foreach (var m in msgList.list.data)
            {
                m.messageData = new TypeObject(m.messageData);
            }
            //subscriberManagmentServer.postOfflineMessages(ApplicationClient.SessionID, msgList);
            RESTAPIClient.Call<VoidRet>(path + "postOfflineMessages", new { sessionID = ApplicationClient.SessionID, msgList = msgList });
        }
        public static void postOfflineMessages(byte[] data)
        {
            RESTAPIClient.Call<VoidRet>(path + "postOfflineMessagesAsBytes", new { sessionID = ApplicationClient.SessionID, data = data });
        }

        public static void setSubscriptionCoordinate(int subcriptionID, long version, double x, double y)
        {
            //subscriberManagmentServer.setSubscriptionCoordinate(ApplicationClient.SessionID, subcriptionID, version, x, y);
            RESTAPIClient.Call<VoidRet>(path + "setSubscriptionCoordinate", new { sessionID = ApplicationClient.SessionID, subcriptionID = subcriptionID, version = version, x = x, y = y });
        }

        public static INTAPS.SubscriberManagment.MeterMap getMetersNearBy(int readerID, double lat0, double lng0, double radius, long version)
        {
            // return subscriberManagmentServer.getMetersNearBy(ApplicationClient.SessionID, readerID, lat0, lng0, radius, version);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.MeterMap>(path + "getMetersNearBy", new { sessionID = ApplicationClient.SessionID, readerID = readerID, lat0 = lat0, lng0 = lng0, radius = radius, version = version });
        }

        public static INTAPS.SubscriberManagment.MeterMap getAllMetersMap(long version)
        {
            //return subscriberManagmentServer.getAllMetersMap(ApplicationClient.SessionID, version);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.MeterMap>(path + "getAllMetersMap", new { sessionID = ApplicationClient.SessionID, version = version });
        }
        public static INTAPS.SubscriberManagment.MeterMap getUnreadMetersMap(long version)
        {
            //return subscriberManagmentServer.getUnreadMetersMap(ApplicationClient.SessionID, version);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.MeterMap>(path + "getUnreadMetersMap", new { sessionID = ApplicationClient.SessionID, version = version });
        }
        public static string builWebMapString(MeterMap map)
        {
            string ret = null;
            int nItem = 0;
            foreach (MeterMarker m in map.markers)
            {
                string thisMarker = m.lat + "," + m.lng + "," + m.connectionCode.Replace(',', '_') + "," + (int)m.symbol;
                //if (nItem++ == 1000)
                //    break;
                if (ret == null)
                    ret = thisMarker;
                else
                    ret = ret + "," + thisMarker;
            }
            return ret;
        }

        public static void getMapCenter(MeterMarker[] markers, out double lat, out double lng)
        {
            lat = 0;
            lng = 0;
            int n = 0;
            foreach (MeterMarker m in markers)
            {
                lat += m.lat;
                lng += m.lng;
            }
            if (n > 0)
            {
                lat = lat / n;
                lng = lng / n;
            }
        }
        public static void getMapBound(MeterMarker[] markers, out double lat1, out double lng1, out double lat2, out double lng2)
        {
            lat1 = 0;
            lng1 = 0;
            lat2 = 0;
            lng2 = 0;
            bool first = true;
            foreach (MeterMarker m in markers)
            {
                if (first)
                {
                    lat1 = lat2 = m.lat;
                    lng1 = lng2 = m.lng;
                    first = false;
                }
                else
                {
                    if (m.lat < lat1)
                        lat1 = m.lat;
                    else if (m.lat > lat2)
                        lat2 = m.lat;
                    if (m.lng < lng1)
                        lng1 = m.lng;
                    else if (m.lng > lng2)
                        lng2 = m.lng;
                }
            }

        }
        public static string webServerPort
        {
            get
            {
                string config = System.Configuration.ConfigurationManager.AppSettings["webServer"];
                if (string.IsNullOrEmpty(config))
                    return ApplicationClient.Server.Substring(ApplicationClient.Server.IndexOf("//") + 2, ApplicationClient.Server.IndexOf(":", ApplicationClient.Server.IndexOf("//")) - (ApplicationClient.Server.IndexOf("//") + 2));
                return config;
            }
        }
        public static void showAllCustomersMap()
        {
            string serverName = webServerPort;
            string url = string.Format("{0}/subsc/metermap/meters?sid={1}&maptype=allmeters", serverName, ApplicationClient.SessionID);
            System.Diagnostics.Process.Start(url);
        }
        public static void showUnreadCustomersMap()
        {
            string serverName = webServerPort;
            string url = string.Format("{0}/subsc/metermap/meters?sid={1}&maptype=unread", serverName, ApplicationClient.SessionID);
            System.Diagnostics.Process.Start(url);
        }

        public static void showReadingMap(int periodID)
        {
            string serverName = webServerPort;
            string url = string.Format("{0}/subsc/metermap/meters?sid={1}&maptype=reading&prd={2}", serverName, ApplicationClient.SessionID, periodID);
            System.Diagnostics.Process.Start(url);
        }

        public static void showMineOthers(int periodID, int readerID)
        {
            string serverName = webServerPort;
            string url = string.Format("{0}/subsc/metermap/meters?sid={1}&maptype=allocation&prd={2}&reader={3}", serverName, ApplicationClient.SessionID, periodID, readerID);
            System.Diagnostics.Process.Start(url);
        }
        public static void showReadingPath(int periodID, int readerID)
        {
            string serverName = webServerPort;
            string url = string.Format("{0}/subsc/metermap/meters?sid={1}&maptype=reading_path&prd={2}&reader={3}", serverName, ApplicationClient.SessionID, periodID, readerID);
            System.Diagnostics.Process.Start(url);
        }
        public static void showReadingPath(int periodID, int readerID, DateTime date)
        {
            string serverName = webServerPort;
            string url = string.Format("{0}/subsc/metermap/meters?sid={1}&maptype=reading_path&prd={2}&reader={3}&date={4}", serverName, ApplicationClient.SessionID, periodID, readerID, System.Web.HttpUtility.HtmlEncode(date.ToString()));
            System.Diagnostics.Process.Start(url);
        }
        public static void locateWaterMeter(int connectionID, bool reading)
        {
            string serverName = webServerPort;
            string url = string.Format("{0}/subsc/metermap/meters?sid={1}&maptype={3}&con={2}", serverName, ApplicationClient.SessionID, connectionID
                , reading ? "locate_reading" : "locate"
                );
            System.Diagnostics.Process.Start(url);
        }
        public static void showMeterReadingCluster(int connectionID)
        {
            string serverName = webServerPort;
            string url = string.Format("{0}/subsc/metermap/meters?sid={1}&maptype=locate_reading&con={2}", serverName, ApplicationClient.SessionID, connectionID);
            System.Diagnostics.Process.Start(url);
        }
        public static INTAPS.SubscriberManagment.MeterMap getReadingMap(int periodID)
        {
            // return subscriberManagmentServer.getReadingMap(ApplicationClient.SessionID, periodID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.MeterMap>(path + "getReadingMap", new { sessionID = ApplicationClient.SessionID, periodID = periodID });
        }

        public static INTAPS.SubscriberManagment.MeterMap getMineOthers(int periodID, int readerID)
        {
            // return subscriberManagmentServer.getMineOthers(ApplicationClient.SessionID, periodID, readerID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.MeterMap>(path + "getMineOthers", new { sessionID = ApplicationClient.SessionID, periodID = periodID, readerID = readerID });
        }

        public static INTAPS.SubscriberManagment.MeterMap locateWaterMeter(int connectionID, long version)
        {
            //return subscriberManagmentServer.locateWaterMeter(ApplicationClient.SessionID, connectionID, version);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.MeterMap>(path + "locateWaterMeter", new { sessionID = ApplicationClient.SessionID, connectionID = connectionID, version = version });
        }

        public static INTAPS.SubscriberManagment.MeterMap disconnectCandidate(int readingPeriodID)
        {
            // return subscriberManagmentServer.disconnectCandidate(ApplicationClient.SessionID, readingPeriodID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.MeterMap>(path + "disconnectCandidate", new { sessionID = ApplicationClient.SessionID, readingPeriodID = readingPeriodID });
        }

        public static INTAPS.SubscriberManagment.BWFMeterReading getAverageReading(int subscriptionID, int periodID, int nmonths)
        {
            // return subscriberManagmentServer.getAverageReading(ApplicationClient.SessionID, subscriptionID, periodID, nmonths);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.BWFMeterReading>(path + "getAverageReading", new { sessionID = ApplicationClient.SessionID, subscriptionID = subscriptionID, periodID = periodID, nmonths = nmonths });
        }

        public static INTAPS.Payroll.Employee GetPaymentCenterEmployeeByCashAccountID(int cashAccountID)
        {
            PaymentCenter pc = GetPaymentCenterByCashAccount(cashAccountID);
            if (pc == null)
                return null;
            return INTAPS.Payroll.Client.PayrollClient.GetEmployee(pc.casheir);
        }

        public static INTAPS.SubscriberManagment.MeterReaderEmployee GetMeterReadingEmployee(string loginName)
        {
            // return subscriberManagmentServer.GetMeterReadingEmployee(ApplicationClient.SessionID, loginName);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.MeterReaderEmployee>(path + "GetMeterReadingEmployee", new { sessionID = ApplicationClient.SessionID, loginName = loginName });
        }

        public static INTAPS.SubscriberManagment.ReadingCycle getReadingCycle(int periodID)
        {
            //return subscriberManagmentServer.getReadingCycle(ApplicationClient.SessionID, periodID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.ReadingCycle>(path + "getReadingCycle", new { sessionID = ApplicationClient.SessionID, periodID = periodID });
        }

        public static INTAPS.SubscriberManagment.ReadingCycle getCurrentReadingCycle()
        {
            //  return subscriberManagmentServer.getCurrentReadingCycle(ApplicationClient.SessionID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.ReadingCycle>(path + "getCurrentReadingCycle", new { sessionID = ApplicationClient.SessionID });
        }

        public static void openReadingCycle(int periodID)
        {
            // subscriberManagmentServer.openReadingCycle(ApplicationClient.SessionID, periodID);
            RESTAPIClient.Call<VoidRet>(path + "openReadingCycle", new { sessionID = ApplicationClient.SessionID, periodID = periodID });
        }

        public static void deleteReadingCycle(int periodID)
        {
            //subscriberManagmentServer.deleteReadingCycle(ApplicationClient.SessionID, periodID);
            RESTAPIClient.Call<VoidRet>(path + "deleteReadingCycle", new { sessionID = ApplicationClient.SessionID, periodID = periodID });
        }

        public static INTAPS.SubscriberManagment.ReadingSheetRow[] getReadingSheet(int periodID)
        {
            //return subscriberManagmentServer.getReadingSheet(ApplicationClient.SessionID, periodID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.ReadingSheetRow[]>(path + "getReadingSheet", new { sessionID = ApplicationClient.SessionID, periodID = periodID });
        }

        public static void closeReadingCycle(int periodID)
        {
            //subscriberManagmentServer.closeReadingCycle(ApplicationClient.SessionID, periodID);
            RESTAPIClient.Call<VoidRet>(path + "closeReadingCycle", new { sessionID = ApplicationClient.SessionID, periodID = periodID });
        }

        public static INTAPS.SubscriberManagment.ReadingSheetRow getReadingShowRow(int periodID, int rowIndex)
        {
            //return subscriberManagmentServer.getReadingShowRow(ApplicationClient.SessionID, periodID, rowIndex);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.ReadingSheetRow>(path + "getReadingShowRow", new { sessionID = ApplicationClient.SessionID, periodID = periodID, rowIndex = rowIndex });
        }

        public static int readSheetSize(int periodID)
        {
            //return subscriberManagmentServer.readSheetSize(ApplicationClient.SessionID, periodID);
            return RESTAPIClient.Call<int>(path + "readSheetSize", new { sessionID = ApplicationClient.SessionID, periodID = periodID });
        }

        public static void setReadingSheetRow(INTAPS.SubscriberManagment.ReadingSheetRow row)
        {
            //subscriberManagmentServer.setReadingSheetRow(ApplicationClient.SessionID, row);
            RESTAPIClient.Call<VoidRet>(path + "setReadingSheetRow", new { sessionID = ApplicationClient.SessionID, row = row });
        }
        public class GetReadingPath2Out
        {
            public int notMapped;
            public INTAPS.SubscriberManagment.ReadingPath _ret;
        }
        public static INTAPS.SubscriberManagment.ReadingPath getReadingPath(int periodID, int readerID, out int notMapped)
        {
            //return subscriberManagmentServer.getReadingPath(ApplicationClient.SessionID, periodID, readerID, out notMapped);
            var _ret = RESTAPIClient.Call<GetReadingPath2Out>(path + "getReadingPath2", new { sessionID = ApplicationClient.SessionID, periodID = periodID, readerID = readerID });
            notMapped = _ret.notMapped;
            return _ret._ret;
        }

        public static INTAPS.SubscriberManagment.CustomerPaymentReceipt[] searchCustomerReceipts(string query, INTAPS.SubscriberManagment.SubscriberSearchField field, int casherAccountID, int resultSize)
        {
            //return subscriberManagmentServer.searchCustomerReceipts(ApplicationClient.SessionID, query, field, casherAccountID, resultSize);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.CustomerPaymentReceipt[]>(path + "searchCustomerReceipts", new { sessionID = ApplicationClient.SessionID, query = query, field = field, casherAccountID = casherAccountID, resultSize = resultSize });
        }

        public static INTAPS.SubscriberManagment.MeterReaderEmployee getMeterReadingEmployee()
        {
            //return subscriberManagmentServer.getMeterReadingEmployee(ApplicationClient.SessionID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.MeterReaderEmployee>(path + "getMeterReadingEmployee2", new { sessionID = ApplicationClient.SessionID });
        }

        public static void generateOutstandingBills(int customerID)
        {
            // subscriberManagmentServer.generateOutstandingBills(ApplicationClient.SessionID, customerID);
            RESTAPIClient.Call<VoidRet>(path + "generateOutstandingBills", new { sessionID = ApplicationClient.SessionID, customerID = customerID });
        }

        public static INTAPS.SubscriberManagment.BillingRuleSettingType[] getSettingTypes()
        {
            //return subscriberManagmentServer.getSettingTypes(ApplicationClient.SessionID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.BillingRuleSettingType[]>(path + "getSettingTypes", new { sessionID = ApplicationClient.SessionID });
        }

        public static void setBillingRuleSetting(System.Type t, object setting)
        {
            //subscriberManagmentServer.setBillingRuleSetting(ApplicationClient.SessionID, t, setting);
            RESTAPIClient.Call<VoidRet>(path + "setBillingRuleSetting", new { sessionID = ApplicationClient.SessionID, t = t, setting = new BinObject(setting) });
        }

        public static object getBillingRuleSetting(System.Type t)
        {
            //  return subscriberManagmentServer.getBillingRuleSetting(ApplicationClient.SessionID, t);
            return RESTAPIClient.Call<object>(path + "getBillingRuleSetting", new { sessionID = ApplicationClient.SessionID, t = t });
        }

        public static INTAPS.SubscriberManagment.CustomerSubtype[] getCustomerSubtypes(INTAPS.SubscriberManagment.SubscriberType type)
        {
            //  return subscriberManagmentServer.getCustomerSubtypes(ApplicationClient.SessionID, type);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.CustomerSubtype[]>(path + "getCustomerSubtypes", new { sessionID = ApplicationClient.SessionID, type = type });
        }

        public static INTAPS.SubscriberManagment.CustomerSubtype getCustomerSubtype(INTAPS.SubscriberManagment.SubscriberType type, int subTypeID)
        {
            // return subscriberManagmentServer.getCustomerSubtype(ApplicationClient.SessionID, type, subTypeID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.CustomerSubtype>(path + "getCustomerSubtype", new { sessionID = ApplicationClient.SessionID, type = type, subTypeID = subTypeID });
        }

        public static INTAPS.SubscriberManagment.PrePaymentBill PreviewPrepaidReceipt(System.DateTime date, int subscriptionID, double cubicMeter)
        {
            // return subscriberManagmentServer.PreviewPrepaidReceipt(ApplicationClient.SessionID, date, subscriptionID, cubicMeter);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.PrePaymentBill>(path + "PreviewPrepaidReceipt", new { sessionID = ApplicationClient.SessionID, date = date, subscriptionID = subscriptionID, cubicMeter = cubicMeter });
        }
        public class GeneratePrepaidReceiptOut
        {
            public PrePaymentBill bill;
            public INTAPS.SubscriberManagment.CustomerPaymentReceipt _ret;
        }
        public static INTAPS.SubscriberManagment.CustomerPaymentReceipt GeneratePrepaidReceipt(System.DateTime date, int subscriptionID, double cubicMeter, BIZNET.iERP.PaymentInstrumentItem[] paymentInstruments, out PrePaymentBill bill)
        {
            //return subscriberManagmentServer.GeneratePrepaidReceipt(ApplicationClient.SessionID, date, subscriptionID, cubicMeter, paymentInstruments, out bill);
            var _ret = RESTAPIClient.Call<GeneratePrepaidReceiptOut>(path + "GeneratePrepaidReceipt", new { sessionID = ApplicationClient.SessionID, date = date, subscriptionID = subscriptionID, cubicMeter = cubicMeter, paymentInstruments = paymentInstruments });
            bill = _ret.bill;
            return _ret._ret;
        }
        public static string getCustomerAddressString(Subscriber customer)
        {
            string ret = GetKebele(customer.Kebele).name;
            if (!string.IsNullOrEmpty(customer.address))
                ret = INTAPS.StringExtensions.AppendOperand(ret, ", ", "House No: " + customer.address);
            if (!string.IsNullOrEmpty(customer.nameOfPlace))
                ret = INTAPS.StringExtensions.AppendOperand(ret, ", ", "Name of place : " + customer.nameOfPlace);
            return ret;
        }

        public static void updateBillingCycle(INTAPS.SubscriberManagment.BillingCycle cycle)
        {
            //subscriberManagmentServer.updateBillingCycle(ApplicationClient.SessionID, cycle);
            RESTAPIClient.Call<VoidRet>(path + "updateBillingCycle", new { sessionID = ApplicationClient.SessionID, cycle = cycle });
        }

        public static INTAPS.SubscriberManagment.BillingCycle[] getAllBillingCycles()
        {
            //return subscriberManagmentServer.getAllBillingCycles(ApplicationClient.SessionID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.BillingCycle[]>(path + "getAllBillingCycles", new { sessionID = ApplicationClient.SessionID });
        }

        public static void createBillingCycle(INTAPS.SubscriberManagment.BillingCycle cycle)
        {
            //subscriberManagmentServer.createBillingCycle(ApplicationClient.SessionID, cycle);
            RESTAPIClient.Call<VoidRet>(path + "createBillingCycle", new { sessionID = ApplicationClient.SessionID, cycle = cycle });
        }

        public static INTAPS.SubscriberManagment.BillingCycle getCurrentBillingCycle()
        {
            //return subscriberManagmentServer.getCurrentBillingCycle(ApplicationClient.SessionID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.BillingCycle>(path + "getCurrentBillingCycle", new { sessionID = ApplicationClient.SessionID });
        }

        public static INTAPS.SubscriberManagment.BillingCycle getBillingCycle(int periodID)
        {
            //return subscriberManagmentServer.getBillingCycle(ApplicationClient.SessionID, periodID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.BillingCycle>(path + "getBillingCycle", new { sessionID = ApplicationClient.SessionID, periodID = periodID });
        }

        public static void deleteBillingCycle(int periodID)
        {
            // subscriberManagmentServer.deleteBillingCycle(ApplicationClient.SessionID, periodID);
            RESTAPIClient.Call<VoidRet>(path + "deleteBillingCycle", new { sessionID = ApplicationClient.SessionID, periodID = periodID });
        }

        public static INTAPS.SubscriberManagment.MeterMap singleMeterHistoricalReadingMap(int connectionID)
        {
            // return subscriberManagmentServer.singleMeterHistoricalReadingMap(ApplicationClient.SessionID, connectionID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.MeterMap>(path + "singleMeterHistoricalReadingMap", new { sessionID = ApplicationClient.SessionID, connectionID = connectionID });
        }
        public class GetReadingPathOut
        {
            public int notMapped;
            public INTAPS.SubscriberManagment.ReadingPath _ret;
        }
        public static INTAPS.SubscriberManagment.ReadingPath getReadingPath(int periodID, int readerID, System.DateTime d1, System.DateTime d2, out int notMapped)
        {
            // return subscriberManagmentServer.getReadingPath(ApplicationClient.SessionID, periodID, readerID, d1, d2, out notMapped);
            var _ret = RESTAPIClient.Call<GetReadingPathOut>(path + "getReadingPath", new { sessionID = ApplicationClient.SessionID, periodID = periodID, readerID = readerID, d1 = d1, d2 = d2 });
            notMapped = _ret.notMapped;
            return _ret._ret;
        }

        public static System.DateTime[] getReadingDates(int periodID, int readerID)
        {
            //  return subscriberManagmentServer.getReadingDates(ApplicationClient.SessionID, periodID, readerID);
            return RESTAPIClient.Call<System.DateTime[]>(path + "getReadingDates", new { sessionID = ApplicationClient.SessionID, periodID = periodID, readerID = readerID });
        }

        public static void analyzeReading(int periodID, bool updateConnectionLocations)
        {
            // subscriberManagmentServer.analyzeReading(ApplicationClient.SessionID, periodID, updateConnectionLocations);
            RESTAPIClient.Call<VoidRet>(path + "analyzeReading", new { sessionID = ApplicationClient.SessionID, periodID = periodID, updateConnectionLocations = updateConnectionLocations });
        }

        public static INTAPS.SubscriberManagment.CreditScheme getCreditScheme(int schemeID)
        {
            //return subscriberManagmentServer.getCreditScheme(ApplicationClient.SessionID, schemeID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.CreditScheme>(path + "getCreditScheme", new { sessionID = ApplicationClient.SessionID, schemeID = schemeID });
        }

        public static INTAPS.SubscriberManagment.CreditScheme[] getCustomerCreditSchemes(int customerID)
        {
            //return subscriberManagmentServer.getCustomerCreditSchemes(ApplicationClient.SessionID, customerID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.CreditScheme[]>(path + "getCustomerCreditSchemes2", new { sessionID = ApplicationClient.SessionID, customerID = customerID });
        }

        public static int addNoneBillCrediScheme(INTAPS.SubscriberManagment.CreditScheme scheme)
        {
            // return subscriberManagmentServer.addNoneBillCrediScheme(ApplicationClient.SessionID, scheme);
            return RESTAPIClient.Call<int>(path + "addNoneBillCrediScheme", new { sessionID = ApplicationClient.SessionID, scheme = scheme });
        }

        public static void deleteCreditScheme(int schemeID, bool deleteBills)
        {
            //subscriberManagmentServer.deleteCreditScheme(ApplicationClient.SessionID, schemeID, deleteBills);
            RESTAPIClient.Call<VoidRet>(path + "deleteCreditScheme", new { sessionID = ApplicationClient.SessionID, schemeID = schemeID, deleteBills = deleteBills });
        }
        public class GetCustomerCreditSchemesOut
        {
            public double[] settlement;
            public INTAPS.SubscriberManagment.CreditScheme[] _ret;
        }
        public static INTAPS.SubscriberManagment.CreditScheme[] getCustomerCreditSchemes(int customerID, out double[] settlement)
        {
            // return subscriberManagmentServer.getCustomerCreditSchemes(ApplicationClient.SessionID, customerID, out settlement);
            var _ret = RESTAPIClient.Call<GetCustomerCreditSchemesOut>(path + "getCustomerCreditSchemes", new { sessionID = ApplicationClient.SessionID, customerID = customerID });
            settlement = _ret.settlement;
            return _ret._ret;
        }

        public static void transferCustomerData(int wsisPeriodID)
        {
            //subscriberManagmentServer.transferCustomerData(ApplicationClient.SessionID, wsisPeriodID);
            RESTAPIClient.Call<VoidRet>(path + "transferCustomerData", new { sessionID = ApplicationClient.SessionID, wsisPeriodID = wsisPeriodID });
        }
        public static void UploadReading(int wsisPeriodID)
        {
            //subscriberManagmentServer.UploadReading(ApplicationClient.SessionID, wsisPeriodID);
            RESTAPIClient.Call<VoidRet>(path + "UploadReading", new { sessionID = ApplicationClient.SessionID, wsisPeriodID = wsisPeriodID });
        }
        public static void registerMobileReader(Employee emp, string password, int periodID)
        {
            //subscriberManagmentServer.registerMobileReader(ApplicationClient.SessionID, emp, password, periodID);
            RESTAPIClient.Call<VoidRet>(path + "registerMobileReader", new { sessionID = ApplicationClient.SessionID, emp = emp, password = password, periodID = periodID });
        }
        public static void changePassword(string userName, string password)
        {
            //subscriberManagmentServer.changePassword(ApplicationClient.SessionID, userName, password);
            RESTAPIClient.Call<VoidRet>(path + "changePassword", new { sessionID = ApplicationClient.SessionID, userName = userName, password = password });
        }
        public static DataTable getReadingStatistics(int periodID)
        {
            // return subscriberManagmentServer.getReadingStatistics(ApplicationClient.SessionID, periodID);
            return RESTAPIClient.Call<DataTable>(path + "getReadingStatistics", new { sessionID = ApplicationClient.SessionID, periodID = periodID });
        }
        public static int getReaderCount(int periodID, int readerID)
        {
            //return subscriberManagmentServer.getReaderCount(ApplicationClient.SessionID, periodID, readerID);
            return RESTAPIClient.Call<int>(path + "getReaderCount", new { sessionID = ApplicationClient.SessionID, periodID = periodID, readerID = readerID });
        }
        public static ReadingCycle getReadingCycle(int periodID, int status)
        {
            //return subscriberManagmentServer.getReadingCycle(ApplicationClient.SessionID, periodID, status);
            return RESTAPIClient.Call<ReadingCycle>(path + "getReadingCycle2", new { sessionID = ApplicationClient.SessionID, periodID = periodID, status = status });
        }
        public class BWFGetDescribedReadings2Out
        {
            public int NRecords;
            public BWFDescribedMeterReading[] _ret;
        }
        public static BWFDescribedMeterReading[] BWFGetDescribedReadings(int blockID, int periodID, string contractNo, string name, int kebele, int customerType, int index, int pageSize, out int NRecords)
        {
            //return subscriberManagmentServer.BWFGetDescribedReadings(ApplicationClient.SessionID, blockID, periodID, contractNo, name, kebele, customerType, index, pageSize, out NRecords);
            var _ret = RESTAPIClient.Call<BWFGetDescribedReadings2Out>(path + "BWFGetDescribedReadings2", new { sessionID = ApplicationClient.SessionID, blockID = blockID, periodID = periodID, contractNo = contractNo, name = name, kebele = kebele, customerType = customerType, index = index, pageSize = pageSize });
            NRecords = _ret.NRecords;
            return _ret._ret;
        }
        public static void transferSubscriptionsToReader(int[] subscriptions, int readerID, int periodID)
        {
            //subscriberManagmentServer.transferSubscriptionsToReader(ApplicationClient.SessionID, subscriptions, readerID, periodID);
            RESTAPIClient.Call<VoidRet>(path + "transferSubscriptionsToReader", new { sessionID = ApplicationClient.SessionID, subscriptions = subscriptions, readerID = readerID, periodID = periodID });
        }

        public static int registerProductionPoint(INTAPS.SubscriberManagment.ProductionPoint productionPoint)
        {
            // return subscriberManagmentServer.registerProductionPoint(ApplicationClient.SessionID, productionPoint);
            return RESTAPIClient.Call<int>(path + "registerProductionPoint", new { sessionID = ApplicationClient.SessionID, productionPoint = productionPoint });
        }
        public class GetProductionPoint2Out
        {
            public int NRecords;
            public INTAPS.SubscriberManagment.ProductionPoint[] _ret;
        }
        public static INTAPS.SubscriberManagment.ProductionPoint[] getProductionPoint(string name, int kebele, int index, int type, int pageSize, out int NRecords)
        {
            // return subscriberManagmentServer.getProductionPoint(ApplicationClient.SessionID, name, kebele, index, type, pageSize, out NRecords);
            var _ret = RESTAPIClient.Call<GetProductionPoint2Out>(path + "getProductionPoint2", new { sessionID = ApplicationClient.SessionID, name = name, kebele = kebele, index = index, type = type, pageSize = pageSize });
            NRecords = _ret.NRecords;
            return _ret._ret;
        }

        public static INTAPS.SubscriberManagment.ProductionPoint getProductionPoint(int id)
        {
            // return subscriberManagmentServer.getProductionPoint(ApplicationClient.SessionID, id);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.ProductionPoint>(path + "getProductionPoint", new { sessionID = ApplicationClient.SessionID, id = id });
        }

        public static int setProductionPointReading(System.DateTime readingDate, double reading, MeterReadingType readingType, int productionPointID)
        {
            // return subscriberManagmentServer.setProductionPointReading(ApplicationClient.SessionID, readingDate, reading, readingType, productionPointID);
            return RESTAPIClient.Call<int>(path + "setProductionPointReading", new { sessionID = ApplicationClient.SessionID, readingDate = readingDate, reading = reading, readingType = readingType, productionPointID = productionPointID });
        }


        public static void updateProductionPointReading(ProductionPointReading reading, bool reset)
        {
            //subscriberManagmentServer.updateProductionPointReading(ApplicationClient.SessionID, reading, reset);
            RESTAPIClient.Call<VoidRet>(path + "updateProductionPointReading", new { sessionID = ApplicationClient.SessionID, reading = reading, reset = reset });
        }
        public class HtmlDocGetCustomerBillDataOut
        {
            public string headers;
            public string _ret;
        }
        public static string htmlDocGetCustomerBillData(int customerID, int connectionID, out string headers)
        {
            //return subscriberManagmentServer.htmlDocGetCustomerBillData(ApplicationClient.SessionID, customerID, connectionID, out headers);
            var _ret = RESTAPIClient.Call<HtmlDocGetCustomerBillDataOut>(path + "htmlDocGetCustomerBillData", new { sessionID = ApplicationClient.SessionID, customerID = customerID, connectionID = connectionID });
            headers = _ret.headers;
            return _ret._ret;
        }
        public class HtmlDocGetCustomerListOut
        {
            public string headers;
            public string _ret;
        }
        public static string htmlDocGetCustomerList(SubscriberSearchResult[] result, out string headers)
        {
            // return subscriberManagmentServer.htmlDocGetCustomerList(ApplicationClient.SessionID, result, out headers);
            var _ret = RESTAPIClient.Call<HtmlDocGetCustomerListOut>(path + "htmlDocGetCustomerList", new { sessionID = ApplicationClient.SessionID, result = result });
            headers = _ret.headers;
            return _ret._ret;
        }
        public class HtmlDocGetReadingsOut
        {
            public string headers;
            public string _ret;
        }
        public static string htmlDocGetReadings(int kebele, int subscriptionID, int periodID, int employeeID, out string headers)
        {
            //return subscriberManagmentServer.htmlDocGetReadings(ApplicationClient.SessionID, kebele, subscriptionID, periodID, employeeID, out headers);
            var _ret = RESTAPIClient.Call<HtmlDocGetReadingsOut>(path + "htmlDocGetReadings", new { sessionID = ApplicationClient.SessionID, kebele = kebele, subscriptionID = subscriptionID, periodID = periodID, employeeID = employeeID });
            headers = _ret.headers;
            return _ret._ret;
        }

        public static string[] getReadingCustomFields()
        {
            // return subscriberManagmentServer.getReadingCustomFields(ApplicationClient.SessionID);
            return RESTAPIClient.Call<string[]>(path + "getReadingCustomFields", new { sessionID = ApplicationClient.SessionID });
        }

        public static string getSMSPreviewHTML(INTAPS.SubscriberManagment.CustomerSMSSendParameter send)
        {
            //return subscriberManagmentServer.getSMSPreviewHTML(ApplicationClient.SessionID, send);
            return RESTAPIClient.Call<string>(path + "getSMSPreviewHTML", new { sessionID = ApplicationClient.SessionID, send = send });
        }

        public static string getSMSInterfaceStatus()
        {
            // return subscriberManagmentServer.getSMSInterfaceStatus(ApplicationClient.SessionID);
            return RESTAPIClient.Call<string>(path + "getSMSInterfaceStatus", new { sessionID = ApplicationClient.SessionID });
        }

        public static void sendSMS(INTAPS.SubscriberManagment.CustomerSMSSendParameter send)
        {
            //subscriberManagmentServer.sendSMS(ApplicationClient.SessionID, send);
            RESTAPIClient.Call<VoidRet>(path + "sendSMS", new { sessionID = ApplicationClient.SessionID, send = send });
        }
        public class BWFGetMeterReadings2Out
        {
            public int NRecords;
            public INTAPS.SubscriberManagment.BWFMeterReading[] _ret;
        }
        public static INTAPS.SubscriberManagment.BWFMeterReading[] BWFGetMeterReadings(INTAPS.SubscriberManagment.MeterReadingFilter filter, int index, int pageSize, out int NRecords)
        {
            // return subscriberManagmentServer.BWFGetMeterReadings(ApplicationClient.SessionID, filter, index, pageSize, out NRecords);
            var _ret = RESTAPIClient.Call<BWFGetMeterReadings2Out>(path + "BWFGetMeterReadings2", new { sessionID = ApplicationClient.SessionID, filter = filter, index = index, pageSize = pageSize });
            NRecords = _ret.NRecords;
            return _ret._ret;
        }

        public static void BWFAcceptRejectReading(bool accept, int periodID, INTAPS.SubscriberManagment.MeterReadingFilter filter, int[] specific)
        {
            //subscriberManagmentServer.BWFAcceptRejectReading(ApplicationClient.SessionID, accept, periodID, filter, specific);
            RESTAPIClient.Call<VoidRet>(path + "BWFAcceptRejectReading", new { sessionID = ApplicationClient.SessionID, accept = accept, periodID = periodID, filter = filter, specific = specific });
        }

        public static void changeSMSPassCode(string oldPassCode, string newPassCode)
        {
            // subscriberManagmentServer.changeSMSPassCode(ApplicationClient.SessionID, oldPassCode, newPassCode);
            RESTAPIClient.Call<VoidRet>(path + "changeSMSPassCode", new { sessionID = ApplicationClient.SessionID, oldPassCode = oldPassCode, newPassCode = newPassCode });
        }

        public static INTAPS.SubscriberManagment.SMSLicenseInformation getSMSLicense()
        {
            //  return subscriberManagmentServer.getSMSLicense(ApplicationClient.SessionID);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.SMSLicenseInformation>(path + "getSMSLicense", new { sessionID = ApplicationClient.SessionID });
        }

        public static DistrictMeteringZone[] getAllDMAs()
        {
            // return subscriberManagmentServer.getAllDMAs(ApplicationClient.SessionID);
            return RESTAPIClient.Call<DistrictMeteringZone[]>(path + "getAllDMAs", new { sessionID = ApplicationClient.SessionID });
        }

        public static PressureZone[] getAllPressureZones()
        {
            //return subscriberManagmentServer.getAllPressureZones(ApplicationClient.SessionID);
            return RESTAPIClient.Call<PressureZone[]>(path + "getAllPressureZones", new { sessionID = ApplicationClient.SessionID });
        }

        public static DistrictMeteringZone getDMA(int dma)
        {
            // return subscriberManagmentServer.getDMA(ApplicationClient.SessionID, dma);
            return RESTAPIClient.Call<DistrictMeteringZone>(path + "getDMA", new { sessionID = ApplicationClient.SessionID, id = dma });
        }

        public static PressureZone getPressureZone(int zone)
        {
            // return subscriberManagmentServer.getPressureZone(ApplicationClient.SessionID, zone);
            return RESTAPIClient.Call<PressureZone>(path + "getPressureZone", new { sessionID = ApplicationClient.SessionID, id = zone });
        }

        public static INTAPS.SubscriberManagment.CustomerSMS[] getSMSLogByPhoneNo(int sendID, int receivedID, string phoneNo)
        {
            //  return subscriberManagmentServer.getSMSLogByPhoneNo(ApplicationClient.SessionID, sendID, receivedID, phoneNo);
            return RESTAPIClient.Call<INTAPS.SubscriberManagment.CustomerSMS[]>(path + "getSMSLogByPhoneNo", new { sessionID = ApplicationClient.SessionID, sendID = sendID, receivedID = receivedID, phoneNo = phoneNo });
        }
        public static void mergeCustomers(int sid1, int sid2)
        {
            //subscriberManagmentServer.mergeCustomers(ApplicationClient.SessionID, sid1, sid2);
            RESTAPIClient.Call<VoidRet>(path + "mergeCustomers", new { sessionID = ApplicationClient.SessionID, sid1 = sid1, sid2 = sid2 });
        }

        internal static SystemFeeDeposit[] getDeposits(int periodID, int readerID, DateTime from, DateTime to)
        {
            //return subscriberManagmentServer.getDeposits(ApplicationClient.SessionID, periodID, readerID, from, to);
            return RESTAPIClient.Call<SystemFeeDeposit[]>(path + "getDeposits", new { sessionID = ApplicationClient.SessionID, periodID = periodID, readerID = readerID, from = from, to = to });
        }

        internal static SystemFeeDeposit getDeposit(int periodID, int readerID, DateTime day)
        {
            // return subscriberManagmentServer.getDeposit(ApplicationClient.SessionID, periodID, readerID, day);
            return RESTAPIClient.Call<SystemFeeDeposit>(path + "getDeposit", new { sessionID = ApplicationClient.SessionID, periodID = periodID, readerID = readerID, day = day });
        }
        public static void addSystemFeeDeposit(SystemFeeDeposit deposit)
        {
            // subscriberManagmentServer.addSystemFeeDeposit(ApplicationClient.SessionID, deposit);
            RESTAPIClient.Call<VoidRet>(path + "addSystemFeeDeposit", new { sessionID = ApplicationClient.SessionID, deposit = deposit });
        }
        public static void updateSystemFeeDeposit(SystemFeeDeposit deposit)
        {
            // subscriberManagmentServer.updateSystemFeeDeposit(ApplicationClient.SessionID, deposit);
            RESTAPIClient.Call<VoidRet>(path + "updateSystemFeeDeposit", new { sessionID = ApplicationClient.SessionID, deposit = deposit });
        }
        public static void deleteSystemFeeDeposit(int periodID, int readerID, DateTime returnDate)
        {
            // subscriberManagmentServer.deleteSystemFeeDeposit(ApplicationClient.SessionID, periodID, readerID, returnDate);
            RESTAPIClient.Call<VoidRet>(path + "deleteSystemFeeDeposit", new { sessionID = ApplicationClient.SessionID, periodID = periodID, readerID = readerID, returnDate = returnDate });
        }
        public static double getSystemDepositFeeBalance(int readerID, DateTime date)
        {
            //return subscriberManagmentServer.getSystemDepositFeeBalance(ApplicationClient.SessionID, readerID, date);
            return RESTAPIClient.Call<double>(path + "getSystemDepositFeeBalance", new { sessionID = ApplicationClient.SessionID, readerID = readerID, date = date });
        }
        public static void startBillGenerationByKebele(System.DateTime time, int periodID, int[] kebeles)
        {
            RESTAPIClient.Call<VoidRet>(path + "startBillGenerationByKebele",
                new { sessionID = ApplicationClient.SessionID, time = time, periodID = periodID, kebeles = kebeles });
        }
        public static bool IsBillGenerating()
        {
            return RESTAPIClient.Call<bool>(path + "IsBillGenerating", new { SessionID = ApplicationClient.SessionID });
        }
        public static BatchJobResult[] getBillGenerationResult()
        {
            return RESTAPIClient.Call<BatchJobResult[]>(path + "getBillGenerationResult", new { SessionID = ApplicationClient.SessionID });

        }
    }
}

