namespace INTAPS.SubscriberManagment.Client
{
    using INTAPS.SubscriberManagment;
    using System;
    using System.Runtime.CompilerServices;

    public delegate void SubscriptionChangeHandler(INTAPS.ClientServer.Client.DataChange change, Subscription obj, int objID);
}

