﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using BIZNET.iERP;
using INTAPS.Accounting.Client;
using System.ComponentModel;
using BIZNET.iERP.Client;

namespace INTAPS.SubscriberManagment.Client
{
    public class BankAccountPlaceholder : ComboBox
    {
        BankAccountInfo[] _bankAccounts;
        void loadItems()
        {
            foreach (BankAccountInfo c in _bankAccounts = iERPTransactionClient.GetAllBankAccounts())
                Items.Add(c.ToString());
            if (Items.Count > 0)
                this.SelectedIndex = 0;
        }
        public BankAccountPlaceholder()
        {
            base.DropDownStyle = ComboBoxStyle.DropDownList;
            if (AccountingClient.IsConnected)
                loadItems();
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public BankAccountInfo bankAccount
        {
            get
            {
                if (this.SelectedIndex == -1)
                    return null;
                return _bankAccounts[this.SelectedIndex];
            }
            set
            {
                if (value == null)
                    this.mainCsAccountID = -1;
                else
                    this.mainCsAccountID = value.mainCsAccount;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int mainCsAccountID
        {
            get
            {
                if (this.SelectedIndex == -1)
                    return -1;
                return _bankAccounts[this.SelectedIndex].mainCsAccount;
            }
            set
            {
                int i = 0;
                foreach (BankAccountInfo ac in _bankAccounts)
                {
                    if (ac.mainCsAccount == value)
                    {
                        this.SelectedIndex = i;
                        return;
                    }
                    i++;
                }
                this.SelectedIndex = -1;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int bankServiceChargeAccountID
        {
            get
            {
                if (this.SelectedIndex == -1)
                    return -1;
                return _bankAccounts[this.SelectedIndex].bankServiceChargeCsAccountID;
            }
            set
            {
                int i = 0;
                foreach (BankAccountInfo ac in _bankAccounts)
                {
                    if (ac.bankServiceChargeCsAccountID == value)
                    {
                        this.SelectedIndex = i;
                        return;
                    }
                    i++;
                }
                this.SelectedIndex = -1;
            }
        }

    }
}
