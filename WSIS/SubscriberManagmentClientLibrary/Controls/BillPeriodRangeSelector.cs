
    using INTAPS.Accounting.Client;
    using INTAPS.SubscriberManagment;
    using System;
namespace INTAPS.SubscriberManagment.Client
{

    public class BillPeriodRangeSelector : AccountingPeriodRangeSelector<BillPeriod, BillPeriodSelector>
    {
        protected override BillPeriod GetAccountingPeriod(int periodID)
        {
            return SubscriberManagmentClient.GetBillPeriod(periodID);
        }

        protected override BillPeriod[] GetAccountingPeriods(int Year)
        {
            return SubscriberManagmentClient.GetBillPeriods(Year, SubscriberManagmentClient.Ethiopian);
        }
    }
}

