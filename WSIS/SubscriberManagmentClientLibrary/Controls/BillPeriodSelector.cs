
    using INTAPS.Accounting.Client;
    using INTAPS.Ethiopic;
    using INTAPS.SubscriberManagment;
    using System;
namespace INTAPS.SubscriberManagment.Client
{

    public class BillPeriodSelector : AccountingPeriodSelector<BillPeriod>
    {
        protected override int GetAccountPeriodYear(int year)
        {
            return EtGrDate.ToEth(SubscriberManagmentClient.GetBillPeriod(year).fromDate).Year;
        }

        protected override BillPeriod[] GetPayPeriods(int Year)
        {
            return SubscriberManagmentClient.GetBillPeriods(Year, SubscriberManagmentClient.Ethiopian);
        }

        public override void LoadData(int year)
        {
            base.LoadData(year);
            base.SelectedPeriod = SubscriberManagmentClient.CurrentPeriod;
        }
    }
}

