
    using INTAPS.UI;
    using System;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public class CodedComboBox : ComboBox, IEnterNavigable
    {
        private string[] _codes;
        private object _default = null;
        private object[] _items;
        private bool _modified = false;
        private string[] _names;

        public event EventHandler GoToNext;

        internal void clearValue()
        {
            this.setSelectedItem(this._default);
        }

        public object getSelectedItem()
        {
            if (this.SelectedIndex == -1)
            {
                return this._default;
            }
            return this._items[this.SelectedIndex];
        }

        private void goToNextItem()
        {
            if (this.GoToNext != null)
            {
                this.GoToNext(this, null);
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Return)
            {
                base.OnKeyDown(e);
            }
            else if (!this._modified)
            {
                this.goToNextItem();
            }
            else
            {
                for (int i = 0; i < this._codes.Length; i++)
                {
                    if (this._codes[i].Equals(this.Text))
                    {
                        this.SelectedIndex = i;
                        this._modified = false;
                        this.goToNextItem();
                        break;
                    }
                }
                if (this._modified)
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserError("Invalid code:" + this.Text);
                    this.Text = this._names[this.SelectedIndex];
                    this._modified = false;
                }
            }
        }

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            this._modified = false;
            base.OnSelectedIndexChanged(e);
        }

        protected override void OnTextChanged(EventArgs e)
        {
            this._modified = true;
        }

        private void Populate()
        {
            base.Items.Clear();
            base.Items.AddRange(this._names);
        }

        public void setDefaultValue(object def)
        {
            this._default = def;
        }

        public void setEntries(object[] items, string[] codes)
        {
            this._items = items;
            this._codes = codes;
            this._names = new string[this._items.Length];
            for (int i = 0; i < this._items.Length; i++)
            {
                this._names[i] = this._items[i].ToString();
            }
            this.Populate();
        }

        public void setEntries(object[] items, string[] codes, string[] names)
        {
            this._items = items;
            this._codes = codes;
            this._names = names;
            this.Populate();
        }

        public void setSelectedItem(object value)
        {
            for (int i = 0; i < this._items.Length; i++)
            {
                if (this._items[i].Equals(value))
                {
                    this.SelectedIndex = i;
                    this._modified = false;
                    return;
                }
            }
            this.SelectedIndex = -1;
            this._modified = false;
        }

        public void StartEdit()
        {
            base.Focus();
        }

        public Control ControlObject
        {
            get
            {
                return this;
            }
        }

        public bool Skip
        {
            get
            {
                return false;
            }
        }
    }
}

