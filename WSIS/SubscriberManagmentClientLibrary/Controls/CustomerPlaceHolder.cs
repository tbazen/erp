using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{
    public class CustomerPlaceHolder : TextBox
    {
        private Button m_btnPick = new Button();
        private bool m_changed;
        private Subscriber m_picked;
        private SubscriptionList m_picker;

        public event EventHandler CustomerChanged;

        public CustomerPlaceHolder()
        {
            this.m_btnPick.Text = "...";
            this.m_btnPick.Dock = DockStyle.Right;
            this.m_btnPick.Width = 20;
            this.m_btnPick.Click += new EventHandler(this.m_btnPick_Click);
            this.m_changed = false;
            base.Controls.Add(this.m_btnPick);
        }

        public int GetCustomerID()
        {
            if (this.m_picked == null)
            {
                return -1;
            }
            return this.m_picked.ObjectIDVal;
        }

        private void m_btnPick_Click(object sender, EventArgs e)
        {
            if (this.m_picker == null)
            {
                this.m_picker = new SubscriptionList();
            }
            if (this.m_picker.PickSubscription() == DialogResult.OK)
            {
                this.customer = this.m_picker.pickedSubscription.subscriber;
                this.OnSubscriberChanged();
            }
        }

        protected override void OnEnter(EventArgs e)
        {
            base.OnEnter(e);
            base.SelectAll();
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                string customerCode = this.Text.Trim();
                if (customerCode == "")
                {
                    this.customer = null;
                    this.OnSubscriberChanged();
                }
                else
                {
                    try
                    {
                        Subscriber cust = SubscriberManagmentClient.GetSubscriber(customerCode);
                        if (cust == null)
                            throw new Exception("Subscription not found");

                        this.customer = cust;
                        this.OnSubscriberChanged();
                    }
                    catch (Exception exception)
                    {
                        UIFormApplicationBase.CurrentAppliation.ShowUserMessage(exception.Message);
                        this.customer = this.m_picked;
                        base.SelectAll();
                    }
                }
            }
        }

        protected override void OnLeave(EventArgs e)
        {
            this.customer = this.m_picked;
            base.OnLeave(e);
        }

        protected void OnSubscriberChanged()
        {
            if (this.CustomerChanged != null)
            {
                this.CustomerChanged(this, null);
            }
            this.m_changed = true;
        }

        public void SetByID(int ID)
        {
            if (ID == -1)
            {
                this.customer = null;
            }
            else
            {
                try
                {
                    this.customer = SubscriberManagmentClient.GetSubscriber(ID);
                }
                catch
                {
                    this.m_picked = null;
                    this.Text = "error";
                }
            }
        }

        public bool Changed
        {
            get
            {
                return this.m_changed;
            }
        }

        [Browsable(false)]
        public Subscriber customer
        {
            get
            {
                return this.m_picked;
            }
            set
            {
                this.m_picked = value;
                if (this.m_picked == null)
                {
                    this.Text = "";
                }
                else
                {
                    this.Text = value.customerCode + " - " + value.name;
                }
            }
        }
    }
}
