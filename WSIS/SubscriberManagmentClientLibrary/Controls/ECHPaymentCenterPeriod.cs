
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System;
namespace INTAPS.SubscriberManagment.Client
{

    public class ECHPaymentCenterPeriod : GenericReportClientHandler<RFPaymentCenterPeriod>
    {
        public ECHPaymentCenterPeriod(ReportType rt)
            : base(rt)
        {
        }
    }
}

