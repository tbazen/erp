
    using INTAPS.UI;
    using System;
    using System.IO;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public class FilePicker : ObjectPlaceHolder<string>
    {
        private FolderBrowserDialog folderPicker = null;
        private string m_filter = null;
        private bool m_folderMode = false;
        private OpenFileDialog of = null;

        protected override string GetObjectBYID(int id)
        {
            return null;
        }

        protected override int GetObjectID(string obj)
        {
            return -1;
        }

        protected override string GetObjectString(string obj)
        {
            return obj;
        }

        protected override string PickObject()
        {
            if (!this.m_folderMode)
            {
                if (this.of == null)
                {
                    this.of = new OpenFileDialog();
                    this.of.Filter = this.m_filter;
                    this.of.Multiselect = false;
                }
                if (this.of.ShowDialog() == DialogResult.OK)
                {
                    return this.of.FileName;
                }
            }
            else
            {
                if (this.m_folderMode)
                {
                    this.folderPicker = new FolderBrowserDialog();
                }
                if (this.folderPicker.ShowDialog() == DialogResult.OK)
                {
                    return this.folderPicker.SelectedPath;
                }
            }
            return null;
        }

        protected override string[] SearchObject(string query)
        {
            if (File.Exists(query))
            {
                return new string[] { query };
            }
            return new string[0];
        }

        public string Filter
        {
            get
            {
                return this.m_filter;
            }
            set
            {
                this.m_filter = value;
            }
        }

        public bool FolderMode
        {
            get
            {
                return this.m_folderMode;
            }
            set
            {
                this.m_folderMode = value;
            }
        }
    }
}

