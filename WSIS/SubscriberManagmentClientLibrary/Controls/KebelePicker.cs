
    using INTAPS.SubscriberManagment;
    using System;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    internal class KebelePicker : ListView
    {
        private bool m_IgnoreEvent = false;

        public KebelePicker()
        {
            base.CheckBoxes = true;
            base.View = View.List;
        }

        public int[] GetData()
        {
            if (base.Items[0].Checked)
            {
                return null;
            }
            int[] numArray = new int[base.CheckedItems.Count];
            int num = 0;
            foreach (ListViewItem item in base.CheckedItems)
            {
                numArray[num++] = (int) item.Tag;
            }
            return numArray;
        }

        public void LoadData()
        {
            Kebele kebele = new Kebele();
            kebele.id = -1;
            kebele.name = "All Kebele";
            base.Items.Add(kebele.ToString()).Tag = -1;
            foreach (Kebele kebele2 in SubscriberManagmentClient.GetAllKebeles())
            {
                base.Items.Add(kebele2.ToString()).Tag = kebele2.id;
            }
        }

        protected override void OnItemChecked(ItemCheckedEventArgs e)
        {
            if (!this.m_IgnoreEvent)
            {
                this.m_IgnoreEvent = true;
                try
                {
                    if (e.Item == base.Items[0])
                    {
                        foreach (ListViewItem item in base.Items)
                        {
                            if (item != e.Item)
                            {
                                item.Checked = e.Item.Checked;
                            }
                        }
                    }
                }
                finally
                {
                    this.m_IgnoreEvent = false;
                }
                base.OnItemChecked(e);
            }
        }

        public void SetData(int[] kebeles)
        {
            foreach (ListViewItem item in base.Items)
            {
                if (kebeles == null)
                {
                    item.Checked = true;
                }
                else
                {
                    foreach (int num in kebeles)
                    {
                        if (((int) item.Tag) == num)
                        {
                            item.Checked = true;
                            break;
                        }
                    }
                }
            }
        }
    }
}

