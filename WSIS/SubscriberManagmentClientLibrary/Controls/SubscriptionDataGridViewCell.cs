
    using INTAPS.SubscriberManagment;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public class SubscriptionDataGridViewCell : DataGridViewTextBoxCell
    {
        protected override object GetFormattedValue(object value, int rowIndex, ref DataGridViewCellStyle cellStyle, TypeConverter valueTypeConverter, TypeConverter formattedValueTypeConverter, DataGridViewDataErrorContexts context)
        {
            Subscription subscription = value as Subscription;
            if (subscription == null)
            {
                return "";
            }
            return (subscription.contractNo + " - " + subscription.subscriber.name);
        }
    }
}

