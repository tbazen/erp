
    using INTAPS.SubscriberManagment;
    using INTAPS.UI;
    using System;
    using System.ComponentModel;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public class SubscriptionPlaceHolder : TextBox
    {
        private Button m_btnPick = new Button();
        private bool m_changed;
        private Subscription m_picked;
        private SubscriptionList m_picker;

        public event EventHandler SubscriptionChanged;

        public SubscriptionPlaceHolder()
        {
            this.m_btnPick.Text = "...";
            this.m_btnPick.Dock = DockStyle.Right;
            this.m_btnPick.Width = 20;
            this.m_btnPick.Click += new EventHandler(this.m_btnPick_Click);
            this.m_changed = false;
            base.Controls.Add(this.m_btnPick);
        }

        public int GetSubscriptionID()
        {
            if (this.m_picked == null)
            {
                return -1;
            }
            return this.m_picked.ObjectIDVal;
        }

        private void m_btnPick_Click(object sender, EventArgs e)
        {
            if (this.m_picker == null)
            {
                this.m_picker = new SubscriptionList();
            }
            if (this.m_picker.PickSubscription() == DialogResult.OK)
            {
                this.subscription = this.m_picker.pickedSubscription;
                this.OnSubscriptionChanged();
            }
        }

        protected override void OnEnter(EventArgs e)
        {
            base.OnEnter(e);
            base.SelectAll();
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                string contractNO = this.Text.Trim();
                if (contractNO == "")
                {
                    this.subscription = null;
                    this.OnSubscriptionChanged();
                }
                else
                {
                    try
                    {
                        Subscription subscription = SubscriberManagmentClient.GetSubscription(contractNO,DateTime.Now.Ticks);
                        if (subscription == null)
                            throw new Exception("Subscription not found");

                        this.subscription = subscription;
                        this.OnSubscriptionChanged();
                    }
                    catch (Exception exception)
                    {
                        UIFormApplicationBase.CurrentAppliation.ShowUserMessage(exception.Message);
                        this.subscription = this.m_picked;
                        base.SelectAll();
                    }
                }
            }
        }

        protected override void OnLeave(EventArgs e)
        {
            this.subscription = this.m_picked;
            base.OnLeave(e);
        }

        protected void OnSubscriptionChanged()
        {
            if (this.SubscriptionChanged != null)
            {
                this.SubscriptionChanged(this, null);
            }
            this.m_changed = true;
        }

        public void SetByID(int ID,long version)
        {
            if (ID == -1)
            {
                this.subscription = null;
            }
            else
            {
                try
                {
                    this.subscription = SubscriberManagmentClient.GetSubscription(ID,version);
                }
                catch
                {
                    this.m_picked = null;
                    this.Text = "error";
                }
            }
        }

        public bool Changed
        {
            get
            {
                return this.m_changed;
            }
        }

        [Browsable(false)]
        public Subscription subscription
        {
            get
            {
                return this.m_picked;
            }
            set
            {
                this.m_picked = value;
                if (this.m_picked == null)
                {
                    this.Text = "";
                }
                else
                {
                    this.Text = value.contractNo + " - " + value.subscriber.name;
                }
            }
        }
    }
    
}

