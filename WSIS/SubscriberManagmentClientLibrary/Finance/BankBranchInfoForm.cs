﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using BIZNET.iERP;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class BankBranchInfoForm : Form
    {
        public BankBranchInfo branch;
        BankInfo _bank;
        int _branchID = -1;
        public BankBranchInfoForm( int bankID)
        {
            InitializeComponent();
            _bank=BIZNET.iERP.Client.iERPTransactionClient.getBankInfo(bankID);
            lableBankInfo.Text = _bank.ToString();
        }
        public BankBranchInfoForm(BankBranchInfo branch)
            : this(branch.bankID)
        {
            textName.Text = branch.name;
            _branchID = branch.id;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (textName.Text.Trim() == "")
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Enter branch name");
                return;
            }
            try
            {
                BankBranchInfo branch=new BankBranchInfo();
                branch.id=_branchID;
                branch.bankID = _bank.id;
                branch.name=textName.Text;
                branch.id=BIZNET.iERP.Client.iERPTransactionClient.saveBankBranchInfo(branch);
                this.branch = branch;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
