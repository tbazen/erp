﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BIZNET.iERP;
using INTAPS.UI;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class BankInfoForm : Form
    {
        public BankInfo bank=null;
        int _bankID=-1;
        public BankInfoForm()
        {
            InitializeComponent();
            buttonBranches.Visible = false;
        }
        public BankInfoForm(BankInfo bankInfo)
            : this()
        {
            _bankID = bankInfo.id;
            buttonBranches.Visible = true;
            textName.Text = bankInfo.name;
            updateBranchTable();
        }
        void updateBranchTable()
        {
            buttonBranches.Text = "Branches (" + BIZNET.iERP.Client.iERPTransactionClient.getAllBranchsOfBank(_bankID).Length + ")";
        }

        private void buttonBranches_Click(object sender, EventArgs e)
        {
            BankBranchList list = new BankBranchList(_bankID);
            list.ReloadList();
            list.ShowDialog(this);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (textName.Text.Trim() == "")
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Enter branch name");
                return;
            }
            try
            {
                BankInfo bank = new BankInfo();
                bank.id = _bankID;
                bank.name = textName.Text;
                bank.id = BIZNET.iERP.Client.iERPTransactionClient.saveBankInfo(bank);
                this.bank = bank;
                buttonBranches.Visible = true;
                this.DialogResult = DialogResult.OK;
                this.Close();   
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
    }
    public class BankList : SimpleObjectList<BankInfo>
    {
        public BankList()
        {
        }
        protected override bool AllowEdit
        {
            get
            {
                return true;
            }
        }
        protected override BankInfo EditObject(BankInfo obj)
        {
            BankInfoForm f = new BankInfoForm(obj);
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                return f.bank;
            }
            return null;
        }
        protected override BankInfo CreateObject()
        {
            BankInfoForm f = new BankInfoForm();
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                return f.bank;
            }
            return null;
        }

        protected override void DeleteObject(BankInfo obj)
        {
            BIZNET.iERP.Client.iERPTransactionClient.deleteBankInfo(obj.id);
        }

        protected override BankInfo[] GetObjects()
        {
            return BIZNET.iERP.Client.iERPTransactionClient.getAllBanks();
        }

        protected override string GetObjectString(BankInfo obj)
        {
            return obj.ToString();
        }

        protected override string ObjectTypeName
        {
            get { return "Bank"; }
        }
    }
    public class BankBranchList : SimpleObjectList<BankBranchInfo>
    {
        int _bankID;
        public BankBranchList(int bankID)
        {
            _bankID = bankID;
        }
        protected override bool loadInConstructor
        {
            get
            {
                return false;
            }
        }
        protected override bool AllowEdit
        {
            get
            {
                return true;
            }
        }
        protected override BankBranchInfo EditObject(BankBranchInfo obj)
        {
            BankBranchInfoForm f = new BankBranchInfoForm(obj);
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                return f.branch;
            }
            return null;
        }
        protected override BankBranchInfo CreateObject()
        {
            BankBranchInfoForm f = new BankBranchInfoForm(_bankID);
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                return f.branch;
            }
            return null;
        }

        protected override void DeleteObject(BankBranchInfo obj)
        {
            BIZNET.iERP.Client.iERPTransactionClient.deleteBankBranchInfo(obj.id);
        }

        protected override BankBranchInfo[] GetObjects()
        {
            return BIZNET.iERP.Client.iERPTransactionClient.getAllBranchsOfBank(_bankID);
        }

        protected override string GetObjectString(BankBranchInfo obj)
        {
            return obj.ToString();
        }

        protected override string ObjectTypeName
        {
            get { return "Bank Branch"; }
        }
    }

}
