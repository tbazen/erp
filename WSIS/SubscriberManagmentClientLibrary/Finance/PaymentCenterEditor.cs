
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.ClientServer.Client;
using INTAPS.Payroll.Client;
using INTAPS.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{
    public partial class PaymentCenterEditor : Form
    {
        private IContainer components;
        private bool m_new;
        private List<SerialBatch> m_newSerial;
        public PaymentCenter paymentCenter;

        public PaymentCenterEditor()
        {
            this.paymentCenter = null;
            this.components = null;
            this.InitializeComponent();
            this.m_new = true;
            this.paymentCenter = new PaymentCenter();
            this.m_newSerial = new List<SerialBatch>();
            this.buttonDownloadConfiguration.Enabled = false;
            this.comboStore.Items.AddRange(BIZNET.iERP.Client.iERPTransactionClient.GetAllStores());
            BIZNET.iERP.CashAccount[] cash=BIZNET.iERP.Client.iERPTransactionClient.GetAllCashAccounts(false);
            this.comboDailySales.Items.AddRange(cash);
            this.comboSummarySales.Items.AddRange(cash);
        }
        bool _ignoreEvents = false;
        public PaymentCenterEditor(int paymentCenterID)
            : this()
        {
            _ignoreEvents = true;
            try
            {
                this.paymentCenter = SubscriberManagmentClient.GetPaymentCenter(paymentCenterID);
                this.checkOnline.Checked = (paymentCenter.collectionMethod & CollectionMethod.Online) == CollectionMethod.Online;
                this.checkMobilePayment.Checked = (paymentCenter.collectionMethod & CollectionMethod.Mobile) == CollectionMethod.Mobile;
                this.textName.Text = paymentCenter.centerName;
                SerialBatch sb;
                PaymentCenterSerialBatch pcBatch = getPaymentCenterSerialBatch(paymentCenter, out sb);
                if (pcBatch != null)
                {
                    int rowIndex = this.gridSerial.Rows.Add(new object[] { sb.ToString(), "X" });
                    this.gridSerial.Rows[rowIndex].Tag = pcBatch;
                }
                
                this.m_new = false;
                
                foreach (BIZNET.iERP.StoreInfo s in comboStore.Items)
                {
                    if (s.costCenterID == paymentCenter.storeCostCenterID)
                    {
                        comboStore.SelectedItem = s;
                        break;
                    }
                }
                foreach (BIZNET.iERP.CashAccount c in comboDailySales.Items)
                {
                    if (c.csAccountID == paymentCenter.casherAccountID)
                    {
                        comboDailySales.SelectedItem =c ;
                        break;
                    }
                }
                foreach (BIZNET.iERP.CashAccount c in comboSummarySales.Items)
                {
                    if (c.csAccountID == paymentCenter.summaryAccountID)
                    {
                        comboSummarySales.SelectedItem = c;
                        break;
                    }
                }
                this.buttonDownloadConfiguration.Enabled = (paymentCenter.collectionMethod & CollectionMethod.Offline) == CollectionMethod.Offline;
            }
            finally
            {
                _ignoreEvents = false;
            }
        }

        private static PaymentCenterSerialBatch getPaymentCenterSerialBatch(PaymentCenter center, out SerialBatch serialBatch)
        {
            foreach (PaymentCenterSerialBatch batch in center.serialBatches)
            {
                foreach (SerialBatch sBatch in AccountingClient.GetAllActiveSerialBatches())
                {
                    if (sBatch.id == batch.batchID)
                    {
                        serialBatch = sBatch;
                        return batch;
                    }
                }

            }
            serialBatch = null;
            return null;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.paymentCenter.casheir == -1)
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please select the cashier for this payment center");
                }
                else
                {
                    this.paymentCenter.casherAccountID = comboDailySales.SelectedItem==null?-1:((BIZNET.iERP.CashAccount)comboDailySales.SelectedItem).csAccountID;
                    if (this.paymentCenter.casherAccountID == -1)
                    {
                        UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please select the cash account for this payment center");
                    }
                    else
                    {
                        this.paymentCenter.summaryAccountID = comboSummarySales.SelectedItem == null ? -1 : ((BIZNET.iERP.CashAccount)comboSummarySales.SelectedItem).csAccountID;
                        if (this.paymentCenter.summaryAccountID == -1)
                        {
                            UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please select the summary account for this payment center");
                        }
                        else
                        {
                            this.paymentCenter.centerName = this.textName.Text.Trim();
                            if (this.paymentCenter.centerName == "")
                            {
                                UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please enter the payment center name.");
                            }
                            else
                            {
                                this.paymentCenter.serialBatches = new PaymentCenterSerialBatch[this.gridSerial.Rows.Count];
                                foreach (DataGridViewRow row in (IEnumerable)this.gridSerial.Rows)
                                {
                                    PaymentCenterSerialBatch tag = row.Tag as PaymentCenterSerialBatch;
                                    if (tag.batchID == -1)
                                    {
                                        SerialBatch batch = (SerialBatch)row.Cells[0].Tag;
                                        if (SubscriberManagmentClient.IsPaymentSerialBatchOverlapped(batch))
                                        {
                                            MessageBox.Show("The new serial batch you're creating overlaps with other existing payment serial batches. Please check and try again!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            return;
                                        }
                                        AccountingClient.CreateBatch(batch);
                                        tag.batchID = batch.id;
                                    }
                                    tag.documentTypeID = AccountingClient.GetDocumentTypeByType(typeof(CustomerPaymentReceipt)).id;
                                    this.paymentCenter.serialBatches[row.Index] = tag;
                                }
                                this.paymentCenter.storeCostCenterID = comboStore.SelectedItem == null ? -1 : ((BIZNET.iERP.StoreInfo)comboStore.SelectedItem).costCenterID;
                                
                                this.paymentCenter.collectionMethod =
                                    checkOnline.Checked?CollectionMethod.Online:
                                    (checkMobilePayment.Checked?(CollectionMethod.Mobile|CollectionMethod.Offline)
                                    :CollectionMethod.Offline);
                                if (this.m_new)
                                {
                                    this.paymentCenter.id=SubscriberManagmentClient.CreatePaymentCenter(this.paymentCenter);
                                }
                                else
                                {
                                    SubscriberManagmentClient.UpdatePaymentCenter(this.paymentCenter);
                                }
                                base.DialogResult = DialogResult.OK;
                                this.m_new = false;
                                base.Close();
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't save payment center infromation.", exception);
            }
        }
        private void btnSaveConfiguration_Click(object sender, EventArgs e)
        {
            if (this.paymentCenter == null)
                return;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Sales Point Configuration|*.spconfig";
            SPSystemParameters par = new SPSystemParameters();
            SerialBatch sb;
            PaymentCenterSerialBatch pcBatch = getPaymentCenterSerialBatch(this.paymentCenter, out sb);
            if (pcBatch == null || sb == null)
                return;
            if (pcBatch.documentTypeID != AccountingClient.GetDocumentTypeByType(typeof(CustomerPaymentReceipt)).id)
                return;
            if (!sb.active)
                return;
            par.batchFrom = AccountingClient.GetNextSerial(sb.id);
            if (par.batchFrom < sb.serialFrom)
                par.batchFrom = sb.serialFrom;
            par.batchID = sb.id;
            par.batchTo = sb.serialTo;
            par.centerName = paymentCenter.centerName;
            par.centerAccountID = paymentCenter.casherAccountID;
            par.password = "@**&#&@*@";
            par.userName = paymentCenter.userName;
            par.voidPassword = "@##@@@@##";
            par.cashInstrumentCode = BIZNET.iERP.Client.iERPTransactionClient.GetSystemParamter("cashInstrumentCode") as string;
            par.receiptTitle = SubscriberManagmentClient.GetSystemParameter("receiptTitle") as string;
            if (sfd.ShowDialog(this) == DialogResult.OK)
            {
                System.IO.FileStream s = System.IO.File.Create(sfd.FileName);
                try
                {
                    new System.Xml.Serialization.XmlSerializer(typeof(SPSystemParameters)).Serialize(s, par);
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(ex.Message, ex);
                }
                finally
                {
                    s.Close();
                }

            }
        }
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            SerialDocumentTypePicker picker = new SerialDocumentTypePicker();
            if (picker.ShowDialog() == DialogResult.OK)
            {
                if (picker.batch.id == -1)
                {
                    this.m_newSerial.Add(picker.batch);
                }
                int rowIndex = this.gridSerial.Rows.Add(new object[] { picker.batch.ToString(), "X" });
                PaymentCenterSerialBatch batch = new PaymentCenterSerialBatch();
                batch.batchID = picker.batch.id;
                this.gridSerial.Rows[rowIndex].Tag = batch;
                this.gridSerial[0, rowIndex].Tag = picker.batch;
                buttonDownloadConfiguration.Enabled = false;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void gridSerial_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.RowIndex != -1) && (e.ColumnIndex == 1))
            {
                this.gridSerial.Rows.RemoveAt(e.RowIndex);
            }
        }


        private void label3_Click(object sender, EventArgs e)
        {
        }

        private void dataChanged(object sender, EventArgs e)
        {
            buttonDownloadConfiguration.Enabled = false;
        }

        private void checkOnline_CheckedChanged(object sender, EventArgs e)
        {
            buttonDownloadConfiguration.Enabled = false;
            if (checkOnline.Checked)
                checkMobilePayment.Checked = false;
        }

        private void checkMobilePayment_CheckedChanged(object sender, EventArgs e)
        {
            if (checkMobilePayment.Checked)
                checkOnline.Checked = false;
        }
    }
}

