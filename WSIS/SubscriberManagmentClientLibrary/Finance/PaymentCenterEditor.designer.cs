﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class PaymentCenterEditor : System.Windows.Forms.Form
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PaymentCenterEditor));
            this.textName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.gridSerial = new System.Windows.Forms.DataGridView();
            this.colSerial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDelete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonDownloadConfiguration = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.comboStore = new System.Windows.Forms.ComboBox();
            this.comboDailySales = new System.Windows.Forms.ComboBox();
            this.comboSummarySales = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkOnline = new System.Windows.Forms.CheckBox();
            this.checkMobilePayment = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridSerial)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textName
            // 
            this.textName.Location = new System.Drawing.Point(155, 11);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(382, 20);
            this.textName.TabIndex = 5;
            this.textName.TextChanged += new System.EventHandler(this.dataChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Center Name:";
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnOk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(379, 4);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(65, 28);
            this.btnOk.TabIndex = 29;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(466, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(71, 28);
            this.btnCancel.TabIndex = 28;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label3.Location = new System.Drawing.Point(12, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Daily Sales Account:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // gridSerial
            // 
            this.gridSerial.AllowUserToAddRows = false;
            this.gridSerial.AllowUserToDeleteRows = false;
            this.gridSerial.AllowUserToOrderColumns = true;
            this.gridSerial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.gridSerial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridSerial.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSerial,
            this.colDelete});
            this.gridSerial.Location = new System.Drawing.Point(12, 155);
            this.gridSerial.Name = "gridSerial";
            this.gridSerial.Size = new System.Drawing.Size(525, 249);
            this.gridSerial.TabIndex = 33;
            this.gridSerial.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridSerial_CellClick);
            // 
            // colSerial
            // 
            this.colSerial.HeaderText = "Serial Batch";
            this.colSerial.Name = "colSerial";
            this.colSerial.Width = 300;
            // 
            // colDelete
            // 
            this.colDelete.HeaderText = "";
            this.colDelete.Name = "colDelete";
            this.colDelete.Width = 40;
            // 
            // buttonAdd
            // 
            this.buttonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdd.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdd.ForeColor = System.Drawing.Color.White;
            this.buttonAdd.Image = ((System.Drawing.Image)(resources.GetObject("buttonAdd.Image")));
            this.buttonAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAdd.Location = new System.Drawing.Point(401, 121);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(136, 28);
            this.buttonAdd.TabIndex = 34;
            this.buttonAdd.Text = "&Receipt Serial No";
            this.buttonAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAdd.UseVisualStyleBackColor = false;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label6.Location = new System.Drawing.Point(12, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 17);
            this.label6.TabIndex = 3;
            this.label6.Text = "Summary Account:";
            this.label6.Click += new System.EventHandler(this.label3_Click);
            // 
            // buttonDownloadConfiguration
            // 
            this.buttonDownloadConfiguration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDownloadConfiguration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonDownloadConfiguration.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonDownloadConfiguration.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDownloadConfiguration.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDownloadConfiguration.ForeColor = System.Drawing.Color.White;
            this.buttonDownloadConfiguration.Location = new System.Drawing.Point(12, 4);
            this.buttonDownloadConfiguration.Name = "buttonDownloadConfiguration";
            this.buttonDownloadConfiguration.Size = new System.Drawing.Size(166, 28);
            this.buttonDownloadConfiguration.TabIndex = 29;
            this.buttonDownloadConfiguration.Text = "&Download Configuration";
            this.buttonDownloadConfiguration.UseVisualStyleBackColor = false;
            this.buttonDownloadConfiguration.Click += new System.EventHandler(this.btnSaveConfiguration_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label7.Location = new System.Drawing.Point(12, 95);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 17);
            this.label7.TabIndex = 3;
            this.label7.Text = "Mini Store:";
            this.label7.Click += new System.EventHandler(this.label3_Click);
            // 
            // comboStore
            // 
            this.comboStore.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboStore.FormattingEnabled = true;
            this.comboStore.Location = new System.Drawing.Point(155, 94);
            this.comboStore.Name = "comboStore";
            this.comboStore.Size = new System.Drawing.Size(382, 21);
            this.comboStore.TabIndex = 35;
            this.comboStore.SelectedIndexChanged += new System.EventHandler(this.dataChanged);
            // 
            // comboDailySales
            // 
            this.comboDailySales.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDailySales.FormattingEnabled = true;
            this.comboDailySales.Location = new System.Drawing.Point(155, 43);
            this.comboDailySales.Name = "comboDailySales";
            this.comboDailySales.Size = new System.Drawing.Size(382, 21);
            this.comboDailySales.TabIndex = 35;
            this.comboDailySales.SelectedIndexChanged += new System.EventHandler(this.dataChanged);
            // 
            // comboSummarySales
            // 
            this.comboSummarySales.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSummarySales.FormattingEnabled = true;
            this.comboSummarySales.Location = new System.Drawing.Point(155, 67);
            this.comboSummarySales.Name = "comboSummarySales";
            this.comboSummarySales.Size = new System.Drawing.Size(382, 21);
            this.comboSummarySales.TabIndex = 35;
            this.comboSummarySales.SelectedIndexChanged += new System.EventHandler(this.dataChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Controls.Add(this.buttonDownloadConfiguration);
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 410);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(543, 35);
            this.panel1.TabIndex = 36;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Document Type";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Serial Batch";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // checkOnline
            // 
            this.checkOnline.AutoSize = true;
            this.checkOnline.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.checkOnline.Location = new System.Drawing.Point(15, 126);
            this.checkOnline.Name = "checkOnline";
            this.checkOnline.Size = new System.Drawing.Size(177, 21);
            this.checkOnline.TabIndex = 37;
            this.checkOnline.Text = "Online Payment Center";
            this.checkOnline.UseVisualStyleBackColor = true;
            this.checkOnline.CheckedChanged += new System.EventHandler(this.checkOnline_CheckedChanged);
            // 
            // checkMobilePayment
            // 
            this.checkMobilePayment.AutoSize = true;
            this.checkMobilePayment.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.checkMobilePayment.Location = new System.Drawing.Point(209, 128);
            this.checkMobilePayment.Name = "checkMobilePayment";
            this.checkMobilePayment.Size = new System.Drawing.Size(131, 21);
            this.checkMobilePayment.TabIndex = 37;
            this.checkMobilePayment.Text = "Mobile Payment";
            this.checkMobilePayment.UseVisualStyleBackColor = true;
            this.checkMobilePayment.CheckedChanged += new System.EventHandler(this.checkMobilePayment_CheckedChanged);
            // 
            // PaymentCenterEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Khaki;
            this.ClientSize = new System.Drawing.Size(543, 445);
            this.Controls.Add(this.checkMobilePayment);
            this.Controls.Add(this.checkOnline);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textName);
            this.Controls.Add(this.comboStore);
            this.Controls.Add(this.comboSummarySales);
            this.Controls.Add(this.comboDailySales);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.gridSerial);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PaymentCenterEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Payment Center";
            ((System.ComponentModel.ISupportInitialize)(this.gridSerial)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private System.Windows.Forms.TextBox textName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView gridSerial;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonDownloadConfiguration;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboStore;
        private System.Windows.Forms.ComboBox comboDailySales;
        private System.Windows.Forms.ComboBox comboSummarySales;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSerial;
        private System.Windows.Forms.DataGridViewButtonColumn colDelete;
        private System.Windows.Forms.CheckBox checkOnline;
        private System.Windows.Forms.CheckBox checkMobilePayment;

    }
}