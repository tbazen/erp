
    
    using INTAPS.UI;
    using System;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public class PaymentCenterList : SimpleObjectList<PaymentCenter>
    {
        protected override PaymentCenter CreateObject()
        {
            PaymentCenterEditor editor = new PaymentCenterEditor();
            if (editor.ShowDialog(this) == DialogResult.OK)
            {
                return editor.paymentCenter;
            }
            return null;
        }

        protected override void DeleteObject(PaymentCenter obj)
        {
            SubscriberManagmentClient.DeletePaymentCenter(obj.id);
        }

        protected override PaymentCenter EditObject(PaymentCenter obj)
        {
            PaymentCenterEditor editor = new PaymentCenterEditor(obj.id);
            if (editor.ShowDialog() == DialogResult.OK)
            {
                return editor.paymentCenter;
            }
            return null;
        }

        protected override PaymentCenter[] GetObjects()
        {
            return SubscriberManagmentClient.AllPaymentCenters;
        }

        protected override string GetObjectString(PaymentCenter obj)
        {
            if (obj == null)
            {
                return "";
            }
            return obj.centerName;
        }

        protected override bool AllowEdit
        {
            get
            {
                return true;
            }
        }

        protected override string ObjectTypeName
        {
            get
            {
                return "Payment center";
            }
        }
    }
}

