﻿namespace INTAPS.SubscriberManagment.Client
{
    partial class PaymentInstrumentEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.textName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.checkIsBank = new System.Windows.Forms.CheckBox();
            this.checkIsCurrency = new System.Windows.Forms.CheckBox();
            this.checkIsToken = new System.Windows.Forms.CheckBox();
            this.checkIsTransferFromAccount = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.itemPlaceholder = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnOk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(381, 3);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(65, 28);
            this.btnOk.TabIndex = 33;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(452, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(65, 28);
            this.btnCancel.TabIndex = 32;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // textName
            // 
            this.textName.Location = new System.Drawing.Point(103, 61);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(414, 20);
            this.textName.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 17);
            this.label1.TabIndex = 38;
            this.label1.Text = "Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(12, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 38;
            this.label2.Text = "Item Code:";
            // 
            // checkIsBank
            // 
            this.checkIsBank.AutoSize = true;
            this.checkIsBank.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.checkIsBank.ForeColor = System.Drawing.Color.White;
            this.checkIsBank.Location = new System.Drawing.Point(15, 118);
            this.checkIsBank.Name = "checkIsBank";
            this.checkIsBank.Size = new System.Drawing.Size(100, 21);
            this.checkIsBank.TabIndex = 41;
            this.checkIsBank.Text = "Bank Based";
            this.checkIsBank.UseVisualStyleBackColor = true;
            // 
            // checkIsCurrency
            // 
            this.checkIsCurrency.AutoSize = true;
            this.checkIsCurrency.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.checkIsCurrency.ForeColor = System.Drawing.Color.White;
            this.checkIsCurrency.Location = new System.Drawing.Point(134, 118);
            this.checkIsCurrency.Name = "checkIsCurrency";
            this.checkIsCurrency.Size = new System.Drawing.Size(84, 21);
            this.checkIsCurrency.TabIndex = 41;
            this.checkIsCurrency.Text = "Currency";
            this.checkIsCurrency.UseVisualStyleBackColor = true;
            // 
            // checkIsToken
            // 
            this.checkIsToken.AutoSize = true;
            this.checkIsToken.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.checkIsToken.ForeColor = System.Drawing.Color.White;
            this.checkIsToken.Location = new System.Drawing.Point(248, 118);
            this.checkIsToken.Name = "checkIsToken";
            this.checkIsToken.Size = new System.Drawing.Size(64, 21);
            this.checkIsToken.TabIndex = 41;
            this.checkIsToken.Text = "Token";
            this.checkIsToken.UseVisualStyleBackColor = true;
            // 
            // checkIsTransferFromAccount
            // 
            this.checkIsTransferFromAccount.AutoSize = true;
            this.checkIsTransferFromAccount.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.checkIsTransferFromAccount.ForeColor = System.Drawing.Color.White;
            this.checkIsTransferFromAccount.Location = new System.Drawing.Point(350, 118);
            this.checkIsTransferFromAccount.Name = "checkIsTransferFromAccount";
            this.checkIsTransferFromAccount.Size = new System.Drawing.Size(167, 21);
            this.checkIsTransferFromAccount.TabIndex = 41;
            this.checkIsTransferFromAccount.Text = "Transfer from Account";
            this.checkIsTransferFromAccount.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 166);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(538, 37);
            this.panel1.TabIndex = 42;
            // 
            // itemPlaceholder
            // 
            this.itemPlaceholder.anobject = null;
            this.itemPlaceholder.Location = new System.Drawing.Point(103, 23);
            this.itemPlaceholder.Name = "itemPlaceholder";
            this.itemPlaceholder.Size = new System.Drawing.Size(414, 20);
            this.itemPlaceholder.TabIndex = 40;
            // 
            // PaymentInstrumentEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(538, 203);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.checkIsTransferFromAccount);
            this.Controls.Add(this.checkIsToken);
            this.Controls.Add(this.checkIsCurrency);
            this.Controls.Add(this.checkIsBank);
            this.Controls.Add(this.itemPlaceholder);
            this.Controls.Add(this.textName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "PaymentInstrumentEditorForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Payment Instrument";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox textName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private BIZNET.iERP.Client.TransactionItemPlaceHolder itemPlaceholder;
        private System.Windows.Forms.CheckBox checkIsBank;
        private System.Windows.Forms.CheckBox checkIsCurrency;
        private System.Windows.Forms.CheckBox checkIsToken;
        private System.Windows.Forms.CheckBox checkIsTransferFromAccount;
        private System.Windows.Forms.Panel panel1;
    }
}