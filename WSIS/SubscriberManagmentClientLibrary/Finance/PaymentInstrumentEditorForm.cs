﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BIZNET.iERP;
using INTAPS.UI;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class PaymentInstrumentEditorForm : Form
    {
        public PaymentInstrumentType instType;
        public PaymentInstrumentEditorForm()
        {
            InitializeComponent();
        }
        public PaymentInstrumentEditorForm(PaymentInstrumentType instType):this()
        {
            this.instType = instType;
            itemPlaceholder.SetByID(instType.itemCode);
            textName.Text = instType.name;
            checkIsBank.Checked = instType.isBankDocument;
            checkIsCurrency.Checked = instType.isCurreny;
            checkIsTransferFromAccount.Checked = instType.isTransferFromAccount;
            checkIsToken.Checked = instType.isToken;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                PaymentInstrumentType inst = new PaymentInstrumentType();
                if (itemPlaceholder.GetObjectID() == null)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter valid item code");
                    return;
                }
                inst.itemCode = itemPlaceholder.GetObjectID();
                
                if (textName.Text.Trim() == "")
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter valid name");
                    return; 
                }
                inst.name = textName.Text.Trim();
                inst.isBankDocument = checkIsBank.Checked;
                inst.isCurreny = checkIsCurrency.Checked;
                inst.isToken= checkIsToken.Checked;
                inst.isTransferFromAccount= checkIsTransferFromAccount.Checked;
                BIZNET.iERP.Client.iERPTransactionClient.savePaymentInstrumentType(inst);
                this.instType = inst;

                this.DialogResult = DialogResult.OK;
                this.Close();
                
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
    public class PaymentInstrumentTypeList : SimpleObjectList<PaymentInstrumentType>
    {
        public PaymentInstrumentTypeList()
        {
        }
        protected override bool AllowEdit
        {
            get
            {
                return true;
            }
        }
        protected override PaymentInstrumentType EditObject(PaymentInstrumentType obj)
        {
            PaymentInstrumentEditorForm f = new PaymentInstrumentEditorForm(obj);
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                return f.instType;
            }
            return null;
        }
        protected override PaymentInstrumentType CreateObject()
        {
            PaymentInstrumentEditorForm f = new PaymentInstrumentEditorForm();
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                return f.instType;
            }
            return null;
        }

        protected override void DeleteObject(PaymentInstrumentType obj)
        {
            BIZNET.iERP.Client.iERPTransactionClient.deletePaymentInstrumentType(obj.itemCode);
        }

        protected override PaymentInstrumentType[] GetObjects()
        {
            return BIZNET.iERP.Client.iERPTransactionClient.getAllPaymentInstrumentTypes();
        }

        protected override string GetObjectString(PaymentInstrumentType obj)
        {
            return obj.ToString();
        }

        protected override string ObjectTypeName
        {
            get { return "Payment Instrument Type"; }
        }
    }
}
