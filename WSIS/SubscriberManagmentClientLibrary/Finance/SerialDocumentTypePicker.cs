
    using INTAPS.Accounting;
    using INTAPS.Accounting.Client;
    using INTAPS.UI;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class SerialDocumentTypePicker : Form
    {
        public SerialBatch batch;
        
        private IContainer components = null;
        
        public SerialDocumentTypePicker()
        {
            this.InitializeComponent();
            this.comboSerialNo.Items.AddRange(AccountingClient.GetAllActiveSerialBatches());
            this.comboSerialNo.Items.Add("New Batch");
            if (this.comboSerialNo.Items.Count > 1)
            {
                this.comboSerialNo.SelectedIndex = 0;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.batch = this.comboSerialNo.SelectedItem as SerialBatch;
            if ((this.batch == null) )
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please select serial batch.");
            }
            else
            {
                base.DialogResult = DialogResult.OK;
            }
        }

        private void comboSerialNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboSerialNo.SelectedItem is string)
            {
                SerialBatchEditor editor = new SerialBatchEditor();
                if (editor.ShowDialog(UIFormApplicationBase.MainForm) == DialogResult.OK)
                {
                    this.comboSerialNo.Items[this.comboSerialNo.Items.Count - 1] = editor.formData;
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

