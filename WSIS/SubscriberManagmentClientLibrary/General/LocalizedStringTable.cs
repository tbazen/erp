﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.SubscriberManagment.Client
{
    class LocalizedStringTable
    {
        static int languageID=0;
        static LocalizedStringTable()
        {
            string lid = System.Configuration.ConfigurationManager.AppSettings["languageID"];
            if (string.IsNullOrEmpty(lid) || !int.TryParse(lid, out languageID))
                languageID= 0;

        }
        public static string getString(string key)
        {
            try
            {
                System.Resources.ResourceManager r = SubscriberManagmentClient.billingRuleClient.getStringTable(languageID);
                string ret = r.GetString(key);
                if (ret == null)
                    return key;
                return ret;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
