﻿namespace INTAPS.SubscriberManagment.Client
{
    partial class SimpleHTMLViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bowserController1 = new INTAPS.UI.HTML.BowserController();
            this.controlBrowser1 = new INTAPS.UI.HTML.ControlBrowser();
            this.SuspendLayout();
            // 
            // bowserController1
            // 
            this.bowserController1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.bowserController1.Location = new System.Drawing.Point(0, 0);
            this.bowserController1.Name = "bowserController1";
            this.bowserController1.ShowBackForward = true;
            this.bowserController1.ShowRefresh = true;
            this.bowserController1.Size = new System.Drawing.Size(600, 25);
            this.bowserController1.TabIndex = 0;
            this.bowserController1.Text = "bowserController1";
            // 
            // controlBrowser1
            // 
            this.controlBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlBrowser1.Location = new System.Drawing.Point(0, 25);
            this.controlBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.controlBrowser1.Name = "controlBrowser1";
            this.controlBrowser1.Size = new System.Drawing.Size(600, 406);
            this.controlBrowser1.StyleSheetFile = null;
            this.controlBrowser1.TabIndex = 1;
            // 
            // SimpleHTMLViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(600, 431);
            this.Controls.Add(this.controlBrowser1);
            this.Controls.Add(this.bowserController1);
            this.Name = "SimpleHTMLViewer";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HTML Viewer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UI.HTML.BowserController bowserController1;
        private UI.HTML.ControlBrowser controlBrowser1;
    }
}