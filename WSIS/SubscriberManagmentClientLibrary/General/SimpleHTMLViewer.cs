﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class SimpleHTMLViewer : Form
    {
        public SimpleHTMLViewer(string caption,string html,string css)
        {
            InitializeComponent();
            bowserController1.SetBrowser(controlBrowser1);
            controlBrowser1.StyleSheetFile = css;
            controlBrowser1.LoadTextPage(caption, html);
            this.Text = caption;
        }
    }
}
