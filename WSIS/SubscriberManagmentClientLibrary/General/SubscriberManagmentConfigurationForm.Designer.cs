﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class SubscriberManagmentConfigurationForm : System.Windows.Forms.Form
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.catMeter = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.billPeriodReading = new INTAPS.SubscriberManagment.Client.BillPeriodSelector();
            this.billPeriodSelector = new INTAPS.SubscriberManagment.Client.BillPeriodSelector();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textReadingDistanceTolerance = new System.Windows.Forms.TextBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.pageGeneral = new System.Windows.Forms.TabPage();
            this.pageAccount = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.acPayable = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.acReceivable = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.acPenality = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.label2 = new System.Windows.Forms.Label();
            this.acFixedFee = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.label6 = new System.Windows.Forms.Label();
            this.acAdditoinalFee = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.acIncomeAccount = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.textReceivableAccountFormula = new System.Windows.Forms.TextBox();
            this.textDepositAccountFormula = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.pageGeneral.SuspendLayout();
            this.pageAccount.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Image = global::INTAPS.SubscriberManagment.Client.Properties.Resources.SaveAll_32x32;
            this.btnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOk.Location = new System.Drawing.Point(513, 5);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(80, 28);
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "&Save";
            this.btnOk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Image = global::INTAPS.SubscriberManagment.Client.Properties.Resources.Close_32x32;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(599, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(96, 28);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(21, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(212, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Water Meter Material Category:";
            // 
            // catMeter
            // 
            this.catMeter.Location = new System.Drawing.Point(294, 25);
            this.catMeter.Name = "catMeter";
            this.catMeter.Size = new System.Drawing.Size(280, 23);
            this.catMeter.TabIndex = 9;
            this.catMeter.TextChanged += new System.EventHandler(this.catMeter_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(20, 132);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Billing Period:";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(20, 173);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 17);
            this.label8.TabIndex = 1;
            this.label8.Text = "Reading Period:";
            this.label8.Click += new System.EventHandler(this.label7_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 632);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(709, 38);
            this.panel1.TabIndex = 11;
            // 
            // billPeriodReading
            // 
            this.billPeriodReading.Location = new System.Drawing.Point(294, 169);
            this.billPeriodReading.Name = "billPeriodReading";
            this.billPeriodReading.NullSelection = null;
            this.billPeriodReading.SelectedPeriod = null;
            this.billPeriodReading.Size = new System.Drawing.Size(280, 24);
            this.billPeriodReading.TabIndex = 10;
            this.billPeriodReading.Load += new System.EventHandler(this.billPeriodSelector_Load);
            // 
            // billPeriodSelector
            // 
            this.billPeriodSelector.Location = new System.Drawing.Point(294, 132);
            this.billPeriodSelector.Name = "billPeriodSelector";
            this.billPeriodSelector.NullSelection = null;
            this.billPeriodSelector.SelectedPeriod = null;
            this.billPeriodSelector.Size = new System.Drawing.Size(280, 24);
            this.billPeriodSelector.TabIndex = 10;
            this.billPeriodSelector.Load += new System.EventHandler(this.billPeriodSelector_Load);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(20, 84);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(245, 17);
            this.label10.TabIndex = 1;
            this.label10.Text = "Prepaid Water Meter Item/Category:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(21, 56);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(261, 17);
            this.label11.TabIndex = 1;
            this.label11.Text = "Maximum Distance for Mobile Reading:";
            // 
            // textReadingDistanceTolerance
            // 
            this.textReadingDistanceTolerance.Location = new System.Drawing.Point(294, 54);
            this.textReadingDistanceTolerance.Name = "textReadingDistanceTolerance";
            this.textReadingDistanceTolerance.Size = new System.Drawing.Size(280, 23);
            this.textReadingDistanceTolerance.TabIndex = 9;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.pageGeneral);
            this.tabControl.Controls.Add(this.pageAccount);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(709, 632);
            this.tabControl.TabIndex = 14;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // pageGeneral
            // 
            this.pageGeneral.BackColor = System.Drawing.Color.Gray;
            this.pageGeneral.Controls.Add(this.label3);
            this.pageGeneral.Controls.Add(this.label10);
            this.pageGeneral.Controls.Add(this.label7);
            this.pageGeneral.Controls.Add(this.billPeriodReading);
            this.pageGeneral.Controls.Add(this.label11);
            this.pageGeneral.Controls.Add(this.billPeriodSelector);
            this.pageGeneral.Controls.Add(this.label8);
            this.pageGeneral.Controls.Add(this.textReadingDistanceTolerance);
            this.pageGeneral.Controls.Add(this.catMeter);
            this.pageGeneral.Location = new System.Drawing.Point(4, 24);
            this.pageGeneral.Name = "pageGeneral";
            this.pageGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.pageGeneral.Size = new System.Drawing.Size(770, 604);
            this.pageGeneral.TabIndex = 0;
            this.pageGeneral.Text = "General";
            // 
            // pageAccount
            // 
            this.pageAccount.BackColor = System.Drawing.Color.Gray;
            this.pageAccount.Controls.Add(this.textDepositAccountFormula);
            this.pageAccount.Controls.Add(this.textReceivableAccountFormula);
            this.pageAccount.Controls.Add(this.label12);
            this.pageAccount.Controls.Add(this.label1);
            this.pageAccount.Controls.Add(this.acPayable);
            this.pageAccount.Controls.Add(this.acReceivable);
            this.pageAccount.Controls.Add(this.label13);
            this.pageAccount.Controls.Add(this.label4);
            this.pageAccount.Controls.Add(this.label9);
            this.pageAccount.Controls.Add(this.label5);
            this.pageAccount.Controls.Add(this.acPenality);
            this.pageAccount.Controls.Add(this.label2);
            this.pageAccount.Controls.Add(this.acFixedFee);
            this.pageAccount.Controls.Add(this.label6);
            this.pageAccount.Controls.Add(this.acAdditoinalFee);
            this.pageAccount.Controls.Add(this.acIncomeAccount);
            this.pageAccount.Location = new System.Drawing.Point(4, 24);
            this.pageAccount.Name = "pageAccount";
            this.pageAccount.Padding = new System.Windows.Forms.Padding(3);
            this.pageAccount.Size = new System.Drawing.Size(701, 604);
            this.pageAccount.TabIndex = 1;
            this.pageAccount.Text = "Accounts";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(18, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(263, 17);
            this.label1.TabIndex = 18;
            this.label1.Text = "Customer Receivable Control Account:";
            // 
            // acPayable
            // 
            this.acPayable.account = null;
            this.acPayable.AllowAdd = false;
            this.acPayable.Location = new System.Drawing.Point(292, 171);
            this.acPayable.Name = "acPayable";
            this.acPayable.OnlyLeafAccount = false;
            this.acPayable.Size = new System.Drawing.Size(373, 23);
            this.acPayable.TabIndex = 24;
            // 
            // acReceivable
            // 
            this.acReceivable.account = null;
            this.acReceivable.AllowAdd = false;
            this.acReceivable.Location = new System.Drawing.Point(292, 32);
            this.acReceivable.Name = "acReceivable";
            this.acReceivable.OnlyLeafAccount = false;
            this.acReceivable.Size = new System.Drawing.Size(373, 23);
            this.acReceivable.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(18, 173);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(187, 17);
            this.label4.TabIndex = 25;
            this.label4.Text = "Customer Deposit Account:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(18, 389);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(140, 17);
            this.label9.TabIndex = 20;
            this.label9.Text = "Water Sale Account:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(18, 423);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(272, 17);
            this.label5.TabIndex = 21;
            this.label5.Text = "Subscription Monthely Fixed Fee Account:";
            // 
            // acPenality
            // 
            this.acPenality.account = null;
            this.acPenality.AllowAdd = false;
            this.acPenality.Location = new System.Drawing.Point(291, 491);
            this.acPenality.Name = "acPenality";
            this.acPenality.OnlyLeafAccount = false;
            this.acPenality.Size = new System.Drawing.Size(373, 23);
            this.acPenality.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(18, 458);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(222, 17);
            this.label2.TabIndex = 22;
            this.label2.Text = "Service Charge Income Account:";
            // 
            // acFixedFee
            // 
            this.acFixedFee.account = null;
            this.acFixedFee.AllowAdd = false;
            this.acFixedFee.Location = new System.Drawing.Point(292, 421);
            this.acFixedFee.Name = "acFixedFee";
            this.acFixedFee.OnlyLeafAccount = false;
            this.acFixedFee.Size = new System.Drawing.Size(373, 23);
            this.acFixedFee.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(18, 493);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(173, 17);
            this.label6.TabIndex = 23;
            this.label6.Text = "Late Bill Penality Account:";
            // 
            // acAdditoinalFee
            // 
            this.acAdditoinalFee.account = null;
            this.acAdditoinalFee.AllowAdd = false;
            this.acAdditoinalFee.Location = new System.Drawing.Point(291, 456);
            this.acAdditoinalFee.Name = "acAdditoinalFee";
            this.acAdditoinalFee.OnlyLeafAccount = false;
            this.acAdditoinalFee.Size = new System.Drawing.Size(373, 23);
            this.acAdditoinalFee.TabIndex = 16;
            // 
            // acIncomeAccount
            // 
            this.acIncomeAccount.account = null;
            this.acIncomeAccount.AllowAdd = false;
            this.acIncomeAccount.Location = new System.Drawing.Point(291, 387);
            this.acIncomeAccount.Name = "acIncomeAccount";
            this.acIncomeAccount.OnlyLeafAccount = false;
            this.acIncomeAccount.Size = new System.Drawing.Size(373, 23);
            this.acIncomeAccount.TabIndex = 17;
            // 
            // textReceivableAccountFormula
            // 
            this.textReceivableAccountFormula.Location = new System.Drawing.Point(105, 90);
            this.textReceivableAccountFormula.Multiline = true;
            this.textReceivableAccountFormula.Name = "textReceivableAccountFormula";
            this.textReceivableAccountFormula.Size = new System.Drawing.Size(559, 75);
            this.textReceivableAccountFormula.TabIndex = 26;
            // 
            // textDepositAccountFormula
            // 
            this.textDepositAccountFormula.Location = new System.Drawing.Point(105, 244);
            this.textDepositAccountFormula.Multiline = true;
            this.textDepositAccountFormula.Name = "textDepositAccountFormula";
            this.textDepositAccountFormula.Size = new System.Drawing.Size(562, 75);
            this.textDepositAccountFormula.TabIndex = 26;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(18, 59);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(319, 17);
            this.label12.TabIndex = 18;
            this.label12.Text = "Customer Receivable Control Account Formula:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(18, 210);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(243, 17);
            this.label13.TabIndex = 25;
            this.label13.Text = "Customer Deposit Account Formula:";
            // 
            // SubscriberManagmentConfigurationForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Gray;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(709, 670);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SubscriberManagmentConfigurationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Billing System Configuration";
            this.panel1.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.pageGeneral.ResumeLayout(false);
            this.pageGeneral.PerformLayout();
            this.pageAccount.ResumeLayout(false);
            this.pageAccount.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox catMeter;
        private System.Windows.Forms.Label label7;
        private BillPeriodSelector billPeriodSelector;
        private System.Windows.Forms.Label label8;
        private BillPeriodSelector billPeriodReading;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textReadingDistanceTolerance;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage pageGeneral;
        private System.Windows.Forms.TabPage pageAccount;
        private System.Windows.Forms.Label label1;
        private Accounting.Client.AccountPlaceHolder acPayable;
        private Accounting.Client.AccountPlaceHolder acReceivable;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private Accounting.Client.AccountPlaceHolder acPenality;
        private System.Windows.Forms.Label label2;
        private Accounting.Client.AccountPlaceHolder acFixedFee;
        private System.Windows.Forms.Label label6;
        private Accounting.Client.AccountPlaceHolder acAdditoinalFee;
        private Accounting.Client.AccountPlaceHolder acIncomeAccount;
        private System.Windows.Forms.TextBox textDepositAccountFormula;
        private System.Windows.Forms.TextBox textReceivableAccountFormula;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
    }
}