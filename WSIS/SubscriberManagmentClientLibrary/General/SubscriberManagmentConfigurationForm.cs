using INTAPS.Accounting.Client;
using INTAPS.UI;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class SubscriberManagmentConfigurationForm : Form
    {
        private Button btnCancel;

        private IContainer components = null;

        public SubscriberManagmentConfigurationForm()
        {
            this.InitializeComponent();
            object[] systemParameters = SubscriberManagmentClient.GetSystemParameters(new string[] { 
                "materialIssueAccount"
                , "meterMaterialCategory"
                , "meterStockAcount"
                , "receivableAccount"
                , "incomeAccount"
                , "rentIncomeAccount"
                , "billPenalityIncomeAccount"
            ,"additionalFeeIncomeAcount"
            ,"customerDepositAccount"
            ,"readingDistanceTolerance"
            ,"customerDepositAccountFormula"
            ,"receivableAccountFormula"
            });
            BIZNET.iERP.ItemCategory cat=BIZNET.iERP.Client.iERPTransactionClient.GetItemCategory(Convert.ToInt32 (systemParameters[1]));
            this.catMeter.Text=cat==null?"":cat.Code;
            this.acReceivable.SetByID(Convert.ToInt32(systemParameters[3]));
            this.acIncomeAccount.SetByID(Convert.ToInt32(systemParameters[4]));
            this.acFixedFee.SetByID(Convert.ToInt32(systemParameters[5]));
            this.acPenality.SetByID(Convert.ToInt32(systemParameters[6]));
            this.acAdditoinalFee.SetByID(Convert.ToInt32(systemParameters[7]));
            this.acPayable.SetByID(Convert.ToInt32(systemParameters[8]));
            this.billPeriodSelector.LoadData(SubscriberManagmentClient.CurrentYear);
            this.billPeriodReading.LoadData(SubscriberManagmentClient.CurrentYear);
            this.billPeriodSelector.SelectedPeriod = SubscriberManagmentClient.CurrentPeriod;
            this.billPeriodReading.SelectedPeriod = SubscriberManagmentClient.ReadingPeriod;
            this.textReadingDistanceTolerance.Text = ((double)systemParameters[9]) == 0 ? "" : ((double)systemParameters[9]).ToString();

            this.textDepositAccountFormula.Text = systemParameters[10] is String ? (String)systemParameters[10] : "";
            this.textReceivableAccountFormula.Text = systemParameters[11] is String ? (String)systemParameters[11] : "";

        }

        private void billPeriodSelector_Load(object sender, EventArgs e)
        {
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                BIZNET.iERP.ItemCategory cat = BIZNET.iERP.Client.iERPTransactionClient.GetItemCategory(catMeter.Text);
                int itemCategory = cat == null ? -1 : cat.ID;
                double tol;
                if (textReadingDistanceTolerance.Text == "")
                    tol = 0;
                else if (!INTAPS.UI.Helper.ValidateDoubleTextBox(textReadingDistanceTolerance, "Please enter valid maximum reading distance", false, true, out tol))
                        return;
                SubscriberManagmentClient.SetSystemParameters(new string[] { 
                     "meterMaterialCategory"
                    , "receivableAccount"
                    , "incomeAccount"
                    , "rentIncomeAccount"
                    , "billPenalityIncomeAccount"
                    , "currentPeriod"
                    , "readingPeriod"
                    ,"additionalFeeIncomeAcount"
                    ,"customerDepositAccount"
                    ,"readingDistanceTolerance"
                    ,"customerDepositAccountFormula"
                    ,"receivableAccountFormula"
                }, new object[] { 
                         itemCategory
                         , this.acReceivable.GetAccountID()
                         , this.acIncomeAccount.GetAccountID()
                         , this.acFixedFee.GetAccountID()
                         , this.acPenality.GetAccountID()
                         , this.billPeriodSelector.SelectedPeriod.id
                         , this.billPeriodReading.SelectedPeriod.id 
                        ,this.acAdditoinalFee.GetAccountID()  
                        ,this.acPayable.GetAccountID()
                        ,tol
                        ,textDepositAccountFormula.Text.Trim()
                        ,textReceivableAccountFormula.Text.Trim()
                });
                base.DialogResult = DialogResult.OK;
                base.Close();
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to save system parameters.", exception);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void label7_Click(object sender, EventArgs e)
        {
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void catMeter_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

