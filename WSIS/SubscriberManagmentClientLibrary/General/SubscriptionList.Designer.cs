﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class SubscriptionList : System.Windows.Forms.Form
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colContractNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colKebele = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colZone = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDMA = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.miViewBills = new System.Windows.Forms.ToolStripMenuItem();
            this.miReadings = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlDialog = new System.Windows.Forms.Panel();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.subscriberFilter = new INTAPS.SubscriberManagment.Client.SubscriberFilter();
            this.contextMenuStrip.SuspendLayout();
            this.pnlDialog.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView
            // 
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.colContractNo,
            this.columnHeader2,
            this.colName,
            this.colType,
            this.colStatus,
            this.colKebele,
            this.colZone,
            this.colDMA});
            this.listView.ContextMenuStrip = this.contextMenuStrip;
            this.listView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView.FullRowSelect = true;
            this.listView.HideSelection = false;
            this.listView.Location = new System.Drawing.Point(0, 139);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(915, 342);
            this.listView.TabIndex = 0;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.Details;
            this.listView.SelectedIndexChanged += new System.EventHandler(this.listView_SelectedIndexChanged);
            this.listView.DoubleClick += new System.EventHandler(this.listView_DoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Customer Code";
            this.columnHeader1.Width = 128;
            // 
            // colContractNo
            // 
            this.colContractNo.Text = "Contract No.";
            this.colContractNo.Width = 103;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Meter No.";
            this.columnHeader2.Width = 142;
            // 
            // colName
            // 
            this.colName.Text = "Name";
            this.colName.Width = 152;
            // 
            // colType
            // 
            this.colType.Text = "Type";
            this.colType.Width = 133;
            // 
            // colStatus
            // 
            this.colStatus.Text = "Status";
            this.colStatus.Width = 147;
            // 
            // colKebele
            // 
            this.colKebele.Text = "Kebele";
            this.colKebele.Width = 96;
            // 
            // colZone
            // 
            this.colZone.Text = "Pressure Zone";
            this.colZone.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colZone.Width = 100;
            // 
            // colDMA
            // 
            this.colDMA.Text = "DMA";
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miOpen,
            this.miViewBills,
            this.miReadings});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(123, 70);
            this.contextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip_Opening);
            // 
            // miOpen
            // 
            this.miOpen.Name = "miOpen";
            this.miOpen.Size = new System.Drawing.Size(122, 22);
            this.miOpen.Text = "Open";
            this.miOpen.Click += new System.EventHandler(this.miOpen_Click);
            // 
            // miViewBills
            // 
            this.miViewBills.Name = "miViewBills";
            this.miViewBills.Size = new System.Drawing.Size(122, 22);
            this.miViewBills.Text = "Bills";
            this.miViewBills.Click += new System.EventHandler(this.miViewBills_Click);
            // 
            // miReadings
            // 
            this.miReadings.Name = "miReadings";
            this.miReadings.Size = new System.Drawing.Size(122, 22);
            this.miReadings.Text = "Readings";
            this.miReadings.Click += new System.EventHandler(this.miReadings_Click);
            // 
            // pnlDialog
            // 
            this.pnlDialog.BackColor = System.Drawing.Color.DarkOrange;
            this.pnlDialog.Controls.Add(this.btnOk);
            this.pnlDialog.Controls.Add(this.btnCancel);
            this.pnlDialog.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlDialog.Location = new System.Drawing.Point(0, 481);
            this.pnlDialog.Name = "pnlDialog";
            this.pnlDialog.Size = new System.Drawing.Size(915, 41);
            this.pnlDialog.TabIndex = 3;
            this.pnlDialog.Visible = false;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnOk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(767, 6);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(65, 28);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(838, 6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(65, 28);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            // 
            // subscriberFilter
            // 
            this.subscriberFilter.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.subscriberFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.subscriberFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.subscriberFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subscriberFilter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(99)))), ((int)(((byte)(99)))));
            this.subscriberFilter.Location = new System.Drawing.Point(0, 0);
            this.subscriberFilter.Name = "subscriberFilter";
            this.subscriberFilter.Size = new System.Drawing.Size(915, 139);
            this.subscriberFilter.TabIndex = 1;
            this.subscriberFilter.AfterSearch += new INTAPS.SubscriberManagment.Client.SubscriberSearchHandler(this.subscriberFilter_AfterSearch);
            this.subscriberFilter.Load += new System.EventHandler(this.subscriberFilter_Load);
            // 
            // SubscriptionList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 522);
            this.Controls.Add(this.listView);
            this.Controls.Add(this.pnlDialog);
            this.Controls.Add(this.subscriberFilter);
            this.Name = "SubscriptionList";
            this.ShowIcon = false;
            this.Text = "Customers List";
            this.contextMenuStrip.ResumeLayout(false);
            this.pnlDialog.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.ListView listView;
        private System.Windows.Forms.ColumnHeader colContractNo;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader colType;
        private System.Windows.Forms.ColumnHeader colStatus;
        private System.Windows.Forms.ColumnHeader colKebele;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem miOpen;
        private System.Windows.Forms.ToolStripMenuItem miViewBills;
        private System.Windows.Forms.ToolStripMenuItem miReadings;
        private SubscriberFilter subscriberFilter;
        private System.Windows.Forms.Panel pnlDialog;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader colZone;
        private System.Windows.Forms.ColumnHeader colDMA;

    }
}