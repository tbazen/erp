using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using INTAPS.ClientServer.Client;
namespace INTAPS.SubscriberManagment.Client
{
    public partial class SubscriptionList : Form
    {
        private bool m_pickerMode = false;

        public Subscription pickedSubscription;
        Type customerProflieType;
        SubscriberSearchResult[] searchResult = null;
        public SubscriptionList()
        {
            this.InitializeComponent();
            this.subscriberFilter.LoadData();
            SubscriberManagmentClient.SubscriberChanged += new SubscriberChangeHandler(this.SubscriberManagmentClient_SubscriberChanged);
            SubscriberManagmentClient.SubscriptionChanged += new SubscriptionChangeHandler(this.SubscriberManagmentClient_SubscriptionChanged);
            customerProflieType = Type.GetType("INTAPS.WSIS.Job.Client.CustomerProfile,JobManagerClientLibrary");
            subscriberFilter.chkExportAll.CheckedChanged+=chkExportAll_CheckedChanged;
            subscriberFilter.ExportTool.setDelegate(new INTAPS.UI.HTML.GenerateHTMLDelegate(getCustomerListHtml));
        }

        private void chkExportAll_CheckedChanged(object sender, EventArgs e)
        {
            if (subscriberFilter.chkExportAll.Checked)
                exportAllPages();
            else
            {
                SubscriberFilterCriteria filter = subscriberFilter.GetSubscriberFilterCriteria;
                if (filter != null)
                {
                    int N;
                    searchResult = SubscriberManagmentClient.GetSubscriptions(filter.query, filter.searchField, filter.multipleVersion, filter.kebele,filter.zone,filter.dma, subscriberFilter.RecordIndex, 100, out N);
                    foreach (SubscriberSearchResult result in searchResult)
                    {
                        if (result.subscription != null)
                            result.subscription.subscriber = result.subscriber;
                    }
                }
            }
        }

        private void exportAllPages()
        {
            SubscriberFilterCriteria filter = subscriberFilter.GetSubscriberFilterCriteria;
            if (filter != null)
            {
                int N;
                searchResult = SubscriberManagmentClient.GetSubscriptions(filter.query, filter.searchField, filter.multipleVersion, filter.kebele,filter.zone,filter.dma, 0, 1000000, out N);
                foreach (SubscriberSearchResult result in searchResult)
                {
                    if (result.subscription != null)
                        result.subscription.subscriber = result.subscriber;
                }
            }
        }

        private string getCustomerListHtml(out string headers)
        {
            string html = "";
            headers = "";
            if (searchResult != null)
                html = SubscriberManagmentClient.htmlDocGetCustomerList(searchResult, out headers);
            return html;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (this.listView.SelectedItems.Count == 0)
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please select a subscription.");
            }
            else
            {
                this.listView_DoubleClick(this.listView, null);
            }
        }


        private void contextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            e.Cancel = this.listView.SelectedItems.Count == 0;
            if (!e.Cancel)
            {
                SubscriberSearchResult sr = (SubscriberSearchResult)this.listView.SelectedItems[0].Tag;
                this.miReadings.Enabled = sr.subscription != null;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }
        

        private void listView_DoubleClick(object sender, EventArgs e)
        {
            if (this.listView.SelectedItems.Count == 1)
            {
                SubscriberSearchResult tag = this.listView.SelectedItems[0].Tag as SubscriberSearchResult;
                if (tag != null)
                {
                    if (this.m_pickerMode)
                    {
                        this.pickedSubscription = tag.subscription;
                        if (this.pickedSubscription != null)
                        {
                            this.pickedSubscription.subscriber = tag.subscriber;
                        }
                        base.DialogResult = DialogResult.OK;
                        base.Close();
                    }
                    else
                    {
                        ShowSubscriber(tag.subscriber.id);
                    }
                }
            }
        }

        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        

        

 
        private void miOpen_Click(object sender, EventArgs e)
        {
            SubscriberSearchResult tag = this.listView.SelectedItems[0].Tag as SubscriberSearchResult;
            if (tag != null)
            {
                ShowSubscriber(tag.subscriber.id);
            }
        }

        private void ShowSubscriber(int customerID)
        {
            if (this.customerProflieType == null)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Customer profile editor not found");
                return;
            }
            else
            {
                Form f = this.customerProflieType.GetConstructor(new Type[] { typeof(int) }).Invoke(new object[] { customerID }) as Form;
                f.Show(UIFormApplicationBase.MainForm);
            }
        }

        private void miReadings_Click(object sender, EventArgs e)
        {
            SubscriberSearchResult tag = (SubscriberSearchResult)this.listView.SelectedItems[0].Tag;
            new MeterReadingBrowser(tag.subscription).Show(INTAPS.UI.UIFormApplicationBase.MainForm);
        }

        private void miViewBills_Click(object sender, EventArgs e)
        {
            SubscriberSearchResult tag = (SubscriberSearchResult)this.listView.SelectedItems[0].Tag;
            BillsViewer f = new BillsViewer(tag.subscriber.id);
            f.Show(INTAPS.UI.UIFormApplicationBase.MainForm);
        }

        public DialogResult PickSubscription()
        {
            this.m_pickerMode = true;
            return base.ShowDialog(UIFormApplicationBase.MainForm);
        }

        private static void SetListViewItem(SubscriberSearchResult s, ListViewItem li)
        {
            li.Text = s.subscriber.customerCode;
            if (s.subscription == null)
            {
                li.SubItems[1].Text = "<No connection>";
                li.SubItems[2].Text = "<No connection>";
            }
            else
            {

                li.SubItems[1].Text = s.subscription.contractNo;
                li.SubItems[2].Text = s.subscription.serialNo;

            }
            li.Tag = s;
            if (s.subscriber == null)
            {
                li.SubItems[3].Text = "<Not avilable>";
            }
            else
            {
                li.SubItems[3].Text = s.subscriber.name;
            }
            if (s.subscription == null)
            {
                li.SubItems[4].Text = "N/A";
                li.SubItems[5].Text = "N/A";
                li.SubItems[6].Text = "N/A";
                li.SubItems[7].Text = "N/A";
                li.SubItems[8].Text = "N/A";
            }
            else
            {
                li.SubItems[4].Text = s.subscription.subscriptionType.ToString();
                li.SubItems[5].Text = s.subscription.subscriptionStatus.ToString();
                if (s.subscription.Kebele != -1)
                {
                    li.SubItems[6].Text = SubscriberManagmentClient.GetKebeleName(s.subscription.Kebele);
                }
                if (s.subscription.dma != -1)
                {
                    DistrictMeteringZone dma= SubscriberManagmentClient.getDMA(s.subscription.dma);
                    li.SubItems[7].Text = dma==null?"":dma.name;
                }
                if (s.subscription.pressureZone != -1)
                {
                    PressureZone zone = SubscriberManagmentClient.getPressureZone(s.subscription.pressureZone);
                    li.SubItems[8].Text = zone == null?"":zone.name;
                }
            }
        }

        private void subscriberFilter_AfterSearch(SubscriberFilter filter, SubscriberSearchResult[] sub)
        {
            if (subscriberFilter.ExportAllPages)
                exportAllPages();
            else
                searchResult = sub;
            this.listView.Items.Clear();
            foreach (SubscriberSearchResult result in sub)
            {
                ListViewItem li = new ListViewItem();
                li.SubItems.AddRange(new string[] { "", "", "", "", "", "","","" });
                SetListViewItem(result, li);
                this.listView.Items.Add(li);
            }
        }

        private void subscriberFilter_Load(object sender, EventArgs e)
        {
        }

        private void SubscriberManagmentClient_SubscriberChanged(DataChange change, Subscriber obj, int objID)
        {
            if (change != DataChange.Create)
            {
                for (int i = this.listView.Items.Count - 1; i >= 0; i--)
                {
                    ListViewItem li = this.listView.Items[i];
                    SubscriberSearchResult tag = (SubscriberSearchResult)li.Tag;
                    if ((tag.subscriber != null) && (tag.subscriber.id == objID))
                    {
                        switch (change)
                        {
                            case DataChange.Delete:
                                li.Remove();
                                break;

                            case DataChange.Update:
                                tag.subscriber = obj;
                                SetListViewItem(tag, li);
                                break;
                        }
                    }
                }
            }
        }

        private void SubscriberManagmentClient_SubscriptionChanged(DataChange change, Subscription obj, int objID)
        {
            if (change != DataChange.Create)
            {
                foreach (ListViewItem item in this.listView.Items)
                {
                    SubscriberSearchResult tag = (SubscriberSearchResult)item.Tag;
                    if ((tag.subscription != null && tag.subscription.id == objID) || (obj != null && tag.subscription == null && tag.subscriber != null && tag.subscriber.id == obj.subscriberID))
                    {
                        switch (change)
                        {
                            case DataChange.Delete:
                                item.Remove();
                                return;

                            case DataChange.Update:
                                tag.subscription = obj;
                                SetListViewItem(tag, item);
                                return;
                        }
                    }
                }
            }
        }

        private void miDelete_Click(object sender, EventArgs e)
        {
            //if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete the selected data?"))
            //    return;
            //try
            //{
            //    SubscriberSearchResult res = (SubscriberSearchResult)listView.SelectedItems[0].Tag;
            //    if (res.subscription != null && res.subscriber == null)
            //        SubscriberManagmentClient.DeleteSubscription(res.subscription.id);
            //    else if (res.subscription == null && res.subscriber != null)
            //        SubscriberManagmentClient.DeleteSubscriber(res.subscriber.id);
            //    else
            //    {
            //        Subscription[] subsc = SubscriberManagmentClient.GetSubscriptions(res.subscriber.id);
            //        if (subsc.Length > 1)
            //            SubscriberManagmentClient.DeleteSubscription(res.subscription.id);
            //        else
            //            SubscriberManagmentClient.DeleteSubscriber(res.subscriber.id);
            //    }

            //}
            //catch (Exception exception)
            //{
            //    UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to delete data.", exception);
            //}
        }

    }
}

