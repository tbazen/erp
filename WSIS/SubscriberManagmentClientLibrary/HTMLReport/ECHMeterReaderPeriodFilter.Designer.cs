﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class ECHMeterReaderPeriodFilter : System.Windows.Forms.UserControl
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.billPeriodSelector = new INTAPS.SubscriberManagment.Client.BillPeriodSelector();
            this.comboReader = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.Control;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(99)))), ((int)(((byte)(99)))));
            this.label3.Location = new System.Drawing.Point(2, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Period:";
            // 
            // billPeriodSelector
            // 
            this.billPeriodSelector.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(99)))), ((int)(((byte)(99)))));
            this.billPeriodSelector.Location = new System.Drawing.Point(61, 3);
            this.billPeriodSelector.Name = "billPeriodSelector";
            this.billPeriodSelector.NullSelection = null;
            this.billPeriodSelector.SelectedPeriod = null;
            this.billPeriodSelector.Size = new System.Drawing.Size(279, 24);
            this.billPeriodSelector.TabIndex = 12;
            // 
            // comboReader
            // 
            this.comboReader.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboReader.Font = new System.Drawing.Font("Microsoft JhengHei UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboReader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(99)))), ((int)(((byte)(99)))));
            this.comboReader.FormattingEnabled = true;
            this.comboReader.Location = new System.Drawing.Point(511, 5);
            this.comboReader.Name = "comboReader";
            this.comboReader.Size = new System.Drawing.Size(187, 22);
            this.comboReader.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(99)))), ((int)(((byte)(99)))));
            this.label1.Location = new System.Drawing.Point(412, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Meter Reader:";
            // 
            // ECHMeterReaderPeriodFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.comboReader);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.billPeriodSelector);
            this.Name = "ECHMeterReaderPeriodFilter";
            this.Size = new System.Drawing.Size(701, 30);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private System.Windows.Forms.Label label3;
        private BillPeriodSelector billPeriodSelector;
        private System.Windows.Forms.ComboBox comboReader;
        private System.Windows.Forms.Label label1;

    }
}