
    using INTAPS.SubscriberManagment;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class ECHMeterReaderPeriodFilter : UserControl
    { 
        private IContainer components = null;
         
        public ECHMeterReaderPeriodFilter()
        {
            this.InitializeComponent();
            this.billPeriodSelector.LoadData(SubscriberManagmentClient.CurrentYear);
            this.comboReader.Items.AddRange(SubscriberManagmentClient.GetAllMeterReaderEmployees(this.billPeriodSelector.SelectedPeriod.id));
            this.billPeriodSelector.SelectionChanged += new EventHandler(this.billPeriodSelector_SelectionChanged);
        }

        private void billPeriodSelector_SelectionChanged(object sender, EventArgs e)
        {
            this.comboReader.Items.Clear();
            this.comboReader.Items.AddRange(SubscriberManagmentClient.GetAllMeterReaderEmployees(this.billPeriodSelector.SelectedPeriod.id));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        public int FilterPeriodID
        {
            get
            {
                if (this.billPeriodSelector.SelectedPeriod == null)
                {
                    return -1;
                }
                return this.billPeriodSelector.SelectedPeriod.id;
            }
        }

        public MeterReaderEmployee Reader
        {
            get
            {
                return (this.comboReader.SelectedItem as MeterReaderEmployee);
            }
        }
    }
}

