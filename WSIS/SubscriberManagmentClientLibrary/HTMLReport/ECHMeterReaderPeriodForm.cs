
    using INTAPS.Accounting.Client;
    using System;
    using System.Windows.Forms;
using System.Text;
using INTAPS.UI.HTML;
namespace INTAPS.SubscriberManagment.Client
{

    public class ECHMeterReaderPeriodForm : EHTMLForm
    {
        protected ECHMeterReaderPeriodFilter m_filter = new ECHMeterReaderPeriodFilter();

        public ECHMeterReaderPeriodForm()
        {
            base.Controls.Add(this.m_filter);
            this.m_filter.BringToFront();
            this.m_filter.Dock = DockStyle.Top;
            base.SetTabDock(DockStyle.Fill);
        }

        public override object[] GetParamters()
        {
            return new object[] { this.m_filter.FilterPeriodID, ((this.m_filter.Reader == null) ? -1 : this.m_filter.Reader.employeeID) };
        }
        public override bool ProcessUrl(string path, System.Collections.Hashtable query)
        {
            try
            {
                switch (path)
                {
                    case "show_reading_map":
                        this.BeginInvoke(new ProcessTowParameter<int, bool>(processShowOnMap), int.Parse((string)query["conID"]),true);
                        return true;

                    default:
                        return false;
                }
            }
            catch (Exception ex)
            {
                this.BeginInvoke(new ShowErrorMesssageDelegate(showErrorMessage), ex);
                return true;
            }
        }
        static void showErrorMessage(Exception ex)
        {
            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error", ex);
        }
        static void processShowOnMap(int connectionID,bool reading)
        {
            SubscriberManagmentClient.locateWaterMeter(connectionID,reading);
        }
    }
}

