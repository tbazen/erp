
    using INTAPS.Accounting;
    using INTAPS.Accounting.Client;
    using System;
namespace INTAPS.SubscriberManagment.Client
{

    public class ECHSalePointSummary : GenericReportClientHandler<SalePointSummary>
    {
        public ECHSalePointSummary(ReportType rt) : base(rt)
        {
        }
    }
    public class ECHPaymentCenterPeriodDateRange : GenericReportClientHandler<RFPaymentCenterDateRangePeriod>
    {
        public ECHPaymentCenterPeriodDateRange(ReportType rt)
            : base(rt)
        {
        }
    }
}

