﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class KebeleFilter : System.Windows.Forms.UserControl
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboKebele = new System.Windows.Forms.ComboBox();
            this.billPeriodSelector = new INTAPS.SubscriberManagment.Client.BillPeriodSelector();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label3.Location = new System.Drawing.Point(17, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Period:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label4.Location = new System.Drawing.Point(364, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 17);
            this.label4.TabIndex = 52;
            this.label4.Text = "Kebele:";
            // 
            // comboKebele
            // 
            this.comboKebele.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboKebele.FormattingEnabled = true;
            this.comboKebele.Location = new System.Drawing.Point(426, 2);
            this.comboKebele.Name = "comboKebele";
            this.comboKebele.Size = new System.Drawing.Size(187, 21);
            this.comboKebele.TabIndex = 53;
            // 
            // billPeriodSelector
            // 
            this.billPeriodSelector.Location = new System.Drawing.Point(72, 3);
            this.billPeriodSelector.Name = "billPeriodSelector";
            this.billPeriodSelector.NullSelection = null;
            this.billPeriodSelector.SelectedPeriod = null;
            this.billPeriodSelector.Size = new System.Drawing.Size(261, 24);
            this.billPeriodSelector.TabIndex = 12;
            // 
            // KebeleFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.comboKebele);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.billPeriodSelector);
            this.Name = "KebeleFilter";
            this.Size = new System.Drawing.Size(751, 30);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private System.Windows.Forms.Label label3;
        private BillPeriodSelector billPeriodSelector;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboKebele;

    }
}