
    using INTAPS.Ethiopic;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
using INTAPS.ClientServer.Client;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class KebeleFilter : UserControl
    {
        
        private IContainer components = null;

        public KebeleFilter()
        {
            this.InitializeComponent();
            if (ApplicationClient.isConnected())
            {
                comboKebele.Items.AddRange(SubscriberManagmentClient.GetAllKebeles());
                if(comboKebele.Items.Count>0)
                    comboKebele.SelectedIndex = 0;
                this.billPeriodSelector.LoadData(SubscriberManagmentClient.CurrentYear);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        public Kebele Kebele
        {
            get
            {
                return comboKebele.SelectedItem as Kebele;
            }
        }


        public int filterPeriodID
        {
            get
            {
                if (this.billPeriodSelector.SelectedPeriod == null)
                {
                    return -1;
                }
                return this.billPeriodSelector.SelectedPeriod.id;
            }
        }
    }
}

