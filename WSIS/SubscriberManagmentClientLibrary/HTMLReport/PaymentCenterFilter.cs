
using INTAPS.Ethiopic;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class PaymentCenterFilter : UserControl
    {


        private IContainer components = null;





        public event EventHandler FilterUpdated;

        public PaymentCenterFilter()
        {
            this.InitializeComponent();
            this.chkTo_CheckedChanged(null, null);
            this.comboPaymentCenters.Items.AddRange(SubscriberManagmentClient.AllPaymentCenters);
            if (this.comboPaymentCenters.Items.Count > 0)
            {
                this.comboPaymentCenters.SelectedIndex = 0;
            }
        }

        private void chkTo_CheckedChanged(object sender, EventArgs e)
        {
            this.dtpTo.Enabled = this.chkTo.Checked;
        }

        private void comboPaymentCenters_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.OnFilterUpdate();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void etdtp_Changed(object sender, EventArgs e)
        {
            this.OnFilterUpdate();
        }


        private void OnFilterUpdate()
        {
            if (this.FilterUpdated != null)
            {
                this.FilterUpdated(this, null);
            }
        }

        public DateTime FilterDateFrom
        {
            get
            {
                return this.dtpFrom.Value.Date;
            }
        }

        public DateTime FilterDateTo
        {
            get
            {
                if (this.chkTo.Checked)
                {
                    return this.dtpTo.Value.AddDays(1.0);
                }
                return this.dtpFrom.Value.Date.AddDays(1.0);
            }
        }

        public int PaymentCenterID
        {
            get
            {
                if (this.comboPaymentCenters.SelectedItem is PaymentCenter)
                {
                    return (this.comboPaymentCenters.SelectedItem as PaymentCenter).id;
                }
                return -1;
            }
        }
    }
}

