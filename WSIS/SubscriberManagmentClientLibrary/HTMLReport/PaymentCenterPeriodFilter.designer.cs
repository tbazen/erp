﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class PaymentCenterPeriodFilter : System.Windows.Forms.UserControl
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpFrom = new INTAPS.Ethiopic.ETDateTimeDropDown();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpTo = new INTAPS.Ethiopic.ETDateTimeDropDown();
            this.chkTo = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboPaymentCenters = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.billPeriodSelector = new INTAPS.SubscriberManagment.Client.BillPeriodSelector();
            this.SuspendLayout();
            // 
            // dtpFrom
            // 
            this.dtpFrom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dtpFrom.Location = new System.Drawing.Point(124, 75);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(221, 20);
            this.dtpFrom.Style = INTAPS.Ethiopic.ETDateStyle.Gregorian;
            this.dtpFrom.TabIndex = 0;
            this.dtpFrom.Changed += new System.EventHandler(this.etdtp_Changed);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.Location = new System.Drawing.Point(3, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "From:";
            // 
            // dtpTo
            // 
            this.dtpTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dtpTo.Location = new System.Drawing.Point(427, 75);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(221, 20);
            this.dtpTo.Style = INTAPS.Ethiopic.ETDateStyle.Gregorian;
            this.dtpTo.TabIndex = 0;
            this.dtpTo.Changed += new System.EventHandler(this.etdtp_Changed);
            // 
            // chkTo
            // 
            this.chkTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkTo.AutoSize = true;
            this.chkTo.Location = new System.Drawing.Point(377, 76);
            this.chkTo.Name = "chkTo";
            this.chkTo.Size = new System.Drawing.Size(44, 19);
            this.chkTo.TabIndex = 2;
            this.chkTo.Text = "To:";
            this.chkTo.UseVisualStyleBackColor = true;
            this.chkTo.CheckedChanged += new System.EventHandler(this.chkTo_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.Location = new System.Drawing.Point(3, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Payment Center:";
            // 
            // comboPaymentCenters
            // 
            this.comboPaymentCenters.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboPaymentCenters.FormattingEnabled = true;
            this.comboPaymentCenters.Location = new System.Drawing.Point(124, 8);
            this.comboPaymentCenters.Name = "comboPaymentCenters";
            this.comboPaymentCenters.Size = new System.Drawing.Size(521, 23);
            this.comboPaymentCenters.TabIndex = 3;
            this.comboPaymentCenters.SelectedIndexChanged += new System.EventHandler(this.comboPaymentCenters_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label3.Location = new System.Drawing.Point(3, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 13;
            this.label3.Text = "Period:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // billPeriodSelector
            // 
            this.billPeriodSelector.Location = new System.Drawing.Point(124, 37);
            this.billPeriodSelector.Name = "billPeriodSelector";
            this.billPeriodSelector.NullSelection = null;
            this.billPeriodSelector.SelectedPeriod = null;
            this.billPeriodSelector.Size = new System.Drawing.Size(304, 24);
            this.billPeriodSelector.TabIndex = 14;
            // 
            // PaymentCenterPeriodFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.billPeriodSelector);
            this.Controls.Add(this.comboPaymentCenters);
            this.Controls.Add(this.chkTo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpTo);
            this.Controls.Add(this.dtpFrom);
            this.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.Name = "PaymentCenterPeriodFilter";
            this.Size = new System.Drawing.Size(662, 103);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private INTAPS.Ethiopic.ETDateTimeDropDown dtpFrom;
        private System.Windows.Forms.Label label1;
        private INTAPS.Ethiopic.ETDateTimeDropDown dtpTo;
        private System.Windows.Forms.CheckBox chkTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboPaymentCenters;
        private System.Windows.Forms.Label label3;
        private BillPeriodSelector billPeriodSelector;
    }

}