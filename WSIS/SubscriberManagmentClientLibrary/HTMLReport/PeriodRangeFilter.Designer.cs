﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class PeriodRangeFilter : System.Windows.Forms.UserControl
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.periodFrom = new INTAPS.SubscriberManagment.Client.BillPeriodSelector();
            this.periodTo = new INTAPS.SubscriberManagment.Client.BillPeriodSelector();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label3.Location = new System.Drawing.Point(11, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 17);
            this.label3.TabIndex = 13;
            this.label3.Text = "From:";
            // 
            // periodFrom
            // 
            this.periodFrom.Location = new System.Drawing.Point(61, 6);
            this.periodFrom.Name = "periodFrom";
            this.periodFrom.NullSelection = null;
            this.periodFrom.SelectedPeriod = null;
            this.periodFrom.Size = new System.Drawing.Size(279, 24);
            this.periodFrom.TabIndex = 14;
            // 
            // periodTo
            // 
            this.periodTo.Location = new System.Drawing.Point(462, 6);
            this.periodTo.Name = "periodTo";
            this.periodTo.NullSelection = null;
            this.periodTo.SelectedPeriod = null;
            this.periodTo.Size = new System.Drawing.Size(279, 24);
            this.periodTo.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label1.Location = new System.Drawing.Point(414, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 17);
            this.label1.TabIndex = 13;
            this.label1.Text = "To:";
            // 
            // PeriodRangeFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.periodTo);
            this.Controls.Add(this.periodFrom);
            this.Name = "PeriodRangeFilter";
            this.Size = new System.Drawing.Size(741, 32);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private System.Windows.Forms.Label label3;
        private BillPeriodSelector periodFrom;
        private BillPeriodSelector periodTo;
        private System.Windows.Forms.Label label1;

    }
}