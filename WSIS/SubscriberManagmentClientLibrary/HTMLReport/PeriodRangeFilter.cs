
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class PeriodRangeFilter : UserControl
    {
        private IContainer components = null;
        
        
        
        

        public PeriodRangeFilter()
        {
            this.InitializeComponent();
            this.periodFrom.LoadData(SubscriberManagmentClient.CurrentYear);
            this.periodTo.LoadData(SubscriberManagmentClient.CurrentYear);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        public int FilterPeriodFromID
        {
            get
            {
                if (this.periodFrom.SelectedPeriod == null)
                {
                    return -1;
                }
                return this.periodFrom.SelectedPeriod.id;
            }
        }

        public int FilterPeriodToID
        {
            get
            {
                if (this.periodTo.SelectedPeriod == null)
                {
                    return -1;
                }
                return this.periodTo.SelectedPeriod.id;
            }
        }
    }
}

