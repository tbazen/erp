
using INTAPS.Accounting.Client;
using System;
using System.Windows.Forms;
using INTAPS.Accounting;
namespace INTAPS.SubscriberManagment.Client
{

    public class RFPKebelePeriod : EHTMLForm
    {
        protected KebeleFilter m_filter = new KebeleFilter();

        public RFPKebelePeriod()
        {
            base.Controls.Add(this.m_filter);
            this.m_filter.BringToFront();
            this.m_filter.Dock = DockStyle.Top;
            base.SetTabDock(DockStyle.Fill);
        }

        public override object[] GetParamters()
        {
            PaymentCenter currentPaymentCenter = SubscriberManagmentClient.GetCurrentPaymentCenter();
            int num = (currentPaymentCenter == null) ? -1 : currentPaymentCenter.id;
            return new object[] { num, this.m_filter.Kebele.id,  this.m_filter.filterPeriodID, this.m_filter.filterPeriodID};
        }
    }
    
}

