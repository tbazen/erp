
using INTAPS.Accounting.Client;
using System;
using System.Windows.Forms;
using INTAPS.Accounting;
namespace INTAPS.SubscriberManagment.Client
{

    public class RFPaymentCenterPeriod : EHTMLForm
    {
        protected PaymentCenterFilter m_filter = new PaymentCenterFilter();

        public RFPaymentCenterPeriod()
        {
            base.Controls.Add(this.m_filter);
            this.m_filter.BringToFront();
            this.m_filter.Dock = DockStyle.Top;
            base.SetTabDock(DockStyle.Fill);
        }

        public override object[] GetParamters()
        {
            return new object[] { this.m_filter.FilterDateFrom, this.m_filter.FilterDateTo, this.m_filter.PaymentCenterID,-1 };
        }
    }
    public class RFPaymentCenterDateRangePeriod : EHTMLForm
    {
        protected PaymentCenterPeriodFilter m_filter = new PaymentCenterPeriodFilter();

        public RFPaymentCenterDateRangePeriod()
        {
            base.Controls.Add(this.m_filter);
            this.m_filter.BringToFront();
            this.m_filter.Dock = DockStyle.Top;
            base.SetTabDock(DockStyle.Fill);
        }

        public override object[] GetParamters()
        {
            return new object[] { this.m_filter.FilterDateFrom, this.m_filter.FilterDateTo, this.m_filter.PaymentCenterID, m_filter.filterPeriodID };
        }
    }
}

