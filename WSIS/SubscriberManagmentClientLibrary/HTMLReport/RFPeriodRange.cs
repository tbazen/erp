
using INTAPS.Accounting.Client;
using System;
using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public class RFPeriodRange : EHTMLForm
    {
        protected PeriodRangeFilter m_filter = new PeriodRangeFilter();

        public RFPeriodRange()
        {
            base.Controls.Add(this.m_filter);
            this.m_filter.BringToFront();
            this.m_filter.Dock = DockStyle.Top;
            base.SetTabDock(DockStyle.Fill);
        }

        public override object[] GetParamters()
        {
            PaymentCenter currentPaymentCenter = SubscriberManagmentClient.GetCurrentPaymentCenter();
            int num = (currentPaymentCenter == null) ? -1 : currentPaymentCenter.id;
            return new object[] { num, -1, this.m_filter.FilterPeriodFromID, this.m_filter.FilterPeriodToID };
        }
    }
}

