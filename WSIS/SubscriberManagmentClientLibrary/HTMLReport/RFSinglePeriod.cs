using INTAPS.Accounting.Client;
using System;
using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public class RFSinglePeriod : EHTMLForm
    {
        protected SinglePeriodFilter m_filter = new SinglePeriodFilter();

        public RFSinglePeriod()
        {
            base.Controls.Add(this.m_filter);
            this.m_filter.BringToFront();
            this.m_filter.Dock = DockStyle.Top;
            base.SetTabDock(DockStyle.Fill);
        }

        public override object[] GetParamters()
        {
            PaymentCenter currentPaymentCenter = SubscriberManagmentClient.GetCurrentPaymentCenter();
            int num = (currentPaymentCenter == null) ? -1 : currentPaymentCenter.id;
            return new object[] { num, -1, this.m_filter.FilterPeriodID, this.m_filter.FilterPeriodID };
        }
    }
}

