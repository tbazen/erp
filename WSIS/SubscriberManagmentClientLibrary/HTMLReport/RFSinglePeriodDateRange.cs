using INTAPS.Accounting.Client;
using System;
using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public class RFSinglePeriodDateRange : EHTMLForm
    {
        protected SinglePeriodDateRangeFilter m_filter = new SinglePeriodDateRangeFilter();

        public RFSinglePeriodDateRange()
        {
            base.Controls.Add(this.m_filter);
            this.m_filter.BringToFront();
            this.m_filter.Dock = DockStyle.Top;
            base.SetTabDock(DockStyle.Fill);
        }

        public override object[] GetParamters()
        {
            PaymentCenter currentPaymentCenter = SubscriberManagmentClient.GetCurrentPaymentCenter();
            int num = (currentPaymentCenter == null) ? -1 : currentPaymentCenter.id;
            return new object[] { num, -1, this.m_filter.filterPeriodID, this.m_filter.filterPeriodID, this.m_filter.dateFrom, this.m_filter.dateTo };
        }
    }
}

