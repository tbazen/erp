
    using INTAPS.Ethiopic;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class SinglePeriodDateRangeFilter : UserControl
    {
        
        private IContainer components = null;
         
        public SinglePeriodDateRangeFilter()
        {
            this.InitializeComponent();
            this.billPeriodSelector.LoadData(SubscriberManagmentClient.CurrentYear);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        public DateTime dateFrom
        {
            get
            {
                return this.dtpFrom.Value.Date;
            }
        }

        public DateTime dateTo
        {
            get
            {
                return this.dtpTo.Value.AddDays(1.0);
            }
        }

        public int filterPeriodID
        {
            get
            {
                if (this.billPeriodSelector.SelectedPeriod == null)
                {
                    return -1;
                }
                return this.billPeriodSelector.SelectedPeriod.id;
            }
        }
    }
}

