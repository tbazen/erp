﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class SinglePeriodFilter : System.Windows.Forms.UserControl
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.billPeriodSelector = new INTAPS.SubscriberManagment.Client.BillPeriodSelector();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label3.Location = new System.Drawing.Point(29, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Period:";
            // 
            // billPeriodSelector
            // 
            this.billPeriodSelector.Location = new System.Drawing.Point(84, 3);
            this.billPeriodSelector.Name = "billPeriodSelector";
            this.billPeriodSelector.NullSelection = null;
            this.billPeriodSelector.SelectedPeriod = null;
            this.billPeriodSelector.Size = new System.Drawing.Size(279, 24);
            this.billPeriodSelector.TabIndex = 12;
            // 
            // SinglePeriodFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.billPeriodSelector);
            this.Name = "SinglePeriodFilter";
            this.Size = new System.Drawing.Size(366, 30);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.Label label3;
        private BillPeriodSelector billPeriodSelector;
    }
}