using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{
    public partial class SinglePeriodFilter : UserControl
    {
        public SinglePeriodFilter()
        {
            this.InitializeComponent();
            this.billPeriodSelector.LoadData(SubscriberManagmentClient.CurrentYear);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        public int FilterPeriodID
        {
            get
            {
                if (this.billPeriodSelector.SelectedPeriod == null)
                {
                    return -1;
                }
                return this.billPeriodSelector.SelectedPeriod.id;
            }
        }
    }
}

