﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class SubscriberFilter : System.Windows.Forms.UserControl
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SubscriberFilter));
            this.pageNavigator = new INTAPS.UI.PageNavigator();
            this.textQuery = new System.Windows.Forms.TextBox();
            this.comboKebeles = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.comboField = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkMultiple = new System.Windows.Forms.CheckBox();
            this.exportTool = new INTAPS.UI.HTML.HTMLExportTool();
            this.chkExportAll = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.comboDMA = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboZone = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pageNavigator
            // 
            this.pageNavigator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pageNavigator.Location = new System.Drawing.Point(473, 85);
            this.pageNavigator.Name = "pageNavigator";
            this.pageNavigator.NRec = 0;
            this.pageNavigator.PageSize = 0;
            this.pageNavigator.RecordIndex = 0;
            this.pageNavigator.Size = new System.Drawing.Size(367, 50);
            this.pageNavigator.TabIndex = 0;
            this.pageNavigator.PageChanged += new System.EventHandler(this.pageNavigator_PageChanged);
            // 
            // textQuery
            // 
            this.textQuery.Location = new System.Drawing.Point(207, 15);
            this.textQuery.Name = "textQuery";
            this.textQuery.Size = new System.Drawing.Size(198, 20);
            this.textQuery.TabIndex = 2;
            this.textQuery.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textQuery_KeyDown);
            // 
            // comboKebeles
            // 
            this.comboKebeles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboKebeles.FormattingEnabled = true;
            this.comboKebeles.Location = new System.Drawing.Point(79, 46);
            this.comboKebeles.Name = "comboKebeles";
            this.comboKebeles.Size = new System.Drawing.Size(223, 21);
            this.comboKebeles.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label2.Location = new System.Drawing.Point(6, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Kebele:";
            // 
            // buttonSearch
            // 
            this.buttonSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSearch.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSearch.ForeColor = System.Drawing.Color.White;
            this.buttonSearch.Image = ((System.Drawing.Image)(resources.GetObject("buttonSearch.Image")));
            this.buttonSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSearch.Location = new System.Drawing.Point(330, 41);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(75, 28);
            this.buttonSearch.TabIndex = 4;
            this.buttonSearch.Text = "&Search";
            this.buttonSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonSearch.UseVisualStyleBackColor = false;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // comboField
            // 
            this.comboField.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboField.FormattingEnabled = true;
            this.comboField.Items.AddRange(new object[] {
            "All",
            "Customer Name",
            "Customer Code",
            "Contract Number",
            "Meter Number",
            "Phone Number"});
            this.comboField.Location = new System.Drawing.Point(79, 15);
            this.comboField.Name = "comboField";
            this.comboField.Size = new System.Drawing.Size(110, 21);
            this.comboField.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Type:";
            // 
            // checkMultiple
            // 
            this.checkMultiple.AutoSize = true;
            this.checkMultiple.Location = new System.Drawing.Point(430, 17);
            this.checkMultiple.Name = "checkMultiple";
            this.checkMultiple.Size = new System.Drawing.Size(139, 17);
            this.checkMultiple.TabIndex = 7;
            this.checkMultiple.Text = "Include old version data";
            this.checkMultiple.UseVisualStyleBackColor = true;
            // 
            // exportTool
            // 
            this.exportTool.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.exportTool.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.exportTool.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exportTool.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.exportTool.ForeColor = System.Drawing.Color.White;
            this.exportTool.Location = new System.Drawing.Point(112, 4);
            this.exportTool.Name = "exportTool";
            this.exportTool.Size = new System.Drawing.Size(120, 32);
            this.exportTool.TabIndex = 8;
            this.exportTool.Load += new System.EventHandler(this.exportTool_Load);
            // 
            // chkExportAll
            // 
            this.chkExportAll.AutoSize = true;
            this.chkExportAll.Location = new System.Drawing.Point(3, 14);
            this.chkExportAll.Name = "chkExportAll";
            this.chkExportAll.Size = new System.Drawing.Size(103, 17);
            this.chkExportAll.TabIndex = 9;
            this.chkExportAll.Text = "Export All Pages";
            this.chkExportAll.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.chkExportAll);
            this.panel1.Controls.Add(this.exportTool);
            this.panel1.Location = new System.Drawing.Point(598, 141);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(239, 41);
            this.panel1.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label3.Location = new System.Drawing.Point(6, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "DMA:";
            // 
            // comboDMA
            // 
            this.comboDMA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDMA.FormattingEnabled = true;
            this.comboDMA.Location = new System.Drawing.Point(79, 73);
            this.comboDMA.Name = "comboDMA";
            this.comboDMA.Size = new System.Drawing.Size(223, 21);
            this.comboDMA.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label4.Location = new System.Drawing.Point(6, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "Pressure Zone:";
            // 
            // comboZone
            // 
            this.comboZone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboZone.FormattingEnabled = true;
            this.comboZone.Location = new System.Drawing.Point(109, 100);
            this.comboZone.Name = "comboZone";
            this.comboZone.Size = new System.Drawing.Size(193, 21);
            this.comboZone.TabIndex = 3;
            // 
            // SubscriberFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.checkMultiple);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboField);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.comboZone);
            this.Controls.Add(this.comboDMA);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboKebeles);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textQuery);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pageNavigator);
            this.Name = "SubscriberFilter";
            this.Size = new System.Drawing.Size(840, 182);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private INTAPS.UI.PageNavigator pageNavigator;
        private System.Windows.Forms.TextBox textQuery;
        private System.Windows.Forms.ComboBox comboKebeles;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.ComboBox comboField;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkMultiple;
        private UI.HTML.HTMLExportTool exportTool;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.CheckBox chkExportAll;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboDMA;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboZone;
    }

}