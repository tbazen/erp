
    using INTAPS.SubscriberManagment;
using INTAPS.UI;
using INTAPS.UI.HTML;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class SubscriberFilter : UserControl
    { 
        private IContainer components = null;
        
        
        private const int PAGE_SIZE = 100;
        
        

        public event SubscriberSearchHandler AfterSearch;

        public event EventHandler BeforeSearch;

        private SubscriberFilterCriteria filterCriteria;
        public SubscriberFilter()
        {
            this.InitializeComponent();
            this.pageNavigator.PageSize = 100;
            comboField.SelectedIndex = 0;
        }
        public HTMLExportTool ExportTool
        {
            get
            {
                return this.exportTool;
            }
        }
        public bool ExportAllPages
        {
            get
            {
                return chkExportAll.Checked && this.pageNavigator.RecordIndex == 0;
            }
        }
        public int RecordIndex
        {
            get
            {
                return this.pageNavigator.RecordIndex;
            }
        }
        public SubscriberFilterCriteria GetSubscriberFilterCriteria
        {
            get
            {
                return filterCriteria;
            }
        }
        private void buttonSearch_Click(object sender, EventArgs e)
        {
            this.pageNavigator.RecordIndex = 0;
            this.UpdateSearch();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        public void LoadData()
        {
            this.comboKebeles.Items.Add("All");
            this.comboKebeles.Items.AddRange(SubscriberManagmentClient.GetAllKebeles());

            this.comboDMA.Items.Add("All");
            this.comboDMA.Items.AddRange(SubscriberManagmentClient.getAllDMAs());

            this.comboZone.Items.Add("All");
            this.comboZone.Items.AddRange(SubscriberManagmentClient.getAllPressureZones());

        }

        private void pageNavigator_PageChanged(object sender, EventArgs e)
        {
            this.UpdateSearch();
        }

        private void textQuery_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.buttonSearch_Click(null, null);
            }
        }

        private void UpdateSearch()
        {
            int kebeleID;
            if (this.comboKebeles.SelectedIndex < 1)
            {
                kebeleID = -1;
            }
            else
            {
                kebeleID = ((Kebele) this.comboKebeles.SelectedItem).id;
            }
            int dma;
            if (this.comboDMA.SelectedIndex < 1)
            {
                dma = -1;
            }
            else
            {
                dma = ((DistrictMeteringZone)this.comboDMA.SelectedItem).id;
            }

            int zone;
            if (this.comboZone.SelectedIndex < 1)
            {
                zone = -1;
            }
            else
            {
                zone = ((PressureZone)this.comboZone.SelectedItem).id;
            }
            if (this.BeforeSearch != null)
            {
                this.BeforeSearch(this, null);
            }
            try
            {
                int num;
                SubscriberSearchField fields=SubscriberSearchField.All;
                switch (comboField.SelectedIndex)
                {
                    case 1: fields = SubscriberSearchField.CustomerName; break;
                    case 2: fields = SubscriberSearchField.CustomerCode; break;
                    case 3: fields = SubscriberSearchField.ConstactNo; break;
                    case 4: fields = SubscriberSearchField.MeterNo; break;
                    case 5: fields = SubscriberSearchField.PhoneNo; break;
                }
                SubscriberSearchResult[] sub = SubscriberManagmentClient.GetSubscriptions(this.textQuery.Text, fields,checkMultiple.Checked,kebeleID,zone,dma, this.pageNavigator.RecordIndex, 100, out num);
                filterCriteria = new SubscriberFilterCriteria
                {
                    query=this.textQuery.Text,
                    searchField=fields,
                    multipleVersion=checkMultiple.Checked,
                    kebele=kebeleID,
                    zone=zone,
                    dma=dma,
                };
                foreach (SubscriberSearchResult result in sub)
                {
                    if(result.subscription!=null)
                        result.subscription.subscriber = result.subscriber;
                }
                this.pageNavigator.NRec = num;
                if (this.AfterSearch != null)
                {
                    this.AfterSearch(this, sub);
                }
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error searching", exception);
            }
        }

        private void exportTool_Load(object sender, EventArgs e)
        {

        }
    }
    public class SubscriberFilterCriteria
    {
        public string query;
        public SubscriberSearchField searchField;
        public bool multipleVersion;
        public int kebele;
        public int zone;
        public int dma;
    }
}

