﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using INTAPS.Accounting;

namespace INTAPS.SubscriberManagment.Client.MobilePayment
{
    public partial class CommisionDepositTransaction : Form
    {
        void initForm()
        {
            InitializeComponent();
            foreach(MeterReaderEmployee e in SubscriberManagmentClient.GetAllMeterReaderEmployees(SubscriberManagmentClient.getCurrentReadingCycle().periodID))
            {
                comboReader.Items.Add(e); 
            }
            if (comboReader.Items.Count > 0)
                comboReader.SelectedIndex = 0;
        }
        public CommisionDepositTransaction()
        {
            initForm();
            updateDefaultAmount();
        }

        private void updateDefaultAmount()
        {
            if (comboReader.SelectedIndex == -1)
                textAmount.Text = "";
            else 
                textAmount.Text = AccountBase.FormatAmount(SubscriberManagmentClient.getSystemDepositFeeBalance( ((MeterReaderEmployee) comboReader.SelectedItem).employeeID, dualCalendar.DateTime));
        }
        int readerID = -1;
        DateTime depositDay;
        public CommisionDepositTransaction(int periodID,int readerID,DateTime day)
        {
            initForm();
            this.readerID = readerID;
            this.depositDay = day;
            
            SystemFeeDeposit deposit = SubscriberManagmentClient.getDeposit(periodID, readerID, day);
            if (deposit == null)
                throw new ClientServer.ServerUserMessage("Deposit information not found");
            dualCalendar.DateTime = deposit.returnDate;
            textAmount.Text = AccountBase.FormatAmount(deposit.amount);
            foreach(MeterReaderEmployee e in comboReader.Items)
            {
                if(e.employeeID==deposit.readerID)
                {
                    comboReader.SelectedItem = e;
                    break;
                }
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if(comboReader.SelectedIndex==-1)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Select reader");
                return;
            }
            double amount;
            if(!INTAPS.UI.Helper.ValidateDoubleTextBox(textAmount,"Please enter valid amount",false,false,out amount))
                return;
            if(this.readerID==-1)
            {
                try
                {
                    SystemFeeDeposit deposit=new SystemFeeDeposit();
                    deposit.readerID=((MeterReaderEmployee)comboReader.SelectedItem).employeeID;
                    deposit.returnDate=this.dualCalendar.DateTime.Date;
                    deposit.amount=amount;
                    deposit.bankRef=deposit.returnDate.Year+"-"+deposit.returnDate.Month.ToString("00")+"-"+deposit.returnDate.Day.ToString("00");
                    SubscriberManagmentClient.addSystemFeeDeposit(deposit);
                    this.DialogResult=DialogResult.OK;
                }
                catch(Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
                }
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void comboReader_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateDefaultAmount();
        }
    }
}
