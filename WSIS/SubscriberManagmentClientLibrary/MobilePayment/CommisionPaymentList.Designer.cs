﻿namespace INTAPS.SubscriberManagment.Client.MobilePayment
{
    partial class CommisionPaymentList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvPayments = new System.Windows.Forms.ListView();
            this.colDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colReader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colAmount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colRef = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkFilterByReader = new System.Windows.Forms.CheckBox();
            this.checkFilterByPeriod = new System.Windows.Forms.CheckBox();
            this.comboReader = new System.Windows.Forms.ComboBox();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonReload = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.billPeriodSelector = new INTAPS.SubscriberManagment.Client.BillPeriodSelector();
            this.buttonReceipt = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvPayments
            // 
            this.lvPayments.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colDate,
            this.colReader,
            this.colAmount,
            this.colRef});
            this.lvPayments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvPayments.FullRowSelect = true;
            this.lvPayments.Location = new System.Drawing.Point(0, 100);
            this.lvPayments.Name = "lvPayments";
            this.lvPayments.Size = new System.Drawing.Size(756, 256);
            this.lvPayments.TabIndex = 0;
            this.lvPayments.UseCompatibleStateImageBehavior = false;
            this.lvPayments.View = System.Windows.Forms.View.Details;
            // 
            // colDate
            // 
            this.colDate.Text = "Date";
            // 
            // colReader
            // 
            this.colReader.Text = "Reader";
            this.colReader.Width = 129;
            // 
            // colAmount
            // 
            this.colAmount.Text = "Amount";
            this.colAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colAmount.Width = 117;
            // 
            // colRef
            // 
            this.colRef.Text = "Bank Reference";
            this.colRef.Width = 168;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkFilterByReader);
            this.panel1.Controls.Add(this.checkFilterByPeriod);
            this.panel1.Controls.Add(this.billPeriodSelector);
            this.panel1.Controls.Add(this.comboReader);
            this.panel1.Controls.Add(this.buttonReceipt);
            this.panel1.Controls.Add(this.buttonAdd);
            this.panel1.Controls.Add(this.buttonDelete);
            this.panel1.Controls.Add(this.buttonEdit);
            this.panel1.Controls.Add(this.buttonReload);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(756, 100);
            this.panel1.TabIndex = 1;
            // 
            // checkFilterByReader
            // 
            this.checkFilterByReader.AutoSize = true;
            this.checkFilterByReader.Location = new System.Drawing.Point(13, 14);
            this.checkFilterByReader.Name = "checkFilterByReader";
            this.checkFilterByReader.Size = new System.Drawing.Size(100, 17);
            this.checkFilterByReader.TabIndex = 4;
            this.checkFilterByReader.Text = "Filter by Reader";
            this.checkFilterByReader.UseVisualStyleBackColor = true;
            // 
            // checkFilterByPeriod
            // 
            this.checkFilterByPeriod.AutoSize = true;
            this.checkFilterByPeriod.Location = new System.Drawing.Point(13, 44);
            this.checkFilterByPeriod.Name = "checkFilterByPeriod";
            this.checkFilterByPeriod.Size = new System.Drawing.Size(95, 17);
            this.checkFilterByPeriod.TabIndex = 4;
            this.checkFilterByPeriod.Text = "Filter by Period";
            this.checkFilterByPeriod.UseVisualStyleBackColor = true;
            // 
            // comboReader
            // 
            this.comboReader.FormattingEnabled = true;
            this.comboReader.Location = new System.Drawing.Point(125, 10);
            this.comboReader.Name = "comboReader";
            this.comboReader.Size = new System.Drawing.Size(279, 21);
            this.comboReader.TabIndex = 2;
            // 
            // buttonDelete
            // 
            this.buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDelete.Location = new System.Drawing.Point(477, 67);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(85, 27);
            this.buttonDelete.TabIndex = 0;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonEdit
            // 
            this.buttonEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEdit.Location = new System.Drawing.Point(568, 67);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(85, 27);
            this.buttonEdit.TabIndex = 0;
            this.buttonEdit.Text = "Edit";
            this.buttonEdit.UseVisualStyleBackColor = true;
            this.buttonEdit.Visible = false;
            // 
            // buttonReload
            // 
            this.buttonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReload.Location = new System.Drawing.Point(659, 67);
            this.buttonReload.Name = "buttonReload";
            this.buttonReload.Size = new System.Drawing.Size(85, 27);
            this.buttonReload.TabIndex = 0;
            this.buttonReload.Text = "Reload";
            this.buttonReload.UseVisualStyleBackColor = true;
            this.buttonReload.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAdd.Location = new System.Drawing.Point(386, 67);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(85, 27);
            this.buttonAdd.TabIndex = 0;
            this.buttonAdd.Text = "Add";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // billPeriodSelector
            // 
            this.billPeriodSelector.Location = new System.Drawing.Point(125, 37);
            this.billPeriodSelector.Name = "billPeriodSelector";
            this.billPeriodSelector.NullSelection = null;
            this.billPeriodSelector.SelectedPeriod = null;
            this.billPeriodSelector.Size = new System.Drawing.Size(279, 24);
            this.billPeriodSelector.TabIndex = 3;
            // 
            // buttonReceipt
            // 
            this.buttonReceipt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReceipt.Location = new System.Drawing.Point(285, 67);
            this.buttonReceipt.Name = "buttonReceipt";
            this.buttonReceipt.Size = new System.Drawing.Size(85, 27);
            this.buttonReceipt.TabIndex = 0;
            this.buttonReceipt.Text = "Receipt";
            this.buttonReceipt.UseVisualStyleBackColor = true;
            this.buttonReceipt.Click += new System.EventHandler(this.buttonReceipt_Click);
            // 
            // CommisionPaymentList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 356);
            this.Controls.Add(this.lvPayments);
            this.Controls.Add(this.panel1);
            this.Name = "CommisionPaymentList";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Commision Payment List";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvPayments;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ColumnHeader colDate;
        private System.Windows.Forms.ColumnHeader colReader;
        private System.Windows.Forms.ColumnHeader colAmount;
        private System.Windows.Forms.ColumnHeader colRef;
        private System.Windows.Forms.Button buttonReload;
        private System.Windows.Forms.ComboBox comboReader;
        private System.Windows.Forms.CheckBox checkFilterByReader;
        private System.Windows.Forms.CheckBox checkFilterByPeriod;
        private BillPeriodSelector billPeriodSelector;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonReceipt;
    }
}