﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using INTAPS.Payroll;

namespace INTAPS.SubscriberManagment.Client.MobilePayment
{
    public partial class CommisionPaymentList : Form
    {
        public CommisionPaymentList()
        {
            InitializeComponent();
            reload();
        }
        void reload()
        {
            try
            {
                lvPayments.Items.Clear();
                foreach(SystemFeeDeposit d in SubscriberManagmentClient.getDeposits(checkFilterByPeriod.Checked?billPeriodSelector.SelectedPeriod.id:-1,checkFilterByReader.Checked?((Employee)comboReader.SelectedItem).id:-1,new DateTime(1900,1,1),DateTime.Now))
                {
                    ListViewItem li = new ListViewItem(d.returnDate.ToString("MMM dd,yyy"));
                    //li.SubItems.Add(SubscriberManagmentClient.GetBillPeriod(d.periodID).name);
                    li.SubItems.Add(Payroll.Client.PayrollClient.GetEmployee(d.readerID).EmployeeNameID);
                    li.SubItems.Add(Accounting.AccountBase.FormatAmount(d.amount));
                    li.SubItems.Add(d.bankRef);
                    li.Tag = d;
                    lvPayments.Items.Add(li);                   
                }
            }
            catch(Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
        private void buttonSave_Click(object sender, EventArgs e)
        {

        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            CommisionDepositTransaction f = new CommisionDepositTransaction();
            if (f.ShowDialog(this) == DialogResult.OK)
                reload();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (lvPayments.SelectedItems.Count == 0)
                return;
            if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete the seleted records?"))
                return;
            try
            {
                foreach(ListViewItem item in lvPayments.SelectedItems )
                {
                    SystemFeeDeposit d = (SystemFeeDeposit)item.Tag;
                    SubscriberManagmentClient.deleteSystemFeeDeposit(d.periodID, d.readerID, d.returnDate);
                }
                reload();
            }
            catch(Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }

        private void buttonReceipt_Click(object sender, EventArgs e)
        {
            if (lvPayments.SelectedItems.Count == 0)
                return;
            try
            {
                SystemFeeDeposit sd=(SystemFeeDeposit)lvPayments.SelectedItems[0].Tag;
                string h;
                String html=INTAPS.Accounting.Client.AccountingClient.EvaluateEHTML("System.MobilePaymentReceipt", new object[]{"readerID",
                new INTAPS.Evaluator.EData(sd.readerID),"ref",new INTAPS.Evaluator.EData(sd.bankRef)
                }, out h);
                SimpleHTMLViewer hv = new SimpleHTMLViewer("Receipt", html, "jobstyle.css");
                hv.Show(this);
            }
            catch(Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
    }
}
