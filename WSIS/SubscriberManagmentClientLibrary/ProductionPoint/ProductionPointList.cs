﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class ProductionPointList : Form
    {
        public ProductionPointList()
        {
            InitializeComponent();
            cmbType.SelectedIndex = 0;
        }

        private void GroupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void RegisterProductionPoint_Load(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //regiser production point
                //ProductionPoint production = new ProductionPoint();
                //production.address = txtAddress.Text;
                //production.remark = txtRemark.Text;
                //production.waterMeterX = txtWaterMeterX

               // SubscriberManagmentClient.registerProductionPoint(production);
              //  MessageBox.Show("registered successfully");
                //  SubscriberManagmentClient.registerProductionPoint(productionPoint)
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            RegisterProductionPoint register = new RegisterProductionPoint();
            if (register.ShowDialog() == DialogResult.OK)
                MessageBox.Show("Production point successfully registered");
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            listProduction.Items.Clear();
            int N;
            ProductionPoint[] productions = SubscriberManagmentClient.getProductionPoint(txtName.Text, -1, 0, cmbType.SelectedIndex, -1, out N);
            foreach (var prod in productions)
            {
                ListViewItem item = new ListViewItem(prod.name);
                item.SubItems.Add(prod.Date.ToShortDateString());
                item.SubItems.Add(prod.kebele.ToString());
                item.SubItems.Add(prod.latitude.ToString());
                item.SubItems.Add(prod.longitude.ToString());
                item.SubItems.Add(prod.address);
                item.SubItems.Add(prod.type.ToString());
                item.SubItems.Add(prod.capacity.ToString());
                item.SubItems.Add(prod.remark);
                item.Tag = prod.id;
                listProduction.Items.Add(item);
            }
        }

        private void btnSetReading_Click(object sender, EventArgs e)
        {
            if (listProduction.SelectedItems.Count == 0)
                MessageBox.Show("Please select production to set new reading");
            else
            {
                ProductionReadingRegistration reading = new ProductionReadingRegistration((int)listProduction.SelectedItems[0].Tag);
                if (reading.ShowDialog() == DialogResult.OK)
                    MessageBox.Show("reading successfully set");
            }
        }
    }
}
