﻿namespace INTAPS.SubscriberManagment.Client
{
    partial class ProductionReadingRegistration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductionReadingRegistration));
            this.Panel4 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.cmbReadingType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dateReading = new BIZNET.iERP.Client.BNDualCalendar();
            this.txtReading = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel4
            // 
            this.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel4.Controls.Add(this.btnClose);
            this.Panel4.Controls.Add(this.btnSave);
            this.Panel4.Location = new System.Drawing.Point(496, 12);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(104, 79);
            this.Panel4.TabIndex = 26;
            // 
            // btnClose
            // 
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClose.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnClose.Image = global::INTAPS.SubscriberManagment.Client.Properties.Resources.Close_32x32;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(10, 41);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(82, 28);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Close";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnSave.Image = global::INTAPS.SubscriberManagment.Client.Properties.Resources.SaveAll_32x32;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(10, 7);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(82, 28);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cmbReadingType
            // 
            this.cmbReadingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbReadingType.FormattingEnabled = true;
            this.cmbReadingType.Items.AddRange(new object[] {
            "Normal",
            "Meter Reset"});
            this.cmbReadingType.Location = new System.Drawing.Point(114, 100);
            this.cmbReadingType.Name = "cmbReadingType";
            this.cmbReadingType.Size = new System.Drawing.Size(351, 21);
            this.cmbReadingType.TabIndex = 31;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.Location = new System.Drawing.Point(8, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 17);
            this.label3.TabIndex = 30;
            this.label3.Text = "Reading Type";
            // 
            // dateReading
            // 
            this.dateReading.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateReading.Location = new System.Drawing.Point(114, 15);
            this.dateReading.Name = "dateReading";
            this.dateReading.ShowEthiopian = true;
            this.dateReading.ShowGregorian = true;
            this.dateReading.ShowTime = false;
            this.dateReading.Size = new System.Drawing.Size(351, 42);
            this.dateReading.TabIndex = 29;
            this.dateReading.VerticalLayout = true;
            // 
            // txtReading
            // 
            this.txtReading.Location = new System.Drawing.Point(114, 66);
            this.txtReading.Name = "txtReading";
            this.txtReading.Size = new System.Drawing.Size(351, 20);
            this.txtReading.TabIndex = 25;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.Location = new System.Drawing.Point(8, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 17);
            this.label1.TabIndex = 23;
            this.label1.Text = "Reading Date";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.Label2.Location = new System.Drawing.Point(10, 65);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(62, 17);
            this.Label2.TabIndex = 17;
            this.Label2.Text = "Reading";
            // 
            // ProductionReadingRegistration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(612, 153);
            this.Controls.Add(this.cmbReadingType);
            this.Controls.Add(this.Panel4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dateReading);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.txtReading);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ProductionReadingRegistration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Production Point Reading Registration";
            this.Panel4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Panel Panel4;
        internal System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cmbReadingType;
        internal System.Windows.Forms.Label label3;
        private BIZNET.iERP.Client.BNDualCalendar dateReading;
        private System.Windows.Forms.TextBox txtReading;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Button btnClose;
    }
}