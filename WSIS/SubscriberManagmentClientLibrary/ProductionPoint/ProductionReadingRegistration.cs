﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class ProductionReadingRegistration : Form
    {
        int m_productionPointID;
        public ProductionReadingRegistration(int productionPointID)
        {
            InitializeComponent();
            m_productionPointID = productionPointID;
            cmbReadingType.SelectedIndex = 0;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                SubscriberManagmentClient.setProductionPointReading(dateReading.DateTime.Date, double.Parse(txtReading.Text), (MeterReadingType)cmbReadingType.SelectedIndex, m_productionPointID);
                this.DialogResult = DialogResult.OK;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
