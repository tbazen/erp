﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class RegisterProductionPoint : Form
    {
        public RegisterProductionPoint()
        {
            InitializeComponent();
            cmbType.SelectedIndex = 0;
            Kebele[] kebeles = SubscriberManagmentClient.GetAllKebeles();
            foreach (var kebele in kebeles)
            {
                cmbKebele.Items.Add(kebele);
            }
            if (cmbKebele.Items.Count > 0)
                cmbKebele.SelectedIndex = 0;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ProductionPoint production = new ProductionPoint();
                production.address = txtAddress.Text;
                production.kebele = ((Kebele)cmbKebele.SelectedItem).id;
                production.capacity = double.Parse(txtCapacity.Text);
                production.Date = date.DateTime.Date;
                production.latitude = txtLatitude.Text == "" ? 0.0 : double.Parse(txtLatitude.Text);
                production.longitude = txtLongitude.Text == "" ? 0.0 : double.Parse(txtLongitude.Text);
                production.name = txtName.Text;
                production.remark = txtRemark.Text;
                production.type = (ProductionPointType)cmbType.SelectedIndex;
                SubscriberManagmentClient.registerProductionPoint(production);
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
