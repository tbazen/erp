﻿namespace INTAPS.SubscriberManagment.Client.BWF
{
    public partial class AnalyzeReading : INTAPS.UI.Windows.AsyncForm
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSetReading = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.labelProgress = new System.Windows.Forms.Label();
            this.periodSelector = new INTAPS.SubscriberManagment.Client.BillPeriodSelector();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonSetReading
            // 
            this.buttonSetReading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonSetReading.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSetReading.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetReading.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonSetReading.ForeColor = System.Drawing.Color.White;
            this.buttonSetReading.Location = new System.Drawing.Point(508, 28);
            this.buttonSetReading.Name = "buttonSetReading";
            this.buttonSetReading.Size = new System.Drawing.Size(127, 28);
            this.buttonSetReading.TabIndex = 6;
            this.buttonSetReading.Text = "&Start";
            this.buttonSetReading.UseVisualStyleBackColor = false;
            this.buttonSetReading.Click += new System.EventHandler(this.buttonSetReading_Click);
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(12, 147);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(623, 13);
            this.progressBar.TabIndex = 5;
            // 
            // labelProgress
            // 
            this.labelProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelProgress.AutoSize = true;
            this.labelProgress.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.labelProgress.ForeColor = System.Drawing.Color.White;
            this.labelProgress.Location = new System.Drawing.Point(12, 127);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(52, 17);
            this.labelProgress.TabIndex = 3;
            this.labelProgress.Text = "Ready:";
            // 
            // periodSelector
            // 
            this.periodSelector.Location = new System.Drawing.Point(12, 32);
            this.periodSelector.Name = "periodSelector";
            this.periodSelector.NullSelection = null;
            this.periodSelector.SelectedPeriod = null;
            this.periodSelector.Size = new System.Drawing.Size(424, 24);
            this.periodSelector.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, -24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Period:";
            // 
            // AnalyzeReading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(656, 171);
            this.Controls.Add(this.buttonSetReading);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.periodSelector);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelProgress);
            this.MaximizeBox = false;
            this.Name = "AnalyzeReading";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Analyze Readings";
            this.Controls.SetChildIndex(this.labelProgress, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.periodSelector, 0);
            this.Controls.SetChildIndex(this.progressBar, 0);
            this.Controls.SetChildIndex(this.buttonSetReading, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private System.Windows.Forms.Button buttonSetReading;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label labelProgress;
        private BillPeriodSelector periodSelector;
        private System.Windows.Forms.Label label1;
    }
}
