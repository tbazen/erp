using INTAPS.Accounting.Client;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI;
using INTAPS.UI.Windows;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using INTAPS.Accounting;
namespace INTAPS.SubscriberManagment.Client.BWF
{

    public partial class AnalyzeReading : AsyncForm
    {
        private IContainer components = null;

        public AnalyzeReading()
        {
            this.InitializeComponent();
            this.periodSelector.LoadData(SubscriberManagmentClient.CurrentYear);
            this.periodSelector.SelectedPeriod = SubscriberManagmentClient.ReadingPeriod;
        }

        private void buttonSetReading_Click(object sender, EventArgs e)
        {
            double num;
            
            base.WaitFor(new AnalyzeReadingJob(this.periodSelector.SelectedPeriod.id));
        }


        protected override void CheckProgress()
        {
            string str;
            this.progressBar.Value = (int)(100.0 * AccountingClient.GetProccessProgress(out str));
            this.labelProgress.Text = str;
            base.CheckProgress();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        protected override void WaitIsOver(ServerDataDownloader d)
        {
            AnalyzeReadingJob job = (AnalyzeReadingJob)d;
            this.progressBar.Value = 0;
            this.labelProgress.Text = "Ready";
            if (job.error != null)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error analyzing readings", job.error);
            }
            else
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Reading analysis succesfully complete.");
            }
            base.WaitIsOver(d);
        }
    }
    internal class AnalyzeReadingJob : ServerDataDownloader
    {
        private int _periodID;
        public Exception error = null;

        public AnalyzeReadingJob(int periodID)
        {
            this._periodID = periodID;
        }

        public override void RunDownloader()
        {
            try
            {
                SubscriberManagmentClient.analyzeReading(_periodID, true);
                this.error = null;
            }
            catch (Exception exception)
            {
                this.error = exception;
            }
        }

        public override string Description
        {
            get
            {
                return "Analyzing Readings";
            }
        }
    }
}