
    using INTAPS.Payroll;
    using INTAPS.Payroll.Client;
    using INTAPS.SubscriberManagment;
    using INTAPS.UI;
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class AssignReadingBlock : Form
    {
        private int _periodID;
        private Employee _reader;
        
        private IContainer components = null;
       
        public AssignReadingBlock(int periodID, int employeeID)
        {
            this.InitializeComponent();
            this._reader = PayrollClient.GetEmployee(employeeID);
            this._periodID = periodID;
            this.Text = "Assign blocks for " + this._reader.EmployeeNameID;
        }

        private void AddBlock(string blockName)
        {
            BWFReadingBlock[] blockArray = SubscriberManagmentClient.GetBlock(this._periodID, blockName);
            if (blockArray.Length == 0)
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Block not found");
            }
            else
            {
                foreach (BWFReadingBlock block in blockArray)
                {
                    bool flag = false;
                    foreach (DataGridViewRow row in (IEnumerable) this.dataGridView.Rows)
                    {
                        BWFReadingBlock tag = row.Tag as BWFReadingBlock;
                        if (tag != null)
                        {
                            if (tag.id == block.id)
                            {
                                UIFormApplicationBase.CurrentAppliation.ShowUserInformation("As " + block.blockName + " already added, it is not added");
                                flag = true;
                                break;
                            }
                            if (this._reader.id == block.readerID)
                            {
                                UIFormApplicationBase.CurrentAppliation.ShowUserInformation("As " + block.blockName + " already assigned to the reader, it is not added");
                                flag = true;
                                break;
                            }
                        }
                    }
                    if (!flag)
                    {
                        int num = this.dataGridView.Rows.Add(new object[] { block.blockName, (block.readerID == -1) ? "<No Reader Assigned>" : PayrollClient.GetEmployee(block.readerID).EmployeeNameID, "" });
                        this.dataGridView.Rows[num].Tag = block;
                    }
                }
            }
        }

        private void AssignReadingBlock_Load(object sender, EventArgs e)
        {
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            int num = 0;
            foreach (DataGridViewRow row in (IEnumerable) this.dataGridView.Rows)
            {
                BWFReadingBlock tag = row.Tag as BWFReadingBlock;
                if (tag != null)
                {
                    tag.readerID = this._reader.id;
                    try
                    {
                        SubscriberManagmentClient.BWFUpdateBlock(tag);
                        row.DefaultCellStyle.BackColor = Color.Green;
                    }
                    catch (Exception exception)
                    {
                        UIFormApplicationBase.CurrentAppliation.HandleException("Error assigning block:" + tag.blockName, exception);
                        row.Cells[2].Value = exception.Message;
                        row.DefaultCellStyle.BackColor = Color.Red;
                        num++;
                    }
                }
            }
            if (num > 0)
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserError(num + " errors");
            }
            else
            {
                base.Close();
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.AddBlock(this.textBlockName.Text);
                this.textBlockName.Text = "";
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to add blocks", exception);
            }
        }

        private void buttonAddRange_Click(object sender, EventArgs e)
        {
            int num;
            int num2;
            string[] strArray = this.textBlockName.Text.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            if (((strArray.Length == 2) && ((strArray[0].Length == strArray[1].Length) && (strArray[0].Length != 0))) && ((int.TryParse(strArray[0], out num) && int.TryParse(strArray[1], out num2)) && (num2 >= num)))
            {
                int length = strArray[0].Length;
                string format = new string('0', length);
                for (int i = num; i <= num2; i++)
                {
                    string blockName = i.ToString(format);
                    try
                    {
                        this.AddBlock(blockName);
                    }
                    catch (Exception exception)
                    {
                        if (!UIFormApplicationBase.CurrentAppliation.UserConfirms("Error adding " + blockName + " do you want to continue?\n" + exception.Message))
                        {
                            return;
                        }
                    }
                }
                this.textBlockName.Text = "";
            }
            else
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserError("Invalid range format it should have the form of eg. 0010-0019");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

