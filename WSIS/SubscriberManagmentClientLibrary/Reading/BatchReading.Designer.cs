﻿namespace INTAPS.SubscriberManagment.Client.BWF
{
    public partial class BatchReading : INTAPS.UI.Windows.AsyncForm
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSetReading = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.labelProgress = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboProblem = new System.Windows.Forms.ComboBox();
            this.listTypes = new System.Windows.Forms.CheckedListBox();
            this.checkAverage = new System.Windows.Forms.CheckBox();
            this.periodSelector = new INTAPS.SubscriberManagment.Client.BillPeriodSelector();
            this.listErrors = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // buttonSetReading
            // 
            this.buttonSetReading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonSetReading.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSetReading.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetReading.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonSetReading.ForeColor = System.Drawing.Color.White;
            this.buttonSetReading.Location = new System.Drawing.Point(485, 103);
            this.buttonSetReading.Name = "buttonSetReading";
            this.buttonSetReading.Size = new System.Drawing.Size(75, 28);
            this.buttonSetReading.TabIndex = 6;
            this.buttonSetReading.Text = "&Set";
            this.buttonSetReading.UseVisualStyleBackColor = false;
            this.buttonSetReading.Click += new System.EventHandler(this.buttonSetReading_Click);
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(233, 386);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(501, 13);
            this.progressBar.TabIndex = 5;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft JhengHei UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.textBox1.Location = new System.Drawing.Point(225, 108);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(254, 21);
            this.textBox1.TabIndex = 4;
            // 
            // labelProgress
            // 
            this.labelProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelProgress.AutoSize = true;
            this.labelProgress.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.labelProgress.ForeColor = System.Drawing.Color.White;
            this.labelProgress.Location = new System.Drawing.Point(179, 382);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(52, 17);
            this.labelProgress.TabIndex = 3;
            this.labelProgress.Text = "Ready:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(179, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Value:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(475, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Problem:";
            // 
            // comboProblem
            // 
            this.comboProblem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboProblem.Font = new System.Drawing.Font("Microsoft JhengHei UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.comboProblem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(99)))), ((int)(((byte)(99)))));
            this.comboProblem.FormattingEnabled = true;
            this.comboProblem.Location = new System.Drawing.Point(551, 13);
            this.comboProblem.Name = "comboProblem";
            this.comboProblem.Size = new System.Drawing.Size(183, 22);
            this.comboProblem.TabIndex = 1;
            // 
            // listTypes
            // 
            this.listTypes.CheckOnClick = true;
            this.listTypes.Dock = System.Windows.Forms.DockStyle.Left;
            this.listTypes.Font = new System.Drawing.Font("Microsoft JhengHei UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listTypes.FormattingEnabled = true;
            this.listTypes.Location = new System.Drawing.Point(0, 0);
            this.listTypes.Name = "listTypes";
            this.listTypes.Size = new System.Drawing.Size(169, 411);
            this.listTypes.TabIndex = 7;
            // 
            // checkAverage
            // 
            this.checkAverage.AutoSize = true;
            this.checkAverage.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.checkAverage.ForeColor = System.Drawing.Color.White;
            this.checkAverage.Location = new System.Drawing.Point(181, 67);
            this.checkAverage.Name = "checkAverage";
            this.checkAverage.Size = new System.Drawing.Size(151, 21);
            this.checkAverage.TabIndex = 8;
            this.checkAverage.Text = "Six Months Average";
            this.checkAverage.UseVisualStyleBackColor = true;
            this.checkAverage.CheckedChanged += new System.EventHandler(this.checkAverage_CheckedChanged);
            // 
            // periodSelector
            // 
            this.periodSelector.Location = new System.Drawing.Point(178, 13);
            this.periodSelector.Name = "periodSelector";
            this.periodSelector.NullSelection = null;
            this.periodSelector.SelectedPeriod = null;
            this.periodSelector.Size = new System.Drawing.Size(279, 24);
            this.periodSelector.TabIndex = 0;
            // 
            // listErrors
            // 
            this.listErrors.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listErrors.FormattingEnabled = true;
            this.listErrors.Location = new System.Drawing.Point(178, 134);
            this.listErrors.Name = "listErrors";
            this.listErrors.Size = new System.Drawing.Size(568, 238);
            this.listErrors.TabIndex = 9;
            // 
            // BatchReading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(758, 411);
            this.Controls.Add(this.listErrors);
            this.Controls.Add(this.checkAverage);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.listTypes);
            this.Controls.Add(this.comboProblem);
            this.Controls.Add(this.buttonSetReading);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.periodSelector);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.MaximizeBox = false;
            this.Name = "BatchReading";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Estimate Readings";
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.labelProgress, 0);
            this.Controls.SetChildIndex(this.periodSelector, 0);
            this.Controls.SetChildIndex(this.progressBar, 0);
            this.Controls.SetChildIndex(this.buttonSetReading, 0);
            this.Controls.SetChildIndex(this.comboProblem, 0);
            this.Controls.SetChildIndex(this.listTypes, 0);
            this.Controls.SetChildIndex(this.textBox1, 0);
            this.Controls.SetChildIndex(this.checkAverage, 0);
            this.Controls.SetChildIndex(this.listErrors, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private System.Windows.Forms.Button buttonSetReading;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label labelProgress;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboProblem;
        private System.Windows.Forms.CheckedListBox listTypes;
        private BillPeriodSelector periodSelector;
        private System.Windows.Forms.CheckBox checkAverage;
        private System.Windows.Forms.ListBox listErrors;
    }
}
