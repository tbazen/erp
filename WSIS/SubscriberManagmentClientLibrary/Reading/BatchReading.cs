
    using INTAPS.Accounting.Client;
    using INTAPS.SubscriberManagment;
    using INTAPS.SubscriberManagment.Client;
    using INTAPS.UI;
    using INTAPS.UI.Windows;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using INTAPS.Accounting;
namespace INTAPS.SubscriberManagment.Client.BWF
{

    public partial class BatchReading : AsyncForm
    { 
        private IContainer components = null;
         
        public BatchReading()
        {
            this.InitializeComponent();
            this.periodSelector.LoadData(SubscriberManagmentClient.CurrentYear);
            this.periodSelector.SelectedPeriod = SubscriberManagmentClient.ReadingPeriod;
            foreach (BWFReadingProblemType type in Enum.GetValues(typeof(BWFReadingProblemType)))
            {
                this.comboProblem.Items.Add(type);
            }
            this.comboProblem.SelectedIndex = 0;
            foreach (SubscriberType type2 in Enum.GetValues(typeof(SubscriberType)))
            {
                this.listTypes.Items.Add(type2);
            }
            this.checkAverage_CheckedChanged(null, null);
        }

        private void buttonSetReading_Click(object sender, EventArgs e)
        {
            double num;
            if (this.checkAverage.Checked)
            {
                num = -1.0;
            }
            else if (!Helper.ValidateDoubleTextBox(this.textBox1, "Please enter valid consumption", false, true, out num))
            {
                return;
            }
            SubscriberType[] typeArray = new SubscriberType[this.listTypes.CheckedItems.Count];
            int num2 = 0;
            foreach (SubscriberType type in this.listTypes.CheckedItems)
            {
                typeArray[num2++] = type;
            }
            listErrors.Items.Clear();

            base.WaitFor(new FixProblemJob(typeArray, (BWFReadingProblemType) this.comboProblem.SelectedItem, this.periodSelector.SelectedPeriod.id, num));
        }

        private void checkAverage_CheckedChanged(object sender, EventArgs e)
        {
            this.textBox1.Enabled = !this.checkAverage.Checked;
        }

        protected override void CheckProgress()
        {
            string str;
            this.progressBar.Value = (int) (100.0 * AccountingClient.GetProccessProgress(out str));
            this.labelProgress.Text = str;
            base.CheckProgress();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        protected override void WaitIsOver(ServerDataDownloader d)
        {
            FixProblemJob job = (FixProblemJob) d;
            this.progressBar.Value = 0;
            this.labelProgress.Text = "Ready";
            if (job.error != null)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error fixing reading problem", job.error);
            }
            else
            {
                if (job.errors == null || job.errors.Count == 0)
                    UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Successfully set readings");
                else
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Fix reading completed with {0} errors.".format(job.errors.Count));
                    listErrors.Items.AddRange(job.errors.ToArray());
                }
            }
            base.WaitIsOver(d);
        }
    }
}

