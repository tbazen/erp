using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class CreateReadingCycle: INTAPS.UI.Windows.AsyncForm
    {
        int _periodID;
        public CreateReadingCycle(int periodID)
        {
            InitializeComponent();
            _periodID = periodID;

        }
        protected override bool ShowAnmiation
        {
            get
            {
                return false;
            }
        }
        protected override void WaitIsOver(INTAPS.UI.Windows.ServerDataDownloader d)
        {
            ///this.Enabled = true;
            CreateReadingCycleProcess dp = (CreateReadingCycleProcess)d;
            if (dp.ex != null)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Failed to create reading cycle", dp.ex);
                this.DialogResult = DialogResult.Cancel;
            }
            else
                this.DialogResult = DialogResult.OK;
            this.Close();
        }
        protected override void CheckProgress()
        {
            string msg1;
            progressBar.Value=(int)(100* 
                INTAPS.Accounting.Client.AccountingClient.GetProccessProgress(out msg1)
                );
            lblStage.Text = msg1;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            //this.Enabled = false;
            base.WaitFor(new CreateReadingCycleProcess(_periodID));
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
    public class CreateReadingCycleProcess : INTAPS.UI.Windows.ServerDataDownloader
    {
        public Exception ex;
        int _periodID;
        public CreateReadingCycleProcess(int periodID)
        {
            _periodID = periodID;
        }


        public override void RunDownloader()
        {
            try
            {
                SubscriberManagment.Client.SubscriberManagmentClient.openReadingCycle(_periodID);
            }
            catch (Exception ex)
            {
                this.ex = ex;
            }
        }

        public override string Description
        {
            get
            {
                return "Creating Reading Cycle";

            }
        }
    }
}