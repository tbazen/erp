
    using INTAPS.Ethiopic;
    using INTAPS.SubscriberManagment;
    using INTAPS.SubscriberManagment.Client;
    using INTAPS.UI;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client.BWF
{

    public partial class CreateReadingSheet : Form
    {
        private int _blockID;
        private int _employeeID;
        private int _periodID;
        public BWFReadingBlock block;

        private IContainer components;

        public CreateReadingSheet(int blockID)
        {
            this.components = null;
            this._blockID = -1;
            this.InitializeComponent();
            this.block = SubscriberManagmentClient.GetBlock(blockID);
            this.textBlock.Text = this.block.blockName;
            this.dtpStart.Value = this.block.readingStart;
            this.dtpEnd.Value = this.block.readingEnd;
            this._blockID = blockID;
            this.Text = "Update Block Information";
            this.buttonCreate.Text = "Update";
        }

        public CreateReadingSheet(int periodID, int employeeID)
        {
            this.components = null;
            this._blockID = -1;
            this.InitializeComponent();
            this._periodID = periodID;
            this._employeeID = employeeID;
            this.Text = "Create New Block";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.DialogResult = DialogResult.Cancel;
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            try
            {
                if (this._blockID == -1)
                {
                    this.block = new BWFReadingBlock();
                    this.block.periodID = this._periodID;
                    this.block.readerID = this._employeeID;
                }
                if (Helper.ValidateNonEmptyTextBox(this.textBlock, "Please enter block name", out this.block.blockName))
                {
                    this.block.readingStart = this.dtpStart.Value;
                    this.block.readingEnd = this.dtpEnd.Value;
                    if (this._blockID == -1)
                    {
                        this.block.id = SubscriberManagmentClient.BWFCreateReadingBlock(this.block);
                    }
                    else
                    {
                        this.block.id = this._blockID;
                        SubscriberManagmentClient.BWFUpdateBlock(this.block);
                    }
                    base.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error creating reading sheet.", exception);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}