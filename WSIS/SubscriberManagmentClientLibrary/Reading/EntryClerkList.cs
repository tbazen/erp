
    using INTAPS.SubscriberManagment;
    using INTAPS.UI;
    using System;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public class EntryClerkList : SimpleObjectList<ReadingEntryClerk>
    {
        protected override ReadingEntryClerk CreateObject()
        {
            ReadingEntryClerkEditor editor = new ReadingEntryClerkEditor();
            if (editor.ShowDialog() == DialogResult.OK)
            {
                return editor.clerk;
            }
            return null;
        }

        protected override void DeleteObject(ReadingEntryClerk obj)
        {
            SubscriberManagmentClient.DeleteEntryClerk(obj.employeeID);
        }

        protected override ReadingEntryClerk[] GetObjects()
        {
            return SubscriberManagmentClient.GetAllReadingEntryClerk();
        }

        protected override string GetObjectString(ReadingEntryClerk obj)
        {
            if (obj == null)
            {
                return "";
            }
            if (obj.emp == null)
            {
                return "error";
            }
            return obj.emp.EmployeeNameID;
        }

        protected override string ObjectTypeName
        {
            get
            {
                return "Reading Entry Clerk";
            }
        }
    }
}

