
    using INTAPS.SubscriberManagment;
    using INTAPS.SubscriberManagment.Client;
    using INTAPS.UI.Windows;
    using System;
namespace INTAPS.SubscriberManagment.Client.BWF
{

    internal class FixProblemJob : ServerDataDownloader
    {
        private double _consumption;
        private int _periodID;
        private BWFReadingProblemType _problem;
        private SubscriberType[] _type;
        public Exception error = null;
        public System.Collections.Generic.List<Exception> errors = null;
        public FixProblemJob(SubscriberType[] type, BWFReadingProblemType problem, int periodID, double consumption)
        {
            this._type = type;
            this._problem = problem;
            this._periodID = periodID;
            this._consumption = consumption;
        }

        public override void RunDownloader()
        {
            try
            {
                SubscriberManagmentClient.BWFBatchCorrectReadings(this._periodID, this._problem, this._type, this._consumption,out errors);
                this.error = null;
            }
            catch (Exception exception)
            {
                this.error = exception;
            }
        }

        public override string Description
        {
            get
            {
                return "Setting readings";
            }
        }
    }
}

