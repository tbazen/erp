
    using INTAPS.SubscriberManagment;
    using INTAPS.UI;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class ImportReadings : Form
    {
        private BWFReadingBlock _destBlock;
        private List<ListViewItem> _searchResult = new List<ListViewItem>();
        private BWFReadingBlock _srcBlock = null;
        
        private IContainer components = null;
        private Color highLightedNodeColor = Color.Red;
        private Font highlightedNodeFont;
         
        public ImportReadings(int destBlock)
        {
            this.InitializeComponent();
            this._destBlock = SubscriberManagmentClient.GetBlock(destBlock);
            this.Text = "Import Readings to " + this._destBlock.blockName;
            this.highlightedNodeFont = new Font(this.listReadings.Font, FontStyle.Underline | FontStyle.Italic);
        }

        private void buttonClearSearch_Click(object sender, EventArgs e)
        {
            this.textSearchBlock.Text = "";
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            try
            {
                ListView.CheckedListViewItemCollection checkedItems = this.listReadings.CheckedItems;
                if (checkedItems.Count == 0)
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select readings");
                }
                else
                {
                    int[] subscriptionID = new int[checkedItems.Count];
                    int index = 0;
                    foreach (ListViewItem item in checkedItems)
                    {
                        subscriptionID[index] = ((BWFMeterReading) item.Tag).subscriptionID;
                        index++;
                    }
                    SubscriberManagmentClient.BWFImportReadings(this._destBlock.id, this._srcBlock.id, subscriptionID);
                    base.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to add blocks", exception);
            }
        }

        private void buttonList_Click(object sender, EventArgs e)
        {
            try
            {
                BWFReadingBlock[] block = SubscriberManagmentClient.GetBlock(this._destBlock.periodID, this.textBlockName.Text);
                if (block.Length == 0)
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Block not found");
                }
                else
                {
                    int num;
                    this._srcBlock = block[0];
                    BWFMeterReading[] readingArray = SubscriberManagmentClient.GetReadings(block[0].id, 0, -1, out num);
                    this.listReadings.Items.Clear();
                    foreach (BWFMeterReading reading in readingArray)
                    {
                        Subscription subscription = SubscriberManagmentClient.GetSubscription(reading.subscriptionID,DateTime.Now.Ticks);
                        ListViewItem item = new ListViewItem(subscription.contractNo);
                        item.SubItems.Add(subscription.subscriber.name);
                        item.SubItems.Add(reading.reading.ToString());
                        this.listReadings.Items.Add(item);
                        item.Tag = reading;
                    }
                }
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to add blocks", exception);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void textBlockName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.buttonList_Click(null, null);
            }
        }

        private void textSearchBlock_TextChanged(object sender, EventArgs e)
        {
            string str = this.textSearchBlock.Text.Trim();
            this._searchResult.Clear();
            foreach (ListViewItem item in this.listReadings.Items)
            {
                if ((str != "") && (item.Text.IndexOf(str, StringComparison.InvariantCultureIgnoreCase) != -1))
                {
                    this._searchResult.Add(item);
                    item.SubItems[0].Font = this.highlightedNodeFont;
                    item.SubItems[0].BackColor = this.highLightedNodeColor;
                }
                else
                {
                    item.SubItems[0].Font = this.listReadings.Font;
                    item.SubItems[0].BackColor = this.listReadings.BackColor;
                }
            }
            if (this._searchResult.Count == 0)
            {
                this.labelResults.Text = "Not Found";
            }
            else
            {
                this._searchResult[0].EnsureVisible();
                if (this._searchResult.Count == 1)
                {
                    this.labelResults.Text = "Unique contact no found";
                }
                else
                {
                    this.labelResults.Text = this._searchResult.Count + " contract numbers found, type more letters to find unique result";
                }
            }
        }
    }
}

