
    using INTAPS.SubscriberManagment.Client;
    using INTAPS.UI.Windows;
    using System;
namespace INTAPS.SubscriberManagment.Client.BWF
{

    public class ImporterProcess : ServerDataDownloader
    {
        private int[] _blocks;
        private int _destPerioID;
        public Exception error = null;

        public ImporterProcess(int destPeriod, int[] blocks)
        {
            this._destPerioID = destPeriod;
            this._blocks = blocks;
        }

        public override void RunDownloader()
        {
            try
            {
                SubscriberManagmentClient.BWFImportReadingBlocks(this._destPerioID, this._blocks);
            }
            catch (Exception exception)
            {
                this.error = exception;
            }
        }

        public override string Description
        {
            get
            {
                return "Importing reading blocks";
            }
        }
    }
}

