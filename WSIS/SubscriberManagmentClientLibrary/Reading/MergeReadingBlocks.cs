
    using INTAPS.Payroll.Client;
    using INTAPS.SubscriberManagment;
    using INTAPS.UI;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class MergeReadingBlocks : Form
    {
        private BWFReadingBlock _destBlock;
        
        
        
        
        
        
        
        private IContainer components = null;
       
        
        
        
        public int[] deletedBlocks;
        
        

        public MergeReadingBlocks(int destBlock)
        {
            this.InitializeComponent();
            this._destBlock = SubscriberManagmentClient.GetBlock(destBlock);
            this.Text = "Import Blocks to " + this._destBlock.blockName;
        }

        private void AddBlock(string blockName)
        {
            BWFReadingBlock[] blockArray = SubscriberManagmentClient.GetBlock(this._destBlock.periodID, blockName);
            if (blockArray.Length == 0)
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Block not found");
            }
            else
            {
                foreach (BWFReadingBlock block in blockArray)
                {
                    bool flag = false;
                    foreach (DataGridViewRow row in (IEnumerable) this.dataGridView.Rows)
                    {
                        BWFReadingBlock tag = row.Tag as BWFReadingBlock;
                        if (tag != null)
                        {
                            if (tag.id == block.id)
                            {
                                UIFormApplicationBase.CurrentAppliation.ShowUserInformation("As " + block.blockName + " already added, it is not added");
                                flag = true;
                                break;
                            }
                            if (this._destBlock.id == block.id)
                            {
                                UIFormApplicationBase.CurrentAppliation.ShowUserInformation("You can't add the destination block");
                                flag = true;
                                break;
                            }
                        }
                    }
                    if (!flag)
                    {
                        int num = this.dataGridView.Rows.Add(new object[] { block.blockName, (block.readerID == -1) ? "<No Reader Assigned>" : PayrollClient.GetEmployee(block.readerID).EmployeeNameID, "" });
                        this.dataGridView.Rows[num].Tag = block;
                    }
                }
            }
        }

        private void AssignReadingBlock_Load(object sender, EventArgs e)
        {
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            List<int> list = new List<int>();
            foreach (DataGridViewRow row in (IEnumerable) this.dataGridView.Rows)
            {
                BWFReadingBlock tag = row.Tag as BWFReadingBlock;
                if (tag != null)
                {
                    list.Add(tag.id);
                }
            }
            try
            {
                SubscriberManagmentClient.BWFMergeBlock(this._destBlock.id, list.ToArray());
                this.deletedBlocks = list.ToArray();
                base.DialogResult = DialogResult.OK;
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error mergking blocks", exception);
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.AddBlock(this.textBlockName.Text);
                this.textBlockName.Text = "";
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to add blocks", exception);
            }
        }

        private void buttonAddRange_Click(object sender, EventArgs e)
        {
            int num;
            int num2;
            string[] strArray = this.textBlockName.Text.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            if (((strArray.Length == 2) && ((strArray[0].Length == strArray[1].Length) && (strArray[0].Length != 0))) && ((int.TryParse(strArray[0], out num) && int.TryParse(strArray[1], out num2)) && (num2 >= num)))
            {
                int length = strArray[0].Length;
                string format = new string('0', length);
                for (int i = num; i <= num2; i++)
                {
                    string blockName = i.ToString(format);
                    try
                    {
                        this.AddBlock(blockName);
                    }
                    catch (Exception exception)
                    {
                        if (!UIFormApplicationBase.CurrentAppliation.UserConfirms("Error adding " + blockName + " do you want to continue?\n" + exception.Message))
                        {
                            return;
                        }
                    }
                }
                this.textBlockName.Text = "";
            }
            else
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserError("Invalid range format it should have the form of eg. 0010-0019");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

