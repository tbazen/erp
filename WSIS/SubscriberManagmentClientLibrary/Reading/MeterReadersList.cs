
    using INTAPS.Payroll.Client;
    using INTAPS.SubscriberManagment;
    using INTAPS.UI;
    using System;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public class MeterReadersList : SimpleObjectList<MeterReaderEmployee>
    {
        private EmployeePicker m_picker = null;
        private BillPeriodSelector m_selector = new BillPeriodSelector();

        public MeterReadersList()
        {
            this.m_selector.LoadData(SubscriberManagmentClient.CurrentYear);
            this.m_selector.SelectedPeriod = SubscriberManagmentClient.ReadingPeriod;
            this.Dock = DockStyle.Top;
            base.Controls.Add(this.m_selector);
            this.m_selector.SelectionChanged += new EventHandler(this.m_selector_SelectionChanged);
            base.ReloadList();
        }

        protected override MeterReaderEmployee CreateObject()
        {
            if (this.m_picker == null)
            {
                this.m_picker = new EmployeePicker();
                this.m_picker.LoadData();
            }
            if (this.m_picker.ShowDialog(this) == DialogResult.OK)
            {
                MeterReaderEmployee employee = new MeterReaderEmployee();
                employee.emp = this.m_picker.PickedEmployee;
                employee.employeeID = this.m_picker.PickedEmployee.ObjectIDVal;
                employee.periodID = this.m_selector.SelectedPeriod.id;
                SubscriberManagmentClient.CreateMeterReaderEmployee(employee);
                return employee;
            }
            return null;
        }

        protected override void DeleteObject(MeterReaderEmployee obj)
        {
            SubscriberManagmentClient.DeleteMeterReaderEmployee(obj.employeeID, obj.periodID);
        }

        protected override MeterReaderEmployee[] GetObjects()
        {
            if (this.m_selector == null)
            {
                return new MeterReaderEmployee[0];
            }
            if (this.m_selector.SelectedPeriod == null)
            {
                return new MeterReaderEmployee[0];
            }
            MeterReaderEmployee[] allMeterReaderEmployees = SubscriberManagmentClient.GetAllMeterReaderEmployees(this.m_selector.SelectedPeriod.id);
            if (allMeterReaderEmployees.Length != 0)
            {
                return allMeterReaderEmployees;
            }
            MeterReaderEmployee[] employeeArray2 = SubscriberManagmentClient.GetAllMeterReaderEmployees(SubscriberManagmentClient.GetPreviousBillPeriod(this.m_selector.SelectedPeriod.id).id);
            if (employeeArray2.Length == 0)
            {
                return allMeterReaderEmployees;
            }
            if (!UIFormApplicationBase.CurrentAppliation.UserConfirms("There are no readers registerd for this period, do you want to import from the previos period?"))
            {
                return allMeterReaderEmployees;
            }
            int num = 0;
            try
            {
                foreach (MeterReaderEmployee employee in employeeArray2)
                {
                    employee.periodID = this.m_selector.SelectedPeriod.id;
                    SubscriberManagmentClient.CreateMeterReaderEmployee(employee);
                }
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException(((num == 0) ? "No reader " : ("Only " + num + " readers ")) + "imported", exception);
            }
            return SubscriberManagmentClient.GetAllMeterReaderEmployees(this.m_selector.SelectedPeriod.id);
        }

        protected override string GetObjectString(MeterReaderEmployee obj)
        {
            if (obj == null)
            {
                return "";
            }
            if (obj.emp == null)
            {
                return "error";
            }
            return obj.emp.EmployeeNameID;
        }

        private void m_selector_SelectionChanged(object sender, EventArgs e)
        {
            base.ReloadList();
        }

        protected override string ObjectTypeName
        {
            get
            {
                return "Meter reading employee";
            }
        }
    }
}

