﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class MeterReadingBrowser : System.Windows.Forms.Form
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.exportTool = new INTAPS.UI.HTML.HTMLExportTool();
            this.chkAllPeriod = new System.Windows.Forms.CheckBox();
            this.subscriptionPlaceHolder = new INTAPS.SubscriberManagment.Client.SubscriptionPlaceHolder();
            this.label1 = new System.Windows.Forms.Label();
            this.pageNavigator = new INTAPS.UI.PageNavigator();
            this.comboKebele = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.comboReader = new System.Windows.Forms.ComboBox();
            this.billPeriodSelector = new INTAPS.SubscriberManagment.Client.BillPeriodSelector();
            this.listView = new System.Windows.Forms.ListView();
            this.colContractNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colPeriod = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colPrevReaderPeriod = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colPrevReading = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colThisReading = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colConsumption = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colReadingDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colReadingType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colBlock = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel1.Controls.Add(this.exportTool);
            this.panel1.Controls.Add(this.chkAllPeriod);
            this.panel1.Controls.Add(this.subscriptionPlaceHolder);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pageNavigator);
            this.panel1.Controls.Add(this.comboKebele);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.btnClear);
            this.panel1.Controls.Add(this.buttonLoad);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.comboReader);
            this.panel1.Controls.Add(this.billPeriodSelector);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(838, 144);
            this.panel1.TabIndex = 2;
            // 
            // exportTool
            // 
            this.exportTool.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exportTool.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.exportTool.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exportTool.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.exportTool.ForeColor = System.Drawing.Color.White;
            this.exportTool.Location = new System.Drawing.Point(733, 44);
            this.exportTool.Name = "exportTool";
            this.exportTool.Size = new System.Drawing.Size(102, 28);
            this.exportTool.TabIndex = 14;
            // 
            // chkAllPeriod
            // 
            this.chkAllPeriod.AutoSize = true;
            this.chkAllPeriod.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.chkAllPeriod.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.chkAllPeriod.Location = new System.Drawing.Point(402, 44);
            this.chkAllPeriod.Name = "chkAllPeriod";
            this.chkAllPeriod.Size = new System.Drawing.Size(87, 21);
            this.chkAllPeriod.TabIndex = 13;
            this.chkAllPeriod.Text = "All Period";
            this.chkAllPeriod.UseVisualStyleBackColor = true;
            this.chkAllPeriod.CheckedChanged += new System.EventHandler(this.chkAllPeriod_CheckedChanged);
            // 
            // subscriptionPlaceHolder
            // 
            this.subscriptionPlaceHolder.Font = new System.Drawing.Font("Microsoft JhengHei UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subscriptionPlaceHolder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(99)))), ((int)(((byte)(99)))));
            this.subscriptionPlaceHolder.Location = new System.Drawing.Point(117, 104);
            this.subscriptionPlaceHolder.Name = "subscriptionPlaceHolder";
            this.subscriptionPlaceHolder.Size = new System.Drawing.Size(279, 21);
            this.subscriptionPlaceHolder.subscription = null;
            this.subscriptionPlaceHolder.TabIndex = 0;
            this.subscriptionPlaceHolder.SubscriptionChanged += new System.EventHandler(this.subscriptionPlaceHolder_SubscriptionChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label1.Location = new System.Drawing.Point(7, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Kebele:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // pageNavigator
            // 
            this.pageNavigator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pageNavigator.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pageNavigator.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(99)))), ((int)(((byte)(99)))));
            this.pageNavigator.Location = new System.Drawing.Point(466, 95);
            this.pageNavigator.Name = "pageNavigator";
            this.pageNavigator.NRec = 0;
            this.pageNavigator.PageSize = 0;
            this.pageNavigator.RecordIndex = 0;
            this.pageNavigator.Size = new System.Drawing.Size(372, 46);
            this.pageNavigator.TabIndex = 9;
            this.pageNavigator.PageChanged += new System.EventHandler(this.pageNavigator_PageChanged);
            // 
            // comboKebele
            // 
            this.comboKebele.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboKebele.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboKebele.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.comboKebele.FormattingEnabled = true;
            this.comboKebele.Location = new System.Drawing.Point(117, 12);
            this.comboKebele.Name = "comboKebele";
            this.comboKebele.Size = new System.Drawing.Size(279, 23);
            this.comboKebele.TabIndex = 12;
            this.comboKebele.SelectedIndexChanged += new System.EventHandler(this.comboKebele_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label4.Location = new System.Drawing.Point(10, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Customer:";
            this.label4.Click += new System.EventHandler(this.label3_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label3.Location = new System.Drawing.Point(7, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Period:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(402, 99);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(58, 28);
            this.btnClear.TabIndex = 8;
            this.btnClear.Text = "&Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // buttonLoad
            // 
            this.buttonLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonLoad.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLoad.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonLoad.ForeColor = System.Drawing.Color.White;
            this.buttonLoad.Location = new System.Drawing.Point(733, 12);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(102, 28);
            this.buttonLoad.TabIndex = 8;
            this.buttonLoad.Text = "&Reload";
            this.buttonLoad.UseVisualStyleBackColor = false;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label2.Location = new System.Drawing.Point(7, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Meter Reader:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // comboReader
            // 
            this.comboReader.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboReader.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboReader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.comboReader.FormattingEnabled = true;
            this.comboReader.Location = new System.Drawing.Point(117, 74);
            this.comboReader.Name = "comboReader";
            this.comboReader.Size = new System.Drawing.Size(279, 23);
            this.comboReader.TabIndex = 2;
            this.comboReader.SelectedIndexChanged += new System.EventHandler(this.comboReader_SelectedIndexChanged);
            // 
            // billPeriodSelector
            // 
            this.billPeriodSelector.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billPeriodSelector.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.billPeriodSelector.Location = new System.Drawing.Point(117, 41);
            this.billPeriodSelector.Name = "billPeriodSelector";
            this.billPeriodSelector.NullSelection = null;
            this.billPeriodSelector.SelectedPeriod = null;
            this.billPeriodSelector.Size = new System.Drawing.Size(284, 26);
            this.billPeriodSelector.TabIndex = 10;
            this.billPeriodSelector.SelectionChanged += new System.EventHandler(this.billPeriodSelector_SelectionChanged);
            this.billPeriodSelector.Load += new System.EventHandler(this.billPeriodSelector_Load);
            // 
            // listView
            // 
            this.listView.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colContractNo,
            this.colName,
            this.colPeriod,
            this.colPrevReaderPeriod,
            this.colPrevReading,
            this.colThisReading,
            this.colConsumption,
            this.colReadingDate,
            this.colReadingType,
            this.colBlock});
            this.listView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(99)))), ((int)(((byte)(99)))));
            this.listView.FullRowSelect = true;
            this.listView.HideSelection = false;
            this.listView.Location = new System.Drawing.Point(0, 144);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(838, 289);
            this.listView.TabIndex = 3;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.Details;
            this.listView.SelectedIndexChanged += new System.EventHandler(this.listView_SelectedIndexChanged);
            this.listView.DoubleClick += new System.EventHandler(this.listView_DoubleClick);
            // 
            // colContractNo
            // 
            this.colContractNo.Text = "Contract No";
            this.colContractNo.Width = 91;
            // 
            // colName
            // 
            this.colName.Text = "Owner";
            this.colName.Width = 133;
            // 
            // colPeriod
            // 
            this.colPeriod.Text = "Period";
            this.colPeriod.Width = 117;
            // 
            // colPrevReaderPeriod
            // 
            this.colPrevReaderPeriod.Text = "Previous Period";
            this.colPrevReaderPeriod.Width = 129;
            // 
            // colPrevReading
            // 
            this.colPrevReading.Text = "Previous Reading";
            this.colPrevReading.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colPrevReading.Width = 91;
            // 
            // colThisReading
            // 
            this.colThisReading.Text = "Reading";
            this.colThisReading.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colThisReading.Width = 91;
            // 
            // colConsumption
            // 
            this.colConsumption.Text = "Consumption";
            this.colConsumption.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colConsumption.Width = 99;
            // 
            // colReadingDate
            // 
            this.colReadingDate.Text = "Reading Date";
            this.colReadingDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colReadingDate.Width = 120;
            // 
            // colReadingType
            // 
            this.colReadingType.Text = "Reading Type";
            this.colReadingType.Width = 100;
            // 
            // colBlock
            // 
            this.colBlock.Text = "Block";
            // 
            // MeterReadingBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(838, 433);
            this.Controls.Add(this.listView);
            this.Controls.Add(this.panel1);
            this.Name = "MeterReadingBrowser";
            this.Text = "Meter Reading Browser";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chkAllPeriod;
        private SubscriptionPlaceHolder subscriptionPlaceHolder;
        private System.Windows.Forms.Label label1;
        private INTAPS.UI.PageNavigator pageNavigator;
        private System.Windows.Forms.ComboBox comboKebele;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboReader;
        private BillPeriodSelector billPeriodSelector;
        private System.Windows.Forms.ListView listView;
        private System.Windows.Forms.ColumnHeader colContractNo;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader colPeriod;
        private System.Windows.Forms.ColumnHeader colPrevReaderPeriod;
        private System.Windows.Forms.ColumnHeader colPrevReading;
        private System.Windows.Forms.ColumnHeader colThisReading;
        private System.Windows.Forms.ColumnHeader colConsumption;
        private System.Windows.Forms.ColumnHeader colReadingDate;
        private System.Windows.Forms.ColumnHeader colReadingType;
        private System.Windows.Forms.ColumnHeader colBlock;
        private UI.HTML.HTMLExportTool exportTool;

    }
}