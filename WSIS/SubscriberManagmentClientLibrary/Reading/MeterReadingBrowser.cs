    using INTAPS.Ethiopic;
    using INTAPS.SubscriberManagment;
    using INTAPS.UI;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
using System.Collections.Generic;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class MeterReadingBrowser : Form
    {

        private IContainer components;

        private bool m_IgnoreEvents;
        private const int PAGE_SIZE = 30;
        int employeeID = -1;

        public MeterReadingBrowser()
        {
            this.m_IgnoreEvents = false;
            this.components = null;
            this.InitializeComponent();
            this.billPeriodSelector.LoadData(SubscriberManagmentClient.CurrentYear);
            this.billPeriodSelector.SelectedPeriod = SubscriberManagmentClient.CurrentPeriod;
            this.comboReader.Items.Add("All");
            this.comboReader.Items.AddRange(SubscriberManagmentClient.GetAllMeterReaderEmployees(this.billPeriodSelector.SelectedPeriod.id));
            if (this.comboReader.Items.Count > 0)
            {
                this.comboReader.SelectedIndex = 0;
            }
            Kebele item = new Kebele();
            item.id = -1;
            item.name = "All";
            this.comboKebele.Items.Add(item);
            this.comboKebele.Items.AddRange(SubscriberManagmentClient.GetAllKebeles());
            this.comboKebele.SelectedIndex = 0;
            this.pageNavigator.PageSize = 30;
        }

        public MeterReadingBrowser(Subscription subsc)
            : this()
        {
            this.subscriptionPlaceHolder.subscription = subsc;
            this.LoadData();
            this.exportTool.setDelegate(new INTAPS.UI.HTML.GenerateHTMLDelegate(getReadingsHtml));
        }
        private string getReadingsHtml(out string headers)
        {
            string html = SubscriberManagmentClient.htmlDocGetReadings(((Kebele)comboKebele.SelectedItem).id, subscriptionPlaceHolder.subscription.id, -1, employeeID, out headers);
            return html;
        }
        private void billPeriodSelector_SelectionChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvents)
            {
                this.btnClear_Click(null, null);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.subscriptionPlaceHolder.subscription = null;
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            this.pageNavigator.RecordIndex = 0;
            this.LoadData();
        }

        private void chkAllPeriod_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvents)
            {
                this.billPeriodSelector.Enabled = !this.chkAllPeriod.Checked;
                this.btnClear_Click(null, null);
            }
        }

        private void comboKebele_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvents)
            {
                this.btnClear_Click(null, null);
            }
        }

        private void comboReader_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvents)
            {
                this.btnClear_Click(null, null);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void label3_Click(object sender, EventArgs e)
        {
        }

        private void listView_DoubleClick(object sender, EventArgs e)
        {
            if (this.listView.SelectedItems.Count == 1)
            {
                BWFMeterReading tag = (BWFMeterReading)this.listView.SelectedItems[0].Tag;
                if (new SingleMeterReadingEntry(tag).ShowDialog() == DialogResult.OK)
                {
                    this.buttonLoad_Click(null, null);
                }
            }
        }

        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void LoadData()
        {
            try
            {
                int num;
                int id;
                BWFMeterReading[] readingArray;
                Cursor.Current = Cursors.WaitCursor;
                if (this.comboReader.SelectedItem is MeterReaderEmployee)
                {
                    employeeID = ((MeterReaderEmployee)this.comboReader.SelectedItem).employeeID;
                }
                else
                {
                    employeeID = -1;
                }
                if (this.chkAllPeriod.Checked)
                {
                    id = -1;
                }
                else
                {
                    id = this.billPeriodSelector.SelectedPeriod.id;
                }
                if (this.subscriptionPlaceHolder.subscription == null)
                {
                    readingArray = SubscriberManagmentClient.BWFGetMeterReadings(((Kebele)this.comboKebele.SelectedItem).id, -1, id, employeeID, this.pageNavigator.RecordIndex, 30, out num);
                }
                else
                {
                    readingArray = SubscriberManagmentClient.BWFGetMeterReadings(-1, this.subscriptionPlaceHolder.GetSubscriptionID(), -1, -1, this.pageNavigator.RecordIndex, 30, out num);
                }
                this.pageNavigator.NRec = num;
                this.listView.Items.Clear();
                Dictionary<int, BWFReadingBlock> blocks = new Dictionary<int, BWFReadingBlock>();
                foreach (BWFMeterReading reading in readingArray)
                {
                    ListViewItem item = new ListViewItem("");
                    item.Tag = reading;
                    this.listView.Items.Add(item);
                    try
                    {
                        Subscription subscription = SubscriberManagmentClient.GetSubscription(reading.subscriptionID, SubscriberManagmentClient.GetBillPeriod(reading.periodID).toDate.Ticks);
                        item.Text = subscription.contractNo;
                        item.SubItems.Add(subscription.subscriber.name);
                        item.SubItems.Add(SubscriberManagmentClient.GetBillPeriod(reading.periodID).name);
                        BWFMeterReading reading2 = SubscriberManagmentClient.BWFGetMeterPreviousReading(subscription.id, reading.periodID);
                        if (reading2 == null)
                        {
                            item.SubItems.Add("");
                            item.SubItems.Add("");
                        }
                        else
                        {
                            item.SubItems.Add(SubscriberManagmentClient.GetBillPeriod(reading2.periodID).name);
                            item.SubItems.Add(reading2.reading.ToString("0.0"));
                        }
                        if (reading.bwfStatus != BWFStatus.Read)
                        {
                            item.SubItems.Add("");
                            item.SubItems.Add("");
                            item.SubItems.Add("");
                            item.SubItems.Add("Not Read");
                        }
                        else
                        {
                            item.SubItems.Add(reading.reading.ToString("0.0"));
                            item.SubItems.Add(reading.consumption.ToString("0.0"));
                            item.SubItems.Add((reading.extraInfo&ReadingExtraInfo.ReadingDate)==ReadingExtraInfo.ReadingDate? EtGrDate.ToEth(reading.readingTime).ToString():"");
                            item.SubItems.Add(reading.readingType.ToString());
                        }
                        
                        if (!blocks.ContainsKey(reading.readingBlockID))
                            blocks.Add(reading.readingBlockID, SubscriberManagmentClient.GetBlock(reading.readingBlockID));
                        item.SubItems.Add(blocks[reading.readingBlockID].blockName);
                    }
                    catch (Exception rowEx)
                    {
                        item.Text = "error";
                    }
                }
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error loading data.", exception);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void miOpen_Click(object sender, EventArgs e)
        {
        }

        private void pageNavigator_PageChanged(object sender, EventArgs e)
        {
            this.LoadData();
        }

        private void subscriptionPlaceHolder_SubscriptionChanged(object sender, EventArgs e)
        {
            this.m_IgnoreEvents = true;
            try
            {
                this.comboKebele.SelectedIndex = 0;
                this.chkAllPeriod.Checked = true;
                this.billPeriodSelector.Enabled = false;
                this.comboReader.SelectedIndex = 0;
                if (this.subscriptionPlaceHolder.subscription != null)
                {
                    this.buttonLoad_Click(null, null);
                }
            }
            finally
            {
                this.m_IgnoreEvents = false;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void billPeriodSelector_Load(object sender, EventArgs e)
        {

        }

        private void btnExport_Click(object sender, EventArgs e)
        {

        }
    }
}