﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class ProposedReadingList : System.Windows.Forms.Form
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkSelectAll = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pageNavigator = new INTAPS.UI.PageNavigator();
            this.comboKebele = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonReject = new System.Windows.Forms.Button();
            this.buttonAccept = new System.Windows.Forms.Button();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.billPeriodSelector = new INTAPS.SubscriberManagment.Client.BillPeriodSelector();
            this.listView = new System.Windows.Forms.ListView();
            this.colContractNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colPeriod = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colPrevReaderPeriod = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colPrevReading = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colThisReading = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colConsumption = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colReadingDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colReadingType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colBlock = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel1.Controls.Add(this.checkSelectAll);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pageNavigator);
            this.panel1.Controls.Add(this.comboKebele);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.buttonReject);
            this.panel1.Controls.Add(this.buttonAccept);
            this.panel1.Controls.Add(this.buttonLoad);
            this.panel1.Controls.Add(this.billPeriodSelector);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(981, 119);
            this.panel1.TabIndex = 2;
            // 
            // checkSelectAll
            // 
            this.checkSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkSelectAll.AutoSize = true;
            this.checkSelectAll.Checked = true;
            this.checkSelectAll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkSelectAll.Location = new System.Drawing.Point(10, 96);
            this.checkSelectAll.Name = "checkSelectAll";
            this.checkSelectAll.Size = new System.Drawing.Size(70, 17);
            this.checkSelectAll.TabIndex = 15;
            this.checkSelectAll.Text = "Select All";
            this.checkSelectAll.UseVisualStyleBackColor = true;
            this.checkSelectAll.CheckedChanged += new System.EventHandler(this.checkSelectAll_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label1.Location = new System.Drawing.Point(7, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Kebele:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // pageNavigator
            // 
            this.pageNavigator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pageNavigator.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pageNavigator.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(99)))), ((int)(((byte)(99)))));
            this.pageNavigator.Location = new System.Drawing.Point(609, 67);
            this.pageNavigator.Name = "pageNavigator";
            this.pageNavigator.NRec = 0;
            this.pageNavigator.PageSize = 0;
            this.pageNavigator.RecordIndex = 0;
            this.pageNavigator.Size = new System.Drawing.Size(372, 46);
            this.pageNavigator.TabIndex = 9;
            this.pageNavigator.PageChanged += new System.EventHandler(this.pageNavigator_PageChanged);
            // 
            // comboKebele
            // 
            this.comboKebele.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboKebele.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboKebele.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.comboKebele.FormattingEnabled = true;
            this.comboKebele.Location = new System.Drawing.Point(117, 12);
            this.comboKebele.Name = "comboKebele";
            this.comboKebele.Size = new System.Drawing.Size(279, 23);
            this.comboKebele.TabIndex = 12;
            this.comboKebele.SelectedIndexChanged += new System.EventHandler(this.comboKebele_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label3.Location = new System.Drawing.Point(7, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Period:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // buttonReject
            // 
            this.buttonReject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReject.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonReject.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonReject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonReject.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonReject.ForeColor = System.Drawing.Color.White;
            this.buttonReject.Location = new System.Drawing.Point(510, 12);
            this.buttonReject.Name = "buttonReject";
            this.buttonReject.Size = new System.Drawing.Size(149, 28);
            this.buttonReject.TabIndex = 8;
            this.buttonReject.Text = "Re&ject Readings";
            this.buttonReject.UseVisualStyleBackColor = false;
            this.buttonReject.Click += new System.EventHandler(this.buttonReject_Click);
            // 
            // buttonAccept
            // 
            this.buttonAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAccept.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonAccept.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonAccept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAccept.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonAccept.ForeColor = System.Drawing.Color.White;
            this.buttonAccept.Location = new System.Drawing.Point(678, 12);
            this.buttonAccept.Name = "buttonAccept";
            this.buttonAccept.Size = new System.Drawing.Size(149, 28);
            this.buttonAccept.TabIndex = 8;
            this.buttonAccept.Text = "&Accept Readings";
            this.buttonAccept.UseVisualStyleBackColor = false;
            this.buttonAccept.Click += new System.EventHandler(this.buttonAccept_Click);
            // 
            // buttonLoad
            // 
            this.buttonLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonLoad.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLoad.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonLoad.ForeColor = System.Drawing.Color.White;
            this.buttonLoad.Location = new System.Drawing.Point(876, 12);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(102, 28);
            this.buttonLoad.TabIndex = 8;
            this.buttonLoad.Text = "&Reload";
            this.buttonLoad.UseVisualStyleBackColor = false;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // billPeriodSelector
            // 
            this.billPeriodSelector.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billPeriodSelector.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.billPeriodSelector.Location = new System.Drawing.Point(117, 41);
            this.billPeriodSelector.Name = "billPeriodSelector";
            this.billPeriodSelector.NullSelection = null;
            this.billPeriodSelector.SelectedPeriod = null;
            this.billPeriodSelector.Size = new System.Drawing.Size(284, 26);
            this.billPeriodSelector.TabIndex = 10;
            this.billPeriodSelector.SelectionChanged += new System.EventHandler(this.billPeriodSelector_SelectionChanged);
            this.billPeriodSelector.Load += new System.EventHandler(this.billPeriodSelector_Load);
            // 
            // listView
            // 
            this.listView.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.listView.CheckBoxes = true;
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colContractNo,
            this.colName,
            this.colPeriod,
            this.colPrevReaderPeriod,
            this.colPrevReading,
            this.colThisReading,
            this.colConsumption,
            this.colReadingDate,
            this.colReadingType,
            this.colBlock});
            this.listView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(99)))), ((int)(((byte)(99)))));
            this.listView.FullRowSelect = true;
            this.listView.HideSelection = false;
            this.listView.Location = new System.Drawing.Point(0, 119);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(981, 314);
            this.listView.TabIndex = 3;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.Details;
            this.listView.SelectedIndexChanged += new System.EventHandler(this.listView_SelectedIndexChanged);
            this.listView.DoubleClick += new System.EventHandler(this.listView_DoubleClick);
            // 
            // colContractNo
            // 
            this.colContractNo.Text = "Contract No";
            this.colContractNo.Width = 91;
            // 
            // colName
            // 
            this.colName.Text = "Owner";
            this.colName.Width = 133;
            // 
            // colPeriod
            // 
            this.colPeriod.Text = "Period";
            this.colPeriod.Width = 117;
            // 
            // colPrevReaderPeriod
            // 
            this.colPrevReaderPeriod.Text = "Previos Period";
            this.colPrevReaderPeriod.Width = 129;
            // 
            // colPrevReading
            // 
            this.colPrevReading.Text = "Previos Reading";
            this.colPrevReading.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colPrevReading.Width = 91;
            // 
            // colThisReading
            // 
            this.colThisReading.Text = "Reading";
            this.colThisReading.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colThisReading.Width = 91;
            // 
            // colConsumption
            // 
            this.colConsumption.Text = "Consumption";
            this.colConsumption.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colConsumption.Width = 99;
            // 
            // colReadingDate
            // 
            this.colReadingDate.Text = "Reading Date";
            this.colReadingDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colReadingDate.Width = 120;
            // 
            // colReadingType
            // 
            this.colReadingType.Text = "Reading Type";
            this.colReadingType.Width = 100;
            // 
            // colBlock
            // 
            this.colBlock.Text = "Block";
            // 
            // ProposedReadingList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(981, 433);
            this.Controls.Add(this.listView);
            this.Controls.Add(this.panel1);
            this.Name = "ProposedReadingList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Proposed Meter Reading List";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private INTAPS.UI.PageNavigator pageNavigator;
        private System.Windows.Forms.ComboBox comboKebele;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonLoad;
        private BillPeriodSelector billPeriodSelector;
        private System.Windows.Forms.ListView listView;
        private System.Windows.Forms.ColumnHeader colContractNo;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader colPeriod;
        private System.Windows.Forms.ColumnHeader colPrevReaderPeriod;
        private System.Windows.Forms.ColumnHeader colPrevReading;
        private System.Windows.Forms.ColumnHeader colThisReading;
        private System.Windows.Forms.ColumnHeader colConsumption;
        private System.Windows.Forms.ColumnHeader colReadingDate;
        private System.Windows.Forms.ColumnHeader colReadingType;
        private System.Windows.Forms.ColumnHeader colBlock;
        private System.Windows.Forms.CheckBox checkSelectAll;
        private System.Windows.Forms.Button buttonReject;
        private System.Windows.Forms.Button buttonAccept;

    }
}