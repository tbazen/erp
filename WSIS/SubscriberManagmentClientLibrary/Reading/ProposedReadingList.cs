    using INTAPS.Ethiopic;
    using INTAPS.SubscriberManagment;
    using INTAPS.UI;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
using System.Collections.Generic;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class ProposedReadingList : Form
    {

        private IContainer components;

        private bool m_IgnoreEvents;
        private const int PAGE_SIZE = 30;
        int employeeID = -1;

        public ProposedReadingList()
        {
            this.m_IgnoreEvents = false;
            this.components = null;
            this.InitializeComponent();
            this.billPeriodSelector.LoadData(SubscriberManagmentClient.CurrentYear);
            this.billPeriodSelector.SelectedPeriod = SubscriberManagmentClient.CurrentPeriod;
            Kebele item = new Kebele();
            item.id = -1;
            item.name = "All";
            this.comboKebele.Items.Add(item);
            this.comboKebele.Items.AddRange(SubscriberManagmentClient.GetAllKebeles());
            this.comboKebele.SelectedIndex = 0;
            this.pageNavigator.PageSize = 30;
        }

        
     
        private void billPeriodSelector_SelectionChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvents)
            {
                this.btnClear_Click(null, null);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            this.pageNavigator.RecordIndex = 0;
            this.LoadData();
        }


        private void comboKebele_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvents)
            {
                this.btnClear_Click(null, null);
            }
        }

        private void comboReader_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvents)
            {
                this.btnClear_Click(null, null);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void label3_Click(object sender, EventArgs e)
        {
        }

        private void listView_DoubleClick(object sender, EventArgs e)
        {
            if (this.listView.SelectedItems.Count == 1)
            {
                BWFMeterReading tag = (BWFMeterReading)this.listView.SelectedItems[0].Tag;
                if (new SingleMeterReadingEntry(tag).ShowDialog() == DialogResult.OK)
                {
                    this.buttonLoad_Click(null, null);
                }
            }
        }

        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void LoadData()
        {
            try
            {
                int num;
                int periodID;
                BWFMeterReading[] readingArray;
                Cursor.Current = Cursors.WaitCursor;

                periodID = this.billPeriodSelector.SelectedPeriod.id;

                readingArray = SubscriberManagmentClient.BWFGetMeterReadings(
                    this.filter
                    , this.pageNavigator.RecordIndex, 30, out num);
                this.pageNavigator.NRec = num;
                this.listView.Items.Clear();
                Dictionary<int, BWFReadingBlock> blocks = new Dictionary<int, BWFReadingBlock>();
                foreach (BWFMeterReading reading in readingArray)
                {
                    ListViewItem item = new ListViewItem("");
                    item.Tag = reading;
                    this.listView.Items.Add(item);
                    try
                    {
                        Subscription subscription = SubscriberManagmentClient.GetSubscription(reading.subscriptionID, SubscriberManagmentClient.GetBillPeriod(reading.periodID).toDate.Ticks);
                        item.Text = subscription.contractNo;
                        item.SubItems.Add(subscription.subscriber.name);
                        item.SubItems.Add(SubscriberManagmentClient.GetBillPeriod(reading.periodID).name);
                        BWFMeterReading reading2 = SubscriberManagmentClient.BWFGetMeterPreviousReading(subscription.id, reading.periodID);
                        if (reading2 == null)
                        {
                            item.SubItems.Add("");
                            item.SubItems.Add("");
                        }
                        else
                        {
                            item.SubItems.Add(SubscriberManagmentClient.GetBillPeriod(reading2.periodID).name);
                            item.SubItems.Add(reading2.reading.ToString("0.0"));
                        }
                        if (reading.bwfStatus != BWFStatus.Read)
                        {
                            item.SubItems.Add("");
                            item.SubItems.Add("");
                            item.SubItems.Add("");
                            item.SubItems.Add("Not Read");
                        }
                        else
                        {
                            item.SubItems.Add(reading.reading.ToString("0.0"));
                            item.SubItems.Add(reading.consumption.ToString("0.0"));
                            item.SubItems.Add((reading.extraInfo & ReadingExtraInfo.ReadingDate) == ReadingExtraInfo.ReadingDate ? EtGrDate.ToEth(reading.readingTime).ToString() : "");
                            item.SubItems.Add(reading.readingType.ToString()+"-"+reading.method);
                        }

                        if (!blocks.ContainsKey(reading.readingBlockID))
                            blocks.Add(reading.readingBlockID, SubscriberManagmentClient.GetBlock(reading.readingBlockID));
                        
                        item.SubItems.Add(blocks[reading.readingBlockID].blockName);
                        item.Checked = checkSelectAll.Checked;
                    }
                    catch (Exception rowEx)
                    {
                        item.Text = "error:"+rowEx.Message;
                    }
                }
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error loading data.", exception);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void miOpen_Click(object sender, EventArgs e)
        {
        }

        private void pageNavigator_PageChanged(object sender, EventArgs e)
        {
            this.LoadData();
        }

        

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void billPeriodSelector_Load(object sender, EventArgs e)
        {

        }

        private void btnExport_Click(object sender, EventArgs e)
        {

        }

        private void checkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem li in listView.Items)
                li.Checked = checkSelectAll.Checked;
        }

        void acceptReject(bool accept)
        {
            try
            {
                MeterReadingFilter filter=null;
                int[] specific=null;
                if (checkSelectAll.Checked)
                {
                    filter = this.filter;
                }
                else
                {
                    specific = new int[listView.CheckedItems.Count];
                    int i=0;
                    foreach (ListViewItem it in listView.CheckedItems)
                        specific[i++] = ((BWFMeterReading)it.Tag).subscriptionID;
                }
                SubscriberManagmentClient.BWFAcceptRejectReading(accept,billPeriodSelector.SelectedPeriod.id,filter,specific);
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Proposed readings succesfully {0}".format(accept ? "Accepted" : "Rejected"));
                buttonLoad_Click(null, null);
            }
            catch(Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
        private void buttonAccept_Click(object sender, EventArgs e)
        {
            acceptReject(true);
        }

        public MeterReadingFilter filter { get
            {
                return new MeterReadingFilter()
                        {
                            readingStatus = new BWFStatus[] { BWFStatus.Proposed },
                            periods = new int[] { billPeriodSelector.SelectedPeriod.id}
                        };
            }
        }

        private void buttonReject_Click(object sender, EventArgs e)
        {
            acceptReject(false);
        }
    }
}