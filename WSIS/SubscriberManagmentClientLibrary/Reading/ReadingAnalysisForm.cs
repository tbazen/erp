
    using INTAPS.SubscriberManagment;
    using INTAPS.UI.HTML;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class ReadingAnalysisForm : Form
    {
        private int _kebele;
        private int _periodid;
         
        private IContainer components = null;
        private string[] Contract;
         
        public ReadingAnalysisForm()
        {
            this.InitializeComponent();
            this.LoadControl();
            this.bowserController.SetBrowser(this.controlBrowser);
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            int num = (int) Enum.Parse(typeof(SubscriberType), (string) this.CboCategory.SelectedItem);
            this._periodid = this.billPeriodSelector.SelectedPeriod.id;
            if (this.comboKebeles.SelectedIndex < 1)
            {
                this._kebele = -1;
            }
            else
            {
                this._kebele = ((Kebele) this.comboKebeles.SelectedItem).id;
            }
            string str = (this.comboKebeles.SelectedIndex < 1) ? ("Kebelle : " + this.comboKebeles.Text) : this.comboKebeles.Text;
            str = str + "<br>Period :" + this.billPeriodSelector.SelectedPeriod.name;
            if (this.radioButton1.Checked)
            {
                this.controlBrowser.LoadTextPage("Reading Analysis", "<h2>" + this.radioButton1.Text + "</h2>" + str + "<Not implemeted>");
            }
            else if (this.radioButton2.Checked)
            {
                this.controlBrowser.LoadTextPage("Reading Analysis", "<h2>" + this.radioButton2.Text + "</h2>" + str + "<Not implemeted>");
            }
            else if (this.radioButton3.Checked)
            {
                this.controlBrowser.LoadTextPage("Reading Analysis", "<h2>" + str + "<Not implemeted>");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void LoadControl()
        {
            this.comboKebeles.Items.Add("All");
            this.comboKebeles.Items.AddRange(SubscriberManagmentClient.GetAllKebeles());
            this.billPeriodSelector.LoadData(SubscriberManagmentClient.CurrentYear);
            this.billPeriodSelector.SelectedPeriod = SubscriberManagmentClient.CurrentPeriod;
            this.comboKebeles.SelectedIndex = 0;
            this.CboCategory.Items.AddRange(Enum.GetNames(typeof(SubscriberType)));
            this.CboCategory.SelectedIndex = 0;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void bowserController_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}

