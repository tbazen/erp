﻿namespace INTAPS.SubscriberManagment.Client
{
    partial class ReadingAnalysisSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textNPeriods = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textLatMin = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textLngMin = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textLatMax = new System.Windows.Forms.TextBox();
            this.textLngMax = new System.Windows.Forms.TextBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Number of Reading Periods to Analyse:";
            // 
            // textNPeriods
            // 
            this.textNPeriods.Location = new System.Drawing.Point(237, 17);
            this.textNPeriods.Name = "textNPeriods";
            this.textNPeriods.Size = new System.Drawing.Size(208, 20);
            this.textNPeriods.TabIndex = 1;
            this.textNPeriods.Text = "3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Lower Bound of Lattitude:";
            // 
            // textLatMin
            // 
            this.textLatMin.Location = new System.Drawing.Point(237, 52);
            this.textLatMin.Name = "textLatMin";
            this.textLatMin.Size = new System.Drawing.Size(208, 20);
            this.textLatMin.TabIndex = 1;
            this.textLatMin.Text = "-90";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Lower Bound of Longituide:";
            // 
            // textLngMin
            // 
            this.textLngMin.Location = new System.Drawing.Point(237, 78);
            this.textLngMin.Name = "textLngMin";
            this.textLngMin.Size = new System.Drawing.Size(208, 20);
            this.textLngMin.TabIndex = 1;
            this.textLngMin.Text = "-180";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Upper Bound of Lattitude:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 133);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Upper Bound of Longituide:";
            // 
            // textLatMax
            // 
            this.textLatMax.Location = new System.Drawing.Point(237, 104);
            this.textLatMax.Name = "textLatMax";
            this.textLatMax.Size = new System.Drawing.Size(208, 20);
            this.textLatMax.TabIndex = 1;
            this.textLatMax.Text = "90";
            // 
            // textLngMax
            // 
            this.textLngMax.Location = new System.Drawing.Point(237, 130);
            this.textLngMax.Name = "textLngMax";
            this.textLngMax.Size = new System.Drawing.Size(208, 20);
            this.textLngMax.TabIndex = 1;
            this.textLngMax.Text = "180";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(353, 180);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(92, 32);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(255, 180);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(92, 32);
            this.buttonOk.TabIndex = 2;
            this.buttonOk.Text = "Ok";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // ReadingAnalysisSetting
            // 
            this.AcceptButton = this.buttonOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(466, 224);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.textLngMax);
            this.Controls.Add(this.textLngMin);
            this.Controls.Add(this.textLatMax);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textLatMin);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textNPeriods);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ReadingAnalysisSetting";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reading Analysis Setting";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textNPeriods;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textLatMin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textLngMin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textLatMax;
        private System.Windows.Forms.TextBox textLngMax;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOk;
    }
}