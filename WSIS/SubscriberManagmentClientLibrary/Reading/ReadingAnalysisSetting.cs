﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class ReadingAnalysisSetting : Form
    {
        public ReadingAnalysisSetting()
        {
            InitializeComponent();
            ReadingAnalysisMethod method = SubscriberManagmentClient.GetSystemParameter("analysisMethod") as ReadingAnalysisMethod;
            if(method!=null)
            {
                textNPeriods.Text = method.nPeriod.ToString();
                textLatMin.Text = method.minLat.ToString();
                textLngMin.Text = method.minLng.ToString();
                textLatMax.Text = method.maxlat.ToString();
                textLngMax.Text = method.maxLng.ToString();
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                ReadingAnalysisMethod method = new ReadingAnalysisMethod();
                if(!int.TryParse(textNPeriods.Text,out method.nPeriod) || method.nPeriod<2)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter valid number of periods. Should be at least 2");
                    return;
                }
                
                if (!double.TryParse(textLatMin.Text, out method.minLat) || method.minLat < -90 || method.minLat > 90)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter valid lower bound for latitude");
                    return;
                }

                if (!double.TryParse(textLngMin.Text, out method.minLng) || method.minLng < -180 || method.minLng > 180)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter valid lower bound for longitud");
                    return;
                }

                if (!double.TryParse(textLatMax.Text, out method.maxlat) || method.maxlat < -90 || method.maxlat > 90 || method.maxlat<method.minLat)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter valid upper bound for latitude");
                    return;
                }

                if (!double.TryParse(textLngMax.Text, out method.maxLng) || method.maxLng < -180 || method.maxLng > 180 || method.maxLng < method.minLng)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter valid upper bound for longitud");
                    return;
                }
                SubscriberManagmentClient.SetSystemParameters(new string[] { "analysisMethod" }, new object[] { method });
                this.Close();

            }
            catch(Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
