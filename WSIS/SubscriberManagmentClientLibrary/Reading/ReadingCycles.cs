﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class ReadingCycles : Form
    {
        public ReadingCycles()
        {
            InitializeComponent();
            billPeriodSelector.LoadData(SubscriberManagmentClient.CurrentYear);
            loadCycle();
        }
        void loadCycle()
        {
            ReadingCycle cycle = SubscriberManagmentClient.getReadingCycle(billPeriodSelector.SelectedPeriod.id);
            buttonCreate.Enabled = cycle == null;
            buttonClose.Enabled = cycle != null && cycle.status == ReadingCycleStatus.Open;
            buttonDelete.Enabled = cycle != null && cycle.status == ReadingCycleStatus.Open;
            if (cycle == null)
                labelStatus.Text = "";
            else
            {
                labelStatus.Text = string.Format("Created on {0}{1}", AccountBase.FormatDate(cycle.openTime), cycle.status == ReadingCycleStatus.Open ? "" : " closed on " + AccountBase.FormatDate(cycle.closeTime));
            }
        }
        private void buttonClose_Click(object sender, EventArgs e)
        {
            if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to close this reading cycle"))
                return;
                
            try
            {
                SubscriberManagmentClient.closeReadingCycle(billPeriodSelector.SelectedPeriod.id);
                loadCycle();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            try
            {
                CreateReadingCycle c = new CreateReadingCycle(billPeriodSelector.SelectedPeriod.id);
                c.ShowDialog(this);
                loadCycle();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete this reading cycle"))
                return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                SubscriberManagmentClient.deleteReadingCycle(billPeriodSelector.SelectedPeriod.id);
            }
            catch (Exception ex)
            {

                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

        }

        private void billPeriodSelector_SelectionChanged(object sender, EventArgs e)
        {
            loadCycle();
        }

        private void ReadingCycles_Load(object sender, EventArgs e)
        {

        }
    }
}
