
    using INTAPS.ClientServer.Client;
    using INTAPS.Payroll.Client;
    using INTAPS.SubscriberManagment;
    using INTAPS.UI;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class ReadingEntryClerkEditor : Form
    {
        
        
        public ReadingEntryClerk clerk = null;
        
        private IContainer components = null;
        
        
        

        public ReadingEntryClerkEditor()
        {
            this.InitializeComponent();
            this.clerk = new ReadingEntryClerk();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                this.clerk.employeeID = this.employeeCashier.GetEmployeeID();
                if (this.clerk.employeeID == -1)
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please select the clerk");
                }
                else
                {
                    this.clerk.emp = this.employeeCashier.employee;
                    SubscriberManagmentClient.CreateReadingEntryClerk(this.clerk);
                    base.DialogResult = DialogResult.OK;
                    base.Close();
                }
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't save payment center infromation.", exception);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

