﻿namespace INTAPS.SubscriberManagment.Client.BWF
{
    public partial class ReadingSheetBrowser : INTAPS.UI.Windows.AsyncForm
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.treeView = new System.Windows.Forms.TreeView();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miCreateBlock = new System.Windows.Forms.ToolStripMenuItem();
            this.miDeleteBlock = new System.Windows.Forms.ToolStripMenuItem();
            this.miEditBlock = new System.Windows.Forms.ToolStripMenuItem();
            this.assignReadingBlockToAnotherEmployeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel = new System.Windows.Forms.Panel();
            this.labelResults = new System.Windows.Forms.Label();
            this.textSearchBlock = new System.Windows.Forms.TextBox();
            this.buttonClearSearch = new System.Windows.Forms.Button();
            this.dataGridView = new INTAPS.UI.EDataGridView();
            this.colContractNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAverageMonth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReading = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReadingType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPreviousMonth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPreviosReading = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colConsumption = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDelete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panelBlockInfo = new System.Windows.Forms.Panel();
            this.linkImportReadings = new System.Windows.Forms.LinkLabel();
            this.linkImport = new System.Windows.Forms.LinkLabel();
            this.linkEdit = new System.Windows.Forms.LinkLabel();
            this.buttonSurvey = new System.Windows.Forms.Button();
            this.textBlockInformation = new System.Windows.Forms.TextBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.panelNavigation = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonImportReadingSheets = new System.Windows.Forms.Button();
            this.pageNavigator = new INTAPS.UI.PageNavigator();
            this.billPeriodSelector = new INTAPS.SubscriberManagment.Client.BillPeriodSelector();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlDialog = new System.Windows.Forms.Panel();
            this.checkBoxSelectAll = new System.Windows.Forms.CheckBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.panelBlockInfo.SuspendLayout();
            this.panelNavigation.SuspendLayout();
            this.pnlDialog.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.Location = new System.Drawing.Point(0, 53);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.treeView);
            this.splitContainer.Panel1.Controls.Add(this.panel);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.dataGridView);
            this.splitContainer.Panel2.Controls.Add(this.panelBlockInfo);
            this.splitContainer.Size = new System.Drawing.Size(898, 337);
            this.splitContainer.SplitterDistance = 269;
            this.splitContainer.TabIndex = 0;
            // 
            // treeView
            // 
            this.treeView.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.treeView.ContextMenuStrip = this.contextMenuStrip;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeView.FullRowSelect = true;
            this.treeView.HideSelection = false;
            this.treeView.Location = new System.Drawing.Point(0, 51);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(269, 286);
            this.treeView.TabIndex = 0;
            this.treeView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterCheck);
            this.treeView.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeView_BeforeSelect);
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
            this.treeView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeView_MouseDown);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCreateBlock,
            this.miDeleteBlock,
            this.miEditBlock,
            this.assignReadingBlockToAnotherEmployeeToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(193, 92);
            this.contextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip_Opening);
            // 
            // miCreateBlock
            // 
            this.miCreateBlock.Name = "miCreateBlock";
            this.miCreateBlock.Size = new System.Drawing.Size(192, 22);
            this.miCreateBlock.Text = "Create Reading Block";
            this.miCreateBlock.Click += new System.EventHandler(this.miCreateBlock_Click);
            // 
            // miDeleteBlock
            // 
            this.miDeleteBlock.Name = "miDeleteBlock";
            this.miDeleteBlock.Size = new System.Drawing.Size(192, 22);
            this.miDeleteBlock.Text = "Delete Reading Block";
            this.miDeleteBlock.Click += new System.EventHandler(this.miDeleteBlock_Click);
            // 
            // miEditBlock
            // 
            this.miEditBlock.Name = "miEditBlock";
            this.miEditBlock.Size = new System.Drawing.Size(192, 22);
            this.miEditBlock.Text = "Edit Reading Block";
            // 
            // assignReadingBlockToAnotherEmployeeToolStripMenuItem
            // 
            this.assignReadingBlockToAnotherEmployeeToolStripMenuItem.Name = "assignReadingBlockToAnotherEmployeeToolStripMenuItem";
            this.assignReadingBlockToAnotherEmployeeToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.assignReadingBlockToAnotherEmployeeToolStripMenuItem.Text = "Assign Reading Blocks";
            this.assignReadingBlockToAnotherEmployeeToolStripMenuItem.Click += new System.EventHandler(this.assignReadingBlockToAnotherEmployeeToolStripMenuItem_Click);
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel.Controls.Add(this.labelResults);
            this.panel.Controls.Add(this.textSearchBlock);
            this.panel.Controls.Add(this.buttonClearSearch);
            this.panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(269, 51);
            this.panel.TabIndex = 1;
            // 
            // labelResults
            // 
            this.labelResults.AutoSize = true;
            this.labelResults.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.labelResults.Location = new System.Drawing.Point(10, 30);
            this.labelResults.Name = "labelResults";
            this.labelResults.Size = new System.Drawing.Size(68, 17);
            this.labelResults.TabIndex = 2;
            this.labelResults.Text = "No Result";
            // 
            // textSearchBlock
            // 
            this.textSearchBlock.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textSearchBlock.Location = new System.Drawing.Point(13, 7);
            this.textSearchBlock.Name = "textSearchBlock";
            this.textSearchBlock.Size = new System.Drawing.Size(190, 20);
            this.textSearchBlock.TabIndex = 1;
            this.textSearchBlock.TextChanged += new System.EventHandler(this.textSearchBlock_TextChanged);
            this.textSearchBlock.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textSearchBlock_KeyDown);
            // 
            // buttonClearSearch
            // 
            this.buttonClearSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClearSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonClearSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClearSearch.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonClearSearch.ForeColor = System.Drawing.Color.White;
            this.buttonClearSearch.Location = new System.Drawing.Point(209, 2);
            this.buttonClearSearch.Name = "buttonClearSearch";
            this.buttonClearSearch.Size = new System.Drawing.Size(57, 28);
            this.buttonClearSearch.TabIndex = 0;
            this.buttonClearSearch.Text = "&Clear";
            this.buttonClearSearch.UseVisualStyleBackColor = false;
            this.buttonClearSearch.Click += new System.EventHandler(this.buttonClearSearch_Click);
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colContractNo,
            this.colName,
            this.colAverageMonth,
            this.colReading,
            this.colReadingType,
            this.colPreviousMonth,
            this.colPreviosReading,
            this.colConsumption,
            this.colDelete});
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.HorizontalEnterNavigationMode = true;
            this.dataGridView.Location = new System.Drawing.Point(0, 86);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.Size = new System.Drawing.Size(625, 251);
            this.dataGridView.SuppessEnterKey = true;
            this.dataGridView.TabIndex = 3;
            this.dataGridView.EnterComit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_EnterComit);
            this.dataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellClick);
            this.dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellContentClick);
            this.dataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellDoubleClick);
            this.dataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellEndEdit);
            this.dataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView_DataError);
            this.dataGridView.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView_RowPostPaint);
            this.dataGridView.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridView_RowsAdded);
            this.dataGridView.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridView_RowValidating);
            this.dataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView_KeyDown);
            // 
            // colContractNo
            // 
            this.colContractNo.HeaderText = "Conatract No";
            this.colContractNo.Name = "colContractNo";
            this.colContractNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colName
            // 
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colAverageMonth
            // 
            this.colAverageMonth.HeaderText = "Average Month";
            this.colAverageMonth.Name = "colAverageMonth";
            this.colAverageMonth.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colReading
            // 
            dataGridViewCellStyle1.Format = "N0";
            dataGridViewCellStyle1.NullValue = null;
            this.colReading.DefaultCellStyle = dataGridViewCellStyle1;
            this.colReading.HeaderText = "Reading";
            this.colReading.Name = "colReading";
            this.colReading.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colReadingType
            // 
            this.colReadingType.HeaderText = "Reading Type";
            this.colReadingType.Name = "colReadingType";
            this.colReadingType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colReadingType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colPreviousMonth
            // 
            this.colPreviousMonth.HeaderText = "Previos Month";
            this.colPreviousMonth.Name = "colPreviousMonth";
            this.colPreviousMonth.ReadOnly = true;
            this.colPreviousMonth.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colPreviosReading
            // 
            this.colPreviosReading.HeaderText = "Previos Reading";
            this.colPreviosReading.Name = "colPreviosReading";
            this.colPreviosReading.ReadOnly = true;
            this.colPreviosReading.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colConsumption
            // 
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.colConsumption.DefaultCellStyle = dataGridViewCellStyle2;
            this.colConsumption.HeaderText = "Consumption";
            this.colConsumption.Name = "colConsumption";
            this.colConsumption.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colDelete
            // 
            this.colDelete.HeaderText = "";
            this.colDelete.Name = "colDelete";
            this.colDelete.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colDelete.Width = 30;
            // 
            // panelBlockInfo
            // 
            this.panelBlockInfo.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panelBlockInfo.Controls.Add(this.linkImportReadings);
            this.panelBlockInfo.Controls.Add(this.linkImport);
            this.panelBlockInfo.Controls.Add(this.linkEdit);
            this.panelBlockInfo.Controls.Add(this.buttonSurvey);
            this.panelBlockInfo.Controls.Add(this.textBlockInformation);
            this.panelBlockInfo.Controls.Add(this.progressBar);
            this.panelBlockInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelBlockInfo.Location = new System.Drawing.Point(0, 0);
            this.panelBlockInfo.Name = "panelBlockInfo";
            this.panelBlockInfo.Size = new System.Drawing.Size(625, 86);
            this.panelBlockInfo.TabIndex = 0;
            // 
            // linkImportReadings
            // 
            this.linkImportReadings.AutoSize = true;
            this.linkImportReadings.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.linkImportReadings.Location = new System.Drawing.Point(293, 63);
            this.linkImportReadings.Name = "linkImportReadings";
            this.linkImportReadings.Size = new System.Drawing.Size(114, 17);
            this.linkImportReadings.TabIndex = 6;
            this.linkImportReadings.TabStop = true;
            this.linkImportReadings.Text = "Import Readings";
            this.linkImportReadings.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkImportReadings_LinkClicked);
            // 
            // linkImport
            // 
            this.linkImport.AutoSize = true;
            this.linkImport.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.linkImport.Location = new System.Drawing.Point(208, 63);
            this.linkImport.Name = "linkImport";
            this.linkImport.Size = new System.Drawing.Size(94, 17);
            this.linkImport.TabIndex = 6;
            this.linkImport.TabStop = true;
            this.linkImport.Text = "Import Blocks";
            this.linkImport.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkImport_LinkClicked);
            // 
            // linkEdit
            // 
            this.linkEdit.AutoSize = true;
            this.linkEdit.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.linkEdit.Location = new System.Drawing.Point(126, 63);
            this.linkEdit.Name = "linkEdit";
            this.linkEdit.Size = new System.Drawing.Size(98, 17);
            this.linkEdit.TabIndex = 5;
            this.linkEdit.TabStop = true;
            this.linkEdit.Text = "Edit Block Info";
            this.linkEdit.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkEdit_LinkClicked);
            // 
            // buttonSurvey
            // 
            this.buttonSurvey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSurvey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonSurvey.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSurvey.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonSurvey.ForeColor = System.Drawing.Color.White;
            this.buttonSurvey.Location = new System.Drawing.Point(13, 56);
            this.buttonSurvey.Name = "buttonSurvey";
            this.buttonSurvey.Size = new System.Drawing.Size(93, 28);
            this.buttonSurvey.TabIndex = 5;
            this.buttonSurvey.Text = "&Survey Data";
            this.buttonSurvey.UseVisualStyleBackColor = false;
            // 
            // textBlockInformation
            // 
            this.textBlockInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBlockInformation.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBlockInformation.Location = new System.Drawing.Point(13, 7);
            this.textBlockInformation.Multiline = true;
            this.textBlockInformation.Name = "textBlockInformation";
            this.textBlockInformation.ReadOnly = true;
            this.textBlockInformation.Size = new System.Drawing.Size(600, 44);
            this.textBlockInformation.TabIndex = 4;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(397, 63);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(213, 13);
            this.progressBar.TabIndex = 3;
            // 
            // panelNavigation
            // 
            this.panelNavigation.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panelNavigation.Controls.Add(this.button1);
            this.panelNavigation.Controls.Add(this.buttonImportReadingSheets);
            this.panelNavigation.Controls.Add(this.pageNavigator);
            this.panelNavigation.Controls.Add(this.billPeriodSelector);
            this.panelNavigation.Controls.Add(this.label3);
            this.panelNavigation.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelNavigation.Location = new System.Drawing.Point(0, 0);
            this.panelNavigation.Name = "panelNavigation";
            this.panelNavigation.Size = new System.Drawing.Size(898, 53);
            this.panelNavigation.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(443, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 28);
            this.button1.TabIndex = 4;
            this.button1.Text = "&Reload";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonImportReadingSheets
            // 
            this.buttonImportReadingSheets.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonImportReadingSheets.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonImportReadingSheets.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonImportReadingSheets.ForeColor = System.Drawing.Color.White;
            this.buttonImportReadingSheets.Location = new System.Drawing.Point(362, 6);
            this.buttonImportReadingSheets.Name = "buttonImportReadingSheets";
            this.buttonImportReadingSheets.Size = new System.Drawing.Size(75, 28);
            this.buttonImportReadingSheets.TabIndex = 4;
            this.buttonImportReadingSheets.Text = "&Import";
            this.buttonImportReadingSheets.UseVisualStyleBackColor = false;
            this.buttonImportReadingSheets.Click += new System.EventHandler(this.buttonImportReadingSheets_Click);
            // 
            // pageNavigator
            // 
            this.pageNavigator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pageNavigator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pageNavigator.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.pageNavigator.Location = new System.Drawing.Point(528, 6);
            this.pageNavigator.Name = "pageNavigator";
            this.pageNavigator.NRec = 0;
            this.pageNavigator.PageSize = 3000;
            this.pageNavigator.RecordIndex = 0;
            this.pageNavigator.Size = new System.Drawing.Size(362, 47);
            this.pageNavigator.TabIndex = 3;
            this.pageNavigator.PageChanged += new System.EventHandler(this.pageNavigator_PageChanged);
            // 
            // billPeriodSelector
            // 
            this.billPeriodSelector.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.billPeriodSelector.Location = new System.Drawing.Point(64, 10);
            this.billPeriodSelector.Name = "billPeriodSelector";
            this.billPeriodSelector.NullSelection = null;
            this.billPeriodSelector.SelectedPeriod = null;
            this.billPeriodSelector.Size = new System.Drawing.Size(276, 24);
            this.billPeriodSelector.TabIndex = 2;
            this.billPeriodSelector.SelectionChanged += new System.EventHandler(this.billPeriodSelector_SelectionChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label3.Location = new System.Drawing.Point(10, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Period:";
            // 
            // pnlDialog
            // 
            this.pnlDialog.BackColor = System.Drawing.Color.DarkOrange;
            this.pnlDialog.Controls.Add(this.checkBoxSelectAll);
            this.pnlDialog.Controls.Add(this.btnOk);
            this.pnlDialog.Controls.Add(this.btnCancel);
            this.pnlDialog.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlDialog.Location = new System.Drawing.Point(0, 390);
            this.pnlDialog.Name = "pnlDialog";
            this.pnlDialog.Size = new System.Drawing.Size(898, 49);
            this.pnlDialog.TabIndex = 4;
            this.pnlDialog.Visible = false;
            // 
            // checkBoxSelectAll
            // 
            this.checkBoxSelectAll.AutoSize = true;
            this.checkBoxSelectAll.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.checkBoxSelectAll.Location = new System.Drawing.Point(16, 16);
            this.checkBoxSelectAll.Name = "checkBoxSelectAll";
            this.checkBoxSelectAll.Size = new System.Drawing.Size(84, 21);
            this.checkBoxSelectAll.TabIndex = 6;
            this.checkBoxSelectAll.Text = "Select All";
            this.checkBoxSelectAll.UseVisualStyleBackColor = true;
            this.checkBoxSelectAll.CheckedChanged += new System.EventHandler(this.checkBoxSelectAll_CheckedChanged);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(721, 14);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(81, 28);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(818, 14);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(65, 28);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Conatract No";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Average Month";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle3.Format = ",.0";
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn4.HeaderText = "Reading";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Reading Type";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Previos Month";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Previos Reading";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle4.Format = "#.0";
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn8.HeaderText = "Consumption";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ReadingSheetBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 439);
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.panelNavigation);
            this.Controls.Add(this.pnlDialog);
            this.Name = "ReadingSheetBrowser";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reading Sheet Browser";
            this.Controls.SetChildIndex(this.pnlDialog, 0);
            this.Controls.SetChildIndex(this.panelNavigation, 0);
            this.Controls.SetChildIndex(this.splitContainer, 0);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.ResumeLayout(false);
            this.contextMenuStrip.ResumeLayout(false);
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.panelBlockInfo.ResumeLayout(false);
            this.panelBlockInfo.PerformLayout();
            this.panelNavigation.ResumeLayout(false);
            this.panelNavigation.PerformLayout();
            this.pnlDialog.ResumeLayout(false);
            this.pnlDialog.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem miCreateBlock;
        private System.Windows.Forms.ToolStripMenuItem miDeleteBlock;
        private System.Windows.Forms.ToolStripMenuItem miEditBlock;
        private System.Windows.Forms.ToolStripMenuItem assignReadingBlockToAnotherEmployeeToolStripMenuItem;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Label labelResults;
        private System.Windows.Forms.TextBox textSearchBlock;
        private System.Windows.Forms.Button buttonClearSearch;
        private INTAPS.UI.EDataGridView dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn colContractNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAverageMonth;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReading;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReadingType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPreviousMonth;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPreviosReading;
        private System.Windows.Forms.DataGridViewTextBoxColumn colConsumption;
        private System.Windows.Forms.DataGridViewButtonColumn colDelete;
        private System.Windows.Forms.Panel panelBlockInfo;
        private System.Windows.Forms.LinkLabel linkImportReadings;
        private System.Windows.Forms.LinkLabel linkImport;
        private System.Windows.Forms.LinkLabel linkEdit;
        private System.Windows.Forms.Button buttonSurvey;
        private System.Windows.Forms.TextBox textBlockInformation;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Panel panelNavigation;
        private System.Windows.Forms.Button buttonImportReadingSheets;
        private INTAPS.UI.PageNavigator pageNavigator;
        private BillPeriodSelector billPeriodSelector;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel pnlDialog;
        private System.Windows.Forms.CheckBox checkBoxSelectAll;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.Button button1;

    }
}