
    using INTAPS.Accounting.Client;
    using INTAPS.Payroll.Client;
    using INTAPS.SubscriberManagment;
    using INTAPS.SubscriberManagment.Client;
    using INTAPS.UI;
    using INTAPS.UI.Windows;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using INTAPS.Accounting;
namespace INTAPS.SubscriberManagment.Client.BWF
{

    public partial class ReadingSheetBrowser : AsyncForm
    {
        private bool _importMode;
        private List<TreeNode> _searchResult = new List<TreeNode>();
         
        private const int COL_AVG_COUNT = 2;
        private const int COL_CN = 0;
        private const int COL_CONSUMPTION = 7;
        private const int COL_NAME = 1;
        private const int COL_PREV_MONTH = 5;
        private const int COL_PREV_READING = 6;
        private const int COL_READING = 3;
        private const int COL_READING_TYPE = 4;
         
        private Color highLightedNodeColor = Color.Red;
        private Font highlightedNodeFont;
         
        private bool m_ignoreEvents = false;
        private BWFReadingBlock m_selectedBlock = null;
         
        private bool rowModified;
        public int[] selectedBlocks;

        public ReadingSheetBrowser(bool importMode)
        {
            this.InitializeComponent();
            this.billPeriodSelector.LoadData(SubscriberManagmentClient.CurrentYear);
            this.billPeriodSelector.SelectedPeriod = SubscriberManagmentClient.ReadingPeriod;
            this._importMode = importMode;
            this.pnlDialog.Visible = this._importMode;
            this.treeView.CheckBoxes = this._importMode;
            this.buttonImportReadingSheets.Visible = !this._importMode;
            this.LoadTree(-1);
            this.InitGrid();
            this.highlightedNodeFont = new Font(this.treeView.Font, FontStyle.Underline | FontStyle.Italic);
            this.treeView_AfterSelect(null, null);
        }
        public ReadingSheetBrowser(int periodID,bool importMode):this(importMode)
        {
            this.billPeriodSelector.SelectedPeriod = SubscriberManagmentClient.GetBillPeriod(periodID);
            LoadTree(-1);
            
        }
        private void assignReadingBlockToAnotherEmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MeterReaderEmployee tag = this.treeView.SelectedNode.Tag as MeterReaderEmployee;
            if (tag != null)
            {
                try
                {
                    new AssignReadingBlock(this.billPeriodSelector.SelectedPeriod.id, tag.employeeID).ShowDialog(this);
                    this.LoadTree((this.m_selectedBlock == null) ? -1 : this.m_selectedBlock.id);
                }
                catch (Exception exception)
                {
                    UIFormApplicationBase.CurrentAppliation.HandleException("Error loading form", exception);
                }
            }
        }

        private void billPeriodSelector_SelectionChanged(object sender, EventArgs e)
        {
            this.LoadTree(-1);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.DialogResult = DialogResult.Cancel;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            List<int> list = new List<int>();
            foreach (TreeNode node in this.treeView.Nodes)
            {
                foreach (TreeNode node2 in node.Nodes)
                {
                    if (node2.Checked)
                    {
                        list.Add(((BWFReadingBlock) node2.Tag).id);
                    }
                }
            }
            this.selectedBlocks = list.ToArray();
            base.DialogResult = DialogResult.OK;
        }

        private void buttonClearSearch_Click(object sender, EventArgs e)
        {
            this.textSearchBlock.Text = "";
        }

        private void buttonImportReadingSheets_Click(object sender, EventArgs e)
        {
            ReadingSheetBrowser browser = new ReadingSheetBrowser(true);
            if (browser.ShowDialog() == DialogResult.OK)
            {
                this.progressBar.Maximum = 100;
                this.progressBar.Value = 0;
                base.WaitFor(new ImporterProcess(this.billPeriodSelector.SelectedPeriod.id, browser.selectedBlocks));
            }
        }

       

        private void checkBoxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.m_ignoreEvents)
            {
                this.m_ignoreEvents = true;
                try
                {
                    foreach (TreeNode node in this.treeView.Nodes)
                    {
                        node.Checked = this.checkBoxSelectAll.Checked;
                        foreach (TreeNode node2 in node.Nodes)
                        {
                            node2.Checked = this.checkBoxSelectAll.Checked;
                        }
                    }
                }
                finally
                {
                    this.m_ignoreEvents = false;
                }
            }
        }

        protected override void CheckProgress()
        {
            string str;
            base.CheckProgress();
            this.progressBar.Value = (int) (100.0 * AccountingClient.GetProccessProgress(out str));
        }

        private void contextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            TreeNode selectedNode = this.treeView.SelectedNode;
            this.miCreateBlock.Enabled = selectedNode != null;
            this.miEditBlock.Enabled = (selectedNode != null) && (selectedNode.Tag is BWFReadingBlock);
            this.miDeleteBlock.Enabled = (selectedNode != null) && (selectedNode.Tag is BWFReadingBlock);
            this.assignReadingBlockToAnotherEmployeeToolStripMenuItem.Enabled = (selectedNode != null) && (selectedNode.Tag is MeterReaderEmployee);
        }

        private void dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 8)
            {
                try
                {
                    object tag = this.dataGridView.Rows[e.RowIndex].Tag;
                    if (tag is int)
                    {
                        SubscriberManagmentClient.BWFDeleteMeterReading((int) tag, this.billPeriodSelector.SelectedPeriod.id);
                    }
                    this.dataGridView.Rows.RemoveAt(e.RowIndex);
                }
                catch (Exception exception)
                {
                    UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to delete reading.", exception);
                }
            }
        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void dataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            this.rowModified = true;
            string str = this.dataGridView[e.ColumnIndex, e.RowIndex].Value as string;
            DataGridViewRow row = this.dataGridView.Rows[e.RowIndex];
            try
            {
                switch (e.ColumnIndex)
                {
                    case 0:
                        if (!string.IsNullOrEmpty(str))
                        {
                            break;
                        }
                        foreach (DataGridViewCell cell in row.Cells)
                        {
                            cell.Value = null;
                            cell.Tag = null;
                        }
                        return;

                    case 1:
                        return;

                    case 2:
                        row.Cells[4].Value = "4";
                        this.UpdateConsumption(row);
                        return;

                    case 3:
                        this.UpdateConsumption(row);
                        return;

                    case 4:
                        this.UpdateConsumption(row);
                        return;

                    default:
                        return;
                }
                Subscription subscription = SubscriberManagmentClient.GetSubscription(str, billPeriodSelector.SelectedPeriod.toDate.Ticks);
                if (subscription == null)
                    throw new Exception("Subscription not found");
                if (subscription.subscriptionStatus!=SubscriptionStatus.Active)
                    throw new Exception("Subscription not active");
                row.Cells[0].Tag = subscription.id;
                row.Cells[1].Value = subscription.subscriber.name;
                BWFMeterReading reading = SubscriberManagmentClient.BWFGetMeterPreviousReading(subscription.id, this.billPeriodSelector.SelectedPeriod.id);
                if (reading == null)
                {
                    row.Cells[5].Value = null;
                    row.Cells[6].Value = null;
                }
                else
                {
                    row.Cells[5].Value = SubscriberManagmentClient.GetBillPeriod(SubscriberManagmentClient.BWFGetReadingPeriodID(reading.readingBlockID)).name;
                    row.Cells[6].Value = reading.reading;
                }
                this.UpdateConsumption(row);
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException(exception.Message, exception);
            }
        }

        private void dataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            this.dataGridView[e.ColumnIndex, e.RowIndex].Value = null;
            e.Cancel = false;
            e.ThrowException = false;
        }

        private void dataGridView_EnterComit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = this.dataGridView.Rows[e.RowIndex];
            switch (e.ColumnIndex)
            {
                case 0:
                    this.dataGridView.CurrentCell = row.Cells[3];
                    break;

                case 3:
                case 7:
                    if (this.dataGridView.Rows[e.RowIndex].Tag != null)
                    {
                        this.dataGridView.CurrentCell = this.dataGridView[3, e.RowIndex + 1];
                    }
                    else
                    {
                        this.dataGridView.CurrentCell = this.dataGridView[0, e.RowIndex + 1];
                    }
                    break;

                case 4:
                    if (row.Cells[4].Tag is MeterReadingType)
                    {
                        if (((MeterReadingType) row.Cells[4].Tag) == MeterReadingType.Normal)
                        {
                            if (this.dataGridView.Rows[e.RowIndex].Tag != null)
                            {
                                this.dataGridView.CurrentCell = this.dataGridView[3, e.RowIndex + 1];
                            }
                            else
                            {
                                this.dataGridView.CurrentCell = this.dataGridView[0, e.RowIndex + 1];
                            }
                            break;
                        }
                        this.dataGridView.CurrentCell = this.dataGridView[7, row.Index];
                    }
                    break;
            }
        }

        private void dataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (((((e.KeyCode & Keys.Delete) == Keys.Delete) && (this.dataGridView.CurrentRow != null)) && ((this.dataGridView.CurrentCell.ColumnIndex == 3) && !this.dataGridView.CurrentRow.IsNewRow)) && (this.dataGridView.CurrentRow.Tag is int))
            {
                int tag = (int) this.dataGridView.CurrentRow.Tag;
                BWFMeterReading rowMeterReading = this.GetRowMeterReading(this.dataGridView.CurrentRow);
                if (rowMeterReading.bwfStatus == BWFStatus.Read)
                {
                    Exception exception;
                    double reading = rowMeterReading.reading;
                    rowMeterReading.reading = 0.0;
                    rowMeterReading.readingType = MeterReadingType.Normal;
                    rowMeterReading.averageMonths = 0;
                    rowMeterReading.bwfStatus = BWFStatus.Unread;
                    try
                    {
                        SubscriberManagmentClient.BWFUpdateMeterReading(rowMeterReading);
                    }
                    catch (Exception exception1)
                    {
                        exception = exception1;
                        UIFormApplicationBase.CurrentAppliation.HandleException("Error clearing reading", exception);
                    }
                    finally
                    {
                        try
                        {
                            this.SetRow(this.dataGridView.CurrentRow, new BWFDescribedMeterReading( SubscriberManagmentClient.GetSubscription(rowMeterReading.subscriptionID,billPeriodSelector.SelectedPeriod.toDate.Ticks), SubscriberManagmentClient.BWFGetMeterReading(rowMeterReading.subscriptionID, rowMeterReading.readingBlockID)));
                        }
                        catch (Exception exception2)
                        {
                            exception = exception2;
                            UIFormApplicationBase.CurrentAppliation.HandleException("Error retreiving reading", exception);
                        }
                    }
                }
            }
        }

        private void dataGridView_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            for (int i = 0; i < e.RowCount; i++)
            {
                this.dataGridView[8, e.RowIndex + i].Value = "X";
            }
        }

        private void dataGridView_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (this.rowModified)
            {
                this.rowModified = false;
                DataGridViewRow row = this.dataGridView.Rows[e.RowIndex];
                try
                {
                    if (row.Cells[0].Tag == null)
                    {
                        if (row.Tag is int)
                        {
                            SubscriberManagmentClient.BWFDeleteMeterReading((int) row.Tag, this.billPeriodSelector.SelectedPeriod.id);
                        }
                    }
                    else
                    {
                        int cellSubscID = (int) row.Cells[0].Tag;
                        BWFMeterReading rowMeterReading = this.GetRowMeterReading(row);
                        bool newReading = true;
                        int subscriptionID = -1;
                        if (row.Tag is int)
                        {
                            int rowSubscID = (int) row.Tag;
                            if (rowSubscID == cellSubscID)
                            {
                                newReading = false;
                            }
                            else
                            {
                                subscriptionID = rowSubscID;
                            }
                        }
                        if (newReading)
                        {
                            if (SubscriberManagmentClient.BWFGetMeterReading(rowMeterReading.subscriptionID, rowMeterReading.readingBlockID) != null)
                            {
                                throw new Exception("Reading allready entered.");
                            }
                            SubscriberManagmentClient.BWFAddMeterReading(rowMeterReading);
                        }
                        else
                        {
                            SubscriberManagmentClient.BWFUpdateMeterReading(rowMeterReading);
                        }
                        if (rowMeterReading.readingType == MeterReadingType.Average)
                        {
                            BWFMeterReading averageReading = SubscriberManagmentClient.BWFGetMeterReading(rowMeterReading.subscriptionID, rowMeterReading.readingBlockID);
                            row.Cells[3].Value = averageReading.reading.ToString("#");
                            row.Cells[7].Value = averageReading.consumption;
                        }
                        if (subscriptionID != -1)
                        {
                            SubscriberManagmentClient.BWFDeleteMeterReading(subscriptionID, this.billPeriodSelector.SelectedPeriod.id);
                        }
                        row.Tag = rowMeterReading.subscriptionID;
                    }
                }
                catch (Exception exception)
                {
                    UIFormApplicationBase.CurrentAppliation.HandleException(exception.Message, exception);
                }
                finally
                {
                    if (row.Tag is int)
                    {
                        this.SetRow(row, new BWFDescribedMeterReading(SubscriberManagmentClient.GetSubscription((int)row.Tag, billPeriodSelector.SelectedPeriod.toDate.Ticks), SubscriberManagmentClient.BWFGetMeterReading((int)row.Tag, this.m_selectedBlock.id)));
                    }
                    else
                    {
                        row.Tag = null;
                        foreach (DataGridViewCell cell in row.Cells)
                        {
                            cell.Value = null;
                            cell.Tag = null;
                        }
                    }
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void enableEditing(bool enable)
        {
            this.panelBlockInfo.Enabled = enable;
            this.panelNavigation.Enabled = enable;
            this.splitContainer.Panel1.Enabled = enable;
        }

        private BWFMeterReading GetRowMeterReading(DataGridViewRow row)
        {
            if (this.m_selectedBlock == null)
            {
                throw new Exception("Reading block not selected");
            }
            BWFMeterReading reading = new BWFMeterReading();
            if (!(row.Cells[0].Tag is int))
            {
                throw new Exception("Subscription not set.");
            }
            reading.subscriptionID = (int) row.Cells[0].Tag;
            if (!(row.Cells[4].Tag is MeterReadingType))
            {
                throw new Exception("Meter reading type not set.");
            }
            reading.readingType = (MeterReadingType) row.Cells[4].Tag;
            if (reading.readingType == MeterReadingType.Average)
            {
                if (!(row.Cells[2].Value is int))
                {
                    throw new Exception("Number of months for average calculation not set.");
                }
                reading.averageMonths = (int) row.Cells[2].Value;
                reading.bwfStatus = BWFStatus.Read;
            }
            else
            {
                double num;
                string s = row.Cells[3].Value as string;
                if (!double.TryParse(s, out num))
                {
                    reading.bwfStatus = BWFStatus.Unread;
                    reading.reading = 0.0;
                }
                else
                {
                    reading.bwfStatus = BWFStatus.Read;
                    reading.reading = num;
                    if (!(row.Cells[7].Value is double))
                    {
                        throw new Exception("Consumption not set.");
                    }
                    reading.consumption = (double) row.Cells[7].Value;
                }
            }
            reading.readingBlockID = this.m_selectedBlock.id;
            reading.orderN = (row.Index + 1) + this.pageNavigator.RecordIndex;
            return reading;
        }

        private void InitGrid()
        {
            this.dataGridView.Columns[2].ValueType = typeof(int);
            this.dataGridView.Columns[3].ValueType = typeof(string);
            this.dataGridView.Columns[6].ValueType = typeof(double);
            this.dataGridView.Columns[7].ValueType = typeof(double);
        }


        private void linkEdit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                CreateReadingSheet sheet = new CreateReadingSheet(this.m_selectedBlock.id);
                if (sheet.ShowDialog(this) == DialogResult.OK)
                {
                    this.m_selectedBlock = SubscriberManagmentClient.GetBlock(this.m_selectedBlock.id);
                    this.updateBlockInfoHeader();
                    this.treeView.SelectedNode.Text = this.m_selectedBlock.blockName;
                    this.treeView.SelectedNode.Tag = this.m_selectedBlock;
                }
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error loading form", exception);
            }
        }

        private void linkImport_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MergeReadingBlocks blocks = new MergeReadingBlocks(this.m_selectedBlock.id);
            if (blocks.ShowDialog(this) == DialogResult.OK)
            {
                List<TreeNode> list = new List<TreeNode>();
                foreach (int num in blocks.deletedBlocks)
                {
                    foreach (TreeNode node in this.treeView.Nodes)
                    {
                        bool flag = false;
                        foreach (TreeNode node2 in node.Nodes)
                        {
                            BWFReadingBlock tag = node2.Tag as BWFReadingBlock;
                            if ((tag != null) && (tag.id == num))
                            {
                                flag = true;
                                list.Add(node2);
                                break;
                            }
                        }
                        if (flag)
                        {
                            break;
                        }
                    }
                }
                foreach (TreeNode node3 in list)
                {
                    node3.Remove();
                }
                this.LoadPage();
            }
        }

        private void linkImportReadings_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ImportReadings readings = new ImportReadings(this.m_selectedBlock.id);
            if (readings.ShowDialog(this) == DialogResult.OK)
            {
                this.LoadPage();
            }
        }

        private void LoadPage()
        {
            try
            {
                int NRec;
                this.enableEditing(false);
                this.dataGridView.Rows.Clear();
                this.m_selectedBlock = (BWFReadingBlock) this.treeView.SelectedNode.Tag;
                this.updateBlockInfoHeader();
                BWFDescribedMeterReading[] readingArray = SubscriberManagmentClient.BWFGetDescribedReadings(this.m_selectedBlock.id, this.pageNavigator.RecordIndex, this.pageNavigator.PageSize, out NRec);
                this.progressBar.Maximum = readingArray.Length;
                this.pageNavigator.NRec = NRec;
                int progress = 0;
                foreach (BWFDescribedMeterReading reading in readingArray)
                {
                    int rowIndex = -1;
                    rowIndex = this.dataGridView.Rows.Add(new object[] { reading.contractNo, reading.customerName });
                    DataGridViewRow row = this.dataGridView.Rows[rowIndex];
                    this.SetRow(row, reading);
                    row.Tag = reading.reading.subscriptionID;
                    this.progressBar.Value = progress++;
                }
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error loading data", exception);
            }
            finally
            {
                this.enableEditing(true);
                this.progressBar.Value = 0;
            }
        }

        private void LoadTree(int selectBlockID)
        {
            this.treeView.Nodes.Clear();
            try
            {
                foreach (MeterReaderEmployee employee in SubscriberManagmentClient.GetAllMeterReaderEmployees(this.billPeriodSelector.SelectedPeriod.id))
                {
                    if (employee.employeeID != -1)
                    {
                        TreeNode node = new TreeNode(PayrollClient.GetEmployee(employee.employeeID).EmployeeNameID);
                        node.Tag = employee;
                        this.treeView.Nodes.Add(node);
                        node.Checked = true;
                    }
                }
                TreeNode node2 = new TreeNode("Unknown reader");
                foreach (BWFReadingBlock block in SubscriberManagmentClient.GetBlocks(this.billPeriodSelector.SelectedPeriod.id, -1))
                {
                    TreeNode node3 = new TreeNode(block.blockName);
                    node3.Tag = block;
                    TreeNode node4 = node2;
                    foreach (TreeNode node5 in this.treeView.Nodes)
                    {
                        MeterReaderEmployee tag = node5.Tag as MeterReaderEmployee;
                        if ((tag != null) && (tag.employeeID == block.readerID))
                        {
                            node4 = node5;
                            break;
                        }
                    }
                    if ((node4 == node2) && (node2.TreeView == null))
                    {
                        this.treeView.Nodes.Add(node2);
                    }
                    node4.Checked = true;
                    node4.Nodes.Add(node3);
                    node3.Checked = true;
                    if (block.id == selectBlockID)
                    {
                        node3.Parent.Expand();
                        this.treeView.SelectedNode = node3;
                    }
                }
                this.checkBoxSelectAll.Checked = true;
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error loading reading structure", exception);
            }
        }

        private void miCreateBlock_Click(object sender, EventArgs e)
        {
            TreeNode node = (this.treeView.SelectedNode.Parent == null) ? this.treeView.SelectedNode : this.treeView.SelectedNode.Parent;
            CreateReadingSheet sheet = new CreateReadingSheet(this.billPeriodSelector.SelectedPeriod.id, ((MeterReaderEmployee) node.Tag).employeeID);
            if (sheet.ShowDialog() == DialogResult.OK)
            {
                this.LoadTree(sheet.block.id);
            }
        }

        private void miDeleteBlock_Click(object sender, EventArgs e)
        {
            if (UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete this reading block?"))
            {
                try
                {
                    SubscriberManagmentClient.BWFDeleteReadingBlock(((BWFReadingBlock) this.treeView.SelectedNode.Tag).id);
                    this.LoadTree(-1);
                }
                catch (Exception exception)
                {
                    UIFormApplicationBase.CurrentAppliation.HandleException("Errortrying to delete reading block", exception);
                }
            }
        }

        private void pageNavigator_PageChanged(object sender, EventArgs e)
        {
            this.LoadPage();
        }       

        private void sde_SurveyDataAdded(MeterSurveyData data)
        {
            throw new NotImplementedException("Depricated");
        }

        private void sde_SurveyDataUpdated(MeterSurveyData data)
        {
            throw new NotImplementedException("Depricated");
        }        

        private void SetRow(DataGridViewRow row, BWFDescribedMeterReading m)
        {
            try
            {
                row.Cells[3].Value = m.reading.reading.ToString("#");
                row.Cells[0].Tag = m.reading.subscriptionID;
                row.Cells[0].Value = m.contractNo;
                switch (m.reading.readingType)
                {
                    case MeterReadingType.Normal:
                        row.Cells[4].Value = null;
                        row.Cells[2].Value = null;
                        break;

                    case MeterReadingType.MeterReset:
                        row.Cells[4].Value = "Meter Reset";
                        row.Cells[2].Value = null;
                        break;


                    case MeterReadingType.Average:
                        row.Cells[4].Value = "Average";
                        row.Cells[2].Value = m.reading.averageMonths;
                        break;
                }
                row.Cells[4].Tag = m.reading.readingType;
                if (m.previousReading != null)
                {
                    row.Cells[5].Value = m.previousPeriod.name;
                    row.Cells[6].Value = m.previousReading.reading;
                }
                row.Cells[7].Value = m.reading.consumption;
                row.Tag = m.reading.subscriptionID;
            }
            catch
            {
                row.Cells[0].Value = "error";
            }
        }

        private void textSearchBlock_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Return) && (this._searchResult.Count == 1))
            {
                this.treeView.SelectedNode = this._searchResult[0];
            }
        }

        private void textSearchBlock_TextChanged(object sender, EventArgs e)
        {
            string str = this.textSearchBlock.Text.Trim();
            this._searchResult.Clear();
            foreach (TreeNode node in this.treeView.Nodes)
            {
                foreach (TreeNode node2 in node.Nodes)
                {
                    if ((str != "") && (node2.Text.IndexOf(str, StringComparison.InvariantCultureIgnoreCase) != -1))
                    {
                        this._searchResult.Add(node2);
                        node2.NodeFont = this.highlightedNodeFont;
                        node2.BackColor = this.highLightedNodeColor;
                        if (!node.IsExpanded)
                        {
                            node.Expand();
                        }
                    }
                    else
                    {
                        node2.NodeFont = this.treeView.Font;
                        node2.BackColor = this.treeView.BackColor;
                    }
                }
            }
            if (this._searchResult.Count == 0)
            {
                this.labelResults.Text = "Not Found";
            }
            else
            {
                this._searchResult[0].EnsureVisible();
                if (this._searchResult.Count == 1)
                {
                    this.labelResults.Text = "Unique block found, press enter to view details";
                }
                else
                {
                    this.labelResults.Text = this._searchResult.Count + " blocks found, type more letters to find unique result";
                }
            }
        }

        private void treeView_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (m_ignoreEvents) return;
            m_ignoreEvents = true;
            try
            {
                if (e.Node.Parent != null)
                {
                    bool flag = true;
                    foreach (TreeNode node in e.Node.Parent.Nodes)
                    {
                        if (!node.Checked)
                        {
                            flag = false;
                            break;
                        }
                    }

                    e.Node.Parent.Checked = flag;
                }

                foreach (TreeNode child in e.Node.Nodes)
                    child.Checked = e.Node.Checked;

            }
            finally
            {
                m_ignoreEvents = false;
            }
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if ((this.treeView.SelectedNode != null) && (this.treeView.SelectedNode.Tag is BWFReadingBlock))
            {
                this.splitContainer.Panel2.Enabled = true;
                this.linkEdit.Visible = true;
                this.pageNavigator.RecordIndex = 0;
                this.LoadPage();
            }
            else
            {
                this.textBlockInformation.Text = "";
                this.linkEdit.Visible = false;
                this.m_selectedBlock = null;
                this.dataGridView.Rows.Clear();
                this.splitContainer.Panel2.Enabled = false;
            }
        }

        private void treeView_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
        }

        private void treeView_MouseDown(object sender, MouseEventArgs e)
        {
            TreeViewHitTestInfo info = this.treeView.HitTest(new Point(e.X, e.Y));
            this.treeView.SelectedNode = info.Node;
        }

        private void updateBlockInfoHeader()
        {
            this.textBlockInformation.Text = "Block: " + this.m_selectedBlock.blockName + "\tRead by: " + ((this.m_selectedBlock.readerID == -1) ? " unknown" : PayrollClient.GetEmployee(this.m_selectedBlock.readerID).EmployeeNameID) + "\tEncoded by: " + ((this.m_selectedBlock.entryClerkID == -1) ? " unknown" : PayrollClient.GetEmployee(this.m_selectedBlock.entryClerkID).EmployeeNameID) + "\tReadig Started on: " + this.m_selectedBlock.readingStart.ToString("MMM dd, yyyy") + "\tReading Finished on: " + this.m_selectedBlock.readingEnd.ToString("MMM dd, yyyy");
        }

        private void UpdateConsumption(DataGridViewRow row)
        {
            if (!(row.Cells[0].Tag is int))
            {
                throw new Exception("Subscription not set.");
            }
            MeterReadingType normal = MeterReadingType.Normal;
            string str = row.Cells[4].Value as string;
            if (!string.IsNullOrEmpty(str))
            {
                string str2 = str;
                if (str2 == null)
                {
                    goto Label_00FF;
                }
                if (!(str2 == "1"))
                {
                    if (str2 == "4")
                    {
                        normal = MeterReadingType.Average;
                        row.Cells[4].Value = "Average";
                        goto Label_0116;
                    }
                    goto Label_00FF;
                }
                normal = MeterReadingType.MeterReset;
                row.Cells[4].Value = "Meter Reset";
            }
            goto Label_0116;
        Label_00FF:
            normal = MeterReadingType.Normal;
            row.Cells[4].Value = null;
        Label_0116:
            row.Cells[4].Tag = normal;
            if (normal == MeterReadingType.Normal)
            {
                double num;
                if (double.TryParse(row.Cells[3].Value as string, out num) && (row.Cells[6].Value is double))
                {
                    row.Cells[7].Value = num - ((double) row.Cells[6].Value);
                }
                else if (double.TryParse(row.Cells[3].Value as string, out num))
                {
                    row.Cells[7].Value = num;
                }
                else
                {
                    row.Cells[7].Value = null;
                }
            }
        }
        private void dataGridView_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var grid = sender as DataGridView;
            var rowIdx = (e.RowIndex + 1).ToString();

            var centerFormat = new StringFormat()
            {
                // right alignment might actually make more sense for numbers
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            var headerBounds = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, grid.RowHeadersWidth, e.RowBounds.Height);
            e.Graphics.DrawString(rowIdx, this.Font, SystemBrushes.ControlText, headerBounds, centerFormat);
        }
        protected override void WaitIsOver(ServerDataDownloader d)
        {
            ImporterProcess process = (ImporterProcess) d;
            if (process.error == null)
            {
                this.LoadTree(-1);
            }
            else
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to import reading blocks", process.error);
            }
            this.progressBar.Value = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.LoadTree(-1);
        }
    }

}

