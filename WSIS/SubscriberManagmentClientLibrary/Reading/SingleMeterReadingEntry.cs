
    using INTAPS.Ethiopic;
    using INTAPS.SubscriberManagment;
    using INTAPS.UI;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class SingleMeterReadingEntry : Form
    {
        
        
        
        
        private IContainer components;
        
       
        
        
        
        
        private BWFMeterReading m_formData;
        private bool m_new;
        
        

        public SingleMeterReadingEntry()
        {
            this.components = null;
            this.InitializeComponent();
            this.comboType.Items.AddRange(Enum.GetNames(typeof(MeterReadingType)));
            this.comboType.SelectedValue = MeterReadingType.Normal.ToString();
            this.billPeriodSelector.LoadData(SubscriberManagmentClient.CurrentYear);
        }

        public SingleMeterReadingEntry(BWFMeterReading m) : this()
        {
            this.m_formData = m;
            this.dtpDate.Value = m.entryDate;
            this.billPeriodSelector.SelectedPeriod = SubscriberManagmentClient.GetBillPeriod(m.periodID);
            this.billPeriodSelector.Enabled = false;
            this.dtpDate.Enabled = false;
            this.textReading.Text = m.reading.ToString();
            this.textConsumption.Text = m.consumption.ToString();
            this.comboType.SelectedItem = m.readingType.ToString();
            this.m_new = false;
        }

        public SingleMeterReadingEntry(int subscriptionID) : this()
        {
            this.billPeriodSelector.SelectedPeriod = SubscriberManagmentClient.CurrentPeriod;
            this.m_formData = new BWFMeterReading();
            this.m_formData.readingBlockID = -1;
            this.m_formData.periodID = -1;
            this.m_new = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.m_formData.readingBlockID == -1)
                {
                    this.m_formData.entryDate = this.dtpDate.Value;
                    this.m_formData.periodID = this.billPeriodSelector.SelectedPeriod.id;
                    this.m_formData.readingBlockID = SubscriberManagmentClient.BWFGetReadingBlockID(this.m_formData.subscriptionID, this.m_formData.periodID);
                    if (this.m_formData.readingBlockID == -1)
                    {
                        UIFormApplicationBase.CurrentAppliation.ShowUserError("You can't enter reading because the subcription not included in reading blocks for the given period");
                        return;
                    }
                }
                if (!double.TryParse(this.textReading.Text, out this.m_formData.reading))
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Enter reading.");
                }
                else
                {
                    this.m_formData.readingType = (MeterReadingType) Enum.Parse(typeof(MeterReadingType), this.comboType.SelectedItem.ToString());
                    if ((this.m_formData.readingType != MeterReadingType.Normal) && !double.TryParse(this.textConsumption.Text, out this.m_formData.consumption))
                    {
                        UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Enter consumption.");
                    }
                    else
                    {
                        if (this.m_formData.bwfStatus == BWFStatus.Unread || this.m_formData.bwfStatus == BWFStatus.Read
                            || this.m_formData.bwfStatus==BWFStatus.Proposed
                            )
                        {
                            this.m_formData.bwfStatus = BWFStatus.Read;
                            SubscriberManagmentClient.BWFUpdateMeterReading(this.m_formData);
                            base.DialogResult = DialogResult.OK;
                            base.Close();
                        }
                        else
                            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Bill already generated");

                    }
                }
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't save reading.", exception);
            }
        }

        private void comboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.textConsumption.Enabled = this.comboType.SelectedItem.ToString() != MeterReadingType.Normal.ToString();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void billPeriodSelector_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textReading_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textConsumption_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

    }
}

