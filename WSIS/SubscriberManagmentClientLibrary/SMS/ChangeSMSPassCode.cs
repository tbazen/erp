﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class ChangeSMSPassCode : Form
    {
        public ChangeSMSPassCode()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (!textNewPasscode.Text.Equals(textNewPasscodeConfirm.Text))
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("New password confirmed incorrectly");
                return;
            }
            try
            {
                SubscriberManagmentClient.changeSMSPassCode(textOldPasscode.Text, textNewPasscode.Text);
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
    }
}
