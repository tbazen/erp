﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class CheckSMSPassCode : Form
    {
        public string passCode;
        public CheckSMSPassCode()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            passCode = this.textPasscode.Text;
            this.DialogResult = DialogResult.OK;
        }
    }
}
