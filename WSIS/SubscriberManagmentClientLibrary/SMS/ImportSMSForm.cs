using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class ImportSMSForm : Form
    {
        public CustomerSMS[] custSMS = null;
        List<CustomerSMS> importedSMS = null;
        public ImportSMSForm()
        {
            InitializeComponent();
        }
        static string getCellText(Cell cell, List<string> stringTable)
        {
            if (cell == null)
                return null;
            if (cell.DataType != null && cell.DataType.InnerText == "s")
                return stringTable[int.Parse(cell.CellValue.Text)];
            double d;
            if (cell.CellValue == null)
                return null;
                if (double.TryParse(cell.CellValue.Text, out d))
                    return Math.Round(d, 6).ToString();
            return cell.CellValue.Text;
        }
        private Cell getCell(Dictionary<int, Cell> cells, int row)
        {
            if (cells.ContainsKey(row))
                return cells[row];
            return null;
        }
        string isValidPhoneNo(string phoneNo)
        {
            if (string.IsNullOrEmpty(phoneNo))
                return null;
            StringBuilder sb = new StringBuilder();
            foreach (char ch in phoneNo)
            {
                if (Char.IsDigit(ch))
                    sb.Append(ch);
            }
            phoneNo = sb.ToString();
            if (phoneNo.Length == 9)
                phoneNo = "0" + phoneNo;
            if (phoneNo.Length!=10)
                return null;
            if (!phoneNo.Substring(0, 2).Equals("09"))
                return null;
            long val;
            if (!long.TryParse(phoneNo, out val))
                return null;
            return phoneNo;
        }
        List<CustomerSMS> importFile(string fileName)
        {
            SpreadsheetDocument doc = null;
            try
            {
                doc = SpreadsheetDocument.Open(fileName, true);
                if (doc == null)
                    throw new ClientServer.ServerUserMessage("Couldn't open " + fileName);


                SharedStringTable ss = doc.WorkbookPart.SharedStringTablePart.SharedStringTable;
                List<string> stringTable = new List<string>();
                foreach (SharedStringItem item in ss)
                {
                    stringTable.Add(item.Text.Text);
                }
                int recordCount = 0;
                List<CustomerSMS> smses = new List<CustomerSMS>();
                foreach (WorksheetPart ws in doc.WorkbookPart.WorksheetParts)
                {
                    foreach (OpenXmlElement data in ws.Worksheet)
                    {
                        if (data is SheetData)
                        {
                            int row = -1;
                            Console.WriteLine("Transfering " + (data.ChildElements.Count - 1) + " Rows");
                            Console.WriteLine();
                            foreach (Row el in data)
                            {
                                row++;
                                if (row < 1)
                                    continue;
                                try
                                {

                                    Console.WriteLine(row + "                       ");

                                    Dictionary<int, Cell> cells = new Dictionary<int, Cell>();
                                    foreach (Cell c in el)
                                    {
                                        string r = c.CellReference.Value;
                                        for (int i = 0; i < r.Length; i++)
                                            if (Char.IsNumber(r[i]))
                                            {
                                                r = r.Substring(0, i);
                                                break;
                                            }

                                        cells.Add((int)(r[0] - 'A') + 1, c);
                                    }
                                    int cellColIndex = 1;

                                    string strPhoneNo = getCellText(getCell(cells, cellColIndex++), stringTable);
                                    string strCustomerCode = getCellText(getCell(cells, cellColIndex++), stringTable);
                                    string strMessage = getCellText(getCell(cells, cellColIndex++), stringTable);
                                    string correctedPhoneNo;
                                    if ((correctedPhoneNo = isValidPhoneNo(strPhoneNo)) == null)
                                    {
                                        throw new Exception(string.Format("Invalid phoneNo: '{0}'", strPhoneNo));
                                    }
                                    CustomerSMS theSMS = new CustomerSMS();
                                    theSMS.phoneNo = correctedPhoneNo;
                                    theSMS.customerCode = strCustomerCode;
                                    theSMS.message = strMessage;
                                    smses.Add(theSMS);
                                    recordCount++;
                                }
                                catch (Exception ex)
                                {
                                    CustomerSMS theSMS = new CustomerSMS();
                                    theSMS.phoneNo = null;
                                    theSMS.customerCode = null;
                                    theSMS.message = ex.Message;
                                    smses.Add(theSMS);
                                }
                            }
                        }
                    }
                }
                return smses;
            }
            finally
            {
                if (doc != null)
                    doc.Close();
            }
        }        
        private void buttonImport_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "*.xlsx|*.xlsx";
            openFile.Multiselect = false;

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    importedSMS = importFile(openFile.FileName);
                    string preview = SubscriberManagmentClient.getSMSPreviewHTML(
                        new CustomerSMSSendParameter()
                        {
                            manualSMS=importedSMS.ToArray(),
                            sendType=CustomerSMSSendType.General
                        }

                        );
                    browser.LoadTextPage("SMS Preview",  preview);
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (importedSMS == null)
                return;
            List<CustomerSMS> filtered = new List<CustomerSMS>();
            foreach (CustomerSMS sms in importedSMS)
                if (!string.IsNullOrEmpty(sms.phoneNo))
                    filtered.Add(sms);
            this.custSMS = filtered.ToArray();
            this.DialogResult = DialogResult.OK;

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.custSMS = null;
            this.DialogResult = DialogResult.Cancel;
        }

        private void ImportSMSForm_Load(object sender, EventArgs e)
        {
            buttonImport_Click(null, null);
        }

    }
}