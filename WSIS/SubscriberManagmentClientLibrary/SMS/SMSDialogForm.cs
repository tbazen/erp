﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using INTAPS;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class SMSDialogForm : Form
    {
        int sendID = -1, receiveID = -1;
        string _phoneNo;
        bool _run = true;
        System.Threading.Thread updateLoopThread;
        public SMSDialogForm(string phoneNo)
        {
            InitializeComponent();
            webBrowser.DocumentText = "<div id='msg'></div>";
            _phoneNo = phoneNo;
            updateLoopThread = new System.Threading.Thread(updateLoop);
            updateLoopThread.Start();
        }
        void updateLoop()
        {
            do
            {
                CustomerSMS [] sms=SubscriberManagmentClient.getSMSLogByPhoneNo(sendID, receiveID, _phoneNo);
                if (sms.Length > 0)
                {
                    this.Invoke(new INTAPS.UI.HTML.ProcessSingleParameter<CustomerSMS[]>(showMessages), sms);
                    foreach (CustomerSMS s in sms)
                        if (s.smsOut)
                            sendID = s.smsID;
                        else
                            receiveID = s.smsID;
                }
                if(_run)
                    System.Threading.Thread.Sleep(500);
            }
            while (_run);
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            _run = false;
            updateLoopThread.Join();
            base.OnClosing(e);
        }
        public void showMessages(CustomerSMS[] msgs)
        {
            HtmlElement lastElement = null;
            foreach (CustomerSMS m in msgs)
            {
                HtmlElement el = appendMessage(m);
                lastElement = el;
            }
            if (lastElement != null)
                lastElement.ScrollIntoView(false);
            textMessage.Focus();
            this.Show();
        }

        private HtmlElement appendMessage(CustomerSMS m)
        {
            string msgHTML = string.Format("<small>{0}</small></br>{1}",
                System.Web.HttpUtility.HtmlEncode(m.time.ToString("MMM dd,yyy hh:mm:ss t")),
                System.Web.HttpUtility.HtmlEncode(m.message)
                );
            HtmlElement el = webBrowser.Document.CreateElement("div");
            el.InnerHtml = msgHTML;
            el.Id = (m.smsOut ? "out_" : "in_") + m.smsID;
            if(m.smsOut)
                el.Style = "text-align:right;width:100%";
            webBrowser.Document.GetElementById("msg").AppendChild(el);
            return el;
        }
        

        public void showSendMessage()
        {
            this.Show();
        }
        private void InstantMessageForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            try
            {
                if (textMessage.Text.Trim().Equals(""))
                    return;
                CustomerSMS msg = new CustomerSMS()
                                        {
                                            ticks=DateTime.Now.Ticks,
                                            message = textMessage.Text,
                                            smsOut=true,
                                        };

                SubscriberManagmentClient.sendSMS(
                    new CustomerSMSSendParameter()
                    {
                        phoneNo=msg.phoneNo,
                        manualSMS=new CustomerSMS[]{msg}
                    }
                    );
                appendMessage(msg);
                textMessage.Text = "";
            }
            catch(Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
    }
}
