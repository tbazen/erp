﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using INTAPS;
using INTAPS.ClientServer;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class SMSMessagingForm : Form
    {
        public SMSMessagingForm()
        {
            InitializeComponent();
            webBrowser.DocumentText = "<div id='msg'></div>";

            string []u;
            try
            {
                u = SecurityClient.GetAllUsers();
                Array.Sort(u);

                comboUser.Items.AddRange(u);
            }
            catch { }
        }
        
        public void showMessages(InstantMessageData[] msgs)
        {
            string lastMessage = null;
            string lastUserName = "";
            foreach (InstantMessageData m in msgs)
            {
                HtmlElement el = appendMessage(m);
                try
                {
                    ApplicationClient.setMessageRead(m.messageID);
                }
                catch { }
                lastMessage = m.messageID;
                lastUserName = m.from;
            }
            if (lastMessage != null)
                webBrowser.Document.GetElementById(lastMessage).ScrollIntoView(false);
            if (!this.Visible || string.IsNullOrEmpty(comboUser.Text))
            {
                comboUser.Text = lastUserName;
                textMessage.Focus();
            }
            this.Show();
        }

        private HtmlElement appendMessage(InstantMessageData m)
        {
            string msgHTML = string.Format("<b>From {0}</b> <small>{1}</small></br>{2}",
                System.Web.HttpUtility.HtmlEncode(m.from),
                System.Web.HttpUtility.HtmlEncode(m.dateSent.ToString("MMM dd,yyy hh:mm:ss t")),
                System.Web.HttpUtility.HtmlEncode(m.messages),
                m.messageID
                );
            HtmlElement el = webBrowser.Document.CreateElement("div");
            el.InnerHtml = msgHTML;
            el.Id = m.messageID;
            webBrowser.Document.GetElementById("msg").AppendChild(el);
            return el;
        }
        private HtmlElement appendMessageRight(InstantMessageData m)
        {
            string msgHTML = string.Format("<b>To {0}</b> <small>{1}</small></br>{2}",
                System.Web.HttpUtility.HtmlEncode(m.to),
                System.Web.HttpUtility.HtmlEncode(m.dateSent.ToString("MMM dd,yyy hh:mm:ss t")),
                System.Web.HttpUtility.HtmlEncode(m.messages),
                m.messageID
                );
            HtmlElement el = webBrowser.Document.CreateElement("div");
            el.InnerHtml = msgHTML;
            el.Id = m.messageID;
            el.Style = "text-align:right;width:100%";
            webBrowser.Document.GetElementById("msg").AppendChild(el);
            return el;
        }

        public void showSendMessage()
        {
            this.Show();
        }
        private void InstantMessageForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            try
            {
                if (textMessage.Text.Trim().Equals(""))
                    return;
                InstantMessageData msg = new InstantMessageData()
                                        {
                                            from=ApplicationClient.UserName,
                                            dateSent=DateTime.Now,
                                            to = comboUser.Text,
                                            messages = textMessage.Text,
                                        };
                msg.messageID=ApplicationClient.sendInstantMessage(msg);
                appendMessageRight(msg);
                textMessage.Text = "";
            }
            catch(Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
    }
}
