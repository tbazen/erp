﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using INTAPS.ClientServer.Client;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class SMSParameters : UserControl
    {
        CustomerSMSSendType _type = CustomerSMSSendType.Bills;
        public SMSParameters()
        {
            InitializeComponent();
            if (ApplicationClient.isConnected())
                textPasscode.Text = SubscriberManagmentClient.smsPassCode ?? "";
        }
        private void subscriptionPlaceHolder1_TextChanged(object sender, EventArgs e)
        {

        }
        public bool validInput(out string error)
        {
            if (buttonImport.Tag != null)
            {
                error = null;
                return true;
            }
            bool valid = _type != CustomerSMSSendType.General
            || !string.IsNullOrEmpty(textMessage.Text.Trim());
            if (valid)
                error = null;
            else
                error = "Please enter message text";
            return valid;
        }
        public CustomerSMSSendParameter parameters
        {
            get
            {
                SubscriberManagmentClient.smsPassCode = textPasscode.Text;
                return new CustomerSMSSendParameter()
                {
                    sendType=_type,
                    singleCustomer=customerPlaceHolder.GetCustomerID(),
                    messageFormat=textMessage.Text.Trim(),
                    passCode=textPasscode.Text,
                    phoneNo=textPhoneNo.Text,
                    manualSMS=buttonImport.Tag as CustomerSMS[],
                };
            }
            set
            {
                _ignoreEvents=true;
                try
                {
                    this._type = value.sendType;
                    switch (this._type)
                    {
                        case CustomerSMSSendType.Bills:
                        case CustomerSMSSendType.Disconnection:
                            textMessage.ReadOnly = true;
                            textPhoneNo.ReadOnly = true;
                            buttonImport.Enabled = false;
                            break;
                        case CustomerSMSSendType.General:
                            textMessage.ReadOnly = false;
                            textPhoneNo.ReadOnly = false;
                            buttonImport.Enabled = true;
                            break;
                    }
                    this.textMessage.Text = value.messageFormat;
                    this.customerPlaceHolder.SetByID(value.singleCustomer);
                }
                finally
                {
                    _ignoreEvents = false;
                }
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
        bool _ignoreEvents = false;
        private void textPasscode_TextChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents)
                return;
            customerPlaceHolder.SetByID(-1);
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (_ignoreEvents)
                return;
            PickSMSItem pickItem = new PickSMSItem(_type);
            if (pickItem.ShowDialog() == DialogResult.OK)
            {

            }
        }

        private void customerPlaceHolder_CustomerChanged(object sender, EventArgs e)
        {
            if (customerPlaceHolder.GetCustomerID() != -1)
                textMessage.Text = "";
        }
        
        private void buttonImport_Click(object sender, EventArgs e)
        {
            ImportSMSForm import = new ImportSMSForm();
            if (import.ShowDialog() == DialogResult.OK)
            {
                buttonImport.Text = "Import SMS (" + import.custSMS.Length + ")";
                buttonImport.Tag = import.custSMS;

                textMessage.Text = "";
                textPhoneNo.Text = "";
                customerPlaceHolder.SetByID(-1);
                textMessage.ReadOnly = true;
                textPhoneNo.ReadOnly = true;
                customerPlaceHolder.Enabled = false;
            }
        }
    }
}
