using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class SendSMSProgresForm:Form
    {
        public SendSMSProgresForm(CustomerSMSSendType type)
        {
            InitializeComponent();
            smsParameters.parameters=new CustomerSMSSendParameter()
            {
                sendType=type
            };
            switch (type)
            {
                case CustomerSMSSendType.Bills:
                    this.Text = "Send Bill SMS Notification";
                    break;
                case CustomerSMSSendType.Disconnection:
                    this.Text = "Send Disconnection SMS Notification";
                    break;
                case CustomerSMSSendType.General:
                    this.Text = "Send General SMS";
                    break;
                case CustomerSMSSendType.UnreadNotification:
                    this.Text = "Send Unread Meter SMS Notification";
                    break;
                default:
                    break;
            }
        }


        void CheckProgress()
        {
          try
          {
              lblStage.Text = SubscriberManagmentClient.getSMSInterfaceStatus();
          }
            catch(Exception ex)
          {
              lblStage.Text = "Error trying to retrieve status. " + ex.Message;
          }
        }
        
        private void btnOk_Click(object sender, EventArgs e)
        {
            string msg;
            if(!smsParameters.validInput(out msg))
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError(msg);
                return;
            }
            if(smsParameters.parameters.singleCustomer==-1 && string.IsNullOrEmpty(smsParameters.parameters.phoneNo))
                if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to send these sms?"))
                    return;

            this.Enabled = false;
            new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(
                delegate(object par)
                {
                    try
                    {
                        SubscriberManagmentClient.sendSMS((CustomerSMSSendParameter)par);
                        this.Invoke(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                        {
                            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Finished preparing SMS.");
                            this.Enabled = true;
                        }));

                    }
                    catch (Exception ex)
                    {
                        this.Invoke(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                            {
                                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
                                this.Enabled = true;
                            }));
                    }

                })).Start(smsParameters.parameters);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            CheckProgress();
        }

        private void buttonPreview_Click(object sender, EventArgs e)
        {
            try
            {
                string html = SubscriberManagmentClient.getSMSPreviewHTML(smsParameters.parameters);
                SimpleHTMLViewer v = new SimpleHTMLViewer("SMS Preview", html, "jobstyle.css");
                v.Show(this);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }

    }
}