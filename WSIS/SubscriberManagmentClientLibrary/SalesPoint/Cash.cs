using INTAPS.UI;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using INTAPS.Accounting;
using System.Collections.Generic;
using BIZNET.iERP;
using System.IO;
using System.Xml.Serialization;

namespace INTAPS.SubscriberManagment.Client
{

    public class Cash : Form
    {
        private Button btnCancel;
        private Button btnPrint;
        private IContainer components = null;
        private Label label1;
        private Label label2;
        private Label lblChange;
        private CustomerBillDocument[] _bills;
        private double _total;
        private Panel panelInstruments;
        private Button buttonAddInstrument;
        private TextBox textCash;
        List<PaymentInstrumentItem> _instruments = new List<PaymentInstrumentItem>();

        const int WIDTH_TYPE = 70;
        const int WIDTH_NUMBER = 130;
        const int WIDTH_ACCOUNT = 150;
        const int WIDTH_BANK = 100;
        const int WIDTH_AMOUNT = 120;
        const int WIDTH_REMARK = 100;
        const int WIDTH_DELETE = 30;

        const int HEIGHT_INSTRUMENT = WIDTH_DELETE;

        const int N_CONTROLS_PER_INSTRUMENT = 7;
        private Label label3;
        private Label labelAmount;
        private Panel panel1;
        SalePointBDE bde;
        int _nCopies;
        public Cash(SalePointBDE bde, CustomerBillDocument[] bills,int nCopies)
        {
            this.InitializeComponent();
            this._bills = bills;
            this._total = 0.0;
            this.bde = bde;
            int i = 0;
            foreach (CustomerBillDocument bill in bills)
            {
                foreach (BillItem item in bde.getCustomerBillItems(bill.AccountDocumentID))
                    this._total += item.netPayment;
                i++;
            }
            this.textCash.Text = (Math.Ceiling(_total*100)/100).ToString("#,#0.00");
            resizeFormForPaymentInstruments();
            this.labelAmount.Text = (Math.Ceiling(_total * 100) / 100).ToString("#,#0.00");
            _nCopies = nCopies;
        }

        private void resizeFormForPaymentInstruments()
        {
            int h1 = panelInstruments.Height;
            if (panelInstruments.Controls.Count == 0)
                panelInstruments.Height = 0;
            else
            {
                Control lastControl = panelInstruments.Controls[panelInstruments.Controls.Count - 1];
                panelInstruments.Height = lastControl.Bottom;
            }
            int delta = panelInstruments.Height - h1;
            this.Height += delta;
        }

        void addPaymentInstrument(PaymentInstrumentItem instrument)
        {
            int x = 0;
            int y = panelInstruments.Height;
            Label lbl;
            PaymentInstrumentType type = bde.getInstrumentType(instrument.instrumentTypeItemCode);
            //type   
            lbl = new Label();
            lbl.AutoSize = false;
            lbl.Left = x;
            lbl.Top = y;
            lbl.Height = HEIGHT_INSTRUMENT;
            panelInstruments.Controls.Add(lbl);

            lbl.Width = WIDTH_TYPE;
            lbl.Text = type.name;

            x += lbl.Width;


            //Document reference   
            lbl = new Label();
            lbl.AutoSize = false;
            lbl.Left = x;
            lbl.Top = y;
            lbl.Height = HEIGHT_INSTRUMENT;
            panelInstruments.Controls.Add(lbl);

            lbl.Width = WIDTH_NUMBER;
            lbl.Text = instrument.documentReference;

            x += lbl.Width;
            //deposit account
            lbl = new Label();
            lbl.AutoSize = false;
            lbl.Left = x;
            lbl.Top = y;
            lbl.Height = HEIGHT_INSTRUMENT;
            panelInstruments.Controls.Add(lbl);

            lbl.Width = WIDTH_NUMBER;
            lbl.Text = type.isToken ? "" : (instrument.despositToBankAccountID == -1 ? "" : BIZNET.iERP.Client.iERPTransactionClient.GetBankAccount(instrument.despositToBankAccountID).ToString());

            x += lbl.Width;

            //Bank name
            lbl = new Label();
            lbl.AutoSize = false;
            lbl.Left = x;
            lbl.Top = y;
            lbl.Height = HEIGHT_INSTRUMENT;
            panelInstruments.Controls.Add(lbl);

            lbl.Width = WIDTH_BANK;
            lbl.Text = type.isTransferFromAccount? bde.getBranchName(instrument.bankBranchID):"";

            x += lbl.Width;

            //Account Number
            lbl = new Label();
            lbl.AutoSize = false;
            lbl.Left = x;
            lbl.Top = y;
            lbl.Height = HEIGHT_INSTRUMENT;
            panelInstruments.Controls.Add(lbl);

            lbl.Width = WIDTH_ACCOUNT;
            lbl.Text =type.isTransferFromAccount?  instrument.accountNumber:"";

            x += lbl.Width;

            //Delete button
            Button btn = new Button();
            btn.Left = panelInstruments.Width - WIDTH_DELETE;
            btn.Top = y;
            btn.Height = HEIGHT_INSTRUMENT;
            panelInstruments.Controls.Add(btn);

            btn.Width = WIDTH_DELETE;
            btn.Text = "X";
            btn.Click += new EventHandler(deleteInstrumentClicked);
            btn.Tag = instrument;

            //Remark
            lbl = new Label();
            lbl.AutoSize = false;
            lbl.Left = btn.Left - WIDTH_REMARK;
            lbl.Top = y;
            lbl.Height = HEIGHT_INSTRUMENT;
            panelInstruments.Controls.Add(lbl);

            lbl.Width = WIDTH_REMARK;
            lbl.Text = instrument.remark;

            //Ammount
            lbl = new Label();
            lbl.AutoSize = false;
            lbl.Left = x;
            lbl.Top = y;
            lbl.Height = HEIGHT_INSTRUMENT;
            panelInstruments.Controls.Add(lbl);

            lbl.Width = panelInstruments.Width - WIDTH_DELETE - x;
            lbl.Text = AccountBase.FormatAmount(instrument.amount);
            resizeFormForPaymentInstruments();

            _instruments.Add(instrument);

            updateAddInstrumentButtonStatus();    
        }

        private void updateAddInstrumentButtonStatus()
        {
           
            PaymentInstrumentItem[] items;
            double change;
            if (!getEffectiveItems(out items,out change, false))
            {
                buttonAddInstrument.Visible = false;
                this.lblChange.Text = "...";
                return;
            }
            double totalIntrument = 0;
            foreach (PaymentInstrumentItem i in items)
                totalIntrument += i.amount;
            if (AccountBase.AmountLess(change,0.01))
            {
                this.lblChange.Text = "";
                buttonAddInstrument.Visible = true;
            }
            else
            {
                buttonAddInstrument.Visible = false;
                if(AccountBase.AmountGreater(change,0.009))
                    this.lblChange.Text = (change).ToString("0,0.00");
                else
                    this.lblChange.Text = "";
            }
        }

        void deleteInstrumentClicked(object sender, EventArgs e)
        {
            PaymentInstrumentItem instrument = ((Control)sender).Tag as PaymentInstrumentItem;
            int index = _instruments.IndexOf(instrument);
            int i;
            for (i = index + 1; i < panelInstruments.Controls.Count; i++)
                panelInstruments.Controls[i].Top -= HEIGHT_INSTRUMENT;
            for (i = 0; i < N_CONTROLS_PER_INSTRUMENT; i++)
            {
                panelInstruments.Controls.RemoveAt((index +1)* N_CONTROLS_PER_INSTRUMENT - i - 1);
            }
            _instruments.RemoveAt(index);
            resizeFormForPaymentInstruments();
            updateAddInstrumentButtonStatus();
            
        }
        
        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }
        
        bool getEffectiveItems(out PaymentInstrumentItem[] items,out double change,bool verbos)
        {
            bool hasCashPayment = !textCash.Text.Equals("");
            double cashAmount = 0;
            items = null;
            change = 0;
            if (hasCashPayment)
            {
                if (!double.TryParse(textCash.Text, out cashAmount) || AccountBase.AmountLess(cashAmount, 0))
                {
                    if(verbos)
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter valid cash amount");
                    return false;
                }
            }

            items = new PaymentInstrumentItem[_instruments.Count + (hasCashPayment ? 1 : 0)];
            _instruments.CopyTo(items, 0);
            double totalInst = 0;
            foreach (PaymentInstrumentItem itm in _instruments)
            {
                totalInst += itm.amount;
            }
            if (AccountBase.AmountGreater( totalInst , _total))
            {
                if(verbos)
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Total instruments greater than bill");
                return false;
            }
            if (hasCashPayment)
            {
                PaymentInstrumentItem cashinst = new PaymentInstrumentItem();
                cashinst.instrumentTypeItemCode = bde.getCashInstrumentCode();
                cashinst.amount = _total-totalInst;
                items[_instruments.Count] = cashinst;
                if (cashinst.amount < 0)
                {
                    if (verbos)
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Not enough cash amount");
                    return false;
                }
                change = cashAmount - cashinst.amount;
            }
            return true;
        }
        private void btnPrint_Click(object sender, EventArgs e)
        {
            bool posted = false;
            PrintReceiptParameters printParameters = new PrintReceiptParameters();
            CustomerPaymentReceipt payment = null;
            try
            {
                PaymentInstrumentItem[] items;
                double change;
                if (!getEffectiveItems(out items, out change, true))
                    return;
                payment = bde.PayBill(DateTime.Now, items, this._bills);
                posted = true;

                printParameters.casheirName = bde.SysPars.centerName;
                printParameters.mainTitle = bde.SysPars.receiptTitle;

                SubscriberManagmentClient.billingRuleClient.printReceipt(payment
                    , new PrintReceiptParameters() { casheirName = bde.SysPars.centerName, mainTitle = bde.SysPars.receiptTitle }
                    , false, _nCopies);

                //Write print log
                PrintLogData.WritePrintLog(payment, printParameters, _nCopies, null, false);
                base.DialogResult = DialogResult.OK;
                base.Close();
            }
            catch (Exception exception)
            {
                PrintLogData.WritePrintLog(payment, printParameters, _nCopies, exception, false);
                if (posted)
                {
                    UIFormApplicationBase.CurrentAppliation.HandleException("Payment is registerd but there was problem with printing the receipt", exception);
                    this.Close();
                }
                else
                    UIFormApplicationBase.CurrentAppliation.HandleException("Error settling bill", exception);
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cash));
            this.textCash = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblChange = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panelInstruments = new System.Windows.Forms.Panel();
            this.buttonAddInstrument = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.labelAmount = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textCash
            // 
            this.textCash.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCash.Location = new System.Drawing.Point(97, 12);
            this.textCash.Name = "textCash";
            this.textCash.Size = new System.Drawing.Size(550, 29);
            this.textCash.TabIndex = 2;
            this.textCash.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textCash.TextChanged += new System.EventHandler(this.textContractNo_TextChanged);
            this.textCash.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textCash_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Cash:";
            // 
            // lblChange
            // 
            this.lblChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblChange.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.lblChange.Location = new System.Drawing.Point(110, 168);
            this.lblChange.Name = "lblChange";
            this.lblChange.Size = new System.Drawing.Size(179, 32);
            this.lblChange.TabIndex = 4;
            this.lblChange.Text = "0.00";
            this.lblChange.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.Location = new System.Drawing.Point(10, 176);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Change:";
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnPrint.ForeColor = System.Drawing.Color.White;
            this.btnPrint.Location = new System.Drawing.Point(556, 7);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(91, 28);
            this.btnPrint.TabIndex = 5;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(663, 7);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(86, 28);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // panelInstruments
            // 
            this.panelInstruments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelInstruments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelInstruments.Location = new System.Drawing.Point(12, 97);
            this.panelInstruments.Name = "panelInstruments";
            this.panelInstruments.Size = new System.Drawing.Size(737, 62);
            this.panelInstruments.TabIndex = 6;
            // 
            // buttonAddInstrument
            // 
            this.buttonAddInstrument.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddInstrument.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonAddInstrument.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddInstrument.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonAddInstrument.ForeColor = System.Drawing.Color.White;
            this.buttonAddInstrument.Image = ((System.Drawing.Image)(resources.GetObject("buttonAddInstrument.Image")));
            this.buttonAddInstrument.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAddInstrument.Location = new System.Drawing.Point(674, 63);
            this.buttonAddInstrument.Name = "buttonAddInstrument";
            this.buttonAddInstrument.Size = new System.Drawing.Size(75, 28);
            this.buttonAddInstrument.TabIndex = 7;
            this.buttonAddInstrument.Text = "&Add";
            this.buttonAddInstrument.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAddInstrument.UseVisualStyleBackColor = false;
            this.buttonAddInstrument.Click += new System.EventHandler(this.buttonAddInstrument_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.Location = new System.Drawing.Point(12, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Bill Amount:";
            // 
            // labelAmount
            // 
            this.labelAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelAmount.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.labelAmount.Location = new System.Drawing.Point(146, 44);
            this.labelAmount.Name = "labelAmount";
            this.labelAmount.Size = new System.Drawing.Size(143, 32);
            this.labelAmount.TabIndex = 4;
            this.labelAmount.Text = "0.00";
            this.labelAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelAmount.Click += new System.EventHandler(this.labelAmount_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Controls.Add(this.btnPrint);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 217);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(757, 38);
            this.panel1.TabIndex = 8;
            // 
            // Cash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(757, 255);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buttonAddInstrument);
            this.Controls.Add(this.panelInstruments);
            this.Controls.Add(this.labelAmount);
            this.Controls.Add(this.lblChange);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textCash);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Cash";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cash";
            this.Load += new System.EventHandler(this.Cash_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private void textCash_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.btnPrint_Click(this.btnPrint, null);
            }
        }

        private void textContractNo_TextChanged(object sender, EventArgs e)
        {
           updateAddInstrumentButtonStatus();
        }

        private void Cash_Load(object sender, EventArgs e)
        {

        }

        private void buttonAddInstrument_Click(object sender, EventArgs e)
        {
            PaymentInstrumentItem[] items;
            double change;
            if (!getEffectiveItems(out items,out change,true))
                return;
            double totalIntrument = 0;
            foreach (PaymentInstrumentItem i in items)
                totalIntrument += i.amount;
            if (AccountBase.AmountGreater(change, 0.009))
            {
                lblChange.Text = change.ToString("#,#0.00");
            }
            else
                lblChange.Text = "";
            PaymentInstrumentEditor f = new PaymentInstrumentEditor(bde,_total-totalIntrument);
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                addPaymentInstrument(f.instrument);
            }
        }

        private void labelAmount_Click(object sender, EventArgs e)
        {

        }
    }
}