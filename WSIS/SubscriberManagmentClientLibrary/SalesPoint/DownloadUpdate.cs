
    using INTAPS.SubscriberManagment;
    using INTAPS.UI;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Windows.Forms;
using INTAPS.ClientServer;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class DownloadUpdate : Form
    {
        const int NMSG_PER_FILE = 1000;
        private IContainer components;
         
   
         
        public DownloadUpdate()
        {
            this.components = null;
            this.InitializeComponent();
            if (!string.IsNullOrEmpty(INTAPS.SubscriberManagment.Client.General.PCSettings.Default.ImportExportFolder))
                filePicker.Text = INTAPS.SubscriberManagment.Client.General.PCSettings.Default.ImportExportFolder;
        }

        

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            INTAPS.SubscriberManagment.Client.General.PCSettings.Default.ImportExportFolder = filePicker.Text;
            INTAPS.SubscriberManagment.Client.General.PCSettings.Default.Save();
            e.Cancel = !btnCancel.Enabled;
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            FileStream serializationStream = null;
            btnOk.Enabled = btnCancel.Enabled = textID.Enabled = filePicker.Enabled = false;
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                string path = this.filePicker.Text;
                int from;
                if (this.textID.Text == "")
                {
                    from = SubscriberManagmentClient.getFirstMessageNumber();
                    if (from == -1)
                        from = 0;
                    else
                        from = from - 1;
                }
                else
                {
                    if (!int.TryParse(this.textID.Text, out from))
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Enter valid update number");
                    }
                }
                int last = SubscriberManagmentClient.getLastMessageNumber();
                int count=last-from;
                int nFile = count / NMSG_PER_FILE + (count % NMSG_PER_FILE == 0 ? 0 : 1);
                if(nFile>0)
                    progressBar1.Maximum = nFile - 1;
                for (int i = 0; i < nFile; i++)
                {
                    int fileSize = count > NMSG_PER_FILE ? NMSG_PER_FILE : count;
                    MessageList list = SubscriberManagmentClient.getMessageList(from + 1, from + fileSize);
                    if (list.data.Length != 0)
                    {
                        serializationStream = File.Create(path + @"\" + (from + 1).ToString("0000000000") + "-" + (from + fileSize).ToString("0000000000") + ".posupdate");
                        formatter.Serialize(serializationStream, list);
                        serializationStream.Close();
                    }
                        progressBar1.Value = i;
                    from += NMSG_PER_FILE;
                    count -= NMSG_PER_FILE;
                    Application.DoEvents();
                }
                btnOk.Enabled = btnCancel.Enabled = textID.Enabled = filePicker.Enabled = true;
                base.Close();
            }
            catch (Exception exception)
            {
                btnOk.Enabled = btnCancel.Enabled = textID.Enabled = filePicker.Enabled = true;
                if (serializationStream != null)
                {
                    serializationStream.Close();
                }
                UIFormApplicationBase.CurrentAppliation.HandleException("Error downloading updates.", exception);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

