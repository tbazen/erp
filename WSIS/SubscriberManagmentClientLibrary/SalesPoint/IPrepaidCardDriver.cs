﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTAPS.SubscriberManagment.Client.PrepaidDriver
{
    public enum ConnectionIDType
    {
        ConnectionID,
        ContractNo,
        MeterNo
    }
    public delegate void ConnectionIDRead(String conectionID);
    public delegate void CardReaderFailed(String msg,Exception ex);
    public delegate void TopupOk();
    public interface IPrepaidCardDriver
    {
        
        bool supportsReadConnectionID { get; }
        void readConnectionID( ConnectionIDType idType,ConnectionIDRead read,CardReaderFailed failed);
        void topUp(int connectionID, String tag, int meterNo, double cubicMeters,TopupOk ok,CardReaderFailed failed);
    }
}
