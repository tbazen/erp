﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class ImportPayment : System.Windows.Forms.Form
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportPayment));
            this.label1 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.lvErrors = new System.Windows.Forms.ListView();
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colError = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chkDelete = new System.Windows.Forms.CheckBox();
            this.filePicker = new INTAPS.SubscriberManagment.Client.FilePicker();
            this.textError = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label1.Location = new System.Drawing.Point(9, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Folder:";
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnOk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(434, 14);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(65, 28);
            this.btnOk.TabIndex = 29;
            this.btnOk.Text = "&Import";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lvErrors
            // 
            this.lvErrors.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lvErrors.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colName,
            this.colError});
            this.lvErrors.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvErrors.ForeColor = System.Drawing.Color.Black;
            this.lvErrors.FullRowSelect = true;
            this.lvErrors.Location = new System.Drawing.Point(12, 51);
            this.lvErrors.Name = "lvErrors";
            this.lvErrors.Size = new System.Drawing.Size(487, 440);
            this.lvErrors.TabIndex = 30;
            this.lvErrors.UseCompatibleStateImageBehavior = false;
            this.lvErrors.View = System.Windows.Forms.View.Details;
            this.lvErrors.SelectedIndexChanged += new System.EventHandler(this.lvErrors_SelectedIndexChanged);
            // 
            // colName
            // 
            this.colName.Text = "File";
            this.colName.Width = 159;
            // 
            // colError
            // 
            this.colError.Text = "Error";
            this.colError.Width = 152;
            // 
            // chkDelete
            // 
            this.chkDelete.AutoSize = true;
            this.chkDelete.Checked = true;
            this.chkDelete.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDelete.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDelete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.chkDelete.Location = new System.Drawing.Point(505, 19);
            this.chkDelete.Name = "chkDelete";
            this.chkDelete.Size = new System.Drawing.Size(90, 19);
            this.chkDelete.TabIndex = 48;
            this.chkDelete.Text = "Delete files";
            this.chkDelete.UseVisualStyleBackColor = true;
            // 
            // filePicker
            // 
            this.filePicker.anobject = null;
            this.filePicker.Filter = "*.posupdate";
            this.filePicker.FolderMode = true;
            this.filePicker.Location = new System.Drawing.Point(59, 19);
            this.filePicker.Name = "filePicker";
            this.filePicker.Size = new System.Drawing.Size(369, 20);
            this.filePicker.TabIndex = 0;
            // 
            // textError
            // 
            this.textError.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textError.Location = new System.Drawing.Point(505, 51);
            this.textError.Multiline = true;
            this.textError.Name = "textError";
            this.textError.ReadOnly = true;
            this.textError.Size = new System.Drawing.Size(419, 440);
            this.textError.TabIndex = 49;
            // 
            // ImportPayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.ClientSize = new System.Drawing.Size(936, 503);
            this.Controls.Add(this.textError);
            this.Controls.Add(this.chkDelete);
            this.Controls.Add(this.lvErrors);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.filePicker);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImportPayment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Import Bill";
            this.Load += new System.EventHandler(this.ImportBill_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.ListView lvErrors;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader colError;
        private System.Windows.Forms.CheckBox chkDelete;
        private FilePicker filePicker;
        private System.Windows.Forms.TextBox textError;
    }
}