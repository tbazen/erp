using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using INTAPS.ClientServer;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class ImportPayment : Form
    {


        public ImportPayment()
        {
            this.InitializeComponent();
            if (!string.IsNullOrEmpty(INTAPS.SubscriberManagment.Client.General.PCSettings.Default.ImportExportFolder))
                filePicker.Text = INTAPS.SubscriberManagment.Client.General.PCSettings.Default.ImportExportFolder;
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            INTAPS.SubscriberManagment.Client.General.PCSettings.Default.ImportExportFolder = filePicker.Text;
            INTAPS.SubscriberManagment.Client.General.PCSettings.Default.Save();
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            Exception exception;
            try
            {
                base.Enabled = false;
                string[] files = Directory.GetFiles(this.filePicker.Text, "*.posmessage");
                Array.Sort<string>(files);
                this.lvErrors.Items.Clear();
                int i = 0;
                foreach (string fileName in files)
                {
                    ListViewItem item;

                    try
                    {
                        var bytes=File.ReadAllBytes(fileName);
                        SubscriberManagmentClient.postOfflineMessages(bytes);
                        if (this.chkDelete.Checked)
                        {
                            File.Delete(fileName);
                        }
                        item = new ListViewItem(fileName);
                        item.SubItems.Add("Done");
                        item.ForeColor = Color.Green;

                        this.lvErrors.Items.Insert(0, item);
                        Application.DoEvents();
                    }
                    catch (Exception ex)
                    {
                        item = new ListViewItem(fileName);
                        item.ForeColor = Color.Red;
                        item.SubItems.Add(ex.Message);
                        this.lvErrors.Items.Insert(0, item);
                        item.Tag = ex;
                        i++;
                    }
                    Application.DoEvents();
                }
                if (i == 0)
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Update imported with no errors.");
                }
                else
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserMessage(string.Concat(new object[] { "Update imported with ", i, " error", (this.lvErrors.Items.Count == 1) ? "." : "s." }));
                }
            }
            catch (Exception exception2)
            {
                exception = exception2;
                UIFormApplicationBase.CurrentAppliation.HandleException("Error importing bills.", exception);
            }
            finally
            {
                base.Enabled = true;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void ImportBill_Load(object sender, EventArgs e)
        {
        }

        private void lvErrors_SelectedIndexChanged(object sender, EventArgs e)
        {
            textError.Text = "";
            if (lvErrors.SelectedItems.Count == 0)
                return;
            Exception ex = lvErrors.SelectedItems[0].Tag as Exception;
            if (ex == null)
                return;
            textError.Text = ex.Message + "\n" + ex.StackTrace;
        }


    }
}