﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting;
using BIZNET.iERP;
using INTAPS.UI;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class PaymentInstrumentEditor : Form
    {
        public PaymentInstrumentItem instrument;
        EnterKeyNavigation _nav = new EnterKeyNavigation();
        double _amount;
        SalePointBDE bde;
        public PaymentInstrumentEditor(SalePointBDE bde, double amount)
        {
            InitializeComponent();
            this.bde = bde;
            _nav.AddNavigableItem(new ComboNavigator(comboType));
            _nav.AddNavigableItem(new ComboNavigator(comboBank));
            _nav.AddNavigableItem(new ComboNavigator(comboBranch));
            _nav.AddNavigableItem(new TextNavigator(textAccountNumber));
            _nav.AddNavigableItem(new TextNavigator(textReference));
            _nav.AddNavigableItem(new TextNavigator(textAmount));
            _nav.AddNavigableItem(new ButtonNavigator(buttonOk));
            _nav.StartEdit();

            _amount = amount;
            foreach (PaymentInstrumentType type in bde.getAllPaymentInstrumentTypes())
            {
              //  if (type.isBankDocument)
                    comboType.Items.Add(type);
            }
            if (comboType.Items.Count > 0)
                comboType.SelectedIndex = 0;
            foreach (BankInfo bank in bde.getAllBanks())
            {
                comboBank.Items.Add(bank);
            }
            BankInfo unlistedBank = new BankInfo();
            unlistedBank.id = -1;
            unlistedBank.name = "Unlisted";
            comboBank.Items.Add(unlistedBank);
            comboBank.SelectedIndex = 0;
            textAmount.Text = AccountBase.FormatAmount(amount);
        }
        public PaymentInstrumentEditor(SalePointBDE bde, PaymentInstrumentItem item,double amount):this(bde,amount)
        {
            foreach (PaymentInstrumentType type in comboType.Items)
            {
                if (type.itemCode.Equals(item.instrumentTypeItemCode))
                {
                    comboType.SelectedItem = type;
                    break;
                }
            }
            BankBranchInfo branch=bde.getBankBranchInfo(item.bankBranchID);
            foreach (BankInfo bi in comboBank.Items)
            {
                if (bi.id == branch.bankID)
                {
                    comboBank.SelectedItem = bi;
                    break;
                }
            }
            foreach (BankBranchInfo bi in comboBranch.Items)
            {
                if (bi.id == branch.id)
                {
                    comboBank.SelectedItem = bi;
                    break;
                }
            }
            textAccountNumber.Text = item.accountNumber;
            textReference.Text = item.documentReference;
            date.DateTime = item.documentDate;
            textAmount.Text = AccountBase.FormatAmount(item.amount);
            textRemark.Text = item.remark;
            this.instrument = item;
        }

        private void comboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            PaymentInstrumentType instrumentType = comboType.SelectedItem as PaymentInstrumentType;
            comboBank.Enabled=comboBranch.Enabled= textAccountNumber.Enabled = instrumentType.isTransferFromAccount;
            comboDeposit.Enabled = instrumentType.isBankDocument && !instrumentType.isToken;
        }

        private void comboBank_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBranch.Items.Clear();
            BankInfo bi = comboBank.SelectedItem as BankInfo;
            if (bi != null && bi.id != -1)
            {
                foreach (BankBranchInfo bank in bde.getAllBranchsOfBank(bi.id))
                {
                    comboBranch.Items.Add(bank);
                }
            }
            BankBranchInfo unlistedBank = new BankBranchInfo();
            unlistedBank.id = -1;
            unlistedBank.name = "Unlisted";
            comboBranch.Items.Add(unlistedBank);
            comboBranch.SelectedIndex = 0;
            
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            PaymentInstrumentType instrumentType = comboType.SelectedItem as PaymentInstrumentType;
            if (instrumentType == null)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Select payment instrument type");
                return;
            }
            BankBranchInfo branch = comboBranch.SelectedItem as BankBranchInfo;
            string accountNumber = textAccountNumber.Text.Trim();
            if (instrumentType.isTransferFromAccount)
            {
                if (branch == null)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Select bank and branch");
                    return;
                }
                if (string.IsNullOrEmpty(accountNumber))
                {

                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter account number");
                        textAccountNumber.Focus();
                        return;
                    }
                }
            }
            else
            {
                branch = null;
                accountNumber = "";
            }
            int depositAccount=-1;
            if (instrumentType.isBankDocument && !instrumentType.isToken)
            {
                depositAccount = comboDeposit.mainCsAccountID;
                if (depositAccount==-1)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Select deposit bank account");
                    return;
                }
            }
            string reference=textReference.Text.Trim();
            if (string.IsNullOrEmpty(reference))
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter reference number");
                textReference.Focus();
                return;
            }
            double amount;
            if (!double.TryParse(textAmount.Text, out amount) || !AccountBase.AmountGreater(amount, 0))
            {

                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter valid amount");
                textAmount.Focus();
                return;
            }
            if (AccountBase.AmountGreater(amount, _amount))
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter amount less than or equal to "+AccountBase.FormatAmount(_amount));
                textAmount.Focus();
                textAmount.SelectAll();
                return;
            }
            if (DateTime.Now < date.DateTime)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Future dated document is not valid");
                date.Focus();
                return;
            }

            instrument = new PaymentInstrumentItem();
            instrument.instrumentTypeItemCode = instrumentType.itemCode;
            instrument.bankBranchID = branch==null?-1: branch.id;
            instrument.accountNumber = accountNumber;
            instrument.documentReference = reference;
            instrument.documentDate = date.DateTime;
            instrument.remark = textRemark.Text;
            instrument.amount = amount;
            instrument.despositToBankAccountID=depositAccount;
            this.DialogResult = DialogResult.OK;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

    }
}
