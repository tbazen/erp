
using INTAPS.Accounting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO.Ports;
using System.Text;
using System.Threading;
using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public class PosPrinter
    {
        private bool m_errorMode = true;
        private SerialPort m_port;
        private System.Windows.Forms.Timer m_timer;
        private const int MAX_POLL_COUNT = 50;
        private const int POLL_INTERVAL = 200;
        private const int TIME_OUT = 10;
        private const int TOTAL_CHARS = 0x26;

        public PosPrinter()
        {
            this.InitPrinter();
        }

        private void CheckErrorStatus()
        {
            Console.WriteLine("Checking error");
            this.m_port.Write(new byte[] { 0x10, 4, 3 }, 0, 3);
            int num = 0;
            while (true)
            {
                num++;
                Thread.Sleep(200);
                int bytesToRead = this.m_port.BytesToRead;
                if (bytesToRead == 0)
                {
                    num++;
                    if (num > 50)
                    {
                        this.ClosePort();
                        throw new Exception("Time out waiting for printer");
                    }
                }
                else
                {
                    Console.WriteLine(bytesToRead + " bytes to read");
                    List<byte[]> list = new List<byte[]>();
                    int num3 = 0;
                    do
                    {
                        num3 += bytesToRead;
                        byte[] item = new byte[bytesToRead];
                        list.Add(item);
                        Console.WriteLine("Reading " + bytesToRead);
                        this.m_port.Read(item, 0, bytesToRead);
                        Console.WriteLine((bytesToRead = this.m_port.BytesToRead) + " bytes to read");
                    }
                    while (bytesToRead > 0);
                    Console.WriteLine("All bytes read");
                    byte[] array = new byte[num3];
                    int index = 0;
                    foreach (byte[] buffer3 in list)
                    {
                        buffer3.CopyTo(array, index);
                        index += buffer3.Length;
                    }
                    string str = Encoding.ASCII.GetString(array);
                    string str2 = "";
                    for (int i = 0; i < array.Length; i++)
                    {
                        str2 = str2 + array[i].ToString("X");
                    }
                    Console.WriteLine("Read :" + str + " (" + str2 + ")");
                    if (array[0] != 0x12)
                    {
                        this.ClosePort();
                        throw new Exception("Printer error");
                    }
                    Console.WriteLine("Ok Status");
                    return;
                }
                if (num >= 100)
                {
                    this.ClosePort();
                    throw new Exception("Time out waiting for printer status.");
                }
            }
        }

        private void ClosePort()
        {
            this.m_port.Close();
        }

        private void CutPaper()
        {
            this.m_port.Write(new byte[] { 10, 10, 10, 10, 10, 0x1b, 0x69 }, 0, 7);
        }

        private void InitPrinter()
        {
            this.m_port = new SerialPort();
            this.m_port.PortName = ConfigurationManager.AppSettings["PosPrinterPort"];
            this.m_timer = new System.Windows.Forms.Timer();
            this.m_timer.Interval = 0xbb8;
            this.m_timer.Enabled = false;
            this.m_timer.Tick += new EventHandler(this.TimerTick);
            this.TryOpen();
        }

        private void Print(string text)
        {
            this.m_port.Write(text);
        }

        public void PrintBody(CustomerPaymentReceipt receipt, string[] headerLines, bool quantityColumn, bool copy)
        {
            throw new NotImplementedException("PosPrinter.PrintBody not upgraded");
            //this.ResetPrinter();
            //this.PrintLine("Date: " + receipt.DocumentDate.ToString("MM/dd/yyyy hh:mm"));
            //this.SetTextSize(0x11);
            //this.PrintLine("No: " + receipt.PaperRef + (copy ? "(Copy)" : ""));
            //this.SetTextSize(0);
            //this.PrintLine("Paid by:" + receipt.paidBy);
            //foreach (string str in headerLines)
            //{
            //    this.PrintLine(str);
            //}
            //this.SetUnderlineMode(1);
            //this.PrintLine("                                     ");
            //this.SetUnderlineMode(0);
            //this.PrintLine("");
            //this.SetUnderlineMode(1);
            //if (quantityColumn)
            //{
            //    this.PrintLine("Item     UnitPrice    Quantity Amount");
            //}
            //else
            //{
            //    this.PrintLine("Item                              Amount");
            //}
            //this.SetUnderlineMode(0);
            //double num = 0.0;
            //for (int i = 0; i < receipt.item.Length; i++)
            //{
            //    this.PrintLine((i + 1) + "." + receipt.item[i]);
            //    string text = "";
            //    if (quantityColumn)
            //    {
            //        string str3;
            //        string str4;
            //        if (Account.AmountGreater(receipt.unitValue[i], 0.0))
            //        {
            //            str3 = receipt.unitValue[i].ToString("#,#0.00");
            //        }
            //        else
            //        {
            //            str3 = "      ";
            //        }
            //        if (Account.AmountGreater(receipt.quantity[i], 0.0))
            //        {
            //            str4 = receipt.quantity[i].ToString();
            //        }
            //        else
            //        {
            //            str4 = "      ";
            //        }
            //        text = str3 + "   " + str4;
            //    }
            //    this.SetUnderlineMode(1);
            //    text = text + ((text == "") ? "" : "    ") + receipt.amount[i].ToString("0,0.00");
            //    int length = text.Length;
            //    for (int j = 0; j < (0x26 - length); j++)
            //    {
            //        text = " " + text;
            //    }
            //    this.PrintLine(text);
            //    this.SetUnderlineMode(0);
            //    num += receipt.amount[i];
            //}
            //this.PrintLine("");
            //this.SetTextSize(0x11);
            //this.Print("      Total ");
            //this.SetUnderlineMode(2);
            //this.PrintLine(receipt.TotalAmount.ToString("0,0.00"));
            //this.SetTextSize(0);
            //this.SetUnderlineMode(0);
            //this.PrintLine("");
            //this.PrintLine("");
            //this.PrintLine("Casher:" + receipt.casheirName);
            //this.PrintLine("Water Suppply Information System");
            //this.PrintLine("Computerized by INTAPS.");
            //this.PrintLine("www.intaps.com");
            //this.CheckErrorStatus();
            //this.CutPaper();
        }

        public void PrintHeader(bool quantityColumn)
        {
            if (this.m_errorMode)
            {
                throw new Exception("Printer is in error.");
            }
            this.ResetPrinter();
            this.PrintLine("         Dire Dawa Water Supply");
            this.PrintLine("                  and");
            this.PrintLine("           Sewerage Authority");
            this.PrintLine("             Cash Receipt");
            this.PrintLine("");
            this.PrintLine("Tel: 025 112 0094, 025 191 5731/5072\nPO.Box 446, Dire Dawa");
            this.SetUnderlineMode(1);
            this.PrintLine("                                     ");
            this.SetUnderlineMode(0);
            this.CheckErrorStatus();
        }

        private void PrintLine(string text)
        {
            string[] strArray = text.Split(new char[] { '\n' });
            foreach (string str in strArray)
            {
                this.m_port.Write(str);
                this.m_port.Write(new byte[] { 10 }, 0, 1);
                Console.WriteLine(str);
            }
        }

        public void PrintReceipt(CustomerPaymentReceipt receipt, string[] headerlines, bool quantityColumn)
        {
            if (this.m_errorMode)
            {
                throw new Exception("Printer is in error.");
            }
            this.PrintHeader(quantityColumn);
            this.PrintBody(receipt, headerlines, quantityColumn, false);
        }

        private void ResetPrinter()
        {
            this.m_port.Write(new byte[] { 0x1b, 0x40 }, 0, 2);
        }

        private void SetTextSize(byte sz)
        {
            this.m_port.Write(new byte[] { 0x1d, 0x21, sz }, 0, 3);
        }

        private void SetUnderlineMode(byte mode)
        {
            this.m_port.Write(new byte[] { 0x1b, 0x2d, mode }, 0, 3);
        }

        private void TimerTick(object sender, EventArgs e)
        {
            this.TryOpen();
        }

        private void TryOpen()
        {
            try
            {
                Console.WriteLine("Trying to open port.");
                this.m_port.Open();
                this.CheckErrorStatus();
                this.m_timer.Enabled = false;
                this.m_errorMode = false;
                Console.WriteLine("Done opening port.");
            }
            catch (Exception exception)
            {
                Console.WriteLine("Failed to open port." + exception.Message);
                this.m_timer.Enabled = true;
                this.m_errorMode = true;
            }
        }

        public bool InError
        {
            get
            {
                return this.m_errorMode;
            }
        }
    }
}

