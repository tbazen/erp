using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.ClientServer;
using INTAPS.RDBMS;
using INTAPS.SubscriberManagment;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using System.Windows.Forms;
using BIZNET.iERP;
using System.Data.SqlClient;
using System.Linq;
namespace INTAPS.SubscriberManagment.Client
{
    public partial class SalePointBDE : BDEBase
    {
        static System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormater = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        public const int NMSG_PER_FILE = 1000;
        private ConnectionMode _connectionMode;
        private ReportDefination m_detailReport = null;
        private ReportDefination m_statusReport = null;
        private SPSystemParameters m_sysPars;
        MessageRepository _msgRepo = null;
        PrepaidDriver.IPrepaidCardDriver _cardDriver = null;
        public MessageRepository MsgRepo
        {
            get { return _msgRepo; }
            set { _msgRepo = value; }
        }
        public SalePointBDE(string DBName, string connectionString)
            : base("SalesPoint", DBName, new SQLHelper(connectionString), connectionString, false)
        {
            loadTypeIDs();
            loadCardDriver();
        }

        private void loadCardDriver()
        {
            _cardDriver = null;
            String t = System.Configuration.ConfigurationManager.AppSettings["cardDriver"];
            if (String.IsNullOrEmpty(t))
                return;
            Type type= Type.GetType(t);
            if (type == null)
                Console.WriteLine("Card driver type {0} couldn't be loaded", type);
            try
            {
                _cardDriver = (PrepaidDriver.IPrepaidCardDriver)Activator.CreateInstance(type);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error trying to instantiate card driver {type}. \n {ex.Message}\n{ex.StackTrace}");
                _cardDriver = null;
            }
        }
        public PrepaidDriver.IPrepaidCardDriver cardDriver
        {
            get
            {
                return this._cardDriver;
            }
        }
        public void Connect(ConnectionMode mode, bool cacheObjects)
        {
            this._connectionMode = mode;
            if (mode != ConnectionMode.Connected)
            {
                AutoIncrement.Initialize(dspWriter);
                this.m_sysPars = this.WriterHelper.LoadSystemParameters<SPSystemParameters>();
                _msgRepo = new MessageRepository(this, true);

                //start update loop
                _runUpdateLoop = false;
                if ("true".Equals(System.Configuration.ConfigurationManager.AppSettings["runUpdateLoop"]))
                {
                    startUpateLoop();
                }
                ObjectTable.bindConnection(dspWriter);
                ObjectTable.loadTypes(typeof(SubscriptionIndexer).Assembly);
            }
            else if (mode == ConnectionMode.Connected)
            {

                PaymentCenter center = SubscriberManagmentClient.GetCurrentPaymentCenter();
                if (center == null)
                {
                    throw new ServerUserMessage("Only casher can open sale point.");
                }
                if ((center.collectionMethod&CollectionMethod.None)!=CollectionMethod.None)
                    throw new ServerUserMessage("You are not allowed to operate an online payment center.");
                this.m_sysPars = new SPSystemParameters();
                this.m_sysPars.centerName = center.centerName;
                this.m_sysPars.centerAccountID = center.casherAccountID;
                this.m_sysPars.receiptTitle = SubscriberManagmentClient.GetSystemParameter("receiptTitle") as string;
                string val;
                val = ConfigurationManager.AppSettings["StatusReport"];
                if (!string.IsNullOrEmpty(val))
                {
                    this.m_statusReport = AccountingClient.GetReportDefinationInfo((string)val);
                }

                val = ConfigurationManager.AppSettings["DetailReport"];
                if (!string.IsNullOrEmpty(val))
                {
                    this.m_detailReport = AccountingClient.GetReportDefinationInfo((string)val);
                }
            }
        }
        
        void loadTypeIDs()
        {
            string alist = System.Configuration.ConfigurationManager.AppSettings["typeIDAssemblies"];
            if (string.IsNullOrEmpty(alist))
                return;
            string[] a = alist.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string aa in a)
                ObjectTable.loadTypeIDs(System.Reflection.Assembly.Load(aa));
            if (_connectionMode != ConnectionMode.Connected)
                ObjectTable.loadTypes(System.Reflection.Assembly.GetCallingAssembly());
        }
        public SerialBatch batch
        {
            get
            {
                SerialBatch batch = new SerialBatch();
                batch.serialFrom = this.m_sysPars.batchFrom;
                batch.serialTo = this.m_sysPars.batchTo;
                batch.batchDate = DateTime.Now;
                batch.active = true;
                return batch;
            }
        }
        public ConnectionMode ConMode
        {
            get
            {
                return this._connectionMode;
            }
        }
        public SPSystemParameters SysPars
        {
            get
            {
                return this.m_sysPars;
            }
        }

        public string GetSalesReport()
        {
            return "Not implemented";
        }

        public string GetStatusHTML()
        {
            try
            {
                DateTime time1 = DateTime.Today.Date;
                DateTime time2 = time1.AddDays(1.0);

                if (this._connectionMode == ConnectionMode.Connected)
                {
                    string htmlHeaders;
                    return AccountingClient.EvaluateEHTML(this.m_statusReport.id, new object[] { time1, time2, m_sysPars.centerAccountID, -1 }, out htmlHeaders);
                }
                else
                {
                    lock (this.dspWriter)
                    {
                        int receiptCount = 0;
                        HashSet<int> paidBillsTolday = new HashSet<int>();
                        int receiptCountToday = 0;
                                                double totalPaidToday = 0.0;
                        double totalPaid = 0.0;

                        _msgRepo.parseRepo(delegate(OfflineMessage msg)
                        {
                            PaymentReceivedMessage pmsg = msg.messageData as PaymentReceivedMessage;
                            if (pmsg == null)
                                return true;
                            if (pmsg.payment.DocumentDate >= time1 && pmsg.payment.DocumentDate<time2)
                            {
                                receiptCountToday++;
                                foreach(int pt in pmsg.payment.settledBills)
                                    paidBillsTolday.Add(pt);
                                totalPaidToday+=pmsg.paidAmount;
                                //if (msg.delivered)
                                //{
                                //    foreach (int r in pmsg.payment.settledBills)
                                //    {
                                //        if (getCustomerBillRecord(r).paymentDocumentID==-1)
                                //        {
                                //            foreach (BillItem bi in getCustomerBillItems(r))
                                //            {
                                //                if(bi.
                                //            }
                                //        }
                                //    }
                                //}
                            }
                            if (!msg.delivered)
                            {
                                receiptCount++;
                                totalPaid+=pmsg.paidAmount;
                                Console.WriteLine("{0}", pmsg.payment.receiptNumber.reference);
                            }
                            return true;
                        });


                        int nBills = (int)dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.CustomerBill where paymentDocumentID=-1 and paymentDiffered=0",this.DBName));

                        StringBuilder strTotal = new StringBuilder();
                        strTotal.Append("<h3> Total Collected Today");
                        strTotal.Append(HttpUtility.HtmlEncode(": " + totalPaidToday.ToString("0,0.00")));
                        strTotal.Append("</h3>");
                        strTotal.Append("<h3>");
                        strTotal.Append(receiptCountToday.ToString());
                        strTotal.Append(" receipts issued today.</h3>");

                        strTotal.Append("<h3>Amount not transfered to server");
                        strTotal.Append(HttpUtility.HtmlEncode(": " + totalPaid.ToString("0,0.00")));
                        strTotal.Append("</h3>");
                        strTotal.Append("<h3>");
                        strTotal.Append(receiptCount.ToString());
                        strTotal.Append(" receipts not transfered to server.</h3>");
                        strTotal.Append("<h3>");
                        strTotal.Append(nBills.ToString());
                        strTotal.Append(" bills in this payment center.</h3>");
                        
                        object lastUpdate = this.dspWriter.ExecuteScalar("SELECT max([updateID])  FROM [SalePoint].[dbo].[UpdateLog]");
                        if (lastUpdate is int)
                        {
                            strTotal.Append("<h3>Update Number:</h3>");
                            strTotal.Append(lastUpdate.ToString());
                        }
                        else
                            strTotal.Append("<h3>No Update</h3>");
                        string html = strTotal.ToString();
                        return html;
                    }
                }
            }
            catch (Exception exception)
            {
                return exception.Message + "\n" + exception.StackTrace;
            }
        }
        
        //SystemParameters methods
        void checkConfiguration()
        {
            if (m_sysPars == null)
                throw new ServerUserMessage("Configuration is not loaded");
            if (m_sysPars.centerAccountID < 1)
                throw new ServerUserMessage("Cash account ID is not valid");
            if (string.IsNullOrEmpty(m_sysPars.cashInstrumentCode))
                throw new ServerUserMessage("Cash Instrument is not valid");
            if (getAllPaymentInstrumentTypes().Length == 0)
                throw new ServerUserMessage("No payment instrument found");
        }
        public string getCashInstrumentCode()
        {
            if (_connectionMode == ConnectionMode.Connected)
                return BIZNET.iERP.Client.iERPTransactionClient.GetSystemParamter("cashInstrumentCode") as string;
            return m_sysPars.cashInstrumentCode;
        }
        public int getLastUpdateID()
        {
            lock (dspWriter)
            {
                object _maxUpdateID = this.WriterHelper.ExecuteScalar("Select max(updateID) from UpdateLog");
                return (_maxUpdateID is int) ? ((int)_maxUpdateID) : -1;
            }
        }
        public void updateParameter(SPSystemParameters pars)
        {
            if (_connectionMode == ConnectionMode.Connected)
                throw new ServerUserMessage("Updating configuraiton is not supported in connected mode");
            blockIfHasTransaction();
            try
            {
                dspWriter.SetSystemParameters<SPSystemParameters>(null, pars);
            }
            finally
            {
                this.m_sysPars = this.dspWriter.LoadSystemParameters<SPSystemParameters>();
            }
        }
        
        //Offline database query methods
        public T getOfflineObjectSubClass<T, IDType>(IDType id) where T : class
        {
            foreach (Type t in ObjectTable.allIdiedTypes())
            {
                if (t.IsSubclassOf(typeof(T)))
                {
                    int typeID = ObjectTable.getTypeID(t, true);
                    if (((int)dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.ObjectInstance where id='{1}' and typeID={2}", DBName, id.ToString(), typeID))) == 0)
                        continue;
                    byte[] data = dspWriter.ExecuteScalar(string.Format("Select data from {0}.dbo.ObjectInstance where id='{1}' and typeID={2}", DBName, id.ToString(), typeID)) as byte[];
                    if (data == null)
                        return null;
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(data);
                    T ret = binaryFormater.Deserialize(ms) as T;
                    ms.Dispose();
                    return ret;
                }
            }
            return null;
        }
        public T getOfflineObject<T, IDType>(IDType id) where T : class
        {
            Type t = typeof(T);
            int typeID = ObjectTable.getTypeID(t, true);
            byte[] data = dspWriter.ExecuteScalar(string.Format("Select data from {0}.dbo.ObjectInstance where id='{1}' and typeID={2}", DBName, id.ToString(), typeID)) as byte[];
            if (data == null)
                return null;
            System.IO.MemoryStream ms = new System.IO.MemoryStream(data);
            T ret = binaryFormater.Deserialize(ms) as T;
            ms.Dispose();
            return ret;
        }
        public T getOfflineObject<T>(int id1, int id2) where T : class
        {
            return getOfflineObject<T, int, int>(id1, id2);
        }
        public T getOfflineObject<T>(int id) where T : class
        {
            return getOfflineObject<T, int>(id);
        }
        public T getOfflineObject<T, IDType1, IDType2>(IDType1 id1, IDType2 id2) where T : class
        {
            Type t = typeof(T);
            int typeID = ObjectTable.getTypeID(t, true);
            lock (dspWriter)
            {
                byte[] data = dspWriter.ExecuteScalar(string.Format("Select data from {0}.dbo.ObjectInstance where id='{1}' and typeID={2}", this.DBName, id1 + "~" + id2, typeID)) as byte[];
                if (data == null)
                    return null;
                return SQLHelper.BinaryToObject(data) as T;
            }
        }
        List<ObjectType> getAllObjectsByType<ObjectType>() where ObjectType : class
        {
            IDataReader reader = dspWriter.ExecuteReader(string.Format("Select data from {0}.dbo.ObjectInstance where typeID={1} order by id", this.DBName, ObjectTable.getTypeID(typeof(ObjectType), true)));
            try
            {
                List<ObjectType> ret = new List<ObjectType>();
                while (reader.Read())
                {
                    byte[] data = reader[0] as byte[];
                    using (System.IO.MemoryStream ms = new System.IO.MemoryStream(data))
                    {
                        ret.Add(binaryFormater.Deserialize(ms) as ObjectType);
                    }
                }
                return ret;
            }
            finally
            {
                reader.Dispose();
            }
        }
        
        //Specialized offline database query methods
        public PaymentInstrumentType getInstrumentType(string itemCode)
        {
            if (_connectionMode == ConnectionMode.Connected)
                return BIZNET.iERP.Client.iERPTransactionClient.getPaymentInstrumentType(itemCode);
            return getOfflineObject<PaymentInstrumentType, string>(itemCode);
        }
        public string getBranchName(int bankBranchID)
        {
            BankBranchInfo branch;
            BankInfo bank;

            if (_connectionMode == ConnectionMode.Connected)
            {
                branch = BIZNET.iERP.Client.iERPTransactionClient.getBankBranchInfo(bankBranchID);
                bank = BIZNET.iERP.Client.iERPTransactionClient.getBankInfo(branch.bankID);
            }
            else
            {
                branch = getOfflineObject<BankBranchInfo>(bankBranchID);
                bank = getOfflineObject<BankInfo>(branch.bankID);
            }
            return bank.name + "-" + branch.name;
        }
        public PaymentInstrumentType[] getAllPaymentInstrumentTypes()
        {
            if (_connectionMode == ConnectionMode.Connected)
                return BIZNET.iERP.Client.iERPTransactionClient.getAllPaymentInstrumentTypes();
            return getAllObjectsByType<PaymentInstrumentType>().ToArray();
        }

        public BankInfo[] getAllBanks()
        {
            if (_connectionMode == ConnectionMode.Connected)
                return BIZNET.iERP.Client.iERPTransactionClient.getAllBanks();
            return getAllObjectsByType<BankInfo>().ToArray();
        }
        public BankBranchInfo[] getAllBranchsOfBank(int bankID)
        {
            if (_connectionMode == ConnectionMode.Connected)
                return BIZNET.iERP.Client.iERPTransactionClient.getAllBranchsOfBank(bankID);
            return getAllObjectsByType<BankBranchInfo>().Where(x => x.bankID == bankID).ToArray();
        }
        public BankBranchInfo getBankBranchInfo(int branchID)
        {
            if (_connectionMode == ConnectionMode.Connected)
                return BIZNET.iERP.Client.iERPTransactionClient.getBankBranchInfo(branchID);
            return getOfflineObject<BankBranchInfo>(branchID);
        }

        public Subscriber[] searchSustomers(string query, int index, int pageSize, out int N)
        {
            throw new NotImplementedException();
        }
        public CustomerBillRecord getCustomerBillRecord(int billRecordID)
        {
            if (_connectionMode == ConnectionMode.Connected)
                return SubscriberManagmentClient.getCustomerBillRecord(billRecordID);
            return getOfflineObject<CustomerBillRecord>(billRecordID);

        }
        public CustomerBillDocument getCustomerBillDocument(int billRecordID)
        {
            if (_connectionMode == ConnectionMode.Connected)
                return AccountingClient.GetAccountDocument(billRecordID, true) as CustomerBillDocument;
            return getOfflineObjectSubClass<CustomerBillDocument, int>(billRecordID);

        }
        public BillItem[] getCustomerBillItems(int billRecordID)
        {
            if (_connectionMode == ConnectionMode.Connected)
                return SubscriberManagmentClient.getCustomerBillItems(billRecordID);
            return dspWriter.GetSTRArrayByFilter<BillItem>("customerBillID=" + billRecordID);
        }
        public Subscription GetSubscription(string code, DateTime dateTime)
        {
            if (this._connectionMode == ConnectionMode.Connected)
                return SubscriberManagmentClient.GetSubscription(code, DateTime.Now.Ticks);
            throw new ServerUserMessage("Prepayment can't be done offline");
        }
        public Subscription GetSubscription(int conID, DateTime dateTime)
        {
            if (this._connectionMode == ConnectionMode.Connected)
                return SubscriberManagmentClient.GetSubscription(conID, DateTime.Now.Ticks);
            throw new ServerUserMessage("Prepayment can't be done offline");
        }
        public CustomerBillDocument[] SearchBill(SubscriberSearchField field, string code, out Subscriber customer)
        {
            if (this._connectionMode == ConnectionMode.Connected)
            {
                if (field == SubscriberSearchField.CustomerCode)
                    customer = SubscriberManagmentClient.GetSubscriber(code);
                else
                {
                    Subscription subsc = SubscriberManagmentClient.GetSubscription(code, DateTime.Now.Ticks);
                    if (subsc == null)
                        customer = null;
                    else
                        customer = subsc.subscriber;
                }
                if (customer == null)
                {
                    throw new ServerUserMessage("Invalid customer code " + code);
                }
                return SubscriberManagmentClient.getBillDocuments(-1, customer.id, -1, -1, true);
            }
            else
            {
                if (field == SubscriberSearchField.CustomerCode)
                {
                    Subscriber[] ss = dspWriter.GetSTRArrayByFilter<Subscriber>("customerCode='" + code + "'");
                    if (ss.Length == 0)
                    {
                        throw new ServerUserMessage("Invalid customer code " + code);
                    }
                    customer = ss[0];
                }
                else
                {
                    Subscription[] ss = dspWriter.GetSTRArrayByFilter<Subscription>("contractNo='" + code + "'");
                    if (ss.Length == 0)
                    {
                        throw new ServerUserMessage("Invalid contract no " + code);
                    }
                    Subscriber[] cust = dspWriter.GetSTRArrayByFilter<Subscriber>("id=" + ss[0].subscriberID);
                    if (cust.Length == 0)
                    {
                        throw new ServerUserMessage("Customer not found for contract  no " + code);
                    }
                    customer = cust[0];
                }
                int customerID = customer.id;
                int[] recs = dspWriter.GetColumnArray<int>(string.Format("Select id from {0}.dbo.CustomerBill where customerID={1}", this.DBName, customerID));
                List<CustomerBillDocument> _ret = new List<CustomerBillDocument>();
                foreach (int r in recs)
                {
                    if (isOfflinePaid(r))
                        continue;
                    CustomerBillRecord billRec = getCustomerBillRecord(r);
                    if (billRec.isPayedOrDiffered)
                        continue;
                    CustomerBillDocument billdoc = getOfflineObjectSubClass<CustomerBillDocument,int>(billRec.id);
                    _ret.Add(billdoc);
                }
                return _ret.ToArray();
            }
        }

        //Message repo methods
        bool isOfflinePaid(int billDocID)
        {
            bool ret = false;
            _msgRepo.parseRepo(delegate(OfflineMessage msg)
            {

                if (!msg.delivered && msg.messageData is PaymentReceivedMessage)
                {
                    PaymentReceivedMessage pr = (PaymentReceivedMessage)msg.messageData;

                    foreach (int billID in pr.payment.settledBills)
                    {
                        if (billDocID == billID)
                        {
                            ret = true;
                            return false;
                        }
                    }
                }
                return true;
            }
                    );
            return ret;
        }
        bool hasTranscation()
        {
            lock (this.dspWriter)
            {
                bool ret = false;
                _msgRepo.parseRepo(delegate(OfflineMessage msg)
                {
                    PaymentReceivedMessage pmsg = msg.messageData as PaymentReceivedMessage;
                    if (pmsg == null)
                        return true;
                    if (!msg.delivered)
                    {
                        ret = true;
                        return false;
                    }
                    return true;
                });
                return ret;
            }
        }
        enum TransactionTimeError
        {
            None,
            OtherDayTransactions,
            InvalidTimeSequence
        }
        TransactionTimeError hasOtherDayOrInvalidDateSequenceTranscation()
        {

            lock (this.dspWriter)
            {
                TransactionTimeError ret = TransactionTimeError.None;
                DateTime now = DateTime.Now;
                DateTime today = now.Date;
                _msgRepo.parseRepo(delegate(OfflineMessage msg)
                {
                    if (msg.sentDate > now)
                    {
                        ret = TransactionTimeError.InvalidTimeSequence;
                        return false;
                    }
                    PaymentReceivedMessage pmsg = msg.messageData as PaymentReceivedMessage;
                    if (pmsg == null)
                        return true;

                    if (!msg.delivered)
                    {
                        if (!msg.sentDate.Date.Equals(today))
                        {
                            ret = TransactionTimeError.OtherDayTransactions;
                            return false;
                        }
                    }
                    return true;
                });
                return ret;
            }
        }
        private void blockIfHasTransaction()
        {
            if (_connectionMode == ConnectionMode.Connected)
                return;
            if (hasTranscation())
                throw new ServerUserMessage("Export all transactions and import updates from the server first.");
        }
        private void blockIfHasOtherDayOrInvalidDateSequenceTransaction()
        {
            if (_connectionMode == ConnectionMode.Connected)
                return;
            switch(hasOtherDayOrInvalidDateSequenceTranscation())
            {
                case TransactionTimeError.OtherDayTransactions:
                    throw new ServerUserMessage("Export all transactions and import updates from the server first.");
                case TransactionTimeError.InvalidTimeSequence:
                    throw new ServerUserMessage("Invalid transaction date, please check your computer clock.");
            }                
        }
        public CustomerPaymentReceipt[] getReceipts(SubscriberSearchField field, string code)
        {
            if (_connectionMode == ConnectionMode.Connected)
            {
                CustomerPaymentReceipt[] ret = SubscriberManagmentClient.searchCustomerReceipts(code, field, m_sysPars.centerAccountID, -1);
                return ret;
            }
            else
            {
                List<CustomerPaymentReceipt> paidBills = new List<CustomerPaymentReceipt>();
                if (field == SubscriberSearchField.ConstactNo)
                {
                    Subscription[] subsc = dspWriter.GetSTRArrayByFilter<Subscription>("contractNo='" + code + "'");
                    if (subsc.Length == 0)
                        return new CustomerPaymentReceipt[0];
                    else
                        code = getOfflineObject<Subscriber>(subsc[0].subscriberID).customerCode;
                }
                _msgRepo.parseRepo(delegate(OfflineMessage msg)
                {
                    PaymentReceivedMessage pmsg = msg.messageData as PaymentReceivedMessage;
                    if (pmsg == null)
                        return true;
                    //if (!msg.delivered)   //Purpose of the check not clear
                    {
                        PaymentReceivedMessage receipt = msg.messageData as PaymentReceivedMessage;
                        if (!string.IsNullOrEmpty(code))
                            if (!receipt.payment.customer.customerCode.Equals(code, StringComparison.CurrentCultureIgnoreCase))
                                return true;
                        paidBills.Add(receipt.payment);
                    }
                    return true;
                });
                return paidBills.ToArray();
            }
        }


        //Transaction
        private void deleteFromIndex(GenericDiff diff)
        {
            ITypeIndexer indexer = ObjectTable.getIndexer(ObjectTable.getTypeByID(diff.typeID));
            if (indexer == null)
                return;
            indexer.deleteIndex(dspWriter, diff.objectID);
        }
        private void addToIndex(GenericDiff diff)
        {
            ITypeIndexer indexer = ObjectTable.getIndexer(ObjectTable.getTypeByID(diff.typeID));
            if (indexer == null)
                return;
            indexer.createIndex(dspWriter, diff.diffData);
        }
        public static void hotFixBillMonth(CustomerBillDocument bill, BillItem bi)
        {
            PeriodicBill pb=bill as PeriodicBill;
            if(pb==null)
                return;
            if (!bi.description.Contains(pb.period.name))
                bi.description += " " + pb.period.name;
            if ("true".Equals(System.Configuration.ConfigurationManager.AppSettings["appendEthiopianMonthToReceipts"], StringComparison.CurrentCultureIgnoreCase))
            {
                INTAPS.Ethiopic.EtGrDate etDate=Ethiopic.EtGrDate.ToEth(pb.period.fromDate);
                string etMonth = INTAPS.Ethiopic.EtGrDate.GetEtMonthName(etDate.Month);
                if(!bi.description.Contains(etMonth))
                    bi.description += "(" +etMonth + ")";
            }
        }
        public void setBillItemField(CustomerPaymentReceipt receipt)
        {
            List<BillItem> billItems = new List<BillItem>();
            for (int i = 0; i < receipt.settledBills.Length; i++)
            {
                CustomerBillDocument bill = getCustomerBillDocument(receipt.settledBills[i]);
                if (receipt.customer == null)
                    receipt.customer = bill.customer;
                else
                    if (receipt.customer.id != bill.customer.id)
                        throw new ServerUserMessage("Only bills of a single customer can be settled with a single receipt");
                receipt.settledBills[i] = bill.AccountDocumentID;
                foreach (BillItem bi in getCustomerBillItems(bill.AccountDocumentID))
                {
                    hotFixBillMonth(bill, bi);
                    billItems.Add(bi);
                }
            }
            receipt.billItems = billItems;
        }
        public CustomerPaymentReceipt PayBill(DateTime time, PaymentInstrumentItem[] instruments, CustomerBillDocument[] bills)
        {
            blockIfHasOtherDayOrInvalidDateSequenceTransaction();
            pauseUpdateLoop(true);
            try
            {
                lock (dspWriter)
                {
                    CustomerPaymentReceipt payment = new CustomerPaymentReceipt();
                    payment.settledBills = new int[bills.Length];
                    payment.assetAccountID = m_sysPars.centerAccountID;
                    List<BillItem> billItems = new List<BillItem>();
                    double totalBill = 0;
                    for (int i = 0; i < bills.Length; i++)
                    {
                        if (payment.customer == null)
                            payment.customer = bills[i].customer;
                        else
                            if (payment.customer.id != bills[i].customer.id)
                                throw new ServerUserMessage("Only bills of a single customer can be settled with a single receipt");
                        payment.settledBills[i] = bills[i].AccountDocumentID;

                        BillItem[] dbBillItems = getCustomerBillItems(bills[i].AccountDocumentID);
                        foreach (BillItem bi in dbBillItems)
                        {
                            hotFixBillMonth(bills[i], bi);
                            billItems.Add(bi);
                        }
                        foreach (BillItem item in bills[i].excludeExemption(dbBillItems))
                            totalBill += item.netPayment;
                    }
                    foreach (PaymentInstrumentItem it in instruments)
                        totalBill -= it.amount;
                    if (!AccountBase.AmountEqual(totalBill, 0))
                        throw new ServerUserMessage("Payment amount and bill not equal");
                    if (payment.customer == null)
                        return null;
                    payment.paymentInstruments = instruments;
                    payment.ShortDescription = "Bill Settlment";
                    payment.billItems = billItems;
                    if (this._connectionMode == ConnectionMode.Connected)
                    {
                        int id = AccountingClient.PostGenericDocument(payment);
                        CustomerPaymentReceipt ret = AccountingClient.GetAccountDocument(id, true) as CustomerPaymentReceipt;
                        ret.billItems = payment.billItems;
                        return ret;
                    }
                    else
                    {
                        checkConfiguration();
                        _msgRepo.parseRepo(delegate(OfflineMessage msg)
                        {
                            if (!msg.delivered && msg.messageData is PaymentReceivedMessage)
                            {
                                PaymentReceivedMessage pr = (PaymentReceivedMessage)msg.messageData;
                                foreach (CustomerBillDocument bill in bills)
                                {
                                    foreach (int billID in pr.payment.settledBills)
                                    {
                                        if (bill.AccountDocumentID == billID)
                                            throw new ServerUserMessage("One or more bills already settled");
                                    }
                                }
                            }
                            return true;
                        }
                        );
                        int batchFrom;
                        object maxUsed = this.dspWriter.ExecuteScalar("Select max([val]) from UsedSerial");
                        if ((maxUsed is int) && ((int)maxUsed) >= this.m_sysPars.batchFrom)
                        {
                            batchFrom = ((int)maxUsed) + 1;
                            if (batchFrom > this.m_sysPars.batchTo)
                            {
                                throw new ServerUserMessage("Serial numbers exahusted.");
                            }
                        }
                        else
                        {
                            batchFrom = this.m_sysPars.batchFrom;
                        }

                        UsedSerial serial = new UsedSerial();
                        serial.batchID = this.m_sysPars.batchID;
                        serial.date = DateTime.Now;
                        serial.isVoid = false;
                        serial.val = batchFrom;
                        dspWriter.BeginTransaction();
                        try
                        {
                            serial.UseSerial(this.m_DBName, -1, this.dspWriter, this.batch);
                            payment.receiptNumber = new DocumentTypedReference(1, serial.val.ToString("000000000"), true);
                            PaymentReceivedMessage msg = new PaymentReceivedMessage();
                            msg.paymentCenterID = m_sysPars.centerAccountID;
                            msg.payment = payment;
                            msg.paidAmount = payment.totalAmount;

                            msg.eachAmount = new double[payment.settledBills.Length];
                            for (int j = 0; j < bills.Length; j++)
                                msg.eachAmount[j] = bills[j].total;

                            OfflineMessage omsg = new OfflineMessage();
                            omsg.messageData = msg;
                            omsg.sentDate = DateTime.Now;
                            _msgRepo.addMessage(-1, omsg);
                            dspWriter.CommitTransaction();
                        }
                        catch
                        {
                            dspWriter.RollBackTransaction();
                            throw;
                        }
                    }

                    return payment;
                }
            }
            finally
            {
                pauseUpdateLoop(false);
                if(_connectionMode!=ConnectionMode.Connected)
                    dspWriter.CheckTransactionIntegrity();
            }
        }
        public void Update(MessageList msgList)
        {
            if (this._connectionMode == ConnectionMode.Connected)
            {
                throw new ServerUserMessage("Update is not valid in connected mode.");
            }
            if (!msgList.Verify())
            {
                throw new ServerUserMessage("Corrupt update.");
            }

            lock (this.WriterHelper)
            {
                object _maxUpdateID = this.WriterHelper.ExecuteScalar("Select max(updateID) from UpdateLog");
                int lastUpdateID = (_maxUpdateID is int) ? ((int)_maxUpdateID) : -1;
                this.WriterHelper.BeginTransaction();
                try
                {
                    int nNull = 0;
                    int nInconsitent = 0;
                    int nRepeat = 0;
                    List<string> list = new List<string>();
                    foreach (OfflineMessage msg in msgList.data)
                    {
                        if (msg.id <= lastUpdateID)
                            continue;
                        if (msg.previousMessageID != lastUpdateID)
                            throw new ServerUserMessage("Unexpected update :" + msg.id);
                        if (msg.messageData is MessageAcknowledgeMessage)
                        {
                            MessageAcknowledgeMessage ack = msg.messageData as MessageAcknowledgeMessage;
                            if (ack.paymentCenterID == m_sysPars.centerAccountID)
                            {
                                _msgRepo.acknowledgeMessage(-1, ack.messageID, DateTime.Now);
                            }
                        }
                        else if (msg.messageData is GenericDiff)
                        {
                            GenericDiff diff = msg.messageData as GenericDiff;
                            ObjectInstance instance;
                            switch (diff.changeType)
                            {
                                case GenericDiffType.Replace:
                                case GenericDiffType.Delete:
                                    dspWriter.Delete(this.DBName, "ObjectInstance", "id='" + diff.objectID + "' and typeID=" + diff.typeID);
                                    deleteFromIndex(diff);
                                    break;
                            }
                            
                            switch (diff.changeType)
                            {
                                case GenericDiffType.Add:
                                case GenericDiffType.Replace:
                                    instance = new ObjectInstance();
                                    instance.id = diff.objectID;
                                    instance.typeID = diff.typeID;
                                    if (ObjectTable.getTypeByID(diff.typeID)==null)
                                        throw new ServerUserMessage("Type ID " + diff.typeID + " is not loaded. Check typeIDAssemblies entry in the configureation file");
                                    if (diff.diffData == null)
                                    {
                                        nNull++;
                                    }
                                    else
                                    {
                                        //if (((int)dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo,ObjectInstance where id='{1}' and typeID={2}", this.DBName, diff.objectID, diff.typeID)))==1)
                                        //{
                                        //    nInconsitent++;
                                        //    dspWriter.Delete(this.DBName, "ObjectInstance", "id='" + diff.objectID + "' and typeID=" + diff.typeID);
                                        //    Console.WriteLine("Object being inserted already exists. DiffType: {0}, ObjectID: {1}, ObjectTypeID: {2}",diff.changeType,diff.objectID,diff.typeID);
                                        //}
                                        object exits=dspWriter.ExecuteScalar("Select count(*) from {0}.dbo.ObjectInstance where id='{1}' and typeID={2}".format(this.DBName,instance.id,instance.typeID));
                                        if (exits is int && (int)exits > 0)
                                        {
                                            //throw new Exception("Object {0} {1} already exists".format(instance.id,instance.typeID));
                                            nRepeat++;
                                        }
                                        else
                                        {
                                            dspWriter.InsertSingleTableRecord(this.DBName, instance, new string[] { "data" }, new object[] { SQLHelper.ObjectToBinary(diff.diffData) });
                                            addToIndex(diff);
                                        }
                                    }
                                    break;
                            }
                        }
                        dspWriter.Insert(this.DBName, "UpdateLog", new string[] { "updateID", "applyDate" }, new object[] { msg.id, DateTime.Now });
                        lastUpdateID = msg.id;
                    }
                    if (nNull > 0 || nInconsitent > 0 || nRepeat>0)
                    {
                        INTAPS.UI.UIFormApplicationBase.runInUIThread(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                            {
                                if (nNull > 0)
                                    System.Windows.Forms.MessageBox.Show(nNull + " null data skipped");
                                if (nInconsitent > 0)
                                    System.Windows.Forms.MessageBox.Show(nInconsitent + " data inconsistency. The system fixed them.");
                                if (nRepeat > 0)
                                    System.Windows.Forms.MessageBox.Show(nRepeat + " repeated objects. The system fixed them.");
                            }));
                    }
                    this.WriterHelper.CommitTransaction();

                }
                catch
                {
                    this.WriterHelper.RollBackTransaction();
                    throw;
                }

            }
        }
        public CustomerPaymentReceipt PayPrepaid(Subscription subscription, double cubic)
        {
            return PayPrepaid(subscription, cubic,null);
        }

        public CustomerPaymentReceipt PayPrepaid(Subscription subscription, double cubic,INTAPS.UI.HTML.ProcessTowParameter<CustomerPaymentReceipt,Exception> result)
        {
            if (this._connectionMode == ConnectionMode.Connected)
            {
                PrePaymentBill bill;
                
                if (_cardDriver != null && result!=null)
                {
                    Subscription check = SubscriberManagmentClient.GetSubscription(subscription.id, DateTime.Now.Ticks);
                    if (check == null)
                        throw new ServerUserMessage("Communication with server failed");
                    int meterNo;
                    if (!int.TryParse(check.serialNo, out meterNo))
                    {
                        result(null, new ServerUserMessage("Bylan water meter meter no can only be composed of digits"));
                        return null;
                    }
                    _cardDriver.topUp(check.id,check.subscriber.subscriberType.ToString(),
                        meterNo,
                        cubic, () =>
                    {
                        try
                        {
                            CustomerPaymentReceipt receipt = SubscriberManagmentClient.GeneratePrepaidReceipt(DateTime.Now, subscription.id, cubic, new PaymentInstrumentItem[0], out bill);
                            receipt.billItems = new List<BillItem>(bill.items);
                            result(receipt, null);
                        }
                        catch (Exception ex)
                        {
                            result(null, ex);
                        }
                    },
                    (m, ex) =>
                    {

                    });
                    return null;
                }
                else
                {
                    CustomerPaymentReceipt ret = SubscriberManagmentClient.GeneratePrepaidReceipt(DateTime.Now, subscription.id, cubic, new PaymentInstrumentItem[0], out bill);
                    ret.billItems = new List<BillItem>(bill.items);
                    return ret;
                }
            }
            throw new ServerUserMessage("Prepayment can't be done offline");
        }
        public void voidReceipt(CustomerPaymentReceipt receipt)
        {
            throw new NotImplementedException();
        }
    }
}