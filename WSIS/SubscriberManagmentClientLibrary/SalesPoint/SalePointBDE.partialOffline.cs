﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.ClientServer.Client;

namespace INTAPS.SubscriberManagment.Client
{
    partial class SalePointBDE
    {
        const int EVERYTHING_UPDATED_SLEEP = 10;
        const int CONNECTION_ERROR_SLEEP = 10;
        const int PAUSE_SLEEP = 10;

        bool _runUpdateLoop = false;
        bool _passwordVerified;
        DateTime _lastSessionTime;
        string _updateLoopStatus="";
        bool _updateLoopError;
        bool _pauseUpdate = false;
        public bool runUpdateLoop
        {
            get
            {
                return _runUpdateLoop;
            }
            set
            {
                _runUpdateLoop = value;
            }
        }

        public void pauseUpdateLoop(bool pause)
        {
            _pauseUpdate = pause;
        }
        public string getUpdateLoopStatus(out bool err)
        {
            err = _updateLoopError;
            return _updateLoopStatus;
        }
        
        void setOfflineUpdateError(string message,Exception ex)
        {
            _updateLoopStatus=message+"\n"+ex.Message;
            _updateLoopError=true;
        }
        void setOfflineUpdateOkStatus(string status)
        {
            _updateLoopStatus = status;
            _updateLoopError = false;
        }
        void startUpateLoop()
        {
            new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object o)
                {
                    updateLoop();
                }
                )).Start();
        }
        bool authenticateForUpdateLoop()
        {
            bool logedIn = false;
            INTAPS.UI.UIFormApplicationBase.runInUIThread(
                new INTAPS.UI.HTML.ProcessParameterless(delegate()
                {
                    OfflinePaymentLogin pl = new OfflinePaymentLogin(this.m_sysPars.userName);
                    pl.TryLogin(INTAPS.UI.UIFormApplicationBase.MainForm);
                    logedIn = pl.logedin;
                })
                );
            return logedIn;
        }
        void updateLoop()
        {
            _runUpdateLoop = true;
            _updateLoopError=false;
            string server = System.Configuration.ConfigurationManager.AppSettings["Server"];
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            while (_runUpdateLoop)
            {
                setOfflineUpdateOkStatus("Trying to establish connection with the server...");
                //Poll for server connection
                do
                {
                    try
                    {
                        ApplicationClient.pingServer(server);
                        break;
                    }
                    catch (Exception ex)
                    {
                        setOfflineUpdateError("Server not avialable." ,ex);
                        if (_runUpdateLoop) System.Threading.Thread.Sleep(CONNECTION_ERROR_SLEEP*1000);
                    }
                }
                while (_runUpdateLoop);
                if (!_runUpdateLoop)
                    break;
                
                if (!_passwordVerified)
                {
                    setOfflineUpdateOkStatus("Authenticating...");
                    if (!authenticateForUpdateLoop())
                        continue;
                    _passwordVerified = true;
                    updateSessionContactTime();
                }
                else
                {
                    
                    try
                    {
                        if (DateTime.Now.Subtract( _lastSessionTime).TotalSeconds > 60)
                        {
                            ApplicationClient.reConnect();
                            SubscriberManagmentClient.Connect(ApplicationClient.Server);
                            updateSessionContactTime();
                        }
                    }
                    catch(Exception ex)
                    {
                        setOfflineUpdateError("Server found but connection failed.", ex);
                        if(_runUpdateLoop)
                            System.Threading.Thread.Sleep(CONNECTION_ERROR_SLEEP*1000);
                        continue;
                    }
                }
                do
                {
                    try
                    {
                        setOfflineUpdateOkStatus("Checking for updates...");
                        int lastUpdateID = this.getLastUpdateID();
                        INTAPS.ClientServer.MessageList msgs = SubscriberManagmentClient.getMessageList(lastUpdateID + 1, lastUpdateID + 20);
                        updateSessionContactTime();
                        if (msgs.data.Length == 0)
                        {
                            setOfflineUpdateOkStatus("No more updates. Trying to transfer receipts to the server");
                            int from;
                            from = this.MsgRepo.getFirstUndeliveredMessageNumber();
                            if (from == -1)
                                from = 0;
                            else
                                from = from - 1;
                            int last = this.MsgRepo.getLastUndeliveredMessageNumber();
                            int count = last - from;
                            if (count > 0)
                            {
                                setOfflineUpdateOkStatus(count + " receipts found.");
                                int nFile = count / NMSG_PER_FILE + (count % NMSG_PER_FILE == 0 ? 0 : 1);

                                for (int j = 0; j < nFile; j++)
                                {
                                    int fileSize = count > NMSG_PER_FILE ? NMSG_PER_FILE : count;
                                    MessageList list = this.MsgRepo.getUndeliveredMessageList(from + 1, from + fileSize);
                                    if (list.data.Length == 0)
                                        continue;
                                    PaymentCenterMessageList pclist = new PaymentCenterMessageList(this.SysPars.centerAccountID, list);
                                    try
                                    {
                                        SubscriberManagmentClient.postOfflineMessages(pclist);
                                        setOfflineUpdateOkStatus(string.Format("{0} of {1} succesfully uploaded to server",j+1,nFile));
                                    }
                                    catch (Exception ex)
                                    {
                                        setOfflineUpdateError("Error trying to transfer sales", ex);
                                    }
                                }
                            }
                            else
                            {
                                setOfflineUpdateOkStatus("Everything updated. Sleeping for 5 minutes");
                                System.Threading.Thread.Sleep(EVERYTHING_UPDATED_SLEEP*1000);
                                continue;
                            }
                        }
                        
                        int i = 0;
                        foreach(OfflineMessage m in msgs.data)
                        {
                            while(_pauseUpdate)
                            {
                                setOfflineUpdateOkStatus("Pausing update process for 10 seconds");
                                System.Threading.Thread.Sleep(PAUSE_SLEEP * 1000);
                            }

                            Update(new MessageList(new OfflineMessage[] { m }));
                            this.WriterHelper.CheckTransactionIntegrity();
                            i++;
                            setOfflineUpdateOkStatus(i+ " of "+msgs.data.Length+ " updates applied");
                        }
                    }
                    catch (Exception ex)
                    {
                        setOfflineUpdateError("Failed to load or apply update data.", ex);
                        if (_runUpdateLoop)
                            System.Threading.Thread.Sleep(CONNECTION_ERROR_SLEEP*1000);
                        break;
                    }
                }
                while (true);
            }
        }

        private void updateSessionContactTime()
        {
            _lastSessionTime = DateTime.Now;
        }

    }
}
