using INTAPS.Accounting.Client;
using System;
namespace INTAPS.SubscriberManagment.Client
{

    public class SalePointSummary : RFSingleDateParameter
    {
        public override object[] GetParamters()
        {
            object[] paramters = base.GetParamters();
            object[] array = new object[paramters.Length + 2];
            paramters.CopyTo(array, 0);
            PaymentCenter currentPaymentCenter = SubscriberManagmentClient.GetCurrentPaymentCenter();
            array[paramters.Length] = (currentPaymentCenter == null) ? -1 : currentPaymentCenter.id;
            array[paramters.Length + 1] = -1;
            return array;
        }
    }
    public class RFCollectionSyummary : RFSingleDateParameter
    {
        public override object[] GetParamters()
        {
            object[] paramters = base.GetParamters();
            object[] array = new object[paramters.Length + 2];
            paramters.CopyTo(array, 0);
            PaymentCenter currentPaymentCenter = SubscriberManagmentClient.GetCurrentPaymentCenter();
            array[paramters.Length] = (currentPaymentCenter == null) ? -1 : currentPaymentCenter.id;
            BillPeriod currentPeriod = SubscriberManagmentClient.CurrentPeriod;
            array[paramters.Length+1] = (currentPeriod == null) ? -1 : currentPeriod.id;
            return array;
        }
    }
}

