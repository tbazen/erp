﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using INTAPS.ClientServer;
using INTAPS.ClientServer.Server;
using Microsoft.AspNetCore.Mvc;
using RazorEngine.Templating;
namespace INTAPS.SubscriberManagment.Service.Controllers
{
    public class MainController : Controller
    {
        [Route("subsc/custprof")]
        [HttpGet]
        public ActionResult getCustomerProfile([FromQuery]string sid,[FromQuery] string con=null, [FromQuery]int cust= -1)
        {

            string sessionID = string.IsNullOrEmpty(sid) ? base.Request.Cookies["sid"] : sid;
            SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
            if (!string.IsNullOrEmpty(con))
                cust = service.GetSubscription(con, DateTime.Now.Ticks).subscriberID;
            Subscriber customer = service.GetSubscriber(cust);
            Subscription[] connections = service.GetSubscriptions(cust, DateTime.Now.Ticks); 
            var model = new
            {
                sid = sessionID,
                cust=customer,
                cons=connections,
                custType=Subscriber.EnumToName(customer.subscriberType)
            };            
            return View(System.Configuration.ConfigurationManager.AppSettings["self_host_html_root"] + "subsc/customerprofile.cshtml", model.ToExpando());
        }

        [Route("subsc/GetCustomer/{sessionID}/{contractNo}")]
        [HttpGet]
        public ActionResult GetCustomer(string sessionID, string contractNo)
        {

            SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
            var customer = service.GetSubscription(contractNo, DateTime.Now.Ticks);
            if (customer != null)
            {
                Console.WriteLine($"GetCustomer With contractNo {contractNo} and name {customer.subscriber.name}");
                return Json(customer.subscriber.name);
            }
            else
            {
                Console.Write($"GetCustomer with contractNo {contractNo} not found");
                return Json(String.Empty);
            }


        }
    }
}
