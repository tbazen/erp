using System.Net;
using System.Net.Http;
using System.Web.Http;
using INTAPS.Accounting.Service;
using Microsoft.AspNetCore.Mvc;
using RazorEngine.Templating;

namespace INTAPS.SubscriberManagment.Service
{
    public class DashboardController : Controller
    {
        [Route("subsc/eps/dashboard")]
        [HttpGet]
        public ActionResult Get(string sid)
        {

            AccountingService accounting=INTAPS.ClientServer.ApplicationServer.GetSessionObject(sid, typeof(AccountingService)) as AccountingService;
            string httpHeaders;
            string db = accounting.EvaluateEHTML("EPS.Dashboard", new object[0], out httpHeaders);
            var model = new
            {
                sid=sid,
                userName = INTAPS.ClientServer.ApplicationServer.getSessionInfo(sid).UserName,
                wsisDashBoard=db
            };

            string template = System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["self_host_html_root"] + "\\subsc\\eps\\dashboard.cshtml");
            string templateKey = template.GetHashCode().ToString();

            string html = RazorEngine.Engine.Razor.RunCompile(template, templateKey, null, model);
            return Content(html, "text/html");            
        }
    }
}