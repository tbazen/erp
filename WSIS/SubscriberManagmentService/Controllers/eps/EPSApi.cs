using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BIZNET.iERP;
using BIZNET.iERP.Server;
using INTAPS.Accounting.Service;
using INTAPS.ClientServer;
using Microsoft.AspNetCore.Mvc;

namespace INTAPS.SubscriberManagment.Service
{
    public class APIReturn
    {
        public string error=null;
        public object res=null;
    }


    public class RecordPaymentController : Controller
    {

        [Route("subsc/eps/api/getBillDetail")]
        [HttpGet]
        public ActionResult getBillDetail(string sessionID, int billID)
        {
            try
            {
                SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
                iERPTransactionService erpService = ApplicationServer.GetSessionObject(sessionID, typeof(iERPTransactionService)) as iERPTransactionService;
                AccountingService accounting = INTAPS.ClientServer.ApplicationServer.GetSessionObject(sessionID, typeof(AccountingService)) as AccountingService;
                EPSBill bill = service.epsGetBill(billID);
                if (bill == null)
                    return Json(new APIReturn() { error = null, res = null });
                return Json(new APIReturn()
                {
                    error = null,
                    res = bill,
                });
            }
            catch (Exception ex)
            {
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }

        [Route("subsc/eps/api/recordPayment")]
        [HttpPost]
        public ActionResult recordPayment([FromQuery]string sessionID, [FromBody]EPSPayment payment)
        {
            try
            {
                SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
                return Json(new APIReturn() { error = null, res = service.epsRecordPayment(payment) });
            }
            catch (Exception ex)
            {
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }
        public class PayAvialableBillsPar
        {
            public string costomerCode;
            public Double amount;
            public string remark;
        }

        double TotalBills(SubscriberManagmentService service, int customerID,out string description)
        {
            double ret = 0.0;
            var bills =  service.getBills(-1, customerID, -1, -1);
            int nBills=0;
            var periods= new List<int>();
            foreach (var bill in bills)
            {
                if (bill.paymentDiffered || bill.paymentDocumentID > 0)
                    continue;
                nBills++;
                if(!periods.Contains(bill.periodID))
                {
                    periods.Add(bill.periodID);
                }
                var items = service.getCustomerBillItems(bill.id);
                if (items != null && items.Length > 0)
                    ret += items.Sum(x => x.price - x.settledFromDepositAmount);
            }
            if (periods.Count == 0)
                description = "No Unpaid Bills";
            else if (periods.Count == 1)
            {
                description = "Bills for period: " + service.GetBillPeriod(periods[0]).name;
            }
            else
                description = "Outstanding bills";
            return ret;
        }

        [Route("subsc/eps/api/getUnpaidBill")]
        [HttpGet]
        public ActionResult getUnpaidBill(string sessionID, String customer_code,String phone_no)
        {
            try
            {
                SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
                Subscriber customer = null;
                if (!String.IsNullOrEmpty(customer_code))
                {
                    customer = service.GetSubscriber(customer_code);
                    if (customer == null)
                        throw new InvalidOperationException("Invalid customer code");
                }
                else if (!String.IsNullOrEmpty(phone_no))
                {
                    int N;
                    var res=service.GetSubscriptions(phone_no, SubscriberSearchField.PhoneNo, false, -1, -1, -1, 0, -1, out N);
                    if(res.Length==0)
                        throw new InvalidOperationException("No customer found with phone no:"+phone_no);
                    if(res.Length>1)
                        throw new InvalidOperationException("Multiple customer found with phone no:" + phone_no);
                    customer = res[0].subscriber;
                }
                else
                    throw new InvalidOperationException("Customer code or phone number should be provided");
                String desc;
                var amount = TotalBills(service,customer.id,out desc);
                if (Accounting.Account.AmountGreater(amount,0))
                    return Json(new 
                    {
                        customerCode = customer.customerCode,
                        phoneNo = customer.phoneNo,
                        amount = Math.Round(amount,3).ToString(),
                        customerName=customer.name,
                        description=desc
                    });
                else
                {
                    return Json(new
                    {
                        customerCode=customer.customerCode,
                        phoneNo=customer.phoneNo,
                        amount = "0",
                        customerName = customer.name,
                        description = "No unpaid bill"
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }

        [Route("subsc/eps/api/payAllBills")]
        [HttpPost]
        public ActionResult payAllBills([FromQuery]string sessionID, [FromBody]PayAvialableBillsPar payment)
        {
            try
            {
                SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
                var customer = service.GetSubscriber(payment.costomerCode);
                if (customer == null)
                    throw new InvalidOperationException("Invalid customer code");
                return Json(new {tran_id= service.PayAvialableBills(customer.id, payment.amount, payment.remark) });
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message});
            }
        }


    }


    public class EPSAccountController : Controller
    {
        public class LoginPars
        {
            public string userName;
            public string passWord;
        }
        public class ChangePasswordPars
        {
            public string oldPassword;
            public string newPassword;
        }
        [Route("subsc/eps/api/login")]
        [HttpPost]
        public ActionResult login([FromBody] LoginPars pars)
        {
            try
            {
                string sessionID = ApplicationServer.CreateUserSession(pars.userName, pars.passWord, "webapi");
                return Json(new { sessionID = sessionID });
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }
        [Route("subsc/eps/api/logout")]
        [HttpPost]
        public ActionResult logout([FromQuery]string sessionID)
        {
            try
            {
                ApplicationServer.CloseSession(sessionID);
                return Json(new { });
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message});
            }
        }       
        [Route("subsc/eps/api/ChangePassword")]
        [HttpPost]
        public ActionResult changePassword([FromQuery]string sessionID, [FromBody]ChangePasswordPars pars)
        {
            try
            {
                UserSessionInfo perm = ApplicationServer.getSessionInfo(sessionID);
                if (perm == null)
                    throw new ServerUserMessage("Invalid user session ID:{0}", sessionID);
                if (ApplicationServer.SecurityBDE.GetUserPermmissions(perm.UserName, pars.oldPassword) == null)
                    throw new ServerUserMessage("Invalid user old password");
                ApplicationServer.SecurityBDE.ChangePassword(perm.UserName, pars.newPassword);
                return Json(new APIReturn() { error = null, res = null });
            }
            catch (Exception ex)
            {
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }
    }
    

    public class EPSSyncController : Controller
    {
        [Route("subsc/eps/api/startsync")]
        [HttpPost]
        public ActionResult Post([FromQuery]string sessionID)
        {
            try
            {
                SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
                service.syncEPS();
                return Json(new APIReturn() { error = null, res = null });
            }
            catch (Exception ex)
            {
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }
        [Route("subsc/eps/api/syncstatus")]
        [HttpGet]
        public ActionResult Get([FromQuery]string sessionID)
        {
            try
            {
                SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
                return Json(new APIReturn()
                {
                    error = null,
                    res = new
                    {
                        prog = service.epsGetProgress(),
                        running=service.epsIsSyncing(),
                    }
                });
            }
            catch (Exception ex)
            {
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }
    }
}