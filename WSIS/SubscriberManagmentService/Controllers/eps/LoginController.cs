﻿using System;
using System.Net;
using System.Net.Http;
using INTAPS.ClientServer;
using Microsoft.AspNetCore.Mvc;
using RazorEngine.Templating;

namespace INTAPS.SubscriberManagment.Service
{
    public class LoginViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class LoginResult
    {
        public bool Result { get; set; }
        public string SessionId { get; set; }
    }
    
    public class LoginPageController : Controller
    {
        [Route("subsc/eps/login")]
        [HttpGet]
        public ActionResult login(string sid=null)
        {

            if(!string.IsNullOrEmpty(sid))
            {
                try
                {
                    ApplicationServer.CloseSession(sid);

                }
                catch
                {
                    
                }
            }
            var model = new
            {
                //error="",
            };

            string template = System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["self_host_html_root"] + "\\subsc\\eps\\login.cshtml");
            string templateKey = template.GetHashCode().ToString();

            string html = RazorEngine.Engine.Razor.RunCompile(template, templateKey, null, model);
            return Content(html, "text/html");
        }

        [Route("subsc/eps/login")]
        [HttpPost]
        public ActionResult Post(LoginViewModel loginviewmodel)
        {
            string sessionID = ApplicationServer.CreateUserSession(loginviewmodel.UserName, loginviewmodel.Password, "web");

            if (string.IsNullOrEmpty(sessionID))
                return Json(new LoginResult {Result = false});
            return Json(new LoginResult { Result = true, SessionId = sessionID});
        }
        
    }
}