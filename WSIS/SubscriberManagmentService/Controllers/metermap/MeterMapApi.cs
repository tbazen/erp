﻿using System;
using System.Collections.Generic;
using INTAPS.ClientServer;
using RazorEngine.Templating;
using INTAPS.Payroll.Service;
using BIZNET.iERP.Server;
using INTAPS.Accounting.Service;
using INTAPS.WSIS.Job;
using Microsoft.AspNetCore.Mvc;
using INTAPS.ClientServer.Server;

namespace INTAPS.SubscriberManagment.Service
{

    public class MeterMapController : Controller
    {
        const double lat_max_zoom = 0.0005;
        const double lng_max_zoom = 0.0005;

        static Dictionary<string, MeterMap> loadedMaps = new Dictionary<string, MeterMap>();

        static Random r = new Random();
        static string addMap(MeterMap map)
        {

            lock (loadedMaps)
            {
                string ret = Guid.NewGuid().ToString();
                for (int i = 0; i < map.markers.Length; i++)
                {
                    int n1 = r.Next(map.markers.Length);
                    int n2 = r.Next(map.markers.Length);
                    if (n1 != n2)
                    {
                        MeterMarker temp = map.markers[n1];
                        map.markers[n1] = map.markers[n2];
                        map.markers[n2] = temp;
                    }
                }
                loadedMaps.Add(ret, map);
                return ret;
            }
        }
        static MeterMap getMap(string id)
        {
            lock (loadedMaps)
            {
                MeterMap ret;
                if (loadedMaps.TryGetValue(id, out ret))
                    return ret;
                return null;
            }
        }

        [Route("subsc/metermap/login")]
        [HttpGet]
        public ActionResult login(string sid = null)
        {
            try
            {

                if (!string.IsNullOrEmpty(sid))
                {
                    try
                    {
                        ApplicationServer.CloseSession(sid);

                    }
                    catch
                    {

                    }
                }
                var model = new
                {
                    //error="",
                };

                return View(System.Configuration.ConfigurationManager.AppSettings["self_host_html_root"] + "subsc/metermap/login.cshtml", model.ToExpando());
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException("Error processing request MeterMapApi.login", ex);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("subsc/metermap/login")]
        [HttpPost]
        public ActionResult Post([FromBody]LoginViewModel lv)
        {
            try
            {
                string sessionID = ApplicationServer.CreateUserSession(lv.UserName, lv.Password, "web");
                if (string.IsNullOrEmpty(sessionID))
                    return Json(new APIReturn() { error = "Login failed", res = null });
                return Json(new APIReturn() { error = null, res = sessionID });
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(null, ex);
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }

        [Route("subsc/metermap/meters")]
        [HttpGet]
        public ActionResult getMetersPage([FromQuery]string sid = null, [FromQuery]string maptype = "all", [FromQuery] int prd = -1, [FromQuery]int reader = -1, [FromQuery]int con = -1, [FromQuery]string date = null)
        {
            try
            {
                string sessionID = string.IsNullOrEmpty(sid) ? base.Request.Cookies["sid"] : sid;
                var model = new
                {
                    sid = sessionID,
                    maptype = maptype,
                    prdID = prd,
                    readerID = reader,
                    con = con,
                    date = string.IsNullOrEmpty(date) ? "" : date
                };

                return View(System.Configuration.ConfigurationManager.AppSettings["self_host_html_root"]+"/subsc/metermap/meters.cshtml", model.ToExpando());
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException("Error processing getMetersPage", ex);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("subsc/metermap/update_request")]
        [HttpGet]
        public ActionResult getUpdateRequestPage([FromQuery]string sid = null, [FromQuery]string maptype = "all", [FromQuery] int prd = -1, [FromQuery]int reader = -1, [FromQuery]int con = -1, [FromQuery]string date = null)
        {
            try
            {
                string sessionID = string.IsNullOrEmpty(sid) ? base.Request.Cookies["sid"] : sid;
                var model = new
                {
                    sid = sessionID,
                    maptype = maptype,
                    prdID = prd,
                    readerID = reader,
                    con = con,
                    date = string.IsNullOrEmpty(date) ? "" : date
                };

                return View(System.Configuration.ConfigurationManager.AppSettings["self_host_html_root"] + "subsc/metermap/update_request.cshtml", model.ToExpando());
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException("Error processing MeterMapAPI.getUpdateRequestPage", ex);
                return StatusCode(500, ex.Message);
            }

        }

        [Route("subsc/metermap/api/buildmetermap")]
        [HttpGet]
        public ActionResult buildMeterMap([FromQuery]string sessionID, [FromQuery]string maptype, [FromQuery] int prd = -1, [FromQuery]int reader = -1, [FromQuery]int con = -1, [FromQuery]string date = null)
        {
            try
            {
                SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
                MeterMap map = null;
                switch (maptype)
                {
                    case "unread":
                        map = service.getUnreadMetersMap(DateTime.Now.Ticks);
                        break;
                    case "allocation":
                    case "reading_path":
                        map = service.getMineOthers(prd, reader);
                        PayrollService payroll = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as PayrollService;
                        string allocationName = payroll.GetEmployee(reader).employeeName;
                        MeterMap path = null;
                        if (maptype.Equals("reading_path"))
                        {
                            int notMapped;

                            if (string.IsNullOrEmpty(date))
                                path = service.getReadingPath(prd, reader, out notMapped);
                            else
                            {
                                DateTime pathDate = DateTime.Parse(date);
                                path = service.getReadingPath(prd, reader, pathDate.Date, pathDate.Date.AddDays(1), out notMapped);
                            }
                            MapBounds pathbound = path.calculateBounds();
                        }
                        return Json(new APIReturn()
                        {
                            error = null,
                            res = new
                            {
                                id = addMap(map),
                                count = map.markers.Length,
                                bounds = path.calculateBounds(),
                                path = path,
                            },
                        });
                    case "locate":
                        string locatedWaterMeterStr = "Water meter of " + service.GetSubscription(con, DateTime.Now.Ticks).contractNo;
                        map = service.locateWaterMeter(con, DateTime.Now.Ticks);
                        string idMap = addMap(map);
                        int i = 0;
                        double l1 = 0, l2 = 0, g1 = 0, g2 = 0;
                        foreach (MeterMarker mrk in map.markers)
                        {
                            if (mrk.symbol == MeterMarkerSymbol.LocatedMeter)
                            {
                                l1 = mrk.lat - lat_max_zoom;
                                l2 = mrk.lat + lat_max_zoom;
                                g1 = mrk.lng - lng_max_zoom;
                                g2 = mrk.lng + lng_max_zoom;
                                break;
                            }
                            i++;
                        }
                        return Json(new APIReturn()
                        {
                            error = null,
                            res = new
                            {
                                id = addMap(map),
                                count = map.markers.Length,
                                bounds = l2 - l1 < 0.0001 || g2 - g1 < 0.0001 ? map.calculateBounds() : new MapBounds() { swLat = l1, neLat = l2, swLng = g1, neLng = g2 }
                            },
                        });
                    case "reading":
                        map = service.getReadingMap(prd);
                        return Json(new APIReturn()
                        {
                            error = null,
                            res = new
                            {
                                id = addMap(map),
                                count = map.markers.Length,
                                bounds = map.calculateBounds(),
                            },
                        });
                    case "locate_reading":
                        locatedWaterMeterStr = "Reading Location of Connection Code: " + service.GetSubscription(con, DateTime.Now.Ticks).contractNo;
                        map = service.singleMeterHistoricalReadingMap(con);
                        foreach (MeterMarker mrk in map.markers)
                        {
                            if (mrk.symbol == MeterMarkerSymbol.LocatedMeter)
                            {
                                l1 = mrk.lat - lat_max_zoom;
                                l2 = mrk.lat + lat_max_zoom;
                                g1 = mrk.lng - lng_max_zoom;
                                g2 = mrk.lng + lng_max_zoom;

                            }
                        }
                        return Json(new APIReturn()
                        {
                            error = null,
                            res = new
                            {
                                id = addMap(map),
                                count = map.markers.Length,
                                bounds = map.calculateBounds(),
                            },
                        });
                }
                map = service.getAllMetersMap(DateTime.Now.Ticks);
                return Json(new APIReturn()
                {
                    error = null,
                    res = new
                    {
                        id = addMap(map),
                        count = map.markers.Length,
                        bounds = map.calculateBounds(),
                    },
                });

            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(null, ex);
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }

        [Route("subsc/metermap/api/getMarkers")]
        [HttpGet]
        public ActionResult getMarkers(string id, int index, int count)
        {
            try
            {
                MeterMap map = getMap(id);
                if (map == null)
                    return Json(new APIReturn()
                    {
                        error = null,
                        res = null,
                    });
                List<MeterMarker> ret = new List<MeterMarker>();
                for (int i = 0; i < count && i + index < map.markers.Length; i++)
                {
                    ret.Add(map.markers[index + i]);
                }
                return Json(new APIReturn()
                {
                    error = null,
                    res = ret,
                });
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(null, ex);
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }
        [Route("subsc/metermap/api/setReading")]
        [HttpPost]
        public ActionResult setReading([FromQuery]string sessionID, [FromBody]ReadingSheetRow row)
        {
            try
            {
                SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
                int employeeIDForLogin = service.getMeterReadingEmployee().employeeID;
                if (row.actualReaderID != employeeIDForLogin)
                    throw new ServerUserMessage("The reader id sent by the client {0} device conflicts with the employee id {1} associated with the login id", row.actualReaderID, employeeIDForLogin);
                row.readerID = row.actualReaderID;

                service.setReadingSheetRow(row);
                return Json(new APIReturn() { error = null, res = null });
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(null, ex);
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }
        [Route("subsc/metermap/api/setReading2")]
        [HttpPost]
        public ActionResult setReading([FromQuery]string sessionID, [FromBody]ReadingSheetRecord row)
        {
            try
            {
                SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
                int employeeIDForLogin = service.getMeterReadingEmployee().employeeID;
                if (row.actualReaderID != employeeIDForLogin)
                    throw new ServerUserMessage("The reader id sent by the client {0} device conflicts with the employee id {1} associated with the login id", row.actualReaderID, employeeIDForLogin);
                row.readerID = row.actualReaderID;

                service.setReadingSheetRow2(row);
                return Json(new APIReturn() { error = null, res = null });
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(null, ex);
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }

        [Route("subsc/metermap/api/paybill")]
        [HttpPost]
        public ActionResult payBill(string sessionID, [FromQuery] int conID, [FromQuery] long date, [FromBody] ReadingSheetRecord.MRBill bill)
        {
            try
            {
                SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
                iERPTransactionService ierp = ApplicationServer.GetSessionObject(sessionID, typeof(iERPTransactionService)) as iERPTransactionService; ;
                AccountingService accounting = ApplicationServer.GetSessionObject(sessionID, typeof(AccountingService)) as AccountingService; ;
                CustomerPaymentReceipt receipt = new CustomerPaymentReceipt();

                receipt.notifyCustomer = true;
                receipt.paymentInstruments = new BIZNET.iERP.PaymentInstrumentItem[]{
                    new BIZNET.iERP.PaymentInstrumentItem((string)ierp.GetSystemParamters(new string[]{"cashInstrumentCode"})[0],bill.totalAmount()+(double)service.GetSystemParameters(new string[]{"commissionAmount_Utility_Share"})[0])};
                receipt.mobile = true;
                receipt.offline = true;
                receipt.receiptNumber = null;

                receipt.settledBills = new int[bill.items.Count];
                receipt.customer = service.GetSubscriber(service.GetSubscription(conID, ReadingSheetRecord.fromJavaTicks(date).Ticks).subscriberID);
                for (int i = 0; i < bill.items.Count; i++)
                {
                    receipt.settledBills[i] = bill.items[i].billID;
                }
                PaymentCenter pc = service.GetPaymentCenter(ApplicationServer.getSessionInfo(service.sessionID).UserName);
                if (pc == null)
                    throw new ServerUserMessage("Payment center not found for: " + ApplicationServer.getSessionInfo(service.sessionID).UserName);
                receipt.ShortDescription = "Bill settled from mobile: " + pc.centerName;
                receipt.assetAccountID = pc.casherAccountID;
                receipt.DocumentDate = ReadingSheetRecord.fromJavaTicks(date);
                int docID = service.postMobilePayment(receipt);
                return Json(new APIReturn() { error = null, res = docID });
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(null, ex);
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }
        public class ReaderSetting
        {
            public class WSISEnumerationItem
            {
                public int value;
                public String description;
            }
            public class WSISEnumerationItemStringValue
            {
                public String value;
                public String description;
            }
            public class CustomField
            {
                public const int FIELD_TYPE_STRING = 1;
                public String description;
                public String fieldName;
                public bool searchable = true;
                public int dataType = FIELD_TYPE_STRING;

                public CustomField(String name, String desc, bool searchable)
                {
                    this.description = desc;
                    this.fieldName = name;
                    this.searchable = searchable;
                    this.dataType = FIELD_TYPE_STRING;
                }
            }

            public int readerID;
            public int periodID;
            public String readerName;
            public String periodName;
            public String customField1 = "";
            public String customField2 = "";
            public String customField3 = "";
            public String customField4 = "";
            public double readingDistanceTolerance = 0;
            public CustomField[] customFields = new CustomField[0];
            public bool canCollect = false;
            public double maxCollection = 2000;
            public int syncedTo = -1;
            public void setCustomFields(String[] fields)
            {
                if (fields.Length > 0)
                    customField1 = fields[0];
                else if (fields.Length > 1)
                    customField2 = fields[1];
                else if (fields.Length > 2)
                    customField3 = fields[2];
                else if (fields.Length > 3)
                    customField4 = fields[3];
            }
            public string sheetHash;
            public double commissionAmount = 5;
            public WSISEnumerationItemStringValue[] meterTypes;
            public WSISEnumerationItem[] connectionTypes;
            public WSISEnumerationItem[] customerTypes;
            public WSISEnumerationItem[] connectionStatusTypes;
        }
        [Route("subsc/metermap/api/getReadingSetting")]
        [HttpGet]
        public ActionResult getReadingSetting(string sessionID)
        {
            try
            {
                SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
                MeterReaderEmployee emp = service.getMeterReadingEmployee();
                if (emp == null)
                    throw new ServerUserMessage("Current user is not reader");
                PayrollService payroll = ApplicationServer.GetSessionObject(sessionID, typeof(PayrollService)) as PayrollService;
                INTAPS.Payroll.Employee e = payroll.GetEmployee(emp.employeeID);
                if (e == null)
                    throw new ServerUserMessage("Error loading employee information for employee ID:" + emp.employeeID);
                ReaderSetting rs = new ReaderSetting();
                ReadingCycle cycle = service.getCurrentReadingCycle();
                if (cycle == null)
                    throw new ServerUserMessage("No current reading cycle");
                rs.periodID = cycle.periodID;
                rs.periodName = service.GetBillPeriod(rs.periodID).name;
                rs.readerID = e.id;
                rs.readerName = e.employeeName;
                rs.sheetHash = Convert.ToBase64String(cycle.hashCode);
                rs.setCustomFields(service.getReadingCustomFields());
                rs.readingDistanceTolerance = (double)(service.GetSystemParameters(new string[] { "readingDistanceTolerance" })[0]);
                rs.syncedTo = cycle.syncNo;
                rs.commissionAmount = (double)(service.GetSystemParameters(new string[] { "commissionAmount" })[0]);

                BIZNET.iERP.TransactionItems[] items = service.getMeterItems();
                rs.meterTypes = new ReaderSetting.WSISEnumerationItemStringValue[items.Length];
                int i;
                for (i = 0; i < rs.meterTypes.Length; i++)
                    rs.meterTypes[i] = new ReaderSetting.WSISEnumerationItemStringValue() { value = items[i].Code, description = items[i].Name };

                Array vals = Enum.GetValues(typeof(SubscriberType));
                rs.customerTypes = new ReaderSetting.WSISEnumerationItem[vals.Length];
                i = 0;
                foreach (SubscriberType st in vals)
                    rs.customerTypes[i++] = new ReaderSetting.WSISEnumerationItem() { value = (int)st, description = Subscriber.EnumToName(st) };

                vals = Enum.GetValues(typeof(ConnectionType));
                rs.connectionTypes = new ReaderSetting.WSISEnumerationItem[vals.Length];
                i = 0;
                foreach (ConnectionType st in vals)
                    rs.connectionTypes[i++] = new ReaderSetting.WSISEnumerationItem() { value = (int)st, description = Subscriber.EnumToName(st) };

                vals = Enum.GetValues(typeof(SubscriptionStatus));
                rs.connectionStatusTypes = new ReaderSetting.WSISEnumerationItem[vals.Length];
                i = 0;
                foreach (SubscriptionStatus st in vals)
                    rs.connectionStatusTypes[i++] = new ReaderSetting.WSISEnumerationItem() { value = (int)st, description = Subscriber.EnumToName(st) };

                return Json(new APIReturn() { error = null, res = rs });
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(null, ex);
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }


        [Route("subsc/metermap/api/getReadingSettingHashCode")]
        [HttpGet]
        public ActionResult getReadingSettingHashCode(string sessionID)
        {
            try
            {
                SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
                MeterReaderEmployee emp = service.getMeterReadingEmployee();
                if (emp == null)
                    throw new ServerUserMessage("Not a reader employee");
                PayrollService payroll = ApplicationServer.GetSessionObject(sessionID, typeof(PayrollService)) as PayrollService;
                INTAPS.Payroll.Employee e = payroll.GetEmployee(emp.employeeID);
                if (e == null)
                    throw new ServerUserMessage("Employee profile couldn't be loaded for employeeID:" + emp.employeeID);
                ReadingCycle cycle = service.getCurrentReadingCycle();
                if (cycle == null)
                    throw new ServerUserMessage("No current reading cycle");
                return Json(new APIReturn() { error = null, res = Convert.ToBase64String(cycle.hashCode) });
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(null, ex);
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }
        public class MobileChangeLog
        {
            public class Change
            {
                public int connectionID = -1;
                public bool billModified = false;
                public int[] paymentConfirmed = null;
                public long paymentTime = -1;
                public double utilityPayment = 0;
                public double systemPayment = 0;

            }
            public int from;
            public int upto;
            public List<Change> changes = new List<Change>();

        }
        [Route("subsc/metermap/api/getSyncN")]
        [HttpGet]
        public ActionResult getSyncN(string sessionID)
        {
            try
            {
                SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
                int syncNo = 0;
                if ("true".Equals(System.Configuration.ConfigurationManager.AppSettings["d2d_sync_bills"]))
                    syncNo = ApplicationServer.MsgRepo.getLastMessageNumber();
                return Json(new APIReturn() { error = null, res = syncNo });
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(null, ex);
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }
        [Route("subsc/metermap/api/getSyncData")]
        [HttpGet]
        public ActionResult getSyncData(string sessionID, int from, int upto)
        {
            try
            {
                SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
                PaymentCenter pc = service.GetPaymentCenter(ApplicationServer.getSessionInfo(service.sessionID).UserName);
                MobileChangeLog ret = new MobileChangeLog();
                ret.from = from;
                ret.upto = upto;
                foreach (OfflineMessage msg in ApplicationServer.MsgRepo.getMessageList(ret.from, ret.upto).data)
                {

                    if (msg.messageData is GenericDiff)
                    {
                        GenericDiff gd = (GenericDiff)msg.messageData;
                        

                        if (gd.diffData is CustomerPaymentReceipt)
                        {
                            CustomerPaymentReceipt receipt = gd.diffData as CustomerPaymentReceipt;
                            //if (receipt.assetAccountID == pc.casherAccountID)
                            {
                                ret.changes.Add(new MobileChangeLog.Change() { paymentConfirmed = receipt.settledBills });
                            }
                        }

                        if (gd.diffData is CustomerBillRecord)
                        {
                            CustomerBillRecord r = gd.diffData as CustomerBillRecord;
                            if (r.connectionID != -1)
                            {
                                ret.changes.Add(new MobileChangeLog.Change() { connectionID = r.connectionID, billModified = true });
                            }
                        }

                        if (pc != null)
                        {
                            if (gd.diffData is CashHandoverDocument)
                            {
                                CashHandoverDocument receipt = gd.diffData as CashHandoverDocument;
                                if (receipt.data.sourceCashAccountID == pc.summaryAccountID)
                                {
                                    ret.changes.Add(new MobileChangeLog.Change()
                                    {
                                        paymentTime = ReadingSheetRecord.toJavaTicks(DateTime.Now),
                                        utilityPayment = receipt.data.total
                                    });
                                }
                            }
                        }
                    }
                }
                return Json(new APIReturn() { error = null, res = ret });
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(null, ex);
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }


        [Route("subsc/metermap/api/getReadingSheetSize")]
        [HttpGet]
        public ActionResult getReadingSheetSize(string sessionID, int periodID)
        {
            try
            {
                SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
                return Json(new APIReturn() { error = null, res = service.readSheetSize(periodID) });
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(null, ex);
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }
        [Route("subsc/metermap/api/getReadingSheetRows")]
        [HttpGet]
        public ActionResult getReadingSheetRows(string sessionID, int periodID, int index, int n)
        {
            try
            {
                SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
                return Json(new APIReturn() { error = null, res = service.getReadingShowRows(periodID, index, n) });
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(null, ex);
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }

        [Route("subsc/metermap/api/getReadingSheetRows2")]
        [HttpGet]
        public ActionResult getReadingSheetRows2(string sessionID, int periodID, int index, int n)
        {
            try
            {
                SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
                return Json(new APIReturn() { error = null, res = service.getReadingShowRows2(periodID, index, n) });
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(null, ex);
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }
        [Route("subsc/metermap/api/save_data")]
        [HttpPost]
        public ActionResult saveData(string sessionID, [FromQuery] int conID, [FromQuery] int data_class,[FromBody] DataUpdate update)
        {
            try
            {
                SubscriberManagmentService service = ApplicationServer.GetSessionObject(sessionID, typeof(SubscriberManagmentService)) as SubscriberManagmentService;
                service.savedDataUpdateRequest(conID, data_class, update);
                return Json(new APIReturn() { error = null, res = null });
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(null, ex);
                return Json(new APIReturn() { error = ex.Message, res = null });
            }
        }

    }
}
