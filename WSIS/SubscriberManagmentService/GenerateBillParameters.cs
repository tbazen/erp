
    using System;
    using System.Runtime.InteropServices;
namespace INTAPS.SubscriberManagment.Service
{

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct GenerateBillParameters
    {
        public bool generatePenality;
        public bool generateCreditBill;
        public DateTime date;
        public int periodID;
        public int[] kebele;
        public int[] subscription;
    }
}

