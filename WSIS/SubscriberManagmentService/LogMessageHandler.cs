using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using INTAPS.ClientServer;

namespace INTAPS.SubscriberManagment.Service
{
    public class LogMessageHandler : DelegatingHandler { 
        protected override async Task<HttpResponseMessage> SendAsync ( HttpRequestMessage request, CancellationToken cancellationToken ) { 
            ApplicationServer.EventLoger.Log(EventLogType.ServerActions, string.Format("Request {0}", request));
            HttpResponseMessage response = await base.SendAsync(request, cancellationToken); 
            if (!response.IsSuccessStatusCode) { 
                // Log the error. 
                ApplicationServer.EventLoger.Log(EventLogType.ServerActions, "Error: " + response.StatusCode);
            } 
            return response; 
        } 
    }
}