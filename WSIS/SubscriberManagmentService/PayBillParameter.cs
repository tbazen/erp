
    using INTAPS.ClientServer;
    using System;
    using System.Runtime.InteropServices;
namespace INTAPS.SubscriberManagment.Service
{

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct PayBillParameter
    {
        public SignedData<SalePointData> paidBills;
    }
}

