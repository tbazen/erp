﻿using INTAPS.ClientServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using INTAPS.Accounting;
using BIZNET.iERP.Server;
using Microsoft.AspNetCore.Mvc;
using INTAPS.Accounting.Service;

namespace INTAPS.Payroll.Service.api
{
    public class PayrollAPIController : Controller
    {


        [Route("payroll/login")]
        [HttpGet]
        public String login(String source, String username, String password)
        {
            //ApplicationServer.SecurityBDE.GetUIDForName
            return ApplicationServer.CreateUserSession(username, password, source);
        }

        [Route("payroll/UserExists")]
        [HttpGet]
        public bool UserExists(string username)
        {
            try
            {
                var uid = ApplicationServer.SecurityBDE.GetUIDForName(username);
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        [Route("payroll/login2")]
        [HttpGet]
        public ActionResult login2(string source, string username, string password)
        {

                var sessionID = ApplicationServer.CreateUserSession(username, password, source);
                var userID = ApplicationServer.SecurityBDE.GetUIDForName(username);
                return Json(
                    new
                    {
                        sessionID = sessionID,
                        userID = userID
                    });

            
        }

        [Route("payroll/GetEmployeeWithLoginName/{sessionID}")]
        [HttpGet]
        public ActionResult GetEmployeeWithLoginName(string sessionID, string loginName)
        {
            var service = getPayrollService(sessionID);
            var emp = service.GetEmployeeByLoginName(loginName);
            if(emp == null)
            {
                return Json("");
            }
            return (Json(emp));
        }

        [Route("payroll/isAlive")]
        [HttpGet]
        public bool isAlive(string sessionID = "")
        {
           
            if (String.IsNullOrEmpty(sessionID) || String.IsNullOrWhiteSpace(sessionID)) return false;
            return ApplicationServer.isAlive(sessionID);
        }

        [Route("payroll/getAllUsers")]
        [HttpGet]
        public ActionResult getAllUsers()
        { 
            
            var users = ApplicationServer.SecurityBDE.getAllUsers().ToList();
            var result = (from b in users select new { userName = b.userName, id = b.id, parentId = b.parentId }).ToList();
            return Json(result);
        }

        [Route("payroll/closeSession")]
        [HttpGet]
        public void close(string sessionID)
        {
            ApplicationServer.CloseSession(sessionID);
        }

        PayrollService getPayrollService(String sessionID)
        {
            //return(IAccountingService)ApplicationServer.GetSessionObject(SessionID, "AccountingService");
            //      return (iERPService)

            return (PayrollService)ApplicationServer.GetSessionObject(sessionID, typeof(PayrollService));
        }

        AccountingService getAccountingService(String sessionID)
        {
            return (AccountingService)ApplicationServer.GetSessionObject(sessionID,typeof(AccountingService));
        }

        iERPTransactionService getiERPService(String sessionID)
        {
            return (iERPTransactionService)ApplicationServer.GetSessionObject(sessionID, typeof(iERPTransactionService));
        }

        [Route("payroll/get_org_units")]
        [HttpGet]
        public OrgUnit[] getOrgUnits(String sessionID, int parentID)
        {
            //   getParyrollService(sessionID).
            //  getPayrollService(sessionID).
            return getPayrollService(sessionID).GetOrgUnits(parentID);
        }


        [Route("payroll/get_employees")]
        [HttpGet]
        public Employee[] getEmployees(String sessionID, int orgUnitID)
        {
            return getPayrollService(sessionID).GetEmployees(orgUnitID, false);
        }

        [Route("payroll/add_employee")]
        [HttpGet]
        public void EnrollEmployee(String sessionID, int id)
        {
            getiERPService(sessionID).EnrollEmployee(id, DateTime.Now);
        }

        [Route("payroll/payroll_by_employee")]
        [HttpGet]
        public PayrollSetDocument getPayroll(String sessionID, int EmpID, int periodID)
        {
            // PayrollSetDocument
            // PayrollDocument
            //return getPayrollService(sessionID).getP
            
            return getPayrollService(sessionID).getPayrollSetByEmployee(periodID, EmpID);
        }

        [Route("payroll/payroll_by_employee1")]
        [HttpGet]
        public PayrollDocument getPayroll1(String sessionID, int EmpID, int periodID)
        {
            return getPayrollService(sessionID).GetPayroll(EmpID, periodID); ;
            // PayrollSetDocument
            // PayrollDocument
            //return getPayrollService(sessionID).getP
            // return getPayrollService(sessionID).GetPayroll(EmpID, periodID);
            //return getPayrollService(sessionID).getPayrollSetByEmployee(periodID, EmpID);
        }

        [Route("payroll/getpayroll")]
        [HttpGet]
        public PayrollSetDocument[] getPayrolls(string sessionID, int EmpID, int periodID)
        {
            PayrollSetDocument[] doc = getPayrollService(sessionID).getPayrollSetsOfAPeriod(periodID);

            return getPayrollService(sessionID).getPayrollSetsOfAPeriod(periodID);
        }

        //[Route("payroll/get_all_employees")]
        //[HttpGet]
        //public Employee[] getEmployees(string sessionID,int OrgID)
        //{
        //    List<Employee> Employees = new List<Employee>();
        //    List < OrgUnit > = getPayrollService(sessionID).GetOrgUnits();
        //    Employee [] e = getPayrollService(sessionID).GetEmployees(OrgID, true).ToList();
        //}


        [Route("payroll/getOrgs")]
        [HttpGet]
        public List<OrgUnit> getOrgs(string sessionID)
        {
            List<OrgUnit> result = new List<OrgUnit>();
            List<OrgUnit> temp = new List<OrgUnit>();


            OrgUnit[] parents = getPayrollService(sessionID).GetOrgUnits(-1);
            foreach (OrgUnit item in parents)
            {
                result.Add(item);
                temp.Add(item);
            }
            while (temp.Count != 0)
            {
                List<OrgUnit> temp2 = new List<OrgUnit>();
                foreach (OrgUnit item in temp)
                {
                    OrgUnit[] p = getPayrollService(sessionID).GetOrgUnits(item.id);
                    temp2.AddRange(p);
                }
                temp.Clear();
                temp.AddRange(temp2);
                result.AddRange(temp2);
                temp2.Clear();

            }

            return result;
        }

        [Route("payroll/getAllEmployees")]
        [HttpGet]
        public Employee[] GetAllEmps(string sessionID)
        {
            List<OrgUnit> Units = getOrgs(sessionID);
            List<Employee> Emps = new List<Employee>();
            foreach (var item in Units)
            {
                Emps.AddRange(getEmployees(sessionID, item.id));
            }
            return Emps.ToArray();


        }

        [Route("payroll/getEmployees")]
        [HttpGet]
        public Employee[] getEmployees(string sessionID)
        {
            OrgUnit[] parents = getPayrollService(sessionID).GetOrgUnits(-1);
            List<Employee> e = new List<Employee>();
            foreach (var item in parents)
            {
                Employee[] emps = getPayrollService(sessionID).GetEmployees(item.id, true);
                e.AddRange(emps);
            }
            return e.ToArray();
        }

        [Route("payroll/getEmployeeInformation")]
        [HttpGet]
        public Employee getEmployeeInfo(string sessionID, int EmpID)
        {
            return getPayrollService(sessionID).GetEmployee(EmpID);
        }

        [Route("payroll/getEmployeePayment")]
        [HttpGet]
        public PayrollDocument getEmployeePayment(string sessionID, int EmpID, int periodID)
        {
            PayrollSetDocument docs = getPayrollService(sessionID).getPayrollSetByEmployee(periodID, EmpID);
            PayrollDocument d = docs.payrolls.Where(m => m.employee.id == EmpID).First();
            return d;
        }

        [Route("payroll/getEmployeePaymentOfYear")]
        [HttpGet]
        public List<PayrollDocument> getPayrollOfYear(string sessionID, int EmpID, int Year, bool Ethiopian)
        {
            PayrollPeriod[] periods = getPayrollService(sessionID).GetPayPeriods(Year, Ethiopian);
            List<PayrollDocument> docs = new List<PayrollDocument>();
            foreach (var item in periods)
            {
                PayrollSetDocument p = getPayrollService(sessionID).getPayrollSetByEmployee(item.id, EmpID);
                if (p != null)
                {
                    docs.Add(p.payrolls.Where(m => m.employee.id == EmpID).First());
                }

            }
            return docs;
        }

        [Route("payroll/periods")]
        [HttpGet]
        public PayrollPeriod[] getPeriods(String sessionID, int Year, bool Ethiopian)
        {
            return getPayrollService(sessionID).GetPayPeriods(Year, Ethiopian);
        }

        [Route("payroll/getPeriodID")]
        [HttpGet]
        public int getPeriodID(String sessionID, int Year, int Month, bool Ethiopian)
        {
            PayrollPeriod[] periods = getPayrollService(sessionID).GetPayPeriods(Year, Ethiopian);
            int x = periods.Where(m => m.fromDate == new DateTime(Year, Month, 1)).FirstOrDefault().id;
            return x;
        }

        [Route("payroll/getPayPeriod")]
        [HttpGet]
        public PayrollPeriod getCurrentPayPeriod(string sessionID, DateTime date)
        {
            PayrollPeriod period = getPayrollService(sessionID).GetPayPeriod(date);
            return period;
        }

        [Route("payroll/RegisterOrgUnit/{sessionID}")]
        [HttpPost]
        public string RegisterOrgUnit(string sessionID, OrgUnit o)
        {
            try
            {
                int id = getPayrollService(sessionID).SaveOrgUnit(o);
                return id.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [Route("payroll/RegisterEmployee/{sessionID}")]
        [HttpPost]
        public string RegisterEmployee(string sessionID, Employee emp)
        {
            try
            {
                emp.accounts = new int[] { };
                int id = getiERPService(sessionID).RegisterEmployee(emp, null, false, new int[0], -1, null, null, null, null);
                //  int id = getPayrollService(sessionID).SaveEmployeeData(emp);
                return id.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        [Route("payroll/getCostCenterID")]
        [HttpGet]
        public int getCostCenterID(string sessionID, int costCenterID)
        {
            try
            {
                var centers = getAccountingService(sessionID).GetLeafAccounts<CostCenter>(costCenterID);
                var id = centers[0].id;
                return id;
            }
            catch (Exception ex)
            {
                return 0;
                throw;

            }
        }

        [Route("payroll/getCostCenters")]
        [HttpGet]
        public CostCenter[] getCostCenters(string sessionID, int costCenterID)
        {
            try
            {
                var centers = getAccountingService(sessionID).GetLeafAccounts<CostCenter>(costCenterID);

                return centers;
            }
            catch (Exception ex)
            {

                throw ex;

            }
        }

        [Route("payroll/createPCDData/{sessionID}")]
        [HttpPost]
        public ActionResult CreatePCDData(string sessionID,PCDCreateModel Model)
        {
            try
            {
                var result = getPayrollService(sessionID).CreatePCData(Model.Data, (Data_RegularTime)Model.additional);
                return Ok(new
                {
                    status = "Success",
                    response = result,
                });
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    status = "failed",
                    error = ex.Message
                });
                
            }
            
            
        }



    }


    public class PCDCreateModel
    {
        public PayrollComponentData Data { get; set; }
        public Data_RegularTime additional { get; set; }
    }


}
