
    using System;
    using System.Runtime.InteropServices;
namespace INTAPS.SubscriberManagment.Service
{

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct PostBillParameters
    {
        public DateTime date;
        public Type billType;
        public int periodID;
        public int[] kebele;
        public Type[] specifiedType;
        public int[] specifiedSubscription;
        public int[] specifiedPeriodID;
    }
}

