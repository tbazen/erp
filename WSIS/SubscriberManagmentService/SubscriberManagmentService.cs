using INTAPS.ClientServer;
using INTAPS.Payroll;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.BDE;
using INTAPS.UI;
using System;
using System.Data;
using System.Runtime.InteropServices;
using System.Threading;
using System.Web.Http;
using INTAPS.Accounting.Service;
namespace INTAPS.SubscriberManagment.Service
{
    public partial class SubscriberManagmentService : SessionObjectBase<SubscriberManagmentBDE>
    {
        private SubscriberManagmentBDE bdeSubscriber;
        private ReadingEntryClerk clerk;
        private BatchJobResult[] m_error;
        private PaymentCenter paymentCenter;

        public SubscriberManagmentService(UserSessionData session)
            : base(session, (SubscriberManagmentBDE)ApplicationServer.GetBDE("Subscriber"))
        {
            this.paymentCenter = null;
            this.clerk = null;
            this.m_error = null;
            this.bdeSubscriber = base._bde;
            foreach (PaymentCenter center in _bde.GetAllPaymentCenters())
            {
                if (center.userName.ToUpper() == session.Permissions.UserName.ToUpper())
                {
                    this.paymentCenter = center;
                }
            }
            this.clerk = base._bde.BWFGetClerkByUserID(base.m_Session.Permissions.UserName);
        }
        void checkMobileReadingLicense()
        {
            //TLicense.TLicenseClass.checkLicense("MobiReadReader", _bde.GetAllMeterReaderEmployees(_bde.SysPars.readingPeriod).Length);
        }
        public int AddSubscription(Subscription subscription)
        {
            this.CheckSubscriberDataPermission();
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteObjectAudit("AddSubscription Attempt", subscription.contractNo, subscription, -1);
                try
                {
                    this.bdeSubscriber.WriterHelper.BeginTransaction();
                    int ret = this.bdeSubscriber.AddSubscription(AID, subscription);
                    base.WriteExecuteAudit("AddSubscription Success", AID);
                    this.bdeSubscriber.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception exception)
                {
                    this.bdeSubscriber.WriterHelper.RollBackTransaction();
                    base.WriteAudit("AddSubscription Failure", exception.Message, exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    this.bdeSubscriber.CheckTransactionIntegrity();
                }
            }
        }
        /// <summary>
        /// Adds data form meter inventory survey
        /// Deppreciated
        /// </summary>
        /// <param name="data">The survey data</param>
        public void AddSurveyReading(MeterSurveyData data)
        {
            this.CheckReadingSheetEditPermssion();
            lock (base._bde.WriterHelper)
            {
                int aID = base.WriteAddAudit("Add Survey Data Attempt", data.contractNo + "_" + data.blockID, -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    base._bde.AddSurveyReading(aID, this.clerk, data);
                    base.WriteAddAudit("Add Survey Data  Success", data.contractNo + "_" + data.blockID, aID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Add Survey Data  Failure", data.contractNo + "_" + data.blockID, exception.Message + "\n" + exception.StackTrace, aID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        internal object PayAvialableBills(int id, double amount, object remark)
        {
            throw new NotImplementedException();
        }

        public void BWFAddMeterReading(BWFMeterReading m)
        {
            this.CheckBillingSpatialPermission();
            if (m.readingType == MeterReadingType.MeterReset)
            {
                this.CheckReadingResetPermission();
            }
            lock (base._bde.WriterHelper)
            {
                int aID = base.WriteAddAudit("Add Meter Reading Attempt", m.subscriptionID + "_" + m.readingBlockID, -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    base._bde.BWFAddMeterReading(aID, this.clerk, m, false);
                    base.WriteAddAudit("Add Meter Reading Success", m.subscriptionID + "_" + m.readingBlockID, aID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Add Meter Reading Failure", m.subscriptionID + "_" + m.readingBlockID, exception.Message + "\n" + exception.StackTrace, aID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public void BWFBatchCorrectReadings(int periodID, BWFReadingProblemType problem, SubscriberType[] types, double consumption, out System.Collections.Generic.List<Exception> errors)
        {
            this.CheckBillingAdminPermission();
            lock (base._bde.WriterHelper)
            {
                int aID = base.WriteAddAudit("Barch Reading Correction Attempt", "", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    base._bde.BWFBatchCorrectReadings(aID, this.clerk, periodID, problem, types, consumption, out errors);
                    base.WriteAddAudit("Barch Reading Correction Success", "", aID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Barch Reading Correction Failure", "", exception.Message + "\n" + exception.StackTrace, aID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public int BWFCreateMeterReading(BWFReadingBlock readingBlock, BWFMeterReading[] readings)
        {
            int num3;
            this.CheckReadingSheetEditPermssion();
            lock (base._bde.WriterHelper)
            {
                int aID = base.WriteAddAudit("Create Meter Reading Attempt", "", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    int num2 = base._bde.BWFCreateMeterReading(aID, this.clerk, readingBlock, readings);
                    base.WriteAddAudit("Create Meter Reading Success", num2.ToString(), aID);
                    base._bde.WriterHelper.CommitTransaction();
                    num3 = num2;
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Create Meter Reading Failure", "", exception.Message + "\n" + exception.StackTrace, aID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw new Exception("Error on creatting meter reading", exception);
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
            return num3;
        }

        public int BWFCreateReadingBlock(BWFReadingBlock block)
        {
            int num3;
            this.CheckBillingSpatialPermission();
            lock (base._bde.WriterHelper)
            {
                int aID = base.WriteAddAudit("Create Reading Block Attempt", "", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    int num2 = base._bde.BWFCreateReadingBlock(aID, this.clerk, block);
                    base.WriteAddAudit("Create Reading Block Success", num2.ToString(), aID);
                    base._bde.WriterHelper.CommitTransaction();
                    num3 = num2;
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Create Reading Block Failure", "", exception.Message + "\n" + exception.StackTrace, aID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
            return num3;
        }

        public void BWFDeleteMeterReading(int subscriptionID, int periodID)
        {
            this.CheckBillingSpatialPermission();
            lock (base._bde.WriterHelper)
            {
                int aID = base.WriteAddAudit("Delete Meter Reading Attempt", subscriptionID + "_" + periodID, -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    base._bde.BWFDeleteMeterReading(aID, this.clerk, subscriptionID, periodID);
                    base.WriteAddAudit("Delete Meter Reading Success", subscriptionID + "_" + periodID, aID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Delete Meter Reading Failure", subscriptionID + "_" + periodID, exception.Message + "\n" + exception.StackTrace, aID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw new Exception("Error delete meter reading", exception);
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public void BWFDeleteReadingBlock(int blockID)
        {
            this.CheckBillingSpatialPermission();
            lock (base._bde.WriterHelper)
            {
                int aID = base.WriteAddAudit("Delete Meter Reading Block Attempt", "", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    base._bde.BWFDeleteReadingBlock(aID, this.clerk, blockID);
                    base.WriteAddAudit("Delete Meter Reading Block  Success", blockID.ToString(), aID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Delete Meter Reading Block  Failure", blockID.ToString(), exception.Message + "\n" + exception.StackTrace, aID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw new Exception("Error on detete reading block", exception);
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public BWFMeterReading BWFGetMeterPreviousReading(int subcriptionID, int periodID)
        {
            return base._bde.BWFGetMeterPreviousReading(subcriptionID, periodID);
        }

        public BWFMeterReading BWFGetMeterReading(int subcriptionID, int blockID)
        {
            return base._bde.BWFGetMeterReading(subcriptionID, blockID);
        }

        public BWFMeterReading[] BWFGetMeterReadings(int kebele, int subscriptionID, int periodID, int employeeID, int index, int pageSize, out int NRecords)
        {
            return base._bde.BWFGetMeterReadings(kebele, subscriptionID, periodID, employeeID, index, pageSize, out NRecords);
        }

        public int BWFGetReadingBlockID(int subcriptionID, int periodID)
        {
            return base._bde.BWFGetReadingBlockID(subcriptionID, periodID);
        }

        public int BWFGetReadingPeriodID(int blockID)
        {
            return base._bde.BWFGetReadingPeriodID(blockID);
        }

        public void BWFImportReadingBlocks(int destPeriodID, int[] blockID)
        {
            this.CheckBillingSpatialPermission();
            lock (base._bde.WriterHelper)
            {
                int aID = base.WriteAddAudit("Import Meter Reading Blocks Attempt", "", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    base._bde.BWFImportReadingBlocks(aID, destPeriodID, blockID);
                    base.WriteAddAudit("Import Meter Reading Blocks Success", "", aID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Import Meter Reading Blocks Failure", "", exception.Message + "\n" + exception.StackTrace, aID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw new Exception("Error on importing reading blocks", exception);
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public void BWFImportReadings(int dest, int src, int[] subscriptionID)
        {
            this.CheckBillingSpatialPermission();
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("Import Readings Attempt", "", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    base._bde.BWFImportReadings(AID, dest, src, subscriptionID);
                    base.WriteAddAudit("Import Readings Attempt Success", "", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Import Readings Attempt Failure", "", exception.Message + "\n" + exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.CheckTransactionIntegrity();
                }
            }
        }

        public void BWFMergeBlock(int dest, int[] sources)
        {
            this.CheckBillingSpatialPermission();
            lock (base._bde.WriterHelper)
            {
                int aID = base.WriteAddAudit("Merge Block Attempt", "", -1);
                try
                {
                    this.bdeSubscriber.WriterHelper.BeginTransaction();
                    base._bde.BWFMergeBlock(aID, dest, sources);
                    base.WriteAddAudit("Merge Block Success", "", aID);
                    this.bdeSubscriber.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    this.bdeSubscriber.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Merge Block Failure", "", exception.Message + "\n" + exception.StackTrace, aID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw new Exception("Error on merging block", exception);
                }
                finally
                {
                    this.bdeSubscriber.CheckTransactionIntegrity();
                }
            }
        }

        public void BWFUpdateBlock(BWFReadingBlock block)
        {
            this.CheckBillingSpatialPermission();
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("Update Block Attempt", "", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    base._bde.BWFUpdateBlock(AID, block);
                    base.WriteAddAudit("Update Block Success", "", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Update Block Failure", "", exception.Message + "\n" + exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.CheckTransactionIntegrity();
                }
            }
        }

        public void BWFUpdateMeterReading(BWFMeterReading reading)
        {
            this.CheckReadingSheetEditPermssion();
            if (reading.readingType == MeterReadingType.MeterReset)
            {
                this.CheckReadingResetPermission();
            }
            lock (base._bde.WriterHelper)
            {
                int aID = base.WriteAddAudit("Update Meter Reading Attempt", reading.subscriptionID + "_" + reading.readingBlockID, -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    base._bde.BWFUpdateMeterReading(aID, this.clerk, reading);
                    base.WriteAddAudit("Update Meter Reading Success", reading.subscriptionID + "_" + reading.readingBlockID, aID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Update Meter Reading Failure", reading.subscriptionID + "_" + reading.readingBlockID, exception.Message + "\n" + exception.StackTrace, aID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public void ChangeSubcriberMeter(int subscriptionID, DateTime changeDate, MeterData meter)
        {
            this.CheckSubscriberDataPermission();
            lock (base._bde.WriterHelper)
            {
                int aID = base.WriteMethodCallAudit("ChangeSubcriberMeter Attempt", subscriptionID.ToString(), -1, new object[] { subscriptionID, changeDate, meter });
                try
                {
                    this.bdeSubscriber.WriterHelper.BeginTransaction();
                    this.bdeSubscriber.ChangeSubcriberMeter(aID, subscriptionID, changeDate, meter);
                    base.WriteExecuteAudit("ChangeSubcriberMeter Success", aID);
                    this.bdeSubscriber.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    this.bdeSubscriber.WriterHelper.RollBackTransaction();
                    base.WriteAudit("ChangeSubcriberMeter Failure", exception.Message, exception.StackTrace, aID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    this.bdeSubscriber.CheckTransactionIntegrity();
                }
            }
        }

        public void ChangeSubcriptionStatus(int subscriptionID, DateTime changeDate, SubscriptionStatus newStatus)
        {
            if (changeDate.Subtract(DateTime.Now).Days > 1)
            {
                this.CheckAdminPermission();
            }
            else
            {
                this.CheckSubscriberDataPermission();
            }
            lock (base._bde.WriterHelper)
            {
                int aID = base.WriteMethodCallAudit("ChangeSubcriptionStatus Attempt", subscriptionID.ToString(), -1, new object[] { subscriptionID, changeDate, newStatus });
                try
                {
                    this.bdeSubscriber.WriterHelper.BeginTransaction();
                    this.bdeSubscriber.ChangeSubcriptionStatus(aID, subscriptionID, changeDate, newStatus, false);
                    base.WriteExecuteAudit("ChangeSubcriptionStatus Success", aID);
                    this.bdeSubscriber.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    this.bdeSubscriber.WriterHelper.RollBackTransaction();
                    base.WriteAudit("ChangeSubcriptionStatuws Failure", exception.Message, exception.StackTrace, aID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    this.bdeSubscriber.CheckTransactionIntegrity();
                }
            }
        }

        public int ChangeSubcriptionType(int subscriptionID, DateTime changeDate, SubscriptionType newType)
        {
            int num3;
            this.CheckSubscriberDataPermission();
            lock (base._bde.WriterHelper)
            {
                int aID = base.WriteMethodCallAudit("ChangeSubcriptionType Attempt", subscriptionID.ToString(), -1, new object[] { subscriptionID, changeDate, newType });
                try
                {
                    this.bdeSubscriber.WriterHelper.BeginTransaction();
                    int num2 = this.bdeSubscriber.ChangeSubcriptionType(aID, subscriptionID, changeDate, newType);
                    base.WriteExecuteAudit("ChangeSubcriptionType Success", aID);
                    this.bdeSubscriber.WriterHelper.CommitTransaction();
                    num3 = num2;
                }
                catch (Exception exception)
                {
                    this.bdeSubscriber.WriterHelper.RollBackTransaction();
                    base.WriteAudit("ChangeSubcriptionType Failure", exception.Message, exception.StackTrace, aID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    this.bdeSubscriber.CheckTransactionIntegrity();
                }
            }
            return num3;
        }

        private void CheckAdjustmentsPermission()
        {
            this.CheckSecurity("Adjustments", "You are not authorized to make adjustments to posted bill.");
        }

        private void CheckAdminPermission()
        {
            if (base.m_Session.Permissions.UserName.ToUpper() != "ADMIN")
            {
                throw new Exception("Only system administrator can execute this operation");
            }
        }

        private void CheckBillingAdminPermission()
        {
            this.CheckSecurity("Admin", "You are not authorized to change billing system settings.");
        }
        private void CheckSMSSendPermission()
        {
            this.CheckSecurity("SmsSend", "You are not authorized to send sms.");
        }
        private void CheckBillingSpatialPermission()
        {
            this.CheckSecurity("Spatial", "You are not authorized to change block structure.");
        }

        private void CheckCollectPaymentPermission()
        {
            this.CheckSecurity("CollectPayment", "You are not authorized to collect bill settlements.");
        }

        private void CheckDeletePostedPermission()
        {
            this.CheckSecurity("DeletePosted", "You are not authorized to delete posted bills.");
        }

        private void CheckGenerateBillPermission()
        {
            this.CheckSecurity("GenerateBill", "You are not authorized to generate bill.");
        }

        private void CheckPostBillPermission()
        {
            this.CheckSecurity("PostBill", "You are not authorized to post bills.");
        }

        private void CheckReadingDataPermission()
        {
            this.CheckSecurity("ReadingData", "You are not authorized to edit reading data.");
        }

        private void CheckReadingSheetEditPermssion()
        {
            this.CheckSecurity("ReadingSheetEdit", "You are not authorized to edit reding sheets.");
        }
        private void CheckReadingResetPermission()
        {
            this.CheckSecurity("ReadingReset", "You are not authorized to reset reading.");
        }
        private void CheckSecurity(string key, string error)
        {
            if (!base.m_Session.Permissions.IsPermited("root/Subscriber/" + key))
            {
                throw new AccessDeniedException(error);
            }
        }

        private void CheckSubscriberDataPermission()
        {
            this.CheckSecurity("SubscriberData", "You are not authorized to edit subscriber data.");
        }
        private void checkDataSurveyPermssion()
        {
            this.CheckSecurity("SurveyData", "You are not authorized to upload survey data.");
        }
        public void CreateMeterReaderEmployee(MeterReaderEmployee employee)
        {
            this.CheckBillingAdminPermission();
            lock (base._bde.WriterHelper)
            {
                int pID = base.WriteObjectAudit("CreateMeterReaderEmployee Attempt", employee.employeeID.ToString(), employee, -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    this.bdeSubscriber.CreateMeterReaderEmployee(employee);
                    base.WriteExecuteAudit("CreateMeterReaderEmployee Success", pID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("CreateMeterReaderEmployee Failure", exception.Message, exception.StackTrace, pID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    this.bdeSubscriber.CheckTransactionIntegrity();
                }
            }
        }

        public void CreateReadingEntryClerk(ReadingEntryClerk employee)
        {
            this.CheckBillingAdminPermission();
            lock (base._bde.WriterHelper)
            {
                int pID = base.WriteAddAudit("Create Reading Entry Clerk Attempt", employee.employeeID.ToString(), -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    base._bde.CreateReadingEntryClerk(employee);
                    base.WriteAddAudit("Create Reading Entry Clerk Success", employee.employeeID.ToString(), pID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Create Reading Entry Clerk Failure", employee.employeeID.ToString(), exception.Message + "\n" + exception.StackTrace, pID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }


        public void DeleteEntryClerk(int employeeID)
        {
            this.CheckBillingAdminPermission();
            lock (base._bde.WriterHelper)
            {
                int pID = base.WriteAddAudit("Delete Reading Entry Clerk Attempt", employeeID.ToString(), -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    base._bde.DeleteEntryClerk(employeeID);
                    base.WriteAddAudit("Delete  Reading Entry Clerk Success", employeeID.ToString(), pID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Delete  Reading Entry Clerk Failure", employeeID.ToString(), exception.Message + "\n" + exception.StackTrace, pID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }



        public void DeleteMeterReaderEmployee(int employeeID, int periodID)
        {
            this.CheckBillingAdminPermission();
            MeterReaderEmployee meterReadingEmployee = this.bdeSubscriber.GetMeterReadingEmployee(employeeID);
            lock (base._bde.WriterHelper)
            {
                int pID = base.WriteObjectAudit("DeleteMeterReaderEmployee Attempt", meterReadingEmployee.employeeID.ToString(), meterReadingEmployee, -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    this.bdeSubscriber.DeleteMeterReaderEmployee(employeeID, periodID);
                    base.WriteExecuteAudit("DeleteMeterReaderEmployee Success", pID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeleteMeterReaderEmployee Failure", exception.Message, exception.StackTrace, pID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    this.bdeSubscriber.CheckTransactionIntegrity();
                }
            }
        }

        public void DeleteSubscriber(int subscriberID)
        {
            this.CheckSubscriberDataPermission();
            lock (base._bde.WriterHelper)
            {
                int aID = base.WriteAddAudit("DeleteSubscriber Attempt", subscriberID.ToString(), -1);
                try
                {
                    this.bdeSubscriber.WriterHelper.BeginTransaction();
                    this.bdeSubscriber.DeleteSubscriber(aID, subscriberID);
                    base.WriteExecuteAudit("DeleteSubscriber Success", aID);
                    this.bdeSubscriber.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    this.bdeSubscriber.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeleteSubscriber Failure", exception.Message, exception.StackTrace, aID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    this.bdeSubscriber.CheckTransactionIntegrity();
                }
            }
        }

        public void DeleteSubscription(int subscriptionID)
        {
            this.CheckSubscriberDataPermission();
            lock (base._bde.WriterHelper)
            {
                int aID = base.WriteAddAudit("DeleteSubscription Attempt", subscriptionID.ToString(), -1);
                try
                {
                    this.bdeSubscriber.WriterHelper.BeginTransaction();
                    this.bdeSubscriber.DeleteSubscription(aID, subscriptionID);
                    base.WriteExecuteAudit("DeleteSubscription Success", aID);
                    this.bdeSubscriber.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    this.bdeSubscriber.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeleteSubscription Failure", exception.Message, exception.StackTrace, aID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    this.bdeSubscriber.CheckTransactionIntegrity();
                }
            }
        }

        public Kebele[] GetAllKebeles()
        {
            Kebele[] allKebeles;
            try
            {
                allKebeles = this.bdeSubscriber.GetAllKebeles();
            }
            catch (Exception exception)
            {
                ApplicationServer.EventLoger.LogException(exception.Message, exception);
                throw;
            }
            return allKebeles;
        }

        public MeterReaderEmployee[] GetAllMeterReaderEmployees(int periodID)
        {
            MeterReaderEmployee[] allMeterReaderEmployees;
            try
            {
                allMeterReaderEmployees = this.bdeSubscriber.GetAllMeterReaderEmployees(periodID);
            }
            catch (Exception exception)
            {
                ApplicationServer.EventLoger.LogException(exception.Message, exception);
                throw;
            }
            return allMeterReaderEmployees;
        }

        public ReadingEntryClerk[] GetAllReadingEntryClerk()
        {
            return base._bde.GetAllReadingEntryClerk();
        }


        public BillPeriod GetBillPeriod(int periodID)
        {
            BillPeriod billPeriod;
            try
            {
                billPeriod = this.bdeSubscriber.GetBillPeriod(periodID);
            }
            catch (Exception exception)
            {
                ApplicationServer.EventLoger.LogException(exception.Message, exception);
                throw;
            }
            return billPeriod;
        }

        public BillPeriod[] GetBillPeriods(int Year, bool EthiopianYear)
        {
            BillPeriod[] billPeriods;
            try
            {
                billPeriods = this.bdeSubscriber.GetBillPeriods(Year, EthiopianYear);
            }
            catch (Exception exception)
            {
                ApplicationServer.EventLoger.LogException(exception.Message, exception);
                throw;
            }
            return billPeriods;
        }

        public BWFReadingBlock GetBlock(int blockID)
        {
            return base._bde.GetBlock(blockID);
        }

        public BWFReadingBlock[] GetBlock(int period, string name)
        {
            return base._bde.GetBlock(period, name);
        }

        public BWFReadingBlock[] GetBlocks(int periodID, int readerID)
        {
            return base._bde.GetBlocks(periodID, readerID);
        }

        public BatchJobStatus GetGeneratorStatus()
        {
            lock (this.bdeSubscriber)
            {
                BatchJobStatus status = new BatchJobStatus();
                status.newResults = this.m_error;
                status.progress = (this.bdeSubscriber.ProgressMax > 0) ? (((double)this.bdeSubscriber.Progress) / ((double)this.bdeSubscriber.ProgressMax)) : 0.0;
                status.stage = this.bdeSubscriber.ProgressStage;
                if ((status.stage == BatchJobStage.Idle) || (status.stage == BatchJobStage.Done))
                {
                    this.m_error = null;
                }
                return status;
            }
        }

        public BWFMeterReading[] GetMeterReadings(int subscriptionID)
        {
            return base._bde.BWFGetMeterReadings(subscriptionID);
        }

        public PrePaymentBill[] GetPrepaymentData(int subscriptionID)
        {
            return this.bdeSubscriber.GetPrepaymentData(subscriptionID);
        }

        public BillPeriod GetPreviousBillPeriod(int periodID)
        {
            return base._bde.GetPreviousBillPeriod(periodID);
        }

        public BWFMeterReading[] GetReadings(int blockID, int index, int pageSize, out int NRecords)
        {
            return base._bde.GetReadings(blockID, index, pageSize, out NRecords);
        }



        public string GetStatusHTML()
        {
            return null;
        }

        public Subscriber GetSubscriber(int id)
        {
            Subscriber subscriber;
            try
            {
                subscriber = this.bdeSubscriber.GetSubscriber(id);
            }
            catch (Exception exception)
            {
                ApplicationServer.EventLoger.LogException(exception.Message, exception);
                throw;
            }
            return subscriber;
        }



        public Subscription GetSubscription(int id, long version)
        {
            Subscription subscription;
            try
            {
                subscription = this.bdeSubscriber.GetSubscription(id, version);
            }
            catch (Exception exception)
            {
                ApplicationServer.EventLoger.LogException(exception.Message, exception);
                throw;
            }
            return subscription;
        }

        public Subscription GetSubscription(string contractNo, long version)
        {
            Subscription subscription;
            try
            {
                subscription = this.bdeSubscriber.GetSubscription(contractNo, version);
            }
            catch (Exception exception)
            {
                ApplicationServer.EventLoger.LogException(exception.Message, exception);
                throw;
            }
            return subscription;
        }
        public Subscription[] GetSubscriptionHistory(int subscriptionID)
        {
            Subscription[] subscriptionHistory;
            try
            {
                subscriptionHistory = this.bdeSubscriber.GetSubscriptionHistory(subscriptionID);
            }
            catch (Exception exception)
            {
                ApplicationServer.EventLoger.LogException(exception.Message, exception);
                throw;
            }
            return subscriptionHistory;
        }

        public Subscription[] GetSubscriptions(int subscriberID, long version)
        {
            Subscription[] subscriptions;
            try
            {
                subscriptions = this.bdeSubscriber.GetSubscriptions(subscriberID, version);
            }
            catch (Exception exception)
            {
                ApplicationServer.EventLoger.LogException(exception.Message, exception);
                throw;
            }
            return subscriptions;
        }

        public SubscriberSearchResult[] GetSubscriptions(string query, SubscriberSearchField fields, bool multipleVersion, int kebele, int zone, int dma, int index, int page, out int NRecords)
        {
            SubscriberSearchResult[] resultArray;
            try
            {
                resultArray = this.bdeSubscriber.GetSubscriptions(query, fields, multipleVersion, kebele, zone, dma, index, page, out NRecords);
            }
            catch (Exception exception)
            {
                ApplicationServer.EventLoger.LogException(exception.Message, exception);
                throw;
            }
            return resultArray;
        }

        public SubscriptionStatus GetSubscriptionStatusAtDate(int subscriptionID, DateTime dateTime)
        {
            return base._bde.GetSubscriptionStatusAtDate(subscriptionID, dateTime);
        }

        public MeterSurveyData GetSurveyData(int blockID, string contractNo)
        {
            return base._bde.GetSurveyData(blockID, contractNo);
        }

        public object[] GetSystemParameters(string[] fields)
        {
            object[] systemParameters;
            try
            {
                systemParameters = this.bdeSubscriber.GetSystemParameters(fields);
            }
            catch (Exception exception)
            {
                ApplicationServer.EventLoger.LogException(exception.Message, exception);
                throw;
            }
            return systemParameters;
        }





        public int IssueMeter(MeterIssuanceDocument doc)
        {
            int num3;
            this.CheckBillingAdminPermission();
            lock (base._bde.WriterHelper)
            {
                int pID = base.WriteAddAudit("Issure Meter Attempt", "", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    int num2 = this.bdeSubscriber.IssueMeter(doc);
                    base.WriteAddAudit("Issue Meter Success", "", pID);
                    base._bde.WriterHelper.CommitTransaction();
                    num3 = num2;
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Issue Meter Failure", "", exception.Message + "\n" + exception.StackTrace, pID);
                    ApplicationServer.EventLoger.LogException("Error on Issuing meter", exception);
                    throw;
                }
                finally
                {
                    this.bdeSubscriber.CheckTransactionIntegrity();
                }
            }
            return num3;
        }

        public int RegisterSubscriber(Subscriber subscriber)
        {
            int num3;
            this.CheckSubscriberDataPermission();
            lock (base._bde.WriterHelper)
            {
                int aID = base.WriteObjectAudit("RegisterSubscriber Attempt", subscriber.name, subscriber, -1);
                try
                {
                    this.bdeSubscriber.WriterHelper.BeginTransaction();
                    int num2 = this.bdeSubscriber.RegisterSubscriber(aID, subscriber);
                    base.WriteExecuteAudit("RegisterSubscriber Success", aID);
                    this.bdeSubscriber.WriterHelper.CommitTransaction();
                    num3 = num2;
                }
                catch (Exception exception)
                {
                    this.bdeSubscriber.WriterHelper.RollBackTransaction();
                    base.WriteAudit("RegisterSubscriber Failure", exception.Message, exception.StackTrace, aID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
            return num3;
        }

        public void ResetUpdateChain()
        {
            this.CheckAdminPermission();
            lock (this.bdeSubscriber.WriterHelper)
            {
                int pID = base.WriteMethodCallAudit("Reset Upate Chain Attempt", "", -1, new object[0]);
                try
                {
                    this.bdeSubscriber.WriterHelper.BeginTransaction();
                    this.bdeSubscriber.ResetUpdateChain();
                    base.WriteExecuteAudit("Reset Upate Chain Attempt Success", pID);
                    this.bdeSubscriber.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    this.bdeSubscriber.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Reset Upate Chain Attempt Failure", "", exception.Message + "\n" + exception.StackTrace, pID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    this.bdeSubscriber.CheckTransactionIntegrity();
                }
            }
        }




        public void SetSystemParameters(string[] fields, object[] vals)
        {
            if (!base.m_Session.Permissions.IsPermited("root/Subscriber/Configure"))
            {
                throw new AccessDeniedException();
            }
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("Set System Parameters Attempt", "", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    this.bdeSubscriber.SetSystemParameters(AID, fields, vals);
                    base.WriteAddAudit("Set System Parameters Success", "", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Set System Parameters Failure", "", exception.Message + "\n" + exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    this.bdeSubscriber.CheckTransactionIntegrity();
                }
            }
        }

        public void UpdateMeterReaderEmployee(MeterReaderEmployee employee)
        {
            this.CheckBillingAdminPermission();
            lock (base._bde.WriterHelper)
            {
                int pID = base.WriteObjectAudit("UpdateMeterReaderEmployee Attempt", employee.employeeID.ToString(), employee, -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    this.bdeSubscriber.UpdateMeterReaderEmployee(employee);
                    base.WriteExecuteAudit("UpdateMeterReaderEmployee Success", pID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("UpdateMeterReaderEmployee Failure", exception.Message, exception.StackTrace, pID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    this.bdeSubscriber.CheckTransactionIntegrity();
                }
            }
        }

        public void UpdateSubscriber(Subscriber subscriber, Subscription[] subsc)
        {
            this.CheckSubscriberDataPermission();
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteObjectAudit("UpdateSubscriber Attempt", subscriber.id.ToString(), subscriber, -1);
                try
                {
                    this.bdeSubscriber.WriterHelper.BeginTransaction();
                    this.bdeSubscriber.UpdateSubscriber(AID, subscriber, subsc);
                    base.WriteExecuteAudit("UpdateSubscriber Success", AID);
                    this.bdeSubscriber.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    this.bdeSubscriber.WriterHelper.RollBackTransaction();
                    base.WriteAudit("UpdateSubscriber Failure", exception.Message, exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public void UpdateSubscription(Subscription subscription)
        {
            this.CheckSubscriberDataPermission();
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("UpdateSubscription Attempt", subscription.id.ToString(), -1);
                try
                {
                    this.bdeSubscriber.WriterHelper.BeginTransaction();
                    this.bdeSubscriber.UpdateSubscription(AID, subscription);
                    base.WriteExecuteAudit("UpdateSubscription Success", AID);
                    this.bdeSubscriber.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    this.bdeSubscriber.WriterHelper.RollBackTransaction();
                    base.WriteAudit("UpdateSubscription Failure", exception.Message, exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    this.bdeSubscriber.CheckTransactionIntegrity();
                }
            }
        }

        public void UpdateSurveyReading(MeterSurveyData data)
        {
            this.CheckReadingSheetEditPermssion();
            lock (base._bde.WriterHelper)
            {
                int aID = base.WriteAddAudit("Update Survey Data Attempt", data.contractNo + "_" + data.blockID, -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    base._bde.UpdateSurveyReading(aID, this.clerk, data);
                    base.WriteAddAudit("Update Survey Data  Success", data.contractNo + "_" + data.blockID, aID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Update Survey Data  Failure", data.contractNo + "_" + data.blockID, exception.Message + "\n" + exception.StackTrace, aID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw new Exception("Error on Updating survey reading", exception);
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        private void VoidReceiptPermission()
        {
            this.CheckSecurity("VoidReceipt", "You are not authorized to void receipts.");
        }

        public INTAPS.SubscriberManagment.Subscription getSubscriptionByMeterNo(string meterNo)
        {
            return _bde.getSubscriptionByMeterNo(meterNo);
        }

        public INTAPS.SubscriberManagment.BillPeriod getPeriodForDate(System.DateTime date)
        {
            return _bde.getPeriodForDate(date);
        }

        public double getExageratedConsumption(int subscriptionID)
        {
            return _bde.getExageratedConsumption(subscriptionID);
        }


        private void checkSpecialBillSettlementPermission()
        {
            if (!m_Session.Permissions.UserName.Equals("GetachewM"))
                throw new AccessDeniedException("You are not allowed to periform this");
            if (DateTime.Now > new DateTime(2013, 12, 30))
                throw new AccessDeniedException("You are not allowed to periform this");
        }

        public int lbSaveLegacySet(INTAPS.SubscriberManagment.LegacyBillSet set, INTAPS.SubscriberManagment.LegacyBill[] bills)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("lbSaveLegacySet Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.lbSaveLegacySet(AID, set, bills);
                    base.WriteExecuteAudit("lbSaveLegacySet Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("lbSaveLegacySet Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.SubscriberManagment.LegacyBill[] lbGetLegacyBills(int setID)
        {
            return _bde.lbGetLegacyBills(setID);
        }

        public INTAPS.SubscriberManagment.LegacyBillSet[] lbGetSet(string userID, int pageIndex, int pageSize, out int N)
        {
            return _bde.lbGetSet(userID, pageIndex, pageSize, out N);
        }

        public INTAPS.SubscriberManagment.LegacyBillItemDefination[] lbGetAllItemDefinations()
        {
            return _bde.lbGetAllItemDefinations();
        }

        public INTAPS.SubscriberManagment.BWFMeterReading BWFGetMeterReadingByPeriod(int subcriptionID, int peridID)
        {
            return _bde.BWFGetMeterReadingByPeriod(subcriptionID, peridID);
        }

        public void lbDeleteSet(int setID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("lbDeleteSet Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.lbDeleteSet(AID, setID);
                    base.WriteExecuteAudit("lbDeleteSet Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("lbDeleteSet Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.SubscriberManagment.SubscriberSearchResult[] GetCustomers(string query, int kebele, int index, int pageSize, out int NRecords)
        {
            return _bde.GetCustomers(query, kebele, index, pageSize, out NRecords);
        }

        public PaymentCenter GetCurrentPaymentCenter()
        {
            return this.paymentCenter;
        }

        public INTAPS.SubscriberManagment.PaymentCenter[] GetAllPaymentCenters()
        {
            return _bde.GetAllPaymentCenters();
        }

        public void DeletePaymentCenter(int centerID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteAudit("DeletePaymentCenter Attempt", centerID.ToString(), "", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.DeletePaymentCenter(AID, centerID);
                    base.WriteExecuteAudit("DeletePaymentCenter Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeletePaymentCenter Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }

        }

        public void UpdatePaymentCenter(INTAPS.SubscriberManagment.PaymentCenter center)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("UpdatePaymentCenter Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.UpdatePaymentCenter(AID, center);
                    base.WriteExecuteAudit("UpdatePaymentCenter Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("UpdatePaymentCenter Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public int CreatePaymentCenter(INTAPS.SubscriberManagment.PaymentCenter center)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("CreatePaymentCenter Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.CreatePaymentCenter(AID, center);
                    base.WriteExecuteAudit("CreatePaymentCenter Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("CreatePaymentCenter Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }

        }
        public bool IsPaymentSerialBatchOverlapped(INTAPS.Accounting.SerialBatch batch)
        {
            return _bde.IsPaymentSerialBatchOverlapped(batch);
        }
        public INTAPS.SubscriberManagment.TranslatorEntry[] getTranslationTable()
        {
            return _bde.getTranslationTable();
        }

        public void saveTranslation(INTAPS.SubscriberManagment.TranslatorEntry[] newEntries)
        {
            _bde.saveTranslation(newEntries);
        }

        public INTAPS.SubscriberManagment.SubscriberManagmentUpdateNumber getUpdateNumber()
        {
            return _bde.getUpdateNumber();
        }

        public INTAPS.SubscriberManagment.BWFDescribedMeterReading[] BWFGetDescribedReadings(int blockID, int index, int pageSize, out int NRecords)
        {
            return _bde.BWFGetDescribedReadings(blockID, index, pageSize, out NRecords);
        }

        public int lbSaveLegacyVerificationSet(INTAPS.SubscriberManagment.LegacyBillSet set, INTAPS.SubscriberManagment.VerificationEntry[] bills)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("lbSaveLegacyVerificationSet Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.lbSaveLegacyVerificationSet(AID, clerk, set, bills);
                    base.WriteExecuteAudit("lbSaveLegacyVerificationSet Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("lbSaveLegacyVerificationSet Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.SubscriberManagment.LegacyBill[] lbFindBySubscriberAndPeriod(int conID, int periodID)
        {
            return _bde.lbFindBySubscriberAndPeriod(conID, periodID);
        }

        public INTAPS.SubscriberManagment.LegacyBill[] lbFindByBillNo(string billNo)
        {
            return _bde.lbFindByBillNo(billNo);
        }

        public bool lbIsAlreadyVerified(int ownSetID, INTAPS.SubscriberManagment.VerificationEntry entry)
        {
            return _bde.lbIsAlreadyVerified(ownSetID, entry);
        }

        public INTAPS.SubscriberManagment.LegacyBillSet lbGetVerifiedSetByCustomerAndPeriod(int customerID, int periodID)
        {
            return _bde.lbGetVerifiedSetByCustomerAndPeriod(customerID, periodID);
        }

        public INTAPS.SubscriberManagment.BWFMeterReading BWFGetMeterReadingByPeriodID(int subcriptionID, int periodID)
        {
            return _bde.BWFGetMeterReadingByPeriodID(subcriptionID, periodID);
        }

        public INTAPS.UI.BatchJobResult[] generatCustomerPeriodicBills(System.DateTime time, int customerID, int periodID)
        {
            CheckBillingAdminPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("generatCustomerPeriodicBills Attempt", -1);
                try
                {
                    INTAPS.UI.BatchJobResult[] ret = _bde.generatCustomerPeriodicBills(AID, time, customerID, periodID);
                    base.WriteExecuteAudit("generatCustomerPeriodicBills Success", AID);
                    return ret;
                }
                catch (Exception ex)
                {
                    base.WriteAudit("generatCustomerPeriodicBills Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.UI.BatchJobResult[] generateBillsByKebele(System.DateTime time, int periodID, int[] kebeles)
        {
            CheckBillingAdminPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("generateBillsByKebele Attempt", -1);
                try
                {
                    INTAPS.UI.BatchJobResult[] ret = _bde.generateBillsByKebele(AID, time, periodID, kebeles);
                    base.WriteExecuteAudit("generateBillsByKebele Success", AID);
                    return ret;
                }
                catch (Exception ex)
                {
                    base.WriteAudit("generateBillsByKebele Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        class BillGenerationState
        {
            public Thread billGenerationThread = null;
            public BatchJobResult[] billGenerationResult = null;
        }
        static BillGenerationState generatorSate = new BillGenerationState();
        public void startBillGenerationByKebele(System.DateTime time, int periodID, int[] kebeles)
        {
            CheckBillingAdminPermission();
            int AID;
            lock (_bde.WriterHelper)
            {
                AID = base.WriteExecuteAudit("startBillGenerationByKebele", -1);
            }
            try
            {
                lock (generatorSate)
                {
                    if (generatorSate.billGenerationThread != null)
                    {
                        lock (generatorSate.billGenerationThread)
                        {
                            if (generatorSate.billGenerationThread.IsAlive)
                                throw new ServerUserMessage("Bill is already being generated");
                        }
                    }

                    generatorSate.billGenerationThread = new Thread(
                        new ThreadStart(delegate ()
                        {
                            lock (_bde.WriterHelper)
                            {
                                //_bde.WriterHelper.BeginTransaction();
                                try
                                {
                                    var result = _bde.generateBillsByKebele(AID, time, periodID, kebeles);
                                    lock (generatorSate)
                                    {
                                        generatorSate.billGenerationResult = result;
                                    }
                                    //_bde.WriterHelper.CommitTransaction();
                                }
                                catch (Exception ex)
                                {
                                   //_bde.WriterHelper.RollBackTransaction();
                                    ApplicationServer.EventLoger.LogException("Error generating bill", ex);
                                }
                                finally
                                {
                                    _bde.CheckTransactionIntegrity();
                                }
                            }
                        }));
                    generatorSate.billGenerationThread.Start();
                }
                
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw;
            }
            
        }

        
        public BatchJobResult[] getBillGenerationResult()
        {
            lock (generatorSate)
            {
                if (generatorSate.billGenerationThread == null)
                    return null;
                return generatorSate.billGenerationResult;
            }
        }
        public bool IsBillGenerating()
        {
            lock (generatorSate)
            {
                if (generatorSate.billGenerationThread == null)
                    return false;
                return generatorSate.billGenerationThread.IsAlive;
            }
        }
        


        public void cancelBillGeneration()
        {
            _bde.cancelBillGeneration();
        }

        public void deleteCustomerBill(int billRecordID)
        {
            CheckBillingAdminPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteCustomerBill Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteCustomerBill(AID, billRecordID);
                    base.WriteExecuteAudit("deleteCustomerBill Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteCustomerBill Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.SubscriberManagment.CustomerBillRecord[] getBills(int mainTypeID, int customerID, int connectionID, int periodID)
        {
            return _bde.getBills(mainTypeID, customerID, connectionID, periodID);
        }

        public BatchJobResult[] deleteUnpaidPeriodicBillByKebele(int periodID, int[] kebeles)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteUnpaidPeriodicBillByKebele Attempt", -1);
                try
                {
                    BatchJobResult[] ret = _bde.deleteUnpaidPeriodicBillByKebele(AID, periodID, kebeles);
                    base.WriteExecuteAudit("deleteUnpaidPeriodicBillByKebele Success", AID);
                    return ret;
                }
                catch (Exception ex)
                {
                    base.WriteAudit("deleteUnpaidPeriodicBillByKebele Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void postBill(System.DateTime time, int billRecordID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("postBill Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.postBill(AID, time, billRecordID);
                    base.WriteExecuteAudit("postBill Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("postBill Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.UI.BatchJobResult[] postBills(System.DateTime time, int periodID, int[] billIDs)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("postBills Attempt", -1);
                try
                {
                    INTAPS.UI.BatchJobResult[] ret = _bde.postBills(AID, time, periodID, billIDs);
                    base.WriteExecuteAudit("postBills Success", AID);
                    return ret;
                }
                catch (Exception ex)
                {
                    base.WriteAudit("postBills Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.UI.BatchJobResult[] postBillsByKebele(System.DateTime time, int periodID, int[] kebeles)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("postBillsByKebele Attempt", -1);
                try
                {
                    INTAPS.UI.BatchJobResult[] ret = _bde.postBillsByKebele(AID, time, periodID, kebeles);
                    base.WriteExecuteAudit("postBillsByKebele Finished", AID);
                    return ret;
                }
                catch (Exception ex)
                {
                    base.WriteAudit("postBillsByKebele Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.SubscriberManagment.CustomerBillDocument[] getBillDocuments(int mainTypeID, int customerID, int connectionID, int periodID, bool excludePaid)
        {
            return _bde.getBillDocuments(mainTypeID, customerID, connectionID, periodID, excludePaid);
        }

        public INTAPS.SubscriberManagment.Subscriber GetSubscriber(string code)
        {
            return _bde.GetSubscriber(code);
        }

        public INTAPS.SubscriberManagment.MeterReaderEmployee GetMeterReaderEmployees(int periodID, int employeeID)
        {
            return _bde.GetMeterReaderEmployees(periodID, employeeID);
        }

        public INTAPS.SubscriberManagment.CustomerBillRecord getCustomerBillRecord(int billRecordID)
        {
            return _bde.getCustomerBillRecord(billRecordID);
        }

        public INTAPS.SubscriberManagment.PaymentCenter GetPaymentCenter(string userID)
        {
            return _bde.GetPaymentCenter(userID);
        }

        public INTAPS.SubscriberManagment.PaymentCenter GetPaymentCenterByCashAccount(int account)
        {
            return _bde.GetPaymentCenterByCashAccount(account);
        }

        public INTAPS.SubscriberManagment.PaymentCenter GetPaymentCenter(int centerID)
        {
            return _bde.GetPaymentCenter(centerID);
        }

        public bool isBillPaid(int invoiceNo)
        {
            return _bde.isBillPaid(invoiceNo);
        }

        public INTAPS.SubscriberManagment.BillItem[] getCustomerBillItems(int billRecordID)
        {
            return _bde.getCustomerBillItems(billRecordID);
        }

        public int getLastMessageNumber()
        {
            return _bde.getLastMessageNumber();
        }

        public int countMessages(int after)
        {
            return _bde.countMessages(after);
        }



        public INTAPS.ClientServer.MessageList getMessageList(int from, int to)
        {
            try
            {
                return _bde.getMessageList(from, to);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw;
            }
        }

        public int getFirstMessageNumber()
        {
            return _bde.getFirstMessageNumber();
        }

        public int postMobilePayment(CustomerPaymentReceipt receipt)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("postMobilePayment Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    receipt.DocumentDate = DateTime.Now;
                    int ret=_bde.postOfflineReceipt(AID, receipt);
                    base.WriteExecuteAudit("postMobilePayment Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("postMobilePayment Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void postOfflineMessages(INTAPS.SubscriberManagment.PaymentCenterMessageList msgList,bool assertSequence)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("postOfflineMessages Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.postOfflineMessages(AID, msgList, assertSequence);
                    base.WriteExecuteAudit("postOfflineMessages Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("postOfflineMessages Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void setSubscriptionCoordinate(int subcriptionID, long version, double x, double y)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("setSubscriptionCoordinate Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.setSubscriptionCoordinate(AID, subcriptionID, version, x, y);
                    base.WriteExecuteAudit("setSubscriptionCoordinate Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("setSubscriptionCoordinate Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.SubscriberManagment.MeterMap getMetersNearBy(int readerID, double lat0, double lng0, double radius, long version)
        {
            return _bde.getMetersNearBy(readerID, lat0, lng0, radius, version);
        }

        public INTAPS.SubscriberManagment.MeterMap getAllMetersMap(long version)
        {
            return _bde.getAllMetersMap(version);
        }
        public INTAPS.SubscriberManagment.MeterMap getUnreadMetersMap(long version)
        {
            return _bde.getUnreadMetersMap(version);
        }
        public INTAPS.SubscriberManagment.MeterMap getReadingMap(int periodID)
        {
            return _bde.getReadingMap(periodID);
        }

        public INTAPS.SubscriberManagment.MeterMap getMineOthers(int periodID, int readerID)
        {
            return _bde.getMineOthers(periodID, readerID);
        }

        public INTAPS.SubscriberManagment.MeterMap locateWaterMeter(int connectionID, long version)
        {
            return _bde.locateWaterMeter(connectionID, version);
        }

        public INTAPS.SubscriberManagment.MeterMap disconnectCandidate(int readingPeriodID)
        {
            return _bde.disconnectCandidate(readingPeriodID);
        }

        public INTAPS.SubscriberManagment.BWFMeterReading getAverageReading(int subscriptionID, int periodID, int nmonths)
        {
            return _bde.getAverageReading(subscriptionID, periodID, nmonths);
        }

        public INTAPS.SubscriberManagment.MeterReaderEmployee GetMeterReadingEmployee(string loginName)
        {
            return _bde.GetMeterReadingEmployee(loginName);
        }
        public INTAPS.SubscriberManagment.MeterReaderEmployee getMeterReadingEmployee()
        {
            return _bde.GetMeterReadingEmployee(m_Session.Permissions.UserName);
        }
        public INTAPS.SubscriberManagment.ReadingCycle getReadingCycle(int periodID)
        {
            return _bde.getReadingCycle(periodID);
        }

        public INTAPS.SubscriberManagment.ReadingCycle getCurrentReadingCycle()
        {
            return _bde.getCurrentReadingCycle();
        }

        public void openReadingCycle(int periodID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("openReadingCycle Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.openReadingCycle(AID, periodID);
                    base.WriteExecuteAudit("openReadingCycle Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("openReadingCycle Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void deleteReadingCycle(int periodID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteReadingCycle Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteReadingCycle(AID, periodID);
                    base.WriteExecuteAudit("deleteReadingCycle Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteReadingCycle Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.SubscriberManagment.ReadingSheetRow[] getReadingSheet(int periodID)
        {
            return _bde.getReadingSheet(periodID);
        }

        public void closeReadingCycle(int periodID)
        {
            CheckBillingAdminPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("closeReadingCycle Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.closeReadingCycle(AID, periodID);
                    base.WriteExecuteAudit("closeReadingCycle Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("closeReadingCycle Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.SubscriberManagment.ReadingSheetRow getReadingShowRow(int periodID, int rowIndex)
        {
            return _bde.getReadingShowRow(periodID, rowIndex);
        }
        public INTAPS.SubscriberManagment.ReadingSheetRow[] getReadingShowRows(int periodID, int rowIndex,int n)
        {
            return _bde.getReadingShowRows(periodID, rowIndex, n);
        }
        public int readSheetSize(int periodID)
        {
            return _bde.readSheetSize(periodID);
        }

        public void setReadingSheetRow(INTAPS.SubscriberManagment.ReadingSheetRow row)
        {
            checkMobileReadingLicense();

            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("setReadingSheetRow Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.setReadingSheetRow(AID, row);
                    base.WriteExecuteAudit("setReadingSheetRow Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("setReadingSheetRow Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void setReadingSheetRow2(ReadingSheetRecord row)
        {
            checkMobileReadingLicense();

            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("setReadingSheetRow Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.setReadingSheetRow2(AID, row);
                    base.WriteExecuteAudit("setReadingSheetRow Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("setReadingSheetRow Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public INTAPS.SubscriberManagment.ReadingPath getReadingPath(int periodID, int readerID, out int notMapped)
        {
            return _bde.getReadingPath(periodID, readerID, out notMapped);
        }

        public INTAPS.SubscriberManagment.CustomerPaymentReceipt[] searchCustomerReceipts(string query, INTAPS.SubscriberManagment.SubscriberSearchField field, int casherAccountID, int resultSize)
        {
            return _bde.searchCustomerReceipts(query, field, casherAccountID, resultSize);
        }

        public void generateOutstandingBills(int customerID)
        {
            CheckBillingAdminPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("generateOutstandingBills Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.generateOutstandingBills(AID, customerID);
                    base.WriteExecuteAudit("generateOutstandingBills Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("generateOutstandingBills Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.SubscriberManagment.BillingRuleSettingType[] getSettingTypes()
        {
            return _bde.getSettingTypes();
        }

        public void setBillingRuleSetting(System.Type t, object setting)
        {
            CheckBillingAdminPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("setBillingRuleSetting Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.setBillingRuleSetting(AID, t, setting);
                    base.WriteExecuteAudit("setBillingRuleSetting Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("setBillingRuleSetting Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }


        }

        public object getBillingRuleSetting(System.Type t)
        {
            return _bde.getBillingRuleSetting(t);
        }

        public INTAPS.SubscriberManagment.CustomerSubtype[] getCustomerSubtypes(INTAPS.SubscriberManagment.SubscriberType type)
        {
            return _bde.getCustomerSubtypes(type);
        }

        public INTAPS.SubscriberManagment.CustomerSubtype getCustomerSubtype(INTAPS.SubscriberManagment.SubscriberType type, int subTypeID)
        {
            return _bde.getCustomerSubtype(type, subTypeID);
        }

        public INTAPS.SubscriberManagment.PrePaymentBill PreviewPrepaidReceipt(System.DateTime date, int subscriptionID, double cubicMeter)
        {
            try
            {
                return _bde.PreviewPrepaidReceipt(date, subscriptionID, cubicMeter);
            }
            catch(Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw;
            }
        }

        public INTAPS.SubscriberManagment.CustomerPaymentReceipt GeneratePrepaidReceipt(System.DateTime date, int subscriptionID, double cubicMeter, BIZNET.iERP.PaymentInstrumentItem[] paymentInstruments, out PrePaymentBill bill)
        {
            assertCasherSession();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("GeneratePrepaidReceipt Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    INTAPS.SubscriberManagment.CustomerPaymentReceipt ret = _bde.GeneratePrepaidReceipt(AID, date, paymentCenter.id, subscriptionID, cubicMeter, paymentInstruments, out bill);
                    base.WriteExecuteAudit("GeneratePrepaidReceipt Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("GeneratePrepaidReceipt Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        private void assertCasherSession()
        {
            if (paymentCenter == null)
                throw new ServerUserMessage("You must login with casher account");
        }

        public void updateBillingCycle(INTAPS.SubscriberManagment.BillingCycle cycle)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("updateBillingCycle Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.updateBillingCycle(AID, cycle);
                    base.WriteExecuteAudit("updateBillingCycle Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("updateBillingCycle Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.SubscriberManagment.BillingCycle[] getAllBillingCycles()
        {
            return _bde.getAllBillingCycles();
        }

        public void createBillingCycle(INTAPS.SubscriberManagment.BillingCycle cycle)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("createBillingCycle Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.createBillingCycle(AID, cycle);
                    base.WriteExecuteAudit("createBillingCycle Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("createBillingCycle Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.SubscriberManagment.BillingCycle getCurrentBillingCycle()
        {
            return _bde.getCurrentBillingCycle();
        }

        public INTAPS.SubscriberManagment.BillingCycle getBillingCycle(int periodID)
        {
            return _bde.getBillingCycle(periodID);
        }

        public void deleteBillingCycle(int periodID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteBillingCycle Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteBillingCycle(AID, periodID);
                    base.WriteExecuteAudit("deleteBillingCycle Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteBillingCycle Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.SubscriberManagment.MeterMap singleMeterHistoricalReadingMap(int connectionID)
        {
            return _bde.singleMeterHistoricalReadingMap(connectionID);
        }

        public INTAPS.SubscriberManagment.ReadingPath getReadingPath(int periodID, int readerID, System.DateTime d1, System.DateTime d2, out int notMapped)
        {
            return _bde.getReadingPath(periodID, readerID, d1, d2, out notMapped);
        }

        public System.DateTime[] getReadingDates(int periodID, int readerID)
        {
            return _bde.getReadingDates(periodID, readerID);
        }

        public void analyzeReading(int periodID, bool updateConnectionLocations)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("analyzeReading Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.analyzeReading(AID, periodID, updateConnectionLocations);
                    base.WriteExecuteAudit("analyzeReading Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("analyzeReading Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.SubscriberManagment.CreditScheme getCreditScheme(int schemeID)
        {
            return _bde.getCreditScheme(schemeID);
        }

        public INTAPS.SubscriberManagment.CreditScheme[] getCustomerCreditSchemes(int customerID)
        {
            return _bde.getCustomerCreditSchemes(customerID);
        }

        public int addNoneBillCrediScheme(INTAPS.SubscriberManagment.CreditScheme scheme)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("addNoneBillCrediScheme Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.addNoneBillCreditScheme(AID, scheme);
                    base.WriteExecuteAudit("addNoneBillCrediScheme Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("addNoneBillCrediScheme Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void deleteCreditScheme(int schemeID, bool deleteBills)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteCreditScheme Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteCreditScheme(AID, schemeID, deleteBills);
                    base.WriteExecuteAudit("deleteCreditScheme Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteCreditScheme Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.SubscriberManagment.CreditScheme[] getCustomerCreditSchemes(int customerID, out double[] settlement)
        {
            return _bde.getCustomerCreditSchemes(customerID, out settlement);
        }

        public void transferCustomerData(int wsisPeriodID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("transfer Hawassa mobile customer data Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.transferCustomerData(AID, wsisPeriodID);
                    base.WriteExecuteAudit("transfer Hawassa mobile customer data Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("transfer Hawassa mobile customer data Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void UploadReading(int wsisPeriodID)
        {
            //Upload reading to Hawassa WSS system. No need to check data integrity since no data will be updated to Subscriber
            _bde.UploadReading(wsisPeriodID);
        }
        public void registerMobileReader(Employee emp, string password, int periodID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("Register Mobile Reader Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.registerMobileReader(AID, emp, password, periodID);
                    base.WriteExecuteAudit("Register Mobile Reader Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Register Mobile Reader Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void changePassword(string userName, string password)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("Change password Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.changePassword(AID, userName, password);
                    base.WriteExecuteAudit("Change password Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Change password Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public DataTable getReadingStatistics(int periodID)
        {
            return _bde.getReadingStatistics(periodID);
        }
        public int getReaderCount(int periodID, int readerID)
        {
            return _bde.getReaderCount(periodID, readerID);
        }
        public ReadingCycle getReadingCycle(int periodID, int status)
        {
            return _bde.getReadingCycle(periodID, status);
        }
        public BWFDescribedMeterReading[] BWFGetDescribedReadings(int blockID, int periodID, string contractNo, string name, int kebele, int customerType, int index, int pageSize, out int NRecords)
        {
            return _bde.BWFGetDescribedReadings(blockID, periodID, contractNo, name, kebele, customerType, index, pageSize, out NRecords);
        }
        public void transferSubscriptionsToReader(int[] subscriptions, int readerID, int periodID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("transfer subscriptions to reader Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.transferSubscriptionsToReader(AID, subscriptions, readerID, periodID);
                    base.WriteExecuteAudit("transfer subscriptions to reader Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("transfer subscriptions to reader Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public int registerProductionPoint(INTAPS.SubscriberManagment.ProductionPoint productionPoint)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("registerProductionPoint Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.registerProductionPoint(AID, productionPoint);
                    base.WriteExecuteAudit("registerProductionPoint Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("registerProductionPoint Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.SubscriberManagment.ProductionPoint[] getProductionPoint(string name, int kebele, int index, int type, int pageSize, out int NRecords)
        {
            return _bde.getProductionPoint(name, kebele, index, type, pageSize, out NRecords);
        }

        public INTAPS.SubscriberManagment.ProductionPoint getProductionPoint(int id)
        {
            return _bde.getProductionPoint(id);
        }

        public int setProductionPointReading(System.DateTime readingDate, double reading, MeterReadingType readingType, int productionPointID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("setProductionPointReading Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.setProductionPointReading(AID, readingDate, reading, readingType, productionPointID);
                    base.WriteExecuteAudit("setProductionPointReading Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("setProductionPointReading Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }


        public void updateProductionPointReading(ProductionPointReading reading, bool reset)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("updateProductionPointReading Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.updateProductionPointReading(AID, reading, reset);
                    base.WriteExecuteAudit("updateProductionPointReading Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("updateProductionPointReading Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public string htmlDocGetCustomerBillData(int customerID, int connectionID, out string headers)
        {
            return _bde.htmlDocGetCustomerBillData(customerID, connectionID, out headers);
        }
        public string htmlDocGetCustomerList(SubscriberSearchResult[] result, out string headers)
        {
            return _bde.htmlDocGetCustomerList(result, out headers);
        }
        public string htmlDocGetReadings(int kebele, int subscriptionID, int periodID, int employeeID, out string headers)
        {
            return _bde.htmlDocGetReadings(kebele, subscriptionID, periodID, employeeID, out headers);
        }

        public string[] getReadingCustomFields()
        {
            return _bde.getReadingCustomFields();
        }

        public string getSMSPreviewHTML(INTAPS.SubscriberManagment.CustomerSMSSendParameter send)
        {
            return _bde.getSMSPreviewHTML(send);
        }

        public string getSMSInterfaceStatus()
        {
            return _bde.getSMSInterfaceStatus();
        }

        public void sendSMS(INTAPS.SubscriberManagment.CustomerSMSSendParameter send)
        {
            //TLicense.TLicenseClass.checkLicense("SMS.Send");

            CheckSMSSendPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("sendSMS Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.sendSMS(AID, true,send);
                    base.WriteExecuteAudit("sendSMS Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("sendSMS Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.SubscriberManagment.BWFMeterReading[] BWFGetMeterReadings(INTAPS.SubscriberManagment.MeterReadingFilter filter, int index, int pageSize, out int NRecords)
        {
            return _bde.BWFGetMeterReadings(filter, index, pageSize, out NRecords);
        }

        public void BWFAcceptRejectReading(bool accept, int periodID, INTAPS.SubscriberManagment.MeterReadingFilter filter, int[] specific)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("BWFAcceptRejectReading Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.BWFAcceptRejectReading(AID, accept, this.clerk, periodID, filter, specific);
                    base.WriteExecuteAudit("BWFAcceptRejectReading Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("BWFAcceptRejectReading Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void changeSMSPassCode(string oldPassCode, string newPassCode)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("changeSMSPassCode Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.changeSMSPassCode(AID, oldPassCode, newPassCode);
                    base.WriteExecuteAudit("changeSMSPassCode Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("changeSMSPassCode Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.SubscriberManagment.SMSLicenseInformation getSMSLicense()
        {
            return _bde.getSMSLicense();
        }

        internal DistrictMeteringZone[] getAllDMAs()
        {
            return _bde.getAllDMAs();
        }

        internal PressureZone[] getAllPressureZones()
        {
            return _bde.getAllPressureZones();
        }

        internal DistrictMeteringZone getDMA(int id)
        {
            return _bde.getDMA(id);
        }

        internal PressureZone getPressureZone(int id)
        {
            return _bde.getPressureZone(id);
        }

        public INTAPS.SubscriberManagment.CustomerSMS[] getSMSLogByPhoneNo(int sendID, int receivedID, string phoneNo)
        {
            return _bde.getSMSLogByPhoneNo(sendID, receivedID, phoneNo);
        }

        internal Kebele getKebele(int kebeleID)
        {
            return _bde.GetKebele(kebeleID);
        }

        static SubscriberManagmentService()
        {
            //new System.Threading.Thread(delegate() {
            //                                           initializeSelfHostWebServer(); }).Start();
        }
        

         public void mergeCustomers(int sid1, int sid2)
        {
            int ret;
            this.CheckReadingSheetEditPermssion();
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("mergeCustomers Attempt", "", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    base._bde.mergeCustomers(AID, this.clerk, sid1, sid2);
                    base.WriteExecuteAudit("mergeCustomers Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("mergeCustomers Failure", "", exception.Message + "\n" + exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }



        internal EPSBill epsGetBill(int billID)
        {
            return _bde.epsGetBill(billID);
        }

        internal ReadingSheetRecord[] getReadingShowRows2(int periodID, int index, int n)
        {
            return _bde.getReadingShowRows2(base.m_Session.Permissions.UserName, periodID, index, n);
        }

        internal BIZNET.iERP.TransactionItems[] getMeterItems()
        {
            return _bde.getMeterItems();
        }

        internal void savedDataUpdateRequest(int conID, int data_class, DataUpdate update)
        {
            int ret;
            this.checkDataSurveyPermssion();
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("savedDataUpdateRequest Attempt", "", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();

                    _bde.savedDataUpdateRequest(AID,m_Session.Permissions.UserName,conID, data_class, update);
                    base.WriteExecuteAudit("savedDataUpdateRequest Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("savedDataUpdateRequest Failure", "", exception.Message + "\n" + exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        internal void deleteSystemFeeDeposit(int periodID, int readerID, DateTime returnDate)
        {
            this.CheckAdminPermission();
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("deleteSystemFeeDeposit Attempt", "", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    _bde.deleteSystemFeeDeposit(AID, periodID,readerID,returnDate);
                    base.WriteExecuteAudit("deleteSystemFeeDeposit Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteSystemFeeDeposit Failure", "", exception.Message + "\n" + exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        internal void updateSystemFeeDeposit(SystemFeeDeposit deposit)
        {
            this.CheckAdminPermission();
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("updateSystemFeeDeposit Attempt", "", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    _bde.updateSystemFeeDeposit(AID, deposit);
                    base.WriteExecuteAudit("updateSystemFeeDeposit Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("updateSystemFeeDeposit Failure", "", exception.Message + "\n" + exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        internal void addSystemFeeDeposit(SystemFeeDeposit deposit)
        {
            this.CheckAdminPermission();
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("addSystemFeeDeposit Attempt", "", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    _bde.addSystemFeeDeposit(AID, deposit);
                    base.WriteExecuteAudit("addSystemFeeDeposit Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("addSystemFeeDeposit Failure", "", exception.Message + "\n" + exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        internal SystemFeeDeposit getDeposit(int periodID, int readerID, DateTime day)
        {
            return _bde.getDeposit(periodID, readerID, day);
        }

        internal SystemFeeDeposit[] getDeposits(int periodID, int readerID, DateTime from, DateTime to)
        {
            return _bde.getDeposits(periodID, readerID, from, to);
        }

        internal double getSystemDepositFeeBalance(int readerID, DateTime date)
        {
            return _bde.getSystemDepositFeeBalance(readerID, date);
        }
    }
}

