using INTAPS.ClientServer;
using INTAPS.Payroll;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.BDE;
using INTAPS.UI;
using System;
using System.Data;
using System.Runtime.InteropServices;
using System.Threading;
using System.Web.Http;
using INTAPS.Accounting.Service;
using BIZNET.iERP.Server;
using BIZNET.iERP;
using System.Collections.Generic;
using System.Linq;

namespace INTAPS.SubscriberManagment.Service
{

    public partial class SubscriberManagmentService : SessionObjectBase<SubscriberManagmentBDE>
    {
        internal IEPSInterface getEPSInterface(int paymentCenterID)
        {
            return _bde.getEPSInterface(paymentCenterID);
        }
        bool EPSSynching = false;
        double epsProgress = 0;
        public int epsGetLastPaymentSeqNo(int pcid)
        {
            return _bde.epsGetLastPaymentSeqNo(pcid);
        }
        public double epsGetProgress()
        {
            return epsProgress;
        }
        public bool epsIsSyncing()
        {
            return EPSSynching;
        }
        public void syncEPS()
        {
            try
            {
                if (EPSSynching)
                    throw new ServerUserMessage("EPS sync is already running. Try again later.");
                if (this.paymentCenter == null)
                    throw new ServerUserMessage("Please login with payment center account.");
                try
                {
                    IEPSInterface eps = getEPSInterface(this.paymentCenter.id);
                    EPSSynching = true;
                    lock (eps)
                    {
                        epsProgress = 0;
                        int count = _bde.getSyncCount(this.paymentCenter.id);
                        if (count == 0)
                            return;
                        
                        Console.WriteLine();
                        for (int i = 0; i < count; i++)
                        {
                            lock (_bde.WriterHelper)
                            {
                                int AID = base.WriteAddAudit("EPS Sync Attempt", "", -1);
                                Console.CursorTop--;
                                Console.WriteLine("{0}/{1}", i + 1, count);
                                _bde.WriterHelper.BeginTransaction();
                                try
                                {
                                    _bde.epsSyncOne(AID, this.paymentCenter.id);
                                    _bde.WriterHelper.CommitTransaction();
                                }
                                catch (Exception ex)
                                {
                                    _bde.WriterHelper.RollBackTransaction();
                                    ApplicationServer.EventLoger.LogException(string.Format("Sync {0} failed", i), ex);
                                    throw;
                                }
                                epsProgress = (double)(i + 1) / (double)count;
                            }
                        }
                    }
                }
                finally
                {
                    epsProgress = 0;
                    EPSSynching = false;
                }
            }
            catch (Exception err)
            {
                ApplicationServer.EventLoger.LogException("Sync failed", err);
            }
        }
        public int epsRecordPayment(EPSPayment payment)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("recordEPSPayment Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    iERPTransactionService erpService = ApplicationServer.GetSessionObject(sessionID, typeof(iERPTransactionService)) as iERPTransactionService;
                    AccountingService accounting = INTAPS.ClientServer.ApplicationServer.GetSessionObject(sessionID, typeof(AccountingService)) as AccountingService;


                    string un = ApplicationServer.getSessionInfo(sessionID).UserName;
                    PaymentCenter paymentCenter = this.GetPaymentCenter(un);
                    if (paymentCenter == null)
                        throw new ServerUserMessage("{0} is not registered as a payment center operator.", un);


                    IEPSInterface inter = this.getEPSInterface(paymentCenter.id);
                    if (inter == null)
                        throw new ServerUserMessage("{0} is not registered as a payment center of an EPS.", un);

                    int lastSeqNo = _bde.epsGetLastPaymentSeqNo(paymentCenter.id);
                    int newSeqNo = _bde.epsLogPaymentID(AID, paymentCenter.id, payment.paymentID);

                    CustomerPaymentReceipt receipt = new CustomerPaymentReceipt();
                    receipt.customer = _bde.GetSubscriber(payment.customerID);
                    if (receipt.customer == null)
                        throw new ServerUserMessage("{0} is not a valid customer ID.", payment.customerID);

                    receipt.assetAccountID = paymentCenter.casherAccountID;
                    receipt.settledBills = payment.billIDs;
                    receipt.DocumentDate = DateTime.Parse(payment.paymentDate);
                    receipt.ShortDescription = "Payment from an EPS";

                    if (payment.bankAccountID > 0)
                    {
                        BankAccountInfo bank = erpService.GetBankAccount(payment.bankAccountID);
                        if (bank == null)
                            throw new ServerUserMessage("{0} is not a valid bank account ID.", payment.bankAccountID);
                        CustomerBankDepositDocument deposit = new CustomerBankDepositDocument();
                        deposit.bankAccountID = payment.bankAccountID;
                        if (string.IsNullOrEmpty(payment.bankReference))
                            throw new ServerUserMessage("Bank transaction reference is not valid");
                        deposit.reference = payment.bankReference;
                        deposit.ShortDescription = "Bank deposit through EP";
                        deposit.DocumentDate = receipt.DocumentDate;
                        deposit.amount = payment.amount;
                        receipt.paymentInstruments = new PaymentInstrumentItem[]{new PaymentInstrumentItem()
                    {
                        instrumentTypeItemCode=erpService.GetSystemParamters(new string[]{"bankDepositInstrumentCode"})[0] as string,
                        amount=payment.amount,
                        despositToBankAccountID=deposit.bankAccountID,
                        documentDate=deposit.DocumentDate,
                        documentReference=deposit.reference,
                    }
                    };
                    }
                    else
                        receipt.paymentInstruments = new PaymentInstrumentItem[]{new PaymentInstrumentItem()
                    {
                        instrumentTypeItemCode=erpService.GetSystemParamters(new string[]{"cashInstrumentCode"})[0] as string,
                        amount=payment.amount,
                    }
                    };
                    int ret = _bde.postOfflineMessages(AID, new PaymentCenterMessageList(

                        paymentCenter.id,
                        new MessageList(new OfflineMessage[] { new OfflineMessage() 
                    { 
                        previousMessageID=lastSeqNo,
                        delivered=false,
                        deliveryDate=DateTime.Now,
                        id=newSeqNo,
                        messageData=new PaymentReceivedMessage(){
                            eachAmount= new double[]{payment.amount},
                            paidAmount= payment.amount,
                            payment=receipt,
                            paymentCenterID=paymentCenter.id,  
                        }
                    } })
                    ), true)[0];
                    base.WriteExecuteAudit("recordEPSPayment Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("recordEPSPayment Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
            }
        }

        public int RecordUnknownPayment(int customerID,double paymentAmount,String description)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("RecordUnknownPayment Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {

                    BillItem i = new BillItem();
                    i.itemTypeID = 0;
                    i.incomeAccountID = -1;
                    i.description = description;
                    i.hasUnitPrice = false;
                    i.price = paymentAmount;
                    i.accounting = BillItemAccounting.Advance;
                    UnclassifiedPayment b = new UnclassifiedPayment();
                    b.customer = _bde.GetSubscriber(customerID);
                    b.ShortDescription = description;
                    b.billItems = new BillItem[] { i };
                    b.draft = false;
                    var billID=_bde.addBill(AID, b, b.itemsLessExemption.ToArray());

                    CustomerPaymentReceipt receipt = new CustomerPaymentReceipt();

                    receipt.notifyCustomer = true;
                    receipt.paymentInstruments = new BIZNET.iERP.PaymentInstrumentItem[]{
                    new BIZNET.iERP.PaymentInstrumentItem((string)bdeSubscriber.bERP.SysPars.cashInstrumentCode,paymentAmount)};

                    receipt.mobile = false;
                    receipt.offline = false;
                    receipt.receiptNumber = null;

                    receipt.settledBills = new int[] { billID };
                    receipt.customer = b.customer;
                    
                    PaymentCenter pc = bdeSubscriber.GetPaymentCenter(ApplicationServer.getSessionInfo(sessionID).UserName);
                    if (pc == null)
                        throw new ServerUserMessage("Payment center not found for: " + ApplicationServer.getSessionInfo(sessionID).UserName);
                    receipt.ShortDescription = description;
                    receipt.assetAccountID = pc.casherAccountID;
                    receipt.DocumentDate = DateTime.Now;
                    int docID = bdeSubscriber.Accounting.PostGenericDocument(AID, receipt);
                    base.WriteExecuteAudit("RecordUnknownPayment Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return docID;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("RecordUnknownPayment Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
            }
        }
        public int PayAvialableBills(int customerID, double paymentAmount, string paymentDescription)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("PayAvialableBills Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    CustomerPaymentReceipt receipt = new CustomerPaymentReceipt();
                    double billAmount = 0.0;
                    var bills = bdeSubscriber.getBills(-1, customerID, -1, -1);
                    var billIDs = new List<int>();
                    foreach (var bill in bills)
                    {
                        if (bill.paymentDiffered || bill.paymentDocumentID > 0)
                            continue;
                        var items = bdeSubscriber.getCustomerBillItems(bill.id);
                        if (items != null && items.Length > 0)
                            billAmount += items.Sum(x => x.price - x.settledFromDepositAmount);
                        billIDs.Add(bill.id);
                    }
                    //billAmount = Math.Ceiling(billAmount * 100) / 100;
                    if (INTAPS.Accounting.AccountBase.AmountEqual(billAmount, 0))
                    {
                        throw new InvalidOperationException($"There is no unpaid bill for the customer");
                    }
                    if (INTAPS.Accounting.AccountBase.AmountLess(paymentAmount,billAmount))
                        throw new InvalidOperationException($"Payment amount ({paymentAmount})  less than the bill amount ({billAmount})");

                    receipt.notifyCustomer = true;
                    receipt.paymentInstruments = new BIZNET.iERP.PaymentInstrumentItem[]{
                    new BIZNET.iERP.PaymentInstrumentItem((string)bdeSubscriber.bERP.SysPars.cashInstrumentCode,paymentAmount)};

                    receipt.mobile = false;
                    receipt.offline = false;
                    receipt.receiptNumber = null;

                    receipt.settledBills = new int[billIDs.Count];
                    receipt.customer = bdeSubscriber.GetSubscriber(customerID);
                    for (int i = 0; i < billIDs.Count; i++)
                    {
                        receipt.settledBills[i] = billIDs[i];
                    }
                    PaymentCenter pc = bdeSubscriber.GetPaymentCenter(ApplicationServer.getSessionInfo(sessionID).UserName);
                    if (pc == null)
                        throw new ServerUserMessage("Payment center not found for: " + ApplicationServer.getSessionInfo(sessionID).UserName);
                    receipt.ShortDescription = paymentDescription;
                    receipt.assetAccountID = pc.casherAccountID;
                    receipt.DocumentDate = DateTime.Now;
                    int docID = bdeSubscriber.Accounting.PostGenericDocument(AID, receipt);
                    base.WriteExecuteAudit("PayAvialableBills Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return docID;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("PayAvialableBills Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
            }
        }

    }
}

