using INTAPS.ClientServer;
using INTAPS.ClientServer.Server;
using INTAPS.Payroll;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
namespace INTAPS.SubscriberManagment.Service
{
    [Route("api/erp/[controller]/[action]")]
    [ApiController]
    public class SubscriberManagmentController : RESTServerControllerBase<SubscriberManagmentService>
    {
        #region AddSubscription
        public class AddSubscriptionPar
        {
            public string sessionID; public Subscription subscription;
        }

        [HttpPost]
        public ActionResult AddSubscription(AddSubscriptionPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).AddSubscription(par.subscription));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region AddSurveyReading
        public class AddSurveyReadingPar
        {
            public string sessionID; public MeterSurveyData data;
        }

        [HttpPost]
        public ActionResult AddSurveyReading(AddSurveyReadingPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).AddSurveyReading(par.data);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFAddMeterReading
        public class BWFAddMeterReadingPar
        {
            public string sessionID; public BWFMeterReading m;
        }

        [HttpPost]
        public ActionResult BWFAddMeterReading(BWFAddMeterReadingPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).BWFAddMeterReading(par.m);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFBatchCorrectReadings
        public class BWFBatchCorrectReadingsPar
        {
            public string sessionID; public int periodID; public BWFReadingProblemType problem; public SubscriberType[] types; public double consumption;
        }
        public class BWFBatchCorrectReadingsOut
        {
            public System.Collections.Generic.List<Exception> errors;

        }

        [HttpPost]
        public ActionResult BWFBatchCorrectReadings(BWFBatchCorrectReadingsPar par)
        {
            try
            {
                var _ret = new BWFBatchCorrectReadingsOut();
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).BWFBatchCorrectReadings(par.periodID, par.problem, par.types, par.consumption, out _ret.errors);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFCreateMeterReading
        public class BWFCreateMeterReadingPar
        {
            public string sessionID; public BWFReadingBlock readingBlock; public BWFMeterReading[] readings;
        }

        [HttpPost]
        public ActionResult BWFCreateMeterReading(BWFCreateMeterReadingPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).BWFCreateMeterReading(par.readingBlock, par.readings));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFCreateReadingBlock
        public class BWFCreateReadingBlockPar
        {
            public string sessionID; public BWFReadingBlock block;
        }

        [HttpPost]
        public ActionResult BWFCreateReadingBlock(BWFCreateReadingBlockPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).BWFCreateReadingBlock(par.block));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFDeleteMeterReading
        public class BWFDeleteMeterReadingPar
        {
            public string sessionID; public int subscriptionID; public int periodID;
        }

        [HttpPost]
        public ActionResult BWFDeleteMeterReading(BWFDeleteMeterReadingPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).BWFDeleteMeterReading(par.subscriptionID, par.periodID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFDeleteReadingBlock
        public class BWFDeleteReadingBlockPar
        {
            public string sessionID; public int ReadingSheetID;
        }

        [HttpPost]
        public ActionResult BWFDeleteReadingBlock(BWFDeleteReadingBlockPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).BWFDeleteReadingBlock(par.ReadingSheetID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFGetMeterPreviousReading
        public class BWFGetMeterPreviousReadingPar
        {
            public string sessionID; public int subcriptionID; public int periodID;
        }

        [HttpPost]
        public ActionResult BWFGetMeterPreviousReading(BWFGetMeterPreviousReadingPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).BWFGetMeterPreviousReading(par.subcriptionID, par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFGetMeterReading
        public class BWFGetMeterReadingPar
        {
            public string sessionID; public int subcriptionID; public int blockID;
        }

        [HttpPost]
        public ActionResult BWFGetMeterReading(BWFGetMeterReadingPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).BWFGetMeterReading(par.subcriptionID, par.blockID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFGetMeterReadings
        public class BWFGetMeterReadingsPar
        {
            public string sessionID; public int kebele; public int subscriptionID; public int periodID; public int employeeID; public int index; public int pageSize;
        }
        public class BWFGetMeterReadingsOut
        {
            public int NRecords;
            public BWFMeterReading[] _ret;
        }

        [HttpPost]
        public ActionResult BWFGetMeterReadings(BWFGetMeterReadingsPar par)
        {
            try
            {
                var _ret = new BWFGetMeterReadingsOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((SubscriberManagmentService)base.SessionObject).BWFGetMeterReadings(par.kebele, par.subscriptionID, par.periodID, par.employeeID, par.index, par.pageSize, out _ret.NRecords);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFGetReadingBlockID
        public class BWFGetReadingBlockIDPar
        {
            public string sessionID; public int subcriptionID; public int periodID;
        }

        [HttpPost]
        public ActionResult BWFGetReadingBlockID(BWFGetReadingBlockIDPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).BWFGetReadingBlockID(par.subcriptionID, par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFGetReadingPeriodID
        public class BWFGetReadingPeriodIDPar
        {
            public string sessionID; public int blockID;
        }

        [HttpPost]
        public ActionResult BWFGetReadingPeriodID(BWFGetReadingPeriodIDPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).BWFGetReadingPeriodID(par.blockID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFImportReadingBlocks
        public class BWFImportReadingBlocksPar
        {
            public string sessionID; public int destPeriodID; public int[] blockID;
        }

        [HttpPost]
        public ActionResult BWFImportReadingBlocks(BWFImportReadingBlocksPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).BWFImportReadingBlocks(par.destPeriodID, par.blockID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFImportReadings
        public class BWFImportReadingsPar
        {
            public string sessionID; public int dest; public int src; public int[] subscriptionID;
        }

        [HttpPost]
        public ActionResult BWFImportReadings(BWFImportReadingsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).BWFImportReadings(par.dest, par.src, par.subscriptionID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFMergeBlock
        public class BWFMergeBlockPar
        {
            public string sessionID; public int dest; public int[] sources;
        }

        [HttpPost]
        public ActionResult BWFMergeBlock(BWFMergeBlockPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).BWFMergeBlock(par.dest, par.sources);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFUpdateBlock
        public class BWFUpdateBlockPar
        {
            public string sessionID; public BWFReadingBlock block;
        }

        [HttpPost]
        public ActionResult BWFUpdateBlock(BWFUpdateBlockPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).BWFUpdateBlock(par.block);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFUpdateMeterReading
        public class BWFUpdateMeterReadingPar
        {
            public string sessionID; public BWFMeterReading reading;
        }

        [HttpPost]
        public ActionResult BWFUpdateMeterReading(BWFUpdateMeterReadingPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).BWFUpdateMeterReading(par.reading);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ChangeSubcriberMeter
        public class ChangeSubcriberMeterPar
        {
            public string sessionID; public int subscriptionID; public DateTime changeDate; public MeterData meterID;
        }

        [HttpPost]
        public ActionResult ChangeSubcriberMeter(ChangeSubcriberMeterPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).ChangeSubcriberMeter(par.subscriptionID, par.changeDate, par.meterID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ChangeSubcriptionStatus
        public class ChangeSubcriptionStatusPar
        {
            public string sessionID; public int subscriptionID; public DateTime changeDate; public SubscriptionStatus newStatus;
        }

        [HttpPost]
        public ActionResult ChangeSubcriptionStatus(ChangeSubcriptionStatusPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).ChangeSubcriptionStatus(par.subscriptionID, par.changeDate, par.newStatus);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ChangeSubcriptionType
        public class ChangeSubcriptionTypePar
        {
            public string sessionID; public int subscriptionID; public DateTime changeDate; public SubscriptionType newType;
        }

        [HttpPost]
        public ActionResult ChangeSubcriptionType(ChangeSubcriptionTypePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).ChangeSubcriptionType(par.subscriptionID, par.changeDate, par.newType));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CreateMeterReaderEmployee
        public class CreateMeterReaderEmployeePar
        {
            public string sessionID; public MeterReaderEmployee employee;
        }

        [HttpPost]
        public ActionResult CreateMeterReaderEmployee(CreateMeterReaderEmployeePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).CreateMeterReaderEmployee(par.employee);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CreateReadingEntryClerk
        public class CreateReadingEntryClerkPar
        {
            public string sessionID; public ReadingEntryClerk employee;
        }

        [HttpPost]
        public ActionResult CreateReadingEntryClerk(CreateReadingEntryClerkPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).CreateReadingEntryClerk(par.employee);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteEntryClerk
        public class DeleteEntryClerkPar
        {
            public string sessionID; public int employeeID;
        }

        [HttpPost]
        public ActionResult DeleteEntryClerk(DeleteEntryClerkPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).DeleteEntryClerk(par.employeeID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteMeterReaderEmployee
        public class DeleteMeterReaderEmployeePar
        {
            public string sessionID; public int employeeID; public int periodID;
        }

        [HttpPost]
        public ActionResult DeleteMeterReaderEmployee(DeleteMeterReaderEmployeePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).DeleteMeterReaderEmployee(par.employeeID, par.periodID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteSubscriber
        public class DeleteSubscriberPar
        {
            public string sessionID; public int subscriberID;
        }

        [HttpPost]
        public ActionResult DeleteSubscriber(DeleteSubscriberPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).DeleteSubscriber(par.subscriberID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteSubscription
        public class DeleteSubscriptionPar
        {
            public string sessionID; public int subscriptionID;
        }

        [HttpPost]
        public ActionResult DeleteSubscription(DeleteSubscriptionPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).DeleteSubscription(par.subscriptionID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllKebeles
        public class GetAllKebelesPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult GetAllKebeles(GetAllKebelesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetAllKebeles());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllMeterReaderEmployees
        public class GetAllMeterReaderEmployeesPar
        {
            public string sessionID; public int periodID;
        }

        [HttpPost]
        public ActionResult GetAllMeterReaderEmployees(GetAllMeterReaderEmployeesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetAllMeterReaderEmployees(par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllReadingEntryClerk
        public class GetAllReadingEntryClerkPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult GetAllReadingEntryClerk(GetAllReadingEntryClerkPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetAllReadingEntryClerk());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBillPeriod
        public class GetBillPeriodPar
        {
            public string sessionID; public int periodID;
        }

        [HttpPost]
        public ActionResult GetBillPeriod(GetBillPeriodPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetBillPeriod(par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBillPeriods
        public class GetBillPeriodsPar
        {
            public string sessionID; public int Year; public bool EthiopianYear;
        }

        [HttpPost]
        public ActionResult GetBillPeriods(GetBillPeriodsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetBillPeriods(par.Year, par.EthiopianYear));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBlock
        public class GetBlockPar
        {
            public string sessionID; public int blockID;
        }

        [HttpPost]
        public ActionResult GetBlock(GetBlockPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetBlock(par.blockID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBlock2
        public class GetBlock2Par
        {
            public string sessionID; public int period; public string name;
        }

        [HttpPost]
        public ActionResult GetBlock2(GetBlock2Par par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetBlock(par.period, par.name));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBlocks
        public class GetBlocksPar
        {
            public string sessionID; public int periodID; public int readerID;
        }

        [HttpPost]
        public ActionResult GetBlocks(GetBlocksPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetBlocks(par.periodID, par.readerID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetGeneratorStatus
        public class GetGeneratorStatusPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult GetGeneratorStatus(GetGeneratorStatusPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetGeneratorStatus());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetMeterReadings
        public class GetMeterReadingsPar
        {
            public string sessionID; public int subscriptionID;
        }

        [HttpPost]
        public ActionResult GetMeterReadings(GetMeterReadingsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetMeterReadings(par.subscriptionID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPrepaymentData
        public class GetPrepaymentDataPar
        {
            public string sessionID; public int subscriptionID;
        }

        [HttpPost]
        public ActionResult GetPrepaymentData(GetPrepaymentDataPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetPrepaymentData(par.subscriptionID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPreviousBillPeriod
        public class GetPreviousBillPeriodPar
        {
            public string sessionID; public int periodID;
        }

        [HttpPost]
        public ActionResult GetPreviousBillPeriod(GetPreviousBillPeriodPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetPreviousBillPeriod(par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetReadings
        public class GetReadingsPar
        {
            public string sessionID; public int blockID; public int index; public int pageSize;
        }
        public class GetReadingsOut
        {
            public int NRecords;
            public BWFMeterReading[] _ret;
        }

        [HttpPost]
        public ActionResult GetReadings(GetReadingsPar par)
        {
            try
            {
                var _ret = new GetReadingsOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((SubscriberManagmentService)base.SessionObject).GetReadings(par.blockID, par.index, par.pageSize, out _ret.NRecords);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }

        #endregion
        #region GetSubscriber2
        public class GetSubscriber2Par
        {
            public string sessionID; public int id;
        }

        [HttpPost]
        public ActionResult GetSubscriber2(GetSubscriber2Par par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetSubscriber(par.id));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSubscription
        public class GetSubscriptionPar
        {
            public string sessionID; public int id; public long version;
        }

        [HttpPost]
        public ActionResult GetSubscription(GetSubscriptionPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetSubscription(par.id, par.version));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion




        #region GetSubscriptionWeb
        public class GetSubscriptionWebPar
        {
            public string sessionID; public int id; public string date;
        }

        [HttpPost]
        public ActionResult GetSubscriptionWeb(GetSubscriptionPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                DateTime time = DateTime.Parse("");

                return Json(((SubscriberManagmentService)base.SessionObject).GetSubscription(par.id, time.Ticks));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion












        #region GetSubscription2
        public class GetSubscription2Par
        {
            public string sessionID; public string contractNo; public long version;
        }

        [HttpPost]
        public ActionResult GetSubscription2(GetSubscription2Par par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetSubscription(par.contractNo, par.version));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSubscriptionHistory
        public class GetSubscriptionHistoryPar
        {
            public string sessionID; public int subscriptionID;
        }

        [HttpPost]
        public ActionResult GetSubscriptionHistory(GetSubscriptionHistoryPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetSubscriptionHistory(par.subscriptionID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSubscriptions
        public class GetSubscriptionsPar
        {
            public string sessionID; public int subscriberID; public long version;
        }

        [HttpPost]
        public ActionResult GetSubscriptions(GetSubscriptionsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetSubscriptions(par.subscriberID, par.version));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSubscriptions2
        public class GetSubscriptions2Par
        {
            public string sessionID; public string query; public SubscriberSearchField fields; public bool multipleVersion; public int kebele; public int zone; public int dma; public int index; public int page;
        }
        public class GetSubscriptions2Out
        {
            public int NRecord;
            public SubscriberSearchResult[] _ret;
        }

        [HttpPost]
        public ActionResult GetSubscriptions2(GetSubscriptions2Par par)
        {
            try
            {
                var _ret = new GetSubscriptions2Out();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((SubscriberManagmentService)base.SessionObject).GetSubscriptions(par.query, par.fields, par.multipleVersion, par.kebele, par.zone, par.dma, par.index, par.page, out _ret.NRecord);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSubscriptionStatusAtDate
        public class GetSubscriptionStatusAtDatePar
        {
            public string sessionID; public int subscriptionID; public DateTime dateTime;
        }

        [HttpPost]
        public ActionResult GetSubscriptionStatusAtDate(GetSubscriptionStatusAtDatePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetSubscriptionStatusAtDate(par.subscriptionID, par.dateTime));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSurveyData
        public class GetSurveyDataPar
        {
            public string sessionID; public int blockID; public string contractNo;
        }

        [HttpPost]
        public ActionResult GetSurveyData(GetSurveyDataPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetSurveyData(par.blockID, par.contractNo));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSystemParameters
        public class GetSystemParametersPar
        {
            public string sessionID; public string[] names;
        }

        [HttpPost]
        public ActionResult GetSystemParameters(GetSystemParametersPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                var temp = new BinObject(((SubscriberManagmentService)base.SessionObject).GetSystemParameters(par.names));
                var tempDeserialized = temp.Deserialized();
                return Json(new BinObject(((SubscriberManagmentService)base.SessionObject).GetSystemParameters(par.names)));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }


        public class GetSystemParametersWebPar
        {
            public string sessionID; public string[] names;
        }

        [HttpPost]
        public ActionResult GetSystemParametersWeb(GetSystemParametersWebPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                var response = ((SubscriberManagmentService)base.SessionObject).GetSystemParameters(par.names);
                return Json(response);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }



        #endregion
        #region IssueMeter
        public class IssueMeterPar
        {
            public string sessionID; public MeterIssuanceDocument doc;
        }

        [HttpPost]
        public ActionResult IssueMeter(IssueMeterPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).IssueMeter(par.doc));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region RegisterSubscriber
        public class RegisterSubscriberPar
        {
            public string sessionID; public Subscriber subscriber;
        }

        [HttpPost]
        public ActionResult RegisterSubscriber(RegisterSubscriberPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).RegisterSubscriber(par.subscriber));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ResetUpdateChain
        public class ResetUpdateChainPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult ResetUpdateChain(ResetUpdateChainPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).ResetUpdateChain();

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SetSystemParameters
        public class SetSystemParametersPar
        {
            public string sessionID; public string[] fields; public BinObject vals;
        }

        [HttpPost]
        public ActionResult SetSystemParameters(SetSystemParametersPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).SetSystemParameters(par.fields, (object[])par.vals.Deserialized());

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateMeterReaderEmployee
        public class UpdateMeterReaderEmployeePar
        {
            public string sessionID; public MeterReaderEmployee employee;
        }

        [HttpPost]
        public ActionResult UpdateMeterReaderEmployee(UpdateMeterReaderEmployeePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).UpdateMeterReaderEmployee(par.employee);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateSubscriber
        public class UpdateSubscriberPar
        {
            public string sessionID; public Subscriber subscriber; public Subscription[] subsc;
        }

        [HttpPost]
        public ActionResult UpdateSubscriber(UpdateSubscriberPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).UpdateSubscriber(par.subscriber, par.subsc);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateSubscription
        public class UpdateSubscriptionPar
        {
            public string sessionID; public Subscription subscription;
        }

        [HttpPost]
        public ActionResult UpdateSubscription(UpdateSubscriptionPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).UpdateSubscription(par.subscription);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateSurveyReading
        public class UpdateSurveyReadingPar
        {
            public string sessionID; public MeterSurveyData data;
        }

        [HttpPost]
        public ActionResult UpdateSurveyReading(UpdateSurveyReadingPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).UpdateSurveyReading(par.data);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSubscriptionByMeterNo
        public class GetSubscriptionByMeterNoPar
        {
            public string sessionID; public string meterNo;
        }

        [HttpPost]
        public ActionResult getSubscriptionByMeterNo(GetSubscriptionByMeterNoPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getSubscriptionByMeterNo(par.meterNo));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPeriodForDate
        public class GetPeriodForDatePar
        {
            public string sessionID; public System.DateTime date;
        }
        public INTAPS.SubscriberManagment.BillPeriod getPeriodForDate(GetPeriodForDatePar par)
        {
            this.SetSessionObject(par.sessionID);
            return ((SubscriberManagmentService)base.SessionObject).getPeriodForDate(par.date);
        }
        #endregion
        #region GetExageratedConsumption
        public class GetExageratedConsumptionPar
        {
            public string sessionID; public int subscriptionID;
        }
        public double getExageratedConsumption(GetExageratedConsumptionPar par)
        {
            this.SetSessionObject(par.sessionID);
            return ((SubscriberManagmentService)base.SessionObject).getExageratedConsumption(par.subscriptionID);
        }
        #endregion
        #region lbSaveLegacySet
        public class lbSaveLegacySetPar
        {
            public string sessionID; public INTAPS.SubscriberManagment.LegacyBillSet set; public INTAPS.SubscriberManagment.LegacyBill[] bills;
        }

        [HttpPost]
        public ActionResult lbSaveLegacySet(lbSaveLegacySetPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).lbSaveLegacySet(par.set, par.bills));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region lbGetLegacyBills
        public class lbGetLegacyBillsPar
        {
            public string sessionID; public int setID;
        }

        [HttpPost]
        public ActionResult lbGetLegacyBills(lbGetLegacyBillsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).lbGetLegacyBills(par.setID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region lbGetSet
        public class lbGetSetPar
        {
            public string sessionID; public string userID; public int pageIndex; public int pageSize;
        }
        public class lbGetSetOut
        {
            public int N;
            public INTAPS.SubscriberManagment.LegacyBillSet[] _ret;
        }

        [HttpPost]
        public ActionResult lbGetSet(lbGetSetPar par)
        {
            try
            {
                var _ret = new lbGetSetOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((SubscriberManagmentService)base.SessionObject).lbGetSet(par.userID, par.pageIndex, par.pageSize, out _ret.N);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region lbGetAllItemDefinations
        public class lbGetAllItemDefinationsPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult lbGetAllItemDefinations(lbGetAllItemDefinationsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).lbGetAllItemDefinations());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFGetMeterReadingByPeriod
        public class BWFGetMeterReadingByPeriodPar
        {
            public string sessionID; public int subcriptionID; public int peridID;
        }

        [HttpPost]
        public ActionResult BWFGetMeterReadingByPeriod(BWFGetMeterReadingByPeriodPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).BWFGetMeterReadingByPeriod(par.subcriptionID, par.peridID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region lbDeleteSet
        public class lbDeleteSetPar
        {
            public string sessionID; public int setID;
        }

        [HttpPost]
        public ActionResult lbDeleteSet(lbDeleteSetPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).lbDeleteSet(par.setID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCustomers
        public class GetCustomersPar
        {
            public string sessionID; public string query; public int kebele; public int index; public int pageSize;
        }
        public class GetCustomersOut
        {
            public int NRecords;
            public INTAPS.SubscriberManagment.SubscriberSearchResult[] _ret;
        }

        [HttpPost]
        public ActionResult GetCustomers(GetCustomersPar par)
        {
            try
            {
                var _ret = new GetCustomersOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((SubscriberManagmentService)base.SessionObject).GetCustomers(par.query, par.kebele, par.index, par.pageSize, out _ret.NRecords);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCurrentPaymentCenter
        public class GetCurrentPaymentCenterPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult GetCurrentPaymentCenter(GetCurrentPaymentCenterPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetCurrentPaymentCenter());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllPaymentCenters
        public class GetAllPaymentCentersPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult GetAllPaymentCenters(GetAllPaymentCentersPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetAllPaymentCenters());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeletePaymentCenter
        public class DeletePaymentCenterPar
        {
            public string sessionID; public int centerID;
        }


        [HttpPost]
        public ActionResult DeletePaymentCenter(DeletePaymentCenterPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).DeletePaymentCenter(par.centerID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdatePaymentCenter
        public class UpdatePaymentCenterPar
        {
            public string sessionID; public INTAPS.SubscriberManagment.PaymentCenter center;
        }

        [HttpPost]
        public ActionResult UpdatePaymentCenter(UpdatePaymentCenterPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).UpdatePaymentCenter(par.center);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CreatePaymentCenter
        public class CreatePaymentCenterPar
        {
            public string sessionID; public INTAPS.SubscriberManagment.PaymentCenter center;
        }

        [HttpPost]
        public ActionResult CreatePaymentCenter(CreatePaymentCenterPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).CreatePaymentCenter(par.center));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region IsPaymentSerialBatchOverlapped
        public class IsPaymentSerialBatchOverlappedPar
        {
            public string sessionID; public INTAPS.Accounting.SerialBatch batch;
        }

        [HttpPost]
        public ActionResult IsPaymentSerialBatchOverlapped(IsPaymentSerialBatchOverlappedPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).IsPaymentSerialBatchOverlapped(par.batch));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetTranslationTable
        public class GetTranslationTablePar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getTranslationTable(GetTranslationTablePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getTranslationTable());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SaveTranslation
        public class SaveTranslationPar
        {
            public string sessionID; public INTAPS.SubscriberManagment.TranslatorEntry[] newEntries;
        }

        [HttpPost]
        public ActionResult saveTranslation(SaveTranslationPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).saveTranslation(par.newEntries);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetUpdateNumber
        public class GetUpdateNumberPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getUpdateNumber(GetUpdateNumberPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getUpdateNumber());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFGetDescribedReadings
        public class BWFGetDescribedReadingsPar
        {
            public string sessionID; public int blockID; public int index; public int pageSize;
        }
        public class BWFGetDescribedReadingsOut
        {
            public int NRecords;
            public INTAPS.SubscriberManagment.BWFDescribedMeterReading[] _ret;
        }

        [HttpPost]
        public ActionResult BWFGetDescribedReadings(BWFGetDescribedReadingsPar par)
        {
            try
            {
                var _ret = new BWFGetDescribedReadingsOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((SubscriberManagmentService)base.SessionObject).BWFGetDescribedReadings(par.blockID, par.index, par.pageSize, out _ret.NRecords);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region lbSaveLegacyVerificationSet
        public class lbSaveLegacyVerificationSetPar
        {
            public string sessionID; public INTAPS.SubscriberManagment.LegacyBillSet set; public INTAPS.SubscriberManagment.VerificationEntry[] bills;
        }

        [HttpPost]
        public ActionResult lbSaveLegacyVerificationSet(lbSaveLegacyVerificationSetPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).lbSaveLegacyVerificationSet(par.set, par.bills));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region lbFindBySubscriberAndPeriod
        public class lbFindBySubscriberAndPeriodPar
        {
            public string sessionID; public int conID; public int periodID;
        }

        [HttpPost]
        public ActionResult lbFindBySubscriberAndPeriod(lbFindBySubscriberAndPeriodPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).lbFindBySubscriberAndPeriod(par.conID, par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region lbFindByBillNo
        public class lbFindByBillNoPar
        {
            public string sessionID; public string billNo;
        }

        [HttpPost]
        public ActionResult lbFindByBillNo(lbFindByBillNoPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).lbFindByBillNo(par.billNo));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region lbIsAlreadyVerified
        public class lbIsAlreadyVerifiedPar
        {
            public string sessionID; public int ownSetID; public INTAPS.SubscriberManagment.VerificationEntry entry;
        }

        [HttpPost]
        public ActionResult lbIsAlreadyVerified(lbIsAlreadyVerifiedPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).lbIsAlreadyVerified(par.ownSetID, par.entry));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region lbGetVerifiedSetByCustomerAndPeriod
        public class lbGetVerifiedSetByCustomerAndPeriodPar
        {
            public string sessionID; public int customerID; public int periodID;
        }

        [HttpPost]
        public ActionResult lbGetVerifiedSetByCustomerAndPeriod(lbGetVerifiedSetByCustomerAndPeriodPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).lbGetVerifiedSetByCustomerAndPeriod(par.customerID, par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFGetMeterReadingByPeriodID
        public class BWFGetMeterReadingByPeriodIDPar
        {
            public string sessionID; public int subcriptionID; public int periodID;
        }

        [HttpPost]
        public ActionResult BWFGetMeterReadingByPeriodID(BWFGetMeterReadingByPeriodIDPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).BWFGetMeterReadingByPeriodID(par.subcriptionID, par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GeneratCustomerPeriodicBills
        public class GeneratCustomerPeriodicBillsPar
        {
            public string sessionID; public System.DateTime time; public int customerID; public int periodID;
        }

        [HttpPost]
        public ActionResult generatCustomerPeriodicBills(GeneratCustomerPeriodicBillsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).generatCustomerPeriodicBills(par.time, par.customerID, par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GenerateBillsByKebele
        public class GenerateBillsByKebelePar
        {
            public string sessionID; public System.DateTime time; public int periodID; public int[] kebeles;
        }

        [HttpPost]
        public ActionResult generateBillsByKebele(GenerateBillsByKebelePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).generateBillsByKebele(par.time, par.periodID, par.kebeles));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CancelBillGeneration
        public class CancelBillGenerationPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult cancelBillGeneration(CancelBillGenerationPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).cancelBillGeneration();

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteCustomerBill
        public class DeleteCustomerBillPar
        {
            public string sessionID; public int billRecordID;
        }

        [HttpPost]
        public ActionResult deleteCustomerBill(DeleteCustomerBillPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).deleteCustomerBill(par.billRecordID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBills
        public class GetBillsPar
        {
            public string sessionID; public int mainTypeID; public int customerID; public int connectionID; public int periodID;
        }

        [HttpPost]
        public ActionResult getBills(GetBillsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getBills(par.mainTypeID, par.customerID, par.connectionID, par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteUnpaidPeriodicBillByKebele
        public class DeleteUnpaidPeriodicBillByKebelePar
        {
            public string sessionID; public int periodID; public int[] kebeles;
        }

        [HttpPost]
        public ActionResult deleteUnpaidPeriodicBillByKebele(DeleteUnpaidPeriodicBillByKebelePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).deleteUnpaidPeriodicBillByKebele(par.periodID, par.kebeles));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region PostBill
        public class PostBillPar
        {
            public string sessionID; public System.DateTime time; public int billRecordID;
        }

        [HttpPost]
        public ActionResult postBill(PostBillPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).postBill(par.time, par.billRecordID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region PostBills
        public class PostBillsPar
        {
            public string sessionID; public System.DateTime time; public int periodID; public int[] billIDs;
        }

        [HttpPost]
        public ActionResult postBills(PostBillsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).postBills(par.time, par.periodID, par.billIDs));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region PostBillsByKebele
        public class PostBillsByKebelePar
        {
            public string sessionID; public System.DateTime time; public int periodID; public int[] kebeles;
        }

        [HttpPost]
        public ActionResult postBillsByKebele(PostBillsByKebelePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).postBillsByKebele(par.time, par.periodID, par.kebeles));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBillDocuments
        public class GetBillDocumentsPar
        {
            public string sessionID; public int mainTypeID; public int customerID; public int connectionID; public int periodID; public bool excludePaid;
        }

        [HttpPost]
        public ActionResult getBillDocuments(GetBillDocumentsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getBillDocuments(par.mainTypeID, par.customerID, par.connectionID, par.periodID, par.excludePaid));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSubscriber
        public class GetSubscriberPar
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult GetSubscriber(GetSubscriberPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetSubscriber(par.code));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetMeterReaderEmployees
        public class GetMeterReaderEmployeesPar
        {
            public string sessionID; public int periodID; public int employeeID;
        }

        [HttpPost]
        public ActionResult GetMeterReaderEmployees(GetMeterReaderEmployeesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetMeterReaderEmployees(par.periodID, par.employeeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCustomerBillRecord
        public class GetCustomerBillRecordPar
        {
            public string sessionID; public int billRecordID;
        }

        [HttpPost]
        public ActionResult getCustomerBillRecord(GetCustomerBillRecordPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getCustomerBillRecord(par.billRecordID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPaymentCenter
        public class GetPaymentCenterPar
        {
            public string sessionID; public string userID;
        }

        [HttpPost]
        public ActionResult GetPaymentCenter(GetPaymentCenterPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetPaymentCenter(par.userID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPaymentCenterByCashAccount
        public class GetPaymentCenterByCashAccountPar
        {
            public string sessionID; public int account;
        }

        [HttpPost]
        public ActionResult GetPaymentCenterByCashAccount(GetPaymentCenterByCashAccountPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetPaymentCenterByCashAccount(par.account));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPaymentCenter2
        public class GetPaymentCenter2Par
        {
            public string sessionID; public int centerID;
        }

        [HttpPost]
        public ActionResult GetPaymentCenter2(GetPaymentCenter2Par par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetPaymentCenter(par.centerID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region IsBillPaid
        public class IsBillPaidPar
        {
            public string sessionID; public int invoiceNo;
        }

        [HttpPost]
        public ActionResult isBillPaid(IsBillPaidPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).isBillPaid(par.invoiceNo));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCustomerBillItems
        public class GetCustomerBillItemsPar
        {
            public string sessionID; public int billRecordID;
        }

        [HttpPost]
        public ActionResult getCustomerBillItems(GetCustomerBillItemsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getCustomerBillItems(par.billRecordID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetLastMessageNumber
        public class GetLastMessageNumberPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getLastMessageNumber(GetLastMessageNumberPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getLastMessageNumber());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CountMessages
        public class CountMessagesPar
        {
            public string sessionID; public int after;
        }

        [HttpPost]
        public ActionResult countMessages(CountMessagesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).countMessages(par.after));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetMessageList
        public class GetMessageListPar
        {
            public string sessionID; public int from; public int to;
        }

        [HttpPost]
        public ActionResult getMessageList(GetMessageListPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                var list = ((SubscriberManagmentService)base.SessionObject).getMessageList(par.from, par.to);
                return Json(new BinObject(list));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetFirstMessageNumber
        public class GetFirstMessageNumberPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getFirstMessageNumber(GetFirstMessageNumberPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getFirstMessageNumber());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region PostOfflineMessages
        public class MessageListPar
        {
            public OfflineMessageTyped[] data;
            public byte[] signature;
            public MessageListPar()
            {
            }
        }
        public class PaymentCenterMessageListPar
        {
            public int paymentCenterID;
            public MessageListPar list;
            public PaymentCenterMessageListPar(int paymentCenterID, MessageListPar list)
            {
                this.paymentCenterID = paymentCenterID;
                this.list = list;
            }
        }
        public class OfflineMessageTyped
        {
            public int id;
            public DateTime sentDate = DateTime.Now;
            public TypeObject messageData;
            public bool delivered = false;
            public DateTime deliveryDate = DateTime.Now;
            public int previousMessageID = -1;
        }
        public class PostOfflineMessagesPar
        {
            public string sessionID; public PaymentCenterMessageListPar msgList;
        }

        [HttpPost]
        public ActionResult postOfflineMessages(PostOfflineMessagesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                var msgList = new MessageList();
                msgList.signature = par.msgList.list.signature;
                msgList.data = par.msgList.list.data.Select(x => new OfflineMessage
                {
                    delivered = x.delivered,
                    deliveryDate = x.deliveryDate,
                    id = x.id,
                    messageData = x.messageData.GetConverted(),
                    previousMessageID = x.previousMessageID,
                    sentDate = x.sentDate
                }).ToArray();

                var list = new INTAPS.SubscriberManagment.PaymentCenterMessageList(par.msgList.paymentCenterID, msgList);

                ((SubscriberManagmentService)base.SessionObject).postOfflineMessages(list, true);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion

        #region postOfflineMessagesAsBytes
        public class PostOfflineMessagesAsBytesPar
        {
            public string sessionID; public byte[] data;
        }
        public ActionResult postOfflineMessagesAsBytes(PostOfflineMessagesAsBytesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                using (var ms = new System.IO.MemoryStream(par.data))
                {
                    var formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    var list = formatter.Deserialize(ms) as INTAPS.SubscriberManagment.PaymentCenterMessageList;
                    ((SubscriberManagmentService)base.SessionObject).postOfflineMessages(list, true);
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SetSubscriptionCoordinate
        public class SetSubscriptionCoordinatePar
        {
            public string sessionID; public int subcriptionID; public long version; public double x; public double y;
        }

        [HttpPost]
        public ActionResult setSubscriptionCoordinate(SetSubscriptionCoordinatePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).setSubscriptionCoordinate(par.subcriptionID, par.version, par.x, par.y);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetMetersNearBy
        public class GetMetersNearByPar
        {
            public string sessionID; public int readerID; public double lat0; public double lng0; public double radius; public long version;
        }

        [HttpPost]
        public ActionResult getMetersNearBy(GetMetersNearByPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getMetersNearBy(par.readerID, par.lat0, par.lng0, par.radius, par.version));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllMetersMap
        public class GetAllMetersMapPar
        {
            public string sessionID; public long version;
        }

        [HttpPost]
        public ActionResult getAllMetersMap(GetAllMetersMapPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getAllMetersMap(par.version));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetUnreadMetersMap
        public class GetUnreadMetersMapPar
        {
            public string sessionID; public long version;
        }

        [HttpPost]
        public ActionResult getUnreadMetersMap(GetUnreadMetersMapPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getUnreadMetersMap(par.version));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetReadingMap
        public class GetReadingMapPar
        {
            public string sessionID; public int periodID;
        }

        [HttpPost]
        public ActionResult getReadingMap(GetReadingMapPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getReadingMap(par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetMineOthers
        public class GetMineOthersPar
        {
            public string sessionID; public int periodID; public int readerID;
        }

        [HttpPost]
        public ActionResult getMineOthers(GetMineOthersPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getMineOthers(par.periodID, par.readerID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region locateWaterMeter
        public class locateWaterMeterPar
        {
            public string sessionID; public int connectionID; public long version;
        }

        [HttpPost]
        public ActionResult locateWaterMeter(locateWaterMeterPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).locateWaterMeter(par.connectionID, par.version));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DisconnectCandidate
        public class DisconnectCandidatePar
        {
            public string sessionID; public int readingPeriodID;
        }

        [HttpPost]
        public ActionResult disconnectCandidate(DisconnectCandidatePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).disconnectCandidate(par.readingPeriodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAverageReading
        public class GetAverageReadingPar
        {
            public string sessionID; public int subscriptionID; public int periodID; public int nmonths;
        }

        [HttpPost]
        public ActionResult getAverageReading(GetAverageReadingPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getAverageReading(par.subscriptionID, par.periodID, par.nmonths));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetMeterReadingEmployee
        public class GetMeterReadingEmployeePar
        {
            public string sessionID; public string loginName;
        }

        [HttpPost]
        public ActionResult GetMeterReadingEmployee(GetMeterReadingEmployeePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).GetMeterReadingEmployee(par.loginName));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetReadingCycle
        public class GetReadingCyclePar
        {
            public string sessionID; public int periodID;
        }

        [HttpPost]
        public ActionResult getReadingCycle(GetReadingCyclePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getReadingCycle(par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCurrentReadingCycle
        public class GetCurrentReadingCyclePar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getCurrentReadingCycle(GetCurrentReadingCyclePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getCurrentReadingCycle());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region OpenReadingCycle
        public class OpenReadingCyclePar
        {
            public string sessionID; public int periodID;
        }

        [HttpPost]
        public ActionResult openReadingCycle(OpenReadingCyclePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).openReadingCycle(par.periodID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteReadingCycle
        public class DeleteReadingCyclePar
        {
            public string sessionID; public int periodID;
        }

        [HttpPost]
        public ActionResult deleteReadingCycle(DeleteReadingCyclePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).deleteReadingCycle(par.periodID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetReadingSheet
        public class GetReadingSheetPar
        {
            public string sessionID; public int periodID;
        }

        [HttpPost]
        public ActionResult getReadingSheet(GetReadingSheetPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getReadingSheet(par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CloseReadingCycle
        public class CloseReadingCyclePar
        {
            public string sessionID; public int periodID;
        }

        [HttpPost]
        public ActionResult closeReadingCycle(CloseReadingCyclePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).closeReadingCycle(par.periodID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetReadingShowRow
        public class GetReadingShowRowPar
        {
            public string sessionID; public int periodID; public int rowIndex;
        }

        [HttpPost]
        public ActionResult getReadingShowRow(GetReadingShowRowPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getReadingShowRow(par.periodID, par.rowIndex));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ReadSheetSize 
        public class ReadSheetSizePar
        {
            public string sessionID; public int periodID;
        }

        [HttpPost]
        public ActionResult readSheetSize(ReadSheetSizePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).readSheetSize(par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SetReadingSheetRow
        public class SetReadingSheetRowPar
        {
            public string sessionID; public INTAPS.SubscriberManagment.ReadingSheetRow row;
        }

        [HttpPost]
        public ActionResult setReadingSheetRow(SetReadingSheetRowPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).setReadingSheetRow(par.row);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetReadingPath2
        public class GetReadingPath2Par
        {
            public string sessionID; public int periodID; public int readerID;
        }
        public class GetReadingPath2Out
        {
            public int notMapped;
            public INTAPS.SubscriberManagment.ReadingPath _ret;
        }

        [HttpPost]
        public ActionResult getReadingPath2(GetReadingPath2Par par)
        {
            try
            {
                var _ret = new GetReadingPath2Out();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((SubscriberManagmentService)base.SessionObject).getReadingPath(par.periodID, par.readerID, out _ret.notMapped);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SearchCustomerReceipts
        public class SearchCustomerReceiptsPar
        {
            public string sessionID; public string query; public INTAPS.SubscriberManagment.SubscriberSearchField field; public int casherAccountID; public int resultSize;
        }

        [HttpPost]
        public ActionResult searchCustomerReceipts(SearchCustomerReceiptsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).searchCustomerReceipts(par.query, par.field, par.casherAccountID, par.resultSize));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetMeterReadingEmployee2
        public class GetMeterReadingEmployee2Par
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getMeterReadingEmployee2(GetMeterReadingEmployee2Par par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getMeterReadingEmployee());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GenerateOutstandingBills
        public class GenerateOutstandingBillsPar
        {
            public string sessionID; public int customerID;
        }

        [HttpPost]
        public ActionResult generateOutstandingBills(GenerateOutstandingBillsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).generateOutstandingBills(par.customerID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSettingTypes
        public class GetSettingTypesPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getSettingTypes(GetSettingTypesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getSettingTypes());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SetBillingRuleSetting
        public class SetBillingRuleSettingPar
        {
            public string sessionID; public System.Type t; public BinObject setting;
        }

        [HttpPost]
        public ActionResult setBillingRuleSetting(SetBillingRuleSettingPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).setBillingRuleSetting(par.t, par.setting.Deserialized());

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBillingRuleSetting
        public class GetBillingRuleSettingPar
        {
            public string sessionID; public System.Type t;
        }

        [HttpPost]
        public ActionResult getBillingRuleSetting(GetBillingRuleSettingPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getBillingRuleSetting(par.t));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCustomerSubtypes
        public class GetCustomerSubtypesPar
        {
            public string sessionID; public INTAPS.SubscriberManagment.SubscriberType type;
        }

        [HttpPost]
        public ActionResult getCustomerSubtypes(GetCustomerSubtypesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getCustomerSubtypes(par.type));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCustomerSubtype
        public class GetCustomerSubtypePar
        {
            public string sessionID; public INTAPS.SubscriberManagment.SubscriberType type; public int subTypeID;
        }

        [HttpPost]
        public ActionResult getCustomerSubtype(GetCustomerSubtypePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getCustomerSubtype(par.type, par.subTypeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region PreviewPrepaidReceipt
        public class PreviewPrepaidReceiptPar
        {
            public string sessionID; public System.DateTime date; public int subscriptionID; public double cubicMeter;
        }

        [HttpPost]
        public ActionResult PreviewPrepaidReceipt(PreviewPrepaidReceiptPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).PreviewPrepaidReceipt(par.date, par.subscriptionID, par.cubicMeter));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GeneratePrepaidReceipt
        public class GeneratePrepaidReceiptPar
        {
            public string sessionID; public System.DateTime date; public int subscriptionID; public double cubicMeter; public BIZNET.iERP.PaymentInstrumentItem[] paymentInstruments;
        }
        public class GeneratePrepaidReceiptOut
        {
            public PrePaymentBill bill;
            public INTAPS.SubscriberManagment.CustomerPaymentReceipt _ret;
        }

        [HttpPost]
        public ActionResult GeneratePrepaidReceipt(GeneratePrepaidReceiptPar par)
        {
            try
            {
                var _ret = new GeneratePrepaidReceiptOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((SubscriberManagmentService)base.SessionObject).GeneratePrepaidReceipt(par.date, par.subscriptionID, par.cubicMeter, par.paymentInstruments, out _ret.bill);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateBillingCycle
        public class UpdateBillingCyclePar
        {
            public string sessionID; public INTAPS.SubscriberManagment.BillingCycle cycle;
        }

        [HttpPost]
        public ActionResult updateBillingCycle(UpdateBillingCyclePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).updateBillingCycle(par.cycle);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllBillingCycles
        public class GetAllBillingCyclesPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getAllBillingCycles(GetAllBillingCyclesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getAllBillingCycles());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CreateBillingCycle
        public class CreateBillingCyclePar
        {
            public string sessionID; public INTAPS.SubscriberManagment.BillingCycle cycle;
        }

        [HttpPost]
        public ActionResult createBillingCycle(CreateBillingCyclePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).createBillingCycle(par.cycle);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCurrentBillingCycle
        public class GetCurrentBillingCyclePar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getCurrentBillingCycle(GetCurrentBillingCyclePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getCurrentBillingCycle());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBillingCycle
        public class GetBillingCyclePar
        {
            public string sessionID; public int periodID;
        }

        [HttpPost]
        public ActionResult getBillingCycle(GetBillingCyclePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getBillingCycle(par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteBillingCycle
        public class DeleteBillingCyclePar
        {
            public string sessionID; public int periodID;
        }

        [HttpPost]
        public ActionResult deleteBillingCycle(DeleteBillingCyclePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).deleteBillingCycle(par.periodID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SingleMeterHistoricalReadingMap
        public class SingleMeterHistoricalReadingMapPar
        {
            public string sessionID; public int connectionID;
        }

        [HttpPost]
        public ActionResult singleMeterHistoricalReadingMap(SingleMeterHistoricalReadingMapPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).singleMeterHistoricalReadingMap(par.connectionID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetReadingPath
        public class GetReadingPathPar
        {
            public string sessionID; public int periodID; public int readerID; public System.DateTime d1; public System.DateTime d2;
        }
        public class GetReadingPathOut
        {
            public int notMapped;
            public INTAPS.SubscriberManagment.ReadingPath _ret;
        }

        [HttpPost]
        public ActionResult getReadingPath(GetReadingPathPar par)
        {
            try
            {
                var _ret = new GetReadingPathOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((SubscriberManagmentService)base.SessionObject).getReadingPath(par.periodID, par.readerID, par.d1, par.d2, out _ret.notMapped);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetReadingDates
        public class GetReadingDatesPar
        {
            public string sessionID; public int periodID; public int readerID;
        }
        public System.DateTime[] getReadingDates(GetReadingDatesPar par)
        {
            this.SetSessionObject(par.sessionID);
            return ((SubscriberManagmentService)base.SessionObject).getReadingDates(par.periodID, par.readerID);
        }

        [HttpPost]
        public ActionResult analyzeReading(string sessionID, int periodID, bool updateConnectionLocations)
        {
            try
            {
                this.SetSessionObject(sessionID);
                ((SubscriberManagmentService)base.SessionObject).analyzeReading(periodID, updateConnectionLocations);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCreditScheme
        public class GetCreditSchemePar
        {
            public string sessionID; public int schemeID;
        }

        [HttpPost]
        public ActionResult getCreditScheme(GetCreditSchemePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getCreditScheme(par.schemeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region getCustomerCreditSchemes2
        public class getCustomerCreditSchemes2Par
        {
            public string sessionID; public int customerID;
        }

        [HttpPost]
        public ActionResult getCustomerCreditSchemes2(getCustomerCreditSchemes2Par par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getCustomerCreditSchemes(par.customerID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region AddNoneBillCrediScheme
        public class AddNoneBillCrediSchemePar
        {
            public string sessionID; public INTAPS.SubscriberManagment.CreditScheme scheme;
        }

        [HttpPost]
        public ActionResult addNoneBillCrediScheme(AddNoneBillCrediSchemePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).addNoneBillCrediScheme(par.scheme));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteCreditScheme
        public class DeleteCreditSchemePar
        {
            public string sessionID; public int schemeID; public bool deleteBills;
        }

        [HttpPost]
        public ActionResult deleteCreditScheme(DeleteCreditSchemePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).deleteCreditScheme(par.schemeID, par.deleteBills);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCustomerCreditSchemes
        public class GetCustomerCreditSchemesPar
        {
            public string sessionID; public int customerID;
        }
        public class GetCustomerCreditSchemesOut
        {
            public double[] settlement;
            public INTAPS.SubscriberManagment.CreditScheme[] _ret;
        }

        [HttpPost]
        public ActionResult getCustomerCreditSchemes(GetCustomerCreditSchemesPar par)
        {
            try
            {
                var _ret = new GetCustomerCreditSchemesOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((SubscriberManagmentService)base.SessionObject).getCustomerCreditSchemes(par.customerID, out _ret.settlement);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region TransferCustomerData
        public class TransferCustomerDataPar
        {
            public string sessionID; public int wsisPeriodID;
        }

        [HttpPost]
        public ActionResult transferCustomerData(TransferCustomerDataPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).transferCustomerData(par.wsisPeriodID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UploadReading 
        public class UploadReadingPar
        {
            public string sessionID; public int wsisPeriodID;
        }

        [HttpPost]
        public ActionResult UploadReading(UploadReadingPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).UploadReading(par.wsisPeriodID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region RegisterMobileReader
        public class RegisterMobileReaderPar
        {
            public string sessionID; public Employee emp; public string password; public int periodID;
        }

        [HttpPost]
        public ActionResult registerMobileReader(RegisterMobileReaderPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).registerMobileReader(par.emp, par.password, par.periodID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ChangePassword
        public class ChangePasswordPar
        {
            public string sessionID; public string userName; public string password;
        }

        [HttpPost]
        public ActionResult changePassword(ChangePasswordPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).changePassword(par.userName, par.password);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetReadingStatistics
        public class GetReadingStatisticsPar
        {
            public string sessionID; public int periodID;
        }

        [HttpPost]
        public ActionResult getReadingStatistics(GetReadingStatisticsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getReadingStatistics(par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetReaderCount
        public class GetReaderCountPar
        {
            public string sessionID; public int periodID; public int readerID;
        }

        [HttpPost]
        public ActionResult getReaderCount(GetReaderCountPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getReaderCount(par.periodID, par.readerID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetReadingCycle2
        public class GetReadingCycle2Par
        {
            public string sessionID; public int periodID; public int status;
        }

        [HttpPost]
        public ActionResult getReadingCycle2(GetReadingCycle2Par par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getReadingCycle(par.periodID, par.status));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFGetDescribedReadings2
        public class BWFGetDescribedReadings2Par
        {
            public string sessionID; public int blockID; public int periodID; public string contractNo; public string name; public int kebele; public int customerType; public int index; public int pageSize;
        }
        public class BWFGetDescribedReadings2Out
        {
            public int NRecords;
            public BWFDescribedMeterReading[] _ret;
        }

        [HttpPost]
        public ActionResult BWFGetDescribedReadings2(BWFGetDescribedReadings2Par par)
        {
            try
            {
                var _ret = new BWFGetDescribedReadings2Out();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((SubscriberManagmentService)base.SessionObject).BWFGetDescribedReadings(par.blockID, par.periodID, par.contractNo, par.name, par.kebele, par.customerType, par.index, par.pageSize, out _ret.NRecords);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region TransferSubscriptionsToReader
        public class TransferSubscriptionsToReaderPar
        {
            public string sessionID; public int[] subscriptions; public int readerID; public int periodID;
        }

        [HttpPost]
        public ActionResult transferSubscriptionsToReader(TransferSubscriptionsToReaderPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).transferSubscriptionsToReader(par.subscriptions, par.readerID, par.periodID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region RegisterProductionPoint
        public class RegisterProductionPointPar
        {
            public string sessionID; public INTAPS.SubscriberManagment.ProductionPoint productionPoint;
        }

        [HttpPost]
        public ActionResult registerProductionPoint(RegisterProductionPointPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).registerProductionPoint(par.productionPoint));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetProductionPoint2
        public class GetProductionPoint2Par
        {
            public string sessionID; public string name; public int kebele; public int index; public int type; public int pageSize;
        }
        public class GetProductionPoint2Out
        {
            public int NRecords;
            public INTAPS.SubscriberManagment.ProductionPoint[] _ret;
        }

        [HttpPost]
        public ActionResult getProductionPoint2(GetProductionPoint2Par par)
        {
            try
            {
                var _ret = new GetProductionPoint2Out();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((SubscriberManagmentService)base.SessionObject).getProductionPoint(par.name, par.kebele, par.index, par.type, par.pageSize, out _ret.NRecords);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetProductionPoint
        public class GetProductionPointPar
        {
            public string sessionID; public int id;
        }

        [HttpPost]
        public ActionResult getProductionPoint(GetProductionPointPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getProductionPoint(par.id));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SetProductionPointReading
        public class SetProductionPointReadingPar
        {
            public string sessionID; public System.DateTime readingDate; public double reading; public MeterReadingType readingType; public int productionPointID;
        }

        [HttpPost]
        public ActionResult setProductionPointReading(SetProductionPointReadingPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).setProductionPointReading(par.readingDate, par.reading, par.readingType, par.productionPointID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateProductionPointReading
        public class UpdateProductionPointReadingPar
        {
            public string sessionID; public ProductionPointReading reading; public bool reset;
        }

        [HttpPost]
        public ActionResult updateProductionPointReading(UpdateProductionPointReadingPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).updateProductionPointReading(par.reading, par.reset);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region HtmlDocGetCustomerBillData
        public class HtmlDocGetCustomerBillDataPar
        {
            public string sessionID; public int customerID; public int connectionID;
        }
        public class HtmlDocGetCustomerBillDataOut
        {
            public string headers;
            public string _ret;
        }

        [HttpPost]
        public ActionResult htmlDocGetCustomerBillData(HtmlDocGetCustomerBillDataPar par)
        {
            try
            {
                var _ret = new HtmlDocGetCustomerBillDataOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((SubscriberManagmentService)base.SessionObject).htmlDocGetCustomerBillData(par.customerID, par.connectionID, out _ret.headers);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region HtmlDocGetCustomerList
        public class HtmlDocGetCustomerListPar
        {
            public string sessionID; public SubscriberSearchResult[] result;
        }
        public class HtmlDocGetCustomerListOut
        {
            public string headers;
            public string _ret;
        }

        [HttpPost]
        public ActionResult htmlDocGetCustomerList(HtmlDocGetCustomerListPar par)
        {
            try
            {
                var _ret = new HtmlDocGetCustomerListOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((SubscriberManagmentService)base.SessionObject).htmlDocGetCustomerList(par.result, out _ret.headers);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region HtmlDocGetReadings
        public class HtmlDocGetReadingsPar
        {
            public string sessionID; public int kebele; public int subscriptionID; public int periodID; public int employeeID;
        }
        public class HtmlDocGetReadingsOut
        {
            public string headers;
            public string _ret;
        }

        [HttpPost]
        public ActionResult htmlDocGetReadings(HtmlDocGetReadingsPar par)
        {
            try
            {
                var _ret = new HtmlDocGetReadingsOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((SubscriberManagmentService)base.SessionObject).htmlDocGetReadings(par.kebele, par.subscriptionID, par.periodID, par.employeeID, out _ret.headers);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetReadingCustomFields
        public class GetReadingCustomFieldsPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getReadingCustomFields(GetReadingCustomFieldsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getReadingCustomFields());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSMSPreviewHTML
        public class GetSMSPreviewHTMLPar
        {
            public string sessionID; public INTAPS.SubscriberManagment.CustomerSMSSendParameter send;
        }

        [HttpPost]
        public ActionResult getSMSPreviewHTML(GetSMSPreviewHTMLPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getSMSPreviewHTML(par.send));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSMSInterfaceStatus
        public class GetSMSInterfaceStatusPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getSMSInterfaceStatus(GetSMSInterfaceStatusPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getSMSInterfaceStatus());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SendSMS
        public class SendSMSPar
        {
            public string sessionID; public INTAPS.SubscriberManagment.CustomerSMSSendParameter send;
        }

        [HttpPost]
        public ActionResult sendSMS(SendSMSPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).sendSMS(par.send);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFGetMeterReadings2
        public class BWFGetMeterReadings2Par
        {
            public string sessionID; public INTAPS.SubscriberManagment.MeterReadingFilter filter; public int index; public int pageSize;
        }
        public class BWFGetMeterReadings2Out
        {
            public int NRecords;
            public INTAPS.SubscriberManagment.BWFMeterReading[] _ret;
        }

        [HttpPost]
        public ActionResult BWFGetMeterReadings2(BWFGetMeterReadings2Par par)
        {
            try
            {
                var _ret = new BWFGetMeterReadings2Out();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((SubscriberManagmentService)base.SessionObject).BWFGetMeterReadings(par.filter, par.index, par.pageSize, out _ret.NRecords);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region BWFAcceptRejectReading
        public class BWFAcceptRejectReadingPar
        {
            public string sessionID; public bool accept; public int periodID; public INTAPS.SubscriberManagment.MeterReadingFilter filter; public int[] specific;
        }

        [HttpPost]
        public ActionResult BWFAcceptRejectReading(BWFAcceptRejectReadingPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).BWFAcceptRejectReading(par.accept, par.periodID, par.filter, par.specific);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ChangeSMSPassCode
        public class ChangeSMSPassCodePar
        {
            public string sessionID; public string oldPassCode; public string newPassCode;
        }

        [HttpPost]
        public ActionResult changeSMSPassCode(ChangeSMSPassCodePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).changeSMSPassCode(par.oldPassCode, par.newPassCode);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSMSLicense
        public class GetSMSLicensePar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getSMSLicense(GetSMSLicensePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getSMSLicense());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllDMAs
        public class GetAllDMAsPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getAllDMAs(GetAllDMAsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getAllDMAs());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllPressureZones
        public class GetAllPressureZonesPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getAllPressureZones(GetAllPressureZonesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getAllPressureZones());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDMA
        public class GetDMAPar
        {
            public string sessionID; public int id;
        }

        [HttpPost]
        public ActionResult getDMA(GetDMAPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getDMA(par.id));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPressureZone
        public class GetPressureZonePar
        {
            public string sessionID; public int id;
        }

        [HttpPost]
        public ActionResult getPressureZone(GetPressureZonePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getPressureZone(par.id));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSMSLogByPhoneNo
        public class GetSMSLogByPhoneNoPar
        {
            public string sessionID; public int sendID; public int receivedID; public string phoneNo;
        }

        [HttpPost]
        public ActionResult getSMSLogByPhoneNo(GetSMSLogByPhoneNoPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getSMSLogByPhoneNo(par.sendID, par.receivedID, par.phoneNo));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetKebele
        public class GetKebelePar
        {
            public string sessionID; public int kebeleID;
        }

        [HttpPost]
        public ActionResult getKebele(GetKebelePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getKebele(par.kebeleID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region MergeCustomers
        public class MergeCustomersPar
        {
            public string sessionID; public int sid1; public int sid2;
        }

        [HttpPost]
        public ActionResult mergeCustomers(MergeCustomersPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).mergeCustomers(par.sid1, par.sid2);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteSystemFeeDeposit
        public class DeleteSystemFeeDepositPar
        {
            public string sessionID; public int periodID; public int readerID; public DateTime returnDate;
        }

        [HttpPost]
        public ActionResult deleteSystemFeeDeposit(DeleteSystemFeeDepositPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).deleteSystemFeeDeposit(par.periodID, par.readerID, par.returnDate);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateSystemFeeDeposit
        public class UpdateSystemFeeDepositPar
        {
            public string sessionID; public SystemFeeDeposit deposit;
        }

        [HttpPost]
        public ActionResult updateSystemFeeDeposit(UpdateSystemFeeDepositPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).updateSystemFeeDeposit(par.deposit);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region AddSystemFeeDeposit
        public class AddSystemFeeDepositPar
        {
            public string sessionID; public SystemFeeDeposit deposit;
        }

        [HttpPost]
        public ActionResult addSystemFeeDeposit(AddSystemFeeDepositPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).addSystemFeeDeposit(par.deposit);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDeposit
        public class GetDepositPar
        {
            public string sessionID; public int periodID; public int readerID; public DateTime day;
        }

        [HttpPost]
        public ActionResult getDeposit(GetDepositPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getDeposit(par.periodID, par.readerID, par.day));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDeposits
        public class GetDepositsPar
        {
            public string sessionID; public int periodID; public int readerID; public DateTime from; public DateTime to;
        }

        [HttpPost]
        public ActionResult getDeposits(GetDepositsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getDeposits(par.periodID, par.readerID, par.from, par.to));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSystemDepositFeeBalance
        public class GetSystemDepositFeeBalancePar
        {
            public string sessionID; public int readerID; public DateTime date;
        }

        [HttpPost]
        public ActionResult getSystemDepositFeeBalance(GetSystemDepositFeeBalancePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getSystemDepositFeeBalance(par.readerID, par.date));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion

        public class StartBillGenerationByKebelePar
        {
            public String sessionID;
            public DateTime time; public int periodID; public int[] kebeles;
        }
        [HttpPost]
        public ActionResult startBillGenerationByKebele(StartBillGenerationByKebelePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((SubscriberManagmentService)base.SessionObject).startBillGenerationByKebele(par.time, par.periodID, par.kebeles);
                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        public class SessionIDPar
        {
            public String SessionID;
        }
        [HttpPost]
        public ActionResult IsBillGenerating(SessionIDPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).IsBillGenerating());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        [HttpPost]
        public ActionResult getBillGenerationResult(SessionIDPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).getBillGenerationResult());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        public class PayAvialableBillsPars
        {
            public string sessionID;
            public int customerID;
            public double paymentAmount;
            public string paymentDescription;
        }
        [HttpPost]
        public ActionResult PayAvialableBills(PayAvialableBillsPars par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).PayAvialableBills(par.customerID, par.paymentAmount, par.paymentDescription));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }


        public class RecordUnknownPaymentPars
        {
            public string sessionID;
            public int customerID;
            public double amount;
            public string description;
        }
        [HttpPost]
        public ActionResult RecordUnknownPayment(RecordUnknownPaymentPars par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((SubscriberManagmentService)base.SessionObject).RecordUnknownPayment(par.customerID, par.amount, par.description));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
    }
}

