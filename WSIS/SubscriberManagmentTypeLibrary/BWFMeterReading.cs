using System;
using INTAPS.RDBMS;
namespace INTAPS.SubscriberManagment
{
    [Flags]
    public enum ReadingExtraInfo
    {
        None = 0,
        Coordinate = 1,
        ReadingDate = 2,
        Remark = 4,
    }
    [Serializable]
    [SingleTableObject]
    public class ReadingReset
    {
        [IDField]
        public int connectionID;
        [DataField]
        public int periodID;
        [DataField]
        public long ticks;
        [DataField]
        public double reading;
    }
    [Serializable, SingleTableObject(orderBy = "readingBlockID,orderN")]
    public class BWFMeterReading
    {

        [IDField]
        public int readingBlockID = -1;
        [IDField]
        public int subscriptionID = -1;

        [DataField]
        public int averageMonths = 0;
        [DataField(false)]
        public int billDocumentID = -1;
        [DataField]
        public BWFStatus bwfStatus = BWFStatus.Unread;
        [DataField(false)]
        public double consumption = 0.0;
        [DataField(false)]
        public DateTime entryDate = DateTime.Now;
        [DataField(false)]
        public int orderN = -1;
        [DataField]
        public int periodID;
        [DataField(false)]
        public double reading = 0.0;

        [DataField(false)]
        public MeterReadingType readingType = MeterReadingType.Normal;

        [DataField(false)]
        public MeterReadingProblem readingProblem = MeterReadingProblem.NoProblem;

        [DataField]
        public ReadingExtraInfo extraInfo = ReadingExtraInfo.None;
        [DataField]
        public string readingRemark = "";
        [DataField]
        public DateTime readingTime = DateTime.Now;
        [DataField]
        public double readingX = 0;
        [DataField]
        public double readingY = 0;
        [DataField]
        public ReadingMethod method = ReadingMethod.ReaderPaper;
        public string readingProblemString
        {
            get
            {
                switch (readingProblem)
                {
                    case MeterReadingProblem.NoProblem:
                        return "No Problem";
                    case MeterReadingProblem.VisistedNoAttendant:
                        return "Nobody Home";
                    case MeterReadingProblem.VisistedNotCooperative:
                        return "Household Not Cooperative";
                    case MeterReadingProblem.VisistedDefectiveMeter:
                        return "Defective Meter";
                    case MeterReadingProblem.VisistedMeterPhysicallyInaccessible:
                        return "Inaccessible Meter";
                    case MeterReadingProblem.Other:
                        return "General Problem";
                    default:
                        return "";
                }
            }
        }

        public string getRangeString()
        {
            switch (this.readingType)
            {
                case MeterReadingType.Normal:
                case MeterReadingType.Average:
                    return string.Concat(new object[] { this.consumption, " meter cube (", (this.reading - this.consumption).ToString("0"), " to ", this.reading.ToString("0"), " )" });
            }
            return (this.consumption + " meter cube ");
        }

        public bool hasCoordinate { get { return readingX != 0 || readingY != 0; } }
    }
    [Serializable]
    public class BWFDescribedMeterReading
    {
        public string customerCode;
        public string customerName;
        public string contractNo;
        public BWFMeterReading reading;
        public BWFMeterReading previousReading = null;
        public BillPeriod previousPeriod = null;
        public BWFDescribedMeterReading(Subscription subscription, BWFMeterReading r)
        {
            customerCode = subscription.subscriber.customerCode;
            customerName = subscription.subscriber.name;
            contractNo = subscription.contractNo;
            this.reading = r;
        }
    }
    [Serializable]
    public class MeterReadingFilter
    {
        public int[] periods= null;
        public int[] customerTypes = null;
        public int[] kebeles = null;
        public BWFStatus[] readingStatus = null;
        public int[] meterReaders = null;
        public int[] subscriptions = null;
    }
}