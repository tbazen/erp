
    using INTAPS.RDBMS;
    using System;
namespace INTAPS.SubscriberManagment
{

    [Serializable, SingleTableObject(orderBy="blockName")]
    public class BWFReadingBlock
    {
        [DataField]
        public string blockName;
        [DataField]
        public int entryClerkID;
        [IDField]
        public int id;
        [DataField]
        public int periodID;
        [DataField]
        public int readerID;
        [DataField]
        public DateTime readingEnd;
        [DataField]
        public DateTime readingStart;
    }
}

