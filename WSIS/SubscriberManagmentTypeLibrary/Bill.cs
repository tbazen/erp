﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.RDBMS;
using System.Xml.Serialization;
using INTAPS.ClientServer;
using INTAPS;
using BIZNET.iERP;
namespace INTAPS.SubscriberManagment
{
    [Serializable]
    public class BillingRuleSettingType
    {
        public Type type()
        {
            return Type.GetType(typeName);
        }
        public String typeName;
        public string name;
    }
    
    public interface IBillingRule
    {
        bool buildMonthlyBill(Subscriber customer, DateTime time, BillPeriod period, out CustomerBillDocument[] billDocs, out CustomerBillRecord[] bills, out BillItem[][] items, out string msg);
        double getExageratedReading(int connectionID);
        BillingRuleSettingType[] getSettingTypes();
        void fixSubscriptionDataForSave(Subscription subsc);
        String formatContractNo(Subscription subs,int serialNo);
        String formatCustomerCode(Subscriber customer, int serialNo);
    }
    public enum BillItemAccounting
    {
        Invoice,
        Advance,
        Cash
    }
    [SingleTableObject("CustomerBillItem")]
    [Serializable]
    [INTAPS.RDBMS.TypeID(2)]
    public class BillItem
    {
        [IDField]
        public int customerBillID=-1;
        [IDField]
        public int itemTypeID = 0;
        [DataField]
        public int incomeAccountID = -1;
        [DataField]
        public string description=null;
        [DataField]
        public bool hasUnitPrice=false;
        [DataField]
        public double quantity=0;
        [DataField]
        public string unit="";
        [DataField]
        public double unitPrice=0;
        [DataField]
        public double price=0;
        [DataField]
        public BillItemAccounting accounting= BillItemAccounting.Invoice;
        [DataField]
        public double settledFromDepositAmount=0;
        public bool fullySettledByDebosit
        {
            get
            {
                return AccountBase.AmountEqual(settledFromDepositAmount, price);
            }
        }
        public bool partiallySettledByDebosit
        {
            get
            {
                return AccountBase.AmountGreater(settledFromDepositAmount, 0);
            }
        }
        public double netPayment
        {
            get
            {
                return price - settledFromDepositAmount;
            }
        }

        public BillItem clone()
        {
            return (BillItem)this.MemberwiseClone();
        }
    }
    [Serializable]
    public class CustomerBillSummaryDocument : AccountDocument
    {
        public int[] billIDs= new int[0];
        public TransactionOfBatch[] transactions = new TransactionOfBatch[0];
    }
    [Serializable]
    [NoBudget]
    public class CustomerPaymentReceiptSummaryDocument : AccountDocument
    {
        public int assetAccountID=-1;
        public int[] receiptIDs = new int[0];
        public TransactionOfBatch[] transactions = new TransactionOfBatch[0];
    }
    [Serializable]
    public class ReverseCustomerBillDocument : AccountDocument
    {
        public int billID;
    }
    [Serializable]
    [NoBudget]
    public class ReverseCustomerPaymentReceiptDocument : AccountDocument
    {
        public int receiptID;
    }
    [Serializable]
    public abstract class CustomerBillDocument : AccountDocument
    {
        [DocumentTypedReferenceField("BILL")]
        public DocumentTypedReference invoiceNumber;
        public Subscriber customer;
        

        public abstract BillItem[] items { get; }
        public bool draft = true;
        public bool summerizeTransaction = false;
        public int billBatchID = -1;
        public virtual string groupText
        {
            get
            {
                return null;
            }
        }
        public double total
        {
            get
            {
                double ret = 0;
                foreach (BillItem bi in items)
                    ret += bi.price;
                if(exemptedAmounts!=null)
                    foreach (double e in exemptedAmounts)
                        ret -= e;
                return ret;
            }
        }
        public double totalLessDeposit
        {
            get
            {
                double ret = 0;
                foreach (BillItem bi in items)
                    ret += bi.netPayment;
                if (exemptedAmounts != null)
                    foreach (double e in exemptedAmounts)
                        ret -= e;
                return ret;
            }
        }
        public override string BuildHTML()
        {
            StringBuilder table = new StringBuilder("<table>");
            table.Append("<tr><td>Item</td><td>Unit Price</td><td>Quanity</td><td class='currencyCell'>Price</td></tr>");
            double total = 0;
            foreach (BillItem item in this.items)
            {
                table.Append(string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td class='currencyCell'>{3}</td></tr>"
                    , System.Web.HttpUtility.HtmlEncode(item.description)
                    , item.hasUnitPrice ? System.Web.HttpUtility.HtmlEncode(item.unitPrice.ToString("#,#0.00")) : ""
                    , item.hasUnitPrice ? System.Web.HttpUtility.HtmlEncode(item.quantity.ToString("0.00") + " " + item.unit) : ""
                    , System.Web.HttpUtility.HtmlEncode(item.price.ToString("#,#0.00"))
                    ));
                int exIndex;
                if ((exIndex=ArrayExtension.indexOf(exemptedItems, item.itemTypeID))!=-1)
                {
                    table.Append(string.Format("<tr><td colspan=2></td><td class='currencyCell'>Exemption</td><td class='currencyCell'>{0}</td></tr>"
                    , System.Web.HttpUtility.HtmlEncode("-"+exemptedAmounts[exIndex].ToString("#,#0.00"))
                    ));
                    total -= exemptedAmounts[exIndex];
                }
                total += item.price;
            }
            table.Append(string.Format("<tr><td colspan=3 class='currencyCell'>Total </td><td class='currencyCell'>{0}</td></tr>"
                    , System.Web.HttpUtility.HtmlEncode(total.ToString("#,#0.00"))
                    ));
            table.Append("</table>");
            string html = string.Format("Customer Name: {0}<br/>Invoice No: {1}<br/>Items<br/>{2}", this.customer.name,this.invoiceNumber==null?"":this.invoiceNumber.reference, table.ToString());
            return html;
        }

        //exemption
        public int[] exemptedItems = new int[0];
        public double[] exemptedAmounts = new double[0];
        public List<BillItem> excludeExemption(IEnumerable<BillItem> billItems)
        {
            List<BillItem> ret = new List<BillItem>();
            int i = 0;
            if (this.items != null)
            {
                foreach (BillItem bi in billItems)
                {
                    BillItem clone = bi.clone();

                    int exIndex;
                    if ((exIndex = INTAPS.ArrayExtension.indexOf(exemptedItems, bi.itemTypeID)) != -1)
                    {
                        clone.price -= exemptedAmounts[exIndex];
                    }
                    if (!AccountBase.AmountEqual(clone.price, 0))
                        ret.Add(clone);
                    i++;
                }
            }
            return ret;
        }
        [XmlIgnore]
        public List<BillItem> itemsLessExemption
        {
            get
            {
                return excludeExemption(this.items);
                //List<BillItem> ret = new List<BillItem>();
                //int i=0;
                //if (this.items != null)
                //{
                //    foreach (BillItem bi in this.items)
                //    {
                //        BillItem clone = bi.clone();

                //        int exIndex;
                //        if ((exIndex = INTAPS.ArrayExtension.indexOf(exemptedItems, bi.itemTypeID)) != -1)
                //        {
                //            clone.price -= exemptedAmounts[exIndex];
                //        }
                //        if (!AccountBase.AmountEqual(clone.price, 0))
                //            ret.Add(clone);
                //        i++;
                //    }
                //}
                //return ret;
            }
        }

    }
    [Serializable]
    public abstract class PeriodicBill : CustomerBillDocument
    {
        public BillPeriod period;
    }
    [Serializable]
    [INTAPS.RDBMS.TypeID(3)]
    public class WaterBillDocument : PeriodicBill
    {
        public Subscription subscription;
        public BWFMeterReading reading;
        public BillItem[] waterBillItems;
        public override BillItem[] items
        {
            get
            {
                return waterBillItems;
            }
        }
        public override string groupText
        {
            get
            {
                return "Water Bill for " + period.name + " contract No:" + subscription.contractNo;
            }
        }

        public double getBillItemAmountByType(int p)
        {
            foreach (BillItem item in itemsLessExemption)
            {
                if (item.itemTypeID == p)
                    return item.price;
            }
            return 0;
        }
    }
    [Serializable]
    [INTAPS.RDBMS.TypeID(4)]
    public class PenalityBillDocument : PeriodicBill
    {
        public int[] unpaidBills;
        public BillItem[] theItems;
        public override BillItem[] items
        {
            get
            {
                return theItems;
            }
        }
    }

    [Serializable]
    [INTAPS.RDBMS.TypeID(6)]
    public class CreditBillDocument : PeriodicBill
    {
        public double amount;
        public int creditSchemeID=-1;
        public override BillItem[] items
        {
            get
            {
                return new BillItem[]{
                new BillItem(){description=string.IsNullOrEmpty(this.ShortDescription)?
                    "Credit settlement for "+period.name:this.ShortDescription, hasUnitPrice=false,price=this.amount,accounting=BillItemAccounting.Advance
                
                }
            };
            }
        }
    }

    [Serializable]
    [INTAPS.RDBMS.TypeID(5)]
    public class InterestBillDocument : PenalityBillDocument
    {
        
    }
    [SingleTableObject("CustomerBill")]
    [Serializable]
    [INTAPS.RDBMS.TypeID(8)]
    public class CustomerBillRecord
    {
        [IDField]
        public int id = -1;
        [DataField]
        public DateTime billDate = DateTime.Now;
        [DataField]
        public int billDocumentTypeID = -1;
        [DataField]
        public int customerID = -1;
        [DataField]
        public int connectionID = -1;
        [DataField]
        public int periodID = -1;
        [DataField]
        public DateTime postDate = DateTime.Now;
        [DataField]
        public int paymentDocumentID = -1;
        [DataField]
        public DateTime paymentDate = DateTime.Now;
        [DataField]
        public bool paymentDiffered = false;
        [DataField]
        public bool draft = true;
        public bool isPayedOrDiffered { get { return paymentDocumentID != -1 || paymentDiffered; } }
    }

    [Serializable]
    [INTAPS.RDBMS.TypeID(7)]
    [NoBudget]
    public class CustomerPaymentReceipt:AccountDocument
    {
        [DocumentTypedReferenceField("BSN","CRV")]
        public DocumentTypedReference receiptNumber;
        public bool offline = false;
        public bool mobile = false;
        public bool notifyCustomer = false;
        public CollectionMethod getCollectionMethod()
        {
            CollectionMethod cm = (offline ? CollectionMethod.Offline : CollectionMethod.Online) |
                (mobile ? CollectionMethod.Mobile : CollectionMethod.None);
            return cm;
        }
        public int assetAccountID;
        public BIZNET.iERP.PaymentInstrumentItem[] paymentInstruments = new BIZNET.iERP.PaymentInstrumentItem[0];
        public int[] bankDepsitDocuments=new int[0];
        
        public Subscriber customer;
        public int[] settledBills;
        public int billBatchID = -1;
        public bool summerizeTransaction = false;

        
        [System.Xml.Serialization.XmlIgnore]    
        public List<BillItem> billItems=null;

        public int[] offlineBills;
        public double totalAmount
        {
            get
            {
                if(billItems==null)
                {
                    return -1;
                }
                double ret = 0;
                foreach (BillItem bi in billItems)
                {
                    ret += bi.netPayment;
                }
                return ret;
            }
        }

        public double totalInstrument
        {
            get
            {
                double ret = 0;
                foreach (BIZNET.iERP.PaymentInstrumentItem bi in paymentInstruments)
                {
                    ret += bi.amount;
                }
                return ret;
            }
        }
    }
    [Serializable]
    public class CustomerBankDepositDocument:AccountDocument
    {
        [DocumentTypedReferenceFieldAttribute("CRV", "JV", "CHKPV", "CPV", "PCPV", "PV")]
        public DocumentTypedReference voucher = null;        
        public Subscriber customer;
        public int bankAccountID;
        public double amount;
        public string reference;
    }
    [Serializable]
    [TypeID(10)]
    public class UnclassifiedPayment : CustomerBillDocument
    {
        public BillItem[] billItems;


        public override BillItem[] items
        {
            get { return billItems; }
        }
    }


    [Serializable]
    [TypeID(10)]
    public class MobilePaymentServiceCharge: CustomerBillDocument
    {
        public BillItem[] billItems;


        public override BillItem[] items
        {
            get { return billItems; }
        }
    }
}
