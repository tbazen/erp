
using INTAPS.Accounting;
using INTAPS.UI;
using System;
namespace INTAPS.SubscriberManagment
{

    [Serializable]
    public class BillGenerationStatus
    {
        public BatchError errors;
        public double progress;
        public BatchJobStatus status;
    }
}

