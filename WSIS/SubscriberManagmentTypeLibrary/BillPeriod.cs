
    using INTAPS.Accounting;
    using INTAPS.RDBMS;
    using System;
namespace INTAPS.SubscriberManagment
{

    [Serializable, SingleTableObject(orderBy="fromDate"), STOInclude(typeof(AccountingPeriod))]
    public class BillPeriod : AccountingPeriod
    {
    }
}

