using INTAPS.RDBMS;
using System;
namespace INTAPS.SubscriberManagment
{
    public enum BillingCycleStatus
    {
        Open=1,
        Closed=2
    }
    [Serializable]
    [SingleTableObject]
    public class BillingCycle
    {
        [IDField]
        public int periodID;
        [DataField]
        public BillingCycleStatus status = BillingCycleStatus.Closed;
        [DataField]
        public long collectionStartTicks = -1;
        [DataField]
        public long collectionEndTicks = -1;
        public DateTime collectionStartTime
        {
            get
            {
                return new DateTime(collectionStartTicks);
            }
            set
            {
                this.collectionStartTicks = value.Ticks;
            }
        }

        public DateTime collectionEndTime
        {
            get
            {
                return new DateTime(collectionEndTicks);
            }
            set
            {
                this.collectionEndTicks = value.Ticks;
            }
        }
        
    }
}
