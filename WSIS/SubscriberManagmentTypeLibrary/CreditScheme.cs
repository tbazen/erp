﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;

namespace INTAPS.SubscriberManagment
{
    [SingleTableObject]
    [Serializable]
    public class CreditScheme
    {
        [IDField]
        public int id;
        [DataField]
        public int customerID;
        [DataField]
        public double creditedAmount;
        [DataField]
        public double paymentAmount;
        [DataField]
        public DateTime issueDate;
        [DataField]
        public int startPeriodID=-1;
        [DataField]
        public bool active = true;
        
    }
}
