namespace INTAPS.SubscriberManagment
{
    using System;

    public enum MaterialOperationalStatus
    {
        Working,
        NotWorking,
        Defective
    }
}

