﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.SubscriberManagment
{
    [Serializable]
    public class MeterData
    {
        public string itemCode;
        public string serialNo;
        public string modelNo;
        public double initialMeterReading;
        public MaterialOperationalStatus opStatus;
        public string detailDesc
        {
            get
            {
                return modelNo;
            }
        }
        public MeterData()
        {
        }
        public MeterData(string itemCode, string serialNo, string modelNo, MaterialOperationalStatus opStatus)
        {
            this.itemCode = itemCode;
            this.serialNo = serialNo;
            this.modelNo = modelNo;
            this.opStatus = opStatus;
        }
    }
}
