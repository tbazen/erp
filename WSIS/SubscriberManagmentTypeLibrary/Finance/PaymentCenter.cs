
using INTAPS.RDBMS;
using System;
namespace INTAPS.SubscriberManagment
{
    [Flags]
    public enum CollectionMethod
    {
        None=0,
        Online=1,
        Offline=2,
        Mobile=4
        
    }

    [Serializable, SingleTableObject]
    public class PaymentCenter
    {
        [IDField]
        public int id;
        [DataField(false)]
        public int casheir;
        [DataField(false)]
        public int casherAccountID;
        [DataField(false)]
        public string centerName;
        [DataField]
        public int summaryAccountID;
        [DataField(false)]
        public string userName;
        [DataField]
        public int storeCostCenterID=-1;
        [DataField]
        public CollectionMethod collectionMethod = CollectionMethod.Online;
        public PaymentCenterSerialBatch[] serialBatches = new PaymentCenterSerialBatch[0];
        public static string getCollectionMethodText(CollectionMethod method)
        {
            String str = null;
            if ((method & CollectionMethod.Online) == CollectionMethod.Online)
                str = INTAPS.StringExtensions.AppendOperand(str, ", ", "Online");
            if ((method & CollectionMethod.Offline) == CollectionMethod.Offline)
                str = INTAPS.StringExtensions.AppendOperand(str, ", ", "Offline");
            if ((method & CollectionMethod.Mobile) == CollectionMethod.Mobile)
                str = INTAPS.StringExtensions.AppendOperand(str, ", ", "Mobile");
            return str;
        }
        public int GetBatchID(int doctTypeID)
        {
            foreach (PaymentCenterSerialBatch batch in this.serialBatches)
            {
                if (batch.documentTypeID == doctTypeID)
                {
                    return batch.batchID;
                }
            }
            return -1;
        }

        public override string ToString()
        {
            return this.centerName;
        }
    }
}

