using INTAPS.RDBMS;
using System;
namespace INTAPS.SubscriberManagment
{
    [Serializable, SingleTableObject(orderBy = "paymentCenterID,documentTypeID,batchID")]
    public class PaymentCenterSerialBatch
    {
        [IDField]
        public int paymentCenterID;
        [IDField]
        public int documentTypeID;
        [IDField]
        public int batchID;

        public override bool Equals(object obj)
        {
            PaymentCenterSerialBatch batch = obj as PaymentCenterSerialBatch;
            if (batch == null)
            {
                return false;
            }
            return (((batch.batchID == this.batchID) && (batch.documentTypeID == this.documentTypeID)) && (batch.paymentCenterID == this.paymentCenterID));
        }

        public override int GetHashCode()
        {
            return this.batchID;
        }
    }
}

