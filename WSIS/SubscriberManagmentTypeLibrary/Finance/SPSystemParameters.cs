
    using System;
namespace INTAPS.SubscriberManagment
{

    public class SPSystemParameters
    {
        public int centerAccountID;
        public string centerName;

        public int batchFrom;
        public int batchID;
        public int batchTo;
        
        
        public string password;
        public string userName;
        public string voidPassword;
        public string cashInstrumentCode = "021001";

        public string receiptTitle;
    }
}

