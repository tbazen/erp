﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INTAPS.SubscriberManagment
{
    public class EPSPayment
    {
        public string paymentID;
        public string paymentDate;
        public int customerID;
        public int[] billIDs;
        public int bankAccountID = -1;
        public string bankReference;
        public double amount;
    }
    public class EPSBill
    {
        public int      billID;
        public string   billDate;
        public double   amount;
        public string   remark;
        public int      customerID;
        public string   customerName;
        public string   customerCode;
        public string   customerPhoneNo;
        public string   status;
        public int      paymentCenterID = -1;
        public EPSBill()
        {

        }
        public EPSBill(PeriodicBill waterBillDocument,string status,int paymentCenterID)
        {
            billID = waterBillDocument.AccountDocumentID;
            billDate = waterBillDocument.DocumentDate.ToString();
            amount = waterBillDocument.totalLessDeposit;
            remark = waterBillDocument.ShortDescription;
            customerID = waterBillDocument.customer.id;
            customerName = waterBillDocument.customer.name;
            customerPhoneNo = waterBillDocument.customer.phoneNo;
            customerCode = waterBillDocument.customer.customerCode;
            this.status = status;
            this.paymentCenterID = paymentCenterID;
        }
    }
    public interface IEPSInterface
    {
        void addBill(EPSBill bill);
        void replaceBill(EPSBill bill);
        void deleteBill(int billID);
        int[] getBillIDs();
        EPSBill getBillDetail(int billID);
    }
}
