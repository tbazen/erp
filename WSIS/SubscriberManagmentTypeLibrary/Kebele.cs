
    using INTAPS.RDBMS;
    using System;
namespace INTAPS.SubscriberManagment
{

    [Serializable, SingleTableObject]
    public class Kebele
    {
        [IDField]
        public int id;
        [DataField]
        public string name;

        public override string ToString()
        {
            return this.name;
        }
    }
}

