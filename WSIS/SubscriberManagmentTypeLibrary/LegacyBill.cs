﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;

namespace INTAPS.SubscriberManagment
{
    enum VerificationType
    {
        Verify,
        VerifyAndUpdate,
        CreateReadging,
        CreateSubscriptionAndReading
    }
    [Serializable]
    public class VerificationEntry
    {
        public LegacyBill bill=null;
        public Subscription subsc=null;
    }
   
    [Serializable, SingleTableObject]
    public class LegacyBillSet
    {
        [IDField]
        public int id=-1;
        [DataField]
        public string description;
        [DataField]
        public string encoderUserID;
        [DataField]
        public bool verfied = false;
        [DataField]
        public double recordTotal=9;
    }
   
    [Serializable, SingleTableObject(orderBy="setOrderN")]
    
    public class LegacyBill
    {
        [IDField]
        public int id=-1;
        [DataField]
        public int customerID;
        [DataField]
        public int periodID;
        [DataField]
        public double reading;
        [DataField]
        public double consumption;
        [DataField]
        public int setID;
        [DataField]
        public int setOrderN;
        [DataField]
        public string reference;
        [DataField]
        public string remark;
        public LegacyBillItem[] items;

        public double getItemAmount(int itemID)
        {
            foreach (LegacyBillItem i in items)
                if (i.itemID == itemID)
                    return i.amount;
            return 0;
        }
        public double billTotal
        {
            get
            {
                double ret = 0;
                foreach (LegacyBillItem i in items)
                    ret += i.amount;
                return ret;
            }
        }
    }
    [Serializable, SingleTableObject(orderBy = "legacyBillID,itemID")]
    public class LegacyBillItem
    {
        [IDField]
        public int legacyBillID;
        [IDField]
        public int itemID;
        [DataField]
        public double amount;
    }
    [Serializable, SingleTableObject(orderBy = "id")]
    public class LegacyBillItemDefination
    {
        [IDField]
        public int id;
        [DataField]
        public string description;
    }
}
