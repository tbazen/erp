﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.SubscriberManagment
{
    public enum MeterMarkerSymbol
    {
        Default=0,
        ReaderLocation,
        Read,
        Unread,
        SelfMeter,
        OtherMeter,
        LocatedMeter,
        PathRead,
        PathUnread,
        ProblemReading1,
        ProblemReading2,
        ProblemReading3,
        ProblemReading4,
        ProblemReading5,
        ReadStart,
        ReadEnd
    }
    [Serializable]
    public class MeterMarker
    {
        public int connectID;
        public string connectionCode;
        public string label;
        public MeterMarkerSymbol symbol;
        public double lat,lng;
        public int color;
    }
    [Serializable]
    public class MapBounds
    {
        public double swLat,swLng;
        public double neLat,neLng;
    }
    [Serializable]
    public class MeterMap
    {
        public string layerName;
        public MeterMarker[] markers;
        public MapBounds calculateBounds()
        {
            MapBounds ret = null;
            foreach (MeterMarker m in markers)
            {
                if (ret == null)
                {
                    ret = new MapBounds();
                    ret.swLat = ret.neLat = m.lat;
                    ret.swLng = ret.neLng = m.lng;
                }
                else
                {
                    if (m.lat < ret.swLat)
                        ret.swLat = m.lat;
                    else if (m.lat > ret.neLat)
                        ret.neLat = m.lat;
                    if (m.lng < ret.swLng)
                        ret.swLng = m.lng;
                    else if (m.lng > ret.neLng)
                        ret.neLng = m.lng;
                }
            }
            return ret;
        }
    }
    [Serializable]
    public class ReadingPath : MeterMap
    {
    }
}
