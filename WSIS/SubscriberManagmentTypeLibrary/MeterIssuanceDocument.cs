
using INTAPS.Accounting;
using System;
namespace INTAPS.SubscriberManagment
{

    [Serializable]
    public class MeterIssuanceDocument : AccountDocument
    {
        public MeterData oldWaterMeter;
        public MeterData newWaterMeter;
        public Subscription subscription;
        public int subscriptionID;
    }
}

