
    using INTAPS.Payroll;
    using INTAPS.RDBMS;
    using System;
    using System.Xml.Serialization;
namespace INTAPS.SubscriberManagment
{

    [Serializable, SingleTableObject, XmlInclude(typeof(Employee))]
    public class MeterReaderEmployee
    {
        [IDField]
        public int employeeID;
        [IDField]
        public int periodID;
        public Employee emp;

        public override string ToString()
        {
            if (this.emp == null)
            {
                return ("System employee identifier :" + this.employeeID.ToString());
            }
            return this.emp.EmployeeNameID;
        }
    }
}

