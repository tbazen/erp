using INTAPS.RDBMS;
using System;
namespace INTAPS.SubscriberManagment
{

    [Serializable, SingleTableObject(orderBy="readingSheetID,orderN")]
    public class MeterReading
    {
        [DataField]
        public int averageMonths;
        [DataField(false)]
        public double consumption;
        [DataField(false)]
        public int orderN;
        [IDField]
        public int periodID;
        [DataField]
        public int readerEmployee;
        [DataField(false)]
        public double reading;
        [DataField(false)]
        public DateTime readingDate;
        [DataField]
        public int readingSheetID;
        [DataField(false)]
        public MeterReadingType readingType;
        [IDField]
        public int subscriptionID;
    }
    
}