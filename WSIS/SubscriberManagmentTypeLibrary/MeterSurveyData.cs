

using System;
namespace INTAPS.SubscriberManagment
{

    [Serializable]
    public class MeterSurveyData
    {
        public int blockID;
        public ConnectionType connectionType;
        public string contractNo;
        public DataStatus dataStatus;
        public DateTime date;
        public string englishName;
        public MaterialOperationalStatus meterCondition;
        public string meterModel;
        public MeterPositioning meterPosition;
        public string meterSerial;
        public string meterType;
        public string ownerName;
        public double reading;
        public string remark;
        public SubscriberType subscriberType;
        public SupplyCondition supplyCondition;
        public string phoneNo;
        public string emial;
    }
}

