﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.RDBMS;

namespace INTAPS.SubscriberManagment.Client
{
    public class SubscriberIndexer : SimpleObjectIndexer<Subscriber,Subscriber>,ITypeIndexer
    {
        
        public override string getObjectID(Subscriber obj)
        {
            return obj.id.ToString();
        }

        public override string mainDB
        {
            get 
            {
                return "SalePoint";
            }
        }
    }
    public class SubscriptionIndexer : SimpleObjectIndexer<Subscription, Subscription>, ITypeIndexer
    {
        public override string getObjectID(Subscription obj)
        {
            return obj.id.ToString();
        }

        public override string mainDB
        {
            get { return "SalePoint"; }
        }
    }
    public class BillItemIndexer : SimpleObjectIndexer<BillItem, BillItem>, ITypeIndexer
    {
        public override string getObjectID(BillItem obj)
        {
            
            return obj.customerBillID + "~" + obj.itemTypeID;
        }

        public override string mainDB
        {
            get { return "SalePoint"; }
        }
    }
    public class CustomerBillRecordIndexer : SimpleObjectIndexer<CustomerBillRecord, CustomerBillRecord>, ITypeIndexer
    {
        public override string getObjectID(CustomerBillRecord obj)
        {
            return obj.id.ToString();
        }

        public override string mainDB
        {
            get {
                return "SalePoint";
            }
        }
    }
}
