﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;
using System.Xml.Serialization;

namespace INTAPS.SubscriberManagment
{
    [Serializable]
    public class SubscriptionPeriod
    {
        public int subscriptionID;
        public int periodID;
        public SubscriptionPeriod(int s, int p)
        {
            this.subscriptionID = s;
            this.periodID = p;
        }
    }

    public enum ORActionType
    {
        setBlock,
        deleteBlock,
        setReading,
        deleteReading,
        setBill,
        deleteBill,
        createPeriod,
        setCurrentPeriod,
    }
    [Serializable]
    [XmlInclude(typeof(BWFMeterReading))]
    [XmlInclude(typeof(BWFReadingBlock))]
    [XmlInclude(typeof(SubscriptionPeriod))]
    [SingleTableObject]
    public class ORAction
    {
        [IDField]
        public int id;
        [DataField]
        public DateTime actionTime;
        [DataField]
        public ORActionType type;
        [XMLField]
        public object data;
    }
    [Serializable]
    public class ORActionPackage : INTAPS.ClientServer.SignedData<ORAction[]>
    {
        public ORActionPackage(ORAction[] data)
            : base(data)
        {
        }
    }
}
