using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;
using System.Xml.Serialization;
using INTAPS.ClientServer;
namespace INTAPS.SubscriberManagment
{
    [Serializable]
    public class PaymentCenterMessageList
    {
        public int paymentCenterID;
        public MessageList list;
        public PaymentCenterMessageList(int paymentCenterID, MessageList list)
        {
            this.paymentCenterID = paymentCenterID;
            this.list = list;
        }
    }
}
