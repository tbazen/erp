using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;
using System.Xml.Serialization;
using INTAPS.ClientServer;
namespace INTAPS.SubscriberManagment
{
    [Serializable]
    public class PaymentReceivedMessage
    {
        public int paymentCenterID;
        public CustomerPaymentReceipt payment;
        public double paidAmount;
        public double[] eachAmount;
    }
}
