using INTAPS.RDBMS;
using System;
using INTAPS.ClientServer;
namespace INTAPS.SubscriberManagment
{

    [Serializable]
    [TypeID(12)]
    public class PrePaymentBill : CustomerBillDocument
    {
        public Subscription connection;
        public int useIncomeAccountID;
        public int rentIncomeAccountID;
        public double cubicMeters;
        public double paymentAmount;
        public BillPeriod[] rentPeriods;
        public double[] rents;
        public override BillItem[] items
        {
            get
            {
                BillItem[] ret = new BillItem[rentPeriods.Length + 1];
                BillItem bi = new BillItem();
                bi.itemTypeID = 1;
                bi.hasUnitPrice = false;
                bi.description = "Prepayment for contract no " + connection.contractNo+" "+cubicMeters+" cubic meters";
                bi.price = paymentAmount;
                bi.incomeAccountID = useIncomeAccountID;
                bi.accounting = BillItemAccounting.Invoice;
                ret[0] = bi;
                for (int i = 0; i < rentPeriods.Length; i++)
                {
                    bi = new BillItem();
                    bi.itemTypeID = 2 + i;
                    bi.hasUnitPrice = false;
                    bi.description = "Rent for contract no " + connection.contractNo + " " + rentPeriods[i].name;
                    bi.price = this.rents[i];
                    bi.incomeAccountID = useIncomeAccountID;
                    bi.accounting = BillItemAccounting.Invoice;
                    ret[1 + i] = bi;
                }
                return ret;
            }
        }
    }
}

