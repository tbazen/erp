
using INTAPS.RDBMS;
using System;
namespace INTAPS.SubscriberManagment
{
    [Serializable]
    [SingleTableObject]
    public class ProductionPoint
    {
        [IDField]
        public int id;
        [DataField]
        public string name;
        [DataField]
        public DateTime Date;
        [DataField]
        public double latitude = 0;
        [DataField]
        public double longitude = 0;
        [DataField]
        public string address;
        [DataField]
        public int kebele;
        [DataField]
        public ProductionPointType type;
        [DataField]
        public double capacity; //measured in liter/second
        [DataField]
        public string remark;
    }
    public enum ProductionPointType
    {
        Well,
        Dam,
        Others
    }
    [Serializable]
    [SingleTableObject]
    public class ProductionPointReading
    {
        [IDField]
        public int id;
        [DataField]
        public int productionPointID;
        [DataField]
        public double reading = 0.0;
        [DataField]
        public double consumption = 0.0;
        [DataField]
        public DateTime readingDate;
        [DataField]
        public MeterReadingType readingType = MeterReadingType.Normal;
    }
    public class ProductionReadingData
    {
        public DateTime readingDate;
        public double previousReading;
        public double reading;
        public double consumption;
        public MeterReadingType readingType;
    }
}

