
    using System;
namespace INTAPS.SubscriberManagment
{

    [Serializable]
    public class RateMatrix
    {
        public double[] ranges = new double[0];
        public double[] rates = new double[0];
        public double toInf;

        public double Calculate(double val)
        {
            int index = 0;
            double num2 = 0.0;
            double num3 = 0.0;
            index = 0;
            while (index < this.ranges.Length)
            {
                if (val > this.ranges[index])
                {
                    num2 += (this.ranges[index] - num3) * this.rates[index];
                }
                else
                {
                    num2 += (val - num3) * this.rates[index];
                    break;
                }
                num3 = this.ranges[index];
                index++;
            }
            if (index == this.ranges.Length)
            {
                num2 += (val - num3) * this.toInf;
            }
            return num2;
        }
    }
}

