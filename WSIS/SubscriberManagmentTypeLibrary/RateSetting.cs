
using INTAPS.RDBMS;
using System;
using System.Xml.Serialization;
namespace INTAPS.SubscriberManagment
{

    [Serializable, LoadFromSetting, XmlInclude(typeof(SingeRateSetting))]
    public class RateSetting
    {
        public SingeRateSetting[] setting;
    }
}

