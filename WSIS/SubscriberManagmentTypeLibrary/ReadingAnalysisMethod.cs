﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.RDBMS;

namespace INTAPS.SubscriberManagment
{
    [Serializable,LoadFromSetting]
    public class ReadingAnalysisMethod
    {
        public int nPeriod;
        public double minLat=-90;
        public double maxlat = 90;
        public double minLng = -180;
        public double maxLng = 180;
    }
    [Serializable,SingleTableObject]
    public class ReadingAnalysisHeader
    {
        [IDField]
        public int periodID;
        [DataField]
        public long ticks;
        [XMLField]
        public ReadingAnalysisMethod method;
        [DataField]
        public int nReading=0;
        [DataField]
        public double totalVolume=0;
        [DataField]
        public int nLocation=0;
        [DataField]
        public double minLat=0;
        [DataField]
        public double minLng=0;
        [DataField]
        public double maxLat=0;
        [DataField]
        public double maxLng=0;        
    }
    
    [Serializable, SingleTableObject]
    public class ReadingAnalysisRecord
    {
        [IDField]
        public int periodID;
        [IDField]
        public int connectionID;
        [DataField]
        public int nReading=0;
        [DataField]
        public double averageConsumption=0;
        [DataField]
        public int nLocation=0;
        [DataField]
        public double medianX=0;
        [DataField]
        public double medianY=0;
        [DataField]
        public double confidence=0;
        [XMLField]
        public BWFMeterReading[] readingSamples;
        [XMLField]
        public BWFMeterReading[] locationSamples;
    }

}
