
    using INTAPS.Payroll;
    using INTAPS.RDBMS;
    using System;
    using System.Xml.Serialization;
namespace INTAPS.SubscriberManagment
{

    [Serializable, XmlInclude(typeof(Employee)), SingleTableObject]
    public class ReadingEntryClerk
    {
        public Employee emp;
        [IDField]
        public int employeeID;
        [DataField]
        public string userID;

        public override string ToString()
        {
            if (this.emp == null)
            {
                return ("System employee identifier :" + this.employeeID.ToString());
            }
            return this.emp.EmployeeNameID;
        }
    }
}

