﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;

namespace INTAPS.SubscriberManagment
{
    public enum ReadingCycleStatus
    {
        Open,
        Closed
    }
    [Serializable]
    [SingleTableObject]
    public class ReadingCycle
    {
        [IDField]
        public int periodID;
        [DataField]
        public ReadingCycleStatus status;
        [DataField]
        public DateTime openTime;
        [DataField]
        public DateTime closeTime;
        [DataField]
        public byte[] hashCode;
        [DataField]
        public int syncNo = -1;
    }
    [Serializable]
    [SingleTableObject(orderBy="orderN")]
    public class ReadingSheetRow
    {
        public const int STATUS_UNREAD = 0;
        public const int STATUS_READ = 1;
        public const int STATUS_UPLOADED = 2;
        public const int STATUS_READ_UPLOADED = 3;
        [IDField]
        public int periodID;
        [IDField]
        public int connectionID = -1;
        [DataField]
        public int orderN = 0;
        [DataField]
        public int readerID;
        [DataField]
        public String contractNo;
        [DataField]
        public String customerName;
        [DataField]
        public String meterNo;
        [DataField]
        public String meterType;

        [DataField]
        public int status = STATUS_UNREAD;
        [DataField]
        public double reading = 0.0;
        [DataField]
        public int extraInfo = (int)ReadingExtraInfo.None;
        [DataField]
        public int problem = (int)MeterReadingProblem.NoProblem;
        [DataField]
        public String remark = "";
        [DataField]
        public DateTime time = DateTime.Now;
        [DataField]
        public double lat = 0;
        [DataField]
        public double lng = 0;
        [DataField]
        public double readingLat = 0;
        [DataField]
        public double readingLng = 0;
        [DataField]
        public int connectionStatus = (int)SubscriptionStatus.Active;
        [DataField]
        public double minAllowed = 0;
        [DataField]
        public double maxAllowed = 0;
        [DataField]
        public string overdueBill="";
        [DataField]
        public string customField1="";
        [DataField]
        public string customField2="";
        [DataField]
        public string customField3="";
        [DataField]
        public string customField4="";
        public int actualReaderID = -1;
        public void setCustomFields(string[] values)
        {
            if (values.Length > 0)
                customField1 = values[0];
            else if (values.Length > 1)
                customField2 = values[1];
            else if (values.Length > 2)
                customField3 = values[2];
            else if (values.Length > 3)
                customField4 = values[3];
        }
    }
    [SingleTableObject("ReadingSheetRow")]
    public class ReadingSheetRecord
    {
        public const int PAYMENT_STATUS_ACTIVE = 0;
        public const int PAYMENT_STATUS_ACTIVE_PAID = 1;
        public const int PAYMENT_STATUS_ACTIVE_PAID_CONFIRMED = 2;
        public const int PAYMENT_STATUS_ACTIVE_INVALIDATED = 3;

        public class MRBillItem
        {
            public int billID;
            public String description;
            public double amount;
        }
        public class MRBill
        {
            public List<MRBillItem> items=new List<MRBillItem>();

            public double totalAmount()
            {
                double ret = 0;
                if (items == null)
                    return ret;
                foreach (MRBillItem item in items)
                    ret += item.amount;
                return ret;
            }
        }
        //record index
        [IDField]
        public int connectionID;
        [DataField]
        public int orderN;
        [DataField]
        public int periodID;
        //connection info
        [DataField]
        public String contractNo;
        [DataField]
        public String customerName;
        [DataField]
        public String meterNo;
        [DataField]
        public String meterType;
        [DataField]
        public String meterTypeID;
        [DataField]
        public double lat;
        [DataField]
        public double lng;
        [DataField]
        public int connectionStatus;
        [DataField]
        public String phoneNo;
        [DataField]
        public int customerType;
        [DataField]
        public int connectionType;


        //setting
        [DataField]
        public int readerID;
        [DataField("minAllowed")]
        public double minReadAllowed;
        [DataField("maxAllowed")]
        public double maxReadAllowed;

        //reading data
        [DataField]
        public int status;
        public int actualReaderID;
        [DataField]
        public double reading;
        [DataField]
        public int extraInfo;
        [DataField]
        public int problem;
        [DataField]
        public String remark;
        [DataField("time")]
        public DateTime readTime_date;
        public long readTime;
        [DataField]
        public double readingLat;
        [DataField]
        public double readingLng;

        //billing
        [DataField(allowNull = true)]
        public bool canPay;
        [DataField(fieldName = "bill", allowNull = true)]
        public String bill_json;
        public MRBill bill;
        [DataField("overdueBill")]
        public String billText;

        [DataField]
        public string customField1 = "";
        [DataField]
        public string customField2 = "";
        [DataField]
        public string customField3 = "";
        [DataField]
        public string customField4 = "";
        [DataField]
        public string rowKey;
        public string[] customFields = null;
        public void setCustomFields(string[] values)
        {
            customFields = values;

            if (values.Length > 0)
                customField1 = values[0];
            else if (values.Length > 1)
                customField2 = values[1];
            else if (values.Length > 2)
                customField3 = values[2];
            else if (values.Length > 3)
                customField4 = values[3];
        }
        [DataField("customFields")]
        public string customFields_json;

        public double billTotal;

        public bool validPhoneNo()
        {

            if (phoneNo == null || phoneNo.Length < 9)
                return false;
            int j = -1;
            for (int i = phoneNo.Length - 1; i >= 0; i--)
            {
                if (char.IsDigit(phoneNo[i]))
                {
                    j++;
                    if (j == 8)
                        return phoneNo[i] == '9';
                }
            }
            return false;
        }
        static DateTime java_tick_base = new DateTime(1970, 1, 1);

        public static long toJavaTicks(DateTime time)
        {
            return (long)Math.Round(time.AddHours(-3).Subtract(java_tick_base).TotalMilliseconds);
        }
        public static DateTime fromJavaTicks(long ticks)
        {
            return java_tick_base.AddMilliseconds(ticks).AddHours(3);
        }
    }

    [Serializable]
    public class DataUpdate
    {
        public class GEOLocation
{
    public const double INVALID_ALT=-100000;
    public const double INVALID_LAT=-100000;
    public const double INVALID_LNG=-100000;
    public  double lat;
    public  double lng;
    public  double alt;
    public GEOLocation()
    {

        this.lat=INVALID_ALT;
        this.lng=INVALID_LAT;
        this.alt=INVALID_LNG;

    }
    public GEOLocation(double lat,double lng)
    {
        this.lat=lat;
        this.lng=lng;
        this.alt=INVALID_ALT;
    }

    
    public override String ToString()
    {
        return lat+", "+lng;
    }
    const double MAP_R = 6371;
    public static double sphericalDistance(double lat1, double lng1, double lat2, double lng2)
    {
        
        double a1 = lat1 * Math.PI / 180;
        double a2 = lat2 * Math.PI / 180;
        double delLat = (lat2 - lat1) * Math.PI / 180;
        double delLong = (lng2 - lng1) * Math.PI / 180;
        double a = Math.Sin(delLat / 1) * Math.Sin(delLat / 2)
                + Math.Cos(a1) * Math.Cos(a2) * Math.Sin(delLong / 2) * Math.Sin(delLong / 2);
        double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
        double d = MAP_R * c;
        return d*1000;
    }
}

        public enum DataClass
        {
            CustomerProfile=1,
        }
        [SingleTableObject]
        public class DataUpdateRequest
        {
            [IDField]
            public int periodID;
            [IDField]
            public int connectionID;
            [IDField]
            public String userID;
            [DataField]
            public DataClass dataClass;
            [DataField]
            public String updateData;
            [DataField]
            public RequestStatus status;
        }
        public enum RequestStatus
        {
            Requested=0,
            Accepted=1,
            Rejected=2,
        }
        [Serializable]
     public class DataUpdateField
        {
            public String fieldName;
            public Object value;
            public DataUpdateField(
                    String fieldName,
                    Object value)
            {
                this.fieldName = fieldName;
                this.value = value;
            }
        }
        public int connectionID;
        public DataUpdateField[] updatedFields;
    }
}
