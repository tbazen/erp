﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INTAPS.SubscriberManagment
{
    public enum CustomerSMSSendType
    {
        Bills,
        Disconnection,
        General,
        UnreadNotification
    }
    [Serializable]
    public class SMSLicenseInformation
    {
        public bool hasLicense()
        {
            return allowBillSend || allowReceive;
        }
        public bool allowReceive;
        public bool allowBillSend;
    }
    [Serializable]
    public class CustomerSMSSpecItem
    {
        public int singleCustomer;
        public int[] kebeles;
        public int[] customerTypes;
    }
    [Serializable]
    public class CustomerSMSSendParameter
    {
        public const int MAX_NAME_LENGTH = 23;
        public CustomerSMSSendType sendType;
        public string messageFormat;
        public SubscriberType[] types;
        public int[] kebeles;
        public int singleCustomer = -1;
        public string passCode="";
        public string phoneNo=null;
        public CustomerSMS[] manualSMS = null;

        public static string truncateName(string name)
        {
            if (name.Length > MAX_NAME_LENGTH)
                return name.Substring(0, MAX_NAME_LENGTH)+"..";
            return name;
        }
    }
    [Serializable]
    public class CustomerSMS
    {
        public enum CustmerSMSStatus
        {
            Sending=0,
            Sent=1,
            Delivered=2,
            Failed=3
        }
        public int smsID;
        public int customerID;
        public string customerCode;
        public string name;
        public string phoneNo;
        public string message;
        public bool smsOut = true;
        public CustmerSMSStatus status = CustmerSMSStatus.Sending;
        public long ticks;
        public DateTime time
        {
            get
            {
                return new DateTime(ticks);
            }
        }
    }

}
