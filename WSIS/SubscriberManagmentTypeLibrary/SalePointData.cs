
    using INTAPS.Accounting;
    using System;
namespace INTAPS.SubscriberManagment
{

    [Serializable]
    public class SalePointData
    {
        public DateTime dataDate;
        public int lastUpdateID;
        public int paymentCenterID;
        public CustomerPaymentReceipt[] receipts = new CustomerPaymentReceipt[0];
        public UsedSerial[] voidSN = new UsedSerial[0];

        public string KeyString
        {
            get
            {
                return this.dataDate.ToString("yyyyMMdd");
            }
        }
    }
}

