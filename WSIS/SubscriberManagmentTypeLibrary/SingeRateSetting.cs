
using INTAPS.Accounting;
using System;
using System.Xml.Serialization;
namespace INTAPS.SubscriberManagment
{

    [Serializable, XmlInclude(typeof(RateMatrix))]
    public class SingeRateSetting
    {
        public string additionalFormula;
        public bool allKebeles = true;
        public int[] kebele = new int[0];
        public RateMatrix matrixes = new RateMatrix();
        public string[] meterType = new string[0];
        public string name;
        public string penalityFormula;
        public PayPeriodRange period = new PayPeriodRange(-1, -1);
        public double[] rent = new double[0];
        public SubscriptionType[] subscType = new SubscriptionType[0];
        public SubscriberType[] subType = new SubscriberType[0];

        public int mainAccountID = -1;
        //public int mainReceivableID = -1;

        public int penalityAccountID = -1;
        //public int penalityReceivableAccountID = -1;

        public int additionalAccountID = -1;
        //public int additionalReceivableAccountID = -1;

        public int rentAccountID = -1;
        //public int rentReceivableAccountID = -1;
        public double Evaluate(double p)
        {
            return this.matrixes.Calculate(p);
        }

        public double EvaluateRent(string meterItemCode)
        {
            if (this.meterType != null)
            {
                for (int i = 0; i < this.meterType.Length; i++)
                {
                    if (this.meterType[i].Equals(meterItemCode))
                    {
                        return this.rent[i];
                    }
                }
            }
            return 0.0;
        }
    }
}

