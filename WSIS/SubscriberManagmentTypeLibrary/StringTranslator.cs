﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;

namespace INTAPS.SubscriberManagment
{
    [Serializable]
    [SingleTableObject]
    public class TranslatorEntry
    {
        [IDField]
        public string str;
        [DataField]
        public string translation;
        public TranslatorEntry()
        {
        }
        public TranslatorEntry(string s, string t)
        {
            this.str = s;
            this.translation = t;
        }
    }

    public class StringTranslator
    {
        public List<TranslatorEntry> newTranslations = new List<TranslatorEntry>();
        Dictionary<string, string> table = new Dictionary<string, string>();

        public StringTranslator(TranslatorEntry[] translatorEntry)
        {
            foreach (TranslatorEntry e in translatorEntry)
            {
                table.Add(e.str.ToUpper(), e.translation);
            }
        }
        public string translate(string str)
        {
            if (table.ContainsKey(str.ToUpper()))
                return table[str.ToUpper()];
            newTranslations.Add(new TranslatorEntry(str, str));
            return str;
        }
    }
}
