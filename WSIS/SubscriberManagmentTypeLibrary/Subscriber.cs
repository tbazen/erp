using INTAPS.ClientServer;
using INTAPS.RDBMS;
using System;
namespace INTAPS.SubscriberManagment
{
    public enum CustomerStatus
    {
        None=0,
        Active = 1,
        Inactive = 2,
    }
    [Serializable, SingleTableObject]
    public class CustomerStatusHistoryItem
    {
        public int customerID;
        public CustomerStatus oldStatus;
        public CustomerStatus newStatus;
        public DateTime changeDate;
    }
    [Serializable, SingleTableObject]
    [TypeID(1)]
    public class Subscriber : IIDObject
    {
        [IDField]
        public int id=-1;
        [DataField(false)]
        public string name;
        [DataField(false)]
        public SubscriberType subscriberType;
        [DataField(false)]
        public int Kebele;
        [DataField]
        public string address;
        [DataField]
        public string amharicName;
        
        [DataField]
        public string customerCode="";
        [DataField]
        public string phoneNo = "";
        [DataField]
        public string email = "";
        [DataField]
        public string nameOfPlace = "";
        [DataField]
        public CustomerStatus status=CustomerStatus.Active;
        [DataField]
        public DateTime statusDate=DateTime.Now;

        [DataField]
        public int accountID;
        [DataField]
        public int depositAccountID;
        [DataField]
        public int subTypeID=-1;
        public int ObjectIDVal
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }
        

        public static string EnumToName(object sub)
        {
            Type type = sub.GetType();
            if (type == typeof(SubscriberType))
            {
                switch (((SubscriberType)sub))
                {
                    case SubscriberType.Unknown:
                        return "Unknown";

                    case SubscriberType.Private:
                        return "Private Residence";

                    case SubscriberType.GovernmentInstitution:
                        return "Governmental Organization";

                    case SubscriberType.CommercialInstitution:
                        return "Commercial Organization";

                    case SubscriberType.NGO:
                        return "NGO";

                    case SubscriberType.RelegiousInstitution:
                        return "Religious Organization";

                    case SubscriberType.Community:
                        return "Community";

                    case SubscriberType.Industry:
                        return "Industry";

                    case SubscriberType.Other:
                        return "Other";
                }
            }
            else if (type == typeof(SubscriptionType))
            {
                switch (((SubscriptionType)sub))
                {
                    case SubscriptionType.Unknown:
                        return "Unknown";

                    case SubscriptionType.Tap:
                        return "Tap Water";

                    case SubscriptionType.Shared:
                        return "Community Shared Water Supply";

                    case SubscriptionType.Hydrant:
                        return "Hydrant";
                    case SubscriptionType.CattleDrink:
                        return "Livestock Drink";
                    case SubscriptionType.Well:
                        return "Water Well";
                }
            }
            else if (type == typeof(SubscriptionStatus))
            {
                switch ((SubscriptionStatus)sub)
                {
                    case SubscriptionStatus.Unknown:
                        return "Unknown";
                    case SubscriptionStatus.Pending:
                        return "Pending";
                    case SubscriptionStatus.Active:
                        return "Active";
                    case SubscriptionStatus.Diconnected:
                        return "Disconnected";
                    case SubscriptionStatus.Discontinued:
                        return "Discontinued";
                    default:
                        break;
                }
            }
            else if (type == typeof(MeterReadingType))
            {
                switch (((MeterReadingType)sub))
                {
                    case MeterReadingType.Normal:
                        return "";

                    case MeterReadingType.MeterReset:
                        return "Meter Reset";

                }
            }
            else if (type == typeof(ConnectionType))
            {
                switch (((ConnectionType)sub))
                {
                    case ConnectionType.Yard_1:
                        return "Yard Connection";
                    case ConnectionType.House_2:
                        return "House Connection";
                    case ConnectionType.Shared_3:
                        return "Shared Connection";
                    case ConnectionType.Unknown:
                        return "Unknown";
                    default:
                        return "";
                }
            }
            return "";
        }

        public static bool isValidPhoneNumber(string phoneNo)
        {
            if(string.IsNullOrEmpty(phoneNo))
                return false;
            if (phoneNo.Length < 10)
                return false;
            foreach (char ch in phoneNo)
            {
                if(!char.IsDigit(ch))
                    return false;
            }
            return true;
        }

        public static bool isValidEmailAddress(string email)
        {
            if (string.IsNullOrEmpty(email))
                return false;
            string[] parts = email.Split('@');
            if(parts.Length!=2)
            return false;
            
            foreach (char ch in email)
            {
                if (!(char.IsLetterOrDigit(ch) || ch == '@' || ch == '.' || ch == '_' || ch == '-'))
                    return false;
                    
            }
            return true;
        }
    }
}

