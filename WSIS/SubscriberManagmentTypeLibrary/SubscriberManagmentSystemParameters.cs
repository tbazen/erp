
using System;
namespace INTAPS.SubscriberManagment
{

    [Serializable]
    public class SubscriberManagmentSystemParameters
    {
        
        public int correctionReader;
        
        public int currentPeriod;
        
        public int materialIssueAccount;
        public int meterMaterialCategory;
        public int meterStockAcount;
        public RateSetting rateSetting;
        public int readingPeriod;

        public int incomeAccount=-1;
        //public int incomeAccountSummary=-1;
        public int billPenalityIncomeAccount=-1;
        //public int billPenalityIncomeAccountSummary= -1;
        public int receivableAccount=-1;
        //public int receivableAccountSummary=-1;
        public int customerDepositAccount = -1;

        //public int customerDepositAccountSummary = -1;

        public String customerDepositAccountFormula = null;
        public String receivableAccountFormula = null;

        public int rentIncomeAccount=-1;
        public int rentIncomeAccountSummary=-1;
        public int otherBillIncomeAccount = -1;
        public int otherBillIncomeAccountSummary = -1;

        public string waterConsumptionBillFormula;
        public string waterConsumptionBillInterestFormula;
        public string waterConsumptionBillPenalityFormula;
        public string waterConsumptionBillTaxFormula;
        public bool useMeterFromInventory = true;
        public bool offlineReadingDB = false;
        public int customerCodeBatchID = -1;
        public string customerCodeFormat = @"text(sn,""000000"")";
        public int additionalFeeIncomeAcount=-1;
        public int additionalFeeIncomeAcountSummary = -1;
        public string receiptTitle="";
        public int billBatchSize = 1000;
        public bool summerizeTransactions = false;

        public ReadingAnalysisMethod analysisMethod=null;
        public double readingDistanceTolerance = 0;
        public int smsReader = -1;
        public string sms_bill_format = null;
        public string sms_passs_code = null;
        public bool enable_mobile_payment = false;
        public double commissionAmount=5.0;
        public double commissionAmount_Utility_Share = 0.5;
        public double commissionAmount_System_Share = 2.5;
        public int mobilePaymentServiceChargeIncome=-1;
        public string sms_unread_format="Dear $name reading is not collected for your water meter, please reply with your reading until $date.\nAdama Water Supply";
    }
}

