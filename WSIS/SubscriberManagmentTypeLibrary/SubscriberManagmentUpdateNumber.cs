﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.SubscriberManagment
{
    [Serializable]
    public class SubscriberManagmentUpdateNumber:IEquatable<SubscriberManagmentUpdateNumber>
    {
        public int customerUpdateNumber = 0;
        public int contractUpdateNumber=0;
        public int readingUpdateNumber = 0;
        public int billingUpdateNumber = 0;
        public int billingConfiguration = 0;

        public bool Equals(SubscriberManagmentUpdateNumber other)
        {
            if (other == null)
                return false;
            return this.customerUpdateNumber == other.customerUpdateNumber
                && this.contractUpdateNumber == other.contractUpdateNumber
                && this.readingUpdateNumber == other.readingUpdateNumber
                && this.billingUpdateNumber == other.billingUpdateNumber
                && this.billingConfiguration == other.billingConfiguration;
        }
    }
}
