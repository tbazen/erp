
    using System;
namespace INTAPS.SubscriberManagment
{
    [Flags]
    public enum SubscriberSearchField
    {
        CustomerName = 1,
        CustomerCode = 2,
        ConstactNo = 4,
        MeterNo = 8,
        PhoneNo=16,
        All = CustomerName | CustomerCode | ConstactNo|MeterNo|PhoneNo
    }
    [Serializable]
    public class SubscriberSearchResult:IComparable<SubscriberSearchResult>
    {
        public Subscriber subscriber;
        public Subscription subscription;
        public int rank;

        public SubscriberSearchResult(int rank)
        {
            this.rank = rank;
        }

        public int CompareTo(SubscriberSearchResult other)
        {
            return rank.CompareTo(other.rank);
        }
    }
}

