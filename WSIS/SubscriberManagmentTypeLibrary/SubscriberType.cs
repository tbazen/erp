using System;
using INTAPS.RDBMS;
namespace INTAPS.SubscriberManagment
{

    public enum     SubscriberType
    {
        Unknown=0,
        Private=1,
        GovernmentInstitution=2,
        CommercialInstitution=3,
        NGO=4,
        RelegiousInstitution=5,
        Community=6,
        Industry=7,
        Other=8
    }
    [Serializable]
    [SingleTableObject]
    public class CustomerSubtype:IEquatable<CustomerSubtype>
    {
        [IDField]
        public SubscriberType type;
        [IDField]
        public int id;
        [DataField]
        public string name;
        public override string ToString()
        {
            return name;
        }
        public CustomerSubtype()
            : this(-1)
        {
        }
        public CustomerSubtype(int id)
        {
            this.id = id;
            this.name = "No Name " + id;
        }

        public bool Equals(CustomerSubtype other)
        {
            if (other == null)
                return false;
            return this.id == other.id;
        }
    }
}

