
using INTAPS.ClientServer;
using INTAPS.RDBMS;
using System;
using System.Xml.Serialization;
namespace INTAPS.SubscriberManagment
{
    [Serializable]
    [SingleTableObject(orderBy="id")]
    public class DistrictMeteringZone
    {
        [IDField]
        public int id;
        [DataField]
        public string name;
        public override string ToString()
        {
            return name;
        }
    }


    [Serializable]
    [SingleTableObject(orderBy = "id")]
    public class PressureZone
    {
        [IDField]
        public int id;
        [DataField]
        public string name;
        public override string ToString()
        {
            return name;
        }

    }

    [Serializable, XmlInclude(typeof(Subscriber)), SingleTableObject(orderBy="contractNo,ticksFrom desc")]
    [TypeID(11)]
    public class Subscription : IIDObject
    {
        public VersionedID versionedID
        {
            get
            {
                return new VersionedID(id, ticksFrom);
            }
            set
            {
                this.id = value.id;
                this.ticksFrom = value.version;
            }
        }

        [IDField]
        public int id = -1;
        [IDField(false)]
        public long ticksFrom= DateTime.Now.Ticks;
        [DataField(true)]
        public long ticksTo=-1;
        [DataField(false)]
        public DataTimeBinding timeBinding = DataTimeBinding.LowerBound;
        public bool hasUpperBound
        {
            get
            {
                return ticksTo > 0;
            }
        }
        [DataField(false)]
        public int subscriberID = -1;
        [DataField]
        public string contractNo = null;
        [DataField(false)]
        public SubscriptionType subscriptionType = SubscriptionType.Tap;
        [DataField(false)]
        public SubscriptionStatus subscriptionStatus = SubscriptionStatus.Unknown;
        [DataField(false)]
        public int Kebele = -1;
        [DataField]
        public string address = null;
        [DataField]
        public string landCertificateNo = null;
        public Subscriber subscriber;
        [DataField]
        public double initialMeterReading = 0;
        [DataField]
        public bool prePaid = false;
        [DataField]
        public string parcelNo = "";

        [DataField]
        public ConnectionType connectionType = ConnectionType.Unknown;
        [DataField]
        public SupplyCondition supplyCondition = SupplyCondition.Unknown;
        [DataField]
        public DataStatus dataStatus = DataStatus.Unknown;
        [DataField]
        public MeterPositioning meterPosition = MeterPositioning.Unknown;

        [DataField]
        public double waterMeterX = 0;
        [DataField]
        public double waterMeterY = 0;
        [DataField]
        public double houseConnectionX = 0;
        [DataField]
        public double houseConnectionY = 0;

        [DataField]
        public string remark = "";
        [DataField]
        public string itemCode;//ITEM CODE REF
        [DataField]
        public string serialNo;
        [DataField]
        public string modelNo;
        [DataField]
        public MaterialOperationalStatus opStatus;
        [DataField]
        public int householdSize=5;
        [DataField]
        public int dma = -1;
        [DataField]
        public int pressureZone = -1;

        public MeterData meterData
        {
            get
            {
                MeterData ret= new MeterData(itemCode, serialNo, modelNo, opStatus);
                ret.initialMeterReading = this.initialMeterReading;
                return ret;
            }
            set
            {
                this.itemCode = value.itemCode;
                this.serialNo = value.serialNo;
                this.modelNo = value.modelNo;
                this.opStatus = value.opStatus;
                this.initialMeterReading = value.initialMeterReading;
            }
        }
        

        public int ObjectIDVal
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }

        public static double sphericalDistance(double lat1, double lng1, double lat2, double lng2)
        {
            const double MAP_R = 6371;
            double a1 = lat1 * Math.PI / 180;
            double a2 = lat2 * Math.PI / 180;
            double delLat = (lat2 - lat1) * Math.PI / 180;
            double delLong = (lng2 - lng1) * Math.PI / 180;
            double a = Math.Sin(delLat / 1) * Math.Sin(delLat / 2)
                + Math.Cos(a1) * Math.Cos(a2) * Math.Sin(delLong / 2) * Math.Sin(delLong / 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            double d = MAP_R * c;
            return d;
        }


        public bool hasCoordinate { get { return waterMeterX != 0 || waterMeterY != 0; } }
    }
    [Serializable]
    public class ConnectionAddresss
    {
        public int kebele;
        public string address;
        public double X;
        public double Y;
        public double Z;
    }
}

