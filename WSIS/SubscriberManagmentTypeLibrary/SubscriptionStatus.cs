using System;
namespace INTAPS.SubscriberManagment
{
    public enum SubscriptionStatus
    {
        Unknown,
        Pending,
        Active,
        Diconnected,
        Discontinued
    }
}

