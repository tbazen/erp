
    using System;
namespace INTAPS.SubscriberManagment
{

    public enum SubscriptionType
    {
        Unknown,
        Tap,
        Shared,
        Hydrant,
        CattleDrink,
        Well,
    }
}

