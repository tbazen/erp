﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.RDBMS;

namespace INTAPS.SubscriberManagment
{
    [Serializable]
    [SingleTableObject]
    [TypeID(31)]
    public class SystemFeeDeposit
    {
        [IDField]
        public int readerID;
        [IDField]
        public int periodID;
        [IDField]
        public DateTime returnDate;
        [DataField]
        public double amount;
        [DataField]
        public string bankRef;
    }
}
