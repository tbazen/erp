xcopy ClientBin  bin\Server\ClientFiles /S /Y
del ServerBin\log\*.* /Q


xcopy ServerBin\*.*  bin\Server /S /Y

del bin\Server\ClientFiles\*.pdb /Q
del bin\Server\ClientFiles\*.config /Q
del bin\Server\ClientFiles\*.vshost.* /Q
del bin\Server\ClientFiles\*.xml /Q
del bin\Server\ClientFiles\DevExpress.* /Q
del bin\Server\ClientFiles\de\* /Q
del bin\Server\ClientFiles\de /Q
del bin\Server\ClientFiles\es\* /Q
del bin\Server\ClientFiles\es /Q
del bin\Server\ClientFiles\ja\* /Q
del bin\Server\ClientFiles\ja /Q
del bin\Server\ClientFiles\ru\* /Q
del bin\Server\ClientFiles\ru /Q

del bin\Server\DevExpress.* /Q
del bin\Server\*.pdb /Q
del bin\Server\*.vshost.* /Q
del bin\Server\*.config /Q
del bin\Server\*.xml /Q
del bin\Server\ClientFiles\de /Y
rd bin\Server\ClientFiles\de
del bin\Server\ClientFiles\es /Y
rd bin\Server\ClientFiles\es
del bin\Server\ClientFiles\ja /Y
rd bin\Server\ClientFiles\ja
del bin\Server\ClientFiles\ru /Y
rd bin\Server\ClientFiles\ru

xcopy SubscriberManagmentService\html\*.*  bin\Server\html /S /Y