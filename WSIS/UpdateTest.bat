xcopy ClientBin  Test\ClientBin /S /Y
del ServerBin\log\*.* /Q
xcopy ServerBin\*.*  Test\ServerBin /S /Y

del Test\ClientBin\*.config /Q
del Test\ClientBin\*.vshost.* /Q
del Test\ClientBin\*.xml /Q
del Test\ClientBin\DevExpress*.* /Q
RMDIR Test\ClientBin\de /S /Q
RMDIR Test\ClientBin\es /S /Q
RMDIR Test\ClientBin\ja /S /Q
RMDIR Test\ClientBin\ru /S /Q
del Test\ServerBin\*.vshost.* /Q
del Test\ServerBin\*.config /Q
del Test\ServerBin\*.xml /Q
