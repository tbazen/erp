﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

public partial class UserDefinedFunctions
{
    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDateTime ticksToDate(SqlInt64 ticks)
    {
        return new SqlDateTime(new DateTime(ticks.Value));
    }
    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlInt64 dateToTicks(SqlDateTime date)
    {
        return new SqlInt64(date.Value.Ticks);
    }
};

