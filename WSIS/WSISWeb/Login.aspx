﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="WSISWeb.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel='Stylesheet' href='page.css' />
    <link rel="icon" type="image/png" href="images/favicon.png" />
    <title>Login to WSIS</title>
</head>
<body>
    <form id="form1" runat="server">
    <table width='100%'>
        <tr>
            <td class='title1'>WSIS</td>
            </tr>
        <tr>
            <td class='spacer'></td>
        </tr>
        <tr>
            <td><input class='myInput' name="textUN" type="text" placeholder="User Name"/></td>
        </tr>
        <tr>
            <td class='spacerSmall' ></td>
        </tr>
        <tr>
            <td><input class='myInput' name="textPW" type="password" placeholder="Password"/></td>
        </tr>
        <tr>
            <td runat='server' id='err' class='error'></td>
        </tr>
        <tr>
            <td class='spacer' ></td>
        </tr>
        <tr>
            <td class='centertd'><input id="loginbtn" type='submit' value="Connect" /></td>
        </tr>
    </table>
    </form>
</body>
</html>
