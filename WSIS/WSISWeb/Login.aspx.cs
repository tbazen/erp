﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using INTAPS.ClientServer.Client;
using INTAPS.Payroll.Client;
using INTAPS.Accounting.Client;

namespace WSISWeb
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                    string userName = this.Request.Form["textUN"];
                    string password= this.Request.Form["textPW"];
                    string server = System.Configuration.ConfigurationManager.AppSettings["WSISServer"];
                    try
                    {
                        ApplicationClient.Connect(server, userName, password);
                        AccountingClient.Connect(server);
                        PayrollClient.Connect(server);
                        INTAPS.SubscriberManagment.Client.SubscriberManagmentClient.Connect(server);
                        INTAPS.WSIS.Job.Client.JobManagerClient.Connect(server);
                        BIZNET.iERP.Client.iERPTransactionClient.Connect(server);

                        base.Session["sid"] = ApplicationClient.SessionID;
                        base.Session["userName"] = userName;
                        Response.Redirect("readersummary.aspx");
                    }
                    catch (Exception ex)
                    {
                        err.InnerText = ex.Message;
                    }
            }
        }
    }
}