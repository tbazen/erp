﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using INTAPS.ClientServer.Client;
using INTAPS.Accounting.Client;
using INTAPS.Payroll.Client;

namespace WSISWeb
{
    public class WSISWeb
    {
        public static void Reconnect(string sessionID)
        {
            string url = System.Configuration.ConfigurationManager.AppSettings["WSISServer"];
            ApplicationClient.Connect(url, sessionID);
            if (!AccountingClient.IsConnected)
            {
                AccountingClient.Connect(url);
                PayrollClient.Connect(url);
                INTAPS.SubscriberManagment.Client.SubscriberManagmentClient.Connect(url);
                INTAPS.WSIS.Job.Client.JobManagerClient.Connect(url);
                BIZNET.iERP.Client.iERPTransactionClient.Connect(url);
            }

        }
    }
}