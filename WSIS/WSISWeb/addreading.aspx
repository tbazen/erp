﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="addreading.aspx.cs" Inherits="WSISWeb.addresing" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Record Reading</title>
    <link rel='Stylesheet' href='page.css' />    
</head>
<body>
<script>
    navigator.geolocation.getCurrentPosition(showPosition);
    function showPosition(position) {
        var x = document.getElementById("xy");
        x.value = position.coords.latitude + "," + position.coords.longitude;
        var y = document.getElementById("txtReading");
        y.visible = true;
    }
</script>
    <form id="form1" runat="server">
    <input class='hiddeninput' runat='server' id='xy' name="xy" type="text" />
        <table width='100%'>
            <tr>
                <td class='title1' >WSIS Mobile Meter Reading</td>
              </tr>
            <tr>
                <td><input class='myInput' runat='server' id='txtCode' name='code' type='text' placeholder="Contract No"/></td>
            </tr>
            <tr>
                <td class='value2'><input class='myInput' visible='false' id='txtReading' name='reading' type='text' placeholder="Reading"/></td>
            </tr>
            <tr>
                <td><span class='label2'>Reading Problem:</span><select runat='server' id="problem">
                    <option>No Problem</option>
                    <option>Nobody Home</option>
                    <option>Not Cooperative</option>
                    <option>Defective Meter</option>
                    <option>Inaccessible Meter</option>
                    <option>Other</option>
                    </select></td>
            </tr>
            <tr>
                <td class='value2'><input class='myInput' runat='server' id='txtRemark' name='remark' type='text' placeholder="Remark"/></td>
            </tr>
            <tr>
                <td runat='server' name='customerName' id='customerName' class='info1'></td>
            </tr>
            <tr>
                <td runat='server' name='err' id='err' align='right' class='error'></td><td style='width:30%'></td>
            </tr>
            <tr>
                <td class='centertd'><input class='sumbtn' name="check" type='submit' value="Check Code" /></td>
            </tr>
            <tr><td class='spacerSmall' ></td></tr>
            <tr>
                <td class='centertd'><input class='sumbtn' name="add" type='submit' value="Submit" /></td>
            </tr>
    </table>
    <div class='spacerSmall' ></div>
    <div class='spacerSmall' ></div>
    <table runat='server' id='custInfo' width='100%'>
        <tr class='custInfoOdd'><td class='label3'>Name:</td><td class ='value3' id='tdCustName'></td></tr>
        <tr class='custInfoEven'><td class='label3'>Status:</td><td class ='value3' id='tdStatus'></td></tr>
        <tr class='custInfoOdd'><td class='label3'>Meter No:</td><td class ='value3' id='tdMeterNo'></td></tr>
        <tr class='custInfoEven'><td class='label3'>Reading:</td><td class ='value3' id='tdReading'></td></tr>
        <tr class='custInfoOdd'><td class='label3'>Remark:</td><td class ='value3' id='tdRemark'></td></tr>
        <tr class='custInfoEven'><td class='label3'>Current Bill:</td><td class ='value3' id='tdCurrentBill'></td></tr>
        <tr class='custInfoOdd'><td class='label3'>Outstanding Bill:</td><td class ='value3' id='tdOutstanding'></td></tr>
    </table>
    <div class='spacerSmall'></div>
    <div class='centertd'><input class='sumbtn' name="prog" type='submit' value="My Progress" /></div>
    </form>
</body>
</html>
