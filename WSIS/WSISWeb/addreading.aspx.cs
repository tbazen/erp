﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.Accounting;

namespace WSISWeb
{
    public partial class addresing : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sid = Session["sid"] as string;
            if (string.IsNullOrEmpty(sid))
            {
                Response.Redirect("login.aspx");
                return;
            }
            if(!(Session["readerID"] is int))
            {
                Response.Redirect("login.aspx");
                return;
            }
            int readerID=(int)Session["readerID"];
            try
            {
                WSISWeb.Reconnect(sid);
                custInfo.Visible = false;
                if (IsPostBack)
                {
                    if (Request.Form["prog"] != null)
                    {
                        Response.Redirect("readersummary.aspx");
                        return;
                    }
                    string contractNo = txtCode.Value;
                    if (string.IsNullOrEmpty(contractNo))
                        throw new Exception("Empty contract no");
                    Subscription con = SubscriberManagmentClient.GetSubscription(contractNo, DateTime.Now.Ticks);
                    if (con == null)
                        throw new Exception("Invalid constract no " + contractNo);
                    BWFMeterReading existing = SubscriberManagmentClient.BWFGetMeterReadingByPeriodID(con.id, SubscriberManagmentClient.ReadingPeriod.id);
                    if (Request.Form["add"]!=null)
                    {
                        if(con.subscriptionStatus!=SubscriptionStatus.Active)
                            throw new Exception("Connection not active");

                        if (existing == null)
                            throw new Exception("This connection is not included in the reading plan of this period");
                        if (SubscriberManagmentClient.GetBlock(existing.readingBlockID).readerID != readerID)
                            throw new Exception("This connection is not assigned to you");
                        string infoText = "";
                        if (existing.bwfStatus == BWFStatus.Read)
                        {
                            infoText="Warning! you have already covered this meter. The previos data is overwritten. ";
                        }
                        BWFStatus status;
                        double reading = 0;
                        string strReading = this.Request.Form["reading"] as string; ;
                        MeterReadingProblem prob = ((MeterReadingProblem)problem.SelectedIndex);
                        if (prob == MeterReadingProblem.NoProblem || !string.IsNullOrEmpty(strReading))
                        {
                            if (string.IsNullOrEmpty(strReading) || !double.TryParse(strReading, out reading))
                                throw new Exception("Please enter valid reading");
                            status = BWFStatus.Read;
                        }
                        else
                            status = BWFStatus.Unread;
                        existing.bwfStatus = status;
                        existing.reading = reading;
                        existing.readingProblem = prob;
                        existing.readingRemark = txtRemark.Value;
                        existing.readingTime = DateTime.Now;
                        existing.extraInfo |= ReadingExtraInfo.ReadingDate;
                        string coord = xy.Value;
                        if (!string.IsNullOrEmpty(coord))
                        {
                            string[] parts = coord.Split(new char[]{','},StringSplitOptions.RemoveEmptyEntries);
                            if (parts.Length != 2)
                                throw new Exception("Invalid coordinate data.");
                            double x,y;
                            if (!double.TryParse(parts[0], out x) || !double.TryParse(parts[1], out y))
                                throw new Exception("Invalid coordinate data.");
                            existing.readingX = x;
                            existing.readingY = y;
                            existing.extraInfo |= ReadingExtraInfo.Coordinate;
                            if (con.waterMeterX==0 && con.waterMeterY==0)
                            {
                                con.waterMeterX = x;
                                con.waterMeterY = y;
                                try
                                {
                                    SubscriberManagmentClient.setSubscriptionCoordinate(con.id, DateTime.Now.Ticks, x, y);
                                    infoText += "Connection coordinate updated. ";
                                }
                                catch
                                {
                                    infoText += "Error trying to update connection coordinates. Error ignored. ";
                                }
                            }
                        }
                        if(!string.IsNullOrEmpty(existing.readingRemark))
                            existing.extraInfo |= ReadingExtraInfo.Remark;                        
                        SubscriberManagmentClient.BWFUpdateMeterReading(existing);
                        infoText += "Reading succesfully saved. ";
                        customerName.InnerText = infoText;
                        txtCode.Value = "";
                        problem.SelectedIndex = 0;
                        txtRemark.Value = "";

                    }

                    //cutomer information
                    tdCustName.InnerText = con.subscriber.name;
                    tdStatus.InnerText = Subscriber.EnumToName(con.subscriptionStatus);
                    tdMeterNo.InnerText = con.serialNo;
                    tdReading.InnerText = existing == null ? "[Can't be read]" : (existing.bwfStatus == BWFStatus.Read ? existing.reading.ToString() : "[Not Read]");
                    string readingRemark = null;
                    if (existing == null)
                        readingRemark = "";
                    else
                    {
                        if (existing.readingProblem != MeterReadingProblem.NoProblem)
                        {
                            if (readingRemark != null)
                                readingRemark += "<br/>";
                            readingRemark += string.Format("<span class='error3'>{0}</span>", HttpUtility.HtmlEncode(existing.readingProblemString));
                        }
                        if (!string.IsNullOrEmpty(existing.readingRemark))
                        {
                            if (readingRemark != null)
                                readingRemark += "<br/>";
                            readingRemark += HttpUtility.HtmlEncode(existing.readingRemark);
                        }
                        if ((existing.extraInfo & ReadingExtraInfo.Coordinate) == ReadingExtraInfo.Coordinate)
                        {
                            if (readingRemark != null)
                                readingRemark += "<br/>";
                            readingRemark += string.Format("<span class='label4'>Coordinate lat: </span>{0}<span class='label4'> long: </span> {1}", existing.readingX, existing.readingY);
                        }
                        if ((existing.extraInfo & ReadingExtraInfo.ReadingDate) == ReadingExtraInfo.ReadingDate)
                        {
                            if (readingRemark != null)
                                readingRemark += "<br/>";
                            readingRemark += string.Format("<span class='label4'>Read on: </span>{0}", HttpUtility.HtmlEncode(existing.readingTime.ToString()));
                        }
                    }
                    tdRemark.InnerHtml = readingRemark == null ? "" : readingRemark;
                    double current = 0;
                    double total = 0;
                    foreach (CustomerBillDocument bill in SubscriberManagmentClient.getBillDocuments(-1, con.subscriber.id, -1, -1, true))
                    {
                        if (bill.draft)
                            continue;
                        total += bill.total;
                        PeriodicBill p = bill as PeriodicBill;
                        if (p == null || p.period.id != SubscriberManagmentClient.ReadingPeriod.id)
                            continue;
                        current += bill.total;
                    }
                    tdCurrentBill.InnerText = AccountBase.AmountEqual(current, 0) ? "" : AccountBase.FormatAmount(current);
                    tdOutstanding.InnerText = AccountBase.AmountEqual(total, 0) ? "" : AccountBase.FormatAmount(total);
                    custInfo.Visible = true;

                }

            }
            catch (Exception ex)
            {
                err.InnerText = ex.Message;
            }

        }
    }
}