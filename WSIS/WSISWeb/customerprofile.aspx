﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="customerprofile.aspx.cs" Inherits="WSISWeb.customerprofile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Profile</title>
    <link rel='Stylesheet' href="jobstyle.css" /> 
</head>
<body>
    <form id="form1" runat="server">
    <span runat='server' name='err' id='err' colspan='2' align='right' class='error'></span>
            <h1>Customer Profile</h1>
    <div runat='server' id='custProf'>
    </div>
    </form>
</body>
</html>
