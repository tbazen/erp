﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.WSIS.Client;

namespace WSISWeb
{
    public partial class customerprofile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sid = Request.QueryString["sid"];
                if (string.IsNullOrEmpty(sid))
                    throw new Exception("Invalid request");
                string server = System.Configuration.ConfigurationManager.AppSettings["WSISServer"];
                WSISAllClients con = new WSISAllClients(server,sid);
                string val = Request.QueryString["con"];
                Subscription subsc = con.subscriberManagmentServer.GetSubscription(sid,val, DateTime.Now.Ticks);
                custProf.InnerHtml= INTAPS.WSIS.Job.Client.CustomerProfileBuilder.buildHTML(con,subsc.subscriberID,INTAPS.WSIS.Job.Client.CustomerProfileBuilder.ConnectionProfileOptions.NoLink);
            }
            catch (Exception ex)
            {
                err.InnerText = ex.Message;
            }

        }
    }
}