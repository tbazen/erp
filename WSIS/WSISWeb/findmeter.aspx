﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="findmeter.aspx.cs" Inherits="WSISWeb.findmeter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
 <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta charset="utf-8" />
    <title>Meters Nearby</title>
    <link rel='Stylesheet' href='page.css'/>    
    <link rel="icon" type="image/png" href="images/favicon.png" />
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script>
        var icons = ['images/meter-blue.ico','images/reader.png'];
        function initialize() {
            var data = document.getElementById("wsis_mapdata").value;
            if (!data || 0 == data.length) {
                navigator.geolocation.getCurrentPosition(showPosition);
                function showPosition(position) {
                    var x = document.getElementById("location");
                    x.value = position.coords.latitude + "," + position.coords.longitude;
                    form1.submit();
                }
                return;
            }
            var boundParts = wsis_map_bound.value.split(",");
            var bound1 = new google.maps.LatLng(parseFloat(boundParts[0]), parseFloat(boundParts[1]));
            var bound2 = new google.maps.LatLng(parseFloat(boundParts[2]), parseFloat(boundParts[3]));
            var bound = new google.maps.LatLngBounds(bound1, bound2);
            var goldStar = {
                path: 'M -10,0,10,0 z,M 0,-10,0,10 z',
                fillColor: 'yellow',
                fillOpacity: 0.8,
                scale: 1,
                strokeColor: 'red',
                strokeWeight: 2
            };
            var mapOptions = {
            }
            var map = new google.maps.Map(document.getElementById('map-canvas'));

            var parts = data.split(",");
            var coordinates = Array(0);
            var markers = Array()
            var centerX = 0;
            var centerY = 0;
            var n = parts.length / 4;
            for (i = 0; i < n; i++) {
                var x, y;
                x = parseFloat(parts[i * 4 + 0]);
                y = parseFloat(parts[i * 4 + 1])
                coordinates[i] = new google.maps.LatLng(x, y);
                var marker = new google.maps.Marker({
                    position: coordinates[i],
                    icon: icons[parseInt(parts[i * 4 + 3])],
                    map: map,
                    title: parts[i * 4 + 2]
                });
                markers[i] = marker;
            }
            map.fitBounds(bound);
        }
        

        google.maps.event.addDomListener(window, 'load', initialize);


    </script>
  </head>
<body>
    

    <form id="form1" runat="server">
    <input runat='server' id='wsis_map_bound' name="data" type="text"  class='hiddeninput' />
    <input runat='server' id='wsis_mapdata' name="data" type="text"  class='hiddeninput' />
    <input runat='server' id='location' name="data" type="text"  class='hiddeninput' />
        <table width='100%' style='height:100%'>
            <tr><td class='title4' >Meters Nearby<br /><span runat='server' name='err' id='err' colspan='2' align='right' class='error'></span></td></tr>
            <tr><td><div id='map-canvas'></div></td></tr>
        </td></tr>

    </table>
    </form>
    
</body>
</html>
