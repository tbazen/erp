﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.Payroll.Client;

namespace WSISWeb
{
    public partial class findmeter : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sid = Session["sid"] as string;
            if (string.IsNullOrEmpty(sid))
            {
                Response.Redirect("login.aspx");
                return;
            }
            if(!(Session["readerID"] is int))
            {
                Response.Redirect("login.aspx");
                return;
            }
            int readerID=(int)Session["readerID"];
            try
            {
                WSISWeb.Reconnect(sid);
                wsis_mapdata.Value=null;
                if(IsPostBack)
                {
                    string[] parts=location.Value.Split(',');
                    double lat,lng;
                    if(double.TryParse(parts[0],out lat) && double.TryParse(parts[1],out lng))
                    {

                        MeterMap map = SubscriberManagmentClient.getMetersNearBy(readerID, lat, lng, 1000, DateTime.Now.Ticks);
                        List<MeterMarker> mrk = new List<MeterMarker>(map.markers);
                        MeterMarker selfMarker = new MeterMarker();
                        selfMarker.label = PayrollClient.GetEmployee(readerID).employeeName;
                        selfMarker.symbol = MeterMarkerSymbol.ReaderLocation;
                        selfMarker.lat = lat;
                        selfMarker.lng = lng;
                        selfMarker.connectionCode = selfMarker.label;
                        mrk.Add(selfMarker);
                        map.markers = mrk.ToArray();
                        
                        string mapstr = SubscriberManagmentClient.builWebMapString(map);
                        double l1,l2,g1,g2;
                        SubscriberManagmentClient.getMapBound(map.markers, out l1, out g1, out l2, out g2);
                        if (Math.Abs(l2-l1)<0.01)
                        {
                            l1 = lat - 0.01;
                            l2 = lat + 0.01;
                        }
                        if(Math.Abs(g2-g1)<0.01)
                        {
                            g1 = lng - 0.01;
                            g2 = lng + 0.01;
                        }
                            
                        wsis_mapdata.Value = mapstr;
                        wsis_map_bound.Value = l1 + "," + g1 + "," + l2 + "," + g2;
                    }
                }
            }
            catch (Exception ex)
            {
                err.InnerText = ex.Message;
            }
        }
    }
}