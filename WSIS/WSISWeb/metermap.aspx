﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="metermap.aspx.cs" Inherits="WSISWeb.metermap" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
 <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta charset="utf-8" />
    <title>Meter Map</title>
    <link rel='Stylesheet' href='page.css'/>    
    <link rel="icon" type="image/png" href="images/favicon.png" />
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script>
        var icons = ['images/default.png'
        , 'images/readerlocation.png'
        , 'images/read.png'
        , 'images/unread.png'
        , 'images/selfmeter.png'
        , 'images/othermeter.png'
        , 'images/locatedmeter.png'
        , 'images/path_read.png'
        , 'images/path_unread.png'
        , 'images/readingproblem.png'

        ];
        function initialize() {
            var sid = document.getElementById("sid").value;
            var data1 = document.getElementById("wsis_mapdata1").value;
            var data2 = document.getElementById("wsis_mapdata2").value;
            var data3 = document.getElementById("wsis_mapdata3").value;
            var data4 = document.getElementById("wsis_mapdata4").value;
            var data5 = document.getElementById("wsis_mapdata5").value;
            var pathdata = document.getElementById("wsis_pathdata").value;
            var boundParts = document.getElementById("wsis_map_bound").value.split(",");
            var bound1 = new google.maps.LatLng(parseFloat(boundParts[0]), parseFloat(boundParts[1]));
            var bound2 = new google.maps.LatLng(parseFloat(boundParts[2]), parseFloat(boundParts[3]));
            var bound = new google.maps.LatLngBounds(bound1, bound2);
            var infoWindow = new google.maps.InfoWindow();
            var mapOptions = {
            }
            var map = new google.maps.Map(document.getElementById('map-canvas'));
            var data = data1 + data2 + data3 + data4 + data5;
            var parts = data.split(",");
            var coordinates = Array(0);
            var markers = Array()
            var n = parts.length / 4;
            for (i = 0; i < n; i++) {
                var x, y;
                x = parseFloat(parts[i * 4 + 0]);
                y = parseFloat(parts[i * 4 + 1])
                coordinates[i] = new google.maps.LatLng(x, y);
                var image = {
                    url: icons[parseInt(parts[i * 4 + 3])],
                    // This marker is 20 pixels wide by 32 pixels tall.
                    size: new google.maps.Size(16, 16),
                    // The origin for this image is 0,0.
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at 0,32.
                    anchor: new google.maps.Point(8, 8)
                };
                
                var marker = new google.maps.Marker({
                    position: coordinates[i],
                    icon: image,
                    map: map,
                    title: parts[i * 4 + 2]
                });
                markers[i] = marker;
                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        infoWindow.setContent("<a href='customerprofile.aspx?sid=" + sid + "&con=" + data + "'>" + data + "</a>");
                        infoWindow.open(map, marker);
                    });
                })(marker, parts[i*4+2]);
            }

            parts = pathdata.split(",");
            coordinates = Array(0);
            n = parts.length / 4;
            for (i = 0; i < n; i++) {
                var x, y;
                x = parseFloat(parts[i * 4 + 0]);
                y = parseFloat(parts[i * 4 + 1])
                coordinates[i] = new google.maps.LatLng(x, y);

            }
            var readingPath = new google.maps.Polyline({
                path: coordinates,
                geodesic: true,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });

            readingPath.setMap(map);

            map.fitBounds(bound);
            var legend = document.getElementById('legend');
            map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);

        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  </head>
<body>

    <form id="form1" runat="server">
    <input runat='server' id='wsis_map_bound' name="data" type="text"  class='hiddeninput' />
    <input runat='server' id='wsis_mapdata1' name="data" type="text"  class='hiddeninput' />
    <input runat='server' id='wsis_mapdata2' name="data" type="text"  class='hiddeninput' />
    <input runat='server' id='wsis_mapdata3' name="data" type="text"  class='hiddeninput' />
    <input runat='server' id='wsis_mapdata4' name="data" type="text"  class='hiddeninput' />
    <input runat='server' id='wsis_mapdata5' name="data" type="text"  class='hiddeninput' />

    <input runat='server' id='wsis_pathdata' name="data" type="text"  class='hiddeninput' />
    <input runat='server' id='location' name="data" type="text"  class='hiddeninput' />
    <span runat='server' name='err' id='err' colspan='2' align='right' class='error'></span>
     <div id='map-canvas'></div>

    </form>
        <div id="legend" runat='server'></div>
            <input runat='server' id='sid' name="data" type="text"  class='hiddeninput' />

</body>
</html>
