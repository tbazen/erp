﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.Payroll.Client;
using System.Web.UI.HtmlControls;
using System.Text;

namespace WSISWeb
{
    public partial class metermap : System.Web.UI.Page
    {
        const int DATA_LENGTH = 300000;
        const double lat_max_zoom = 0.0005;
        const double lng_max_zoom = 0.0005;
        public static string builWebMapString(MeterMap map,int max)
        {
            if (max == -1)
                max = 100;
            StringBuilder sb = new StringBuilder();
            foreach (MeterMarker m in map.markers)
            {
                //string thisMarker = m.lat + "," + m.lng + "," + m.connectionCode.Replace(',', '_') + "," + (int)m.symbol;
                if (sb.Length > 0)
                    sb.Append(",");
                sb.Append(string.Format("{0},{1},{2},{3}", m.lat, m.lng, m.connectionCode.Replace(',', '_'), (int)m.symbol));
            }
            return sb.ToString();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            HtmlInputText[] wsis_data = new HtmlInputText[] { wsis_mapdata1, wsis_mapdata2, wsis_mapdata3, wsis_mapdata4, wsis_mapdata5 };
            try
            {
                MeterMap map = null;
                ReadingPath path = null;
                double l1=0, l2=0, g1=0, g2=0;
                string sessionID = Request.QueryString["sid"];
                if (string.IsNullOrEmpty(sessionID))
                    throw new Exception("Invalid request");
                WSISWeb.Reconnect(sessionID);
                string mapType = Request.QueryString["t"];
                if (string.IsNullOrEmpty(sessionID))
                    throw new Exception("Invalid request");
                int periodID;
                int readerID;
                string val;
                string locatedWaterMeterStr="The Meter";
                string allocationName="Allocted";
                string pathLegend=null;
                sid.Value = sessionID;
                int max;
                if (string.IsNullOrEmpty(Request.QueryString["max"]))
                    max = -1;
                else
                    max = int.Parse(Request.QueryString["max"]);
                int conID;
                switch (mapType)
                {
                    case "all":
                        map = SubscriberManagmentClient.getAllMetersMap(DateTime.Now.Ticks);
                        break;
					case "unread":
					map = SubscriberManagmentClient.getUnreadMetersMap(DateTime.Now.Ticks);
					break;
                    case "reading":
                        val = Request.QueryString["prd"];
                        if (string.IsNullOrEmpty(val) || !int.TryParse(val, out periodID))
                            throw new Exception("Invalid request");
                        map = SubscriberManagmentClient.getReadingMap(periodID);
                        break;
                    case "allocation":
                    case "reading_path":
                        val = Request.QueryString["prd"];
                        if (string.IsNullOrEmpty(val) || !int.TryParse(val, out periodID))
                            throw new Exception("Invalid request");
                        val = Request.QueryString["reader"];
                        if (string.IsNullOrEmpty(val) || !int.TryParse(val, out readerID))
                            throw new Exception("Invalid request");
                        string datestr = Request.QueryString["date"];
                        map = SubscriberManagmentClient.getMineOthers(periodID, readerID);
                        allocationName = PayrollClient.GetEmployee(readerID).employeeName;
                        if (mapType.Equals("reading_path"))
                        {
                            int notMapped;
                            
                            if (string.IsNullOrEmpty(datestr))
                                path = SubscriberManagmentClient.getReadingPath(periodID, readerID, out notMapped);
                            else
                            {
                                DateTime date = DateTime.Parse(datestr);
                                path = SubscriberManagmentClient.getReadingPath(periodID, readerID, date.Date, date.Date.AddDays(1), out notMapped);
                            }
                            SubscriberManagmentClient.getMapBound(path.markers, out l1, out g1, out l2, out g2);

                            if (Math.Abs(l2 - l1) < lat_max_zoom)
                            {
                                l1 = (l1 + l2) / 2 - lat_max_zoom;
                                l2 = (l1 + l2) / 2 + lat_max_zoom;
                            }
                            if (Math.Abs(g2 - g1) < lng_max_zoom)
                            {
                                g1 = (g1 + g2) / 2 - lng_max_zoom;
                                g2 = (g1 + g2) / 2 + lng_max_zoom;
                            }
                            MeterMarker[] mergedMarker =INTAPS.ArrayExtension.mergeArray(map.markers,path.markers);
                            map.markers = mergedMarker;
                            pathLegend = "Reading Path "+datestr;
                            if (notMapped > 0)
                                pathLegend += " " + notMapped + " not mapped";
                        }
                        break;
                    case "locate":
                        val = Request.QueryString["con"];
                        if (string.IsNullOrEmpty(val) || !int.TryParse(val, out conID))
                            throw new Exception("Invalid request");
                        locatedWaterMeterStr = "Water meter of " + SubscriberManagmentClient.GetSubscription(conID, DateTime.Now.Ticks).contractNo;
                        map = SubscriberManagmentClient.locateWaterMeter(conID, DateTime.Now.Ticks);
                        foreach (MeterMarker mrk in map.markers)
                        {
                            if (mrk.symbol == MeterMarkerSymbol.LocatedMeter)
                            {
                                l1 = mrk.lat - lat_max_zoom;
                                l2 = mrk.lat + lat_max_zoom;
                                g1 = mrk.lng - lng_max_zoom;
                                g2 = mrk.lng + lng_max_zoom;

                            }
                        }
                        break;
                    case "locate_reading":
                        val = Request.QueryString["con"];
                        if (string.IsNullOrEmpty(val) || !int.TryParse(val, out conID))
                            throw new Exception("Invalid request");
                        locatedWaterMeterStr = "Reading Location of Connection Code: " + SubscriberManagmentClient.GetSubscription(conID, DateTime.Now.Ticks).contractNo;
                        map = SubscriberManagmentClient.singleMeterHistoricalReadingMap(conID);
                        foreach (MeterMarker mrk in map.markers)
                        {
                            if (mrk.symbol == MeterMarkerSymbol.LocatedMeter)
                            {
                                l1 = mrk.lat - lat_max_zoom;
                                l2 = mrk.lat + lat_max_zoom;
                                g1 = mrk.lng - lng_max_zoom;
                                g2 = mrk.lng + lng_max_zoom;

                            }
                        }
                        break;

                }
                foreach(HtmlInputText t in wsis_data)
                    t.Value="";
                if (map != null && map.markers.Length > 0)
                {
                    List<MeterMarkerSymbol> symbols = new List<MeterMarkerSymbol>();
                    foreach (MeterMarker m in map.markers)
                    {
                        if (!symbols.Contains(m.symbol))
                            symbols.Add(m.symbol);
                    }
                    //string mapstr = SubscriberManagmentClient.builWebMapString(map);
                    string mapstr = builWebMapString(map, max);
                    int n = mapstr.Split(',').Length;
                    if (l1 == 0 && l2 == 0 && g1 == 0 && g2 == 0)
                    {
                        SubscriberManagmentClient.getMapBound(map.markers, out l1, out g1, out l2, out g2);

                        if (Math.Abs(l2 - l1) < lat_max_zoom)
                        {
                            l1 = (l1 + l2) / 2 - lat_max_zoom;
                            l2 = (l1 + l2) / 2 + lat_max_zoom;
                        }
                        if (Math.Abs(g2 - g1) < lng_max_zoom)
                        {
                            g1 = (g1 + g2) / 2 - lng_max_zoom;
                            g2 = (g1 + g2) / 2 + lng_max_zoom;
                        }
                    }
                    string lstr = "";
                    string[] imgsrc = new string[]{  "images/default.png"
                                                ,   "images/readerlocation.png"
                                                ,   "images/read.png"
                                                ,   "images/unread.png"
                                                ,   "images/selfmeter.png"
                                                ,   "images/othermeter.png"
                                                ,   "images/locatedmeter.png"
                                                ,   "images/path_read.png"
                                                ,   "images/path_unread.png"
                                                ,   "images/readingproblem.png"
                                                };
                    
                    foreach (MeterMarkerSymbol s in symbols)
                    {
                        string name = "Others";
                        switch (s)
                        {
                            case MeterMarkerSymbol.Default:
                                name = "Water Meter";
                                break;
                            case MeterMarkerSymbol.LocatedMeter:
                                name = locatedWaterMeterStr;
                                break;
                            case MeterMarkerSymbol.OtherMeter:
                                name = "Other Meters";
                                break;
                            case MeterMarkerSymbol.PathRead:
                                name = "Visted Read";
                                break;
                            case MeterMarkerSymbol.PathUnread:
                                name = "Visited Not Read";
                                break;
                            case MeterMarkerSymbol.ProblemReading1:
                                name = "Reading Problem";
                                break;
                            case MeterMarkerSymbol.ProblemReading2:
                                break;
                            case MeterMarkerSymbol.ProblemReading3:
                                break;
                            case MeterMarkerSymbol.ProblemReading4:
                                break;
                            case MeterMarkerSymbol.ProblemReading5:
                                break;
                            case MeterMarkerSymbol.Read:
                                name = "Read";
                                break;
                            case MeterMarkerSymbol.ReaderLocation:
                                name = "Your location";
                                break;
                            case MeterMarkerSymbol.SelfMeter:
                                name = allocationName;
                                break;
                            case MeterMarkerSymbol.Unread:
                                name = "Unread";
                                break;
                            case MeterMarkerSymbol.ReadStart:
                                name = "Read Start";
                                break;
                            case MeterMarkerSymbol.ReadEnd:
                                name = "Read End";
                                break;
                            default:
                                break;
                        }
                        lstr += string.Format("<div><img src='{0}'>{1}<div>", imgsrc[(int)s], name);
                    }
                    legend.InnerHtml = lstr;
                    wsis_map_bound.Value = l1 + "," + g1 + "," + l2 + "," + g2;
                    int length = mapstr.Length;
                    int i = 0;
                    int index = 0;
                    while (index < length && i < wsis_data.Length)
                    {
                        wsis_data[i].Value = mapstr.Substring(index, length - index > DATA_LENGTH ? DATA_LENGTH : length - index);
                        index += DATA_LENGTH;
                        i++;
                    }
                    while (i < wsis_data.Length)
                    {
                        wsis_data[i].Value = "";
                        i++;
                    }
                }
                else
                {
                    foreach (HtmlInputText t in wsis_data)
                        t.Value = "";

                    wsis_map_bound.Value = "-0.01,-0.-01,0.01,0.01";
                }
                if (path != null && path.markers.Length > 0)
                {
                    List<MeterMarkerSymbol> symbols = new List<MeterMarkerSymbol>();
                    foreach (MeterMarker m in map.markers)
                    {
                        if (!symbols.Contains(m.symbol))
                            symbols.Add(m.symbol);
                    }
                    string mapstr = SubscriberManagmentClient.builWebMapString(path);
                    wsis_pathdata.Value = mapstr;
                }
                else
                {
                    wsis_pathdata.Value = "";
                }
            }
            catch (Exception ex)
            {
                err.InnerText = ex.Message;
            }
        }
    }
}