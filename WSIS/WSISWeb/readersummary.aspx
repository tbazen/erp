﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="readersummary.aspx.cs" Inherits="WSISWeb.readersummary" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link rel='Stylesheet' href='page.css' />    
<title>Reader Summary</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width='100%'>
            <tr><td class='title1' >WSIS Mobile Meter Reading</td></tr>
            <tr><td class='title2' runat='server' id='welcome'></td></tr>
        <tr>
            <td class='spacer' ></td>
        </tr>
            <tr>
                <td runat='server' id='err' class='error'></td>
            </tr>
    </table>
    <div class='spacer'></div>
    <span runat="server" id='summary'></span>

        </div>
    </form>
</body>
</html>
