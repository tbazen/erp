﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using INTAPS.Payroll.Client;
using INTAPS.ClientServer;
using INTAPS.SubscriberManagment.Client;
using INTAPS.SubscriberManagment;
using INTAPS.Accounting;
using INTAPS.Evaluator;

namespace WSISWeb
{
    public partial class readersummary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sid = Session["sid"] as string;
            if (string.IsNullOrEmpty(sid))
            {
                Response.Redirect("login.aspx");
                return;
            }
            if (IsPostBack)
            {
                if (Request.Form["add"] != null)
                {
                    Response.Redirect("addreading.aspx");
                    return;
                }
                if (Request.Form["find"] != null)
                {
                    Response.Redirect("findmeter.aspx");
                    return;
                }
            }

            try
            {
                WSISWeb.Reconnect(sid);
                INTAPS.Payroll.Employee emp = PayrollClient.GetEmployeeByLoginName(Session["userName"] as string);
                if (emp == null)
                    throw new ServerUserMessage("You are are registerd WSIS user but you are not registered as employee of the Water Company");
                int currentPeriodID = SubscriberManagmentClient.ReadingPeriod.id;
                MeterReaderEmployee re = SubscriberManagmentClient.GetMeterReaderEmployees(currentPeriodID, emp.id);
                if (re == null)
                    throw new ServerUserMessage("You are a registered as employee of the Water Company but you are not assigned for meter reading");
                welcome.InnerText = "Welcome " + emp.employeeName;
                Session["readerID"] = emp.id;
                ReportDefination def = INTAPS.Accounting.Client.AccountingClient.GetReportDefinationInfo("Reading.ReaderProfile");
                string s;
                summary.InnerHtml = INTAPS.Accounting.Client.AccountingClient.EvaluateEHTML(def.id, new object[] { "rid", new EData(DataType.Int, emp.id) },out s);
            }
            catch (Exception ex)
            {
                err.InnerText = ex.Message;
            }
        }
    }
}