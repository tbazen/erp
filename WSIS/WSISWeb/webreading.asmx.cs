﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using INTAPS.ClientServer.Client;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.Accounting.Client;
using INTAPS.Payroll.Client;
using System.Runtime.Remoting;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.WSIS.Client;

namespace WSISWeb
{
    public class ReaderSetting
    {
        public int readerID;
        public int periodID;
        public String readerName;
        public String periodName;
        public String customField1="";
        public String customField2="";
        public String customField3="";
        public String customField4="";
        public void setCustomFields(String[] fields)
        {
            if (fields.Length > 0)
                customField1 = fields[0];
            else if (fields.Length > 1)
                customField2 = fields[1];
            else if (fields.Length > 2)
                customField3 = fields[2];
            else if (fields.Length > 3)
                customField4 = fields[3];
        }
        public byte[] sheetHash;
    }
    public class LoginResult
    {
        public string result;
        public bool error;
    }
    public class SetReadingResult
    {
        public bool success;
        public string message;
    }
    /// <summary>
    /// Summary description for webreading
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class webreading : System.Web.Services.WebService
    {
        [WebMethod]
        public string login(string userName, string password)
        {
            string server = System.Configuration.ConfigurationManager.AppSettings["WSISServer"];
            WSISAllClients con = new WSISAllClients(server, null);
            con.SessionID=con.applicationServer.CreateUserSession(userName, password,Environment.MachineName);
            MeterReaderEmployee emp = con.subscriberManagmentServer.GetMeterReadingEmployee(con.SessionID, userName);
            if (emp == null)
                throw new Exception("You are not a meter reading employee");
            return con.SessionID;
        }
        [WebMethod]
        public ReaderSetting getReaderSetting(string sessionID)
        {
            
            string server = System.Configuration.ConfigurationManager.AppSettings["WSISServer"];
            WSISAllClients con = new WSISAllClients(server, sessionID);
            MeterReaderEmployee emp = con.subscriberManagmentServer.getMeterReadingEmployee(sessionID);
            if (emp == null)
                return null;
            Employee e = con.peryollServer.GetEmployee(sessionID, emp.employeeID);
            if (e == null)
                return null;
            ReaderSetting rs = new ReaderSetting();
            ReadingCycle cycle = con.subscriberManagmentServer.getCurrentReadingCycle(sessionID);
            if (cycle == null)
                return null;
            rs.periodID = cycle.periodID;
            rs.periodName = con.subscriberManagmentServer.GetBillPeriod(sessionID, rs.periodID).name;
            rs.readerID = e.id;
            rs.readerName = e.employeeName;
            rs.sheetHash = cycle.hashCode;
            rs.setCustomFields( con.subscriberManagmentServer.getReadingCustomFields(sessionID));
            return rs;
        }
        [WebMethod]
        public int readingSheetSize(string sessionID, int periodID)
        {
            string server = System.Configuration.ConfigurationManager.AppSettings["WSISServer"];
            WSISAllClients con = new WSISAllClients(server, sessionID);
            return con.subscriberManagmentServer.readSheetSize(sessionID, periodID);

        }
        [WebMethod]
        public ReadingSheetRow getReadingSheetRow(string sessionID, int periodID, int index)
        {
            string server = System.Configuration.ConfigurationManager.AppSettings["WSISServer"];
            WSISAllClients con = new WSISAllClients(server, sessionID);
            return con.subscriberManagmentServer.getReadingShowRow(sessionID, periodID, index);
        }
        void filterReading(ReadingSheetRow row)
        {
            row.periodID = 2312;
        }
        [WebMethod]
        public void setReading(
                                string sessionID
                                , int connectionID,
                                int connectionStatus,
                                string contractNo,
                                string customerName,
                                int extraInfo,
                                double lat,
                                double lng,
                                string meterNo,
                                string meterType,
                                int orderN,
                                int periodID,
                                int problem,
                                int readerID,
                                double reading,
                                double readingLat,
                                double readingLng,
                                string remark,
                                int status,
                                DateTime time
            )
        {
            string server = System.Configuration.ConfigurationManager.AppSettings["WSISServer"];
            WSISAllClients con = new WSISAllClients(server, sessionID);

            ReadingSheetRow row = new ReadingSheetRow();
            row.connectionID = connectionID;
            row.connectionStatus = connectionStatus;
            row.contractNo = contractNo;
            row.customerName = customerName;
            row.extraInfo = extraInfo;
            row.lat = lat;
            row.lng = lng;
            row.meterNo = meterNo;
            row.meterType = meterType;
            row.orderN = orderN;
            row.periodID = periodID;
            row.problem = problem;
            row.readerID = readerID;
            row.reading = reading;
            row.readingLat = readingLat;
            row.readingLng = readingLng;
            row.remark = remark;
            row.status = status;
            row.time = time;
            try
            {
                //filterReading(row);
                con.subscriberManagmentServer.setReadingSheetRow(sessionID, row);
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }

    }
}