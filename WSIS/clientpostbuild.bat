copy ..\..\bERP\iCasherFrontEnd\bin\Debug\iCasherFrontend.exe ..\ClientBin
copy ..\..\bERP\iCasherFrontEnd\bin\Debug\iCasherFrontend.pdb ..\ClientBin
copy ..\..\bERP\iERPTransactionClient\bin\Debug\iERPTransactionClient.dll ..\ClientBin
copy ..\..\bERP\iERPTransactionClient\bin\Debug\iERPTransactionClient.pdb ..\ClientBin
copy ..\..\bERP\iERPTransactionClientLibrary\bin\Debug\iERPTransactionClientLibrary.dll ..\ClientBin
copy ..\..\bERP\iERPTransactionClientLibrary\bin\Debug\iERPTransactionClientLibrary.pdb ..\ClientBin
copy ..\..\Accounting\PayrollClient\bin\Debug\PayrollClient.exe ..\ClientBin
copy ..\..\Accounting\PayrollClient\bin\Debug\PayrollClient.pdb ..\ClientBin

xcopy ..\..\bERP\iCasherFrontEnd\bin\Debug\HTMLTemplate ..\ClientBin\HTMLTemplate /Y /S
copy ..\..\Administrator\bin\Debug\Administrator.exe ..\ClientBin