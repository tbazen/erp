 function registerBirthEvent(residentID)
        {
            var elFacility=document.getElementById("comboFacility");
            var facilityValue = elFacility.options[elFacility.selectedIndex].value;

            var elBirthHelp=document.getElementById("comboBirthHelp");
            var birthHelpValue = elBirthHelp.options[elBirthHelp.selectedIndex].value;

            var elBirthMarriageStatus=document.getElementById("comboBirthMarriageStatus");
            var birthMarriageStatusValue = elBirthMarriageStatus.options[elBirthMarriageStatus.selectedIndex].value;

            var elRegistererRel=document.getElementById("comboRegistererRel");
            var registererRelValue = elRegistererRel.options[elRegistererRel.selectedIndex].value;

            var elTypeOfBirth=document.getElementById("comboTypeOfBirth");
            var typeOfBirthValue = elTypeOfBirth.options[elTypeOfBirth.selectedIndex].value;


            //prepare birthevent information in JSON
            var bEventID;
            if(document.getElementById("eventType").value=="Birth")
                bEventID=document.getElementById("eventId").value;
            else
                bEventID=-1;

            var birthInfo = {
                "EventID": bEventID,
                "eventDateString":document.getElementById("birthEventDate").getAttribute("dateval"),
                "FatherID":document.getElementById("father").getAttribute("resid"),
                "MotherID":document.getElementById("mother").getAttribute("resid"),
                "EventAddress":birtheventAddress.selectedAddress,
                "ResidentID":residentID,
                "Facility":facilityValue,
                "FacilityFileNo":facilityFileNo.value,
                "BirthHelp":birthHelpValue,
                "TypeOfBirth":typeOfBirthValue,
                "Registerer":document.getElementById("registererRes").getAttribute("resid"),
                "Weight":birthWeight.value,
                "BirthMarriageStatus":birthMarriageStatusValue,
                "RegistererRel":registererRelValue
        };
        

        _proxy("registerBirthEvent", {birthEvent:birthInfo,newBirthAddress:birtheventAddress.newAddress},
                function (res) {
                    alert("Event succcessfully registered with id="+res);
                }
                , function (err) {
                    alert("Failed: " + err);
                }
        );
        needPopup.hide();
        }

function loadBirthEventEnumerations()
{
     //Load Enumeration combo fields
    _enum("comboFacility", "BirthCardProvider",-1);
    _enum("comboTypeOfBirth", "TypeOfBirth",-1);
    _enum("comboBirthHelp", "BirthHelp",-1)
    _enum("comboBirthMarriageStatus", "BirthMarriageStatus",-1);
    _enum("comboRegistererRel", "RelationShip",-1);
}
function showBirthEditInfo(eventid)
{
 //   alert("The birth Event ID is "+eventid);
     document.getElementById("eventId").value=eventid; //set event id for update
     document.getElementById("eventType").value="Birth";
    _proxy("getEvent", {eventType:"Birth",eventID:eventid},
                function (res) {

                      document.getElementById("comboBirthHelp").value=res.birthHelp;
                      document.getElementById("comboFacility").value=res.facility;
                      document.getElementById("comboTypeOfBirth").value=res.typeOfBirth;
                       document.getElementById("comboBirthMarriageStatus").value=res.birthMarriageStatus;
                       document.getElementById("comboRegistererRel").value=res.registererRel;
                       document.getElementById("father").setAttribute("resid",res.fatherID);
                       document.getElementById("father").value=res.fatherName;
                       document.getElementById("mother").setAttribute("resid",res.motherID);
                       document.getElementById("mother").value=res.motherName;
                       document.getElementById("registererRes").setAttribute("resid",res.registerer);
                       document.getElementById("registererRes").value=res.registererName;
                       calBirthDay.setDate(res.birthDate);
                       birtheventAddress.setAddress(res.birthAddress);
                       //document.getElementById("birthAddressID").value = res.birthAddress;
                       document.getElementById("facilityFileNo").value=res.facilityFileNo;
                       document.getElementById("birthWeight").value=res.weight;
                    //alert("Event succcessfully registered with id="+res);
                }
                , function (err) {
                    alert("Failed: " + err);
                }
        );
}
 function clearBirthEventData()
{
     //document.getElementById("comboFacility").value=-1;
    document.getElementById("eventType").value="NewBirth";
     _enum("comboFacility", "BirthCardProvider",-1);
}