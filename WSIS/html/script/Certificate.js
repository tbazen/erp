 function issueVSCertificate()
      {

       var issueDate=$("#certificateIssuedDate").attr("dateval");
       var referenceNo=$("#certificateReferenceNo").val();
       var description=$("#certificateComment").val();

        _proxy("issueCertificate", {certificateType:__certificateType,residentID:__resID,eventID:__certificateEventID,issuedDate:issueDate,comment:description,certificateNo:referenceNo},
                  function (res) {
                      alert("Certificate successfully issued with id="+res);
                      //window.location = "personal_info.html?sid=$model.sid&residentID=" +res+"&ticksFrom=-1";
                      location.reload();
                  }
                  , function (err) {
                      alert("Failed: " + err);
                  }
                  );
      }

  function voidCertificate(certID)
  {

        $.confirm({
            title: 'Void Certificate!',
            content: 'Are you sure you want to void the certificate?!',
            icon: 'glyphicon glyphicon-warning-sign',
            confirm: function(){
                _proxy("voidCertificate", {certificateID:certID},
                  function (res) {
                      
                      $.alert({
                            title: 'Success!',
                            content: 'Certificate successfully void!',
                            icon: 'glyphicon glyphicon-ok',
                            autoClose: 'confirm|3000',
                            confirm: function(){
                                           location.reload();
                                        },
                            cancel: function(){
                              location.reload();
                                  }
                        });
                  }
                  , function (err) {
                      alert("Failed: " + err);
                  }
                  );
            }
        });
  }
 