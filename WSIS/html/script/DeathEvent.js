 function registerDeathEvent(residentID)
        {
            var elDeathFacility=document.getElementById("comboDeathFacility");
            var facilityValue = elDeathFacility.options[elDeathFacility.selectedIndex].value;

            var elCauseOfDeath=document.getElementById("comboCauseOfDeath");
            var causeOfDeathValue = elCauseOfDeath.options[elCauseOfDeath.selectedIndex].value;

            var elIntervention=document.getElementById("comboIntervention");
            var interventionValue = elIntervention.options[elIntervention.selectedIndex].value;

            var elRelation=document.getElementById("comboApplicantRelation");
            var relationValue = elRelation.options[elRelation.selectedIndex].value;

            //prepare birthevent information in JSON
            var dEventID;
            if(document.getElementById("eventType").value=="Death")
                dEventID=document.getElementById("eventId").value;
            else
                dEventID=-1;

            var deathInfo = {
                "EventID": dEventID,
                "EventAddress":deathAddress.selectedAddress,
                "eventDateString":document.getElementById("dateOfDeath").getAttribute("dateval"),
                "RegistererID":document.getElementById("applicantName").getAttribute("resid"),
                "IndividualID":residentID,
                "Facility":facilityValue,
                "FacilityFileNo":deathFacilityNo.value,
                "CauseOfDeath":causeOfDeathValue,
                "Intervention":interventionValue,
                "RegistererRel":relationValue
        };
        

        _proxy("registerDeathEvent", {deathEvent:deathInfo,newDeathAddress:deathAddress.newAddress},
                function (res) {
                    alert("Event succcessfully registered with id="+res);
                }
                , function (err) {
                    alert("Failed: " + err);
                }
        );
        needPopup.hide();
        }

function loadDeathEventEnumerations()
{
     //Load Enumeration combo fields
    _enum("comboDeathFacility", "DeathCardProvider",-1);
    _enum("comboCauseOfDeath", "CauseOfDeath",-1);
    _enum("comboIntervention", "Intervention",-1);
    _enum("comboApplicantRelation", "Relationship",-1);
}
function showDeathEditInfo(eventid)
{
 //   alert("The birth Event ID is "+eventid);
     document.getElementById("eventId").value=eventid; //set event id for update
     document.getElementById("eventType").value="Death";
    _proxy("getEvent", {eventType:"Death",eventID:eventid},
                function (res) {

                     document.getElementById("comboIntervention").value=res.intervention;
                     document.getElementById("comboDeathFacility").value=res.facility;
                     document.getElementById("comboCauseOfDeath").value=res.causeOfDeath;
                     document.getElementById("comboApplicantRelation").value=res.registererRel;
                     document.getElementById("comboRegistererRel").value=res.registererRel;
                     document.getElementById("applicantName").setAttribute("resid",res.registererID);
                     document.getElementById("applicantName").value=res.applicantName;
                     calDateOfDeath.setDate(res.dateOfDeath);
                     deathAddress.setAddress(res.deathAddress);
                     document.getElementById("deathFacilityNo").value = res.facilityFileNo;
                    //alert("Event succcessfully registered with id="+res);
                }
                , function (err) {
                    alert("Failed: " + err);
                }
        );
}
 function clearDeathEventData()
{
    document.getElementById("eventType").value="NewDeath";
}