 function registerDivorceEvent(residentID)
        {
            var elTypeOfdivorce=document.getElementById("comboTypeOfDivorce");
            var typeOfDivValue = elTypeOfdivorce.options[elTypeOfdivorce.selectedIndex].value;

            var elCauseOfDivorce=document.getElementById("comboCauseOfDivorce");
            var causeOfDivValue = elCauseOfDivorce.options[elCauseOfDivorce.selectedIndex].value;

            //prepare divorce event information in JSON
            var mEventID;
            if(document.getElementById("eventType").value=="Divorce")
                mEventID=document.getElementById("eventId").value;
            else
                mEventID=-1;
            
            var husbandID;
            var wifeID;
            if(__residentSex=="Male")
            {
              husbandID=residentID;
              wifeID=document.getElementById("divorcePartner").getAttribute("resid");
            }
            else
            {
              wifeID=residentID;
              husbandID=document.getElementById("divorcePartner").getAttribute("resid");
            }


            var divorceInfo = {
                "EventID": mEventID,
                "eventDateString":document.getElementById("dateOfDivorce").getAttribute("dateval"),
                "HusbandID":husbandID,
                "WifeID":wifeID,
                "TypeOfDivorce":typeOfDivValue,
                "DivorceReason":causeOfDivValue,
                "NoOfChilds":noOfChildren.value,
                "MarriageAge":marriageAge.value,
                "CourtFileNo":courtFileNo.value,
                "NameOfCourt":courtName.value,
                "NameOfCourt_A":courtName_A.value,
                "Registerer1":document.getElementById("d_Witness1").getAttribute("resid"),
                "RegistererDate1String":document.getElementById("d_Witness1Date").getAttribute("dateval"),
                "Registerer2":document.getElementById("d_Witness2").getAttribute("resid"),
                "RegistererDate2String":document.getElementById("d_Witness2Date").getAttribute("dateval"),
                "Registerer3":document.getElementById("d_Witness3").getAttribute("resid"),
                "RegistererDate3String":document.getElementById("d_Witness3Date").getAttribute("dateval"),
                "Registerer4":document.getElementById("d_Witness4").getAttribute("resid"),
                "RegistererDate4String":document.getElementById("d_Witness4Date").getAttribute("dateval")
        };
        

        _proxy("registerDivorceEvent", {divorceEvent:divorceInfo},
                function (res) {
                    alert("Event succcessfully registered with id="+res);
                }
                , function (err) {
                    alert("Failed: " + err);
                }
        );
        needPopup.hide();
        }

function loadDivorceEventEnumerations()
{
     //Load Enumeration combo fields
    _enum("comboTypeOfDivorce", "TypeOfDivorce",-1);
    _enum("comboCauseOfDivorce", "CauseOfDivorce",-1);
}
function showDivorceEditInfo(eventid)
{
 //   alert("The birth Event ID is "+eventid);
     document.getElementById("eventId").value=eventid; //set event id for update
     document.getElementById("eventType").value="Divorce";
    _proxy("getEvent", {eventType:"Divorce",eventID:eventid},
                function (res) {

   
      var partnerID;
      var partnerName;
      if(__residentSex=="Male")
      {
        partnerID=res.wifeID;
        partnerName=res.wifeName;
      }
      else
      {
        partnerID=res.husbandID;
        partnerName=res.husbandName;
      }
       document.getElementById("comboTypeOfDivorce").value=res.typeOfDivorce;
       document.getElementById("comboCauseOfDivorce").value=res.causeOfDivorce;
       document.getElementById("divorcePartner").setAttribute("resid", partnerID);
       document.getElementById("divorcePartner").value=partnerName;
       document.getElementById("d_Witness1").setAttribute("resid",res.witness1ID);
       document.getElementById("d_Witness1").value=res.witness1;
       document.getElementById("d_Witness2").setAttribute("resid",res.witness2ID);
       document.getElementById("d_Witness2").value=res.witness2;
       document.getElementById("d_Witness3").setAttribute("resid",res.witness3ID);
       document.getElementById("d_Witness3").value=res.witness3;
       document.getElementById("d_Witness4").setAttribute("resid",res.witness4ID);
       document.getElementById("d_Witness4").value=res.witness4;
       calDateOfDivorce.setDate(res.dateOfDivorce);
       document.getElementById("marriageAge").value=res.marriageAge;
       document.getElementById("courtFileNo").value=res.courtFileNo;
       document.getElementById("courtName").value=res.nameOfCourt;
       document.getElementById("courtName_A").value=res.nameOfCourt_A;
       document.getElementById("noOfChildren").value=res.noOfChildren;
       cal_d_registererDate1.setDate(res.witness1Date);
       cal_d_registererDate2.setDate(res.witness2Date);
       cal_d_registererDate3.setDate(res.witness3Date);
       cal_d_registererDate4.setDate(res.witness4Date);

    //alert("Event succcessfully registered with id="+res);
          }
          , function (err) {
              alert("Failed: " + err);
          }
  );
}
 function clearDivorceEventData()
{
    document.getElementById("eventType").value="NewDivorce";
}