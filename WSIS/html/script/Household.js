 function registerHousehold(residentID)
        {
            
            var houseHoldInfo = {
                "head1": document.getElementById("household_head1").getAttribute("resid"),
                "head2": document.getElementById("household_head2").getAttribute("resid"),
                "Address": householdAddress.selectedAddress
                };
        

        _proxy("registerHousehold", {resID:residentID, houseHold:houseHoldInfo,newHouseholdAddress:householdAddress.newAddress},
                function (res) {
                    alert("Household successfully registered with id="+res);
                    needPopup.hide();
                   // window.location = "personal_info.html?sid=$model.sid&residentID=" +residentID+"&ticksFrom=-1";
                }
                , function (err) {
                    alert("Failed: " + err);
                }
        );
        needPopup.hide();
        }

function loadHouseHoldEnumerations()
{
     //Load Enumeration combo fields
    _enum("comboHouseHoldRoles", "HouseHoldRole",-1);
}
function addHouseholdMember(houseHoldID)
{

  var elRole=document.getElementById("comboHouseHoldRoles");
  var roleValue = elRole.options[elRole.selectedIndex].value;

  var memberInfo = {
                "HouseholdID": houseHoldID,
                "ResidentID": document.getElementById("household_memeber").getAttribute("resid"),
                "Role": roleValue
                };

        _proxy("addHouseholdMember", {member:memberInfo},
                function (res) {
                    alert("Member successfully added");
                    //refresh household section  
                   // refreshHouseholdSection();

                }
                , function (err) {
                    alert("Failed: " + err);
                }
        );
       needPopup.hide();
}
function refreshHouseholdSection()
{
    _proxy("getHouseholdMembers", {residentID:__resID},
                function (res) {
                   // alert("Member successfully added");
                    //refresh household section  
                 $("#membersList").empty();
                 var members=res.members;    
                for(var i=0;i<members.length;i++)
                {
                    var rowElement= $("<tr>");
                    if(members[i].isHouseHead)
                        rowElement.attr('class',"household-head");
                    var noEl=$("<td>");
                    noEl.text(members[i].no);
                    var nameEl=$("<td>");
                    nameEl.append("<a href=\"personal_info.html?sid="+__sessionID+"&residentID="+members[i].residentID+"&ticksFrom=-1\"><span>"+members[i].name+"</span></a>");
                    var roleEl=$("<td>");
                    roleEl.text(members[i].role);
                    var resNoEl=$("<td>");
                    resNoEl.text(members[i].residentNo);
                    var statusEl=$("<td>");
                    statusEl.text(members[i].status);
                    var delEl=$("<td>");
                    delEl.append("<a href=\"#\"><img src=\"Images/delete.png\" onclick=\"removeMember("+members[i].residentID+","+res.houseHoldID+") /></a>");
                   rowElement.append(noEl,nameEl,roleEl,resNoEl,statusEl,delEl);
                   $("#membersList").append(rowElement);
                }

                }
                , function (err) {
                    alert("Failed: " + err);
                }
        );
}

function removeMember(residentID, houseHoldID)
{ 
    $.confirm({
            title: 'Remove Member!',
            content: 'Are you sure you want to remove the household member?!',
            icon: 'glyphicon glyphicon-warning-sign',
            confirm: function(){
               _proxy("removeHouseholdMember", {resID:residentID,householdID:houseHoldID},
                function (res) {
                      $.alert({
                            title: 'Success!',
                            content: 'Member successfully removed!',
                            icon: 'glyphicon glyphicon-ok',
                            autoClose: 'confirm|1000',
                            confirm: function(){
                                           refreshHouseholdSection();
                                        },
                            cancel: function(){
                              refreshHouseholdSection();
                                  }
                        });

                }
                , function (err) {
                    alert("Failed: " + err);
                }
        );
            }
        });
}

