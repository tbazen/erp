 function registerMarriageEvent(residentID)
        {
            var elTypeOfMarriage=document.getElementById("comboTypeOfMarriage");
            var typeOfMarrValue = elTypeOfMarriage.options[elTypeOfMarriage.selectedIndex].value;

            var elProvider=document.getElementById("comboMarriageProvider");
            var providerValue = elProvider.options[elProvider.selectedIndex].value;

            //prepare marriage event information in JSON
            var mEventID;
            if(document.getElementById("eventType").value=="Marriage")
                mEventID=document.getElementById("eventId").value;
            else
                mEventID=-1;
            
            var groomID;
            var brideID;
            if(__residentSex=="Male")
            {
              groomID=residentID;
              brideID=document.getElementById("marriagePartner").getAttribute("resid");
            }
            else
            {
              brideID=residentID;
              groomID=document.getElementById("marriagePartner").getAttribute("resid");
            }



            var marriageInfo = {
                "EventID": mEventID,
                "eventDateString":document.getElementById("dateOfMarriage").getAttribute("dateval"),
                "GroomID":groomID,
                "BrideID":brideID,
                "TypeOfMarriage":typeOfMarrValue,
                "Provider":providerValue,
                "PlaceOfMarriage":placeOfMarriage.value,
                "Registerer1":document.getElementById("m_Witness1").getAttribute("resid"),
                "RegistererDate1String":document.getElementById("m_Witness1SignedDate").getAttribute("dateval"),
                "Registerer2":document.getElementById("m_Witness2").getAttribute("resid"),
                "RegistererDate2String":document.getElementById("m_Witness2SignedDate").getAttribute("dateval"),
                "Registerer3":document.getElementById("m_Witness3").getAttribute("resid"),
                "RegistererDate3String":document.getElementById("m_Witness3SignedDate").getAttribute("dateval"),
                "Registerer4":document.getElementById("m_Witness4").getAttribute("resid"),
                "RegistererDate4String":document.getElementById("m_Witness4SignedDate").getAttribute("dateval")
        };
        

        _proxy("registerMarriageEvent", {marriageEvent:marriageInfo},
                function (res) {
                    alert("Event succcessfully registered with id="+res);
                }
                , function (err) {
                    alert("Failed: " + err);
                }
        );
        needPopup.hide();
        }

function loadMarriageEventEnumerations()
{
     //Load Enumeration combo fields
    _enum("comboTypeOfMarriage", "TypeOfMarriage",-1);
    _enum("comboMarriageProvider", "MarriageProvider",-1);
}
function showMarriageEditInfo(eventid)
{
 //   alert("The birth Event ID is "+eventid);
     document.getElementById("eventId").value=eventid; //set event id for update
     document.getElementById("eventType").value="Marriage";
    _proxy("getEvent", {eventType:"Marriage",eventID:eventid},
                function (res) {

      var partnerID;
      var partnerName;
      if(__residentSex=="Male")
      {
        partnerID=res.brideID;
        partnerName=res.brideName;
      }
      else
      {
        partnerID=res.groomID;
        partnerName=res.groomName;
      }
       document.getElementById("comboTypeOfMarriage").value=res.typeOfMarriage;
       document.getElementById("comboMarriageProvider").value=res.provider;
       document.getElementById("marriagePartner").setAttribute("resid", partnerID);
       document.getElementById("marriagePartner").value=partnerName;
       document.getElementById("m_Witness1").setAttribute("resid",res.witness1ID);
       document.getElementById("m_Witness1").value=res.witness1;
       document.getElementById("m_Witness2").setAttribute("resid",res.witness2ID);
       document.getElementById("m_Witness2").value=res.witness2;
       document.getElementById("m_Witness3").setAttribute("resid",res.witness3ID);
       document.getElementById("m_Witness3").value=res.witness3;
       document.getElementById("m_Witness4").setAttribute("resid",res.witness4ID);
       document.getElementById("m_Witness4").value=res.witness4;
       calDateOfMarriage.setDate(res.dateOfMarriage);
       document.getElementById("placeOfMarriage").value=res.placeOfMarriage;
       cal_m_registererDate1.setDate(res.witness1Date);
       cal_m_registererDate2.setDate(res.witness2Date);
       cal_m_registererDate3.setDate(res.witness3Date);
       cal_m_registererDate4.setDate(res.witness4Date);
    //alert("Event succcessfully registered with id="+res);
          }
          , function (err) {
              alert("Failed: " + err);
          }
  );
}
 function clearMarriageEventData()
{
    document.getElementById("eventType").value="NewMarriage";
}