 function registerMigrationEvent(residentID)
        {
            var elReason=document.getElementById("comboMigrationReason");
            var reasonValue = elReason.options[elReason.selectedIndex].value;

            var elDirection=document.getElementById("comboDirection");
            var directionID = elDirection.options[elDirection.selectedIndex].id;
           
           var direction;
           if(directionID=="Migrate")
            direction=1;
           else
             direction=0;
            

            //prepare migrationevent information in JSON
            var migEventID;
            if(document.getElementById("eventType").value=="Migration")
                migEventID=document.getElementById("eventId").value;
            else
                migEventID=-1;

            var migrationInfo = {
                "EventID": migEventID,
                "eventDateString":document.getElementById("dateOfMigration").getAttribute("dateval"),
                "SourceDestination":migrationAddress.selectedAddress,
                "IndividualID":residentID,
                "ReasonType":reasonValue,
                "ReasonNote":migrationReasonNote.value,
                "Direction": direction
        };
        
        _proxy("registerMigrationEvent", {migrationEvent:migrationInfo,newMigrationAddress:migrationAddress.newAddress},
                function (res) {
                    alert("Event succcessfully registered with id="+res);
                }
                , function (err) {
                    alert("Failed: " + err);
                }
        );
        needPopup.hide();
        }

function loadMigrationEventEnumerations()
{
     //Load Enumeration combo fields
    _enum("comboMigrationReason", "MigrationReason",-1);
}

function showMigrationEditInfo(eventid)
{
 //   alert("The birth Event ID is "+eventid);
     document.getElementById("eventId").value=eventid; //set event id for update
     document.getElementById("eventType").value="Migration";
    _proxy("getEvent", {eventType:"Migration",eventID:eventid},
                function (res) {

                  document.getElementById("comboMigrationReason").value=res.migrationReason;
                   calDateOfMigration.setDate(res.migrationDate);
                   migrationAddress.setAddress(res.migrationAddress);
                   document.getElementById("migrationReasonNote").value=res.reasonNote;
                   if(res.direction=="Migrate")
                    comboDirection.selectedIndex=0;
                  else
                    comboDirection.selectedIndex=1;
                }
                , function (err) {
                    alert("Failed: " + err);
                }
        );
}
 function clearMigrationEventData()
{
     //document.getElementById("comboFacility").value=-1;
    document.getElementById("eventType").value="NewMigration";
}