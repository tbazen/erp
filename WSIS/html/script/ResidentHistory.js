function createBirthEventSection(eventData)
{
    
    var $birthSectionDiv= $("<div>", {id: "b_event", class: "center_title"});
    var $birthSectionImg = $('<img>');
    $birthSectionImg.attr('src', "Images/babycart2.png")
  
    var birthInfoId="birth_"+eventData.birthEventID;
    var $birthInfoDiv= $("<div>", {id: birthInfoId, class: "birth_informatation"});

    var $birthTitle = $("<a>", { href: "javascript:ReverseDisplay('birth_" + eventData.birthEventID + "','b_event')" });
    $birthTitle.text('BIRTH INFORMATION');

    var $birthEdit= $("<a>", {id: "b-edit",href: "#",class:"biobtn"});
    $birthEdit.attr('data-needpopup-show',"#birth_event_registration");
    $birthEdit.text('EDIT');
   if(eventData.issued)
        $birthEdit.hide();
    else
        $birthEdit.show();

    var $birthIssue= $("<a>", {id: "issu-b-card",href: "#",class:"biobtn issubtn"});
    $birthIssue.attr('data-needpopup-show',"#birth_event_certeficate");
    $birthIssue.attr('onclick',"loadIssueFrame('"+eventData.url+"',"+eventData.certificateID+","+"'"+eventData.certificateType+"'"+","+eventData.birthEventID+")");
    $birthIssue.text(eventData.certificateStatus);
    if(eventData.showButton)
        $birthIssue.show();
    else
        $birthIssue.hide();
    $birthSectionDiv.append($birthSectionImg,$birthTitle,$birthEdit,$birthIssue);

   
    var $birthRowDiv= $("<div>", {class:"row"});

    $birthInfoDiv.append($birthRowDiv)
    var $birthcol1Div= $("<div>", {class:"col-sm-5 col-md-6"});
    $birthRowDiv.append($birthcol1Div)
    var $birthTable1= $("<Table>",{id:"birthTable1"});
    $birthcol1Div.append($birthTable1)

    var $birthcol2Div= $("<div>", {class:"col-sm-5 col-md-6"});
    var $birthTable2= $("<Table>",{id:"birthTable2"});
    $birthcol2Div.append($birthTable2);
    $birthRowDiv.append($birthcol2Div);

    var birthEventID=eventData.birthEventID;
    $birthEdit.attr('onclick',"showBirthEditInfo("+birthEventID+")");
    //$("#"+__lastElement.id).after($birthSectionDiv,$birthInfoDiv);
     $("#"+$(__lastElement)[0].id).after($birthSectionDiv,$birthInfoDiv);
    
    //__lastElement.after($birthSectionDiv,$birthInfoDiv);
    __lastElement=$birthInfoDiv;
    //insert content
    
    $birthTable1.append("<tr> \
                        <td><span  class=\"t-left\">BIRTH DATE:</span>"+ eventData.birthDate+" </td> \
                    </tr> \
                     <tr> \
                        <td><span class=\"t-left\">MOTHER NAME:</span>"+ eventData.mother+" </td> \
                    </tr> \
                        <tr> \
                        <td><span class=\"t-left\">FACILITY:</span>"+ eventData.facility+" </td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">FACILITY FILE ID:</span>"+ eventData.facilityFileNo+"</td> \
                    </tr> \
                        <td><span class=\"t-left\">BIRTH HELP:</span>"+eventData.birthHelp+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">TYPE OF BIRTH:</span>"+eventData.typeOfBirth+"</td> \
                    </tr>");
    $birthTable2.append(" <tr> \
                        <td><span class=\"t-left\">WEIGHT:</span>"+ eventData.weight+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">B-MARRIAGE STATUS:</span>"+eventData.birthMarriageStatus+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">REGISTERER:</span>"+eventData.registerer+"</td> \
                    </tr> \
                    <tr> \
                        <td><span  class=\"t-left\">REGISTERER DATE:</span>"+eventData.registererDate+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">REGISTERER RELATION:</span>"+eventData.registererRel+"</td> \
                    </tr>");

}

function createMarriageEventSection(eventData)
{
    var $marriageSectionDiv= $("<div>", {id: "m_event", class: "center_title"});
   
    var $marriageSectionImg = $('<img>');
    $marriageSectionImg.attr('src', "Images/marrege_ring2.png")

    var marriageInfoId="marriage_"+eventData.marriageEventID;
    var $marriageInfoDiv= $("<div>", {id: marriageInfoId, class: "marrige_inforamtion"});

    var $marriageTitle = $("<a>", { href: "javascript:ReverseDisplay('marriage_" + eventData.marriageEventID + "','m_event')" });
    $marriageTitle.text('MARITAL INFORMATION');

    var $marriageEdit= $("<a>", {id: "m-edit",href: "#",class:"biobtn"});
    $marriageEdit.attr('data-needpopup-show',"#marrige_event_registration");
    $marriageEdit.text('EDIT');
    if(eventData.issued)
        $marriageEdit.hide();
    else
        $marriageEdit.show();

    var $marriageIssue= $("<a>", {id: "issue-m-certeficate",href: "#",class:"biobtn issubtn"});
    $marriageIssue.attr('data-needpopup-show',"#birth_event_certeficate");
    $marriageIssue.attr('onclick',"loadIssueFrame('"+eventData.url+"',"+eventData.certificateID+","+"'"+eventData.certificateType+"'"+","+eventData.marriageEventID+")");
    $marriageIssue.text(eventData.certificateStatus);
    if(eventData.showButton)
        $marriageIssue.show();
    else
        $marriageIssue.hide();

    $marriageSectionDiv.append($marriageSectionImg,$marriageTitle,$marriageEdit,$marriageIssue);

    var $marriageRowDiv= $("<div>", {class:"row"});

    $marriageInfoDiv.append($marriageRowDiv)
    var $marriagecol1Div= $("<div>", {class:"col-sm-5 col-md-6"});
    $marriageRowDiv.append($marriagecol1Div)
    var $marriageTable1= $("<Table>",{id:"marriageTable1"});
    $marriagecol1Div.append($marriageTable1)

    var $marriagecol2Div= $("<div>", {class:"col-sm-5 col-md-6"});
    var $marriageTable2= $("<Table>",{id:"marriageTable2"});
    $marriagecol2Div.append($marriageTable2);
    $marriageRowDiv.append($marriagecol2Div);

    var marriageEventID=eventData.marriageEventID;
    $marriageEdit.attr('onclick',"showMarriageEditInfo("+marriageEventID+")");

    //$("#"+__lastElement.id).after($marriageSectionDiv,$marriageInfoDiv);
     $("#"+$(__lastElement)[0].id).after($marriageSectionDiv,$marriageInfoDiv);
    //__lastElement.after($marriageSectionDiv,$marriageInfoDiv);
    __lastElement=$marriageInfoDiv;
    //insert content
    $marriageTable1.append("<tr> \
                        <td><span class=\"t-left\">BRIDE/GROOM NAME:</span><a href=\"personal_info.html?sid="+__sessionID+"&residentID="+eventData.residentNo+"&ticksFrom=-1\">"+eventData.partnerName+"</a></td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">BRIDE/GROOM RESIDENT NO:</span>"+eventData.residentNo+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">TYPE OF MARRIAGE:</span>"+eventData.typeOfMarriage+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">PLACE OF MARRIAGE:</span>"+eventData.placeOfMarriage+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">MARRIAGE PROVIDER:</span>"+eventData.provider+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">DATE OF MARRIAGE:</span>"+eventData.dateOfMarriage+"</td> \
                    </tr>");
    $marriageTable2.append(" <tr> \
                        <td><span class=\"t-left\">WITNESS 1:</span>"+eventData.witness1+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">WITNESS 1 SIGNED DATE:</span>"+eventData.witness1Date+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">WITNESS 2:</span>"+eventData.witness2+"</td> \
                    <tr> \
                        <td><span class=\"t-left\">WITNESS 2 SIGNED DATE:</span>"+eventData.witness2Date+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">WITNESS 3:</span>"+eventData.witness3+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">WITNESS 3 SIGNED DATE:</span>"+eventData.witness3Date+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">WITNESS 4:</span>"+eventData.witness4+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">WITNESS 4 SIGNED DATE:</span>"+eventData.witness4Date+"</td> \
                    </tr>");
            
}
function createDivorceEventSection(eventData)
{
    var $divorceSectionDiv= $("<div>", {id: "divorce_event", class: "center_title"});
   
    var $divorceSectionImg = $('<img>');
    $divorceSectionImg.attr('src', "Images/divorce_event2.png")


    var divorceInfoId="divorce_"+eventData.divorceEventID;
    var $divorceInfoDiv= $("<div>", {id: divorceInfoId, class: "divorce_information"});

    var $divorceTitle = $("<a>", { href: "javascript:ReverseDisplay('divorce_" + eventData.divorceEventID + "','divorce_event')" });
    $divorceTitle.text('DIVORCE INFORMATION');

    var $divorceEdit= $("<a>", {id: "divorce-edit",href: "#",class:"biobtn"});
    $divorceEdit.attr('data-needpopup-show',"#divorce_event_registration");
    $divorceEdit.text('EDIT');
   if(eventData.issued)
        $divorceEdit.hide();
    else
        $divorceEdit.show();

    var $divorceIssue= $("<a>", {id: "issue-divorce-cereteficate",href: "#",class:"biobtn issubtn"});
    $divorceIssue.attr('data-needpopup-show',"#birth_event_certeficate");
    $divorceIssue.attr('onclick',"loadIssueFrame('"+eventData.url+"',"+eventData.certificateID+","+"'"+eventData.certificateType+"'"+","+eventData.divorceEventID+")");
    $divorceIssue.text(eventData.certificateStatus);
    if(eventData.showButton)
        $divorceIssue.show();
    else
        $divorceIssue.hide();

    $divorceSectionDiv.append($divorceSectionImg,$divorceTitle,$divorceEdit,$divorceIssue);

    var $divorceRowDiv= $("<div>", {class:"row"});

    $divorceInfoDiv.append($divorceRowDiv)
    var $divorcecol1Div= $("<div>", {class:"col-sm-5 col-md-6"});
    $divorceRowDiv.append($divorcecol1Div)
    var $divorceTable1= $("<Table>",{id:"divorceTable1"});
    $divorcecol1Div.append($divorceTable1)

    var $divorcecol2Div= $("<div>", {class:"col-sm-5 col-md-6"});
    var $divorceTable2= $("<Table>",{id:"divorceTable2"});
    $divorcecol2Div.append($divorceTable2);
    $divorceRowDiv.append($divorcecol2Div);

    var divorceEventID=eventData.divorceEventID;
    $divorceEdit.attr('onclick',"showDivorceEditInfo("+divorceEventID+")");
    
     $("#"+$(__lastElement)[0].id).after($divorceSectionDiv,$divorceInfoDiv);
    __lastElement=$divorceInfoDiv;
    //insert content
    $divorceTable1.append(" <tr> \
                        <td><span class=\"t-left\">HUSBAND/WIFE NAME:</span><a href=\"personal_info.html?sid="+__sessionID+"&residentID="+eventData.residentNo+"&ticksFrom=-1\">"+eventData.partnerName+"</a></td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">HUSBAND/WIFE RESIDENT NO:</span>"+eventData.residentNo+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">DATE OF DIVORCE:</span>"+eventData.dateOfDivorce+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">TYPE OF DIVORCE:</span>"+eventData.typeOfDivorce+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">REASON OF DIVORCE:</span>"+eventData.divorceReason+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">MARRIAGE AGE:</span>"+eventData.marriageAge+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">NUMBER OF CHILD:</span>"+eventData.noOfChildren+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">COURT NAME:</span>"+eventData.nameOfCourt+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">COURT FILE NO:</span>"+eventData.courtFileNo+"</td> \
                    </tr> \
                    <tr>");
    $divorceTable2.append("<tr> \
                        <td><span class=\"t-left\">WITNESS 1:</span>"+eventData.divWitness1+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">WITNESS 1 SIGNED DATE:</span>"+eventData.divWitness1Date+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">WITNESS 2:</span>"+eventData.divWitness2+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">WITNESS 2 SIGNED DATE:</span>"+eventData.divWitness2Date+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">WITNESS 3:</span>"+eventData.divWitness3+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">WITNESS 3 SIGNED DATE:</span>"+eventData.divWitness3Date+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">WITNESS 4:</span>"+eventData.divWitness4+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">WITNESS 4 SIGNED DATE:</span>"+eventData.divWitness4Date+"</td> \
                    </tr>");
}
function createMigrationEventSection(eventData)
{
    var $migrationSectionDiv= $("<div>", {id: "migration_event", class: "center_title"});
   
    var $migrationSectionImg = $('<img>');
    $migrationSectionImg.attr('src', "Images/migration2.png")

    var migrationInfoId="migration_"+eventData.migrationEventID;
    var $migrationInfoDiv= $("<div>", {id: migrationInfoId, class: "migration_information"});

    var $migrationTitle = $("<a>", { href: "javascript:ReverseDisplay('migration_" + eventData.migrationEventID + "','migration_event')" });
    $migrationTitle.text('MIGRATION INFORMATION');

    var $migrationEdit= $("<a>", {id: "edit-migration",href: "#",class:"biobtn"});
    $migrationEdit.attr('data-needpopup-show',"#migration_event_registration");
    $migrationEdit.text('EDIT');

     if(eventData.issued)
            $migrationEdit.hide();
        else
            $migrationEdit.show();

    var $migrationIssue= $("<a>", {id: "issue-migration-certeficate",href: "#",class:"biobtn issubtn"});
     $migrationIssue.attr('data-needpopup-show',"#birth_event_certeficate");
    $migrationIssue.attr('onclick',"loadIssueFrame('"+eventData.url+"',"+eventData.certificateID+","+"'"+eventData.certificateType+"'"+","+eventData.migrationEventID+")");
    $migrationIssue.text(eventData.certificateStatus);
    if(eventData.hideIssueButton)
        $migrationIssue.hide();
    else
        $migrationIssue.show();


    $migrationSectionDiv.append($migrationSectionImg,$migrationTitle,$migrationEdit,$migrationIssue);

    var $migrationRowDiv= $("<div>", {class:"row"});

    $migrationInfoDiv.append($migrationRowDiv)
    var $migrationcol1Div= $("<div>", {class:"col-sm-5 col-md-6"});
    $migrationRowDiv.append($migrationcol1Div)
    var $migrationTable1= $("<Table>",{id:"migrationTable1"});
    $migrationcol1Div.append($migrationTable1)

    var $migrationcol2Div= $("<div>", {class:"col-sm-5 col-md-6"});
    var $migrationTable2= $("<Table>",{id:"migrationTable2"});
    $migrationcol2Div.append($migrationTable2);
    $migrationRowDiv.append($migrationcol2Div);

    var migrationEventID=eventData.migrationEventID;
    $migrationEdit.attr('onclick',"showMigrationEditInfo("+migrationEventID+")");

    $("#"+$(__lastElement)[0].id).after($migrationSectionDiv,$migrationInfoDiv);
    __lastElement=$migrationInfoDiv;

    //insert content
    $migrationTable1.append("<tr> \
                        <td><span class=\"t-left\">DATE OF MIGRATION:</span>"+eventData.migrationDate+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">TO/FROM ADDRESS:</span>"+eventData.migrationAddress+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">MIGRATION REASON:</span>"+eventData.migrationReason+"</td> \
                    </tr>");
    $migrationTable2.append(" <tr> \
                        <td><span class=\"t-left\">STATUS:</span>"+eventData.direction+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">REASON NOTE:</span>"+eventData.reasonNote+"</td> \
                    </tr>");
}

function createDeathEventSection(eventData)
{  
    var $deathSectionDiv= $("<div>", {id: "death_event", class: "center_title"});
   
    var $deathSectionImg = $('<img>');
    $deathSectionImg.attr('src', "Images/death_event2.png")

    var deathInfoId="death_"+eventData.deathEventID;
    var $deathInfoDiv= $("<div>", {id: deathInfoId, class: "death_information"});

    var $deathTitle = $("<a>", { href: "javascript:ReverseDisplay('death_" + eventData.deathEventID + "','death_event')" });
    $deathTitle.text('DEATH INFORMATION');

    var $deathEdit= $("<a>", {id: "edit-death",href: "#",class:"biobtn"});
    $deathEdit.attr('data-needpopup-show',"#death_event_registration");
    $deathEdit.text('EDIT');
    if(eventData.issued)
        $deathEdit.hide();
    else
        $deathEdit.show();

    var $deathIssue= $("<a>", {id: "issue-death-certeficate",href: "#",class:"biobtn issubtn"});
    $deathIssue.attr('data-needpopup-show',"#birth_event_certeficate");
    $deathIssue.attr('onclick',"loadIssueFrame('"+eventData.url+"',"+eventData.certificateID+","+"'"+eventData.certificateType+"'"+","+eventData.deathEventID+")");
    $deathIssue.text(eventData.certificateStatus);


    $deathSectionDiv.append($deathSectionImg,$deathTitle,$deathEdit,$deathIssue);

    
    var $deathRowDiv= $("<div>", {class:"row"});

    $deathInfoDiv.append($deathRowDiv)
    var $deathcol1Div= $("<div>", {class:"col-sm-5 col-md-6"});
    $deathRowDiv.append($deathcol1Div)
    var $deathTable1= $("<Table>",{id:"deathTable1"});
    $deathcol1Div.append($deathTable1)

    var $deathcol2Div= $("<div>", {class:"col-sm-5 col-md-6"});
    var $deathTable2= $("<Table>",{id:"deathTable2"});
    $deathcol2Div.append($deathTable2);
    $deathRowDiv.append($deathcol2Div);

    var deathEventID=eventData.deathEventID;
    $deathEdit.attr('onclick',"showDeathEditInfo("+deathEventID+")");

    $("#"+$(__lastElement)[0].id).after($deathSectionDiv,$deathInfoDiv);
    __lastElement=$deathInfoDiv;

     $deathTable1.append("<tr> \
                                    <td><span class=\"t-left\">CAUSE OF DEATH:</span>"+eventData.causeOfDeath+"</td> \
                                </tr> \
                                <tr> \
                                    <td><span class=\"t-left\">DATE OF DEATH:</span>"+eventData.dateOfDeath+"</td> \
                                </tr> \
                                <tr> \
                                    <td><span class=\"t-left\">FACILITY:</span>"+eventData.facility+"</td> \
                                </tr> \
                                <tr> \
                                    <td><span class=\"t-left\">INTERVENTION:</span>"+eventData.intervention+"</td> \
                                </tr>");
    $deathTable2.append("<tr> \
                        <td><span class=\"t-left\">APPLICANT:</span>"+eventData.registerer+"</td> \
                    </tr> \
                    <tr> \
                        <td><span class=\"t-left\">APPLICANT RELATION:</span>"+eventData.registererRel+"</td> \
                    </tr>");

}



function _buildHistory(resID, afterElementId)
{
     __lastElement=document.getElementById(afterElementId);

       _proxy("getEvents"
       , { residentID: resID}
       , function (ret) {
           
           //populate history
            $("#b_event").remove();
            $("#birth-info").remove();
            $("#m_event").remove();
            $("#marrige-info").remove();
            $("#divorce_event").remove();
            $("#divorce-info").remove();
            $("#migration_event").remove(); 
            $("#migration-info").remove();
            $("#death_event").remove(); 
            $("#death-info").remove();
        for (var i = 0; i < ret.length; i++) 
        {
            if(ret[i].eventType=="Birth") //birth event
            {
                createBirthEventSection(ret[i]);
            }
            else if(ret[i].eventType=="Marriage") 
            {
                createMarriageEventSection(ret[i]);
            }
            else if(ret[i].eventType=="Divorce") 
            {
               createDivorceEventSection(ret[i]);
            }
             else if(ret[i].eventType=="Migration") 
            {
               createMigrationEventSection(ret[i]);
            }
             else if(ret[i].eventType=="Death") 
            {
                createDeathEventSection(ret[i]);
            }
        }
    }
       , function (err) {
           alert("Error loading resident\n" + err);
       }
       );
}


