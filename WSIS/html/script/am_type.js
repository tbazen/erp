var amk_Map = (function () {
    function amk_Map() {
    }
    return amk_Map;
})();
var Map_Table = (function () {
    function Map_Table() {
        this.table = [];
        this.nMap = 0;
        this.nMap = 0;
        this.loadData();
        this.table.sort(function (x, y) {
            if (x.key > y.key)
                return 1;
            else if (x.key < y.key)
                return -1;
            else
                return 0;
        });
    }
    Map_Table.prototype.KeyStroke = function (unicode, caps, chars) {
        if (chars != null) {
            var l = unicode.length;
            var map = new amk_Map();
            map.Unicode = unicode.charCodeAt(0);
            map.Caps = caps;
            map.Chars = chars;
            map.key = (caps ? "C_" : "") + map.Chars;
            this.table.push(map);
            this.nMap = this.nMap + 1;
        }
    };
    Map_Table.prototype.loadData = function () {
        this.KeyStroke("ሀ", false, "h");
        this.KeyStroke("ሁ", false, "hu");
        this.KeyStroke("ሂ", false, "hi");
        this.KeyStroke("ሃ", false, "ha");
        this.KeyStroke("ሄ", false, "hy");
        this.KeyStroke("ህ", false, "he");
        this.KeyStroke("ሆ", false, "ho");
        this.KeyStroke("ለ", false, "l");
        this.KeyStroke("ሉ", false, "lu");
        this.KeyStroke("ሊ", false, "li");
        this.KeyStroke("ላ", false, "la");
        this.KeyStroke("ሌ", false, "ly");
        this.KeyStroke("ል", false, "le");
        this.KeyStroke("ሎ", false, "lo");
        this.KeyStroke("ሏ", true, "lwa");
        this.KeyStroke("ሐ", false, "^h");
        this.KeyStroke("ሑ", false, "^hu");
        this.KeyStroke("ሒ", false, "^hi");
        this.KeyStroke("ሓ", false, "^ha");
        this.KeyStroke("ሔ", false, "^hy");
        this.KeyStroke("ሕ", false, "^he");
        this.KeyStroke("ሖ", false, "^ho");
        this.KeyStroke("ሗ", false, null);
        this.KeyStroke("መ", false, "m");
        this.KeyStroke("ሙ", false, "mu");
        this.KeyStroke("ሚ", false, "mi");
        this.KeyStroke("ማ", false, "ma");
        this.KeyStroke("ሜ", false, "my");
        this.KeyStroke("ም", false, "me");
        this.KeyStroke("ሞ", false, "mo");
        this.KeyStroke("ሟ", false, "mwa");
        this.KeyStroke("ሠ", false, "^s");
        this.KeyStroke("ሡ", false, "^su");
        this.KeyStroke("ሢ", false, "^si");
        this.KeyStroke("ሣ", false, "^sa");
        this.KeyStroke("ሤ", false, "^sy");
        this.KeyStroke("ሥ", false, "^se");
        this.KeyStroke("ሦ", false, "^so");
        this.KeyStroke("ሧ", false, null);
        this.KeyStroke("ረ", false, "r");
        this.KeyStroke("ሩ", false, "ru");
        this.KeyStroke("ሪ", false, "ri");
        this.KeyStroke("ራ", false, "ra");
        this.KeyStroke("ሬ", false, "ry");
        this.KeyStroke("ር", false, "re");
        this.KeyStroke("ሮ", false, "ro");
        this.KeyStroke("ሯ", false, "rwa");
        this.KeyStroke("ሰ", false, "s");
        this.KeyStroke("ሱ", false, "su");
        this.KeyStroke("ሲ", false, "si");
        this.KeyStroke("ሳ", false, "sa");
        this.KeyStroke("ሴ", false, "sy");
        this.KeyStroke("ስ", false, "se");
        this.KeyStroke("ሶ", false, "so");
        this.KeyStroke("ሷ", false, "swa");
        this.KeyStroke("ሸ", true, "s");
        this.KeyStroke("ሹ", true, "su");
        this.KeyStroke("ሺ", true, "si");
        this.KeyStroke("ሻ", true, "sa");
        this.KeyStroke("ሼ", true, "sy");
        this.KeyStroke("ሽ", true, "se");
        this.KeyStroke("ሾ", true, "so");
        this.KeyStroke("ሿ", true, "shwa");
        this.KeyStroke("ቀ", false, "q");
        this.KeyStroke("ቁ", false, "qu");
        this.KeyStroke("ቂ", false, "qi");
        this.KeyStroke("ቃ", false, "qa");
        this.KeyStroke("ቄ", false, "qy");
        this.KeyStroke("ቅ", false, "qe");
        this.KeyStroke("ቆ", false, "qo");
        this.KeyStroke("ቈ", true, "qwo");
        this.KeyStroke("ቊ", true, "qwe");
        this.KeyStroke("ቋ", true, "qwa");
        this.KeyStroke("ቌ", true, "^qwe");
        this.KeyStroke("ቍ", true, "qwi");
        this.KeyStroke("ቐ", false, "^Q");
        this.KeyStroke("ቑ", false, "^Qu");
        this.KeyStroke("ቒ", false, "^Qi");
        this.KeyStroke("ቓ", false, "^Qa");
        this.KeyStroke("ቔ", false, "^Qy");
        this.KeyStroke("ቕ", false, "^Qe");
        this.KeyStroke("ቖ", false, "^Qo");
        this.KeyStroke("ቘ", false, null);
        this.KeyStroke("ቚ", false, null);
        this.KeyStroke("ቛ", false, null);
        this.KeyStroke("ቜ", false, null);
        this.KeyStroke("ቝ", false, null);
        this.KeyStroke("በ", false, "b");
        this.KeyStroke("ቡ", false, "bu");
        this.KeyStroke("ቢ", false, "bi");
        this.KeyStroke("ባ", false, "ba");
        this.KeyStroke("ቤ", false, "by");
        this.KeyStroke("ብ", false, "be");
        this.KeyStroke("ቦ", false, "bo");
        this.KeyStroke("ቧ", true, "bwa");
        this.KeyStroke("ቨ", false, "v");
        this.KeyStroke("ቩ", false, "vu");
        this.KeyStroke("ቪ", false, "vi");
        this.KeyStroke("ቫ", false, "va");
        this.KeyStroke("ቬ", false, "vy");
        this.KeyStroke("ቭ", false, "ve");
        this.KeyStroke("ቮ", false, "vo");
        this.KeyStroke("ቯ", true, "vwa");
        this.KeyStroke("ተ", false, "t");
        this.KeyStroke("ቱ", false, "tu");
        this.KeyStroke("ቲ", false, "ti");
        this.KeyStroke("ታ", false, "ta");
        this.KeyStroke("ቴ", false, "ty");
        this.KeyStroke("ት", false, "te");
        this.KeyStroke("ቶ", false, "to");
        this.KeyStroke("ቷ", true, "twa");
        this.KeyStroke("ቸ", false, "c");
        this.KeyStroke("ቹ", false, "cu");
        this.KeyStroke("ቺ", false, "ci");
        this.KeyStroke("ቻ", false, "ca");
        this.KeyStroke("ቼ", false, "cy");
        this.KeyStroke("ች", false, "ce");
        this.KeyStroke("ቾ", false, "co");
        this.KeyStroke("ቿ", true, "cwa");
        this.KeyStroke("ኀ", true, "h");
        this.KeyStroke("ኁ", true, "hu");
        this.KeyStroke("ኂ", true, "hi");
        this.KeyStroke("ኃ", true, "ha");
        this.KeyStroke("ኄ", true, "hy");
        this.KeyStroke("ኅ", true, "he");
        this.KeyStroke("ኆ", true, "ho");
        this.KeyStroke("ኈ", true, "hwo");
        this.KeyStroke("ኊ", true, "hwi");
        this.KeyStroke("ኋ", true, "hwa");
        this.KeyStroke("ኌ", true, "hwe");
        this.KeyStroke("ኍ", true, "hwu");
        this.KeyStroke("ነ", false, "n");
        this.KeyStroke("ኑ", false, "nu");
        this.KeyStroke("ኒ", false, "ni");
        this.KeyStroke("ና", false, "na");
        this.KeyStroke("ኔ", false, "ny");
        this.KeyStroke("ን", false, "ne");
        this.KeyStroke("ኖ", false, "no");
        this.KeyStroke("ኗ", true, "nwa");
        this.KeyStroke("ኘ", false, "^n");
        this.KeyStroke("ኙ", false, "^nu");
        this.KeyStroke("ኚ", false, "^ni");
        this.KeyStroke("ኛ", false, "^na");
        this.KeyStroke("ኜ", false, "^ny");
        this.KeyStroke("ኝ", false, "^ne");
        this.KeyStroke("ኞ", false, "^no");
        this.KeyStroke("ኟ", true, "Nwa");
        this.KeyStroke("አ", false, "x");
        this.KeyStroke("ኡ", false, "xu");
        this.KeyStroke("ኢ", false, "xi");
        this.KeyStroke("ኣ", false, "xa");
        this.KeyStroke("ኤ", false, "xy");
        this.KeyStroke("እ", false, "xe");
        this.KeyStroke("ኦ", false, "xo");
        this.KeyStroke("ኧ", false, "^2");
        this.KeyStroke("ከ", false, "k");
        this.KeyStroke("ኩ", false, "ku");
        this.KeyStroke("ኪ", false, "ki");
        this.KeyStroke("ካ", false, "ka");
        this.KeyStroke("ኬ", false, "ky");
        this.KeyStroke("ክ", false, "ke");
        this.KeyStroke("ኮ", false, "ko");
        this.KeyStroke("ኰ", true, "kwo");
        this.KeyStroke("ኲ", true, "kwi");
        this.KeyStroke("ኳ", true, "kwa");
        this.KeyStroke("ኴ", true, "kwe");
        this.KeyStroke("ኵ", true, "^kwi");
        this.KeyStroke("ኸ", true, "^h");
        this.KeyStroke("ኹ", true, "^hu");
        this.KeyStroke("ኺ", true, "^hi");
        this.KeyStroke("ኻ", true, "^ha");
        this.KeyStroke("ኼ", true, "^hy");
        this.KeyStroke("ኽ", true, "^he");
        this.KeyStroke("ኾ", true, "^ho");
        this.KeyStroke("ዀ", false, null);
        this.KeyStroke("ዂ", false, null);
        this.KeyStroke("ዃ", false, null);
        this.KeyStroke("ዄ", false, null);
        this.KeyStroke("ዅ", false, null);
        this.KeyStroke("ወ", false, "w");
        this.KeyStroke("ዉ", false, "wu");
        this.KeyStroke("ዊ", false, "wi");
        this.KeyStroke("ዋ", false, "wa");
        this.KeyStroke("ዌ", false, "wy");
        this.KeyStroke("ው", false, "we");
        this.KeyStroke("ዎ", false, "wo");
        this.KeyStroke("ዐ", false, "^x");
        this.KeyStroke("ዑ", false, "^xu");
        this.KeyStroke("ዒ", false, "^xi");
        this.KeyStroke("ዓ", false, "^xa");
        this.KeyStroke("ዔ", false, "^xy");
        this.KeyStroke("ዕ", false, "^xe");
        this.KeyStroke("ዖ", false, "^xo");
        this.KeyStroke("ዘ", false, "z");
        this.KeyStroke("ዙ", false, "zu");
        this.KeyStroke("ዚ", false, "zi");
        this.KeyStroke("ዛ", false, "za");
        this.KeyStroke("ዜ", false, "zy");
        this.KeyStroke("ዝ", false, "ze");
        this.KeyStroke("ዞ", false, "zo");
        this.KeyStroke("ዟ", true, "zwa");
        this.KeyStroke("ዠ", false, "^z");
        this.KeyStroke("ዡ", false, "^zu");
        this.KeyStroke("ዢ", false, "^zi");
        this.KeyStroke("ዣ", false, "^za");
        this.KeyStroke("ዤ", false, "^zy");
        this.KeyStroke("ዥ", false, "^ze");
        this.KeyStroke("ዦ", false, "^zo");
        this.KeyStroke("ዧ", true, "^zwa");
        this.KeyStroke("የ", false, "^y");
        this.KeyStroke("ዩ", false, "^yu");
        this.KeyStroke("ዪ", false, "^yi");
        this.KeyStroke("ያ", false, "^ya");
        this.KeyStroke("ዬ", false, "^yy");
        this.KeyStroke("ይ", false, "^ye");
        this.KeyStroke("ዮ", false, "^yo");
        this.KeyStroke("ደ", false, "d");
        this.KeyStroke("ዱ", false, "du");
        this.KeyStroke("ዲ", false, "di");
        this.KeyStroke("ዳ", false, "da");
        this.KeyStroke("ዴ", false, "dy");
        this.KeyStroke("ድ", false, "de");
        this.KeyStroke("ዶ", false, "do");
        this.KeyStroke("ዷ", true, "dwa");
        this.KeyStroke("ዸ", false, null);
        this.KeyStroke("ዹ", false, null);
        this.KeyStroke("ዺ", false, null);
        this.KeyStroke("ዻ", false, null);
        this.KeyStroke("ዼ", false, null);
        this.KeyStroke("ዽ", false, null);
        this.KeyStroke("ዾ", false, null);
        this.KeyStroke("ዿ", false, null);
        this.KeyStroke("ጀ", false, "j");
        this.KeyStroke("ጁ", false, "ju");
        this.KeyStroke("ጂ", false, "ji");
        this.KeyStroke("ጃ", false, "ja");
        this.KeyStroke("ጄ", false, "jy");
        this.KeyStroke("ጅ", false, "je");
        this.KeyStroke("ጆ", false, "jo");
        this.KeyStroke("ጇ", true, "jwa");
        this.KeyStroke("ገ", false, "g");
        this.KeyStroke("ጉ", false, "gu");
        this.KeyStroke("ጊ", false, "gi");
        this.KeyStroke("ጋ", false, "ga");
        this.KeyStroke("ጌ", false, "gy");
        this.KeyStroke("ግ", false, "ge");
        this.KeyStroke("ጎ", false, "go");
        this.KeyStroke("ጐ", true, "gwo");
        this.KeyStroke("ጒ", true, "gwi");
        this.KeyStroke("ጓ", true, "gwa");
        this.KeyStroke("ጔ", true, "gwe");
        this.KeyStroke("ጕ", true, "gwu");
        this.KeyStroke("ጘ", false, null);
        this.KeyStroke("ጙ", false, null);
        this.KeyStroke("ጚ", false, null);
        this.KeyStroke("ጛ", false, null);
        this.KeyStroke("ጜ", false, null);
        this.KeyStroke("ጝ", false, null);
        this.KeyStroke("ጞ", false, null);
        this.KeyStroke("ጠ", false, "^t");
        this.KeyStroke("ጡ", false, "^tu");
        this.KeyStroke("ጢ", false, "^ti");
        this.KeyStroke("ጣ", false, "^ta");
        this.KeyStroke("ጤ", false, "^ty");
        this.KeyStroke("ጥ", false, "^te");
        this.KeyStroke("ጦ", false, "^to");
        this.KeyStroke("ጧ", true, "^twa");
        this.KeyStroke("ጨ", false, "^c");
        this.KeyStroke("ጩ", false, "^cu");
        this.KeyStroke("ጪ", false, "^ci");
        this.KeyStroke("ጫ", false, "^ca");
        this.KeyStroke("ጬ", false, "^cy");
        this.KeyStroke("ጭ", false, "^ce");
        this.KeyStroke("ጮ", false, "^co");
        this.KeyStroke("ጯ", true, "^cwa");
        this.KeyStroke("ጰ", false, "^p");
        this.KeyStroke("ጱ", false, "^pu");
        this.KeyStroke("ጲ", false, "^pi");
        this.KeyStroke("ጳ", false, "^pa");
        this.KeyStroke("ጴ", false, "^py");
        this.KeyStroke("ጵ", false, "^pe");
        this.KeyStroke("ጶ", false, "^po");
        this.KeyStroke("ጷ", false, null);
        this.KeyStroke("ጸ", true, "t");
        this.KeyStroke("ጹ", true, "tu");
        this.KeyStroke("ጺ", true, "ti");
        this.KeyStroke("ጻ", true, "ta");
        this.KeyStroke("ጼ", true, "ty");
        this.KeyStroke("ጽ", true, "te");
        this.KeyStroke("ጾ", true, "to");
        this.KeyStroke("ጿ", true, "tswa");
        this.KeyStroke("ፀ", true, "^t");
        this.KeyStroke("ፁ", true, "^tu");
        this.KeyStroke("ፂ", true, "^ti");
        this.KeyStroke("ፃ", true, "^ta");
        this.KeyStroke("ፄ", true, "^ty");
        this.KeyStroke("ፅ", true, "^te");
        this.KeyStroke("ፆ", true, "^to");
        this.KeyStroke("ፈ", false, "f");
        this.KeyStroke("ፉ", false, "fu");
        this.KeyStroke("ፊ", false, "fi");
        this.KeyStroke("ፋ", false, "fa");
        this.KeyStroke("ፌ", false, "fy");
        this.KeyStroke("ፍ", false, "fe");
        this.KeyStroke("ፎ", false, "fo");
        this.KeyStroke("ፏ", true, "fwa");
        this.KeyStroke("ፐ", false, "p");
        this.KeyStroke("ፑ", false, "pu");
        this.KeyStroke("ፒ", false, "pi");
        this.KeyStroke("ፓ", false, "pa");
        this.KeyStroke("ፔ", false, "py");
        this.KeyStroke("ፕ", false, "pe");
        this.KeyStroke("ፖ", false, "po");
        this.KeyStroke("ፗ", false, null);
        this.KeyStroke("ፘ", false, null);
        this.KeyStroke("ፙ", false, null);
        this.KeyStroke("ፚ", false, null);
        this.KeyStroke("፡", false, ":");
        this.KeyStroke("።", false, "::");
        this.KeyStroke("፣", false, ",");
        this.KeyStroke("፤", false, ";");
        this.KeyStroke("፥", false, ".");
        this.KeyStroke("፦", false, "-");
        this.KeyStroke("፧", false, null);
        this.KeyStroke("፨", false, null);
        this.KeyStroke(" ", false, null);
        this.KeyStroke("!", false, null);
        this.KeyStroke("\"", false, null);
        this.KeyStroke("#", false, null);
        this.KeyStroke("$", false, null);
        this.KeyStroke("%", false, null);
        this.KeyStroke("&", false, null);
        this.KeyStroke("\"", false, null);
        this.KeyStroke("(", false, null);
        this.KeyStroke(")", false, null);
        this.KeyStroke("*", false, null);
        this.KeyStroke("+", false, null);
        this.KeyStroke(",", false, null);
        this.KeyStroke("-", false, null);
        this.KeyStroke(".", false, null);
        this.KeyStroke("/", false, null);
        this.KeyStroke("0", false, null);
        this.KeyStroke("1", false, null);
        this.KeyStroke("2", false, null);
        this.KeyStroke("3", false, null);
        this.KeyStroke("4", false, null);
        this.KeyStroke("5", false, null);
        this.KeyStroke("6", false, null);
        this.KeyStroke("7", false, null);
        this.KeyStroke("8", false, null);
        this.KeyStroke("9", false, null);
        this.KeyStroke(":", false, null);
        this.KeyStroke(";", false, null);
        this.KeyStroke("<", false, null);
        this.KeyStroke("=", false, null);
        this.KeyStroke(">", false, null);
        this.KeyStroke("?", false, null);
        this.KeyStroke(" ", false, null);
        this.KeyStroke("[", false, null);
        this.KeyStroke("\\", false, null);
        this.KeyStroke("]", false, null);
        this.KeyStroke("^", false, null);
        this.KeyStroke("_", false, null);
        this.KeyStroke("`", false, null);
        this.KeyStroke("{", false, null);
        this.KeyStroke("}", false, null);
        this.KeyStroke("~", false, null);
        this.KeyStroke("ቐ", false, "^q");
        this.KeyStroke("ቑ", false, "^qu");
        this.KeyStroke("ቒ", false, "^qi");
        this.KeyStroke("ቓ", false, "^qa");
        this.KeyStroke("ቔ", false, "^qy");
        this.KeyStroke("ቕ", false, "^qe");
        this.KeyStroke("ቖ", false, "^qo");
    };
    return Map_Table;
})();
var AmharicTextHandler = (function () {
    function AmharicTextHandler(text) {
        var _this = this;
        this.Current = -1;
        this.CurrentKeys = "";
        this.IgnoreKey = false;
        this.text = text;
        if (!this.text)
            return;
        this.text.onkeydown = function (event) {
            _this.handKeyDown(event);
            if (_this.onkeydown)
                _this.onkeydown(event);
        };
        var _self = this;
        this.text.onkeypress = function (event) {
            _this.handKeyPress(event);
            if (_this.onkeypress)
                _this.onkeypress(event);
        };
        var am_control = document.getElementById("am_control_on");
        if (am_control) {
            am_control.onchange = function () {
                AmharicTextHandler.m_Enabled = true;
            };
            AmharicTextHandler.m_Enabled = am_control.value == "1";
        }
        var am_control_off = document.getElementById("am_control_off");
        if (am_control_off) {
            am_control_off.onchange = function () {
                AmharicTextHandler.m_Enabled = false;
            };
        }
    }
    AmharicTextHandler.prototype.handKeyDown = function (event) {
        
    };
    AmharicTextHandler.prototype.handKeyPress = function (event) {
        if (!AmharicTextHandler.m_Enabled)
            return;
        if (event.ctrlKey) {
            this.Current = -1;
        }
        var caps;
        var keyCar = String.fromCharCode(event.keyCode);
        if (event.shiftKey)
            caps = keyCar == keyCar.toLocaleLowerCase();
        else
            caps = keyCar == keyCar.toUpperCase();
        var chArray = this.GetNextChar(event.keyCode, caps, event.shiftKey);
        if (chArray == null || chArray.length == 0) {
            return;
        }
        for (var i = 0; i < chArray.length; i++) {
            var ch = chArray[i];
            var c = '\b'.charCodeAt(0);
            if (ch == c) {
                if (this.text.selectionStart > 0) {
                    var ss = this.text.selectionStart;
                    this.text.value = this.text.value.substr(0, this.text.selectionStart - 1) + this.text.value.substr(this.text.selectionStart);
                    this.text.selectionStart = ss - 1;
                    this.text.selectionEnd = ss - 1;
                }
            }
            else {
                if (this.text.selectionStart == this.text.selectionEnd) {
                    if (this.text.selectionStart == this.text.value.length)
                        this.text.value += String.fromCharCode(ch);
                    else {
                        var ss = this.text.selectionStart;
                        this.text.value = this.text.value.substr(0, this.text.selectionStart) + String.fromCharCode(ch) + this.text.value.substr(this.text.selectionStart);
                        this.text.selectionStart = ss + 1;
                        this.text.selectionEnd = ss + 1;
                    }
                }
            }
        }
        event.preventDefault();
    };
    AmharicTextHandler.prototype.ToChar = function (ks) {
        switch (ks) {
            case 0xdd:
                return ']';
            case 0xde:
                return '\'';
            case 0xd5:
                return '[';
            case 0xbf:
                return '/';
            case 0xba:
                return ';';
            case 0xbc:
                return ',';
            case 110:
                return '.';
        }
        switch (ks) {
            case 8:
            case 33:
            case 34:
            case 45:
            case 46:
            case 96:
            case 97:
            case 98:
            case 99:
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 112:
            case 113:
            case 114:
            case 115:
            case 116:
            case 117:
            case 118:
            case 119:
            case 120:
            case 121:
            case 122:
            case 122:
                return '\0';
        }
        return String.fromCharCode(ks).toLowerCase();
    };
    AmharicTextHandler.prototype.GetNextChar = function (ks, Caps, Shift) {
        var current;
        var str;
        var count = AmharicTextHandler.amk_table.table.length;
        var str2 = String.fromCharCode(ks).toLocaleLowerCase();
        if (Shift) {
            str2 = "^" + str2;
        }
        if (this.Current == -1) {
            current = 0;
            str = (Caps ? "C_" : "") + str2;
        }
        else {
            current = this.Current;
            str = this.CurrentKeys + str2;
        }
        while (current < count) {
            var k = AmharicTextHandler.amk_table.table[current].key;
            if (k.indexOf(str) == 0) {
                var chArray;
                chArray = [];
                this.CurrentKeys = str;
                var unicode = AmharicTextHandler.amk_table.table[current].Unicode;
                if (this.Current == -1) {
                    chArray.push(unicode);
                }
                else {
                    chArray.push('\b'.charCodeAt(0));
                    chArray.push(unicode);
                }
                this.Current = current;
                return chArray;
            }
            current++;
        }
        if (this.Current == -1) {
            return null;
        }
        this.Current = -1;
        return this.GetNextChar(ks, Caps, Shift);
    };
    AmharicTextHandler.amk_table = new Map_Table();
    AmharicTextHandler.m_Enabled = true;
    return AmharicTextHandler;
})();
//# sourceMappingURL=am_type.js.map