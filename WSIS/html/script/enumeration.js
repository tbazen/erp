﻿function _enum(elementName,enumName,elementValue)
{
    var el = document.getElementById(elementName);
    while (el.firstChild) {
        el.removeChild(el.firstChild);
    }

    if (el==null)
        return;
    _proxy("getEnumerationData"
        , {EnumName:enumName}
        , function (ret)
        {
            op = document.createElement("option");
            op.innerText = "";
            op.value = -1;
            el.appendChild(op);

            for(i=0;i<ret.IntCodes.length;i++)
            {
                var op = document.createElement("option");
                op.innerText = ret.Names[i];
                op.value = ret.IntCodes[i];
                el.appendChild(op);
            }
            //set enumeration value
            for (i = 0; i < el.options.length; i++) {

                if (el.options[i].value == elementValue)
                    el.selectedIndex = i;
            }
        }
        ,function(err)
        {
            alert("Error loading enumeration\n" + err);
        }
        );
}

var address_cache = { templates: null, adTypes: [] };
function getAddressTypes(done)
{
    if (address_cache.templates)
        done(address_cache.templates);
    else
    {
        _proxy("getAllAddressTemplates", {},
            function(res)
            {
                address_cache.templates = res;
                done(res);
            }
            );
    }
}
function getAddressFields(typeIDs, done) {
    var notCached = [];
    for (i = 0; i < typeIDs.length;i++)
    {
        if (address_cache.adTypes[typeIDs[i]] == null)
            notCached.push(typeIDs[i]);
    }
    if (notCached.length == 0) {
        var ret = [];
        for (i = 0; i < typeIDs.length; i++)
        {
            ret.push(address_cache.adTypes[typeIDs[i]]);
        }
        done(ret);
        return;
    }
    _proxy("getAddressTypeArray", {addressIDs:notCached},
        function (res) {
            for (i = 0; i < res.length; i++)
            {
                address_cache.adTypes[res[i].AddressTypeID] = res[i];
            }
            var ret = [];
            for (i = 0; i < typeIDs.length; i++)
            {
                ret.push(address_cache.adTypes[typeIDs[i]]);
            }
            done(ret);
        }
        );
}
 function addressPicker(elementID,doneloading)
{
    //constructor
    var _this = this;
    
    this.container = document.getElementById(elementID);
    this.selector = document.createElement("Select");
    this.selector.setAttribute("class", "addressSelector");
    this.container.appendChild(this.selector);
    this.fieldContainer = document.createElement("div");
    this.fieldContainer.setAttribute("class", "addressFieldContainer");
    this.container.appendChild(this.fieldContainer);
    this.selectedAddress = -1;
    this.currentParentID = -1;
    this.newAddress = null;

    getAddressTypes(function (res) {
        var dfltIndex;
        for (i = 0; i < res.length; i++) {
            var op = document.createElement("option");
            op.setAttribute("value", res[i].id);
            if (i == 1)
                op.selected = true;
            op.innerText = res[i].Name;
            _this.selector.appendChild(op);
            if (res[i].Name == "Dire Dawa")
            {
                dfltIndex = i;
            }
        }
        if (dfltIndex)
        {
            _this.selector.selectedIndex = dfltIndex;
            _this.setTemplate(res[dfltIndex], true, doneloading);
        }
        else
            doneloading();

    });

    //methods
    this.populateEnumByData = function (ads, pars) {
        var sel = pars[0];
        var propag = pars[1];
        var index = pars[2];

        while (sel.firstChild)
            sel.removeChild(sel.firstChild);

        var o = document.createElement("option");
        o.innerText = "-";
        o.setAttribute("value", -1)
        sel.appendChild(o);

        for (j = 0; j < ads.length; j++) {
            o = document.createElement("option");
            o.innerText = ads[j].ShortName;
            o.setAttribute("value", ads[j].AddressID)
            sel.appendChild(o);
        }
        if (propag) {
            sel.selectedIndex = 0;
            if (index < _this.selectedTemplate.AddressType.length - 1) {
                if (_this.selectedTemplate.Enumerated[index + 1]) {
                    _this.populateEnum(-1, index + 1,_this.selectedTemplate.AddressType[index+1],propag);
                }
                else {
                    _this.fieldContainer.children.item(index * 2 + 3).value = "";
                }
            }
        }
    };
    this.setPath = function (path, enumerations)
    {
        for (i = 0; i < path.length; i++) {
            var el = _this.fieldContainer.children.item(2 * i + 1);
            if (_this.selectedTemplate.Enumerated[i]) {
                _this.populateEnumByData(enumerations[i], [el, false,i]);
                el.value = path[i].AddressID;
                _this.currentParentID = path[i].AddressID;
            }
            else
                el.value = path[i].Code;
        }
        _this.selectedAddress = path[path.length - 1].AddressID;
        _this.newAddress = null;
    }
    this.setSelectorIndexByTemplateID=function(tid)
    {
        var op = _this.selector.firstChild;
        while(op)
        {
            if(op.value==tid.toString())
            {
                _this.selector.value = op.value;
                break;
            }
            op = op.nextSibling;
        }
    }
    this.setAddress=function(addressID)
    {
        if (addressID == -1)
        {
            _this.currentParentID = -1;
            _this.newAddress = null;
            _this.selectedAddress = -1;
            if (_this.selectedTemplate != null)
            {
                for (i = 0; i < _this.selectedTemplate.AddressType.length; i++) {
                    var el = _this.fieldContainer.children.item(2 * i + 1);
                    if (_this.selectedTemplate.Enumerated[i]) {
                        el.selectedIndex = 0;
                    }
                    else
                        el.value = "";
                }
            }
            return;
        }
        _proxy("getAddressTemplateAndPath",
            { addressID: addressID },
            function(res)
            {
                if(res.template==null)
                {
                    _this.setTemplate(_this.selectedTemplate,false)
                    return;
                }
                if (_this.selectedTemplate == null || res.template.id != _this.selectedTemplate.id) {
                    {
                        
                        _this.setTemplate(res.template,
                        false,
                        function () {
                            _this.setPath(res.path, res.enumerations);
                            _this.setSelectorIndexByTemplateID(_this.selectedTemplate.id);
                        });
                    }
                }
                else
                    _this.setPath(res.path, res.enumerations);
                
            }
            );
    }
    
    this._onAddressChanged = function (a) {
        this.selectedAddress = a;
        if (_this.onAddressChanged)
            _this.onAddressChanged(a, _this);
    }

    this.autoComplete=function(at, e)
    {
        _proxy("getAddressByName"
            , { typeID: at,parentAddressID:_this.currentParentID, name: e.value }
            ,function(res)
            {
                if (res == null) {
                    _this.newAddress = { AddressTypeID: at, ParentAddressID: _this.currentParentID, ShortName: e.value };
                    _this._onAddressChanged(-1);
                    return;
                }
                _this.newAddress = null;
                e.value = res.Code;
                _this._onAddressChanged(res.AddressID);
            }
            );
    }
    
    this.populateEnum = function (parent, index,type,propag) {
        var e = _this.fieldContainer.children.item(index * 2 + 1);
        _proxy("getChildAddressesByType",
        { AddressID: parent, type: type },
            _this.populateEnumByData, null, [e,propag,index]
    );
    }
    
    this.setTemplate=function(template,popen,done)
    {

        while (_this.fieldContainer.firstChild)
            _this.fieldContainer.removeChild(_this.fieldContainer.firstChild);

        
        if (template) {
            getAddressFields(template.AddressType, function (res) {
                while (_this.fieldContainer.firstChild)
                    _this.fieldContainer.removeChild(_this.fieldContainer.firstChild);
                for (i = 0; i < res.length; i++) {
                    var l = document.createElement("span");
                    l.setAttribute("class", "addresFieldLabel");
                    l.innerText = res[i].Name;
                    _this.fieldContainer.appendChild(l);
                    var e;
                    if (template.Enumerated[i]) {
                        e = document.createElement("select");
                        e.setAttribute("class", "addresFieldSelect");
                        _this.fieldContainer.appendChild(e);
                        if (i == 0) {
                            if(popen)
                                _this.populateEnum(-1, 0, template.AddressType[0],false);
                        }
                        e.onchange = function (event) {
                            var index = 0;
                            while (_this.fieldContainer.children.item(index) != event.srcElement)
                                index++;
                            index = Math.floor(index / 2);
                            var val = parseInt(event.srcElement.value);
                            _this.currentParentID = val;
                            if (val != -1 || index == 0)
                                _this._onAddressChanged(val, event.srcElement);
                            else {
                                var parEl = _this.fieldContainer.children.item(index * 2 - 1);
                                _this._onAddressChanged(parseInt(parEl.value), parEl);
                            }
                            if (index == template.Enumerated.length - 1) {
                                return;
                            }
                            if (popen) {
                                if (template.Enumerated[index + 1])
                                    _this.populateEnum(val, index + 1, template.AddressType[index + 1],true);
                                else {
                                    _this.fieldContainer.children.item(index * 2 + 3).value = "";
                                }
                            }
                        };
                    }
                    else {
                        e = document.createElement("input");
                        e.setAttribute("typeID", res[i].AddressTypeID);
                        e.setAttribute("class", "addressFieldEdit");
                        e.setAttribute("type", "text");
                        e.onkeydown = function (event) {
                            if (event.keyCode == 13) {
                                _this.autoComplete(parseInt(event.srcElement.getAttribute("typeID")), event.srcElement);
                                event.preventDefault();
                            }
                        }
                        $(e).blur(function (event) {
                            _this.autoComplete(parseInt(event.currentTarget.getAttribute("typeID")), event.currentTarget);
                        });
                        _this.fieldContainer.appendChild(e);
                    }

                }
                _this.selectedTemplate = template;
                if (done)
                    done();
            }
            );
        }
        else
            _this.selectedTemplate = null;
    }
    this.selector.onchange = function (event)
    {
        var tid=parseInt(_this.selector.value);
        var template;
        for (i = 0; i < address_cache.templates.length;i++)
        {
            var t = address_cache.templates[i];
            if (t.id == tid) {
                template = t;
                break;
            }
        }
        _this.setTemplate(template,true);
    }
}
