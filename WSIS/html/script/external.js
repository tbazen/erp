// JavaScript source code

function ReverseDisplay(content,head) {
    if (document.getElementById(content).style.display == "block") {
        $("#" + content).slideUp();
        $("#" + head).removeClass('open');
        $("#" + head).addClass("center_title");
            }
            else {
        $("#" + content).slideDown();
        $("#" + head).removeClass('center_title');
        $("#" + head).addClass("open");
            }
}
var resident_picker_html;
function getPickerHtml(done) {
    if (resident_picker_html)
        return done(resident_picker_html);
    $.ajax({ url: "resident_picker.html" })
.done(function (data) {
    resident_picker_html = data;
    var el=document.createElement("div");
    document.getElementsByTagName("body").item(0).appendChild(el);
    el.innerHTML = resident_picker_html;
    done(data);
});
}
var focusedResidentPicker;
function popupSelectResident(id,name)
{
    if (!focusedResidentPicker)
        return;
    var oldRes = focusedResidentPicker.resID;
    focusedResidentPicker.resID = parseInt(id);
    focusedResidentPicker.input.val(name);
    focusedResidentPicker.input.attr("resid", id);
    focusedResidentPicker.hide();

    if (oldRes != focusedResidentPicker.resID) {
        if (focusedResidentPicker.onResidentSelected) {

            focusedResidentPicker.onResidentSelected(focusedResidentPicker.resID);
        }
    }
}
function ResidentPicker(el)
{
    var _this = this;
    this.resID = -1;
    var el=$('#'+el);
    this.input = el;
    this.hide=function()
    {
        el.popModal("hide");
    }
    el.focus(function ()
    {
        getPickerHtml(function (content) {
            el.popModal({
                html: content,
                placement: 'bottomLeft',
                showCloseBut: true,
                onDocumentClickClose: true,
                onDocumentClickClosePrevent: '',
                overflowContent: false,
                inline: true,
                beforeLoadingContent: 'Please, wait...',
                onOkBut: function () { },
                onCancelBut: function () { },
                onClose: function () { },
            });
            focusedResidentPicker = _this;
            var pickerInput = document.getElementById("individual_picker_text");
            pickerInput.focus();
            var aminput=new AmharicTextHandler(pickerInput);
            pickerInput.onkeyup = function (event) {
                _proxy("searchResidentAdvanced", {
                    query: { ByName: pickerInput.value },
                    index: 0,
                    PageSize: 30,
                }
                , function (res) {
                    var rows = document.getElementById("individual_picker_rows");
                    while (rows.firstChild)
                        rows.removeChild(rows.firstChild);
                    var data = res.res;
                    if (event.keyCode == 13) {
                        if(data.length>0)
                        {
                            popupSelectResident(data[0].ResidentID, data[0].Text);
                        }
                    }
                    for (i = 0; i < data.length; i++) {
                        var resident = data[i];
                        var rowHTML = S("<tr class='resident_picker_row' onclick='popupSelectResident(\"{{id}}\",\"{{name}}\")'><td>{{name}}</td><td>{{id}}</td></tr>").template(
                            {
                                name: S(resident.Text).escapeHTML().s,
                                id: S(resident.ResidentID).escapeHTML().s,
                            }).s;

                        $("#individual_picker_rows").append($(rowHTML));
                    }
                }
                );
            }
        });
        

    });

}


function loadIssueFrame(url,certificateID,certificateType,eventID) {

    __certificateType = certificateType;
    __certificateEventID = eventID;
    if (__certificateType == "ID") {
        $("#refRow").hide();
        document.getElementById("pageLabel").style.display = "block";
    }
    else {
        $("#refRow").show();
        document.getElementById("pageLabel").style.display = "none";
    }

    var button = document.getElementById("issueCertificate");
    var printButton = document.getElementById("printCertificate");
    var validatePrintButtonVisibility = (function () {
        _proxy("validatePrintVisibility", { residentID: __resID, certificateType: __certificateType, eventID: __certificateEventID, certID: certificateID },
                 function (res) {
                     printButton.style.visibility = res ? "visible" : "hidden";
                 }
                 , function (err) {
                     alert("Failed: " + err);
                 }
                 );
    }());

    button.style.visibility = certificateID == -1 ? "visible" : "hidden";
    
    document.getElementById('certificate_iframe').src = url;
}
function printCertificate()
{
    var frm = document.getElementById("certificate_iframe").contentWindow;
    frm.focus();// focus on contentWindow is needed on some ie versions
    frm.print();
}



