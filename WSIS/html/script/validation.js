﻿
//for new resident registration and update

function validateform() {
    var valid = true;
   // var requiredfields = ['firstName', 'firstName_A', 'middleName', 'middleName_A', 'lastName', 'lastName_A', 'birthDate', 'comboEthnic', 'comboReligion', 'comboCitizenship', 'comboMaritalStatus', 'comboJob', 'comboEducation', 'homePhone', 'workPhone', 'representative', 'comboHealthStatus', 'salary', 'uploadimg'];

    var requiredfields = ['firstName', 'firstName_A', 'middleName', 'middleName_A', 'lastName', 'lastName_A', 'comboEthnic', 'comboReligion', 'comboCitizenship', 'comboMaritalStatus', 'comboJob', 'comboEducation', 'comboHealthStatus'];

    for(var i=0; i<requiredfields.length; i++)
    {
        var arraylist = $('#' + requiredfields[i]).val();
        if (arraylist == ""|| arraylist==-1) {
            $('#' + requiredfields[i] + '_error').show();
            valid = false;
        }
        else
            $('#' + requiredfields[i] + '_error').hide();
    }
    if(valid)
    {
        registerResident();
    }
}


//for birth event registration validation
function validateBirthEvent(residentID)
{
    var valid = true;
    var requiredfields = ['birthEventDate', 'mother', 'father', 'comboFacility', 'comboBirthHelp', 'comboTypeOfBirth', 'birthWeight', 'comboBirthMarriageStatus', 'registererRes', 'comboRegistererRel'];

    for (var i = 0; i < requiredfields.length; i++) {
        var arraylist = $('#' + requiredfields[i]).val();
        if (arraylist == "" || arraylist == -1) {
            $('#' + requiredfields[i] + '_error').show();
            valid = false;
        }
        else
            $('#' + requiredfields[i] + '_error').hide();
    }
    if (valid)
    {
        registerBirthEvent(residentID);
    }
}

//for marriage event registration validation

function validateMarriageEvent(residentID,sex)
{
    var valid = true;
    var requiredfields = ['marriagePartner', 'comboTypeOfMarriage', 'placeOfMarriage', 'placeOfMarriage_A', 'comboMarriageProvider', 'dateOfMarriage', 'm_Witness1', 'm_Witness2', 'm_Witness3', 'm_Witness4'];

    for (var i = 0; i < requiredfields.length; i++) {
        var arraylist = $('#' + requiredfields[i]).val();
        if (arraylist == "" || arraylist == -1) {
            $('#' + requiredfields[i] + '_error').show();
            valid = false;
        }
        else
            $('#' + requiredfields[i] + '_error').hide();
    }
    if (valid) {
        registerMarriageEvent(residentID, sex);
    }
}

//for divorce event registration validation 
function validateDivorceEvent(residentID) {

    var valid = true;
    var requiredfields = ['dateOfDivorce', 'divorcePartner', 'comboTypeOfDivorce', 'comboCauseOfDivorce', 'marriageAge', 'noOfChildren'];
    for (var i = 0; i < requiredfields.length; i++) {
        var arraylist = $('#' + requiredfields[i]).val();
        if (arraylist == "" || arraylist == -1) {
            $('#' + requiredfields[i] + '_error').show();
            valid = false;
        }
        else
            $('#' + requiredfields[i] + '_error').hide();
    }
    if (valid) {
        registerDivorceEvent(residentID);
    }
}

//for migration event registration validation

function validateMigrationEvent(residentID) {

    var valid = true;
    var requiredfields = ['dateOfMigration', 'comboMigrationReason'];
    for (var i = 0; i < requiredfields.length; i++) {
        var arraylist = $('#' + requiredfields[i]).val();
        if (arraylist == "" || arraylist == -1) {
            $('#' + requiredfields[i] + '_error').show();
            valid = false;
        }
        else
            $('#' + requiredfields[i] + '_error').hide();
    }
    if (valid) {
        registerMigrationEvent(residentID);
    }
}

//for death event registration validation

function validateDeathEvent(residentID) {

    var valid = true;
    var requiredfields = ['comboCauseOfDeath', 'dateOfDeath', 'comboDeathFacility', 'applicantName', 'comboApplicantRelation'];
    for (var i = 0; i < requiredfields.length; i++) {
        var arraylist = $('#' + requiredfields[i]).val();
        if (arraylist == "" || arraylist == -1) {
            $('#' + requiredfields[i] + '_error').show();
            valid = false;
        }
        else
            $('#' + requiredfields[i] + '_error').hide();
    }
    if (valid) {
        registerDeathEvent(residentID);
    }
}

//for add household member validation

function validateHouseholdMember(residentID) {

    var valid = true;
    var requiredfields = ['household_memeber', 'comboHouseHoldRoles'];
    for (var i = 0; i < requiredfields.length; i++) {
        var arraylist = $('#' + requiredfields[i]).val();
        if (arraylist == "" || arraylist == -1) {
            $('#' + requiredfields[i] + '_error').show();
            valid = false;
        }
        else
            $('#' + requiredfields[i] + '_error').hide();
    }
    if (valid) {
        addHouseholdMember(residentID);
    }
}


//for change passowrd

function validateChangePassword() {

    var valid = true;
    var requiredfields = ['old-pass', 'new-pass', 'confirm-pass'];
    for (var i = 0; i < requiredfields.length; i++) {
        var arraylist = $('#' + requiredfields[i]).val();
        if (arraylist == "" || arraylist == -1) {
            $('#' + requiredfields[i] + '_error').show();
            valid = false;
        }
        else
            $('#' + requiredfields[i] + '_error').hide();
    }
    if (valid) {
        if ($('#new-pass').val() != $('#confirm-pass').val())
        {
            $('#passNotMatch_error').show();
        }
        else
            changePassword();
    }
}