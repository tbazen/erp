﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIZNET.iERP.Client;
using INTAPS.Accounting;

namespace INTAPS.WSIS.iERP
{
    public class DCHLongTermLoanWithServiceCharge: BIZNET.iERP.Client.DCHStaffPaymentTransactionBase
    {
        public DCHLongTermLoanWithServiceCharge()
            : base(typeof(LongTermWithServiceChargeFomr))
        {

        }
        
    }
    public class LongTermWithServiceChargeFomr : BIZNET.iERP.Client.LongTermLoanForm
    {
        public LongTermWithServiceChargeFomr(IAccountingClient client, ActivationParameter param)
            : base(client, param)
        {
        }
        protected override BIZNET.iERP.StaffReceivableAccountDocument createDocumentInstance()
        {
            return new LongTermLoanWithServiceChargeDocument();
        }
    }
}
