﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using BIZNET.iERP.Server;
using BIZNET.iERP;
using INTAPS.Payroll;

namespace INTAPS.WSIS.iERP
{
    class DHLongTermLoanWithServiceCharge : DHStaffPaymentTransactionBase, IDocumentServerHandler
    {
        public DHLongTermLoanWithServiceCharge(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        public int postInternal(int AID, AccountDocument doc, int debitAccount)
        {
            StaffReceivableAccountDocument staffReceivableAccountDoc = (StaffReceivableAccountDocument)doc;
            if (staffReceivableAccountDoc.voucher != null)
                staffReceivableAccountDoc.PaperRef = staffReceivableAccountDoc.voucher.reference;

            Employee eme = bdePayroll.GetEmployee(staffReceivableAccountDoc.employeeID);
            TransactionOfBatch[] transactions = null;

            Employee employee = bdePayroll.GetEmployee(staffReceivableAccountDoc.employeeID);
            CostCenterAccount assetAccount = bdeAccounting.GetCostCenterAccount(staffReceivableAccountDoc.assetAccountID);
            int parentReceivableAccount = debitAccount;
            debitAccount = GetEmployeeAccountID(employee, debitAccount);
            if (debitAccount == -1)
            {
                throw new INTAPS.ClientServer.ServerUserMessage(String.Format("Account not set up for employee {0} under account ID {1}", employee.EmployeeNameID, parentReceivableAccount));
            }
            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                int costCenterID = employee.costCenterID;
                List<TransactionOfBatch> transactionBatches = new List<TransactionOfBatch>();
                if (staffReceivableAccountDoc is StaffPerDiemDocument)
                {
                    StaffPerDiemDocument perdiem = (StaffPerDiemDocument)staffReceivableAccountDoc;
                    CostCenterAccount csAccount = bdeAccounting.GetCostCenterAccount(perdiem.costCenterID, debitAccount);
                    if (csAccount != null)
                        costCenterID = perdiem.costCenterID;
                    else
                    {
                        CreateEmpleeCostCenterAccount(AID, debitAccount, perdiem.costCenterID);
                        costCenterID = perdiem.costCenterID;
                    }
                }
                transactions = StaffTransactionUtility.GenerateStaffPaymentReceiptTransaction(AID, this.bde, bdeAccounting, staffReceivableAccountDoc, debitAccount, costCenterID, 1);
                //TransactionOfBatch otherIncome = new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID,bde.SysPars.mainCostCenterID, bde.SysPars.otherIncomeAccountID).id, -staffReceivableAccountDoc.amount * 0.05, "Other income from long term loan interest");
                foreach (TransactionOfBatch batch in transactions)
                {
                    transactionBatches.Add(batch);
                }
                //if (transactions.Length > 0)
                //    transactionBatches.Add(otherIncome);

                DocumentTypedReference[] typedRefs = staffReceivableAccountDoc.voucher == null ? new DocumentTypedReference[0] :
                    new DocumentTypedReference[] { staffReceivableAccountDoc.voucher };
                if (doc.AccountDocumentID == -1)
                    ret = bdeAccounting.RecordTransaction(AID, staffReceivableAccountDoc,
                    transactionBatches.ToArray()
                    , true, typedRefs);
                else
                    ret = bdeAccounting.UpdateTransaction(AID, staffReceivableAccountDoc, transactionBatches.ToArray(), true, typedRefs);
                if (!bde.AllowNegativeTransactionPost())
                    bdeAccounting.validateNegativeBalance(staffReceivableAccountDoc.assetAccountID, 0, false, DateTime.Now, "The transaction is blocked because it will cause negative asset balance.");
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }
        public int Post(int AID, AccountDocument _doc)
        {

            bdeAccounting.WriterHelper.BeginTransaction();
            try
            {

                StaffLoandDocumentBase old = null;
                StaffLoandDocumentBase doc = (StaffLoandDocumentBase)_doc;
                if (_doc.AccountDocumentID != -1)
                {
                    old = bdeAccounting.GetAccountDocument(_doc.AccountDocumentID, true) as StaffLoandDocumentBase;
                    if (doc.directReturnDocumentID == null)
                        doc.directReturnDocumentID = old.directReturnDocumentID;
                }
                else
                {
                    if (doc.directReturnDocumentID == null)
                        doc.directReturnDocumentID = new int[0];
                }

                DHShortTermLoan.SendToPayroll(AID, bdePayroll, bde, bdePayroll.GetEmployee(((StaffLoandDocumentBase)_doc).employeeID), old, (StaffLoandDocumentBase)_doc, bdePayroll.SysPars.staffLongTermLoanPayrollFormulaID);
                int ret = this.postInternal(AID, _doc, bdePayroll.SysPars.staffLongTermLoanAccountID);
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }
    }
}
