REM collect html files
xcopy C:\source\code\BERP\iERPTransactionService\html C:\source\code\WSIS\SubscriberManagmentService\html\ /S /Y /D
xcopy C:\source\code\WorkFlow\PurchaseWorkflow\html C:\source\code\WSIS\SubscriberManagmentService\html\wfservice\ /S /Y /D

copy C:\source\code\RemottingServer\bin\Debug\RemottingServer.exe c:\source\code\wsis\serverbin /Y
copy C:\source\code\RemottingServer\bin\Debug\RemottingServer.pdb c:\source\code\wsis\serverbin /Y

REM create server log folder if it doesn't exist
IF EXIST C:\source\code\WSIS\ServerBin\log GOTO L2
	MKDIR C:\source\code\WSIS\ServerBin\log
:L2