using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BN.DocumentTypeRegisteration
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static INTAPS.RDBMS.DSP DBConnection;
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            InitializeFormSkinning();
            InitializeDataBaseConnection();
            Application.Run(new RegisterDocumentTypes());
        }

        private static void InitializeDataBaseConnection()
        {
            DBConnection = new INTAPS.RDBMS.DSP(INTAPS.RDBMS.DBProvider.MSSQLSERVER);
            DBConnection.ConnectionString = System.Configuration.ConfigurationManager.AppSettings[0];
        }

        private static void InitializeFormSkinning()
        {
            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.LookAndFeel.LookAndFeelHelper.ForceDefaultLookAndFeelChanged();
            DevExpress.UserSkins.BonusSkins.Register();
        }
    }
}