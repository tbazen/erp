using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Reflection;
using BIZNET.iERP;
using System.IO;
using INTAPS.Accounting;

namespace BN.DocumentTypeRegisteration
{
    public partial class RegisterDocumentTypes : XtraForm
    {
        private const string DOCUMENTTYPE_ASSEMBLY_BASETYPE = "INTAPS.Accounting.AccountDocument";
        private const string INITIAL_DIRECTORY = "D:\\Projects";
        private string documentTypeAssemblyPath;
        private string clientHandlerAssemblyPath;
        private string serverHandlerAssemblyPath;

        private Assembly clientHandlerAssembly;
        private Assembly serverHandlerAssembly;
        private string documentTypeAssemblyName;
        private string documentClass;
        private string dbDocName;
        private string clientClass;
        private string serverClass;
        private string loggedErrors;
        private bool errorLogged = false;
        private int registrationCount = 0;
        private int unregisteredCount = 0;
        public RegisterDocumentTypes()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnDocumentTypeAssembly_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            using (OpenFileDialog openDocumentTypeAssembly = new OpenFileDialog())
            {
                openDocumentTypeAssembly.InitialDirectory = INITIAL_DIRECTORY;
                openDocumentTypeAssembly.Filter = "dll files (*.dll)|*.dll";
                openDocumentTypeAssembly.FilterIndex = 0;
                if (openDocumentTypeAssembly.ShowDialog() == DialogResult.OK)
                {
                   documentTypeAssemblyPath = openDocumentTypeAssembly.FileName;
                   documentTypeAssemblyName = System.IO.Path.GetFileNameWithoutExtension(documentTypeAssemblyPath);
                    if (IsValidDocumentTypeAssembly(documentTypeAssemblyName))
                    {
                        btnDocumentTypeAssembly.Text = documentTypeAssemblyPath;
                    }
                    else
                    {
                        Utility.MessageBox.ShowErrorMessage("Please select the correct Document Type assembly and try again!");
                    }
                }
            }

        }

        private static bool IsValidDocumentTypeAssembly(string assemblyName)
        {
            if (assemblyName.Contains("TypeLibrary"))
            {
                return true;
            }
            return false;
        }

        private void btnClientHandlerAssembly_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            using (OpenFileDialog openClientHandlerAssembly = new OpenFileDialog())
            {
                openClientHandlerAssembly.InitialDirectory = INITIAL_DIRECTORY;
                openClientHandlerAssembly.Filter = "dll files (*.dll)|*.dll";
                openClientHandlerAssembly.FilterIndex = 0;
                if (openClientHandlerAssembly.ShowDialog() == DialogResult.OK)
                {
                    clientHandlerAssemblyPath = openClientHandlerAssembly.FileName;
                    string assemblyName = System.IO.Path.GetFileNameWithoutExtension(clientHandlerAssemblyPath);
                    if (IsValidClientHandlerAssembly(assemblyName))
                    {
                        btnClientHandlerAssembly.Text = clientHandlerAssemblyPath;
                    }
                    else
                    {
                        Utility.MessageBox.ShowErrorMessage("Please select the correct Client Handler assembly and try again!");
                    }
                }
            }

        }

        private static bool IsValidClientHandlerAssembly(string assemblyName)
        {
            if (assemblyName.Contains("Client"))
            {
                return true;
            }
            return false;
        }

        private static bool IsValidServerHandlerAssembly(string assemblyName)
        {
            if (assemblyName.Contains("Server"))
            {
                return true;
            }
            return false;
        }

        private void btnServerHandlerAssembly_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            using (OpenFileDialog openServerHandlerAssembly = new OpenFileDialog())
            {
                openServerHandlerAssembly.InitialDirectory = INITIAL_DIRECTORY;
                openServerHandlerAssembly.Filter = "dll files (*.dll)|*.dll";
                openServerHandlerAssembly.FilterIndex = 0;
                if (openServerHandlerAssembly.ShowDialog() == DialogResult.OK)
                {
                    serverHandlerAssemblyPath= openServerHandlerAssembly.FileName;
                    string assemblyName = System.IO.Path.GetFileNameWithoutExtension(serverHandlerAssemblyPath);
                    if (IsValidServerHandlerAssembly(assemblyName))
                    {
                        btnServerHandlerAssembly.Text = serverHandlerAssemblyPath;
                    }
                    else
                    {
                        Utility.MessageBox.ShowErrorMessage("Please select the correct Server Handler assembly and try again!");
                    }
                }
            }
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
           errorLogged = false;
           registrationCount = 0;
           unregisteredCount = 0;
           clientClass = serverClass = null;

            if (ValidateInputs())
            {
                try
                {
                    clientHandlerAssembly = Assembly.LoadFrom(clientHandlerAssemblyPath);
                    serverHandlerAssembly = Assembly.LoadFrom(serverHandlerAssemblyPath);
                    if (btnRegister.Text == "&Register")
                    {
                        loggedErrors = "The following errors occurred while trying to register document types\n";
                        Type[] documentTypes = new Type[chkListUnregDocTypes.CheckedItems.Count];
                        int i = 0;
                        foreach (ListViewItem item in chkListUnregDocTypes.CheckedItems)
                        {
                            Type docType = (Type)item.Tag;
                            documentTypes[i] = docType;
                            i++;
                        }
                        RegisterSelectedDocumentTypes(documentTypes);
                        DisplayInformationToUser(errorLogged, registrationCount);
                        LoadDocumentTypes();
                    }
                    else
                    {
                        loggedErrors = "The following errors occurred while trying to unregister document types\n";
                        Type[] documentTypes = new Type[chkListRegDocTypes.CheckedItems.Count];
                        int i = 0;
                        foreach (ListViewItem item in chkListRegDocTypes.CheckedItems)
                        {
                            Type docType = (Type)item.Tag;
                            documentTypes[i] = docType;
                            i++;
                        }
                        UnregisterSelectedDocumentTypes(documentTypes);
                        DisplayUnregistrationInformationToUser(errorLogged, unregisteredCount);
                        LoadDocumentTypes();
                    }
                }
                catch (ReflectionTypeLoadException ex)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (Exception exSub in ex.LoaderExceptions)
                    {
                        sb.AppendLine(exSub.Message);
                        if (exSub is FileNotFoundException)
                        {
                            FileNotFoundException exFileNotFound = exSub as FileNotFoundException;
                            if (!string.IsNullOrEmpty(exFileNotFound.FusionLog))
                            {
                                sb.AppendLine("Fusion Log:");
                                sb.AppendLine(exFileNotFound.FusionLog);
                            }
                        }
                        sb.AppendLine();
                    }
                    string errorMessage = sb.ToString();
                    Utility.MessageBox.ShowErrorMessage(errorMessage);
                }
            }
            else
            {
                Utility.MessageBox.ShowErrorMessage("Please make sure you have added all assemblies and selected document types and try again!");
            }
        }

        private void DisplayUnregistrationInformationToUser(bool errorLogged, int unregisteredCount)
        {
            if (unregisteredCount > 0)
            {
                string message = registrationCount == 1 ? " document type successfully unregistered!" : " document types successfully unregistered!";
                Utility.MessageBox.ShowSuccessMessage(unregisteredCount + message);
            }
            if (errorLogged)
            {
                Utility.MessageBox.ShowErrorMessage(loggedErrors);
            }
        }

        private void RegisterSelectedDocumentTypes(Type[] documentTypes)
        {
            foreach (Type documentType in documentTypes)
            {
                documentClass = documentType.FullName;
                serverClass = clientClass = null;
                string docName = documentType.Name;
                dbDocName = docName.Replace("Document", "");
                GetDocumentTypeClientHandler(clientHandlerAssembly);
                GetDocumentTypeServerHandler(serverHandlerAssembly);
                ProcessDocumentTypeRegistration(ref errorLogged, ref registrationCount);
            }

        }

        private void ProcessDocumentTypeRegistration(ref bool errorLogged, ref int registrationCount)
        {
            if (!RegistrationErrorsLogged())
            {
                RegisterDocumentType();
                registrationCount++;
            }
            else
            {
                errorLogged = true;
            }
        }

        private void DisplayInformationToUser(bool errorLogged, int registrationCount)
        {
            if (registrationCount > 0)
            {
                string message = registrationCount == 1 ? " document type successfully registered!" : " document types successfully registered!";
                Utility.MessageBox.ShowSuccessMessage(registrationCount + message);
            }
            if (errorLogged)
            {
                Utility.MessageBox.ShowErrorMessage(loggedErrors);
            }
        }

        private void UnregisterSelectedDocumentTypes(Type[] documentTypes)
        {
            foreach (Type documentType in documentTypes)
            {
                documentClass = documentType.FullName;
                serverClass = clientClass = null;
                string docName = documentType.Name;
                dbDocName = docName.Replace("Document", "");
                GetDocumentTypeClientHandler(clientHandlerAssembly);
                GetDocumentTypeServerHandler(serverHandlerAssembly);
                ProcessDocumentTypeUnregistration(ref errorLogged, ref registrationCount);
            }
        }
        private void ProcessDocumentTypeUnregistration(ref bool errorLogged, ref int registrationCount)
        {
            if (!RegistrationErrorsLogged())
            {
                UnregisterDocumentType();
                unregisteredCount++;
            }
            else
            {
                errorLogged = true;
            }
        }
        private void UnregisterDocumentType()
        {
            try
            {
                Program.DBConnection.BeginTransaction();
                UnregisterAccountingDocumentType();
                UnregisterMaterialManagementDocumentType();
                UnregisterMaterialManagementDocumentHandler();
                Program.DBConnection.CommitTransaction();
            }
            catch (Exception ex)
            {
                Program.DBConnection.RollBackTransaction();
                Utility.MessageBox.ShowErrorMessage(ex.ToString());
            }
        }
        private void UnregisterAccountingDocumentType()
        {
            string query = String.Format("Delete from DocumentType where documentClass='{0}'", documentClass);
            Program.DBConnection.ExecuteNonQuery(query);
        }
        private void UnregisterMaterialManagementDocumentType()
        {
            string query = String.Format("Delete from [Material_2005].[dbo].[DocumentType] where documentClass='{0}'", documentClass);
            Program.DBConnection.ExecuteNonQuery(query);

        }
        private void UnregisterMaterialManagementDocumentHandler()
        {
            string query = String.Format("Delete from [Material_2005].[dbo].[DocumentHandler] where clientClass='{0}'", clientClass);
            Program.DBConnection.ExecuteNonQuery(query);
        }
        private void GetDocumentTypeServerHandler(Assembly serverHandlerAssembly)
        {
            foreach (Type serverHandlerType in serverHandlerAssembly.GetTypes())
            {
                if(IsServerHandlerFoundInAssemblyBaseTypes(serverHandlerType))
                {
                    //If handler is server handler of the current document type
                    if (serverHandlerType.Name.Equals("DH" + dbDocName))
                    {
                        serverClass = String.Format("{0},{1}", serverHandlerType.FullName, System.IO.Path.GetFileNameWithoutExtension(btnServerHandlerAssembly.Text));
                        break;
                    }
                }

                #region Commented Lines
                //If current type is server handler
                //if (typeof(IDocumentServerHandler).IsAssignableFrom(serverHandlerType))
                //{
                //    //If handler is server handler of the current document type
                //    if (serverHandlerType.Name.Equals("DH" + dbDocName))
                //    {
                //        serverClass = String.Format("{0},{1}", serverHandlerType.FullName, System.IO.Path.GetFileNameWithoutExtension(btnServerHandlerAssembly.Text));
                //        break;
                //    }
                //}
                #endregion

            }
        }

        private bool IsServerHandlerFoundInAssemblyBaseTypes(Type baseType)
        {
            if (baseType != null)
            {
                if (!typeof(IDocumentServerHandler).IsAssignableFrom(baseType))
                {
                    return IsServerHandlerFoundInAssemblyBaseTypes(baseType.BaseType);
                }
                else
                    return true;
            }
            else
                return false;
        }

        private void GetDocumentTypeClientHandler(Assembly clientHandlerAssembly)
        {
            foreach (Type clientHandlerType in clientHandlerAssembly.GetTypes())
            {
                if(IsClientHandlerFoundInAssemblyBaseTypes(clientHandlerType))
                {
                    //If handler is client handler of the current document type
                    if (clientHandlerType.Name.Equals("DCH" + dbDocName))
                    {
                        clientClass = String.Format("{0},{1}", clientHandlerType.FullName, System.IO.Path.GetFileNameWithoutExtension(btnClientHandlerAssembly.Text));
                        break;
                    }
                }

                #region Commendted Lines
                //If current type is client handler
                //if (typeof(IGenericDocumentClientHandler).IsAssignableFrom(clientHandlerType))
                //{
                //    //If handler is client handler of the current document type
                //    if (clientHandlerType.Name.Equals("DCH" + dbDocName))
                //    {
                //        clientClass = String.Format("{0},{1}", clientHandlerType.FullName, System.IO.Path.GetFileNameWithoutExtension(btnClientHandlerAssembly.Text));
                //        break;
                //    }
                //}
                #endregion
            }
        }

        private bool IsClientHandlerFoundInAssemblyBaseTypes(Type baseType)
        {
            if (baseType != null)
            {
                if (!typeof(IGenericDocumentClientHandler).IsAssignableFrom(baseType))
                {
                    return IsClientHandlerFoundInAssemblyBaseTypes(baseType.BaseType);
                }
                else
                    return true;
            }
            else
                return false;
        }

        private bool RegistrationErrorsLogged()
        {
            string dataStructure = "for datastructure " + documentClass ?? "";

           
                bool errorFound = false;
                if (documentClass == null)
                {
                    loggedErrors += "-> Document Type data structure could not found\n";
                    errorFound = true;
                }

                if (clientClass == null)
                {
                    loggedErrors += String.Format("-> Client handler could not be found {0}\n", dataStructure);
                    errorFound = true;
                }
                if (serverClass == null)
                {
                    loggedErrors += String.Format("-> Server handler could not be found {0}\n", dataStructure);
                    errorFound = true;
                }
                if (errorFound)
                {
                    return true;
                }
            
            return false;
        }

        private void RegisterDocumentType()
        {
            const string selectIDQuery = "Select Top(1) id From DocumentType order by id desc";
            int currDocTypeID = (int)Program.DBConnection.ExecuteScalar(selectIDQuery);
            int newDocTypeID = currDocTypeID + 1;
            try
            {
                Program.DBConnection.BeginTransaction();
                RegisterAccountingDocumentTypeAndHandler(newDocTypeID);
                //RegisterMaterialManagementDocumentType(newDocTypeID);
               // RegisterMaterialManagementDocumentHandler(newDocTypeID);
                Program.DBConnection.CommitTransaction();
            }
            catch (Exception ex)
            {
                Program.DBConnection.RollBackTransaction();
                Utility.MessageBox.ShowErrorMessage(ex.ToString());
            }
        }

        private void RegisterMaterialManagementDocumentHandler(int newDocTypeID)
        {
            Program.DBConnection.Insert("[Material_2005].[dbo].[DocumentHandler]"
                  , new string[]
                   {
                    "[documentTypeID]",
                    "[name]",
                    "[serverClass]",
                    "[clientClass]",
                   }, new object[]
                {   newDocTypeID,
                    dbDocName,
                    serverClass,
                    clientClass,
                });
        }

        private void RegisterMaterialManagementDocumentType(int newDocTypeID)
        {
            Program.DBConnection.Insert("[Material_2005].[dbo].[DocumentType]"
                  , new string[]
                   {
                    "[id]",
                    "[name]",
                    "[description]",
                    "[documentClass]",
                    "[assemblyName]",
                    "[system]"
                   }, new object[]
                {   newDocTypeID,
                    dbDocName,
                    null,
                    documentClass,
                    documentTypeAssemblyName,
                    false
                });
        }

        private void RegisterAccountingDocumentTypeAndHandler(int newDocTypeID)
        {
            Program.DBConnection.Insert("DocumentType"
                , new string[]
                   {
                    "[id]",
                    "[name]",
                    "[description]",
                    "[documentClass]",
                    "[assemblyName]",
                    "[system]"
                   }, new object[]
                {   newDocTypeID,
                    dbDocName,
                    null,
                    documentClass,
                    documentTypeAssemblyName,
                    false
                });
            Program.DBConnection.Insert("DocumentHandler"
                  , new string[]
                   {
                    "[documentTypeID]",
                    "[name]",
                    "[serverClass]",
                    "[clientClass]",
                   }, new object[]
                {   newDocTypeID,
                    dbDocName,
                    serverClass,
                    clientClass,
                });
        }

        private bool IsDocumentTypeAlreadyRegistered(string documentTypeClass)
        {
            string query = String.Format("Select count(*) from DocumentType where documentClass='{0}'", documentTypeClass);
            if ((int)Program.DBConnection.ExecuteScalar(query) > 0)
            {
                return true;
            }
            return false;
        }

        private bool ValidateInputs()
        {
            if (btnDocumentTypeAssembly.Text == "" 
                || btnClientHandlerAssembly.Text == "" 
                || btnServerHandlerAssembly.Text == ""
                || (chkListUnregDocTypes.CheckedItems.Count== 0 && chkListRegDocTypes.CheckedItems.Count==0))
            {
                return false;
            }
            return true;
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (btnDocumentTypeAssembly.Text == "")
            {
                Utility.MessageBox.ShowErrorMessage("You have to select the document type library assembly to load documents.");
                return;
            }
            else
            {
                LoadDocumentTypes();
            }
        }

        private void LoadDocumentTypes()
        {
            chkListUnregDocTypes.Items.Clear();
            chkListRegDocTypes.Items.Clear();
            Assembly documentTypeAssembly = Assembly.LoadFrom(documentTypeAssemblyPath);
            int i = 0;
            int j = 0;
            foreach (Type documentType in documentTypeAssembly.GetTypes())
            {
                if (DocumentTypeFoundInAssemblyBaseTypes(documentType))
                {
                    if (!IsDocumentTypeAlreadyRegistered(documentType.FullName))//Unregistred
                    {
                        // object[] docAttributes = documentType.GetCustomAttributes(typeof(AccountDocumentAttribute), true);
                        // string docName = docAttributes!=null && docAttributes.Length == 1 ? ((AccountDocumentAttribute)docAttributes[0]).documentName : documentType.Name;
                        string docName = documentType.Name;
                        chkListUnregDocTypes.Items.Add(docName);
                        chkListUnregDocTypes.Items[i].Tag = documentType;
                        i++;
                    }
                    else //registered
                    {
                        string documentName = documentType.Name;
                        chkListRegDocTypes.Items.Add(documentName);
                        chkListRegDocTypes.Items[j].Tag = documentType;
                        j++;
                    }
                }
            }
            #region Commented Lines
            //foreach (Type documentType in documentTypeAssembly.GetTypes())
            //{

            //    if (documentType.BaseType != null)
            //    {
            //        Type documentBaseType = documentType.BaseType;
            //        Type documentTopBestType = documentBaseType.BaseType;
            //        if (documentTopBestType == null)
            //        {
            //            if (documentBaseType.ToString().Equals(DOCUMENTTYPE_ASSEMBLY_BASETYPE))
            //            {
            //                if (!IsDocumentTypeAlreadyRegistered(documentType.FullName))//Unregistred
            //                {
            //                    // object[] docAttributes = documentType.GetCustomAttributes(typeof(AccountDocumentAttribute), true);
            //                    // string docName = docAttributes!=null && docAttributes.Length == 1 ? ((AccountDocumentAttribute)docAttributes[0]).documentName : documentType.Name;
            //                    string docName = documentType.Name;
            //                    chkListUnregDocTypes.Items.Add(docName);
            //                    chkListUnregDocTypes.Items[i].Tag = documentType;
            //                    i++;
            //                }
            //                else //registered
            //                {
            //                    string documentName = documentType.Name;
            //                    chkListRegDocTypes.Items.Add(documentName);
            //                    chkListRegDocTypes.Items[j].Tag = documentType;
            //                    j++;
            //                }
            //            }
            //        }
            //        else
            //        {
            //            if (documentBaseType.ToString().Equals(DOCUMENTTYPE_ASSEMBLY_BASETYPE)
            //                || documentTopBestType.ToString().Equals(DOCUMENTTYPE_ASSEMBLY_BASETYPE))
            //            {
            //                if (!IsDocumentTypeAlreadyRegistered(documentType.FullName))//Unregistred
            //                {
            //                    // object[] docAttributes = documentType.GetCustomAttributes(typeof(AccountDocumentAttribute), true);
            //                    // string docName = docAttributes!=null && docAttributes.Length == 1 ? ((AccountDocumentAttribute)docAttributes[0]).documentName : documentType.Name;
            //                    string docName = documentType.Name;
            //                    chkListUnregDocTypes.Items.Add(docName);
            //                    chkListUnregDocTypes.Items[i].Tag = documentType;
            //                    i++;
            //                }
            //                else //registered
            //                {
            //                    string documentName = documentType.Name;
            //                    chkListRegDocTypes.Items.Add(documentName);
            //                    chkListRegDocTypes.Items[j].Tag = documentType;
            //                    j++;
            //                }
            //            }
            //        }
            //    }

            //}
            #endregion
        }

        private bool DocumentTypeFoundInAssemblyBaseTypes(Type baseType)
        {
            if (baseType != null)
            {
                if (!baseType.ToString().Equals(DOCUMENTTYPE_ASSEMBLY_BASETYPE))
                {
                    return DocumentTypeFoundInAssemblyBaseTypes(baseType.BaseType);
                }
                else
                    return true;
            }
            else
                return false;
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem item in chkListUnregDocTypes.Items)
            {
                if (chkSelectAllReg.Checked)
                    item.Checked = true;
                else
                    item.Checked = false;
            }
        }
        private void chkListRegDocTypes_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (e.Item.Checked)
            {
                if (chkListUnregDocTypes.CheckedItems.Count > 0)
                    UncheckUnregisteredDocuments();
                btnRegister.Text = "&Unregister";
            }
        }
        private void UncheckUnregisteredDocuments()
        {
            foreach (ListViewItem item in chkListUnregDocTypes.CheckedItems)
            {
                item.Checked = false;
            }
        }

        private void chkListUnregDocTypes_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (e.Item.Checked)
            {
                if (chkListRegDocTypes.CheckedItems.Count > 0)
                    UncheckRegisteredDocuments();
                btnRegister.Text = "&Register";
            }
        }

        private void UncheckRegisteredDocuments()
        {
            foreach (ListViewItem item in chkListRegDocTypes.CheckedItems)
            {
                item.Checked = false;
            }
        }

        private void chkSelectAllUnreg_CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem item in chkListUnregDocTypes.Items)
            {
                if (chkSelectAllUnreg.Checked)
                    item.Checked = true;
                else
                    item.Checked = false;
            }
        }

        
    }
}