using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Xml;



namespace BN.DocumentTypeRegisteration.Utility
{
    public class MessageBox
    {
        public static DialogResult ShowErrorMessage(string error)
        {
            return XtraMessageBox.Show(error, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
        }

        public static DialogResult ShowWarningMessage(string warning)
        {
            return XtraMessageBox.Show(warning, "Warning", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning);
        }

        public static DialogResult ShowSuccessMessage(string success)
        {
            return XtraMessageBox.Show(success, "Success", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
        }
      
    }

    public class XMLUtility
    {
        public static object XmlToObject(string xml, Type type)
        {
            if (string.IsNullOrEmpty(xml))
                return null;
            XmlSerializer x = new XmlSerializer(type);
            return x.Deserialize(new System.IO.StringReader(xml));
        }
        public static string ObjectToXml(object o)
        {
            XmlSerializer x = new XmlSerializer(o.GetType());
            using (System.IO.StringWriter sw = new System.IO.StringWriter())
            {
                x.Serialize(sw, o);
                sw.Close();
                return sw.ToString();
            }
        }
    }
}
