using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using BIZNET.iERP.Server;
using BIZNET.iERP;


namespace INTAPS.ClientServer.RemottingServer
{
    public static partial class MaintainanceJob
    {
        static void deprecatePayrollDocument(iERPTransactionBDE bde)
        {
            
            DocumentType dt = bde.Accounting.GetDocumentTypeByType(typeof(PayrollDocument));
            if (dt == null)
                throw new ServerUserMessage("Invalid document type " + typeof(PayrollDocument));
            Console.WriteLine("Upgrading " + dt.name);
            int[] docids = bde.Accounting.WriterHelper.GetColumnArray<int>(string.Format("Select ID from {0}.dbo.Document where documentTypeID={1}"
                , bde.Accounting.DBName, bde.Accounting.GetDocumentTypeByType(typeof(PayrollDocument)).id), 0);
            DocumentSerialType type = bde.Accounting.getDocumentSerialType("GEN");
            if (type == null)
                throw new ServerUserMessage("Configure GEN Document refernce type");
            genRefType = type.id;
            int c = docids.Length;
            Console.WriteLine("Grouping payroll documents");
            Console.WriteLine(c + " payroll documents found");
            Console.WriteLine();
            Dictionary<string, List<PayrollDocument>> sets = new Dictionary<string, List<PayrollDocument>>();
            foreach (int docid in docids)
            {
                c--;
                Console.CursorTop--;
                Console.WriteLine(c + "                         ");
                PayrollDocument oldDoc = null;
                try
                {
                    AccountTransaction[] _oldTran = bde.Accounting.GetTransactionsOfDocument(docid);
                    oldDoc = bde.Accounting.GetAccountDocument(docid, true) as PayrollDocument;
                    if (oldDoc == null)
                        throw new ServerUserMessage("Coudlnt' load document ID:" + docid);
                    string key = oldDoc.payPeriod.id + "_" + oldDoc.DocumentDate.ToString();
                    List<PayrollDocument> set;
                    if (sets.ContainsKey(key))
                    {
                        set = sets[key];
                    }
                    else
                    {
                        set = new List<PayrollDocument>();
                        sets.Add(key, set);
                    }
                    set.Add(oldDoc);
                }
                catch (Exception ex)
                {
                    bde.WriterHelper.RollBackTransaction();
                    Console.WriteLine(string.Format("Error processing document ID {0} PaperRef {1}\n  {2} ", docid
                        , oldDoc == null ? "[Invalid Doc ID]" : oldDoc.PaperRef
                        , ex.Message));
                    Console.WriteLine();
                }
                List<IncomeTaxDeclarationDocument> incomeTaxDeclarations = new List<IncomeTaxDeclarationDocument>();
                List<PensionTaxDeclarationDocument> pensionDeclrations = new List<PensionTaxDeclarationDocument>();

                c = sets.Count;
                Console.WriteLine(c + " groups prepared.");
                Console.WriteLine(c + " posting groups.");
                foreach (KeyValuePair<string, List<PayrollDocument>> kv in sets)
                {
                    PayrollSetDocument set = new PayrollSetDocument();
                    set.at = new AppliesToEmployees();

                    set.at.Employees = new int[kv.Value.Count];
                    int i = 0;
                    foreach (PayrollDocument doc in kv.Value)
                    {
                        set.at.Employees[i++] = doc.employee.id;
                    }
                    set.DocumentDate = kv.Value[0].DocumentDate;
                    set.LongDescription = kv.Value[0].payPeriod.name + " Payroll";

                }
            }
        }

    }
}
