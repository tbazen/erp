using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using BIZNET.iERP.Server;
using BIZNET.iERP;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using INTAPS.RDBMS;
using System.Xml.Serialization;


namespace INTAPS.ClientServer.RemottingServer
{

    public static partial class MaintainanceJob
    {

        [MaintenanceJobMethod("Ticks Upgrade: Convert core date time fields to ticks")]
        public static void convertCoreDateTimeToTicks()
        {
            iERPTransactionBDE bdeBERP = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            AccountingBDE accounting = bdeBERP.Accounting;
            lock (accounting.WriterHelper)
            {
                INTAPS.RDBMS.SQLHelper helper= accounting.GetReaderHelper();
                int count = (int)helper.ExecuteScalar("Select count(*) from AccountTransaction");
                SqlDataReader reader = helper.ExecuteReader("Select id,ExecDate from AccountTransaction");
                Console.WriteLine(count + " records in AccountTransaction table");
                Console.WriteLine();
                while (reader.Read())
                {
                    Console.CursorTop--;
                    Console.WriteLine((count--) + "                          ");
                    int id = (int)reader[0];
                    DateTime time = (DateTime)reader[1];
                    accounting.WriterHelper.ExecuteNonQuery(string.Format("Update {0}.dbo.AccountTransaction set tranTicks={1} where id={2}", accounting.DBName, time.Ticks, id));
                }
                reader.Close();

                count = (int)helper.ExecuteScalar("Select count(*) from AccountBalance");
                reader = helper.ExecuteReader("Select accountID,itemID,LastTransactionDate from AccountBalance");
                Console.WriteLine(count + " records in AccountBalance table");
                Console.WriteLine();
                while (reader.Read())
                {
                    Console.CursorTop--;
                    Console.WriteLine((count--) + "                          ");
                    int accountID = (int)reader[0];
                    int itemID= (int)reader[1];
                    DateTime time = (DateTime)reader[2];
                    accounting.WriterHelper.ExecuteNonQuery(string.Format("Update {0}.dbo.AccountBalance set tranTicks={1} where accountID={2} and itemID={3}", accounting.DBName, time.Ticks,accountID,itemID));
                }
                reader.Close();

                count = (int)helper.ExecuteScalar("Select count(*) from Document");
                reader = helper.ExecuteReader("Select id,DocumentDate from Document");
                Console.WriteLine(count + " records in Document table");
                Console.WriteLine();
                while (reader.Read())
                {
                    Console.CursorTop--;
                    Console.WriteLine((count--) + "                          ");
                    int id = (int)reader[0];
                    DateTime time = (DateTime)reader[1];
                    accounting.WriterHelper.ExecuteNonQuery(string.Format("Update {0}.dbo.Document set tranTicks={1} where id={2}", accounting.DBName, time.Ticks, id));
                }
                reader.Close();

                count = (int)helper.ExecuteScalar("Select count(*) from Document_Deleted");
                reader = helper.ExecuteReader("Select id,DocumentDate from Document_Deleted");
                Console.WriteLine(count + " records in Document_Deleted table");
                Console.WriteLine();
                while (reader.Read())
                {
                    Console.CursorTop--;
                    Console.WriteLine((count--) + "                          ");
                    int id = (int)reader[0];
                    DateTime time = (DateTime)reader[1];
                    accounting.WriterHelper.ExecuteNonQuery(string.Format("Update {0}.dbo.Document_Deleted set tranTicks={1} where id={2}", accounting.DBName, time.Ticks, id));
                }
                reader.Close();
            }
        }

    }
}
