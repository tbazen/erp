using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using BIZNET.iERP.Server;
using BIZNET.iERP;


namespace INTAPS.ClientServer.RemottingServer
{
    public static partial class MaintainanceJob
    {
        public static void upgradeTradeRelationTransactionToVersion2()
        {
            try
            {
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                Console.WriteLine("Upgrading Supplier Advance Payment");
                int[] docids = accounting.WriterHelper.GetColumnArray<int>(string.Format("Select ID from {0}.dbo.Document where documentTypeID={1}", accounting.DBName, accounting.GetDocumentTypeByType(typeof(SupplierReceivableDocument)).id), 0);
                int N;
                TransactionItems[] test = bde.SearchTransactionItems(0, 5, new object[] { "Supplier Service Advance Payment" }, new string[] { "name" }, out N);
                if (test.Length == 0)
                    throw new Exception("Supplier Service Advance Payment item not found in database");
                if (test.Length >1)
                    throw new Exception("Multipe Supplier Service Advance Payment items found in database");
                TransactionItems item = test[0];
                int c = docids.Length;
                Console.WriteLine();
                foreach (int docid in docids)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                         ");
                    try
                    {
                        SupplierReceivableDocument advance = accounting.GetAccountDocument(docid, true) as SupplierReceivableDocument;
                        SupplierAdvancePayment2Document advance2 = new SupplierAdvancePayment2Document();
                        advance2.CoypFrom(advance);
                        advance2.paymentMethod = advance.paymentMethod;
                        advance2.assetAccountID = advance.assetAccountID;
                        advance2.serviceChargeAmount=advance.serviceChargeAmount;
                        advance2.serviceChargePayer=advance.serviceChargePayer;
                        advance2.relationCode=advance.supplierCode;
                        advance2.checkNumber=advance.checkNumber;
                        advance2.transferReference=advance.slipReferenceNo;
                        advance2.cpoNumber=advance.cpoNumber;
                        advance2.amount=advance2.paidAmount = advance.amount;
                        advance2.manualVATBase = 0;
                        advance2.manualWithholdBase= 0;
                        advance2.items = new TransactionDocumentItem[] { new TransactionDocumentItem(bde.SysPars.mainCostCenterID, item, 1, advance.amount) };
                        advance2.taxImposed = new TaxImposed[0];
                        advance2.taxRates = null;
                        advance2.paymentVoucher = advance.voucher;
                        accounting.PostGenericDocument(-1, advance2);

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(string.Format("Error processing document ID {0}\n  {1} ", docid, ex.Message));
                    }
                    c--;
                }
                
                Console.WriteLine("Upgrading Customer Advance Payment");
                docids = accounting.WriterHelper.GetColumnArray<int>(string.Format("Select ID from {0}.dbo.Document where documentTypeID={1}", accounting.DBName, accounting.GetDocumentTypeByType(typeof(CustomerPayableDocument)).id), 0);
                test = bde.SearchTransactionItems(0, 5, new object[] { "Customer Service Advance Payment" }, new string[] { "name" }, out N);
                if (test.Length == 0)
                    throw new Exception("Customer Service Advance Payment item not found in database");
                if (test.Length > 1)
                    throw new Exception("Multipe Customer Service Advance Payment items found in database");
                item = test[0];
                c = docids.Length;
                Console.WriteLine();
                foreach (int docid in docids)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                         ");
                    try
                    {
                        CustomerPayableDocument advance = accounting.GetAccountDocument(docid, true) as CustomerPayableDocument;
                        CustomerAdvancePayment2Document advance2 = new CustomerAdvancePayment2Document();
                        advance2.CoypFrom(advance);
                        advance2.paymentMethod = advance.paymentMethod;
                        advance2.assetAccountID = advance.assetAccountID;
                        advance2.serviceChargeAmount = advance.serviceChargeAmount;
                        advance2.serviceChargePayer = advance.serviceChargePayer;
                        advance2.relationCode = advance.customerCode;
                        advance2.checkNumber = advance.checkNumber;
                        advance2.transferReference = advance.slipReferenceNo;
                        advance2.cpoNumber = advance.cpoNumber;
                        advance2.amount = advance2.paidAmount = advance.amount;
                        advance2.manualVATBase = -1;
                        advance2.manualWithholdBase = -1;
                        advance2.items = new TransactionDocumentItem[] { new TransactionDocumentItem(bde.SysPars.mainCostCenterID, item, 1, advance.amount) };
                        
                        advance2.taxRates = null;
                        List<TaxImposed> taxes = new List<TaxImposed>();
                        if (advance.calculateVAT && AccountBase.AmountGreater(advance.amount,0))
                        {
                            taxes.Add(new TaxImposed(TaxType.VAT, advance.amount, advance.amount * .15));
                        }
                        if (advance.calculateWithholding && AccountBase.AmountGreater(advance.amount, 0))
                        {
                            taxes.Add(new TaxImposed(TaxType.WithHoldingTax, advance.amount, -advance.amount * .02));
                        }
                        advance2.taxImposed = taxes.ToArray();
                        advance2.salesVoucher = advance.voucher;
                        accounting.PostGenericDocument(-1, advance2);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(string.Format("Error processing document ID {0}\n  {1} ", docid, ex.Message));
                    }
                    c--;
                }
                
                //supplier credit settlment
                Console.WriteLine("Upgrading Supplier Credit Settlment");
                docids = accounting.WriterHelper.GetColumnArray<int>(string.Format("Select ID from {0}.dbo.Document where documentTypeID={1}", accounting.DBName, accounting.GetDocumentTypeByType(typeof(SupplierPayableDocument)).id), 0);
                test = bde.SearchTransactionItems(0, 5, new object[] { "Supplier Credit Settlement Payment" }, new string[] { "name" }, out N);
                if (test.Length == 0)
                    throw new Exception("Supplier Credit Settlement Payment item not found in database");
                if (test.Length > 1)
                    throw new Exception("Multipe Supplier Credit Settlement Payment items found in database");
                item = test[0];
                c = docids.Length;
                Console.WriteLine();
                foreach (int docid in docids)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                         ");
                    try
                    {
                        SupplierPayableDocument creditSettlement = accounting.GetAccountDocument(docid, true) as SupplierPayableDocument;
                        SupplierCreditSettlement2Document creditSettlement2 = new SupplierCreditSettlement2Document();
                        creditSettlement2.CoypFrom(creditSettlement);
                        creditSettlement2.paymentMethod = creditSettlement.paymentMethod;
                        creditSettlement2.assetAccountID = creditSettlement.assetAccountID;
                        creditSettlement2.serviceChargeAmount = creditSettlement.serviceChargeAmount;
                        creditSettlement2.serviceChargePayer = creditSettlement.serviceChargePayer;
                        creditSettlement2.relationCode = creditSettlement.supplierCode;
                        creditSettlement2.checkNumber = creditSettlement.checkNumber;
                        creditSettlement2.transferReference = creditSettlement.slipReferenceNo;
                        creditSettlement2.cpoNumber = creditSettlement.cpoNumber;
                        creditSettlement2.amount = creditSettlement2.paidAmount = creditSettlement.amount;
                        creditSettlement2.manualVATBase = 0;
                        creditSettlement2.manualWithholdBase = 0;
                        creditSettlement2.items = new TransactionDocumentItem[] { new TransactionDocumentItem(bde.SysPars.mainCostCenterID, item, 1, creditSettlement.amount) };
                        creditSettlement2.taxRates = null;
                        creditSettlement2.taxImposed = new TaxImposed[0];
                        creditSettlement2.paymentVoucher = creditSettlement.voucher;
                        accounting.PostGenericDocument(-1, creditSettlement2);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(string.Format("Error processing document ID {0}\n  {1} ", docid, ex.Message));
                    }
                    c--;
                }

                //Settlement of Customer Creditt
                Console.WriteLine("Settlement of Customer Creditt");
                docids = accounting.WriterHelper.GetColumnArray<int>(string.Format("Select ID from {0}.dbo.Document where documentTypeID={1}", accounting.DBName, accounting.GetDocumentTypeByType(typeof(CustomerReceivableDocument)).id), 0);
                test = bde.SearchTransactionItems(0, 5, new object[] { "Customer Credit Settlement Payment" }, new string[] { "name" }, out N);
                if (test.Length == 0)
                    throw new Exception("Customer Credit Settlement Payment item not found in database");
                if (test.Length > 1)
                    throw new Exception("Multipe Customer Credit Settlement Payment items found in database");
                item = test[0];
                c = docids.Length;
                Console.WriteLine();
                foreach (int docid in docids)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                         ");
                    try
                    {
                        CustomerReceivableDocument creditSettlement = accounting.GetAccountDocument(docid, true) as CustomerReceivableDocument;
                        CustomerCreditSettlement2Document creditSettlement2 = new CustomerCreditSettlement2Document();
                        creditSettlement2.CoypFrom(creditSettlement);
                        creditSettlement2.paymentMethod = creditSettlement.paymentMethod;
                        creditSettlement2.assetAccountID = creditSettlement.assetAccountID;
                        creditSettlement2.serviceChargeAmount = creditSettlement.serviceChargeAmount;
                        creditSettlement2.serviceChargePayer = creditSettlement.serviceChargePayer;
                        creditSettlement2.relationCode = creditSettlement.customerCode;
                        creditSettlement2.checkNumber = creditSettlement.checkNumber;
                        creditSettlement2.transferReference = creditSettlement.slipReferenceNo;
                        creditSettlement2.cpoNumber = creditSettlement.cpoNumber;
                        creditSettlement2.amount = creditSettlement2.paidAmount = creditSettlement.amount;
                        creditSettlement2.manualVATBase = 0;
                        creditSettlement2.manualWithholdBase = 0;
                        creditSettlement2.items = new TransactionDocumentItem[] { new TransactionDocumentItem(bde.SysPars.mainCostCenterID, item, 1, creditSettlement.amount) };
                        creditSettlement2.taxRates = null;
                        creditSettlement2.taxImposed = new TaxImposed[0];
                        creditSettlement2.salesVoucher = creditSettlement.voucher;
                        accounting.PostGenericDocument(-1, creditSettlement2);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(string.Format("Error processing document ID {0}\n  {1} ", docid, ex.Message));
                    }
                    c--;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(string.Format("Fatal error upgrading trade relation transactions {0} ", ex.Message));
            }
        }
        public static void upgradeAdjustmentDocumentType()
        {
            try
            {
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                Console.WriteLine("Upgrading Adjustment Documents");
                int[] docids = accounting.WriterHelper.GetColumnArray<int>(string.Format("Select ID from {0}.dbo.Document where documentTypeID={1}", accounting.DBName, accounting.GetDocumentTypeByType(typeof(AdjustmentDocument)).id), 0);

                int c = docids.Length;
                Console.WriteLine();
                foreach (int docid in docids)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                         ");
                    try
                    {
                        AdjustmentDocument adj = accounting.GetAccountDocument(docid, true) as AdjustmentDocument;
                        bool upgraded = false;
                        foreach (AdjustmentAccount ac in adj.adjustmentAccounts)
                        {
                            if (ac.costCenterID!=-1)
                            {
                                upgraded = true;
                            }
                            else if (ac.costCenterID == -1)
                            {
                                if (upgraded)
                                    throw new ServerUserMessage("Upgraded and unappgraded mixed");
                            }
                        }
                        if (upgraded)
                        {
                            Console.WriteLine("Already upgraded");
                        }
                        else
                        {
                            foreach (AdjustmentAccount ac in adj.adjustmentAccounts)
                            {
                                CostCenterAccount cs = accounting.GetCostCenterAccount(ac.accountID);
                                ac.accountID = cs.accountID;
                                ac.costCenterID = cs.costCenterID;
                            }
                            accounting.PostGenericDocument(-1, adj);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(string.Format("Error processing document ID {0}\n  {1} ", docid, ex.Message));
                    }
                    c--;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("Fatal error upgrading trade relation transactions {0} ", ex.Message));
            }
        }
        public static void fixPayrollDocumentDescription()
        {
            Console.WriteLine("Fixing payroll description for transaction journal by adding payroll period.");
            Console.WriteLine("You should not run this job more than once. Are you sure you want to continue?(Y/N)");
            if (Console.ReadLine() != "Y")
                return;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            try
            {
                int[] docids = accounting.WriterHelper.GetColumnArray<int>(string.Format("Select id from {0}.dbo.Document where documentTypeID={1}", accounting.DBName, accounting.GetDocumentTypeByType(typeof(PayrollSetDocument)).id), 0);
                Console.WriteLine("Fixing " + docids.Length + " documents");
                int c = docids.Length;
                Console.WriteLine();
                foreach (int docid in docids)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                               ");
                    PayrollSetDocument doc = accounting.GetAccountDocument(docid, true) as PayrollSetDocument;
                    doc.LongDescription = doc.ShortDescription;
                    doc.ShortDescription = doc.payPeriod.name + " " + doc.ShortDescription;
                    accounting.UpdateAccountDocumentData(-1, doc);
                    accounting.WriterHelper.Update(accounting.DBName, "Document"
                        , new string[] { "LongDescription", "ShortDescription" }, new object[] { doc.LongDescription, doc.ShortDescription }
                        , "ID=" + doc.AccountDocumentID);
                    c--;
                }
            }
            catch (Exception ex)
            {
                Console.Write("Fatal error trying to fix payrol description \n" + ex.Message);
                Console.ReadLine();
            }
        }
    }
}
