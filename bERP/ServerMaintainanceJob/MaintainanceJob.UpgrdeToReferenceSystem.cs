using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using BIZNET.iERP.Server;
using BIZNET.iERP;


namespace INTAPS.ClientServer.RemottingServer
{
    public static partial class MaintainanceJob
    {
        static int getAIDForUpgrade()
        {
            return ApplicationServer.SecurityBDE.WriteAudit("System", -1, "System Upgrade", "", "", -1);
        }
        delegate void DocumentUpgradeDelegate<DocType>(iERPTransactionBDE bde, DocType doc, string fieldName) where DocType : AccountDocument;
        static int genRefType;
        static void upgradeDocumentType<DocType>(iERPTransactionBDE bde, DocumentUpgradeDelegate<DocType> upgrdeMethod,string fieldName) where DocType : AccountDocument
        {

            DocumentType dt = bde.Accounting.GetDocumentTypeByType(typeof(DocType));
            if (dt == null)
                throw new ServerUserMessage("Invalid document type " + typeof(DocType));
            Console.WriteLine("Upgrading " + dt.name);
            int[] docids = bde.Accounting.WriterHelper.GetColumnArray<int>(string.Format("Select ID from {0}.dbo.Document where documentTypeID={1}"
                , bde.Accounting.DBName, bde.Accounting.GetDocumentTypeByType(typeof(DocType)).id), 0);
            DocumentSerialType type = bde.Accounting.getDocumentSerialType("GEN");
            if (type == null)
                throw new ServerUserMessage("Configure GEN Document refernce type");
            genRefType = type.id;
            int c = docids.Length;
            Console.WriteLine(c + " documents found");
            Console.WriteLine();

            foreach (int docid in docids)
            {
                c--;
                Console.CursorTop--;
                Console.WriteLine(c + "                         ");
                bde.WriterHelper.BeginTransaction();
                DocType oldDoc = null;
                try
                {
                    AccountTransaction[] _oldTran = bde.Accounting.GetTransactionsOfDocument(docid);
                    oldDoc = bde.Accounting.GetAccountDocument(docid, true) as DocType;
                    if (oldDoc == null)
                        throw new ServerUserMessage("Coudlnt' load document ID:" + docid);

                    upgrdeMethod(bde, oldDoc,fieldName);
                    AccountTransaction[] _newTran = bde.Accounting.GetTransactionsOfDocument(docid);
                    
                    AccountTransaction[] oldTran = bde.Accounting.summerizeTransaction(_oldTran);
                    AccountTransaction[] newTran = bde.Accounting.summerizeTransaction(_newTran);
                    if (oldTran.Length != newTran.Length)
                        throw new ServerUserMessage("Old and new transaction mismatch");
                    for (int i = 0; i < oldTran.Length; i++)
                    {
                        bool found = false;
                        for (int j = 0; j < newTran.Length; j++)
                        {
                            if (oldTran[i].AccountID == newTran[j].AccountID && oldTran[i].ItemID == newTran[j].ItemID)
                            {
                                found = true;
                                if (!AccountBase.AmountEqual(oldTran[i].Amount, newTran[j].Amount))
                                    throw new ServerUserMessage("Old and new transaction mismatch");
                                break;
                            }
                        }
                        if (!found)
                            throw new ServerUserMessage("Old and new transaction mismatch");
                    }
                    bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bde.WriterHelper.RollBackTransaction();
                    Console.WriteLine(string.Format("Error processing document ID {0} PaperRef {1}\n  {2} ", docid
                        ,oldDoc==null?"[Invalid Doc ID]":oldDoc.PaperRef
                        ,ex.Message));
                    Console.WriteLine();
                }
            }
        }
        public static void upgradeINTAPSDB()
        {
            try
            {
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                Console.WriteLine("Declaration constraint checking is now disabled");
                INTAPS.ClientServer.ObjectFilters.GetFilterInstance<DeclaredDocumentFilter>().enabled=false;
                upgradeDocumentType<AdjustmentDocument>(bde, new DocumentUpgradeDelegate<AdjustmentDocument>(upgradeINTAPSDB), "paymentVoucher");
                upgradeDocumentType<Purchase2Document>(bde, new DocumentUpgradeDelegate<Purchase2Document>(upgradeINTAPSDB), "paymentVoucher");
                upgradeDocumentType<Sell2Document>(bde, new DocumentUpgradeDelegate<Sell2Document>(upgradeINTAPSDB), "paymentVoucher");
                upgradeDocumentType<UnclaimedSalaryDocument>(bde, new DocumentUpgradeDelegate<UnclaimedSalaryDocument>(upgradeINTAPSDB), "voucher");
                upgradeDocumentType<StaffPerDiemDocument>(bde, new DocumentUpgradeDelegate<StaffPerDiemDocument>(upgradeINTAPSDB), "voucher");
                upgradeDocumentType<BIZNET.iERP.BankWithdrawalDocument>(bde
                    , new DocumentUpgradeDelegate<BIZNET.iERP.BankWithdrawalDocument>(
                        upgradeINTAPSDBGen<BIZNET.iERP.BankWithdrawalDocument>), "voucher");

                upgradeDocumentType<BIZNET.iERP.LoanFromStaffDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.LoanFromStaffDocument>(
                        upgradeINTAPSDBGen<BIZNET.iERP.LoanFromStaffDocument>), "voucher");

                upgradeDocumentType<BIZNET.iERP.LoanFromStaffDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.LoanFromStaffDocument>(
                        upgradeINTAPSDBGen<BIZNET.iERP.LoanFromStaffDocument>), "voucher");

                upgradeDocumentType<BIZNET.iERP.CashierToCashierDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.CashierToCashierDocument>(
                        upgradeINTAPSDBGen<BIZNET.iERP.CashierToCashierDocument>), "voucher");

                upgradeDocumentType<BIZNET.iERP.ExpenseAdvanceDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.ExpenseAdvanceDocument>(
                        upgradeINTAPSDBGen<BIZNET.iERP.ExpenseAdvanceDocument>), "voucher");

                upgradeDocumentType<BIZNET.iERP.ExpenseAdvanceDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.ExpenseAdvanceDocument>(
                        upgradeINTAPSDBGen<BIZNET.iERP.ExpenseAdvanceDocument>), "voucher");

                upgradeDocumentType<BIZNET.iERP.ShortTermLoanDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.ShortTermLoanDocument>(
                        upgradeINTAPSDBGenNoPost<BIZNET.iERP.ShortTermLoanDocument>), "voucher");

                upgradeDocumentType<BIZNET.iERP.BankDepositDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.BankDepositDocument>(
                        upgradeINTAPSDBGen<BIZNET.iERP.BankDepositDocument>), "voucher");

                upgradeDocumentType<BIZNET.iERP.StaffLoanPaymentDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.StaffLoanPaymentDocument>(
                        upgradeINTAPSDBGen<BIZNET.iERP.StaffLoanPaymentDocument>), "voucher");

                upgradeDocumentType<BIZNET.iERP.IncomeTaxDeclarationDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.IncomeTaxDeclarationDocument>(
                        upgradeINTAPSDBGen<BIZNET.iERP.IncomeTaxDeclarationDocument>), "voucher");

                upgradeDocumentType<BIZNET.iERP.PensionTaxDeclarationDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.PensionTaxDeclarationDocument>(
                        upgradeINTAPSDBGen<BIZNET.iERP.PensionTaxDeclarationDocument>), "voucher");

                upgradeDocumentType<BIZNET.iERP.VATDeclarationDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.VATDeclarationDocument>(
                        upgradeINTAPSDBGen<BIZNET.iERP.VATDeclarationDocument>), "voucher");

                upgradeDocumentType<BIZNET.iERP.BondPaymentDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.BondPaymentDocument>(
                        upgradeINTAPSDBGenNoPost<BIZNET.iERP.BondPaymentDocument>), "voucher");

                upgradeDocumentType<BIZNET.iERP.WithholdingTaxDeclarationDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.WithholdingTaxDeclarationDocument>(
                        upgradeINTAPSDBGen<BIZNET.iERP.WithholdingTaxDeclarationDocument>), "voucher");

                upgradeDocumentType<BIZNET.iERP.CustomerPayableDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.CustomerPayableDocument>(
                        upgradeINTAPSDBConvertoDE), "voucher");

                upgradeDocumentType<BIZNET.iERP.BondReturnDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.BondReturnDocument>(
                        upgradeINTAPSDBGenNoPost<BIZNET.iERP.BondReturnDocument>), "voucher");


                upgradeDocumentType<BIZNET.iERP.SupplierReceivableDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.SupplierReceivableDocument>(
                        upgradeINTAPSDBConvertoDE), "voucher");

                upgradeDocumentType<BIZNET.iERP.ShareHoldersLoanDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.ShareHoldersLoanDocument>(
                        upgradeINTAPSDBGen<BIZNET.iERP.ShareHoldersLoanDocument>), "voucher");

                upgradeDocumentType<BIZNET.iERP.CustomerAdvanceReturnDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.CustomerAdvanceReturnDocument>(
                        upgradeINTAPSDBConvertoDE), "voucher");

                upgradeDocumentType<BIZNET.iERP.LongTermLoanDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.LongTermLoanDocument>(
                        upgradeINTAPSDBGenNoPost<BIZNET.iERP.LongTermLoanDocument>), "voucher");

                upgradeDocumentType<BIZNET.iERP.SupplierPayableDocument>(bde
                     , new DocumentUpgradeDelegate<BIZNET.iERP.SupplierPayableDocument>(
                        upgradeINTAPSDBConvertoDE), "voucher");

            }
            catch (Exception ex)
            {
                Console.WriteLine("Fatal error\n:"+ ex.Message);
                Console.ReadLine();
            }
        }
        static DocumentTypedReference getTypedReference(string paperRef)
        {
            if (string.IsNullOrEmpty(paperRef))
                return new DocumentTypedReference(genRefType, "Empty", true);
            paperRef = paperRef.Replace(' ', '_');
            return new DocumentTypedReference(genRefType, paperRef, true);
        }
        static void upgradeINTAPSDB(iERPTransactionBDE bde, Sell2Document doc, string fieldName)
        {
            if (doc.salesVoucher != null)
                return;


            if (doc.getAttachmentArray().Count > 0)
                throw new ServerUserMessage("Purchase document with attachments not supported");
            foreach (TransactionDocumentItem item in doc.items)
            {
                TransactionItems titme = bde.GetTransactionItems(item.code);
                if (titme.IsFixedAssetItem)
                    item.directExpense = true;
            }
            int AID = getAIDForUpgrade();
            INTAPS.ClientServer.ObjectFilters.GetFilterInstance<DeclaredDocumentFilter>().allowAction(AID);
            doc.salesVoucher = getTypedReference(doc.PaperRef);
            TransactionItems[] items = new TransactionItems[doc.items.Length];

            for (int i = 0; i < items.Length; i++)
            {
                items[i] = bde.GetTransactionItems(doc.items[i].code);
            }

            /*bool withHeld = false;
            bool tot = false;
            bool serviceTot = false;
            foreach (TaxImposed tt in doc.taxImposed)
            {
                if (tt.TaxType == TaxType.WithHoldingTax)
                {
                    withHeld = true;
                }
                else if (tt.TaxType == TaxType.TOT)
                {
                    tot = true;
                    serviceTot = AccountBase.AmountEqual(tt.rate, 0.1);
                }
            }
            TradeRelation supplier = bde.GetSupplier(doc.relationCode);
            bool prevWithheld = supplier.Withhold;
            bool withHoldOptionChanged;
            if (withHoldOptionChanged = (supplier.Withhold ^ withHeld))
            {
                supplier.Withhold = withHeld;
                bde.RegisterRelation(getAIDForUpgrade(), supplier);
            }
            bool totChanged = tot && doc.items.Length == 1 && (items[0].GoodOrService == GoodOrService.Service) ^ serviceTot;
            if (totChanged)
            {
                items[0].GoodOrService = serviceTot ? GoodOrService.Service : GoodOrService.Good;
                bde.RegisterTransactionItem(getAIDForUpgrade(), items[0], new int[0]);
            }*/
            bde.Accounting.PostGenericDocument(AID, doc);
            /*if (withHoldOptionChanged)
            {
                supplier.Withhold = !withHeld;
                bde.RegisterRelation(getAIDForUpgrade(), supplier);
            }
            if (totChanged)
            {
                items[0].GoodOrService = !serviceTot ? GoodOrService.Service : GoodOrService.Good;
                bde.RegisterTransactionItem(getAIDForUpgrade(), items[0], new int[0]);
            }*/

        }
        static void upgradeINTAPSDB(iERPTransactionBDE bde, Purchase2Document doc, string fieldName)
        {
            if (doc.paymentVoucher != null)
                return;
            

            if (doc.getAttachmentArray().Count > 0)
                throw new ServerUserMessage("Purchase document with attachments not supported");
            foreach (TransactionDocumentItem item in doc.items)
            {
                TransactionItems titme = bde.GetTransactionItems(item.code);
                if (titme.IsFixedAssetItem)
                    item.directExpense = true;
            }
            int AID = getAIDForUpgrade();
            INTAPS.ClientServer.ObjectFilters.GetFilterInstance<DeclaredDocumentFilter>().allowAction(AID);
            doc.paymentVoucher = getTypedReference(doc.PaperRef);
            TransactionItems[] items = new TransactionItems[doc.items.Length];
            
            for (int i = 0; i < items.Length; i++)
            {
                items[i] = bde.GetTransactionItems(doc.items[i].code);
            }

            bool withHeld=false;
            bool tot=false;
            bool serviceTot = false;
            foreach (TaxImposed tt in doc.taxImposed)
            {
                if (tt.TaxType == TaxType.WithHoldingTax)
                {
                    withHeld = true;
                }
                else if (tt.TaxType == TaxType.TOT)
                {
                    tot = true;
                    serviceTot = AccountBase.AmountEqual(tt.rate, 0.1);
                }
            }
            TradeRelation supplier = bde.GetSupplier(doc.relationCode);
            bool prevWithheld = supplier.Withhold;
            bool withHoldOptionChanged;
            if (withHoldOptionChanged=(supplier.Withhold ^ withHeld))
            {
                supplier.Withhold = withHeld;
                bde.RegisterRelation(getAIDForUpgrade(),supplier);
            }
            bool totChanged = tot && doc.items.Length == 1 && (items[0].GoodOrService == GoodOrService.Service)^serviceTot;
            if (totChanged)
            {
                items[0].GoodOrService = serviceTot ? GoodOrService.Service : GoodOrService.Good;
                bde.RegisterTransactionItem(getAIDForUpgrade(), items[0], new int[0]);
            }
            bde.Accounting.PostGenericDocument(AID, doc);
            if (withHoldOptionChanged)
            {
                supplier.Withhold = !withHeld;
                bde.RegisterRelation(getAIDForUpgrade(), supplier);
            }
            if (totChanged)
            {
                items[0].GoodOrService = !serviceTot ? GoodOrService.Service : GoodOrService.Good;
                bde.RegisterTransactionItem(getAIDForUpgrade(), items[0], new int[0]);
            }

        }
        static void upgradeINTAPSDB(iERPTransactionBDE bde, UnclaimedSalaryDocument doc, string fieldName)
        {
            if (doc.voucher != null)
                return;


            int AID = getAIDForUpgrade();
            INTAPS.ClientServer.ObjectFilters.GetFilterInstance<DeclaredDocumentFilter>().allowAction(AID);
            doc.voucher = getTypedReference(doc.PaperRef);
            bde.Accounting.PostGenericDocument(AID, doc);
        }
        static void upgradeINTAPSDB(iERPTransactionBDE bde, StaffPerDiemDocument doc, string fieldName)
        {
            if (doc.voucher != null)
                return;
            int AID = getAIDForUpgrade();
            if (doc.costCenterID < 1)
                doc.costCenterID = bde.SysPars.mainCostCenterID;
            doc.voucher = getTypedReference(doc.PaperRef);
            bde.Accounting.PostGenericDocument(AID, doc);
        }
        static void upgradeINTAPSDB(iERPTransactionBDE bde, BondPaymentDocument doc, string fieldName)
        {
            if (doc.voucher != null)
                return;


            int AID = getAIDForUpgrade();
            doc.voucher = getTypedReference(doc.PaperRef);
            
            bde.Accounting.PostGenericDocument(AID, doc);
        }
        static void upgradeINTAPSDBGen<DocType>(iERPTransactionBDE bde, DocType doc,string fieldName) where DocType:AccountDocument
        {
            FieldInfo fi = typeof(DocType).GetField(fieldName);
            if (fi == null)
                throw new ServerUserMessage("Field " + fieldName + " is not found in " + typeof(DocType));
            DocumentTypedReference v = fi.GetValue(doc) as DocumentTypedReference;
            if (v!= null)
                return;


            int AID = getAIDForUpgrade();
            INTAPS.ClientServer.ObjectFilters.GetFilterInstance<DeclaredDocumentFilter>().allowAction(AID);
            fi.SetValue(doc, getTypedReference(doc.PaperRef));
            bde.Accounting.PostGenericDocument(AID, doc);
        }
        static void upgradeINTAPSDBConvertoDE(iERPTransactionBDE bde, AccountDocument doc, string fieldName)
        {

            int AID = getAIDForUpgrade();
            AccountTransaction[] tran = bde.Accounting.GetTransactionsOfDocument(doc.AccountDocumentID);
            AdjustmentDocument adj = new AdjustmentDocument();
            adj.voucher = getTypedReference(doc.PaperRef);
            adj.adjustmentAccounts = new AdjustmentAccount[tran.Length];
            for (int i = 0; i < tran.Length; i++)
            {
                CostCenterAccount csa = bde.Accounting.GetCostCenterAccount(tran[i].AccountID);
                if (tran[i].ItemID != TransactionItem.DEFAULT_CURRENCY)
                    throw new ServerUserMessage("Only money transaction can be converted to Direct Entery");
                AdjustmentAccount a = new AdjustmentAccount();
                a.accountID = csa.accountID;
                a.costCenterID = csa.costCenterID;
                a.debitAmount = tran[i].Amount >= 0 ? tran[i].Amount : 0;
                a.creditAmount= tran[i].Amount <0 ? -tran[i].Amount : 0;
                adj.adjustmentAccounts[i] = a;
            }
            DocumentType type = bde.Accounting.GetDocumentTypeByID(doc.DocumentTypeID);
            adj.CoypFrom(doc);
            adj.ShortDescription = type.name + ": " + adj.ShortDescription;
            bde.Accounting.PostGenericDocument(AID, adj);
        }
        static void upgradeINTAPSDBGenNoPost<DocType>(iERPTransactionBDE bde, DocType doc, string fieldName) where DocType : AccountDocument
        {
            FieldInfo fi = typeof(DocType).GetField(fieldName);
            if (fi == null)
                throw new ServerUserMessage("Field " + fieldName + " is not found in " + typeof(DocType));
            DocumentTypedReference v = fi.GetValue(doc) as DocumentTypedReference;
            if (v != null)
                return;


            int AID = getAIDForUpgrade();
            INTAPS.ClientServer.ObjectFilters.GetFilterInstance<DeclaredDocumentFilter>().allowAction(AID);
            bde.Accounting.addDocumentSerials(AID, doc, getTypedReference(doc.PaperRef));
        }

        static void upgradeINTAPSDB(iERPTransactionBDE bde, AdjustmentDocument doc, string fieldName)
        {
            if (doc.voucher != null)
                return;
            int AID = getAIDForUpgrade();
            doc.voucher = getTypedReference(doc.PaperRef);
            bool upgraded = false;
            foreach (AdjustmentAccount ac in doc.adjustmentAccounts)
            {
                if (ac.costCenterID != -1)
                {
                    upgraded = true;
                }
                else if (ac.costCenterID == -1)
                {
                    if (upgraded)
                        throw new ServerUserMessage("Upgraded and unappgraded mixed");
                }
            }
            if (upgraded)
                return;
            foreach (AdjustmentAccount ac in doc.adjustmentAccounts)
            {
                CostCenterAccount cs = bde.Accounting.GetCostCenterAccount(ac.accountID);
                ac.accountID = cs.accountID;
                ac.costCenterID = cs.costCenterID;
            }
            bde.Accounting.PostGenericDocument(AID, doc);
        }
    }
}
