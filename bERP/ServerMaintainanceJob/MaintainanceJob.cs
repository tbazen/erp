
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using DevExpress.XtraRichEdit.Layout;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using BIZNET.iERP.Server;
using BIZNET.iERP;
using System.Threading;
using INTAPS.RDBMS;


namespace INTAPS.ClientServer.RemottingServer
{
    class MaintenanceJobMethod : Attribute
    {
        public string name;
        public string password=null;
        public MaintenanceJobMethod(string n)
        {
            this.name = n;
        }
        public MaintenanceJobMethod(string n,string pw)
        {
            this.name = n;
            this.password = pw;
        }
    }
    [SingleTableObject]
    public class CostCenterLeafAccount
    {
        [IDField]
        public int childCostCenterAccountID;
        [IDField]
        public int parentCostCenterAccountID;
        [DataField]
        public int accountID;
        [DataField]
        public int costCenterID;
    }
    public static partial class MaintainanceJob
    {
        public static void DoJob(string args)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            try
            {
                Console.WriteLine("Which job do want to run?");
                SortedList<string, MethodInfo> methods = new SortedList<string, MethodInfo>();
                Dictionary<string,MaintenanceJobMethod> methodData=new Dictionary<string, MaintenanceJobMethod>();
                foreach (MethodInfo m in typeof(MaintainanceJob).GetMethods())
                {
                    object[] atr = m.GetCustomAttributes(typeof(MaintenanceJobMethod), false);
                    if (atr == null || atr.Length == 0)
                        continue;
                    MaintenanceJobMethod jb = atr[0] as MaintenanceJobMethod;
                    methodData.Add(jb.name,jb);
                    methods.Add(jb.name, m);
                }
                int count = 1;

                foreach (KeyValuePair<string, MethodInfo> kv in methods)
                {
                    Console.WriteLine("{0}:\t{1}", count++, kv.Key);
                }
                Console.WriteLine("{0}:\t{1}", "Q", "Quit");
                do
                {
                    Console.Write("?: ");
                    string read = Console.ReadLine();
                    if (read.Equals("Q", StringComparison.CurrentCultureIgnoreCase))
                        return;
                    if (int.TryParse(read, out count) && count <= methods.Count && count > 0)
                    {
                        MaintenanceJobMethod data = methodData[methods.Keys[count - 1]];
                        if (!string.IsNullOrEmpty(data.password))
                        {
                            do
                            {

                                Console.Write("Enter password:");
                                
                                String typedPass = "";
                                while (true)
                                {
                                    ConsoleKeyInfo key = Console.ReadKey(true);
                                    if(key.Key==ConsoleKey.Enter)
                                        break;
                                    typedPass += key.KeyChar;
                                }
                                if (!typedPass.Equals(data.password))
                                    Console.WriteLine("Invalid password");
                                else
                                    break;
                            
                            } while (true);
                            Console.WriteLine();
                        }
                        methods.Values[count - 1].Invoke(null, new object[0]);
                        return;

                    }
                }
                while (true);
            }
            catch (Exception bex)
            {
                Console.WriteLine(bex.Message);
                Console.WriteLine();
            }
            finally
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Job Finished, press enter to terminate application");
                Console.ReadLine();
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }

        private static void deleteChildAcccounts(string p)
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            int n;
            Account[] childs = accounting.GetChildAcccount<Account>(accounting.GetAccount<Account>(p).id, 0, -1, out n);
            Console.WriteLine();
            foreach (Account ch in childs)
            {
                try
                {
                    Console.CursorTop--;
                    Console.WriteLine(n-- + "             ");
                    accounting.DeleteAccount<Account>(-1, ch.id, true);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                }
            }

        }
        public static List<CostCenterAccount> getChildCostCenterAccount(CostCenterAccount[] costCenterAccounts, CostCenterAccount parent)
        {
            List<CostCenterAccount> ret = new List<CostCenterAccount>();
            foreach (CostCenterAccount csa in costCenterAccounts)
            {
                if (csa.accountPID == parent.accountID || csa.costCenterPID == parent.costCenterID)
                {
                    ret.Add(csa);
                }
            }
            return ret;
        }
        static bool isParent<AccountType>(AccountingBDE bde, Dictionary<string, bool> cache, int child, int parent)
            where AccountType : AccountBase, new()
        {
            if (child == parent)
                return false;
            string key = child + "_" + parent;
            if (cache.ContainsKey(key))
                return cache[key];
            bool ret = bde.IsControlOf<AccountType>(parent, child);
            cache.Add(key, ret);
            return ret;
        }
        private static void rebuildCostCenterAccountLeaveEntries()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            accounting.WriterHelper.setReadDB(accounting.DBName);
            accounting.WriterHelper.BeginTransaction();
            try
            {
                Console.WriteLine("Fixing childcount fields");
                accounting.WriterHelper.ExecuteNonQuery(string.Format("Update {0}.dbo.CostCenterAccount set childCostCenterCount=(Select childCount from {0}.dbo.CostCenter where ID=CostCenterAccount.costCenterID)", accounting.DBName));
                accounting.WriterHelper.ExecuteNonQuery(string.Format("Update {0}.dbo.CostCenterAccount set childAccountCount=(Select childCount from {0}.dbo.Account where ID=CostCenterAccount.accountID)", accounting.DBName));
                Console.WriteLine("Clearing CostCenterLeafAccount table");
                accounting.WriterHelper.Delete(accounting.DBName, "CostCenterLeafAccount", "");
                CostCenterAccount[] costCenterAccounts = accounting.WriterHelper.GetSTRArrayByFilter<CostCenterAccount>(null);
                int count = costCenterAccounts.Length;

                Console.WriteLine();
                Dictionary<string, bool> csCashe = new Dictionary<string, bool>();
                Dictionary<string, bool> acCashe = new Dictionary<string, bool>();
                foreach (CostCenterAccount csa1 in costCenterAccounts)
                {
                    Console.CursorTop--;
                    Console.WriteLine(count + "                          ");
                    if (!csa1.isControlAccount)
                        continue;
                    foreach (CostCenterAccount csa2 in costCenterAccounts)
                    {
                        if (csa2.isControlAccount)
                            continue;

                        if (isParent<Account>(accounting, acCashe, csa1.accountID, csa2.accountID)
                            || isParent<CostCenter>(accounting, csCashe, csa1.costCenterID, csa2.costCenterID))
                        {
                            accounting.WriterHelper.Insert(accounting.DBName, "CostCenterLeafAccount", new string[] { 
                            "childCostCenterAccountID"
                            , "parentCostCenterAccountID"
                            , "accountID"
                            , "costCenterID" }
                            , new object[] { csa2.id, csa1.id, csa1.accountID, csa1.costCenterID });
                        }
                    }
                    count--;
                }
                accounting.WriterHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                accounting.WriterHelper.RollBackTransaction();
                Console.WriteLine("Rebuidling entires failed\n" + ex.Message);
            }
        }
        static int joinEverythingToMainCostCenter(AccountingBDE accounting, int PID, int c, int mcsa)
        {
            int N;
            foreach (Account ac in accounting.GetChildAcccount<Account>(PID, 0, -1, out N))
            {
                //c=joinEverythingToMainCostCenter(accounting, ac.id, c,mcsa);
                Console.CursorTop--;
                Console.WriteLine(c + "                                  ");
                try
                {
                    accounting.CreateCostCenterAccount(-1, mcsa, ac.id);
                    c--;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error creating account for account id" + ac + "\n" + ex.Message);
                }
            }
            return c;
        }
        public static void joinEverythingToMainCostCenter()
        {

            Console.WriteLine("Creating cost center accounts");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            lock (accounting.WriterHelper)
            {
                int c = (int)accounting.WriterHelper.ExecuteScalar("Select count(*) from " + accounting.DBName + ".dbo.Account");
                Console.WriteLine(c + " accounts found");
                Console.WriteLine();
                joinEverythingToMainCostCenter(accounting, -1, c, 1);
            }
        }
        public static void fixSuppliersAndCustomers()
        {
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            int N;
            TradeRelation[] tr = bde.searchTradeRelation(null, null, 0, -1, null, null, out N);
            Console.CursorTop--;
            Console.WriteLine(tr.Length + " relations found");
            int c = tr.Length;
            Console.WriteLine();
            foreach (TradeRelation r in tr)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                                               ");
                try
                {
                    bde.RegisterRelation(-1, r);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error processing " + r.Code);
                    Console.WriteLine(ex.Message);

                }
            }
        }
        [MaintenanceJobMethod("Clear Data: Items, Trade Relations, Employees, Projects")]
        public static void clearAccountDependencies()
        {
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            PayrollBDE payroll = ApplicationServer.GetBDE("Payroll") as PayrollBDE;
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;


            int N;
            int AID;
            BIZNET.iERP.TradeRelation[] custs = bde.searchTradeRelation(null, null, 0, -1, new object[] { }, new string[] { }, out N);

            int c = custs.Length;
            Console.WriteLine(c + " trade relations found");
            Console.WriteLine();
            foreach (BIZNET.iERP.TradeRelation cust in custs)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    bde.DeleteRelation(AID, cust.Code);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                c--;

            }

            BIZNET.iERP.TransactionItems[] items = bde.SearchTransactionItems(0, -1, new object[0], new string[0], out N);
            c = items.Length;
            Console.WriteLine(c + " items found");
            Console.WriteLine();
            foreach (BIZNET.iERP.TransactionItems item in items)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    bde.DeleteTransactionItem(AID, item.Code);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                c--;
            }


            BankAccountInfo[] bas = bde.GetAllBankAccounts();
            c = bas.Length;
            Console.WriteLine(c + " bank accounts found");
            Console.WriteLine();
            foreach (BIZNET.iERP.BankAccountInfo b in bas)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    bde.DeleteBankAccount(AID, b.mainCsAccount);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                c--;
            }

            CashAccount[] cas = bde.GetAllCashAccounts(false);
            c = cas.Length;
            Console.WriteLine(c + " cash accounts found");
            Console.WriteLine();
            foreach (BIZNET.iERP.CashAccount ca in cas)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    bde.DeleteCashAccount(AID, ca.code);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                c--;
            }
            Employee[] es = payroll.GetEmployees(new AppliesToEmployees(true));
            c = es.Length;
            Console.WriteLine(c + " employees found");
            Console.WriteLine();
            foreach (Employee e in es)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    payroll.DeleteEmployee(AID, e.id);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                c--;
            }

            int[] parentAccounts = new int[]
                        {
                            payroll.SysPars.staffLongTermLoanAccountID
                            ,payroll.SysPars.staffSalaryAdvanceAccountID
                            ,payroll.SysPars.staffExpenseAdvanceAccountID
                            ,payroll.SysPars.UnclaimedSalaryAccount
                            ,payroll.SysPars.staffIncomeTaxAccountID
                            ,payroll.SysPars.staffPensionPayableAccountID
                            ,payroll.SysPars.staffCostSharingPayableAccountID
                            ,payroll.SysPars.LoanFromStaffAccountID
                            ,payroll.SysPars.OwnersEquityAccount
                            ,payroll.SysPars.staffPerDiemCostAccountID
                            ,payroll.SysPars.staffPerDiemAccountID
                            ,payroll.SysPars.PermanentPayrollCostAccount
                            ,payroll.SysPars.TemporaryPayrollCostAccount
                            ,payroll.SysPars.PermanentPayrollExpenseAccount
                            ,payroll.SysPars.TemporaryPayrollExpenseAccount
                            ,payroll.SysPars.ShareHoldersLoanAccountID
                        };
            Console.WriteLine();
            foreach (int parAc in parentAccounts)
            {
                if (parAc == -1)
                    continue;
                foreach (Account child in accounting.GetChildAcccount<Account>(parAc, 0, -1, out N))
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                       ");
                    try
                    {
                        AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                        accounting.DeleteAccount<Account>(AID, child.id, true);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    c++;
                }
            }
            Project[] prjs = bde.GetAllProjects();
            c = prjs.Length;
            Console.WriteLine(c + " projects found");
            Console.WriteLine();
            foreach (BIZNET.iERP.Project p in prjs)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    foreach (int e in p.projectData.employees)
                    {
                        try
                        {
                            AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                            bde.RemoveEmployeeFromProject(AID, e, p.code);

                        }
                        catch { }
                    }
                    foreach (string storeCode in p.projectData.stores)
                    {
                        StoreInfo s = bde.GetStoreInfo(storeCode);
                        if (s != null)
                        {
                            try
                            {
                                AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                                bde.RemoveStoreFromProject(AID, s.costCenterID, storeCode, p.code);
                            }
                            catch { }
                        }
                    }
                    foreach (int m in p.projectData.machinaryAndVehicles)
                    {
                        try
                        {
                            AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                            bde.RemoveMachinaryAndVehicleFromProject(AID, m, p.code);
                        }
                        catch { }
                    }
                    foreach (int div in p.projectData.projectDivisions)
                    {
                        try
                        {
                            AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                            bde.RemoveDivisionFromProject(AID, div, p.code);
                        }
                        catch { }
                    }
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    bde.DeleteProject(AID, p.code);

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                }
                c--;
            }

        }
        private static void clearTransaction()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            int[] docs = accounting.WriterHelper.GetColumnArray<int>(
                string.Format("Select ID from {0}.dbo.Document", accounting.DBName), 0);
            int c = 1;
            foreach (int docID in docs)
            {
                Console.WriteLine((c++) + "/" + docs.Length);

                try
                {
                    accounting.DeleteAccountDocument(-1, docID, false);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error deleting " + docID + ".\n" + ex.Message);
                }
            }
        }

        private static void updateSalesAndPurchasePaidAmountValue(int AID)
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;

            foreach (int docID in accounting.WriterHelper.GetColumnArray<int>(string.Format("Select id from {0}.dbo.Document where documentTypeID in ({1})", accounting.DBName, "142,146"), 0))
            {
                AccountDocument doc = accounting.GetAccountDocument(docID, true);
                AccountTransaction[] tran = accounting.GetTransactionsOfDocument(docID);

                if (doc is Sell2Document)
                {
                    Sell2Document sell = doc as Sell2Document;
                    if (!Account.AmountEqual(sell.paidAmount, 0))
                        continue;
                    bool done = false;
                    foreach (AccountTransaction t in tran)
                    {
                        if (t.AccountID == sell.assetAccountID)
                        {
                            done = true;
                            sell.paidAmount = t.Amount;
                            sell.doWithheldVAT = true;
                            accounting.UpdateAccountDocumentData(AID, sell);
                            continue;
                        }
                    }
                    if (!done)
                        Console.WriteLine("Sells :" + sell.PaperRef + " has no payment");
                }
                else
                {
                    Purchase2Document purchase = doc as Purchase2Document;
                    if (!Account.AmountEqual(purchase.paidAmount, 0))
                        continue;

                    bool done = false;
                    foreach (AccountTransaction t in tran)
                    {
                        if (t.AccountID == purchase.assetAccountID)
                        {
                            done = true;
                            purchase.paidAmount = -t.Amount;
                            accounting.UpdateAccountDocumentData(AID, purchase);
                            continue;
                        }
                    }
                    if (!done)
                        Console.WriteLine("Purchase :" + purchase.PaperRef + " has no payment");
                }
            }
        }
        [MaintenanceJobMethod("Fix child count fields for CostCenterAccount table")]
        public static void fixCostCenterAccountDataError()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            string sql;

            Console.WriteLine("Synching child count fields for CostCenterAccount");
            sql = @"Update {0}.dbo.CostCenterAccount set childAccountCount=(Select childCount from {0}.dbo.Account where id=CostCenterAccount.accountID)
                        ,childCostCenterCount=(Select childCount from {0}.dbo.CostCenter where id=CostCenterAccount.costCenterID)";
            accounting.WriterHelper.ExecuteNonQuery(string.Format(sql, accounting.DBName));

            accounting.WriterHelper.setReadDB(accounting.DBName);
            accounting.WriterHelper.BeginTransaction();
            try
            {
                sql = "Delete from {0}.dbo.CostCenterLeafAccount";
                accounting.WriterHelper.ExecuteNonQuery(string.Format(sql, accounting.DBName));
                CostCenterAccount[] allAccounts = accounting.WriterHelper.GetSTRArrayByFilter<CostCenterAccount>("childCostCenterCount=0 and childAccountCount=0");
                foreach (CostCenterAccount a in allAccounts)
                {
                    int[] ac = accounting.ExpandParents<Account>(accounting.WriterHelper, a.accountID);
                    int[] cs = accounting.ExpandParents<CostCenter>(accounting.WriterHelper, a.costCenterID);
                    if (ac.Length == 1 && cs.Length == 1)
                        throw new Exception();
                    foreach (int acID in ac)
                    {
                        foreach (int csID in cs)
                        {
                            if (acID == a.accountID && csID == a.costCenterID)
                                continue;
                            accounting.WriterHelper.Insert(accounting.DBName, "CostCenterLeafAccount"
                                , new string[] { "childCostCenterAccountID", "parentCostCenterAccountID", "accountID", "costCenterID" }
                                , new object[] { a.id, accounting.GetCostCenterAccount(csID, acID).id, a.accountID, a.costCenterID });
                        }
                    }
                }
                accounting.WriterHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                accounting.WriterHelper.RollBackTransaction();
                Console.WriteLine("Failed to rebuild cost center leaf account table." + ex.Message);
            }
        }
        [Flags]
        enum ClearingOption
        {
            Everything = ClearItems | ClearEverythingElse,
            ClearItems = 1,
            ClearEverythingElse = 2
        }
        private static void clearEverything(ClearingOption options)
        {
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            PayrollBDE payroll = ApplicationServer.GetBDE("Payroll") as PayrollBDE;
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;

            List<int> docIDs = new List<int>();
            docIDs.AddRange(accounting.WriterHelper.GetColumnArray<int>(string.Format("Select distinct DocumentID from {0}.dbo.DocumentSerial where primaryDocument=0", accounting.DBName), 0));
            docIDs.AddRange(accounting.WriterHelper.GetColumnArray<int>(string.Format("Select distinct DocumentID from {0}.dbo.DocumentSerial where primaryDocument=1", accounting.DBName), 0));
            docIDs.AddRange(accounting.WriterHelper.GetColumnArray<int>(string.Format("Select ID from {0}.dbo.Document where ID not in (Select DocumentID from {0}.dbo.DocumentSerial)", accounting.DBName), 0));

            int c = docIDs.Count;
            Console.WriteLine(c + " documents found");
            Console.WriteLine();
            int AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
            foreach (int docID in docIDs)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    accounting.DeleteGenericDocument(AID, docID);
                }
                catch (Exception ex)
                {
                    AccountDocument doc = accounting.GetAccountDocument(docID, false);
                    if (doc != null)
                    {
                        DocumentType t = accounting.GetDocumentTypeByID(doc.DocumentTypeID);
                        Console.WriteLine(string.Format("Error trying to delete document ID:{0} and Type:{1}", doc.AccountDocumentID, t == null ? t.id.ToString() : t.name));
                        Console.WriteLine(ex.Message);
                        Console.WriteLine();
                    }
                }
                c--;
            }
            int N;

            BIZNET.iERP.TradeRelation[] custs = bde.searchTradeRelation(null,null, 0, -1, new object[] { }, new string[] { }, out N);
            c = custs.Length;
            Console.WriteLine(c + " trade relations found");
            Console.WriteLine();
            foreach (BIZNET.iERP.TradeRelation cust in custs)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    bde.DeleteRelation(AID, cust.Code);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                c--;

            }
            if ((options & ClearingOption.ClearItems) == ClearingOption.ClearItems)
            {
                clearTransactionItems(bde);
            }
            Project[] prjs = bde.GetAllProjects();
            c = prjs.Length;
            Console.WriteLine(c + " projects found");
            Console.WriteLine();
            foreach (BIZNET.iERP.Project p in prjs)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    foreach (int e in p.projectData.employees)
                    {
                        try
                        {
                            AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                            bde.RemoveEmployeeFromProject(AID, e, p.code);

                        }
                        catch { }
                    }
                    foreach (string storeCode in p.projectData.stores)
                    {
                        StoreInfo s = bde.GetStoreInfo(storeCode);
                        if (s != null)
                        {
                            try
                            {
                                AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                                bde.RemoveStoreFromProject(AID, s.costCenterID, storeCode, p.code);
                            }
                            catch { }
                        }
                    }
                    foreach (int m in p.projectData.machinaryAndVehicles)
                    {
                        try
                        {
                            AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                            bde.RemoveMachinaryAndVehicleFromProject(AID, m, p.code);
                        }
                        catch { }
                    }
                    foreach (int div in p.projectData.projectDivisions)
                    {
                        try
                        {
                            AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                            bde.RemoveDivisionFromProject(AID, div, p.code);
                        }
                        catch { }
                    }
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    bde.DeleteProject(AID, p.code);

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                }
                c--;
            }

            BankAccountInfo[] bas = bde.GetAllBankAccounts();
            c = bas.Length;
            Console.WriteLine(c + " bank accounts found");
            Console.WriteLine();
            foreach (BIZNET.iERP.BankAccountInfo b in bas)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    bde.DeleteBankAccount(AID, b.mainCsAccount);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                c--;
            }

            CashAccount[] cas = bde.GetAllCashAccounts(false);
            c = cas.Length;
            Console.WriteLine(c + " cash accounts found");
            Console.WriteLine();
            foreach (BIZNET.iERP.CashAccount ca in cas)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    bde.DeleteCashAccount(AID, ca.code);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                c--;
            }
            Employee[] es = payroll.GetEmployees(new AppliesToEmployees(true));
            c = es.Length;
            Console.WriteLine(c + " employees found");
            Console.WriteLine();
            foreach (Employee e in es)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    payroll.DeleteEmployee(AID, e.id);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                c--;
            }

            int[] parentAccounts = new int[]
                        {
                            payroll.SysPars.staffLongTermLoanAccountID
                            ,payroll.SysPars.staffSalaryAdvanceAccountID
                            ,payroll.SysPars.staffExpenseAdvanceAccountID
                            ,payroll.SysPars.UnclaimedSalaryAccount
                            ,payroll.SysPars.staffIncomeTaxAccountID
                            ,payroll.SysPars.staffPensionPayableAccountID
                            ,payroll.SysPars.staffCostSharingPayableAccountID
                            ,payroll.SysPars.LoanFromStaffAccountID
                            ,payroll.SysPars.OwnersEquityAccount
                            ,payroll.SysPars.staffPerDiemCostAccountID
                            ,payroll.SysPars.staffPerDiemAccountID
                            ,payroll.SysPars.PermanentPayrollCostAccount
                            ,payroll.SysPars.TemporaryPayrollCostAccount
                            ,payroll.SysPars.PermanentPayrollExpenseAccount
                            ,payroll.SysPars.TemporaryPayrollExpenseAccount
                            ,payroll.SysPars.ShareHoldersLoanAccountID
                        };
            Console.WriteLine();
            foreach (int parAc in parentAccounts)
            {
                if (parAc == -1)
                    continue;
                foreach (Account child in accounting.GetChildAcccount<Account>(parAc, 0, -1, out N))
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                       ");
                    try
                    {
                        AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                        accounting.DeleteAccount<Account>(AID, child.id, true);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    c++;
                }
            }
        }

        

        static System.IO.StreamWriter logStream = null;
        static void OpenLogStream()
        {
            if (logStream == null)
                logStream = System.IO.File.CreateText("ServerMaintenanceJob.txt");

        }
        static void writeLog(string line)
        {
            Console.WriteLine(line);
            if (logStream != null)
                OpenLogStream();
            logStream.WriteLine(line);
        }
        static void closeLogStream()
        {
            if (logStream != null)
                logStream.Close();
        }
        static void repostIntegrityTest()
        {
            OpenLogStream();
            try
            {
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                bde.WriterHelper.ExecuteNonQuery(string.Format("delete from {0}.dbo.Document_Deleted", bde.Accounting.DBName));
                int[] docIDs = bde.WriterHelper.GetColumnArray<int>(string.Format("Select ID from {0}.dbo.Document where DocumentTypeID not in (1,107,106,152,120)order by DocumentDate", bde.Accounting.DBName), 0);
                Console.WriteLine(docIDs.Length + " found");
                Console.WriteLine();
                for (int i = 0; i < docIDs.Length; i++)
                {
                    Console.CursorTop--;
                    Console.WriteLine((docIDs.Length - i) + "                                ");
                    AccountDocument doc;
                    AccountTransaction[] oldTransaction, newTransaction;
                    try
                    {
                        doc = bde.Accounting.GetAccountDocument(docIDs[i], true);
                        oldTransaction = bde.Accounting.GetTransactionsOfDocument(docIDs[i]);
                        try
                        {
                            int oldDocID = doc.AccountDocumentID;
                            int newDocID = bde.Accounting.PostGenericDocument(-1, doc);
                            newTransaction = bde.Accounting.GetTransactionsOfDocument(newDocID);
                            if (oldDocID != newDocID)
                            {
                                writeLog(string.Format("Old new document ID mismatch new docid:{0} documenType:{1}", newDocID, bde.Accounting.GetDocumentTypeByID(doc.DocumentTypeID).name));
                                Console.WriteLine();
                            }
                            foreach (AccountTransaction oldt in oldTransaction)
                            {

                                double amount = 0;
                                foreach (AccountTransaction oldtt in oldTransaction)
                                {
                                    if (oldt.AccountID == oldtt.AccountID)
                                    {
                                        amount += oldtt.Amount;
                                    }
                                }
                                foreach (AccountTransaction newT in newTransaction)
                                {
                                    if (oldt.AccountID == newT.AccountID)
                                    {
                                        amount -= newT.Amount;
                                    }
                                }
                                if (!Account.AmountEqual(amount, 0))
                                {
                                    writeLog(string.Format("Old new transaction set mismatch new docid:{0} documenType:{1}", newDocID, bde.Accounting.GetDocumentTypeByID(doc.DocumentTypeID).name));
                                    Console.WriteLine();
                                    break;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            writeLog("Error reposting document ID:" + docIDs[i]);
                            writeLog(ex.Message);
                            Console.WriteLine();
                        }
                    }
                    catch (Exception ex)
                    {
                        writeLog("Error loading document ID:" + docIDs[i]);
                        writeLog(ex.Message);
                        Console.WriteLine();
                    }


                }
            }
            finally
            {
                closeLogStream();
            }
        }
        //private static void CreateCostCenterLeafAccounts()
        //{
        //    AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;

        //    string sql = "Select id from {0}.dbo.CostCenterAccount where [childAccountCount]=0 and [childCostCenterCount]=0";
        //    int[] csids = accounting.WriterHelper.GetColumnArray<int>(string.Format(sql,accounting.DBName),0);
        //    try
        //    {
        //        int c = csids.Length;
        //        Console.WriteLine();
        //        foreach (int id in csids)
        //        {
        //            Console.CursorTop--;
        //            Console.WriteLine(id + "                    ");
        //            accounting.rebuildLeaveEntries(id);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //        Console.ReadLine();
        //    }
        //}
        static void FixOrderN()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;

            try
            {
                DataTable dt = accounting.WriterHelper.GetDataTable(@"SELECT * 
  FROM [Accounting_2006].[dbo].[AccountTransaction]
  where AccountID=8862
   order by ExecDate");
                int i = 1;
                accounting.WriterHelper.BeginTransaction();
                foreach (DataRow row in dt.Rows)
                {
                    string sql = "Update [Accounting_2006].[dbo].[AccountTransaction] Set OrderN=" + i + " where ID=" + row["ID"];
                    accounting.WriterHelper.ExecuteNonQuery(sql);
                    Console.WriteLine("Transaction ID:" + row["ID"] + " OrderN:" + i + " fixed!");
                    i++;
                }
                accounting.WriterHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                accounting.WriterHelper.RollBackTransaction();
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
        }

        private static void FixTransactionSquence(int accountID, int itemID)
        {
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            AccountingBDE finance = bde.Accounting;
            FixTransactionSquence(finance, accountID, itemID);
        }
       

        private static void FixTransactionSquence(AccountingBDE finance, int accountID, int itemID)
        {
            INTAPS.RDBMS.SQLHelper helper;
            Monitor.Enter(helper = finance.WriterHelper);
            try
            {
                string str2;
                finance.WriterHelper.BeginTransaction();
                string sql = string.Format("Select TransactionType,TotalDbBefore,TotalCrBefore,Amount,ID,tranTicks from {2}.dbo.AccountTransaction where AccountID={0} and ItemID={1} order by tranTicks", accountID, itemID, finance.DBName);
                DataTable dataTable = finance.WriterHelper.GetDataTable(sql);
                double newDebit = 0.0;
                double newCredit = 0.0;
                int orderN = 1;
                long now = DateTime.Now.Ticks;
                foreach (DataRow row in dataTable.Rows)
                {
                    double amount = (double)row[3];
                    int tranID = (int)row[4];
                    /*if (AccountBase.AmountEqual(amount, 0))
                    {
                        finance.WriterHelper.ExecuteNonQuery(string.Format("Delete from {0}.dbo.AccountTransaction where ID={1}", finance.DBName, tranID));
                        continue;
                    }*/
                    TransactionOfBatch tran = new TransactionOfBatch(accountID, amount, "");
                    tran.ItemID = itemID;
                    tran.ID = tranID;
                    tran.TransactionType = (INTAPS.Accounting.TransactionType)row[0];
                    tran.TotalCrBefore = newCredit;
                    tran.TotalDbBefore = newDebit;
                    finance.WriterHelper.ExecuteNonQuery(string.Format("Update {0}.dbo.AccountTransaction set TotalDbBefore={1},TotalCrBefore={2},OrderN={3} where ID={4}", finance.DBName, tran.TotalDbBefore, tran.TotalCrBefore, orderN, tran.ID));
                    newCredit = tran.NewCredit;
                    newDebit = tran.NewDebit;
                    now = (long)row[5];
                    orderN++;
                }
                if (orderN == 1)
                {
                    finance.WriterHelper.ExecuteNonQuery(string.Concat(new object[] { "DELETE FROM [Accounting_2006].[dbo].[AccountBalance] WHERE accountID=", accountID, " and ItemID=", itemID }));
                }
                else
                {
                    AccountBalance accountBalance = finance.GetEndBalance(accountID, itemID);
                    if (accountBalance.LastOrderN == 0)
                    {
                        accountBalance.LastOrderN = orderN - 1;
                        accountBalance.tranTicks = now;
                        accountBalance.TotalDebit = newDebit;
                        accountBalance.TotalCredit = newCredit;
                        finance.WriterHelper.InsertSingleTableRecord<AccountBalance>(finance.DBName, accountBalance);
                    }
                    else
                    {
                        accountBalance.LastOrderN = orderN - 1;
                        accountBalance.tranTicks = now;
                        accountBalance.TotalDebit = newDebit;
                        accountBalance.TotalCredit = newCredit;
                        finance.WriterHelper.UpdateSingleTableRecord<AccountBalance>(finance.DBName, accountBalance);
                        //finance.WriterHelper.ExecuteNonQuery(string.Concat(new object[] { "UPDATE [Accounting_2006].[dbo].[AccountBalance] SET [LastTransactionDate] ='", now.ToLongTimeString(), "',[LastOrderN] = ", orderN - 1, ",[TotalCredit] = ", newCredit, "\r\n                                    ,[TotalDebit] = ", newDebit, " WHERE accountID=", accountID, " and ItemID=", itemID }));
                    }
                }
                if (finance.VerifyTransactionConsistency(finance.WriterHelper, accountID, itemID, out str2) != -1)
                {
                    throw new Exception(str2);
                }
                finance.WriterHelper.CommitTransaction();
            }
            catch (Exception exception)
            {
                finance.WriterHelper.RollBackTransaction();
                Console.WriteLine(string.Concat(new object[] { "Failed to fix ", accountID, "\n", exception.Message }));
            }
            finally
            {
                Monitor.Exit(helper);
            }
        }
        [MaintenanceJobMethod("Accounting: Rebuild Leaves Tables")]
        public static void accountingRebuildLeaves()
        {
            Console.WriteLine("Rebuild leaves table?");
            if (!Console.ReadLine().Equals("Y"))
                return;
            iERPTransactionBDE bdeBERP = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            AccountingBDE accounting = bdeBERP.Accounting;
            accounting.WriterHelper.BeginTransaction();
            try
            {
                accounting.rebuildCostCenterLeafAccountTable(-1);
                accounting.WriterHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                accounting.WriterHelper.RollBackTransaction();
                Console.WriteLine(ex.Message);
            }
        }

        [MaintenanceJobMethod("Accounting: Check basic account data structure integritiy")]
        public static void checkBasicAccountingStructureIntegirtyCheck()
        {
            string sqlFix3 = @"Update [Accounting_2006].[dbo].[CostCenterAccount]
set [childAccountCount]=(Select childCount from [Accounting_2006].[dbo].Account
where id=accountID),accountPID=(Select PID from [Accounting_2006].[dbo].Account
where id=accountID)
where [childAccountCount]<>(Select childCount from [Accounting_2006].[dbo].Account
where id=accountID) or accountPID<>(Select PID from [Accounting_2006].[dbo].Account
where id=accountID)";
            string sqlFix4 = @"Update [Accounting_2006].[dbo].[CostCenterAccount]
set [childCostCenterCount]=(Select childCount from [Accounting_2006].[dbo].CostCenter
where id=[costCenterID]), costCenterPID=(Select PID from [Accounting_2006].[dbo].CostCenter
where id=[costCenterID])
where [childCostCenterCount]<>(Select childCount from [Accounting_2006].[dbo].CostCenter
where id=[costCenterID]) or costCenterPID<>(Select PID from [Accounting_2006].[dbo].CostCenter
where id=[costCenterID])";

            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            accounting.WriterHelper.setReadDB(accounting.DBName);
            int c;
            try
            {

                Account[] ac = accounting.WriterHelper.GetSTRArrayByFilter<Account>(null);
                Console.WriteLine("Checking child count list for account table");
                Console.WriteLine();
                c = ac.Length;
                foreach (Account a in ac)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c-- + "                       ");
                    int childCount = (int)accounting.WriterHelper.ExecuteScalar("Select count(*) from Accounting_2006.dbo.Account where PID=" + a.id);
                    if (a.childCount != childCount)
                    {
                        Console.WriteLine("Child count field error for account " + a.Code);
                        Console.WriteLine();
                    }
                }
                CostCenter[] css = accounting.WriterHelper.GetSTRArrayByFilter<CostCenter>(null);
                Console.WriteLine("Checking child count list for CostCenter table");
                Console.WriteLine();
                c = css.Length;
                foreach (CostCenter cs in css)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c-- + "                       ");
                    int childCount = (int)accounting.WriterHelper.ExecuteScalar("Select count(*) from Accounting_2006.dbo.CostCenter where PID=" + cs.id);
                    if (cs.childCount != childCount)
                    {
                        Console.WriteLine("Child count field error for CostCenter " + cs.Code);
                        Console.WriteLine();
                    }
                }
                Console.WriteLine("Checking the cost center leaf account table");
                Console.WriteLine("Loading data..");

                List<CostCenterAccount> csas = new List<CostCenterAccount>(accounting.WriterHelper.GetSTRArrayByFilter<CostCenterAccount>(null));
                SortedDictionary<int, SortedDictionary<int, CostCenterLeafAccount>> leafAccountMap = new SortedDictionary<int, SortedDictionary<int, CostCenterLeafAccount>>();
                foreach (CostCenterLeafAccount la in accounting.WriterHelper.GetSTRArrayByFilter<CostCenterLeafAccount>(null))
                {
                    if (leafAccountMap.ContainsKey(la.childCostCenterAccountID))
                        leafAccountMap[la.childCostCenterAccountID].Add(la.parentCostCenterAccountID, la);
                    else
                    {
                        SortedDictionary<int, CostCenterLeafAccount> parent = new SortedDictionary<int, CostCenterLeafAccount>();
                        parent.Add(la.parentCostCenterAccountID, la);
                        leafAccountMap.Add(la.childCostCenterAccountID, parent);
                    }
                }
                Console.WriteLine("Checking..");
                Console.WriteLine();
                c = csas.Count;
                bool doSqlFix3 = false;
                bool doSqlFix4 = false;
                foreach (CostCenterAccount csa in csas)
                {

                    if (c % 1000 == 0)
                    {
                        Console.CursorTop--;
                        Console.WriteLine(c + "                       ");
                    }
                    c--;
                    foreach (Account a in ac)
                    {
                        if (a.id == csa.accountID)
                        {
                            if (a.childCount != csa.childAccountCount)
                            {
                                doSqlFix3 = true;
                                Console.WriteLine("CostCenterAccountID {0} got wrong childAccountCount field", csa.id);
                                Console.WriteLine();
                            }

                            if (a.PID != csa.accountPID)
                            {
                                doSqlFix3 = true;
                                Console.WriteLine("CostCenterAccountID {0} got wrong accountPID field", csa.id);
                                Console.WriteLine();
                            }
                            break;
                        }
                    }

                    foreach (CostCenter cs in css)
                    {
                        if (cs.id == csa.costCenterID)
                        {
                            if (cs.childCount != csa.childCostCenterCount)
                            {
                                doSqlFix4 = true;
                                Console.WriteLine("CostCenterAccountID {0} got wrong childCostCenterCount field", csa.id);
                                Console.WriteLine();
                            }
                            if (cs.PID != csa.costCenterPID)
                            {
                                doSqlFix4 = true;
                                Console.WriteLine("CostCenterAccountID {0} got wrong costCenterPID field", csa.id);
                                Console.WriteLine();
                            }
                            break;
                        }
                    }
                    if (!csa.isControlAccount)
                    {
                        if (!leafAccountMap.ContainsKey(csa.id))
                        {
                            Console.WriteLine("CostCenterAccount {0} is leaf account but it is not included in CostCenterLeafAccount table", csa.id);
                            Console.WriteLine();
                        }

                        List<int> parentAccounts = new List<int>(accounting.ExpandParents<Account>(accounting.WriterHelper, csa.accountPID));
                        List<int> parentCostCenters = new List<int>(accounting.ExpandParents<CostCenter>(accounting.WriterHelper, csa.costCenterPID));
                        parentAccounts.Insert(0, csa.accountID);
                        parentCostCenters.Insert(0, csa.costCenterID);

                        SortedDictionary<int, CostCenterLeafAccount> parents = leafAccountMap[csa.id];
                        foreach (int parentAccount in parentAccounts)
                        {
                            foreach (int parentCostCenter in parentCostCenters)
                            {
                                CostCenterAccount costCenterAccount = accounting.GetCostCenterAccount(parentCostCenter, parentAccount);
                                if (costCenterAccount == null || costCenterAccount.id == csa.id)
                                    continue;
                                if (!parents.ContainsKey(costCenterAccount.id))
                                {
                                    Console.WriteLine("CostCenterAccount {0} is parent of {1} but CostCenterLeafAccount table doesn't indicate that", costCenterAccount.id, csa.id);
                                    Console.WriteLine();
                                }
                                parents.Remove(costCenterAccount.id);
                            }
                        }

                        if (parents.Count > 0)
                        {
                            foreach (KeyValuePair<int, CostCenterLeafAccount> kv in parents)
                            {
                                Console.WriteLine("Invalid CostCenterLeafAccount entry child:{0} parent:{1}", csa.id, kv.Key);
                            }
                        }
                        leafAccountMap.Remove(csa.id);
                    }
                }
                foreach (KeyValuePair<int, SortedDictionary<int, CostCenterLeafAccount>> kv in leafAccountMap)
                {
                    Console.WriteLine("Invalid CostCenterLeafAccount entry child:{0} parent Count:{1}", kv.Key, kv.Value.Count);
                }
                if (doSqlFix3 || doSqlFix4)
                {
                    Console.WriteLine("Run fixes?");
                    if (Console.ReadLine() == "Y")
                    {
                        accounting.WriterHelper.BeginTransaction();
                        try
                        {
                            if (doSqlFix3)
                                accounting.WriterHelper.ExecuteNonQuery(sqlFix3);
                            if (doSqlFix4)
                                accounting.WriterHelper.ExecuteNonQuery(sqlFix4);
                            accounting.WriterHelper.CommitTransaction();
                        }
                        catch (Exception ex)
                        {
                            accounting.WriterHelper.RollBackTransaction();
                            ApplicationServer.EventLoger.LogException("Fix routines fialed", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [MaintenanceJobMethod("Post all issue transactions again to recalculate weighted average")]
        public static void reissueForWeightedAverageCalculation()
        {
            iERPTransactionBDE bdeBERP = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            int N;
            AccountDocument[] issues = bdeBERP.Accounting.GetAccountDocuments(null, bdeBERP.Accounting.GetDocumentTypeByType(typeof(StoreIssueDocument)).id, false, DateTime.Now, DateTime.Now, 0, -1, out N);
            Console.WriteLine(N + " store issue documents found. Please enter to continue.");
            Console.ReadLine();
            int c = issues.Length;
            Dictionary<AccountDocument, Exception> errors = new Dictionary<AccountDocument, Exception>();
            foreach (AccountDocument d in issues)
            {
                try
                {
                    Console.CursorTop--;
                    Console.WriteLine((c--) + "                                    ");
                    StoreIssueDocument si = bdeBERP.Accounting.GetAccountDocument(d.AccountDocumentID, true) as StoreIssueDocument;
                    bdeBERP.WriterHelper.BeginTransaction();
                    try
                    {
                        bdeBERP.Accounting.PostGenericDocument(1, si);
                        bdeBERP.WriterHelper.CommitTransaction();
                    }
                    catch
                    {
                        bdeBERP.WriterHelper.RollBackTransaction();
                        throw;
                    }
                }
                catch (Exception ex)
                {
                    errors.Add(d, ex);
                    Console.WriteLine(string.Format("Error prossing document ID:{0} ref:{1}\n{2}", d.AccountDocumentID, d.PaperRef, ex.Message));
                    Console.WriteLine();

                }
            }
            if (errors.Count > 0)
            {
                foreach (KeyValuePair<AccountDocument, Exception> d in errors)
                {
                    Console.WriteLine(string.Format("Error prossing document ID:{0} ref:{1}\n{2}", d.Key.AccountDocumentID, d.Key.PaperRef, d.Value.Message));
                }
                Console.ReadLine();
            }
        }
        [MaintenanceJobMethod("Close Year")]
        public static void closeYear()
        {
            Console.Write("Enter year:");
            int year;
            while (!int.TryParse(Console.ReadLine(), out year) || year < 2000 || year > 2010)
            {
                Console.Write("Invalid year. Enter again:");
            }

            try
            {
                iERPTransactionBDE bdeBERP = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                Account capitalAccount;
                Console.WriteLine("Capital accounts");
                foreach (Account ac in bdeBERP.Accounting.GetLeafAccounts<Account>(bdeBERP.Accounting.GetAccountID<Account>("3000")))
                {
                    Console.WriteLine(ac.Code + "\t" + ac.Name);
                }
                Console.Write("Enter capital account code:");
                while ((capitalAccount = bdeBERP.Accounting.GetAccount<Account>(Console.ReadLine())) == null || !capitalAccount.Code.StartsWith("3"))
                {
                    Console.Write("Invalid capital account. Enter again:");
                }
                AdjustmentDocument adj = new AdjustmentDocument();
                adj.DocumentDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(30, 10, year)).GridDate;
                adj.DocumentDate = new DateTime(adj.DocumentDate.Year, adj.DocumentDate.Month, adj.DocumentDate.Day, 23, 50, 0);
                adj.voucher = new DocumentTypedReference(bdeBERP.Accounting.getDocumentSerialType("JV").id, "JV-Closing-" + year, true);
                List<AdjustmentAccount> incomeAccounts = new List<AdjustmentAccount>();
                double netIncome = 0;
                Console.WriteLine("Collecting balances..");
                List<Account> inactiveAccounts = new List<Account>();
                double totalCr=0, totalDb=0;
                foreach (string parent in new string[] { "4000", "5000", "6000", "ITEM-4000", "ITEM-5000", "ITEM-6000" })
                {
                    Account parentAccount = bdeBERP.Accounting.GetAccount<Account>(parent);
                    if (parentAccount == null)
                        continue;
                    foreach (Account la in bdeBERP.Accounting.GetLeafAccounts<Account>(parentAccount.id))
                    {
                        bool hasTransaction = false;
                        foreach (CostCenterAccount csa in bdeBERP.Accounting.GetCostCenterAccountsOf<Account>(la.id))
                        {
                            if (csa.isControlAccount)
                                continue;
                            double balance = bdeBERP.Accounting.GetNetBalanceAsOf(csa.id, 0, adj.DocumentDate);
                            if (AccountBase.AmountEqual(balance, 0))
                                continue;
                            AdjustmentAccount adjac = new AdjustmentAccount();
                            adjac.accountCode = la.Code;
                            adjac.accountID = csa.accountID;
                            adjac.costCenterID = csa.costCenterID;

                            if (csa.creditAccount)
                            {
                                if (balance > 0)
                                {
                                    adjac.creditAmount = 0;
                                    adjac.debitAmount = balance;
                                    totalDb += balance;
                                }
                                else
                                {
                                    adjac.creditAmount = -balance;
                                    adjac.debitAmount = 0;
                                    totalDb += balance;
                                }
                                netIncome += balance;
                            }
                            else
                            {
                                if (balance > 0)
                                {
                                    adjac.creditAmount = balance;
                                    adjac.debitAmount = 0;
                                    totalDb -= balance;
                                }
                                else
                                {
                                    adjac.creditAmount = 0;
                                    adjac.debitAmount = -balance;
                                    totalDb -= balance;
                                }
                                netIncome -= balance;
                            }
                            adjac.description = "Closing transaction";
                            incomeAccounts.Add(adjac);
                            hasTransaction = true;
                        }
                        if (hasTransaction && la.Status!=AccountStatus.Activated)
                        {
                            inactiveAccounts.Add(la);
                        }
                    }
                }
                
                if (!AccountBase.AmountEqual(netIncome, 0))
                {
                    AdjustmentAccount adjac = new AdjustmentAccount();
                    adjac.accountCode = capitalAccount.Code;
                    adjac.accountID = capitalAccount.id;
                    adjac.costCenterID = bdeBERP.SysPars.mainCostCenterID;
                    if (netIncome > 0)
                    {
                        adjac.creditAmount = netIncome;
                        adjac.debitAmount = 0;
                        totalDb -= netIncome;
                    }
                    else
                    {
                        adjac.creditAmount = 0;
                        adjac.debitAmount = -netIncome;
                        totalDb += -netIncome;
                    }
                    adjac.description = "Closing transaction";
                    incomeAccounts.Add(adjac);
                }
                Console.WriteLine("{0} accounts found. Net Income:{1} TotalDb:{2} TotalCr:{3}", incomeAccounts.Count-1, netIncome.ToString("#,#0.00"),totalDb,totalCr);

                adj.adjustmentAccounts = incomeAccounts.ToArray();
                adj.ShortDescription = "Closing transaction for year " + year;
                Console.WriteLine("Enter to post or closing window to cancel.");
                Console.ReadLine();
                Console.WriteLine("Posting transaction. Will take time.");
                bdeBERP.WriterHelper.BeginTransaction();
                try
                {
                    foreach (Account a in inactiveAccounts)
                        bdeBERP.Accounting.ActivateAcount<Account>(1, a.id, DateTime.Now);
                    bdeBERP.Accounting.PostGenericDocument(1, adj);
                    foreach (Account a in inactiveAccounts)
                        bdeBERP.Accounting.DeactivateAccount<Account>(1, a.id, DateTime.Now);
                    bdeBERP.WriterHelper.CommitTransaction();
                }
                catch 
                {
                    bdeBERP.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed\n" + ex.Message);
            }
        }

        [MaintenanceJobMethod("Close Year (Old Version)")]
        public static void closeYearOldVer()
        {
            Console.Write("Enter year:");
            int year;
            while (!int.TryParse(Console.ReadLine(), out year) || year < 2000 || year > 2010)
            {
                Console.Write("Invalid year. Enter again:");
            }

            try
            {
                iERPTransactionBDE bdeBERP = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                Account capitalAccount;
                Console.WriteLine("Capital accounts");
                foreach (Account ac in bdeBERP.Accounting.GetLeafAccounts<Account>(bdeBERP.Accounting.GetAccountID<Account>("3000")))
                {
                    Console.WriteLine(ac.Code + "\t" + ac.Name);
                }
                Console.Write("Enter capital account code:");
                while ((capitalAccount = bdeBERP.Accounting.GetAccount<Account>(Console.ReadLine())) == null || !capitalAccount.Code.StartsWith("3"))
                {
                    Console.Write("Invalid capital account. Enter again:");
                }
                AdjustmentDocument adj = new AdjustmentDocument();
                adj.DocumentDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(30, 10, year)).GridDate;
                adj.DocumentDate = new DateTime(adj.DocumentDate.Year, adj.DocumentDate.Month, adj.DocumentDate.Day, 23, 50, 0);
                adj.voucher = new DocumentTypedReference(bdeBERP.Accounting.getDocumentSerialType("JV").id, "JV-Closing-" + year, true);
                List<AdjustmentAccount> incomeAccounts = new List<AdjustmentAccount>();
                double netIncome = 0;
                Console.WriteLine("Collecting balances..");
                List<Account> inactiveAccounts = new List<Account>();
                double totalCr = 0, totalDb = 0;
                foreach (string parent in new string[] { "4000", "5000", "6000" })
                {
                    Account parentAccount = bdeBERP.Accounting.GetAccount<Account>(parent);
                    foreach (Account la in bdeBERP.Accounting.GetLeafAccounts<Account>(parentAccount.id))
                    {
                        bool hasTransaction = false;
                        foreach (CostCenterAccount csa in bdeBERP.Accounting.GetCostCenterAccountsOf<Account>(la.id))
                        {
                            if (csa.isControlAccount)
                                continue;
                            double balance = bdeBERP.Accounting.GetNetBalanceAsOf(csa.id, 0, adj.DocumentDate);
                            if (AccountBase.AmountEqual(balance, 0))
                                continue;
                            AdjustmentAccount adjac = new AdjustmentAccount();
                            adjac.accountCode = la.Code;
                            adjac.accountID = csa.accountID;
                            adjac.costCenterID = csa.costCenterID;

                            if (csa.creditAccount)
                            {
                                if (balance > 0)
                                {
                                    adjac.creditAmount = 0;
                                    adjac.debitAmount = balance;
                                    totalDb += balance;
                                }
                                else
                                {
                                    adjac.creditAmount = -balance;
                                    adjac.debitAmount = 0;
                                    totalDb += balance;
                                }
                                netIncome += balance;
                            }
                            else
                            {
                                if (balance > 0)
                                {
                                    adjac.creditAmount = balance;
                                    adjac.debitAmount = 0;
                                    totalDb -= balance;
                                }
                                else
                                {
                                    adjac.creditAmount = 0;
                                    adjac.debitAmount = -balance;
                                    totalDb -= balance;
                                }
                                netIncome -= balance;
                            }
                            adjac.description = "Closing transaction";
                            incomeAccounts.Add(adjac);
                            hasTransaction = true;
                        }
                        if (hasTransaction && la.Status != AccountStatus.Activated)
                        {
                            inactiveAccounts.Add(la);
                        }
                    }
                }

                if (!AccountBase.AmountEqual(netIncome, 0))
                {
                    AdjustmentAccount adjac = new AdjustmentAccount();
                    adjac.accountCode = capitalAccount.Code;
                    adjac.accountID = capitalAccount.id;
                    adjac.costCenterID = bdeBERP.SysPars.mainCostCenterID;
                    if (netIncome > 0)
                    {
                        adjac.creditAmount = netIncome;
                        adjac.debitAmount = 0;
                        totalDb -= netIncome;
                    }
                    else
                    {
                        adjac.creditAmount = 0;
                        adjac.debitAmount = -netIncome;
                        totalDb += -netIncome;
                    }
                    adjac.description = "Closing transaction";
                    incomeAccounts.Add(adjac);
                }
                Console.WriteLine("{0} accounts found. Net Income:{1} TotalDb:{2} TotalCr:{3}", incomeAccounts.Count - 1, netIncome.ToString("#,#0.00"), totalDb, totalCr);

                adj.adjustmentAccounts = incomeAccounts.ToArray();
                adj.ShortDescription = "Closing transaction for year " + year;
                Console.WriteLine("Enter to post or closing window to cancel.");
                Console.ReadLine();
                Console.WriteLine("Posting transaction. Will take time.");
                bdeBERP.WriterHelper.BeginTransaction();
                try
                {
                    foreach (Account a in inactiveAccounts)
                        bdeBERP.Accounting.ActivateAcount<Account>(1, a.id, DateTime.Now);
                    bdeBERP.Accounting.PostGenericDocument(1, adj);
                    foreach (Account a in inactiveAccounts)
                        bdeBERP.Accounting.DeactivateAccount<Account>(1, a.id, DateTime.Now);
                    bdeBERP.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bdeBERP.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed\n" + ex.Message);
            }
        }

        
    }
}

