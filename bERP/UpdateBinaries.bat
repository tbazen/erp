xcopy iERPMan\iERPManWin\bin\Debug bin\Client /S /Y
xcopy iCasherFrontEnd\bin\Debug  bin\Client /S /Y
del ServerBin\log\*.* /Q
xcopy iERPTransactionService\bin\Debug\*.*  bin\Server /S /Y

del bin\Client\*.pdb /Q
del bin\Client\*.config /Q
del bin\Client\*.vshost.* /Q
del bin\Client\*.xml /Q
del bin\Client\DevExpress.* /Q

del bin\Server\*.pdb /Q
del bin\Server\*.vshost.* /Q
del bin\Server\*.config /Q
del bin\Server\*.xml /Q
del bin\Server\log\*.* /Q
