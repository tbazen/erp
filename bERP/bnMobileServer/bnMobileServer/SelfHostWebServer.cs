using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.SelfHost;
using System.Web.Routing;

namespace INTAPS.ClientServer.SelfHostingWebServer
{
    public static class SelfHostingExtension
    {
        public static string getCookie(this HttpRequestMessage request, string key)
        {
            foreach (var c in request.Headers.GetCookies())
            {
                foreach (var cv in c.Cookies)
                {
                    if (key.Equals(cv.Name))
                        return cv.Value;
                }
            }
            return null;
        }
    }
    class SelfHostWebServerMessageFilter : DelegatingHandler
    {
        public static string getMIMEFromExtension(string path)
        {
            System.IO.FileInfo fi = new FileInfo(path);
            string mime = null;
            switch (fi.Extension)
            {
                case ".css":
                    mime = "text/css";
                    break;
                case ".jpg":
                    mime = "image/jpeg";
                    break;
                case ".png":
                    mime = "image/png";
                    break;
                case ".htm":
                case ".html":
                    mime = "text/html";
                    break;
                case ".js":
                    mime = "application/javascript";
                    break;
                case ".map":
                    mime = "application/json";
                    break;
                case ".ts":
                    mime = "text/x.typescript";
                    break;
                case ".ico":
                    mime = "image/x-icon";
                    break;
                case ".eot":
                    mime = "application/vnd.ms-fontobject";
                    break;
                case ".woff":
                    mime = "application/font-woff";
                    break;
                case ".ttf":
                    mime = "application/x-font-truetype";
                    break;
                case ".svg":
                    mime = "image/svg+xml";
                    break;
                case ".otf":
                    mime = "application/x-font-opentype";
                    break;
            }
            return mime;
        }
        
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            Console.WriteLine(string.Format("Request {0}",request.ToString()));
            string localPath = request.RequestUri.AbsolutePath.Replace('/', '\\');
            string path = System.Configuration.ConfigurationManager.AppSettings["self_host_html_root"] + localPath;
            string mime = getMIMEFromExtension(path);
            if (System.IO.File.Exists(path) && mime != null)
            {
                //Console.WriteLine("Serving static content {0}", path);
                var staticResp = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(System.IO.File.OpenRead(path)),
                };
                staticResp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(mime);
                var tsc = new TaskCompletionSource<HttpResponseMessage>();
                tsc.SetResult(staticResp);
                return tsc.Task.Result;
            }
            HttpResponseMessage response = await base.SendAsync(request, cancellationToken);
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("Error: " + response.StatusCode);
                
            }
            return response;
        }
    }
    class SelfHostWebServerClass
    {
        private static HttpSelfHostServer server;
        public static void initializeSelfHostWebServer()
        {

            string baseAddress = System.Configuration.ConfigurationManager.AppSettings["wsis_self_host_server"];
            if (string.IsNullOrEmpty(baseAddress))
                return;
            Console.WriteLine("Starting WSIS self host web server.");
            var config = new HttpSelfHostConfiguration(baseAddress);
            config.MapHttpAttributeRoutes();            
            //Logging
            config.MessageHandlers.Add(new SelfHostWebServerMessageFilter());
            
            server = new HttpSelfHostServer(config);
            try
            {
                server.OpenAsync().Wait();
                Console.WriteLine("Self host web server running.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("WSIS self host web server failed to start", ex);
                throw;
            }
            Console.WriteLine("Press Enter to quit.");
        }
    }
}