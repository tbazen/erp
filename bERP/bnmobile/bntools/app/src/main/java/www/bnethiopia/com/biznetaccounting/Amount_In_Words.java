package www.bnethiopia.com.biznetaccounting;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.RelativeLayout;

public class Amount_In_Words extends AppCompatActivity {
    private EditText numbers,results;
    private boolean ignoreEvent=false;
    Double tryParse(String txt) {
        try {

            return Double.parseDouble(txt);
        }
        catch (Exception ex)
        {
            return  0.0;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amount__in__words);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
       // getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
       // getSupportActionBar().hide();

        getSupportActionBar().hide();
        numbers = (EditText) findViewById(R.id.editText);
        results = (EditText) findViewById(R.id.editText2);
        numbers.addTextChangedListener(textWatcher);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                results.setText("");
            }
            else
            {
                Double number1=tryParse(txt);

                results.setText(NumbertoWords(number1));
            }
            ignoreEvent=false;


        }
    };

    String NumbertoWords(Double bv){

        String returnz = Words.convert(bv);

        return returnz;
    }

    public void amtinwords(View view){
       // Intent i = new Intent(Amount_In_Words.this,BizNet.class);
       // startActivity(i);
        Intent intent = new Intent(this, BizNet.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityIfNeeded(intent, 0);
    }

}




