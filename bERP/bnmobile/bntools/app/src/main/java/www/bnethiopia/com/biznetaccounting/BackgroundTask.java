package www.bnethiopia.com.biznetaccounting;


import android.content.Context;

import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

import static android.R.attr.id;

public class BackgroundTask extends AsyncTask<String,Void,String> {
    Context ctx;

    BackgroundTask(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override

    protected String doInBackground(String... params) {
        String postUrl = "https://intaps.com:8010/api/Customers";
        StringBuilder sb = new StringBuilder();
        HttpURLConnection httpURLConnection = null;


            try {
                URL url = new URL(postUrl);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setUseCaches(false);
            httpURLConnection.connect();

           // String id = params[1];
            String companyName = params[1];
            String name = params[2];
            String tin = params[3];
            String comtype = params[4];
            String profession = params[5];
            String phoneNo = params[6];
            String email = params[7];
            //creating json object
            //Customer c = new Customer(id,companyName,name,tin,comtype,profession,phoneNo,email);


            DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
            JSONObject json = new JSONObject();

            //json.put("key", id);
            json.put("CompanyName", companyName);
            json.put("Name", name);
            json.put("Tin", tin);
            json.put("Type", comtype);
            json.put("Profession", profession);
            json.put("PhoneNo", phoneNo);
            json.put("Email", email);
            wr.writeBytes(json.toString());
            wr.flush();
            wr.close();

            int HttpResult = httpURLConnection.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        httpURLConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                System.out.println("" + sb.toString());

            } else {
                System.out.println(httpURLConnection.getResponseMessage());
            }
        } catch (MalformedURLException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (httpURLConnection != null)
                httpURLConnection.disconnect();
        }


            return null;
    }


    @Override
    protected void onPostExecute(String result) {
        Toast.makeText(ctx, result, Toast.LENGTH_LONG).show();

    }
}