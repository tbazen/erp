package www.bnethiopia.com.biznetaccounting;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class BizNet extends AppCompatActivity {

    private int backButtonCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biz_net);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
    }

    public void vat(View view) {
        Intent i = new Intent(BizNet.this, VAT_Calculator.class);
        startActivity(i);
    }
    public void tot(View view) {
        Intent i = new Intent(BizNet.this, tot.class);
        startActivity(i);
    }
    public void totwitholding(View view) {
        Intent i = new Intent(BizNet.this, ToT_Withholding.class);
        startActivity(i);
    }
    public void aboutbiznettool(View view) {
        Intent i = new Intent(BizNet.this, About_us.class);
        startActivity(i);
    }


    public void vat_withholding(View view) {
        Intent i = new Intent(BizNet.this, VAT_WITHHOLDING_Calculator.class);
        startActivity(i);
    }

    public void withholding(View view) {
        Intent i = new Intent(BizNet.this, WITHHOLDING_Calculator.class);
        startActivity(i);
    }

    public void payroll(View view) {
        Intent i = new Intent(BizNet.this, PAYROLL_Calculator.class);
        startActivity(i);
    }

    public void amountinwords(View view) {
        Intent i = new Intent(BizNet.this, Amount_In_Words.class);
        startActivity(i);
    }

    public void caleldar(View view) {
        Intent i = new Intent(BizNet.this, Calendar.class);
        startActivity(i);
    }

    public void calendarconverter(View view) {
        Intent i = new Intent(BizNet.this, Calendar_Converter.class);
        startActivity(i);
    }
    @Override
    public void onBackPressed()
    {

        if(backButtonCount >= 2)
        {
            Toast.makeText(this, "Press the back button once again to close the application.", Toast.LENGTH_SHORT).cancel();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else
        {
            Toast.makeText(this, "Press the back button once again to close the application.", Toast.LENGTH_SHORT).show();
            backButtonCount++;
        }
    }
}
