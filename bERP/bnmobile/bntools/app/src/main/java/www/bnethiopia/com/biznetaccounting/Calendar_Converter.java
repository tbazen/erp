package www.bnethiopia.com.biznetaccounting;
import java.util.*;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.*;
import java.util.Calendar;

public class Calendar_Converter extends AppCompatActivity {

    private EditText ethioday,gregday,ethiomonth,gregmonth,ethioyear,gregyear,
            ethioname,gregname;
    private boolean ignoreEvent=false;
    private int Month;
    private int Day;
    private int Year;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar__converter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        ethioday = (EditText) findViewById(R.id.editText);
        ethiomonth = (EditText) findViewById(R.id.editText2);
        ethioyear = (EditText) findViewById(R.id.editText4);
        ethioname = (EditText) findViewById(R.id.editText6);

        gregday = (EditText) findViewById(R.id.editText1);
        gregmonth = (EditText) findViewById(R.id.editText22);
        gregyear = (EditText) findViewById(R.id.editText44);
        gregname = (EditText) findViewById(R.id.editText66);

        ethioday.addTextChangedListener(textWatcherthio);
        ethiomonth.addTextChangedListener(textWatcherthio);
        ethioyear.addTextChangedListener(textWatcherthio);

        gregday.addTextChangedListener(textWatchergreg);
        gregmonth.addTextChangedListener(textWatchergreg);
        gregyear.addTextChangedListener(textWatchergreg);


        java.util.Calendar now = java.util.Calendar.getInstance();
        this.Month = now.get(java.util.Calendar.MONTH) + 1;
        this.Day = now.get(java.util.Calendar.DAY_OF_MONTH);
        this.Year = now.get(java.util.Calendar.YEAR);


        String strD = Integer.toString(Day);
        String strM = Integer.toString(Month);
        String strY = Integer.toString(Year);

        gregday.setText(strD);
        gregmonth.setText(strM);
        gregyear.setText(strY);

        EtGrDate date = new EtGrDate();
        if (!EtGrDate.TryParse(strD + "\\" + strM + "\\" + strY, false, date)) {
            ethioday.setText("");
            ethiomonth.setText("");
            ethioyear.setText("");
            ethioname.setText("");
            gregname.setText("");
            return;
        }

        EtGrDate grDate = EtGrDate.ToEth(date);

        ethioday.setText(Integer.toString(grDate.Day));
        ethiomonth.setText(Integer.toString(grDate.Month));
        ethioyear.setText(Integer.toString(grDate.Year));


        ethioname.setText(EtGrDate.GetDayOfWeekNameEt(grDate.getGetDayOfWeekEt()) + ",  " + (EtGrDate.GetEtMonthName(grDate.Month)) + "  " + Integer.toString(grDate.Day) + ",  " + Integer.toString(grDate.Year));

        gregname.setText(EtGrDate.GetDayOfWeekNameGrig(date.getDayOfWeekGrig()) + ",  " + EtGrDate.GetGrigMonthName(Integer.parseInt(strM)) + "  " + strD + ",  " + strY);

    }




    TextWatcher textWatcherthio = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            
        }

        @Override
        public void afterTextChanged(Editable s) {


            if (ignoreEvent)
                return;
            ignoreEvent = true;
            try {


                String strD = ethioday.getText().toString();
                String strM = ethiomonth.getText().toString();
                String strY = ethioyear.getText().toString();
                EtGrDate date = new EtGrDate();
                if (!EtGrDate.TryParse(strD + "\\" + strM + "\\" + strY, true, date)) {
                    gregday.setText("");
                    gregmonth.setText("");
                    gregyear.setText("");

                    gregname.setText("");
                    ethioname.setText("");
                    return;
                }

                EtGrDate grDate = EtGrDate.ToGrig(date);


                gregday.setText(Integer.toString(grDate.Day));
                gregmonth.setText(Integer.toString(grDate.Month));
                gregyear.setText(Integer.toString(grDate.Year));

                ethioname.setText(EtGrDate.GetDayOfWeekNameEt(date.getGetDayOfWeekEt())+",  "+EtGrDate.GetEtMonthName(Integer.parseInt(strM))+ "  "+strD+",  "+strY);

                gregname.setText(EtGrDate.GetDayOfWeekNameGrig(grDate.getDayOfWeekGrig()) +",  "+EtGrDate.GetGrigMonthName(grDate.Month)+"  "+Integer.toString(grDate.Day)+",  "+Integer.toString(grDate.Year));


                /*
                gregday2.setText(Integer.toString(grDate.Day));
                gregdayname.setText(EtGrDate.GetDayOfWeekNameGrig(grDate.getDayOfWeekGrig()));
                gregmonthname.setText(EtGrDate.GetGrigMonthName(grDate.Month));
                gregyear2.setText(Integer.toString(grDate.Year));

                ethioday2.setText(strD);
                ethioyear2.setText(strY);
                ethiodayname.setText(EtGrDate.GetDayOfWeekNameEt(date.getGetDayOfWeekEt()));
                ethiomonthname.setText((EtGrDate.GetEtMonthName(Integer.parseInt(strM))));
*/



            }
            finally {
                ignoreEvent=false;
            }

        }
    };



    TextWatcher textWatchergreg = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {



            if (ignoreEvent)
                return;
            ignoreEvent = true;
            try {

                String strD = gregday.getText().toString();
                String strM = gregmonth.getText().toString();
                String strY = gregyear.getText().toString();
                EtGrDate date = new EtGrDate();
                if (!EtGrDate.TryParse(strD + "\\" + strM + "\\" + strY,false, date)) {
                    ethioday.setText("");
                    ethiomonth.setText("");
                    ethioyear.setText("");
                    ethioname.setText("");
                    gregname.setText("");
                    return;
                }

                EtGrDate grDate = EtGrDate.ToEth(date);

                ethioday.setText(Integer.toString(grDate.Day));
                ethiomonth.setText(Integer.toString(grDate.Month));
                ethioyear.setText(Integer.toString(grDate.Year));


                ethioname.setText(EtGrDate.GetDayOfWeekNameEt(grDate.getGetDayOfWeekEt())+",  "+(EtGrDate.GetEtMonthName(grDate.Month))+ "  "+Integer.toString(grDate.Day)+",  "+Integer.toString(grDate.Year));

                gregname.setText(EtGrDate.GetDayOfWeekNameGrig(date.getDayOfWeekGrig()) +",  "+EtGrDate.GetGrigMonthName(Integer.parseInt(strM))+"  "+strD+",  "+strY);
                /*
                ethioday2.setText(Integer.toString(grDate.Day));
                ethioyear2.setText(Integer.toString(grDate.Year));
                ethiodayname.setText(EtGrDate.GetDayOfWeekNameEt(grDate.getGetDayOfWeekEt()));
                ethiomonthname.setText((EtGrDate.GetEtMonthName(grDate.Month)));


                gregday2.setText(strD);
                gregdayname.setText(EtGrDate.GetDayOfWeekNameGrig(date.getDayOfWeekGrig()));
                gregmonthname.setText(EtGrDate.GetGrigMonthName(Integer.parseInt(strM)));
                gregyear2.setText(strY);


*/
            }
            finally {
                ignoreEvent=false;
            }

        }
    };

    public  void calaconverter(View view){
      //  Intent i = new Intent(Calendar_Converter.this,BizNet.class);
       // startActivity(i);
        Intent intent = new Intent(this, BizNet.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityIfNeeded(intent, 0);
    }

    public void todaybutton(View view){

        if (ignoreEvent)
            return;
        ignoreEvent = true;
        try {

            java.util.Calendar now = java.util.Calendar.getInstance();
            this.Month = now.get(java.util.Calendar.MONTH)+1;
            this.Day = now.get(java.util.Calendar.DAY_OF_MONTH);
            this.Year = now.get(java.util.Calendar.YEAR);


            String strD = Integer.toString(Day);
            String strM = Integer.toString(Month);
            String strY = Integer.toString(Year);

            gregday.setText(strD);
            gregmonth.setText(strM);
            gregyear.setText(strY);

            EtGrDate date = new EtGrDate();
            if (!EtGrDate.TryParse(strD + "\\" + strM + "\\" + strY,false, date)) {
                ethioday.setText("");
                ethiomonth.setText("");
                ethioyear.setText("");
                ethioname.setText("");
                gregname.setText("");
                return;
            }

            EtGrDate grDate = EtGrDate.ToEth(date);

            ethioday.setText(Integer.toString(grDate.Day));
            ethiomonth.setText(Integer.toString(grDate.Month));
            ethioyear.setText(Integer.toString(grDate.Year));


            ethioname.setText(EtGrDate.GetDayOfWeekNameEt(grDate.getGetDayOfWeekEt())+",  "+(EtGrDate.GetEtMonthName(grDate.Month))+ "  "+Integer.toString(grDate.Day)+",  "+Integer.toString(grDate.Year));

            gregname.setText(EtGrDate.GetDayOfWeekNameGrig(date.getDayOfWeekGrig()) +",  "+EtGrDate.GetGrigMonthName(Integer.parseInt(strM))+"  "+strD+",  "+strY);



        }

    finally {
        ignoreEvent=false;
    }

    }

}




