package www.bnethiopia.com.biznetaccounting;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by meda on 8/29/2016.
 */
public class EtGrDate

{
    public EtGrDate(){

    }

    class DateTime {

        public DateTime(int Year, int Month, int Day) {


        }

    }
    static class MonthAndDay
    {
        public int m;
        public int d;
    }
    final int c1 = 0x23ab1;
    final int c2 = 0x8eac;
    final int c3 = 0x5b5;
    final int dd = 0x97e;
    public int Day;
    public int Month;
    public int Year;
    private static int Months(int index)
    {
        switch (index)
        {
            case 0:
            case 2:
            case 4:
            case 6:
            case 7:
            case 9:
            case 11:
                return 0x1f;

            case 1:
                return 0x1c;
        }
        return 30;
    }

    private static int AddMonths(int m, int y)
    {
        int num2 = 0;
        for (int i = 0; i < (m - 1); i++)
        {
            num2 += Months(i);
            if ((i == 1) && IsLeapYearGr(y))
            {
                num2++;
            }
        }
        return num2;
    }

    private static void GetMonthAndDayGrig(int n, int y,MonthAndDay md)
    {
        int num = 1;
        while (n >= GetMonthLengthGrig(num, y))
        {
            n -= GetMonthLengthGrig(num, y);
            num++;
        }
        md.m = num;
        md.d = n + 1;
    }

    public static int GetMonthLengthGrig(int m, int y)
    {
        if ((m == 2) && IsLeapYearGr(y))
        {
            return 0x1d;
        }
        return Months(m - 1);

    }

    public static int GetMonthLengthEt(int m, int y)
    {
        if (m == 13)
        {
            return (IsLeapYearEt(y) ? 6 : 5);
        }
        return 30;
    }

    public static boolean IsLeapYearGr(int y)
    {
        return (((y % 4) == 0) && (((y % 100) != 0) || ((y % 400) == 0)));
    }

    public int getDayNoEt()
    {
        int num = this.Year / 4;
        int num2 = this.Year % 4;
        return (((((num * 0x5b5) + (num2 * 0x16d)) + ((this.Month - 1) * 30)) + this.Day) - 1);
    }
    public int getGetDayOfWeekEt()
    {
        return (((this.getDayNoEt() + 1) % 7) + 1);
    }
    public EtGrDate(int dn, boolean Ethiopian)
    {
        int num;
        int num2;
        int num3;
        int num4;
        if (Ethiopian)
        {
            num = dn / 0x5b5;
            num2 = dn % 0x5b5;
            num3 = num2 / 0x16d;
            num4 = num2 % 0x16d;
            if (num2 != 0x5b4)
            {
                this.Year = (num * 4) + num3;
                this.Month = (num4 / 30) + 1;
                this.Day = (num4 % 30) + 1;
            }
            else
            {
                this.Year = ((num * 4) + num3) - 1;
                this.Month = 13;
                this.Day = 6;
            }
        }
        else
        {
            num = dn / 0x23ab1;
            num2 = dn % 0x23ab1;
            num3 = num2 / 0x8eac;
            num4 = num2 % 0x8eac;
            int num5 = num4 / 0x5b5;
            int num6 = num4 % 0x5b5;
            int num7 = num6 / 0x16d;
            int n = num6 % 0x16d;
            this.Year = ((((num * 400) + (num3 * 100)) + (num5 * 4)) + num7) + 1;
            this.Month = 0;
            this.Day = 0;
            MonthAndDay md=new MonthAndDay();
            md.m=this.Month;
            md.d=this.Day;
            GetMonthAndDayGrig(n, this.Year, md);
            this.Month=md.m;
            this.Day=md.d;
        }
    }

    public int getDayNoGrig()
    {
        int num = (this.Year - 1) / 400;
        int num2 = (this.Year - 1) % 400;
        int num3 = num2 / 100;
        int num4 = num2 % 100;
        int num5 = num4 / 4;
        int num6 = num4 % 4;
        int num7 = AddMonths(this.Month, this.Year);
        return (((((((num * 0x23ab1) + (num3 * 0x8eac)) + (num5 * 0x5b5)) + (num6 * 0x16d)) + num7) + this.Day) - 1);

    }
    public int getDayOfWeekGrig()
    {
        return ((this.getDayNoGrig() % 7) + 1);
    }
    public EtGrDate(int d, int m, int y)
    {
        this.Day = d;
        this.Month = m;
        this.Year = y;
    }

    public EtGrDate(java.util.Calendar dt)
    {
        java.util.Calendar now = java.util.Calendar.getInstance();
        this.Month = now.get(java.util.Calendar.MONTH)+1;
        this.Day = now.get(java.util.Calendar.DAY_OF_MONTH);
        this.Year = now.get(java.util.Calendar.YEAR);


        //   this.Day = dt.getDay();
        // this.Month = dt.getMonth();
        // this.Year = dt.getYear();
    }

    public DateTime getGridDate()
    {
        return new DateTime(this.Year, this.Month, this.Day);
    }
    public static EtGrDate ToEth(EtGrDate gr)
    {
        return new EtGrDate(gr.getDayNoGrig() - 0x97e, true);
    }

    public static EtGrDate ToEth(java.util.Calendar dt)
    {
        return ToEth(new EtGrDate(dt));
    }


    public static EtGrDate ToGrig(EtGrDate et)
    {
        return new EtGrDate(et.getDayNoEt() + 0x97e, false);
    }

    public static String GetEtMonthName(int m)
    {
        switch (m)
        {
            case 1:
                return "መስከረም";

            case 2:
                return "ጥቅምት";

            case 3:
                return "ህዳር";

            case 4:
                return "ታህሳስ";

            case 5:
                return "ጥር";

            case 6:
                return "የካቲት";

            case 7:
                return "መጋቢት";

            case 8:
                return "ሚያዚያ";

            case 9:
                return "ግንቦት";

            case 10:
                return "ሰኔ";

            case 11:
                return "ሐምሌ";

            case 12:
                return "ነሀሴ";

            case 13:
                return "ጳጉሜ";
        }
        return "";
    }


    public static String GetEtMonthNameEng(int m)
    {
        switch (m)
        {
            case 1:
                return "Meskrem";

            case 2:
                return "Tikimt";

            case 3:
                return "Hidar";

            case 4:
                return "Tahisas";

            case 5:
                return "Tir";

            case 6:
                return "Yekatis";

            case 7:
                return "Megabit";

            case 8:
                return "Miyazia";

            case 9:
                return "Ginbot";

            case 10:
                return "Sene";

            case 11:
                return "Hamle";

            case 12:
                return "Nehase";
            case 13:
                return "Pagume";
        }
        return "";

    }
    public static String GetDayOfWeekNameEt(int d)
    {
        switch (d)
        {
            case 1:
                return "ሰኞ";

            case 2:
                return "ማክሰኞ";

            case 3:
                return "ረቡዕ";

            case 4:
                return "ሀሙስ";

            case 5:
                return "አርብ";

            case 6:
                return "ቅዳሜ";

            case 7:
                return "እሁድ";
        }
        return "";
    }

    public static String GetGrigMonthName(int m)
    {
        switch (m)
        {
            case 1:
                return "January";

            case 2:
                return "February";

            case 3:
                return "March";

            case 4:
                return "April";

            case 5:
                return "May";

            case 6:
                return "June";

            case 7:
                return "July";

            case 8:
                return "August";

            case 9:
                return "September";

            case 10:
                return "October";

            case 11:
                return "Novomber";

            case 12:
                return "December";
        }
        return "";
    }

    public static String GetDayOfWeekNameGrig(int d)
    {
        switch (d)
        {
            case 1:
                return "Monday";

            case 2:
                return "Tuesday";

            case 3:
                return "Wednesday";

            case 4:
                return "Thursday";

            case 5:
                return "Friday";

            case 6:
                return "Saturday";

            case 7:
                return "Sunday";
        }
        return "";
    }

    public static boolean IsLeapYearEt(int y)
    {
        return ((y % 4) == 3);
    }


    public String toString()
    {

        return Integer.toString(this.Day)+ "/" + Integer.toString(this.Month)
                + "/" + Integer.toString(this.Year);
    }

    public static EtGrDate Parse(String date)
    {

        String[] strArray = date.split("\\d{1,}");
        if (strArray.length < 3)
        {
            throw new IllegalArgumentException("Invalid date format");
        }
        int d = Integer.parseInt(strArray[0]);
        int m = Integer.parseInt(strArray[1]);
        return new EtGrDate(d, m, Integer.parseInt(strArray[2]));
    }
    static class ParseIntResult
    {
        public int val;
        public boolean success;
    }
    static ParseIntResult tryParseInt(String s)
    {
        ParseIntResult ret=new ParseIntResult();
        try
        {
            ret.val=Integer.parseInt(s);
            ret.success=true;
            return ret;
        }
        catch(Exception ex)
        {
            ret.success=false;
            return ret;
        }
    }
    public static boolean TryParse(String str,boolean ethiopian,EtGrDate date)
    {
        Pattern p=Pattern.compile("\\d{1,}");
        ArrayList<String> parts=new ArrayList<String>();
        Matcher match=p.matcher(str);
        while(match.find()) {
            {
                parts.add(match.group());
            }
        }
        if (parts.size()!= 3)
        {
            return false;
        }

        int d;
        int m;
        int y;
        ParseIntResult res;

        res=tryParseInt(parts.get(0));
        if(!res.success)
            return false;
        d=res.val;

        res=tryParseInt(parts.get(1));
        if(!res.success)
            return false;
        m=res.val;

        res = tryParseInt(parts.get(2));
        if(!res.success)
            return false;
        y=res.val;
        if (ethiopian)
        {

            if (m < 1 || d<1 || y<1800)
                return false;
            if (m < 13)
            {
                if (d < 1 || d > 30)
                    return false;
            }
            if (m == 13)
            {
                if (IsLeapYearEt(y) && d > 6)
                    return false;
                if (d > 5)
                    return false;
            }
            if (m > 13)
                return false;
            if (y > 2100)
                return false;
        }

        else{
            if (m < 1 || d<1 || y<1800)
                return false;
            if (m < 13)
            {
                if (d < 1 || d > 31)
                    return false;
            }
            if (m > 12)
                return false;
            if (y > 2100)
                return false;
        }

        date.Day=d;
        date.Month=m;
        date.Year=y;
        return true;
    }
    static boolean IsValidDMY(int day,int month,int year,boolean _eth)
    {
        if ((year < 0x3e8) || (year > 0xbb8))
        {
            return false;
        }
        if (month < 1)
        {
            return false;
        }
        if (day < 1)
        {
            return false;
        }
        if (_eth)
        {
            if (month > 13)
            {
                return false;
            }
            if (day > GetMonthLengthEt(month, year))
            {
                return false;
            }
        }
        else
        {
            if (month > 12)
            {
                return false;
            }
            if (day > GetMonthLengthGrig(month, year))
            {
                return false;
            }
        }
        return true;
    }
}


