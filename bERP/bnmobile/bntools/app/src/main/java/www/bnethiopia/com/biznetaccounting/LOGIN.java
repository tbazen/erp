package www.bnethiopia.com.biznetaccounting;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.R.attr.id;

public class LOGIN extends AppCompatActivity {
    private Spinner select_cotype;
    EditText Company_Name, Employee_Name, TIN_No, profess, phone_num, emailadress;
    public long Id;
    public String CompanyName;
    public String Name;
    public String Tin;
    public String Type;
    public String Profession;
    public String PhoneNo;
    public String Email;
    private Button btnSubmit;

    void registerBackground() {
        //if succesffull modified=false
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biznet__accounting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        SharedPreferences preferences = getPreferences(0);
     /*  if (preferences.getBoolean("registered", false)) {
            if (preferences.getBoolean("modified", false)) {
                registerBackground();
            }
            Intent ii = new Intent(LOGIN.this, BizNet.class);
            startActivity(ii);
            return;
        }
*/
        Company_Name = (EditText) findViewById(R.id.editText7);
        Employee_Name = (EditText) findViewById(R.id.editText10);
        TIN_No = (EditText) findViewById(R.id.editText12);
        profess = (EditText) findViewById(R.id.editText11);
        phone_num = (EditText) findViewById(R.id.editText13);
        emailadress = (EditText) findViewById(R.id.editText14);
        select_cotype = (Spinner) findViewById(R.id.spinner2);
        addItemsOnSpinner2();

        //set controls from preferences
        Company_Name.setText(preferences.getString("companyName", ""));
        Employee_Name.setText(preferences.getString("name", ""));
        TIN_No.setText(preferences.getString("Tin", ""));
        profess.setText(preferences.getString("Profession", ""));
        phone_num.setText(preferences.getString("PhoneNo", ""));
        emailadress.setText(preferences.getString("Email", ""));
    }

    // add items into spinner dynamically
    public void addItemsOnSpinner2() {

        select_cotype = (Spinner) findViewById(R.id.spinner2);
        List<String> list = new ArrayList<String>();
        list.add("Select Company Type");
        list.add("Private Limited Company");
        list.add("Sole Property");
        list.add("Joint  ");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        select_cotype.setAdapter(dataAdapter);
    }


    public void register(View view) throws IOException {

        CompanyName = Company_Name.getText().toString();
        Name = Employee_Name.getText().toString();
        Tin = TIN_No.getText().toString();
        Type = select_cotype.getSelectedItem().toString();
        Profession = profess.getText().toString();
        PhoneNo = phone_num.getText().toString();
        Email = emailadress.getText().toString();
        registerCustomerLocal(CompanyName, Name, Tin, Type, Profession, PhoneNo, Email);

        HashMap postData = new HashMap();

        String login_name = "admin";
        String login_pass = "wsisadmin";

        postData.put("login_name", login_name);
        postData.put("login_pass", login_pass);
        PostResponseAsyncTask task1 = new PostResponseAsyncTask(SIGN_IN.this, postData, new AsyncResponse() {
            @Override
            public void processFinish(String s) {

                Log.d(LOG, s);
                if (s.contains("success")) {
                    Toast.makeText(SIGN_IN.this, "Login Succesfully", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(SIGN_IN.this, MENU.class);
                    startActivity(i);
                } else {
                    Toast.makeText(SIGN_IN.this, "Try Again", Toast.LENGTH_LONG).show();

                }
            }
        });
        task1.execute("http://192.168.137.1/webapp1/loginFood1.php");

        BackgroundTask backgroundTask = new BackgroundTask(this);
        backgroundTask.execute(CompanyName, Name, Tin, Type, Profession, PhoneNo, Email);


        Intent ii = new Intent(LOGIN.this, BizNet.class);
        startActivity(ii);
        /*
        if (registerCustomer(Id, CompanyName, Name, Tin, Type, Profession, PhoneNo, Email) == true) {


        } else {
            Toast.makeText(LOGIN.this, "Try Again", Toast.LENGTH_LONG).show();
        }*/
    }

    public void registerCustomerLocal(String companyName, String name, String tin, String comtype, String profession, String phoneNo, String email) {
        SharedPreferences preferences = getPreferences(0);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString("companyName", companyName);
        edit.putString("name", name);
        edit.putString("tin", tin);
        edit.putString("comtype", comtype);
        edit.putString("profession", profession);
        edit.putString("phoneNo", phoneNo);
        edit.putString("email", email);

        edit.putBoolean("modfied", true);
        edit.putBoolean("registered", true);
        edit.commit();


        //BackgroundTask backgroundTask = new BackgroundTask(this);
        //backgroundTask.execute(companyName, name, tin, comtype, profession, phoneNo, email);
        finish();
    }
        }

  /* class Customer
    {
        public long Id;
        public String CompanyName;
        public String Name;
        public String Tin;
        public String Type;
        public String Profession;
        public String PhoneNo;
        public String Email;

         Customer(long id, String companyName, String name, String tin, String type, String profession, String phoneNo, String email){
            this.Id = id;
            this.CompanyName = companyName;
            this.Name = name;
            this.Tin = tin;
            this.Type = type;
            this.Profession = profession;
            this.PhoneNo = phoneNo;
            this.Email = email;
        }
    }*/
