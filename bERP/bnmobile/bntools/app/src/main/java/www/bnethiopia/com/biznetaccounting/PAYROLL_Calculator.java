package www.bnethiopia.com.biznetaccounting;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Switch;

public class PAYROLL_Calculator extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    private EditText editGROSS,editTAX,editNONTAX,editINCOME,editPENS,editNET;
    private Switch mySwitch;
    public static boolean OnOff;

    private boolean ignoreEvent=false;


    double tryParse(String txt) {
        try {

            return Double.parseDouble(txt);
        }
        catch (Exception ex)
        {
            return  0.0;
        }
    }
    double tryParse2(String tx){
        try {
            return Double.parseDouble(tx);
        }
        catch (Exception ex){
            return 0.0;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payroll__calculator);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();


        mySwitch = (Switch) findViewById(R.id.switch1);
        mySwitch.setOnCheckedChangeListener(this);

        editGROSS = (EditText) findViewById(R.id.editGROSS);
        editTAX = (EditText) findViewById(R.id.editTAX);
        editNONTAX = (EditText)findViewById(R.id.editNONTAX);
        editINCOME = (EditText)findViewById(R.id.editINCOME);
        editPENS = (EditText) findViewById(R.id.editPENS);
        editNET = (EditText) findViewById(R.id.editNET);

        editGROSS.addTextChangedListener(textWatcherGROSS);
        editINCOME.addTextChangedListener(textWatcherINCOME);
        editPENS.addTextChangedListener(textWatcherPENS);
        editNET.addTextChangedListener(textWatcherNET);
        editTAX.addTextChangedListener(textWatcherTAX);
        editNONTAX.addTextChangedListener(textWatcherNONTAX);


       getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public  void backon(View view){
       // Intent i = new Intent(PAYROLL_Calculator.this,BizNet.class);
       // startActivity(i);
        Intent intent = new Intent(this, BizNet.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityIfNeeded(intent, 0);

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            // do something when check is selected
            editPENS.setEnabled(true);
            findViewById(R.id.content_four).setVisibility(View.VISIBLE);

            editGROSS.setText("");
            editINCOME.setText("");
            editNET.setText("");
            editPENS.setText("");

        } else {
            //do something when unchecked

            findViewById(R.id.content_four).setVisibility(View.GONE);

            editPENS.setEnabled(false);

            editGROSS.setText("");
            editINCOME.setText("");
            editNET.setText("");
            editPENS.setText("");



        }
    }
    TextWatcher textWatcherGROSS = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0 ) {
                editINCOME.setText("");
                editPENS.setText("");
                editNET.setText("");

            }
            else

            {

                double a = tryParse2(editTAX.getText().toString());
                double b = tryParse2(editNONTAX.getText().toString());

                double bv = tryParse(txt);
                if (editPENS.isEnabled()==true) {

                    editINCOME.setText(Double.valueOf(Math.round(getINCOMEFromGROSS(bv,a) * 100) / 100.0).toString());
                    editPENS.setText(Double.valueOf(Math.round(getPENSFromGROSS(bv) * 100) / 100.0).toString());
                    editNET.setText(Double.valueOf(Math.round(getNETFromGROSSP(bv,a,b) * 100) / 100.0).toString());
                }
                else{
                    editINCOME.setText(Double.valueOf(Math.round(getINCOMEFromGROSS(bv,a) * 100) / 100.0).toString());
                    editNET.setText(Double.valueOf(Math.round(getNETFromGROSS(bv,a,b) * 100) / 100.0).toString());
                    editPENS.setText(null);


                }

            }
            ignoreEvent=false;

        }
    };






    TextWatcher textWatcherINCOME = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                editGROSS.setText("");
                editPENS.setText("");
                editNET.setText("");

            }
            else

            {
                double a = tryParse2(editTAX.getText().toString());
                double b = tryParse2(editNONTAX.getText().toString());

                double bv=tryParse(txt);

                if (editPENS.isEnabled()==true) {

                    editGROSS.setText(Double.valueOf(Math.round(getGROSSFromINCOME(bv,a) * 100) / 100.0).toString());
                    editPENS.setText(Double.valueOf(Math.round(getPENSFromINCOME(bv,a) * 100) / 100.0).toString());
                    editNET.setText(Double.valueOf(Math.round(getNETFromINCOMEP(bv,a,b) * 100) / 100.0).toString());
                }
                else{
                    editGROSS.setText(Double.valueOf(Math.round(getGROSSFromINCOME(bv,a) * 100) / 100.0).toString());
                    editPENS.setText(null);
                    editNET.setText(Double.valueOf(Math.round(getNETFromINCOME(bv,a,b) * 100) / 100.0).toString());
                }

            }
            ignoreEvent=false;


        }
    };



    TextWatcher textWatcherPENS = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {


            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {

                editINCOME.setText("");
                editGROSS.setText("");
                editNET.setText("");

            }
            else

            {
                double a = tryParse2(editTAX.getText().toString());
                double b = tryParse2(editNONTAX.getText().toString());


                double bv=tryParse(txt);
                editINCOME.setText(Double.valueOf(Math.round(getINCOMEFromPENS(bv,a)*100)/100.0).toString());
                editGROSS.setText(Double.valueOf(Math.round(getGROSSFromPENS(bv)*100)/100.0).toString());
                editNET.setText(Double.valueOf(Math.round(getNETFromPENS(bv,a,b)*100)/100.0).toString());
            }
            ignoreEvent=false;

        }
    };



    TextWatcher textWatcherNET  =new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                editINCOME.setText("");
                editPENS.setText("");
                editGROSS.setText("");

            }
            else

            {
                double a = tryParse2(editTAX.getText().toString());
                double b = tryParse2(editNONTAX.getText().toString());

                double bv=tryParse(txt);

                try {


                    if (editPENS.isEnabled() == true) {


                        editINCOME.setText(Double.valueOf(Math.round(getINCOMEFromNETP(bv, a, b) * 100) / 100.0).toString());
                        editPENS.setText(Double.valueOf(Math.round(getPENSFromNETP(bv, a, b) * 100) / 100.0).toString());
                        editGROSS.setText(Double.valueOf(Math.round(getGROSSFromNETP(bv, a, b) * 100) / 100.0).toString());
                    } else {
                        editINCOME.setText(Double.valueOf(Math.round(getINCOMEFromNET(bv, a, b) * 100) / 100.0).toString());
                        editPENS.setText(null);
                        editGROSS.setText(Double.valueOf(Math.round(getGROSSFromNET(bv, a, b) * 100) / 100.0).toString());

                    }
                }
                catch(IllegalArgumentException ex)
                {
                    editINCOME.setText("No solution");
                    editPENS.setText("No solution");
                    editGROSS.setText("No solution");
                }
            }
            ignoreEvent=false;

        }
    };

    TextWatcher textWatcherTAX = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();


                double bv = tryParse(txt);
                double g = tryParse2(editGROSS.getText().toString());
                double i = tryParse2(editINCOME.getText().toString());
                double n = tryParse2(editNET.getText().toString());
                double p = tryParse2(editPENS.getText().toString());
                double b = tryParse2(editNONTAX.getText().toString());

                     editINCOME.setText(Double.valueOf(Math.round(getINCOMEFromTAX(bv, g) * 100) / 100.0).toString());

                editNET.setText(Double.valueOf(Math.round(getNETFromTAX(bv, b, p, g) * 100) / 100.0).toString());


            ignoreEvent=false;


        }
    };


    TextWatcher textWatcherNONTAX = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {


            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();

                double bv = tryParse(txt);

                double g = tryParse2(editGROSS.getText().toString());
                double i = tryParse2(editINCOME.getText().toString());
                 double p = tryParse2(editPENS.getText().toString());
                //double n = tryParse2(editNET.getText().toString());
                 double a = tryParse2(editTAX.getText().toString());


                editNET.setText(Double.valueOf(Math.round(getNETFromNONTAX(bv, g, p,i,a) * 100) / 100.0).toString());


            ignoreEvent=false;

        }
    };



    double getINCOMEFromGROSS(double GROSS,double a)
    {

        return INCOMETAX(GROSS+a);

    }
    double getPENSFromGROSS(double GROSS)
    {

        return 0.07*GROSS;
    }
    double getNETFromGROSS(double GROSS,double a,double b)
    {

        return GROSS-INCOMETAX(GROSS+a)+a+b;
    }
    double getNETFromGROSSP(double GROSS,double a ,double b)
    {

        return GROSS-INCOMETAX(GROSS+a)-0.07*GROSS+a+b;
    }






    double getINCOMEFromTAX(double TAX,double g)
    {

        return INCOMETAX(g+TAX);

    }

    double getNETFromTAX(double TAX,double b,double p,double g)
    {

        return g-p-INCOMETAX(g+TAX)+b+TAX;
    }




    double getNETFromNONTAX(double NONTAX,double g,double p,double i,double a)
    {

        return g-p-i+NONTAX+a;
    }






    double getGROSSFromINCOME(double INCOME,double a)
    {
        return INVERSEINCOMETAX(INCOME)-a;
    }
    double getPENSFromINCOME(double INCOME,double a)
    {
        return 0.07*(INVERSEINCOMETAX(INCOME)-a);
    }
    double getNETFromINCOMEP(double INCOME,double a,double b)
    {
        return (INVERSEINCOMETAX(INCOME)-a)-INCOME-(0.07*(INVERSEINCOMETAX(INCOME)-a))+b+a;
    }
    double getNETFromINCOME(double INCOME,double a,double b)
    {
        return (INVERSEINCOMETAX(INCOME)-a)-INCOME+b+a;
    }





    double getGROSSFromPENS(double PENS)
    {
        return PENS/0.07;
    }
    double getINCOMEFromPENS(double PENS,double a)
    {
        return INCOMETAX(PENS/0.07+a);
    }
    double getNETFromPENS(double PENS,double a,double b)
    {
        return PENS/0.07-INCOMETAX(PENS/0.07+a)-PENS+a+b;
    }









    double getGROSSFromNET(double NET,double a,double b) throws IllegalArgumentException
    {

        return inverseNetIncome(NET,a,b)-(inverseNetIncome(NET,a,b)*0.07);
    }
    double getGROSSFromNETP(double NET,double a,double b) throws IllegalArgumentException
    {

        return inverseNetIncome(NET,a,b);
    }

    double getINCOMEFromNET(double NET,double a,double b) throws IllegalArgumentException
    {
        return INCOMETAX((inverseNetIncome(NET,a,b)-(inverseNetIncome(NET,a,b)*0.07)));
    }

    double getINCOMEFromNETP(double NET,double a,double b) throws IllegalArgumentException
    {
        return INCOMETAX(inverseNetIncome(NET,a,b));
    }


    double getPENSFromNETP(double NET,double a,double b) throws IllegalArgumentException
    {
        return inverseNetIncome(NET,a,b)*0.07;
    }


    double netIncome(double g,double a,double b)
    {
        return 0.93*g+a+b-INCOMETAX(g+a);
    }
    double inverseNetIncome(double x,double a,double b) throws IllegalArgumentException
    {
        double y1=a;
        double y2=x+a+b;
        int trials=0;
        while(Math.abs(y1-y2)>0.00001)
        {
            double t1=netIncome(y1,a,b);
            double t2=netIncome(y2,a,b);
            double y=(y2-y1)*(x-t1)/(t2-t1)+y1;
            if(Math.abs(y-y1)>Math.abs(y-y2))
                y1=y;
            else
                y2=y;
            trials++;
            if(trials>100)
                throw new IllegalArgumentException("No solution");
        }
        return y1;
    }


    double INCOMETAX(double GROSS){

        if(GROSS>=0 && GROSS<=600) {
            return 0;
        }
        else if(GROSS>600 && GROSS<=1650) {
            return 0.1*GROSS-60;
        }


        else if(GROSS>1650 && GROSS<=3200) {

            return 0.15 * GROSS-142.5;
        }

        else if(GROSS>3200 && GROSS<=5250) {

            return 0.2 * GROSS-302.5;

        }
        else if(GROSS>5250 && GROSS<=7800) {
            return 0.25 * GROSS-565;
        }

        else if(GROSS>7800 && GROSS<=10900) {
            return 0.3 * GROSS-955;
        }
        else {
            return 0.35 * GROSS-1500;

        }
    }



    double INVERSEINCOMETAX(double mo){
        if(mo ==0) {
            return 0;

        }
        else if (mo >0 && mo <=105){
            return mo/0.1+600;
        }
        else if(mo>105 && mo<=337.5){
            return mo/0.15+950;

        }
        else if(mo>337.5 && mo<=747.5){
            return mo/0.2+1512.5;

        }
        else if(mo>747.5 && mo<1385){
            return mo/0.25+2260;

        }
        else if(mo>1385 && mo<= 2315){
            return mo/0.3+955/0.3;

        }
        else if(mo>2315){
            return mo/0.35+1500/0.35;
        }
        else{
            return 0;
        }



    }


}



