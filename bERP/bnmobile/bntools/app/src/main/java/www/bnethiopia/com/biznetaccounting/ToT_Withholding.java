package www.bnethiopia.com.biznetaccounting;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

import org.w3c.dom.Text;

public class ToT_Withholding extends AppCompatActivity {
    private EditText editBT,edittot2,edittot10,editwith,editAT2,editAT10,editatot2,editatot10;

    protected boolean ignoreEvent=false;
    double tryParse(String txt) {
        try {

            return Double.parseDouble(txt);
        }
        catch (Exception ex)
        {
            return  0.0;
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_t__withholding);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        editBT = (EditText) findViewById(R.id.editText);
        edittot2 = (EditText) findViewById(R.id.editText2);
        edittot10 = (EditText) findViewById(R.id.editText245);
        editwith = (EditText) findViewById(R.id.editText3);
        editAT2 = (EditText) findViewById(R.id.editText4);
        editAT10 = (EditText) findViewById(R.id.editText42);
        editatot2 = (EditText) findViewById(R.id.editText32);
        editatot10 =(EditText) findViewById(R.id.editText2435);

        findViewById(R.id.contentone34).setVisibility(View.GONE);
        findViewById(R.id.contentone).setVisibility(View.VISIBLE);

        findViewById(R.id.contentone1).setVisibility(View.VISIBLE);
        findViewById(R.id.contentone345).setVisibility(View.GONE);

        findViewById(R.id.content_three).setVisibility(View.VISIBLE);
        findViewById(R.id.content_three1).setVisibility(View.GONE);

        editBT.addTextChangedListener(textWatcherBT);
        edittot2.addTextChangedListener(textWatchertot2);
        edittot10.addTextChangedListener(textWatchertot10);
        editwith.addTextChangedListener(textWatcherwith);
        editAT2.addTextChangedListener(textWatcherAT2);
        editAT10.addTextChangedListener(textWatcherAT10);
        editatot2.addTextChangedListener(textWatcheratot2);
        editatot10.addTextChangedListener(textWatcheratot10);




        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }




    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();


        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioButton2:
                if (checked)
                    // Pirates are the best
                    findViewById(R.id.contentone34).setVisibility(View.GONE);
                findViewById(R.id.contentone).setVisibility(View.VISIBLE);

                findViewById(R.id.contentone1).setVisibility(View.VISIBLE);
                findViewById(R.id.contentone345).setVisibility(View.GONE);

                findViewById(R.id.content_three).setVisibility(View.VISIBLE);
                findViewById(R.id.content_three1).setVisibility(View.GONE);
                    break;
            case R.id.radioButton10:
                if (checked)
                    // Ninjas rule

                    findViewById(R.id.contentone1).setVisibility(View.GONE);
                findViewById(R.id.contentone345).setVisibility(View.VISIBLE);

                    findViewById(R.id.contentone).setVisibility(View.GONE);
                findViewById(R.id.contentone34).setVisibility(View.VISIBLE);
                findViewById(R.id.content_three).setVisibility(View.GONE);
                findViewById(R.id.content_three1).setVisibility(View.VISIBLE);

                    break;



        }
    }


    TextWatcher textWatcherBT  = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {


            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                edittot2.setText("");
                edittot10.setText("");
                editwith.setText("");
                editAT2.setText("");
                editAT10.setText("");

                editatot2.setText("");
                editatot10.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                edittot2.setText(Double.valueOf(Math.round(getTOT2FromBT(bv)*100)/100.0).toString());
                edittot10.setText(Double.valueOf(Math.round(getTOT10FromBT(bv)*100)/100.0).toString());
                editwith.setText(Double.valueOf(Math.round(getWITHFromBT(bv)*100)/100.0).toString());
                editAT2.setText(Double.valueOf(Math.round(getAT2FromBT(bv)*100)/100.0).toString());
                editAT10.setText(Double.valueOf(Math.round(getAT10FromBT(bv)*100)/100.0).toString());
                editatot2.setText(Double.valueOf(Math.round(getATOT2FromBT(bv)*100)/100.0).toString());
                editatot10.setText(Double.valueOf(Math.round(getATOT10FromBT(bv)*100)/100.0).toString());


            }
            ignoreEvent=false;


        }
    };


    TextWatcher textWatchertot2 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {



            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                editBT.setText("");
                edittot10.setText("");
                editwith.setText("");
                editAT2.setText("");
                editAT10.setText("");
                editatot2.setText("");
                editatot10.setText("");


            }
            else

            {
                double bv=tryParse(txt);
                editBT.setText(Double.valueOf(Math.round(getBTFromTOT2(bv)*100)/100.0).toString());
                edittot10.setText(Double.valueOf(Math.round(getTOT10FromTOT2(bv)*100)/100.0).toString());
                editwith.setText(Double.valueOf(Math.round(getWITHFromTOT2(bv)*100)/100.0).toString());
                editAT2.setText(Double.valueOf(Math.round(getAT2FromTOT2(bv)*100)/100.0).toString());
                editAT10.setText(Double.valueOf(Math.round(getAT10FromTOT2(bv)*100)/100.0).toString());
                editatot2.setText(Double.valueOf(Math.round(getATOT2FromTOT2(bv)*100)/100.0).toString());
                editatot10.setText(Double.valueOf(Math.round(getATOT10FromTOT2(bv)*100)/100.0).toString());

            }
            ignoreEvent=false;

        }
    };


    TextWatcher textWatchertot10 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {




            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                editBT.setText("");
                edittot2.setText("");
                editwith.setText("");
                editAT2.setText("");
                editAT10.setText("");
                editatot2.setText("");
                editatot10.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                editBT.setText(Double.valueOf(Math.round(getBTFromTOT10(bv)*100)/100.0).toString());
                edittot2.setText(Double.valueOf(Math.round(getTOT2FromTOT10(bv)*100)/100.0).toString());
                editwith.setText(Double.valueOf(Math.round(getWITHFromTOT10(bv)*100)/100.0).toString());
                editAT2.setText(Double.valueOf(Math.round(getAT2FromTOT10(bv)*100)/100.0).toString());
                editAT10.setText(Double.valueOf(Math.round(getAT10FromTOT10(bv)*100)/100.0).toString());
                editatot2.setText(Double.valueOf(Math.round(getATOT2FromTOT10(bv)*100)/100.0).toString());
                editatot10.setText(Double.valueOf(Math.round(getATOT10FromTOT10(bv)*100)/100.0).toString());

            }
            ignoreEvent=false;

        }
    };


    TextWatcher textWatcherwith = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {




            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                editBT.setText("");
                edittot2.setText("");
                edittot10.setText("");
                editAT2.setText("");
                editAT10.setText("");
                editatot2.setText("");
                editatot10.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                editBT.setText(Double.valueOf(Math.round(getBTFromWITH(bv)*100)/100.0).toString());
                edittot2.setText(Double.valueOf(Math.round(getTOT2FromWITH(bv)*100)/100.0).toString());
                edittot10.setText(Double.valueOf(Math.round(getTOT10FromWITH(bv)*100)/100.0).toString());
                editAT2.setText(Double.valueOf(Math.round(getAT2FromWITH(bv)*100)/100.0).toString());
                editAT10.setText(Double.valueOf(Math.round(getAT10FromWITH(bv)*100)/100.0).toString());
                editatot2.setText(Double.valueOf(Math.round(getATOT2FromWITH(bv)*100)/100.0).toString());
                editatot10.setText(Double.valueOf(Math.round(getATOT10FromWITH(bv)*100)/100.0).toString());

            }
            ignoreEvent=false;


        }
    };


    TextWatcher textWatcherAT2 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {


            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                editBT.setText("");
                edittot2.setText("");
                edittot10.setText("");
                editwith.setText("");
                editAT10.setText("");
                edittot2.setText("");
                editatot10.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                editBT.setText(Double.valueOf(Math.round(getBTFromAT2(bv)*100)/100.0).toString());
                edittot2.setText(Double.valueOf(Math.round(getTOT2FromAT2(bv)*100)/100.0).toString());
                edittot10.setText(Double.valueOf(Math.round(getTOT10FromAT2(bv)*100)/100.0).toString());
                editwith.setText(Double.valueOf(Math.round(getWITHFromAT2(bv)*100)/100.0).toString());
               editAT10.setText(Double.valueOf(Math.round(getAT10FromAT2(bv)*100)/100.0).toString());
                editatot2.setText(Double.valueOf(Math.round(getATOT2FromAT2(bv)*100)/100.0).toString());
                editatot10.setText(Double.valueOf(Math.round(getATOT10FromAT2(bv)*100)/100.0).toString());


            }
            ignoreEvent=false;



        }
    };

    TextWatcher textWatcherAT10 =new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {


            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                editBT.setText("");
                edittot2.setText("");
                edittot10.setText("");
                editwith.setText("");
                editAT2.setText("");
                editatot2.setText("");
                editatot10.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                editBT.setText(Double.valueOf(Math.round(getBTFromAT10(bv)*100)/100.0).toString());
                edittot2.setText(Double.valueOf(Math.round(getTOT2FromAT10(bv)*100)/100.0).toString());
                edittot10.setText(Double.valueOf(Math.round(getTOT10FromAT10(bv)*100)/100.0).toString());
                editwith.setText(Double.valueOf(Math.round(getWITHFromAT10(bv)*100)/100.0).toString());
                editAT2.setText(Double.valueOf(Math.round(getAT2FromAT10(bv)*100)/100.0).toString());
                editatot2.setText(Double.valueOf(Math.round(getATOT2FromAT10(bv)*100)/100.0).toString());
                editatot10.setText(Double.valueOf(Math.round(getATOT10FromAT10(bv)*100)/100.0).toString());


            }
            ignoreEvent=false;


        }
    };


    TextWatcher textWatcheratot2 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                editBT.setText("");
                edittot2.setText("");
                edittot10.setText("");
                editwith.setText("");
                editAT2.setText("");
                editAT10.setText("");
                editatot10.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                editBT.setText(Double.valueOf(Math.round(getBTFromATOT2(bv)*100)/100.0).toString());
                edittot2.setText(Double.valueOf(Math.round(getTOT2FromATOT2(bv)*100)/100.0).toString());
                edittot10.setText(Double.valueOf(Math.round(getTOT10FromATOT2(bv)*100)/100.0).toString());
                editwith.setText(Double.valueOf(Math.round(getWITHFromATOT2(bv)*100)/100.0).toString());
                editAT2.setText(Double.valueOf(Math.round(getAT2FromATOT2(bv)*100)/100.0).toString());
                editAT10.setText(Double.valueOf(Math.round(getAT10FromATOT2(bv)*100)/100.0).toString());
                editatot10.setText(Double.valueOf(Math.round(getATOT10FromATOT2(bv)*100)/100.0).toString());


            }
            ignoreEvent=false;



        }
    };


    TextWatcher textWatcheratot10 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                editBT.setText("");
                edittot2.setText("");
                edittot10.setText("");
                editwith.setText("");
                editAT2.setText("");
                editatot2.setText("");
                editAT10.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                editBT.setText(Double.valueOf(Math.round(getBTFromATOT10(bv)*100)/100.0).toString());
                edittot2.setText(Double.valueOf(Math.round(getTOT2FromATOT10(bv)*100)/100.0).toString());
                edittot10.setText(Double.valueOf(Math.round(getTOT10FromATOT10(bv)*100)/100.0).toString());
                editwith.setText(Double.valueOf(Math.round(getWITHFromATOT10(bv)*100)/100.0).toString());
                editAT2.setText(Double.valueOf(Math.round(getAT2FromATOT10(bv)*100)/100.0).toString());
                editatot2.setText(Double.valueOf(Math.round(getATOT2FromATOT10(bv)*100)/100.0).toString());
                editAT10.setText(Double.valueOf(Math.round(getAT10FromATOT10(bv)*100)/100.0).toString());


            }
            ignoreEvent=false;




        }
    };

    double getTOT2FromBT(double BT)
    {
        return 0.02*BT;
    }
    double getTOT10FromBT(double BT)
    {
        return 0.1*BT;
    }
    double getWITHFromBT(double BT)
    {
        return 0.02*BT;
    }
    double getATOT2FromBT(double BT)
    {
        return 1.02*BT;
    }
    double getATOT10FromBT(double BT)
    {
        return 1.1*BT;
    }

    double getAT2FromBT(double BT)
    {
        return BT;
    }
    double getAT10FromBT(double BT)
    {
        return 1.08*BT;
    }






    double getBTFromTOT2(double TOT2)
    {
        return 50*TOT2;
    }
    double getATOT2FromTOT2(double TOT2)
    {
        return 51*TOT2;
    }
    double getTOT10FromTOT2(double TOT2)
    {
        return 5*TOT2;
    }
    double getATOT10FromTOT2(double TOT2)
    {
        return TOT2*55;
    }

    double getWITHFromTOT2(double TOT2)
    {
        return TOT2;
    }

    double getAT2FromTOT2(double TOT2)
    {
        return 50*TOT2;
    }
    double getAT10FromTOT2(double TOT2)
    {
        return 54*TOT2;
    }





    double getBTFromTOT10(double TOT10)
    {
        return 10*TOT10;
    }
    double getATOT10FromTOT10(double TOT10)
    {
        return TOT10*11;
    }
    double getTOT2FromTOT10(double TOT10)
    {
        return 0.2*TOT10;
    }
    double getATOT2FromTOT10(double TOT10)
    {
        return TOT10*10.2;
    }
    double getWITHFromTOT10(double TOT10)
    {
        return 0.2*TOT10;
    }

    double getAT2FromTOT10(double TOT10)
    {
        return 10*TOT10;
    }

    double getAT10FromTOT10(double TOT10)
    {
        return 10.8*TOT10;
    }





    double getBTFromWITH(double WITH)
    {
        return 50*WITH;
    }
    double getTOT2FromWITH(double WITH)
    {
        return WITH;
    }
    double getATOT2FromWITH(double WITH)
    {
        return 51*WITH;
    }
    double getTOT10FromWITH(double WITH)
    {
        return 5*WITH;
    }
    double getATOT10FromWITH(double WITH)
    {
        return 55*WITH;
    }

    double getAT2FromWITH(double WITH)
    {
        return 50*WITH;
    }
    double getAT10FromWITH(double WITH)
    {
        return 54*WITH;
    }






    double getBTFromAT2(double AT)
    {
        return AT;
    }
    double getTOT2FromAT2(double AT)
    {
        return 0.02*AT;
    }
    double getATOT2FromAT2(double AT)
    {
        return 1.02*AT;
    }
    double getTOT10FromAT2(double AT)
    {
        return 0.1*AT;
    }
    double getATOT10FromAT2(double AT)
    {
        return 1.1*AT;
    }
    double getWITHFromAT2(double AT)
    {
        return 0.02*AT;
    }
    double getAT10FromAT2(double AT)
    {
        return 1.08*AT;
    }




    double getBTFromAT10(double AT)
    {
        return AT/1.08;
    }
    double getTOT2FromAT10(double AT)
    {
        return (0.02*AT)/1.08;
    }
    double getATOT2FromAT10(double AT)
    {
        return (1.02*AT)/1.08;
    }
    double getTOT10FromAT10(double AT)
    {
        return (0.1*AT)/1.08;
    }
    double getATOT10FromAT10(double AT)
    {
        return (1.1*AT)/1.08;
    }
    double getWITHFromAT10(double AT)
    {
        return (0.02*AT)/1.08;
    }

    double getAT2FromAT10(double AT)
    {
        return AT/1.08;
    }



    double getBTFromATOT2(double ATOT2)
    {
        return ATOT2/1.02;
    }
    double getTOT2FromATOT2(double ATOT2)
    {
        return (0.02*ATOT2)/1.02;
    }
    double getAT10FromATOT2(double ATOT2)
    {
        return (1.08*ATOT2)/1.02;
    }
    double getTOT10FromATOT2(double ATOT2)
    {
        return (0.1*ATOT2)/1.02;
    }
    double getATOT10FromATOT2(double ATOT2)
    {
        return (1.1*ATOT2)/1.02;
    }
    double getWITHFromATOT2(double ATOT2)
    {
        return (0.02*ATOT2)/1.02;
    }

    double getAT2FromATOT2(double ATOT2)
    {
        return ATOT2/1.02;
    }



    double getBTFromATOT10(double ATOT10)
    {
        return ATOT10/1.1;
    }
    double getTOT2FromATOT10(double ATOT10)
    {
        return (0.2*ATOT10)/11;
    }
    double getATOT2FromATOT10(double ATOT10)
    {
        return (10.2*ATOT10)/11;
    }
    double getTOT10FromATOT10(double ATOT10)
    {
        return ATOT10/11;
    }
    double getAT10FromATOT10(double ATOT10)
    {
        return (1.08*ATOT10)/1.1;
    }
    double getWITHFromATOT10(double ATOT10)
    {
        return (0.2*ATOT10)/11;
    }

    double getAT2FromATOT10(double ATOT10)
    {
        return ATOT10/1.1;
    }







    public void totwitholding(View view){
      //  Intent i = new Intent(ToT_Withholding.this,BizNet.class);
       // startActivity(i);
        Intent intent = new Intent(this, BizNet.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityIfNeeded(intent, 0);
    }

}
