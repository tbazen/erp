package www.bnethiopia.com.biznetaccounting;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import java.text.DecimalFormat;

public class VAT_Calculator extends AppCompatActivity {

    EditText editBV,editVAT,editAV;

    private boolean ignoreEvent=false;
    double tryParse(String txt1) {
        try {

            return Double.parseDouble(txt1);
        } catch (Exception ex) {
            return 0.0;
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vat__calculator);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        editBV = (EditText) findViewById(R.id.editBV);
        editVAT = (EditText) findViewById(R.id.editVAT);
        editAV = (EditText) findViewById(R.id.editAV);

        editBV.addTextChangedListener(textWatcherBV);
        editAV.addTextChangedListener(textWatcherAV);
        editVAT.addTextChangedListener(textWatcherVAT);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }




    TextWatcher textWatcherBV = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                editVAT.setText("");
                editAV.setText("");
            }
            else

            {
                double bv=tryParse(txt);
                editVAT.setText(Double.valueOf(Math.round(getVATFromBV(bv)*100)/100.0).toString());
                editAV.setText(Double.valueOf(Math.round(getAVFromBV(bv)*100)/100.0).toString());

            }
            ignoreEvent=false;

        }
    };


    TextWatcher textWatcherVAT = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                editBV.setText("");
                editAV.setText("");
            }
            else

            {

                double bv=tryParse(txt);
                editBV.setText(Double.valueOf(Math.round(getBVFromVAT(bv)*100)/100.0).toString());
                editAV.setText(Double.valueOf(Math.round(getAVFromVAT(bv)*100)/100.0).toString());


            }
            ignoreEvent=false;

        }
    };

    TextWatcher textWatcherAV = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {


            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                editBV.setText("");
                editVAT.setText("");
            }
            else

            {
                //   DecimalFormat formatter = new DecimalFormat("#,###.00");
                // String txt1  = formatter.format(txt);
                double bv=tryParse(txt);

                editBV.setText(Double.valueOf(Math.round((getBVFromAV(bv))*100)/100.0).toString());
                editVAT.setText(Double.valueOf(Math.round((getVATFromAV(bv))*100)/100.0).toString());

            }
            ignoreEvent=false;
        }
    };


    Double getVATFromBV(double BV)
    {

        return BV*0.15;
    }
    Double getAVFromBV(double BV)
    {

        return BV*1.15;

    }
    Double getBVFromVAT(double VAT)
    {

        return VAT/0.15;

    }
    Double getAVFromVAT(double VAT)
    {

        return VAT*1.15/0.15;

    }

    Double getBVFromAV(double AV)
    {

        return AV/1.15;

    }
    Double getVATFromAV(double AV)
    {

        return AV*0.15/1.15;

    }
    public void vat(View view){
        //Intent i = new Intent(VAT_Calculator.this,BizNet.class);
        //startActivity(i);
        Intent intent = new Intent(this, BizNet.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityIfNeeded(intent, 0);

    }

}



