package www.bnethiopia.com.biznetaccounting;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

public class VAT_WITHHOLDING_Calculator extends AppCompatActivity {


    private EditText editBT,editWITH,editVAT,editAT,editAVAT;

    protected boolean ignoreEvent=false;
    double tryParse(String txt) {
        try {

            return Double.parseDouble(txt);
        }
        catch (Exception ex)
        {
            return  0.0;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vat__withholding__calculator);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();


        editBT = (EditText) findViewById(R.id.editBT);
        editWITH = (EditText) findViewById(R.id.editWITH);
        editVAT = (EditText) findViewById(R.id.editVAT);
        editAT = (EditText) findViewById(R.id.editAT);
        editAVAT =(EditText) findViewById(R.id.editVAT2);

        editBT.addTextChangedListener(textWatcherBT);
        editWITH.addTextChangedListener(textWatcherWITH);
        editVAT.addTextChangedListener(textWatcherVAT);
        editAT.addTextChangedListener(textWatcherAF);
        editAVAT.addTextChangedListener(textWatcherAVAT);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    TextWatcher textWatcherBT = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                editWITH.setText("");
                editVAT.setText("");
                editAT.setText("");
                editAVAT.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                editWITH.setText(Double.valueOf(Math.round(getWITHFromBT(bv)*100)/100.0).toString());
                editVAT.setText(Double.valueOf(Math.round(getVATFromBT(bv)*100)/100.0).toString());
                editAT.setText(Double.valueOf(Math.round(getATFromBT(bv)*100)/100.0).toString());
                editAVAT.setText(Double.valueOf(Math.round(getAVATFromBT(bv)*100)/100.0).toString());
            }
            ignoreEvent=false;
        }


    };


    TextWatcher textWatcherWITH = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                editBT.setText("");
                editVAT.setText("");
                editAT.setText("");
                editAVAT.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                editBT.setText(Double.valueOf(Math.round(getBTFromWITH(bv)*100)/100.0).toString());
                editVAT.setText(Double.valueOf(Math.round(getVATFromWITH(bv)*100)/100.0).toString());
                editAT.setText(Double.valueOf(Math.round(getATFromWITH(bv)*100)/100.0).toString());
                editAVAT.setText(Double.valueOf(Math.round(getAVATFromWITH(bv)*100)/100.0).toString());
            }
            ignoreEvent=false;



        }
    };

    TextWatcher textWatcherVAT = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                editWITH.setText("");
                editBT.setText("");
                editAT.setText("");
                editAVAT.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                editWITH.setText(Double.valueOf(Math.round(getWITHFromVAT(bv)*100)/100.0).toString());
                editBT.setText(Double.valueOf(Math.round(getBTFromVAT(bv)*100)/100.0).toString());
                editAT.setText(Double.valueOf(Math.round(getATFromVAT(bv)*100)/100.0).toString());
                editAVAT.setText(Double.valueOf(Math.round(getAVATFromVAT(bv)*100)/100.0).toString());
            }
            ignoreEvent=false;


        }
    };


    TextWatcher textWatcherAF = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {


            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                editWITH.setText("");
                editVAT.setText("");
                editBT.setText("");
                editAVAT.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                editWITH.setText(Double.valueOf(Math.round(getWITHFromAT(bv)*100)/100.0).toString());
                editVAT.setText(Double.valueOf(Math.round(getVATFromAT(bv)*100)/100.0).toString());
                editBT.setText(Double.valueOf(Math.round(getBTFromAT(bv)*100)/100.0).toString());
                editAVAT.setText(Double.valueOf(Math.round(getAVATFromAT(bv)*100)/100.0).toString());
            }
            ignoreEvent=false;

        }
    };

    TextWatcher textWatcherAVAT = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {



            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {
                editWITH.setText("");
                editVAT.setText("");
                editBT.setText("");
                editAT.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                editWITH.setText(Double.valueOf(Math.round(getWITHFromAVAT(bv)*100)/100.0).toString());
                editVAT.setText(Double.valueOf(Math.round(getVATFromAVAT(bv)*100)/100.0).toString());
                editBT.setText(Double.valueOf(Math.round(getBTFromAVAT(bv)*100)/100.0).toString());
                editAT.setText(Double.valueOf(Math.round(getATFromAVAT(bv)*100)/100.0).toString());
            }
            ignoreEvent=false;


        }
    };



    double getVATFromBT(double BT)
    {
        return BT*0.15;
    }
    double getATFromBT(double BT)
    {
        return 1.13*BT;
    }
    double getWITHFromBT(double BT)
    {
        return 0.02*BT;
    }
    double getAVATFromBT(double BT)
    {
        return BT*1.15;
    }




    double getBTFromVAT(double VAT)
    {
        return VAT/0.15;
    }
    double getATFromVAT(double VAT)
    {
        return (1.13*VAT)/0.15;
    }
    double getWITHFromVAT(double VAT)
    {
        return (0.02*VAT)/0.15;
    }
    double getAVATFromVAT(double VAT)
    {
        return (VAT*1.15)/0.15;
    }


    double getBTFromAVAT(double AVAT)
    {
        return AVAT/1.15;
    }
    double getVATFromAVAT(double AVAT)
    {
        return (AVAT*0.15)/1.15;
    }
    double getWITHFromAVAT(double AVAT)
    {
        return (AVAT*0.02)/1.15;
    }
    double getATFromAVAT(double AVAT)
    {
        return (AVAT*1.13)/1.15;
    }



    double getBTFromAT(double AT)
    {
        return AT/1.13;
    }
    double getVATFromAT(double AT)
    {
        return (AT*0.15)/1.13;
    }
    double getWITHFromAT(double AT)
    {
        return (AT*0.02)/1.13;
    }
    double getAVATFromAT(double AT)
    {
        return (AT*1.15)/1.13;
    }




    double getBTFromWITH(double WITH)
    {
        return WITH/0.02;
    }
    double getVATFromWITH(double WITH)
    {
        return 7.5*WITH;
    }
    double getATFromWITH(double WITH)
    {
        return (WITH*1.13)/0.02;
    }
    double getAVATFromWITH(double WITH)
    {
        return (WITH*1.15)/0.02;
    }


public void vat_withholding1(View view){
   // Intent i = new Intent(VAT_WITHHOLDING_Calculator.this,BizNet.class);
    //startActivity(i);

    Intent intent = new Intent(this, BizNet.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    startActivityIfNeeded(intent, 0);
}


}



