package www.bnethiopia.com.biznetaccounting;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class WITHHOLDING_Calculator extends AppCompatActivity {

    private EditText editBWITH,editWITH2,editWITH10,editAWITH2,editAWITH10;

    private boolean ignoreEvent=false;
    double tryParse(String txt) {
        try {

            return Double.parseDouble(txt);
        }
        catch (Exception ex)
        {
            return  0.0;
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withholding__calculator);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        editAWITH2 = (EditText) findViewById(R.id.editAWITH);
        editWITH2 = (EditText) findViewById(R.id.editWITH);
        editBWITH = (EditText)findViewById(R.id.editBWITH);
        editWITH10 = (EditText) findViewById(R.id.editText3);
        editAWITH10 = (EditText) findViewById(R.id.editAWITHfor10);


        findViewById(R.id.content_two).setVisibility(View.GONE);
        findViewById(R.id.content_four).setVisibility(View.GONE);



        findViewById(R.id.contentone).setVisibility(View.VISIBLE);
        findViewById(R.id.content_three).setVisibility(View.VISIBLE);

        editBWITH.addTextChangedListener(textWatcherBWITH);
        editWITH2.addTextChangedListener(textWatcherWITH2);
        editAWITH2.addTextChangedListener(textWatcherAWITH2);
        editWITH10.addTextChangedListener(textWatcherWITH30);
        editAWITH10.addTextChangedListener(textWatcherAWITH30);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioButton2:
                if (checked)
                    // Pirates are the best

                    findViewById(R.id.content_two).setVisibility(View.GONE);
                findViewById(R.id.content_four).setVisibility(View.GONE);



                findViewById(R.id.contentone).setVisibility(View.VISIBLE);
                findViewById(R.id.content_three).setVisibility(View.VISIBLE);
                    break;
            case R.id.radioButton10:
                if (checked)
                    // Ninjas rule
                    findViewById(R.id.content_two).setVisibility(View.VISIBLE);
                findViewById(R.id.content_four).setVisibility(View.VISIBLE);

                findViewById(R.id.contentone).setVisibility(View.GONE);
                findViewById(R.id.content_three).setVisibility(View.GONE);

                    break;
        }
    }





    TextWatcher textWatcherBWITH = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {

                editWITH2.setText("");
                editAWITH2.setText("");
                editWITH10.setText("");
                editAWITH10.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                editWITH2.setText(Double.valueOf(Math.round(getWITH2FromBWITH(bv)*100)/100.0).toString());
                editAWITH2.setText(Double.valueOf(Math.round(getAWITH2FromBWITH(bv)*100)/100.0).toString());
                editWITH10.setText(Double.valueOf(Math.round(getWITH30FromBWITH(bv)*100)/100.0).toString());
                editAWITH10.setText(Double.valueOf(Math.round(getAWITH30FromBWITH(bv)*100)/100.0).toString());

            }
            ignoreEvent=false;
        }
    };
    TextWatcher textWatcherWITH2 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {

                editBWITH.setText("");
                editAWITH2.setText("");
                editWITH10.setText("");
                editAWITH10.setText("");

            }
            else

            {
                double bv=tryParse(txt);

                editBWITH.setText(Double.valueOf(Math.round(getBWITHFromWITH2(bv)*100)/100.0).toString());
                editAWITH2.setText(Double.valueOf(Math.round(getAWITH2FromWITH2(bv)*100)/100.0).toString());
                editAWITH10.setText(Double.valueOf(Math.round(getAWITH30FromWITH2(bv)*100)/100.0).toString());
                editWITH10.setText(Double.valueOf(Math.round(getWITH30FromWITH2(bv)*100)/100.0).toString());
            }
            ignoreEvent=false;
        }
    };

    TextWatcher textWatcherAWITH2 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {

                editWITH2.setText("");
                editBWITH.setText("");
                editWITH10.setText("");
                editAWITH10.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                editWITH2.setText(Double.valueOf(Math.round(getWITH2FromAWITH2(bv)*100)/100.0).toString());
                editBWITH.setText(Double.valueOf(Math.round(getBWITHFromAWITH2(bv)*100)/100.0).toString());
                editAWITH10.setText(Double.valueOf(Math.round(getAWITH30FromAWITH2(bv)*100)/100.0).toString());
                editWITH10.setText(Double.valueOf(Math.round(getWITH30FromAWITH2(bv)*100)/100.0).toString());


            }
            ignoreEvent=false;

        }
    };

    TextWatcher textWatcherWITH30 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {

                editBWITH.setText("");
                editAWITH2.setText("");
                editAWITH2.setText("");
                editAWITH10.setText("");

            }
            else

            {
                double bv=tryParse(txt);

                editBWITH.setText(Double.valueOf(Math.round(getBWITHFromWITH30(bv)*100)/100.0).toString());
                editAWITH10.setText(Double.valueOf(Math.round(getAWITH30FromWITH30(bv)*100)/100.0).toString());
                editAWITH2.setText(Double.valueOf(Math.round(getAWITH2FromWITH30(bv)*100)/100.0).toString());
                editWITH2.setText(Double.valueOf(Math.round(getWITH2FromWITH30(bv)*100)/100.0).toString());
            }
            ignoreEvent=false;
        }
    };

    TextWatcher textWatcherAWITH30 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {

                editBWITH.setText("");
                editAWITH2.setText("");
                editWITH10.setText("");
                editWITH2.setText("");

            }
            else

            {
                double bv=tryParse(txt);

                editBWITH.setText(Double.valueOf(Math.round(getBWITHFromAWITH30(bv)*100)/100.0).toString());
                editWITH10.setText(Double.valueOf(Math.round(getWITH30FromAWITH30(bv)*100)/100.0).toString());
                editAWITH2.setText(Double.valueOf(Math.round(getAWITH2FromAWITH30(bv)*100)/100.0).toString());
                editWITH2.setText(Double.valueOf(Math.round(getWITH2FromAWITH30(bv)*100)/100.0).toString());
            }
            ignoreEvent=false;
        }
    };
    double getWITH2FromBWITH(double BWITH)
    {
        return 0.02*BWITH;
    }
    double getAWITH2FromBWITH(double BWITH)
    {
        return 0.98*BWITH;
    }
    double getWITH30FromBWITH(double BWITH)
    {
        return 3*BWITH/10;
    }
    double getAWITH30FromBWITH(double BWITH)
    {
        return 7*BWITH/10;
    }



    double getBWITHFromWITH2(double WITH2)
    {
        return WITH2/0.02;
    }
    double getAWITH2FromWITH2(double WITH2)
    {
        return 49*WITH2;
    }
    double getWITH30FromWITH2(double WITH2)
    {
        return WITH2*15;
    }
    double getAWITH30FromWITH2(double WITH2)
    {
        return WITH2*35;
    }


    double getBWITHFromWITH30(double WITH30)
    {
        return WITH30*10/3;
    }
    double getAWITH30FromWITH30(double WITH30)
    {
        return WITH30*7/3;
    }
    double getAWITH2FromWITH30(double WITH30)
    {
        return WITH30*9.8/3;
    }
    double getWITH2FromWITH30(double WITH30)
    {
        return WITH30*0.2/3;
    }





    double getWITH2FromAWITH2(double AWITH2)
    {
        return 0.02*AWITH2/0.98;
    }

    double getBWITHFromAWITH2(double AWITH2)
    {
        return AWITH2/0.98;
    }
    double getWITH30FromAWITH2(double AWITH2)
    {
        return AWITH2*3/9.8;
    }
    double getAWITH30FromAWITH2(double AWITH2)
    {
        return AWITH2*7/9.8;
    }


    double getWITH30FromAWITH30(double AWITH30)
    {
        return AWITH30*3/7;
    }
    double getBWITHFromAWITH30(double AWITH30)
    {
        return AWITH30*10/7;
    }
    double getWITH2FromAWITH30(double AWITH30)
    {
        return  AWITH30*0.2/7;
    }

    double getAWITH2FromAWITH30(double AWITH30)
    {
        return  AWITH30*1.4;
    }


public  void witholding11(View view){
   // Intent i = new Intent(WITHHOLDING_Calculator.this,BizNet.class);
    //startActivity(i);
    Intent intent = new Intent(this, BizNet.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    startActivityIfNeeded(intent, 0);
}
}



