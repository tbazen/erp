package www.bnethiopia.com.biznetaccounting;

import android.content.Intent;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.RadioButton;

public class tot extends AppCompatActivity {

    private EditText editBtot,edittot2,edittot10,editAtot2,editAtot10;


    private boolean ignoreEvent=false;
    double tryParse(String txt) {
        try {

            return Double.parseDouble(txt);
        }
        catch (Exception ex)
        {
            return  0.0;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tot);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        editBtot = (EditText) findViewById(R.id.editText);
        edittot2 = (EditText) findViewById(R.id.editText22);
        edittot10 = (EditText)findViewById(R.id.editText2);
        editAtot2 = (EditText) findViewById(R.id.editText333);
        editAtot10 = (EditText) findViewById(R.id.editText3);

        findViewById(R.id.contentone1).setVisibility(View.GONE);
        findViewById(R.id.content_two1).setVisibility(View.GONE);
        findViewById(R.id.contentone).setVisibility(View.VISIBLE);
        findViewById(R.id.content_two).setVisibility(View.VISIBLE);

        editBtot.addTextChangedListener(textWatcherBtot);
        edittot2.addTextChangedListener(textWatchertot2);
        edittot10.addTextChangedListener(textWatchertot10);
        editAtot2.addTextChangedListener(textWatcherAtot2);
        editAtot10.addTextChangedListener(textWatcherAtot10);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioButton2:


                if (checked)
                    // Pirates are the best
                    findViewById(R.id.contentone1).setVisibility(View.GONE);
                findViewById(R.id.content_two1).setVisibility(View.GONE);



                findViewById(R.id.contentone).setVisibility(View.VISIBLE);
                findViewById(R.id.content_two).setVisibility(View.VISIBLE);

                    break;
            case R.id.radioButton10:
                if (checked)


                    findViewById(R.id.contentone).setVisibility(View.GONE);
                findViewById(R.id.content_two).setVisibility(View.GONE);



                findViewById(R.id.contentone1).setVisibility(View.VISIBLE);
                findViewById(R.id.content_two1).setVisibility(View.VISIBLE);
                    // Ninjas rule
                    break;
        }
    }


    TextWatcher textWatcherBtot = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {


            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {

                edittot2.setText("");
                edittot10.setText("");
                editAtot2.setText("");
                editAtot10.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                edittot2.setText(Double.valueOf(Math.round(getTOT2FromBTOT(bv)*100)/100.0).toString());
                edittot10.setText(Double.valueOf(Math.round(getTOT10FromBTOT(bv)*100)/100.0).toString());
                editAtot2.setText(Double.valueOf(Math.round(getATOT2FromBTOT(bv)*100)/100.0).toString());
                editAtot10.setText(Double.valueOf(Math.round(getATOT10FromBTOT(bv)*100)/100.0).toString());

            }
            ignoreEvent=false;

        }
    };


    TextWatcher textWatchertot2 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {

                editBtot.setText("");
                edittot10.setText("");
                editAtot2.setText("");
                editAtot10.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                editBtot.setText(Double.valueOf(Math.round(getBTOTFromTOT2(bv)*100)/100.0).toString());
                edittot10.setText(Double.valueOf(Math.round(getTOT10FromTOT2(bv)*100)/100.0).toString());
                editAtot2.setText(Double.valueOf(Math.round(getATOT2FromTOT2(bv)*100)/100.0).toString());
                editAtot10.setText(Double.valueOf(Math.round(getATOT10FromTOT2(bv)*100)/100.0).toString());

            }
            ignoreEvent=false;


        }
    };


    TextWatcher textWatchertot10 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {


            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {

                editBtot.setText("");
                edittot2.setText("");
                editAtot2.setText("");
                editAtot10.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                editBtot.setText(Double.valueOf(Math.round(getBTOTFromTOT10(bv)*100)/100.0).toString());
                edittot2.setText(Double.valueOf(Math.round(getTOT2FromTOT10(bv)*100)/100.0).toString());
                editAtot2.setText(Double.valueOf(Math.round(getATOT2FromTOT10(bv)*100)/100.0).toString());
                editAtot10.setText(Double.valueOf(Math.round(getATOT10FromTOT10(bv)*100)/100.0).toString());

            }
            ignoreEvent=false;

        }
    };


    TextWatcher textWatcherAtot2 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {


            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {

                editBtot.setText("");
                edittot2.setText("");
                edittot10.setText("");
                editAtot10.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                editBtot.setText(Double.valueOf(Math.round(getBTOTFromATOT2(bv)*100)/100.0).toString());
                edittot2.setText(Double.valueOf(Math.round(getTOT2FromATOT2(bv)*100)/100.0).toString());
                edittot10.setText(Double.valueOf(Math.round(getTOT10FromATOT2(bv)*100)/100.0).toString());
                editAtot10.setText(Double.valueOf(Math.round(getATOT10FromATOT2(bv)*100)/100.0).toString());

            }
            ignoreEvent=false;


        }
    };


    TextWatcher textWatcherAtot10 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {



            if(ignoreEvent)
                return;
            ignoreEvent=true;
            String txt=s.toString();
            if(txt==null || txt.length()==0) {

                editBtot.setText("");
                edittot2.setText("");
                edittot10.setText("");
                editAtot2.setText("");

            }
            else

            {
                double bv=tryParse(txt);
                editBtot.setText(Double.valueOf(Math.round(getBTOTFromATOT10(bv)*100)/100.0).toString());
                edittot2.setText(Double.valueOf(Math.round(getTOT2FromATOT10(bv)*100)/100.0).toString());
                edittot10.setText(Double.valueOf(Math.round(getTOT10FromATOT10(bv)*100)/100.0).toString());
                editAtot2.setText(Double.valueOf(Math.round(getATOT2FromATOT10(bv)*100)/100.0).toString());

            }
            ignoreEvent=false;

        }
    };


    double getTOT2FromBTOT(double BTOT)
    {
        return 0.02*BTOT;
    }
    double getTOT10FromBTOT(double BTOT)
    {
        return 0.1*BTOT;
    }
    double getATOT2FromBTOT(double BTOT)
    {
        return 1.02*BTOT;
    }
    double getATOT10FromBTOT(double BTOT)
    {
        return 1.1*BTOT;
    }







    double getBTOTFromTOT2(double TOT2)
    {
        return TOT2*50;
    }
    double getATOT2FromTOT2(double TOT2)
    {
        return 51*TOT2;
    }
    double getTOT10FromTOT2(double TOT2)
    {
        return TOT2*5;
    }
    double getATOT10FromTOT2(double TOT2)
    {
        return TOT2*55;
    }




    double getBTOTFromTOT10(double TOT10)
    {
        return TOT10*10;
    }
    double getTOT2FromTOT10(double TOT10)
    {
        return 0.2*TOT10;
    }
    double getATOT2FromTOT10(double TOT10)
    {
        return TOT10*10.2;
    }
    double getATOT10FromTOT10(double TOT10)
    {
        return TOT10*11;
    }







    double getBTOTFromATOT2(double ATOT2)
    {
        return ATOT2/1.02;
    }
    double getTOT2FromATOT2(double ATOT2)
    {
        return (0.02*ATOT2)/1.02;
    }
    double getTOT10FromATOT2(double ATOT2)
    {
        return (0.1*ATOT2)/1.02;
    }
    double getATOT10FromATOT2(double ATOT2)
    {
        return (1.1*ATOT2)/1.02;
    }





    double getBTOTFromATOT10(double ATOT10)
    {
        return ATOT10/1.1;
    }
    double getTOT2FromATOT10(double ATOT10)
    {
        return (0.2*ATOT10)/11;
    }
    double getATOT2FromATOT10(double ATOT10)
    {
        return (10.2*ATOT10)/11;
    }
    double getTOT10FromATOT10(double ATOT10)
    {
        return ATOT10/11;
    }





    public void tot(View view){
        //Intent i = new Intent(tot.this,BizNet.class);
        //startActivity(i);

        Intent intent = new Intent(this, BizNet.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityIfNeeded(intent, 0);
    }

}
