package et.biznet.bntools;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;

public class MainActivity extends AppCompatActivity
{
    boolean mainWindow=true;
    VATController vc;
    void initializeToolWindows()
    {
        this.mainWindow=true;
        setContentView(R.layout.activity_main);
        final MainActivity thisActivity = this;
        ((Button) findViewById(R.id.main_show_vat)).setOnClickListener(
                new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                       View vw = getLayoutInflater().inflate(R.layout.vat_window, null);
                        vw.startAnimation(AnimationUtils.loadAnimation(thisActivity, android.R.anim.slide_in_left));

                        thisActivity.mainWindow=false;
                        setContentView(vw);
                        vc=new VATController(vw);
                    }
                }
        );
    }
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        initializeToolWindows();
    }

    @Override
    public void onBackPressed()
    {
        if(mainWindow)
        {
            super.onBackPressed();
            return;
        }
        initializeToolWindows();
    }

}
