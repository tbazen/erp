package et.biznet.bntools;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

/**
 * Created by Teweldemedhin Aberra on 8/23/2016.
 */
public class VATController
{
    EditText editBeforeVAT;
    EditText editVAT;
    public VATController(View v)
    {
        editBeforeVAT=(EditText)v.findViewById(R.id.vat_bv_value);
        editVAT=(EditText)v.findViewById(R.id.vat_vat_value);
        editBeforeVAT.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                editVAT.setText("VAT+2");
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });
    }
}
