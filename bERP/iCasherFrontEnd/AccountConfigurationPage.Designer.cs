﻿namespace BIZNET.iERP.Client
{
    partial class AccountConfigurationPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.costCenterEmployee = new INTAPS.Accounting.Client.CostCenterPlaceHolder();
            this.accountPlaceHolder44 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder43 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder42 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder41 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder40 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder39 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder38 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder37 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder36 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder13 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder12 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder11 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder25 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.costCenterPlaceHolder3 = new INTAPS.Accounting.Client.CostCenterPlaceHolder();
            this.costCenterPlaceHolder2 = new INTAPS.Accounting.Client.CostCenterPlaceHolder();
            this.acHQRec = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.acHQLiability = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.costCenterPlaceHolder1 = new INTAPS.Accounting.Client.CostCenterPlaceHolder();
            this.accountPlaceHolder35 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder34 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder31 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder30 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder29 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder28 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder24 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder3 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder2 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder1 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder33 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder32 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder27 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder26 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder23 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder22 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder21 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder20 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder19 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder18 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder17 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder45 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder16 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder15 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder46 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder14 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder10 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder9 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder8 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder7 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder6 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder5 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder4 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutWithdrawal = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWithdrawal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.costCenterEmployee);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder44);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder43);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder42);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder41);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder40);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder39);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder38);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder37);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder36);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder13);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder12);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder11);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder25);
            this.layoutControl1.Controls.Add(this.costCenterPlaceHolder3);
            this.layoutControl1.Controls.Add(this.costCenterPlaceHolder2);
            this.layoutControl1.Controls.Add(this.acHQRec);
            this.layoutControl1.Controls.Add(this.acHQLiability);
            this.layoutControl1.Controls.Add(this.costCenterPlaceHolder1);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder35);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder34);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder31);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder30);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder29);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder28);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder24);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder3);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder2);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder1);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder33);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder32);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder27);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder26);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder23);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder22);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder21);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder20);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder19);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder18);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder17);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder45);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder16);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder15);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder46);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder14);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder10);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder9);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder8);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder7);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder6);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder5);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder4);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(454, 143, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(563, 996);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // costCenterEmployee
            // 
            this.costCenterEmployee.account = null;
            this.costCenterEmployee.AllowAdd = false;
            this.costCenterEmployee.Location = new System.Drawing.Point(228, 124);
            this.costCenterEmployee.Name = "costCenterEmployee";
            this.costCenterEmployee.OnlyLeafAccount = false;
            this.costCenterEmployee.Size = new System.Drawing.Size(299, 20);
            this.costCenterEmployee.TabIndex = 65;
            this.costCenterEmployee.Tag = "employeeCostCenterID";
            // 
            // accountPlaceHolder44
            // 
            this.accountPlaceHolder44.account = null;
            this.accountPlaceHolder44.AllowAdd = false;
            this.accountPlaceHolder44.Location = new System.Drawing.Point(229, 1844);
            this.accountPlaceHolder44.Name = "accountPlaceHolder44";
            this.accountPlaceHolder44.OnlyLeafAccount = false;
            this.accountPlaceHolder44.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder44.TabIndex = 64;
            this.accountPlaceHolder44.Tag = "closingAccountID";
            // 
            // accountPlaceHolder43
            // 
            this.accountPlaceHolder43.account = null;
            this.accountPlaceHolder43.AllowAdd = false;
            this.accountPlaceHolder43.Location = new System.Drawing.Point(229, 1263);
            this.accountPlaceHolder43.Name = "accountPlaceHolder43";
            this.accountPlaceHolder43.OnlyLeafAccount = false;
            this.accountPlaceHolder43.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder43.TabIndex = 63;
            this.accountPlaceHolder43.Tag = "collectedWithHoldingTaxNotInvoicedAccountID";
            // 
            // accountPlaceHolder42
            // 
            this.accountPlaceHolder42.account = null;
            this.accountPlaceHolder42.AllowAdd = false;
            this.accountPlaceHolder42.Location = new System.Drawing.Point(229, 1323);
            this.accountPlaceHolder42.Name = "accountPlaceHolder42";
            this.accountPlaceHolder42.OnlyLeafAccount = false;
            this.accountPlaceHolder42.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder42.TabIndex = 62;
            this.accountPlaceHolder42.Tag = "outputVATWithHeldNotInvoicedAccountID";
            // 
            // accountPlaceHolder41
            // 
            this.accountPlaceHolder41.account = null;
            this.accountPlaceHolder41.AllowAdd = false;
            this.accountPlaceHolder41.Location = new System.Drawing.Point(229, 1293);
            this.accountPlaceHolder41.Name = "accountPlaceHolder41";
            this.accountPlaceHolder41.OnlyLeafAccount = false;
            this.accountPlaceHolder41.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder41.TabIndex = 61;
            this.accountPlaceHolder41.Tag = "outputVATWithHeldAccountID";
            // 
            // accountPlaceHolder40
            // 
            this.accountPlaceHolder40.account = null;
            this.accountPlaceHolder40.AllowAdd = false;
            this.accountPlaceHolder40.Location = new System.Drawing.Point(229, 1694);
            this.accountPlaceHolder40.Name = "accountPlaceHolder40";
            this.accountPlaceHolder40.OnlyLeafAccount = false;
            this.accountPlaceHolder40.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder40.TabIndex = 60;
            this.accountPlaceHolder40.Tag = "otherIncomeAccountID";
            // 
            // accountPlaceHolder39
            // 
            this.accountPlaceHolder39.account = null;
            this.accountPlaceHolder39.AllowAdd = false;
            this.accountPlaceHolder39.Location = new System.Drawing.Point(229, 346);
            this.accountPlaceHolder39.Name = "accountPlaceHolder39";
            this.accountPlaceHolder39.OnlyLeafAccount = false;
            this.accountPlaceHolder39.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder39.TabIndex = 59;
            this.accountPlaceHolder39.Tag = "expenseAccountID";
            // 
            // accountPlaceHolder38
            // 
            this.accountPlaceHolder38.account = null;
            this.accountPlaceHolder38.AllowAdd = false;
            this.accountPlaceHolder38.Location = new System.Drawing.Point(229, 316);
            this.accountPlaceHolder38.Name = "accountPlaceHolder38";
            this.accountPlaceHolder38.OnlyLeafAccount = false;
            this.accountPlaceHolder38.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder38.TabIndex = 58;
            this.accountPlaceHolder38.Tag = "costOfSalesAccountID";
            this.accountPlaceHolder38.TextChanged += new System.EventHandler(this.accountPlaceHolder38_TextChanged);
            // 
            // accountPlaceHolder37
            // 
            this.accountPlaceHolder37.account = null;
            this.accountPlaceHolder37.AllowAdd = false;
            this.accountPlaceHolder37.Location = new System.Drawing.Point(229, 286);
            this.accountPlaceHolder37.Name = "accountPlaceHolder37";
            this.accountPlaceHolder37.OnlyLeafAccount = false;
            this.accountPlaceHolder37.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder37.TabIndex = 57;
            this.accountPlaceHolder37.Tag = "assetIncomeAccountID";
            // 
            // accountPlaceHolder36
            // 
            this.accountPlaceHolder36.account = null;
            this.accountPlaceHolder36.AllowAdd = false;
            this.accountPlaceHolder36.Location = new System.Drawing.Point(229, 256);
            this.accountPlaceHolder36.Name = "accountPlaceHolder36";
            this.accountPlaceHolder36.OnlyLeafAccount = false;
            this.accountPlaceHolder36.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder36.TabIndex = 56;
            this.accountPlaceHolder36.Tag = "assetCapitalAccountID";
            // 
            // accountPlaceHolder13
            // 
            this.accountPlaceHolder13.account = null;
            this.accountPlaceHolder13.AllowAdd = false;
            this.accountPlaceHolder13.Location = new System.Drawing.Point(229, 226);
            this.accountPlaceHolder13.Name = "accountPlaceHolder13";
            this.accountPlaceHolder13.OnlyLeafAccount = false;
            this.accountPlaceHolder13.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder13.TabIndex = 55;
            this.accountPlaceHolder13.Tag = "assetLiabilityAccountID";
            // 
            // accountPlaceHolder12
            // 
            this.accountPlaceHolder12.account = null;
            this.accountPlaceHolder12.AllowAdd = false;
            this.accountPlaceHolder12.Location = new System.Drawing.Point(229, 196);
            this.accountPlaceHolder12.Name = "accountPlaceHolder12";
            this.accountPlaceHolder12.OnlyLeafAccount = false;
            this.accountPlaceHolder12.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder12.TabIndex = 54;
            this.accountPlaceHolder12.Tag = "assetAccountID";
            // 
            // accountPlaceHolder11
            // 
            this.accountPlaceHolder11.account = null;
            this.accountPlaceHolder11.AllowAdd = false;
            this.accountPlaceHolder11.Location = new System.Drawing.Point(229, 1621);
            this.accountPlaceHolder11.Name = "accountPlaceHolder11";
            this.accountPlaceHolder11.OnlyLeafAccount = false;
            this.accountPlaceHolder11.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder11.TabIndex = 53;
            this.accountPlaceHolder11.Tag = "taxInterestAccountID";
            // 
            // accountPlaceHolder25
            // 
            this.accountPlaceHolder25.account = null;
            this.accountPlaceHolder25.AllowAdd = false;
            this.accountPlaceHolder25.Location = new System.Drawing.Point(226, 983);
            this.accountPlaceHolder25.Name = "accountPlaceHolder25";
            this.accountPlaceHolder25.OnlyLeafAccount = false;
            this.accountPlaceHolder25.Size = new System.Drawing.Size(303, 20);
            this.accountPlaceHolder25.TabIndex = 52;
            this.accountPlaceHolder25.Tag = "materialInFlowAccountID";
            // 
            // costCenterPlaceHolder3
            // 
            this.costCenterPlaceHolder3.account = null;
            this.costCenterPlaceHolder3.AllowAdd = false;
            this.costCenterPlaceHolder3.Location = new System.Drawing.Point(226, 669);
            this.costCenterPlaceHolder3.Name = "costCenterPlaceHolder3";
            this.costCenterPlaceHolder3.OnlyLeafAccount = false;
            this.costCenterPlaceHolder3.Size = new System.Drawing.Size(303, 20);
            this.costCenterPlaceHolder3.TabIndex = 51;
            this.costCenterPlaceHolder3.Tag = "onSupplierOrderCostCenterID";
            // 
            // costCenterPlaceHolder2
            // 
            this.costCenterPlaceHolder2.account = null;
            this.costCenterPlaceHolder2.AllowAdd = false;
            this.costCenterPlaceHolder2.Location = new System.Drawing.Point(226, 639);
            this.costCenterPlaceHolder2.Name = "costCenterPlaceHolder2";
            this.costCenterPlaceHolder2.OnlyLeafAccount = false;
            this.costCenterPlaceHolder2.Size = new System.Drawing.Size(303, 20);
            this.costCenterPlaceHolder2.TabIndex = 50;
            this.costCenterPlaceHolder2.Tag = "onCustomerOrderCostCenterID";
            // 
            // acHQRec
            // 
            this.acHQRec.account = null;
            this.acHQRec.AllowAdd = false;
            this.acHQRec.Location = new System.Drawing.Point(229, 65);
            this.acHQRec.Name = "acHQRec";
            this.acHQRec.OnlyLeafAccount = false;
            this.acHQRec.Size = new System.Drawing.Size(297, 20);
            this.acHQRec.TabIndex = 49;
            this.acHQRec.Tag = "internalServiceReceivableAccountID";
            // 
            // acHQLiability
            // 
            this.acHQLiability.account = null;
            this.acHQLiability.AllowAdd = false;
            this.acHQLiability.Location = new System.Drawing.Point(229, 95);
            this.acHQLiability.Name = "acHQLiability";
            this.acHQLiability.OnlyLeafAccount = false;
            this.acHQLiability.Size = new System.Drawing.Size(297, 20);
            this.acHQLiability.TabIndex = 48;
            this.acHQLiability.Tag = "internalServicePayableAccountID";
            // 
            // costCenterPlaceHolder1
            // 
            this.costCenterPlaceHolder1.account = null;
            this.costCenterPlaceHolder1.AllowAdd = false;
            this.costCenterPlaceHolder1.Location = new System.Drawing.Point(227, 37);
            this.costCenterPlaceHolder1.Name = "costCenterPlaceHolder1";
            this.costCenterPlaceHolder1.OnlyLeafAccount = false;
            this.costCenterPlaceHolder1.Size = new System.Drawing.Size(301, 20);
            this.costCenterPlaceHolder1.TabIndex = 47;
            this.costCenterPlaceHolder1.Tag = "mainCostCenterID";
            // 
            // accountPlaceHolder35
            // 
            this.accountPlaceHolder35.account = null;
            this.accountPlaceHolder35.AllowAdd = false;
            this.accountPlaceHolder35.Location = new System.Drawing.Point(226, 953);
            this.accountPlaceHolder35.Name = "accountPlaceHolder35";
            this.accountPlaceHolder35.OnlyLeafAccount = false;
            this.accountPlaceHolder35.Size = new System.Drawing.Size(303, 20);
            this.accountPlaceHolder35.TabIndex = 46;
            this.accountPlaceHolder35.Tag = "inventoryExpenseAccount";
            // 
            // accountPlaceHolder34
            // 
            this.accountPlaceHolder34.account = null;
            this.accountPlaceHolder34.AllowAdd = false;
            this.accountPlaceHolder34.Location = new System.Drawing.Point(226, 923);
            this.accountPlaceHolder34.Name = "accountPlaceHolder34";
            this.accountPlaceHolder34.OnlyLeafAccount = false;
            this.accountPlaceHolder34.Size = new System.Drawing.Size(303, 20);
            this.accountPlaceHolder34.TabIndex = 45;
            this.accountPlaceHolder34.Tag = "inventoryIncomeAccount";
            // 
            // accountPlaceHolder31
            // 
            this.accountPlaceHolder31.account = null;
            this.accountPlaceHolder31.AllowAdd = false;
            this.accountPlaceHolder31.Location = new System.Drawing.Point(229, 479);
            this.accountPlaceHolder31.Name = "accountPlaceHolder31";
            this.accountPlaceHolder31.OnlyLeafAccount = false;
            this.accountPlaceHolder31.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder31.TabIndex = 44;
            this.accountPlaceHolder31.Tag = "bankServiceChargeByCash";
            // 
            // accountPlaceHolder30
            // 
            this.accountPlaceHolder30.account = null;
            this.accountPlaceHolder30.AllowAdd = false;
            this.accountPlaceHolder30.Location = new System.Drawing.Point(229, 509);
            this.accountPlaceHolder30.Name = "accountPlaceHolder30";
            this.accountPlaceHolder30.OnlyLeafAccount = false;
            this.accountPlaceHolder30.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder30.TabIndex = 43;
            this.accountPlaceHolder30.Tag = "cashAccountID";
            // 
            // accountPlaceHolder29
            // 
            this.accountPlaceHolder29.account = null;
            this.accountPlaceHolder29.AllowAdd = false;
            this.accountPlaceHolder29.Location = new System.Drawing.Point(229, 449);
            this.accountPlaceHolder29.Name = "accountPlaceHolder29";
            this.accountPlaceHolder29.OnlyLeafAccount = false;
            this.accountPlaceHolder29.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder29.TabIndex = 42;
            this.accountPlaceHolder29.Tag = "bankServiceChargeAccountID";
            // 
            // accountPlaceHolder28
            // 
            this.accountPlaceHolder28.account = null;
            this.accountPlaceHolder28.AllowAdd = false;
            this.accountPlaceHolder28.Location = new System.Drawing.Point(229, 419);
            this.accountPlaceHolder28.Name = "accountPlaceHolder28";
            this.accountPlaceHolder28.OnlyLeafAccount = false;
            this.accountPlaceHolder28.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder28.TabIndex = 41;
            this.accountPlaceHolder28.Tag = "mainBankAccountID";
            // 
            // accountPlaceHolder24
            // 
            this.accountPlaceHolder24.account = null;
            this.accountPlaceHolder24.AllowAdd = false;
            this.accountPlaceHolder24.Location = new System.Drawing.Point(229, 1814);
            this.accountPlaceHolder24.Name = "accountPlaceHolder24";
            this.accountPlaceHolder24.OnlyLeafAccount = false;
            this.accountPlaceHolder24.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder24.TabIndex = 40;
            this.accountPlaceHolder24.Tag = "WithdrawalAccountID";
            // 
            // accountPlaceHolder3
            // 
            this.accountPlaceHolder3.account = null;
            this.accountPlaceHolder3.AllowAdd = false;
            this.accountPlaceHolder3.Location = new System.Drawing.Point(229, 1784);
            this.accountPlaceHolder3.Name = "accountPlaceHolder3";
            this.accountPlaceHolder3.OnlyLeafAccount = false;
            this.accountPlaceHolder3.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder3.TabIndex = 39;
            this.accountPlaceHolder3.Tag = "zReportNonTaxableAccountID";
            // 
            // accountPlaceHolder2
            // 
            this.accountPlaceHolder2.account = null;
            this.accountPlaceHolder2.AllowAdd = false;
            this.accountPlaceHolder2.Location = new System.Drawing.Point(229, 1754);
            this.accountPlaceHolder2.Name = "accountPlaceHolder2";
            this.accountPlaceHolder2.OnlyLeafAccount = false;
            this.accountPlaceHolder2.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder2.TabIndex = 38;
            this.accountPlaceHolder2.Tag = "zReportTaxableAccountID";
            // 
            // accountPlaceHolder1
            // 
            this.accountPlaceHolder1.account = null;
            this.accountPlaceHolder1.AllowAdd = false;
            this.accountPlaceHolder1.Location = new System.Drawing.Point(229, 1113);
            this.accountPlaceHolder1.Name = "accountPlaceHolder1";
            this.accountPlaceHolder1.OnlyLeafAccount = false;
            this.accountPlaceHolder1.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder1.TabIndex = 37;
            this.accountPlaceHolder1.Tag = "accumulatedReceivableVATAccountID";
            // 
            // accountPlaceHolder33
            // 
            this.accountPlaceHolder33.account = null;
            this.accountPlaceHolder33.AllowAdd = false;
            this.accountPlaceHolder33.Location = new System.Drawing.Point(229, 1561);
            this.accountPlaceHolder33.Name = "accountPlaceHolder33";
            this.accountPlaceHolder33.OnlyLeafAccount = false;
            this.accountPlaceHolder33.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder33.TabIndex = 36;
            this.accountPlaceHolder33.Tag = "taxExemptionsAccountID";
            // 
            // accountPlaceHolder32
            // 
            this.accountPlaceHolder32.account = null;
            this.accountPlaceHolder32.AllowAdd = false;
            this.accountPlaceHolder32.Location = new System.Drawing.Point(229, 1591);
            this.accountPlaceHolder32.Name = "accountPlaceHolder32";
            this.accountPlaceHolder32.OnlyLeafAccount = false;
            this.accountPlaceHolder32.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder32.TabIndex = 35;
            this.accountPlaceHolder32.Tag = "taxPenalityAcountID";
            // 
            // accountPlaceHolder27
            // 
            this.accountPlaceHolder27.account = null;
            this.accountPlaceHolder27.AllowAdd = false;
            this.accountPlaceHolder27.Location = new System.Drawing.Point(229, 1531);
            this.accountPlaceHolder27.Name = "accountPlaceHolder27";
            this.accountPlaceHolder27.OnlyLeafAccount = false;
            this.accountPlaceHolder27.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder27.TabIndex = 30;
            this.accountPlaceHolder27.Tag = "laborIncomeTaxAccountID";
            // 
            // accountPlaceHolder26
            // 
            this.accountPlaceHolder26.account = null;
            this.accountPlaceHolder26.AllowAdd = false;
            this.accountPlaceHolder26.Location = new System.Drawing.Point(229, 1724);
            this.accountPlaceHolder26.Name = "accountPlaceHolder26";
            this.accountPlaceHolder26.OnlyLeafAccount = false;
            this.accountPlaceHolder26.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder26.TabIndex = 29;
            this.accountPlaceHolder26.Tag = "laborExpenseAccountID";
            // 
            // accountPlaceHolder23
            // 
            this.accountPlaceHolder23.account = null;
            this.accountPlaceHolder23.AllowAdd = false;
            this.accountPlaceHolder23.Location = new System.Drawing.Point(229, 1501);
            this.accountPlaceHolder23.Name = "accountPlaceHolder23";
            this.accountPlaceHolder23.OnlyLeafAccount = false;
            this.accountPlaceHolder23.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder23.TabIndex = 26;
            this.accountPlaceHolder23.Tag = "surTaxExpenseAccountID";
            // 
            // accountPlaceHolder22
            // 
            this.accountPlaceHolder22.account = null;
            this.accountPlaceHolder22.AllowAdd = false;
            this.accountPlaceHolder22.Location = new System.Drawing.Point(229, 1471);
            this.accountPlaceHolder22.Name = "accountPlaceHolder22";
            this.accountPlaceHolder22.OnlyLeafAccount = false;
            this.accountPlaceHolder22.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder22.TabIndex = 25;
            this.accountPlaceHolder22.Tag = "exciseTaxExpenseAccountID";
            // 
            // accountPlaceHolder21
            // 
            this.accountPlaceHolder21.account = null;
            this.accountPlaceHolder21.AllowAdd = false;
            this.accountPlaceHolder21.Location = new System.Drawing.Point(229, 1441);
            this.accountPlaceHolder21.Name = "accountPlaceHolder21";
            this.accountPlaceHolder21.OnlyLeafAccount = false;
            this.accountPlaceHolder21.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder21.TabIndex = 24;
            this.accountPlaceHolder21.Tag = "customDutyTaxExpenseAccountID";
            // 
            // accountPlaceHolder20
            // 
            this.accountPlaceHolder20.account = null;
            this.accountPlaceHolder20.AllowAdd = false;
            this.accountPlaceHolder20.Location = new System.Drawing.Point(228, 1412);
            this.accountPlaceHolder20.Name = "accountPlaceHolder20";
            this.accountPlaceHolder20.OnlyLeafAccount = false;
            this.accountPlaceHolder20.Size = new System.Drawing.Size(299, 20);
            this.accountPlaceHolder20.TabIndex = 23;
            this.accountPlaceHolder20.Tag = "withholdingExpenseAccountID";
            // 
            // accountPlaceHolder19
            // 
            this.accountPlaceHolder19.account = null;
            this.accountPlaceHolder19.AllowAdd = false;
            this.accountPlaceHolder19.Location = new System.Drawing.Point(229, 1383);
            this.accountPlaceHolder19.Name = "accountPlaceHolder19";
            this.accountPlaceHolder19.OnlyLeafAccount = false;
            this.accountPlaceHolder19.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder19.TabIndex = 22;
            this.accountPlaceHolder19.Tag = "collectedTOTAccountID";
            // 
            // accountPlaceHolder18
            // 
            this.accountPlaceHolder18.account = null;
            this.accountPlaceHolder18.AllowAdd = false;
            this.accountPlaceHolder18.Location = new System.Drawing.Point(229, 1353);
            this.accountPlaceHolder18.Name = "accountPlaceHolder18";
            this.accountPlaceHolder18.OnlyLeafAccount = false;
            this.accountPlaceHolder18.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder18.TabIndex = 21;
            this.accountPlaceHolder18.Tag = "TOTExpenseAccountID";
            // 
            // accountPlaceHolder17
            // 
            this.accountPlaceHolder17.account = null;
            this.accountPlaceHolder17.AllowAdd = false;
            this.accountPlaceHolder17.Location = new System.Drawing.Point(229, 1233);
            this.accountPlaceHolder17.Name = "accountPlaceHolder17";
            this.accountPlaceHolder17.OnlyLeafAccount = false;
            this.accountPlaceHolder17.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder17.TabIndex = 20;
            this.accountPlaceHolder17.Tag = "collectedWithHoldingTaxAccountID";
            // 
            // accountPlaceHolder45
            // 
            this.accountPlaceHolder45.account = null;
            this.accountPlaceHolder45.AllowAdd = false;
            this.accountPlaceHolder45.Location = new System.Drawing.Point(229, 1203);
            this.accountPlaceHolder45.Name = "accountPlaceHolder45";
            this.accountPlaceHolder45.OnlyLeafAccount = false;
            this.accountPlaceHolder45.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder45.TabIndex = 19;
            this.accountPlaceHolder45.Tag = "outPutVATNotInoicedAccountID";
            // 
            // accountPlaceHolder16
            // 
            this.accountPlaceHolder16.account = null;
            this.accountPlaceHolder16.AllowAdd = false;
            this.accountPlaceHolder16.Location = new System.Drawing.Point(229, 1173);
            this.accountPlaceHolder16.Name = "accountPlaceHolder16";
            this.accountPlaceHolder16.OnlyLeafAccount = false;
            this.accountPlaceHolder16.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder16.TabIndex = 19;
            this.accountPlaceHolder16.Tag = "outPutVATAccountID";
            // 
            // accountPlaceHolder15
            // 
            this.accountPlaceHolder15.account = null;
            this.accountPlaceHolder15.AllowAdd = false;
            this.accountPlaceHolder15.Location = new System.Drawing.Point(229, 1143);
            this.accountPlaceHolder15.Name = "accountPlaceHolder15";
            this.accountPlaceHolder15.OnlyLeafAccount = false;
            this.accountPlaceHolder15.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder15.TabIndex = 18;
            this.accountPlaceHolder15.Tag = "paidWithHoldingTaxAccountID";
            // 
            // accountPlaceHolder46
            // 
            this.accountPlaceHolder46.account = null;
            this.accountPlaceHolder46.AllowAdd = false;
            this.accountPlaceHolder46.Location = new System.Drawing.Point(229, 1083);
            this.accountPlaceHolder46.Name = "accountPlaceHolder46";
            this.accountPlaceHolder46.OnlyLeafAccount = false;
            this.accountPlaceHolder46.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder46.TabIndex = 17;
            this.accountPlaceHolder46.Tag = "inputVATAccountIDNotInvoiced";
            // 
            // accountPlaceHolder14
            // 
            this.accountPlaceHolder14.account = null;
            this.accountPlaceHolder14.AllowAdd = false;
            this.accountPlaceHolder14.Location = new System.Drawing.Point(229, 1053);
            this.accountPlaceHolder14.Name = "accountPlaceHolder14";
            this.accountPlaceHolder14.OnlyLeafAccount = false;
            this.accountPlaceHolder14.Size = new System.Drawing.Size(297, 20);
            this.accountPlaceHolder14.TabIndex = 17;
            this.accountPlaceHolder14.Tag = "inputVATAccountID";
            // 
            // accountPlaceHolder10
            // 
            this.accountPlaceHolder10.account = null;
            this.accountPlaceHolder10.AllowAdd = false;
            this.accountPlaceHolder10.Location = new System.Drawing.Point(226, 893);
            this.accountPlaceHolder10.Name = "accountPlaceHolder10";
            this.accountPlaceHolder10.OnlyLeafAccount = false;
            this.accountPlaceHolder10.Size = new System.Drawing.Size(303, 20);
            this.accountPlaceHolder10.TabIndex = 13;
            this.accountPlaceHolder10.Tag = "FixedAssetDepreciationAccountID";
            // 
            // accountPlaceHolder9
            // 
            this.accountPlaceHolder9.account = null;
            this.accountPlaceHolder9.AllowAdd = false;
            this.accountPlaceHolder9.Location = new System.Drawing.Point(226, 863);
            this.accountPlaceHolder9.Name = "accountPlaceHolder9";
            this.accountPlaceHolder9.OnlyLeafAccount = false;
            this.accountPlaceHolder9.Size = new System.Drawing.Size(303, 20);
            this.accountPlaceHolder9.TabIndex = 12;
            this.accountPlaceHolder9.Tag = "FixedAssetAccumulatedDepreciationAccountID";
            // 
            // accountPlaceHolder8
            // 
            this.accountPlaceHolder8.account = null;
            this.accountPlaceHolder8.AllowAdd = false;
            this.accountPlaceHolder8.Location = new System.Drawing.Point(226, 833);
            this.accountPlaceHolder8.Name = "accountPlaceHolder8";
            this.accountPlaceHolder8.OnlyLeafAccount = false;
            this.accountPlaceHolder8.Size = new System.Drawing.Size(303, 20);
            this.accountPlaceHolder8.TabIndex = 11;
            this.accountPlaceHolder8.Tag = "OriginalFixedAssetAccountID";
            // 
            // accountPlaceHolder7
            // 
            this.accountPlaceHolder7.account = null;
            this.accountPlaceHolder7.AllowAdd = false;
            this.accountPlaceHolder7.Location = new System.Drawing.Point(226, 766);
            this.accountPlaceHolder7.Name = "accountPlaceHolder7";
            this.accountPlaceHolder7.OnlyLeafAccount = false;
            this.accountPlaceHolder7.Size = new System.Drawing.Size(303, 20);
            this.accountPlaceHolder7.TabIndex = 10;
            this.accountPlaceHolder7.Tag = "financePayablesAccountID";
            // 
            // accountPlaceHolder6
            // 
            this.accountPlaceHolder6.account = null;
            this.accountPlaceHolder6.AllowAdd = false;
            this.accountPlaceHolder6.Location = new System.Drawing.Point(226, 736);
            this.accountPlaceHolder6.Name = "accountPlaceHolder6";
            this.accountPlaceHolder6.OnlyLeafAccount = false;
            this.accountPlaceHolder6.Size = new System.Drawing.Size(303, 20);
            this.accountPlaceHolder6.TabIndex = 9;
            this.accountPlaceHolder6.Tag = "financeReceivablesAccountID";
            // 
            // accountPlaceHolder5
            // 
            this.accountPlaceHolder5.account = null;
            this.accountPlaceHolder5.AllowAdd = false;
            this.accountPlaceHolder5.Location = new System.Drawing.Point(226, 609);
            this.accountPlaceHolder5.Name = "accountPlaceHolder5";
            this.accountPlaceHolder5.OnlyLeafAccount = false;
            this.accountPlaceHolder5.Size = new System.Drawing.Size(303, 20);
            this.accountPlaceHolder5.TabIndex = 8;
            this.accountPlaceHolder5.Tag = "tradePayablesAccountID";
            // 
            // accountPlaceHolder4
            // 
            this.accountPlaceHolder4.account = null;
            this.accountPlaceHolder4.AllowAdd = false;
            this.accountPlaceHolder4.Location = new System.Drawing.Point(226, 579);
            this.accountPlaceHolder4.Name = "accountPlaceHolder4";
            this.accountPlaceHolder4.OnlyLeafAccount = false;
            this.accountPlaceHolder4.Size = new System.Drawing.Size(303, 20);
            this.accountPlaceHolder4.TabIndex = 7;
            this.accountPlaceHolder4.Tag = "tradeReceivablesAccountID";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.layoutControlGroup2,
            this.layoutControlGroup6,
            this.layoutControlGroup7,
            this.layoutControlGroup8,
            this.layoutControlGroup9});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(546, 1884);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.CustomizationFormText = "TradeRelation Accounts";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem37,
            this.layoutControlItem38});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 543);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(6, 6, 6, 6);
            this.layoutControlGroup3.Size = new System.Drawing.Size(540, 157);
            this.layoutControlGroup3.Text = "Suppliers/Customers";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.accountPlaceHolder4;
            this.layoutControlItem4.CustomizationFormText = "Suppliers Receivable:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(522, 30);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem4.Text = "Receivable:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.accountPlaceHolder5;
            this.layoutControlItem5.CustomizationFormText = "Suppliers Payable:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(522, 30);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem5.Text = "Payable:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.costCenterPlaceHolder2;
            this.layoutControlItem37.CustomizationFormText = "TradeRelation Order Accounting Center:";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(522, 30);
            this.layoutControlItem37.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem37.Text = "Items Ordered by Customers Cost Center:";
            this.layoutControlItem37.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.costCenterPlaceHolder3;
            this.layoutControlItem38.CustomizationFormText = "TradeRelation Order Acccounting Center";
            this.layoutControlItem38.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(522, 30);
            this.layoutControlItem38.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem38.Text = "Items Ordered from Suppliers Cost Center:";
            this.layoutControlItem38.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup4.CustomizationFormText = "TradeRelation Accounts";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem7});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 700);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(6, 6, 6, 6);
            this.layoutControlGroup4.Size = new System.Drawing.Size(540, 97);
            this.layoutControlGroup4.Text = "Financers/Financee";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.accountPlaceHolder6;
            this.layoutControlItem6.CustomizationFormText = "Customers Receivable:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(522, 30);
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem6.Text = "Receivable:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.accountPlaceHolder7;
            this.layoutControlItem7.CustomizationFormText = "Customers Payable:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(522, 30);
            this.layoutControlItem7.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem7.Text = "Payable:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup5.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup5.CustomizationFormText = "Fixed Asset Accounts";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem10,
            this.layoutControlItem9,
            this.layoutControlItem31,
            this.layoutControlItem34,
            this.layoutControlItem39});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 797);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(6, 6, 6, 6);
            this.layoutControlGroup5.Size = new System.Drawing.Size(540, 217);
            this.layoutControlGroup5.Text = "Inventory and Fixed Asset Accounts";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.accountPlaceHolder8;
            this.layoutControlItem8.CustomizationFormText = "Original Fixed Asset Value:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(522, 30);
            this.layoutControlItem8.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem8.Text = "Original Fixed Asset Value:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.accountPlaceHolder10;
            this.layoutControlItem10.CustomizationFormText = "Depreciation:";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(522, 30);
            this.layoutControlItem10.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem10.Text = "Depreciation:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.accountPlaceHolder9;
            this.layoutControlItem9.CustomizationFormText = "Accumulated Depreciation:";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(522, 30);
            this.layoutControlItem9.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem9.Text = "Accumulated Depreciation:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.accountPlaceHolder34;
            this.layoutControlItem31.CustomizationFormText = "Inventory Gain Account";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(522, 30);
            this.layoutControlItem31.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem31.Text = "Inventory Gain Account:";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.accountPlaceHolder35;
            this.layoutControlItem34.CustomizationFormText = "Inventory Loss Account";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(522, 30);
            this.layoutControlItem34.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem34.Text = "Inventory Loss Account:";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.accountPlaceHolder25;
            this.layoutControlItem39.CustomizationFormText = "Material Inflow Account:";
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(522, 30);
            this.layoutControlItem39.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem39.Text = "Material Inflow Account:";
            this.layoutControlItem39.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "Bank Accounts";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem24,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 380);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(540, 163);
            this.layoutControlGroup2.Text = "Bank and Cash Accounts";
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.accountPlaceHolder28;
            this.layoutControlItem24.CustomizationFormText = "Main Bank Account";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem24.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem24.Text = "Main Bank Account:";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.accountPlaceHolder29;
            this.layoutControlItem28.CustomizationFormText = "Bank Service Charge Account";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem28.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem28.Text = "Bank Service Charge Account:";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.accountPlaceHolder30;
            this.layoutControlItem29.CustomizationFormText = "Cash Account:";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem29.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem29.Text = "Cash Account:";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.accountPlaceHolder31;
            this.layoutControlItem30.CustomizationFormText = "Bank Service Charge by Cash";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem30.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem30.Text = "Bank Service Charge by Cash";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup6.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup6.CustomizationFormText = "Accounting Center";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem35,
            this.layoutControlItem36,
            this.layoutControlItem25,
            this.layoutControlItem53});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(540, 157);
            this.layoutControlGroup6.Text = "Cost Accounting";
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.costCenterPlaceHolder1;
            this.layoutControlItem35.CustomizationFormText = "Head Quarter Cost Center:";
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem35.Size = new System.Drawing.Size(516, 26);
            this.layoutControlItem35.Text = "Main Cost Center:";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.acHQRec;
            this.layoutControlItem36.CustomizationFormText = "HQ Receivable for Internal Services:";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem36.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem36.Text = "HQ Receivable for Internal Services:";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.acHQLiability;
            this.layoutControlItem25.CustomizationFormText = "HQ Liability for Internal Services:";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem25.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem25.Text = "HQ Liability for Internal Services:";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.Control = this.costCenterEmployee;
            this.layoutControlItem53.CustomizationFormText = "layoutControlItem53";
            this.layoutControlItem53.Location = new System.Drawing.Point(0, 86);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(516, 28);
            this.layoutControlItem53.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem53.Text = "layoutControlItem53";
            this.layoutControlItem53.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup7.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup7.CustomizationFormText = "Tax Accounts";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem14,
            this.layoutControlItem1,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem27,
            this.layoutControlItem33,
            this.layoutControlItem32,
            this.layoutControlItem11,
            this.layoutControlItem45,
            this.layoutControlItem46,
            this.layoutControlItem47,
            this.layoutControlItem51,
            this.layoutControlItem52});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 1014);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(540, 641);
            this.layoutControlGroup7.Text = "Tax Accounts";
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.accountPlaceHolder14;
            this.layoutControlItem14.CustomizationFormText = "Input VAT Account:";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem14.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem14.Text = "Input VAT Account:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.accountPlaceHolder1;
            this.layoutControlItem1.CustomizationFormText = "Accumulated VAT Receivable:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Text = "Accumulated VAT Receivable:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.accountPlaceHolder15;
            this.layoutControlItem15.CustomizationFormText = "Paid Withholding Tax Account:";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem15.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem15.Text = "Paid Withholding Tax Account:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.accountPlaceHolder16;
            this.layoutControlItem16.CustomizationFormText = "Out Put VAT Account:";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem16.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem16.Text = "Output VAT Account:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.accountPlaceHolder17;
            this.layoutControlItem17.CustomizationFormText = "Collected Withholding Tax Account:";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 180);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem17.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem17.Text = "Collected Withholding Tax Account:";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.accountPlaceHolder18;
            this.layoutControlItem18.CustomizationFormText = "Paid TOT Account:";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 300);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem18.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem18.Text = "TOT Expense Account:";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.accountPlaceHolder19;
            this.layoutControlItem19.CustomizationFormText = "Collected TOT Account:";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 330);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem19.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem19.Text = "Collected TOT Account:";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.accountPlaceHolder20;
            this.layoutControlItem20.CustomizationFormText = "Withholding Expense Account:";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 360);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(516, 28);
            this.layoutControlItem20.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem20.Text = "Withholding Expense Account:";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.accountPlaceHolder21;
            this.layoutControlItem21.CustomizationFormText = "Custom duty Tax Expense Account:";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 388);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem21.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem21.Text = "Custom duty Tax Expense Account:";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.accountPlaceHolder22;
            this.layoutControlItem22.CustomizationFormText = "Excise Tax Expense Account:";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 418);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem22.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem22.Text = "Excise Tax Expense Account:";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.accountPlaceHolder23;
            this.layoutControlItem23.CustomizationFormText = "SurTax Expense Account:";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 448);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem23.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem23.Text = "SurTax Expense Account:";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.accountPlaceHolder27;
            this.layoutControlItem27.CustomizationFormText = "Labor Income Tax Account:";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 478);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem27.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem27.Text = "Labor Income Tax Account:";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.accountPlaceHolder33;
            this.layoutControlItem33.CustomizationFormText = "Tax Exemption Account:";
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 508);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem33.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem33.Text = "Tax Exemption Account:";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.accountPlaceHolder32;
            this.layoutControlItem32.CustomizationFormText = "Tax Penality Account:";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 538);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem32.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem32.Text = "Tax Penality Account:";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.accountPlaceHolder11;
            this.layoutControlItem11.CustomizationFormText = "Tax Interest Account:";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 568);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem11.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem11.Text = "Tax Interest Account:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.Control = this.accountPlaceHolder41;
            this.layoutControlItem45.CustomizationFormText = "Withheld VAT Account:";
            this.layoutControlItem45.Location = new System.Drawing.Point(0, 240);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem45.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem45.Text = "Withheld VAT Account:";
            this.layoutControlItem45.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.Control = this.accountPlaceHolder42;
            this.layoutControlItem46.CustomizationFormText = "Witheld VAT (Not Invoiced)";
            this.layoutControlItem46.Location = new System.Drawing.Point(0, 270);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem46.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem46.Text = "Witheld VAT (Not Invoiced)";
            this.layoutControlItem46.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.Control = this.accountPlaceHolder43;
            this.layoutControlItem47.CustomizationFormText = "Collected Witholding Tax (Not Invoiced):";
            this.layoutControlItem47.Location = new System.Drawing.Point(0, 210);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem47.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem47.Text = "Collected Witholding Tax (Not Invoiced):";
            this.layoutControlItem47.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.Control = this.accountPlaceHolder45;
            this.layoutControlItem51.CustomizationFormText = "Output VAT Account (Not Invoiced):";
            this.layoutControlItem51.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem51.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem51.Text = "Output VAT Account (Not Invoiced):";
            this.layoutControlItem51.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.Control = this.accountPlaceHolder46;
            this.layoutControlItem52.CustomizationFormText = "Input VAT Account (Not Invoiced):";
            this.layoutControlItem52.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem52.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem52.Text = "Input VAT Account (Not Invoiced):";
            this.layoutControlItem52.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Others";
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem26,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutWithdrawal,
            this.layoutControlItem44,
            this.layoutControlItem49});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 1655);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(540, 223);
            this.layoutControlGroup8.Text = "Others";
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.accountPlaceHolder26;
            this.layoutControlItem26.CustomizationFormText = "Labor Expense Account:";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem26.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem26.Text = "Labor Expense Account:";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.accountPlaceHolder2;
            this.layoutControlItem2.CustomizationFormText = "Z-Report Taxable AccountID:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem2.Text = "Z-Report Taxable AccountID:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.accountPlaceHolder3;
            this.layoutControlItem3.CustomizationFormText = "Z-Report Non-Taxable Account ID:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "Z-Report Non-Taxable Account ID:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutWithdrawal
            // 
            this.layoutWithdrawal.Control = this.accountPlaceHolder24;
            this.layoutWithdrawal.CustomizationFormText = "Withdrawal Account ID:";
            this.layoutWithdrawal.Location = new System.Drawing.Point(0, 120);
            this.layoutWithdrawal.Name = "layoutWithdrawal";
            this.layoutWithdrawal.Size = new System.Drawing.Size(516, 30);
            this.layoutWithdrawal.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutWithdrawal.Text = "Withdrawal Account ID:";
            this.layoutWithdrawal.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.Control = this.accountPlaceHolder40;
            this.layoutControlItem44.CustomizationFormText = "Other Income Account:";
            this.layoutControlItem44.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem44.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem44.Text = "Other Income Account:";
            this.layoutControlItem44.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.Control = this.accountPlaceHolder44;
            this.layoutControlItem49.CustomizationFormText = "Closing Capital Account";
            this.layoutControlItem49.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem49.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem49.Text = "Closing Capital Account";
            this.layoutControlItem49.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup9.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup9.CustomizationFormText = "Root Accounts";
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem40,
            this.layoutControlItem41,
            this.layoutControlItem42,
            this.layoutControlItem43});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 157);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(540, 223);
            this.layoutControlGroup9.Text = "Root Accounts";
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.accountPlaceHolder12;
            this.layoutControlItem12.CustomizationFormText = "Asset Account:";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem12.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem12.Text = "Asset Account:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.accountPlaceHolder13;
            this.layoutControlItem13.CustomizationFormText = "Liability Account:";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem13.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem13.Text = "Liability Account:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.accountPlaceHolder36;
            this.layoutControlItem40.CustomizationFormText = "Capital Account:";
            this.layoutControlItem40.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem40.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem40.Text = "Capital Account:";
            this.layoutControlItem40.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.Control = this.accountPlaceHolder37;
            this.layoutControlItem41.CustomizationFormText = "Income Account:";
            this.layoutControlItem41.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem41.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem41.Text = "Income Account:";
            this.layoutControlItem41.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.Control = this.accountPlaceHolder38;
            this.layoutControlItem42.CustomizationFormText = "Cost Account:";
            this.layoutControlItem42.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem42.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem42.Text = "Cost Account:";
            this.layoutControlItem42.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.Control = this.accountPlaceHolder39;
            this.layoutControlItem43.CustomizationFormText = "Expense Account:";
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem43.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem43.Text = "Expense Account:";
            this.layoutControlItem43.TextSize = new System.Drawing.Size(206, 13);
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.Control = this.accountPlaceHolder24;
            this.layoutControlItem48.CustomizationFormText = "Withdrawal Account ID:";
            this.layoutControlItem48.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem48.Name = "layoutWithdrawal";
            this.layoutControlItem48.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem48.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem48.Text = "Withdrawal Account ID:";
            this.layoutControlItem48.TextSize = new System.Drawing.Size(206, 13);
            this.layoutControlItem48.TextToControlDistance = 5;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.Control = this.accountPlaceHolder16;
            this.layoutControlItem50.CustomizationFormText = "Out Put VAT Account:";
            this.layoutControlItem50.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem50.Name = "layoutControlItem16";
            this.layoutControlItem50.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem50.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem50.Text = "Out Put VAT Account:";
            this.layoutControlItem50.TextSize = new System.Drawing.Size(206, 13);
            this.layoutControlItem50.TextToControlDistance = 5;
            // 
            // AccountConfigurationPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "AccountConfigurationPage";
            this.Size = new System.Drawing.Size(563, 996);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWithdrawal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder7;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder17;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder16;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder15;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder19;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder23;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder33;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder3;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder24;
        internal DevExpress.XtraLayout.LayoutControlItem layoutWithdrawal;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder29;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder28;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder35;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private INTAPS.Accounting.Client.CostCenterPlaceHolder costCenterPlaceHolder1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private INTAPS.Accounting.Client.AccountPlaceHolder acHQRec;
        private INTAPS.Accounting.Client.AccountPlaceHolder acHQLiability;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private INTAPS.Accounting.Client.CostCenterPlaceHolder costCenterPlaceHolder3;
        private INTAPS.Accounting.Client.CostCenterPlaceHolder costCenterPlaceHolder2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder39;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder38;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder37;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder36;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder13;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder43;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder42;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        internal DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder45;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private INTAPS.Accounting.Client.CostCenterPlaceHolder costCenterEmployee;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
    }
}
