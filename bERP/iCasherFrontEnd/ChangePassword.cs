﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BIZNET.iERP.Client
{
    public partial class ChangePassword : DevExpress.XtraEditors.XtraForm
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            string p1 = textPassword1.Text;
            string p2 = textPassword2.Text;
            if (!p1.Equals(p2))
            {
                MessageBox.ShowErrorMessage("Password not confirmed correctly");
                return;
            }
            try
            {
                iERPTransactionClient.changePassword(p1);
                MessageBox.ShowSuccessMessage("Password changed successfully!");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}