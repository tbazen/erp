﻿namespace BIZNET.iERP.Client
{
    partial class CodeFormatConfigurationPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtCashAccSuffDigits = new DevExpress.XtraEditors.TextEdit();
            this.txtBankAccSufDigits = new DevExpress.XtraEditors.TextEdit();
            this.txtAttachFormat = new DevExpress.XtraEditors.TextEdit();
            this.txtSalesItemAccCodeSuffDig = new DevExpress.XtraEditors.TextEdit();
            this.txtSalesItemCodeDig = new DevExpress.XtraEditors.TextEdit();
            this.txtExpItemAccCodeSuffDig = new DevExpress.XtraEditors.TextEdit();
            this.txtExpItemCodeDig = new DevExpress.XtraEditors.TextEdit();
            this.txtFixedAssAccCodeDig = new DevExpress.XtraEditors.TextEdit();
            this.txtFixedAssCodeDig = new DevExpress.XtraEditors.TextEdit();
            this.txtCustAccCodeDig = new DevExpress.XtraEditors.TextEdit();
            this.txtCustCodeDig = new DevExpress.XtraEditors.TextEdit();
            this.txtSuppAccCodeDig = new DevExpress.XtraEditors.TextEdit();
            this.txtSuppCodeDig = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tx = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.validateCodeFormat = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCashAccSuffDigits.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccSufDigits.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttachFormat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesItemAccCodeSuffDig.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesItemCodeDig.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpItemAccCodeSuffDig.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpItemCodeDig.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedAssAccCodeDig.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedAssCodeDig.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustAccCodeDig.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustCodeDig.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuppAccCodeDig.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuppCodeDig.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validateCodeFormat)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtCashAccSuffDigits);
            this.layoutControl1.Controls.Add(this.txtBankAccSufDigits);
            this.layoutControl1.Controls.Add(this.txtAttachFormat);
            this.layoutControl1.Controls.Add(this.txtSalesItemAccCodeSuffDig);
            this.layoutControl1.Controls.Add(this.txtSalesItemCodeDig);
            this.layoutControl1.Controls.Add(this.txtExpItemAccCodeSuffDig);
            this.layoutControl1.Controls.Add(this.txtExpItemCodeDig);
            this.layoutControl1.Controls.Add(this.txtFixedAssAccCodeDig);
            this.layoutControl1.Controls.Add(this.txtFixedAssCodeDig);
            this.layoutControl1.Controls.Add(this.txtCustAccCodeDig);
            this.layoutControl1.Controls.Add(this.txtCustCodeDig);
            this.layoutControl1.Controls.Add(this.txtSuppAccCodeDig);
            this.layoutControl1.Controls.Add(this.txtSuppCodeDig);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(514, 139, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(521, 647);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtCashAccSuffDigits
            // 
            this.txtCashAccSuffDigits.Location = new System.Drawing.Point(231, 76);
            this.txtCashAccSuffDigits.Name = "txtCashAccSuffDigits";
            this.txtCashAccSuffDigits.Properties.Mask.EditMask = "\\w*";
            this.txtCashAccSuffDigits.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtCashAccSuffDigits.Size = new System.Drawing.Size(263, 20);
            this.txtCashAccSuffDigits.StyleController = this.layoutControl1;
            this.txtCashAccSuffDigits.TabIndex = 16;
            // 
            // txtBankAccSufDigits
            // 
            this.txtBankAccSufDigits.Location = new System.Drawing.Point(231, 46);
            this.txtBankAccSufDigits.Name = "txtBankAccSufDigits";
            this.txtBankAccSufDigits.Properties.Mask.EditMask = "\\w*";
            this.txtBankAccSufDigits.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtBankAccSufDigits.Size = new System.Drawing.Size(263, 20);
            this.txtBankAccSufDigits.StyleController = this.layoutControl1;
            this.txtBankAccSufDigits.TabIndex = 15;
            // 
            // txtAttachFormat
            // 
            this.txtAttachFormat.Location = new System.Drawing.Point(219, 609);
            this.txtAttachFormat.Name = "txtAttachFormat";
            this.txtAttachFormat.Properties.Mask.EditMask = "\\w*";
            this.txtAttachFormat.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtAttachFormat.Size = new System.Drawing.Size(287, 20);
            this.txtAttachFormat.StyleController = this.layoutControl1;
            this.txtAttachFormat.TabIndex = 14;
            // 
            // txtSalesItemAccCodeSuffDig
            // 
            this.txtSalesItemAccCodeSuffDig.Location = new System.Drawing.Point(228, 570);
            this.txtSalesItemAccCodeSuffDig.Name = "txtSalesItemAccCodeSuffDig";
            this.txtSalesItemAccCodeSuffDig.Properties.Mask.EditMask = "\\w*";
            this.txtSalesItemAccCodeSuffDig.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtSalesItemAccCodeSuffDig.Size = new System.Drawing.Size(269, 20);
            this.txtSalesItemAccCodeSuffDig.StyleController = this.layoutControl1;
            this.txtSalesItemAccCodeSuffDig.TabIndex = 13;
            // 
            // txtSalesItemCodeDig
            // 
            this.txtSalesItemCodeDig.Location = new System.Drawing.Point(228, 540);
            this.txtSalesItemCodeDig.Name = "txtSalesItemCodeDig";
            this.txtSalesItemCodeDig.Properties.Mask.EditMask = "\\w*";
            this.txtSalesItemCodeDig.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtSalesItemCodeDig.Size = new System.Drawing.Size(269, 20);
            this.txtSalesItemCodeDig.StyleController = this.layoutControl1;
            this.txtSalesItemCodeDig.TabIndex = 12;
            // 
            // txtExpItemAccCodeSuffDig
            // 
            this.txtExpItemAccCodeSuffDig.Location = new System.Drawing.Point(228, 473);
            this.txtExpItemAccCodeSuffDig.Name = "txtExpItemAccCodeSuffDig";
            this.txtExpItemAccCodeSuffDig.Properties.Mask.EditMask = "\\w*";
            this.txtExpItemAccCodeSuffDig.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtExpItemAccCodeSuffDig.Size = new System.Drawing.Size(269, 20);
            this.txtExpItemAccCodeSuffDig.StyleController = this.layoutControl1;
            this.txtExpItemAccCodeSuffDig.TabIndex = 11;
            // 
            // txtExpItemCodeDig
            // 
            this.txtExpItemCodeDig.Location = new System.Drawing.Point(228, 443);
            this.txtExpItemCodeDig.Name = "txtExpItemCodeDig";
            this.txtExpItemCodeDig.Properties.Mask.EditMask = "\\w*";
            this.txtExpItemCodeDig.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtExpItemCodeDig.Size = new System.Drawing.Size(269, 20);
            this.txtExpItemCodeDig.StyleController = this.layoutControl1;
            this.txtExpItemCodeDig.TabIndex = 10;
            // 
            // txtFixedAssAccCodeDig
            // 
            this.txtFixedAssAccCodeDig.Location = new System.Drawing.Point(231, 373);
            this.txtFixedAssAccCodeDig.Name = "txtFixedAssAccCodeDig";
            this.txtFixedAssAccCodeDig.Properties.Mask.EditMask = "\\w*";
            this.txtFixedAssAccCodeDig.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtFixedAssAccCodeDig.Size = new System.Drawing.Size(263, 20);
            this.txtFixedAssAccCodeDig.StyleController = this.layoutControl1;
            this.txtFixedAssAccCodeDig.TabIndex = 9;
            // 
            // txtFixedAssCodeDig
            // 
            this.txtFixedAssCodeDig.Location = new System.Drawing.Point(231, 343);
            this.txtFixedAssCodeDig.Name = "txtFixedAssCodeDig";
            this.txtFixedAssCodeDig.Properties.Mask.EditMask = "\\w*";
            this.txtFixedAssCodeDig.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtFixedAssCodeDig.Size = new System.Drawing.Size(263, 20);
            this.txtFixedAssCodeDig.StyleController = this.layoutControl1;
            this.txtFixedAssCodeDig.TabIndex = 8;
            // 
            // txtCustAccCodeDig
            // 
            this.txtCustAccCodeDig.Location = new System.Drawing.Point(228, 273);
            this.txtCustAccCodeDig.Name = "txtCustAccCodeDig";
            this.txtCustAccCodeDig.Properties.Mask.EditMask = "\\w*";
            this.txtCustAccCodeDig.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtCustAccCodeDig.Size = new System.Drawing.Size(269, 20);
            this.txtCustAccCodeDig.StyleController = this.layoutControl1;
            this.txtCustAccCodeDig.TabIndex = 7;
            // 
            // txtCustCodeDig
            // 
            this.txtCustCodeDig.Location = new System.Drawing.Point(228, 243);
            this.txtCustCodeDig.Name = "txtCustCodeDig";
            this.txtCustCodeDig.Properties.Mask.EditMask = "\\w*";
            this.txtCustCodeDig.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtCustCodeDig.Size = new System.Drawing.Size(269, 20);
            this.txtCustCodeDig.StyleController = this.layoutControl1;
            this.txtCustCodeDig.TabIndex = 6;
            // 
            // txtSuppAccCodeDig
            // 
            this.txtSuppAccCodeDig.Location = new System.Drawing.Point(228, 176);
            this.txtSuppAccCodeDig.Name = "txtSuppAccCodeDig";
            this.txtSuppAccCodeDig.Properties.Mask.EditMask = "\\w*";
            this.txtSuppAccCodeDig.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtSuppAccCodeDig.Properties.Mask.ShowPlaceHolders = false;
            this.txtSuppAccCodeDig.Size = new System.Drawing.Size(269, 20);
            this.txtSuppAccCodeDig.StyleController = this.layoutControl1;
            this.txtSuppAccCodeDig.TabIndex = 5;
            // 
            // txtSuppCodeDig
            // 
            this.txtSuppCodeDig.Location = new System.Drawing.Point(228, 146);
            this.txtSuppCodeDig.Name = "txtSuppCodeDig";
            this.txtSuppCodeDig.Properties.Mask.EditMask = "\\w*";
            this.txtSuppCodeDig.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtSuppCodeDig.Properties.Mask.ShowPlaceHolders = false;
            this.txtSuppCodeDig.Size = new System.Drawing.Size(269, 20);
            this.txtSuppCodeDig.StyleController = this.layoutControl1;
            this.txtSuppCodeDig.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.layoutControlGroup6,
            this.layoutControlItem10,
            this.layoutControlGroup7});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(521, 647);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "TradeRelation Code Format";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 103);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(6, 6, 6, 6);
            this.layoutControlGroup2.Size = new System.Drawing.Size(501, 97);
            this.layoutControlGroup2.Text = "Supplier Code Format";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtSuppCodeDig;
            this.layoutControlItem1.CustomizationFormText = "TradeRelation Code Digits:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(483, 30);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Text = "Supplier Code Digits:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(200, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtSuppAccCodeDig;
            this.layoutControlItem2.CustomizationFormText = "Supplier Account Code Suffix Digits:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(483, 30);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem2.Text = "Supplier Account Code Suffix Digits:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(200, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.CustomizationFormText = "TradeRelation Code Format";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 200);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(6, 6, 6, 6);
            this.layoutControlGroup3.Size = new System.Drawing.Size(501, 97);
            this.layoutControlGroup3.Text = "Customer Code Format";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtCustAccCodeDig;
            this.layoutControlItem4.CustomizationFormText = "TradeRelation Account Code Suffix Digits:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(483, 30);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem4.Text = "Customer Account Code Suffix Digits:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(200, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtCustCodeDig;
            this.layoutControlItem3.CustomizationFormText = "TradeRelation Code Digits:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(483, 30);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "Customer Code Digits:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(200, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup4.CustomizationFormText = "Fixed Asset Code Format";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.tx});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 297);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(501, 103);
            this.layoutControlGroup4.Text = "Fixed Asset Code Format";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtFixedAssCodeDig;
            this.layoutControlItem5.CustomizationFormText = "Fixed Asset Code Digits:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(477, 30);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem5.Text = "Fixed Asset Code Digits:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(200, 13);
            // 
            // tx
            // 
            this.tx.Control = this.txtFixedAssAccCodeDig;
            this.tx.CustomizationFormText = "Fixed Asset Account Code Suffix Digits:";
            this.tx.Location = new System.Drawing.Point(0, 30);
            this.tx.Name = "tx";
            this.tx.Size = new System.Drawing.Size(477, 30);
            this.tx.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.tx.Text = "Fixed Asset Account Code Suffix Digits:";
            this.tx.TextSize = new System.Drawing.Size(200, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup5.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup5.CustomizationFormText = "Expense Item Code Format";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem7});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 400);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(6, 6, 6, 6);
            this.layoutControlGroup5.Size = new System.Drawing.Size(501, 97);
            this.layoutControlGroup5.Text = "Expense Item Code Format";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtExpItemCodeDig;
            this.layoutControlItem6.CustomizationFormText = "Expense Item Code Digits:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(483, 30);
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem6.Text = "Expense Item Code Digits:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(200, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtExpItemAccCodeSuffDig;
            this.layoutControlItem7.CustomizationFormText = "Expense Item Account Code Suffix Digits:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(483, 30);
            this.layoutControlItem7.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem7.Text = "Expense Item Account Code Suffix Digits:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(200, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup6.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup6.CustomizationFormText = "Saels Item Code Format";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 497);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(6, 6, 6, 6);
            this.layoutControlGroup6.Size = new System.Drawing.Size(501, 97);
            this.layoutControlGroup6.Text = "Saels Item Code Format";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtSalesItemCodeDig;
            this.layoutControlItem8.CustomizationFormText = "Sales Item Code Digits:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(483, 30);
            this.layoutControlItem8.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem8.Text = "Sales Item Code Digits:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(200, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtSalesItemAccCodeSuffDig;
            this.layoutControlItem9.CustomizationFormText = "Sales Item Account Code Suffix Digits:";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(483, 30);
            this.layoutControlItem9.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem9.Text = "Sales Item Account Code Suffix Digits:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(200, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txtAttachFormat;
            this.layoutControlItem10.CustomizationFormText = "Attachment Number Format (Digits):";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 594);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(501, 33);
            this.layoutControlItem10.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem10.Text = "Attachment Number Format (Digits):";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(200, 13);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup7.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup7.CustomizationFormText = "Bank and Cash Code Format";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11,
            this.layoutControlItem12});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(501, 103);
            this.layoutControlGroup7.Text = "Bank and Cash Code Format";
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txtBankAccSufDigits;
            this.layoutControlItem11.CustomizationFormText = "Bank Account Code Suffix Digits:";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(477, 30);
            this.layoutControlItem11.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem11.Text = "Bank Account Code Suffix Digits:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(200, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.txtCashAccSuffDigits;
            this.layoutControlItem12.CustomizationFormText = "Cash Account Code Suffix Digits:";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(477, 30);
            this.layoutControlItem12.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem12.Text = "Cash Account Code Suffix Digits:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(200, 13);
            // 
            // validateCodeFormat
            // 
            this.validateCodeFormat.ValidateHiddenControls = false;
            // 
            // CodeFormatConfigurationPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "CodeFormatConfigurationPage";
            this.Size = new System.Drawing.Size(521, 647);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCashAccSuffDigits.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccSufDigits.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttachFormat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesItemAccCodeSuffDig.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesItemCodeDig.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpItemAccCodeSuffDig.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpItemCodeDig.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedAssAccCodeDig.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFixedAssCodeDig.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustAccCodeDig.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustCodeDig.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuppAccCodeDig.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuppCodeDig.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validateCodeFormat)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        internal DevExpress.XtraEditors.TextEdit txtSuppCodeDig;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        internal DevExpress.XtraEditors.TextEdit txtCustCodeDig;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        internal DevExpress.XtraEditors.TextEdit txtCustAccCodeDig;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem tx;
        internal DevExpress.XtraEditors.TextEdit txtFixedAssAccCodeDig;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        internal DevExpress.XtraEditors.TextEdit txtExpItemCodeDig;
        internal DevExpress.XtraEditors.TextEdit txtExpItemAccCodeSuffDig;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        internal DevExpress.XtraEditors.TextEdit txtSalesItemCodeDig;
        internal DevExpress.XtraEditors.TextEdit txtSalesItemAccCodeSuffDig;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        internal DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validateCodeFormat;
        internal DevExpress.XtraEditors.TextEdit txtSuppAccCodeDig;
        internal DevExpress.XtraEditors.TextEdit txtFixedAssCodeDig;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        internal DevExpress.XtraEditors.TextEdit txtAttachFormat;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        internal DevExpress.XtraEditors.TextEdit txtCashAccSuffDigits;
        internal DevExpress.XtraEditors.TextEdit txtBankAccSufDigits;
    }
}
