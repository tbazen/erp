﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;

namespace BIZNET.iERP.Client
{
    public partial class CodeFormatConfigurationPage : DevExpress.XtraEditors.XtraUserControl
    {
        public CodeFormatConfigurationPage()
        {
            InitializeComponent();
            InitializeCodeFormatValidation();
            InitializeFocusLostEvents();
        }
        private void InitializeCodeFormatValidation()
        {
            CodeFormatValidationRule formatValidation = new CodeFormatValidationRule();
            formatValidation.ErrorText = "Wrong format. Please enter number of digits using '0'. E.g Entering '0000' without the quotes implies 4 digits";
            formatValidation.ErrorType = ErrorType.Default;
            SetValidationRules(formatValidation);
        }

        private void SetValidationRules(CodeFormatValidationRule formatValidation)
        {
            validateCodeFormat.SetValidationRule(txtBankAccSufDigits, formatValidation);
            validateCodeFormat.SetValidationRule(txtCashAccSuffDigits, formatValidation);
            //validateCodeFormat.SetValidationRule(txtSuppCodeDig, formatValidation);
            //validateCodeFormat.SetValidationRule(txtSuppAccCodeDig, formatValidation);
            //validateCodeFormat.SetValidationRule(txtCustCodeDig, formatValidation);
            //validateCodeFormat.SetValidationRule(txtCustAccCodeDig, formatValidation);
            //validateCodeFormat.SetValidationRule(txtFixedAssCodeDig, formatValidation);
            //validateCodeFormat.SetValidationRule(txtFixedAssAccCodeDig, formatValidation);
            //validateCodeFormat.SetValidationRule(txtExpItemCodeDig, formatValidation);
            //validateCodeFormat.SetValidationRule(txtExpItemAccCodeSuffDig, formatValidation);
            //validateCodeFormat.SetValidationRule(txtSalesItemCodeDig, formatValidation);
            //validateCodeFormat.SetValidationRule(txtSalesItemAccCodeSuffDig, formatValidation);
            //validateCodeFormat.SetValidationRule(txtAttachFormat, formatValidation);
        }
        private void InitializeFocusLostEvents()
        {
            txtBankAccSufDigits.LostFocus += Control_LostFocus;
            txtCashAccSuffDigits.LostFocus += Control_LostFocus;
            //txtCustCodeDig.LostFocus += Control_LostFocus;
            //txtCustAccCodeDig.LostFocus += Control_LostFocus;
            //txtFixedAssCodeDig.LostFocus += Control_LostFocus;
            //txtFixedAssAccCodeDig.LostFocus += Control_LostFocus;
            //txtExpItemCodeDig.LostFocus += Control_LostFocus;
            //txtExpItemAccCodeSuffDig.LostFocus += Control_LostFocus;
            //txtSalesItemCodeDig.LostFocus += Control_LostFocus;
            //txtSalesItemAccCodeSuffDig.LostFocus += Control_LostFocus;
            //txtAttachFormat.LostFocus += Control_LostFocus;


        }

        void Control_LostFocus(object sender, EventArgs e)
        {
            validateCodeFormat.Validate((Control)sender);
        }
    }
}
