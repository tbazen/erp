﻿namespace BIZNET.iERP.Client
{
    partial class iERPConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navBarControl1 = new DevExpress.XtraNavBar.NavBarControl();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navStaffAccConfig = new DevExpress.XtraNavBar.NavBarItem();
            this.navAccountConfig = new DevExpress.XtraNavBar.NavBarItem();
            this.navVATConfig = new DevExpress.XtraNavBar.NavBarItem();
            this.navTOTConfig = new DevExpress.XtraNavBar.NavBarItem();
            this.navWithHolding = new DevExpress.XtraNavBar.NavBarItem();
            this.navPension = new DevExpress.XtraNavBar.NavBarItem();
            this.navAccCodeConfig = new DevExpress.XtraNavBar.NavBarItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnResetToDefault = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.panConfigPages = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panConfigPages)).BeginInit();
            this.SuspendLayout();
            // 
            // navBarControl1
            // 
            this.navBarControl1.ActiveGroup = this.navBarGroup1;
            this.navBarControl1.AllowSelectedLink = true;
            this.navBarControl1.Appearance.GroupHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.navBarControl1.Appearance.GroupHeader.Options.UseFont = true;
            this.navBarControl1.Appearance.GroupHeaderActive.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.navBarControl1.Appearance.GroupHeaderActive.Options.UseFont = true;
            this.navBarControl1.Appearance.NavigationPaneHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.navBarControl1.Appearance.NavigationPaneHeader.ForeColor = System.Drawing.Color.Black;
            this.navBarControl1.Appearance.NavigationPaneHeader.Options.UseFont = true;
            this.navBarControl1.Appearance.NavigationPaneHeader.Options.UseForeColor = true;
            this.navBarControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.navBarControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.navBarControl1.ExplorerBarShowGroupButtons = false;
            this.navBarControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.navBarControl1.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup1});
            this.navBarControl1.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.navAccountConfig,
            this.navVATConfig,
            this.navTOTConfig,
            this.navAccCodeConfig,
            this.navWithHolding,
            this.navPension,
            this.navStaffAccConfig});
            this.navBarControl1.Location = new System.Drawing.Point(0, 0);
            this.navBarControl1.Name = "navBarControl1";
            this.navBarControl1.OptionsNavPane.ExpandedWidth = 219;
            this.navBarControl1.OptionsNavPane.ShowExpandButton = false;
            this.navBarControl1.OptionsNavPane.ShowOverflowButton = false;
            this.navBarControl1.OptionsNavPane.ShowOverflowPanel = false;
            this.navBarControl1.OptionsNavPane.ShowSplitter = false;
            this.navBarControl1.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.NavigationPane;
            this.navBarControl1.Size = new System.Drawing.Size(219, 626);
            this.navBarControl1.StoreDefaultPaintStyleName = true;
            this.navBarControl1.TabIndex = 5;
            this.navBarControl1.Text = "navBarControl1";
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.navBarGroup1.Appearance.Options.UseFont = true;
            this.navBarGroup1.Caption = "Select Setting";
            this.navBarGroup1.DragDropFlags = DevExpress.XtraNavBar.NavBarDragDrop.None;
            this.navBarGroup1.Expanded = true;
            this.navBarGroup1.GroupCaptionUseImage = DevExpress.XtraNavBar.NavBarImage.Small;
            this.navBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.SmallIconsText;
            this.navBarGroup1.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navStaffAccConfig),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navAccountConfig),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navVATConfig),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navTOTConfig),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navWithHolding),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navPension),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navAccCodeConfig)});
            this.navBarGroup1.Name = "navBarGroup1";
            this.navBarGroup1.NavigationPaneVisible = false;
            this.navBarGroup1.SelectedLinkIndex = 0;
            this.navBarGroup1.SmallImageIndex = 0;
            // 
            // navStaffAccConfig
            // 
            this.navStaffAccConfig.Caption = "Staff Account Configuration";
            this.navStaffAccConfig.Name = "navStaffAccConfig";
            this.navStaffAccConfig.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navStaffAccConfig_LinkClicked);
            // 
            // navAccountConfig
            // 
            this.navAccountConfig.CanDrag = false;
            this.navAccountConfig.Caption = "Miscellaneous Account Configuration";
            this.navAccountConfig.Name = "navAccountConfig";
            this.navAccountConfig.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navAccountConfig_LinkClicked);
            // 
            // navVATConfig
            // 
            this.navVATConfig.CanDrag = false;
            this.navVATConfig.Caption = "VAT (Value Added Tax) Configuration";
            this.navVATConfig.Name = "navVATConfig";
            this.navVATConfig.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navVATConfig_LinkClicked);
            // 
            // navTOTConfig
            // 
            this.navTOTConfig.CanDrag = false;
            this.navTOTConfig.Caption = "TOT (Turn Over Tax) Configuration";
            this.navTOTConfig.Name = "navTOTConfig";
            this.navTOTConfig.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navTOTConfig_LinkClicked);
            // 
            // navWithHolding
            // 
            this.navWithHolding.CanDrag = false;
            this.navWithHolding.Caption = "With-Holding Tax Configuration";
            this.navWithHolding.Name = "navWithHolding";
            this.navWithHolding.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navWithHolding_LinkClicked);
            // 
            // navPension
            // 
            this.navPension.CanDrag = false;
            this.navPension.Caption = "Payroll Configuration";
            this.navPension.Name = "navPension";
            this.navPension.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navPension_LinkClicked);
            // 
            // navAccCodeConfig
            // 
            this.navAccCodeConfig.CanDrag = false;
            this.navAccCodeConfig.Caption = "Code Format Configuration";
            this.navAccCodeConfig.Name = "navAccCodeConfig";
            this.navAccCodeConfig.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navAccCodeConfig_LinkClicked);
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnClose);
            this.panelControl1.Controls.Add(this.btnResetToDefault);
            this.panelControl1.Controls.Add(this.btnSave);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(219, 595);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(504, 31);
            this.panelControl1.TabIndex = 6;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(426, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnResetToDefault
            // 
            this.btnResetToDefault.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetToDefault.Location = new System.Drawing.Point(6, 3);
            this.btnResetToDefault.Name = "btnResetToDefault";
            this.btnResetToDefault.Size = new System.Drawing.Size(149, 23);
            this.btnResetToDefault.TabIndex = 0;
            this.btnResetToDefault.Text = "&Reset to default settings";
            this.btnResetToDefault.Visible = false;
            this.btnResetToDefault.Click += new System.EventHandler(this.btnResetToDefault_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(336, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panConfigPages
            // 
            this.panConfigPages.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panConfigPages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panConfigPages.Location = new System.Drawing.Point(219, 0);
            this.panConfigPages.Name = "panConfigPages";
            this.panConfigPages.Size = new System.Drawing.Size(504, 595);
            this.panConfigPages.TabIndex = 7;
            // 
            // iERPConfiguration
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(723, 626);
            this.Controls.Add(this.panConfigPages);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.navBarControl1);
            this.Name = "iERPConfiguration";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panConfigPages)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraNavBar.NavBarControl navBarControl1;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraNavBar.NavBarItem navAccountConfig;
        private DevExpress.XtraNavBar.NavBarItem navVATConfig;
        private DevExpress.XtraNavBar.NavBarItem navTOTConfig;
        private DevExpress.XtraNavBar.NavBarItem navAccCodeConfig;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.PanelControl panConfigPages;
        private DevExpress.XtraNavBar.NavBarItem navWithHolding;
        private DevExpress.XtraEditors.SimpleButton btnResetToDefault;
        private DevExpress.XtraNavBar.NavBarItem navPension;
        private DevExpress.XtraNavBar.NavBarItem navStaffAccConfig;

    }
}