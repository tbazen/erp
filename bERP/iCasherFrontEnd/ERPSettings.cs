﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using DevExpress.XtraEditors;
using INTAPS.Payroll.Client;
using DevExpress.XtraLayout;

namespace BIZNET.iERP.Client
{
    public partial class iERPConfiguration : XtraForm
    {
        private AccountConfigurationPage m_AccountConfigPage;
        private int m_AttachmentNumber;
        private StaffAccountConfigurationPage m_staffAccConfigPage;
        private VATConfigurationPage m_VATConfigPage;
        private TOTConfigurationPage m_TOTConfigPage;
        private WithHoldingConfigurationPage m_WithHoldingConfigPage;
        private PayrollConfigurationPage m_PayrollConfigPage;
        private CodeFormatConfigurationPage m_CodeFormatConfigPage;
        public iERPConfiguration()
        {
            InitializeComponent();
            m_AccountConfigPage = new AccountConfigurationPage();
            m_staffAccConfigPage = new StaffAccountConfigurationPage();
            m_VATConfigPage = new VATConfigurationPage();
            m_TOTConfigPage = new TOTConfigurationPage();
            m_WithHoldingConfigPage = new WithHoldingConfigurationPage();
            m_PayrollConfigPage = new PayrollConfigurationPage();
            m_CodeFormatConfigPage = new CodeFormatConfigurationPage();
            //AddAccountConfigPage();
            bool shareHolderLoan = ShareHoldersLoanVisible();
            if (shareHolderLoan)
                m_AccountConfigPage.layoutWithdrawal.HideToCustomization();
            else
            {
                m_staffAccConfigPage.layoutShareHoldersLoan.HideToCustomization();
                m_staffAccConfigPage.layoutOwnerEquity.HideToCustomization();
            }
            AddStaffAccountConfigPage();
            LoadStaffAccountConfigPage();
            LoadAccountConfigurations();
            LoadVATConfigurations();
            LoadTOTConfigurations();
            LoadWithHoldingConfigurations();
            LoadPayrollConfigurations();
            LoadCodeFormatConfigurations();
        }

        private bool ShareHoldersLoanVisible()
        {
            bool showShareHolderLoan = false;
            CompanyProfile profile = (CompanyProfile)iERPTransactionClient.GetSystemParamter("companyProfile");
            if (profile == null)
            {
                MessageBox.ShowErrorMessage("Company profile not found! Please configure your company profile and try again!");
            }
            else
            {
                switch (profile.BusinessEntity)
                {
                    case BusinessEntity.SoleProprietorship:
                        showShareHolderLoan = false;
                        break;
                    case BusinessEntity.Partnership:
                    case BusinessEntity.GeneralPartnership:
                    case BusinessEntity.LimitedPartnership:
                    case BusinessEntity.ShareCompany:
                    case BusinessEntity.PrivateLimitedCompany:
                    case BusinessEntity.JointVenture:
                        showShareHolderLoan = true;
                        break;
                    default:
                        break;
                }
            }
            return showShareHolderLoan;
        }

        private void AddStaffAccountConfigPage()
        {
            panConfigPages.Controls.Clear();
            panConfigPages.Controls.Add(m_staffAccConfigPage);
            m_staffAccConfigPage.Dock = DockStyle.Fill;
        }

        private void LoadStaffAccountConfigPage()
        {
            List<string> payrollSettingNames = new List<string>();
            foreach (Control c in m_staffAccConfigPage.Controls[0].Controls)
            {
                if (c is AccountPlaceHolder || c is CostCenterPlaceHolder)
                    payrollSettingNames.Add((string)c.Tag);
            }
            object[] payrollPars = PayrollClient.GetSystemParameters(payrollSettingNames.ToArray());

            int i = 0;
            foreach (Control c in m_staffAccConfigPage.Controls[0].Controls)
            {
                if (c is AccountPlaceHolder)
                {
                    ((AccountPlaceHolder)c).SetByID((int)payrollPars[i]);
                    i++;
                }
                else if (c is CostCenterPlaceHolder)
                {
                    ((CostCenterPlaceHolder)c).SetByID((int)payrollPars[i]);
                    i++;
                }
            }
        }
        private  void LoadAccountConfigurations()
        {
            List<string> settingNames = new List<string>();
          
            foreach (Control c in m_AccountConfigPage.Controls[0].Controls)
            {
                if (c is AccountPlaceHolder || c is CostCenterPlaceHolder)
                    settingNames.Add((string)c.Tag);
            }

            object[] pars = iERPTransactionClient.GetSystemParameters(settingNames.ToArray());
            
            int i = 0;
            foreach (Control c in m_AccountConfigPage.Controls[0].Controls)
            {
                if (c is AccountPlaceHolder)
                {
                    ((AccountPlaceHolder)c).SetByID(Convert.ToInt32(pars[i]));
                    i++;
                }
                else if (c is CostCenterPlaceHolder)
                {
                    ((CostCenterPlaceHolder)c).SetByID(Convert.ToInt32(pars[i]));
                    i++;
                }
            }
        }

        private void LoadVATConfigurations()
        {
            object[] values = iERPTransactionClient.GetSystemParameters(new string[] { "CommonVATRate", "ExportVATRate", "RawHideExportVATRate","VATWithholdingNonTaxablePriceLimit" });

            if (values != null)
            {
                if (values.Length > 0)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (i == 0) //Common VAT
                        {
                            if ((double)values[i] != 0)
                                m_VATConfigPage.txtCommonVAT.Text = values[i].ToString();
                            else
                                m_VATConfigPage.txtCommonVAT.Text = 0.15.ToString();
                        }
                        else if (i == 1) //Export VAT
                        {
                            if ((double)values[i] != 0)
                                m_VATConfigPage.txtExportVAT.Text = values[i].ToString();
                            else
                                m_VATConfigPage.txtExportVAT.Text = 0.ToString();
                        }
                        else if (i == 2) //Raw Hide Export VAT
                        {
                            if ((double)values[i] != 0)
                                m_VATConfigPage.txtRawHideExportVAT.Text = values[i].ToString();
                            else
                                m_VATConfigPage.txtRawHideExportVAT.Text = 1.5.ToString();
                        }
                        else if (i == 3) //VAT Withholding
                        {
                            if ((double)values[i] != 0)
                                m_VATConfigPage.txtVATWithholdLimit.Text = values[i].ToString();
                            else
                                m_VATConfigPage.txtVATWithholdLimit.Text =5000.ToString();
                        }
                    }
                }
                else
                    LoadDefaultVATSettings();

            }
            else
                LoadDefaultVATSettings();

        }

        private void LoadTOTConfigurations()
        {
            object[] values = iERPTransactionClient.GetSystemParameters(new string[] { "GoodsTaxableRate", "ServicesTaxable" });

            if (values != null)
            {
                if (values.Length > 0)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (i == 0) //Goods Taxable
                        {
                            if ((double)values[i] != 0)
                                m_TOTConfigPage.txtGoods.Text= values[i].ToString();
                            else
                                m_TOTConfigPage.txtGoods.Text = 0.02.ToString();
                        }
                        else if (i == 1)
                        {
                            if (values[i] != null)
                            {
                                ServicesTaxable servicesTaxable = (ServicesTaxable)values[i];
                                m_TOTConfigPage.txtContractors.Text = servicesTaxable.ContractorsTaxableRate.ToString();
                                m_TOTConfigPage.txtMillService.Text = servicesTaxable.MillServicesTaxableRate.ToString();
                                m_TOTConfigPage.txtTractors.Text = servicesTaxable.TractorsAndHarvTaxableRate.ToString();
                                m_TOTConfigPage.txtOtherServices.Text = servicesTaxable.OtherServicesTaxableRate.ToString();
                            }
                            else
                            {
                                m_TOTConfigPage.txtContractors.Text = 0.02.ToString();
                                m_TOTConfigPage.txtMillService.Text = 0.02.ToString();
                                m_TOTConfigPage.txtTractors.Text = 0.02.ToString();
                                m_TOTConfigPage.txtOtherServices.Text = 0.1.ToString();
                            }
                        }
                    }
                }
                else
                    LoadDefaultTOTSettings();
            }
            else
                LoadDefaultTOTSettings();

        }

        private void LoadWithHoldingConfigurations()
        {
            LoadGoodsWithHoldingSettings();
            LoadServicesWithHoldingSettings();
            LoadUnknownTINWithHoldingSettings();

        }
        private void LoadPayrollConfigurations()
        {
            object[] values = PayrollClient.GetSystemParameters(new string[] { "EmployeePensionRate", "EmployerPensionRate", "EmployeeMonthlyFullWorkingHours", "EmployeeDailyFullWorkingHours" });

            if (values != null)
            {
                if (values.Length > 0)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (i == 0) //Employee Pension Rate
                        {
                            if ((double)values[i] != 0)
                                m_PayrollConfigPage.txtEmployeeRate.Text = values[i].ToString();
                            else
                                m_PayrollConfigPage.txtEmployeeRate.Text = 0.05.ToString();
                        }
                        else if (i == 1) //Employer Pension Rate
                        {
                            if ((double)values[i] != 0)
                                m_PayrollConfigPage.txtEmployerRate.Text = values[i].ToString();
                            else
                                m_PayrollConfigPage.txtEmployerRate.Text = 0.07.ToString();
                        }
                        else if (i == 2)
                        {
                            if ((double)values[i] != 0)
                                m_PayrollConfigPage.txtMonthlyWorkingHours.Text = values[i].ToString();
                            else
                                m_PayrollConfigPage.txtMonthlyWorkingHours.Text = 192.ToString();
                        }
                        else if (i == 3)
                        {
                            if ((double)values[i] != 0)
                                m_PayrollConfigPage.txtDailyWorkingHours.Text = values[i].ToString();
                            else
                                m_PayrollConfigPage.txtDailyWorkingHours.Text = 8.ToString();
                        }
                    }
                }
                else
                    LoadDefaultPayrollSettings();

            }
            else
                LoadDefaultPayrollSettings();


        }

        private void LoadDefaultPayrollSettings()
        {
            m_PayrollConfigPage.txtEmployeeRate.Text = 0.05.ToString();
            m_PayrollConfigPage.txtEmployerRate.Text = 0.07.ToString();
            m_PayrollConfigPage.txtMonthlyWorkingHours.Text = 192.ToString();
            m_PayrollConfigPage.txtDailyWorkingHours.Text = 8.ToString();
        }
        private void LoadUnknownTINWithHoldingSettings()
        {
            object[] values = iERPTransactionClient.GetSystemParameters(new string[] { "UnknownTINWithHoldingRate" });
            if (values != null)
            {
                if (values.Length > 0)
                {
                    if ((double)values[0] != 0)
                        m_WithHoldingConfigPage.txtUnknownTIN.Text = values[0].ToString();
                    else
                        m_WithHoldingConfigPage.txtUnknownTIN.Text = 0.3.ToString();
                }
                else
                    m_WithHoldingConfigPage.txtUnknownTIN.Text = 0.3.ToString();

            }
            else
                m_WithHoldingConfigPage.txtUnknownTIN.Text = 0.3.ToString();
        }

        private void LoadGoodsWithHoldingSettings()
        {
            object[] values = iERPTransactionClient.GetSystemParameters(new string[] { "WHGoodsNonTaxablePriceLimit", "WHGoodsTaxableRate" });

            if (values != null)
            {
                if (values.Length > 0)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (i == 0)
                        {
                            if ((double)values[i] != 0)
                                m_WithHoldingConfigPage.txtGoodsLimitPrice.Text = values[i].ToString();
                            else
                                m_WithHoldingConfigPage.txtGoodsLimitPrice.Text =10000.ToString("N");

                        }
                        else if (i == 1)
                        {
                            if ((double)values[i] != 0)
                                m_WithHoldingConfigPage.txtGoodsTaxable.Text = values[i].ToString();
                            else
                                m_WithHoldingConfigPage.txtGoodsTaxable.Text = 0.02.ToString();

                        }

                    }
                }
                else
                {
                    LoadDefaultGoodsWithHoldingSettings();
                }

            }
            else
            {
                LoadDefaultGoodsWithHoldingSettings();
            }
        }

        private void LoadDefaultGoodsWithHoldingSettings()
        {
            m_WithHoldingConfigPage.txtGoodsLimitPrice.Text = 10000.ToString("N");
            m_WithHoldingConfigPage.txtGoodsTaxable.Text = 0.02.ToString();
        }
        private void LoadServicesWithHoldingSettings()
        {
            object[] values = iERPTransactionClient.GetSystemParameters(new string[] { "WHServicesNonTaxablePriceLimit", "WHServicesTaxableRate" });

            if (values != null)
            {
                if (values.Length > 0)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (i == 0) //Goods Taxable
                        {
                            if ((double)values[i] != 0)
                                m_WithHoldingConfigPage.txtServicesLimitPrice.Text= values[i].ToString();
                            else
                                m_WithHoldingConfigPage.txtServicesLimitPrice.Text = 500.ToString("N");
                        }
                        else if (i == 1)
                        {
                            if ((double)values[i] != 0)
                                m_WithHoldingConfigPage.txtServicesTaxable.Text= values[i].ToString();
                            else
                                m_WithHoldingConfigPage.txtServicesTaxable.Text = 0.02.ToString();

                        }

                    }
                }
                else
                    LoadDefaultServicesWithHoldingSettings();
            }
            else
                LoadDefaultServicesWithHoldingSettings();

        }

        private void LoadDefaultServicesWithHoldingSettings()
        {
            m_WithHoldingConfigPage.txtServicesLimitPrice.Text = 500.ToString("N");
            m_WithHoldingConfigPage.txtServicesTaxable.Text = 0.02.ToString();
        }
        private void LoadDefaultTOTSettings()
        {
            m_TOTConfigPage.txtGoods.Text = 0.02.ToString();
            m_TOTConfigPage.txtContractors.Text = 0.02.ToString();
            m_TOTConfigPage.txtMillService.Text = 0.02.ToString();
            m_TOTConfigPage.txtTractors.Text = 0.02.ToString();
            m_TOTConfigPage.txtOtherServices.Text = 0.1.ToString();

        }
        private void LoadCodeFormatConfigurations()
        {
            //LoadSupplierCodeFormatSettings();
            //LoadCustomerCodeFormatSettings();
            //LoadFixedAssetCodeFormatSettings();
            //LoadExpenseItemCodeFormatSettings();
            //LoadSalesItemCodeFormatSettings();
            //LoadAttachmentNumberFormatSettings();
            LoadBankAccountCodeFormatSettings();
            LoadCashAccountCodeFormatSettings();
        }

        private void LoadCashAccountCodeFormatSettings()
        {
            object[] values = iERPTransactionClient.GetSystemParameters(new string[] { "CashAccountSuffixCodeFormat" });

            if (values != null)
            {
                if (values.Length > 0)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (i == 0)
                        {
                            if (values[i] != null)
                                m_CodeFormatConfigPage.txtCashAccSuffDigits.Text = values[i].ToString();
                            else
                                m_CodeFormatConfigPage.txtCashAccSuffDigits.Text = "0000";
                        }
                    }
                }
                else
                    LoadDefaultCashAccountFormatSettings();
            }
            else
                LoadDefaultCashAccountFormatSettings();
        }

        private void LoadDefaultCashAccountFormatSettings()
        {
            m_CodeFormatConfigPage.txtCashAccSuffDigits.Text = "0000";
        }
        private void LoadBankAccountCodeFormatSettings()
        {
            object[] values = iERPTransactionClient.GetSystemParameters(new string[] { "BankAccountSuffixCodeFormat" });

            if (values != null)
            {
                if (values.Length > 0)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (i == 0)
                        {
                            if (values[i] != null)
                                m_CodeFormatConfigPage.txtBankAccSufDigits.Text = values[i].ToString();
                            else
                                m_CodeFormatConfigPage.txtBankAccSufDigits.Text ="0000";
                        }
                    }
                }
                else
                    LoadDefaultBankAccountFormatSettings();
            }
            else
                LoadDefaultBankAccountFormatSettings();
        }

        private void LoadDefaultBankAccountFormatSettings()
        {
            m_CodeFormatConfigPage.txtBankAccSufDigits.Text = "0000";
        }

        private void LoadSupplierCodeFormatSettings()
        {
            object[] values = iERPTransactionClient.GetSystemParameters(new string[] { "SupplierCodeFormat", "SupplierAccountCodeSuffixFormat" });

            if (values != null)
            {
                if (values.Length > 0)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (i == 0) 
                        {
                            if (values[i] != null)
                                m_CodeFormatConfigPage.txtSuppCodeDig.Text = values[i].ToString();
                            else
                                m_CodeFormatConfigPage.txtSuppCodeDig.Text = 6.ToString();
                        }
                        else if (i == 1)
                        {
                            if (values[i] != null)
                                m_CodeFormatConfigPage.txtSuppAccCodeDig.Text = values[i].ToString();
                            else
                                m_CodeFormatConfigPage.txtSuppAccCodeDig.Text = 4.ToString();
                        }
                    }
                }
                else
                    LoadDefaultSupplierCodeFormatSettings();
            }
            else
                LoadDefaultSupplierCodeFormatSettings();
        }

        private void LoadDefaultSupplierCodeFormatSettings()
        {
            m_CodeFormatConfigPage.txtSuppCodeDig.Text = 6.ToString();
            m_CodeFormatConfigPage.txtSuppAccCodeDig.Text = 4.ToString();

        }

        private void LoadCustomerCodeFormatSettings()
        {
            object[] values = iERPTransactionClient.GetSystemParameters(new string[] { "CustomerCodeFormat", "CustomerAccountCodeSuffixFormat" });

            if (values != null)
            {
                if (values.Length > 0)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (i == 0)
                        {
                            if (values[i] != null)
                                m_CodeFormatConfigPage.txtCustCodeDig.Text = values[i].ToString();
                            else
                                m_CodeFormatConfigPage.txtCustCodeDig.Text = 6.ToString();
                        }
                        else if (i == 1)
                        {
                            if (values[i] != null)
                                m_CodeFormatConfigPage.txtCustAccCodeDig.Text = values[i].ToString();
                            else
                                m_CodeFormatConfigPage.txtCustAccCodeDig.Text = 4.ToString();
                        }
                    }
                }
                else
                    LoadDefaultCustomerCodeFormatSettings();
            }
            else
                LoadDefaultCustomerCodeFormatSettings();
        }

        private void LoadDefaultCustomerCodeFormatSettings()
        {
            m_CodeFormatConfigPage.txtCustCodeDig.Text = 6.ToString();
            m_CodeFormatConfigPage.txtCustAccCodeDig.Text = 4.ToString();

        }

        private void LoadFixedAssetCodeFormatSettings()
        {
            object[] values = iERPTransactionClient.GetSystemParameters(new string[] { "FixedAssetCodeFormat", "FixedAssetAccountCodeSuffixFormat" });

            if (values != null)
            {
                if (values.Length > 0)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (i == 0)
                        {
                            if (values[i] != null)
                                m_CodeFormatConfigPage.txtFixedAssCodeDig.Text = values[i].ToString();
                            else
                                m_CodeFormatConfigPage.txtFixedAssCodeDig.Text = 6.ToString();
                        }
                        else if (i == 1)
                        {
                            if (values[i] != null)
                                m_CodeFormatConfigPage.txtFixedAssAccCodeDig.Text = values[i].ToString();
                            else
                                m_CodeFormatConfigPage.txtFixedAssAccCodeDig.Text = 4.ToString();
                        }
                    }
                }
                else
                    LoadDefaultFixedAssetCodeFormatSettings();
            }
            else
                LoadDefaultFixedAssetCodeFormatSettings();
        }

        private void LoadDefaultFixedAssetCodeFormatSettings()
        {
            m_CodeFormatConfigPage.txtFixedAssCodeDig.Text = 6.ToString();
            m_CodeFormatConfigPage.txtFixedAssAccCodeDig.Text = 4.ToString();

        }

        private void LoadExpenseItemCodeFormatSettings()
        {
            object[] values = iERPTransactionClient.GetSystemParameters(new string[] { "ExpenseItemCodeFormat", "ExpenseItemAccountCodeSuffixFormat" });

            if (values != null)
            {
                if (values.Length > 0)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (i == 0)
                        {
                            if (values[i] != null)
                                m_CodeFormatConfigPage.txtExpItemCodeDig.Text = values[i].ToString();
                            else
                                m_CodeFormatConfigPage.txtExpItemCodeDig.Text = 6.ToString();
                        }
                        else if (i == 1)
                        {
                            if (values[i] != null)
                                m_CodeFormatConfigPage.txtExpItemAccCodeSuffDig.Text = values[i].ToString();
                            else
                                m_CodeFormatConfigPage.txtExpItemAccCodeSuffDig.Text = 4.ToString();
                        }
                    }
                }
                else
                    LoadDefaultExpenseItemCodeFormatSettings();
            }
            else
                LoadDefaultExpenseItemCodeFormatSettings();
        }

        private void LoadDefaultExpenseItemCodeFormatSettings()
        {
            m_CodeFormatConfigPage.txtExpItemCodeDig.Text = 6.ToString();
            m_CodeFormatConfigPage.txtExpItemAccCodeSuffDig.Text = 4.ToString();
        }

        private void LoadSalesItemCodeFormatSettings()
        {
            object[] values = iERPTransactionClient.GetSystemParameters(new string[] { "SalesItemCodeFormat", "SalesItemAccountCodeSuffixFormat" });

            if (values != null)
            {
                if (values.Length > 0)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (i == 0)
                        {
                            if (values[i] != null)
                                m_CodeFormatConfigPage.txtSalesItemCodeDig.Text = values[i].ToString();
                            else
                                m_CodeFormatConfigPage.txtSalesItemCodeDig.Text = 6.ToString();
                        }
                        else if (i == 1)
                        {
                            if (values[i] != null)
                                m_CodeFormatConfigPage.txtSalesItemAccCodeSuffDig.Text = values[i].ToString();
                            else
                                m_CodeFormatConfigPage.txtSalesItemAccCodeSuffDig.Text = 4.ToString();
                        }
                    }
                }
                else
                    LoadDefaultSalesItemCodeFormatSettings();
            }
            else
                LoadDefaultSalesItemCodeFormatSettings();
        }

        private void LoadAttachmentNumberFormatSettings()
        {
            object value = iERPTransactionClient.GetSystemParamter("AttachmentNumberFormat");
            if (value != null)
                m_CodeFormatConfigPage.txtAttachFormat.Text = (string)value;
            else
                m_CodeFormatConfigPage.txtAttachFormat.Text = "00000000";
        }
        private void LoadDefaultSalesItemCodeFormatSettings()
        {
            m_CodeFormatConfigPage.txtSalesItemCodeDig.Text = 6.ToString();
            m_CodeFormatConfigPage.txtSalesItemAccCodeSuffDig.Text = 4.ToString();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!m_CodeFormatConfigPage.validateCodeFormat.Validate())
                {
                    AddCodeFormatConfigPage();
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                    return;
                }
                if (!m_PayrollConfigPage.validateHour.Validate())
                {
                    AddPayrollConfigPage();
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                    return;
                }
                SaveStaffAccountConfigurations();
                SaveAccountConfigurations();
                SaveVATConfigurations();
                SaveTOTConfigurations();
                SaveWithHoldingConfigurations();
                SavePayrollConfigurations();
                SaveCodeFormatConfigurations();
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to save system parameters.", ex);
            }
        }

        private void SaveStaffAccountConfigurations()
        {
            List<string> payrollNames = new List<string>();
            List<object> payrollValues = new List<object>();
            foreach (Control c in m_staffAccConfigPage.Controls[0].Controls)
            {
                if (c is AccountPlaceHolder)
                {
                    payrollNames.Add((string)c.Tag);
                    payrollValues.Add(((AccountPlaceHolder)c).GetAccountID());
                }
                else if (c is CostCenterPlaceHolder)
                {
                    payrollNames.Add((string)c.Tag);
                    payrollValues.Add(((CostCenterPlaceHolder)c).GetAccountID());
                }
            }
            PayrollClient.SetSystemParameters(payrollNames.ToArray(), payrollValues.ToArray());

        }
        private void SaveAccountConfigurations()
        {
            try
            {
                List<string> names = new List<string>();
                List<object> values = new List<object>();
              
                foreach (Control c in m_AccountConfigPage.Controls[0].Controls)
                {
                    if (c is AccountPlaceHolder)
                    {
                        names.Add((string)c.Tag);
                        values.Add(((AccountPlaceHolder)c).GetAccountID());
                    }
                    else if (c is CostCenterPlaceHolder)
                    {
                        names.Add((string)c.Tag);
                        values.Add(((CostCenterPlaceHolder)c).GetAccountID());
                    }
                }
                iERPTransactionClient.SetSystemParameters(names.ToArray(), values.ToArray());
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error encountered trying to save account configurations!/n" + ex.Message);
            }
        }

        private void SaveVATConfigurations()
        {
            try
            {
                string[] names = new string[4];
                names[0] = "CommonVATRate";
                names[1] = "ExportVATRate";
                names[2] = "RawHideExportVATRate";
                names[3] = "VATWithholdingNonTaxablePriceLimit";

                object[] values = new object[4];
                values[0] = m_VATConfigPage.txtCommonVAT.Text == "" ? 0 : double.Parse(m_VATConfigPage.txtCommonVAT.EditValue.ToString());
                values[1] = m_VATConfigPage.txtExportVAT.Text == "" ? 0 : double.Parse(m_VATConfigPage.txtExportVAT.EditValue.ToString());
                values[2] = m_VATConfigPage.txtRawHideExportVAT.Text == "" ? 0 : double.Parse(m_VATConfigPage.txtRawHideExportVAT.EditValue.ToString());
                values[3] = m_VATConfigPage.txtVATWithholdLimit.Text == "" ? 0 : double.Parse(m_VATConfigPage.txtVATWithholdLimit.EditValue.ToString());

                iERPTransactionClient.SetSystemParameters(names, values);

            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error encountered trying to save VAT configurations!\n" + ex.Message);

            }
        }
        private void SavePayrollConfigurations()
        {
            try
            {
                string[] names = new string[4];
                names[0] = "EmployeePensionRate";
                names[1] = "EmployerPensionRate";
                names[2] = "EmployeeMonthlyFullWorkingHours";
                names[3] = "EmployeeDailyFullWorkingHours";

                object[] values = new object[4];
                values[0] = m_PayrollConfigPage.txtEmployeeRate.EditValue.ToString() == "" ? 0 : double.Parse(m_PayrollConfigPage.txtEmployeeRate.EditValue.ToString());
                values[1] = m_PayrollConfigPage.txtEmployerRate.EditValue.ToString() == "" ? 0 : double.Parse(m_PayrollConfigPage.txtEmployerRate.EditValue.ToString());
                values[2] = m_PayrollConfigPage.txtMonthlyWorkingHours.EditValue.ToString() == "" ? 0 : double.Parse(m_PayrollConfigPage.txtMonthlyWorkingHours.EditValue.ToString());
                values[3] = m_PayrollConfigPage.txtDailyWorkingHours.EditValue.ToString() == "" ? 0 : double.Parse(m_PayrollConfigPage.txtDailyWorkingHours.EditValue.ToString());


                PayrollClient.SetSystemParameters(names, values);

            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error encountered trying to save payroll configurations!\n" + ex.Message);

            }
        }
        private void SaveTOTConfigurations()
        {
            try
            {
                string[] names = new string[2];
                names[0] = "GoodsTaxableRate";
                names[1] = "ServicesTaxable";

                object[] values = new object[2];
                values[0] = m_TOTConfigPage.txtGoods.Text == "" ? 0 : double.Parse(m_TOTConfigPage.txtGoods.EditValue.ToString());
                ServicesTaxable taxable = new ServicesTaxable();
                taxable.ContractorsTaxableRate = m_TOTConfigPage.txtContractors.Text == "" ? 0 : double.Parse(m_TOTConfigPage.txtContractors.EditValue.ToString());
                taxable.MillServicesTaxableRate = m_TOTConfigPage.txtMillService.Text == "" ? 0 : double.Parse(m_TOTConfigPage.txtMillService.EditValue.ToString());
                taxable.TractorsAndHarvTaxableRate = m_TOTConfigPage.txtTractors.Text == "" ? 0 : double.Parse(m_TOTConfigPage.txtTractors.EditValue.ToString());
                taxable.OtherServicesTaxableRate = m_TOTConfigPage.txtOtherServices.Text == "" ? 0 : double.Parse(m_TOTConfigPage.txtOtherServices.EditValue.ToString());
                values[1] = taxable;

                iERPTransactionClient.SetSystemParameters(names, values);

            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error encountered trying to save TOT configurations!\n" + ex.Message);

            }
        }
        private void SaveWithHoldingConfigurations()
        {
            try
            {
                string[] names = new string[5];
                names[0] = "WHGoodsNonTaxablePriceLimit";
                names[1] = "WHGoodsTaxableRate";
                names[2] = "WHServicesNonTaxablePriceLimit";
                names[3] = "WHServicesTaxableRate";
                names[4] = "UnknownTINWithHoldingRate";

                object[] values = new object[5];
                values[0] = m_WithHoldingConfigPage.txtGoodsLimitPrice.Text == "" ? 0 : double.Parse(m_WithHoldingConfigPage.txtGoodsLimitPrice.EditValue.ToString());
                values[1] = m_WithHoldingConfigPage.txtGoodsTaxable.Text == "" ? 0 : double.Parse(m_WithHoldingConfigPage.txtGoodsTaxable.EditValue.ToString());
                values[2] = m_WithHoldingConfigPage.txtServicesLimitPrice.Text == "" ? 0 : double.Parse(m_WithHoldingConfigPage.txtServicesLimitPrice.EditValue.ToString());
                values[3] = m_WithHoldingConfigPage.txtServicesTaxable.Text == "" ? 0 : double.Parse(m_WithHoldingConfigPage.txtServicesTaxable.EditValue.ToString());
                values[4] = m_WithHoldingConfigPage.txtUnknownTIN.Text == "" ? 0 : double.Parse(m_WithHoldingConfigPage.txtUnknownTIN.EditValue.ToString());

                iERPTransactionClient.SetSystemParameters(names, values);

            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error encountered trying to save With-Holding Tax configurations!\n" + ex.Message);

            }
        }
        private void SaveCodeFormatConfigurations()
        {
            try
            {
                string[] names = new string[2];
                names[0] = "BankAccountSuffixCodeFormat";
                names[1] = "CashAccountSuffixCodeFormat";
                //names[1] = "SupplierAccountCodeSuffixFormat";
                //names[2] = "CustomerCodeFormat";
                //names[3] = "CustomerAccountCodeSuffixFormat";
                //names[4] = "FixedAssetCodeFormat";
                //names[5] = "FixedAssetAccountCodeSuffixFormat";
                //names[6] = "ExpenseItemCodeFormat";
                //names[7] = "ExpenseItemAccountCodeSuffixFormat";
                //names[8] = "SalesItemCodeFormat";
                //names[9] = "SalesItemAccountCodeSuffixFormat";
                //names[10] = "AttachmentNumberFormat";

                string[] values = new string[11];
                values[0] = m_CodeFormatConfigPage.txtBankAccSufDigits.Text;
                values[1] = m_CodeFormatConfigPage.txtCashAccSuffDigits.Text;
                //values[1] = m_CodeFormatConfigPage.txtSuppAccCodeDig.Text;
                //values[2] = m_CodeFormatConfigPage.txtCustCodeDig.Text;
                //values[3] = m_CodeFormatConfigPage.txtCustAccCodeDig.Text;
                //values[4] = m_CodeFormatConfigPage.txtFixedAssCodeDig.Text;
                //values[5] = m_CodeFormatConfigPage.txtFixedAssAccCodeDig.Text;
                //values[6] = m_CodeFormatConfigPage.txtExpItemCodeDig.Text;
                //values[7] = m_CodeFormatConfigPage.txtExpItemAccCodeSuffDig.Text;
                //values[8] = m_CodeFormatConfigPage.txtSalesItemCodeDig.Text;
                //values[9] = m_CodeFormatConfigPage.txtSalesItemAccCodeSuffDig.Text;
                //values[10] = m_CodeFormatConfigPage.txtAttachFormat.Text;

                iERPTransactionClient.SetSystemParameters(names, values);

            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error encountered trying to save  Code Format configurations!\n" + ex.Message);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void navAccountConfig_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            AddAccountConfigPage();
            btnResetToDefault.Visible = false;
        }

        private void AddAccountConfigPage()
        {
            panConfigPages.Controls.Clear();
            panConfigPages.Controls.Add(m_AccountConfigPage);
            m_AccountConfigPage.Dock = DockStyle.Fill;
        }

        private void navVATConfig_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            panConfigPages.Controls.Clear();
            panConfigPages.Controls.Add(m_VATConfigPage);
            m_VATConfigPage.Dock = DockStyle.Fill;
            btnResetToDefault.Visible = true;

        }


        private void LoadDefaultVATSettings()
        {
            m_VATConfigPage.txtCommonVAT.Text = 0.15.ToString();
            m_VATConfigPage.txtExportVAT.Text = 0.ToString();
            m_VATConfigPage.txtRawHideExportVAT.Text = 1.5.ToString();
            m_VATConfigPage.txtVATWithholdLimit.Text = 5000.ToString();
        }

        private void navTOTConfig_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            panConfigPages.Controls.Clear();
            panConfigPages.Controls.Add(m_TOTConfigPage);
            m_TOTConfigPage.Dock = DockStyle.Fill;
            btnResetToDefault.Visible = true;

        }

        private void navWithHolding_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            panConfigPages.Controls.Clear();
            panConfigPages.Controls.Add(m_WithHoldingConfigPage);
            m_WithHoldingConfigPage.Dock = DockStyle.Fill;
            btnResetToDefault.Visible = true;

        }

        private void navAccCodeConfig_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            AddCodeFormatConfigPage();
            btnResetToDefault.Visible = true;

        }

        private void AddCodeFormatConfigPage()
        {
            panConfigPages.Controls.Clear();
            panConfigPages.Controls.Add(m_CodeFormatConfigPage);
            m_CodeFormatConfigPage.Dock = DockStyle.Fill;
        }

        
        private void btnResetToDefault_Click(object sender, EventArgs e)
        {
            if (navVATConfig.Links[0].State == DevExpress.Utils.Drawing.ObjectState.Selected)
            {
               if(MessageBox.ShowWarningMessage("Are you sure you want to reset all VAT Configurations to its default settings?")==DialogResult.Yes)
               {
                   LoadDefaultVATSettings();
               }
            }
            else if (navTOTConfig.Links[0].State == DevExpress.Utils.Drawing.ObjectState.Selected)
            {
                if (MessageBox.ShowWarningMessage("Are you sure you want to reset all TOT Configurations to its default settings?") == DialogResult.Yes)
                {
                    LoadDefaultTOTSettings();
                }
            }
            else if (navWithHolding.Links[0].State == DevExpress.Utils.Drawing.ObjectState.Selected)
            {
                if (MessageBox.ShowWarningMessage("Are you sure you want to reset all With-Holding Tax Configurations to its default settings?") == DialogResult.Yes)
                {
                    LoadDefaultGoodsWithHoldingSettings();
                    LoadDefaultServicesWithHoldingSettings();
                }
            }
            else if (navPension.Links[0].State == DevExpress.Utils.Drawing.ObjectState.Selected)
            {
                if (MessageBox.ShowWarningMessage("Are you sure you want to reset all Pension Tax Configurations to its default settings?") == DialogResult.Yes)
                {
                    LoadDefaultPayrollSettings();
                }
            }
            else if (navAccCodeConfig.Links[0].State == DevExpress.Utils.Drawing.ObjectState.Selected)
            {
                if (MessageBox.ShowWarningMessage("Are you sure you want to reset all Code Format Configurations to its default settings?") == DialogResult.Yes)
                {
                    LoadDefaultSupplierCodeFormatSettings();
                    LoadDefaultCustomerCodeFormatSettings();
                    LoadDefaultFixedAssetCodeFormatSettings();
                    LoadDefaultExpenseItemCodeFormatSettings();
                    LoadDefaultSalesItemCodeFormatSettings();
                    LoadDefaultAttachmentNumberFormatSettings();
                }
            }
        }

        private void LoadDefaultAttachmentNumberFormatSettings()
        {
            m_CodeFormatConfigPage.txtAttachFormat.Text = "00000000";
        }

        private void navPension_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            AddPayrollConfigPage();
            btnResetToDefault.Visible = true;
        }

        private void AddPayrollConfigPage()
        {
            panConfigPages.Controls.Clear();
            panConfigPages.Controls.Add(m_PayrollConfigPage);
            m_PayrollConfigPage.Dock = DockStyle.Fill;
        }

        private void navStaffAccConfig_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            AddStaffAccountConfigPage();
        }

    }
}
