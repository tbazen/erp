﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.Accounting;
using INTAPS.UI.ButtonGrid;
using INTAPS.Accounting.Client;
using System.Windows.Forms;

namespace BIZNET.iERP.Client
{
    public class HTMLReportList : ObjectBGList<Object>
    {
        int _categoryID;
        public HTMLReportList(INTAPS.UI.ButtonGrid.ButtonGrid grid,INTAPS.UI.ButtonGrid.ButtonGridItem parent, int categoryID):base(grid,parent)
        {
            _categoryID = categoryID;
        }
        public override List<object> LoadData(string query)
        {
            List<object> ret = new List<object>();
            ret.AddRange(AccountingClient.GetChildReportCategories(_categoryID));
            foreach (ReportDefination def in AccountingClient.GetReportByCategory(_categoryID))
            {
                if (INTAPS.Accounting.Client.AccountingClient.reportClientHandlers.ContainsKey(def.id))
                    ret.Add(def);
            }
            ret.Add("New Report");
            return ret;
        }

        public override INTAPS.UI.ButtonGrid.ButtonGridItem[] CreateButton(object obj)
        {
            if (obj is ReportDefination)
            {
                ReportDefination def = obj as ReportDefination;
                ButtonGridBodyButtonItem defButton=ButtonGridHelper.CreateButton("DEF_"+def.name, def.name);
                EventActivator<ReportDefination> ev=new EventActivator<ReportDefination>(defButton, def);
                defButton.Tag = ev;
                ev.Clicked += new GenericEventHandler<ButtonGridBodyButtonItem, ReportDefination>(report_Clicked);
                 
                return new ButtonGridItem[] { defButton };
            }
            else if (obj is ReportCategory)
            {
                ReportCategory cat = obj as ReportCategory;
                ButtonGridItem catButton = ButtonGridHelper.CreateButton("CAT_" + cat.name, cat.name);
                _grid.AddButtonGridItem(catButton, new HTMLReportList(_grid, catButton, cat.id));
                return new ButtonGridItem[] { catButton };
            }
            else
            {
                ButtonGridBodyButtonItem newButton = ButtonGridHelper.CreateButton("New" , "New Report");
                EventActivator<int> ev = new EventActivator<int>(newButton, 0);
                newButton.Tag = ev;
                ev.Clicked+=new GenericEventHandler<ButtonGridBodyButtonItem,int>(newReport_clicked);
                return new ButtonGridItem[] { newButton };
            }
        }

        void newReport_clicked(ButtonGridBodyButtonItem t, int z)
        {
            ReportTypeSelector rt = new ReportTypeSelector();
            if (rt.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm) == DialogResult.OK)
            {
                Form f = INTAPS.Accounting.Client.AccountingClient.reportClientHandlers[rt.pickedType.id].LoadCreateForm(_categoryID);
                f.Show(INTAPS.UI.UIFormApplicationBase.MainForm);
            }
        }

        void report_Clicked(ButtonGridBodyButtonItem t, ReportDefination z)
        {
            Form f = INTAPS.Accounting.Client.AccountingClient.reportClientHandlers[z.reportTypeID].LoadReport(z);
            f.Show(INTAPS.UI.UIFormApplicationBase.MainForm);
        }
        
        public override bool Searchable
        {
            get { return false; }
        }

        public override string searchLabel
        {
            get { throw new NotImplementedException(); }
        }
    }
}
