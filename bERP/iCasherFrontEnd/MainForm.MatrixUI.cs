using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.UI.ButtonGrid;
using System.Windows.Forms;
using INTAPS.Accounting;
using System.Drawing;
using INTAPS.Accounting.Client;
using INTAPS.ClientServer.Client;


namespace BIZNET.iERP.Client
{
    public partial class MainForm
    {
        ButtonGrid buttonGrid;
        public static ButtonGrid CreateButtonGrid()
        {
            ButtonGrid ret = new ButtonGrid();
            ret.Dock = System.Windows.Forms.DockStyle.Fill;

            ret.ShowLeafNode = false;

            //global default style
            ret.RootStyle.Add(StyleType.FontBrush, Brushes.Black); //Button Caption Color
           // ret.RootStyle.Add(StyleType.FillBrush, Brushes.LightCyan); //Grid group background color
            //ret.RootStyleButtonBody.Add(StyleType.FillBrush, Brushes.WhiteSmoke);
            ret.RootStyleButtonBody.Add(StyleType.FillBrush, Brushes.Transparent); //Button Fill Color
            ret.RootStyleButtonBody.Add(StyleType.BroderPen, Pens.Black); //Button Border Color


            //header buttons global style
            ret.RootStylesCollectionHeaderButton.DefaultStyle.Add(StyleType.FillBrush, System.Drawing.Brushes.LightGray);
            ret.RootStylesCollectionHeaderButton.DefaultStyle.Add(StyleType.Font, new System.Drawing.Font("Arial", 8.0f));
            ret.RootStylesCollectionHeaderButton.DefaultStyle.Add(StyleType.StringFormat
                , StringFormatExtensions.GetStringFormat(StringAlignment.Center, StringAlignment.Center
                , StringFormatFlags.NoWrap, StringTrimming.EllipsisCharacter));

            ret.RootStylesCollectionHeaderButton.SnapStyle.Add(StyleType.Font, new System.Drawing.Font("Arial", 8.0f));
            ret.RootStylesCollectionHeaderButton.SnapStyle.Add(StyleType.StringFormat
                , StringFormatExtensions.GetStringFormat(StringAlignment.Center, StringAlignment.Center
                , StringFormatFlags.NoWrap, StringTrimming.EllipsisCharacter));

            ret.RootStylesCollectionHeaderButton.SelectStyle.Add(StyleType.ButtonWidth, 200);
            ret.RootStylesCollectionHeaderButton.SelectStyle.Add(StyleType.Font, new System.Drawing.Font("Arial", 10.0f, FontStyle.Bold));
            ret.RootStylesCollectionHeaderButton.SelectStyle.Add(StyleType.StringFormat
                    , StringFormatExtensions.GetStringFormat(StringAlignment.Center, StringAlignment.Center
                    , StringFormatFlags.NoWrap, StringTrimming.EllipsisCharacter));

            //page selector buttons
            ret.RootStylesCollectionNavigatorButton.DefaultStyle.Add(StyleType.ButtonWidth, 40);
            ret.RootStylesCollectionNavigatorButton.DefaultStyle.Add(StyleType.ButtonHeight, 20);

            ret.RootStylesCollectionNavigatorButton.SelectStyle.Add(StyleType.ButtonWidth, 40);
            ret.RootStylesCollectionNavigatorButton.SelectStyle.Add(StyleType.ButtonHeight, 20);
            ret.RootStylesCollectionNavigatorButton.SelectStyle.Add(StyleType.FillBrush, Brushes.RoyalBlue);
            ret.RootStylesCollectionNavigatorButton.SelectStyle.Add(StyleType.BroderPen, Pens.Brown);

            ret.RootStylesCollectionNavigatorButton.SnapStyle.Add(StyleType.ButtonWidth, 40);
            ret.RootStylesCollectionNavigatorButton.SnapStyle.Add(StyleType.ButtonHeight, 20);
            ret.RootStylesCollectionNavigatorButton.SnapStyle.Add(StyleType.FillBrush, Brushes.RoyalBlue);

            ret.FilterBoxBackColor = Color.LightGray;
            ret.FilterLabel.ForeColor = Color.Black;
            ret.FilterBox.Width = 400;
            ret.BackColor = Color.FromArgb(0xD9, 0xE0, 0xE0);
            return ret;
        }


        #region Button Group Items
        ButtonGridGroupButtonItem _transactionItemsGroup;
        ButtonGridGroupButtonItem _taxDeclarationsItemsGroup;
        ButtonGridGroupButtonItem _listItemsGroup;
        ButtonGridGroupButtonItem _payrollItemsGroup;
        ButtonGridGroupButtonItem _systemItemsGroup;
        ButtonGridGroupButtonItem _accountingItemsGroup;
        #endregion

        #region Transaction Button Items
        ButtonGridBodyButtonItem _openingBalanceItem;
        ButtonGridBodyButtonItem _staffAndOwnersTransactionsItem;
        ButtonGridBodyButtonItem _suppliersAndCustomersTransactionsItem;
        ButtonGridBodyButtonItem _inventoryTransactionsItem;
        ButtonGridBodyButtonItem _miscellaneousTransactionsItem;

        ButtonGridBodyButtonItem _adjustmentTransactionItem;
        ButtonGridBodyButtonItem _CashierToCashierTransactionItem;
        ButtonGridBodyButtonItem _BankToBankTransactionItem;
        ButtonGridBodyButtonItem _PayToCashierItem;
        ButtonGridBodyButtonItem _ReceiveFromCashierItem;

        #endregion

        #region Payment Transaction Button Items
        ButtonGridBodyButtonItem _bankDepositItem;
        ButtonGridBodyButtonItem _bankWithdrawalItem;
        ButtonGridBodyButtonItem _staffExpenseAdvanceItem;
        ButtonGridBodyButtonItem _staffSalaryAdvanceItem;
        ButtonGridBodyButtonItem _staffLongTermLoanItem;
        ButtonGridBodyButtonItem _staffLongTermLoanWithServiceChargeItem;
        ButtonGridBodyButtonItem _staffSalaryItem;
        ButtonGridBodyButtonItem _staffLoanPaymentItem;
        ButtonGridBodyButtonItem _groupPayrollPaymentItem;
        ButtonGridBodyButtonItem _localPurchaseItem;
        ButtonGridBodyButtonItem _internationalPurchaseItem;
        ButtonGridBodyButtonItem _bankServiceChargePaymentItem;
        ButtonGridBodyButtonItem _bondPaymentItem;
        ButtonGridBodyButtonItem _laborPaymentItem;
        ButtonGridBodyButtonItem _staffPerDiemPaymentItem;
        ButtonGridBodyButtonItem _shareHoldersLoanItem;
        ButtonGridBodyButtonItem _withdrawItem;
        ButtonGridBodyButtonItem _penalityItem;
        ButtonGridBodyButtonItem _bankReconciliation;
        #endregion
        #region Trade relation transactions
        ButtonGridBodyButtonItem _supplierAdvancePaymentitem;
        ButtonGridBodyButtonItem _supplierCreditSettlementitem;
        ButtonGridBodyButtonItem _supplierAdvanceReturnItem;
        ButtonGridBodyButtonItem _supplierRetentionSettlement;
        ButtonGridBodyButtonItem _customerAdvanceReturnitem;
        ButtonGridBodyButtonItem _customerAdvancePaymentItem;
        ButtonGridBodyButtonItem _customerCreditSettlementItem;
        ButtonGridBodyButtonItem _customerRetentionSettlement;
        #endregion
        
        #region Receipt Transaction Button Items
        ButtonGridBodyButtonItem _staffExpenseAdvanceReturnItem;
        ButtonGridBodyButtonItem _staffShortTermLoanSettlementItem;
        ButtonGridBodyButtonItem _staffLongTermLoanSettlementItem;
        ButtonGridBodyButtonItem _loanFromStaffItem;
        ButtonGridBodyButtonItem _LocalSalesItem;
        ButtonGridBodyButtonItem _exportItem;
        ButtonGridBodyButtonItem _bondReturn;
        ButtonGridBodyButtonItem _zReportItem;
        ButtonGridBodyButtonItem _shareHoldersLoanSettlementItem;
        ButtonGridBodyButtonItem _replenishmentItem;

        #endregion

        #region Tax Declaration Button Items
        ButtonGridBodyButtonItem _VATDeclarationItem;
        ButtonGridBodyButtonItem _VATWithholdingDeclarationItem;
        ButtonGridBodyButtonItem _withHoldingTaxDeclarationItem;
        ButtonGridBodyButtonItem _incomeTaxDeclarationItem;
        ButtonGridBodyButtonItem _pensionDeclarationItem;
        ButtonGridBodyButtonItem _costSharingDeclarationItem;
        #endregion

        #region List Button Items
        ButtonGridBodyButtonItem _tradeRelationListItem;
        ButtonGridBodyButtonItem _transactionItemListItem;
        ButtonGridBodyButtonItem _employeesListItem;
        ButtonGridBodyButtonItem _ZReportListItem;
        ButtonGridBodyButtonItem _StoreListItem;
        ButtonGridBodyButtonItem _CashAccountListItem;
        ButtonGridBodyButtonItem _BankAccountListItem;
        ButtonGridBodyButtonItem _ProjectListItem;
        ButtonGridBodyButtonItem _ChartOfAccountListItem;
        #endregion


        #region Payroll Button Items
        ButtonGridBodyButtonItem _managePayroll;
        ButtonGridBodyButtonItem _taxCalculator;
        ButtonGridBodyButtonItem _groupPayroll;

        #endregion
        #region inventory items
        ButtonGridBodyButtonItem _inventory;
        ButtonGridBodyButtonItem _inventoryFixedAsset;
        ButtonGridBodyButtonItem _finishedGoods;
        ButtonGridBodyButtonItem _storeIssue;
        ButtonGridBodyButtonItem _storeReturn;
        ButtonGridBodyButtonItem _internalServices;
        ButtonGridBodyButtonItem _fixedAssetDepreciation;
        ButtonGridBodyButtonItem _storeTransfer;
        ButtonGridBodyButtonItem _convertFixedAssetToProperty;
        ButtonGridBodyButtonItem _assignPropertyToCustodian;
        ButtonGridBodyButtonItem _disposeFixedAsset;
        #endregion

        
        #region System Button Items
        ButtonGridBodyButtonItem _settingsItem;
        ButtonGridBodyButtonItem _serialNumbers;
        ButtonGridBodyButtonItem _companyInfoItem;
        ButtonGridBodyButtonItem _changePassword;
        #endregion

        #region Accounting Button Items
        ButtonGridBodyButtonItem _chartOfAccounts;
        ButtonGridBodyButtonItem _documentBrowserItem;
        ButtonGridBodyButtonItem _reports;
        ButtonGridBodyButtonItem _transactionJournal;
        ButtonGridBodyButtonItem _budget;
        ButtonGridBodyButtonItem _transactionLocking;
        ButtonGridBodyButtonItem _bookClosing;
        ButtonGridBodyButtonItem _recalculateWeightedAverage;
        ButtonGridBodyButtonItem _ledgerExplorer;
        ButtonGridBodyButtonItem _budgetLedgerExplorer;

        #endregion
        void InitializeButtonGrid()
        {
            MainForm.MainButtonGrid = buttonGrid = CreateButtonGrid();
            splitContainer.Panel1.Controls.Add(buttonGrid);
            buttonGrid.BringToFront();
            buttonGrid.LeafButtonClicked += buttonGrid_LeafButtonClicked;

            InitializeButtonGroupItems();

            InitializeOpeningBalanceButtonItem();
            InitializeStaffButtonItems();
            InitializePurchaseButtonItems();
            InitializeTradeTranButtonItems();
            InitializeInventoryTranButtonItems();
            InitializeMiscellaneousTranButtonItems();
            InitializeSalesButtonItems();

            
            InitializeAdjustmentTranButtonItem();
            InitializeCashierToCashierTranButtonItem();
            InitializeBankToBankTranButtonItem();
            InitializePayToCashierTranButtonItem();
            InitializeReceiveFromCashierTranButtonItem();
            InitializeTaxDeclarationButtonItems();
            InitializeListButtonItems();
            InitializePayrollButtonItems();
            
            InitializeAccountingButtonItems();
            InitializeSystemButtonItems();


            AddRootButtonGridItems();
            AddStaffTranButtonItems();
            AddTradeTranButtonItems();
            AddInventoryTranButtonItems();
            AddMiscellaneousTransactionItems();

            buttonGrid.setFilterBox();
            buttonGrid.UpdateLayout();
        }

        private void AddTradeTranButtonItems()
        {
            buttonGrid.AddButtonGridItem(_suppliersAndCustomersTransactionsItem,
                           new ArrayChildSource(buttonGrid, _suppliersAndCustomersTransactionsItem
                           , _customerAdvancePaymentItem
                           , _customerCreditSettlementItem
                           , _customerRetentionSettlement
                           , _customerAdvanceReturnitem
                           , _supplierAdvancePaymentitem
                           , _supplierCreditSettlementitem
                           , _supplierRetentionSettlement
                           , _supplierAdvanceReturnItem
                           , _bondPaymentItem
                           , _bondReturn
                           ));
        }
        private void AddInventoryTranButtonItems()
        {
            buttonGrid.AddButtonGridItem(_inventoryTransactionsItem,
                           new ArrayChildSource(buttonGrid, _inventoryTransactionsItem
                                        , _finishedGoods
                                        , _storeIssue
                                        ,_storeReturn
                                        , _internalServices
                                        , _storeTransfer
                                        , _convertFixedAssetToProperty
                                        , _assignPropertyToCustodian
                                        , _disposeFixedAsset
                           ));
        }
        private void AddMiscellaneousTransactionItems()
        {
            buttonGrid.AddButtonGridItem(_miscellaneousTransactionsItem,
                           new ArrayChildSource(buttonGrid, _miscellaneousTransactionsItem
                           , _zReportItem
                           , _penalityItem
                            , _bankServiceChargePaymentItem
                            ));


        }

        private void AddStaffTranButtonItems()
        {
            buttonGrid.AddButtonGridItem(_staffAndOwnersTransactionsItem,
                new ArrayChildSource(buttonGrid, _staffAndOwnersTransactionsItem
                , _staffSalaryItem
                , _staffPerDiemPaymentItem
                , _laborPaymentItem
                , _staffExpenseAdvanceItem
                , _staffExpenseAdvanceReturnItem
                , _staffSalaryAdvanceItem
                , _staffShortTermLoanSettlementItem
                , "true".Equals(System.Configuration.ConfigurationManager.AppSettings["long_term_loan_service_charge"])? _staffLongTermLoanWithServiceChargeItem: _staffLongTermLoanItem  
                , _staffLongTermLoanSettlementItem
                , _loanFromStaffItem
                , _staffLoanPaymentItem
                , ShareHolderButtonVisible() ? _shareHoldersLoanItem : _withdrawItem
                , ShareHolderButtonVisible() ? _shareHoldersLoanSettlementItem : _replenishmentItem
                , _groupPayrollPaymentItem
                ));
        }

        private void AddRootButtonGridItems()
        {
            CompanyProfile cinfo = iERPTransactionClient.GetCompanyProfile();
            List<ButtonGridItem> items = new List<ButtonGridItem>();
                            //Transactions group
                items.Add(_transactionItemsGroup);
                items.Add(_bankDepositItem);
                items.Add(_bankWithdrawalItem);
                items.Add(_CashierToCashierTransactionItem);
                items.Add(_BankToBankTransactionItem);
                items.Add(_PayToCashierItem);
                items.Add(_ReceiveFromCashierItem);
                items.Add(_localPurchaseItem);
                items.Add(_LocalSalesItem);
                items.Add(_staffAndOwnersTransactionsItem);
                items.Add(_suppliersAndCustomersTransactionsItem);
                items.Add(_inventoryTransactionsItem);
                items.Add(_miscellaneousTransactionsItem);

                //Tax declaration group
                items.Add(_taxDeclarationsItemsGroup);
                if(cinfo!=null && cinfo.TaxRegistrationType==TaxRegisrationType.VAT)
                    items.Add(_VATDeclarationItem);
                if (cinfo != null && cinfo.BusinessEntity == BusinessEntity.GovernmentalOrganization && cinfo.isVATAgent)
                    items.Add(_VATWithholdingDeclarationItem);
                if(cinfo != null && cinfo.isWitholdingAgent)
                    items.Add(_withHoldingTaxDeclarationItem);
                items.Add(_incomeTaxDeclarationItem);
                items.Add(_pensionDeclarationItem);
                
                //Lists
                items.Add(_listItemsGroup);
                items.Add(_tradeRelationListItem);
                items.Add(_transactionItemListItem);
                items.Add(_employeesListItem);
                items.Add(_ZReportListItem);
                items.Add(_StoreListItem);
                items.Add(_CashAccountListItem);
                items.Add(_BankAccountListItem);
                items.Add(_ProjectListItem);
                items.Add(_ChartOfAccountListItem);
                items.Add(_payrollItemsGroup);
                items.Add(_employeesListItem);
                items.Add(_managePayroll);
                items.Add(_taxCalculator);
                items.Add(_groupPayroll);
                
                //Account Group                
                items.Add(_accountingItemsGroup);
                items.Add(_documentBrowserItem);
                items.Add(_transactionJournal);
                items.Add(_chartOfAccounts);
                items.Add(_ledgerExplorer);
                items.Add(_budgetLedgerExplorer);
                items.Add(_reports);
                items.Add(_budget);
                items.Add(_bankReconciliation);
                items.Add(_adjustmentTransactionItem);
                items.Add(_inventory);
                items.Add(_inventoryFixedAsset);
                items.Add(_recalculateWeightedAverage);
                items.Add(_fixedAssetDepreciation);
                items.Add(_bookClosing);
                items.Add(_transactionLocking);
                //System Group
                items.Add(_systemItemsGroup);
                items.Add(_settingsItem);
                items.Add(_serialNumbers);
                items.Add(_companyInfoItem);
                items.Add(_changePassword);

            buttonGrid.AddButtonGridItem(buttonGrid.rootButtonItem,
                new ArrayChildSource(buttonGrid
                , buttonGrid.rootButtonItem
                ,items.ToArray()
            ));
        }


        private void InitializeStaffButtonItems()
        {
            _staffAndOwnersTransactionsItem = ButtonGridHelper.CreateButtonWithImage("", "Staff and Owners Transaction", Properties.Resources.employess);

            _bankDepositItem = ButtonGridHelper.CreateButtonWithImage("", "Bank Deposit", Properties.Resources.deposit);
            _bankDepositItem.Tag = new DocumentActivator(INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(102));

            _bankWithdrawalItem = ButtonGridHelper.CreateButtonWithImage("", "Bank Withdrawal", Properties.Resources.bank_withdrawal1);
            _bankWithdrawalItem.Tag = new DocumentActivator(INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(104));

            _staffExpenseAdvanceItem = ButtonGridHelper.CreateButtonWithImage("", "Staff Purchase Advance", Properties.Resources.staff_expense);
            buttonGrid.AddButtonGridItem(_staffExpenseAdvanceItem, new StaffPaymentMethodListsForPayment(buttonGrid, _staffExpenseAdvanceItem, StaffPaymentListType.ExpenseAdvanceList));
            ButtonGridHelper.SetHeaderModeText(_staffExpenseAdvanceItem, "Select Payment Type");

            _staffPerDiemPaymentItem = ButtonGridHelper.CreateButton("", "Staff Per Diem");
            buttonGrid.AddButtonGridItem(_staffPerDiemPaymentItem, new StaffPaymentMethodListsForPayment(buttonGrid, _staffPerDiemPaymentItem, StaffPaymentListType.PerDiemPayment));
            ButtonGridHelper.SetHeaderModeText(_staffPerDiemPaymentItem, "Select Payment Type");

            _staffSalaryAdvanceItem = ButtonGridHelper.CreateButtonWithImage("", "Staff Salary Advance", Properties.Resources.short_loan);
            buttonGrid.AddButtonGridItem(_staffSalaryAdvanceItem, new StaffPaymentMethodListsForPayment(buttonGrid, _staffSalaryAdvanceItem, StaffPaymentListType.SalaryAdvanceList));
            ButtonGridHelper.SetHeaderModeText(_staffSalaryAdvanceItem, "Select Payment Type");

            _staffLongTermLoanItem = ButtonGridHelper.CreateButtonWithImage("", "Staff Long Term Loan", Properties.Resources.long_loan);
            buttonGrid.AddButtonGridItem(_staffLongTermLoanItem, new StaffPaymentMethodListsForPayment(buttonGrid, _staffLongTermLoanItem, StaffPaymentListType.LongTermLoanList));
            ButtonGridHelper.SetHeaderModeText(_staffLongTermLoanItem, "Select Payment Type");

            _staffLongTermLoanWithServiceChargeItem = ButtonGridHelper.CreateButtonWithImage("", "Staff Long Term Loan", Properties.Resources.long_loan);
            buttonGrid.AddButtonGridItem(_staffLongTermLoanWithServiceChargeItem, new StaffPaymentMethodListsForPayment(buttonGrid, _staffLongTermLoanWithServiceChargeItem, StaffPaymentListType.LongTermLoanWithServiceChargeList));
            ButtonGridHelper.SetHeaderModeText(_staffLongTermLoanWithServiceChargeItem, "Select Payment Type");

            _staffSalaryItem = ButtonGridHelper.CreateButtonWithImage("", "Staff Salary Payment", Properties.Resources.salary);
            buttonGrid.AddButtonGridItem(_staffSalaryItem, new StaffPaymentMethodListsForPayment(buttonGrid, _staffSalaryItem, StaffPaymentListType.UnclaimedSalaryList));
            ButtonGridHelper.SetHeaderModeText(_staffSalaryItem, "Select Payment Type");

            _groupPayrollPaymentItem = ButtonGridHelper.CreateButtonWithImage("", "Group Payroll Salary Payment", Properties.Resources.salary);
            buttonGrid.AddButtonGridItem(_groupPayrollPaymentItem, new GroupPayrollPaymentLists(buttonGrid, _groupPayrollPaymentItem));
            ButtonGridHelper.SetHeaderModeText(_groupPayrollPaymentItem, "Select Payment Type");

            _staffLoanPaymentItem = ButtonGridHelper.CreateButtonWithImage("", "Staff Loan Payment", Properties.Resources.salary);
            buttonGrid.AddButtonGridItem(_staffLoanPaymentItem, new StaffPaymentMethodListsForPayment(buttonGrid, _staffLoanPaymentItem, StaffPaymentListType.StaffLoanPayment));
            ButtonGridHelper.SetHeaderModeText(_staffLoanPaymentItem, "Select Payment Type");

            _laborPaymentItem = ButtonGridHelper.CreateButton("", "Labor Payment");
            buttonGrid.AddButtonGridItem(_laborPaymentItem, new LaborPaymentLists(buttonGrid, _laborPaymentItem));
            ButtonGridHelper.SetHeaderModeText(_laborPaymentItem, "Select Payment Type");



            _shareHoldersLoanItem = ButtonGridHelper.CreateButton("", "Share Holders Loan");
            buttonGrid.AddButtonGridItem(_shareHoldersLoanItem, new StaffPaymentMethodListsForPayment(buttonGrid, _shareHoldersLoanItem, StaffPaymentListType.ShareHoldersLoanList));
            ButtonGridHelper.SetHeaderModeText(_shareHoldersLoanItem, "Select Payment Type");

            _withdrawItem = ButtonGridHelper.CreateButton("", "Withdrawal of Money by Owner");
            buttonGrid.AddButtonGridItem(_withdrawItem, new WithdrawalPaymentLists(buttonGrid, _withdrawItem));
            ButtonGridHelper.SetHeaderModeText(_withdrawItem, "Select Payment Type");

            _staffExpenseAdvanceReturnItem = ButtonGridHelper.CreateButton("", "Staff Purchase Advance Return");
            buttonGrid.AddButtonGridItem(_staffExpenseAdvanceReturnItem, new StaffPaymentMethodListsForReturn(buttonGrid, _staffExpenseAdvanceReturnItem, StaffReturnListType.ExpenseAdvanceReturnList));
            ButtonGridHelper.SetHeaderModeText(_staffExpenseAdvanceReturnItem, "Select Payment Type");

            _staffShortTermLoanSettlementItem = ButtonGridHelper.CreateButton("", "Staff Short Term Loan Settlement");
            buttonGrid.AddButtonGridItem(_staffShortTermLoanSettlementItem, new StaffPaymentMethodListsForReturn(buttonGrid, _staffShortTermLoanSettlementItem, StaffReturnListType.ShortTermLoanReturnList));
            ButtonGridHelper.SetHeaderModeText(_staffShortTermLoanSettlementItem, "Select Payment Type");

            _staffLongTermLoanSettlementItem = ButtonGridHelper.CreateButton("", "Staff Long Term Loan Settlement");
            buttonGrid.AddButtonGridItem(_staffLongTermLoanSettlementItem, new StaffPaymentMethodListsForReturn(buttonGrid, _staffLongTermLoanSettlementItem, StaffReturnListType.LongTermLoanReturnList));
            ButtonGridHelper.SetHeaderModeText(_staffLongTermLoanSettlementItem, "Select Payment Type");

            _loanFromStaffItem = ButtonGridHelper.CreateButton("", "Loan From Staff");
            buttonGrid.AddButtonGridItem(_loanFromStaffItem, new StaffPaymentMethodListsForReturn(buttonGrid, _loanFromStaffItem, StaffReturnListType.LoanFromStaff));
            ButtonGridHelper.SetHeaderModeText(_loanFromStaffItem, "Select Payment Type");

            _replenishmentItem = ButtonGridHelper.CreateButton("", "Money Replenshment by Owner");
            buttonGrid.AddButtonGridItem(_replenishmentItem, new WithdrawalReturnLists(buttonGrid, _replenishmentItem));
            ButtonGridHelper.SetHeaderModeText(_replenishmentItem, "Select Payment Type");

            _shareHoldersLoanSettlementItem = ButtonGridHelper.CreateButton("", "Share Holders Loan Settlement");
            buttonGrid.AddButtonGridItem(_shareHoldersLoanSettlementItem, new StaffPaymentMethodListsForReturn(buttonGrid, _shareHoldersLoanSettlementItem, StaffReturnListType.ShareHoldersLoanReturnList));
            ButtonGridHelper.SetHeaderModeText(_shareHoldersLoanSettlementItem, "Select Payment Type");

        }
        void initTradeRelationButtons()
        {

        }

        private void InitializePurchaseButtonItems()
        {
            _localPurchaseItem = ButtonGridHelper.CreateButtonWithImage("", "Local Purchase",Properties.Resources.local_Purchase);
            ButtonGridHelper.SetHeaderModeText(_localPurchaseItem, "Select Payment Type");
            _internationalPurchaseItem = ButtonGridHelper.CreateButton("", "International Purchase");
            PurchaseAndSalesData localPurchaseData = new PurchaseAndSalesData();
            localPurchaseData.itemTransactionType = ItemTransactionType.Purchase;
            localPurchaseData.purchaseType = PurchaseType.LocalPurchase;
            localPurchaseData.assetAccountID = -1;
            localPurchaseData.PayableAccountID = -1;
            localPurchaseData.ReceivableAccountID = -1;
            buttonGrid.AddButtonGridItem(_localPurchaseItem, new SuppliersPurchaseList(buttonGrid, _localPurchaseItem));
            ButtonGridHelper.SetHeaderModeText(_localPurchaseItem, "Select Supplier");

        }

        private void InitializeMiscellaneousTranButtonItems()
        {
            _miscellaneousTransactionsItem= ButtonGridHelper.CreateButtonWithImage("", "Miscellaneous Transactions", null);
            
            _zReportItem = ButtonGridHelper.CreateButton("", "Z-Report");
            buttonGrid.AddButtonGridItem(_zReportItem, new ZReportPaymentLists(buttonGrid, _zReportItem));
            ButtonGridHelper.SetHeaderModeText(_zReportItem, "Select Payment Type");

            _penalityItem = ButtonGridHelper.CreateButton("", "Penality Payment");
            buttonGrid.AddButtonGridItem(_penalityItem, new PenalityPaymentLists(buttonGrid, _penalityItem));
            ButtonGridHelper.SetHeaderModeText(_penalityItem, "Select Payment Type");

            _bankServiceChargePaymentItem = ButtonGridHelper.CreateButton("", "Bank Service Charge Payment");
            buttonGrid.AddButtonGridItem(_bankServiceChargePaymentItem, new BankServiceChargePaymentLists(buttonGrid, _bankServiceChargePaymentItem));
            ButtonGridHelper.SetHeaderModeText(_bankServiceChargePaymentItem, "Select Payment Type");

        }
        private void InitializeInventoryTranButtonItems()
        {
            _inventoryTransactionsItem = ButtonGridHelper.CreateButtonWithImage("", "Inventory Transactions", Properties.Resources.Inventory_Transaction);

            _fixedAssetDepreciation = ButtonGridHelper.CreateButtonWithImage("", "Fixed Asset Depreciation",Properties.Resources.Fixed_asset_depreciacion);
            _fixedAssetDepreciation.Tag = new DocumentActivator(INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(145));

            _storeIssue = ButtonGridHelper.CreateButton("", "Store Issue");
            _storeIssue.Tag = new DocumentActivator(INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(149));

            _storeReturn= ButtonGridHelper.CreateButton("", "Return to Store");
            _storeReturn.Tag = new DocumentActivator(INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(
                AccountingClient.GetDocumentTypeByType(typeof(ReturnToStoreDocument)).id                
                ));

            _storeTransfer = ButtonGridHelper.CreateButton("", "Store Transfer");
            _storeTransfer.Tag = new DocumentActivator(INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(150));
            DocumentType dt=AccountingClient.GetDocumentTypeByType(typeof(ConvertFixedAssetToPropertyDocument));
            if(dt!=null)
            {
                _convertFixedAssetToProperty = ButtonGridHelper.CreateButton("", "Convert Fixed Asset to Property");
                _convertFixedAssetToProperty.Tag = new DocumentActivator(INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(dt.id));
            }
            
            dt=AccountingClient.GetDocumentTypeByType(typeof(AssignToCustodianDocument));
            if (dt != null)
            {
                _assignPropertyToCustodian = ButtonGridHelper.CreateButton("", "Assign Property to Custodian");
                _assignPropertyToCustodian.Tag = new DocumentActivator(INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(dt.id));
            }
            dt = AccountingClient.GetDocumentTypeByType(typeof(FixedAssetDisposalDocument));
            if (dt != null)
            {
                _disposeFixedAsset = ButtonGridHelper.CreateButton("", "Dispose Fixed Asset");
                _disposeFixedAsset.Tag = new DocumentActivator(INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(dt.id));
            }
           _finishedGoods = ButtonGridHelper.CreateButton("", "Finished Goods/Services");
            _finishedGoods.Tag = new DocumentActivator(INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(148));

            _internalServices = ButtonGridHelper.CreateButton("", "Internal Services");
            _internalServices.Tag = new DocumentActivator(INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(151));
        }

        private void InitializeTradeTranButtonItems()
        {
            _suppliersAndCustomersTransactionsItem = ButtonGridHelper.CreateButtonWithImage("", "Suppliers and Customers Transaction", Properties.Resources.Supplier___Customer_Traunsaction);

            _customerAdvancePaymentItem = ButtonGridHelper.CreateButtonWithImage("", "Customer Advance Payment", Properties.Resources.customer_advance_payment);
            buttonGrid.AddButtonGridItem(_customerAdvancePaymentItem, new TradeRelationDocumentTypeActivatorList<CustomerAdvancePayment2Document>(buttonGrid, _customerAdvancePaymentItem, TradeRelationType.Customer));

            _supplierAdvanceReturnItem = ButtonGridHelper.CreateButton("", "Supplier Advance Return");
            buttonGrid.AddButtonGridItem(_supplierAdvanceReturnItem, new TradeRelationDocumentTypeActivatorList<SupplierAdvanceReturn2Document>(buttonGrid, _supplierAdvanceReturnItem, TradeRelationType.Supplier));

            _supplierAdvancePaymentitem = ButtonGridHelper.CreateButtonWithImage("", "Supplier Advance Payment", Properties.Resources.supplier_advance_payment);
            buttonGrid.AddButtonGridItem(_supplierAdvancePaymentitem, new TradeRelationDocumentTypeActivatorList<SupplierAdvancePayment2Document>(buttonGrid, _supplierAdvancePaymentitem, TradeRelationType.Supplier));

            _customerCreditSettlementItem = ButtonGridHelper.CreateButton("", "Customer Credit Settlement");
            buttonGrid.AddButtonGridItem(_customerCreditSettlementItem, new TradeRelationDocumentTypeActivatorList<CustomerCreditSettlement2Document>(buttonGrid, _customerCreditSettlementItem, TradeRelationType.Customer));


            _supplierCreditSettlementitem = ButtonGridHelper.CreateButton("", "Supplier Credit Settlement");
            buttonGrid.AddButtonGridItem(_supplierCreditSettlementitem, new TradeRelationDocumentTypeActivatorList<SupplierCreditSettlement2Document>(buttonGrid, _supplierCreditSettlementitem, TradeRelationType.Supplier));

            _customerAdvanceReturnitem = ButtonGridHelper.CreateButton("", "Customer Advance Return");
            buttonGrid.AddButtonGridItem(_customerAdvanceReturnitem, new TradeRelationDocumentTypeActivatorList<CustomerAdvanceReturn2Document>(buttonGrid, _customerAdvanceReturnitem, TradeRelationType.Customer));

            _supplierRetentionSettlement= ButtonGridHelper.CreateButton("", "Supplier Retention Settlment");
            buttonGrid.AddButtonGridItem(_supplierRetentionSettlement, new TradeRelationDocumentTypeActivatorList<SupplierRetentionSettlementDocument>(buttonGrid, _supplierRetentionSettlement, TradeRelationType.Supplier));

            _customerRetentionSettlement = ButtonGridHelper.CreateButton("", "Customer Retention Settlment");
            buttonGrid.AddButtonGridItem(_customerRetentionSettlement, new TradeRelationDocumentTypeActivatorList<CustomerRetentionSettlementDocument>(buttonGrid, _customerRetentionSettlement, TradeRelationType.Customer));


            _bondReturn = ButtonGridHelper.CreateButton("", "Bond Return");
            buttonGrid.AddButtonGridItem(_bondReturn, new CustomerPaymentMethodLists(buttonGrid, _bondReturn, CustomerListType.BondReturnList));
            ButtonGridHelper.SetHeaderModeText(_bondReturn, "Select Payment Type");

            _bondPaymentItem = ButtonGridHelper.CreateButton("", "Bond Payment");
            buttonGrid.AddButtonGridItem(_bondPaymentItem, new CustomerPaymentMethodLists(buttonGrid, _bondPaymentItem, CustomerListType.BondPaymentList));
            ButtonGridHelper.SetHeaderModeText(_bondPaymentItem, "Select Payment Type");
        }

        private void InitializeAdjustmentTranButtonItem()
        {
            

        }
        private void InitializeCashierToCashierTranButtonItem()
        {
            _CashierToCashierTransactionItem = ButtonGridHelper.CreateButton("", "Cashier To Cashier Transfer");
            _CashierToCashierTransactionItem.Tag = new DocumentActivator(
                INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(126));
        }
        private void InitializeBankToBankTranButtonItem()
        {
            _BankToBankTransactionItem = ButtonGridHelper.CreateButtonWithImage("", "Bank To Bank Transfer",Properties.Resources.Bank_to_Bank_Transaction);
            _BankToBankTransactionItem.Tag = new DocumentActivator(
        INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(127));

        }
        private void InitializePayToCashierTranButtonItem()
        {
            _PayToCashierItem = ButtonGridHelper.CreateButton("", "Pay To Cashier");
            _PayToCashierItem.Tag = new DocumentActivator(
        INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(AccountingClient.GetDocumentTypeByType(typeof(PayToCashierDocument)).id));

        }
        private void InitializeReceiveFromCashierTranButtonItem()
        {
            _ReceiveFromCashierItem = ButtonGridHelper.CreateButton("", "Receive From Cashier");
            _ReceiveFromCashierItem.Tag = new DocumentActivator(
        INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(AccountingClient.GetDocumentTypeByType(typeof(ReceiveFromCashierDocument)).id));

        }
        private void InitializeSalesButtonItems()
        {
            _LocalSalesItem = ButtonGridHelper.CreateButtonWithImage("", "Local Sales",Properties.Resources.local_sales);
            ButtonGridHelper.SetHeaderModeText(_LocalSalesItem, "Select Payment Type");

            _exportItem = ButtonGridHelper.CreateButton("", "Export");
            PurchaseAndSalesData localSalesData = new PurchaseAndSalesData();
            localSalesData.itemTransactionType = ItemTransactionType.Sales;
            localSalesData.SalesType = SalesType.LocalSales;
            localSalesData.assetAccountID = -1;
            localSalesData.PayableAccountID = -1;
            localSalesData.ReceivableAccountID = -1;
            buttonGrid.AddButtonGridItem(_LocalSalesItem, new CustomersSalesList(buttonGrid, _LocalSalesItem));
            ButtonGridHelper.SetHeaderModeText(_LocalSalesItem, "Select customer");
        }
        private void InitializeOpeningBalanceButtonItem()
        {
        }

        private void InitializeButtonGroupItems()
        {
            _transactionItemsGroup = ButtonGridHelper.CreateGroupButton("Transactions");
            _taxDeclarationsItemsGroup = ButtonGridHelper.CreateGroupButton("Tax Declarations");
            _listItemsGroup = ButtonGridHelper.CreateGroupButton("Lists");
            _payrollItemsGroup = ButtonGridHelper.CreateGroupButton("Payroll");
            _accountingItemsGroup = ButtonGridHelper.CreateGroupButton("Accounting");
            _systemItemsGroup = ButtonGridHelper.CreateGroupButton("System");

        }
        
        private void InitializeTaxDeclarationButtonItems()
        {
            _VATDeclarationItem = ButtonGridHelper.CreateButton("", "VAT Declaration");
            EventActivator<int> eventVAT = new EventActivator<int>(_VATDeclarationItem, 0);
            eventVAT.Clicked += VATDeclarationButtonClicked;
            _VATDeclarationItem.Tag = eventVAT;


            _VATWithholdingDeclarationItem = ButtonGridHelper.CreateButton("", "VAT Withholding Declaration");
            EventActivator<int> eventVATWithhold = new EventActivator<int>(_VATWithholdingDeclarationItem, 0);
            eventVATWithhold.Clicked += VATWithholdingDeclarationButtonClicked;
            _VATWithholdingDeclarationItem.Tag = eventVATWithhold;

            _withHoldingTaxDeclarationItem = ButtonGridHelper.CreateButton("", "Withholding Tax Declaration");
            EventActivator<int> ac = new EventActivator<int>(_withHoldingTaxDeclarationItem, 0);
            ac.Clicked += WithHoldingTaxDeclarationButtonClicked;
            _withHoldingTaxDeclarationItem.Tag = ac;

            _incomeTaxDeclarationItem = ButtonGridHelper.CreateButton("", "Income Tax Declaration");
            EventActivator<int> eventIncome = new EventActivator<int>(_incomeTaxDeclarationItem, 0);
            eventIncome.Clicked += IncomeTaxDeclarationButtonClicked;
            _incomeTaxDeclarationItem.Tag = eventIncome;

            _pensionDeclarationItem = ButtonGridHelper.CreateButton("", "Pension Declaration");
            EventActivator<int> eventPension = new EventActivator<int>(_pensionDeclarationItem, 0);
            eventPension.Clicked += PensionTaxDeclarationClicked;
            _pensionDeclarationItem.Tag = eventPension;


            _costSharingDeclarationItem = ButtonGridHelper.CreateButton("", "Cost Sharing Declaration");
        }

        void VATDeclarationButtonClicked(ButtonGridBodyButtonItem t, int z)
        {
            DocumentType docType = INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(VATDeclarationDocument));
            TaxDeclarationManager manager = new TaxDeclarationManager(docType.id);
            TaxDeclarationManager.TaxDeclarationManagerForm = manager;
            //AccountingClient.DefaultAccountingClient client = AccountingClient.AccountingClientDefaultInstance;
            manager.Show(this);
        }
        void VATWithholdingDeclarationButtonClicked(ButtonGridBodyButtonItem t, int z)
        {
            DocumentType docType = INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(VATWithholdingDeclarationDocument));
            TaxDeclarationManager manager = new TaxDeclarationManager(docType.id);
            TaxDeclarationManager.TaxDeclarationManagerForm = manager;
            manager.Show(this);
        }
        void PensionTaxDeclarationClicked(ButtonGridBodyButtonItem t, int z)
        {
            DocumentType docType = INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(PensionTaxDeclarationDocument));
            TaxDeclarationManager manager = new TaxDeclarationManager(docType.id);
            TaxDeclarationManager.TaxDeclarationManagerForm = manager;
            manager.Show(this);
        }

        void IncomeTaxDeclarationButtonClicked(ButtonGridBodyButtonItem t, int z)
        {
            DocumentType docType = INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(IncomeTaxDeclarationDocument));
            TaxDeclarationManager manager = new TaxDeclarationManager(docType.id);
            TaxDeclarationManager.TaxDeclarationManagerForm = manager;
            manager.Show(this);
        }

        void WithHoldingTaxDeclarationButtonClicked(ButtonGridBodyButtonItem t, int z)
        {
            DocumentType docType = INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(WithholdingTaxDeclarationDocument));
            TaxDeclarationManager manager = new TaxDeclarationManager(docType.id);
            TaxDeclarationManager.TaxDeclarationManagerForm = manager;
            manager.Show(this);

        }
        private void InitializeListButtonItems()
        {
            _tradeRelationListItem = ButtonGridHelper.CreateButtonWithImage("", "Trade Relations", null);
            _tradeRelationListItem.Tag = new FormActivator(this, typeof(RelationManager), false, TradeRelationType.All, ManagerPurpose.Browser);


            _transactionItemListItem = ButtonGridHelper.CreateButtonWithImage("", "Items", Properties.Resources.Items);
            _transactionItemListItem.Tag = new FormActivator(this, typeof(ItemManager), false);

            _employeesListItem = ButtonGridHelper.CreateButton("", "Employees");
            _employeesListItem.Tag = new FormActivator(this, typeof(EmployeeManager), false);

            _ZReportListItem = ButtonGridHelper.CreateButton("", "Z-Report");
            _ZReportListItem.Tag = new FormActivator(this, typeof(ZReportBrowser), false);

            _StoreListItem = ButtonGridHelper.CreateButtonWithImage("", "Stores",Properties.Resources.Store);
            _StoreListItem.Tag = new FormActivator(this, typeof(ListManager), false, ListManagerType.Store);

            _CashAccountListItem = ButtonGridHelper.CreateButton("", "Cash Accounts");
            _CashAccountListItem.Tag = new FormActivator(this, typeof(ListManager), false, ListManagerType.CashAccount);

            _BankAccountListItem = ButtonGridHelper.CreateButton("", "Bank Accounts");
            _BankAccountListItem.Tag = new FormActivator(this, typeof(ListManager), false, ListManagerType.BankAccount);

            _ProjectListItem = ButtonGridHelper.CreateButton("", "Branches/Projects");
            EventActivator<int> eventGen = new EventActivator<int>(_ProjectListItem, 0);
            eventGen.Clicked += ProjectListClicked;
            _ProjectListItem.Tag = eventGen;

            _ChartOfAccountListItem = ButtonGridHelper.CreateButton("", "Chart Of Accounts List");
            _ChartOfAccountListItem.Tag = new FormActivator(this, typeof(ChartOfAccountTemplate), false);


        }
        private void ProjectListClicked(ButtonGridBodyButtonItem t, int z)
        {
            ProjectListForm list = new ProjectListForm();
            ProjectListForm.ProjectList = list;
            list.Show(this);
        }
        private void InitializePayrollButtonItems()
        {
            _managePayroll = ButtonGridHelper.CreateButton("", "Manage Payroll");
            EventActivator<int> eventGen = new EventActivator<int>(_managePayroll, 0);
            eventGen.Clicked += GeneratePayrollButtonClicked;
            _managePayroll.Tag = eventGen;

            _taxCalculator = ButtonGridHelper.CreateButton("", "Income Tax Calculator");
            _taxCalculator.Tag = new FormActivator(this, typeof(TaxSolver), false);

            _groupPayroll= ButtonGridHelper.CreateButton("", "Group Payroll");
            _groupPayroll.Tag = new DocumentActivator(INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(153));

        }

       

        void GeneratePayrollButtonClicked(ButtonGridBodyButtonItem t, int z)
        {
            //INTAPS.Payroll.Client.MainForm gp = new INTAPS.Payroll.Client.MainForm();
            //gp.Show(INTAPS.UI.UIFormApplicationBase.MainForm);
            PayrollManager2 manager = new PayrollManager2();
            //PayrollManager.MainPayrollManager = manager;
            manager.Show(this);
        }

        private void InitializeSystemButtonItems()
        {
            _settingsItem = ButtonGridHelper.CreateButtonWithImage("", "Settings", Properties.Resources.company_config);
            _settingsItem.Tag = new FormActivator(this, typeof(iERPConfiguration), false);


            _companyInfoItem = ButtonGridHelper.CreateButtonWithImage("", "Company Information", Properties.Resources.company_info);
            _companyInfoItem.Tag = new FormActivator(this, typeof(RegisterCompanyProfile), false);

            _changePassword = ButtonGridHelper.CreateButtonWithImage("", "Change Password", null);
            ((EventActivator<int>)(_changePassword.Tag = new EventActivator<int>(_changePassword, 0))).Clicked += new GenericEventHandler<ButtonGridBodyButtonItem, int>(ChangePasswordClicked);

            _serialNumbers= ButtonGridHelper.CreateButtonWithImage("", "Reference Settings", null);
            _serialNumbers.Tag = new FormActivator(this, typeof(DocumentSerialSetting), false);
      }

        void ChangePasswordClicked(ButtonGridBodyButtonItem t, int z)
        {
            new ChangePassword().ShowDialog(this);
        }
        private void InitializeAccountingButtonItems()
        {
            _chartOfAccounts = ButtonGridHelper.CreateButtonWithImage("", "Accounts Explorer", Properties.Resources.accounting_Explore);
            _chartOfAccounts.Tag = new FormActivator(this, typeof(AccountExplorer), false);

            _ledgerExplorer = ButtonGridHelper.CreateButtonWithImage("", "Ledger Explorer", Properties.Resources.accounting_Explore);
            _ledgerExplorer.Tag = new FormActivator(this, typeof(LedgerExplorer), false);

            _budgetLedgerExplorer = ButtonGridHelper.CreateButtonWithImage("", "Budget Ledger Explorer", Properties.Resources.accounting_Explore);
            _budgetLedgerExplorer.Tag = new FormActivator(this, typeof(LedgerExplorer), false,true,TransactionItem.BUDGET);
            
            _documentBrowserItem = ButtonGridHelper.CreateButtonWithImage("", "Browse Documents", Properties.Resources.Browse_doc);
            _documentBrowserItem.Tag = new FormActivator(this, typeof(DocumentBrowser), false);

            _transactionJournal = ButtonGridHelper.CreateButtonWithImage("", "Transaction Journal", null);
            _transactionJournal.Tag = new FormActivator(this, typeof(JournalExplorer), false);

            _budget = ButtonGridHelper.CreateButtonWithImage("", "Budgeting", null);
            _budget.Tag = new FormActivator(this, typeof(BudgetForm), false);

            _bookClosing= ButtonGridHelper.CreateButtonWithImage("", "Close Book", null);
            _bookClosing.Tag =  new DocumentActivator(INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(
                INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(BookClosingDocument)).id
                ));

            _recalculateWeightedAverage = ButtonGridHelper.CreateButtonWithImage("", "Recalculate Weighted Average", null);
            _recalculateWeightedAverage.Tag = new FormActivator(this, typeof(RecalculateInventoryWeightedAverage), false);
            
            _transactionLocking = ButtonGridHelper.CreateButtonWithImage("", "Lock Transaction", null);
            _transactionLocking.Tag = new FormActivator(this, typeof(ClosingListForm), false);

            _bankReconciliation = ButtonGridHelper.CreateButtonWithImage("", "Bank Reconciliation",Properties.Resources.Bank_conciliation);
            buttonGrid.AddButtonGridItem(_bankReconciliation, new AllBankAccountsList(buttonGrid, _bankReconciliation, 152));

            _inventory = ButtonGridHelper.CreateButtonWithImage("", "Store Inventory",Properties.Resources.Inventory_Transaction);
            _inventory.Tag = new FormActivator(this, typeof(InventoryForm),false,new AccountingClient.DefaultAccountingClient(),new ActivationParameter(),false);

            _inventoryFixedAsset = ButtonGridHelper.CreateButton("", "Fixed Asset Inventory");
            _inventoryFixedAsset.Tag = new FormActivator(this, typeof(FixedAssetInventoryForm), false, new AccountingClient.DefaultAccountingClient(), new ActivationParameter());

            _openingBalanceItem = ButtonGridHelper.CreateButtonWithImage("", "Opening Transaction", Properties.Resources.opening_balance);
            _openingBalanceItem.Tag = new DocumentActivator(
            INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(103));

            _adjustmentTransactionItem = ButtonGridHelper.CreateButtonWithImage("", "Direct Entry",Properties.Resources.Adustment);
            _adjustmentTransactionItem.Tag = new DocumentActivator(
                INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(123));

            _reports = ButtonGridHelper.CreateButtonWithImage("", "Reports", Properties.Resources.report);
            buttonGrid.AddButtonGridItem(_reports, new HTMLReportList(buttonGrid, _reports, -1));
        }
        void buttonGrid_LeafButtonClicked(ButtonGrid t, ButtonGridItem z)
        {
            ButtonGridBodyButtonItem b = z as ButtonGridBodyButtonItem;
            if (b == null)
                return;
            IiERPCommandActivator cmd = b.Tag as IiERPCommandActivator;
            if (cmd == null)
                return;
            try
            {
                cmd.Activate();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error activating this item", ex);
            }
        }
    }
}
