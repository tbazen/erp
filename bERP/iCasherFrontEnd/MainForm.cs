using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.UI.ButtonGrid;
using INTAPS.Accounting;
using INTAPS.ClientServer.Client;

namespace BIZNET.iERP.Client
{
    public partial class MainForm : XtraForm
    {
        public static ButtonGrid MainButtonGrid = null;
        public static MainForm TheMainForm;
        public static bool _quiting = false;
        DashboardBuilder db = new DashboardBuilder();
        public MainForm()
        {
            InitializeComponent();
            InitializeButtonGrid();
            ShareHolderButtonVisible();
            InitPeriodSelector();
            TheMainForm = this;
            Globals.currentCostCenterID = iERPTransactionClient.mainCostCenterID;
            currentCostCenter.SetByID(Globals.currentCostCenterID);
            this.RefreshDashboard();
        }
        protected override void OnClosed(EventArgs e)
        {
            MainForm._quiting = true;
            base.OnClosed(e);
        }
        
        void RefreshDashboard()
        {
            btnRefresh.Enabled = false;
            statusView.DocumentText = "Retreiving data from server..";
            new System.Threading.Thread(new System.Threading.ThreadStart(delegate()
            {
                db.Build();
                INTAPS.UI.UIFormApplicationBase.runInUIThread(new INTAPS.UI.HTML.ProcessParameterless(
                    delegate()
                    {
                        statusView.LoadControl(db);
                        btnRefresh.Enabled = true;
                    }));
            })).Start();

        }
        
        private void InitPeriodSelector()
        {
            string text=(string)iERPTransactionClient.GetSystemParamter("taxDeclarationPeriods");
            periodTax.selectedPeriod = Globals.CurrentTaxDeclarationPeriod;
            periodAccounting.selectedPeriod = Globals.CurrentAccountingPeriod;
        }
        private bool ShareHolderButtonVisible()
        {
            bool showShareHolderButton = false;
            CompanyProfile profile = (CompanyProfile)iERPTransactionClient.GetSystemParamter("companyProfile");
            if (profile == null)
            {
                MessageBox.ShowErrorMessage("Company profile not found! Please configure your company profile and try again!");
            }
            else
            {
                switch (profile.BusinessEntity)
                {
                    case BusinessEntity.SoleProprietorship:
                        showShareHolderButton = false;
                        break;
                    case BusinessEntity.Partnership:
                    case BusinessEntity.GeneralPartnership:
                    case BusinessEntity.LimitedPartnership:
                    case BusinessEntity.ShareCompany:
                    case BusinessEntity.PrivateLimitedCompany:
                    case BusinessEntity.JointVenture:
                        showShareHolderButton = true;
                        break;
                    default:
                        break;
                }
            }
            return showShareHolderButton;
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            buttonGrid.handleKeyDown(new KeyEventArgs(keyData));
            return base.ProcessCmdKey(ref msg, keyData);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            //buttonGrid.handleKeyDown(e);
            base.OnKeyDown(e);
        }

        private void comboTaxPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            Globals.CurrentTaxDeclarationPeriod =(INTAPS.Accounting.AccountingPeriod) ((ComboBoxEdit)sender).SelectedItem;
            
        }
        
        bool _materilizaing = false;
        private void timerCheck_Tick(object sender, EventArgs e)
        {
            if (Program.pendingMaterilization != null)
            {
                if (Program.pendingMaterilization != null && !_materilizaing)
                {
                    _materilizaing = true;
                    try
                    {
                        ConfirmScheduledDocuments conf = new ConfirmScheduledDocuments();
                        conf.ShowDialog(this);
                        Program.pendingMaterilization = null;
                    }
                    finally
                    {
                        _materilizaing = false;
                    }
                }
            }
        }

        private void periodSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            Globals.CurrentAccountingPeriod = periodAccounting.selectedPeriod;
        }

        private void periodTax_SelectedIndexChanged(object sender, EventArgs e)
        {
            Globals.CurrentTaxDeclarationPeriod = periodTax.selectedPeriod;
        }

        private void currentCostCenter_AccountChanged(object sender, EventArgs e)
        {
            Globals.currentCostCenterID = currentCostCenter.GetAccountID();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            this.RefreshDashboard();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
          //  ApplicationClient.Disconnect();
            base.OnClosing(e);
        }
    }
}
