﻿namespace BIZNET.iERP.Client
{
    partial class PayrollConfigurationPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtDailyWorkingHours = new DevExpress.XtraEditors.TextEdit();
            this.txtMonthlyWorkingHours = new DevExpress.XtraEditors.TextEdit();
            this.txtEmployerRate = new DevExpress.XtraEditors.TextEdit();
            this.txtEmployeeRate = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.validateHour = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDailyWorkingHours.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonthlyWorkingHours.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validateHour)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtDailyWorkingHours);
            this.layoutControl1.Controls.Add(this.txtMonthlyWorkingHours);
            this.layoutControl1.Controls.Add(this.txtEmployerRate);
            this.layoutControl1.Controls.Add(this.txtEmployeeRate);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(477, 144);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtDailyWorkingHours
            // 
            this.txtDailyWorkingHours.Location = new System.Drawing.Point(201, 45);
            this.txtDailyWorkingHours.Name = "txtDailyWorkingHours";
            this.txtDailyWorkingHours.Properties.Mask.EditMask = "#,########0.00;";
            this.txtDailyWorkingHours.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDailyWorkingHours.Size = new System.Drawing.Size(261, 20);
            this.txtDailyWorkingHours.StyleController = this.layoutControl1;
            this.txtDailyWorkingHours.TabIndex = 7;
            // 
            // txtMonthlyWorkingHours
            // 
            this.txtMonthlyWorkingHours.Location = new System.Drawing.Point(201, 15);
            this.txtMonthlyWorkingHours.Name = "txtMonthlyWorkingHours";
            this.txtMonthlyWorkingHours.Properties.Mask.EditMask = "#,########0.00;";
            this.txtMonthlyWorkingHours.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtMonthlyWorkingHours.Size = new System.Drawing.Size(261, 20);
            this.txtMonthlyWorkingHours.StyleController = this.layoutControl1;
            this.txtMonthlyWorkingHours.TabIndex = 6;
            this.txtMonthlyWorkingHours.Tag = "staffFullWorkingHours";
            // 
            // txtEmployerRate
            // 
            this.txtEmployerRate.Location = new System.Drawing.Point(201, 105);
            this.txtEmployerRate.Name = "txtEmployerRate";
            this.txtEmployerRate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtEmployerRate.Properties.Mask.EditMask = "#,#0.00%;";
            this.txtEmployerRate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtEmployerRate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtEmployerRate.Size = new System.Drawing.Size(261, 20);
            this.txtEmployerRate.StyleController = this.layoutControl1;
            this.txtEmployerRate.TabIndex = 5;
            // 
            // txtEmployeeRate
            // 
            this.txtEmployeeRate.Location = new System.Drawing.Point(201, 75);
            this.txtEmployeeRate.Name = "txtEmployeeRate";
            this.txtEmployeeRate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtEmployeeRate.Properties.Mask.EditMask = "#,#0.00%;";
            this.txtEmployeeRate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtEmployeeRate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtEmployeeRate.Size = new System.Drawing.Size(261, 20);
            this.txtEmployeeRate.StyleController = this.layoutControl1;
            this.txtEmployeeRate.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(477, 144);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtEmployeeRate;
            this.layoutControlItem1.CustomizationFormText = "Employee Pension Calculation Rate:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(457, 30);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Text = "Employee Pension Calculation Rate:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(183, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtEmployerRate;
            this.layoutControlItem2.CustomizationFormText = "Employer Pension Calculation Rate:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(457, 34);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem2.Text = "Employer Pension Calculation Rate:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(183, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtMonthlyWorkingHours;
            this.layoutControlItem3.CustomizationFormText = "Employee Full Working Hours:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(457, 30);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "Employee Monthly Full Working Hours:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(183, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtDailyWorkingHours;
            this.layoutControlItem4.CustomizationFormText = "Employee Full Working Days:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(457, 30);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem4.Text = "Employee Daily Full Working Hours:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(183, 13);
            // 
            // validateHour
            // 
            this.validateHour.ValidateHiddenControls = false;
            // 
            // PayrollConfigurationPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "PayrollConfigurationPage";
            this.Size = new System.Drawing.Size(477, 144);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDailyWorkingHours.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonthlyWorkingHours.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployerRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validateHour)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        internal DevExpress.XtraEditors.TextEdit txtEmployeeRate;
        internal DevExpress.XtraEditors.TextEdit txtEmployerRate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        internal DevExpress.XtraEditors.TextEdit txtDailyWorkingHours;
        internal DevExpress.XtraEditors.TextEdit txtMonthlyWorkingHours;
        internal DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validateHour;
    }
}
