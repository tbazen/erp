﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;

namespace BIZNET.iERP.Client
{
    public partial class PayrollConfigurationPage : DevExpress.XtraEditors.XtraUserControl
    {
        public PayrollConfigurationPage()
        {
            InitializeComponent();
            InitializeWorkKindValidation();
            InitializeFocusLostEvents();

        }
        private void InitializeWorkKindValidation()
        {
            EmployeeWorkKindValidationRule workHourValidation = new EmployeeWorkKindValidationRule(EmployeeWorkValidationKind.DayHour,true);
            workHourValidation.ErrorType = ErrorType.Default;

            EmployeeWorkKindValidationRule monthlyHourValidation = new EmployeeWorkKindValidationRule(EmployeeWorkValidationKind.MonthHour, true);
            monthlyHourValidation.ErrorType = ErrorType.Default;

            SetDailyWorkHourValidationRule(workHourValidation);
            SetMonthlyWorkHourValidationRule(monthlyHourValidation);
        }

        private void SetMonthlyWorkHourValidationRule(EmployeeWorkKindValidationRule monthlyHourValidation)
        {
            validateHour.SetValidationRule(txtMonthlyWorkingHours, monthlyHourValidation);
        }

        private void SetDailyWorkHourValidationRule(EmployeeWorkKindValidationRule workHourValidation)
        {
            validateHour.SetValidationRule(txtDailyWorkingHours, workHourValidation);
        }

        private void InitializeFocusLostEvents()
        {
            txtDailyWorkingHours.LostFocus += Control_LostFocus;
            txtMonthlyWorkingHours.LostFocus += Control_LostFocus;
        }

        void Control_LostFocus(object sender, EventArgs e)
        {
            validateHour.Validate((Control)sender);
        }
    }
   
}
