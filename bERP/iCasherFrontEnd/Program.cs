using System;
using System.Collections.Generic;
using System.Windows.Forms;

using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    static class Program
    {
        static DateTime lastRefresh = DateTime.Now;
        static int lastTransactionNumber = -1;
        static int tranNum = -1;
        public static int[] pendingMaterilization = null;

       

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                InitializeFormSkinning();
                Login l = new Login();
                l.TryLogin();
                if (l.logedin)
                {

                 
                    try
                    {

                        //Globals.Initialize();
                        MainForm mf = new MainForm();
                        new INTAPS.UI.UIFormApplicationBase(mf);
                        Application.Idle += new EventHandler(Application_Idle);
                        if (!INTAPS.ClientServer.Client.SecurityClient.IsPermited("root/iERP/login"))
                        {
                            System.Windows.Forms.MessageBox.Show("You are not authorized to login to finance system");
                            return;
                        }
                        Application.Run(mf);
                    }
                    finally
                    {
                        
                    }
                }
            }
            catch(Exception ex)
            {
                string msg = "";
                while(ex!=null)
                {
                    msg += "\n" + ex.Message+"\nStack Trace:\n"+ex.StackTrace;
                    ex = ex.InnerException;
                }
                System.Windows.Forms.MessageBox.Show(msg);

            }
        }
       
        static void Application_Idle(object sender, EventArgs e)
        {
        }
        
        static void getTransactionNumberThread()
        {
            while (true)
            {
                if (MainForm._quiting)
                    return;
                try
                {
                    tranNum = INTAPS.Accounting.Client.AccountingClient.GetTransactionNumber();
                    if (pendingMaterilization == null)
                    {
                        int[] id = INTAPS.Accounting.Client.AccountingClient.getMaterializePendingDocumentIDs(DateTime.Now);
                        if (id.Length > 0)
                            pendingMaterilization = id;
                    }
                }
                catch { }
                System.Threading.Thread.Sleep(4000);
            }
        }

        private static void InitializeFormSkinning()
        {
            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.LookAndFeel.LookAndFeelHelper.ForceDefaultLookAndFeelChanged();
            DevExpress.UserSkins.BonusSkins.Register();
        }
    }
}
