﻿namespace BIZNET.iERP.Client
{
    partial class StaffAccountConfigurationPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.accountPlaceHolder24 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder23 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder22 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder21 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder20 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder19 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder18 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder17 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder16 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.costCenterPlaceHolder1 = new INTAPS.Accounting.Client.CostCenterPlaceHolder();
            this.accountPlaceHolder15 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder14 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder13 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder12 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder11 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder10 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder9 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder8 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder7 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder6 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder5 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder4 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder3 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder2 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountPlaceHolder1 = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutShareHoldersLoan = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutOwnerEquity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.accountContextMenu1 = new INTAPS.Accounting.Client.AccountContextMenu();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutShareHoldersLoan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOwnerEquity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.accountPlaceHolder24);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder23);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder22);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder21);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder20);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder19);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder18);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder17);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder16);
            this.layoutControl1.Controls.Add(this.costCenterPlaceHolder1);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder15);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder14);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder13);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder12);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder11);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder10);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder9);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder8);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder7);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder6);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder5);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder4);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder3);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder2);
            this.layoutControl1.Controls.Add(this.accountPlaceHolder1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(489, 13, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(481, 458);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // accountPlaceHolder24
            // 
            this.accountPlaceHolder24.account = null;
            this.accountPlaceHolder24.AllowAdd = false;
            this.accountPlaceHolder24.Location = new System.Drawing.Point(208, 663);
            this.accountPlaceHolder24.Name = "accountPlaceHolder24";
            this.accountPlaceHolder24.OnlyLeafAccount = false;
            this.accountPlaceHolder24.Size = new System.Drawing.Size(242, 20);
            this.accountPlaceHolder24.TabIndex = 28;
            this.accountPlaceHolder24.Tag = "groupPayrollOverTimeCostAccount";
            // 
            // accountPlaceHolder23
            // 
            this.accountPlaceHolder23.account = null;
            this.accountPlaceHolder23.AllowAdd = false;
            this.accountPlaceHolder23.Location = new System.Drawing.Point(208, 633);
            this.accountPlaceHolder23.Name = "accountPlaceHolder23";
            this.accountPlaceHolder23.OnlyLeafAccount = false;
            this.accountPlaceHolder23.Size = new System.Drawing.Size(242, 20);
            this.accountPlaceHolder23.TabIndex = 27;
            this.accountPlaceHolder23.Tag = "groupPayrollOverTimeExpenseAccount";
            // 
            // accountPlaceHolder22
            // 
            this.accountPlaceHolder22.account = null;
            this.accountPlaceHolder22.AllowAdd = false;
            this.accountPlaceHolder22.Location = new System.Drawing.Point(202, 128);
            this.accountPlaceHolder22.Name = "accountPlaceHolder22";
            this.accountPlaceHolder22.OnlyLeafAccount = false;
            this.accountPlaceHolder22.Size = new System.Drawing.Size(254, 20);
            this.accountPlaceHolder22.TabIndex = 26;
            this.accountPlaceHolder22.Tag = "PermanentPayrollPensionContributionCostAccount";
            // 
            // accountPlaceHolder21
            // 
            this.accountPlaceHolder21.account = null;
            this.accountPlaceHolder21.AllowAdd = false;
            this.accountPlaceHolder21.Location = new System.Drawing.Point(202, 98);
            this.accountPlaceHolder21.Name = "accountPlaceHolder21";
            this.accountPlaceHolder21.OnlyLeafAccount = false;
            this.accountPlaceHolder21.Size = new System.Drawing.Size(254, 20);
            this.accountPlaceHolder21.TabIndex = 25;
            this.accountPlaceHolder21.Tag = "PermanentPayrollPensionContributionExpenseAccount";
            // 
            // accountPlaceHolder20
            // 
            this.accountPlaceHolder20.account = null;
            this.accountPlaceHolder20.AllowAdd = false;
            this.accountPlaceHolder20.Location = new System.Drawing.Point(208, 753);
            this.accountPlaceHolder20.Name = "accountPlaceHolder20";
            this.accountPlaceHolder20.OnlyLeafAccount = false;
            this.accountPlaceHolder20.Size = new System.Drawing.Size(242, 20);
            this.accountPlaceHolder20.TabIndex = 24;
            this.accountPlaceHolder20.Tag = "groupPayrollUnclaimedSalaryAccount";
            // 
            // accountPlaceHolder19
            // 
            this.accountPlaceHolder19.account = null;
            this.accountPlaceHolder19.AllowAdd = false;
            this.accountPlaceHolder19.Location = new System.Drawing.Point(208, 723);
            this.accountPlaceHolder19.Name = "accountPlaceHolder19";
            this.accountPlaceHolder19.OnlyLeafAccount = false;
            this.accountPlaceHolder19.Size = new System.Drawing.Size(242, 20);
            this.accountPlaceHolder19.TabIndex = 23;
            this.accountPlaceHolder19.Tag = "groupPayrollPensionPayableAccount";
            // 
            // accountPlaceHolder18
            // 
            this.accountPlaceHolder18.account = null;
            this.accountPlaceHolder18.AllowAdd = false;
            this.accountPlaceHolder18.Location = new System.Drawing.Point(208, 693);
            this.accountPlaceHolder18.Name = "accountPlaceHolder18";
            this.accountPlaceHolder18.OnlyLeafAccount = false;
            this.accountPlaceHolder18.Size = new System.Drawing.Size(242, 20);
            this.accountPlaceHolder18.TabIndex = 22;
            this.accountPlaceHolder18.Tag = "groupPayrollIncomeTaxAccount";
            // 
            // accountPlaceHolder17
            // 
            this.accountPlaceHolder17.account = null;
            this.accountPlaceHolder17.AllowAdd = false;
            this.accountPlaceHolder17.Location = new System.Drawing.Point(208, 603);
            this.accountPlaceHolder17.Name = "accountPlaceHolder17";
            this.accountPlaceHolder17.OnlyLeafAccount = false;
            this.accountPlaceHolder17.Size = new System.Drawing.Size(242, 20);
            this.accountPlaceHolder17.TabIndex = 21;
            this.accountPlaceHolder17.Tag = "groupPayrollCostAccount";
            // 
            // accountPlaceHolder16
            // 
            this.accountPlaceHolder16.account = null;
            this.accountPlaceHolder16.AllowAdd = false;
            this.accountPlaceHolder16.Location = new System.Drawing.Point(208, 573);
            this.accountPlaceHolder16.Name = "accountPlaceHolder16";
            this.accountPlaceHolder16.OnlyLeafAccount = false;
            this.accountPlaceHolder16.Size = new System.Drawing.Size(242, 20);
            this.accountPlaceHolder16.TabIndex = 20;
            this.accountPlaceHolder16.Tag = "groupPayrollExpenseAccount";
            // 
            // costCenterPlaceHolder1
            // 
            this.costCenterPlaceHolder1.account = null;
            this.costCenterPlaceHolder1.AllowAdd = false;
            this.costCenterPlaceHolder1.Location = new System.Drawing.Point(202, 8);
            this.costCenterPlaceHolder1.Name = "costCenterPlaceHolder1";
            this.costCenterPlaceHolder1.OnlyLeafAccount = false;
            this.costCenterPlaceHolder1.Size = new System.Drawing.Size(254, 20);
            this.costCenterPlaceHolder1.TabIndex = 19;
            this.costCenterPlaceHolder1.Tag = "mainCostCenterID";
            this.costCenterPlaceHolder1.TextChanged += new System.EventHandler(this.costCenterPlaceHolder1_TextChanged);
            // 
            // accountPlaceHolder15
            // 
            this.accountPlaceHolder15.account = null;
            this.accountPlaceHolder15.AllowAdd = false;
            this.accountPlaceHolder15.Location = new System.Drawing.Point(202, 458);
            this.accountPlaceHolder15.Name = "accountPlaceHolder15";
            this.accountPlaceHolder15.OnlyLeafAccount = false;
            this.accountPlaceHolder15.Size = new System.Drawing.Size(254, 20);
            this.accountPlaceHolder15.TabIndex = 18;
            this.accountPlaceHolder15.Tag = "staffPerDiemCostAccountID";
            // 
            // accountPlaceHolder14
            // 
            this.accountPlaceHolder14.account = null;
            this.accountPlaceHolder14.AllowAdd = false;
            this.accountPlaceHolder14.Location = new System.Drawing.Point(202, 158);
            this.accountPlaceHolder14.Name = "accountPlaceHolder14";
            this.accountPlaceHolder14.OnlyLeafAccount = false;
            this.accountPlaceHolder14.Size = new System.Drawing.Size(254, 20);
            this.accountPlaceHolder14.TabIndex = 17;
            this.accountPlaceHolder14.Tag = "TemporaryPayrollExpenseAccount";
            // 
            // accountPlaceHolder13
            // 
            this.accountPlaceHolder13.account = null;
            this.accountPlaceHolder13.AllowAdd = false;
            this.accountPlaceHolder13.Location = new System.Drawing.Point(202, 188);
            this.accountPlaceHolder13.Name = "accountPlaceHolder13";
            this.accountPlaceHolder13.OnlyLeafAccount = false;
            this.accountPlaceHolder13.Size = new System.Drawing.Size(254, 20);
            this.accountPlaceHolder13.TabIndex = 16;
            this.accountPlaceHolder13.Tag = "TemporaryPayrollCostAccount";
            // 
            // accountPlaceHolder12
            // 
            this.accountPlaceHolder12.account = null;
            this.accountPlaceHolder12.AllowAdd = false;
            this.accountPlaceHolder12.Location = new System.Drawing.Point(202, 68);
            this.accountPlaceHolder12.Name = "accountPlaceHolder12";
            this.accountPlaceHolder12.OnlyLeafAccount = false;
            this.accountPlaceHolder12.Size = new System.Drawing.Size(254, 20);
            this.accountPlaceHolder12.TabIndex = 15;
            this.accountPlaceHolder12.Tag = "PermanentPayrollCostAccount";
            // 
            // accountPlaceHolder11
            // 
            this.accountPlaceHolder11.account = null;
            this.accountPlaceHolder11.AllowAdd = false;
            this.accountPlaceHolder11.Location = new System.Drawing.Point(202, 38);
            this.accountPlaceHolder11.Name = "accountPlaceHolder11";
            this.accountPlaceHolder11.OnlyLeafAccount = false;
            this.accountPlaceHolder11.Size = new System.Drawing.Size(254, 20);
            this.accountPlaceHolder11.TabIndex = 14;
            this.accountPlaceHolder11.Tag = "PermanentPayrollExpenseAccount";
            // 
            // accountPlaceHolder10
            // 
            this.accountPlaceHolder10.account = null;
            this.accountPlaceHolder10.AllowAdd = false;
            this.accountPlaceHolder10.Location = new System.Drawing.Point(202, 488);
            this.accountPlaceHolder10.Name = "accountPlaceHolder10";
            this.accountPlaceHolder10.OnlyLeafAccount = false;
            this.accountPlaceHolder10.Size = new System.Drawing.Size(254, 20);
            this.accountPlaceHolder10.TabIndex = 13;
            this.accountPlaceHolder10.Tag = "OwnersEquityAccount";
            // 
            // accountPlaceHolder9
            // 
            this.accountPlaceHolder9.account = null;
            this.accountPlaceHolder9.AllowAdd = false;
            this.accountPlaceHolder9.Location = new System.Drawing.Point(202, 518);
            this.accountPlaceHolder9.Name = "accountPlaceHolder9";
            this.accountPlaceHolder9.OnlyLeafAccount = false;
            this.accountPlaceHolder9.Size = new System.Drawing.Size(254, 20);
            this.accountPlaceHolder9.TabIndex = 12;
            this.accountPlaceHolder9.Tag = "ShareHoldersLoanAccountID";
            // 
            // accountPlaceHolder8
            // 
            this.accountPlaceHolder8.account = null;
            this.accountPlaceHolder8.AllowAdd = false;
            this.accountPlaceHolder8.Location = new System.Drawing.Point(202, 428);
            this.accountPlaceHolder8.Name = "accountPlaceHolder8";
            this.accountPlaceHolder8.OnlyLeafAccount = false;
            this.accountPlaceHolder8.Size = new System.Drawing.Size(254, 20);
            this.accountPlaceHolder8.TabIndex = 11;
            this.accountPlaceHolder8.Tag = "staffPerDiemAccountID";
            // 
            // accountPlaceHolder7
            // 
            this.accountPlaceHolder7.account = null;
            this.accountPlaceHolder7.AllowAdd = false;
            this.accountPlaceHolder7.Location = new System.Drawing.Point(202, 398);
            this.accountPlaceHolder7.Name = "accountPlaceHolder7";
            this.accountPlaceHolder7.OnlyLeafAccount = false;
            this.accountPlaceHolder7.Size = new System.Drawing.Size(254, 20);
            this.accountPlaceHolder7.TabIndex = 10;
            this.accountPlaceHolder7.Tag = "LoanFromStaffAccountID";
            // 
            // accountPlaceHolder6
            // 
            this.accountPlaceHolder6.account = null;
            this.accountPlaceHolder6.AllowAdd = false;
            this.accountPlaceHolder6.Location = new System.Drawing.Point(202, 368);
            this.accountPlaceHolder6.Name = "accountPlaceHolder6";
            this.accountPlaceHolder6.OnlyLeafAccount = false;
            this.accountPlaceHolder6.Size = new System.Drawing.Size(254, 20);
            this.accountPlaceHolder6.TabIndex = 9;
            this.accountPlaceHolder6.Tag = "staffIncomeTaxAccountID";
            // 
            // accountPlaceHolder5
            // 
            this.accountPlaceHolder5.account = null;
            this.accountPlaceHolder5.AllowAdd = false;
            this.accountPlaceHolder5.Location = new System.Drawing.Point(202, 338);
            this.accountPlaceHolder5.Name = "accountPlaceHolder5";
            this.accountPlaceHolder5.OnlyLeafAccount = false;
            this.accountPlaceHolder5.Size = new System.Drawing.Size(254, 20);
            this.accountPlaceHolder5.TabIndex = 8;
            this.accountPlaceHolder5.Tag = "staffCostSharingPayableAccountID";
            // 
            // accountPlaceHolder4
            // 
            this.accountPlaceHolder4.account = null;
            this.accountPlaceHolder4.AllowAdd = false;
            this.accountPlaceHolder4.Location = new System.Drawing.Point(202, 308);
            this.accountPlaceHolder4.Name = "accountPlaceHolder4";
            this.accountPlaceHolder4.OnlyLeafAccount = false;
            this.accountPlaceHolder4.Size = new System.Drawing.Size(254, 20);
            this.accountPlaceHolder4.TabIndex = 7;
            this.accountPlaceHolder4.Tag = "staffPensionPayableAccountID";
            // 
            // accountPlaceHolder3
            // 
            this.accountPlaceHolder3.account = null;
            this.accountPlaceHolder3.AllowAdd = false;
            this.accountPlaceHolder3.Location = new System.Drawing.Point(202, 278);
            this.accountPlaceHolder3.Name = "accountPlaceHolder3";
            this.accountPlaceHolder3.OnlyLeafAccount = false;
            this.accountPlaceHolder3.Size = new System.Drawing.Size(254, 20);
            this.accountPlaceHolder3.TabIndex = 6;
            this.accountPlaceHolder3.Tag = "staffLongTermLoanAccountID";
            // 
            // accountPlaceHolder2
            // 
            this.accountPlaceHolder2.account = null;
            this.accountPlaceHolder2.AllowAdd = false;
            this.accountPlaceHolder2.Location = new System.Drawing.Point(202, 248);
            this.accountPlaceHolder2.Name = "accountPlaceHolder2";
            this.accountPlaceHolder2.OnlyLeafAccount = false;
            this.accountPlaceHolder2.Size = new System.Drawing.Size(254, 20);
            this.accountPlaceHolder2.TabIndex = 5;
            this.accountPlaceHolder2.Tag = "staffExpenseAdvanceAccountID";
            // 
            // accountPlaceHolder1
            // 
            this.accountPlaceHolder1.account = null;
            this.accountPlaceHolder1.AllowAdd = false;
            this.accountPlaceHolder1.Location = new System.Drawing.Point(202, 218);
            this.accountPlaceHolder1.Name = "accountPlaceHolder1";
            this.accountPlaceHolder1.OnlyLeafAccount = false;
            this.accountPlaceHolder1.Size = new System.Drawing.Size(254, 20);
            this.accountPlaceHolder1.TabIndex = 4;
            this.accountPlaceHolder1.Tag = "staffSalaryAdvanceAccountID";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutShareHoldersLoan,
            this.layoutOwnerEquity,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlGroup2,
            this.layoutControlItem20,
            this.layoutControlItem21});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(464, 787);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.accountPlaceHolder1;
            this.layoutControlItem1.CustomizationFormText = "Staff Salary Advance:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 210);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(458, 30);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Text = "Staff Salary Advance:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.accountPlaceHolder2;
            this.layoutControlItem2.CustomizationFormText = "Staff Purchase Advance:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 240);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(458, 30);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem2.Text = "Staff Purchase Advance:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.accountPlaceHolder3;
            this.layoutControlItem3.CustomizationFormText = "Staff Long Term Loan:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 270);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(458, 30);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "Staff Long Term Loan:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.accountPlaceHolder4;
            this.layoutControlItem4.CustomizationFormText = "Staff Pension Payable:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 300);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(458, 30);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem4.Text = "Staff Pension Payable:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.accountPlaceHolder5;
            this.layoutControlItem5.CustomizationFormText = "Staff Cost Sharing Payable:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 330);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(458, 30);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem5.Text = "Staff Cost Sharing Payable:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.accountPlaceHolder6;
            this.layoutControlItem6.CustomizationFormText = "Staff Income Tax:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 360);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(458, 30);
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem6.Text = "Staff Income Tax:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.accountPlaceHolder7;
            this.layoutControlItem7.CustomizationFormText = "Loan From Staff:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 390);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(458, 30);
            this.layoutControlItem7.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem7.Text = "Loan From Staff:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.accountPlaceHolder8;
            this.layoutControlItem8.CustomizationFormText = "Staff Per Diem:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 420);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(458, 30);
            this.layoutControlItem8.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem8.Text = "Staff Per Diem Expense:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutShareHoldersLoan
            // 
            this.layoutShareHoldersLoan.Control = this.accountPlaceHolder9;
            this.layoutShareHoldersLoan.CustomizationFormText = "Share Holders Loan:";
            this.layoutShareHoldersLoan.Location = new System.Drawing.Point(0, 510);
            this.layoutShareHoldersLoan.Name = "layoutShareHoldersLoan";
            this.layoutShareHoldersLoan.Size = new System.Drawing.Size(458, 30);
            this.layoutShareHoldersLoan.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutShareHoldersLoan.Text = "Share Holders Loan:";
            this.layoutShareHoldersLoan.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutOwnerEquity
            // 
            this.layoutOwnerEquity.Control = this.accountPlaceHolder10;
            this.layoutOwnerEquity.CustomizationFormText = "Owner\'s Equity:";
            this.layoutOwnerEquity.Location = new System.Drawing.Point(0, 480);
            this.layoutOwnerEquity.Name = "layoutOwnerEquity";
            this.layoutOwnerEquity.Size = new System.Drawing.Size(458, 30);
            this.layoutOwnerEquity.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutOwnerEquity.Text = "Owner\'s Equity:";
            this.layoutOwnerEquity.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.accountPlaceHolder11;
            this.layoutControlItem9.CustomizationFormText = "Premanent Employee Expense:";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(458, 30);
            this.layoutControlItem9.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem9.Text = "Premanent Employee Expense:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.accountPlaceHolder12;
            this.layoutControlItem10.CustomizationFormText = "Permanent Employee Cost:";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(458, 30);
            this.layoutControlItem10.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem10.Text = "Permanent Employee Cost:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.accountPlaceHolder13;
            this.layoutControlItem11.CustomizationFormText = "Temporary Employee Cost:";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 180);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(458, 30);
            this.layoutControlItem11.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem11.Text = "Temporary Employee Cost:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.accountPlaceHolder14;
            this.layoutControlItem12.CustomizationFormText = "Temporary Employee Expense:";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(458, 30);
            this.layoutControlItem12.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem12.Text = "Temporary Employee Expense:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.accountPlaceHolder15;
            this.layoutControlItem13.CustomizationFormText = "Staff Per-Diem Cost:";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 450);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(458, 30);
            this.layoutControlItem13.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem13.Text = "Staff Per-Diem Cost:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.costCenterPlaceHolder1;
            this.layoutControlItem14.CustomizationFormText = "Staff Head Quarter:";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(458, 30);
            this.layoutControlItem14.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem14.Text = "Staff Head Quarter:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "Group Payroll";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem22,
            this.layoutControlItem23});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 540);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup2.Size = new System.Drawing.Size(458, 241);
            this.layoutControlGroup2.Text = "Group Payroll";
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.accountPlaceHolder16;
            this.layoutControlItem15.CustomizationFormText = "Group Payroll Expense:";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(446, 30);
            this.layoutControlItem15.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem15.Text = "Group Payroll Expense:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.accountPlaceHolder17;
            this.layoutControlItem16.CustomizationFormText = "Group Payroll Cost:";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(446, 30);
            this.layoutControlItem16.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem16.Text = "Group Payroll Cost:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.accountPlaceHolder18;
            this.layoutControlItem17.CustomizationFormText = "Group Payroll Income Tax:";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(446, 30);
            this.layoutControlItem17.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem17.Text = "Group Payroll Income Tax:";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.accountPlaceHolder19;
            this.layoutControlItem18.CustomizationFormText = "Group Payroll Pension Payable:";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(446, 30);
            this.layoutControlItem18.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem18.Text = "Group Payroll Pension Payable:";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.accountPlaceHolder20;
            this.layoutControlItem19.CustomizationFormText = "Group Unclaimed Salary:";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 180);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(446, 30);
            this.layoutControlItem19.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem19.Text = "Group Unclaimed Salary:";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.accountPlaceHolder23;
            this.layoutControlItem22.CustomizationFormText = "Group Over Time Expense:";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(446, 30);
            this.layoutControlItem22.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem22.Tag = "";
            this.layoutControlItem22.Text = "Group Over Time Expense:";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.accountPlaceHolder24;
            this.layoutControlItem23.CustomizationFormText = "Group Over Time Cost:";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(446, 30);
            this.layoutControlItem23.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem23.Text = "Group Over Time Cost:";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.accountPlaceHolder21;
            this.layoutControlItem20.CustomizationFormText = "Company Pension Contribution Expense";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(458, 30);
            this.layoutControlItem20.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem20.Text = "Company Pension Contribution Expense";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(191, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.accountPlaceHolder22;
            this.layoutControlItem21.CustomizationFormText = "Company Pension Contribution Cost";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(458, 30);
            this.layoutControlItem21.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem21.Text = "Company Pension Contribution Cost";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(191, 13);
            // 
            // accountContextMenu1
            // 
            this.accountContextMenu1.LedgerFormater = null;
            this.accountContextMenu1.Name = "accountContextMenu1";
            this.accountContextMenu1.Size = new System.Drawing.Size(209, 180);
            // 
            // StaffAccountConfigurationPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "StaffAccountConfigurationPage";
            this.Size = new System.Drawing.Size(481, 458);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutShareHoldersLoan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOwnerEquity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder8;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder7;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder6;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder5;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder4;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder3;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder9;
        internal DevExpress.XtraLayout.LayoutControlItem layoutShareHoldersLoan;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder10;
        internal DevExpress.XtraLayout.LayoutControlItem layoutOwnerEquity;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder14;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder13;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder12;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private INTAPS.Accounting.Client.AccountContextMenu accountContextMenu1;
        private INTAPS.Accounting.Client.CostCenterPlaceHolder costCenterPlaceHolder1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder19;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder18;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder17;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder22;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder24;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountPlaceHolder23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
    }
}
