﻿namespace BIZNET.iERP.Client
{
    partial class TOTConfigurationPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtOtherServices = new DevExpress.XtraEditors.TextEdit();
            this.txtTractors = new DevExpress.XtraEditors.TextEdit();
            this.txtMillService = new DevExpress.XtraEditors.TextEdit();
            this.txtContractors = new DevExpress.XtraEditors.TextEdit();
            this.txtGoods = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtOtherServices.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTractors.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMillService.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContractors.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoods.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtOtherServices);
            this.layoutControl1.Controls.Add(this.txtTractors);
            this.layoutControl1.Controls.Add(this.txtMillService);
            this.layoutControl1.Controls.Add(this.txtContractors);
            this.layoutControl1.Controls.Add(this.txtGoods);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(450, 208);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtOtherServices
            // 
            this.txtOtherServices.Location = new System.Drawing.Point(199, 164);
            this.txtOtherServices.Name = "txtOtherServices";
            this.txtOtherServices.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtOtherServices.Properties.Mask.EditMask = "#,0.00%;";
            this.txtOtherServices.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtOtherServices.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtOtherServices.Size = new System.Drawing.Size(227, 20);
            this.txtOtherServices.StyleController = this.layoutControl1;
            this.txtOtherServices.TabIndex = 8;
            // 
            // txtTractors
            // 
            this.txtTractors.Location = new System.Drawing.Point(199, 134);
            this.txtTractors.Name = "txtTractors";
            this.txtTractors.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtTractors.Properties.Mask.EditMask = "#,0.00%;";
            this.txtTractors.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTractors.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTractors.Size = new System.Drawing.Size(227, 20);
            this.txtTractors.StyleController = this.layoutControl1;
            this.txtTractors.TabIndex = 7;
            // 
            // txtMillService
            // 
            this.txtMillService.Location = new System.Drawing.Point(199, 104);
            this.txtMillService.Name = "txtMillService";
            this.txtMillService.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtMillService.Properties.Mask.EditMask = "#,0.00%;";
            this.txtMillService.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtMillService.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtMillService.Size = new System.Drawing.Size(227, 20);
            this.txtMillService.StyleController = this.layoutControl1;
            this.txtMillService.TabIndex = 6;
            // 
            // txtContractors
            // 
            this.txtContractors.Location = new System.Drawing.Point(199, 74);
            this.txtContractors.Name = "txtContractors";
            this.txtContractors.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtContractors.Properties.Mask.EditMask = "#,0.00%;";
            this.txtContractors.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtContractors.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtContractors.Size = new System.Drawing.Size(227, 20);
            this.txtContractors.StyleController = this.layoutControl1;
            this.txtContractors.TabIndex = 5;
            // 
            // txtGoods
            // 
            this.txtGoods.Location = new System.Drawing.Point(190, 15);
            this.txtGoods.Name = "txtGoods";
            this.txtGoods.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtGoods.Properties.Mask.EditMask = "#,0.00%;";
            this.txtGoods.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGoods.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtGoods.Size = new System.Drawing.Size(245, 20);
            this.txtGoods.StyleController = this.layoutControl1;
            this.txtGoods.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(450, 208);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtGoods;
            this.layoutControlItem1.CustomizationFormText = "Goods Taxable:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(430, 30);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Text = "Goods Taxable Rate:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(171, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "Services Taxable";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 30);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(6, 6, 6, 6);
            this.layoutControlGroup2.Size = new System.Drawing.Size(430, 158);
            this.layoutControlGroup2.Text = "Services Taxable Rate";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtContractors;
            this.layoutControlItem2.CustomizationFormText = "Contractors:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(412, 30);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem2.Text = "Contractors:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(171, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtMillService;
            this.layoutControlItem3.CustomizationFormText = "Mill Sevices:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(412, 30);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "Mill Sevices:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(171, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtTractors;
            this.layoutControlItem4.CustomizationFormText = "Tractors and Combined Harvesters:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(412, 30);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem4.Text = "Tractors and Combined Harvesters:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(171, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtOtherServices;
            this.layoutControlItem5.CustomizationFormText = "All other services:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(412, 30);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem5.Text = "All other services:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(171, 13);
            // 
            // TOTConfigurationPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "TOTConfigurationPage";
            this.Size = new System.Drawing.Size(450, 208);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtOtherServices.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTractors.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMillService.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContractors.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoods.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        internal DevExpress.XtraEditors.TextEdit txtGoods;
        internal DevExpress.XtraEditors.TextEdit txtContractors;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        internal DevExpress.XtraEditors.TextEdit txtMillService;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        internal DevExpress.XtraEditors.TextEdit txtTractors;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        internal DevExpress.XtraEditors.TextEdit txtOtherServices;
    }
}
