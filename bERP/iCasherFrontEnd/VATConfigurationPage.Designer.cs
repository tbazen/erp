﻿namespace BIZNET.iERP.Client
{
    partial class VATConfigurationPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtRawHideExportVAT = new DevExpress.XtraEditors.TextEdit();
            this.txtExportVAT = new DevExpress.XtraEditors.TextEdit();
            this.txtCommonVAT = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.txtVATWithholdLimit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRawHideExportVAT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExportVAT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCommonVAT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATWithholdLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtVATWithholdLimit);
            this.layoutControl1.Controls.Add(this.txtRawHideExportVAT);
            this.layoutControl1.Controls.Add(this.txtExportVAT);
            this.layoutControl1.Controls.Add(this.txtCommonVAT);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(477, 142);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtRawHideExportVAT
            // 
            this.txtRawHideExportVAT.Location = new System.Drawing.Point(214, 75);
            this.txtRawHideExportVAT.Name = "txtRawHideExportVAT";
            this.txtRawHideExportVAT.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtRawHideExportVAT.Properties.Mask.EditMask = "#,#0.00%;";
            this.txtRawHideExportVAT.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtRawHideExportVAT.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtRawHideExportVAT.Size = new System.Drawing.Size(248, 20);
            this.txtRawHideExportVAT.StyleController = this.layoutControl1;
            this.txtRawHideExportVAT.TabIndex = 6;
            // 
            // txtExportVAT
            // 
            this.txtExportVAT.Location = new System.Drawing.Point(214, 45);
            this.txtExportVAT.Name = "txtExportVAT";
            this.txtExportVAT.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtExportVAT.Properties.Mask.EditMask = "#,#0.00%;";
            this.txtExportVAT.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtExportVAT.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtExportVAT.Size = new System.Drawing.Size(248, 20);
            this.txtExportVAT.StyleController = this.layoutControl1;
            this.txtExportVAT.TabIndex = 5;
            // 
            // txtCommonVAT
            // 
            this.txtCommonVAT.Location = new System.Drawing.Point(214, 15);
            this.txtCommonVAT.Name = "txtCommonVAT";
            this.txtCommonVAT.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtCommonVAT.Properties.Mask.EditMask = "#,#0.00%;";
            this.txtCommonVAT.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCommonVAT.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtCommonVAT.Size = new System.Drawing.Size(248, 20);
            this.txtCommonVAT.StyleController = this.layoutControl1;
            this.txtCommonVAT.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(477, 142);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtCommonVAT;
            this.layoutControlItem1.CustomizationFormText = "Common VAT Rate:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(457, 30);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Text = "Common VAT Rate:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(196, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtExportVAT;
            this.layoutControlItem2.CustomizationFormText = "Export VAT Rate:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(457, 30);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem2.Text = "Export VAT Rate:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(196, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtRawHideExportVAT;
            this.layoutControlItem3.CustomizationFormText = "Rawhide Export VAT Rate:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(457, 30);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "Rawhide Export VAT Rate:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(196, 13);
            // 
            // txtVATWithholdLimit
            // 
            this.txtVATWithholdLimit.Location = new System.Drawing.Point(214, 105);
            this.txtVATWithholdLimit.Name = "txtVATWithholdLimit";
            this.txtVATWithholdLimit.Size = new System.Drawing.Size(248, 20);
            this.txtVATWithholdLimit.StyleController = this.layoutControl1;
            this.txtVATWithholdLimit.TabIndex = 7;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtVATWithholdLimit;
            this.layoutControlItem4.CustomizationFormText = "Non-Taxable VAT Withholding Price Limit:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(457, 32);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem4.Text = "Non-Taxable VAT Withholding Price Limit:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(196, 13);
            // 
            // VATConfigurationPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "VATConfigurationPage";
            this.Size = new System.Drawing.Size(477, 142);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtRawHideExportVAT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExportVAT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCommonVAT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATWithholdLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        internal DevExpress.XtraEditors.TextEdit txtCommonVAT;
        internal DevExpress.XtraEditors.TextEdit txtExportVAT;
        internal DevExpress.XtraEditors.TextEdit txtRawHideExportVAT;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        internal DevExpress.XtraEditors.TextEdit txtVATWithholdLimit;
    }
}
