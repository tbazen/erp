﻿namespace BIZNET.iERP.Client
{
    partial class WithHoldingConfigurationPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtUnknownTIN = new DevExpress.XtraEditors.TextEdit();
            this.txtServicesTaxable = new DevExpress.XtraEditors.TextEdit();
            this.txtServicesLimitPrice = new DevExpress.XtraEditors.TextEdit();
            this.txtGoodsTaxable = new DevExpress.XtraEditors.TextEdit();
            this.txtGoodsLimitPrice = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnknownTIN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServicesTaxable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServicesLimitPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoodsTaxable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoodsLimitPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtUnknownTIN);
            this.layoutControl1.Controls.Add(this.txtServicesTaxable);
            this.layoutControl1.Controls.Add(this.txtServicesLimitPrice);
            this.layoutControl1.Controls.Add(this.txtGoodsTaxable);
            this.layoutControl1.Controls.Add(this.txtGoodsLimitPrice);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(423, 270);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtUnknownTIN
            // 
            this.txtUnknownTIN.Location = new System.Drawing.Point(177, 211);
            this.txtUnknownTIN.Name = "txtUnknownTIN";
            this.txtUnknownTIN.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtUnknownTIN.Properties.Mask.EditMask = "#,0.00%;";
            this.txtUnknownTIN.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtUnknownTIN.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtUnknownTIN.Size = new System.Drawing.Size(231, 20);
            this.txtUnknownTIN.StyleController = this.layoutControl1;
            this.txtUnknownTIN.TabIndex = 8;
            // 
            // txtServicesTaxable
            // 
            this.txtServicesTaxable.Location = new System.Drawing.Point(186, 172);
            this.txtServicesTaxable.Name = "txtServicesTaxable";
            this.txtServicesTaxable.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtServicesTaxable.Properties.Mask.EditMask = "#,0.00%;";
            this.txtServicesTaxable.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtServicesTaxable.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtServicesTaxable.Size = new System.Drawing.Size(213, 20);
            this.txtServicesTaxable.StyleController = this.layoutControl1;
            this.txtServicesTaxable.TabIndex = 7;
            // 
            // txtServicesLimitPrice
            // 
            this.txtServicesLimitPrice.Location = new System.Drawing.Point(186, 142);
            this.txtServicesLimitPrice.Name = "txtServicesLimitPrice";
            this.txtServicesLimitPrice.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtServicesLimitPrice.Properties.Mask.EditMask = "#,#####0.00;";
            this.txtServicesLimitPrice.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtServicesLimitPrice.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtServicesLimitPrice.Size = new System.Drawing.Size(213, 20);
            this.txtServicesLimitPrice.StyleController = this.layoutControl1;
            this.txtServicesLimitPrice.TabIndex = 6;
            // 
            // txtGoodsTaxable
            // 
            this.txtGoodsTaxable.Location = new System.Drawing.Point(186, 74);
            this.txtGoodsTaxable.Name = "txtGoodsTaxable";
            this.txtGoodsTaxable.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtGoodsTaxable.Properties.Mask.EditMask = "#,0.00%;";
            this.txtGoodsTaxable.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGoodsTaxable.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtGoodsTaxable.Size = new System.Drawing.Size(213, 20);
            this.txtGoodsTaxable.StyleController = this.layoutControl1;
            this.txtGoodsTaxable.TabIndex = 5;
            // 
            // txtGoodsLimitPrice
            // 
            this.txtGoodsLimitPrice.Location = new System.Drawing.Point(186, 44);
            this.txtGoodsLimitPrice.Name = "txtGoodsLimitPrice";
            this.txtGoodsLimitPrice.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtGoodsLimitPrice.Properties.Mask.EditMask = "#,#####0.00;";
            this.txtGoodsLimitPrice.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGoodsLimitPrice.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtGoodsLimitPrice.Size = new System.Drawing.Size(213, 20);
            this.txtGoodsLimitPrice.StyleController = this.layoutControl1;
            this.txtGoodsLimitPrice.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(423, 270);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "Goods With-Holding";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(6, 6, 6, 6);
            this.layoutControlGroup2.Size = new System.Drawing.Size(403, 98);
            this.layoutControlGroup2.Text = "Goods With-Holding";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtGoodsTaxable;
            this.layoutControlItem2.CustomizationFormText = "Goods Taxable:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(385, 30);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem2.Text = "Goods Taxable Rate:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(158, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtGoodsLimitPrice;
            this.layoutControlItem1.CustomizationFormText = "Goods Non-Taxable price limit:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(385, 30);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Text = "Goods Non-Taxable price limit:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(158, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.CustomizationFormText = "Services With-Holding";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 98);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(6, 6, 6, 6);
            this.layoutControlGroup3.Size = new System.Drawing.Size(403, 98);
            this.layoutControlGroup3.Text = "Services With-Holding";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtServicesLimitPrice;
            this.layoutControlItem3.CustomizationFormText = "Services Non-Taxable price limit:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(385, 30);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "Services Non-Taxable price limit:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(158, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtServicesTaxable;
            this.layoutControlItem4.CustomizationFormText = "Services Taxable:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(385, 30);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem4.Text = "Services Taxable Rate:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(158, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtUnknownTIN;
            this.layoutControlItem5.CustomizationFormText = "Unknown TIN With-Holding Rate:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 196);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(403, 54);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem5.Text = "Unknown TIN With-Holding Rate:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(158, 13);
            // 
            // WithHoldingConfigurationPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "WithHoldingConfigurationPage";
            this.Size = new System.Drawing.Size(423, 270);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtUnknownTIN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServicesTaxable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServicesLimitPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoodsTaxable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoodsLimitPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        internal DevExpress.XtraEditors.TextEdit txtGoodsTaxable;
        internal DevExpress.XtraEditors.TextEdit txtGoodsLimitPrice;
        internal DevExpress.XtraEditors.TextEdit txtServicesLimitPrice;
        internal DevExpress.XtraEditors.TextEdit txtServicesTaxable;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        internal DevExpress.XtraEditors.TextEdit txtUnknownTIN;
    }
}
