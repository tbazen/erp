
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;

using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using BIZNET.iERP.Server;
using BIZNET.iERP;
using System.Threading;
using INTAPS.RDBMS;
using BIZNET.iERPMan;

namespace INTAPS.ClientServer.RemottingServer
{
    class MaintenanceJobMethod : Attribute
    {
        public string name;
        public MaintenanceJobMethod(string n)
        {
            this.name = n;
        }
    }
   
    public static partial class MaintainanceJob
    {
        public static void DoJob(string args)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            try
            {
                Console.WriteLine("Which job do want to run?");
                SortedList<string, MethodInfo> methods = new SortedList<string, MethodInfo>();
                foreach (MethodInfo m in typeof(MaintainanceJob).GetMethods())
                {
                    object[] atr = m.GetCustomAttributes(typeof(MaintenanceJobMethod), false);
                    if (atr == null || atr.Length == 0)
                        continue;
                    MaintenanceJobMethod jb = atr[0] as MaintenanceJobMethod;

                    methods.Add(jb.name, m);
                }
                int count = 1;

                foreach (KeyValuePair<string, MethodInfo> kv in methods)
                {
                    Console.WriteLine("{0}:\t{1}", count++, kv.Key);
                }
                Console.WriteLine("{0}:\t{1}", "Q", "Quit");
                do
                {
                    Console.Write("?: ");
                    string read = Console.ReadLine();
                    if (read.Equals("Q", StringComparison.CurrentCultureIgnoreCase))
                        return;
                    if (int.TryParse(read, out count) && count <= methods.Count && count > 0)
                    {
                        methods.Values[count - 1].Invoke(null, new object[0]);
                        return;
                    }
                }
                while (true);
            }
            catch (Exception bex)
            {
                Console.WriteLine(bex.Message);
                Console.WriteLine();
            }
            finally
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Job Finished, press enter to terminate application");
                Console.ReadLine();
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }

        static System.IO.StreamWriter logStream = null;
        static void OpenLogStream()
        {
            if (logStream == null)
                logStream = System.IO.File.CreateText("ServerMaintenanceJob.txt");

        }
        static void writeLog(string line)
        {
            Console.WriteLine(line);
            if (logStream != null)
                OpenLogStream();
            logStream.WriteLine(line);
        }
        static void closeLogStream()
        {
            if (logStream != null)
                logStream.Close();
        }
        [MaintenanceJobMethod("Open and close costing periods")]
        public static void openAndCloseCostingPeriods()
        {
            var bde = ApplicationServer.GetBDE("iERPMan") as BIZNET.iERPMan.Server.iERPManBDE;
            Console.WriteLine("Closing and opening costing periods");
            int C;
            CostingPeriod [] periods=bde.getCostingPeriods(0, -1, -1, 0, -1, out C);
            bde.WriterHelper.BeginTransaction();
            try
            {
                int AID = ApplicationServer.SecurityBDE.WriteAddAudit("Admin", 1, "Maintenance Job - Open and close costing periods", "", -1);
                Console.WriteLine("{0} periods found", C);
                Console.WriteLine("Opening..", C);
                Console.WriteLine();

                List<CostingPeriod> openedCostingPeriods = new List<CostingPeriod>();
                foreach (BIZNET.iERPMan.CostingPeriod p in periods)
                {
                    Console.CursorTop--;
                    Console.WriteLine("{0}                  ", C--);
                    if (p.closed)
                    {
                        bde.openCostingPeriod(AID, p.level, p.t1);
                        openedCostingPeriods.Add(p);
                    }
                }
                C = openedCostingPeriods.Count;
                Console.WriteLine("{0} periods re-opended", C);
                Console.WriteLine("Closing..", C);
                Console.WriteLine();

                foreach (BIZNET.iERPMan.CostingPeriod p in openedCostingPeriods)
                {
                    Console.CursorTop--;
                    Console.WriteLine("{0}                  ", C--);
                    bde.closeCostingPeriod(AID, p.level, p.t1);
                }

                bde.WriterHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                bde.WriterHelper.RollBackTransaction();
                Console.WriteLine(ex.Message);
            }
        }
    }
}

