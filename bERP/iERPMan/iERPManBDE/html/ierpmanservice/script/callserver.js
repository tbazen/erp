﻿var serviceUrl = 'webproxy.ph';
function _proxy(server_method,pars,done,error,data)
{
    var url = serviceUrl + "?sid=" + __sessionID;
    var method = "POST";
    var async = true;
    var request = new XMLHttpRequest();
    request.onload = function () {
        var resp = request.response;
        var respData = JSON.parse(resp);
        if (respData.error) {
            if (error)
                error(respData.error);
        }
        else {
            if(done)
            {
                done(respData.ret,data);
            }
        }
    };
    var rd = { "requestType": server_method, "requestData": pars };
    request.open(method, url, async);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(rd));
}
