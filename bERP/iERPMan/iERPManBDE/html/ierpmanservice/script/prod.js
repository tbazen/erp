﻿var date1;
var date2;
var trendUnit="";
var updateFunction;
function prod_dateTimeChanged() {
//    console.log(date_ranges);
    for (var i = 0; i < date_ranges.length; i++) {
        if (date_ranges[i].name == prod_date_dd.value) {
            setDateRange(date_ranges[i].d1, date_ranges[i].d2);
            break;
        }
    }
};
function prod_dateFromChanged()
{
    prod_date_dd.value = "";
    setDateRange(datetimepicker1.value, date2);
};
function prod_dateToChanged()
{
    prod_date_dd.value = "";
    setDateRange(date1, datetimepicker2.value);
};
function setDateRange(d1,d2) {
    if (d1 == date2 && d2 == date2) 
        return;
    date1 = d1;
    date2 = d2;
    datetimepicker1.value = d1;
    datetimepicker2.value = d2;
    if(updateFunction)
        updateFunction();
};
function updateCreditSettlementReport() {
    if (date1 == null || date2 == null)
        return;
    creditSettlementContainer.innerText = "Loadding data...";
    _proxy("getCreditSettlementTable", { d1: date1, d2: date2 }, function (r) //done
    {
        creditSettlementContainer.innerHTML = r;
    }, function (e) //errpr
    {
        creditSettlementContainer.innerText = e;
    }
    );
};
function updateProdReport()
{
    if (date1 == null || date2 == null)
        return;
    productionContainer.innerText = "Loadding data...";
    _proxy("getProductionTable", { d1: date1, d2: date2, adj: adjMode }, function(r) //done
        {
            productionContainer.innerHTML = r;
        }, function(e) //errpr
        {
            productionContainer.innerText = e;
        }
    );
};
function clearInventoryReports()
{
    for (i = 0; i < storeIds.length;i++)
        document.getElementById(storeIds[i]).innerHTML = "";
};
function loadInventory(e,storeID)
{
    
    var el = document.getElementById(e);
    if (el.innerHTML != "") {
        el.innerHTML = "";
        return;
    }
    clearInventoryReports();
    el.innerText = "Loadding data...";
    _proxy("getInventoryTable", { "storeID": storeID }
        , function (r)//done
        {
            el.innerHTML = r;
        }
        , function (er)//errpr
        {
            el.innerText = er;
        }
        );
}
function loadBelowNormaLevel(e)
{
    var el = document.getElementById(e);
    if (el.innerHTML != "") {
        el.innerHTML = "";
        return;
    }
    clearInventoryReports();
    el.innerText = "Loadding data...";
    _proxy("getBelowNormalLevelStock", {}
        , function (r)//done
        {
            
            el.innerHTML = r;
        }
        , function (er)//errpr
        {
            el.innerText = er;
        }
        );
};
function loadAboveNormaLevel(e) {
    var el = document.getElementById(e);
    if (el.innerHTML != "") {
        el.innerHTML = "";
        return;
    }
    clearInventoryReports();
    el.innerText = "Loadding data...";
    _proxy("getAboveNormalLevelStock", {}
        , function (r)//done
        {
            el.innerHTML = r;
        }
        , function (er)//errpr
        {
            el.innerText = er;
        }
        );
};
function checkMaterial()
{
    //batchNo.value = "";
    result_pane.innerText = "Loadding data...";
    _proxy("getMaterialDetail", { itemCode: itemCode.value}
        , function (r)//done
        {
            result_pane.innerHTML = r;
        }
        , function (e)//error
        {
            result_pane.innerText = e;
        }
        );
};
function showStockCard(itemName,itemCode,storeID)
{
    result_pane.innerText = "Loadding stock card ...";
    _proxy("getStockCard", { code: itemCode,storeID:storeID}
        , function (r)//done
        {
            result_pane.innerHTML = "<h2>Bin Card of :"+itemName+"</h2>"+ r;
        }
        , function (e)//error
        {
            result_pane.innerText = e;
        }
        );
};
function checkBatch() {
    itemCode.value = "";
    result_pane.innerText = "Loadding data...";
    _proxy("getBatchDetail", { batchNo: batchNo.value }
        , function (r)//done
        {
            result_pane.innerHTML = r;
        }
        , function (e)//errpr
        {
            result_pane.innerText = e;
        }
        );
};
function sales_trendChanged()
{
    if (trendUnit == trendUnitSelector.value)
        return;
    trendUnit = trendUnitSelector.value;
    updateFunction();
};
var salesLineChart=null;
var salesPieChart = null;
var salesCustPieChart = null;
function updateSalesReport()
{
    if (date1 == null || date2 == null)
        return;
    trendUnit = trendUnitSelector.value;
    salesReportError.innerText = "Loadding data...";
    _proxy("getSalesData", { d1: date1, d2: date2,trendUnit:trendUnit,amount:true }
        , function (r)//done
        {
            var lineChartData = {
                labels: r.trendChart.labels,
                datasets: [
                    {
                        fillColor: "rgba(255,255,255,0.1)",
                        strokeColor: "rgba(151,187,205,1)",
                        pointColor: "rgba(25,220,220,1)",
                        pointStrokeColor: "#fff",
                        
                        data: r.trendChart.values,
                    }
                ]

            };
            if (salesLineChart)
                salesLineChart.destroy();
            salesLineChart = new Chart(document.getElementById("salesTend").getContext("2d")).Line
                (
                lineChartData,
                {
                    responsive: true,
                    animation: false,
                    scaleStartValue: 0,
            });
            var pieData = [];
            var leg = document.getElementById("distLegend");
            while (leg.firstChild)
                leg.removeChild(leg.firstChild);

            var legtr = document.createElement("tr");
            legtr.setAttribute("class", "chart_legend_row");

            var tdLengendLabel = document.createElement("th");
            tdLengendLabel.setAttribute("class", "chart_legend_label");
            tdLengendLabel.innerText = "Product";

            var tdLegendColor = document.createElement("th");
            tdLegendColor.setAttribute("class", "chart_legend_color_container");

            var tdLegendValue = document.createElement("th");
            tdLegendValue.setAttribute("class", "chart_legend_value");
            tdLegendValue.innerText = "Price";

            var tdLegendQuantity = document.createElement("th");
            tdLegendQuantity.setAttribute("class", "chart_legend_quantity");
            tdLegendQuantity.innerText = "Quantity";



            legtr.appendChild(tdLegendColor);
            legtr.appendChild(tdLengendLabel);
            legtr.appendChild(tdLegendQuantity);
            legtr.appendChild(tdLegendValue);
            leg.appendChild(legtr);
            for (i = 0; i<r.distChart.labels.length; i++) {
                pieData.push(
                    {
                        value: r.distChart.values[i],
                        color: r.distChart.colors[i],
                        label: r.distChart.labels[i],
                        labelColor: 'white',
                        labelFontSize: '16'
                    }
                    );
                legtr = document.createElement("tr");
                legtr.setAttribute("class", "chart_legend_row");
                tdLengendLabel = document.createElement("td");
                tdLengendLabel.setAttribute("class", "chart_legend_label");
                tdLengendLabel.innerText = r.distChart.labels[i];
                tdLegendColor = document.createElement("td");
                tdLegendColor.setAttribute("class", "chart_legend_color_container");

                tdLegendValue = document.createElement("td");
                tdLegendValue.setAttribute("class", "chart_legend_value");
                tdLegendValue.innerText = numeral(r.distChart.values[i]).format("0,0.00")+" Birr";

                tdLegendQuantity = document.createElement("td");
                tdLegendQuantity.setAttribute("class", "chart_legend_quantity");
                tdLegendQuantity.innerText = numeral(r.distChart.quantity[i]).format('0,0') + " " + r.distChart.untis[i];


                legColor = document.createElement("div");
                legColor.setAttribute("class", "chart_legend_color");
                legColor.setAttribute("style", "background:" + r.distChart.colors[i]);
                
                tdLegendColor.appendChild(legColor);

                legtr.appendChild(tdLegendColor);
                legtr.appendChild(tdLengendLabel);
                legtr.appendChild(tdLegendQuantity);
                legtr.appendChild(tdLegendValue);

                leg.appendChild(legtr);
            }

            if (salesPieChart)
                salesPieChart.destroy();
            salesPieChart= new Chart(document.getElementById("salesDist").getContext("2d")).Pie(pieData,
                {

                    responsive: true,
                    animation:false,
                });
            buildCustSalesReport(r);
            salesReportError.innerText = "";
            productSold2.innerText = numeral(r.productionAmount).format("0,0.00") + " Birr";
            productSold.innerText = r.productionQuantity + " " + r.unit + " " + numeral(r.productionAmount).format("0,0.00") + " Birr";
            costOfProductSold.innerText = numeral(r.costOfProductSold).format("0,0.00") + " Birr";
            if (Math.round(r.costOfProductSold * 1000) == 0)
                sellsProfit.innerText = "-";
            else
                sellsProfit.innerText = numeral((r.productionAmount - r.costOfProductSold) / r.costOfProductSold).format("0%");
        }
        , function (e)//errpr
        {
            salesReportError.innerText = e;
        }
        );
};
function formatNumber(num)
{
    if (num == 0)
        return "";
    else
        return numeral(num).format("0,0.00") + " Birr";
};
function buildCustSalesReport(r)
{
    var pieData = [];
    var leg = document.getElementById("distCustLegend");
    while (leg.firstChild)
        leg.removeChild(leg.firstChild);

    var legtr = document.createElement("tr");
    legtr.setAttribute("class", "chart_legend_row");

    var tdLengendLabel = document.createElement("th");
    tdLengendLabel.setAttribute("class", "chart_legend_label");
    tdLengendLabel.innerText = "Product";

    var tdLegendColor = document.createElement("th");
    tdLegendColor.setAttribute("class", "chart_legend_color_container");

    var tdLegendValue = document.createElement("th");
    tdLegendValue.setAttribute("class", "chart_legend_value");
    tdLegendValue.innerText = "Price";

    var tdLegendCredit = document.createElement("th");
    tdLegendCredit.setAttribute("class", "chart_legend_value");
    tdLegendCredit.innerText = "Credit";



    legtr.appendChild(tdLegendColor);
    legtr.appendChild(tdLengendLabel);
    legtr.appendChild(tdLegendValue);
    legtr.appendChild(tdLegendCredit);
    leg.appendChild(legtr);
    for (i = 0; i < r.custDistChart.custData.length; i++) {
        pieData.push(
            {
                value: r.custDistChart.custData[i].totalValue,
                color: r.custDistChart.custData[i].color,
                label: r.custDistChart.custData[i].label,
                labelColor: 'white',
                labelFontSize: '16'
            }
            );
        legtr = document.createElement("tr");
        legtr.setAttribute("class", "chart_legend_row");

        tdLengendLabel = document.createElement("td");
        tdLengendLabel.setAttribute("class", "chart_legend_label");
        tdLengendLabel.innerText = r.custDistChart.custData[i].label;

        tdLegendColor = document.createElement("td");
        tdLegendColor.setAttribute("class", "chart_legend_color_container");

        tdLegendValue = document.createElement("td");
        tdLegendValue.setAttribute("class", "chart_legend_value");
        tdLegendValue.innerText = numeral(r.custDistChart.custData[i].totalValue).format("0,0.00") + " Birr";

        tdLegendCredit = document.createElement("td");
        tdLegendCredit.setAttribute("class", "chart_legend_value");
        tdLegendCredit.innerText = formatNumber(r.custDistChart.custData[i].creditValue);
            



        legColor = document.createElement("div");
        legColor.setAttribute("class", "chart_legend_color");
        legColor.setAttribute("style", "background:" + r.custDistChart.custData[i].color);

        tdLegendColor.appendChild(legColor);

        legtr.appendChild(tdLegendColor);
        legtr.appendChild(tdLengendLabel);
        legtr.appendChild(tdLegendValue);
        legtr.appendChild(tdLegendCredit);

        leg.appendChild(legtr);
    }

    if (salesCustPieChart)
        salesCustPieChart.destroy();
    salesCustPieChart = new Chart(document.getElementById("salesCustDist").getContext("2d")).Pie(pieData,
        {
            responsive: true,
            animation: false,
        });
};
function search_batch()
{
    var d1 = datetimepicker1.value;
    var d2 = datetimepicker2.value;
    if (d1 == null || d1 == "" || d2 == null || d2 == "")
        return;
    search_result.innerText = "Loadding data...";
    _proxy("searchBatch", { pageIndex: 0,byDate:true,d1:d1,d2:d2,product:null }
        , function (r)//done
        {
            search_result.innerHTML = r.resultTable;
            search_pages.innerHTML = r.navigationPane;
        }
        , function (e)//errpr
        {
            search_result.innerText = e;
            search_pages.innerHTML = "";
        }
        );
};
function showMaterialDetail(code)
{
    itemCode.value=code;
    checkMaterial();
};