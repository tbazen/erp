﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.ClientServer;
using System.Data;
using System.Xml.Serialization;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Reflection;
using INTAPS.Accounting.BDE;
using INTAPS.Accounting;
using BIZNET.iERP;
using INTAPS.ClientServer.RemottingServer;
using INTAPS;
namespace BIZNET.iERPMan.Server
{

    public partial class iERPManBDE : BDEBase
    {
        // BDE exposed
        public Factory getFactory(int costCenterID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                Factory[] ret = reader.GetSTRArrayByFilter<Factory>("costCenterID=" + costCenterID);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public Factory[] getAllFactories()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<Factory>(null);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public void setFactory(int AID, Factory factory)
        {
            CostCenter cs = accountingBDE.GetAccount<CostCenter>(factory.costCenterID);
            if (cs == null || cs.isControl)
                throw new ServerUserMessage("Only a  alid leave cost center can be set as factory floor");
            StoreInfo s = berpBDE.GetStoreInfo(factory.defaultStoreID);
            if (s == null)
                throw new ServerUserMessage("A default store should be set for the factory floor.");
            Factory oldFactory = getFactory(factory.costCenterID);
            if (oldFactory == null)
                dspWriter.InsertSingleTableRecord(this.DBName, factory, new string[] { "__AID" }, new object[] { AID });
            else
            {
                string cr = string.Format("costCenterID={0}", factory.costCenterID);
                dspWriter.logDeletedData(AID, this.DBName + ".dbo.Factory", cr);
                dspWriter.UpdateSingleTableRecord(this.DBName, factory, new string[] { "__AID" }, new object[] { AID });
            }
        }

        // BDE exposed
        public void deleteFactory(int AID, int costCenterID)
        {
            if (((int)dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.ProductionBatchHeader where factoryID={1}", this.DBName, costCenterID))) > 0)
                throw new ServerUserMessage("This factory can't be deleted because there are production batches in the database for this factory.");

            string cr = string.Format("costCenterID={0}", costCenterID);
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.Factory", cr);
            dspWriter.DeleteSingleTableRecrod<Factory>(this.DBName, costCenterID);
        }
        // BDE exposed
        public BatchType getBatchType(int id)
        {
            return base.getSingleObjectIntID<BatchType>(id);
        }
        // BDE exposed
        public BatchType[] getAllBatchTypes()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<BatchType>(null);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public int createBatchType(int AID, BatchType type)
        {
            return base.createSingleOjectIntID<BatchType>(AID, type);
        }
        // BDE exposed
        public void updateBatchType(int AID, BatchType type)
        {
            base.updateSingleOjectIntID<BatchType>(AID, type);
        }
        // BDE exposed
        public void deleteBatchType(int AID, int id)
        {
            if (((int)dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.ProductionBatchHeader where batchTypeID={1}", this.DBName, id))) > 0)
                throw new ServerUserMessage("This batch type can't be deleted because there are production batches in the datbase under this type.");
            base.deleteSingleOjectIntID<BatchType>(AID, id);
        }

        // BDE exposed
        public QualityCheckParameter getQualityCheckParameter(int id)
        {
            return base.getSingleObjectIntID<QualityCheckParameter>(id);
        }
        // BDE exposed
        public QualityCheckParameter[] getAllQualityCheckParameters()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<QualityCheckParameter>(null);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        void assertQualityCheckParameters(QualityCheckParameter par)
        {
            if (par.validMin > par.validMax)
                throw new ServerUserMessage("Valid maximum value can't be less minimum value");
            if (par.standardValue < par.validMin || par.standardValue > par.validMax)
                throw new ServerUserMessage("Standard value is not within valid range");
            if (par.standardValue + par.lowerTol < par.validMin || par.standardValue + par.lowerTol > par.validMax)
                throw new ServerUserMessage("Lower bound tolerance is not within valid range");
            if (par.standardValue + par.upperTol < par.validMin || par.standardValue + par.upperTol > par.validMax)
                throw new ServerUserMessage("Lower bound tolerance is not within valid range");

        }
        // BDE exposed
        public int createQualityCheckParameter(int AID, QualityCheckParameter type)
        {
            assertQualityCheckParameters(type);
            return base.createSingleOjectIntID<QualityCheckParameter>(AID, type);
        }
        // BDE exposed
        public void updateQualityCheckParameter(int AID, QualityCheckParameter type)
        {
            assertQualityCheckParameters(type);
            base.updateSingleOjectIntID<QualityCheckParameter>(AID, type);
        }


        // BDE exposed
        public void deleteQualityCheckParameter(int AID, int id)
        {
            if (((int)dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.ProductionBatchQCData where parameterID={1}", this.DBName, id))) > 0)
                throw new ServerUserMessage("This quality check parameter can't be deleted because there are production batches in the datbase that uses this parameter.");
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.QualityCheckCategoryStandard", "parameterID=" + id);
            dspWriter.Delete(this.DBName, "QualityCheckCategoryStandard", "parameterID=" + id);
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.QualityCheckItemStandard", "parameterID=" + id);
            dspWriter.Delete(this.DBName, "QualityCheckItemStandard", "parameterID=" + id);
            base.deleteSingleOjectIntID<QualityCheckParameter>(AID, id);
        }
        // BDE exposed
        public QualityCheckCategoryStandard[] getCategoryStandards(int parmeterID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<QualityCheckCategoryStandard>("parameterID=" + parmeterID);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public QualityCheckCategoryStandard getQualityCheckCategoryStandard(int parameterID, int categoryID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                QualityCheckCategoryStandard[] ret = reader.GetSTRArrayByFilter<QualityCheckCategoryStandard>(string.Format("parameterID={0} and categoryID={1}", parameterID, categoryID));
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public void setQualityCheckCategoryStandard(int AID, QualityCheckCategoryStandard standard)
        {
            if (berpBDE.GetItemCategory(standard.categoryID) == null)
                throw new ServerUserMessage("Invalid category ID {0}", standard.categoryID);
            assertQualityCheckParameters(standard.toQualityCheckParameter());
            QualityCheckCategoryStandard old = getQualityCheckCategoryStandard(standard.parameterID, standard.categoryID);
            if (old == null)
                dspWriter.InsertSingleTableRecord(this.DBName, standard, new string[] { "__AID" }, new object[] { AID });
            else
            {
                string cr = string.Format("parameterID={0} and categoryID={1}", standard.parameterID, standard.categoryID);
                dspWriter.logDeletedData(AID, this.DBName + ".dbo.QualityCheckCategoryStandard", cr);
                dspWriter.UpdateSingleTableRecord(this.DBName, standard, new string[] { "__AID" }, new object[] { AID });
            }
        }
        // BDE exposed
        public void deleteQualityCheckCategoryStandard(int AID, int parameterID, int categoryID)
        {
            string cr = string.Format("parameterID={0} and categoryID={1}", parameterID, categoryID);
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.QualityCheckCategoryStandard", cr);
            dspWriter.DeleteSingleTableRecrod<QualityCheckCategoryStandard>(this.DBName, parameterID, categoryID);
        }


        // BDE exposed
        public QualityCheckItemStandard[] getItemStandards(int parmeterID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<QualityCheckItemStandard>("parameterID=" + parmeterID);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public QualityCheckItemStandard getQualityCheckItemStandard(int parameterID, string itemCode)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                QualityCheckItemStandard[] ret = reader.GetSTRArrayByFilter<QualityCheckItemStandard>(string.Format("parameterID={0} and itemCode='{1}'", parameterID, itemCode));
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public void setQualityCheckItemStandard(int AID, QualityCheckItemStandard standard)
        {
            if (berpBDE.GetTransactionItems(standard.itemCode) == null)
                throw new ServerUserMessage("Invalid item code {0}", standard.itemCode);
            assertQualityCheckParameters(standard.toQualityCheckParameter());
            QualityCheckItemStandard old = getQualityCheckItemStandard(standard.parameterID, standard.itemCode);
            if (old == null)
                dspWriter.InsertSingleTableRecord(this.DBName, standard, new string[] { "__AID" }, new object[] { AID });
            else
            {
                string cr = string.Format("parameterID={0} and itemCode='{1}'", standard.parameterID, standard.itemCode);
                dspWriter.logDeletedData(AID, this.DBName + ".dbo.QualityCheckItemStandard", cr);
                dspWriter.UpdateSingleTableRecord(this.DBName, standard, new string[] { "__AID" }, new object[] { AID });
            }
        }
        // BDE exposed
        public void deleteQualityCheckItemStandard(int AID, int parameterID, string itemCode)
        {
            string cr = string.Format("parameterID={0} and itemCode='{1}'", parameterID, itemCode);
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.QualityCheckItemStandard", cr);
            dspWriter.DeleteSingleTableRecrod<QualityCheckItemStandard>(this.DBName, parameterID, itemCode);
        }
        // BDE exposed
        public QualityCheckParameter getDynamicStandardForItem(int parameterID, string itemCode)
        {
            Object scope;
            return getDynamicStandardForItem(parameterID, itemCode, out scope);
        }
        // BDE exposed
        public QualityCheckParameter getDynamicStandardForItem(int parameterID, string itemCode, out Object scopeObject)
        {
            QualityCheckParameter ret = getQualityCheckParameter(parameterID);
            if (ret == null)
            {
                scopeObject = null;
                return null;
            }

            QualityCheckItemStandard itemStandard = getQualityCheckItemStandard(parameterID, itemCode);
            if (itemStandard != null)
            {
                ret.standardValue = itemStandard.standardValue;
                ret.lowerTol = itemStandard.lowerTol;
                ret.upperTol = itemStandard.upperTol;
                ret.weight = itemStandard.weight;
                ret.validMin = itemStandard.validMin;
                ret.validMax = itemStandard.validMax;

                scopeObject = itemStandard;
                return ret;
            }
            int categoryID = berpBDE.GetTransactionItems(itemCode).categoryID;
            QualityCheckCategoryStandard catStandard = null;
            while (categoryID != -1 && (catStandard = getQualityCheckCategoryStandard(parameterID, categoryID)) == null)
            {
                categoryID = berpBDE.GetItemCategory(categoryID).PID;
            }
            if (catStandard != null)
            {
                ret.standardValue = catStandard.standardValue;
                ret.lowerTol = catStandard.lowerTol;
                ret.upperTol = catStandard.upperTol;
                ret.weight = catStandard.weight;
                ret.validMin = catStandard.validMin;
                ret.validMax = catStandard.validMax;
                scopeObject = catStandard;
                return ret;
            }
            scopeObject = ret;
            return ret;
        }
        // BDE exposed
        public int createProcessType(int AID, ProcessType type, ProcessMachine[] machines)
        {
            type.id = AutoIncrement.GetKey("bERPMan.ProcessTypeID");
            dspWriter.InsertSingleTableRecord(this.DBName, type, new string[] { "__AID" }, new object[] { AID });
            foreach (ProcessMachine m in machines)
            {
                assertMachineCostCenter(m);
                m.processTypeID = type.id;
                dspWriter.InsertSingleTableRecord(this.DBName, m, new string[] { "__AID" }, new object[] { AID });
            }
            return type.id;
        }


        // BDE exposed
        public void upateProcessType(int AID, ProcessType type, ProcessMachine[] machines)
        {
            ProcessType oldType = getProcessType(type.id);
            if (oldType == null)
                throw new ServerUserMessage("Invalid process type ID {0}.", type.id);
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.ProcessType", "id=" + type.id);
            dspWriter.UpdateSingleTableRecord(this.DBName, type, new string[] { "__AID" }, new object[] { AID });
            ProcessMachine[] oldMachines = getProcessMachines(type.id);
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.ProcessMachine", "processTypeID=" + type.id);
            dspWriter.Delete(this.DBName, "ProcessMachine", "processTypeID=" + type.id);
            foreach (ProcessMachine m in machines)
            {
                assertMachineCostCenter(m);
                m.processTypeID = type.id;
                dspWriter.InsertSingleTableRecord(this.DBName, m, new string[] { "__AID" }, new object[] { AID });
            }
        }

        // BDE exposed
        public void deleteProcessType(int AID, int processTypeID)
        {
            ProcessType oldType = getProcessType(processTypeID);
            if (oldType == null)
                throw new ServerUserMessage("Invalid process type ID {0}.", processTypeID);
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.ProcessType", "id=" + processTypeID);
            dspWriter.Delete(this.DBName, "ProcessMachine", "processTypeID=" + processTypeID);
            dspWriter.DeleteSingleTableRecrod<ProcessType>(this.DBName, processTypeID);
        }
        // BDE exposed
        public CostingSetting[] getAllCostingSettings()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<CostingSetting>(null);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public CostingSetting getCostSetting(string tag)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                CostingSetting[] ret = reader.GetSTRArrayByFilter<CostingSetting>("tag='" + tag + "'");
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public void setCostSetting(int AID, CostingSetting setting)
        {
           
            CostingSetting old = getCostSetting(setting.tag);
            if (old == null)
                dspWriter.InsertSingleTableRecord(this.DBName, setting, new string[] { "__AID" }, new object[] { AID });
            else
            {
                dspWriter.logDeletedData(AID,this.DBName+".dbo.CostingSetting","tag='"+setting.tag+"'");
                dspWriter.UpdateSingleTableRecord(this.DBName,setting,new string[] { "__AID" }, new object[] { AID });
            }
        }
        // BDE exposed
        public void deleteCostSetting(int AID, string tag)
        {
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.CostingSetting", "tag='" + tag+ "'");
            dspWriter.DeleteSingleTableRecrod<CostingSetting>(tag);

        }

        public void saveWorkData(int AID, Data_Absence[] absenceData, Data_OverTime[] overTimeData)
        {
            lock(dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    if (absenceData != null && absenceData.Length > 0)
                    {
                        foreach (Data_Absence absence in absenceData)
                        {
                            if (absence.isNewData)
                            {
                                if (dspWriter.GetSTRArrayByFilter<Data_Absence>("date='" + absence.date + "' and employeeID=" + absence.employeeID).Length > 0)
                                    throw new ServerUserMessage("Absence data already exists for employee id=" + berpBDE.Payroll.GetEmployee(absence.employeeID).employeeID + " on " + absence.date.ToString("dd/MM/yyyy"));
                                dspWriter.InsertSingleTableRecord<Data_Absence>(this.DBName, absence, new string[] { "__AID" }, new object[] { AID });
                            }
                            else
                            {
                                dspWriter.logDeletedData(AID, string.Format("{0}.dbo.Data_Absence", this.DBName), "date='" + absence.date + "' and employeeID=" + absence.employeeID);
                                dspWriter.UpdateSingleTableRecord<Data_Absence>(this.DBName, absence);
                            }
                        }
                    }
                    if (overTimeData != null && overTimeData.Length > 0)
                    {
                        foreach (Data_OverTime overtime in overTimeData)
                        {
                            if (overtime.fromTime > overtime.toTime)
                                throw new ServerUserMessage("Over time beginning time cannot be greater than ending time for employee id=" + berpBDE.Payroll.GetEmployee(overtime.employeeID).employeeID);
                            if (overtime.fromTime == overtime.toTime)
                                throw new ServerUserMessage("Over time beginning time and ending time cannot be the same for employee id=" + berpBDE.Payroll.GetEmployee(overtime.employeeID).employeeID);
                            if(overtime.isNewData)
                            {
                                if (dspWriter.GetSTRArrayByFilter<Data_OverTime>("date='" + overtime.date + "' and employeeID=" + overtime.employeeID).Length > 0)
                                    throw new ServerUserMessage("Absence data already exists for employee id=" + berpBDE.Payroll.GetEmployee(overtime.employeeID).employeeID + " on " + overtime.date.ToString("dd/MM/yyyy"));
                                dspWriter.InsertSingleTableRecord<Data_OverTime>(this.DBName, overtime, new string[] { "__AID" }, new object[] { AID });
                            }
                            else
                            {
                                dspWriter.logDeletedData(AID, string.Format("{0}.dbo.Data_OverTime", this.DBName), "date='" + overtime.date + "' and employeeID=" + overtime.employeeID + " and fromTime=" + overtime.fromTime);
                                dspWriter.UpdateSingleTableRecord<Data_OverTime>(this.DBName, overtime, new string[] { "__AID" }, new object[] { AID });
                            }
                        }
                    }
                    
                    dspWriter.CommitTransaction();

                }
                catch(Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }

        public Data_Absence[] getAbsenceWorkData(DateTime date)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                return helper.GetSTRArrayByFilter<Data_Absence>("date='" + date + "'");
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public OverTimeRateConfiguration[] getOverTimeConfiguration(DateTime date)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                return helper.GetSTRArrayByFilter<OverTimeRateConfiguration>("fromTime>={0} and fromTime<{1}".format(date.Ticks,date.AddDays(1).Ticks));
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public Data_OverTime[] getOverTimeWorkData(DateTime date)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                return helper.GetSTRArrayByFilter<Data_OverTime>("date='" + date + "'");
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public WorkDataResult[] getWorkData(int month, int year)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                string sqlOverTime = string.Format(@"SELECT [date],
       sum(datediff(hh,Accounting_2006.dbo.ticksToDate(fromTime),Accounting_2006.dbo.ticksToDate(toTime))) as otHours
  FROM [bERPMan].[dbo].[Data_OverTime]
  where datepart(mm,[date])={0} and datepart(yyyy,[date])={1}
  group by [date]", month, year);

                string sqlAbsence = string.Format(@"SELECT [date],
      sum([absenceHours]) as absenceHours
  FROM [bERPMan].[dbo].[Data_Absence] where datepart(mm,[date])={0} and datepart(yyyy,[date])={1}
  group by [date]", month, year);

                List<WorkDataResult> result = new List<WorkDataResult>();
                DataTable dtOverTime = helper.GetDataTable(sqlOverTime);
                DataTable dtAbsence = helper.GetDataTable(sqlAbsence);
                foreach (DataRow row in dtOverTime.Rows)
                {
                    WorkDataResult r = new WorkDataResult();
                    r.date = (DateTime)row[0];
                    r.overTimeNote = row[1].ToString() + " hours";
                    result.Add(r);
                }
                foreach (DataRow row in dtAbsence.Rows)
                {
                    WorkDataResult existing = null;
                    foreach (WorkDataResult wr in result)
                    {
                        if (wr.date == (DateTime)row[0])
                        {
                            existing = wr;
                            break;
                        }
                    }
                    WorkDataResult r = existing == null ? new WorkDataResult() : existing;
                    r.date = (DateTime)row[0];
                    r.absenceNote = row[1].ToString() + " hours";
                    if (existing == null)
                        result.Add(r);
                }
                return result.ToArray();
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }

        internal string getInventoryTable(int storeID,DateTime date)
        {
            if(storeID==-1)
                return berpBDE.renderInventoryTable(
                    new InventoryGeneratorParameters()
                    {
                        costCenterID = 1,
                        to = date,
                        includeTitle = false,
                        seprateOrder = false,
                        showInventoryRunTime=true,
                    }
                    );
            
            return berpBDE.renderInventoryTable(
                new InventoryGeneratorParameters()
                {
                    costCenterID=storeID,
                    to=date,
                    includeTitle=false,
                    seprateOrder=false,
                }
                );
        }
        
        internal ProductionBatchHeader getProductionBatchHeader(string batchNo)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                ProductionBatchHeader[] ret = reader.GetSTRArrayByFilter<ProductionBatchHeader>(string.Format("batchNo='{0}' ", batchNo));
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        internal string getBelowAboveReorderTable(DateTime date)
        {
            return "";
        }

        internal string getBelowNormalLevelStock(DateTime date)
        {
            return berpBDE.renderInventoryTable(
                new InventoryGeneratorParameters()
                {
                    costCenterID = 1,
                    to = date,
                    includeTitle = false,
                    seprateOrder = false,
                    showBelowMinStock=true,
                    filterAbnormalStockLevel=true,
                }
                );
        }

        internal string getAboveNormalLevelStock(DateTime date)
        {
            return berpBDE.renderInventoryTable(
               new InventoryGeneratorParameters()
               {
                   costCenterID = 1,
                   to = date,
                   includeTitle = false,
                   seprateOrder = false,
                   showAboveMaxStock = true,
                   filterAbnormalStockLevel = true,
               }
               );
        }
    }
}
