﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.ClientServer;
using System.Data;
using System.Xml.Serialization;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Reflection;
using INTAPS.Accounting.BDE;
using INTAPS.Accounting;
using BIZNET.iERP;

namespace BIZNET.iERPMan.Server
{

    public partial class iERPManBDE : BDEBase
    {
        bool isPeriodOpen(int level,long t)
        {
            CostingPeriod p = getCostingPeriod(level, t);
            return p != null && !p.closed;
        }
        CostingPeriod getCostingPeriodByLowerBound(INTAPS.RDBMS.SQLHelper helper, int level,long t1)
        {
            helper.setReadDB(this.DBName);
            try
            {
                CostingPeriod[] p = helper.GetSTRArrayByFilter<CostingPeriod>(string.Format("level={0} and t1={1}", level, t1));
                if (p.Length == 0)
                    return null;
                return p[0];
            }
            finally
            {
                helper.restoreReadDB();
            }
        }
        // BDE exposed
        public CostingPeriod getCostingPeriod(int level, long t)
        {
            CostingPeriod c = caculateCostingPeriond(level, t);
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return getCostingPeriodByLowerBound(reader, level, c.t1);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public CostingPeriod caculateCostingPeriond(int level, long t)
        {
            CostingPeriod ret=new CostingPeriod();
            DateTime date=new DateTime(t).Date;
            ret.closed = false;
            ret.level = level;
            ret.name = date.ToString("MMM dd,yyyy");
            ret.t1 = date.Ticks;
            ret.t2 = date.AddDays(1).Ticks;
            return ret;
        }
        // BDE exposed
        public CostingPeriod getLastCostingPeriod(int level, long before)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                string sql;
                if (before == -1)
                    sql = string.Format("Select max(t1) from {0}.dbo.CostingPeriod where level={1}", this.DBName,level);
                else
                    sql = string.Format("Select max(t1) from {0}.dbo.CostingPeriod where level={1} and t1<{2}",this.DBName, level, before);
                object max = reader.ExecuteScalar(sql);
                if (max is long)
                    return getCostingPeriodByLowerBound(reader, level, (long)max);
                return null;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public CostingPeriod[] getOpenCostingPeriods(int level, long from, long to)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                string cr=string.Format("level={0} and closed=0", level);;
                if (from != -1)
                    cr = INTAPS.RDBMS.SQLHelper.AppendOperand(cr, " AND ", "level={0} and t1>=" + from);
                if (to != -1)
                    cr = INTAPS.RDBMS.SQLHelper.AppendOperand(cr, " AND ", "level={0} and t1<" + to);
                return reader.GetSTRArrayByFilter<CostingPeriod>(cr);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public CostingPeriod[] getClosedCostingPeriods(int level, long from, long to)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                string cr = string.Format("level={0} and closed=1", level); ;
                if (from != -1)
                    cr = INTAPS.RDBMS.SQLHelper.AppendOperand(cr, " AND ", "level={0} and t1>=" + from);
                if (to != -1)
                    cr = INTAPS.RDBMS.SQLHelper.AppendOperand(cr, " AND ", "level={0} and t1<" + to);
                return reader.GetSTRArrayByFilter<CostingPeriod>(cr);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public CostingPeriod[] getCostingPeriods(int level, long from, long to, int index, int pageSize, out int N)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                string cr = string.Format("level={0}", level); ;
                if (from != -1)
                    cr = INTAPS.RDBMS.SQLHelper.AppendOperand(cr, " AND ", "level={0} and t1>=" + from);
                if (to != -1)
                    cr = INTAPS.RDBMS.SQLHelper.AppendOperand(cr, " AND ", "level={0} and t1<" + to);
                return reader.GetSTRArrayByFilter<CostingPeriod>(cr,index,pageSize,out N);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        void updateCostingPeriodInternal(int AID,CostingPeriod c)
        {
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.CostingPeriod", string.Format("level={0} and t1={1}", c.level, c.t1));
            dspWriter.UpdateSingleTableRecord(this.DBName, c, new string[] { "__AID" }, new object[] { AID });
        }
        // BDE exposed
        public CostingPeriod openCostingPeriod(int AID, int level, long time)
        {
            CostingPeriod c=caculateCostingPeriond(level,time);
            CostingPeriod last = getLastCostingPeriod(level,-1);
            
            if(last==null)
            {
                dspWriter.InsertSingleTableRecord(this.DBName, c, new string[] { "__AID" }, new object[] { AID });
                return c;
            }
            CostingPeriod old=getCostingPeriodByLowerBound(dspWriter,level,c.t1);
            if(old==null)
            {
                dspWriter.InsertSingleTableRecord(this.DBName, c, new string[] { "__AID" }, new object[] { AID });
                return c;
            }
            if(old.closed)
            {
                clearEstimationForPeriod(AID,old);
                updateCostingPeriodInternal(AID, c);
                return c;
            }
            return c;
        }
        // BDE exposed
        public void closeCostingPeriod(int AID, int level, long time)
        {
            dspWriter.setReadDB(this.DBName);
            try
            {
                CostingPeriod c = caculateCostingPeriond(level, time);
                CostingPeriod old = getCostingPeriodByLowerBound(dspWriter, level, c.t1);
                if (old == null)
                {
                    throw new ServerUserMessage("Costing period {0} doesn't exist.", c.name);
                }
                if (old.closed)
                    throw new ServerUserMessage("Costing period {0} is already closed.", c.name);
                c.closed = true;
                updateCostingPeriodInternal(AID, c);
                calculateAndSaveEstimationForPeriod(AID, c);
            }
            finally
            {
                dspWriter.restoreReadDB();
            }
        }
    }
}
