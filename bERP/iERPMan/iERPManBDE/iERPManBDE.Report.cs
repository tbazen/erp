﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.ClientServer;
using System.Data;
using System.Xml.Serialization;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Reflection;
using INTAPS.Accounting.BDE;
using INTAPS.Accounting;
using BIZNET.iERP;
using INTAPS.Payroll;
using INTAPS;
using INTAPS.ClientServer.RemottingServer;
using RazorEngine;
using RazorEngine.Templating;

namespace BIZNET.iERPMan.Server
{
    public partial class iERPManBDE : BDEBase
    {
        public string getSingleBatchReport(int batchID, long ticksFrom, long ticksTo)
        {
            ProductionBatch batch = getProductionBatch(batchID);
            return "";
        }
        public TransactionDocumentItem[] estimateMaterialsForProcess(ProductionBatch batch, int processID)
        {
            ReportDefination def = accountingBDE.GetReportDefination("iERPMan.ItemEstimator");
            if (def == null)
                return new TransactionDocumentItem[0];
            INTAPS.Evaluator.EData ret = accountingBDE.EvaluateEHTML(def.reportTypeID, def.formula, new object[] { "batch", new INTAPS.Evaluator.EData(batch), "pid", new INTAPS.Evaluator.EData(processID) }, "Items");
            if (ret.Type != INTAPS.Evaluator.DataType.ListData)
                throw new ServerUserMessage("Estimator returned unexpected data. Returned data {0}", ret);

            INTAPS.Evaluator.ListData list = ret.Value as INTAPS.Evaluator.ListData;
            TransactionDocumentItem[] items = new TransactionDocumentItem[list.elements.Length];
            int i = 0;
            foreach (INTAPS.Evaluator.EData row in list.elements)
            {
                if (ret.Type != INTAPS.Evaluator.DataType.ListData)
                    throw new ServerUserMessage("Estimator returned unexpected data at row {1}. Returned data {0}", row, i);
                INTAPS.Evaluator.ListData rowList = row.Value as INTAPS.Evaluator.ListData;
                if (rowList.elements.Length < 2)
                    throw new ServerUserMessage("Estimator returned too few elements at row {0}.", i);
                INTAPS.Evaluator.EData el = rowList.elements[0];
                if (el.Type == INTAPS.Evaluator.DataType.Empty)
                    throw new ServerUserMessage("Estimator returned empty first element for row {0}.", i);
                TransactionItems item = berpBDE.GetTransactionItems(el.Value.ToString());
                if (item == null)
                    throw new ServerUserMessage("Estimator returned invalid item code {1} for row {0}.", i, el.Value.ToString());

                el = rowList.elements[1];
                if (el.Type != INTAPS.Evaluator.DataType.Int && el.Type != INTAPS.Evaluator.DataType.Float)
                    throw new ServerUserMessage("Estimator returned none numberic second element for row {0}.", i);
                double q = Convert.ToDouble(el.Value);
                if (q < 0)
                    throw new ServerUserMessage("Estimator returned negative quantity {1} for row {0}.", i, q);
                items[i] = new TransactionDocumentItem() { code = item.Code, quantity = q };
                i++;
            }
            return items;
        }
        INTAPS.CachedObject<int, ProcessType> getCachedObjectProcessTypes()
        {
            return new INTAPS.CachedObject<int, ProcessType>(
                new INTAPS.RetrieveObjectDelegate<int, ProcessType>(delegate(int id)
                    {
                        return getProcessType(id);
                    })
                );
        }
        public Dictionary<int, Dictionary<string, CostItem>> estimateOtherCosts(ProductionBatch batch)
        {
            return estimateOtherCosts(getCachedObjectProcessTypes(), batch);
        }
        Dictionary<int, Dictionary<string, CostItem>> estimateOtherCosts(INTAPS.CachedObject<int, ProcessType> processTypes, ProductionBatch batch)
        {
            if (m_sysPars.otherCostReportingScheme == null)
                throw new ServerUserMessage("Configure other material cost reporting scheme.");
            Dictionary<int, Dictionary<string, CostItem>> ret = new Dictionary<int, Dictionary<string, CostItem>>();
            ReportDefination def = accountingBDE.GetReportDefination("iERPMan.CostEstimator");
            if (def == null)
                return ret;
            EHTML f = accountingBDE.getEHTMLEvaluator(def.reportTypeID, def.formula, new object[] { "batch", new INTAPS.Evaluator.EData(batch) });

            foreach (ProductionBatchProcessData proc in batch.processData)
            {
                string pvar = "p" + processTypes[proc.processID].tag;
                if (!f.SymbolDefined(pvar))
                    continue;
                f.setValue("hour", new INTAPS.Evaluator.EData(proc.hours));
                INTAPS.Evaluator.EData processCostData = f.GetData(pvar);
                if (processCostData.Type != INTAPS.Evaluator.DataType.ListData)
                    throw new ServerUserMessage("Estimator returned unexpected data. Returned data {0}", processCostData);

                INTAPS.Evaluator.ListData list = processCostData.Value as INTAPS.Evaluator.ListData;
                Dictionary<string, CostItem> costs = new Dictionary<string, CostItem>();
                int i = 0;
                foreach (INTAPS.Evaluator.EData row in list.elements)
                {
                    if (processCostData.Type != INTAPS.Evaluator.DataType.ListData)
                        throw new ServerUserMessage("Estimator returned unexpected data at row {1}. Returned data {0}", row, i);
                    INTAPS.Evaluator.ListData rowList = row.Value as INTAPS.Evaluator.ListData;
                    if (rowList.elements.Length < 2)
                        throw new ServerUserMessage("Estimator returned too few elements at row {0}.", i);
                    INTAPS.Evaluator.EData el = rowList.elements[0];
                    if (el.Type == INTAPS.Evaluator.DataType.Empty)
                        throw new ServerUserMessage("Estimator returned empty first element for row {0}.", i);
                    string costTag = el.Value.ToString();

                    el = rowList.elements[1];
                    if (el.Type != INTAPS.Evaluator.DataType.Int && el.Type != INTAPS.Evaluator.DataType.Float)
                        throw new ServerUserMessage("Estimator returned none numberic second element for row {0}.", i);
                    costs.Add(costTag, new CostItem() { tag = costTag, amount = Convert.ToDouble(el.Value) });
                    i++;
                }
                if (costs.Count > 0)
                    ret.Add(proc.processID, costs);
            }
            return ret;
        }
        Dictionary<int, SummaryValueSection> estimateOtherCosts(INTAPS.CachedObject<int, ProcessType> processTypes, Dictionary<int, Dictionary<string, CostItem>> systemCalculated, ProductionBatch batch)
        {
            Dictionary<int, SummaryValueSection> ret = new Dictionary<int, SummaryValueSection>();
            Dictionary<int, Dictionary<string, CostItem>> processCosts = estimateOtherCosts(processTypes, batch);
            foreach (ProductionBatchProcessData p in batch.processData)
            {
                Dictionary<string, CostItem> sys = systemCalculated.ContainsKey(p.processID) ? systemCalculated[p.processID] : new Dictionary<string, CostItem>();
                Dictionary<string, CostItem> costs = processCosts.ContainsKey(p.processID) ? processCosts[p.processID] : new Dictionary<string, CostItem>();
                SummaryValueSection sum = berpBDE.evaluateSummaryTableByItem(m_sysPars.otherCostReportingScheme,
                    new BIZNET.iERP.Server.iERPTransactionBDE.EvaluteSummeryItemByItemDelegate(delegate(SummaryItemInfo item)
                        {
                            if (costs.ContainsKey(item.tag))
                                return new AccountBalance(0, 0, costs[item.tag].amount);
                            if (sys.ContainsKey(item.tag))
                                return new AccountBalance(0, 0, sys[item.tag].amount);
                            return new AccountBalance();
                        }
                    ));
                ret.Add(p.processID, sum);
            }
            return ret;
        }
        // BDE exposed
        public SummaryInformation getCostReportingScheme()
        {
            if (m_sysPars.materialCostReportingScheme == null)
                throw new ServerUserMessage("Configure material cost reporting scheme.");
            SummaryInformation matScheme = m_sysPars.materialCostReportingScheme;
            if (m_sysPars.otherCostReportingScheme == null)
                throw new ServerUserMessage("Configure other cost reporting scheme.");
            SummaryInformation ret = new SummaryInformation();
            ret.merge(matScheme);
            ret.merge(m_sysPars.otherCostReportingScheme);
            return ret;
        }
        internal SummaryInformation getMaterialReportingScheme()
        {
            if (m_sysPars.materialCostReportingScheme == null)
                throw new ServerUserMessage("Configure material cost reporting scheme.");
            SummaryInformation ret = m_sysPars.materialCostReportingScheme;
            return ret;
        }
        internal SummaryInformation getOtherCostReportingScheme()
        {
            if (m_sysPars.otherCostReportingScheme == null)
                throw new ServerUserMessage("Configure other cost reporting scheme.");
            return m_sysPars.otherCostReportingScheme;
        }

        Dictionary<int, SummaryValueSection> calculateMaterialCosts(ProductionBatch batch)
        {
            if (m_sysPars.materialCostReportingScheme == null)
                throw new ServerUserMessage("Configure material cost reporting scheme.");
            Dictionary<string, double> unitCostCache = new Dictionary<string, double>();
            Dictionary<string, TransactionItems> cashedItems = new Dictionary<string, TransactionItems>();
            Dictionary<int, SummaryValueSection> ret = new Dictionary<int, SummaryValueSection>();
            foreach (ProductionBatchProcessData d in batch.processData)
            {

                SummaryValueSection val = berpBDE.evaluateSummaryTable(m_sysPars.materialCostReportingScheme, 1, DateTime.Now
                    , DateTime.Now, false
                    , new iERP.Server.iERPTransactionBDE.EvaluteSummeryItemDelegate(
                        delegate(int c, int a, int i, DateTime t1, DateTime t2, bool f)
                        {
                            AccountBalance costBalance = new AccountBalance(a, i);
                            foreach (TransactionDocumentItem ditem in d.items)
                            {
                                TransactionItems titem;
                                if (cashedItems.ContainsKey(ditem.code))
                                    titem = cashedItems[ditem.code];
                                else
                                    cashedItems.Add(ditem.code, titem = berpBDE.GetTransactionItems(ditem.code));
                                if (accountingBDE.IsControlOf<Account>(a, titem.expenseAccountID) || a == titem.expenseAccountID)
                                {
                                    double unitCost;
                                    if (unitCostCache.ContainsKey(titem.Code))
                                    {
                                        unitCost = unitCostCache[titem.Code];
                                    }
                                    else
                                    {
                                        unitCost = 0;
                                        int csAccountID = accountingBDE.GetCostCenterAccountID(d.storeCostCenterID, titem.inventoryAccountID);
                                        if (csAccountID == -1)
                                            unitCost = 0;
                                        else
                                        {
                                            double price = accountingBDE.GetNetBalanceAsOf(csAccountID, 0, new DateTime(d.startTicks));
                                            double q = accountingBDE.GetNetBalanceAsOf(csAccountID, 1, new DateTime(d.startTicks));
                                            if (AccountBase.AmountEqual(q, 0))
                                                unitCost = 0;
                                            else
                                                unitCost = price / q;
                                        }
                                        unitCostCache.Add(ditem.code, unitCost);
                                    }
                                    costBalance.AddBalance(new AccountBalance(i, 0, unitCost * ditem.quantity));
                                }
                            }

                            return costBalance;
                        }
                        )
                    );
                ret.Add(d.processID, val);
            }
            return ret;
        }
        void getEmployeeRateByCostCenter(Dictionary<int, Employee> usedEmployees, int costCenterID, long ticks)
        {
            foreach (Employee e in berpBDE.Payroll.getEmployessByCostCenter(costCenterID, ticks, true))
            {
                if (!usedEmployees.ContainsKey(e.id))
                    usedEmployees.Add(e.id, e);
            }
        }
        void getEmployeeRateForProcessType(Dictionary<int, Employee> usedEmployees, int processTypeID, long ticks)
        {
            foreach (ProcessMachine m in getProcessMachines(processTypeID))
            {
                getEmployeeRateByCostCenter(usedEmployees, m.costCenterID, ticks);
            }
        }
        double getOverlapHours(DateTime t11,DateTime t12,DateTime t21, DateTime t22)
        {
            DateTime t1 = t11 > t12 ? t11 : t12;
            DateTime t2 = t21 < t22 ? t21 : t22;
            double ret=t2.Subtract(t1).TotalHours;
            if (ret < 0)
                return 0;
            return ret;
        }
        Dictionary<int, Dictionary<int, double>> analyzePeriodEmployeeCost(CostingPeriod p, ProductionBatch[] batchs)
        {
            ProductionBatch ret = new ProductionBatch();

            Dictionary<ulong, Dictionary<int, double>> batchEmployeeHours = new Dictionary<ulong, Dictionary<int, double>>();
            Dictionary<int, double> employeeHours = new Dictionary<int, double>();
            Dictionary<int, Employee> allEmployees = new Dictionary<int, Employee>();
            OverTimeRateConfiguration[] overTimeConfig = getOverTimeConfiguration(new DateTime(p.t1).Date);
            Data_Absence[] absence=getAbsenceWorkData(new DateTime(p.t1).Date);
            foreach (ProductionBatch b in batchs)
            {
                foreach (ProductionBatchProcessData d in b.processData)
                {
                    Dictionary<int, Employee> usedEmployees = new Dictionary<int, Employee>();
                    getEmployeeRateForProcessType(usedEmployees, d.processID, d.startTicks);
                    foreach (Employee e in usedEmployees.Values)
                        if (!allEmployees.ContainsKey(e.id))
                            allEmployees.Add(e.id, e);
                    
                    DateTime timeProcessStart=new DateTime(d.startTicks);
                    DateTime timeProcessEnd=new DateTime(d.endTicks);
                    double hours = timeProcessEnd.Subtract(timeProcessStart).TotalHours;
                    
                    /*foreach(OverTimeRateConfiguration ot in overTimeConfig)
                    {
                        double h = getOverlapHours(timeProcessStart, timeProcessEnd, new DateTime(ot.fromTime), new DateTime(ot.toTime));
                        if(h>0)
                        {
                            hours += (ot.rate - 1) * h;
                        }
                    }
                    foreach(Data_Absence ab in absence)
                    {
                        if(ab.employeeID==e.id)
                    }*/
                    if (!AccountBase.AmountGreater(hours, 0))
                        continue;
                    ulong bpid = (ulong)d.processID;
                    bpid = bpid | (ulong)((ulong)b.header.id << 32);
                    Dictionary<int, double> thisEmployeeHours = new Dictionary<int, double>();
                    foreach (int eid in usedEmployees.Keys)
                    {

                        if (thisEmployeeHours.ContainsKey(eid))
                            thisEmployeeHours[eid] += hours;
                        else
                            thisEmployeeHours.Add(eid, hours);

                        if (employeeHours.ContainsKey(eid))
                            employeeHours[eid] += hours;
                        else
                            employeeHours.Add(eid, hours);
                    }
                    batchEmployeeHours.Add(bpid, thisEmployeeHours);
                }
            }

            Dictionary<int, Dictionary<int, double>> batchProcessLaberCost = new Dictionary<int, Dictionary<int, double>>();
            double fullWorDay = berpBDE.Payroll.SysPars.EmployeeDailyFullWorkingHours;
            foreach (KeyValuePair<ulong, Dictionary<int, double>> kv in batchEmployeeHours)
            {
                double total = 0;
                foreach (KeyValuePair<int, double> employeeLabourHour in kv.Value)
                {
                    double totalEmployeeHours = employeeHours[employeeLabourHour.Key];
                    double ratePerHour = berpBDE.Payroll.getEmployeeSalaryPerHours(allEmployees[employeeLabourHour.Key]);
                    total += employeeLabourHour.Value * ratePerHour * fullWorDay / totalEmployeeHours;
                }
                int lProcessID = (int)(kv.Key & (ulong)(0xFFFFFFFF));
                int lBatchID = (int)(kv.Key >> 32);
                Dictionary<int, double> batchCost;
                if (batchProcessLaberCost.ContainsKey(lBatchID))
                    batchCost = batchProcessLaberCost[lBatchID];
                else
                    batchProcessLaberCost.Add(lBatchID, batchCost = new Dictionary<int, double>());
                batchCost.Add(lProcessID, total);
            }
            return batchProcessLaberCost;
        }
        void addCostItem(int AID, int batchID, int processID, string tag, double cost)
        {
            dspWriter.InsertSingleTableRecord(this.DBName, new BatchCostItem() { batchID = batchID, cost = cost, tag = tag, processID = processID }, new string[] { "__AID" }, new object[] { AID });
        }
        void addCostItems(int AID, int batchID, int processID, SummaryValueSection values)
        {
            foreach (SummaryValueItem c in values.childs)
            {
                if (c is SummaryValueSection)
                    addCostItems(AID, batchID, processID, (SummaryValueSection)c);
                else
                {
                    if (!AccountBase.AmountEqual(c.dbValue, c.crValue))
                        addCostItem(AID, batchID, processID, c.tag, c.dbValue - c.crValue);
                }
            }
        }
        void addCostItems(int AID, int batchID, Dictionary<int, SummaryValueSection> values)
        {
            foreach (KeyValuePair<int, SummaryValueSection> kv in values)
            {
                addCostItems(AID, batchID, kv.Key, kv.Value);
            }
        }
        void calculateAndSaveEstimationForPeriod(int AID, CostingPeriod p)
        {

            accountingBDE.PushProgress(string.Format("Calculating cost estimations for period {0}", p.name));
            try
            {
                CachedObject<int, ProcessType> processTypes = getCachedObjectProcessTypes();
                accountingBDE.SetProgress(0);
                int N;
                ProductionBatchHeader[] headers = searchBatch(new ProductionBatchSearchOption() { tickFrom = p.t1, tickTo = p.t2 }
                    , 0, -1, out N);
                ProductionBatch[] batches = new ProductionBatch[headers.Length];
                accountingBDE.SetProgress("Analyzing employee cost...");
                for (int i = 0; i < headers.Length; i++)
                    batches[i] = getProductionBatch(headers[i].id);

                Dictionary<int, Dictionary<int, double>> employeeCost = analyzePeriodEmployeeCost(p, batches);

                accountingBDE.SetProgress(0.3);
                double prog = 0;
                foreach (ProductionBatch b in batches)
                {
                    addCostItems(AID, b.header.id, calculateMaterialCosts(b));
                    Dictionary<int, Dictionary<string, CostItem>> empCosts = new Dictionary<int, Dictionary<string, CostItem>>();
                    if (employeeCost.ContainsKey(b.header.id))
                    {

                        foreach (KeyValuePair<int, double> kv in employeeCost[b.header.id])
                        {
                            Dictionary<string, CostItem> proccesEmployeeCost = new Dictionary<string, CostItem>();
                            proccesEmployeeCost.Add("sys_employee", new CostItem() { tag = "sys_employee", amount = kv.Value });
                            empCosts.Add(kv.Key, proccesEmployeeCost);
                        }
                    }
                    addCostItems(AID, b.header.id, estimateOtherCosts(processTypes, empCosts, b));
                    accountingBDE.SetProgress(0.3 + 0.7 * prog / (double)batches.Length);
                    prog++;
                }
            }
            finally
            {
                accountingBDE.PopProgress();
            }
        }
        public QualitySummary getQualityIndex(ProductionBatch batch)
        {
            QualitySummary ret = new QualitySummary();
            ret.parmeters = new QualityCheckParameter[batch.qcData.Length];
            ret.measuredValues = new double[batch.qcData.Length];
            double totalDev = -1;
            double totalWeight = 0;
            int i = 0;
            foreach (ProductionBatchQCData qc in batch.qcData)
            {
                QualityCheckParameter par = getDynamicStandardForItem(qc.parameterID, batch.header.itemCode);
                if (totalDev == -1)
                    totalDev = 0;
                else
                    totalDev += Math.Abs(par.standardValue - qc.measuredValue) * par.weight;
                totalWeight += par.weight;
                ret.parmeters[i] = par;
                ret.measuredValues[i] = qc.measuredValue;
                i++;
            }
            if (totalDev == -1 || totalWeight == 0)
                ret.qualityIndex = -1;
            else

                ret.qualityIndex = totalDev / totalWeight;
            return ret;
        }
        void clearEstimationForPeriod(int AID, CostingPeriod p)
        {
            string cr = string.Format("batchID in (Select id from {0}.dbo.ProductionBatchHeader where ticksCreated>={1} and ticksCreated<{2})", this.DBName, p.t1, p.t2);
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.BatchCostItem", cr);
            dspWriter.Delete(this.DBName, "BatchCostItem", cr);
        }
        // BDE exposed
        public BatchCostItem[] getBatchCostItems(int batchID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<BatchCostItem>("batchID=" + batchID);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        internal string getProductionTable(DateTime date1, DateTime date2)
        {
            //return getProductionTable(date1, date2, null,null);
            AdjustedCostHeader header = getActualCostHeader(date1.Ticks);
            if (header == null)
                return getProductionTable(date1, date2, null, null,true);
            //if (header.ticksTo != date2.Ticks)
            //    return getProductionTable(date1, date2, null, null,true);
            AdjustedCostHeader h = getActualCostHeader(date1.Ticks);
            AdjustedCostDetail[] d = getActualCostDetail(date1.Ticks);
            return getProductionTable(date1, date2, h, d, true);

        }

        internal class CreditSettlementRow
        {
            public string  SNo { get; set; }
            public string CustomerName { get; set; }
            public string Tin { get; set; }
            public string TotalSell { get; set; }
            public string CreditTaken { get; set; }
            public string CreditSettle { get; set; }
            public string OutstandingCredit { get; set; }
        }
        internal class CreditSettlementRowValue
        {
            public string SNo { get; set; }
            public string CustomerName { get; set; }
            public string Tin { get; set; }
            public double TotalSell { get; set; }
            public double CreditTaken { get; set; }
            public double CreditSettle { get; set; }
            public double OutstandingCredit { get; set; }
        }
        internal string getCreditSettlementTable(DateTime date1, DateTime date2)
        {
            int N;
            AccountDocument[] docs=berpBDE.Accounting.GetAccountDocuments(
                new DocumentSearchPar()
                {
                    type = -1,
                    includeType=new int[]{berpBDE.Accounting.GetDocumentTypeByType(typeof(Sell2Document)).id,
                        berpBDE.Accounting.GetDocumentTypeByType(typeof(CustomerCreditSettlement2Document)).id
                    },
                    date =true,
                    from = date1,
                    to=date2,
                    
                }
                , null, 0, -1, out N);
            Dictionary<string, CreditSettlementRowValue> rows = new Dictionary<string, CreditSettlementRowValue>();
            foreach (Sell2Document sell in docs)
            {
                if (!rows.ContainsKey(sell.relationCode))
                {
                    TradeRelation customer = berpBDE.GetRelation(sell.relationCode);
                    if (sell is CustomerCreditSettlement2Document)
                    {
                        CreditSettlementRowValue row = new CreditSettlementRowValue()
                        {
                            CustomerName = customer.Name,
                            Tin = customer.TIN,
                            CreditTaken = 0,
                            CreditSettle =((CustomerCreditSettlement2Document)sell).amount,
                        };
                        rows.Add(sell.relationCode, row);
                    }
                    else
                    {
                        CreditSettlementRowValue row = new CreditSettlementRowValue()
                        {
                            CustomerName = customer.Name,
                            Tin = customer.TIN,
                            CreditTaken = sell.supplierCredit,
                            CreditSettle = 0,
                            TotalSell = sell.totalBeforeTax,
                        };
                        rows.Add(sell.relationCode, row);
                    }

                }
                else
                {
                    if (sell is CustomerCreditSettlement2Document)
                    {
                        CreditSettlementRowValue row = (CreditSettlementRowValue)rows[sell.relationCode];
                        row.CreditSettle += ((CustomerCreditSettlement2Document)sell).amount;
                    }
                    else
                    {
                        CreditSettlementRowValue row = (CreditSettlementRowValue)rows[sell.relationCode];
                        row.CreditTaken += sell.supplierCredit;
                        row.TotalSell += sell.totalBeforeTax;
                    }
                }
            }
            List<object> creditsettlementsList = new List<object>();
            int n = 1;
            foreach (CreditSettlementRowValue row in rows.Values)
            {
                creditsettlementsList.Add(
                    new CreditSettlementRow()
                    {
                        SNo=(n++).ToString(),
                        CustomerName=row.CustomerName,
                        Tin=row.Tin,
                        CreditSettle=AccountBase.FormatAmount(row.CreditSettle),
                        CreditTaken = AccountBase.FormatAmount(row.CreditTaken),
                        TotalSell = AccountBase.FormatAmount(row.TotalSell),
                        OutstandingCredit = AccountBase.FormatAmount(row.CreditTaken - row.CreditSettle),
                    }
                    );
            }
            var model = new
            {
                creditsettlementsList = creditsettlementsList,
            };

            string template = System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["html_root"] + "\\iERPManService\\creditSettlement_table.cshtml");
            string templateKey = template.GetHashCode().ToString();
            string html = RazorEngine.Engine.Razor.RunCompile(template, templateKey, null, model);
            return html;
            
//            Dictionary<string, object> vdata = new Dictionary<string, object>();
//            vdata.Add("model", model);
//            StringBuilder sb = new StringBuilder();
//            TemplateEngineWrapper.evaluateTemplate(sb, "iERPManService\\creditSettlement_table.html", vdata);
//            return sb.ToString();

            //return "<h1>Customer Credit Settlement</h1>";
        }
        internal string getProductionTable(DateTime date1, DateTime date2, AdjustedCostHeader adj, AdjustedCostDetail[] detail, bool singleColumn)
        {
            string sql;
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {

                DataTable data;

                List<string> productTypes = new List<string>();
                data = reader.GetDataTable(string.Format("Select itemCode,sum(actualQuantity) from {0}.dbo.ProductionBatchHeader where ticksCreated>={1} and ticksCreated<{2} group by itemCode", this.DBName, date1.Ticks, date2.Ticks));
                Dictionary<string, TransactionItems> titems = new Dictionary<string, TransactionItems>();
                foreach (DataRow row in data.Rows)
                {
                    string itemCode = row[0] as string;
                    if (!productTypes.Contains(itemCode))
                    {
                        productTypes.Add(itemCode);
                        titems.Add(itemCode, berpBDE.GetTransactionItems(itemCode));
                    }
                }
                object[] production = new object[productTypes.Count];
                double totalProductionAmount = 0;
                double totalproductionQuantity = 0;
                int i = 0;
                double[] productionQuantity = new double[productTypes.Count];
                string[] productionUnit = new string[productTypes.Count];
                foreach (DataRow row in data.Rows)
                {
                    string itemCode = row[0] as string;
                    double c = 0;
                    double q = (double)row[1];
                    totalProductionAmount += c;
                    totalproductionQuantity += q;
                    production[i] = new { a = AccountBase.FormatAmount(c), q = AccountBase.FormatQuantity(q) + " litter"};
                    productionUnit[i] = "litter";
                    productionQuantity[i] = q;
                    i++;
                }

                sql = string.Format(@"SELECT     BatchCostItem.tag, ProductionBatchHeader.itemCode, SUM(BatchCostItem.cost) AS cost
FROM         {0}.dbo.BatchCostItem INNER JOIN
                      {0}.dbo.ProductionBatchHeader ON BatchCostItem.batchID = ProductionBatchHeader.id
where ticksCreated>={1} and ticksCreated<{2}
GROUP BY BatchCostItem.tag, ProductionBatchHeader.itemCode order by itemCode", this.DBName, date1.Ticks, date2.Ticks);
                data = reader.GetDataTable(sql);

                List<string> costTypes = new List<string>();
                 
                foreach (DataRow row in data.Rows)
                {
                    string tag = row[0] as string;
                    string itemCode = row[1] as string;
                    double cost = (double)row[2];
                    if (!costTypes.Contains(tag))
                        costTypes.Contains(tag);

                }

                Dictionary<string, Dictionary<string, double[]>> materialData = new Dictionary<string, Dictionary<string, double[]>>();
                Dictionary<string, Dictionary<string, double[]>> otherData = new Dictionary<string, Dictionary<string, double[]>>();
                SummaryInformation matsum = getMaterialReportingScheme();
                SummaryInformation othsum = getOtherCostReportingScheme();
                //populate the materialData and otherData from database BatchCostItem table
                foreach (DataRow row in data.Rows)
                {
                    string tag = row[0] as string;
                    string itemCode = row[1] as string;
                    double cost = (double)row[2];
                    bool isMaterial = matsum.getItemByTag(tag) != null;
                    Dictionary<string, Dictionary<string, double[]>> thisData = isMaterial ? materialData : otherData;
                    Dictionary<string, double[]> costRow = new Dictionary<string, double[]>();
                    if (thisData.ContainsKey(tag))
                        costRow = thisData[tag];
                    else
                        thisData.Add(tag, costRow = new Dictionary<string, double[]>());
                    if(adj==null)
                        costRow.Add(itemCode, new double[] { 0, cost });
                    else
                        costRow.Add(itemCode, new double[] { 0, cost,0 });
                }
                List<object> products = new List<object>();
                foreach (string itemCode in productTypes)
                    products.Add(titems[itemCode].Name);


                //calculate quantity and cost totals
                double[][] colTotals = new double[productTypes.Count][];
                for (i = 0; i < colTotals.Length; i++)
                    if (adj == null)
                        colTotals[i] = new double[] { 0, 0 };
                    else
                        colTotals[i] = new double[] { 0, 0, 0 };
                double[][] rowTotals = new double[materialData.Count + otherData.Count][];
                double grandTotalQ = 0;
                double grandTotalA = 0;
                i = 0;
                foreach (Dictionary<string, Dictionary<string, double[]>> d in new Dictionary<string, Dictionary<string, double[]>>[] { materialData, otherData })
                {
                    foreach (KeyValuePair<string, Dictionary<string, double[]>> kvSubItem in d)
                    {
                        double totalQ = 0;
                        double totalA = 0;
                        foreach (KeyValuePair<string, double[]> kvCosts in kvSubItem.Value)
                        {
                            int index = productTypes.IndexOf(kvCosts.Key);
                            totalQ += kvCosts.Value[0];
                            grandTotalQ+= kvCosts.Value[0];
                            totalA += kvCosts.Value[1];
                            grandTotalA += kvCosts.Value[1];
                            colTotals[index][0] += kvCosts.Value[0];
                            colTotals[index][1] += kvCosts.Value[1];
                        }
                        if (adj == null)
                            rowTotals[i] = new double[] { totalQ, totalA};
                        else
                            rowTotals[i] = new double[] { totalQ, totalA,0 };
                        i++;
                    }
                }
                //prorate adjusted costs
                if(adj!=null)
                {
                    double adjFactor = date2.Subtract(date1).TotalSeconds / adj.dateTo.Subtract(adj.dateFrom).TotalSeconds;
                    Dictionary<string, AdjustedCostDetail> adjustedCosts=new Dictionary<string,AdjustedCostDetail>();
                    foreach(AdjustedCostDetail d in detail)
                    {
                        adjustedCosts.Add(d.itemTag, d);
                    }
                    i = 0;
                    foreach (Dictionary<string, Dictionary<string, double[]>> d in new Dictionary<string, Dictionary<string, double[]>>[] { materialData, otherData })
                    {

                        foreach (KeyValuePair<string, Dictionary<string, double[]>> kvSubItem in d)
                        {
                            AdjustedCostDetail adjusted = null;
                            if (!adjustedCosts.TryGetValue(kvSubItem.Key, out adjusted))
                                adjusted = null;

                            double totalAdj = 0;
                            foreach (KeyValuePair<string, double[]> kvCosts in kvSubItem.Value)
                            {
                                int index = productTypes.IndexOf(kvCosts.Key);
                                if (adjusted ==null)
                                    kvCosts.Value[2] = kvCosts.Value[1];
                                else
                                {
                                    double thisRowTotal = rowTotals[i][1];
                                    if (AccountBase.AmountEqual(thisRowTotal, 0))
                                        kvCosts.Value[2] = colTotals[index][1] * (adjFactor*adjusted.cost) / grandTotalA;
                                    else
                                        kvCosts.Value[2] = kvCosts.Value[1] * (adjFactor*adjusted.cost) / thisRowTotal;
                                        
                                }
                                totalAdj += kvCosts.Value[2];
                                colTotals[index][2] += kvCosts.Value[2];
                            }
                            rowTotals[i][2] = totalAdj;
                            i++;
                        }
                    }
                }

                object[] sections = new object[2];
                int no = 1;
                int sIndex = 0;

                foreach (Dictionary<string, Dictionary<string, double[]>> prodCostData in new Dictionary<string, Dictionary<string, double[]>>[] { materialData, otherData })
                {
                    bool isMaterial = prodCostData == materialData;
                    string sectionName = isMaterial ? "Material Costs" : "Other Costs";
                    object[] subItems = new object[prodCostData.Count];
                    i = 0;
                    int[] schemeIndex = new int[prodCostData.Count];
                    foreach (KeyValuePair<string, Dictionary<string, double[]>> kvSubItem in prodCostData)
                    {
                        SummaryInformation sinfo=(isMaterial ? matsum : othsum);
                        SummaryItemBase sitem=sinfo.getItemByTag(kvSubItem.Key);
                        string desc = sitem.label;
                        schemeIndex[i] = sinfo.getItemParent(kvSubItem.Key).summaryItem.IndexOf(sitem);

                        object[] costs = new object[productTypes.Count];
                        for (int z = 0; z < costs.Length; z++)
                        {
                            if(adj==null || singleColumn)
                                costs[z] = new { q = "", a = "" };
                            else
                                costs[z] = new { q = "", a = "",adj="" };
                        }
                        foreach (KeyValuePair<string, double[]> kvCosts in kvSubItem.Value)
                        {
                            int index = productTypes.IndexOf(kvCosts.Key);
                            if(adj==null || singleColumn)
                                costs[index] = new { q = "", a = AccountBase.FormatAmount(kvCosts.Value[adj==null?1:2]) };
                            else
                                costs[index] = new { q = "", a = AccountBase.FormatAmount(kvCosts.Value[1]), adj = AccountBase.FormatAmount(kvCosts.Value[2]) };

                        }
                        subItems[i] = new { 
                            tag=kvSubItem.Key,
                            no = no.ToString(), 
                            desc = desc, 
                            costs = costs, 
                            totalCost = 
                            ((adj==null || singleColumn)?
                                (object)new { q = "-", a = AccountBase.FormatAmount(rowTotals[no-1][adj==null?1:2]) }
                                : (object)new { q = "-", a = AccountBase.FormatAmount(rowTotals[no - 1][1]), adj = AccountBase.FormatAmount(rowTotals[no-1][2]) }) 
                        };
                        no++;
                        i++;
                    }
                    Array.Sort(schemeIndex, subItems);
                    sections[sIndex] = new { name = sectionName, subItems = subItems };
                    sIndex++;
                }
                double totalQuantity = 0;
                double totalAmount = 0;
                double totalAdjustedAmount = 0;
                foreach (double[] qa in colTotals)
                {
                    totalQuantity += qa[0];
                    totalAmount += qa[1];
                    if (adj != null)
                        totalAdjustedAmount += qa[2];
                }
                object[] _colTotals = new object[colTotals.Length];
                for (i = 0; i < colTotals.Length; i++)
                    if(adj==null || singleColumn)
                        _colTotals[i] = new { q = AccountBase.FormatQuantity(colTotals[i][0]), a = AccountBase.FormatAmount(colTotals[i][adj==null?1:2]) };
                    else
                        _colTotals[i] = new { q = AccountBase.FormatQuantity(colTotals[i][0]), a = AccountBase.FormatAmount(colTotals[i][1]), adj = AccountBase.FormatAmount(colTotals[i][2]) };
                
                object[] unitCosts = new object[colTotals.Length];

                for (i = 0; i < unitCosts.Length; i++)
                {
                    string unitCost;
                    if (AccountBase.AmountEqual(colTotals[i][1], 0))
                        unitCost = "-";
                    else if (AccountBase.AmountEqual(productionQuantity[i], 0))
                        unitCost = "Data Error";
                    else
                        unitCost = AccountBase.FormatQuantity(colTotals[i][1] / productionQuantity[i]) + " Birr/" + productionUnit[i];

                    if (adj == null)
                    {
                            unitCosts[i] = unitCost;
                    }
                    else 
                    {

                        string adjustedUnitCost;
                        if (AccountBase.AmountEqual(colTotals[i][2], 0))
                            adjustedUnitCost = "-";
                        else if (AccountBase.AmountEqual(productionQuantity[i], 0))
                            adjustedUnitCost = "Data Error";
                        else
                            adjustedUnitCost = AccountBase.FormatQuantity(colTotals[i][2] / productionQuantity[i]) + " Birr/" + productionUnit[i];
                        unitCosts[i] =singleColumn?(object)adjustedUnitCost:new { a = unitCost, adj = adjustedUnitCost };
                        
                    }
                }
                string totalUnitCost;
                string totalUnitAdjustedCost=null;
                
                if (AccountBase.AmountEqual(totalAmount, 0))
                    totalUnitCost = "-";
                else if (AccountBase.AmountEqual(totalproductionQuantity, 0))
                    totalUnitCost = "Data Error";
                else
                    totalUnitCost = AccountBase.FormatQuantity(totalAmount / totalproductionQuantity) + " Birr/litter";
                if (adj != null)
                {
                    if (AccountBase.AmountEqual(totalAdjustedAmount, 0))
                        totalUnitAdjustedCost = "-";
                    else if (AccountBase.AmountEqual(totalproductionQuantity, 0))
                        totalUnitAdjustedCost = "Data Error";
                    else
                        totalUnitAdjustedCost = AccountBase.FormatQuantity(totalAdjustedAmount / totalproductionQuantity) + " Birr/litter";
                }

                var model = new
                {
                    products = products,
                    production = production,
                    costSections = sections,
                    colTotals = _colTotals,
                    itemCount = colTotals.Length,
                    unitCosts = unitCosts,
                    totalUnitCost = totalUnitCost,
                    nColPerItem=2,//adj==null?2:3,
                    totalUnitAdjustedCost=singleColumn?null:totalUnitAdjustedCost,
                    totalCost =
                        (adj == null || singleColumn) ? (object)new { q = AccountBase.FormatQuantity(totalQuantity), a = AccountBase.FormatAmount(adj == null ? totalAmount : totalAdjustedAmount) }
                        : (object)new { q = AccountBase.FormatQuantity(totalQuantity), a = AccountBase.FormatAmount(totalAmount), adj = AccountBase.FormatAmount(totalAdjustedAmount) },
                    totalProduction = new { q = AccountBase.FormatQuantity(totalproductionQuantity) + " litter", a = AccountBase.FormatAmount(totalProductionAmount) },
                };

                string template = System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["html_root"] + "\\iERPManService\\production_table.cshtml");
                string templateKey = template.GetHashCode().ToString();
                string html = RazorEngine.Engine.Razor.RunCompile(template, templateKey, null, model);
                return html;


//                Dictionary<string, object> vdata = new Dictionary<string, object>();
//                vdata.Add("model", model);
//                StringBuilder sb = new StringBuilder();
//                TemplateEngineWrapper.evaluateTemplate(sb, "iERPManService\\production_table.html", vdata);
//                return sb.ToString();
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        public double[][][] getSalesSeries(DateRange[] ranges, out string[] items)
        {
            INTAPS.RDBMS.SQLHelper reader = berpBDE.GetReaderHelper();
            try
            {
                items = reader.GetColumnArray<string>("Select code from {0}.dbo.TransactionItems where IsSalesItem=1".format(berpBDE.DBName));
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
            double[][][] all = getSalesSeries(items, ranges);
            List<Double[][]> ret = new List<double[][]>();
            List<string> retItems = new List<string>();
            for (int i = all.Length - 1; i >= 0; i--)
            {
                bool include = false;
                for (int j = 0; j < ranges.Length; j++)
                {
                    if (!AccountBase.AmountEqual(all[i][j][0], 0))
                    {
                        include = true;
                        break;
                    }
                }
                if (include)
                {
                    ret.Add(all[i]);
                    retItems.Add(items[i]);
                }
            }
            items = retItems.ToArray();
            return ret.ToArray();
        }
        public double[][][] getSalesSeries(string[] items, DateRange[] ranges)
        {

            double[][][] ret = new double[items.Length][][];
            if (ranges.Length == 0)
                return ret;
            AccountDocument[][] salesDocs = new AccountDocument[ranges.Length][];
            int N;
            for (int i = 0; i < ranges.Length; i++)
                salesDocs[i] = accountingBDE.GetAccountDocuments(null, accountingBDE.GetDocumentTypeByType(typeof(BIZNET.iERP.Sell2Document)).id, true, ranges[i].t1, ranges[i].t2, 0, -1, out N);

            for (int i = 0; i < items.Length; i++)
            {
                TransactionItems titem = berpBDE.GetTransactionItems(items[i]);
                ret[i] = new double[ranges.Length][];
                for (int j = 0; j < ranges.Length; j++)
                {

                    ret[i][j] = new double[] { 0, 0, 0 };
                    foreach (Sell2Document sell in salesDocs[j])
                    {
                        foreach (TransactionDocumentItem item in sell.items)
                        {
                            if (item.code.Equals(items[i]))
                            {
                                ret[i][j][0] += item.price;
                                ret[i][j][1] += item.quantity;
                            }
                        }
                        foreach (int delivery in sell.deliverieIDs)
                        {
                            foreach (TransactionDocumentItem item in accountingBDE.GetAccountDocument<SoldItemDeliveryDocument>(delivery).items)
                            {
                                if (item.code.Equals(items[i]))
                                {
                                    ret[i][j][2] += item.price;
                                }
                            }
                        }
                    }
                }
            }
            return ret;
        }
        public double[][] getCustSalesReport(DateTime t1, DateTime t2, out TradeRelation[] cust, out TransactionItems[] items,out double[] credit)
        {
            int N;
            AccountDocument[] salesDocs = accountingBDE.GetAccountDocuments(null, accountingBDE.GetDocumentTypeByType(typeof(BIZNET.iERP.Sell2Document)).id, true, t1, t2, 0, -1, out N);
            SortedDictionary<string, TransactionItems> dictItems = new SortedDictionary<string, TransactionItems>();
            SortedDictionary<string, Dictionary<string, double>> dictData = new SortedDictionary<string, Dictionary<string, double>>();

            foreach (Sell2Document sell in salesDocs)
            {
                string custCode = sell.relationCode.ToUpper();
                Dictionary<string, double> custSales;
                if (!dictData.TryGetValue(custCode, out custSales))
                {
                    dictData.Add(custCode, custSales = new Dictionary<string, double>());
                }

                foreach (TransactionDocumentItem item in sell.items)
                {
                    string itemCode = item.code.ToUpper();
                    TransactionItems tItem;
                    if (!dictItems.TryGetValue(itemCode, out tItem))
                        dictItems.Add(itemCode, tItem = berpBDE.GetTransactionItems(itemCode));
                    double custAmount;
                    if (custSales.TryGetValue(itemCode, out custAmount))
                        custSales[itemCode] = custAmount + item.price;
                    else
                        custSales.Add(itemCode, item.price);
                }

            }
            double[][] ret = new double[dictData.Count][];
            credit = new double[dictData.Count];
            int i = 0;
            cust = new TradeRelation[ret.Length];
            items = new TransactionItems[dictItems.Count];
            dictItems.Values.CopyTo(items, 0);
            foreach (KeyValuePair<string, Dictionary<string, double>> kv in dictData)
            {
                ret[i] = new double[dictItems.Count];
                cust[i] = berpBDE.GetRelation(kv.Key);
                credit[i] = accountingBDE.GetFlow(CostCenter.ROOT_COST_CENTER, cust[i].ReceivableAccountID, 0,t1, t2).DebitBalance;
                foreach (KeyValuePair<string, double> itemAmount in kv.Value)
                {
                    int j = 0;
                    foreach (string code in dictItems.Keys)
                    {
                        if (code.Equals(itemAmount.Key))
                            ret[i][j] = itemAmount.Value;
                        else
                            ret[i][j] = 0;
                        j++;
                    }
                }
                i++;
            }
            return ret;
        }
        // BDE exposed
        public AdjustedCostHeader getActualCostHeader(long ticksFrom)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                AdjustedCostHeader[] ret = reader.GetSTRArrayByFilter<AdjustedCostHeader>("ticksFrom<=" + ticksFrom + " and ticksTo>"+ticksFrom);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public AdjustedCostHeader getActualCostHeaderForTicks(long ticks)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                AdjustedCostHeader[] ret = reader.GetSTRArrayByFilter<AdjustedCostHeader>("ticksFrom<={0} and ticksTo>{0}".format(ticks));
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        // BDE exposed
        public AdjustedCostDetail[] getActualCostDetail(long ticksFrom)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                AdjustedCostDetail[] ret = reader.GetSTRArrayByFilter<AdjustedCostDetail>("ticksFrom=" + ticksFrom);
                return ret;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public AdjustedCostHeader[] getAdjustedCostOverlapingWithTimeRange(long ticksFrom,long ticksTo)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                string cr = "(ticksFrom<={0} and ticksTo>{0}) or ({0}<=ticksFrom and {1}>ticksFrom)".format(ticksFrom,ticksTo);
                AdjustedCostHeader[] ret = reader.GetSTRArrayByFilter<AdjustedCostHeader>(cr);
                return ret;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public void setActualCost(int AID, AdjustedCostHeader header, AdjustedCostDetail[] detail)
        {
            bool insert;
            AdjustedCostHeader []test = getAdjustedCostOverlapingWithTimeRange(header.ticksFrom,header.ticksTo);
            if (test.Length>1)
                throw new ServerUserMessage("This cost adjustment data conflicts with another {0} adjustments", test.Length);
            if(test.Length==1)
            {
                if (test[0].ticksFrom == header.ticksFrom)
                    insert = false;
                else
                    throw new ServerUserMessage("This cost adjustment data conflicts with another adjustment saved for dates {0} to {1}", test[0].dateFrom.ToString("MMM dd,yyy"), test[0].dateTo.AddDays(-1).ToString("MMM dd,yyy"));
            }
            else
                insert = true;
            if (!insert)
            {
                dspWriter.logDeletedData(AID, this.DBName + ".dbo.AdjustedCostDetail", "ticksFrom=" + header.ticksFrom);
                dspWriter.logDeletedData(AID, this.DBName + ".dbo.AdjustedCostHeader", "ticksFrom=" + header.ticksFrom);
                dspWriter.Delete(this.DBName, "AdjustedCostDetail", "ticksFrom=" + header.ticksFrom);
                dspWriter.Delete(this.DBName, "AdjustedCostHeader", "ticksFrom=" + header.ticksFrom);
            }
            dspWriter.InsertSingleTableRecord<AdjustedCostHeader>(this.DBName, header, new string[] { "__AID" }, new object[] { AID });
            foreach (AdjustedCostDetail d in detail)
            {
                d.ticksFrom = header.ticksFrom;
                dspWriter.InsertSingleTableRecord<AdjustedCostDetail>(this.DBName, d, new string[] { "__AID" }, new object[] { AID });
            }
        }
        // BDE exposed
        public AdjustedCostHeader[] getActualCosts(long ticksFrom, long ticksTo)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                AdjustedCostHeader[] ret = reader.GetSTRArrayByFilter<AdjustedCostHeader>(null);
                return ret;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public string renderAdjustedCostReport(long ticksFrom, long ticksTo)
        {
            AdjustedCostHeader header = getActualCostHeader(ticksFrom);
            if (header == null)
                return "<h1>Cost adjustment not avialable</h1>";
            if(header.ticksTo!=ticksTo)
                return "<h1>Cost adjustment not avialable</h1>";
            AdjustedCostHeader h=getActualCostHeader(ticksFrom);
            AdjustedCostDetail[] d=getActualCostDetail(ticksFrom);
            return getProductionTable(new DateTime(ticksFrom), new DateTime(ticksTo), h, d,false);
        }
        // BDE exposed
        public void deleteActualCost(int AID, long ticksFrom)
        {
            lock (dspWriter)
            {
                dspWriter.logDeletedData(AID, this.DBName + ".dbo.AdjustedCostDetail", "ticksFrom=" + ticksFrom);
                dspWriter.logDeletedData(AID, this.DBName + ".dbo.AdjustedCostHeader", "ticksFrom=" + ticksFrom);
                dspWriter.Delete(this.DBName, "AdjustedCostDetail", "ticksFrom=" + ticksFrom);
                dspWriter.Delete(this.DBName, "AdjustedCostHeader", "ticksFrom=" + ticksFrom);
            }
        }
    }
}
