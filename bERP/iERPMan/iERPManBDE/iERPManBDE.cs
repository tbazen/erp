﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.ClientServer;
using System.Data;
using System.Xml.Serialization;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Reflection;
using INTAPS.Accounting.BDE;
using INTAPS.Accounting;
using BIZNET.iERP;
using INTAPS;
namespace BIZNET.iERPMan.Server
{

    public partial class iERPManBDE : BDEBase
    {
        
        iERPManSystemParameters m_sysPars;
        
        public iERPManBDE(string DBName, string DB, INTAPS.RDBMS.SQLHelper writer, string connectionString)
            : base(DBName, DB, writer, connectionString)
        {
            LoadSystemParameters();
        }
        public iERPManSystemParameters SysPars
        {
            get
            {
                return m_sysPars;
            }
        }
        void LoadSystemParameters()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                m_sysPars = dspReader.LoadSystemParameters<iERPManSystemParameters>();
            }
            catch (Exception e)
            {
                INTAPS.ClientServer.ApplicationServer.EventLoger.LogException("Error loading system parameters.", e);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        DocumentSerialType getBatchDocumentSerialType(bool throwException)
        {
            DocumentSerialType type = accountingBDE.getDocumentSerialType("JOB");
            if (type == null && throwException)
                throw new ServerUserMessage("BATCH document serial type not configured.");
            return type;
        }
        // BDE exposed
        public object[] GetSystemParameters(string[] fields)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSystemParameters<iERPManSystemParameters>(m_sysPars, fields);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public void SetSystemParameters(int AID, string[] fields, object[] vals)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.SetSystemParameters<iERPManSystemParameters>(m_DBName, m_sysPars, fields, vals, true, AID);
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        internal AccountingBDE accountingBDE
        {
            get
            {
                return ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            }
        }
        internal BIZNET.iERP.Server.iERPTransactionBDE berpBDE
        {
            get
            {
                return ApplicationServer.GetBDE("iERP") as BIZNET.iERP.Server.iERPTransactionBDE;
            }
        }
        public ProductionBatchHeader getProductionBatchHeader(int id)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                ProductionBatchHeader[] ret = reader.GetSTRArrayByFilter<ProductionBatchHeader>(string.Format("id={0} ", id));
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public ProductionBatch getProductionBatch(int id)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                ProductionBatchHeader[] headers=reader.GetSTRArrayByFilter<ProductionBatchHeader>("id=" + id);
                if(headers.Length==0)
                    return null;
                ProductionBatch ret = new ProductionBatch();
                ret.header = headers[0];
                ret.processData = reader.GetSTRArrayByFilter<ProductionBatchProcessData>("batchID=" + id);
                ret.qcData = reader.GetSTRArrayByFilter<ProductionBatchQCData>("batchID=" + id);
                return ret;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public int createProductionBatch(int AID, ProductionBatch batch)
        {
            if (!isPeriodOpen(0,batch.header.ticksCreated))
                throw new ServerUserMessage("Costing period is not open for the specified date.");
            DocumentSerialType refType = getBatchDocumentSerialType(true);
            if (batch.header.id == -1)
            {
                batch.header.id = AutoIncrement.GetKey("iERPMan.batchID");
                int s = accountingBDE.GetNextSerial(m_sysPars.batchNoSerialBatchID);
                batch.header.batchNo = accountingBDE.formatSerialNo(refType.id, s);
                UsedSerial us = new UsedSerial(m_sysPars.batchNoSerialBatchID, s, "New Manufacturing Batch");
                us.date = new DateTime(batch.header.ticksCreated);
                accountingBDE.UseSerial(AID, us);
            }
                

            batch.header.status = ProductionBatchStatus.Closed;
            batch.header.ticksStatus = batch.header.ticksCreated;


            TransactionItems prodItem=berpBDE.GetTransactionItems(batch.header.itemCode);
            if(prodItem==null)
                throw new ServerUserMessage("Invalid production item code {0}",batch.header.itemCode);
            if(!prodItem.IsInventoryItem)
                throw new ServerUserMessage("Item code {0} should be configured as an inventory item",batch.header.itemCode);
            
            //Save process data and post issue transaction from factory floor
            long processEnd=batch.header.ticksCreated;
            foreach(ProductionBatchProcessData p in batch.processData)
            {
                ProcessType ptype=getProcessType(p.processID);
                if (ptype == null)
                    throw new ServerUserMessage("Invalid process ID {0}", p.processID);

                if (p.startTicks < batch.header.ticksCreated)
                    throw new ServerUserMessage("Process start time can't before batch date");
                if(p.endTicks<p.startTicks)
                    throw new ServerUserMessage("Process end time can't before process start time");
                if(processEnd<p.endTicks)
                    processEnd=p.endTicks;
                p.batchID = batch.header.id;
                if (p.items.Length > 0)
                {
                    StoreIssueDocument issue = new StoreIssueDocument();
                    issue.tranTicks = p.startTicks;
                    issue.ShortDescription = string.Format("Items issued for production batch {0} and process {1}", batch.header.batchNo, ptype.description);
                    issue.voucher = new DocumentTypedReference(refType.id, batch.header.batchNo, true);
                    issue.storeID = p.storeCostCenterID;
                    issue.items = p.items;
                    foreach (TransactionDocumentItem item in p.items)
                    {
                        item.costCenterID = batch.header.factoryID;
                    }
                    p.issuedMaterialDocumentID = accountingBDE.PostGenericDocument(AID, issue);
                }
                else
                    p.issuedMaterialDocumentID = -1;
                dspWriter.InsertSingleTableRecord(this.DBName, p,new string[]{"__AID"},new object[]{AID});
                
            }
            
            //post finished good
            double totalMaterialCost = 0;
            foreach (ProductionBatchProcessData p in batch.processData)
            {
                if(p.issuedMaterialDocumentID!=-1)
                    totalMaterialCost += accountingBDE.GetAccountDocument<StoreIssueDocument>(p.issuedMaterialDocumentID).items.totalAmount();
            }
            StoreFinishedGoodsDocument fg = new StoreFinishedGoodsDocument();
            fg.tranTicks = processEnd;
            fg.ShortDescription = "Output of Production Batch {0}".format(batch.header.batchNo);
            fg.voucher= new DocumentTypedReference(refType.id, batch.header.batchNo, true);
            Factory factory=getFactory(batch.header.factoryID);
            if(factory==null)
                throw new ServerUserMessage("Invalid factory id {0}".format(batch.header.factoryID));
            fg.costCenterID = factory.defaultStoreID;
            fg.items = new TransactionDocumentItem[] { 
                new TransactionDocumentItem(factory.costCenterID, prodItem, batch.header.actualQuantity, totalMaterialCost / batch.header.actualQuantity)
                    {
                        unitID=m_sysPars.defaultProductionUnit
                    }
            };
            batch.header.finishedGoodDocumentID = accountingBDE.PostGenericDocument(AID, fg);
            dspWriter.InsertSingleTableRecord(this.DBName, batch.header, new string[] { "__AID" }, new object[] { AID });
            
            //save quality check data
            foreach(ProductionBatchQCData qc in batch.qcData)
            {
                QualityCheckParameter par = getQualityCheckParameter(qc.parameterID);
                if (par == null)
                    throw new ServerUserMessage("Invalid quality check parameter id {0}", qc.parameterID);
                QualityCheckParameter standard=getDynamicStandardForItem(par.id,prodItem.Code);
                if(standard==null)
                    throw new ServerUserMessage("System Inconsistency: parameter {0} is registerd in the database but its standard couldn't be retreived.",par.name);

                if(qc.measuredValue<standard.validMin || qc.measuredValue>standard.validMax)
                    throw new ServerUserMessage("Parameter {0} is out of allowed range.", par.name); ;
                qc.batchID = batch.header.id;
                dspWriter.InsertSingleTableRecord(this.DBName,qc, new string[] { "__AID" }, new object[] { AID });
            }
            return batch.header.id;
        }
        // BDE exposed
        public void updateProductionBatch(int AID, ProductionBatch batch)
        {
            ProductionBatchHeader oldBatchHeader = getProductionBatchHeader(batch.header.id);
            batch.header.batchNo = oldBatchHeader.batchNo;
            deleteProductionBatch(AID, batch.header.id);
            createProductionBatch(AID, batch);
        }
        // BDE exposed
        public void deleteProductionBatch(int AID, int batchID)
        {
            ProductionBatch batch = getProductionBatch(batchID);
            if (batch == null)
                throw new ServerUserMessage("Batch ID {0} not valid.", batchID);
            foreach(ProductionBatchProcessData p in batch.processData)
            {
                if(p.issuedMaterialDocumentID>0 && accountingBDE.DocumentExists(p.issuedMaterialDocumentID))
                    accountingBDE.DeleteGenericDocument(AID, p.issuedMaterialDocumentID);
            }
            if (batch.header.finishedGoodDocumentID != -1 && accountingBDE.DocumentExists(batch.header.finishedGoodDocumentID))
                accountingBDE.DeleteGenericDocument(AID, batch.header.finishedGoodDocumentID);
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.ProductionBatchProcessData", "batchID=" + batchID);
            dspWriter.Delete(this.DBName, "ProductionBatchProcessData", "batchID=" + batchID);
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.ProductionBatchQCData", "batchID=" + batchID);
            dspWriter.Delete(this.DBName, "ProductionBatchQCData", "batchID=" + batchID);

            dspWriter.logDeletedData(AID, this.DBName + ".dbo.ProductionBatchHeader", "id=" + batchID);
            dspWriter.DeleteSingleTableRecrod<ProductionBatchHeader>(this.DBName, batchID);
        }
        // BDE exposed
        public ProductionBatchHeader[] searchBatch(ProductionBatchSearchOption options, int pageIndex, int pageSize, out int N)
        {
            string cr=null;
            if (!string.IsNullOrEmpty(options.remark))
                cr = string.Format("remark like '%{0}%", INTAPS.RDBMS.SQLHelper.EscapeString(options.remark));
            if(!string.IsNullOrEmpty(options.batchNo))
                cr = INTAPS.RDBMS.SQLHelper.AppendOperand(cr," AND ", string.Format("batchNo ='{0}'", INTAPS.RDBMS.SQLHelper.EscapeString(options.batchNo)));
            if (options.tickFrom!=-1)
                cr = INTAPS.RDBMS.SQLHelper.AppendOperand(cr, " AND ", string.Format("ticksCreated>={0}",options.tickFrom));
            if (options.tickTo!= -1)
                cr = INTAPS.RDBMS.SQLHelper.AppendOperand(cr, " AND ", string.Format("ticksCreated<{0}", options.tickTo));
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<ProductionBatchHeader>(cr,pageIndex,pageSize,out N);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public void changeStatus(int AID, int productionBatchID, long ticks, ProductionBatchStatus newStatus)
        {
            ProductionBatchHeader b = getProductionBatchHeader(productionBatchID);
            if (b == null)
                throw new ServerUserMessage("Production batch not found id:{0}", productionBatchID);
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.ProductionBatch", "id=" + productionBatchID);
            dspWriter.Update(this.DBName, "ProductionBatch", new string[] { "status", "ticksStatus", "__AID" }, new object[] { 
                AID,newStatus,ticks}, "id=" + productionBatchID);
        }
        // BDE exposed
        public ProductionBatchHeader[] getBatchesByStatus(long before, ProductionBatchStatus status)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<ProductionBatchHeader>(string.Format("status={0} and ticksStatus={1}",(int)status,before));
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public ProcessType getProcessType(int processTypeID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                ProcessType[] ret = reader.GetSTRArrayByFilter<ProcessType>("id=" + processTypeID);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public ProcessMachine[] getProcessMachines(int processTypeID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<ProcessMachine>("processTypeID=" + processTypeID);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public ProcessType[] getAllProcessTypes()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<ProcessType>(null);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        private void assertMachineCostCenter(ProcessMachine m)
        {
            if (this.accountingBDE.GetAccount<CostCenter>(m.costCenterID) == null)
                throw new ServerUserMessage("Invalid cost center ID {0} for the process machine.", m.costCenterID);
        }

    }
}
