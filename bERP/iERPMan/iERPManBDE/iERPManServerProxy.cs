﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.ClientServer;
using System.Data;
using INTAPS.ClientServer.RemottingServer;

namespace BIZNET.iERPMan.Server
{
    internal class iERPManServerProxy : RemottingServerProxyBase, IiERPManService
    {

        public BIZNET.iERPMan.ProductionBatch getProductionBatch(string sessionID, int id)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getProductionBatch(id);
        }

        public int createProductionBatch(string sessionID, BIZNET.iERPMan.ProductionBatch batch)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).createProductionBatch(batch);
        }

        public void updateProductionBatch(string sessionID, BIZNET.iERPMan.ProductionBatch batch)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).updateProductionBatch(batch);
        }

        public void deleteProductionBatch(string sessionID, int batchID)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).deleteProductionBatch(batchID);
        }

        public BIZNET.iERPMan.ProductionBatchHeader[] searchBatch(string sessionID, BIZNET.iERPMan.ProductionBatchSearchOption options, int pageIndex, int pageSize, out int N)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).searchBatch(options, pageIndex, pageSize, out N);
        }

        public void changeStatus(string sessionID, int productionBatchID, long ticks, BIZNET.iERPMan.ProductionBatchStatus newStatus)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).changeStatus(productionBatchID, ticks, newStatus);
        }

        public BIZNET.iERPMan.ProductionBatchHeader[] getBatchesByStatus(string sessionID, long before, BIZNET.iERPMan.ProductionBatchStatus status)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getBatchesByStatus(before, status);
        }

        public BIZNET.iERPMan.ProcessType getProcessType(string sessionID, int processTypeID)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getProcessType(processTypeID);
        }

        public BIZNET.iERPMan.ProcessMachine[] getProcessMachines(string sessionID, int processTypeID)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getProcessMachines(processTypeID);
        }

        public BIZNET.iERPMan.ProcessType[] getAllProcessTypes(string sessionID)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getAllProcessTypes();
        }

        public int createProcessType(string sessionID, BIZNET.iERPMan.ProcessType type, BIZNET.iERPMan.ProcessMachine[] machines)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).createProcessType(type, machines);
        }

        public void upateProcessType(string sessionID, BIZNET.iERPMan.ProcessType type, BIZNET.iERPMan.ProcessMachine[] machines)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).upateProcessType(type, machines);
        }

        public BIZNET.iERPMan.Factory getFactory(string sessionID, int costCenterID)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getFactory(costCenterID);
        }

        public BIZNET.iERPMan.Factory[] getAllFactories(string sessionID)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getAllFactories();
        }

        public void setFactory(string sessionID, BIZNET.iERPMan.Factory factory)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).setFactory(factory);
        }

        public void deleteFactory(string sessionID, int costCenterID)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).deleteFactory(costCenterID);
        }

        public BIZNET.iERPMan.BatchType getBatchType(string sessionID, int id)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getBatchType(id);
        }

        public BIZNET.iERPMan.BatchType[] getAllBatchTypes(string sessionID)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getAllBatchTypes();
        }

        public int createBatchType(string sessionID, BIZNET.iERPMan.BatchType type)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).createBatchType(type);
        }

        public void updateBatchType(string sessionID, BIZNET.iERPMan.BatchType type)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).updateBatchType(type);
        }

        public void deleteBatchType(string sessionID, int id)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).deleteBatchType(id);
        }

        public BIZNET.iERPMan.QualityCheckParameter getQualityCheckParameter(string sessionID, int id)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getQualityCheckParameter(id);
        }

        public BIZNET.iERPMan.QualityCheckParameter[] getAllQualityCheckParameters(string sessionID)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getAllQualityCheckParameters();
        }

        public int createQualityCheckParameter(string sessionID, BIZNET.iERPMan.QualityCheckParameter type)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).createQualityCheckParameter(type);
        }

        public void updateQualityCheckParameter(string sessionID, BIZNET.iERPMan.QualityCheckParameter type)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).updateQualityCheckParameter(type);
        }

        public void deleteQualityCheckParameter(string sessionID, int id)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).deleteQualityCheckParameter(id);
        }

        public BIZNET.iERPMan.QualityCheckCategoryStandard[] getCategoryStandards(string sessionID, int parmeterID)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getCategoryStandards(parmeterID);
        }

        public BIZNET.iERPMan.QualityCheckCategoryStandard getQualityCheckCategoryStandard(string sessionID, int parameterID, int categoryID)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getQualityCheckCategoryStandard(parameterID, categoryID);
        }

        public void setQualityCheckCategoryStandard(string sessionID, BIZNET.iERPMan.QualityCheckCategoryStandard standard)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).setQualityCheckCategoryStandard(standard);
        }

        public void deleteQualityCheckCategoryStandard(string sessionID, int parameterID, int categoryID)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).deleteQualityCheckCategoryStandard(parameterID, categoryID);
        }

        public BIZNET.iERPMan.QualityCheckItemStandard[] getItemStandards(string sessionID, int parmeterID)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getItemStandards(parmeterID);
        }

        public BIZNET.iERPMan.QualityCheckItemStandard getQualityCheckItemStandard(string sessionID, int parameterID, string itemCode)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getQualityCheckItemStandard(parameterID, itemCode);
        }

        public void setQualityCheckItemStandard(string sessionID, BIZNET.iERPMan.QualityCheckItemStandard standard)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).setQualityCheckItemStandard(standard);
        }

        public void deleteQualityCheckItemStandard(string sessionID, int parameterID, string itemCode)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).deleteQualityCheckItemStandard(parameterID, itemCode);
        }

        public BIZNET.iERPMan.QualityCheckParameter getDynamicStandardForItem(string sessionID, int parameterID, string itemCode)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getDynamicStandardForItem(parameterID, itemCode);
        }

        public BIZNET.iERPMan.QualityCheckParameter getDynamicStandardForItem(string sessionID, int parameterID, string itemCode, out object scopeObject)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getDynamicStandardForItem(parameterID, itemCode, out scopeObject);
        }

        public object[] GetSystemParameters(string sessionID, string[] fields)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).GetSystemParameters(fields);
        }

        public void SetSystemParameters(string sessionID, string[] fields, object[] vals)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).SetSystemParameters(fields, vals);
        }

        public void deleteProcessType(string sessionID, int processTypeID)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).deleteProcessType(processTypeID);
        }

        public BIZNET.iERPMan.CostingPeriod getCostingPeriod(string sessionID, int level, long t)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getCostingPeriod(level, t);
        }

        public BIZNET.iERPMan.CostingPeriod caculateCostingPeriond(string sessionID, int level, long t)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).caculateCostingPeriond(level, t);
        }

        public BIZNET.iERPMan.CostingPeriod getLastCostingPeriod(string sessionID, int level, long before)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getLastCostingPeriod(level, before);
        }

        public BIZNET.iERPMan.CostingPeriod[] getOpenCostingPeriods(string sessionID, int level, long from, long to)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getOpenCostingPeriods(level, from, to);
        }

        public BIZNET.iERPMan.CostingPeriod[] getCostingPeriods(string sessionID, int level, long from, long to, int index, int pageSize, out int N)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getCostingPeriods(level, from, to, index, pageSize, out N);
        }

        public BIZNET.iERPMan.CostingPeriod openCostingPeriod(string sessionID, int level, long time)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).openCostingPeriod(level, time);
        }

        public void closeCostingPeriod(string sessionID, int level, long time)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).closeCostingPeriod(level, time);
        }

        public BIZNET.iERPMan.BatchCostItem[] getBatchCostItems(string sessionID, int batchID)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getBatchCostItems(batchID);
        }

        public BIZNET.iERPMan.CostingSetting getCostSetting(string sessionID, string tag)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getCostSetting(tag);
        }

        public void setCostSetting(string sessionID, BIZNET.iERPMan.CostingSetting setting)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).setCostSetting(setting);
        }

        public void deleteCostSetting(string sessionID, string tag)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).deleteCostSetting(tag);
        }
        public void saveWorkData(string sessionID, Data_Absence[] absenceData, Data_OverTime[] overTimeData)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).saveWorkData(absenceData, overTimeData);
        }
        public BIZNET.iERPMan.CostingSetting[] getAllCostingSettings(string sessionID)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getAllCostingSettings();
        }
        public Data_OverTime[] getOverTimeWorkData(string sessionID, DateTime date)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getOverTimeWorkData(date);
        }
        public Data_Absence[] getAbsenceWorkData(string sessionID, DateTime date)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getAbsenceWorkData(date);
        }
        public WorkDataResult[] getWorkData(string sessionID, int month, int year)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getWorkData(month, year);
        }
        public BIZNET.iERPMan.AdjustedCostHeader getActualCostHeader(string sessionID, long ticksFrom)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getActualCostHeader(ticksFrom);
        }

        public BIZNET.iERPMan.AdjustedCostDetail[] getActualCostDetail(string sessionID, long ticksFrom)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getActualCostDetail(ticksFrom);
        }

        public void setActualCost(string sessionID, BIZNET.iERPMan.AdjustedCostHeader header, BIZNET.iERPMan.AdjustedCostDetail[] detail)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).setActualCost(header, detail);
        }

        public BIZNET.iERPMan.AdjustedCostHeader[] getActualCosts(string sessionID, long ticksFrom, long ticksTo)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getActualCosts(ticksFrom, ticksTo);
        }

        public string renderAdjustedCostReport(string sessionID, long ticksFrom, long ticksTo)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).renderAdjustedCostReport(ticksFrom, ticksTo);
        }

        public BIZNET.iERP.SummaryInformation getCostReportingScheme(string sessionID)
        {
            this.SetSessionObject(sessionID);
            return ((iERPManService)base.SessionObject).getCostReportingScheme();
        }

        public void deleteActualCost(string sessionID, long ticksFrom)
        {
            this.SetSessionObject(sessionID);
            ((iERPManService)base.SessionObject).deleteActualCost(ticksFrom);
        }

    }
}
