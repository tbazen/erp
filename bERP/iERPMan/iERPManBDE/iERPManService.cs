﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.ClientServer;
using System.Data;

namespace BIZNET.iERPMan.Server
{
    public partial class iERPManService : SessionObjectBase<iERPManBDE>
    {
        public iERPManBDE bde
        {
            get
            {
                return _bde;
            }
        }
        public iERPManService(UserSessionData session)
            : base(session, ApplicationServer.GetBDE("iERPMan") as iERPManBDE)
        {
        }

        public BIZNET.iERPMan.ProductionBatch getProductionBatch(int id)
        {
            checkViewProductionBatchDetail();
            return _bde.getProductionBatch(id);
        }
        private void checkTimeSheetPermission()
        {
            if (!this.m_Session.Permissions.IsPermited("root/ierpman/timesheet"))
                throw new AccessDeniedException("You are not allowed manage time employee time sheet");
        }
        private void checkCostAdjustmentPermission()
        {
            if (!this.m_Session.Permissions.IsPermited("root/ierpman/costadjustment"))
                throw new AccessDeniedException("You are not allowed set cost adjustment");
        }

        private void checkOpenPeriodManagmentPermission()
        {
            if (!this.m_Session.Permissions.IsPermited("root/ierpman/openperiod"))
                throw new AccessDeniedException("You are not allowed to open costing periods");
        }
        private void checkClosePeriodManagmentPermission()
        {
            if (!this.m_Session.Permissions.IsPermited("root/ierpman/closeperiod"))
                throw new AccessDeniedException("You are not allowed to open costing periods");
        }

        public void checkViewProductionBatchList()
        {
            if (!this.m_Session.Permissions.IsPermited("root/ierpman/batchlist/view"))
                throw new AccessDeniedException("You are not allowed to list of production batches");
        }
        public void checkDeleteProductionBatchDetail()
        {
            if (!this.m_Session.Permissions.IsPermited("root/ierpman/batchlist/delete"))
                throw new AccessDeniedException("You are not allowed to delete production batch detail");
        }

        public void checkViewProductionBatchDetail()
        {
            if(!this.m_Session.Permissions.IsPermited("root/ierpman/batch/view"))
                throw new AccessDeniedException("You are not allowed to view production batch detail");
        }
        public void checkEditProductionBatchDetail()
        {
            if(!this.m_Session.Permissions.IsPermited("root/ierpman/batch/edit"))
                throw new AccessDeniedException("You are not allowed to edit of production batch detail");
        }
        public void checkChangeProductionBatchStatus()
        {
            if (!this.m_Session.Permissions.IsPermited("root/ierpman/batch/changestatus"))
                throw new AccessDeniedException("You are not allowed to change status of production batch");
        }
        private void checkProductionConfigurationPermission()
        {
            if (!this.m_Session.Permissions.IsPermited("root/ierpman/configure"))
                throw new AccessDeniedException("You are not allowed to change configuration");
        }
        public void checkEditOverrideProductionBatchDetail()
        {
            if (!this.m_Session.Permissions.IsPermited("root/ierpman/batch/editoverride"))
                throw new AccessDeniedException("You are not allowed to edit of production batche detail entered by other user");
        }
        void checkEstimationConfigurationPermission()
        {
        }
        void securityAdminPermission()
        {
        }
        void storeKeeperPermission()
        {
        }

        public int createProductionBatch(BIZNET.iERPMan.ProductionBatch batch)
        {
            checkEditProductionBatchDetail();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("createProductionBatch Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.createProductionBatch(AID, batch);
                    base.WriteExecuteAudit("createProductionBatch Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("createProductionBatch Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void updateProductionBatch(BIZNET.iERPMan.ProductionBatch batch)
        {
            checkEditProductionBatchDetail();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("updateProductionBatch Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.updateProductionBatch(AID, batch);
                    base.WriteExecuteAudit("updateProductionBatch Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("updateProductionBatch Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void deleteProductionBatch(int batchID)
        {
            checkDeleteProductionBatchDetail();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteProductionBatch Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteProductionBatch(AID, batchID);
                    base.WriteExecuteAudit("deleteProductionBatch Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteProductionBatch Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public BIZNET.iERPMan.ProductionBatchHeader[] searchBatch(BIZNET.iERPMan.ProductionBatchSearchOption options, int pageIndex, int pageSize, out int N)
        {
            checkViewProductionBatchList();
            return _bde.searchBatch(options, pageIndex, pageSize, out N);
        }

        public void changeStatus(int productionBatchID, long ticks, BIZNET.iERPMan.ProductionBatchStatus newStatus)
        {
            checkChangeProductionBatchStatus();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("changeStatus Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.changeStatus(AID, productionBatchID, ticks, newStatus);
                    base.WriteExecuteAudit("changeStatus Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("changeStatus Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public BIZNET.iERPMan.ProductionBatchHeader[] getBatchesByStatus(long before, BIZNET.iERPMan.ProductionBatchStatus status)
        {
            checkViewProductionBatchList();
            return _bde.getBatchesByStatus(before, status);
        }

        public BIZNET.iERPMan.ProcessType getProcessType(int processTypeID)
        {
            return _bde.getProcessType(processTypeID);
        }

        public BIZNET.iERPMan.ProcessMachine[] getProcessMachines(int processTypeID)
        {
            return _bde.getProcessMachines(processTypeID);
        }

        public BIZNET.iERPMan.ProcessType[] getAllProcessTypes()
        {
            return _bde.getAllProcessTypes();
        }

        public int createProcessType(BIZNET.iERPMan.ProcessType type, BIZNET.iERPMan.ProcessMachine[] machines)
        {
            checkProductionConfigurationPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("createProcessType Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.createProcessType(AID, type, machines);
                    base.WriteExecuteAudit("createProcessType Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("createProcessType Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }


        public void upateProcessType(BIZNET.iERPMan.ProcessType type, BIZNET.iERPMan.ProcessMachine[] machines)
        {
            checkProductionConfigurationPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("upateProcessType Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.upateProcessType(AID, type, machines);
                    base.WriteExecuteAudit("upateProcessType Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("upateProcessType Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public BIZNET.iERPMan.Factory getFactory(int costCenterID)
        {
            return _bde.getFactory(costCenterID);
        }

        public BIZNET.iERPMan.Factory[] getAllFactories()
        {
            return _bde.getAllFactories();
        }

        public void setFactory(BIZNET.iERPMan.Factory factory)
        {
            checkProductionConfigurationPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("setFactory Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.setFactory(AID, factory);
                    base.WriteExecuteAudit("setFactory Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("setFactory Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void deleteFactory(int costCenterID)
        {
            checkProductionConfigurationPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteFactory Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteFactory(AID, costCenterID);
                    base.WriteExecuteAudit("deleteFactory Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteFactory Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public BIZNET.iERPMan.BatchType getBatchType(int id)
        {
            return _bde.getBatchType(id);
        }

        public BIZNET.iERPMan.BatchType[] getAllBatchTypes()
        {
            return _bde.getAllBatchTypes();
        }

        public int createBatchType(BIZNET.iERPMan.BatchType type)
        {
            checkProductionConfigurationPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("createBatchType Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.createBatchType(AID, type);
                    base.WriteExecuteAudit("createBatchType Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("createBatchType Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void updateBatchType(BIZNET.iERPMan.BatchType type)
        {
            checkProductionConfigurationPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("updateBatchType Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.updateBatchType(AID, type);
                    base.WriteExecuteAudit("updateBatchType Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("updateBatchType Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void deleteBatchType(int id)
        {
            checkProductionConfigurationPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteBatchType Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteBatchType(AID, id);
                    base.WriteExecuteAudit("deleteBatchType Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteBatchType Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public BIZNET.iERPMan.QualityCheckParameter getQualityCheckParameter(int id)
        {
            return _bde.getQualityCheckParameter(id);
        }

        public BIZNET.iERPMan.QualityCheckParameter[] getAllQualityCheckParameters()
        {
            return _bde.getAllQualityCheckParameters();
        }

        public int createQualityCheckParameter(BIZNET.iERPMan.QualityCheckParameter type)
        {
            checkProductionConfigurationPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("createQualityCheckParameter Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.createQualityCheckParameter(AID, type);
                    base.WriteExecuteAudit("createQualityCheckParameter Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("createQualityCheckParameter Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void updateQualityCheckParameter(BIZNET.iERPMan.QualityCheckParameter type)
        {
            checkProductionConfigurationPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("updateQualityCheckParameter Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.updateQualityCheckParameter(AID, type);
                    base.WriteExecuteAudit("updateQualityCheckParameter Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("updateQualityCheckParameter Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void deleteQualityCheckParameter(int id)
        {
            checkProductionConfigurationPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteQualityCheckParameter Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteQualityCheckParameter(AID, id);
                    base.WriteExecuteAudit("deleteQualityCheckParameter Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteQualityCheckParameter Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public BIZNET.iERPMan.QualityCheckCategoryStandard[] getCategoryStandards(int parmeterID)
        {
            return _bde.getCategoryStandards(parmeterID);
        }

        public BIZNET.iERPMan.QualityCheckCategoryStandard getQualityCheckCategoryStandard(int parameterID, int categoryID)
        {
            return _bde.getQualityCheckCategoryStandard(parameterID, categoryID);
        }

        public void setQualityCheckCategoryStandard(BIZNET.iERPMan.QualityCheckCategoryStandard standard)
        {
            checkProductionConfigurationPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("setQualityCheckCategoryStandard Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.setQualityCheckCategoryStandard(AID, standard);
                    base.WriteExecuteAudit("setQualityCheckCategoryStandard Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("setQualityCheckCategoryStandard Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void deleteQualityCheckCategoryStandard(int parameterID, int categoryID)
        {
            checkProductionConfigurationPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteQualityCheckCategoryStandard Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteQualityCheckCategoryStandard(AID, parameterID, categoryID);
                    base.WriteExecuteAudit("deleteQualityCheckCategoryStandard Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteQualityCheckCategoryStandard Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public BIZNET.iERPMan.QualityCheckItemStandard[] getItemStandards(int parmeterID)
        {
            return _bde.getItemStandards(parmeterID);
        }

        public BIZNET.iERPMan.QualityCheckItemStandard getQualityCheckItemStandard(int parameterID, string itemCode)
        {
            return _bde.getQualityCheckItemStandard(parameterID, itemCode);
        }

        public void setQualityCheckItemStandard(BIZNET.iERPMan.QualityCheckItemStandard standard)
        {
            checkProductionConfigurationPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("setQualityCheckItemStandard Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.setQualityCheckItemStandard(AID, standard);
                    base.WriteExecuteAudit("setQualityCheckItemStandard Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("setQualityCheckItemStandard Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void deleteQualityCheckItemStandard(int parameterID, string itemCode)
        {
            checkProductionConfigurationPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteQualityCheckItemStandard Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteQualityCheckItemStandard(AID, parameterID, itemCode);
                    base.WriteExecuteAudit("deleteQualityCheckItemStandard Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteQualityCheckItemStandard Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public BIZNET.iERPMan.QualityCheckParameter getDynamicStandardForItem(int parameterID, string itemCode)
        {
            return _bde.getDynamicStandardForItem(parameterID, itemCode);
        }

        public BIZNET.iERPMan.QualityCheckParameter getDynamicStandardForItem(int parameterID, string itemCode, out object scopeObject)
        {
            return _bde.getDynamicStandardForItem(parameterID, itemCode, out scopeObject);
        }

        public object[] GetSystemParameters(string[] fields)
        {
            return _bde.GetSystemParameters(fields);
        }

        public void SetSystemParameters(string[] fields, object[] vals)
        {
            checkProductionConfigurationPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("SetSystemParameters Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.SetSystemParameters(AID, fields, vals);
                    base.WriteExecuteAudit("SetSystemParameters Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("SetSystemParameters Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void deleteProcessType(int processTypeID)
        {
            checkProductionConfigurationPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteProcessType Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteProcessType(AID, processTypeID);
                    base.WriteExecuteAudit("deleteProcessType Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteProcessType Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public BIZNET.iERPMan.CostingPeriod getCostingPeriod(int level, long t)
        {
            return _bde.getCostingPeriod(level, t);
        }

        public BIZNET.iERPMan.CostingPeriod caculateCostingPeriond(int level, long t)
        {
            return _bde.caculateCostingPeriond(level, t);
        }

        public BIZNET.iERPMan.CostingPeriod getLastCostingPeriod(int level, long before)
        {
            return _bde.getLastCostingPeriod(level, before);
        }

        public BIZNET.iERPMan.CostingPeriod[] getOpenCostingPeriods(int level, long from, long to)
        {
            return _bde.getOpenCostingPeriods(level, from, to);
        }

        public BIZNET.iERPMan.CostingPeriod[] getCostingPeriods(int level, long from, long to, int index, int pageSize, out int N)
        {
            return _bde.getCostingPeriods(level, from, to, index, pageSize, out N);
        }

        public BIZNET.iERPMan.CostingPeriod openCostingPeriod(int level, long time)
        {
            checkOpenPeriodManagmentPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("openCostingPeriod Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    BIZNET.iERPMan.CostingPeriod ret = _bde.openCostingPeriod(AID, level, time);
                    base.WriteExecuteAudit("openCostingPeriod Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("openCostingPeriod Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }


        public void closeCostingPeriod(int level, long time)
        {
            checkClosePeriodManagmentPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("closeCosting Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.closeCostingPeriod(AID, level, time);
                    base.WriteExecuteAudit("closeCosting Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("closeCosting Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public BIZNET.iERPMan.BatchCostItem[] getBatchCostItems(int batchID)
        {
            return _bde.getBatchCostItems(batchID);
        }

        public BIZNET.iERPMan.CostingSetting getCostSetting(string tag)
        {
            return _bde.getCostSetting(tag);
        }

        public void setCostSetting(BIZNET.iERPMan.CostingSetting setting)
        {
            checkEstimationConfigurationPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("setCostSetting Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.setCostSetting(AID, setting);
                    base.WriteExecuteAudit("setCostSetting Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("setCostSetting Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void deleteCostSetting(string tag)
        {
            checkEstimationConfigurationPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteCostSetting Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteCostSetting(AID, tag);
                    base.WriteExecuteAudit("deleteCostSetting Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteCostSetting Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void saveWorkData(Data_Absence[] absenceData, Data_OverTime[] overTimeData)
        {
            checkTimeSheetPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("saveWorkData Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.saveWorkData(AID,absenceData,overTimeData);
                    base.WriteExecuteAudit("saveWorkData Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("saveWorkData Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

      
        public BIZNET.iERPMan.CostingSetting[] getAllCostingSettings()
        {
            return _bde.getAllCostingSettings();
        }
        public Data_OverTime[] getOverTimeWorkData(DateTime date)
        {
            return _bde.getOverTimeWorkData(date);
        }
        public Data_Absence[] getAbsenceWorkData(DateTime date)
        {
            return _bde.getAbsenceWorkData(date);
        }
        public WorkDataResult[] getWorkData(int month, int year)
        {
            return _bde.getWorkData(month, year);
        }
        public string getProductionTable(DateTime date1, DateTime date2)
        {
            return _bde.getProductionTable(date1, date2);
        }

        public string getCreditSettlementTable(DateTime date1, DateTime date2)
        {
            return _bde.getCreditSettlementTable(date1, date2);
        }
        public string getInventoryTable(int storeID, DateTime date)
        {
            return _bde.getInventoryTable(storeID, date);
        }
        internal string getBelowAboveReorderTable(DateTime date)
        {
            return _bde.getBelowAboveReorderTable(date);
        }

        internal string getBelowNormalLevelStock(DateTime date)
        {
            return _bde.getBelowNormalLevelStock(date);
        }

        internal string getAboveNormalLevelStock(DateTime date)
        {
            return _bde.getAboveNormalLevelStock(date);
        }

        public BIZNET.iERPMan.AdjustedCostHeader getActualCostHeader(long ticksFrom)
        {
            return _bde.getActualCostHeader(ticksFrom);
        }

        public BIZNET.iERPMan.AdjustedCostDetail[] getActualCostDetail(long ticksFrom)
        {
            return _bde.getActualCostDetail(ticksFrom);
        }

        public void setActualCost(BIZNET.iERPMan.AdjustedCostHeader header, BIZNET.iERPMan.AdjustedCostDetail[] detail)
        {
            checkCostAdjustmentPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("setActualCost Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.setActualCost(AID, header, detail);
                    base.WriteExecuteAudit("setActualCost Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("setActualCost Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public BIZNET.iERPMan.AdjustedCostHeader[] getActualCosts(long ticksFrom, long ticksTo)
        {
            return _bde.getActualCosts(ticksFrom, ticksTo);
        }

        public string renderAdjustedCostReport(long ticksFrom, long ticksTo)
        {
            return _bde.renderAdjustedCostReport(ticksFrom, ticksTo);
        }

        public BIZNET.iERP.SummaryInformation getCostReportingScheme()
        {
            return _bde.getCostReportingScheme();
        }

        public void deleteActualCost(long ticksFrom)
        {
            checkCostAdjustmentPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteActualCost Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteActualCost(AID, ticksFrom);
                    base.WriteExecuteAudit("deleteActualCost Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteActualCost Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        internal ProductionBatchHeader getProductionBatchHeader(string batchNo)
        {
            return _bde.getProductionBatchHeader(batchNo);
        }

        internal double[][][] getSalesSeries(DateRange[] dateRange, out string[] items)
        {
            return _bde.getSalesSeries(dateRange, out items);
        }

        internal double[][] getCustSalesReport(DateTime date1, DateTime date2, out iERP.TradeRelation[] cust, out iERP.TransactionItems[] custItems, out double[] credit)
        {
            return _bde.getCustSalesReport(date1, date2, out cust, out custItems, out credit);
        }
    }
}
