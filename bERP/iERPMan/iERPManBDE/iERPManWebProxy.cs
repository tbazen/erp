﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using BIZNET.iERP;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using BIZNET.iERP.Server;
using RazorEngine;
using RazorEngine.Templating;
using INTAPS.ClientServer.RemottingServer;

namespace BIZNET.iERPMan.Server
{
    public class SalesReportData
    {
        public double productionAmount;
        public double productionQuantity;
        public double costOfProductSold;
        public string unit;
        public class TrendChart
        {
            public string chartTitle;
            public string[] labels;
            public double[] values;
        }
        public TrendChart trendChart;

        public class DistChart
        {
            public string chartTitle;
            public string[] labels;
            public string[] untis;
            public string[] colors;
            public double[] values;
            public double[] quantity;

        }
        public DistChart distChart;
        public class CustData
        {
            public string label;
            public string color;
            public double totalValue;
            public double creditValue;
            public double[] productValue;
        }
        public class CustDistChart
        {
            public string chartTitle;
            public string[] products;

            //public string[] labels;
            //public string[] colors;
            //public double[] totalValues;
            //public double[][] productValues;
            //public double[] creditValues;
            public CustData[] custData;

        }
        public CustDistChart custDistChart;

    }

    [INTAPS.ClientServer.RemottingServer.HttpPageHandler(page = "webproxy.ph")]
    class iERPManWebProxy : INTAPS.ClientServer.RemottingServer.WebServiceProxyBase<iERPManService>
    {

        [WebProxyMethod]
        public string testMethod2(iERPManService service, double a)
        {
            //throw new Exception("Test Exception");
            return "Test Method. Parametrer:" + a;
        }
        [WebProxyMethod]
        public string getProductionTable(iERPManService service, string d1, string d2,bool adj)
        {
            DateTime date1 = DateTime.Parse(d1);
            DateTime date2 = DateTime.Parse(d2).AddDays(1);

            if(adj)
                return service.renderAdjustedCostReport(date1.Ticks, date2.AddDays(-1).Ticks);
            else
                return service.getProductionTable(date1, date2);

        }
        [WebProxyMethod]
        public string getCreditSettlementTable(iERPManService service, string d1, string d2)
        {
            DateTime date1 = DateTime.Parse(d1);
            DateTime date2 = DateTime.Parse(d2).AddDays(1);

            return service.getCreditSettlementTable(date1, date2);

        }

        [WebProxyMethod]
        public string getInventoryTable(iERPManService service, string storeID)
        {
            return service.getInventoryTable(int.Parse(storeID), DateTime.Now);
        }
        [WebProxyMethod]
        public string getBelowAboveReorderTable(iERPManService service)
        {
            return service.getBelowAboveReorderTable(DateTime.Now);
        }
        [WebProxyMethod]
        public string getBelowNormalLevelStock(iERPManService service)
        {
            return service.getBelowNormalLevelStock(DateTime.Now);
        }
        [WebProxyMethod]
        public string getAboveNormalLevelStock(iERPManService service)
        {
            return service.getAboveNormalLevelStock(DateTime.Now);
        }
        iERPTransactionService getIERPService(string sessionID)
        {
            return ApplicationServer.GetSessionObject(sessionID, "iERPService") as iERPTransactionService; 
        }
        INTAPS.Accounting.Service.AccountingService getAccountingService(string sessionID)
        {
            return ApplicationServer.GetSessionObject(sessionID, "AccountingService") as INTAPS.Accounting.Service.AccountingService;
        }
        [WebProxyMethod]
        public string getMaterialDetail(iERPManService service, string itemCode)
        {
            iERPTransactionService erpService = getIERPService(service.sessionID);
            INTAPS.Accounting.Service.AccountingService acService = getAccountingService(service.sessionID);
            BIZNET.iERP.TransactionItems item = erpService.GetTransactionItems(itemCode);

            if (item == null || !item.IsInventoryItem)
            {
                int N;
                BIZNET.iERP.TransactionItems[] items= erpService.SearchTransactionItems(0, 100, new object[] { itemCode }, new string[] { "Code" }, out N);
                if (items.Length == 0)
                {
                    items = erpService.SearchTransactionItems(0, 100, new object[] { itemCode }, new string[] { "Name" }, out N);
                }
                else if(items.Length==0)
                    throw new INTAPS.ClientServer.ServerUserMessage("Item code {0} is not correct.", itemCode);
                if (items.Length > 1)
                {
                    return renderMaterialSearchResult(items);
                }
                else
                    item = items[0];
            }
            string munit = erpService.GetMeasureUnit(item.MeasureUnitID).Name;
            
            double quantity = 0;
            
            double totalPrice = 0;
            int nStore = 0;
            List<object> stores = new List<object>();
            foreach (StoreInfo s in erpService.GetAllStores())
            {
                CostCenterAccount csa = acService.GetCostCenterAccount(s.costCenterID, item.materialAssetAccountID(true));
                if (csa == null)
                    continue;
                AccountBalance qbal = acService.GetBalanceAsOf(csa.id, 1, DateTime.Now);
                double q = qbal.DebitBalance;
                if (qbal.IsZero)
                    continue;
                double p = acService.GetNetBalanceAsOf(csa.id, 0, DateTime.Now);
                stores.Add(
                    new
                    {
                        storeID=s.costCenterID,
                        store=s.description,
                        q=q.ToString("0.00")+" "+munit,
                        p=AccountBase.FormatAmount(p),
                        up=AccountBase.FormatUnitPrice(q,p),
                    }
                    );
                

                quantity += q;
                totalPrice += p;
                nStore++;
            }
            //if (nStore == 0)
            //    return "You don't have item " + item.NameCode + " in stock";
            var model = new
            {
                itemName=item.Name+" ("+item.Code+")",
                itemCode=itemCode,
                items = stores,
                totalQ = quantity.ToString("0.00") + " " + munit,
                totalP = AccountBase.FormatAmount(totalPrice),
                totalUp = AccountBase.FormatUnitPrice(quantity,totalPrice),
            };

            string template = System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["html_root"] + "\\iERPManService\\query_result.cshtml");
            string templateKey = template.GetHashCode().ToString();
            string html = RazorEngine.Engine.Razor.RunCompile(template, templateKey, null, model);
            return html;

//            Dictionary<string, object> vdata = new Dictionary<string, object>();
//            vdata.Add("model", model);
//            StringBuilder sb = new StringBuilder();
//            INTAPS.ClientServer.RemottingServer.TemplateEngineWrapper.evaluateTemplate(sb, "iERPManService\\query_result.html", vdata);
//            return sb.ToString();

        }

        private string renderMaterialSearchResult(BIZNET.iERP.TransactionItems[] items)
        {
            List<object> renderItems = new List<object>();
            foreach (BIZNET.iERP.TransactionItems i in items)
            {
                renderItems.Add(new { itemCode = i.Code, itemName = i.Name });
            }
            var model = new
            {
                items = renderItems,
            };

            string template = System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["html_root"] + "\\iERPManService\\mat_search_result.cshtml");
            string templateKey = template.GetHashCode().ToString();
            string html = RazorEngine.Engine.Razor.RunCompile(template, templateKey, null, model);
            return html;

//            Dictionary<string, object> vdata = new Dictionary<string, object>();
//            vdata.Add("model", model);
//            StringBuilder sb = new StringBuilder();
//            INTAPS.ClientServer.RemottingServer.TemplateEngineWrapper.evaluateTemplate(sb, "iERPManService\\mat_search_result.html", vdata);
//            return sb.ToString();
        }
        [WebProxyMethod]
        public string getStockCard(iERPManService service, string code, int storeID)
        {
            iERPTransactionService erpservice = INTAPS.ClientServer.ApplicationServer.GetSessionObject(service.sessionID, "iERPService") as iERPTransactionService;
            if (storeID == -1)
                storeID = 1;
            try
            {
                TransactionItems titem = erpservice.GetTransactionItems(code);
                if (titem == null)
                    throw new ServerUserMessage("Invalid item code:{0}", code);

                var model = new
                {
                    stockCard = erpservice.renderLedgerTable(new LedgerParameters()
                    {
                        costCenterID = storeID,
                        accountIDs = new int[] { titem.inventoryType == InventoryType.GeneralInventory ? titem.inventoryAccountID : titem.finishedGoodAccountID },
                        itemID = TransactionItem.MATERIAL_QUANTITY,
                        time1=new DateTime(1900,1,1),
                        time2=DateTime.Now,
                        remarkDetail=LedgerParameters.LedgerRemarkDetail.Concise,
                        qauntityOnly = true,
                    }),
                    date_ranges = BIZNET.iERPMan.Server.pagehandlers.HtmlExtract.getDateRanges(),
                };

                string template = System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["html_root"] + "\\iERPManService\\stock_card.cshtml");
                string templateKey = template.GetHashCode().ToString();
                string html = RazorEngine.Engine.Razor.RunCompile(template, templateKey, null, model);
                return html;

//                Dictionary<string, object> vdata = new Dictionary<string, object>();
//                vdata.Add("model", model);
//                StringBuilder sb = new StringBuilder();
//                INTAPS.ClientServer.RemottingServer.TemplateEngineWrapper.evaluateTemplate(sb, "iERPManService\\stock_card.html", vdata);
//                return sb.ToString();
            }
            catch (Exception ex)
            {
                return System.Web.HttpUtility.HtmlEncode(ex.Message);
            }
        }
        [WebProxyMethod]
        public string getBatchDetail(iERPManService service, string batchNo)
        {
            ProductionBatchHeader header = service.getProductionBatchHeader(batchNo);
            return "";
        }
        static System.Drawing.Color[] lengedColors = new System.Drawing.Color[]{
            System.Drawing.Color.Green,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Red,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.Black,
            System.Drawing.Color.Violet,
            System.Drawing.Color.Orange,
            System.Drawing.Color.Cyan,
        };
        static System.Drawing.Color calculateLedendColor(int n, int i)
        {
            return lengedColors[i % lengedColors.Length];
        }
        [WebProxyMethod]
        public SalesReportData getSalesData(iERPManService service, string d1, string d2, string trendUnit, bool amount)
        {
            iERPTransactionService erpService = getIERPService(service.sessionID);
            DateTime date1 = DateTime.Parse(d1);
            DateTime date2 = DateTime.Parse(d2).AddDays(1);
            List<DateRange> ranges = new List<DateRange>();
            List<string> rangeLabels = new List<string>();
            DateTime date = date1;

            while (date < date2)
            {
                string label;
                DateTime nextDate;
                switch (trendUnit)
                {
                    case "dy":
                        nextDate = date.AddDays(1);
                        label = date.ToString("MMM dd,yyy");
                        break;
                    case "wk":
                        nextDate = date.AddDays(7 - (int)date.DayOfWeek);
                        label = "Week " + (ranges.Count + 1);
                        break;
                    default://case "mn":
                        if (date.Month < 12)
                            nextDate = new DateTime(date.Year, date.Month + 1, 1);
                        else
                            nextDate = new DateTime(date.Year + 1, 1, 1);
                        label = date.ToString("MMM yyy");
                        break;
                }
                if (nextDate > date2)
                    nextDate = date2;
                ranges.Add(new DateRange() { t1 = date, t2 = nextDate });
                rangeLabels.Add(label);
                date = nextDate;
            }
            string[] items;
            double[][][] values = service.getSalesSeries(ranges.ToArray(), out items);

            SalesReportData ret = new SalesReportData()
            {
                productionAmount = 0,
                productionQuantity = 0,
                costOfProductSold = 0,
            };
            ret.distChart = new SalesReportData.DistChart()
            {
                chartTitle = "Product Share",
                labels = new string[items.Length],
                untis = new string[items.Length],
                colors = new string[items.Length],
                values = new double[items.Length],
                quantity = new double[items.Length],
            };
            int unitID = -1;
            for (int i = 0; i < items.Length; i++)
            {
                TransactionItems titem = erpService.GetTransactionItems(items[i]);

                ret.distChart.labels[i] = titem.NameCode;
                double totalSales = 0;
                double totalQuantity = 0;
                foreach (double[] val in values[i])
                {
                    totalSales += val[amount ? 0 : 1];
                    totalQuantity += val[1];
                    ret.productionAmount += val[0];
                    if (unitID == -1)
                    {
                        unitID = titem.MeasureUnitID;
                        ret.productionQuantity += val[1];
                    }
                    else
                        try
                        {
                            ret.productionQuantity += erpService.convertUnit(titem.Code, val[1], titem.MeasureUnitID, unitID);
                        }
                        catch (Exception ex)
                        {
                            throw new ServerUserMessage("Problem with item {0}\n{1}", titem.Code, ex.Message);
                        }
                    ret.productionQuantity = Math.Round(ret.productionQuantity, 2);
                    ret.costOfProductSold += val[2];
                }
                ret.distChart.values[i] = totalSales;
                ret.distChart.quantity[i] = totalQuantity;
                ret.distChart.untis[i] = erpService.GetMeasureUnit(titem.MeasureUnitID).Name;
                System.Drawing.Color color = calculateLedendColor(items.Length, i);
                ret.distChart.colors[i] = "#" + color.R.ToString("x2") + color.G.ToString("x2") + color.B.ToString("x2");

            }
            ret.unit = unitID == -1 ? "" : erpService.GetMeasureUnit(unitID).Name;
            ret.trendChart = new SalesReportData.TrendChart()
            {
                chartTitle = "Sales Trend",
                labels = rangeLabels.ToArray(),
                values = new double[ranges.Count],
            };

            for (int i = 0; i < ret.trendChart.values.Length; i++)
            {
                double rangeTotal = 0;
                for (int j = 0; j < items.Length; j++)
                    rangeTotal += values[j][i][amount ? 0 : 1];
                ret.trendChart.values[i] = rangeTotal;

            }
            TransactionItems[] custItems;
            TradeRelation[] cust;
            double[] credit;
            double[][] productValues = service.getCustSalesReport(date1, date2, out cust, out custItems, out credit);

            ret.custDistChart = new SalesReportData.CustDistChart()
            {
                chartTitle = "Customer Sales Distribution",
                custData=new SalesReportData.CustData[cust.Length],
                products = new string[custItems.Length],
            };
            for (int i = 0; i < cust.Length; i++)
            {
                ret.custDistChart.custData[i] = new SalesReportData.CustData();
                System.Drawing.Color color = calculateLedendColor(cust.Length, i);
                ret.custDistChart.custData[i].color = "#" + color.R.ToString("x2") + color.G.ToString("x2") + color.B.ToString("x2");
                ret.custDistChart.custData[i].label = cust[i].Name;
                ret.custDistChart.custData[i].totalValue = 0;
                ret.custDistChart.custData[i].productValue = productValues[i];
                ret.custDistChart.custData[i].creditValue = credit[i];
                foreach (double v in ret.custDistChart.custData[i].productValue)
                    ret.custDistChart.custData[i].totalValue += v;
            }
            int[] k = new int[cust.Length];
            for (int i = 0; i < cust.Length; i++)
                k[i] = i;
            Array.Sort(ret.custDistChart.custData, delegate(BIZNET.iERPMan.Server.SalesReportData.CustData x, BIZNET.iERPMan.Server.SalesReportData.CustData y) { return y.totalValue.CompareTo(x.totalValue); });
            for (int i = 0; i < custItems.Length; i++)
            {
                ret.custDistChart.products[i] = custItems[i].NameCode;
            }

            return ret;
        }

        public class BatchSearchResult
        {
            public string resultTable;
            public string navigationPane;
        }
        [WebProxyMethod]
        public BatchSearchResult searchBatch(iERPManService service,int pageIndex, bool byDate, string d1,string d2,string product)
        {
            return new BatchSearchResult()
            {
                resultTable = "<table><tr><td>That is</td><td>It</td></tr><tr><td>30</td><td>40</td></tr></table>",
                navigationPane="1,2"
            };
        }
    }
}
