using System;
using System.Collections.Generic;
using System.Text;
using BIZNET.iERP.Server;
using INTAPS.ClientServer.RemottingServer;
using RazorEngine;
using RazorEngine.Templating;

namespace BIZNET.iERPMan.Server.pagehandlers
{
    [Serializable]
    [HttpPageHandler(page = "creditSettlement.html")]
    public class CreditSettlementPage : HttpPageHandlerBase, IHttpPageHandler
    {
        public override bool handleSessionGet(System.Net.HttpListenerContext context, object sessionObject, QueryDictionary queries)
        {

            iERPManService service = sessionObject as iERPManService;
            iERPTransactionService erpservice = INTAPS.ClientServer.ApplicationServer.GetSessionObject(service.sessionID, "iERPService") as iERPTransactionService;
            long ticksFrom = -1, ticksTo = -1;
            bool adjMode = false;
            if (queries.ContainsKey("ticksFrom"))
            {
                ticksFrom = long.Parse(queries["ticksFrom"]);
                ticksTo = long.Parse(queries["ticksTo"]);
                adjMode = "true".Equals(queries["adj"], StringComparison.CurrentCultureIgnoreCase);
            }
            object model;
            try
            {

                model = new
                {
                    sid = service.sessionID,
                    header = HtmlExtract.getHeaderHtml(service.sessionID, "hidden"),
                    date_ranges = HtmlExtract.getDateRanges(),
                    searchPanel = ticksFrom == -1 ? "visible" : "hidden",
                    fromDate = (ticksFrom == -1 ? DateTime.Now : new DateTime(ticksFrom)).ToString(),
                    toDate = (ticksTo == -1 ? DateTime.Now : new DateTime(ticksTo)).ToString(),
                    adj = adjMode
                };
            }
            catch (Exception ex)
            {
                model = new
                {
                    error=ex.Message
                };
            }

            string template = System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["html_root"] + "\\iERPManService\\creditSettlement.cshtml");
            string templateKey = template.GetHashCode().ToString();
            string html = RazorEngine.Engine.Razor.RunCompile(template, templateKey, null, model);
            INTAPS.ClientServer.RemottingServer.BDEWebServer.writeHtmlReponse(context.Response, html);
            return true;
        }
    }
}