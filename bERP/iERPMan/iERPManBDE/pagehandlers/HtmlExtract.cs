﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RazorEngine;
using RazorEngine.Templating;

namespace BIZNET.iERPMan.Server.pagehandlers
{
    class HtmlExtract
    {
        public static string getHeaderHtml(object sid,
            string logoutVisibility= "visible"
            )
        {
            string un=null;
            if (logoutVisibility != null && logoutVisibility.Equals("visible"))
                un = INTAPS.ClientServer.ApplicationServer.getSessionInfo(sid.ToString()).UserName;
            var model = new {
                sid=sid, 
                date=DateTime.Now.ToString("MM/dd/yy"),
                vis_logout = logoutVisibility, 
                userName = un,
            };

            string template = System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["html_root"] + "\\iERPManService\\header.cshtml");
            string templateKey = template.GetHashCode().ToString();
            string html = RazorEngine.Engine.Razor.RunCompile(template, templateKey, null, model);
            return html;
        }
        public static string formatDate(DateTime d)
        {
            return d.ToString("MMM dd,yyy");
        }
        public static List<object> getDateRanges()
        {
            List<object> dateRanges = new List<object>();
            DateTime today = DateTime.Today;
            //this week
            dateRanges.Add(new
            {
                name = "tw",
                d1 = formatDate(today.AddDays(-(int)today.DayOfWeek)),
                d2 = formatDate(today.AddDays(6 - (int)today.DayOfWeek)),
                desc = "This Week",
            });

            //last week
            dateRanges.Add(new
            {
                name = "lw",
                d1 = formatDate(today.AddDays(-(int)today.DayOfWeek - 7)),
                d2 = formatDate(today.AddDays(-(int)today.DayOfWeek-1)),
                desc = "Last Week",
            });

            //today
            dateRanges.Add(new
            {
                name = "td",
                d1 = formatDate(today),
                d2 = formatDate(today),
                desc = "Today",
            });

            //yesterday
            dateRanges.Add(new
            {
                name = "yd",
                d1 = formatDate(today.AddDays(-1)),
                d2 = formatDate(today.AddDays(-1)),
                desc = "Yesterday",
            });

            //this month
            dateRanges.Add(new
            {
                name = "tm",
                d1 = formatDate(new DateTime(today.Year, today.Month, 1)),
                d2 = formatDate((new DateTime(today.Year, today.Month, 1)).AddMonths(1).AddDays(-1)),
                desc = "This Month",
            });

            //last month
            dateRanges.Add(new
            {
                name = "lm",
                d1 = formatDate((new DateTime(today.Year, today.Month, 1)).AddMonths(-1)),
                d2 = formatDate(new DateTime(today.Year, today.Month, 1).AddDays(-1)),
                desc = "Last Month",
            });

            //This Year
            dateRanges.Add(new
            {
                name = "ty",
                d1 = formatDate(new DateTime(today.Year, 1, 1)),
                d2 = formatDate((new DateTime(today.Year, 1, 1)).AddYears(1).AddDays(-1)),
                desc = "This Year",
            });

            //last Year
            dateRanges.Add(new
            {
                name = "ly",
                d1 = formatDate((new DateTime(today.Year, 1, 1)).AddYears(-1)),
                d2 = formatDate(new DateTime(today.Year, 1, 1).AddDays(-1)),
                desc = "Last Year",
            });
            return dateRanges;
        }
    }
}
