﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.ClientServer.RemottingServer;
using RazorEngine;
using RazorEngine.Templating;

namespace BIZNET.iERPMan.Server.pagehandlers
{
    [HttpPageHandler(page = "login.html")]
    public class LoginPage :HttpPageHandlerBase,  IHttpPageHandler
    {
        public override bool handleSessionGet(System.Net.HttpListenerContext context, object sessionObject, QueryDictionary queries)
        {
            string sid = queries["sid"];
            if (!string.IsNullOrEmpty(sid))
                try
                {
                    ApplicationServer.CloseSession(sid);
                }
                catch
                {

                }

            return this.handleStaticGet(context, queries);
        }
        public override bool handleStaticGet(System.Net.HttpListenerContext context, QueryDictionary queries)
        {
            var model = new
            {
                header_page = HtmlExtract.getHeaderHtml(null, null),
                error = "",
            };

            string template = System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["html_root"] + "\\iERPManService\\login.cshtml");
            string templateKey = template.GetHashCode().ToString();
            string html = RazorEngine.Engine.Razor.RunCompile(template, templateKey, null, model);
            INTAPS.ClientServer.RemottingServer.BDEWebServer.writeHtmlReponse(context.Response, html);
            return true;
        }
        public override void handlePostForm(object sessionObject,System.Net.HttpListenerContext context, FormDataDictionary formData)
        {
            try
            {
                string userName = formData["username"].textValue;
                string password = formData["password"].textValue;
                string sessionID = ApplicationServer.CreateUserSession(userName, password, "web");
                ApplicationServer.GetSessionObject(sessionID, ApplicationServer.getSessionShortNameByType(typeof(iERPManService)));
                ApplicationServer.GetSessionObject(sessionID, ApplicationServer.getSessionShortNameByType(typeof(BIZNET.iERP.Server.iERPTransactionService)));
                context.Response.Redirect("material.html?sid=" + sessionID);
                context.Response.Close();
            }
            catch(Exception ex)
            {
                var model = new
                {
                    header_page = HtmlExtract.getHeaderHtml(null, null),
                    error = ex.Message,
                };

                string template = System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["html_root"] + "\\iERPManService\\login.cshtml");
                string templateKey = template.GetHashCode().ToString();
                string html = RazorEngine.Engine.Razor.RunCompile(template, templateKey, null, model);
                INTAPS.ClientServer.RemottingServer.BDEWebServer.writeHtmlReponse(context.Response, html);

//                Dictionary<string, object> vdata = new Dictionary<string, object>();
//                vdata.Add("model", model);
//                StringBuilder sb = new StringBuilder();
//                TemplateEngineWrapper.evaluateTemplate(sb, "ierpmanservice\\login.html", vdata);
//                INTAPS.ClientServer.RemottingServer.BDEWebServer.writeHtmlReponse(context.Response, sb.ToString());
            }
        }
    }
}
