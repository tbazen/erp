﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIZNET.iERP;
using BIZNET.iERP.Server;
using INTAPS.ClientServer.RemottingServer;
using RazorEngine;
using RazorEngine.Templating;

namespace BIZNET.iERPMan.Server.pagehandlers
{
    [Serializable]
    [INTAPS.ClientServer.RemottingServer.HttpPageHandler(page = "query.html")]
    public class QueryPage : HttpPageHandlerBase, IHttpPageHandler
    {
        public override bool handleSessionGet(System.Net.HttpListenerContext context, object sessionObject, QueryDictionary queries)
        {

            iERPManService service = sessionObject as iERPManService;
            iERPTransactionService erpservice = INTAPS.ClientServer.ApplicationServer.GetSessionObject(service.sessionID, "iERPService") as iERPTransactionService;
            object model;

            List<object> stores = new List<object>();
            foreach (StoreInfo s in erpservice.GetAllStores())
            {
                stores.Add(new
                {
                    id = s.costCenterID,
                    name = s.description,
                });
            }
            
            model = new
            {
                sid = service.sessionID,
                header = HtmlExtract.getHeaderHtml(service.sessionID, "hidden"),
                date_ranges = HtmlExtract.getDateRanges(),
                stores = stores,
            };

            string template = System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["html_root"] + "\\iERPManService\\query.cshtml");
            string templateKey = template.GetHashCode().ToString();
            string html = RazorEngine.Engine.Razor.RunCompile(template, templateKey, null, model);
            INTAPS.ClientServer.RemottingServer.BDEWebServer.writeHtmlReponse(context.Response, html);
            return true;
        }
    }
}
