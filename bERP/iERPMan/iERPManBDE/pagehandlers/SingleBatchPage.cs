﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIZNET.iERP;
using BIZNET.iERP.Server;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.ClientServer.RemottingServer;
using RazorEngine;
using RazorEngine.Templating;

namespace BIZNET.iERPMan.Server.pagehandlers
{
    [Serializable]
    [INTAPS.ClientServer.RemottingServer.HttpPageHandler(page = "single_batch.html")]
    public class SingleBatchPage:HttpPageHandlerBase, IHttpPageHandler
    {
        public override bool handleSessionGet(System.Net.HttpListenerContext context, object sessionObject, QueryDictionary queries)
        {
            
            iERPManService service = sessionObject as iERPManService;
            iERPTransactionService erpservice = INTAPS.ClientServer.ApplicationServer.GetSessionObject(service.sessionID, "iERPService") as iERPTransactionService;
            object model;
            try
            {
                int batchID = int.Parse(queries["batchID"] as string);
                ProductionBatch batch=service.getProductionBatch(batchID);
                CostingPeriod p=service.getCostingPeriod(0, batch.header.ticksCreated);
                if (!p.closed)
                    p = null;
                BIZNET.iERP.TransactionItems prodItem = erpservice.GetTransactionItems(batch.header.itemCode);
                
                
                List<TransactionDocumentItem> materials = new List<TransactionDocumentItem>();
                foreach(ProductionBatchProcessData pd in batch.processData)
                {
                    if (pd.issuedMaterialDocumentID != -1)
                    {
                        StoreIssueDocument issue = service.bde.accountingBDE.GetAccountDocument<StoreIssueDocument>(pd.issuedMaterialDocumentID);
                        foreach (TransactionDocumentItem issueditem in issue.items)
                        {
                            bool found = true;

                            foreach (TransactionDocumentItem m in materials)
                            {
                                if (m.code.Equals(issueditem.code))
                                {
                                    found = true;
                                    m.quantity += issueditem.quantity;
                                    m.price += issueditem.price;
                                    if (INTAPS.Accounting.AccountBase.AmountGreater(m.price * m.quantity, 0))
                                        m.unitPrice = m.price / m.quantity;
                                    else
                                        m.unitPrice = 0;
                                    break;
                                }
                            }
                            if (found)
                                materials.Add(issueditem);
                        }
                    }
                }
                List<object> materialCosts = new List<object>();
                int no = 1;
                double totalMatCost = 0;
                foreach (TransactionDocumentItem item in materials)
                {
                    TransactionItems titem=erpservice.GetTransactionItems(item.code);
                    materialCosts.Add(new
                    {
                        no = (no++).ToString(),
                        code = item.code,
                        desc =titem.Name,
                        quantity = item.quantity.ToString("0.00"),
                        unit=erpservice.GetMeasureUnit(titem.MeasureUnitID).Name,
                        unitCost=item.unitPrice.ToString("#,#0.00"),
                        cost = item.price.ToString("#,#0.00"),
                    });
                    totalMatCost += item.price;
                }
                BatchCostItem[] costItems = service.getBatchCostItems(batchID);
                double totalProcessCost = 0;
                List<object> procCost = new List<object>();
                List<string> procCostTypes = new List<string>();
                SummaryInformation sum = service.bde.getOtherCostReportingScheme();
                if (p != null)
                {

                    Dictionary<int, Dictionary<string, CostItem>> procCostData = BatchCostItem.groupByProcessType(costItems,
                        delegate(BatchCostItem x)
                        {
                            bool ret = sum.getItemByTag(x.tag) != null;
                            if (ret)
                            {
                                if (!procCostTypes.Contains(x.tag)) procCostTypes.Add(x.tag);
                            }
                            return ret;
                        }
                        );
                    
                    foreach (KeyValuePair<int, Dictionary<string, CostItem>> oneProcCost in procCostData)
                    {
                        ProductionBatchProcessData proc = batch.GetProcessDataByProcessType(oneProcCost.Key);
                        string procName = service.getProcessType(oneProcCost.Key).description;
                        object[] row = new object[procCostTypes.Count];
                        double cost = 0;
                        foreach (KeyValuePair<string, CostItem> kv in oneProcCost.Value)
                        {
                            int index = procCostTypes.IndexOf(kv.Key);
                            row[index] = kv.Value.amount.ToString("#,#0.00");
                            cost += kv.Value.amount;
                        }
                        totalProcessCost += cost;
                        procCost.Add(new
                        {
                            name = procName,
                            hours = new DateTime(proc.endTicks).Subtract(new DateTime(proc.startTicks)).TotalHours.ToString("0"),
                            costs = row,
                            cost = cost.ToString("#,#0.00"),
                        });
                    }
                    for (int i = 0; i < procCostTypes.Count; i++)
                    {
                        SummaryItemBase si = sum.getItemByTag(procCostTypes[i]);
                        if (si != null)
                            procCostTypes[i] = si.label;
                    }
                }
                List<object> costSummary = new List<object>();

                double totalCost = 0;
                
                if (p != null)
                {

                    BIZNET.iERP.SummaryInformation info = service.bde.getCostReportingScheme();

                    foreach (KeyValuePair<string,double> kv in BatchCostItem.groupByTag(costItems,null))
                    {
                        string itemName;
                        BIZNET.iERP.SummaryItemBase itemInfo = info.getItemByTag(kv.Key);
                        if (itemInfo == null)
                            itemName = kv.Key;
                        else
                            itemName = itemInfo.label;

                        costSummary.Add(new { desc = itemName, cost = kv.Value.ToString("#,#0.00") });
                        totalCost += kv.Value;
                    }

                }

                QualitySummary qc = service.bde.getQualityIndex(batch);
                List<object> qcData = new List<object>();
                for (int i = 0; i < qc.measuredValues.Length;i++)
                {
                    qcData.Add(new
                    {
                        no=(i+1).ToString(),
                        desc=qc.parmeters[i].name,
                        standard = qc.parmeters[i].standardValue.ToString("0.00"),
                        actual=qc.measuredValues[i].ToString("0.00"),
                        deviation = (qc.measuredValues[i] - qc.parmeters[i].standardValue).ToString("0.00"),
                    });
                }

                    model = new
                    {
                        header=HtmlExtract.getHeaderHtml(service.sessionID,"hidden"),
                        batchNo = batch.header.batchNo,
                        batchDate = batch.header.formatedDate,
                        productName = prodItem.Name,
                        productCode = prodItem.Code,
                        batchVolume = batch.header.actualQuantity + " " + erpservice.GetMeasureUnit(prodItem.MeasureUnitID).Name,
                        batchType = service.getBatchType(batch.header.batchTypeID).name,
                        totalCost = (p == null ? "Not avialable" : totalCost.ToString("#,#0.00")),
                        costSum = p == null ? null : costSummary,
                        totalMatCost = totalMatCost.ToString("#,#0.00"),
                        materialCosts = materialCosts,
                        totalProcessCost = p == null ? "Not avialable" : totalProcessCost.ToString("#,#0.00"),
                        procCostTypes = procCostTypes,
                        procCosts = procCost,
                        qcData=qcData,
                        qualityIndex=qc.qualityIndex>=0?qc.qualityIndex.ToString("0.00"):"",
                        nProcCostTypes=procCostTypes.Count,
                    };

                    string template = System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["html_root"] + "\\iERPManService\\single_batch.cshtml");
                    string templateKey = template.GetHashCode().ToString();
                    string html = RazorEngine.Engine.Razor.RunCompile(template, templateKey, null, model);
                    INTAPS.ClientServer.RemottingServer.BDEWebServer.writeHtmlReponse(context.Response, html);
                    return true;
            }
            catch(Exception ex)
            {
                model = new
                {
                    errorType=ex.GetType().ToString(),
                    errorText=ex.Message,
                    stackTrace=ex.StackTrace
                };
                string template = System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["html_root"] + "\\iERPManService\\error_page.cshtml");
                string templateKey = template.GetHashCode().ToString();
                string html = RazorEngine.Engine.Razor.RunCompile(template, templateKey, null, model);
                INTAPS.ClientServer.RemottingServer.BDEWebServer.writeHtmlReponse(context.Response, html);
                return true;
            }
        }
    }
}
