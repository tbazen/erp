﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERPMan.Client
{
    public partial class AddAsset : XtraForm
    {
        public ProcessMachine PM = null;
        public AddAsset()
        {
            InitializeComponent();
        }
        public AddAsset(ProcessMachine asset)
            : this()
        {
            comAsset.SetByID(asset.costCenterID);
            txtCostPerHour.Text = asset.costPerHour.ToString("#,#0.00");
            PM = asset;
        }


        private void frmAddAsset_Load(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (comAsset.Text == "")
                {
                    MessageBox.Show("Please Select Cost Cenetr", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    comAsset.Focus();
                    return;
                }
                ProcessMachine pp = new ProcessMachine();

                if (txtCostPerHour.Text == "")
                {
                    if (!double.TryParse(txtCostPerHour.Text, out pp.costPerHour))
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter valid cost per hour");
                        return;
                    }
                }
                pp.costCenterID = comAsset.GetAccountID();
                pp.costPerHour = Double.Parse(txtCostPerHour.Text);
                PM = pp;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void comAsset_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

