﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERPMan.Client
{
    public partial class CategoryStandard : XtraForm
    {
        private int EditQualityParameterID = -1;
        public CategoryStandard(int parameterID)
        {
            EditQualityParameterID = parameterID;
            InitializeComponent();
        }
        public CategoryStandard(int parameterID,int categoryID):this(parameterID)
        {

            QualityCheckCategoryStandard q = iERPManClient.getQualityCheckCategoryStandard(parameterID, categoryID);
            txtCategoryPlaceholder.SetByID(q.categoryID);
            txtStandard.Text= q.standardValue.ToString();
            txtUpperTolerance.Text=q.upperTol.ToString();
            txtLowerTolerance.Text=q.lowerTol.ToString();
            txtValidMinimum.Text= q.validMin.ToString();
            txtValidMaximum.Text=q.validMax.ToString();
            txtWeight.Text = q.weight.ToString();
            txtCategoryPlaceholder.ReadOnly = true;
        }
       

        private void CategoryStandard_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCategoryPlaceholder.GetObjectID()==-1)
                {
                    MessageBox.Show("Please enter Item Category ", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCategoryPlaceholder.Focus();
                    return;
                }
                if (txtStandard.Text == "")
                {
                    MessageBox.Show("Please enter Item Standard ", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtStandard.Focus();
                    return;
                }
                if (txtUpperTolerance.Text == "")
                {
                    MessageBox.Show("Please enter Upper Tolerance", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtUpperTolerance.Focus();
                    return;
                }
                if (txtLowerTolerance.Text == "")
                {
                    MessageBox.Show("Please enter Lower Tolerance", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtLowerTolerance.Focus();
                    return;
                }
                if (txtValidMaximum.Text == "")
                {
                    MessageBox.Show("Please enter valid maximum", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtValidMaximum.Focus();
                    return;
                }
                if (txtValidMinimum.Text == "")
                {
                    MessageBox.Show("Please enter valid Minimum", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtValidMinimum.Focus();
                    return;
                }
                if (txtWeight.Text == "")
                {
                    MessageBox.Show("Please enter Weight", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtWeight.Focus();
                    return;
                }

                QualityCheckCategoryStandard parameter = new QualityCheckCategoryStandard();
                parameter.categoryID = txtCategoryPlaceholder.GetObjectID();
                parameter.parameterID = EditQualityParameterID;
                parameter.standardValue = Double.Parse(txtStandard.Text);
                parameter.upperTol = Double.Parse(txtUpperTolerance.Text);
                parameter.lowerTol = Double.Parse(txtLowerTolerance.Text);
                parameter.validMin = Double.Parse(txtValidMinimum.Text);
                parameter.validMax = Double.Parse(txtValidMaximum.Text);
                parameter.weight = Double.Parse(txtWeight.Text);
                iERPManClient.setQualityCheckCategoryStandard(parameter);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        }
    }
