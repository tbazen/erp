﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERPMan.Client
{
    public partial class ConfigureItemandCategory : XtraForm
    {
        int ParameterID;
        DataTable tableCategory = new DataTable();
        DataTable tableItem = new DataTable();

        public ConfigureItemandCategory(int parameterID)
        {
            InitializeComponent();
            this.ParameterID = parameterID;

            tableCategory.Columns.Add("ItemCategory", typeof(int));
            tableCategory.Columns.Add("standard", typeof(Double));
            tableCategory.Columns.Add("UpperTolerance", typeof(Double));
            tableCategory.Columns.Add("lowerTolerance", typeof(Double));
            tableCategory.Columns.Add("ValidMinimum", typeof(Double));
            tableCategory.Columns.Add("validMaximum", typeof(Double));
            tableCategory.Columns.Add("Weight", typeof(Double));
            gridControl1.DataSource = tableCategory;
            reloadCategoryTable();

            tableItem.Columns.Add("ItemCode", typeof(String));
            tableItem.Columns.Add("standard", typeof(Double));
            tableItem.Columns.Add("UpperTolerance", typeof(Double));
            tableItem.Columns.Add("lowerTolerance", typeof(Double));
            tableItem.Columns.Add("ValidMinimum", typeof(Double));
            tableItem.Columns.Add("validMaximum", typeof(Double));
            tableItem.Columns.Add("Weight", typeof(Double));
            gridControl2.DataSource = tableItem;
            reloadItemTable();
        }

       
        void reloadCategoryTable()
        {
            tableCategory.Rows.Clear();
            QualityCheckCategoryStandard[] Q = iERPManClient.getCategoryStandards(ParameterID);
            foreach (QualityCheckCategoryStandard qq in Q)
            {
                tableCategory.Rows.Add(qq.categoryID, qq.standardValue, qq.upperTol, qq.lowerTol, qq.validMin, qq.validMax, qq.weight);
            }

        }

        void reloadItemTable()
        {
           tableItem.Rows.Clear();
            QualityCheckItemStandard[] Q = iERPManClient.getItemStandards(ParameterID);
            foreach (QualityCheckItemStandard qq in Q)
            {
                tableItem.Rows.Add(qq.itemCode,qq.standardValue, qq.upperTol, qq.lowerTol, qq.validMin, qq.validMax, qq.weight);
            }
        }

        private void ConfigureItemandCategory_Load(object sender, EventArgs e)
        {

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            CategoryStandard category = new CategoryStandard(ParameterID);
            category.Show();
            
           
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            RigisterItemStandard itemstandard = new RigisterItemStandard(ParameterID);
            if(itemstandard.ShowDialog(this)==System.Windows.Forms.DialogResult.OK)
                reloadItemTable();

        }

        private void gridControl1_Click(object sender, EventArgs e)
        {

        }

        private void panelControl1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnRigister_Click(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void barLargeButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
             MessageBox.Show("Are you sure you want to delete?", "Caution", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            try
            {

                int rowHandle = gridView2.GetSelectedRows()[0];
                DataRow selectedRow = gridView2.GetDataRow(rowHandle);

                iERPManClient.deleteQualityCheckItemStandard(ParameterID, (String)selectedRow[0]);
                reloadItemTable();
                

            
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void barLargeButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
             MessageBox.Show("Are you sure you want to delete?", "Caution", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            try
            {
                int rowHandle = gridView1.GetSelectedRows()[0];
                DataRow selectedRow = gridView1.GetDataRow(rowHandle);
                iERPManClient.deleteQualityCheckCategoryStandard(ParameterID, (int)selectedRow[0]);
                reloadCategoryTable();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            
            if (gridView1.SelectedRowsCount == 0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please Select Item Category to edit.");
                return;
            }

            int selectedHandle = gridView1.GetSelectedRows()[0];
            DataRow selectedRow = gridView1.GetDataRow(selectedHandle);

            CategoryStandard quality = new CategoryStandard(this.ParameterID, (int)selectedRow[0]);
            if (quality.ShowDialog() == DialogResult.OK)
            {
                reloadCategoryTable();
            }
        }

        private void barLargeButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            if (gridView2.SelectedRowsCount == 0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please Select Item to edit.");
                return;
            }

            int selectedHandle = gridView2.GetSelectedRows()[0];
            DataRow selectedRow = gridView2.GetDataRow(selectedHandle);

            RigisterItemStandard quality = new RigisterItemStandard(ParameterID, (String)selectedRow[0]);
            if (quality.ShowDialog() == DialogResult.OK)
            {
                reloadItemTable();
            }
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }

    }

