﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERPMan.Client
{
    public partial class Costing_Setting : Form
    {
        public Costing_Setting()
        {
            InitializeComponent();
            loadcostsetting();

        }

        private void loadcostsetting()
        {
            CostingSetting[] Q = iERPManClient.getAllCostingSettings();
            DataTable table = new DataTable();
            table.Columns.Add("tag", typeof(String));
            table.Columns.Add("Description", typeof(string));
            table.Columns.Add("workIndependent", typeof(string));
            foreach (CostingSetting cc in Q)
            {
                table.Rows.Add(cc.description, cc.tag, cc.workIndependent  );
            }
           
            gridControl1.DataSource = table;
        }


        private void panelControl1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {

        }

        private void Costing_Setting_Load(object sender, EventArgs e)
        {

        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RegisterCostSettings rc = new RegisterCostSettings();
            rc.Show(this);
        }

        private void barLargeButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.SelectedRowsCount == 0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please Select Cost to edit.");
                return;
            }

            int selectedHandle = gridView1.GetSelectedRows()[0];
            DataRow selectedRow = gridView1.GetDataRow(selectedHandle);




            RegisterCostSettings cost = new RegisterCostSettings();
            if (cost.ShowDialog() == DialogResult.OK)
            {


                loadcostsetting();
            }
        }

        private void barLargeButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            MessageBox.Show("Are you sure you want to delete?", "Caution", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            try
            {

                int rowHandle = gridView1.GetSelectedRows()[0];
                DataRow selectedRow = gridView1.GetDataRow(rowHandle);

                iERPManClient.deleteCostSetting((string)selectedRow[2]);
                loadcostsetting();



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
