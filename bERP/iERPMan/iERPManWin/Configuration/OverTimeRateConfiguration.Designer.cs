﻿namespace BIZNET.iERPMan.Client.Configuration
{
    partial class OverTimeRateConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.gridControlConfig = new DevExpress.XtraGrid.GridControl();
            this.gridViewConfig = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewConfig)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnClose);
            this.panelControl1.Controls.Add(this.btnSave);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 390);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(632, 41);
            this.panelControl1.TabIndex = 0;
            // 
            // gridControlConfig
            // 
            this.gridControlConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlConfig.Location = new System.Drawing.Point(0, 0);
            this.gridControlConfig.MainView = this.gridViewConfig;
            this.gridControlConfig.Name = "gridControlConfig";
            this.gridControlConfig.Size = new System.Drawing.Size(632, 390);
            this.gridControlConfig.TabIndex = 1;
            this.gridControlConfig.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewConfig});
            // 
            // gridViewConfig
            // 
            this.gridViewConfig.GridControl = this.gridControlConfig;
            this.gridViewConfig.GroupPanelText = "Configure Over Time Hours and Rate";
            this.gridViewConfig.Name = "gridViewConfig";
            this.gridViewConfig.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewConfig.OptionsCustomization.AllowFilter = false;
            this.gridViewConfig.OptionsCustomization.AllowGroup = false;
            this.gridViewConfig.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewConfig.OptionsCustomization.AllowSort = false;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(442, 6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(535, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            // 
            // OverTimeRateConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 431);
            this.Controls.Add(this.gridControlConfig);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "OverTimeRateConfiguration";
            this.ShowIcon = false;
            this.Text = "Over Time Rate Configuration";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewConfig)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControlConfig;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewConfig;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnSave;
    }
}