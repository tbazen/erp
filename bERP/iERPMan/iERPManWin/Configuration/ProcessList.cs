﻿using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERPMan.Client
{
    public partial class ProcessList : XtraForm
    {
        DataTable processTypeTable = new DataTable();

        public ProcessList()
        {
            InitializeComponent();

            processTypeTable.Columns.Add("Name", typeof(string));
            processTypeTable.Columns.Add("Asset", typeof(string));
            processTypeTable.Columns.Add("ID", typeof(int));
            processTypeTable.Columns.Add("Tag", typeof(string));

            reloadData();
        }
        void reloadData()
        {
            ProcessType[] p = iERPManClient.getAllProcessTypes();
            processTypeTable.Rows.Clear();
            foreach (ProcessType pp in p)
            {
                ProcessMachine[] assets = iERPManClient.getProcessMachines(pp.id);
                string assetStr = "";
                for (int i = 0; i < assets.Length; i++)
                    assetStr = (string.IsNullOrEmpty(assetStr)?"": assetStr + ", " )+ AccountingClient.GetAccount<CostCenter>(assets[i].costCenterID).Name;
                processTypeTable.Rows.Add(pp.description, assetStr, pp.id, pp.tag);
            }
            gridControl1.DataSource = processTypeTable;
        }
        private void ProcessList_Load(object sender, EventArgs e)
        {

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {

        }

        private void btnAddProcessType_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RegisterProcessType RT = new RegisterProcessType();
            RT.ShowDialog(this);
            reloadData();

        }

        private void btnEditProcesType_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.SelectedRowsCount == 0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select a process type to edit.");
                return;
            }

            int rowHandle = gridView1.GetSelectedRows()[0];
            DataRow selectedRow = gridView1.GetDataRow(rowHandle);

            RegisterProcessType RT = new RegisterProcessType((int)selectedRow[2]);
            RT.ShowDialog(this);
            reloadData();
        }

        private void btnDeleteProcessType_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            MessageBox.Show("Are you sure you want to delete?", "Caution", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            try
            {

                int rowHandle = gridView1.GetSelectedRows()[0];
                DataRow selectedRow = gridView1.GetDataRow(rowHandle);

                iERPManClient.deleteProcessType((int)selectedRow[2]);

                reloadData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}