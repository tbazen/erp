﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERPMan.Client
{
    public partial class QualityControlTestRecord : XtraForm
    {
        DataTable QualityControlTest = new DataTable();
     
        int editProcessTypeID = -1;
        public QualityControlTestRecord()
        {
            InitializeComponent();
            loadQualityCheckParameters();
        

        }


        private void loadQualityCheckParameters()
        {
            QualityCheckParameter[] Q = iERPManClient.getAllQualityCheckParameters();
            DataTable table = new DataTable();
            table.Columns.Add("Parameter" ,typeof(String));
            table.Columns.Add("standard", typeof(Double));
            table.Columns.Add("uppertolerance", typeof(Double));
            table.Columns.Add("lowerTolerance" ,typeof(Double));
            table.Columns.Add("ValidMinimum",typeof(Double));
            table.Columns.Add("validMaximum",typeof(Double));
            table.Columns.Add("Weight",typeof(Double));
            table.Columns.Add("unit",typeof(String));
            table.Columns.Add("ID", typeof(int));

            foreach (QualityCheckParameter qq in Q)
            {
                table.Rows.Add(qq.name, qq.standardValue, qq.upperTol, qq.lowerTol, qq.validMin, qq.validMax, qq.weight, qq.unit, qq.id);
            }
            gridControl1.DataSource = table;
        }

       

        private void QualityControlTestRecord_Load(object sender, EventArgs e)
        {

        }

        private void simpleButton1_Click(object sender, EventArgs e)
       {
            RigisterQualityControlTest quality = new RigisterQualityControlTest();
           quality.Show();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void barLargeButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RigisterQualityControlTest quality = new RigisterQualityControlTest();
           if(quality.ShowDialog()==DialogResult.OK)
           {
               //load all quality check parameters
               loadQualityCheckParameters();
           }
        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.SelectedRowsCount == 0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please Select asset to edit.");
                return;
            }

            int selectedHandle = gridView1.GetSelectedRows()[0];
            DataRow selectedRow = gridView1.GetDataRow(selectedHandle);

          


           RigisterQualityControlTest quality = new RigisterQualityControlTest((int)selectedRow[8]);
            if (quality.ShowDialog() == DialogResult.OK)
            {


                loadQualityCheckParameters();
            }
            
        }

        private void barLargeButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            MessageBox.Show("Are you sure you want to delete?", "Caution", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            try
            {

                int rowHandle = gridView1.GetSelectedRows()[0];
                DataRow selectedRow = gridView1.GetDataRow(rowHandle);

                iERPManClient.deleteQualityCheckParameter((int)selectedRow[8]);
                loadQualityCheckParameters();

            
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void barLargeButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int rowHandle = gridView1.FocusedRowHandle;
          
            int parameterID = (int)gridView1.GetRowCellValue(rowHandle, "ID");
            ConfigureItemandCategory CT = new ConfigureItemandCategory(parameterID);
            CT.ShowDialog(this);
        }

       

        
    }
}
