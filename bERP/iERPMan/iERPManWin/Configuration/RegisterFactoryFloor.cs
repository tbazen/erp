﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERPMan.Client
{
    public partial class RegisterFactoryFloor : XtraForm
    {
        public RegisterFactoryFloor()
        {
           
            InitializeComponent();
        }


         public  RegisterFactoryFloor(int costCenterID):this()
    {

        Factory q = iERPManClient.getFactory(costCenterID);
           costCenterPlaceHolder1.SetByID(q.costCenterID);
           storePlaceholder1.storeID= q.defaultStoreID;          
            costCenterPlaceHolder1.ReadOnly = true;

             
    }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
             try {
                if (costCenterPlaceHolder1.Text == "")
                {
                    MessageBox.Show("Please Select Factory Floor ", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    costCenterPlaceHolder1.Focus();
                    return;
                }
                if(storePlaceholder1.Text == "")
                {
                    MessageBox.Show("Please Select Store ", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    storePlaceholder1.Focus();
                    return;
                }
                 Factory pp = new Factory();
                 pp.costCenterID = costCenterPlaceHolder1.GetAccountID();
                 pp.defaultStoreID = storePlaceholder1.storeID;
                 iERPManClient.setFactory(pp); 
                 this.DialogResult = DialogResult.OK;
                 this.Close();
            }
              
            
                catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            }

        private void RegisterFactoryFloor_Load(object sender, EventArgs e)
        {

        }
    }
        }
    

