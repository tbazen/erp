﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERPMan.Client
{
    public partial class RegisterFactoryFloorRecord : XtraForm
    {
        DataTable table = new DataTable();
         int EditFactoryFloorID = -1;
        
        public RegisterFactoryFloorRecord()
        {
            InitializeComponent();
            

            
            table.Columns.Add("FactoryFLoor", typeof(int));
            table.Columns.Add("Store", typeof(int));
            loadFactoryFloor();
           
        }

        private void loadFactoryFloor()
        {
            Factory[] Q = iERPManClient.getAllFactories();
            table.Rows.Clear();
            foreach (Factory qq in Q)
            {
                table.Rows.Add(qq.costCenterID, qq.defaultStoreID);
            }
            gridControl1.DataSource = table;
            
        }
        private void RegisterFactoryFoor_Load(object sender, EventArgs e)
        {

        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RegisterFactoryFloor FF = new RegisterFactoryFloor();
            FF.ShowDialog(this);
            loadFactoryFloor();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void barLargeButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.SelectedRowsCount == 0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please Select Factory Floor to edit.");
                return;
            }

            int selectedHandle = gridView1.GetSelectedRows()[0];
            DataRow selectedRow = gridView1.GetDataRow(selectedHandle);

            

            RegisterFactoryFloor FF = new RegisterFactoryFloor((int)selectedRow[0]);
            if (FF.ShowDialog() == DialogResult.OK)
            {


                loadFactoryFloor();
          
            }
           
        }

        private void barLargeButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            MessageBox.Show("Are you sure you want to delete?", "Caution", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            try
            {

                int rowHandle = gridView1.GetSelectedRows()[0];
                DataRow selectedRow = gridView1.GetDataRow(rowHandle);

                iERPManClient.deleteFactory((int)selectedRow[0]);
                loadFactoryFloor();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
