﻿using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERPMan.Client
{
    public partial class RegisterProcessType : XtraForm
    {
        DataTable assetTable;
        int editProcessTypeID = -1;
        public RegisterProcessType()
        {
            InitializeComponent();
            assetTable = new DataTable();
            assetTable.Columns.Add("CostCenter",typeof(string));
            assetTable.Columns.Add("CostPerHour",typeof(double));
            assetTable.Columns.Add("CostCenterID",typeof(int));
            assetTable.Columns.Add("Tag",typeof(string));
            gridControl1.DataSource = assetTable;
        }
        public RegisterProcessType(int processTypeID):this()
        {
            editProcessTypeID = processTypeID;
            ProcessType p = iERPManClient.getProcessType(processTypeID);
            txtDescription.Text = p.description;
            txtTag.Text = p.tag;
            ProcessMachine[] assets = iERPManClient.getProcessMachines(processTypeID);
            foreach (ProcessMachine a in assets)
            {
                CostCenter cs = AccountingClient.GetAccount<CostCenter>(a.costCenterID);
                assetTable.Rows.Add(cs.Name, a.costPerHour, a.costCenterID, p.tag);
            }

        }

        private void frmProcess_Load(object sender, EventArgs e)
        {

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            AddAsset addasset = new AddAsset();
            addasset.Show();
        }

        private void groupControl1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void barLargeButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            AddAsset addasset = new AddAsset();
            if(addasset.ShowDialog()==DialogResult.OK)
            {
                ProcessMachine m = addasset.PM;
                CostCenter cs= AccountingClient.GetAccount<CostCenter>(m.costCenterID);
                assetTable.Rows.Add(cs.Name, m.costPerHour,m.costCenterID);
                //add asset to grid
            }
        }

        private void btnRigister_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessType processType = new ProcessType();
                if (txtDescription.Text == "")
                {
                    MessageBox.Show("Please Enter Process Type", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDescription.Focus();
                    return;
                }
                    
                processType.tag = txtTag.Text.Trim();
                if (processType.tag == "")
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter valid tag");
                    return;
                }
                processType.description = txtDescription.Text;

                
                ProcessMachine[] assets = new ProcessMachine[assetTable.Rows.Count];
                for (int i = 0; i < assets.Length; i++)
                {
                    DataRow row = assetTable.Rows[i];
                    ProcessMachine oneAsset = new ProcessMachine();
                    oneAsset.costPerHour = (double)row[1];
                    oneAsset.costCenterID = (int)row[2];
                    assets[i] = oneAsset;
                }
                
                if (editProcessTypeID == -1) //create case
                    iERPManClient.createProcessType(processType, assets);
                else //udpate case
                {
                    processType.id = editProcessTypeID;
                    iERPManClient.upateProcessType(processType, assets);
                }
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(gridView1.SelectedRowsCount==0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please Select asset to edit.");
                return;
            }
            
            int selectedHandle=gridView1.GetSelectedRows()[0];
            DataRow selectedRow = gridView1.GetDataRow(selectedHandle);

            ProcessMachine asset;
            asset = new ProcessMachine();
            asset.costCenterID = (int) selectedRow[2];
            asset.costPerHour = (double)selectedRow[1];
           

            AddAsset addasset = new AddAsset(asset);
            if (addasset.ShowDialog() == DialogResult.OK)
            {
                ProcessMachine m = addasset.PM;
                CostCenter cs = AccountingClient.GetAccount<CostCenter>(m.costCenterID);
                selectedRow[0] = cs.Name;
                selectedRow[1] = m.costPerHour;
                selectedRow[2] = m.costCenterID;
                //selectedRow[3] = m.
            }
        }

        private void barLargeButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void gridControl1_Click(object sender, EventArgs e)
        {

        }
        
    }
}
