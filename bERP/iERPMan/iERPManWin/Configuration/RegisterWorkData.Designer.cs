﻿namespace BIZNET.iERPMan.Client
{
    partial class RegisterWorkData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlOverTime = new DevExpress.XtraGrid.GridControl();
            this.gridViewOverTime = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControlAbsence = new DevExpress.XtraGrid.GridControl();
            this.gridViewAbsence = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.dtDate = new BIZNET.iERP.Client.BNDualCalendar();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAbsence = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutOverTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOverTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOverTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAbsence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAbsence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAbsence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOverTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.gridControlOverTime);
            this.layoutControl1.Controls.Add(this.gridControlAbsence);
            this.layoutControl1.Controls.Add(this.dtDate);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(709, 541);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnClose);
            this.panelControl1.Controls.Add(this.btnSave);
            this.panelControl1.Location = new System.Drawing.Point(12, 498);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(685, 31);
            this.panelControl1.TabIndex = 7;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(588, 2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 28);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(492, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 28);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gridControlOverTime
            // 
            this.gridControlOverTime.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlOverTime.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlOverTime.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlOverTime.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControlOverTime.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridControlOverTime.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridControlOverTime.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControlOverTime.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridControlOverTime.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControlOverTime.Location = new System.Drawing.Point(12, 326);
            this.gridControlOverTime.MainView = this.gridViewOverTime;
            this.gridControlOverTime.Name = "gridControlOverTime";
            this.gridControlOverTime.Size = new System.Drawing.Size(685, 168);
            this.gridControlOverTime.TabIndex = 6;
            this.gridControlOverTime.UseEmbeddedNavigator = true;
            this.gridControlOverTime.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewOverTime});
            this.gridControlOverTime.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridControlOverTime_ProcessGridKey);
            // 
            // gridViewOverTime
            // 
            this.gridViewOverTime.GridControl = this.gridControlOverTime;
            this.gridViewOverTime.GroupPanelText = "Employees Over Time Data";
            this.gridViewOverTime.Name = "gridViewOverTime";
            this.gridViewOverTime.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewOverTime_CellValueChanged);
            this.gridViewOverTime.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewOverTime_KeyDown);
            // 
            // gridControlAbsence
            // 
            this.gridControlAbsence.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlAbsence.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlAbsence.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlAbsence.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridControlAbsence.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridControlAbsence.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridControlAbsence.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControlAbsence.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridControlAbsence.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControlAbsence.Location = new System.Drawing.Point(12, 98);
            this.gridControlAbsence.MainView = this.gridViewAbsence;
            this.gridControlAbsence.Name = "gridControlAbsence";
            this.gridControlAbsence.Size = new System.Drawing.Size(685, 224);
            this.gridControlAbsence.TabIndex = 5;
            this.gridControlAbsence.UseEmbeddedNavigator = true;
            this.gridControlAbsence.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAbsence});
            this.gridControlAbsence.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridControlAbsence_ProcessGridKey);
            // 
            // gridViewAbsence
            // 
            this.gridViewAbsence.GridControl = this.gridControlAbsence;
            this.gridViewAbsence.GroupPanelText = "Employees Absence Data";
            this.gridViewAbsence.Name = "gridViewAbsence";
            this.gridViewAbsence.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewAbsence_CellValueChanged);
            this.gridViewAbsence.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewAbsence_KeyDown);
            // 
            // dtDate
            // 
            this.dtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dtDate.Location = new System.Drawing.Point(12, 28);
            this.dtDate.Name = "dtDate";
            this.dtDate.ShowEthiopian = true;
            this.dtDate.ShowGregorian = true;
            this.dtDate.ShowTime = false;
            this.dtDate.Size = new System.Drawing.Size(685, 42);
            this.dtDate.TabIndex = 4;
            this.dtDate.VerticalLayout = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutAbsence,
            this.layoutOverTime,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(709, 541);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.dtDate;
            this.layoutControlItem1.CustomizationFormText = "Date:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(689, 86);
            this.layoutControlItem1.Text = "Date:";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(30, 13);
            // 
            // layoutAbsence
            // 
            this.layoutAbsence.Control = this.gridControlAbsence;
            this.layoutAbsence.CustomizationFormText = "layoutAbsence";
            this.layoutAbsence.Location = new System.Drawing.Point(0, 86);
            this.layoutAbsence.Name = "layoutAbsence";
            this.layoutAbsence.Size = new System.Drawing.Size(689, 228);
            this.layoutAbsence.Text = "layoutAbsence";
            this.layoutAbsence.TextSize = new System.Drawing.Size(0, 0);
            this.layoutAbsence.TextToControlDistance = 0;
            this.layoutAbsence.TextVisible = false;
            // 
            // layoutOverTime
            // 
            this.layoutOverTime.Control = this.gridControlOverTime;
            this.layoutOverTime.CustomizationFormText = "layoutOverTime";
            this.layoutOverTime.Location = new System.Drawing.Point(0, 314);
            this.layoutOverTime.Name = "layoutOverTime";
            this.layoutOverTime.Size = new System.Drawing.Size(689, 172);
            this.layoutOverTime.Text = "layoutOverTime";
            this.layoutOverTime.TextSize = new System.Drawing.Size(0, 0);
            this.layoutOverTime.TextToControlDistance = 0;
            this.layoutOverTime.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.panelControl1;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 486);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(689, 35);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // RegisterWorkData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 541);
            this.Controls.Add(this.layoutControl1);
            this.Name = "RegisterWorkData";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Register Work Data";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOverTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOverTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAbsence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAbsence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAbsence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOverTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl gridControlAbsence;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewAbsence;
        private iERP.Client.BNDualCalendar dtDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutAbsence;
        private DevExpress.XtraGrid.GridControl gridControlOverTime;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewOverTime;
        private DevExpress.XtraLayout.LayoutControlItem layoutOverTime;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnSave;
    }
}