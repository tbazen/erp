﻿using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using INTAPS.Payroll.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERPMan.Client
{
    public partial class RegisterWorkData : DevExpress.XtraEditors.XtraForm
    {
        private DataTable _absenceData;
        private DataTable _overTimeData;
        public RegisterWorkData()
        {
            InitializeComponent();
            _absenceData = new DataTable();
            _overTimeData = new DataTable();
            PrepareAbsenceData();
            PrepareOverTimeData();
        }
        public RegisterWorkData(DateTime date)
            :this()
        {
            dtDate.DateTime = date;
            LoadAbsenceData();
            LoadOverTimeData();
        }

        private void LoadAbsenceData()
        {
            _absenceData.Rows.Clear();
            Data_Absence[] absenceData = iERPManClient.getAbsenceWorkData(dtDate.DateTime);
            foreach (Data_Absence abs in absenceData)
            {
                Employee emp = PayrollClient.GetEmployee(abs.employeeID);
                DataRow row = _absenceData.NewRow();
                row[0] = emp.employeeID;
                row[1] = emp.employeeName;
                row[2] = abs.absenceHours;
                row[3] = abs.employeeID;
                row[4] = false;
                _absenceData.Rows.Add(row);
                gridControlAbsence.DataSource = _absenceData;
            }
            gridViewAbsence.BestFitColumns();
        }

        private void LoadOverTimeData()
        {
            _overTimeData.Rows.Clear();
            Data_OverTime[] overtimeData = iERPManClient.getOverTimeWorkData(dtDate.DateTime);
            foreach (Data_OverTime overtime in overtimeData)
            {
                Employee emp = PayrollClient.GetEmployee(overtime.employeeID);
                DataRow row = _overTimeData.NewRow();
                row[0] = emp.employeeID;
                row[1] = emp.employeeName;
                row[2] = new DateTime(overtime.fromTime);
                row[3] = new DateTime(overtime.toTime);
                row[4] = overtime.employeeID;
                row[5] = false;
                _overTimeData.Rows.Add(row);
                gridControlOverTime.DataSource = _overTimeData;
            }
            gridViewOverTime.BestFitColumns();
        }

        private void PrepareAbsenceData()
        {
            _absenceData.Columns.Add("Employee ID", typeof(string));
            _absenceData.Columns.Add("Name", typeof(string));
            _absenceData.Columns.Add("Absence Hours", typeof(double));
            _absenceData.Columns.Add("Id", typeof(int));
            _absenceData.Columns.Add("IsNew", typeof(bool));
            gridControlAbsence.DataSource = _absenceData;
            gridViewAbsence.Columns["Id"].Visible = false;
            gridViewAbsence.Columns["IsNew"].Visible = false;
            gridViewAbsence.Columns["Name"].OptionsColumn.AllowEdit = false;
        }

        private void PrepareOverTimeData()
        {
            _overTimeData.Columns.Add("Employee ID", typeof(string));
            _overTimeData.Columns.Add("Name", typeof(string));
            _overTimeData.Columns.Add("From", typeof(object));
            _overTimeData.Columns.Add("To", typeof(object));
            _overTimeData.Columns.Add("Id", typeof(int));
            _overTimeData.Columns.Add("IsNew", typeof(bool));
            gridControlOverTime.DataSource = _overTimeData;
            RepositoryItemTimeEdit timeFrom = new RepositoryItemTimeEdit();
            RepositoryItemTimeEdit timeTo = new RepositoryItemTimeEdit();
            gridViewOverTime.Columns["From"].ColumnEdit = timeFrom;
            gridViewOverTime.Columns["To"].ColumnEdit = timeTo;
            gridViewOverTime.Columns["Id"].Visible = false;
            gridViewOverTime.Columns["IsNew"].Visible = false;
            gridViewOverTime.Columns["Name"].OptionsColumn.AllowEdit = false;
            gridControlOverTime.RepositoryItems.AddRange(new RepositoryItem[] { timeFrom, timeTo });

         
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void gridControlAbsence_ProcessGridKey(object sender, KeyEventArgs e)
        {
            processGridKey(e);
        }

        private static void processGridKey(KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
                return;
        }

        private void gridControlOverTime_ProcessGridKey(object sender, KeyEventArgs e)
        {
            processGridKey(e);
        }

        private void gridViewAbsence_KeyDown(object sender, KeyEventArgs e)
        {
            GridView grid = null;
            if (sender is GridView)
                grid = sender as GridView;
            else if (sender is BaseEdit)
                grid = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;

            NavigateRecordOnEnter(e, grid, "Employee ID", "Absence Hours", "Absence Hours");
        }

        private void NavigateRecordOnEnter(KeyEventArgs e, GridView grid, string beginColumn,string focusColumn,string endColumn)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (grid.FocusedColumn.FieldName.Equals(beginColumn))
                {
                    BeginInvoke(new MethodInvoker(() =>
                    {
                        int col = grid.Columns[focusColumn].VisibleIndex;
                        grid.FocusedColumn = grid.VisibleColumns[col];
                        grid.ShowEditor();
                    }));
                }
                if (grid.FocusedColumn.FieldName.Equals(endColumn) && !grid.HasColumnErrors)
                {
                    BeginInvoke(new MethodInvoker(() =>
                    {
                        grid.AddNewRow();
                        grid.MoveNext();
                        int col = grid.Columns[beginColumn].VisibleIndex;
                        grid.FocusedColumn = grid.VisibleColumns[col];
                        grid.ShowEditor();
                    }));
                }
            }
        }

        private void gridViewOverTime_KeyDown(object sender, KeyEventArgs e)
        {
            GridView grid = null;
            if (sender is GridView)
                grid = sender as GridView;
            else if (sender is BaseEdit)
                grid = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;

            NavigateRecordOnEnter(e, grid, "Employee ID", "From", "To");

        }

        private void gridViewAbsence_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            GridView grid = null;
            if (sender is GridView)
                grid = sender as GridView;
            else if (sender is BaseEdit)
                grid = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;

            validateEmployee(e, grid);
        }

        private void validateEmployee(DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e, GridView grid)
        {
            if (e.Column.FieldName.Equals("Employee ID"))
            {
                string empID = (string)grid.GetFocusedRowCellValue("Employee ID");
                Employee emp = PayrollClient.GetEmployeeByID(empID);
                if (emp == null)
                {
                    DevExMessageBox.ShowErrorMessage("Employee does not exist");
                    FocusEmployeeIDCell(grid);
                    grid.SetColumnError(grid.Columns["Employee ID"], "Employee does not exist", DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical);
                }
                else
                {
                    if (employeeAlreadyExists(emp.employeeID, grid.FocusedRowHandle, grid))
                    {
                        if (grid.Name.Equals("gridViewAbsence"))
                        {
                            DevExMessageBox.ShowErrorMessage("Employee: " + emp.employeeID + " already exists");
                            FocusEmployeeIDCell(grid);
                            grid.SetColumnError(grid.Columns["Employee ID"], "Employee already exists", DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical);
                        }
                        else
                        {
                            grid.SetRowCellValue(grid.FocusedRowHandle, "Name", emp.employeeName);
                            grid.ClearColumnErrors();
                            grid.Columns["Name"].Group();
                        }
                    }
                    else
                    {
                        grid.SetRowCellValue(grid.FocusedRowHandle, "Name", emp.employeeName);
                        grid.ClearColumnErrors();
                    }

                }
            }
            else if(e.Column.FieldName.Equals("From"))
            {
                DateTime time = (DateTime)grid.GetFocusedRowCellValue("From");
                string empID = (string)grid.GetFocusedRowCellValue("Employee ID");
                if(employeeDataRepeated(empID,time,grid.FocusedRowHandle))
                {
                    DevExMessageBox.ShowErrorMessage("Employee data repeated for employee: " + empID + ", Time:" + time.ToLongTimeString());
                    FocusEmployeeIDCell(grid);
                    grid.SetColumnError(grid.Columns["Employee ID"], "Employee data repeated", DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical);

                }
            }
        }

        private bool employeeDataRepeated(string empID, DateTime time, int rowHandle)
        {
            for (int i = 0; i < gridViewOverTime.DataRowCount; i++)
            {
                if(i!=rowHandle)
                {
                    DateTime time2 = (DateTime)gridViewOverTime.GetRowCellValue(i, "From");
                    string empID2 = (string)gridViewOverTime.GetRowCellValue(i, "Employee ID");
                    if(DateTime.Compare(time,time2)==0 && empID.ToLower().Equals(empID2.ToLower()))
                        return true;
                }
            }
            return false;
        }

        private bool employeeAlreadyExists(string empID, int rowHandle, GridView grid)
        {
            for (int i = 0; i < grid.DataRowCount; i++)
            {
                if (i != rowHandle)
                {
                    if (empID.ToLower().Equals(((string)grid.GetRowCellValue(i, "Employee ID")).ToLower()))
                        return true;
                }
            }
            return false;
        }
        private void FocusEmployeeIDCell(GridView grid)
        {
            BeginInvoke(new MethodInvoker(() =>
            {
                int col = grid.Columns["Employee ID"].VisibleIndex;
                grid.FocusedColumn = grid.VisibleColumns[col];
                grid.ShowEditor();
                grid.ActiveEditor.SelectAll();
            }));
        }

        private void gridViewOverTime_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            GridView grid = null;
            if (sender is GridView)
                grid = sender as GridView;
            else if (sender is BaseEdit)
                grid = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;

            validateEmployee(e, grid);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (dtDate.DateTime > DateTime.Now)
            {
                DevExMessageBox.ShowErrorMessage("Date cannot be in the future");
                return;
            }
            Data_Absence[] absence = processAbscenceData();
            Data_OverTime[] overtime = processOverTimeData();
            try
            {
                iERPManClient.saveWorkData(absence, overtime);
                DevExMessageBox.ShowSuccessMessage("Work data successfully saved");
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch(Exception ex)
            {
                DevExMessageBox.ShowErrorMessage(ex.Message);
            }
        }

       

        private Data_Absence[] processAbscenceData()
        {
            List<Data_Absence> absence = new List<Data_Absence>();
            for (int i = 0; i < gridViewAbsence.DataRowCount; i++)
            {
                double hours = gridViewAbsence.GetRowCellValue(i, "Absence Hours") is DBNull ? 0 : (double)gridViewAbsence.GetRowCellValue(i, "Absence Hours");
                if (hours == 0)
                    continue;
                int empID = gridViewAbsence.GetRowCellValue(i, "Employee ID") is DBNull ? -1 : PayrollClient.GetEmployeeByID((string)gridViewAbsence.GetRowCellValue(i, "Employee ID")).id;
                if (empID == -1)
                {
                    DevExMessageBox.ShowErrorMessage("Invalid employee id at row no." + i + 1);
                    return null;
                }
                bool isNew = gridViewAbsence.GetRowCellValue(i, "IsNew") is DBNull ? true : false;
                Data_Absence abs = new Data_Absence();
                abs.absenceHours = hours;
                abs.date = dtDate.DateTime;
                abs.isNewData = isNew;
                abs.employeeID = empID;
                absence.Add(abs);
            }
            return absence.ToArray();
        }

        private Data_OverTime[] processOverTimeData()
        {
            List<Data_OverTime> overTime = new List<Data_OverTime>();
            for (int i = 0; i < gridViewOverTime.DataRowCount; i++)
            {
                int empID = gridViewOverTime.GetRowCellValue(i, "Employee ID") is DBNull ? -1 : PayrollClient.GetEmployeeByID((string)gridViewOverTime.GetRowCellValue(i, "Employee ID")).id;
                if (empID == -1)
                {
                    DevExMessageBox.ShowErrorMessage("Invalid employee id at row no." + i + 1);
                    return null;
                }
                bool isNew = gridViewOverTime.GetRowCellValue(i, "IsNew") is DBNull ? true : false;
                Data_OverTime ov = new Data_OverTime();
                ov.date = dtDate.DateTime;
                ov.isNewData = isNew;
                ov.employeeID = empID;
                DateTime from = (DateTime)gridViewOverTime.GetRowCellValue(i, "From");
                DateTime to = (DateTime)gridViewOverTime.GetRowCellValue(i, "To");
                ov.fromTime = new DateTime(dtDate.DateTime.Year, dtDate.DateTime.Month, dtDate.DateTime.Day, from.Hour, from.Second, from.Millisecond).Ticks;
                ov.toTime = new DateTime(dtDate.DateTime.Year, dtDate.DateTime.Month, dtDate.DateTime.Day, to.Hour, to.Second, to.Millisecond).Ticks;
                overTime.Add(ov);
            }
            return overTime.ToArray();
        }
      
    }
}
