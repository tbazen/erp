﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERPMan.Client
{
    public partial class RigisterItemStandard : Form
    {
        private int EditQualityParameterID = -1;
        public RigisterItemStandard(int parameterID)
        {
            EditQualityParameterID = parameterID;
            InitializeComponent();
        }

        public RigisterItemStandard(int parameterID, String itemCode):this(parameterID)
    {
        QualityCheckItemStandard q = iERPManClient.getQualityCheckItemStandard(parameterID,itemCode);
            txtItemType.SetByID(q.itemCode);
            txtStandard.Text= q.standardValue.ToString();
            txtUpperTolerance.Text=q.upperTol.ToString();
            txtLowerTolerance.Text=q.lowerTol.ToString();
            txtValidMinium.Text = q.validMin.ToString();
            txtvalidMaximum.Text = q.validMax.ToString();
            txtWeight.Text = q.weight.ToString();
            txtItemType.ReadOnly = true;
    }
        private void RigisterItemStandard_Load(object sender, EventArgs e)
        {

           
      
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
             try {
                if (txtItemType.Text == "")
                {
                    MessageBox.Show("Please enter Item Categor ", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtItemType.Focus();
                    return;
                }
                if(txtStandard.Text == "")
                {
                    MessageBox.Show("Please enter Item Standard ", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtStandard.Focus();
                    return;
                }
                if(txtUpperTolerance.Text=="")
                {
                    MessageBox.Show("Please enter Upper Tolerance", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtUpperTolerance.Focus();
                    return;
                }
                if (txtLowerTolerance.Text == "")
                {
                    MessageBox.Show("Please enter Lower Tolerance", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtLowerTolerance.Focus();
                    return;
                }
                if(txtValidMinium.Text =="")
                {
                    MessageBox.Show("Please enter valid maximum", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtValidMinium.Focus();
                    return;
                }
                if(txtvalidMaximum.Text=="")
                {
                    MessageBox.Show("Please enter valid Minimum", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtvalidMaximum.Focus();
                    return;
                }
                if(txtWeight.Text=="")
                {
                    MessageBox.Show("Please enter Weight", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtWeight.Focus();
                    return;
                }

               QualityCheckItemStandard parameter = new QualityCheckItemStandard();
               parameter.itemCode = txtItemType.GetObjectID();
                parameter.parameterID = EditQualityParameterID;
                parameter.standardValue = Double.Parse(txtStandard.Text);
                parameter.upperTol = Double.Parse(txtUpperTolerance.Text);
                parameter.lowerTol = Double.Parse(txtLowerTolerance.Text);
                parameter.validMin = Double.Parse(txtValidMinium.Text);
                parameter.validMax = Double.Parse(txtvalidMaximum.Text);
                parameter.weight = Double.Parse(txtWeight.Text);
                iERPManClient.setQualityCheckItemStandard(parameter);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
                catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        }
    }

