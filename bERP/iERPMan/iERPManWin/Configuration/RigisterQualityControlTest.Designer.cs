﻿namespace BIZNET.iERPMan.Client
{
    partial class RigisterQualityControlTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.comUnit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtvalidMaximum = new DevExpress.XtraEditors.TextEdit();
            this.txtValidMinimum = new DevExpress.XtraEditors.TextEdit();
            this.txtWeight = new DevExpress.XtraEditors.TextEdit();
            this.txtLowerTolerance = new DevExpress.XtraEditors.TextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnRegister = new DevExpress.XtraEditors.SimpleButton();
            this.txtUpperTolerance = new DevExpress.XtraEditors.TextEdit();
            this.txtStandard = new DevExpress.XtraEditors.TextEdit();
            this.txtParameter = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comUnit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvalidMaximum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValidMinimum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLowerTolerance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUpperTolerance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtParameter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.comUnit);
            this.layoutControl1.Controls.Add(this.txtvalidMaximum);
            this.layoutControl1.Controls.Add(this.txtValidMinimum);
            this.layoutControl1.Controls.Add(this.txtWeight);
            this.layoutControl1.Controls.Add(this.txtLowerTolerance);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.txtUpperTolerance);
            this.layoutControl1.Controls.Add(this.txtStandard);
            this.layoutControl1.Controls.Add(this.txtParameter);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(541, 254);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // comUnit
            // 
            this.comUnit.Location = new System.Drawing.Point(98, 180);
            this.comUnit.Name = "comUnit";
            this.comUnit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comUnit.Size = new System.Drawing.Size(431, 20);
            this.comUnit.StyleController = this.layoutControl1;
            this.comUnit.TabIndex = 12;
            // 
            // txtvalidMaximum
            // 
            this.txtvalidMaximum.Location = new System.Drawing.Point(98, 156);
            this.txtvalidMaximum.Name = "txtvalidMaximum";
            this.txtvalidMaximum.Properties.Mask.EditMask = "#,########0.00;";
            this.txtvalidMaximum.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtvalidMaximum.Size = new System.Drawing.Size(431, 20);
            this.txtvalidMaximum.StyleController = this.layoutControl1;
            this.txtvalidMaximum.TabIndex = 11;
            // 
            // txtValidMinimum
            // 
            this.txtValidMinimum.Location = new System.Drawing.Point(98, 132);
            this.txtValidMinimum.Name = "txtValidMinimum";
            this.txtValidMinimum.Properties.Mask.EditMask = "#,########0.00;";
            this.txtValidMinimum.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtValidMinimum.Size = new System.Drawing.Size(431, 20);
            this.txtValidMinimum.StyleController = this.layoutControl1;
            this.txtValidMinimum.TabIndex = 10;
            // 
            // txtWeight
            // 
            this.txtWeight.Location = new System.Drawing.Point(98, 108);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Properties.Mask.EditMask = "#,########0.00;";
            this.txtWeight.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtWeight.Size = new System.Drawing.Size(431, 20);
            this.txtWeight.StyleController = this.layoutControl1;
            this.txtWeight.TabIndex = 9;
            // 
            // txtLowerTolerance
            // 
            this.txtLowerTolerance.Location = new System.Drawing.Point(98, 84);
            this.txtLowerTolerance.Name = "txtLowerTolerance";
            this.txtLowerTolerance.Properties.Mask.EditMask = "#,########0.00;";
            this.txtLowerTolerance.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtLowerTolerance.Size = new System.Drawing.Size(431, 20);
            this.txtLowerTolerance.StyleController = this.layoutControl1;
            this.txtLowerTolerance.TabIndex = 8;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.btnRegister);
            this.panelControl1.Location = new System.Drawing.Point(12, 204);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(517, 38);
            this.panelControl1.TabIndex = 7;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(418, 6);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(94, 27);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "&Close";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // btnRegister
            // 
            this.btnRegister.Location = new System.Drawing.Point(315, 6);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(97, 27);
            this.btnRegister.TabIndex = 0;
            this.btnRegister.Text = "&Register";
            this.btnRegister.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // txtUpperTolerance
            // 
            this.txtUpperTolerance.Location = new System.Drawing.Point(98, 60);
            this.txtUpperTolerance.Name = "txtUpperTolerance";
            this.txtUpperTolerance.Properties.Mask.EditMask = "#,########0.00;";
            this.txtUpperTolerance.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtUpperTolerance.Size = new System.Drawing.Size(431, 20);
            this.txtUpperTolerance.StyleController = this.layoutControl1;
            this.txtUpperTolerance.TabIndex = 6;
            // 
            // txtStandard
            // 
            this.txtStandard.Location = new System.Drawing.Point(98, 36);
            this.txtStandard.Name = "txtStandard";
            this.txtStandard.Properties.Mask.EditMask = "#,########0.00;";
            this.txtStandard.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtStandard.Size = new System.Drawing.Size(431, 20);
            this.txtStandard.StyleController = this.layoutControl1;
            this.txtStandard.TabIndex = 5;
            // 
            // txtParameter
            // 
            this.txtParameter.Location = new System.Drawing.Point(98, 12);
            this.txtParameter.Name = "txtParameter";
            this.txtParameter.Size = new System.Drawing.Size(431, 20);
            this.txtParameter.StyleController = this.layoutControl1;
            this.txtParameter.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(541, 254);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtParameter;
            this.layoutControlItem1.CustomizationFormText = "Parameter";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(521, 24);
            this.layoutControlItem1.Text = "Parameter:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtStandard;
            this.layoutControlItem2.CustomizationFormText = "Standard";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(521, 24);
            this.layoutControlItem2.Text = "Standard:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtUpperTolerance;
            this.layoutControlItem3.CustomizationFormText = "Actual";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(521, 24);
            this.layoutControlItem3.Text = "Upper Tolerance:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.panelControl1;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 192);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(521, 42);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtLowerTolerance;
            this.layoutControlItem5.CustomizationFormText = "Lower Tolernace";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(521, 24);
            this.layoutControlItem5.Text = "Lower Tolernace:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtWeight;
            this.layoutControlItem6.CustomizationFormText = "Weight:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(521, 24);
            this.layoutControlItem6.Text = "Weight:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtValidMinimum;
            this.layoutControlItem7.CustomizationFormText = "Valid Minimum";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(521, 24);
            this.layoutControlItem7.Text = "Valid Minimum:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtvalidMaximum;
            this.layoutControlItem8.CustomizationFormText = "Valid Maximum";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(521, 24);
            this.layoutControlItem8.Text = "Valid Maximum:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.comUnit;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(521, 24);
            this.layoutControlItem9.Text = "Unit:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(83, 13);
            // 
            // RigisterQualityControlTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 254);
            this.Controls.Add(this.layoutControl1);
            this.Name = "RigisterQualityControlTest";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quality Control Parameter";
            this.Load += new System.EventHandler(this.QualityControlTest_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comUnit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtvalidMaximum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValidMinimum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLowerTolerance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtUpperTolerance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtParameter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton btnRegister;
        private DevExpress.XtraEditors.TextEdit txtUpperTolerance;
        private DevExpress.XtraEditors.TextEdit txtStandard;
        private DevExpress.XtraEditors.TextEdit txtParameter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit txtLowerTolerance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.TextEdit txtWeight;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.TextEdit txtvalidMaximum;
        private DevExpress.XtraEditors.TextEdit txtValidMinimum;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.ComboBoxEdit comUnit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
    }
}