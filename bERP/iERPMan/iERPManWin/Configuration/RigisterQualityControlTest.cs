﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERPMan.Client
{
    public partial class RigisterQualityControlTest : XtraForm
    {
        private int EditQualityParameterID=-1;

        public RigisterQualityControlTest()
        {
            InitializeComponent();
        }

        public RigisterQualityControlTest(int QualityParameterId)
            : this()
        {

            EditQualityParameterID = QualityParameterId;
            QualityCheckParameter p = iERPManClient.getQualityCheckParameter(QualityParameterId);
            txtParameter.Text = p.name;
            txtStandard.Text = p.standardValue.ToString();
            txtUpperTolerance.Text = p.upperTol.ToString();
            txtLowerTolerance.Text = p.lowerTol.ToString();
            txtValidMinimum.Text = p.validMin.ToString();
            txtvalidMaximum.Text = p.validMax.ToString();
            txtWeight.Text = p.weight.ToString();
            comUnit.Text = p.unit;



        }

        private void QualityControlTest_Load(object sender, EventArgs e)
        {
           
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtParameter.Text == "")
                {
                    MessageBox.Show("Please enter Parameter ", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtParameter.Focus();
                    return;
                }
                if (txtStandard.Text == "")
                {
                    MessageBox.Show("Please enter Item Standard ", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtStandard.Focus();
                    return;
                }
                if (txtUpperTolerance.Text == "")
                {
                    MessageBox.Show("Please enter Upper Tolerance", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtUpperTolerance.Focus();
                    return;
                }
                if (txtLowerTolerance.Text == "")
                {
                    MessageBox.Show("Please enter Lower Tolerance", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtLowerTolerance.Focus();
                    return;
                }
                if (txtValidMinimum.Text == "")
                {
                    MessageBox.Show("Please enter valid maximum", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtValidMinimum.Focus();
                    return;
                }
                if (txtvalidMaximum.Text == "")
                {
                    MessageBox.Show("Please enter valid Minimum", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtvalidMaximum.Focus();
                    return;
                }
                if (txtWeight.Text == "")
                {
                    MessageBox.Show("Please enter Weight", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtWeight.Focus();
                    return;
                }
                QualityCheckParameter parameter = new QualityCheckParameter();
                parameter.name = txtParameter.Text;
                parameter.standardValue = Double.Parse(txtStandard.Text);
                parameter.upperTol = Double.Parse(txtUpperTolerance.Text);
                parameter.lowerTol = Double.Parse(txtLowerTolerance.Text);
                parameter.validMin = Double.Parse(txtValidMinimum.Text);
                parameter.validMax = Double.Parse(txtvalidMaximum.Text);
                parameter.weight = Double.Parse(txtWeight.Text);
                parameter.unit = comUnit.Text;
                if (EditQualityParameterID == -1)
                {
                    iERPManClient.createQualityCheckParameter(parameter);
                }
                else //udpate case
                {
                    parameter.id = EditQualityParameterID;
                    iERPManClient.updateQualityCheckParameter(parameter);
                }
                 
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
            
        }
    }
}
