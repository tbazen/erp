﻿using BIZNET.iERP;
using BIZNET.iERP.Client;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERPMan.Client
{
    public partial class StockLevel : XtraForm
    {
        private DataTable m_StockLevelTable;
        private StockLevelConfiguration[] m_config;
        public StockLevel()
        {
            InitializeComponent();
            m_StockLevelTable = new DataTable();
            prepareDataTable();
            gridViewLevel.AddNewRow();
        }
        public StockLevel(StockLevelConfiguration[] config)
            : this()
        {
            m_config = config;
            loadConfiguration();
        }

        private void loadConfiguration()
        {
            m_StockLevelTable.Rows.Clear();
            foreach (StockLevelConfiguration conf in m_config)
            {
                DataRow row = m_StockLevelTable.NewRow();
                TransactionItems item = iERPTransactionClient.GetTransactionItems(conf.itemCode);
                row[0] = item == null ? "Error:Item does not exist" : conf.itemCode;
                row[1] = item == null ? "Error:Item does not exist" : item.Name;
                row[2] = item == null ? "Error:Item does not exist" : iERPTransactionClient.GetMeasureUnit(item.MeasureUnitID).Name;
                row[3] = conf.minimum;
                row[4] = conf.maximum;
                row[5] = conf.dailyUse;
                m_StockLevelTable.Rows.Add(row);
                gridControlLevel.DataSource = m_StockLevelTable;
            }
        }

        private void prepareDataTable()
        {
            m_StockLevelTable.Columns.Add("Item Code", typeof(string));
            m_StockLevelTable.Columns.Add("Name", typeof(string));
            m_StockLevelTable.Columns.Add("Unit", typeof(string));
            m_StockLevelTable.Columns.Add("Minimum", typeof(double));
            m_StockLevelTable.Columns.Add("Maximum", typeof(double));
            m_StockLevelTable.Columns.Add("Daily Use", typeof(double));
            gridControlLevel.DataSource = m_StockLevelTable;
            gridViewLevel.Columns["Name"].OptionsColumn.AllowEdit = false;
            gridViewLevel.Columns["Unit"].OptionsColumn.AllowEdit = false;
        }

       
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridControlLevel_ProcessGridKey(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
                return;

        }

        private void gridViewLevel_KeyDown(object sender, KeyEventArgs e)
        {

            GridView grid = null;
            if (sender is GridView)
                grid = sender as GridView;
            else if (sender is BaseEdit)
                grid = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;


            if (e.KeyCode == Keys.Enter)
            {
                if (gridViewLevel.FocusedColumn.FieldName.Equals("Item Code"))
                {
                    BeginInvoke(new MethodInvoker(() =>
                    {
                        int col = grid.Columns["Minimum"].VisibleIndex;
                        grid.FocusedColumn = grid.VisibleColumns[col];
                        grid.ShowEditor();
                    }));
                }
                if (gridViewLevel.FocusedColumn.FieldName.Equals("Maximum") && !gridViewLevel.HasColumnErrors)
                {
                    BeginInvoke(new MethodInvoker(() =>
                    {
                        grid.AddNewRow();
                        grid.MoveNext();
                        int col = grid.Columns["Item Code"].VisibleIndex;
                        grid.FocusedColumn = grid.VisibleColumns[col];
                        grid.ShowEditor();
                    }));
                }
            }
        }

        private bool itemAlreadyExists(string code,int rowHandle)
        {
            for (int i = 0; i < gridViewLevel.DataRowCount; i++)
            {
                if (i != rowHandle)
                {
                    if(code.ToLower().Equals(((string)gridViewLevel.GetRowCellValue(i, "Item Code")).ToLower()))
                        return true;
                }
            }
            return false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            List<StockLevelConfiguration> configList = new List<StockLevelConfiguration>();
            for (int i = 0; i < gridViewLevel.DataRowCount; i++)
            {
                string itemCode = gridViewLevel.GetRowCellValue(i, "Item Code") is DBNull ? "" : (string)gridViewLevel.GetRowCellValue(i, "Item Code");
                double minimum = gridViewLevel.GetRowCellValue(i, "Minimum") is DBNull ? 0 : (double)gridViewLevel.GetRowCellValue(i, "Minimum");
                double maximum = gridViewLevel.GetRowCellValue(i, "Maximum") is DBNull ? 0 : (double)gridViewLevel.GetRowCellValue(i, "Maximum");
                double dailyUse = gridViewLevel.GetRowCellValue(i, "Daily Use") is DBNull ? 0 : (double)gridViewLevel.GetRowCellValue(i, "Daily Use");
                if(string.IsNullOrEmpty(itemCode))
                {
                    DevExMessageBox.ShowErrorMessage("Item Code cannot be empty for row no." + (i + 1));
                    return;
                }
                if(minimum==0 && maximum==0)
                {
                    DevExMessageBox.ShowErrorMessage("Both minimum and maximum values cannot be empty for row no." + (i + 1));
                    return;
                }
                else if (minimum >= maximum)
                {
                    DevExMessageBox.ShowErrorMessage("Minimum value cannot be greater or equal to maximum value for row no." + (i + 1));
                    return;
                }
                StockLevelConfiguration conf = new StockLevelConfiguration();
                conf.itemCode = itemCode;
                conf.minimum = minimum;
                conf.maximum = maximum;
                conf.dailyUse = dailyUse;
                configList.Add(conf);
            }
            try
            {
                iERPTransactionClient.SaveStockLevelConfiguration(configList.ToArray());
                DevExMessageBox.ShowSuccessMessage("Successfully saved!");
            }
            catch(Exception ex)
            {
                DevExMessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void gridControlLevel_EditorKeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void gridViewLevel_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void gridViewLevel_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {

            if (e.Column.FieldName.Equals("Item Code"))
            {
                string itemCode = (string)gridViewLevel.GetFocusedRowCellValue("Item Code");
                TransactionItems item = iERPTransactionClient.GetTransactionItems(itemCode);
                if (item == null)
                {
                    DevExMessageBox.ShowErrorMessage("Item does not exist");
                    FocusItemCodeCell();
                    gridViewLevel.SetColumnError(gridViewLevel.Columns["Item Code"], "Item does not exist",DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical);
                }
                else
                {
                    if (itemAlreadyExists(item.Code, gridViewLevel.FocusedRowHandle))
                    {
                        DevExMessageBox.ShowErrorMessage("Item Code: " + item.Code + " already exists");
                        FocusItemCodeCell();
                        gridViewLevel.SetColumnError(gridViewLevel.Columns["Item Code"], "Item already exists", DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical);
                    }
                    else
                    {
                        if (!item.IsInventoryItem)
                        {
                            DevExMessageBox.ShowErrorMessage("Item Code: " + item.Code + " is not inventory item");
                            FocusItemCodeCell();
                            gridViewLevel.SetColumnError(gridViewLevel.Columns["Item Code"], "Item is not inventory item", DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical);
                        }
                        else
                        {
                            gridViewLevel.SetRowCellValue(gridViewLevel.FocusedRowHandle, "Name", item.Name);
                            gridViewLevel.SetRowCellValue(gridViewLevel.FocusedRowHandle, "Unit", iERPTransactionClient.GetMeasureUnit(item.MeasureUnitID).Name);
                            gridViewLevel.ClearColumnErrors();
                        }
                    }

                }
            }
        }

        private void FocusItemCodeCell()
        {
            BeginInvoke(new MethodInvoker(() =>
            {
                int col = gridViewLevel.Columns["Item Code"].VisibleIndex;
                gridViewLevel.FocusedColumn = gridViewLevel.VisibleColumns[col];
                gridViewLevel.ShowEditor();
                gridViewLevel.ActiveEditor.SelectAll();
            }));
        }


        private void gridViewLevel_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            
        }

        private void gridViewLevel_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (gridViewLevel.HasColumnErrors)
                e.Valid = false;
        }
    }
    
}
