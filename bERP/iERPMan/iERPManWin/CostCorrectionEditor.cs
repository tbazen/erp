﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BIZNET.iERP;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERPMan.Client
{
    public partial class CostCorrectionEditor : DevExpress.XtraEditors.XtraForm
    {
        SummaryInformation _summary;
        long _actualCostTicks;
        Dictionary<string, DevExpress.XtraTreeList.Nodes.TreeListNode> nodes = new Dictionary<string, DevExpress.XtraTreeList.Nodes.TreeListNode>();
        public CostCorrectionEditor()
        {
            InitializeComponent();
            _actualCostTicks = -1;
            colCost.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.Decimal;
            _summary = iERPManClient.getCostReportingScheme();
            
            addChilds(treeAccounts.Nodes, _summary.rootSection);
        }

        void addChilds(DevExpress.XtraTreeList.Nodes.TreeListNodes nodes, SummarySection section)
        {
            foreach (SummaryItemBase it in section.summaryItem)
            {

                SummaryItemInfo itinf = it as SummaryItemInfo;
                DevExpress.XtraTreeList.Nodes.TreeListNode n = nodes.Add(new object[] { itinf.tag, itinf.label, null });
                n.Tag = itinf.tag;
                if (it is SummarySection)
                {
                    addChilds(n.Nodes, (SummarySection)it);
                }
                else
                    this.nodes.Add(itinf.tag, n);
            }
        }        
        public CostCorrectionEditor(long actualCostTicks)
            : this()
        {
            _actualCostTicks = actualCostTicks;
            AdjustedCostHeader header = iERPManClient.getActualCostHeader(actualCostTicks);
            
            dateFrom.DateTime = new DateTime( header.ticksFrom);
            dateTo.DateTime = new DateTime(header.ticksTo);

            foreach (AdjustedCostDetail e in iERPManClient.getActualCostDetail(actualCostTicks))
            {
                setEntry(e);
            }
        }

        private void setEntry(AdjustedCostDetail e)
        {
            DevExpress.XtraTreeList.Nodes.TreeListNode n;
            if(!nodes.TryGetValue(e.itemTag,out n))
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Item tag not found tag: " + e.itemTag);
                return;
            }
            n.SetValue(colCost.FieldName, e.cost);
        }

        bool getAdjustmentEntries(DevExpress.XtraTreeList.Nodes.TreeListNodes nodes, List<AdjustedCostDetail> entries)
        {
            foreach(DevExpress.XtraTreeList.Nodes.TreeListNode n in nodes)
            {
                object _am=n.GetValue(2);
                double amount;
                if (n.Tag is string && _am != null && double.TryParse(_am.ToString(),out amount) && AccountBase.AmountGreater(amount,0))
                {

                    entries.Add(new AdjustedCostDetail()
                    {
                        itemTag=(string)n.Tag,
                        cost=amount,
                    });
                }
                if (!getAdjustmentEntries(n.Nodes, entries))
                    return false;
            }
            return true;
        }
        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                List<AdjustedCostDetail> entries = new List<AdjustedCostDetail>();
                if (!getAdjustmentEntries(this.treeAccounts.Nodes, entries))
                    return;

                AdjustedCostHeader header = new AdjustedCostHeader();
                header.ticksFrom = dateFrom.DateTime.Ticks;
                header.ticksTo = dateTo.DateTime.AddDays(1).Ticks;
                iERPManClient.setActualCost(header, entries.ToArray());
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null,ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        const int MAX_CHILD = 500;
        private void treeAccounts_BeforeExpand(object sender, DevExpress.XtraTreeList.BeforeExpandEventArgs e)
        {
            
        }
        bool _ignoreChangeEvent = false;
        private void treeAccounts_CellValueChanged(object sender, DevExpress.XtraTreeList.CellValueChangedEventArgs e)
        {
            if (_ignoreChangeEvent)
                return;
            DevExpress.XtraTreeList.Nodes.TreeListNode parent = e.Node.ParentNode;
            while (parent != null)
            {
                parent.SetValue(2,0);
                parent = parent.ParentNode;
            }
            if(e.Node.HasChildren)
            {
                DevExpress.XtraTreeList.Nodes.TreeListNode child=e.Node.FirstNode;
                while (child!=null)
                {
                    child.SetValue(2, 0);
                    child = child.NextNode;
                }
            }
        }
        
    }
}