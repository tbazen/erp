﻿namespace BIZNET.iERPMan.Client
{
    partial class CostCorrectionEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.dateTo = new INTAPS.Ethiopic.DualCalendar();
            this.dateFrom = new INTAPS.Ethiopic.DualCalendar();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.treeAccounts = new DevExpress.XtraTreeList.TreeList();
            this.colItemCode = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colItem = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colCost = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeAccounts)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.buttonOk);
            this.panelControl1.Controls.Add(this.buttonCancel);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 334);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(781, 33);
            this.panelControl1.TabIndex = 2;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(613, 4);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 0;
            this.buttonOk.Text = "Ok";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(694, 4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 22);
            this.buttonCancel.TabIndex = 0;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.dateTo);
            this.panelControl2.Controls.Add(this.dateFrom);
            this.panelControl2.Controls.Add(this.labelControl4);
            this.panelControl2.Controls.Add(this.labelControl3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(781, 91);
            this.panelControl2.TabIndex = 4;
            // 
            // dateTo
            // 
            this.dateTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTo.Location = new System.Drawing.Point(418, 24);
            this.dateTo.Name = "dateTo";
            this.dateTo.ShowEthiopian = true;
            this.dateTo.ShowGregorian = true;
            this.dateTo.ShowTime = false;
            this.dateTo.Size = new System.Drawing.Size(351, 40);
            this.dateTo.TabIndex = 3;
            this.dateTo.VerticalLayout = true;
            // 
            // dateFrom
            // 
            this.dateFrom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateFrom.Location = new System.Drawing.Point(12, 24);
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.ShowEthiopian = true;
            this.dateFrom.ShowGregorian = true;
            this.dateFrom.ShowTime = false;
            this.dateFrom.Size = new System.Drawing.Size(351, 40);
            this.dateFrom.TabIndex = 3;
            this.dateFrom.VerticalLayout = true;
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl4.Location = new System.Drawing.Point(418, 1);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(14, 13);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "To";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Location = new System.Drawing.Point(12, 5);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(29, 13);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "From";
            // 
            // treeAccounts
            // 
            this.treeAccounts.Appearance.FocusedRow.BackColor = System.Drawing.Color.PaleGreen;
            this.treeAccounts.Appearance.FocusedRow.Options.UseBackColor = true;
            this.treeAccounts.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeAccounts.Appearance.Row.Options.UseFont = true;
            this.treeAccounts.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colItemCode,
            this.colItem,
            this.colCost});
            this.treeAccounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeAccounts.Location = new System.Drawing.Point(0, 91);
            this.treeAccounts.Name = "treeAccounts";
            this.treeAccounts.OptionsSelection.MultiSelect = true;
            this.treeAccounts.RowHeight = 25;
            this.treeAccounts.Size = new System.Drawing.Size(781, 243);
            this.treeAccounts.TabIndex = 12;
            this.treeAccounts.BeforeExpand += new DevExpress.XtraTreeList.BeforeExpandEventHandler(this.treeAccounts_BeforeExpand);
            this.treeAccounts.CellValueChanged += new DevExpress.XtraTreeList.CellValueChangedEventHandler(this.treeAccounts_CellValueChanged);
            // 
            // colItemCode
            // 
            this.colItemCode.Caption = "Cost Item Code";
            this.colItemCode.FieldName = "costItemCode";
            this.colItemCode.Name = "colItemCode";
            this.colItemCode.OptionsColumn.ReadOnly = true;
            this.colItemCode.Visible = true;
            this.colItemCode.VisibleIndex = 0;
            this.colItemCode.Width = 146;
            // 
            // colItem
            // 
            this.colItem.Caption = "Cost Item";
            this.colItem.FieldName = "Cost Item";
            this.colItem.Name = "colItem";
            this.colItem.OptionsColumn.ReadOnly = true;
            this.colItem.Visible = true;
            this.colItem.VisibleIndex = 1;
            this.colItem.Width = 245;
            // 
            // colCost
            // 
            this.colCost.Caption = "Cost";
            this.colCost.FieldName = "Cost";
            this.colCost.Name = "colCost";
            this.colCost.RowFooterSummaryStrFormat = "";
            this.colCost.SummaryFooterStrFormat = "";
            this.colCost.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            this.colCost.Visible = true;
            this.colCost.VisibleIndex = 2;
            this.colCost.Width = 372;
            // 
            // CostCorrectionEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(781, 367);
            this.Controls.Add(this.treeAccounts);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "CostCorrectionEditor";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Actual Cost Editor";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeAccounts)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl apanelControl1;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private INTAPS.Ethiopic.DualCalendar dateFrom;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraTreeList.TreeList treeAccounts;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colItem;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colCost;
        private INTAPS.Ethiopic.DualCalendar dateTo;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colItemCode;
    }
}