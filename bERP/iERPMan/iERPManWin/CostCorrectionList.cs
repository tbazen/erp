﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Web;
using BIZNET.iERP;
using BIZNET.iERP.Client;
using INTAPS;
namespace BIZNET.iERPMan.Client
{
    public partial class CostCorrectionList: DevExpress.XtraEditors.XtraForm
    {
        AdjustedCostHeader _currentAdjustment = null;
        public CostCorrectionList()
        {
            InitializeComponent();
            htmlTool.setBrowser(htmlBrowser);
            reloadAdjustmentList();
            loadCurrentAdjustment(_currentAdjustment);
        }

        private void loadCurrentAdjustment(AdjustedCostHeader b)
        {
            _currentAdjustment = b;
            if (_currentAdjustment == null)
            {
                htmlBrowser.LoadTextPage("Adjustment", "");
                buttonEdit.Enabled = buttonDelete.Enabled = false;
                return;
            }
            long today = DateTime.Today.Ticks;
            long target=today<b.ticksTo?today:b.ticksTo;
            StringBuilder sb = new StringBuilder();

            generateTitleForBudget(sb, new DateTime(b.ticksFrom), new DateTime(b.ticksTo));

            buttonEdit.Enabled = buttonDelete.Enabled = true;
            try
            {
                string ws = System.Configuration.ConfigurationManager.AppSettings["webserver"];
                if (string.IsNullOrEmpty(ws))
                {
                    htmlBrowser.DocumentText = "<b>Web server configuration not set</b>";
                    return;
                }
                htmlBrowser.Navigate(ws + "ierpmanservice/production.html?sid={0}&&ticksFrom={1}&&ticksTo={2}&&adj=true".format(INTAPS.ClientServer.Client.ApplicationClient.SessionID,b.ticksFrom,b.ticksTo));
            }
            catch (Exception ex)
            {
                htmlBrowser.LoadTextPage("Adjustment", string.Format("<h2>Error Generating Adjusted Costing Report</h2>{0}", System.Web.HttpUtility.HtmlEncode(ex.Message)));
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            CostCorrectionEditor be = new CostCorrectionEditor();
            if(be.ShowDialog(this)==DialogResult.OK)
            {
                reloadAdjustmentList();
            }
        }

        private void reloadAdjustmentList()
        {
            listView.Items.Clear();
            buttonAdd.Enabled = true;
            try
            {
                ListViewItem selected=null;
                foreach (AdjustedCostHeader b in iERPManClient.getActualCosts(-1,-1))
                {
                    ListViewItem li = new ListViewItem(new DateTime(b.ticksFrom).ToString("MMM dd,yyyy"));
                    li.SubItems.Add(new DateTime(b.ticksTo).ToString("MMM dd,yyyy"));
                    listView.Items.Add(li);
                    li.Tag = b;
                    if (_currentAdjustment != null && b.ticksFrom== _currentAdjustment.ticksFrom)
                        selected = li;
                }
                if (selected != null)
                    selected.Selected = true;
                else
                {
                    if (listView.Items.Count > 0)
                        listView.Items[0].Selected = true;
                    else 
                        loadCurrentAdjustment(null);
                }
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
        private void generateTitleForBudget(StringBuilder builder, DateTime from,DateTime to)
        {
            TSConstants.addCompanyNameLine(iERPTransactionClient.GetCompanyProfile(), builder);
            builder.Append("<h2>Adjusted Cost Report</h2>");
            builder.Append(string.Format("<br/><span class='SubTitle'><b>From </b> <u>{0}</u><b> to </b> <u>{1}</u></span>"
                , HttpUtility.HtmlEncode(AccountBase.FormatDate(from))
                , HttpUtility.HtmlEncode(AccountBase.FormatDate(to))));
        }
        private void costCenterPlaceHolder_AccountChanged(object sender, EventArgs e)
        {
            reloadAdjustmentList();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (_currentAdjustment == null)
                return;
            if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete the selected cost adjustment?"))
                return;
            try
            {
                iERPManClient.deleteActualCost(_currentAdjustment.ticksFrom);
                reloadAdjustmentList();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }


        }

        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView.SelectedItems.Count == 0)
                loadCurrentAdjustment(null);
            else
                loadCurrentAdjustment(listView.SelectedItems[0].Tag as AdjustedCostHeader);
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (_currentAdjustment == null)
                return;
            CostCorrectionEditor be = new CostCorrectionEditor(_currentAdjustment.ticksFrom);
            if (be.ShowDialog(this) == DialogResult.OK)
            {
                reloadAdjustmentList();
            }
        }


    }
}