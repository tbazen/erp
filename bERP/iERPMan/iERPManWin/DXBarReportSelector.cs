using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using DevExpress.XtraEditors;
namespace BIZNET.iERPMan.Client
{
    public class DXBarReportSelector : ReportSelectorBase, IReportSelector
    {
        DevExpress.XtraBars.BarSubItem m_dropDown;
        DevExpress.XtraBars.BarManager m_manager;
        public DXBarReportSelector(DevExpress.XtraBars.BarManager manager, DevExpress.XtraBars.BarSubItem button)
        {
            m_dropDown = button;
            m_manager = manager;
            INTAPS.Accounting.Client.AccountingClient.ReportChanged += new INTAPS.Accounting.Client.ReportChangedHandler(ToolStripDrowDownReportSelector_ReportChanged);


            DevExpress.XtraBars.BarButtonItem item;

            item = new DevExpress.XtraBars.BarButtonItem(manager, "Manage Reports");
            item.ItemClick += ShowReportManager;
            m_dropDown.ItemLinks.Add(item,true);

            item = new DevExpress.XtraBars.BarButtonItem(manager, "Refresh Menu");
            item.ItemClick += RefreshMenu;
            m_dropDown.ItemLinks.Add(item);
        }


        void RefreshMenu(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            while (m_dropDown.ItemLinks.Count > 2)
                m_dropDown.ItemLinks.RemoveAt(0);
            INTAPS.Accounting.Client.AccountingClient.PopulateReportSelector(-1, this);
        }

        void ShowReportManager(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ReportManager rm = new ReportManager();
            INTAPS.Accounting.Client.AccountingClient.PopulateReportSelector(-1, rm.selector);
            rm.ShowDialog();
            if (rm.Changed)
            {
                RefreshMenu(null, null);
            }
        }


        void ToolStripDrowDownReportSelector_ReportChanged(INTAPS.ClientServer.Client.DataChange change, ReportDefination def)
        {
            DevExpress.XtraBars.BarSubItem catItem = null;
            foreach (DevExpress.XtraBars.BarItemLink mi in this.m_dropDown.ItemLinks)
            {
                ReportCategory cat = ((DevExpress.XtraBars.BarItem)mi.LinkedObject).Tag as ReportCategory;
                if (cat == null)
                    continue;
                if (cat.id == def.categoryID)
                {
                    catItem = ((DevExpress.XtraBars.BarItem)mi.LinkedObject) as DevExpress.XtraBars.BarSubItem;
                    break;
                }
            }
            if (catItem == null)
                return;
            DevExpress.XtraBars.BarButtonItem reportItem = null;
            int removeAt=-1;
            if (change != INTAPS.ClientServer.Client.DataChange.Create)
            {
                foreach (DevExpress.XtraBars.BarItem mi in catItem.ItemLinks)
                {
                    removeAt++;
                    ReportDefination rd = mi.Tag as ReportDefination;
                    if (rd == null)
                        continue;
                    if (rd.id == def.id)
                    {
                        reportItem = mi as DevExpress.XtraBars.BarButtonItem;
                        break;
                    }
                }
                if (reportItem == null)
                    return;
                
            }
            switch (change)
            {
                case INTAPS.ClientServer.Client.DataChange.Create:
                    this.CreateReportItem(catItem, def, INTAPS.Accounting.Client.AccountingClient.reportClientHandlers.ContainsKey(def.reportTypeID));
                    break;
                case INTAPS.ClientServer.Client.DataChange.Update:
                    reportItem.Caption = def.name;
                    reportItem.Tag = def;
                    break;
                case INTAPS.ClientServer.Client.DataChange.Delete:
                    catItem.ItemLinks.RemoveAt(removeAt);
                    break;
            }
        }
        #region IReportSelector Members

        public object CreateCategoryItem(object parentItem, ReportCategory category)
        {
            //DevExpress.XtraBars.BarButtonItem
            DevExpress.XtraBars.BarSubItem paritem = new DevExpress.XtraBars.BarSubItem(m_manager, category.name);
            DevExpress.XtraBars.BarSubItem catParent = parentItem as DevExpress.XtraBars.BarSubItem;
            if (catParent == null)
                m_dropDown.LinksPersistInfo.Insert(m_dropDown.LinksPersistInfo.Count - 2, new DevExpress.XtraBars.LinkPersistInfo(paritem));
            else
            {
                catParent.LinksPersistInfo.Insert(catParent.LinksPersistInfo.Count - 1, new DevExpress.XtraBars.LinkPersistInfo(paritem));
            }
            paritem.Tag = category;

            DevExpress.XtraBars.BarButtonItem newItem = new DevExpress.XtraBars.BarButtonItem(m_manager, "New Report");
            newItem.ItemClick += OnCreateNewReport;
            newItem.Tag = category;
            paritem.LinksPersistInfo.Add(new DevExpress.XtraBars.LinkPersistInfo(newItem,true));
            return paritem;
        }

        public object CreateReportItem(object parentItem, ReportDefination rd, bool enabled)
        {
            DevExpress.XtraBars.BarButtonItem newItem = new DevExpress.XtraBars.BarButtonItem(m_manager, rd.name);
            newItem.Tag = rd;
            newItem.ItemClick += OnShowReport;
            newItem.Enabled = enabled;
            ToolStripMenuItem catParent = parentItem as ToolStripMenuItem;
            if (catParent == null)
                m_dropDown.LinksPersistInfo.Insert(m_dropDown.LinksPersistInfo.Count - 2, new DevExpress.XtraBars.LinkPersistInfo(newItem));
            else
                ((DevExpress.XtraBars.BarSubItem)parentItem).LinksPersistInfo.Insert(catParent.DropDown.Items.Count - 1, new DevExpress.XtraBars.LinkPersistInfo(newItem));

            return newItem;
        }

        void OnCreateNewReport(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                ReportCategory cat = e.Item.Tag as ReportCategory;
                ReportType t;
                if (base.SelectReportType(out t))
                    base.CreateReport(t.id, cat.id);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Failed to load report form", ex);
            }
        }

        void OnShowReport(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                ReportDefination def = e.Item.Tag as ReportDefination;
                base.ShowReport(def);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Failed to load report form", ex);
            }
        }

        #endregion
    }
}
