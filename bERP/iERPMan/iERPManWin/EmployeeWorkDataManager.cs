﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERPMan.Client
{
    public partial class EmployeeWorkDataManager : DevExpress.XtraEditors.XtraForm
    {
        private DataTable _workData;
        public EmployeeWorkDataManager()
        {
            InitializeComponent();
            _workData = new DataTable();
            PrepareWorkData();
            cmbMonth.Month = DateTime.Now.Month;
            spinYear.Value = (Decimal)DateTime.Now.Year;
            LoadWorkData();
        }

        private void LoadWorkData()
        {
            _workData.Rows.Clear();
            WorkDataResult[] result = iERPManClient.getWorkData(cmbMonth.Month, (int)spinYear.Value);
            foreach (WorkDataResult r in result)
            {
                DataRow row = _workData.NewRow();
                row[0] = r.date;
                row[1] = r.overTimeNote;
                row[2] = r.absenceNote;
                _workData.Rows.Add(row);
                gridControlData.DataSource = _workData;
            }
            gridViewData.BestFitColumns();
        }

        private void PrepareWorkData()
        {
            _workData.Columns.Add("Date", typeof(DateTime));
            _workData.Columns.Add("Total Overtime Hours", typeof(string));
            _workData.Columns.Add("Total Abscence Hours", typeof(string));
            gridControlData.DataSource = _workData;
        }

        private void cmbMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadWorkData();
        }

        private void btnNew_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RegisterWorkData data = new RegisterWorkData();
            if (data.ShowDialog(this) == DialogResult.OK)
                LoadWorkData();
        }

        private void btnEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DateTime date = (DateTime)gridViewData.GetRowCellValue(gridViewData.FocusedRowHandle, "Date");
            RegisterWorkData data = new RegisterWorkData(date);
            if (data.ShowDialog(this) == DialogResult.OK)
                LoadWorkData();
        }

        private void spinYear_EditValueChanged(object sender, EventArgs e)
        {
            LoadWorkData();
        }
    }
}
