﻿namespace BIZNET.iERPMan.Client
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barsiConfigure = new DevExpress.XtraBars.BarSubItem();
            this.bariMaterialCostReporting = new DevExpress.XtraBars.BarButtonItem();
            this.bariOtherCostReporting = new DevExpress.XtraBars.BarButtonItem();
            this.barLargeButtonItem6 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barLargeButtonItem1 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barLargeButtonItem7 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barLargeButtonItem8 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnStockLevel = new DevExpress.XtraBars.BarButtonItem();
            this.barsiReport = new DevExpress.XtraBars.BarSubItem();
            this.btnProductionManager = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnCostAdjustment = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.menPeriods = new DevExpress.XtraBars.BarSubItem();
            this.buttonCreatePeriod = new DevExpress.XtraBars.BarButtonItem();
            this.buttonClosePeriod = new DevExpress.XtraBars.BarButtonItem();
            this.btnWorkData = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barsiTransactions = new DevExpress.XtraBars.BarSubItem();
            this.barLargeButtonItem2 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barLargeButtonItem3 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barLargeButtonItem4 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barLargeButtonItem5 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.bariProcessTypes = new DevExpress.XtraBars.BarSubItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.comboPeriods = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.browser = new System.Windows.Forms.WebBrowser();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.barLargeButtonItem9 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barLargeButtonItem10 = new DevExpress.XtraBars.BarLargeButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboPeriods)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barsiTransactions,
            this.barsiConfigure,
            this.barsiReport,
            this.bariProcessTypes,
            this.barStaticItem1,
            this.bariMaterialCostReporting,
            this.bariOtherCostReporting,
            this.btnProductionManager,
            this.barLargeButtonItem2,
            this.barLargeButtonItem3,
            this.barLargeButtonItem4,
            this.barLargeButtonItem5,
            this.barLargeButtonItem6,
            this.barStaticItem2,
            this.buttonCreatePeriod,
            this.buttonClosePeriod,
            this.menPeriods,
            this.barLargeButtonItem1,
            this.barLargeButtonItem7,
            this.barLargeButtonItem8,
            this.btnStockLevel,
            this.btnCostAdjustment,
            this.btnWorkData});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 46;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.comboPeriods});
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barsiConfigure, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barsiReport, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnProductionManager, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnCostAdjustment, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem2, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.menPeriods),
            new DevExpress.XtraBars.LinkPersistInfo(this.buttonClosePeriod),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnWorkData)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barsiConfigure
            // 
            this.barsiConfigure.Caption = "Configure";
            this.barsiConfigure.Id = 14;
            this.barsiConfigure.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bariMaterialCostReporting),
            new DevExpress.XtraBars.LinkPersistInfo(this.bariOtherCostReporting),
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem6),
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem7),
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem8),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnStockLevel)});
            this.barsiConfigure.Name = "barsiConfigure";
            // 
            // bariMaterialCostReporting
            // 
            this.bariMaterialCostReporting.Caption = "Material Cost Reporting Scheme";
            this.bariMaterialCostReporting.Id = 24;
            this.bariMaterialCostReporting.Name = "bariMaterialCostReporting";
            this.bariMaterialCostReporting.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bariMaterialCostReporting_ItemClick);
            // 
            // bariOtherCostReporting
            // 
            this.bariOtherCostReporting.Caption = "Other Cost Reporting Scheme";
            this.bariOtherCostReporting.Id = 25;
            this.bariOtherCostReporting.Name = "bariOtherCostReporting";
            this.bariOtherCostReporting.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bariOtherCostReporting_ItemClick);
            // 
            // barLargeButtonItem6
            // 
            this.barLargeButtonItem6.Caption = "Process Types";
            this.barLargeButtonItem6.Id = 31;
            this.barLargeButtonItem6.Name = "barLargeButtonItem6";
            this.barLargeButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem6_ItemClick_1);
            // 
            // barLargeButtonItem1
            // 
            this.barLargeButtonItem1.Caption = "Quality Control ";
            this.barLargeButtonItem1.Id = 37;
            this.barLargeButtonItem1.Name = "barLargeButtonItem1";
            this.barLargeButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem1_ItemClick_1);
            // 
            // barLargeButtonItem7
            // 
            this.barLargeButtonItem7.Caption = "Factory Floor ";
            this.barLargeButtonItem7.Id = 40;
            this.barLargeButtonItem7.Name = "barLargeButtonItem7";
            this.barLargeButtonItem7.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem7_ItemClick_1);
            // 
            // barLargeButtonItem8
            // 
            this.barLargeButtonItem8.Caption = "Costing Settings";
            this.barLargeButtonItem8.Id = 41;
            this.barLargeButtonItem8.Name = "barLargeButtonItem8";
            this.barLargeButtonItem8.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem8_ItemClick);
            // 
            // btnStockLevel
            // 
            this.btnStockLevel.Caption = "Stock Level Setting";
            this.btnStockLevel.Id = 42;
            this.btnStockLevel.Name = "btnStockLevel";
            this.btnStockLevel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnStockLevel_ItemClick);
            // 
            // barsiReport
            // 
            this.barsiReport.Caption = "Report";
            this.barsiReport.Id = 17;
            this.barsiReport.Name = "barsiReport";
            // 
            // btnProductionManager
            // 
            this.btnProductionManager.Caption = "Production Batch Manager";
            this.btnProductionManager.Id = 26;
            this.btnProductionManager.Name = "btnProductionManager";
            this.btnProductionManager.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem1_ItemClick);
            // 
            // btnCostAdjustment
            // 
            this.btnCostAdjustment.Caption = "Cost Adjustment";
            this.btnCostAdjustment.Id = 43;
            this.btnCostAdjustment.Name = "btnCostAdjustment";
            this.btnCostAdjustment.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCostAdjustment_ItemClick);
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "Costing Period";
            this.barStaticItem2.Id = 32;
            this.barStaticItem2.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barStaticItem2.ItemAppearance.Normal.Options.UseFont = true;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // menPeriods
            // 
            this.menPeriods.Caption = "[No Period Selected]";
            this.menPeriods.Id = 36;
            this.menPeriods.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.buttonCreatePeriod, true)});
            this.menPeriods.Name = "menPeriods";
            // 
            // buttonCreatePeriod
            // 
            this.buttonCreatePeriod.Caption = "New Period";
            this.buttonCreatePeriod.Id = 34;
            this.buttonCreatePeriod.Name = "buttonCreatePeriod";
            this.buttonCreatePeriod.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.buttonCreatePeriod_ItemClick);
            // 
            // buttonClosePeriod
            // 
            this.buttonClosePeriod.Caption = "Close Period";
            this.buttonClosePeriod.Id = 35;
            this.buttonClosePeriod.Name = "buttonClosePeriod";
            this.buttonClosePeriod.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.buttonClosePeriod_ItemClick);
            // 
            // btnWorkData
            // 
            this.btnWorkData.Caption = "Overtime/Abscence";
            this.btnWorkData.Id = 45;
            this.btnWorkData.Name = "btnWorkData";
            this.btnWorkData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnWorkData_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(858, 22);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 412);
            this.barDockControlBottom.Size = new System.Drawing.Size(858, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 390);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(858, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 390);
            // 
            // barsiTransactions
            // 
            this.barsiTransactions.Caption = "Transactions";
            this.barsiTransactions.Id = 5;
            this.barsiTransactions.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem3),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barLargeButtonItem4, DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu),
            new DevExpress.XtraBars.LinkPersistInfo(this.barLargeButtonItem5)});
            this.barsiTransactions.Name = "barsiTransactions";
            // 
            // barLargeButtonItem2
            // 
            this.barLargeButtonItem2.Caption = "Good Reieved";
            this.barLargeButtonItem2.Id = 27;
            this.barLargeButtonItem2.Name = "barLargeButtonItem2";
            this.barLargeButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem2_ItemClick);
            // 
            // barLargeButtonItem3
            // 
            this.barLargeButtonItem3.Caption = "Sales";
            this.barLargeButtonItem3.Id = 28;
            this.barLargeButtonItem3.Name = "barLargeButtonItem3";
            // 
            // barLargeButtonItem4
            // 
            this.barLargeButtonItem4.Caption = "Store Transfer";
            this.barLargeButtonItem4.Id = 29;
            this.barLargeButtonItem4.Name = "barLargeButtonItem4";
            // 
            // barLargeButtonItem5
            // 
            this.barLargeButtonItem5.Caption = "Store Issue";
            this.barLargeButtonItem5.Id = 30;
            this.barLargeButtonItem5.Name = "barLargeButtonItem5";
            // 
            // bariProcessTypes
            // 
            this.bariProcessTypes.Caption = "Process Types";
            this.bariProcessTypes.Id = 18;
            this.bariProcessTypes.Name = "bariProcessTypes";
            this.bariProcessTypes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barSubItem7_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "_";
            this.barStaticItem1.Id = 23;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barStaticItem1_ItemClick);
            // 
            // comboPeriods
            // 
            this.comboPeriods.AutoHeight = false;
            this.comboPeriods.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboPeriods.Name = "comboPeriods";
            // 
            // browser
            // 
            this.browser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browser.Location = new System.Drawing.Point(0, 22);
            this.browser.MinimumSize = new System.Drawing.Size(20, 20);
            this.browser.Name = "browser";
            this.browser.Size = new System.Drawing.Size(858, 390);
            this.browser.TabIndex = 4;
            this.browser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.browser_DocumentCompleted);
            // 
            // barLargeButtonItem9
            // 
            this.barLargeButtonItem9.Caption = "Production Batch Manager";
            this.barLargeButtonItem9.Id = 26;
            this.barLargeButtonItem9.Name = "barLargeButtonItem9";
            // 
            // barLargeButtonItem10
            // 
            this.barLargeButtonItem10.Caption = "Production Batch Manager";
            this.barLargeButtonItem10.Id = 26;
            this.barLargeButtonItem10.Name = "barLargeButtonItem10";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 412);
            this.Controls.Add(this.browser);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BIZNET Cost Accounting";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboPeriods)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarSubItem barsiTransactions;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarSubItem barsiConfigure;
        private DevExpress.XtraBars.BarSubItem bariProcessTypes;
        private DevExpress.XtraBars.BarSubItem barsiReport;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarButtonItem bariMaterialCostReporting;
        private DevExpress.XtraBars.BarButtonItem bariOtherCostReporting;
        private System.Windows.Forms.WebBrowser browser;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraBars.BarLargeButtonItem btnProductionManager;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem2;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem3;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem4;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem5;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem6;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox comboPeriods;
        private DevExpress.XtraBars.BarButtonItem buttonCreatePeriod;
        private DevExpress.XtraBars.BarButtonItem buttonClosePeriod;
        private DevExpress.XtraBars.BarSubItem menPeriods;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem1;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem7;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem8;
        private DevExpress.XtraBars.BarButtonItem btnStockLevel;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem9;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem10;
        private DevExpress.XtraBars.BarButtonItem btnCostAdjustment;
        private DevExpress.XtraBars.BarButtonItem btnWorkData;


    }
}