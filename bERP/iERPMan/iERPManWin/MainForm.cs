﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using DevExpress.XtraEditors;
using BIZNET.iERP;
using BIZNET.iERP.Client;

namespace BIZNET.iERPMan.Client
{
    public partial class MainForm : XtraForm
    {
        public static MainForm theInstance;
        public MainForm()
        {
            InitializeComponent();
            btnProductionManager.Visibility = iERPManClient.canViewProductionBatchList() ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.Never;
            btnWorkData.Visibility = iERPManClient.canEditTimeSheet() ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.Never;
            btnCostAdjustment.Visibility = iERPManClient.canAdjustCost() ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.Never;
            barsiConfigure.Visibility = iERPManClient.canConfigure() ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.Never;
            buttonClosePeriod.Visibility = iERPManClient.canClosePeriod() ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.Never;
            buttonCreatePeriod.Visibility = iERPManClient.canOpenPeriod() ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.Never;
            AccountingClient.PopulateReportSelector(-1, new DXBarReportSelector(barManager1, this.barsiReport));
            loadPeriods(null);
            theInstance = this;
            loadDashboard();
        }
        void loadDashboard()
        {
            string ws = System.Configuration.ConfigurationManager.AppSettings["webserver"];
            if (string.IsNullOrEmpty(ws))
            {
                browser.DocumentText = "<b>Web server configuration not set</b>";
                return;
            }
            browser.Navigate(ws + "ierpmanservice/production.html?sid=" + INTAPS.ClientServer.Client.ApplicationClient.SessionID);
        }
        private void barSubItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void barStaticItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void bariMaterialCostReporting_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BIZNET.iERP.SummaryInformation rep = iERPManClient.GetSystemParameter("materialCostReportingScheme") as BIZNET.iERP.SummaryInformation;
            if (rep == null)
                rep = new iERP.SummaryInformation();
            BIZNET.iERP.Client.StatementDesigner designer = new iERP.Client.StatementDesigner(rep);
            designer.Show(this);
            designer.DesignUpdated += delegate(object o, EventArgs ee)
            {
                iERPManClient.SetSystemParameter("materialCostReportingScheme", designer.summaryInformation);
            };
        }

        private void barLargeButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            QualityControlTestRecord Qt = new QualityControlTestRecord();
            Qt.Show(this);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ProductionBatchManager manager = new ProductionBatchManager();
            manager.Show();
        }

        private void barLargeButtonItem6_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ProcessList pl = new ProcessList();
            pl.Show(this);
        }

        private void bariOtherCostReporting_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BIZNET.iERP.SummaryInformation rep = iERPManClient.GetSystemParameter("otherCostReportingScheme") as BIZNET.iERP.SummaryInformation;
            if (rep == null)
                rep = new iERP.SummaryInformation();
            BIZNET.iERP.Client.StatementDesigner designer = new iERP.Client.StatementDesigner(rep);
            designer.Show(this);
            designer.DesignUpdated += delegate(object o, EventArgs ee)
            {
                iERPManClient.SetSystemParameter("otherCostReportingScheme", designer.summaryInformation);
            };
        }
        void loadPeriods(CostingPeriod selectperiod)
        {

            while (menPeriods.ItemLinks.Count > 1)
                menPeriods.ItemLinks.RemoveAt(0);

            CostingPeriod[] period = iERPManClient.getOpenCostingPeriods(0, -1, -1);
            foreach (CostingPeriod p in period)
            {
                DevExpress.XtraBars.BarButtonItem b = new DevExpress.XtraBars.BarButtonItem(barManager1, p.name);
                b.Tag = p;
                b.ItemClick += periodSelected;
                menPeriods.ItemLinks.Insert(menPeriods.LinksPersistInfo.Count - 1, b);
            }
            if (period.Length > 0)
            {
                if (selectperiod == null)
                    this.CurrentPeriod = period[period.Length - 1];
                else
                {
                    this.CurrentPeriod = selectperiod;
                }
            }
            else
                this.CurrentPeriod = null;
            updateClosePeriodEnabled();
        }
        void updateClosePeriodEnabled()
        {
            CostingPeriod p = this.CurrentPeriod;
            buttonClosePeriod.Enabled = p != null && !p.closed;
        }
        public CostingPeriod CurrentPeriod
        {
            get
            {
                return menPeriods.Tag as CostingPeriod;
            }
            set
            {
                if (value != null)
                {

                    foreach (DevExpress.XtraBars.LinkPersistInfo i in menPeriods.LinksPersistInfo)
                    {
                        CostingPeriod cp = i.Item.Tag as CostingPeriod;
                        if (cp == null)
                            continue;
                        if (cp.t1 == value.t1)
                        {
                            menPeriods.Caption = value.name;
                            menPeriods.Tag = value;
                            return;
                        }
                    }
                }
                menPeriods.Caption = "[Period Not Selected]";
                menPeriods.Tag = null;
            }
        }
        private void buttonCreatePeriod_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            iERP.Client.DatePickerDialog dp = new iERP.Client.DatePickerDialog();
            if (dp.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    CostingPeriod p = iERPManClient.openCostingPeriod(0, dp.DateTime.Ticks);
                    loadPeriods(p);
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
                }
            }
        }

        private void periodSelected(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CostingPeriod s = e.Item.Tag as CostingPeriod;
            this.CurrentPeriod = s;
            updateClosePeriodEnabled();
        }

        private void buttonClosePeriod_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (new EstimationProgress(this.CurrentPeriod.t1).ShowDialog(this) == DialogResult.OK)
            {
                loadPeriods(null);
            }
        }

        private void barLargeButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // ConfigureItemandCategory CIC = new ConfigureItemandCategory();
            // CIC.ShowDialog(this);
        }

        private void barLargeButtonItem1_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            QualityControlTestRecord Qt = new QualityControlTestRecord();
            Qt.ShowDialog(this);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
        }

        private void barLargeButtonItem7_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RegisterFactoryFloorRecord FF = new RegisterFactoryFloorRecord();
            FF.ShowDialog(this);
        }

        private void barLargeButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void barLargeButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Costing_Setting cst = new Costing_Setting();
            cst.Show();
        }

        private void btnStockLevel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            StockLevelConfiguration[] conf = iERPTransactionClient.getStockLevelConfiguration();
            if (conf.Length == 0)
            {
                StockLevel stockLevel = new StockLevel();
                stockLevel.Show();
            }
            else
            {
                StockLevel level = new StockLevel(conf);
                level.Show();
            }
        }

        private void btnCostAdjustment_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CostCorrectionList list = new CostCorrectionList();
            list.Show(this);
        }

        private void btnWorkData_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EmployeeWorkDataManager dataManager = new EmployeeWorkDataManager();
            dataManager.Show();
        }

        private void browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }
    }
}


