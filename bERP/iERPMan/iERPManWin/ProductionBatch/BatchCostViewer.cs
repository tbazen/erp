﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using INTAPS.ClientServer.Client;

namespace BIZNET.iERPMan.Client
{
    public partial class BatchCostViewer : Form
    {
        public BatchCostViewer(int batchID)
        {
            InitializeComponent();

            webBrowser.Navigate(System.Configuration.ConfigurationManager.AppSettings["webserver"] + "ierpmanservice/single_batch.html?sid=" + ApplicationClient.SessionID + "&batchID=" + batchID);
        }
    }
}
