﻿namespace BIZNET.iERPMan.Client
{
    partial class BatchProcessPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.chkInclude = new DevExpress.XtraEditors.CheckEdit();
            this.storePicker = new BIZNET.iERP.Client.StorePlaceholder();
            this.dateEnd = new INTAPS.Ethiopic.DualCalendar();
            this.dateStart = new INTAPS.Ethiopic.DualCalendar();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.itemGrid = new BIZNET.iERP.Client.ItemGrid();
            this.gridViewItems = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storePicker.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewItems)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.chkInclude);
            this.layoutControl1.Controls.Add(this.storePicker);
            this.layoutControl1.Controls.Add(this.dateEnd);
            this.layoutControl1.Controls.Add(this.dateStart);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(895, 369, 250, 350);
            this.layoutControl1.OptionsView.HighlightFocusedItem = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(720, 152);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // chkInclude
            // 
            this.chkInclude.Location = new System.Drawing.Point(12, 12);
            this.chkInclude.Name = "chkInclude";
            this.chkInclude.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInclude.Properties.Appearance.Options.UseFont = true;
            this.chkInclude.Properties.Caption = "Include";
            this.chkInclude.Size = new System.Drawing.Size(696, 19);
            this.chkInclude.StyleController = this.layoutControl1;
            this.chkInclude.TabIndex = 9;
            this.chkInclude.CheckedChanged += new System.EventHandler(this.chkInclude_CheckedChanged);
            // 
            // storePicker
            // 
            this.storePicker.Location = new System.Drawing.Point(78, 120);
            this.storePicker.Name = "storePicker";
            this.storePicker.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.storePicker.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.storePicker.Size = new System.Drawing.Size(280, 20);
            this.storePicker.StyleController = this.layoutControl1;
            this.storePicker.TabIndex = 8;
            // 
            // dateEnd
            // 
            this.dateEnd.Location = new System.Drawing.Point(365, 54);
            this.dateEnd.Name = "dateEnd";
            this.dateEnd.ShowEthiopian = true;
            this.dateEnd.ShowGregorian = true;
            this.dateEnd.ShowTime = true;
            this.dateEnd.Size = new System.Drawing.Size(340, 63);
            this.dateEnd.TabIndex = 6;
            this.dateEnd.VerticalLayout = true;
            // 
            // dateStart
            // 
            this.dateStart.Location = new System.Drawing.Point(15, 54);
            this.dateStart.Name = "dateStart";
            this.dateStart.ShowEthiopian = true;
            this.dateStart.ShowGregorian = true;
            this.dateStart.ShowTime = true;
            this.dateStart.Size = new System.Drawing.Size(340, 63);
            this.dateStart.TabIndex = 5;
            this.dateStart.VerticalLayout = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(720, 152);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.dateStart;
            this.layoutControlItem2.CustomizationFormText = "Start Time:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(350, 85);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem2.Text = "Start Time:";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(63, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.dateEnd;
            this.layoutControlItem3.CustomizationFormText = "End Time:";
            this.layoutControlItem3.Location = new System.Drawing.Point(350, 23);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(350, 85);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "End Time:";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(63, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.storePicker;
            this.layoutControlItem1.CustomizationFormText = "Store:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 108);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(350, 24);
            this.layoutControlItem1.Text = "Store:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(63, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(350, 108);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(350, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.chkInclude;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(700, 23);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // itemGrid
            // 
            this.itemGrid.AllowPriceEdit = false;
            this.itemGrid.AllowRepeatedItem = false;
            this.itemGrid.AllowUnitPriceEdit = false;
            this.itemGrid.AutoUnitPrice = true;
            this.itemGrid.CurrentBalanceCaption = "Current Balance";
            this.itemGrid.DirectExpenseCaption = "Direct Issue?";
            this.itemGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.itemGrid.GroupByCostCenter = false;
            this.itemGrid.Location = new System.Drawing.Point(0, 152);
            this.itemGrid.MainView = this.gridViewItems;
            this.itemGrid.Name = "itemGrid";
            this.itemGrid.PrepaidCaption = "Prepaid?";
            this.itemGrid.PriceCaption = "Price";
            this.itemGrid.QuantityCaption = "Quantity";
            this.itemGrid.ReadOnly = false;
            this.itemGrid.ShowCostCenterColumn = false;
            this.itemGrid.ShowCurrentBalanceColumn = true;
            this.itemGrid.ShowDirectExpenseColumn = false;
            this.itemGrid.ShowPrepaid = false;
            this.itemGrid.ShowPrice = false;
            this.itemGrid.ShowQuantity = true;
            this.itemGrid.ShowRate = false;
            this.itemGrid.ShowRemitedColumn = false;
            this.itemGrid.ShowUnit = true;
            this.itemGrid.ShowUnitPrice = true;
            this.itemGrid.Size = new System.Drawing.Size(720, 319);
            this.itemGrid.TabIndex = 7;
            this.itemGrid.UnitPriceCaption = "Unit Price";
            this.itemGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewItems});
            // 
            // gridViewItems
            // 
            this.gridViewItems.Appearance.FocusedCell.BackColor = System.Drawing.Color.AliceBlue;
            this.gridViewItems.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridViewItems.Appearance.FocusedRow.BackColor = System.Drawing.Color.PaleTurquoise;
            this.gridViewItems.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewItems.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewItems.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewItems.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.PaleTurquoise;
            this.gridViewItems.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewItems.Appearance.Row.BackColor = System.Drawing.Color.PaleTurquoise;
            this.gridViewItems.Appearance.Row.Options.UseBackColor = true;
            this.gridViewItems.Appearance.SelectedRow.BackColor = System.Drawing.Color.PaleTurquoise;
            this.gridViewItems.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridViewItems.GridControl = this.itemGrid;
            this.gridViewItems.GroupPanelText = "Materials To Issue";
            this.gridViewItems.Name = "gridViewItems";
            this.gridViewItems.OptionsView.ShowFooter = true;
            this.gridViewItems.CustomDrawColumnHeader += new DevExpress.XtraGrid.Views.Grid.ColumnHeaderCustomDrawEventHandler(this.gridViewItems_CustomDrawColumnHeader);
            // 
            // BatchProcessPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.itemGrid);
            this.Controls.Add(this.layoutControl1);
            this.Name = "BatchProcessPage";
            this.Size = new System.Drawing.Size(720, 471);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storePicker.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewItems)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewItems;
        internal INTAPS.Ethiopic.DualCalendar dateEnd;
        internal INTAPS.Ethiopic.DualCalendar dateStart;
        internal iERP.Client.ItemGrid itemGrid;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        internal iERP.Client.StorePlaceholder storePicker;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        internal DevExpress.XtraEditors.CheckEdit chkInclude;
    }
}
