﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BIZNET.iERP.Client;
using BIZNET.iERP;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using DevExpress.Utils.Drawing;
using DevExpress.Utils.Text;
using DevExpress.Utils;
using DevExpress.Data;

namespace BIZNET.iERPMan.Client
{
    public partial class BatchProcessPage : XtraUserControl, IItemGridController
    {
        int m_ProcessTypeID;
        string m_ProcessName;
        ProductionBatchProcessData processData = null;
        StoreIssueDocument m_StoreIssueDoc = null;
        public delegate void ActivateProcessType(bool check, BatchProcessPage processPage);
        public event ActivateProcessType ProcessTypeActivated;
        public BatchProcessPage(int processID,string description,ProductionBatch batch, int storeCostCenterID)
        {
            InitializeComponent();
            chkInclude.Checked = false;
            //chkInclude_CheckedChanged(null, null);
            itemGrid.initializeGrid();
            itemGrid.Controller = this;
            gridViewItems.Columns[5].SummaryItem.SummaryType = SummaryItemType.Sum;
            gridViewItems.Columns[5].SummaryItem.DisplayFormat = "SUM={0:n2}";
            m_ProcessTypeID = processID;
            m_ProcessName = description;
            storePicker.storeID = storeCostCenterID;
            if (batch != null)
                loadBatchProcessData(batch, processID);
        }

        private void loadBatchProcessData(ProductionBatch batch,int processID)
        {
            processData = batch.GetProcessDataByProcessType(processID);
            if (processData != null)
            {
                chkInclude.Checked = true;
                //chkInclude_CheckedChanged(null, null);
                m_StoreIssueDoc = (StoreIssueDocument)AccountingClient.GetAccountDocument(processData.issuedMaterialDocumentID, true);
                dateStart.DateTime = new DateTime(processData.startTicks);
                dateEnd.DateTime = new DateTime(processData.endTicks);
                storePicker.storeID = processData.storeCostCenterID;
                foreach (TransactionDocumentItem item in processData.items)
                {
                    itemGrid.Add(item);
                }
                itemGrid.updateBalances();
            }

        }
        public int ProcessTypeID
        {
            get
            {
                return m_ProcessTypeID;
            }
        }
        public string ProcessName
        {
            get
            {
                return m_ProcessName;
            }
        }
        public TransactionDocumentItem[] GetTransactionDocumentItems()
        {
            TransactionDocumentItem[] docItems = new TransactionDocumentItem[itemGrid.Count];
            int i = 0;
            foreach (BIZNET.iERP.Client.ItemGridControl.ItemGridData.ItemTableRow row in itemGrid)
            {
                docItems[i] = ItemGrid.toDocumentItem(row);
                docItems[i].price = docItems[i].quantity * docItems[i].unitPrice;
                i++;
            }
            return docItems;
        }

        public void afterComit(iERP.Client.ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            
        }

        public double getBalance(iERP.Client.ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            if (m_StoreIssueDoc != null && m_StoreIssueDoc.DocumentDate < dateStart.DateTime && m_StoreIssueDoc.storeID == storePicker.storeID)
            {
                TransactionDocumentItem indocItem = TransactionDocumentItem.sumByItem(m_StoreIssueDoc.items, item.Code, -1);
                double ret = AccountingClient.GetNetCostCenterAccountBalanceAsOf(storePicker.storeID, item.materialAssetAccountID(true), TransactionItem.MATERIAL_QUANTITY, dateStart.DateTime);
                return ret + indocItem.quantity;
            }
            return AccountingClient.GetNetCostCenterAccountBalanceAsOf(storePicker.storeID, item.materialAssetAccountID(true), TransactionItem.MATERIAL_QUANTITY, dateStart.DateTime);
        }

        public double getUnitPrice(iERP.Client.ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            double totalQuantity = AccountingClient.GetNetCostCenterAccountBalanceAsOf(storePicker.storeID, item.materialAssetAccountID(true), TransactionItem.MATERIAL_QUANTITY, dateStart.DateTime);
            double totalValue = AccountingClient.GetNetCostCenterAccountBalanceAsOf(storePicker.storeID, item.materialAssetAccountID(true), TransactionItem.DEFAULT_CURRENCY, dateStart.DateTime);
            if (m_StoreIssueDoc != null && m_StoreIssueDoc.DocumentDate < dateStart.DateTime && m_StoreIssueDoc.storeID == storePicker.storeID)
            {
                TransactionDocumentItem indocItem = TransactionDocumentItem.sumByItem(m_StoreIssueDoc.items, item.Code, -1);
                totalQuantity += indocItem.quantity;
                totalValue += indocItem.price;
            }

            if (AccountBase.AmountEqual(totalQuantity, 0))
                return 0;
            return totalValue / totalQuantity;
        }

        public bool hasUnitPrice(TransactionItems item)
        {
            return true;
        }

        public bool queryCellChange(iERP.Client.ItemGridControl.ItemGridData.ItemTableRow row, DataColumn column, object oldValue)
        {
            return true;
        }

        public bool queryDeleteRow(iERP.Client.ItemGridControl.ItemGridData.ItemTableRow row)
        {
            return true;
        }

        private void chkInclude_CheckedChanged(object sender, EventArgs e)
        {
            dateStart.Enabled = dateEnd.Enabled = storePicker.Enabled = itemGrid.Enabled = chkInclude.Checked;
            if (ProcessTypeActivated != null)
                ProcessTypeActivated(chkInclude.Checked, this);
        }

        private void gridViewItems_CustomDrawColumnHeader(object sender, DevExpress.XtraGrid.Views.Grid.ColumnHeaderCustomDrawEventArgs e)
        {
            e.Handled = true;
            e.Painter.DrawObject(e.Info);
            e.Cache.FillRectangle(new SolidBrush(Color.PaleTurquoise), new Rectangle(e.Bounds.X + 1, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height));
            HeaderObjectPainter painter = new HeaderObjectPainter(e.Painter);
            Rectangle r = painter.GetObjectClientRectangle(e.Info);
            Brush foreBrush = e.Cache.GetSolidBrush(e.Appearance.ForeColor);
            Point offs = new Point(r.X - e.Info.TopLeft.X, r.Y - e.Info.TopLeft.Y);

            e.Info.InnerElements.DrawObjects(e.Info, e.Cache, offs);
            if (e.Info.Caption.Length == 0 || e.Info.CaptionRect.IsEmpty)
                return;
            r = e.Info.CaptionRect;
            r.Offset(offs);
            if (e.Info.UseHtmlTextDraw)
                StringPainter.Default.DrawString(e.Cache, e.Appearance, e.Info.Caption, r, TextOptions.DefaultOptionsNoWrapEx, e.Info.HtmlContext);
            else
                e.Info.Appearance.DrawString(e.Cache, e.Info.Caption, r, foreBrush, e.Info.Appearance.GetStringFormat(TextOptions.DefaultOptionsNoWrapEx));

        }

        internal void setDateRange(DateTime dateTime1, DateTime dateTime2)
        {
            this.dateStart.DateTime = dateTime1;
            this.dateEnd.DateTime = dateTime2;
        }
    }
}
