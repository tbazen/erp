﻿using BIZNET.iERP.Client;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERPMan.Client
{
    public partial class ProductionBatchManager : XtraForm
    {
        private DataTable m_BatchTable;
        public ProductionBatchManager()
        {
            InitializeComponent();
            btnEditBatch.Visibility=btnNewBatch.Visibility = iERPManClient.canEditProductionBatchDetail() ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.Never;
            barButtonDelete.Visibility = iERPManClient.canDeleteProductionBatch() ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.Never;
            barButtonSummary.Visibility = iERPManClient.canViewProductionBatchDetail() ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.Never;
            m_BatchTable = new DataTable();
            chkDate.Checked = false;
            chkDate_CheckedChanged(null, null);
            prepareDataTable();

            
        }

        private void prepareDataTable()
        {
            m_BatchTable.Columns.Add("Batch No.",typeof(string));
            m_BatchTable.Columns.Add("Date", typeof(string));
            m_BatchTable.Columns.Add("Code", typeof(string));
            m_BatchTable.Columns.Add("Product", typeof(string));
            m_BatchTable.Columns.Add("Batch Type", typeof(string));
            m_BatchTable.Columns.Add("Factory", typeof(string));
            m_BatchTable.Columns.Add("Remark", typeof(string));
            m_BatchTable.Columns.Add("BatchID", typeof(int));
            gridControl.DataSource = m_BatchTable;
            gridView.Columns["BatchID"].Visible = false;
            
        }

        private void btnNewBatch_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RegisterProductionBatch registerBatch = new RegisterProductionBatch(-1);
            if (registerBatch.ShowDialog() == DialogResult.OK)
            {
                ProductionBatchSearchOption option = new ProductionBatchSearchOption();
                txtBatchNo.Text = registerBatch.BatchNo;
                txtRemark.Text = "";
                chkDate.Enabled = false;
                option.batchNo = registerBatch.BatchNo;
                option.remark = "";
                option.tickFrom = -1;
                option.tickTo = -1;
                loadProductionBatchList(option);
            }
        }

        private void loadProductionBatchList(ProductionBatchSearchOption option)
        {
            m_BatchTable.Rows.Clear();
            int N;
            ProductionBatchHeader[] productions = iERPManClient.searchBatch(option, pageNavigator1.RecordIndex, pageNavigator1.PageSize, out N);
            pageNavigator1.NRec = N;
            foreach (ProductionBatchHeader production in productions)
            {
                DataRow row = m_BatchTable.NewRow();
                row[0] = production.batchNo;
                row[1] = new DateTime(production.ticksCreated).ToString("MMM dd, yyyy");
                row[2] = production.itemCode;
                row[3] = iERPTransactionClient.GetTransactionItems(production.itemCode).Name;
                row[4] = iERPManClient.getBatchType(production.batchTypeID);
                row[5] = AccountingClient.GetAccount<CostCenter>(iERPManClient.getFactory(production.factoryID).costCenterID).Name;
                row[6] = production.remark;
                row[7] = production.id;
                m_BatchTable.Rows.Add(row);
                gridControl.DataSource = m_BatchTable;
                gridControl.RefreshDataSource();
            }
        }

        private void pageNavigator1_PageChanged(object sender, EventArgs e)
        {
            ProductionBatchSearchOption option = new ProductionBatchSearchOption();
            option.batchNo = txtBatchNo.Text;
            option.remark = txtRemark.Text;
            option.tickFrom = chkDate.Checked ? dateFrom.DateTime.Ticks : -1;
            option.tickTo = chkDate.Checked ? dateTo.DateTime.AddDays(1).Ticks : -1;
            loadProductionBatchList(option);
        }

        private void chkDate_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDate.Checked)
            {
                layoutDateFrom.RestoreFromCustomization(layoutBatchNo, DevExpress.XtraLayout.Utils.InsertType.Bottom);
                layoutDateTo.RestoreFromCustomization(layoutDateFrom, DevExpress.XtraLayout.Utils.InsertType.Right);
            }
            else
            {
                layoutDateFrom.HideToCustomization();
                layoutDateTo.HideToCustomization();
            }
        }

        private void btnEditBatch_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ShowBatchFormForEdit();
        }

        private void ShowBatchFormForEdit()
        {
            if (gridView.SelectedRowsCount > 0)
            {
                int batchID = (int)gridView.GetRowCellValue(gridView.FocusedRowHandle, "BatchID");
                RegisterProductionBatch registerBatch = new RegisterProductionBatch(batchID);
                if (registerBatch.ShowDialog() == DialogResult.OK)
                {
                    ProductionBatchSearchOption option = new ProductionBatchSearchOption();
                    txtBatchNo.Text = registerBatch.BatchNo;
                    txtRemark.Text = "";
                    chkDate.Enabled = false;
                    option.batchNo = registerBatch.BatchNo;
                    option.remark = "";
                    option.tickFrom = -1;
                    option.tickTo = -1;
                    loadProductionBatchList(option);
                }
            }
        }

        private void gridView_DoubleClick(object sender, EventArgs e)
        {
            ShowBatchFormForEdit();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProductionBatchSearchOption option = new ProductionBatchSearchOption();
                option.batchNo = txtBatchNo.Text;
                option.remark = txtRemark.Text;
                option.tickFrom = chkDate.Checked ? dateFrom.DateTime.Ticks : -1;
                option.tickTo = chkDate.Checked ? dateTo.DateTime.AddDays(1).Ticks : -1;
                loadProductionBatchList(option);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }

        private void barButtonDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView.SelectedRowsCount == 0)
                return;

            int batchID = (int)gridView.GetRowCellValue(gridView.FocusedRowHandle, "BatchID");
            if(INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you wante to delete the selected data."))
            {
                try
                {
                    iERPManClient.deleteProductionBatch(batchID);
                    gridView.DeleteRow(gridView.FocusedRowHandle);
                }
                catch(Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
                }

            }
        }

        private void barButtonSummary_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
             if (gridView.SelectedRowsCount == 0)
                return;
            int batchID = (int)gridView.GetRowCellValue(gridView.FocusedRowHandle, "BatchID");
            new BatchCostViewer(batchID).Show(this);
        }
    }
}
