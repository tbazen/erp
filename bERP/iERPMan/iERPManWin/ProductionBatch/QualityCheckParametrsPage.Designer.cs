﻿namespace BIZNET.iERPMan.Client
{
    partial class QualityCheckParametrsPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControlParameters = new DevExpress.XtraGrid.GridControl();
            this.gridViewParameters = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewParameters)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlParameters
            // 
            this.gridControlParameters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlParameters.Location = new System.Drawing.Point(0, 0);
            this.gridControlParameters.MainView = this.gridViewParameters;
            this.gridControlParameters.Name = "gridControlParameters";
            this.gridControlParameters.Size = new System.Drawing.Size(629, 362);
            this.gridControlParameters.TabIndex = 0;
            this.gridControlParameters.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewParameters});
            // 
            // gridViewParameters
            // 
            this.gridViewParameters.Appearance.FocusedCell.BackColor = System.Drawing.Color.AliceBlue;
            this.gridViewParameters.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridViewParameters.Appearance.FocusedRow.BackColor = System.Drawing.Color.PaleTurquoise;
            this.gridViewParameters.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewParameters.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewParameters.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewParameters.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.PaleTurquoise;
            this.gridViewParameters.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewParameters.Appearance.Row.BackColor = System.Drawing.Color.PaleTurquoise;
            this.gridViewParameters.Appearance.Row.Options.UseBackColor = true;
            this.gridViewParameters.Appearance.SelectedRow.BackColor = System.Drawing.Color.PaleTurquoise;
            this.gridViewParameters.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridViewParameters.Appearance.TopNewRow.BackColor = System.Drawing.Color.PaleTurquoise;
            this.gridViewParameters.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridViewParameters.GridControl = this.gridControlParameters;
            this.gridViewParameters.Name = "gridViewParameters";
            this.gridViewParameters.OptionsView.ShowGroupPanel = false;
            this.gridViewParameters.CustomDrawColumnHeader += new DevExpress.XtraGrid.Views.Grid.ColumnHeaderCustomDrawEventHandler(this.gridViewParameters_CustomDrawColumnHeader);
            // 
            // QualityCheckParametrsPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlParameters);
            this.Name = "QualityCheckParametrsPage";
            this.Size = new System.Drawing.Size(629, 362);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewParameters)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlParameters;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewParameters;
    }
}
