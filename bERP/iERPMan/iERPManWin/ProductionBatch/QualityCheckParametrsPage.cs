﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Utils.Drawing;
using DevExpress.Utils.Text;
using DevExpress.Utils;

namespace BIZNET.iERPMan.Client
{
    public partial class QualityCheckParametrsPage : XtraUserControl
    {
        private DataTable m_ParametersTable;
        private string m_ItemCode;
        public QualityCheckParametrsPage(string itemCode,ProductionBatch batch)
        {
            InitializeComponent();
            m_ItemCode = itemCode;
            m_ParametersTable = new DataTable();
            prepareDataTable();
            loadAllQualityCheckParameters(batch);
        }

        private void prepareDataTable()
        {
            m_ParametersTable.Columns.Add("Parameter", typeof(string));
            m_ParametersTable.Columns.Add("Standard", typeof(double));
            m_ParametersTable.Columns.Add("Unit", typeof(string));
            m_ParametersTable.Columns.Add("Actual", typeof(string));
            m_ParametersTable.Columns.Add("id", typeof(int));
            gridControlParameters.DataSource = m_ParametersTable;
            gridViewParameters.Columns["Parameter"].OptionsColumn.AllowEdit = false;
            gridViewParameters.Columns["Standard"].OptionsColumn.AllowEdit = false;
            gridViewParameters.Columns["Unit"].OptionsColumn.AllowEdit = false;
            gridViewParameters.Columns["id"].Visible = false;
        }

        private void loadAllQualityCheckParameters(ProductionBatch batch)
        {
            m_ParametersTable.Rows.Clear();
            var parameters = iERPManClient.getAllQualityCheckParameters();
            foreach (var param in parameters)
            {
                DataRow row = m_ParametersTable.NewRow();
                row[0] = param.name;
                row[1] = string.IsNullOrEmpty(m_ItemCode) ? param.standardValue : iERPManClient.getDynamicStandardForItem(param.id, m_ItemCode).standardValue;
                row[2] = param.unit;
                row[3] = batch == null ? string.Empty : (batch.GetQualityCheckDataByParameter(param.id) == null ? string.Empty : batch.GetQualityCheckDataByParameter(param.id).measuredValue.ToString("N"));
                row[4] = param.id;
                m_ParametersTable.Rows.Add(row);
                gridControlParameters.DataSource = m_ParametersTable;
                gridControlParameters.RefreshDataSource();
            }
        }
        public ProductionBatchQCData[] GetQualityCheckData()
        {
            List<ProductionBatchQCData> data = new List<ProductionBatchQCData>();
            for (int i = 0; i < gridViewParameters.DataRowCount; i++)
            {
                ProductionBatchQCData qData = new ProductionBatchQCData();
                int paramID = (int)gridViewParameters.GetRowCellValue(i, "id");
                double measuredValue = string.IsNullOrEmpty(gridViewParameters.GetRowCellValue(i, "Actual").ToString()) ? 0d : double.Parse(gridViewParameters.GetRowCellValue(i, "Actual").ToString());
                if (measuredValue != 0)
                {
                    qData.measuredValue = measuredValue;
                    qData.parameterID = paramID;
                    data.Add(qData);
                }
            }
            return data.ToArray();
        }

        private void gridViewParameters_CustomDrawColumnHeader(object sender, DevExpress.XtraGrid.Views.Grid.ColumnHeaderCustomDrawEventArgs e)
        {
            e.Handled = true;
            e.Painter.DrawObject(e.Info);
            e.Cache.FillRectangle(new SolidBrush(Color.PaleTurquoise), new Rectangle(e.Bounds.X + 1, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height));
            HeaderObjectPainter painter = new HeaderObjectPainter(e.Painter);
            Rectangle r = painter.GetObjectClientRectangle(e.Info);
            Brush foreBrush = e.Cache.GetSolidBrush(e.Appearance.ForeColor);
            Point offs = new Point(r.X - e.Info.TopLeft.X, r.Y - e.Info.TopLeft.Y);

            e.Info.InnerElements.DrawObjects(e.Info, e.Cache, offs);
            if (e.Info.Caption.Length == 0 || e.Info.CaptionRect.IsEmpty)
                return;
            r = e.Info.CaptionRect;
            r.Offset(offs);
            if (e.Info.UseHtmlTextDraw)
                StringPainter.Default.DrawString(e.Cache, e.Appearance, e.Info.Caption, r, TextOptions.DefaultOptionsNoWrapEx, e.Info.HtmlContext);
            else
                e.Info.Appearance.DrawString(e.Cache, e.Info.Caption, r, foreBrush, e.Info.Appearance.GetStringFormat(TextOptions.DefaultOptionsNoWrapEx));

        }
    }
}
