﻿using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using DevExpress.XtraNavBar.ViewInfo;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERPMan.Client
{
    public partial class RegisterProductionBatch : XtraForm
    {
        private string m_BatchNo;
        private QualityCheckParametrsPage parametersPage;
        private ProductionBatch m_LoadedBatch = null;
        private class FactoryWithName
        {
            public Factory f;
            public FactoryWithName(Factory factory)
            {
                f = factory;
            }
            public override string ToString()
            {
                return AccountingClient.GetAccount<CostCenter>(f.costCenterID).Name;
            }

        }
        public RegisterProductionBatch(int batchID)
        {
            InitializeComponent();
            loadBatchTypes();
            loadFactories();
            if (batchID != -1)
            {
                m_LoadedBatch = iERPManClient.getProductionBatch(batchID);
                dateBatch.DateTime = new DateTime(m_LoadedBatch.header.ticksCreated);
                txtBatchNo.Text = m_LoadedBatch.header.batchNo;
                txtActualQty.Text = m_LoadedBatch.header.actualQuantity.ToString("N");
                txtPlannedQty.Text = m_LoadedBatch.header.plannedQuantity.ToString("N");
                txtRemark.Text = m_LoadedBatch.header.remark;
                cmbBatchType.SelectedItem = iERPManClient.getBatchType(m_LoadedBatch.header.batchTypeID);
                cmbFactory.SelectedItem = new FactoryWithName(iERPManClient.getFactory(m_LoadedBatch.header.factoryID));
                itemPicker.SetByID(m_LoadedBatch.header.itemCode);
            }
            else
            {
                CostingPeriod[] period = iERPManClient.getOpenCostingPeriods(0, -1, -1);
                if (period.Length > 0)
                {
                    dateBatch.DateTime = MainForm.theInstance.CurrentPeriod.date1;
                }
            }
            loadProductionBatchData();
        }

        
       
        private void loadBatchTypes()
        {
            cmbBatchType.Properties.Items.Clear();
            var batchTypes = iERPManClient.getAllBatchTypes();
            foreach (BatchType batchType in batchTypes)
            {
                cmbBatchType.Properties.Items.Add(batchType);
            }
            if (cmbBatchType.Properties.Items.Count > 0)
                cmbBatchType.SelectedIndex = 0;
        }

        private void loadFactories()
        {
            cmbFactory.Properties.Items.Clear();
            var factories = iERPManClient.getAllFactories();
            foreach (Factory factory in factories)
            {
                cmbFactory.Properties.Items.Add(new FactoryWithName(factory));
            }
            if (cmbFactory.Properties.Items.Count > 0)
                cmbFactory.SelectedIndex = 0;
        }

        private void loadProductionBatchData()
        {
            navBar.Items.Clear();
            var processTypes = iERPManClient.getAllProcessTypes();
            foreach (ProcessType process in processTypes)
            {
                NavBarItem processItem = new NavBarItem(process.description);
                processItem.CanDrag = false;
                if (m_LoadedBatch != null)
                {
                    if (m_LoadedBatch.GetProcessDataByProcessType(process.id) != null)
                        processItem.SmallImage = BIZNET.iERPMan.Client.Properties.Resources.Checked;
                }
                BatchProcessPage processPage = new BatchProcessPage(process.id, process.description, m_LoadedBatch, ((FactoryWithName)cmbFactory.SelectedItem).f.defaultStoreID);
                if(m_LoadedBatch==null)
                    processPage.setDateRange(dateBatch.DateTime, dateBatch.DateTime.AddHours(4));
                processItem.Tag = processPage;
                processPage.ProcessTypeActivated += processPage_ProcessTypeActivated;
                processItem.LinkClicked += processItem_LinkClicked;
                navBar.Items.Add(processItem);
                this.navBarGroup1.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(processItem)});
            }
            
            //Add quality check parameter tab/link
            parametersPage = new QualityCheckParametrsPage(itemPicker.GetObjectID(), m_LoadedBatch);
            NavBarItem qualityCheckItem = new NavBarItem("Quality Check Parameters");
            qualityCheckItem.CanDrag = false;
            qualityCheckItem.LinkClicked += qualityCheckItem_LinkClicked;
            navBar.Items.Add(qualityCheckItem);
             this.navBarGroup1.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(qualityCheckItem)});
             if (navBar.Items.Count > 0)
             {
                 processItem_LinkClicked(navBar.Items[0], null);
                 navBarGroup1.SelectedLinkIndex = 0;
             }
        }

        void processPage_ProcessTypeActivated(bool check, BatchProcessPage processPage)
        {
            foreach (NavBarItem item in navBar.Items)
            {
                BatchProcessPage itemPage = item.Tag as BatchProcessPage;
                if (itemPage != null)
                {
                    if (processPage.ProcessTypeID == itemPage.ProcessTypeID)
                    {
                        item.SmallImage = check ? BIZNET.iERPMan.Client.Properties.Resources.Checked : null;
                        return;
                    }
                }
            }
        }

        void qualityCheckItem_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            AddQualityCheckParamtersPage();
            panConfigPages.Appearance.BackColor = Color.PaleTurquoise;
        }

        private void AddQualityCheckParamtersPage()
        {
            panConfigPages.Controls.Clear();
            panConfigPages.Controls.Add(parametersPage);
            parametersPage.Dock = DockStyle.Fill;
        }

        void processItem_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            panConfigPages.Controls.Clear();
            NavBarItem item = (NavBarItem)sender;
            BatchProcessPage processPage = item.Tag as BatchProcessPage;
            processPage.chkInclude.BackColor = Color.PaleTurquoise;
            panConfigPages.Controls.Add(processPage);
            processPage.Dock = DockStyle.Fill;
            panConfigPages.Appearance.BackColor = Color.PaleTurquoise;
        }
        public string BatchNo
        {
            get
            {
                return m_BatchNo;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            try
            {
                //validate input controls
                if (string.IsNullOrEmpty(itemPicker.Text))
                {
                    DevExMessageBox.ShowErrorMessage("Item should be picked");
                    return;
                }
                if (string.IsNullOrEmpty(txtPlannedQty.Text))
                {
                    DevExMessageBox.ShowErrorMessage("Planned quantity cannot be empty");
                    return;
                }
                if (dateBatch.DateTime.CompareTo(DateTime.Now) > 0)
                {
                    DevExMessageBox.ShowErrorMessage("Batch creation date cannot be in the future");
                    return;
                }
                List<ProductionBatchProcessData> processDataList = new List<ProductionBatchProcessData>();
                ProductionBatch productionBatch = m_LoadedBatch == null ? new ProductionBatch() : m_LoadedBatch;
                ProductionBatchHeader batch = new ProductionBatchHeader();
                batch.actualQuantity = string.IsNullOrEmpty(txtActualQty.Text) ? 0d : double.Parse(txtActualQty.Text);
                batch.batchTypeID = ((BatchType)cmbBatchType.SelectedItem).id;
                batch.factoryID = ((FactoryWithName)cmbFactory.SelectedItem).f.costCenterID;
                batch.itemCode = itemPicker.GetObjectID();
                batch.plannedQuantity = double.Parse(txtPlannedQty.Text);
                batch.remark = txtRemark.Text;
                batch.ticksCreated = dateBatch.DateTime.Ticks;
                batch.status = ProductionBatchStatus.Closed;
                batch.id = m_LoadedBatch == null ? -1 : m_LoadedBatch.header.id;
                productionBatch.header = batch;
                foreach (NavBarItem item in navBar.Items)
                {
                    BatchProcessPage processPage = item.Tag as BatchProcessPage;
                    if (processPage != null)
                    {
                        
                        ProductionBatchProcessData processData = new ProductionBatchProcessData();
                        
                        if (processPage.chkInclude.Checked)
                        {
                            if (!validateProcessData(processPage))
                                return;
                            processData.endTicks = processPage.dateEnd.DateTime.Ticks;
                            processData.startTicks = processPage.dateStart.DateTime.Ticks;
                            processData.processID = processPage.ProcessTypeID;
                            processData.items = processPage.GetTransactionDocumentItems();
                            processData.storeCostCenterID = processPage.storePicker.storeID;
                            processDataList.Add(processData);
                        }
                    }

                }
                productionBatch.processData = processDataList.ToArray();
                productionBatch.qcData = parametersPage.GetQualityCheckData();
                //if (!validateQualityCheckData(productionBatch.qcData))
                //    return;
                //if(productionBatch.processData.Length==0)
                //{
                //    DevExMessageBox.ShowErrorMessage("No process Data found to create batch");
                //    return;
                //}
                //if(productionBatch.qcData.Length==0)
                //{
                //    DevExMessageBox.ShowErrorMessage("No quality check parameter data found to create batch");
                //    return;
                //}
                int batchID;
                if (m_LoadedBatch == null)
                {
                    batchID = iERPManClient.createProductionBatch(productionBatch);
                    DevExMessageBox.ShowSuccessMessage("Batch successfully created");
                }
                else
                {
                    iERPManClient.updateProductionBatch(productionBatch);
                    DevExMessageBox.ShowSuccessMessage("Batch successfully updated");
                    batchID = m_LoadedBatch.header.id;
                }
                ProductionBatch pBath = iERPManClient.getProductionBatch(batchID);
                m_BatchNo = pBath.header.batchNo;
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                DevExMessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private bool validateQualityCheckData(ProductionBatchQCData[] productionBatchQCData)
        {
            foreach (ProductionBatchQCData data in productionBatchQCData)
            {
                if (data.measuredValue == 0d)
                {
                    QualityCheckParameter param = iERPManClient.getQualityCheckParameter(data.parameterID);
                    DevExMessageBox.ShowErrorMessage("Measured value not set for Quantity Check Parameter: " + param.name);
                    return false;
                }
            }
            return true;
        }

        private bool validateProcessData(BatchProcessPage processData)
        {
            string processName = processData.ProcessName;
            if(processData.dateStart.DateTime.Ticks>processData.dateEnd.DateTime.Ticks)
            {
                DevExMessageBox.ShowErrorMessage("Error on " + processName + ": Process start date cannot be after the end date");
                return false;
            }
            if (processData.dateEnd.DateTime.Ticks < processData.dateStart.DateTime.Ticks)
            {
                DevExMessageBox.ShowErrorMessage("Error on " + processName + ": Process end date cannot be before the start date");
                return false;
            }
            if(processData.storePicker.storeID==-1)
            {
                DevExMessageBox.ShowErrorMessage("Error on " + processName + ": Store is not picked");
                return false;
            }
            //if(processData.GetTransactionDocumentItems().Length==0)
            //{
            //    DevExMessageBox.ShowErrorMessage("Error on " + processName + ": Materials are not added");
            //    return false;
            //}
            return true;
        }

        private void navBar_SelectedLinkChanged(object sender, DevExpress.XtraNavBar.ViewInfo.NavBarSelectedLinkChangedEventArgs e)
        {
           
        }

        private void itemPicker_TextChanged(object sender, EventArgs e)
        {
            parametersPage = new QualityCheckParametrsPage(itemPicker.GetObjectID(), m_LoadedBatch);
        }

        private void navBar_CustomDrawLink(object sender, DevExpress.XtraNavBar.ViewInfo.CustomDrawNavBarElementEventArgs e)
        {
            DevExpress.XtraNavBar.NavBarItemLink link = ((NavLinkInfoArgs)e.ObjectInfo).Link;
            if (link.State == DevExpress.Utils.Drawing.ObjectState.Selected)
            {
                e.Graphics.FillRectangle(Brushes.Turquoise, e.RealBounds);
            }
            if (link.State == DevExpress.Utils.Drawing.ObjectState.Hot)
            {
                e.Graphics.FillRectangle(Brushes.Turquoise, e.RealBounds);
            }
            if (link.State == DevExpress.Utils.Drawing.ObjectState.Hot)
            {
                e.Graphics.FillRectangle(Brushes.Turquoise, e.RealBounds);
            }
        }
    }
}
