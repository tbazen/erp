﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIZNET.iERPMan.Client
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //try
            //{
                Application.EnableVisualStyles();
                InitializeFormSkinning();
                Application.SetCompatibleTextRenderingDefault(false);
                Login l = new Login();
                l.TryLogin();
                if (l.logedin)
                {
                    MainForm mf = new MainForm();
                    new INTAPS.UI.UIFormApplicationBase(mf);
                    BIZNET.iERP.Client.Globals.currentCostCenterID = BIZNET.iERP.Client.iERPTransactionClient.mainCostCenterID;
                    Application.Run(mf);
                }
            //}
            //catch (Exception ex)
            //{
            //    string msg = "";
            //    while (ex != null)
            //    {
            //        msg += "\n" + ex.Message + "\nStack Trace:\n" + ex.StackTrace;
            //        ex = ex.InnerException;
            //    }
            //    System.Windows.Forms.MessageBox.Show(msg);

            //}
        }
        private static void InitializeFormSkinning()
        {
            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.LookAndFeel.LookAndFeelHelper.ForceDefaultLookAndFeelChanged();
            DevExpress.UserSkins.BonusSkins.Register();
        }
    }
}
