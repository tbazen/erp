﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using BIZNET.iERP.Client;
using BIZNET.iERP;

namespace BIZNET.iERPMan.Client
{
    public partial class TransactionItemPlaceHolder :INTAPS.UI.ObjectPlaceHolderTypedID<BIZNET.iERP.TransactionItems,string>
    {
        public TransactionItemPlaceHolder():base(null)
        {
            
        }
        public override bool pickable
        {
            get { return false; }
        }
        protected override BIZNET.iERP.TransactionItems GetObjectBYID(string code)
        {
            return iERPTransactionClient.GetTransactionItems(code);
        }

        protected override string GetObjectID(BIZNET.iERP.TransactionItems obj)
        {
            return obj.Code;
        }

        protected override string GetObjectString(BIZNET.iERP.TransactionItems obj)
        {
            return obj.Name + " - " + obj.Code; ;
        }

        protected override BIZNET.iERP.TransactionItems PickObject()
        {
            throw new NotImplementedException();   
        }

        protected override BIZNET.iERP.TransactionItems[] SearchObject(string query)
        {
            /*int N;
            return iERPTransactionClient.SearchTransactionItems(0, 10, new object[] { query }, new string[] { "Code" }, out N);*/
            TransactionItems item = iERPTransactionClient.GetTransactionItems(query);
            if (item == null)
                return new TransactionItems[0];
            return new TransactionItems[] { item };
        }
        
    }
    public class CategoryPlaceHolder : INTAPS.UI.ObjectPlaceHolder<ItemCategory>
    {
        protected override ItemCategory GetObjectBYID(int id)
        {
            return iERPTransactionClient.GetItemCategory(id);
        }

        protected override int GetObjectID(ItemCategory obj)
        {
            return obj.ID;
        }

        protected override string GetObjectString(ItemCategory obj)
        {
            return obj.description + " - " + obj.Code;
        }

        protected override ItemCategory PickObject()
        {
            throw new NotImplementedException();
        }
        protected override bool hasPicker
        {
            get
            {
                return false;
            }
        }

        protected override ItemCategory[] SearchObject(string query)
        {
            ItemCategory ret= iERPTransactionClient.GetItemCategory(query);
            if(ret==null)
                return new ItemCategory[]{};
            return new ItemCategory[] { ret };
        }
    }
}
