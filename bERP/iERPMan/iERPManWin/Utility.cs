using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Xml;
using DevExpress.XtraEditors.DXErrorProvider;
using INTAPS.Accounting;
using BIZNET.iERP.Client;
using DevExpress.XtraReports.UI;
using System.Drawing;
using DevExpress.XtraPrinting;
using INTAPS.ClientServer;
using INTAPS.Accounting.Client;

namespace BIZNET.iERPMan.Client
{
    public class DevExMessageBox
    {
        public static DialogResult ShowErrorMessage(string error)
        {
            return XtraMessageBox.Show(error, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
        }

        public static DialogResult ShowWarningMessage(string warning)
        {
            return XtraMessageBox.Show(warning, "Warning", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning);
        }
        public static DialogResult ShowNormalWarningMessage(string warning)
        {
            return XtraMessageBox.Show(warning, "Warning", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
        public static DialogResult ShowSuccessMessage(string success)
        {
            return XtraMessageBox.Show(success, "Success", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
        }
    }

   
}
