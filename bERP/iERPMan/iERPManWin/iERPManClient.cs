﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Remoting;
using INTAPS.ClientServer.Client;

namespace BIZNET.iERPMan.Client
{
    public class iERPManClient
    {
        static IiERPManService server;
        public static void Connect(string url)
        {
            server = (IiERPManService)RemotingServices.Connect(typeof(IiERPManService), url + "/iERPManServer");
        }
        public static ProductionBatch getProductionBatch(int id)
        {
            return server.getProductionBatch(ApplicationClient.SessionID, id);
        }

        public static int createProductionBatch(BIZNET.iERPMan.ProductionBatch batch)
        {
            return server.createProductionBatch(ApplicationClient.SessionID, batch);
        }

        public static void updateProductionBatch(BIZNET.iERPMan.ProductionBatch batch)
        {
            server.updateProductionBatch(ApplicationClient.SessionID, batch);
        }

        public static void deleteProductionBatch(int batchID)
        {
            server.deleteProductionBatch(ApplicationClient.SessionID, batchID);
        }

        public static BIZNET.iERPMan.ProductionBatchHeader[] searchBatch(BIZNET.iERPMan.ProductionBatchSearchOption options, int pageIndex, int pageSize, out int N)
        {
            return server.searchBatch(ApplicationClient.SessionID, options, pageIndex, pageSize, out N);
        }

        public static void changeStatus(int productionBatchID, long ticks, BIZNET.iERPMan.ProductionBatchStatus newStatus)
        {
            server.changeStatus(ApplicationClient.SessionID, productionBatchID, ticks, newStatus);
        }

        public static BIZNET.iERPMan.ProductionBatchHeader[] getBatchesByStatus(long before, BIZNET.iERPMan.ProductionBatchStatus status)
        {
            return server.getBatchesByStatus(ApplicationClient.SessionID, before, status);
        }

        public static BIZNET.iERPMan.ProcessType getProcessType(int processTypeID)
        {
            return server.getProcessType(ApplicationClient.SessionID, processTypeID);
        }

        public static BIZNET.iERPMan.ProcessMachine[] getProcessMachines(int processTypeID)
        {
            return server.getProcessMachines(ApplicationClient.SessionID, processTypeID);
        }

        public static BIZNET.iERPMan.ProcessType[] getAllProcessTypes()
        {
            return server.getAllProcessTypes(ApplicationClient.SessionID);
        }

        public static int createProcessType(BIZNET.iERPMan.ProcessType type, BIZNET.iERPMan.ProcessMachine[] machines)
        {
            return server.createProcessType(ApplicationClient.SessionID, type, machines);
        }

        public static void upateProcessType(BIZNET.iERPMan.ProcessType type, BIZNET.iERPMan.ProcessMachine[] machines)
        {
            server.upateProcessType(ApplicationClient.SessionID, type, machines);
        }

        public static BIZNET.iERPMan.Factory getFactory(int costCenterID)
        {
            return server.getFactory(ApplicationClient.SessionID, costCenterID);
        }

        public static BIZNET.iERPMan.Factory[] getAllFactories()
        {
            return server.getAllFactories(ApplicationClient.SessionID);
        }

        public static void setFactory(BIZNET.iERPMan.Factory factory)
        {
            server.setFactory(ApplicationClient.SessionID, factory);
        }

        public static void deleteFactory(int costCenterID)
        {
            server.deleteFactory(ApplicationClient.SessionID, costCenterID);
        }

        public static BIZNET.iERPMan.BatchType getBatchType(int id)
        {
            return server.getBatchType(ApplicationClient.SessionID, id);
        }

        public static BIZNET.iERPMan.BatchType[] getAllBatchTypes()
        {
            return server.getAllBatchTypes(ApplicationClient.SessionID);
        }

        public static int createBatchType(BIZNET.iERPMan.BatchType type)
        {
            return server.createBatchType(ApplicationClient.SessionID, type);
        }

        public static void updateBatchType(BIZNET.iERPMan.BatchType type)
        {
            server.updateBatchType(ApplicationClient.SessionID, type);
        }

        public static void deleteBatchType(int id)
        {
            server.deleteBatchType(ApplicationClient.SessionID, id);
        }

        public static BIZNET.iERPMan.QualityCheckParameter getQualityCheckParameter(int id)
        {
            return server.getQualityCheckParameter(ApplicationClient.SessionID, id);
        }

        public static BIZNET.iERPMan.QualityCheckParameter[] getAllQualityCheckParameters()
        {
            return server.getAllQualityCheckParameters(ApplicationClient.SessionID);
        }

        public static int createQualityCheckParameter(BIZNET.iERPMan.QualityCheckParameter type)
        {
            return server.createQualityCheckParameter(ApplicationClient.SessionID, type);
        }

        public static void updateQualityCheckParameter(BIZNET.iERPMan.QualityCheckParameter type)
        {
            server.updateQualityCheckParameter(ApplicationClient.SessionID, type);
        }

        public static void deleteQualityCheckParameter(int id)
        {
            server.deleteQualityCheckParameter(ApplicationClient.SessionID, id);
        }

        public static BIZNET.iERPMan.QualityCheckCategoryStandard[] getCategoryStandards(int parmeterID)
        {
            return server.getCategoryStandards(ApplicationClient.SessionID, parmeterID);
        }

        public static BIZNET.iERPMan.QualityCheckCategoryStandard getQualityCheckCategoryStandard(int parameterID, int categoryID)
        {
            return server.getQualityCheckCategoryStandard(ApplicationClient.SessionID, parameterID, categoryID);
        }

        public static void setQualityCheckCategoryStandard(BIZNET.iERPMan.QualityCheckCategoryStandard standard)
        {
            server.setQualityCheckCategoryStandard(ApplicationClient.SessionID, standard);
        }

        public static void deleteQualityCheckCategoryStandard(int parameterID, int categoryID)
        {
            server.deleteQualityCheckCategoryStandard(ApplicationClient.SessionID, parameterID, categoryID);
        }

        public static BIZNET.iERPMan.QualityCheckItemStandard[] getItemStandards(int parmeterID)
        {
            return server.getItemStandards(ApplicationClient.SessionID, parmeterID);
        }

        public static BIZNET.iERPMan.QualityCheckItemStandard getQualityCheckItemStandard(int parameterID, string itemCode)
        {
            return server.getQualityCheckItemStandard(ApplicationClient.SessionID, parameterID, itemCode);
        }

        public static void setQualityCheckItemStandard(BIZNET.iERPMan.QualityCheckItemStandard standard)
        {
            server.setQualityCheckItemStandard(ApplicationClient.SessionID, standard);
        }

        public static void deleteQualityCheckItemStandard(int parameterID, string itemCode)
        {
            server.deleteQualityCheckItemStandard(ApplicationClient.SessionID, parameterID, itemCode);
        }

        public static BIZNET.iERPMan.QualityCheckParameter getDynamicStandardForItem(int parameterID, string itemCode)
        {
            return server.getDynamicStandardForItem(ApplicationClient.SessionID, parameterID, itemCode);
        }

        public static BIZNET.iERPMan.QualityCheckParameter getDynamicStandardForItem(int parameterID, string itemCode, out object scopeObject)
        {
            return server.getDynamicStandardForItem(ApplicationClient.SessionID, parameterID, itemCode, out scopeObject);
        }

        public static object[] GetSystemParameters(string[] fields)
        {
            return server.GetSystemParameters(ApplicationClient.SessionID, fields);
        }
        public static object GetSystemParameter(string field)
        {
            return server.GetSystemParameters(ApplicationClient.SessionID, new string[] { field })[0];
        }

        public static void SetSystemParameters(string[] fields, object[] vals)
        {
            server.SetSystemParameters(ApplicationClient.SessionID, fields, vals);
        }
        public static void SetSystemParameter(string field, object val)
        {
            server.SetSystemParameters(ApplicationClient.SessionID, new string[] { field }, new object[] { val });
        }

        public static void deleteProcessType(int processTypeID)
        {
            server.deleteProcessType(ApplicationClient.SessionID, processTypeID);
        }

        public static BIZNET.iERPMan.CostingPeriod getCostingPeriod(int level, long t)
        {
            return server.getCostingPeriod(ApplicationClient.SessionID, level, t);
        }

        public static BIZNET.iERPMan.CostingPeriod caculateCostingPeriond(int level, long t)
        {
            return server.caculateCostingPeriond(ApplicationClient.SessionID, level, t);
        }

        public static BIZNET.iERPMan.CostingPeriod getLastCostingPeriod(int level, long before)
        {
            return server.getLastCostingPeriod(ApplicationClient.SessionID, level, before);
        }

        public static BIZNET.iERPMan.CostingPeriod[] getOpenCostingPeriods(int level, long from, long to)
        {
            return server.getOpenCostingPeriods(ApplicationClient.SessionID, level, from, to);
        }

        public static BIZNET.iERPMan.CostingPeriod[] getCostingPeriods(int level, long from, long to, int index, int pageSize, out int N)
        {
            return server.getCostingPeriods(ApplicationClient.SessionID, level, from, to, index, pageSize, out N);
        }

        public static BIZNET.iERPMan.CostingPeriod openCostingPeriod(int level, long time)
        {
            return server.openCostingPeriod(ApplicationClient.SessionID, level, time);
        }

        public static void closeCostingPeriod(int level, long time)
        {
            server.closeCostingPeriod(ApplicationClient.SessionID, level, time);
        }

        public static BIZNET.iERPMan.BatchCostItem[] getBatchCostItems(int batchID)
        {
            return server.getBatchCostItems(ApplicationClient.SessionID, batchID);
        }

        public static BIZNET.iERPMan.CostingSetting getCostSetting(string tag)
        {
            return server.getCostSetting(ApplicationClient.SessionID, tag);
        }

        public static void setCostSetting(BIZNET.iERPMan.CostingSetting setting)
        {
            server.setCostSetting(ApplicationClient.SessionID, setting);
        }

        public static void deleteCostSetting(string tag)
        {
            server.deleteCostSetting(ApplicationClient.SessionID, tag);
        }
        public static void saveWorkData(Data_Absence[] absenceData, Data_OverTime[] overTimeData)
        {
            server.saveWorkData(ApplicationClient.SessionID, absenceData, overTimeData);
        }
        public static BIZNET.iERPMan.CostingSetting[] getAllCostingSettings()
        {
            return server.getAllCostingSettings(ApplicationClient.SessionID);
        }
        public static Data_OverTime[] getOverTimeWorkData(DateTime date)
        {
            return server.getOverTimeWorkData(ApplicationClient.SessionID, date);
        }
        public static Data_Absence[] getAbsenceWorkData(DateTime date)
        {
            return server.getAbsenceWorkData(ApplicationClient.SessionID, date);
        }
        public static WorkDataResult[] getWorkData(int month, int year)
        {
            return server.getWorkData(ApplicationClient.SessionID, month, year);
        }
        public static BIZNET.iERPMan.AdjustedCostHeader getActualCostHeader(long ticksFrom)
        {
            return server.getActualCostHeader(ApplicationClient.SessionID, ticksFrom);
        }

        public static BIZNET.iERPMan.AdjustedCostDetail[] getActualCostDetail(long ticksFrom)
        {
            return server.getActualCostDetail(ApplicationClient.SessionID, ticksFrom);
        }

        public static void setActualCost(BIZNET.iERPMan.AdjustedCostHeader header, BIZNET.iERPMan.AdjustedCostDetail[] detail)
        {
            server.setActualCost(ApplicationClient.SessionID, header, detail);
        }

        public static BIZNET.iERPMan.AdjustedCostHeader[] getActualCosts(long ticksFrom, long ticksTo)
        {
            return server.getActualCosts(ApplicationClient.SessionID, ticksFrom, ticksTo);
        }

        public static string renderAdjustedCostReport(long ticksFrom, long ticksTo)
        {
            return server.renderAdjustedCostReport(ApplicationClient.SessionID, ticksFrom, ticksTo);
        }

        public static BIZNET.iERP.SummaryInformation getCostReportingScheme()
        {
            return server.getCostReportingScheme(ApplicationClient.SessionID);
        }

        public static void deleteActualCost(long ticksFrom)
        {
            server.deleteActualCost(ApplicationClient.SessionID, ticksFrom);
        }
        public static bool canViewProductionBatchList()
        {
            return SecurityClient.IsPermited("root/ierpman/batchlist/view");
        }
        public static bool canDeleteProductionBatch()
        {
            return SecurityClient.IsPermited("root/ierpman/batchlist/delete");
        }
        public static bool canViewProductionBatchDetail()
        {
            return SecurityClient.IsPermited("root/ierpman/batch/view");
        }
        public static bool canEditProductionBatchDetail()
        {
            return SecurityClient.IsPermited("root/ierpman/batch/edit");
        }
        public static bool canEditOverrideProductionBatchDetail()
        {
            return SecurityClient.IsPermited("root/ierpman/batch/editoverride");
        }
        public static bool canEditTimeSheet()
        {
            return SecurityClient.IsPermited("root/ierpman/batch/timesheet");
        }
        public static bool canPostSales()
        {
            return SecurityClient.IsPermited("root/ierp/sell/post");
        }
        public static bool canAdjustCost()
        {
            return SecurityClient.IsPermited("root/ierpman/costadjustment");
        }

        internal static bool canConfigure()
        {
            return SecurityClient.IsPermited("root/ierpman/configure");
        }
        internal static bool canClosePeriod()
        {
            return SecurityClient.IsPermited("root/ierpman/closeperiod");
        }
        internal static bool canOpenPeriod()
        {
            return SecurityClient.IsPermited("root/ierpman/openperiod");
        }
    }

}
