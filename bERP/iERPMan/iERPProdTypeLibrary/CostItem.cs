﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIZNET.iERPMan
{
    [Serializable]
    public class CostItem
    {
        public string tag;
        public double amount;
    }
}
