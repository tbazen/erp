﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.RDBMS;

namespace BIZNET.iERPMan
{
    [SingleTableObject]
    [Serializable]
    public class CostingData_EmployeeOverTime
    {
        public int employeeID;
        public long time1;
        public long time2;
        public double hourlyRate;
    }
    [SingleTableObject]
    [Serializable]
    public class CostingData_EmployeeAbsense
    {
        public int employeeID;
        public long time1;
        public long time2;
        public double deduction;
    }
    [Serializable]
    [SingleTableObject]
    public class AdjustedCostHeader
    {
        [IDField]
        public long ticksFrom;
        [DataField]
        public long ticksTo;
        public DateTime dateFrom { get { return new DateTime(ticksFrom); } }

        public DateTime dateTo { get { return new DateTime(ticksTo); } }
    }
    [Serializable]
    [SingleTableObject]
    public class AdjustedCostDetail
    {
        [IDField]
        public long ticksFrom;
        [IDField]
        public string itemTag;
        [DataField]
        public double cost;
    }
}
