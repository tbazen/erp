﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.RDBMS;

namespace BIZNET.iERPMan
{
    [Serializable]
    [SingleTableObject(orderBy = "t1")]
    public class CostingPeriod
    {
        [IDField]
        public int level = 0;
        [IDField]
        public long t1;
        [DataField]
        public long t2;
        [DataField]
        public string name;
        [DataField]
        public bool closed = false;
        public override string ToString()
        {
            return name;
        }

        public DateTime date1 { get { return new DateTime(t1); } }
    }
}
