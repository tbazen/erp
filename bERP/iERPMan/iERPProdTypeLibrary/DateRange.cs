﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIZNET.iERPMan
{
    [Serializable]
    public class DateRange
    {
        public DateTime t1;
        public DateTime t2;
    }
}
