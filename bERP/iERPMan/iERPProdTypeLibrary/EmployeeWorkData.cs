﻿using INTAPS.RDBMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIZNET.iERPMan
{
    [Serializable]
    [SingleTableObject]
    public class Data_Absence
    {
        [IDField]
        public DateTime date;
        [IDField]
        public int employeeID;
        [DataField]
        public double absenceHours;
        public bool isNewData = false;
    }
    [Serializable]
    [SingleTableObject]
    public class Data_OverTime
    {
        [IDField]
        public DateTime date;
        [IDField]
        public int employeeID;
        [IDField]
        public long fromTime;
        [DataField]
        public long toTime;
        public bool isNewData = false;
    }
    [Serializable]
    [SingleTableObject]
    public class OverTimeRateConfiguration
    {
        [DataField]
        public string Name;
        [IDField]
        public long fromTime;
        [DataField]
        public long toTime;
        [DataField]
        public double rate;
    }
    [Serializable]
    public class WorkDataResult
    {
        public DateTime date;
        public string absenceNote;
        public string overTimeNote;
    }
}
