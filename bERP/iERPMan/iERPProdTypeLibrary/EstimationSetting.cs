﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.RDBMS;

namespace BIZNET.iERPMan
{
    public enum EstimationSettingType
    {
        Floating,
        Integer,
        YesNo,
        Text,
        Formula
    }
    [Serializable]
    [SingleTableObject]
    public class EstimationSetting
    {
        [IDField]
        public string key;
        [DataField(allowNull=true)]
        public string parentKey;
        [DataField]
        public string description;
        [DataField] 
        public string value;
        [DataField]
        public string validation;
        [DataField]
        public EstimationSettingType type;
    }
}
