﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZNET.iERPMan
{
    public interface IiERPManService
    {

        BIZNET.iERPMan.ProductionBatch getProductionBatch(string sessionID, int id);

        int createProductionBatch(string sessionID, BIZNET.iERPMan.ProductionBatch batch);

        void updateProductionBatch(string sessionID, BIZNET.iERPMan.ProductionBatch batch);

        void deleteProductionBatch(string sessionID, int batchID);

        BIZNET.iERPMan.ProductionBatchHeader[] searchBatch(string sessionID, BIZNET.iERPMan.ProductionBatchSearchOption options, int pageIndex, int pageSize, out int N);

        void changeStatus(string sessionID, int productionBatchID, long ticks, BIZNET.iERPMan.ProductionBatchStatus newStatus);

        BIZNET.iERPMan.ProductionBatchHeader[] getBatchesByStatus(string sessionID, long before, BIZNET.iERPMan.ProductionBatchStatus status);

        BIZNET.iERPMan.ProcessType getProcessType(string sessionID, int processTypeID);

        BIZNET.iERPMan.ProcessMachine[] getProcessMachines(string sessionID, int processTypeID);

        BIZNET.iERPMan.ProcessType[] getAllProcessTypes(string sessionID);

        int createProcessType(string sessionID, BIZNET.iERPMan.ProcessType type, BIZNET.iERPMan.ProcessMachine[] machines);

        void upateProcessType(string sessionID, BIZNET.iERPMan.ProcessType type, BIZNET.iERPMan.ProcessMachine[] machines);

        BIZNET.iERPMan.Factory getFactory(string sessionID, int costCenterID);

        BIZNET.iERPMan.Factory[] getAllFactories(string sessionID);

        void setFactory(string sessionID, BIZNET.iERPMan.Factory factory);

        void deleteFactory(string sessionID, int costCenterID);

        BIZNET.iERPMan.BatchType getBatchType(string sessionID, int id);

        BIZNET.iERPMan.BatchType[] getAllBatchTypes(string sessionID);

        int createBatchType(string sessionID, BIZNET.iERPMan.BatchType type);

        void updateBatchType(string sessionID, BIZNET.iERPMan.BatchType type);

        void deleteBatchType(string sessionID, int id);

        BIZNET.iERPMan.QualityCheckParameter getQualityCheckParameter(string sessionID, int id);

        BIZNET.iERPMan.QualityCheckParameter[] getAllQualityCheckParameters(string sessionID);

        int createQualityCheckParameter(string sessionID, BIZNET.iERPMan.QualityCheckParameter type);

        void updateQualityCheckParameter(string sessionID, BIZNET.iERPMan.QualityCheckParameter type);

        void deleteQualityCheckParameter(string sessionID, int id);

        BIZNET.iERPMan.QualityCheckCategoryStandard[] getCategoryStandards(string sessionID, int parmeterID);

        BIZNET.iERPMan.QualityCheckCategoryStandard getQualityCheckCategoryStandard(string sessionID, int parameterID, int categoryID);

        void setQualityCheckCategoryStandard(string sessionID, BIZNET.iERPMan.QualityCheckCategoryStandard standard);

        void deleteQualityCheckCategoryStandard(string sessionID, int parameterID, int categoryID);

        BIZNET.iERPMan.QualityCheckItemStandard[] getItemStandards(string sessionID, int parmeterID);

        BIZNET.iERPMan.QualityCheckItemStandard getQualityCheckItemStandard(string sessionID, int parameterID, string itemCode);

        void setQualityCheckItemStandard(string sessionID, BIZNET.iERPMan.QualityCheckItemStandard standard);

        void deleteQualityCheckItemStandard(string sessionID, int parameterID, string itemCode);

        BIZNET.iERPMan.QualityCheckParameter getDynamicStandardForItem(string sessionID, int parameterID, string itemCode);

        BIZNET.iERPMan.QualityCheckParameter getDynamicStandardForItem(string sessionID, int parameterID, string itemCode, out object scopeObject);

        object[] GetSystemParameters(string sessionID, string[] fields);

        void SetSystemParameters(string sessionID, string[] fields, object[] vals);

        void deleteProcessType(string sessionID, int processTypeID);

        BIZNET.iERPMan.CostingPeriod getCostingPeriod(string sessionID, int level, long t);

        BIZNET.iERPMan.CostingPeriod caculateCostingPeriond(string sessionID, int level, long t);

        BIZNET.iERPMan.CostingPeriod getLastCostingPeriod(string sessionID, int level, long before);

        BIZNET.iERPMan.CostingPeriod[] getOpenCostingPeriods(string sessionID, int level, long from, long to);

        BIZNET.iERPMan.CostingPeriod[] getCostingPeriods(string sessionID, int level, long from, long to, int index, int pageSize, out int N);

        BIZNET.iERPMan.CostingPeriod openCostingPeriod(string sessionID, int level, long time);

        void closeCostingPeriod(string sessionID, int level, long time);
        BIZNET.iERPMan.BatchCostItem[] getBatchCostItems(string sessionID, int batchID);

        BIZNET.iERPMan.CostingSetting getCostSetting(string sessionID, string tag);

        void setCostSetting(string sessionID, BIZNET.iERPMan.CostingSetting setting);

        void deleteCostSetting(string sessionID, string tag);
        void saveWorkData(string sessionID, Data_Absence[] absenceData, Data_OverTime[] overTimeData);
        Data_OverTime[] getOverTimeWorkData(string sessionID, DateTime date);
        Data_Absence[] getAbsenceWorkData(string sessionID, DateTime date);
        WorkDataResult[] getWorkData(string sessionID, int month, int year);
        BIZNET.iERPMan.CostingSetting[] getAllCostingSettings(string sessionID);

        BIZNET.iERPMan.AdjustedCostHeader getActualCostHeader(string sessionID, long ticksFrom);

        BIZNET.iERPMan.AdjustedCostDetail[] getActualCostDetail(string sessionID, long ticksFrom);

        void setActualCost(string sessionID, BIZNET.iERPMan.AdjustedCostHeader header, BIZNET.iERPMan.AdjustedCostDetail[] detail);

        BIZNET.iERPMan.AdjustedCostHeader[] getActualCosts(string sessionID, long ticksFrom, long ticksTo);

        string renderAdjustedCostReport(string sessionID, long ticksFrom, long ticksTo);

        BIZNET.iERP.SummaryInformation getCostReportingScheme(string sessionID);

        void deleteActualCost(string sessionID, long ticksFrom);
    }
}
