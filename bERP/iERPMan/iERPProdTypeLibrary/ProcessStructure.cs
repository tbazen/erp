﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.RDBMS;

namespace BIZNET.iERPMan
{
    [Serializable]
    [SingleTableObject]
    public class ProcessType
    {
        [IDField]
        public int id=-1;
        [DataField]
        public string tag;
        [DataField]
        public string description;
    }
    [Serializable]
    [SingleTableObject]
    public class ProcessMachine
    {
        [IDField]
        public int processTypeID;
        [IDField]
        public int costCenterID;
        [DataField]
        public double costPerHour;
    }
    [Serializable]
    [SingleTableObject]
    public class QualityCheckParameter
    {
        [IDField]
        public int id;
        [DataField]
        public string name;
        [DataField]
        public string unit;
        [DataField]
        public double standardValue;
        [DataField]
        public double lowerTol;
        [DataField]
        public double upperTol;
        [DataField]
        public double weight;
        [DataField]
        public double validMin;
        [DataField]
        public double validMax;
    }
    public enum QualityStandardScope
    {
        Global,
        Category,
        Item
    }
    [Serializable]
    [SingleTableObject]
    public class QualityCheckCategoryStandard
    {
        [IDField]
        public int parameterID;
        [IDField]
        public int categoryID;
        [DataField]
        public double standardValue;
        [DataField]
        public double lowerTol;
        [DataField]
        public double upperTol;
        [DataField]
        public double weight;
        [DataField]
        public double validMin;
        [DataField]
        public double validMax;
        public QualityCheckParameter toQualityCheckParameter()
        {
            return new QualityCheckParameter() { standardValue = standardValue, lowerTol = lowerTol, upperTol = upperTol, weight = weight, validMin = validMin, validMax = validMax };
        }

    }
    [Serializable]
    [SingleTableObject]
    public class QualityCheckItemStandard
    {
        [IDField]
        public int parameterID;
        [IDField]
        public string itemCode;
        [DataField]
        public double standardValue;
        [DataField]
        public double lowerTol;
        [DataField]
        public double upperTol;
        [DataField]
        public double weight;
        [DataField]
        public double validMin;
        [DataField]
        public double validMax;
        public QualityCheckParameter toQualityCheckParameter()
        {
            return new QualityCheckParameter() { standardValue = standardValue, lowerTol = lowerTol, upperTol = upperTol, weight = weight, validMin = validMin, validMax = validMax };
        }
    }
    [Serializable]
    [SingleTableObject]
    public class CostingSetting
    {
        [IDField]
        public string tag;
        [DataField]
        public string description;
        [DataField]
        public bool workIndependent;
    }
}
