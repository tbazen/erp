﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.RDBMS;

namespace BIZNET.iERPMan
{
    public enum ProductionBatchStatus
    {
        Open,
        Closed,
    }
    [Serializable]
    public class ProductionBatch
    {
        public ProductionBatchHeader header;
        public ProductionBatchProcessData[] processData;
        public ProductionBatchQCData[] qcData;
        public ProductionBatchProcessData GetProcessDataByProcessType(int processID)
        {
            foreach (ProductionBatchProcessData data in processData)
            {
                if (data.processID == processID)
                    return data;
            }
            return null;
        }
        public ProductionBatchQCData GetQualityCheckDataByParameter(int parameterID)
        {
            foreach (ProductionBatchQCData data in qcData)
            {
                if (data.parameterID == parameterID)
                    return data;
            }
            return null;
        }
    }
    [Serializable]
    [SingleTableObject]
    public class ProductionBatchHeader
    {
        [IDField]
        public int id=-1;
        [DataField]
        public int factoryID;
        [DataField]
        public int batchTypeID;
        [DataField]
        public string batchNo;
        [DataField]
        public long ticksCreated;
        [DataField]
        public ProductionBatchStatus status = ProductionBatchStatus.Open;
        [DataField]
        public long ticksStatus;
        [DataField]
        public string itemCode;
        [DataField]
        public double plannedQuantity;
        [DataField]
        public double actualQuantity;
        [DataField]
        public string remark;
        [DataField]
        public int finishedGoodDocumentID = -1;

        public string formatedDate
        {
            get
            {
                return new DateTime(ticksCreated).ToString("MMM dd,yyyy hh:mm tt");
            }
        }
    }
    [Serializable]
    [SingleTableObject]
    public class BatchType
    {
        [IDField]
        public int id=-1;
        [DataField]
        public string name;
        public override string ToString()
        {
            return name;
        }
    }
    [Serializable]
    [SingleTableObject]
    public class Factory
    {
        [IDField]
        public int costCenterID;
        [DataField]
        public int defaultStoreID;
    }
    [Serializable]
    [SingleTableObject]
    public class ProductionBatchProcessData
    {
        [IDField]
        public int batchID;
        [IDField]
        public int processID;
        [DataField]
        public long startTicks;
        [DataField]
        public long endTicks;
        [DataField]
        public int storeCostCenterID;
        [DataField]
        public int issuedMaterialDocumentID;
        [XMLField]
        public BIZNET.iERP.TransactionDocumentItem[] items;

        public double hours { get
            {
                return new DateTime(endTicks).Subtract(new DateTime(startTicks)).TotalHours;
            }
        }

    }

    [Serializable]
    [SingleTableObject]
    public class ProductionBatchQCData
    {
        [IDField]
        public int batchID;
        [IDField]
        public int parameterID;
        [DataField]
        public double measuredValue;
    }
    public class QualitySummary
    {
        public double qualityIndex;
        public QualityCheckParameter[] parmeters;
        public double[] measuredValues;
    }
    [Serializable]
    public class BatchTypeAttribute:Attribute
    {
        public int id;
        public string name;
    }
    [Serializable]
    public class BatchTypeRuleHandlerAttribute : Attribute
    {
        public Type batchType;
    }
    
    public interface IProductionBatchHandler
    {

    }
    [Serializable]
    [SingleTableObject]
    public class BatchCostItem
    {
        [IDField]
        public int batchID;
        [IDField]
        public int processID;
        [DataField]
        public string tag;
        [DataField]
        public double cost;

        public delegate bool BatchCostItemFilterDelegate(BatchCostItem b);
        public static Dictionary<int, Dictionary<string, CostItem>> groupByBatch(BatchCostItem[] costItems,BatchCostItemFilterDelegate filter)
        {
            Dictionary<int,Dictionary<string, CostItem>> ret=new Dictionary<int,Dictionary<string,CostItem>>();
            foreach(BatchCostItem c in costItems)
            {
                if (filter != null && !filter(c))
                    continue;
                Dictionary<string, CostItem> batchItem;
                if (ret.ContainsKey(c.batchID))
                    batchItem = ret[c.batchID];
                else
                    ret.Add(c.batchID, batchItem = new Dictionary<string, CostItem>());
                if (batchItem.ContainsKey(c.tag))
                    batchItem[c.tag].amount += c.cost;
                else
                    batchItem.Add(c.tag, new CostItem() { tag = c.tag, amount = c.cost });
            }
            return ret;
        }

        public static Dictionary<int, Dictionary<string, CostItem>> groupByProcessType(BatchCostItem[] costItems, BatchCostItemFilterDelegate filter)
        {
            Dictionary<int, Dictionary<string, CostItem>> ret = new Dictionary<int, Dictionary<string, CostItem>>();
            foreach (BatchCostItem c in costItems)
            {
                if (filter != null && !filter(c))
                    continue;
                Dictionary<string, CostItem> batchItem;
                if (ret.ContainsKey(c.processID))
                    batchItem = ret[c.processID];
                else
                    ret.Add(c.processID, batchItem = new Dictionary<string, CostItem>());
                if (batchItem.ContainsKey(c.tag))
                    batchItem[c.tag].amount += c.cost;
                else
                    batchItem.Add(c.tag, new CostItem() { tag = c.tag, amount = c.cost });
            }
            return ret;
        }
        public static Dictionary<string, double> groupByTag(BatchCostItem[] costItems, BatchCostItemFilterDelegate filter)
        {
            Dictionary<string, double> ret = new Dictionary<string, double>();
            foreach (BatchCostItem c in costItems)
            {
                if (filter != null && !filter(c))
                    continue;
                if (ret.ContainsKey(c.tag))
                    ret[c.tag]+= c.cost;
                else
                    ret.Add(c.tag, c.cost);
            }
            return ret;
        }
    }

    [Serializable]
    [SingleTableObject]
    public class ActualCost
    {
        public string itemTag;
        public long time1;
        public long time2;
    }
}
