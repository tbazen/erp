﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.RDBMS;

namespace BIZNET.iERPMan
{
    [Serializable]
    public class ProductionBatchSearchOption
    {
        public string batchNo = null;
        public string remark = null;
        public long tickFrom = DateTime.Now.Date.Ticks;
        public long tickTo = DateTime.Now.Date.AddDays(1).Ticks;
    }
}