﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIZNET.iERPMan
{
    [Serializable]
    public class iERPManSystemParameters
    {
        public int batchNoSerialBatchID;
        public BIZNET.iERP.SummaryInformation materialCostReportingScheme;
        public BIZNET.iERP.SummaryInformation otherCostReportingScheme;
        public int defaultProductionUnit = 5;
    }
}
