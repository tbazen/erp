Add the following lines the RemottingServer.config to enable web server
	<add key="httpEndPoint" value="http://*:8050/"/>
    <add key="html_root" value="C:\Source\Code\bERP\iERPMan\iERPManBDE\html"/>
	<add key="httpPageHandlers" value="iERPManBDE"/>

Run the following commandin command prompt to enable the port for the webserver
	netsh http add urlacl http://+:8050/ user="Teweldemedhin Aberra" listen=yes

Add the following met tag on every html page
	<meta http-equiv="X-UA-Compatible" content="IE=edge" /> 