﻿namespace BIZNET.iERP.Client
{
    partial class AccountExplorer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.accountBrowser = new INTAPS.Accounting.Client.AccountStructureBrowserControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.pnlWaitPanel = new System.Windows.Forms.Panel();
            this.lblDesc = new System.Windows.Forms.Label();
            this.pbWaitAnimation = new System.Windows.Forms.PictureBox();
            this.webBrowser = new INTAPS.UI.HTML.ControlBrowser();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.comboDate2Mode = new DevExpress.XtraEditors.ComboBoxEdit();
            this.period2 = new BIZNET.iERP.Client.PeriodSelector();
            this.period1 = new BIZNET.iERP.Client.PeriodSelector();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.buttonCenterBalance = new BIZNET.iERP.Client.AEToggleButton();
            this.buttonCostCenterProfile = new BIZNET.iERP.Client.AEToggleButton();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.buttonDetailedCashFlow = new BIZNET.iERP.Client.AEToggleButton();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.comboGroup = new DevExpress.XtraEditors.ComboBoxEdit();
            this.buttonTrialBalance = new BIZNET.iERP.Client.AEToggleButton();
            this.buttonBalanceSheet = new BIZNET.iERP.Client.AEToggleButton();
            this.buttonLedger = new BIZNET.iERP.Client.AEToggleButton();
            this.buttonCashFlow = new BIZNET.iERP.Client.AEToggleButton();
            this.buttonIncomeStatement = new BIZNET.iERP.Client.AEToggleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.buttonMaterialFlow = new BIZNET.iERP.Client.AEToggleButton();
            this.buttonBincard = new BIZNET.iERP.Client.AEToggleButton();
            this.buttonInventory = new BIZNET.iERP.Client.AEToggleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.buttonDesign = new DevExpress.XtraEditors.SimpleButton();
            this.toolExport = new INTAPS.UI.HTML.BowserController();
            this.buttonReloadTree = new DevExpress.XtraEditors.SimpleButton();
            this.buttonReload = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.date2 = new BIZNET.iERP.Client.BNDualCalendar();
            this.date1 = new BIZNET.iERP.Client.BNDualCalendar();
            this.timerCheckThread = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.pnlWaitPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWaitAnimation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboDate2Mode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.period2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.period1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.Text = "Tools";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.accountBrowser);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.panelControl2);
            this.splitContainerControl1.Panel2.Controls.Add(this.panelControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1052, 477);
            this.splitContainerControl1.SplitterPosition = 233;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // accountBrowser
            // 
            this.accountBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.accountBrowser.Location = new System.Drawing.Point(0, 0);
            this.accountBrowser.Name = "accountBrowser";
            this.accountBrowser.PickedAccount = null;
            this.accountBrowser.Size = new System.Drawing.Size(233, 477);
            this.accountBrowser.TabIndex = 0;
            this.accountBrowser.CostCenterViewStructureChanged += new System.EventHandler(this.accountBrowser_CostCenterViewStructureChanged);
            this.accountBrowser.AccountViewStructureChanged += new System.EventHandler(this.accountBrowser_AccountViewStructureChanged);
            this.accountBrowser.SelectedCostCenterChanged += new System.EventHandler(this.accountBrowser_SelectedCostCenterChanged);
            this.accountBrowser.SelectedAccountChanged += new System.EventHandler(this.accountBrowser_SelectedAccountChanged);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.pnlWaitPanel);
            this.panelControl2.Controls.Add(this.webBrowser);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 201);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(814, 276);
            this.panelControl2.TabIndex = 7;
            // 
            // pnlWaitPanel
            // 
            this.pnlWaitPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlWaitPanel.BackColor = System.Drawing.Color.White;
            this.pnlWaitPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlWaitPanel.Controls.Add(this.lblDesc);
            this.pnlWaitPanel.Controls.Add(this.pbWaitAnimation);
            this.pnlWaitPanel.Location = new System.Drawing.Point(260, 87);
            this.pnlWaitPanel.Name = "pnlWaitPanel";
            this.pnlWaitPanel.Size = new System.Drawing.Size(310, 45);
            this.pnlWaitPanel.TabIndex = 1;
            this.pnlWaitPanel.Visible = false;
            // 
            // lblDesc
            // 
            this.lblDesc.BackColor = System.Drawing.Color.White;
            this.lblDesc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDesc.Location = new System.Drawing.Point(39, 0);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(269, 43);
            this.lblDesc.TabIndex = 1;
            this.lblDesc.Text = "Generating Report";
            this.lblDesc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pbWaitAnimation
            // 
            this.pbWaitAnimation.Dock = System.Windows.Forms.DockStyle.Left;
            this.pbWaitAnimation.ErrorImage = null;
            this.pbWaitAnimation.Image = global::BIZNET.iERP.Client.Properties.Resources.wait30trans;
            this.pbWaitAnimation.Location = new System.Drawing.Point(0, 0);
            this.pbWaitAnimation.Name = "pbWaitAnimation";
            this.pbWaitAnimation.Size = new System.Drawing.Size(39, 43);
            this.pbWaitAnimation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWaitAnimation.TabIndex = 0;
            this.pbWaitAnimation.TabStop = false;
            // 
            // webBrowser
            // 
            this.webBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser.Location = new System.Drawing.Point(2, 2);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(810, 272);
            this.webBrowser.StyleSheetFile = "berp.css";
            this.webBrowser.TabIndex = 5;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.comboDate2Mode);
            this.panelControl1.Controls.Add(this.period2);
            this.panelControl1.Controls.Add(this.period1);
            this.panelControl1.Controls.Add(this.groupControl5);
            this.panelControl1.Controls.Add(this.groupControl4);
            this.panelControl1.Controls.Add(this.groupControl3);
            this.panelControl1.Controls.Add(this.groupControl2);
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.date2);
            this.panelControl1.Controls.Add(this.date1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(814, 201);
            this.panelControl1.TabIndex = 6;
            // 
            // comboDate2Mode
            // 
            this.comboDate2Mode.EditValue = "Before Next Day";
            this.comboDate2Mode.Location = new System.Drawing.Point(577, 179);
            this.comboDate2Mode.Name = "comboDate2Mode";
            this.comboDate2Mode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboDate2Mode.Properties.Items.AddRange(new object[] {
            "Before Next Day",
            "23:59:1 (Before Closing)"});
            this.comboDate2Mode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboDate2Mode.Size = new System.Drawing.Size(129, 20);
            this.comboDate2Mode.TabIndex = 12;
            // 
            // period2
            // 
            this.period2.Location = new System.Drawing.Point(466, 178);
            this.period2.Name = "period2";
            this.period2.PeriodType = "AP_Accounting_Month";
            this.period2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.period2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.period2.Size = new System.Drawing.Size(100, 20);
            this.period2.TabIndex = 11;
            this.period2.SelectedIndexChanged += new System.EventHandler(this.period2_SelectedIndexChanged);
            // 
            // period1
            // 
            this.period1.Location = new System.Drawing.Point(466, 152);
            this.period1.Name = "period1";
            this.period1.PeriodType = "AP_Accounting_Month";
            this.period1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.period1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.period1.Size = new System.Drawing.Size(100, 20);
            this.period1.TabIndex = 11;
            this.period1.SelectedIndexChanged += new System.EventHandler(this.period1_SelectedIndexChanged);
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.buttonCenterBalance);
            this.groupControl5.Controls.Add(this.buttonCostCenterProfile);
            this.groupControl5.Location = new System.Drawing.Point(563, 5);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(114, 137);
            this.groupControl5.TabIndex = 10;
            this.groupControl5.Text = "Cost Centers";
            // 
            // buttonCenterBalance
            // 
            this.buttonCenterBalance.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.buttonCenterBalance.Appearance.Options.UseFont = true;
            this.buttonCenterBalance.Appearance.Options.UseTextOptions = true;
            this.buttonCenterBalance.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.buttonCenterBalance.Location = new System.Drawing.Point(6, 25);
            this.buttonCenterBalance.Name = "buttonCenterBalance";
            this.buttonCenterBalance.Size = new System.Drawing.Size(94, 44);
            this.buttonCenterBalance.TabIndex = 1;
            this.buttonCenterBalance.Text = "Cost Center Balances";
            this.buttonCenterBalance.CheckedChanged += new System.EventHandler(this.reportTypeButtonCheckChanged);
            // 
            // buttonCostCenterProfile
            // 
            this.buttonCostCenterProfile.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.buttonCostCenterProfile.Appearance.Options.UseFont = true;
            this.buttonCostCenterProfile.Appearance.Options.UseTextOptions = true;
            this.buttonCostCenterProfile.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.buttonCostCenterProfile.Location = new System.Drawing.Point(6, 75);
            this.buttonCostCenterProfile.Name = "buttonCostCenterProfile";
            this.buttonCostCenterProfile.Size = new System.Drawing.Size(94, 45);
            this.buttonCostCenterProfile.TabIndex = 1;
            this.buttonCostCenterProfile.Text = "Cost Center Profile";
            this.buttonCostCenterProfile.CheckedChanged += new System.EventHandler(this.reportTypeButtonCheckChanged);
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.buttonDetailedCashFlow);
            this.groupControl4.Location = new System.Drawing.Point(443, 5);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(114, 137);
            this.groupControl4.TabIndex = 10;
            this.groupControl4.Text = "Cash Flow";
            // 
            // buttonDetailedCashFlow
            // 
            this.buttonDetailedCashFlow.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.buttonDetailedCashFlow.Appearance.Options.UseFont = true;
            this.buttonDetailedCashFlow.Appearance.Options.UseTextOptions = true;
            this.buttonDetailedCashFlow.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.buttonDetailedCashFlow.Location = new System.Drawing.Point(6, 25);
            this.buttonDetailedCashFlow.Name = "buttonDetailedCashFlow";
            this.buttonDetailedCashFlow.Size = new System.Drawing.Size(94, 44);
            this.buttonDetailedCashFlow.TabIndex = 1;
            this.buttonDetailedCashFlow.Text = "Detailed Cash Flow Report";
            this.buttonDetailedCashFlow.CheckedChanged += new System.EventHandler(this.reportTypeButtonCheckChanged);
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.comboGroup);
            this.groupControl3.Controls.Add(this.buttonTrialBalance);
            this.groupControl3.Controls.Add(this.buttonBalanceSheet);
            this.groupControl3.Controls.Add(this.buttonLedger);
            this.groupControl3.Controls.Add(this.buttonCashFlow);
            this.groupControl3.Controls.Add(this.buttonIncomeStatement);
            this.groupControl3.Location = new System.Drawing.Point(138, 5);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(267, 137);
            this.groupControl3.TabIndex = 9;
            this.groupControl3.Text = "Financial Statements";
            // 
            // comboGroup
            // 
            this.comboGroup.Location = new System.Drawing.Point(114, 88);
            this.comboGroup.Name = "comboGroup";
            this.comboGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboGroup.Properties.Items.AddRange(new object[] {
            "Detailed",
            "Summerize by Day",
            "Summerize by Month"});
            this.comboGroup.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboGroup.Size = new System.Drawing.Size(143, 20);
            this.comboGroup.TabIndex = 2;
            // 
            // buttonTrialBalance
            // 
            this.buttonTrialBalance.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.buttonTrialBalance.Appearance.Options.UseFont = true;
            this.buttonTrialBalance.Location = new System.Drawing.Point(5, 24);
            this.buttonTrialBalance.Name = "buttonTrialBalance";
            this.buttonTrialBalance.Size = new System.Drawing.Size(118, 23);
            this.buttonTrialBalance.TabIndex = 1;
            this.buttonTrialBalance.Text = "Trial Balance";
            this.buttonTrialBalance.CheckedChanged += new System.EventHandler(this.reportTypeButtonCheckChanged);
            // 
            // buttonBalanceSheet
            // 
            this.buttonBalanceSheet.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.buttonBalanceSheet.Appearance.Options.UseFont = true;
            this.buttonBalanceSheet.Location = new System.Drawing.Point(141, 24);
            this.buttonBalanceSheet.Name = "buttonBalanceSheet";
            this.buttonBalanceSheet.Size = new System.Drawing.Size(116, 23);
            this.buttonBalanceSheet.TabIndex = 1;
            this.buttonBalanceSheet.Text = "Balance Sheet";
            this.buttonBalanceSheet.CheckedChanged += new System.EventHandler(this.reportTypeButtonCheckChanged);
            // 
            // buttonLedger
            // 
            this.buttonLedger.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.buttonLedger.Appearance.Options.UseFont = true;
            this.buttonLedger.Location = new System.Drawing.Point(5, 87);
            this.buttonLedger.Name = "buttonLedger";
            this.buttonLedger.Size = new System.Drawing.Size(102, 23);
            this.buttonLedger.TabIndex = 1;
            this.buttonLedger.Text = "Ledger";
            this.buttonLedger.CheckedChanged += new System.EventHandler(this.reportTypeButtonCheckChanged);
            // 
            // buttonCashFlow
            // 
            this.buttonCashFlow.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.buttonCashFlow.Appearance.Options.UseFont = true;
            this.buttonCashFlow.Location = new System.Drawing.Point(141, 55);
            this.buttonCashFlow.Name = "buttonCashFlow";
            this.buttonCashFlow.Size = new System.Drawing.Size(118, 23);
            this.buttonCashFlow.TabIndex = 1;
            this.buttonCashFlow.Text = "Cash Flow";
            this.buttonCashFlow.CheckedChanged += new System.EventHandler(this.reportTypeButtonCheckChanged);
            // 
            // buttonIncomeStatement
            // 
            this.buttonIncomeStatement.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.buttonIncomeStatement.Appearance.Options.UseFont = true;
            this.buttonIncomeStatement.Location = new System.Drawing.Point(5, 55);
            this.buttonIncomeStatement.Name = "buttonIncomeStatement";
            this.buttonIncomeStatement.Size = new System.Drawing.Size(118, 23);
            this.buttonIncomeStatement.TabIndex = 1;
            this.buttonIncomeStatement.Text = "Income Statement";
            this.buttonIncomeStatement.CheckedChanged += new System.EventHandler(this.reportTypeButtonCheckChanged);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.buttonMaterialFlow);
            this.groupControl2.Controls.Add(this.buttonBincard);
            this.groupControl2.Controls.Add(this.buttonInventory);
            this.groupControl2.Location = new System.Drawing.Point(5, 5);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(127, 137);
            this.groupControl2.TabIndex = 8;
            this.groupControl2.Text = "Inventory";
            // 
            // buttonMaterialFlow
            // 
            this.buttonMaterialFlow.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.buttonMaterialFlow.Appearance.Options.UseFont = true;
            this.buttonMaterialFlow.Location = new System.Drawing.Point(5, 24);
            this.buttonMaterialFlow.Name = "buttonMaterialFlow";
            this.buttonMaterialFlow.Size = new System.Drawing.Size(117, 25);
            this.buttonMaterialFlow.TabIndex = 1;
            this.buttonMaterialFlow.Text = "Material Flow";
            this.buttonMaterialFlow.CheckedChanged += new System.EventHandler(this.reportTypeButtonCheckChanged);
            // 
            // buttonBincard
            // 
            this.buttonBincard.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.buttonBincard.Appearance.Options.UseFont = true;
            this.buttonBincard.Location = new System.Drawing.Point(5, 81);
            this.buttonBincard.Name = "buttonBincard";
            this.buttonBincard.Size = new System.Drawing.Size(117, 25);
            this.buttonBincard.TabIndex = 1;
            this.buttonBincard.Text = "Stock Card";
            this.buttonBincard.CheckedChanged += new System.EventHandler(this.reportTypeButtonCheckChanged);
            // 
            // buttonInventory
            // 
            this.buttonInventory.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.buttonInventory.Appearance.Options.UseFont = true;
            this.buttonInventory.Location = new System.Drawing.Point(5, 52);
            this.buttonInventory.Name = "buttonInventory";
            this.buttonInventory.Size = new System.Drawing.Size(117, 25);
            this.buttonInventory.TabIndex = 1;
            this.buttonInventory.Text = "Inventory";
            this.buttonInventory.CheckedChanged += new System.EventHandler(this.reportTypeButtonCheckChanged);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.buttonDesign);
            this.groupControl1.Controls.Add(this.toolExport);
            this.groupControl1.Controls.Add(this.buttonReloadTree);
            this.groupControl1.Controls.Add(this.buttonReload);
            this.groupControl1.Location = new System.Drawing.Point(704, 5);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(105, 163);
            this.groupControl1.TabIndex = 7;
            this.groupControl1.Text = "Tools";
            // 
            // buttonDesign
            // 
            this.buttonDesign.Enabled = false;
            this.buttonDesign.Location = new System.Drawing.Point(17, 55);
            this.buttonDesign.Name = "buttonDesign";
            this.buttonDesign.Size = new System.Drawing.Size(70, 23);
            this.buttonDesign.TabIndex = 4;
            this.buttonDesign.Text = "Design";
            this.buttonDesign.Click += new System.EventHandler(this.buttonDesign_Click);
            // 
            // toolExport
            // 
            this.toolExport.Dock = System.Windows.Forms.DockStyle.None;
            this.toolExport.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolExport.Location = new System.Drawing.Point(14, 24);
            this.toolExport.Name = "toolExport";
            this.toolExport.ShowBackForward = false;
            this.toolExport.ShowRefresh = false;
            this.toolExport.Size = new System.Drawing.Size(70, 25);
            this.toolExport.TabIndex = 5;
            this.toolExport.Text = "bowserController1";
            // 
            // buttonReloadTree
            // 
            this.buttonReloadTree.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReloadTree.Location = new System.Drawing.Point(17, 114);
            this.buttonReloadTree.Name = "buttonReloadTree";
            this.buttonReloadTree.Size = new System.Drawing.Size(70, 23);
            this.buttonReloadTree.TabIndex = 4;
            this.buttonReloadTree.Text = "Reload Tree";
            this.buttonReloadTree.Click += new System.EventHandler(this.buttonReloadTree_Click);
            // 
            // buttonReload
            // 
            this.buttonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReload.Location = new System.Drawing.Point(17, 85);
            this.buttonReload.Name = "buttonReload";
            this.buttonReload.Size = new System.Drawing.Size(70, 23);
            this.buttonReload.TabIndex = 4;
            this.buttonReload.Text = "Generate";
            this.buttonReload.Click += new System.EventHandler(this.buttonReload_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl2.Location = new System.Drawing.Point(12, 182);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl1.Location = new System.Drawing.Point(12, 155);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "From:";
            // 
            // date2
            // 
            this.date2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.date2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.date2.Location = new System.Drawing.Point(49, 178);
            this.date2.Name = "date2";
            this.date2.ShowEthiopian = true;
            this.date2.ShowGregorian = true;
            this.date2.ShowTime = false;
            this.date2.Size = new System.Drawing.Size(411, 21);
            this.date2.TabIndex = 2;
            this.date2.VerticalLayout = false;
            this.date2.DateTimeChanged += new System.EventHandler(this.date2_DateTimeChanged);
            // 
            // date1
            // 
            this.date1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.date1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.date1.Location = new System.Drawing.Point(49, 151);
            this.date1.Name = "date1";
            this.date1.ShowEthiopian = true;
            this.date1.ShowGregorian = true;
            this.date1.ShowTime = false;
            this.date1.Size = new System.Drawing.Size(411, 21);
            this.date1.TabIndex = 2;
            this.date1.VerticalLayout = false;
            this.date1.DateTimeChanged += new System.EventHandler(this.date1_DateTimeChanged);
            // 
            // timerCheckThread
            // 
            this.timerCheckThread.Interval = 500;
            this.timerCheckThread.Tick += new System.EventHandler(this.timerCheckThread_Tick);
            // 
            // AccountExplorer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1052, 477);
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "AccountExplorer";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Account Explorer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.pnlWaitPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbWaitAnimation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboDate2Mode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.period2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.period1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private INTAPS.Accounting.Client.AccountStructureBrowserControl accountBrowser;
        private INTAPS.UI.HTML.ControlBrowser webBrowser;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private AEToggleButton buttonTrialBalance;
        private AEToggleButton buttonBalanceSheet;
        private AEToggleButton buttonIncomeStatement;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private BIZNET.iERP.Client.BNDualCalendar date2;
        private BIZNET.iERP.Client.BNDualCalendar date1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private AEToggleButton buttonLedger;
        private AEToggleButton buttonDetailedCashFlow;
        private System.Windows.Forms.Panel pnlWaitPanel;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.PictureBox pbWaitAnimation;
        private System.Windows.Forms.Timer timerCheckThread;
        private DevExpress.XtraEditors.SimpleButton buttonReload;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton buttonDesign;
        private INTAPS.UI.HTML.BowserController toolExport;
        private AEToggleButton buttonInventory;
        private AEToggleButton buttonBincard;
        private AEToggleButton buttonMaterialFlow;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private PeriodSelector period2;
        private PeriodSelector period1;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private AEToggleButton buttonCenterBalance;
        private AEToggleButton buttonCostCenterProfile;
        private DevExpress.XtraEditors.SimpleButton buttonReloadTree;
        private DevExpress.XtraEditors.ComboBoxEdit comboGroup;
        private AEToggleButton buttonCashFlow;
        private DevExpress.XtraEditors.ComboBoxEdit comboDate2Mode;
    }
}