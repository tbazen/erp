﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    public partial class AccountExplorer : DevExpress.XtraEditors.XtraForm
    {
        private void generateTitleForTrialBalanceSheet(StringBuilder builder, DateTime date, CostCenter agregateTo)
        {
            TSConstants.addCompanyNameLine(iERPTransactionClient.GetCompanyProfile(), builder);
            builder.Append("<h2>Balance Sheet</h2>");
            builder.Append(string.Format("<span class='SubTitle'><b>As of</b> <u>{0}</u></span>", SummaryInformation.formatDate2(date)));
            builder.Append(string.Format("<span class='SubTitle'><b>{0}</b><u>{1}</u></span>", "Accounting Center :", agregateTo.NameCode));
        }
        public void buildBalanceSheet(DateTime date,CostCenter costCenter)
        {
            StringBuilder builder= new StringBuilder();
            generateTitleForTrialBalanceSheet(builder, date, costCenter);
            int maxLevel;
            builder.Append("<br/><br/>");
            builder.Append(iERPTransactionClient.renderSummaryTable( "reportBalanceSheet1",costCenter.id, DateTime.Now,date,out maxLevel));
            _genertedHTML = builder.ToString();
        }
    }
}