﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using INTAPS.Accounting;
using System.Web;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class AccountExplorer : DevExpress.XtraEditors.XtraForm
    {
        static private void generateTitleForBinCard(StringBuilder builder, DateTime date1, DateTime date2, CostCenter costCenter,Account account,TransactionItems item)
        {
            TSConstants.addCompanyNameLine(iERPTransactionClient.GetCompanyProfile(), builder);
            builder.Append("<h2>Bin Card</h2>");
            builder.Append(string.Format("<span class='SubTitle'><b>{0}</b><u>{1}</u></span>", HttpUtility.HtmlEncode("Accounting Center: "), HttpUtility.HtmlEncode(costCenter.NameCode)));
            if(item==null)
                builder.Append(string.Format("<span class='SubTitle'><b>{0}</b><u>{1}</u></span>", HttpUtility.HtmlEncode("Account: "), HttpUtility.HtmlEncode(account.CodeName)));
            else
                builder.Append(string.Format("<span class='SubTitle'><b>{0}</b><u>{1}</u></span>", HttpUtility.HtmlEncode("Item: "), HttpUtility.HtmlEncode(item.Code+"-"+item.Name)));
            builder.Append(string.Format("<span class='SubTitle'><b>From</b> <u>{0}</u> <b>to</b> <u>{1}</u></span>", HttpUtility.HtmlEncode(AccountBase.FormatDate(date1)),
                HttpUtility.HtmlEncode(SummaryInformation.formatDate2(date2))));

        }
        public void buildBinCard(DateTime date1, DateTime date2, CostCenter costCenter, Account account)
        {
            _genertedHTML = getBinCard(date1, date2, costCenter, account);
        }
        public static string getBinCard(DateTime date1, DateTime date2, CostCenter costCenter, Account account)
        {
            StringBuilder builder = new StringBuilder();
            
            if (costCenter == null || account == null)
            {
                builder.Append("<b>Select a accounting center and an item</b>");
            }
            else
            {
                TransactionItems item = iERPTransactionClient.GetTransactionItems(account.id);
                generateTitleForBinCard(builder, date1, date2, costCenter, account,item);
                builder.Append("<br/><br/>");
                CostCenterAccount csAccount = AccountingClient.GetCostCenterAccount(costCenter.id, account.id);
                if (csAccount == null)
                    builder.Append("<b>Account not attached with the selected accounting center");
                else
                    builder.Append(iERPTransactionClient.renderLedgerTable(
                        new LedgerParameters()
                        {
                            costCenterID=costCenter.id,
                            accountIDs = new int[] { account.id },
                            itemID = TransactionItem.MATERIAL_QUANTITY,
                            time1 = date1,
                            time2 = date2,
                            groupByPeriod = null,
                            hideAccountColumnIfUniformAccount = true,
                            hideItemColumnIfUniformItem = true,
                            twoColumns = true,
                        }
                        ));
            }
            return builder.ToString();
        }
    }
}