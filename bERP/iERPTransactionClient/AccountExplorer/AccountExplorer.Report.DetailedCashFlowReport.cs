﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using INTAPS.Accounting;
using System.Web;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class AccountExplorer : DevExpress.XtraEditors.XtraForm
    {
        private void generateTitleForDetailedCashFlowReport(StringBuilder builder, DateTime date1, DateTime date2,CostCenter costCenter)
        {
            TSConstants.addCompanyNameLine(iERPTransactionClient.GetCompanyProfile(), builder);
            builder.Append("<h2>Detailed Cash Flow Report</h2>");
            if (costCenter != null && costCenter.PID != -1)
            {
                builder.Append(string.Format("<span class='SubTitle'><b>Cost Center: </b><u>{0}</u></span>", HttpUtility.HtmlEncode(costCenter.Name)));
            }
            builder.Append(string.Format("<span class='SubTitle'><b>From</b> <u>{0}</u> <b>to</b> <u>{1}</u></span>", HttpUtility.HtmlEncode(AccountBase.FormatDate(date1)),
                HttpUtility.HtmlEncode(SummaryInformation.formatDate2(date2))));
        }
        void generateFooterForCashFlowReport(StringBuilder builder)
        {
            string html = "<div class='footerDiv'>Output of BizNet Cost Accounting, BizNet Technology plc Tel. 0911 200464/0911 227220.</div>";
            builder.Append(html);
        }
        public void buildDetailedCashFlowReport(DateTime date1, DateTime date2, CostCenter filter)
        {
            StringBuilder builder = new StringBuilder();

            generateTitleForDetailedCashFlowReport(builder, date1, date2, filter);
            builder.Append("<br/><br/>");
            builder.Append(iERPTransactionClient.generateDetaileCashFlowReport(date1, date2,new DetailedCashFlowReportOption(filter==null|| filter.PID==-1?-1:filter.id)));
            generateFooterForCashFlowReport(builder);
            _genertedHTML = builder.ToString();
        }
    }
}