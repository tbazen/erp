﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    public partial class AccountExplorer : DevExpress.XtraEditors.XtraForm
    {
        private void generateTitleForIncomeStatement(StringBuilder builder, DateTime date1, DateTime date2, CostCenter agregateTo)
        {
            TSConstants.addCompanyNameLine(iERPTransactionClient.GetCompanyProfile(), builder);
            builder.Append("<h2>Income Statement</h2>");
            builder.Append(string.Format("<span class='SubTitle'><b>From</b> <u>{0}</u> <b>to</b> <u>{1}</u></span>", AccountBase.FormatDate(date1), SummaryInformation.formatDate2(date2)));
            builder.Append(string.Format("<span class='SubTitle'><b>{0}</b><u>{1}</u></span>", "Accounting Center: ", agregateTo.NameCode));
        }
        public void buildIncomeStatement(DateTime date1, DateTime date2, CostCenter costCenter)
        {
            StringBuilder builder = new StringBuilder();
            generateTitleForIncomeStatement(builder, date1, date2, costCenter);
            int maxLevel;
            builder.Append(iERPTransactionClient.renderSummaryTable("reportIncomeStatement1", costCenter.id, date1, date2, out maxLevel));
            _genertedHTML = builder.ToString();
        }

    }
}