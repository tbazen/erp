﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using INTAPS.Accounting;
using INTAPS.UI.HTML;
using System.Web.UI.HtmlControls;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class AccountExplorer : DevExpress.XtraEditors.XtraForm
    {

        void GenerateInventoryList(CostCenter costCenter, DateTime to)
        {
            if (costCenter == null)
            {
                _genertedHTML = "<h1>Please select a store or accounting center</h1>";
                return;
            }

            try
            {
                _genertedHTML = iERPTransactionClient.renderInventoryTable(
                    new InventoryGeneratorParameters()
                    { costCenterID=costCenter.id,
                        to=to,
                        includeTitle=true}
                        );
            }
            catch (Exception ex)
            {
                _genertedHTML = "<h1>" + System.Web.HttpUtility.HtmlEncode(ex.Message) + "</h1>";
            }
        }
    }
}