﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using INTAPS.Accounting;
using System.Web;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class AccountExplorer : DevExpress.XtraEditors.XtraForm
    {
        static internal void generateTitleForLedger(StringBuilder builder, DateTime date1, DateTime date2, string costCenterTitle,string accountTitle)
        {
            TSConstants.addCompanyNameLine(iERPTransactionClient.GetCompanyProfile(), builder);
            builder.Append("<h2>Ledger</h2>");
            builder.Append(string.Format("<span class='SubTitle'><b>{0}</b><u>{1}</u></span><br/>", HttpUtility.HtmlEncode("Account: "), HttpUtility.HtmlEncode(accountTitle)));
            builder.Append(string.Format("<span class='SubTitle'><b>{0}</b><u>{1}</u></span><br/>", HttpUtility.HtmlEncode("Accounting Center: "), HttpUtility.HtmlEncode(costCenterTitle)));
            builder.Append(string.Format("<span class='SubTitle'><b>From</b> <u>{0}</u> <b>to</b> <u>{1}</u></span>", HttpUtility.HtmlEncode(AccountBase.FormatDate(date1)),
                HttpUtility.HtmlEncode(SummaryInformation.formatDate2(date2))));
        }
        static internal string buildLedger(DateTime date1, DateTime date2, CostCenter costCenter, Account account, ReportType ledgerType)
        {
            if (costCenter == null || account == null)
            {
                return "<b>Select a costcenter and an account</b>";
            }
            return buildLedger(date1, date2,costCenter.id,new int[]{ account.id},costCenter.NameCode, account.NameCode, account.CreditAccount, ledgerType,false);
        }
        static internal string buildLedger(DateTime date1, DateTime date2,int costCenterID, int[] accountIDs, string costCenterTitle, string accountTitle,bool asCreditAccount, ReportType ledgerType,bool merged)
        {
            StringBuilder builder = new StringBuilder();

            generateTitleForLedger(builder, date1, date2, costCenterTitle, accountTitle);
            builder.Append("<br/><br/>");
            string periodName = null;
            switch (ledgerType)
            {
                case ReportType.Ledger_Ungrouped_Month:
                    periodName = "AP_Accounting_Month";
                    break;
                case ReportType.Ledger_Ungrouped_Day:
                    periodName = InternationalDayPeriod.NAME;
                    break;
            }
            builder.Append(iERPTransactionClient.renderLedgerTable(
                new LedgerParameters()
                {
                    costCenterID=costCenterID,
                    asCreditAccount = asCreditAccount,
                    accountIDs = accountIDs,
                    itemID = 0,
                    time1 = date1,
                    time2 = date2,
                    groupByPeriod = periodName,
                    hideAccountColumnIfUniformAccount = true,
                    hideItemColumnIfUniformItem = true,
                    twoColumns = true,
                    merged=merged
                }));
            return builder.ToString();
        }
    }
}