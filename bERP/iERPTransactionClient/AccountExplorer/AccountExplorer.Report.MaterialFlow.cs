﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using INTAPS.Accounting;
using INTAPS.UI.HTML;
using System.Web.UI.HtmlControls;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class AccountExplorer : DevExpress.XtraEditors.XtraForm
    {

        void GenerateMaterialFlowListList(CostCenter costCenter,DateTime from, DateTime to)
        {
            if (costCenter == null)
            {
                _genertedHTML = "<h1>Please select a store or accounting center</h1>";
                return;
            }

            StringBuilder builder = new StringBuilder();
            generateTitleForMaterialFlowList(builder, costCenter,from, to);
            bERPHtmlTable table = new bERPHtmlTable();
            bERPHtmlTableRowGroup header = new bERPHtmlTableRowGroup(TableRowGroupType.header);
            table.groups.Add(header);
            bERPHtmlBuilder.htmlAddRow(header,
                    new bERPHtmlTableCell(TDType.ColHeader, "Code","",2,1)
                    , new bERPHtmlTableCell(TDType.ColHeader, "Name", "", 2, 1)
                    , new bERPHtmlTableCell(TDType.ColHeader, "In Flow","centeredCell",1, 3)
                    , new bERPHtmlTableCell(TDType.ColHeader, "Out Flow", "centeredCell", 1, 3)
                    , new bERPHtmlTableCell(TDType.ColHeader, "Net Increase", "centeredCell", 1, 3)
                    );

            bERPHtmlBuilder.htmlAddRow(header
                    , new bERPHtmlTableCell(TDType.ColHeader, "Quantity", "CurrencyCell")
                    , new bERPHtmlTableCell(TDType.ColHeader, "Unit Price", "CurrencyCell")
                    , new bERPHtmlTableCell(TDType.ColHeader, "Price", "CurrencyCell")
                    
                    , new bERPHtmlTableCell(TDType.ColHeader, "Quantity", "CurrencyCell")
                    , new bERPHtmlTableCell(TDType.ColHeader, "Unit Price", "CurrencyCell")
                    , new bERPHtmlTableCell(TDType.ColHeader, "Price", "CurrencyCell")
                    
                    , new bERPHtmlTableCell(TDType.ColHeader, "Quantity", "CurrencyCell")
                    , new bERPHtmlTableCell(TDType.ColHeader, "Unit Price", "CurrencyCell")
                    , new bERPHtmlTableCell(TDType.ColHeader, "Price", "CurrencyCell")
                );
            bERPHtmlTableRowGroup body = new bERPHtmlTableRowGroup(TableRowGroupType.body);
            table.groups.Add(body);
            List<int> levels = new List<int>();
            List<bool> leaf = new List<bool>();
            double totalInFlow;
            double totalOutFlow;
            double totalBeg;
            buildMaterialFlowList(body,from, to, costCenter, -1, 0, levels, leaf, 0, 0,0,out totalBeg, out totalInFlow,out totalOutFlow);

            int maxLevel = 0;
            foreach (int l in levels)
                if (l > maxLevel)
                    maxLevel = l;
            if (maxLevel > 0)
            {
                header.rows[0].cells[1].colSpan = maxLevel + 1;
                int i = 0;
                foreach (bERPHtmlTableRow row in body.rows)
                {
                    if (leaf[i])
                        row.cells[1].colSpan = maxLevel - levels[i] + 1;
                    else
                        row.cells[1].colSpan = maxLevel - levels[i] + 1;
                    for (int k = 0; k < levels[i]; k++)
                    {
                        row.cells.Insert(1, new bERPHtmlTableCell("", "IndentCell"));
                    }
                    i++;
                }
            }
            bERPHtmlTableRowGroup footer = new bERPHtmlTableRowGroup(TableRowGroupType.footer);
            table.groups.Add(footer);

            bERPHtmlBuilder.htmlAddRow(footer,
                new bERPHtmlTableCell(TDType.ColHeader, "Total", 2 + maxLevel)
                , new bERPHtmlTableCell(TDType.ColHeader, AccountBase.FormatBalance(totalInFlow ),"CurrencyCell", 1,3)
                , new bERPHtmlTableCell(TDType.ColHeader, AccountBase.FormatBalance(totalOutFlow), "CurrencyCell", 1, 3)
                , new bERPHtmlTableCell(TDType.ColHeader, AccountBase.FormatBalance(totalInFlow - totalOutFlow), "CurrencyCell", 1,3)
            );

            table.build(builder);
            _genertedHTML = builder.ToString();

        }

        private void generateTitleForMaterialFlowList(StringBuilder builder, CostCenter agregateTo,DateTime date1, DateTime date2)
        {
            TSConstants.addCompanyNameLine(iERPTransactionClient.GetCompanyProfile(), builder);

            builder.Append("<h2>Material Flow</h2>");
            builder.Append(string.Format("<span class='SubTitle'><b>From</b> <u>{0}</u> <b>to</b> <u>{1}</u></span>", AccountBase.FormatDate(date1), SummaryInformation.formatDate2(date2)));
            builder.Append(string.Format("<span class='SubTitle'><b>Store/Accounting Center: </b> <u>{0}</u></span>", agregateTo.NameCode));
            
        }

        private int buildMaterialFlowList(bERPHtmlTableRowGroup rows,DateTime date1, DateTime date2, CostCenter agregateTo, int parentCategory, int level, List<int> levels, List<bool> leaf
            ,double totalBegIn, double totalInFlowIn, double totalOutFlowIn,out double totalBegOut, out double totalInFlowOut, out double totalOutFlowOut
            )
        {

            totalInFlowOut = totalInFlowIn;
            totalOutFlowOut = totalOutFlowIn;
            totalBegOut = totalBegIn;
            int count = 0;
            foreach (ItemCategory cat in iERPTransactionClient.GetItemCategories(parentCategory))
            {
                int index = rows.rows.Count;
                double prevInFlow = totalInFlowOut;
                double prevOutFlow = totalOutFlowOut;
                int c = buildMaterialFlowList(rows, date1, date2, agregateTo, cat.ID, level + 1, levels, leaf,totalBegOut, totalInFlowOut, totalOutFlowOut
                    , out totalBegOut, out totalInFlowOut, out totalOutFlowOut);
                if (c > 0)
                {
                    levels.Insert(index, level);
                    leaf.Insert(index, false);
                    bERPHtmlTableRow catRow = new bERPHtmlTableRow();
                    catRow.cells.Add(new bERPHtmlTableCell(cat.Code));
                    catRow.cells.Add(new bERPHtmlTableCell(cat.description));
                    catRow.cells.Add(new bERPHtmlTableCell(TDType.body, AccountBase.FormatBalance(totalInFlowOut-prevInFlow), "CurrencyCell", 1, 3));
                    catRow.cells.Add(new bERPHtmlTableCell(TDType.body, AccountBase.FormatBalance(totalOutFlowOut-prevOutFlow), "CurrencyCell", 1, 3));
                    catRow.cells.Add(new bERPHtmlTableCell(TDType.body, AccountBase.FormatBalance(totalInFlowOut - totalOutFlowOut
                        -(prevInFlow-prevOutFlow)
                        ), "CurrencyCell", 1, 3));
                    catRow.css = "Section_Header_1";

                    rows.rows.Insert(index, catRow);
                    count += 1 + c;
                }
            }
            int rowIndex = 0;
            foreach (TransactionItems item in iERPTransactionClient.GetItemsInCategory(parentCategory))
            {
                CostCenterAccount csa = AccountingClient.GetCostCenterAccount(agregateTo.id, item.inventoryAccountID);
                if (csa == null)
                    continue;
                AccountBalance q1 = AccountingClient.GetBalanceAsOf(csa.id,TransactionItem.MATERIAL_QUANTITY, date1);
                AccountBalance q2 = AccountingClient.GetBalanceAsOf(csa.id, TransactionItem.MATERIAL_QUANTITY, date2);

                AccountBalance p1 = AccountingClient.GetBalanceAsOf(csa.id, TransactionItem.DEFAULT_CURRENCY, date1);
                AccountBalance p2 = AccountingClient.GetBalanceAsOf(csa.id, TransactionItem.DEFAULT_CURRENCY, date2);

                if (q1.IsZero && q2.IsZero && p1.IsZero && p2.IsZero)
                    continue;

                double qdb = q2.TotalDebit - q1.TotalDebit;
                double qcr = q2.TotalCredit- q1.TotalCredit;
                double pdb = p2.TotalDebit - p1.TotalDebit;
                double pcr = p2.TotalCredit - p1.TotalCredit;

                double pnet = p2.DebitBalance - p1.DebitBalance;
                double qnet = q2.DebitBalance - q1.DebitBalance;

                string up1 = q1.IsZero && !p1.IsZero ? "Data Error" : AccountBase.FormatAmount(AccountingClient.GetAverageUnitPrice(q1.DebitBalance, p1.DebitBalance));
                string updb = AccountBase.AmountEqual(qdb, 0) && !AccountBase.AmountEqual(pdb, 0) ? "Data Error"
                    : AccountBase.FormatAmount(AccountingClient.GetAverageUnitPrice(qdb,pdb));
                string upcr = AccountBase.AmountEqual(qcr, 0) && !AccountBase.AmountEqual(pcr, 0) ? "Data Error"
                    : AccountBase.FormatAmount(AccountingClient.GetAverageUnitPrice(qcr, pcr));

                string upnet = AccountBase.AmountEqual(qnet, 0) && !AccountBase.AmountEqual(pnet, 0) ? "Data Error"
                    : AccountBase.FormatAmount(AccountingClient.GetAverageUnitPrice(qnet, pnet));
                
                levels.Add(level);
                leaf.Add(true);
                
                bERPHtmlTableRow itemRow = new bERPHtmlTableRow();
                itemRow.cells.Add(new bERPHtmlTableCell(item.Code));
                itemRow.cells.Add(new bERPHtmlTableCell(item.Name));
                //itemRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatBalance(q1.DebitBalance),"CurrencyCell"));
                //itemRow.cells.Add(new bERPHtmlTableCell(up1,"CurrencyCell"));
                //itemRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatBalance(p1.DebitBalance), "CurrencyCell"));

                itemRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatBalance(qdb), "CurrencyCell"));
                itemRow.cells.Add(new bERPHtmlTableCell(updb, "CurrencyCell"));
                itemRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatBalance(pdb), "CurrencyCell"));

                itemRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatBalance(qcr), "CurrencyCell"));
                itemRow.cells.Add(new bERPHtmlTableCell(upcr, "CurrencyCell"));
                itemRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatBalance(pcr), "CurrencyCell"));

                itemRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatBalance(qnet), "CurrencyCell"));
                itemRow.cells.Add(new bERPHtmlTableCell(upnet, "CurrencyCell"));
                itemRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatBalance(pnet), "CurrencyCell"));

                
                totalInFlowOut += pdb;
                totalOutFlowOut += pcr;
                totalBegOut += p1.DebitBalance;

                itemRow.css = rowIndex % 2 == 0 ? "Even_row" : "Odd_row";
                rows.rows.Add(itemRow);
                rowIndex++;
                count++;
            }
            return count;
        }

    }
}