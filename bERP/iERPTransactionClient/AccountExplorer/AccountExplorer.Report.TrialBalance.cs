﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using INTAPS.Accounting;
using INTAPS.UI.HTML;
using System.Web.UI.HtmlControls;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class AccountExplorer : DevExpress.XtraEditors.XtraForm
    {
        const string BIRR_FORMAT="#,#0.00";
        const string DATE_FORMAT="dd/MM/yyyy hh:mm";
        CostCenterAccount getCostCenterAccount<AgregateType, DetailType>(AgregateType aval, DetailType dval)
            where AgregateType : AccountBase, new()
            where DetailType : AccountBase, new()
        {
            if (typeof(AgregateType) == typeof(Account))
            {
                return AccountingClient.GetCostCenterAccount((dval as CostCenter).id, (aval as Account).id);
            }
            return AccountingClient.GetCostCenterAccount((aval as CostCenter).id, (dval as Account).id);
        }
        internal static void collectExpanded(Dictionary<int, object> structure, List<int> expanded)
        {
            foreach (KeyValuePair<int, object> kv in structure)
            {
                if (kv.Value is Dictionary<int, object>)
                {
                    expanded.Add(kv.Key);
                    collectExpanded((Dictionary<int, object>)kv.Value, expanded);
                }
            }
        }
        void GenerateTrialBalanceLinear<AgregateType,DetailType>(Dictionary<int,object> structure, AgregateType agregateTo,bool twoColumns)
            where AgregateType:AccountBase,new()
            where DetailType : AccountBase, new()
        {
            if (agregateTo == null)
            {
                _genertedHTML = "<h1>Please select " + AccountBase.GetAccountTypeDescription<AgregateType>(false) + "</h1>";
                return;
            }
            StringBuilder builder = new StringBuilder();
            generateTitleForTrialBalanceLiniar<DetailType,AgregateType>(builder,agregateTo);
            /*bERPHtmlTable table = buildLinearTrialBalance<AgregateType, DetailType>(structure, agregateTo, twoColumns);
            table.build(builder);*/
            List<int> expanded=new List<int>();
            collectExpanded(structure,expanded);
            builder.Append(iERPTransactionClient.getTrialBalance<AgregateType,DetailType>(expanded,accountBrowser.getRootAccounts(), agregateTo,_thread_to,twoColumns));
            
            _genertedHTML = builder.ToString();
        }

        private void generateTitleForTrialBalanceLiniar<DetailType, AgregateType>(StringBuilder builder, AgregateType agregateTo) 
            where DetailType : AccountBase, new()
            where AgregateType : AccountBase, new()

        {
            TSConstants.addCompanyNameLine(iERPTransactionClient.GetCompanyProfile(), builder);
            if (typeof(DetailType) == typeof(Account))
            {
                builder.Append("<h2>Trial Balance</h2>");
                builder.Append(string.Format("<span class='SubTitle'><b>As of</b> <u>{0}</u></span>", SummaryInformation.formatDate2(_thread_to)));
                builder.Append(string.Format("<span class='SubTitle'><b>Accounting Center: </b> <u>{0}</u></span>", agregateTo.NameCode));
            }
            else
            {
                builder.Append("<h2>Account Balance by Cost Center</h2>");
                builder.Append(string.Format("<span class='SubTitle'><b>As of</b> <u>{0}</u></span>", SummaryInformation.formatDate2(_thread_to)));
                builder.Append(string.Format("<span class='SubTitle'><b>Account :</b> <u>{0}</u></span>", agregateTo.NameCode));

            }
        }
    }
}