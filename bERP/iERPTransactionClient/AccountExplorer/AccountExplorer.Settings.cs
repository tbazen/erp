﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;

namespace BIZNET.iERP.Client
{
    public partial class AccountExplorer : DevExpress.XtraEditors.XtraForm
    {
        private void loadSettings()
        {
            try
            {
                SelectedReportType = (ReportType)Enum.Parse(typeof(ReportType), Properties.Settings.Default.AccountExporer_ReportType);
            }
            catch
            {
                SelectedReportType = ReportType.None;
            }
                
                //restore tree state
                try
                {
                    Dictionary<int, object> list = INTAPS.SerializationExtensions.loadFromBytes<Dictionary<int, object>>(INTAPS.SerializationExtensions .loadFromXml<byte[]>(Properties.Settings.Default.AccountExplorer_CostCenterExpanded));
                    accountBrowser.setVisisbleCostCenterStructure(list);
                    list = INTAPS.SerializationExtensions.loadFromBytes<Dictionary<int, object>>(INTAPS.SerializationExtensions.loadFromXml<byte[]>(Properties.Settings.Default.AccountExplorer_AccountExpanded));
                    accountBrowser.setVisisbleAccountStructure(list);
                    accountBrowser.SetCostCenterByID(Properties.Settings.Default.AccountExplorer_SelectedCostCenterID);
                    accountBrowser.SetAccountByID(Properties.Settings.Default.AccountExplorer_SelectedAccountID);
                }
                catch
                {
                    MessageBox.ShowErrorMessage("Error trying to load UI configuration.\nYou can safely ignore this error");
                }
                date1.DateTime = Properties.Settings.Default.AccountExplorer_DateFrom;
                setDate2(Properties.Settings.Default.AccountExplorer_DateTo);
        }
        void saveSettings()
        {

            Properties.Settings.Default.AccountExporer_ReportType = SelectedReportType.ToString();            
            Dictionary<int, object> list = accountBrowser.getVisisbleCostCenterStructure();
            Properties.Settings.Default.AccountExplorer_CostCenterExpanded = INTAPS.SerializationExtensions.getXml(INTAPS.SerializationExtensions.getBytes(list));
            list = accountBrowser.getVisisbleAccountStructure();
            Properties.Settings.Default.AccountExplorer_AccountExpanded = INTAPS.SerializationExtensions.getXml(INTAPS.SerializationExtensions.getBytes(list));
            Properties.Settings.Default.AccountExplorer_DateFrom=date1.DateTime;
            Properties.Settings.Default.AccountExplorer_DateTo = getDate2();
            Properties.Settings.Default.AccountExplorer_SelectedCostCenterID = accountBrowser.SelectedCostCenter == null ? -1 : accountBrowser.SelectedCostCenter.id;
            Properties.Settings.Default.AccountExplorer_SelectedAccountID=accountBrowser.SelectedAccount==null?-1:accountBrowser.SelectedAccount.id;
            
            Properties.Settings.Default.Save();
        }

    }
}