﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using INTAPS.Accounting;
using System.Web;
using System.Threading;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class AccountExplorer : DevExpress.XtraEditors.XtraForm, IHTMLReportHost
    {
        Dictionary<int, object> _costCenterStructure = null;
        Dictionary<int, object> _accountStructure= null;
        DateTime _from=DateTime.Now, _to=DateTime.Now;
        ReportType _type=ReportType.None;
        Account _selectedAccount = null;
        CostCenter _selectedCostCenter = null;
        
        Dictionary<int, object> _thread_costCenterStructure = null;
        Dictionary<int, object> _thread_accountStructure = null;
        DateTime _thread_from = DateTime.Now, _thread_to = DateTime.Now;
        ReportType _thread_type = ReportType.None;
        Account _thread_selectedAccount = null;
        CostCenter _thread_selectedCostCenter = null;

        
        System.Threading.Thread _reportThread = null;
        string _genertedHTML="";
        HTMLLinkHandler _handler = null;


        private void UpdateReport(bool loadControlData)
        {
            _genertedHTML="<h1>Click 'Generate' to generate report</h1>";
            if (loadControlData)
            {
                getDataFromControls();
            }
            if (_handler == null)
            {
                _handler = new HTMLLinkHandler(this);
                webBrowser.LoadControl(_handler);
            }
            else
                webBrowser.UpdateElementInnerHtml("report", _genertedHTML);
        }
        void setDate2(DateTime date2Value)
        {

            if (date2Value.Hour == 23 && date2Value.Minute == 59 && date2Value.Second < 30)
            {
                comboDate2Mode.SelectedIndex = 1;
                date2.DateTime = date2Value.Date;
            }
            else if (date2Value.Hour == 23 && date2Value.Minute == 59 && date2Value.Second > 30)
            {
                comboDate2Mode.SelectedIndex = 2;
                date2.DateTime = date2Value.Date;
            }
            else
            {
                comboDate2Mode.SelectedIndex = 0;
                date2.DateTime = date2Value.Date.AddDays(-1);
            }
        }
        DateTime getDate2()
        {
            switch (comboDate2Mode.SelectedIndex)
            {
                case 1:
                    return new DateTime(date2.DateTime.Year, date2.DateTime.Month, date2.DateTime.Day, 23, 59, 0);
                case 2:
                    return new DateTime(date2.DateTime.Year, date2.DateTime.Month, date2.DateTime.Day, 23, 59, 59);
                default:
                    return date2.DateTime.AddDays(1);
            }
        }
        private void getDataFromControls()
        {
            _costCenterStructure = accountBrowser.getVisisbleCostCenterStructure();
            _accountStructure = accountBrowser.getVisisbleAccountStructure();
            _from = date1.DateTime.Date;
            _to = getDate2();
            _type = SelectedReportType;
            _selectedAccount = accountBrowser.SelectedAccount;
            _selectedCostCenter = accountBrowser.SelectedCostCenter;
        }

        private void startThread()
        {

            if (_type != _thread_type )
            {
                setThreadParsAndStart();
                return;
            }
            if (_type == ReportType.Ledger || _type==ReportType.Ledger_Ungrouped_Month
                || _type == ReportType.Ledger_Ungrouped_Day
                || _type==ReportType.BinCard)
            {
                if (_selectedAccount != _thread_selectedAccount
                    || _selectedCostCenter != _thread_selectedCostCenter
                    || !_to.Equals(_thread_to)
                    || !_from.Equals(_thread_from))
                {
                    setThreadParsAndStart();
                    return;
                }
            }

            if (_type == ReportType.TrialBalance)
            {
                if (!_to.Equals(_thread_to))
                {
                    setThreadParsAndStart();
                    return;
                }

                if (_accountStructure != _thread_accountStructure
                    ||
                    (_selectedCostCenter != _thread_selectedCostCenter
                    && (_selectedCostCenter == null || _thread_selectedCostCenter == null
                    || _selectedCostCenter.id != _thread_selectedCostCenter.id)
                    )
                    )
                {
                    setThreadParsAndStart();
                    return;
                }
            }

            if (_type == ReportType.CostCenterBalances)
            {
                if (!_to.Equals(_thread_to))
                {
                    setThreadParsAndStart();
                    return;
                }

                if (_costCenterStructure != _thread_costCenterStructure
                    || (_selectedAccount != _thread_selectedAccount
                    && (_selectedAccount == null || _thread_selectedAccount == null
                    || _selectedAccount.id != _thread_selectedAccount.id)
                    )
                    )
                {
                    setThreadParsAndStart();
                    return;
                }
            }


            if (_type == ReportType.BlanceSheet || _type==ReportType.Inventory)
            {
                if (!_to.Equals(_thread_to))
                {
                    setThreadParsAndStart();
                    return;
                }

                if (_selectedCostCenter != _thread_selectedCostCenter
                    && (_selectedCostCenter == null || _thread_selectedCostCenter == null
                    || _selectedCostCenter.id != _thread_selectedCostCenter.id)
                    )
                {
                    setThreadParsAndStart();
                    return;
                }
            }

            if (_type == ReportType.DetailedCashFlowReport)
            {
                if (!_to.Equals(_thread_to) || !_from.Equals(_thread_from) 
                    || _selectedCostCenter != _thread_selectedCostCenter)
                {
                    setThreadParsAndStart();
                    return;
                }
            }

            if (_type == ReportType.IncomeStatement|| _type==ReportType.MaterialFlow
                || _type==ReportType.CostCenterProfile
                || _type == ReportType.CashFlow
                )
            {
                if (!_to.Equals(_thread_to) || !_from.Equals(_thread_from))
                {
                    setThreadParsAndStart();
                    return;
                }

                if (_selectedCostCenter != _thread_selectedCostCenter
                    && (_selectedCostCenter == null || _thread_selectedCostCenter == null
                    || _selectedCostCenter.id != _thread_selectedCostCenter.id)
                    )
                {
                    setThreadParsAndStart();
                    return;
                }
            }

        }
        
        void setThreadParsAndStart()
        {
            _thread_costCenterStructure = _costCenterStructure;
            _thread_accountStructure = _accountStructure;
            _thread_from = _from;
            _thread_to = _to;
            _thread_type = _type;
            _thread_selectedCostCenter = _selectedCostCenter;
            _thread_selectedAccount = _selectedAccount;

            _reportThread = new System.Threading.Thread(new System.Threading.ThreadStart(generateThread));
            _reportThread.Start();
            pnlWaitPanel.Show();
            timerCheckThread.Enabled = true;
        }
        
        void generateThread()
        {
            if (_costCenterStructure == null
                    || _accountStructure == null
                    || _type == ReportType.None
                    )
            {
                _genertedHTML = "<h1>Please select correct report generation parameters</h1>";
                return;
            }
            try
            {
                switch (_thread_type)
                {
                    case ReportType.DetailedCashFlowReport:
                        buildDetailedCashFlowReport(_thread_from, _thread_to,_thread_selectedCostCenter);
                        break;
                    case ReportType.Ledger:
                    case ReportType.Ledger_Ungrouped_Month:
                    case ReportType.Ledger_Ungrouped_Day:
                        _genertedHTML=buildLedger(_thread_from, _thread_to, _thread_selectedCostCenter, _thread_selectedAccount,_thread_type);
                        break;

                    case ReportType.TrialBalance:
                        GenerateTrialBalanceLinear<CostCenter, Account>(_thread_accountStructure, _thread_selectedCostCenter, true);
                        break;
                    case ReportType.CostCenterBalances:
                        GenerateTrialBalanceLinear<Account, CostCenter>(_thread_costCenterStructure, _thread_selectedAccount, false);
                        break;
                    case ReportType.BlanceSheet:
                        buildBalanceSheet(_thread_to,_thread_selectedCostCenter);
                        break;
                    case ReportType.IncomeStatement:
                        buildIncomeStatement(_thread_from, _thread_to, _thread_selectedCostCenter);
                        break;
                    case ReportType.Inventory:
                        GenerateInventoryList(_thread_selectedCostCenter, _thread_to);
                        break;
                    case ReportType.BinCard:
                        buildBinCard(_thread_from, _thread_to,_thread_selectedCostCenter,_thread_selectedAccount);
                        break;
                    case ReportType.MaterialFlow:
                        GenerateMaterialFlowListList(_thread_selectedCostCenter,_thread_from, _thread_to);
                        break;
                    case ReportType.CostCenterProfile:
                        GenerateCostCenterProfile(_thread_selectedCostCenter,_thread_from, _thread_to);
                        break;
                    case ReportType.CashFlow:
                        buildCashFlow(_thread_from, _thread_to, _thread_selectedCostCenter);
                        break;
                    default:
                        _genertedHTML = "<h1>Select Report Type</h1>";
                        break;
                }
            }
            catch (Exception ex)
            {
                _genertedHTML = "<h1>" + HttpUtility.HtmlEncode("Error trying to generate report:" + ex.Message) + "</h1>";
            }
        }
        private void timerCheckThread_Tick(object sender, EventArgs e)
        {
            if (!_reportThread.IsAlive)
            {
                if (_handler == null)
                {
                    _handler = new HTMLLinkHandler(this);
                    webBrowser.LoadControl(_handler);
                }
                else
                    webBrowser.UpdateElementInnerHtml("report", _genertedHTML);
                pnlWaitPanel.Hide();
                timerCheckThread.Stop();
                startThread();
            }
        }
        public class HTMLLinkHandler : INTAPS.UI.HTML.IHTMLBuilder
        {
            IHTMLReportHost _parent;
            public HTMLLinkHandler(IHTMLReportHost e)
            {
                _parent = e;
            }

            public void Build()
            {

            }

            public string GetOuterHtml()
            {
                return "<span id='report'>"+_parent.generatledHtml+"</span>";
            }

            public bool ProcessUrl(string path, Hashtable query)
            {
                return AccountExplorer.ProcessUrl(_parent.form, path, query, new INTAPS.UI.HTML.ProcessParameterless(_parent.invalidateReport));
            }

            public void SetHost(INTAPS.UI.HTML.IHTMLDocumentHost host)
            {

            }

            public string WindowCaption
            {
                get { return "Report"; }
            }
        }
        class ReportAccountClientHook:IAccountingClient
        {
            INTAPS.UI.HTML.ProcessParameterless  _reloadMethod;
            public ReportAccountClientHook(INTAPS.UI.HTML.ProcessParameterless reloadMethod)
            {
                _reloadMethod = reloadMethod;
            }
            public int PostGenericDocument(AccountDocument doc)
            {
                int ret= INTAPS.Accounting.Client.AccountingClient.PostGenericDocument(doc);
                try
                {
                    _reloadMethod();
                }
                catch
                {
                }
                return ret;
            }
        }
        static internal bool ProcessUrl(Form parent,string path, Hashtable query,INTAPS.UI.HTML.ProcessParameterless reloadMethod)
        {
            switch (path)
            {

                case SummaryInformation.LINK_OPEN_DOC:
                    int docID = int.Parse((string)query["docID"]);
                    int docTypeID = int.Parse((string)query["docTypeID"]);
                    try
                    {
                        IGenericDocumentClientHandler client = INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(docTypeID);
                        if (client == null || !client.CanEdit)
                        {
                            MessageBox.ShowNormalWarningMessage(string.Format("Sorry, {0} transactions can't be edited", INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByID(docTypeID).name));
                            return true;
                        }
                        client.setAccountingClient(new ReportAccountClientHook(reloadMethod));
                        Form f = client.CreateEditor(false) as Form;
                        AccountDocument doc = INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(docID, true);
                        client.SetEditorDocument(f, doc);
                        f.Show(parent);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.ShowErrorMessage("Error loading editor form\n" + ex.Message);
                    }
                    return true;
                case SummaryInformation.LINK_DELETE_DOC:
                    docID = int.Parse((string)query["docID"]);
                    try
                    {
                        List<AccountDocument> docs = new List<AccountDocument>();
                        docs.Add(AccountingClient.GetAccountDocument(docID, false));
                        DeleteProgressForm dp = new DeleteProgressForm(docs, false);
                        if (dp.ShowDialog(parent) == DialogResult.OK)
                        {
                            reloadMethod();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.ShowErrorMessage("Error deleting document\n" + ex.Message);
                    }
                    return true;

                case SummaryInformation.LINK_VIEW_ENTRIES:
                    docID = int.Parse((string)query["docID"]);
                    EntryViewer ev = new EntryViewer(docID);
                    ev.Show(parent);
                    return true;
            }
            return false;
        }

        public Form form
        {
            get { return this; }
        }


        public string generatledHtml
        {
            get { return _genertedHTML; }
        }
    }
    public interface IHTMLReportHost
    {
        Form form { get; }
        void invalidateReport();
        string generatledHtml { get; }
    }
}