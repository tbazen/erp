﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BIZNET.iERP.Client
{
    public partial class AccountExplorer : DevExpress.XtraEditors.XtraForm
    {
        bool _ignoreEvent = false;
        void setDesigneButtonStatus()
        {
            switch (this.SelectedReportType)
            {
                case ReportType.BlanceSheet:
                case ReportType.IncomeStatement:
                case ReportType.CashFlow:
                    buttonDesign.Enabled = true;
                    break;
                default:
                    buttonDesign.Enabled = false;
                    break;
            }
        }
        private void reportTypeButtonCheckChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent)
                return;
            _ignoreEvent = true;
            try
            {
                
                    
                if (((CheckButton)sender).Checked)
                {
                    uncheckOthers(((CheckButton)sender));
                }
                comboGroup.Enabled = buttonLedger.Checked;
                setDesigneButtonStatus();
                UpdateReport(true);
            }
            finally
            {
                _ignoreEvent = false;
            }
        }
        ReportType SelectedReportType
        {
            get
            {
                if (buttonDetailedCashFlow.Checked)
                {
                    return ReportType.DetailedCashFlowReport;
                }
                if (buttonLedger.Checked)
                {
                    switch (comboGroup.SelectedIndex)
                    {
                        case 0:
                            return ReportType.Ledger;
                        case 1:
                            return ReportType.Ledger_Ungrouped_Day;
                        default:
                            return ReportType.Ledger_Ungrouped_Month;
                    }
                }
                if (buttonDetailedCashFlow.Checked)
                    return ReportType.CostCenterBalances;
                if (buttonTrialBalance.Checked)
                    return ReportType.TrialBalance;
                if (buttonBalanceSheet.Checked)
                    return ReportType.BlanceSheet;
                if (buttonIncomeStatement.Checked)
                    return ReportType.IncomeStatement;
                if (buttonInventory.Checked)
                    return ReportType.Inventory;
                if (buttonBincard.Checked)
                    return ReportType.BinCard;
                if (buttonMaterialFlow.Checked)
                    return ReportType.MaterialFlow;
                if (buttonCenterBalance.Checked)
                    return ReportType.CostCenterBalances;
                if (buttonCostCenterProfile.Checked)
                    return ReportType.CostCenterProfile;
                if (buttonCashFlow.Checked)
                    return ReportType.CashFlow;
                return ReportType.None;
            }
            set
            {
                uncheckOthers(null);
                switch (value)
                {
                    case ReportType.DetailedCashFlowReport:
                        buttonDetailedCashFlow.Checked = true;
                        break;
                    case ReportType.Ledger:
                    case ReportType.Ledger_Ungrouped_Day:
                    case ReportType.Ledger_Ungrouped_Month:
                        buttonLedger.Checked = true;
                        switch(value)
                        {
                            case ReportType.Ledger:
                                comboGroup.SelectedIndex=0;
                                break;
                            case ReportType.Ledger_Ungrouped_Day:
                                comboGroup.SelectedIndex = 1;
                                break;
                            case ReportType.Ledger_Ungrouped_Month:
                                comboGroup.SelectedIndex = 2;
                                break;
                        }
                        break;
                    case ReportType.TrialBalance:
                        buttonTrialBalance.Checked = true;
                        break;
                    case ReportType.BlanceSheet:
                        buttonBalanceSheet.Checked = true;
                        break;
                    case ReportType.IncomeStatement:
                        buttonIncomeStatement.Checked = true;   
                        break;
                    case ReportType.CostCenterBalances:
                        buttonCenterBalance.Checked = true;
                        break;
                    case ReportType.Inventory:
                        buttonInventory.Checked = true;
                        break;
                    case ReportType.BinCard:
                        buttonBincard.Checked = true;
                        break;
                    case ReportType.MaterialFlow:
                        buttonMaterialFlow.Checked = true;
                        break;
                    case ReportType.CostCenterProfile:
                        buttonCostCenterProfile.Checked = true;
                        break;
                    case ReportType.None:
                        break;
                    case ReportType.CashFlow:
                        buttonCashFlow.Checked = true;
                        break;
                    default:
                        break;
                }
                setDesigneButtonStatus();
            }
        }
        
    }
}