﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class AccountExplorer : DevExpress.XtraEditors.XtraForm
    {
        internal enum ReportType
        {
            Ledger,
            Ledger_Ungrouped_Day,
            Ledger_Ungrouped_Month,
            TrialBalance,
            BlanceSheet,
            IncomeStatement,
            CostCenterBalances,
            Inventory,
            BinCard,
            MaterialFlow,
            CostCenterProfile,
            None,
            DetailedCashFlowReport,
            CashFlow,
        }
        Control[] _reportTypeButtons;
        public AccountExplorer()
        {
            InitializeComponent();
            _reportTypeButtons = new Control[] { 
                buttonLedger
                , buttonDetailedCashFlow
                , buttonTrialBalance
                , buttonBalanceSheet
                , buttonIncomeStatement
                ,buttonInventory
                ,buttonBincard
                ,buttonMaterialFlow
                ,buttonCostCenterProfile
                ,buttonDetailedCashFlow
                ,buttonCenterBalance
                ,buttonCashFlow
            };
            loadSettings();
            UpdateReport(true);
            initailizeContextMenu();
            toolExport.SetBrowser(webBrowser);
        }
        public AccountExplorer(int csAccountID)
            :this()
        {
            CostCenterAccount csa = AccountingClient.GetCostCenterAccount(csAccountID);
            if (csa == null)
                return;
            Account account = AccountingClient.GetAccount<Account>(csa.accountID);
            if (account== null)
                return;
            CostCenter costCenter = AccountingClient.GetAccount<CostCenter>(csa.costCenterID);
            if (costCenter == null)
                return;
            _from = new DateTime(1900, 1, 1);
            _to = DateTime.Now;
            _type = ReportType.Ledger   ;
            _selectedAccount = account;
            _selectedCostCenter = costCenter;
            panelControl1.Hide();
            splitContainerControl1.CollapsePanel = SplitCollapsePanel.Panel1;
            splitContainerControl1.Collapsed = true;
            invalidateReport();
            if (_reportThread == null || !_reportThread.IsAlive)
            {
                startThread();
            }
        }
        void uncheckOthers(CheckButton button)
        {
            foreach(CheckButton b in _reportTypeButtons)
                if(b!=button)
                    b.Checked=false;
        }
        

        protected override void OnClosed(EventArgs e)
        {
            saveSettings();
            base.OnClosed(e);
        }

        private void accountBrowser_AccountViewStructureChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            _accountStructure = accountBrowser.getVisisbleAccountStructure();
            UpdateReport(false);
        }

        private void accountBrowser_CostCenterViewStructureChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            _costCenterStructure = accountBrowser.getVisisbleCostCenterStructure();
            UpdateReport(false);
        }

        private void date1_DateTimeChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            _from = date1.DateTime.Date;
            UpdateReport(false);
        }

        private void date2_DateTimeChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            _to = getDate2();
            UpdateReport(false);
        }

        private void buttonReload_Click(object sender, EventArgs e)
        {
            resetThreadParams();
            UpdateReport(true);
            if (_reportThread == null || !_reportThread.IsAlive)
            {
                startThread();
            }
        }

        public void reloadReport(bool transferData)
        {
            resetThreadParams();
            UpdateReport(transferData);
        }
        public void invalidateReport()
        {
            reloadReport(false);
        }
        private void resetThreadParams()
        {
            _thread_selectedAccount = null;
            _thread_selectedCostCenter = null;
            _thread_type = ReportType.None;
            _thread_accountStructure = null;
            _thread_costCenterStructure = null;
        }

        private void accountBrowser_SelectedAccountChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            _selectedAccount = accountBrowser.SelectedAccount;
            UpdateReport(false);
        }

        private void accountBrowser_SelectedCostCenterChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            _selectedCostCenter = accountBrowser.SelectedCostCenter;
            UpdateReport(false);
        }

        private void buttonDesign_Click(object sender, EventArgs e)
        { 
            StatementDesigner designer;
            string rept;
            if (buttonBalanceSheet.Checked)
                rept = "reportBalanceSheet1";
            else if (buttonIncomeStatement.Checked)
                rept = "reportIncomeStatement1";
            else if (buttonCashFlow.Checked)
                rept = "reportCashFlow1";
            else
                return;
            designer = new StatementDesigner(rept);
            designer.DesignUpdated += new EventHandler(designer_DesignUpdated);
            designer.Show(this);
        }

        void designer_DesignUpdated(object sender, EventArgs e)
        {
            invalidateReport();
        }       

        private void checkGroupByPeriod_CheckedChanged(object sender, EventArgs e)
        {
            UpdateReport(true);
        }

        private void period1_SelectedIndexChanged(object sender, EventArgs e)
        {
            date1.DateTime = period1.selectedPeriod.fromDate;
            date1_DateTimeChanged(null, null);
        }

        private void period2_SelectedIndexChanged(object sender, EventArgs e)
        {
            setDate2(period2.selectedPeriod.toDate.Date);
            date2_DateTimeChanged(null, null);
        }

        private void buttonReloadTree_Click(object sender, EventArgs e)
        {
            accountBrowser.reloadTrees();
        }
    }
}