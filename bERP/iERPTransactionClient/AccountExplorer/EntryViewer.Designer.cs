﻿namespace BIZNET.iERP.Client
{
    partial class EntryViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.entriesControl = new INTAPS.UI.HTML.ControlBrowser();
            this.toolBar = new INTAPS.UI.HTML.BowserController();
            this.SuspendLayout();
            // 
            // entriesControl
            // 
            this.entriesControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.entriesControl.Location = new System.Drawing.Point(0, 25);
            this.entriesControl.MinimumSize = new System.Drawing.Size(20, 20);
            this.entriesControl.Name = "entriesControl";
            this.entriesControl.Size = new System.Drawing.Size(829, 423);
            this.entriesControl.StyleSheetFile = "berp.css";
            this.entriesControl.TabIndex = 0;
            // 
            // toolBar
            // 
            this.toolBar.Location = new System.Drawing.Point(0, 0);
            this.toolBar.Name = "toolBar";
            this.toolBar.ShowBackForward = false;
            this.toolBar.ShowRefresh = false;
            this.toolBar.Size = new System.Drawing.Size(829, 25);
            this.toolBar.TabIndex = 1;
            this.toolBar.Text = "bowserController1";
            // 
            // EntryViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 448);
            this.Controls.Add(this.entriesControl);
            this.Controls.Add(this.toolBar);
            this.Name = "EntryViewer";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Entry Viewer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private INTAPS.UI.HTML.ControlBrowser entriesControl;
        private INTAPS.UI.HTML.BowserController toolBar;
    }
}