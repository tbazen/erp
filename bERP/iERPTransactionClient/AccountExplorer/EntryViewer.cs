﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    public partial class EntryViewer : Form
    {
        public EntryViewer()
        {
            InitializeComponent();
            toolBar.SetBrowser(entriesControl);
        }
        public EntryViewer(int docID)
            : this()
        {
            entriesControl.LoadTextPage("Entries",
                iERPTransactionClient.getDocumentEntriesHTML(docID));
        }
        public EntryViewer(DocumentTypedReference tref)
            : this()
        {
            entriesControl.LoadTextPage("Entries",
                iERPTransactionClient.getTypedReferenceEntriesHTML(tref));
        }
    }
}
