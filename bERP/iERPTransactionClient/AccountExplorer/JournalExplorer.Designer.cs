﻿namespace BIZNET.iERP.Client
{
    partial class JournalExplorer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JournalExplorer));
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.webBrowser = new INTAPS.UI.HTML.ControlBrowser();
            this.panelFilter = new DevExpress.XtraEditors.PanelControl();
            this.comboSortBy = new DevExpress.XtraEditors.ComboBoxEdit();
            this.pageNavigator = new INTAPS.UI.PageNavigator();
            this.groupDateFilter = new DevExpress.XtraEditors.GroupControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.date1 = new BIZNET.iERP.Client.BNDualCalendar();
            this.period1 = new BIZNET.iERP.Client.PeriodSelector();
            this.period2 = new BIZNET.iERP.Client.PeriodSelector();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.date2 = new BIZNET.iERP.Client.BNDualCalendar();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.textFilter = new DevExpress.XtraEditors.TextEdit();
            this.toolReport = new INTAPS.UI.HTML.BowserController();
            this.buttonReloadReport = new System.Windows.Forms.ToolStripButton();
            this.listSerialType = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pnlWaitPanel = new System.Windows.Forms.Panel();
            this.lblDesc = new System.Windows.Forms.Label();
            this.pbWaitAnimation = new System.Windows.Forms.PictureBox();
            this.timerCheckThread = new System.Windows.Forms.Timer(this.components);
            this.contextTypes = new System.Windows.Forms.ContextMenuStrip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelFilter)).BeginInit();
            this.panelFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboSortBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupDateFilter)).BeginInit();
            this.groupDateFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.period1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.period2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textFilter.Properties)).BeginInit();
            this.toolReport.SuspendLayout();
            this.pnlWaitPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWaitAnimation)).BeginInit();
            this.SuspendLayout();
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.Text = "Tools";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.webBrowser);
            this.panelControl2.Controls.Add(this.panelFilter);
            this.panelControl2.Controls.Add(this.toolReport);
            this.panelControl2.Controls.Add(this.listSerialType);
            this.panelControl2.Controls.Add(this.pnlWaitPanel);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(951, 477);
            this.panelControl2.TabIndex = 7;
            // 
            // webBrowser
            // 
            this.webBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser.Location = new System.Drawing.Point(201, 202);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(748, 273);
            this.webBrowser.StyleSheetFile = "berp.css";
            this.webBrowser.TabIndex = 7;
            // 
            // panelFilter
            // 
            this.panelFilter.Controls.Add(this.comboSortBy);
            this.panelFilter.Controls.Add(this.pageNavigator);
            this.panelFilter.Controls.Add(this.groupDateFilter);
            this.panelFilter.Controls.Add(this.labelControl4);
            this.panelFilter.Controls.Add(this.labelControl3);
            this.panelFilter.Controls.Add(this.textFilter);
            this.panelFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFilter.Location = new System.Drawing.Point(201, 27);
            this.panelFilter.Name = "panelFilter";
            this.panelFilter.Size = new System.Drawing.Size(748, 175);
            this.panelFilter.TabIndex = 6;
            this.panelFilter.Paint += new System.Windows.Forms.PaintEventHandler(this.panelControl1_Paint);
            // 
            // comboSortBy
            // 
            this.comboSortBy.EditValue = "Date";
            this.comboSortBy.Location = new System.Drawing.Point(93, 33);
            this.comboSortBy.Name = "comboSortBy";
            this.comboSortBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboSortBy.Properties.ImmediatePopup = true;
            this.comboSortBy.Properties.Items.AddRange(new object[] {
            "Date",
            "Reference"});
            this.comboSortBy.Size = new System.Drawing.Size(140, 20);
            this.comboSortBy.TabIndex = 16;
            this.comboSortBy.SelectedIndexChanged += new System.EventHandler(this.comboSortBy_SelectedIndexChanged);
            // 
            // pageNavigator
            // 
            this.pageNavigator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pageNavigator.Location = new System.Drawing.Point(355, 2);
            this.pageNavigator.Name = "pageNavigator";
            this.pageNavigator.NRec = 0;
            this.pageNavigator.PageSize = 0;
            this.pageNavigator.RecordIndex = 0;
            this.pageNavigator.Size = new System.Drawing.Size(383, 46);
            this.pageNavigator.TabIndex = 15;
            this.pageNavigator.PageChanged += new System.EventHandler(this.pageNavigator_PageChanged);
            // 
            // groupDateFilter
            // 
            this.groupDateFilter.Controls.Add(this.labelControl1);
            this.groupDateFilter.Controls.Add(this.date1);
            this.groupDateFilter.Controls.Add(this.period1);
            this.groupDateFilter.Controls.Add(this.period2);
            this.groupDateFilter.Controls.Add(this.labelControl2);
            this.groupDateFilter.Controls.Add(this.date2);
            this.groupDateFilter.Location = new System.Drawing.Point(7, 59);
            this.groupDateFilter.Name = "groupDateFilter";
            this.groupDateFilter.Size = new System.Drawing.Size(702, 100);
            this.groupDateFilter.TabIndex = 14;
            this.groupDateFilter.Text = "Filter By Date";
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl1.Location = new System.Drawing.Point(5, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "From:";
            // 
            // date1
            // 
            this.date1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.date1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.date1.Location = new System.Drawing.Point(5, 44);
            this.date1.Name = "date1";
            this.date1.ShowEthiopian = true;
            this.date1.ShowGregorian = true;
            this.date1.ShowTime = false;
            this.date1.Size = new System.Drawing.Size(324, 42);
            this.date1.TabIndex = 2;
            this.date1.VerticalLayout = true;
            this.date1.DateTimeChanged += new System.EventHandler(this.date1_DateTimeChanged);
            // 
            // period1
            // 
            this.period1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.period1.Location = new System.Drawing.Point(229, 20);
            this.period1.Name = "period1";
            this.period1.PeriodType = "AP_Accounting_Month";
            this.period1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.period1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.period1.Size = new System.Drawing.Size(100, 20);
            this.period1.TabIndex = 11;
            this.period1.SelectedIndexChanged += new System.EventHandler(this.period1_SelectedIndexChanged);
            // 
            // period2
            // 
            this.period2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.period2.Location = new System.Drawing.Point(551, 20);
            this.period2.Name = "period2";
            this.period2.PeriodType = "AP_Accounting_Month";
            this.period2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.period2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.period2.Size = new System.Drawing.Size(121, 20);
            this.period2.TabIndex = 11;
            this.period2.SelectedIndexChanged += new System.EventHandler(this.period2_SelectedIndexChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl2.Location = new System.Drawing.Point(348, 27);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "To:";
            // 
            // date2
            // 
            this.date2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.date2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.date2.Location = new System.Drawing.Point(348, 44);
            this.date2.Name = "date2";
            this.date2.ShowEthiopian = true;
            this.date2.ShowGregorian = true;
            this.date2.ShowTime = false;
            this.date2.Size = new System.Drawing.Size(324, 42);
            this.date2.TabIndex = 2;
            this.date2.VerticalLayout = true;
            this.date2.DateTimeChanged += new System.EventHandler(this.date2_DateTimeChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(7, 36);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(39, 13);
            this.labelControl4.TabIndex = 13;
            this.labelControl4.Text = "Sort By:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(7, 11);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(54, 13);
            this.labelControl3.TabIndex = 13;
            this.labelControl3.Text = "Reference:";
            // 
            // textFilter
            // 
            this.textFilter.Location = new System.Drawing.Point(93, 7);
            this.textFilter.Name = "textFilter";
            this.textFilter.Size = new System.Drawing.Size(140, 20);
            this.textFilter.TabIndex = 12;
            this.textFilter.EditValueChanged += new System.EventHandler(this.textFilter_EditValueChanged);
            this.textFilter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textFilter_KeyDown);
            // 
            // toolReport
            // 
            this.toolReport.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonReloadReport});
            this.toolReport.Location = new System.Drawing.Point(201, 2);
            this.toolReport.Name = "toolReport";
            this.toolReport.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.toolReport.ShowBackForward = false;
            this.toolReport.ShowRefresh = false;
            this.toolReport.Size = new System.Drawing.Size(748, 25);
            this.toolReport.TabIndex = 13;
            this.toolReport.Text = "bowserController1";
            this.toolReport.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.bowserController1_ItemClicked);
            // 
            // buttonReloadReport
            // 
            this.buttonReloadReport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonReloadReport.Image = ((System.Drawing.Image)(resources.GetObject("buttonReloadReport.Image")));
            this.buttonReloadReport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonReloadReport.Name = "buttonReloadReport";
            this.buttonReloadReport.Size = new System.Drawing.Size(47, 22);
            this.buttonReloadReport.Text = "Reload";
            // 
            // listSerialType
            // 
            this.listSerialType.CheckBoxes = true;
            this.listSerialType.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.listSerialType.Dock = System.Windows.Forms.DockStyle.Left;
            this.listSerialType.Location = new System.Drawing.Point(2, 2);
            this.listSerialType.Name = "listSerialType";
            this.listSerialType.Size = new System.Drawing.Size(199, 473);
            this.listSerialType.TabIndex = 8;
            this.listSerialType.UseCompatibleStateImageBehavior = false;
            this.listSerialType.View = System.Windows.Forms.View.Details;
            this.listSerialType.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.listSerialType_ItemCheck_1);
            this.listSerialType.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listSerialType_ItemChecked);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Limit to";
            this.columnHeader1.Width = 171;
            // 
            // pnlWaitPanel
            // 
            this.pnlWaitPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlWaitPanel.BackColor = System.Drawing.Color.White;
            this.pnlWaitPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlWaitPanel.Controls.Add(this.lblDesc);
            this.pnlWaitPanel.Controls.Add(this.pbWaitAnimation);
            this.pnlWaitPanel.Location = new System.Drawing.Point(328, 187);
            this.pnlWaitPanel.Name = "pnlWaitPanel";
            this.pnlWaitPanel.Size = new System.Drawing.Size(310, 45);
            this.pnlWaitPanel.TabIndex = 1;
            this.pnlWaitPanel.Visible = false;
            // 
            // lblDesc
            // 
            this.lblDesc.BackColor = System.Drawing.Color.White;
            this.lblDesc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDesc.Location = new System.Drawing.Point(39, 0);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(269, 43);
            this.lblDesc.TabIndex = 1;
            this.lblDesc.Text = "Generating Report";
            this.lblDesc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pbWaitAnimation
            // 
            this.pbWaitAnimation.Dock = System.Windows.Forms.DockStyle.Left;
            this.pbWaitAnimation.ErrorImage = null;
            this.pbWaitAnimation.Image = global::BIZNET.iERP.Client.Properties.Resources.wait30trans;
            this.pbWaitAnimation.Location = new System.Drawing.Point(0, 0);
            this.pbWaitAnimation.Name = "pbWaitAnimation";
            this.pbWaitAnimation.Size = new System.Drawing.Size(39, 43);
            this.pbWaitAnimation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWaitAnimation.TabIndex = 0;
            this.pbWaitAnimation.TabStop = false;
            // 
            // timerCheckThread
            // 
            this.timerCheckThread.Interval = 500;
            this.timerCheckThread.Tick += new System.EventHandler(this.timerCheckThread_Tick);
            // 
            // contextTypes
            // 
            this.contextTypes.Name = "contextTypes";
            this.contextTypes.Size = new System.Drawing.Size(61, 4);
            // 
            // JournalExplorer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(951, 477);
            this.Controls.Add(this.panelControl2);
            this.Name = "JournalExplorer";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Journal Viewer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.JournalExplorer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelFilter)).EndInit();
            this.panelFilter.ResumeLayout(false);
            this.panelFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboSortBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupDateFilter)).EndInit();
            this.groupDateFilter.ResumeLayout(false);
            this.groupDateFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.period1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.period2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textFilter.Properties)).EndInit();
            this.toolReport.ResumeLayout(false);
            this.toolReport.PerformLayout();
            this.pnlWaitPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbWaitAnimation)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraEditors.PanelControl panelFilter;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private BIZNET.iERP.Client.BNDualCalendar date2;
        private BIZNET.iERP.Client.BNDualCalendar date1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.Panel pnlWaitPanel;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.PictureBox pbWaitAnimation;
        private System.Windows.Forms.Timer timerCheckThread;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private PeriodSelector period2;
        private PeriodSelector period1;
        private INTAPS.UI.HTML.ControlBrowser webBrowser;
        private INTAPS.UI.HTML.BowserController toolReport;
        private System.Windows.Forms.ToolStripButton buttonReloadReport;
        private System.Windows.Forms.ListView listSerialType;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit textFilter;
        private System.Windows.Forms.ContextMenuStrip contextTypes;
        private DevExpress.XtraEditors.GroupControl groupDateFilter;
        private INTAPS.UI.PageNavigator pageNavigator;
        private DevExpress.XtraEditors.ComboBoxEdit comboSortBy;
        private DevExpress.XtraEditors.LabelControl labelControl4;
    }
}