﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Web;

namespace BIZNET.iERP.Client
{
    public partial class JournalExplorer : DevExpress.XtraEditors.XtraForm
    {
        const int PAGE_SIZE = 100;

        List<DocumentTypedReference> expanded = new List<DocumentTypedReference>();
        DocumentSerialType[] _allTypes = null;

        public JournalExplorer()
        {
            InitializeComponent();

            loadSettings();
            pnlWaitPanel.BringToFront();
            toolReport.SetBrowser(webBrowser);
            foreach (DocumentSerialType st in _allTypes=AccountingClient.getAllDocumentSerialTypes())
                listSerialType.Items.Add(st.name).Tag=st;
            pageNavigator.PageSize = PAGE_SIZE;
        }
        public JournalExplorer(DocumentTypedReference[] specfic)
            : this()
        {
            _specific = specfic;
            expanded.AddRange(specfic);
            listSerialType.Visible = false;
            panelFilter.Hide();
            reloadReport(false);
            
        }
        bool _ignoreEvent=false;
        DateTime _from = DateTime.Today, _to = DateTime.Today;
        string _textFilter = null;
        int[] _types = null;
        DocumentTypedReference[] _specific=null;
        int _pageIndex;
        int recordCount=0;
        TransactionJournalField _sortBy;
        DateTime _thread_from = DateTime.Now, _thread_to = DateTime.Now;
        int[] _thread_types = null;
        string _thread_textFilter = null;
        int _thread_pageIndex=-1;
        TransactionJournalField _thread_sortBy=TransactionJournalField.Date;
        System.Threading.Thread _reportThread = null;
        
        
        string _genertedHTML = "";

        private void UpdateReport(bool loadControlData)
        {
            UpdateReport(loadControlData, true);
        }
        private void UpdateReport(bool loadControlData,bool resetPageIndex)
        {
            if (resetPageIndex)
            {
                pageNavigator.RecordIndex = 0;
                _pageIndex = 0;
            }
            if (loadControlData)
            {
                getDataFromControls();
            }
            if (_reportThread == null || !_reportThread.IsAlive)
            {
                startThread();
            }
        }

        private void getDataFromControls()
        {
            _from = date1.DateTime.Date;
            _to = date2.DateTime;
            _textFilter = textFilter.Text;
            _pageIndex = pageNavigator.RecordIndex;
            _sortBy = comboSortBy.SelectedIndex == 0 ? TransactionJournalField.Date : TransactionJournalField.Reference;
            updateTypeFromControl();
        }

        private void updateTypeFromControl()
        {
            try
            {
                int[] type = new int[listSerialType.CheckedItems.Count];
                int index = 0;
                ListViewItem[] items = new ListViewItem[listSerialType.CheckedItems.Count];
                listSerialType.CheckedItems.CopyTo(items, 0);
                foreach (ListViewItem tt in items)
                {
                    int i = tt.Index;
                    i++;
                    i--;
                    type[index++] = _allTypes[i].id;
                }
                _types = type;
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void startThread()
        {
            bool typeEqual;
            if (_types != _thread_types)
            {
                if (_types == null || _thread_types == null)
                    typeEqual = false;
                else
                    if (_types.Length == _thread_types.Length)
                {
                    typeEqual = true;
                    for (int i = 0; i < _types.Length; i++)
                    {
                        if (_types[i] != _thread_types[i])
                        {
                            typeEqual = false;
                            break;
                        }
                    }

                }
                else
                    typeEqual = false;
            }
            else
                typeEqual = true;
            if(!typeEqual || !_from.Equals(_thread_from)||!_to.Equals(_thread_to) || _pageIndex!=_thread_pageIndex)
                setThreadParsAndStart();
        }

        void setThreadParsAndStart()
        {
            _thread_from = _from;
            _thread_to = _to;
            _thread_types = _types;
            _thread_textFilter = _textFilter;
            _thread_pageIndex = _pageIndex;
            _thread_sortBy = _sortBy;
            _reportThread = new System.Threading.Thread(new System.Threading.ThreadStart(generateThread));
            _reportThread.Start();
            
            pnlWaitPanel.Show();
            timerCheckThread.Enabled = true;
        }

        void generateThread()
        {
            try
            {
                buildJournal(_thread_from, _thread_to,_thread_types,_specific,_thread_textFilter,_thread_sortBy, _thread_pageIndex);
            }
            catch (Exception ex)
            {
                _genertedHTML = "<h1>" + System.Web.HttpUtility.HtmlEncode("Error trying to generate report:" + ex.Message) + "</h1>";
            }
        }
        private void generateTitleForJournal(StringBuilder builder, DateTime date1, DateTime date2)
        {
            TSConstants.addCompanyNameLine(iERPTransactionClient.GetCompanyProfile(), builder);
            builder.Append("<h2>Transaction Journal</h2>");
            builder.Append(string.Format("<span class='SubTitle'><b>From</b> <u>{0}</u> <b>to</b> <u>{1}</u></span>", HttpUtility.HtmlEncode(AccountBase.FormatDate(date1)),
                HttpUtility.HtmlEncode(AccountBase.FormatDate(date2))));

        }
        public void buildJournal(DateTime date1, DateTime date2,int[] type,DocumentTypedReference [] specific,string textFilter,TransactionJournalField sortBy, int pageIndex)
        {
            StringBuilder builder = new StringBuilder();
            generateTitleForJournal(builder, date1, date2);
            builder.Append("<br/><br/>");
            int N;
            if(specific==null)
                builder.Append(iERPTransactionClient.getPrimaryJournal(new JournalFilter(date1, date2.AddDays(1),expanded.ToArray(), type, null,textFilter,sortBy),pageIndex,PAGE_SIZE,out N));
            else
                builder.Append(iERPTransactionClient.getPrimaryJournal(new JournalFilter(specific, expanded.ToArray()), pageIndex, PAGE_SIZE, out N));
            _genertedHTML = builder.ToString();
            this.recordCount= N;
        }

        private void timerCheckThread_Tick(object sender, EventArgs e)
        {
            if (!_reportThread.IsAlive)
            {
                webBrowser.LoadControl(new JournalBuilder(this,_genertedHTML));
                pageNavigator.NRec = recordCount;
                pnlWaitPanel.Hide();
                timerCheckThread.Stop();
                startThread();
            }
        }

        private void loadSettings()
        {
            _ignoreEvent = true;
            try
            {
                date1.DateTime = Properties.Settings.Default.AccountExplorer_DateFrom;
            }
            finally
            {
                _ignoreEvent = false;
            }
        }
        void saveSettings()
        {
            Properties.Settings.Default.AccountExplorer_DateFrom = date1.DateTime;
            Properties.Settings.Default.Save();
        }

        protected override void OnClosed(EventArgs e)
        {
            saveSettings();
            base.OnClosed(e);
        }

        

        private void date1_DateTimeChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            _from = date1.DateTime.Date;
            //UpdateReport(false);
        }

        private void date2_DateTimeChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            _to = date2.DateTime;
            //UpdateReport(false);
        }

        private void buttonReload_Click(object sender, EventArgs e)
        {
            reloadReport(true);
        }

        private void reloadReport(bool transferData)
        {
            resetThreadParams();
            UpdateReport(transferData);
        }

        private void resetThreadParams()
        {
            _thread_types = null;
        }

        

          

        private void checkGroupByPeriod_CheckedChanged(object sender, EventArgs e)
        {
            //UpdateReport(true);
        }

        private void period1_SelectedIndexChanged(object sender, EventArgs e)
        {
            date1.DateTime = period1.selectedPeriod.fromDate;
            date1_DateTimeChanged(null, null);
        }

        private void period2_SelectedIndexChanged(object sender, EventArgs e)
        {
            date2.DateTime = period2.selectedPeriod.toDate.AddDays(-1);
            date2_DateTimeChanged(null, null);
        }

        private void groupControl1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void toolExport_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void bowserController1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem==buttonReloadReport)
            {
                buttonReload_Click(null, null);
            }
        }
        class ReportAccountClientHook : IAccountingClient
        {
            JournalExplorer _parent;
            public ReportAccountClientHook(JournalExplorer parent)
            {
                _parent = parent;
            }
            public int PostGenericDocument(AccountDocument doc)
            {
                int ret = INTAPS.Accounting.Client.AccountingClient.PostGenericDocument(doc);
                try
                {
                    _parent.reloadReport(true);

                }
                catch
                {
                }
                return ret;
            }
        }
        public class JournalBuilder : INTAPS.UI.HTML.IHTMLBuilder
        {
            string _html;
            JournalExplorer _parent;
            public JournalBuilder(JournalExplorer parent, string html)
            {
                _html = html;
                _parent = parent;
            }
            public void Build()
            {
            }

            public string GetOuterHtml()
            {
                return "<span id='report'>" + _html+ "</span>";
            }
            DocumentTypedReference tref;
            int docID;
            EntryViewer ev;
            public bool ProcessUrl(string path, System.Collections.Hashtable query)
            {
                switch (path)
                {
                    case "expand":
                        tref = new DocumentTypedReference(int.Parse((string)query["typeID"]), HttpUtility.UrlDecode((string)query["ref"]), false);
                        string htmlPath = INTAPS.UI.HTML.HTMLBuilder.GetAppHTMLRoot();
                        if (_parent.expanded.Contains(tref))
                        {
                            _parent.webBrowser.UpdateElementInnerHtml("detail_" + tref.typeID+"_"+tref.reference, "");
                            _parent.expanded.Remove(tref);
                            string html = "<a href='expand?typeID=" + tref.typeID + "&ref=" + System.Web.HttpUtility.UrlEncode(tref.reference) + "'><img name='type_" + tref.typeID + "' src='" + htmlPath + "images\\itemwithchild.png'></a>";
                            _parent.webBrowser.UpdateElementInnerHtml(tref.typeID + "_" + tref.reference, html);
                        }
                        else
                        {
                            _parent.webBrowser.UpdateElementInnerHtml("detail_" + tref.typeID + "_" + tref.reference,
                                iERPTransactionClient.getDocumentListHtmlByTypedReference(tref.typeID, tref.reference));
                            _parent.expanded.Add(tref);
                            string html = "<a href='expand?typeID=" + tref.typeID + "&ref=" + System.Web.HttpUtility.UrlEncode(tref.reference) + "'><img name='type_" + tref.typeID + "' src='" + htmlPath + "images\\itemwithchild_expanded.png'></a>";
                            _parent.webBrowser.UpdateElementInnerHtml(tref.typeID + "_" + tref.reference, html);
                        }
                        return true;
                    case "refentry":
                        tref = new DocumentTypedReference(int.Parse((string)query["typeID"]), HttpUtility.UrlDecode((string)query["ref"]), false);
                        ev = new EntryViewer(tref);
                        ev.Show(_parent);
                        return true;
                    case "docentry":
                        docID = int.Parse((string)query["docID"]);
                        ev = new EntryViewer(docID);
                        ev.Show(_parent);
                        return true;
                    case "docedit":
                        docID = int.Parse((string)query["docID"]);
                        int docTypeID = int.Parse((string)query["docTypeID"]);
                        try
                        {
                            IGenericDocumentClientHandler client = INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(docTypeID);
                            if (client == null || !client.CanEdit)
                            {

                                MessageBox.ShowNormalWarningMessage(string.Format("Sorry, {0} transactions can't be edited", INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByID(docTypeID).name));
                                return true;
                            }
                            client.setAccountingClient(new ReportAccountClientHook(_parent));
                            Form f = client.CreateEditor(false) as Form;
                            AccountDocument doc = INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(docID, true);
                            client.SetEditorDocument(f, doc);
                            f.Show(_parent);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.ShowErrorMessage("Error loading editor form\n" + ex.Message);
                        }
                        return true;
                    case "docdelete":
                        docID = int.Parse((string)query["docID"]);
                        try
                        {
                            List<AccountDocument> docs=new List<AccountDocument>();
                            docs.Add(AccountingClient.GetAccountDocument(docID,false));
                            DeleteProgressForm dp = new DeleteProgressForm(docs, false);
                            if (dp.ShowDialog(_parent) == DialogResult.OK)
                            {
                                _parent.reloadReport(true);
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.ShowErrorMessage("Error loading editor form\n" + ex.Message);
                        }
                        return true;

                }
                return false;
            }

            public void SetHost(INTAPS.UI.HTML.IHTMLDocumentHost host)
            {
                
            }

            public string WindowCaption
            {
                get { return "Transaction Journal"; }
            }
        }

        private void JournalExplorer_Load(object sender, EventArgs e)
        {

        }

        private void listSerialType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void panelControl1_Paint(object sender, PaintEventArgs e)
        {

        }


        private void listSerialType_ItemCheck(object sender, ItemCheckEventArgs e)
        {

        }

        private void listSerialType_ItemCheck_1(object sender, ItemCheckEventArgs e)
        {

        }

        private void listSerialType_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (_ignoreEvent) return;
            updateTypeFromControl();
            //UpdateReport(false);

        }

        private void textFilter_EditValueChanged(object sender, EventArgs e)
        {
            bool dateEnabled = textFilter.Text.Trim().Equals("");
            if (dateEnabled != groupDateFilter.Enabled)
                groupDateFilter.Enabled = dateEnabled;
        }

        private void textFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                reloadReport(true);
        }

        private void pageNavigator_PageChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            _pageIndex= pageNavigator.RecordIndex;
            UpdateReport(false,false);
        }

        private void comboSortBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            UpdateReport(true, true);
        }
    }
    
}