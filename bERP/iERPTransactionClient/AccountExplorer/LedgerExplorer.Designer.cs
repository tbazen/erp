﻿namespace BIZNET.iERP.Client
{
    partial class LedgerExplorer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.accountBrowser = new INTAPS.Accounting.Client.AccountStructureBrowserControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.pnlWaitPanel = new System.Windows.Forms.Panel();
            this.lblDesc = new System.Windows.Forms.Label();
            this.pbWaitAnimation = new System.Windows.Forms.PictureBox();
            this.webBrowser = new INTAPS.UI.HTML.ControlBrowser();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.checkExcludeWithNoLedger = new DevExpress.XtraEditors.CheckEdit();
            this.checkExcludeNoBalance = new DevExpress.XtraEditors.CheckEdit();
            this.checkMerge = new DevExpress.XtraEditors.CheckEdit();
            this.textExcludeTo = new DevExpress.XtraEditors.TextEdit();
            this.textExcludeFrom = new DevExpress.XtraEditors.TextEdit();
            this.textIncludeTo = new DevExpress.XtraEditors.TextEdit();
            this.textIncludeFrom = new DevExpress.XtraEditors.TextEdit();
            this.buttonReloadTree = new DevExpress.XtraEditors.SimpleButton();
            this.htmlTool = new INTAPS.UI.HTML.HTMLExportTool();
            this.buttonReload = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.comboGroup = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.date2 = new BIZNET.iERP.Client.BNDualCalendar();
            this.date1 = new BIZNET.iERP.Client.BNDualCalendar();
            this.timerCheckThread = new System.Windows.Forms.Timer(this.components);
            this.checkConcise = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.pnlWaitPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWaitAnimation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkExcludeWithNoLedger.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkExcludeNoBalance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkMerge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textExcludeTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textExcludeFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textIncludeTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textIncludeFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkConcise.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.Text = "Tools";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.accountBrowser);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.panelControl2);
            this.splitContainerControl1.Panel2.Controls.Add(this.panelControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1052, 477);
            this.splitContainerControl1.SplitterPosition = 233;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // accountBrowser
            // 
            this.accountBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.accountBrowser.Location = new System.Drawing.Point(0, 0);
            this.accountBrowser.Name = "accountBrowser";
            this.accountBrowser.PickedAccount = null;
            this.accountBrowser.Size = new System.Drawing.Size(233, 477);
            this.accountBrowser.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.pnlWaitPanel);
            this.panelControl2.Controls.Add(this.webBrowser);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 173);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(814, 304);
            this.panelControl2.TabIndex = 7;
            // 
            // pnlWaitPanel
            // 
            this.pnlWaitPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlWaitPanel.BackColor = System.Drawing.Color.White;
            this.pnlWaitPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlWaitPanel.Controls.Add(this.lblDesc);
            this.pnlWaitPanel.Controls.Add(this.pbWaitAnimation);
            this.pnlWaitPanel.Location = new System.Drawing.Point(260, 101);
            this.pnlWaitPanel.Name = "pnlWaitPanel";
            this.pnlWaitPanel.Size = new System.Drawing.Size(310, 45);
            this.pnlWaitPanel.TabIndex = 1;
            this.pnlWaitPanel.Visible = false;
            // 
            // lblDesc
            // 
            this.lblDesc.BackColor = System.Drawing.Color.White;
            this.lblDesc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDesc.Location = new System.Drawing.Point(39, 0);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(269, 43);
            this.lblDesc.TabIndex = 1;
            this.lblDesc.Text = "Generating Report";
            this.lblDesc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pbWaitAnimation
            // 
            this.pbWaitAnimation.Dock = System.Windows.Forms.DockStyle.Left;
            this.pbWaitAnimation.ErrorImage = null;
            this.pbWaitAnimation.Image = global::BIZNET.iERP.Client.Properties.Resources.wait30trans;
            this.pbWaitAnimation.Location = new System.Drawing.Point(0, 0);
            this.pbWaitAnimation.Name = "pbWaitAnimation";
            this.pbWaitAnimation.Size = new System.Drawing.Size(39, 43);
            this.pbWaitAnimation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWaitAnimation.TabIndex = 0;
            this.pbWaitAnimation.TabStop = false;
            // 
            // webBrowser
            // 
            this.webBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser.Location = new System.Drawing.Point(2, 2);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(810, 300);
            this.webBrowser.StyleSheetFile = "berp.css";
            this.webBrowser.TabIndex = 5;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.checkExcludeWithNoLedger);
            this.panelControl1.Controls.Add(this.checkExcludeNoBalance);
            this.panelControl1.Controls.Add(this.checkConcise);
            this.panelControl1.Controls.Add(this.checkMerge);
            this.panelControl1.Controls.Add(this.textExcludeTo);
            this.panelControl1.Controls.Add(this.textExcludeFrom);
            this.panelControl1.Controls.Add(this.textIncludeTo);
            this.panelControl1.Controls.Add(this.textIncludeFrom);
            this.panelControl1.Controls.Add(this.buttonReloadTree);
            this.panelControl1.Controls.Add(this.htmlTool);
            this.panelControl1.Controls.Add(this.buttonReload);
            this.panelControl1.Controls.Add(this.labelControl10);
            this.panelControl1.Controls.Add(this.comboGroup);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.date2);
            this.panelControl1.Controls.Add(this.date1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(814, 173);
            this.panelControl1.TabIndex = 6;
            // 
            // checkExcludeWithNoLedger
            // 
            this.checkExcludeWithNoLedger.Location = new System.Drawing.Point(541, 99);
            this.checkExcludeWithNoLedger.Name = "checkExcludeWithNoLedger";
            this.checkExcludeWithNoLedger.Properties.Caption = "Exclude Accounts With no Transaction In The Period";
            this.checkExcludeWithNoLedger.Size = new System.Drawing.Size(244, 19);
            this.checkExcludeWithNoLedger.TabIndex = 23;
            // 
            // checkExcludeNoBalance
            // 
            this.checkExcludeNoBalance.Location = new System.Drawing.Point(541, 74);
            this.checkExcludeNoBalance.Name = "checkExcludeNoBalance";
            this.checkExcludeNoBalance.Properties.Caption = "Exclude Accounts that are Never Used";
            this.checkExcludeNoBalance.Size = new System.Drawing.Size(208, 19);
            this.checkExcludeNoBalance.TabIndex = 23;
            // 
            // checkMerge
            // 
            this.checkMerge.Location = new System.Drawing.Point(541, 54);
            this.checkMerge.Name = "checkMerge";
            this.checkMerge.Properties.Caption = "Merge Accounts";
            this.checkMerge.Size = new System.Drawing.Size(192, 19);
            this.checkMerge.TabIndex = 23;
            // 
            // textExcludeTo
            // 
            this.textExcludeTo.Location = new System.Drawing.Point(321, 149);
            this.textExcludeTo.Name = "textExcludeTo";
            this.textExcludeTo.Size = new System.Drawing.Size(151, 20);
            this.textExcludeTo.TabIndex = 22;
            // 
            // textExcludeFrom
            // 
            this.textExcludeFrom.Location = new System.Drawing.Point(87, 149);
            this.textExcludeFrom.Name = "textExcludeFrom";
            this.textExcludeFrom.Size = new System.Drawing.Size(151, 20);
            this.textExcludeFrom.TabIndex = 22;
            // 
            // textIncludeTo
            // 
            this.textIncludeTo.Location = new System.Drawing.Point(321, 110);
            this.textIncludeTo.Name = "textIncludeTo";
            this.textIncludeTo.Size = new System.Drawing.Size(151, 20);
            this.textIncludeTo.TabIndex = 22;
            // 
            // textIncludeFrom
            // 
            this.textIncludeFrom.Location = new System.Drawing.Point(87, 110);
            this.textIncludeFrom.Name = "textIncludeFrom";
            this.textIncludeFrom.Size = new System.Drawing.Size(151, 20);
            this.textIncludeFrom.TabIndex = 22;
            // 
            // buttonReloadTree
            // 
            this.buttonReloadTree.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReloadTree.Location = new System.Drawing.Point(663, 142);
            this.buttonReloadTree.Name = "buttonReloadTree";
            this.buttonReloadTree.Size = new System.Drawing.Size(70, 26);
            this.buttonReloadTree.TabIndex = 4;
            this.buttonReloadTree.Text = "Reload Tree";
            // 
            // htmlTool
            // 
            this.htmlTool.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.htmlTool.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.htmlTool.Location = new System.Drawing.Point(573, 142);
            this.htmlTool.Name = "htmlTool";
            this.htmlTool.Size = new System.Drawing.Size(84, 27);
            this.htmlTool.TabIndex = 21;
            // 
            // buttonReload
            // 
            this.buttonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReload.Location = new System.Drawing.Point(739, 142);
            this.buttonReload.Name = "buttonReload";
            this.buttonReload.Size = new System.Drawing.Size(70, 27);
            this.buttonReload.TabIndex = 4;
            this.buttonReload.Text = "Generate";
            this.buttonReload.Click += new System.EventHandler(this.buttonReload_Click_1);
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(284, 152);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(16, 13);
            this.labelControl10.TabIndex = 3;
            this.labelControl10.Text = "To:";
            // 
            // comboGroup
            // 
            this.comboGroup.Location = new System.Drawing.Point(606, 2);
            this.comboGroup.Name = "comboGroup";
            this.comboGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboGroup.Properties.Items.AddRange(new object[] {
            "Detailed",
            "Summerize by Day",
            "Summerize by Month"});
            this.comboGroup.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboGroup.Size = new System.Drawing.Size(143, 20);
            this.comboGroup.TabIndex = 2;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(284, 113);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(16, 13);
            this.labelControl7.TabIndex = 3;
            this.labelControl7.Text = "To:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(24, 56);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "To:";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(17, 130);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(117, 13);
            this.labelControl9.TabIndex = 3;
            this.labelControl9.Text = "Exclude Account Range:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(543, 5);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(47, 13);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "Grouping:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(17, 91);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(115, 13);
            this.labelControl5.TabIndex = 3;
            this.labelControl5.Text = "Include Account Range:";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(24, 152);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(28, 13);
            this.labelControl8.TabIndex = 3;
            this.labelControl8.Text = "From:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(17, 1);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(32, 13);
            this.labelControl3.TabIndex = 3;
            this.labelControl3.Text = "Dates:";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(24, 113);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(28, 13);
            this.labelControl6.TabIndex = 3;
            this.labelControl6.Text = "From:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(24, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "From:";
            // 
            // date2
            // 
            this.date2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.date2.Location = new System.Drawing.Point(61, 52);
            this.date2.Name = "date2";
            this.date2.ShowEthiopian = true;
            this.date2.ShowGregorian = true;
            this.date2.ShowTime = false;
            this.date2.Size = new System.Drawing.Size(411, 21);
            this.date2.TabIndex = 2;
            this.date2.VerticalLayout = false;
            // 
            // date1
            // 
            this.date1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.date1.Location = new System.Drawing.Point(61, 25);
            this.date1.Name = "date1";
            this.date1.ShowEthiopian = true;
            this.date1.ShowGregorian = true;
            this.date1.ShowTime = false;
            this.date1.Size = new System.Drawing.Size(411, 21);
            this.date1.TabIndex = 2;
            this.date1.VerticalLayout = false;
            // 
            // timerCheckThread
            // 
            this.timerCheckThread.Interval = 500;
            // 
            // checkConcise
            // 
            this.checkConcise.Location = new System.Drawing.Point(541, 29);
            this.checkConcise.Name = "checkConcise";
            this.checkConcise.Properties.Caption = "Concise";
            this.checkConcise.Size = new System.Drawing.Size(192, 19);
            this.checkConcise.TabIndex = 23;
            // 
            // LedgerExplorer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1052, 477);
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "LedgerExplorer";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ledger Explorer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.pnlWaitPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbWaitAnimation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkExcludeWithNoLedger.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkExcludeNoBalance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkMerge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textExcludeTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textExcludeFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textIncludeTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textIncludeFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkConcise.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private INTAPS.Accounting.Client.AccountStructureBrowserControl accountBrowser;
        private INTAPS.UI.HTML.ControlBrowser webBrowser;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private BIZNET.iERP.Client.BNDualCalendar date2;
        private BIZNET.iERP.Client.BNDualCalendar date1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.Panel pnlWaitPanel;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.PictureBox pbWaitAnimation;
        private System.Windows.Forms.Timer timerCheckThread;
        private DevExpress.XtraEditors.SimpleButton buttonReload;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton buttonReloadTree;
        private DevExpress.XtraEditors.ComboBoxEdit comboGroup;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit textIncludeTo;
        private DevExpress.XtraEditors.TextEdit textIncludeFrom;
        private INTAPS.UI.HTML.HTMLExportTool htmlTool;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit textExcludeTo;
        private DevExpress.XtraEditors.TextEdit textExcludeFrom;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.CheckEdit checkMerge;
        private DevExpress.XtraEditors.CheckEdit checkExcludeWithNoLedger;
        private DevExpress.XtraEditors.CheckEdit checkExcludeNoBalance;
        private DevExpress.XtraEditors.CheckEdit checkConcise;
    }
}