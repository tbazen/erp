﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Web;

namespace BIZNET.iERP.Client
{
    public partial class LedgerExplorer : DevExpress.XtraEditors.XtraForm,IHTMLReportHost
    {
        bool paramChanged = false;
        bool _ignoreEvent = false;
        System.Threading.Thread generateThread = null;
        AccountExplorer.HTMLLinkHandler _handler = null;
        string _generatedHMTL="";
        int _itemID;
        public LedgerExplorer():this(true,TransactionItem.DEFAULT_CURRENCY)
        {

        }
        public LedgerExplorer(bool setItemID,int itemID)
        {
            InitializeComponent();
            if(setItemID)
                this._itemID = itemID;
            loadSettings();
            htmlTool.setBrowser(webBrowser);
        }
        public LedgerExplorer(int csAccountID)
            : this()
        {
            CostCenterAccount csa = AccountingClient.GetCostCenterAccount(csAccountID);
            if (csa == null)
                return;
            Account account = AccountingClient.GetAccount<Account>(csa.accountID);
            if (account == null)
                return;
            CostCenter costCenter = AccountingClient.GetAccount<CostCenter>(csa.costCenterID);
            if (costCenter == null)
                return;
            date1.DateTime = new DateTime(1900, 1, 1);
            date2.DateTime = DateTime.Now;
            accountBrowser.SelectedAccount = account;
            accountBrowser.SelectedCostCenter = costCenter;
            panelControl1.Hide();
            splitContainerControl1.CollapsePanel = SplitCollapsePanel.Panel1;
            splitContainerControl1.Collapsed = true;
        }
        
        void showMessage(string msg)
        {
            webBrowser.DocumentText=string.Format("<h2>{0}</h2>",System.Web.HttpUtility.HtmlEncode(msg));
        }
        private void reloadReport()
        {
            if (generateThread == null)
            {
                if (accountBrowser.SelectedCostCenter == null)
                {
                    showMessage("Select accounting center");
                    return;
                }
                
                LedgerParameters pars=new LedgerParameters();
                pars.costCenterID = accountBrowser.SelectedCostCenter.id;
                pars.merged = checkMerge.Checked;
                if (checkConcise.Checked)
                {
                    pars.hideAccountColumnIfUniformAccount = true;
                    pars.remarkDetail = LedgerParameters.LedgerRemarkDetail.Concise;
                    pars.printMode = true;
                }
                else
                    pars.remarkDetail = LedgerParameters.LedgerRemarkDetail.Detail;
                if (textIncludeFrom.Text.Trim() == "")
                {
                    if (accountBrowser.SelectedAccount == null)
                    {
                        showMessage("Select account ");
                        return;
                    }
                    int selectedAccountID = accountBrowser.SelectedAccount.id;
                    if (pars.merged)
                    {
                        pars.accountIDs = new int[] { selectedAccountID };
                    }
                    else
                    {

                        List<int> expanded = new List<int>();
                        AccountExplorer.collectExpanded(accountBrowser.getVisisbleAccountStructure(), expanded);

                        for (int i = expanded.Count - 1; i >= 0; i--)
                        {
                            if (selectedAccountID != expanded[i] && !AccountingClient.IsControlOf<Account>(selectedAccountID, expanded[i]))
                                expanded.RemoveAt(i);
                        }
                        if (expanded.Count == 0)
                            pars.accountIDs = new int[] { selectedAccountID };
                        else
                            pars.expandAccounts = expanded.ToArray();
                    }
                }
                else
                {
                    pars.includeAccountFrom = textIncludeFrom.Text.Trim();
                    pars.includAccountTo = textIncludeTo.Text.Trim();
                    pars.excludeAccountFrom = textExcludeFrom.Text.Trim();
                    pars.excludeAccountTo= textExcludeTo.Text.Trim();
                }

                pars.itemID = _itemID;
                pars.time1 = date1.DateTime;
                pars.time2 = date2.DateTime.AddDays(1);
                
                pars.excludeEmptyLedger = checkExcludeWithNoLedger.Checked;
                pars.excludeEmptyBalance = checkExcludeNoBalance.Checked;
                switch (comboGroup.SelectedIndex)
                {
                    case 1: pars.groupByPeriod = InternationalDayPeriod.NAME;
                        break;
                    case 2: pars.groupByPeriod = "AP_Accounting_Month"; break;
                    default:
                        pars.groupByPeriod = null; break;
                }
                pnlWaitPanel.Show();
                (generateThread = new System.Threading.Thread(delegate()
                    {
                        try
                        {
                            StringBuilder sb = new StringBuilder();
                            generateTitleForLedger(sb, pars.time1, pars.time2, AccountingClient.GetAccount<CostCenter>(pars.costCenterID).NameCode);
                            sb.Append(iERPTransactionClient.renderLedgerTable(pars));
                            _generatedHMTL = sb.ToString();
                        }
                        catch (Exception ex)
                        {
                            _generatedHMTL = string.Format("<h2>System error generating ledger</h2><h3>{0}</h3", ex.Message);
                        }
                        this.Invoke(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                        {
                            if (_handler == null)
                            {
                                _handler = new AccountExplorer.HTMLLinkHandler(this);
                                webBrowser.LoadControl(_handler);
                            }
                            else
                                webBrowser.UpdateElementInnerHtml("report", _generatedHMTL);
                            pnlWaitPanel.Hide();
                            generateThread = null;
                        }
                        ));
                    }
                )).Start();
            }
        }
        
        void updateReport()
        {
            if (paramChanged)
                return;
            paramChanged = true;
        }
        protected override void OnClosed(EventArgs e)
        {
            saveSettings();
            base.OnClosed(e);
        }
        
        private void accountBrowser_AccountViewStructureChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            updateReport();
        }

        private void accountBrowser_CostCenterViewStructureChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            updateReport();
        }

        private void date1_DateTimeChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            updateReport();
        }

        private void date2_DateTimeChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            updateReport();
        }

        private void buttonReload_Click(object sender, EventArgs e)
        {
            reloadReport();
        }



        private void accountBrowser_SelectedAccountChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            updateReport();
        }

        private void accountBrowser_SelectedCostCenterChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            updateReport();
        }


        private void checkGroupByPeriod_CheckedChanged(object sender, EventArgs e)
        {
            updateReport();
        }

        private void buttonReloadTree_Click(object sender, EventArgs e)
        {
            accountBrowser.reloadTrees();

        }

        private void loadSettings()
        {

            //restore tree state
            try
            {
                _ignoreEvent = true;
                Dictionary<int, object> list = INTAPS.SerializationExtensions.loadFromBytes<Dictionary<int, object>>(INTAPS.SerializationExtensions.loadFromXml<byte[]>(Properties.Settings.Default.AccountExplorer_CostCenterExpanded));
                accountBrowser.setVisisbleCostCenterStructure(list);
                list = INTAPS.SerializationExtensions.loadFromBytes<Dictionary<int, object>>(INTAPS.SerializationExtensions.loadFromXml<byte[]>(Properties.Settings.Default.AccountExplorer_AccountExpanded));
                accountBrowser.setVisisbleAccountStructure(list);
                accountBrowser.SetCostCenterByID(Properties.Settings.Default.AccountExplorer_SelectedCostCenterID);
                accountBrowser.SetAccountByID(Properties.Settings.Default.AccountExplorer_SelectedAccountID);
                comboGroup.SelectedIndex = 0;
            }
            catch
            {
                MessageBox.ShowErrorMessage("Error trying to load UI configuration.\nYou can safely ignore this error");
            }
            date1.DateTime = Properties.Settings.Default.AccountExplorer_DateFrom;

        }
        void saveSettings()
        {

            Dictionary<int, object> list = accountBrowser.getVisisbleCostCenterStructure();
            Properties.Settings.Default.AccountExplorer_CostCenterExpanded = INTAPS.SerializationExtensions.getXml(INTAPS.SerializationExtensions.getBytes(list));
            list = accountBrowser.getVisisbleAccountStructure();
            Properties.Settings.Default.AccountExplorer_AccountExpanded = INTAPS.SerializationExtensions.getXml(INTAPS.SerializationExtensions.getBytes(list));
            Properties.Settings.Default.AccountExplorer_DateFrom=date1.DateTime;
            Properties.Settings.Default.AccountExplorer_DateTo = date2.DateTime;
            Properties.Settings.Default.AccountExplorer_SelectedCostCenterID = accountBrowser.SelectedCostCenter == null ? -1 : accountBrowser.SelectedCostCenter.id;
            Properties.Settings.Default.AccountExplorer_SelectedAccountID=accountBrowser.SelectedAccount==null?-1:accountBrowser.SelectedAccount.id;
            
            Properties.Settings.Default.Save();
        }



        public Form form
        {
            get { return this; }
        }

        public void invalidateReport()
        {
            this.updateReport();
        }

        public string generatledHtml
        {
            get 
            {
                return _generatedHMTL;
            }
        }

        private void buttonReload_Click_1(object sender, EventArgs e)
        {
            reloadReport();
        }
        static internal void generateTitleForLedger(StringBuilder builder, DateTime date1, DateTime date2, string costCenterTitle)
        {
            TSConstants.addCompanyNameLine(iERPTransactionClient.GetCompanyProfile(), builder);
            builder.Append("<h2>Ledger</h2>");
            builder.Append(string.Format("<span class='SubTitle'><b>{0}</b><u>{1}</u></span><br/>", HttpUtility.HtmlEncode("Accounting Center: "), HttpUtility.HtmlEncode(costCenterTitle)));
            builder.Append(string.Format("<span class='SubTitle'><b>From</b> <u>{0}</u> <b>to</b> <u>{1}</u></span>", HttpUtility.HtmlEncode(AccountBase.FormatDate(date1)),
                HttpUtility.HtmlEncode(AccountBase.FormatDate(date2))));
        }
    }
}