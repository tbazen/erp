﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    public partial class LedgerViewer : Form, INTAPS.UI.HTML.IHTMLBuilder
    {
        int _costCenterID;
        int[] accounts;
        string _ledgerHTML = "";
        string _accountTitle;
        public LedgerViewer(int costCenterID, int[] csAccounts, string accountTitle)
        {
            AccountingPeriod p = iERPTransactionClient.GetAccountingPeriod("AP_Accounting_Year", DateTime.Now);
            if (p == null)
                init(costCenterID, csAccounts, accountTitle, DateTime.Parse("1/1/1900").Ticks, DateTime.Now.Ticks);
            else
                init(costCenterID, csAccounts, accountTitle, p.fromDate.Ticks, p.toDate.Ticks);
        }
        public LedgerViewer(int costCenterID,int[] csAccounts,string accountTitle,long ticksFrom,long ticksTo)
        {
            init(costCenterID, csAccounts, accountTitle, ticksFrom, ticksTo);
        }

        private void init(int costCenterID, int[] csAccounts, string accountTitle, long ticksFrom, long ticksTo)
        {
            InitializeComponent();
            date1.DateTime = new DateTime(ticksFrom);
            date2.DateTime = new DateTime(ticksTo);
            accounts = csAccounts;
            _costCenterID = costCenterID;
            _accountTitle = accountTitle;
            showLedger();
        }
        void setGeneratingMode(bool generating)
        {
            panelControl.Enabled = !generating;
        }
        void showLedger()
        {
            webBrowser.LoadTextPage("", "<h1>Loading ledger </h1>");
            setGeneratingMode(true);
            DateTime t1 = date1.DateTime;
            DateTime t2 = date2.DateTime;
            new System.Threading.Thread(delegate()
                {
                    try
                    {
                        _ledgerHTML = AccountExplorer.buildLedger(t1,t2, _costCenterID, accounts, "Main Cost Center", _accountTitle, false, AccountExplorer.ReportType.Ledger, true);
                    }
                    catch (Exception ex)
                    {
                        _ledgerHTML = string.Format("<h1>Error generating ledger</h1><h2>{0}</h2><h3>{1}</h3>", System.Web.HttpUtility.HtmlEncode(ex.Message), System.Web.HttpUtility.HtmlEncode(ex.StackTrace));
                    }
                    this.Invoke(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                    {
                        webBrowser.LoadControl(this);
                        setGeneratingMode(false);
                    }
                    ));
                }
            ).Start();
        }

        public void Build()
        {
            
        }

        public string GetOuterHtml()
        {
            return _ledgerHTML;
        }

        public bool ProcessUrl(string path, System.Collections.Hashtable query)
        {
            return AccountExplorer.ProcessUrl(this, path, query, new INTAPS.UI.HTML.ProcessParameterless(realoadLedger));
        }

        public void SetHost(INTAPS.UI.HTML.IHTMLDocumentHost host)
        {
            
        }

        public string WindowCaption
        {
            get { return "Ledger"; }
        }
        void realoadLedger()
        {
            invalidateLedger();
        }
        void invalidateLedger()
        {
            webBrowser.LoadTextPage("Ledger", "<h1>Click show ledger to see the leger..</h1>");
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            showLedger();
        }

        private void LedgerViewer_Load(object sender, EventArgs e)
        {

        }
    }
}
