﻿namespace BIZNET.iERP.Client
{
    partial class StockCardViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.webBrowser = new INTAPS.UI.HTML.ControlBrowser();
            this.panelControl = new System.Windows.Forms.Panel();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.comboDate2Mode = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.date2 = new BIZNET.iERP.Client.BNDualCalendar();
            this.date1 = new BIZNET.iERP.Client.BNDualCalendar();
            this.comboQuickPeriod = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panelControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboDate2Mode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboQuickPeriod.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // webBrowser
            // 
            this.webBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser.Location = new System.Drawing.Point(0, 76);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(981, 513);
            this.webBrowser.StyleSheetFile = "berp.css";
            this.webBrowser.TabIndex = 6;
            // 
            // panelControl
            // 
            this.panelControl.Controls.Add(this.buttonRefresh);
            this.panelControl.Controls.Add(this.comboQuickPeriod);
            this.panelControl.Controls.Add(this.comboDate2Mode);
            this.panelControl.Controls.Add(this.labelControl2);
            this.panelControl.Controls.Add(this.labelControl1);
            this.panelControl.Controls.Add(this.date2);
            this.panelControl.Controls.Add(this.date1);
            this.panelControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl.Location = new System.Drawing.Point(0, 0);
            this.panelControl.Name = "panelControl";
            this.panelControl.Size = new System.Drawing.Size(981, 76);
            this.panelControl.TabIndex = 7;
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRefresh.Location = new System.Drawing.Point(903, 3);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(75, 23);
            this.buttonRefresh.TabIndex = 18;
            this.buttonRefresh.Text = "Refresh";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // comboDate2Mode
            // 
            this.comboDate2Mode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comboDate2Mode.EditValue = "Before Next Day";
            this.comboDate2Mode.Location = new System.Drawing.Point(636, 43);
            this.comboDate2Mode.Name = "comboDate2Mode";
            this.comboDate2Mode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboDate2Mode.Properties.Items.AddRange(new object[] {
            "Before Next Day",
            "23:59:1 (Before Closing)"});
            this.comboDate2Mode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboDate2Mode.Size = new System.Drawing.Size(153, 20);
            this.comboDate2Mode.TabIndex = 17;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl2.Location = new System.Drawing.Point(182, 46);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 16;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl1.Location = new System.Drawing.Point(182, 19);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 15;
            this.labelControl1.Text = "From:";
            // 
            // date2
            // 
            this.date2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.date2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.date2.Location = new System.Drawing.Point(219, 42);
            this.date2.Name = "date2";
            this.date2.ShowEthiopian = true;
            this.date2.ShowGregorian = true;
            this.date2.ShowTime = false;
            this.date2.Size = new System.Drawing.Size(411, 21);
            this.date2.TabIndex = 13;
            this.date2.VerticalLayout = false;
            // 
            // date1
            // 
            this.date1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.date1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.date1.Location = new System.Drawing.Point(219, 15);
            this.date1.Name = "date1";
            this.date1.ShowEthiopian = true;
            this.date1.ShowGregorian = true;
            this.date1.ShowTime = false;
            this.date1.Size = new System.Drawing.Size(411, 21);
            this.date1.TabIndex = 14;
            this.date1.VerticalLayout = false;
            // 
            // comboQuickPeriod
            // 
            this.comboQuickPeriod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comboQuickPeriod.EditValue = "";
            this.comboQuickPeriod.Location = new System.Drawing.Point(12, 16);
            this.comboQuickPeriod.Name = "comboQuickPeriod";
            this.comboQuickPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboQuickPeriod.Properties.Items.AddRange(new object[] {
            "Before Next Day",
            "23:59:1 (Before Closing)"});
            this.comboQuickPeriod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboQuickPeriod.Size = new System.Drawing.Size(153, 20);
            this.comboQuickPeriod.TabIndex = 17;
            // 
            // StockCardViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(981, 589);
            this.Controls.Add(this.webBrowser);
            this.Controls.Add(this.panelControl);
            this.Name = "StockCardViewer";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ledger Viewer";
            this.Load += new System.EventHandler(this.LedgerViewer_Load);
            this.panelControl.ResumeLayout(false);
            this.panelControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboDate2Mode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboQuickPeriod.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private INTAPS.UI.HTML.ControlBrowser webBrowser;
        private System.Windows.Forms.Panel panelControl;
        private DevExpress.XtraEditors.ComboBoxEdit comboDate2Mode;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private BNDualCalendar date2;
        private BNDualCalendar date1;
        private System.Windows.Forms.Button buttonRefresh;
        private DevExpress.XtraEditors.ComboBoxEdit comboQuickPeriod;
    }
}