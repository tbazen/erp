﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    public partial class StockCardViewer : Form, INTAPS.UI.HTML.IHTMLBuilder
    {
        int _costCenterID;
        int[] accounts;
        string _ledgerHTML = "";
        string _accountTitle;
        TransactionItems titem=null;
        DateRangeController _dateRange;
        public StockCardViewer(string itemCode,int costCenterID)
        {
            titem=iERPTransactionClient.GetTransactionItems(itemCode);
            
            AccountingPeriod p = iERPTransactionClient.GetAccountingPeriod("AP_Accounting_Year", DateTime.Now);
            if (p == null)
                init(costCenterID, DateTime.Parse("1/1/1900").Ticks, DateTime.Now.Ticks);
            else
                init(costCenterID, p.fromDate.Ticks, p.toDate.Ticks);
        }
        private void init(int costCenterID,long ticksFrom, long ticksTo)
        {
            InitializeComponent();
            _dateRange = new DateRangeController(date1, date2, null, null, comboDate2Mode,comboQuickPeriod);
            _dateRange.DateRangeChanged += _dateRange_DateRangeChanged;
            _costCenterID = costCenterID;
            _dateRange.setDateRange(new DateTime( ticksFrom), new DateTime(ticksTo));
            showLedger();
        }

        void _dateRange_DateRangeChanged(object sender, EventArgs e)
        {
            
        }
        void setGeneratingMode(bool generating)
        {
            panelControl.Enabled = !generating;
        }
        void showLedger()
        {
            webBrowser.LoadTextPage("", "<h1>Loading ledger </h1>");
            setGeneratingMode(true);
            DateTime t1 = _dateRange.getDate1();
            DateTime t2 = _dateRange.getDate2();
            new System.Threading.Thread(delegate()
                {
                    try
                    {
                        _ledgerHTML = AccountExplorer.getBinCard(t1, t2,
                            INTAPS.Accounting.Client.AccountingClient.GetAccount<CostCenter>( _costCenterID),
                            INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(titem.inventoryAccountID));
                    }
                    catch (Exception ex)
                    {
                        _ledgerHTML = string.Format("<h1>Error generating ledger</h1><h2>{0}</h2><h3>{1}</h3>", System.Web.HttpUtility.HtmlEncode(ex.Message), System.Web.HttpUtility.HtmlEncode(ex.StackTrace));
                    }
                    this.Invoke(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                    {
                        webBrowser.LoadControl(this);
                        setGeneratingMode(false);
                    }
                    ));
                }
            ).Start();
        }

        public void Build()
        {
            
        }

        public string GetOuterHtml()
        {
            return _ledgerHTML;
        }

        public bool ProcessUrl(string path, System.Collections.Hashtable query)
        {
            return AccountExplorer.ProcessUrl(this, path, query, new INTAPS.UI.HTML.ProcessParameterless(realoadLedger));
        }

        public void SetHost(INTAPS.UI.HTML.IHTMLDocumentHost host)
        {
            
        }

        public string WindowCaption
        {
            get { return "Ledger"; }
        }
        void realoadLedger()
        {
            invalidateLedger();
        }
        void invalidateLedger()
        {
            webBrowser.LoadTextPage("Ledger", "<h1>Click show ledger to see the leger..</h1>");
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            showLedger();
        }

        private void LedgerViewer_Load(object sender, EventArgs e)
        {

        }
    }
}
