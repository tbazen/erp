﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class BudgetEditor : DevExpress.XtraEditors.XtraForm
    {
        int _costCenterID;
        int _budgetID;
        public BudgetEditor(int costCenterID)
        {
            InitializeComponent();
            textCostCenter.Text = AccountingClient.GetAccount<CostCenter>(costCenterID).CodeName;
            _costCenterID = costCenterID;
            _budgetID = -1;
            colBudget.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.Decimal;
            setStructure(false);
        }
        Account[] getAccountChild(int PID)
        {
            int N;
            return AccountingClient.GetChildAcccount<Account>(PID, 0, -1, out N);
        }
        void addNode(DevExpress.XtraTreeList.Nodes.TreeListNodes nodes, Account account)
        {
            DevExpress.XtraTreeList.Nodes.TreeListNode n = nodes.Add(new object[] { account.Code, account.Name, null });
            n.Tag = account.id;
            if (account.childCount > 0)
            {
                n.Nodes.Add("");
            }
        }        
        public BudgetEditor(Budget doc)
            : this(doc.costCenterID)
        {
            _budgetID = doc.id;
            setStructure(false);
            dateFrom.DateTime = new DateTime( doc.ticksFrom);
            dateTo.DateTime = new DateTime(doc.ticksTo);

            foreach (BudgetEntry e in iERPTransactionClient.getBudgetEntries(doc.id))
            {
                setEntry(e);
            }
        }

        private void setEntry(BudgetEntry e)
        {
            List<Account> parents = new List<Account>();
            Account parent = AccountingClient.GetAccount<Account>(AccountingClient.GetAccount<Account>(e.accountID).PID);
            while (parent != null)
            {
                parents.Add(parent);
                parent = AccountingClient.GetAccount<Account>(AccountingClient.GetAccount<Account>(parent.id).PID);
            }

            DevExpress.XtraTreeList.Nodes.TreeListNodes nodes = treeAccounts.Nodes;
            bool found;
            for (int i = parents.Count - 1;i>=0; i--)
            {
                int searchID = parents[i].id;
                found = false;
                foreach (DevExpress.XtraTreeList.Nodes.TreeListNode n in nodes)
                {
                    int ac = (int)n.Tag;
                    if (ac==searchID)
                    {
                        found = true;
                        nodes = n.Nodes;
                        n.Expanded = true;
                        break;
                    }
                }
                if (!found)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Account not found ID:" + e.accountID);
                    return;
                }
            }
            found = false;   
            foreach (DevExpress.XtraTreeList.Nodes.TreeListNode n in nodes)
            {
                int ac = (int)n.Tag;
                if (ac==e.accountID)
                {
                    found = true;
                    n.SetValue(2, e.amount.ToString("#,#0.00"));
                    break;
                }
            }
            if (!found)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Account not found ID:" + e.accountID);
                return;
            }
        }

        bool getBudgetEntries(DevExpress.XtraTreeList.Nodes.TreeListNodes  nodes,List<BudgetEntry> entries)
        {
            foreach(DevExpress.XtraTreeList.Nodes.TreeListNode n in nodes)
            {
                object _am=n.GetValue(2);
                double amount;
                if (n.Tag is int && _am != null && double.TryParse(_am.ToString(),out amount) && AccountBase.AmountGreater(amount,0))
                {
                    
                    entries.Add(new BudgetEntry()
                    {
                        accountID=(int)n.Tag,
                        amount=amount,
                    });
                }
                if (!getBudgetEntries(n.Nodes, entries))
                    return false;
            }
            return true;
        }
        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                List<BudgetEntry> entries = new List<BudgetEntry>();
                if (!getBudgetEntries(this.treeAccounts.Nodes, entries))
                    return;
                
                Budget budget = new Budget();
                budget.costCenterID = _costCenterID;
                budget.ticksFrom = dateFrom.DateTime.Ticks;
                budget.ticksTo = dateTo.DateTime.Ticks;
                budget.id = _budgetID;

                if(budget.id==-1)
                    iERPTransactionClient.createBudget(budget,entries.ToArray());
                else
                    iERPTransactionClient.updateBudget(budget,entries.ToArray());
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        const int MAX_CHILD = 500;
        private void treeAccounts_BeforeExpand(object sender, DevExpress.XtraTreeList.BeforeExpandEventArgs e)
        {
            if(e.Node.HasChildren)
            {
                if(e.Node.FirstNode.Tag==null)
                {
                    e.Node.Nodes.Clear();
                    foreach (Account ac in getAccountChild((int)e.Node.Tag))
                    {
                        addNode(e.Node.Nodes, ac);
                    }
                }
            }
        }
        bool _ignoreChangeEvent = false;
        private void treeAccounts_CellValueChanged(object sender, DevExpress.XtraTreeList.CellValueChangedEventArgs e)
        {
            if (_ignoreChangeEvent)
                return;
            DevExpress.XtraTreeList.Nodes.TreeListNode parent = e.Node.ParentNode;
            while (parent != null)
            {
                parent.SetValue(2,0);
                parent = parent.ParentNode;
            }
            if(e.Node.HasChildren)
            {
                DevExpress.XtraTreeList.Nodes.TreeListNode child=e.Node.FirstNode;
                while (child!=null)
                {
                    child.SetValue(2, 0);
                    child = child.NextNode;
                }
            }
        }


        void updateTree()
        {
            setStructure(true);
        }

        private void setStructure(bool update)
        {
            List<BudgetEntry> entries = null;
            if (update)
            {
                entries = new List<BudgetEntry>();
                if (!getBudgetEntries(this.treeAccounts.Nodes, entries))
                    return;
            }
            treeAccounts.Nodes.Clear();
            foreach (Account item in getAccountChild(-1))
            {
                addNode(treeAccounts.Nodes, item);
            }
            if (entries != null)
            {
                foreach (BudgetEntry e in entries)
                {
                    setEntry(e);
                }
            }
        }

        private void comboAcType_SelectedIndexChanged(object sender, EventArgs e)
        {
            setStructure(true);
        }
    }
}