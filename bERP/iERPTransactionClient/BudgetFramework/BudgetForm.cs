﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Web;

namespace BIZNET.iERP.Client
{
    public partial class BudgetForm : DevExpress.XtraEditors.XtraForm
    {
        Budget _currentBuget = null;
        public BudgetForm()
        {
            InitializeComponent();
            costCenterPlaceHolder.SetByID(CostCenter.ROOT_COST_CENTER);
            htmlTool.setBrowser(htmlBrowser);
            ReloadBudgetList();
            loadCurrentBudget(_currentBuget);
        }

        private void loadCurrentBudget(Budget b)
        {
            _currentBuget = b;
            if (_currentBuget == null)
            {
                htmlBrowser.LoadTextPage("Budget", "");
                buttonEdit.Enabled = buttonDelete.Enabled = false;
                return;
            }
            long today = DateTime.Today.Ticks;
            long target=today<b.ticksTo?today:b.ticksTo;
            StringBuilder sb = new StringBuilder();

            generateTitleForBudget(sb, AccountingClient.GetAccount<CostCenter>(b.costCenterID), new DateTime(b.ticksFrom), new DateTime(b.ticksTo));

            buttonEdit.Enabled = buttonDelete.Enabled = true;
            try
            {
                sb.Append(iERPTransactionClient.getBudgetReport(new BudgetFilter(_currentBuget.costCenterID,new DateTime(target))));
                htmlBrowser.LoadTextPage("Budget",sb.ToString() );
            }
            catch (Exception ex)
            {
                htmlBrowser.LoadTextPage("Budget", string.Format("<h2>Error Generating Budget Control Report</h2>{0}", System.Web.HttpUtility.HtmlEncode(ex.Message)));
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            int csid;
            if ((csid=costCenterPlaceHolder.GetAccountID()) == -1)
            {
                MessageBox.ShowErrorMessage("Select cost center first");
                return;
            }
            BudgetEditor be = new BudgetEditor(csid);
            if(be.ShowDialog(this)==DialogResult.OK)
            {
                ReloadBudgetList();
            }
        }

        private void ReloadBudgetList()
        {
            listView.Items.Clear();
            int csid=costCenterPlaceHolder.GetAccountID();
            if (csid == -1)
            {
                buttonAdd.Enabled = false;
                return;
            }
            buttonAdd.Enabled = true;

            try
            {
                ListViewItem selected=null;
                foreach (Budget b in iERPTransactionClient.getAllBudgets(csid))
                {
                    ListViewItem li = new ListViewItem(new DateTime(b.ticksFrom).ToString("MMM dd,yyyy") + " - " + new DateTime(b.ticksTo).ToString("MMM dd,yyyy"));
                    li.SubItems.Add(AccountBase.FormatAmount(b.totalBudget));
                    listView.Items.Add(li);
                    li.Tag = b;
                    if (_currentBuget != null && b.id == _currentBuget.id)
                        selected = li;
                }
                if (selected != null)
                    selected.Selected = true;
                else
                {
                    if (listView.Items.Count > 0)
                        listView.Items[0].Selected = true;
                    else 
                        loadCurrentBudget(null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }
        private void generateTitleForBudget(StringBuilder builder, CostCenter cs, DateTime from,DateTime to)
        {
            TSConstants.addCompanyNameLine(iERPTransactionClient.GetCompanyProfile(), builder);
            builder.Append("<h2>Budget Control Report</h2>");
            builder.Append(string.Format("<span class='SubTitle'><b>Cost Center: </b> <u>{0}</u></span>", HttpUtility.HtmlEncode(cs.Name)));
            builder.Append(string.Format("<br/><span class='SubTitle'><b>From </b> <u>{0}</u><b> to </b> <u>{1}</u></span>", HttpUtility.HtmlEncode(AccountBase.FormatDate(from)), HttpUtility.HtmlEncode(AccountBase.FormatDate(to))));

        }
        private void costCenterPlaceHolder_AccountChanged(object sender, EventArgs e)
        {
            ReloadBudgetList();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (_currentBuget == null)
                return;
            if (MessageBox.ShowWarningMessage("Are you sure you want to delete the selected budget?") != DialogResult.Yes)
                return;
            try
            {
                iERPTransactionClient.deleteBudget(_currentBuget.id);
                ReloadBudgetList();
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }


        }

        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView.SelectedItems.Count == 0)
                loadCurrentBudget(null);
            else
                loadCurrentBudget(listView.SelectedItems[0].Tag as Budget);
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (_currentBuget == null)
                return;
            BudgetEditor be = new BudgetEditor(_currentBuget);
            if (be.ShowDialog(this) == DialogResult.OK)
            {
                ReloadBudgetList();
            }
        }


    }
}