﻿namespace BIZNET.iERP.Client
{
    partial class BudgetTransactionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnPost = new DevExpress.XtraEditors.SimpleButton();
            this.addEntry = new DevExpress.XtraEditors.SimpleButton();
            this.btnRemove = new DevExpress.XtraEditors.SimpleButton();
            this.validateControls = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.gridEntry = new DevExpress.XtraGrid.GridControl();
            this.gridViewEntry = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.validateControls)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEntry)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnClose);
            this.panelControl1.Controls.Add(this.btnPost);
            this.panelControl1.Controls.Add(this.addEntry);
            this.panelControl1.Controls.Add(this.btnRemove);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 396);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(750, 105);
            this.panelControl1.TabIndex = 7;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(670, 78);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnPost
            // 
            this.btnPost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPost.Location = new System.Drawing.Point(580, 79);
            this.btnPost.Name = "btnPost";
            this.btnPost.Size = new System.Drawing.Size(75, 23);
            this.btnPost.TabIndex = 0;
            this.btnPost.Text = "&Post";
            this.btnPost.Click += new System.EventHandler(this.btnPost_Click);
            // 
            // addEntry
            // 
            this.addEntry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.addEntry.Location = new System.Drawing.Point(120, 75);
            this.addEntry.Name = "addEntry";
            this.addEntry.Size = new System.Drawing.Size(102, 26);
            this.addEntry.TabIndex = 0;
            this.addEntry.Text = "&Add Entry";
            this.addEntry.Click += new System.EventHandler(this.addEntry_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRemove.Location = new System.Drawing.Point(12, 75);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(102, 26);
            this.btnRemove.TabIndex = 0;
            this.btnRemove.Text = "&Remove Entries";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // validateControls
            // 
            this.validateControls.ValidateHiddenControls = false;
            // 
            // gridEntry
            // 
            this.gridEntry.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEntry.Location = new System.Drawing.Point(0, 0);
            this.gridEntry.MainView = this.gridViewEntry;
            this.gridEntry.Name = "gridEntry";
            this.gridEntry.Padding = new System.Windows.Forms.Padding(3);
            this.gridEntry.Size = new System.Drawing.Size(750, 396);
            this.gridEntry.TabIndex = 8;
            this.gridEntry.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewEntry});
            // 
            // gridViewEntry
            // 
            this.gridViewEntry.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewEntry.Appearance.GroupPanel.ForeColor = System.Drawing.Color.DarkViolet;
            this.gridViewEntry.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewEntry.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewEntry.GridControl = this.gridEntry;
            this.gridViewEntry.GroupPanelText = "Enter Account Code to Debit or Credit";
            this.gridViewEntry.Name = "gridViewEntry";
            this.gridViewEntry.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewEntry.OptionsCustomization.AllowFilter = false;
            this.gridViewEntry.OptionsCustomization.AllowGroup = false;
            this.gridViewEntry.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewEntry.OptionsCustomization.AllowSort = false;
            this.gridViewEntry.OptionsSelection.MultiSelect = true;
            this.gridViewEntry.OptionsView.ShowFooter = true;
            this.gridViewEntry.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewAccountAdjustment_CellValueChanged);
            this.gridViewEntry.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewAccountAdjustment_CellValueChanging);
            // 
            // BudgetTransactionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 501);
            this.Controls.Add(this.gridEntry);
            this.Controls.Add(this.panelControl1);
            this.MaximizeBox = false;
            this.Name = "BudgetTransactionForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Budget Entries";
            this.Load += new System.EventHandler(this.AdjustmentForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.validateControls)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEntry)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnPost;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validateControls;
        private DevExpress.XtraEditors.SimpleButton btnRemove;
        private DevExpress.XtraEditors.SimpleButton addEntry;
        private DevExpress.XtraGrid.GridControl gridEntry;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewEntry;
    }
}