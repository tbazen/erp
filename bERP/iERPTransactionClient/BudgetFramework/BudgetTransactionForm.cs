﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Columns;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using DevExpress.XtraEditors.Controls;

namespace BIZNET.iERP.Client
{
    public partial class BudgetTransactionForm : XtraForm
    {
        private BudgetTransactionDocument m_doc;
        DataTable _entryTable;
        IAccountingClient _accountingClient;
        int _referedDocumentID;
        public BudgetTransactionForm(IAccountingClient client, ActivationParameter pars)
        {
            InitializeComponent();
            _accountingClient = client;
            _entryTable = new DataTable();
            _entryTable.RowChanged += new DataRowChangeEventHandler(_entryTable_RowChanged);
            _entryTable.RowDeleted += new DataRowChangeEventHandler(_entryTable_RowDeleted);
            PrepareAccountLedgerGrid();
            InitializeInplaceTextEditors();
            _referedDocumentID = pars.referedDocumentID;
        }

        void _entryTable_RowDeleted(object sender, DataRowChangeEventArgs e)
        {
        }

        void _entryTable_RowChanged(object sender, DataRowChangeEventArgs e)
        {
        }

        private void PrepareAccountLedgerGrid()
        {
            _entryTable.Columns.Add("Account", typeof(string));
            _entryTable.Columns.Add("Cost Center", typeof(string));
            _entryTable.Columns.Add("Debit", typeof(double));
            _entryTable.Columns.Add("Credit", typeof(double));
            _entryTable.Columns.Add("Note", typeof(string));
            _entryTable.Columns.Add("costCenterID", typeof(int));
            _entryTable.Columns.Add("accountID", typeof(int));
            _entryTable.Columns["costCenterID"].DefaultValue = (int)-1;
            _entryTable.Columns["accountID"].DefaultValue = (int)-1;
            
            gridEntry.DataSource = _entryTable;
            
            gridViewEntry.Columns["costCenterID"].Visible = false;
            gridViewEntry.Columns["accountID"].Visible = false;
            
            gridViewEntry.Columns["Account"].Width = gridEntry.Width * 3/10;
            gridViewEntry.Columns["Cost Center"].Width = gridEntry.Width * 3 / 10;
            
            gridViewEntry.Columns["Debit"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            gridViewEntry.Columns["Debit"].SummaryItem.DisplayFormat = "Total = {0:n2}";

            gridViewEntry.Columns["Credit"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            gridViewEntry.Columns["Credit"].SummaryItem.DisplayFormat = "Total = {0:n2}";
        }

        private void InitializeInplaceTextEditors()
        {
            InitializeAccountCodeInplaceTextEditor();
            InitializeAccountNameInplaceTextEditor();
            InitializeDebitInplaceTextEditor();
            InitializeCreditInplaceTextEditor();
            InitializeNoteInplaceTextEditor();
        }


        private  void InitializeAccountCodeInplaceTextEditor()
        {
            RepositoryItemTextEdit txtAccountCode = new RepositoryItemTextEdit();
            txtAccountCode.Mask.EditMask = "[0-9a-zA-Z].*";
            txtAccountCode.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            gridViewEntry.GridControl.RepositoryItems.Add(txtAccountCode);
            gridViewEntry.Columns["Account"].ColumnEdit = txtAccountCode;
            txtAccountCode.EditValueChanging += txtAccountCode_EditValueChanging;
        }
        ItemSearchPopup _popup =null;
        bool popupShown
        {
            get
            {
                return _popup != null && _popup.Visible;
            }
        }
        void trackPopup(PopupSearchType type, string code,Point p)
        {
            if (_popup == null)
            {
                _popup = new ItemSearchPopup();
                _popup.ItemSelected += new EventHandler(_popup_ItemSelected);
                _popup.FormClosing += new FormClosingEventHandler(_popup_FormClosing);
            }
            if (!popupShown)
            {
                _popup.Show();
                _popup.Location = p;
            }
            _popup.setSearch(code, type);
        }

        void _popup_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        void _popup_ItemSelected(object sender, EventArgs e)
        {
            gridViewEntry.HideEditor();
            _popup.Hide();
            if (_popup.searchType == PopupSearchType.costCenter)
            {
                setCostCenter(_editRow, (CostCenter)_popup.selectedCostCenter);
                gridViewEntry.FocusedColumn = gridViewEntry.Columns["Debit"];
                gridViewEntry.ShowEditor();
                gridEntry.Focus();
            }
            else
            {
                setAccount(_editRow, (Account)_popup.selectedCostCenter);
                gridViewEntry.FocusedColumn = gridViewEntry.Columns["Cost Center"];
                gridViewEntry.ShowEditor();
                gridEntry.Focus();
            }
        }

        private void setAccount(DataRow _editRow, Account account)
        {
            _editRow["Account"] = account.CodeName;
            _editRow["accountID"] = account.id;
        }

        private void setCostCenter(DataRow _editRow, CostCenter costCenter)
        {
            _editRow["Cost Center"] = costCenter.CodeName;
            _editRow["costCenterID"] = costCenter.id;
        }

        void _popup_Deactivate(object sender, EventArgs e)
        {

        }

        void txtAccountCode_EditValueChanging(object sender, ChangingEventArgs e)
        {
            string accountCode = e.NewValue as string;
            string code = accountCode == "" || accountCode == null ? "" : accountCode;
            gridViewEntry.SetRowCellValue(gridViewEntry.FocusedRowHandle, "Account Code", code);
            gridViewEntry.SetRowCellValue(gridViewEntry.FocusedRowHandle, "Account Name", "");
        }
        private  void InitializeAccountNameInplaceTextEditor()
        {
            /*RepositoryItemTextEdit txtAccountName = new RepositoryItemTextEdit();
            txtAccountName.Mask.EditMask = "[0-9a-zA-Z].*";
            txtAccountName.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            gridViewAccountAdjustment.GridControl.RepositoryItems.Add(txtAccountName);
            gridViewAccountAdjustment.Columns["Account Name"].ColumnEdit = txtAccountName;*/
        }

        private void InitializeDebitInplaceTextEditor()
        {
            RepositoryItemTextEdit txtDebit = new RepositoryItemTextEdit();
            txtDebit.Mask.EditMask = "#,########0.00;";
            txtDebit.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            gridViewEntry.GridControl.RepositoryItems.Add(txtDebit);
            gridViewEntry.Columns["Debit"].ColumnEdit = txtDebit;
            txtDebit.EditValueChanging += txtDebit_EditValueChanging;
        }

        void txtDebit_EditValueChanging(object sender, ChangingEventArgs e)
        {
            double debit = e.NewValue as string == "" || e.NewValue == null ? 0 : double.Parse(e.NewValue.ToString());
            gridViewEntry.SetRowCellValue(gridViewEntry.FocusedRowHandle, "Debit", debit);
        }
        private void InitializeCreditInplaceTextEditor()
        {
            RepositoryItemTextEdit txtCredit = new RepositoryItemTextEdit();
            txtCredit.Mask.EditMask = "#,########0.00;";
            txtCredit.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            gridViewEntry.GridControl.RepositoryItems.Add(txtCredit);
            gridViewEntry.Columns["Credit"].ColumnEdit = txtCredit;
            txtCredit.EditValueChanging += txtCredit_EditValueChanging;
        }

        void txtCredit_EditValueChanging(object sender, ChangingEventArgs e)
        {
            double credit = e.NewValue as string == "" || e.NewValue == null ? 0 : double.Parse(e.NewValue.ToString());
            gridViewEntry.SetRowCellValue(gridViewEntry.FocusedRowHandle, "Credit", credit);
        }
        private void InitializeNoteInplaceTextEditor()
        {
            RepositoryItemTextEdit txtNote = new RepositoryItemTextEdit();
            txtNote.Mask.EditMask = "[0-9a-zA-Z].*";
            txtNote.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            gridViewEntry.GridControl.RepositoryItems.Add(txtNote);
            gridViewEntry.Columns["Note"].ColumnEdit = txtNote;
        }
       
        internal void LoadData(BudgetTransactionDocument budgetDoc)
        {
            m_doc = budgetDoc;

            if (budgetDoc != null)
            {
                PopulateGridForUpdate(budgetDoc);
            }
            else
            {
            }
        }

        private void PopulateGridForUpdate(BudgetTransactionDocument adjustmentDoc)
        {
            PopulateGrid(adjustmentDoc);
        }

        private void PopulateGrid(BudgetTransactionDocument adjustmentDoc)
        {
            foreach (BudgetTransaction account in adjustmentDoc.transactions)
            {
                DataRow row = _entryTable.NewRow();
                Account acc = null;
                CostCenter cs= null;
                if ( account.accountID>0)
                {
                    acc = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(account.accountID);
                    cs= INTAPS.Accounting.Client.AccountingClient.GetAccount<CostCenter>(account.costCenterID);
                }
               
                setAccount(row, acc);
                setCostCenter(row, cs);
                if (account.debitAmount != 0)
                    row["Debit"] = account.debitAmount;
                if (account.creditAmount != 0)
                    row["Credit"] = account.creditAmount;
                row["Note"] = account.description;
                _entryTable.Rows.Add(row);
            }
            gridEntry.DataSource = _entryTable;
        }

        private void cmbAdjustType_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void gridControlAccountAdjustment_ProcessGridKey(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
                return;

            
            
        }

 
        private void FocusNextColumn()
        {
            
        }

        private  void GetCurrentCodeAccount(GridView gv)
        {
            
            try
            {
                string accountCode = gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns["Account Code"]) as string;
                //CostCenterAccountWithDescription account = INTAPS.Accounting.Client.AccountingClient.GetCostCenterAccountWithDescription(accountCode);
                CostCenterAccountWithDescription account = Globals.getCostCenterAccountByCode(accountCode);
                if (account == null)
                {
                    MessageBox.ShowErrorMessage("Invalid code " + accountCode);
                    return;
                }
                gv.ClearColumnErrors();
                gv.SetRowCellValue(gv.FocusedRowHandle, gv.Columns["Account Code"], account.code);
                gv.SetRowCellValue(gv.FocusedRowHandle, gv.Columns["Account Name"], account.name);
                gv.SetRowCellValue(gv.FocusedRowHandle, gv.Columns["Debit"], 0);
                gv.SetRowCellValue(gv.FocusedRowHandle, gv.Columns["Credit"], 0);
                gv.FocusedColumn = gv.Columns["Debit"];
               // gv.ShowEditor();
              
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
                gv.SetRowCellValue(gv.FocusedRowHandle, "Account Name", null);
            }
         

        }

       

        private void btnPost_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validateControls.Validate())
                    return;
                List<BudgetTransaction> transactions = new List<BudgetTransaction>();
                int rowCount = 0;
                foreach (DataRow row in _entryTable.Rows)
                {
                    rowCount++;
                    int accountID = row["accountID"] is int ? (int)row["accountID"] : -1;
                    int costCenterID = row["costCenterID"] is int ? (int)row["costCenterID"] : -1;
                    double db = row["Debit"] is double ? (double)row["Debit"] : 0d;
                    double cr = row["Credit"] is double ? (double)row["Credit"] : 0d;
                    string note = row["Note"] is string ? (string)row["Note"] : "";
                    if (accountID == -1 || costCenterID == -1 || (AccountBase.AmountEqual(db, 0) && AccountBase.AmountEqual(cr, 0)))
                        continue;

                    BudgetTransaction adj = new BudgetTransaction();
                    adj.accountID = accountID;
                    adj.costCenterID = costCenterID;
                    adj.debitAmount = db;
                    adj.creditAmount = cr;
                    adj.description = note;
                    transactions.Add(adj);
                }
                int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;
                BudgetTransactionDocument adjDoc = new BudgetTransactionDocument();
                adjDoc.AccountDocumentID = docID;
                adjDoc.transactions = transactions.ToArray();
                adjDoc.referedDocs =new List<int>( new int[] { _referedDocumentID });
                if (adjDoc.IsFutureDate)
                {
                    if (MessageBox.ShowWarningMessage("Are you sure you want to post future transaction?") != DialogResult.Yes)
                        return;
                    adjDoc.scheduled = true;
                    adjDoc.materialized = false;
                }
                _accountingClient.PostGenericDocument(adjDoc);
                string message = docID == -1 ? "Budget entries successfully saved!" : "Budget entrires successfully updated";
                MessageBox.ShowSuccessMessage(message);
                Close();
            }
            catch (Exception ex)
            {
                string exception = ex.InnerException == null ? ex.Message : ex.InnerException.Message + "\n" + ex.Message;
                MessageBox.ShowErrorMessage(exception);
            }
        }
        
        private void ResetControls()
        {
            _entryTable.Rows.Clear();
            gridEntry.DataSource = _entryTable;
            gridEntry.RefreshDataSource();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AdjustmentForm_Load(object sender, EventArgs e)
        {
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            gridViewEntry.DeleteSelectedRows();
        }

        private void gridViewAccountAdjustment_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {

            try
            {
                string value = gridViewEntry.FocusedValue as string;
                if (string.IsNullOrEmpty(value))
                    return;
                if (popupShown)
                    _popup.Hide();
                switch (gridViewEntry.FocusedColumn.FieldName)
                {
                    case "Account":
                        Account ac = AccountingClient.GetAccount<Account>(value);
                        if (ac == null)
                        {
                            ac=(Account) _popup.getTopAccountItem();
                        }
                        if (ac == null)
                        {

                            MessageBox.ShowErrorMessage("Invalid account code:" + value);
                            return;
                        }
                        setAccount(_editRow, ac);
                        if (popupShown)
                            _popup.Hide();
                        //gridViewAccountAdjustment.FocusedColumn = gridViewAccountAdjustment.Columns["Debit"];
                        break;
                    case "Cost Center":
                        CostCenter cs = AccountingClient.GetAccount<CostCenter>(value);
                        if (cs == null)
                        {
                            cs = (CostCenter)_popup.getTopAccountItem();
                        }
                        if (cs == null)
                        {
                            MessageBox.ShowErrorMessage("Invalid cost center code:" + value);
                            return;
                        }
                        setCostCenter(_editRow, cs);
                        if (popupShown)
                            _popup.Hide();
                        //gridViewAccountAdjustment.FocusedColumn = gridViewAccountAdjustment.Columns["Debit"];
                        break;
                    case "Note":
                        //if (gridViewAccountAdjustment.IsNewItemRow(gridViewAccountAdjustment.FocusedRowHandle))
                          //  gridViewAccountAdjustment.AddNewRow();
                        //gridViewAccountAdjustment.MoveNext();
                        //gridViewAccountAdjustment.FocusedColumn = gridViewAccountAdjustment.Columns["Account"];
                        break;

                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }
        DataRow _editRow;
        private void gridViewAccountAdjustment_CellValueChanging(object sender, CellValueChangedEventArgs e)
        {
            BaseEdit be = gridViewEntry.ActiveEditor;
            if(be==null)
                return;
            Point p= be.PointToScreen(new System.Drawing.Point(2, be.Height + 5));
            DataRow editRow=gridViewEntry.GetDataRow(e.RowHandle);
            if (editRow == null)
            {
                _editRow = null;
                return;
            }
            switch (e.Column.FieldName)
            {
                case "Cost Center":
                    trackPopup(PopupSearchType.costCenter, e.Value.ToString(),p);
                    be.Focus();
                    break;
                case "Account":
                    if ((int)editRow["costCenterID"]==-1)
                    {
                        if(Globals.currentCostCenterID!=-1)
                            setCostCenter(editRow, AccountingClient.GetAccount<CostCenter>(Globals.currentCostCenterID));
                    }
                    trackPopup(PopupSearchType.account, e.Value.ToString(),p);
                    be.Focus();
                    break;
                default:
                    break;
            }
            _editRow = editRow;
        }

        private void gridViewAccountAdjustment_ShownEditor(object sender, EventArgs e)
        {
            switch (gridViewEntry.FocusedColumn.FieldName)
            {
                case "Account":case "Cost Center":
                    gridViewEntry.ActiveEditor.SelectAll();
                    break;
            }
        }

        private void gridViewAccountAdjustment_HiddenEditor(object sender, EventArgs e)
        {
            if (popupShown)
                _popup.Hide();
        }

        private void gridViewAccountAdjustment_LostFocus(object sender, EventArgs e)
        {

        }

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void addEntry_Click(object sender, EventArgs e)
        {
            _entryTable.Rows.Add();
        }
    }
}