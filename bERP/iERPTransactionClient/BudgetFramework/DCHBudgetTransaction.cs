using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
namespace BIZNET.iERP.Client
{
    public class DCHBudgetTransaction : bERPClientDocumentHandler
    {
        public DCHBudgetTransaction()
            : base(typeof(BudgetTransactionForm))
        {
        }
        
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            BudgetTransactionForm f = (BudgetTransactionForm)editor;
            f.LoadData((BudgetTransactionDocument)doc);
        }

    }
}
