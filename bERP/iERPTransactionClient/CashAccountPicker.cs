﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZNET.iERP.Client
{
    public class CashAccountPicker : INTAPS.UI.SimpleObjectListPicker<CashAccount>
    {
        protected override string ObjectTypeName => "Casher";
        protected override IList<CashAccount> GetObjects()
        {
            return iERPTransactionClient.GetAllCashAccounts(true);
        }

        protected override string GetObjectString(CashAccount obj)
        {
            return obj.nameCode;
        }
    }
}
