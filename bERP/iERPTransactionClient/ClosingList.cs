﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Web;

namespace BIZNET.iERP.Client
{
    public partial class ClosingListForm : DevExpress.XtraEditors.XtraForm
    {
        DateTime closedUpto;
        public ClosingListForm()
        {
            InitializeComponent();
            loadList();
        }

        private void loadList()
        {
            closedUpto = iERPTransactionClient.getTransactionsClosedUpto();
            if (closedUpto == DateTime.MinValue)
            {
                lableInfo.Text = "The book is open for transaction";
                buttonDelete.Enabled = buttonDelete.Enabled = false;
            }
            else
            {
                lableInfo.Text = "The book is open for transaction for dates later than "+closedUpto.ToString("MMM dd,yyyy");
                buttonDelete.Enabled = buttonDelete.Enabled = true;
            }
            listView.Items.Clear();
            foreach (ClosedRange r in iERPTransactionClient.getBookClosings())
            {
                ListViewItem li = new ListViewItem(r.toDate.ToString("MMM dd,yyyy"));
                li.SubItems.Add(r.comment);
                li.SubItems.Add(r.agentName);
                li.SubItems.Add(r.actionDate.ToString("MMM dd,yyyy"));
                li.Tag = r;
                listView.Items.Add(li);
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            DatePickerDialog be = new DatePickerDialog(true,"",DateTime.Now);
            if(be.ShowDialog(this)==DialogResult.OK)
            {
                try
                {
                    ClosedRange r = new ClosedRange();
                    r.toDate = be.DateTime;
                    r.comment = be.Note;
                    iERPTransactionClient.closeBookForTransaction(r);
                    loadList();
                }
                catch (Exception ex)
                {
                    MessageBox.ShowException(ex);
                }
                
            }
        }

       

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if(listView.Items.Count<0)
                return;
            if (MessageBox.ShowWarningMessage("Are you sure you want to open the book?") != DialogResult.Yes)
                return;
            ClosedRange r=listView.Items[0].Tag as ClosedRange;
            try
            {
                iERPTransactionClient.updateBookClosingForTransaction(r);
                loadList();
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }


        }

       

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            DatePickerDialog be = new DatePickerDialog(true, "", closedUpto);
            if (be.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    ClosedRange r = new ClosedRange();
                    r.toDate = be.DateTime;
                    r.comment = be.Note;
                    iERPTransactionClient.closeBookForTransaction(r);
                    loadList();
                }
                catch (Exception ex)
                {
                    MessageBox.ShowException(ex);
                }

            }
        }
    }
}