﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    class DateRangeController
    {
        public event EventHandler DateRangeChanged = null;
        BNDualCalendar datePicker1 = null;
        BNDualCalendar datePicker2 = null;
        PeriodSelector periodSelector1 = null;
        PeriodSelector periodSelector2 = null;
        DevExpress.XtraEditors.ComboBoxEdit time2Selector = null;
        DevExpress.XtraEditors.ComboBoxEdit quickPeriodSelector = null;
        public DateRangeController(BNDualCalendar datePicker1,
        BNDualCalendar datePicker2,
        PeriodSelector periodSelector1,
        PeriodSelector period2,
        DevExpress.XtraEditors.ComboBoxEdit time2Selector = null,
            DevExpress.XtraEditors.ComboBoxEdit quickPeriodSelector=null
            )
        {
            this.datePicker1 = datePicker1;
            this.datePicker2 = datePicker2;
            this.periodSelector1 = periodSelector1;
            this.periodSelector2 = period2;
            this.time2Selector = time2Selector;
            this.quickPeriodSelector = quickPeriodSelector;
            fixPeriodSelectorsForDates();

            if(this.datePicker1!=null)
                this.datePicker1.DateTimeChanged+=datePicker1_DateTimeChanged;
            if (this.datePicker2 != null)
                this.datePicker2.DateTimeChanged += datePicker2_DateTimeChanged;
            if(this.periodSelector1!=null)
                this.periodSelector1.SelectedIndexChanged += periodSelector1_SelectedIndexChanged;
            if (this.periodSelector2 != null)
                this.periodSelector2.SelectedIndexChanged += periodSelector2_SelectedIndexChanged;
            if (this.quickPeriodSelector != null)
            {
                this.quickPeriodSelector.Properties.Items.Clear();
                this.quickPeriodSelector.Properties.Items.Add("");
                this.quickPeriodSelector.Properties.Items.Add("This Year");
                this.quickPeriodSelector.Properties.Items.Add("Last Year");
                this.quickPeriodSelector.SelectedIndexChanged += quickPeriodSelector_SelectedIndexChanged;
            }
        }
        void onDateRangeChanged()
        {
            if (this.DateRangeChanged != null)
                this.DateRangeChanged(this, null);
        }
        void quickPeriodSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            AccountingPeriod thisYear, lastYear;
            switch(this.quickPeriodSelector.SelectedIndex)
            {
                case 0:
                    break;
                case 1://this year
                    thisYear = iERPTransactionClient.GetAccountingPeriod("AP_Accounting_Year", DateTime.Now);
                    if (thisYear != null)
                    {
                        this.setDate1(thisYear.fromDate);
                        this.setDate2(thisYear.toDate);
                        onDateRangeChanged();
                    }
                    break;
                case 2://last year
                    thisYear = iERPTransactionClient.GetAccountingPeriod("AP_Accounting_Year", DateTime.Now);
                    if (thisYear != null)
                    {
                        lastYear= iERPTransactionClient.GetAccountingPeriod("AP_Accounting_Year", thisYear.fromDate.AddDays(-1));
                        if (lastYear != null)
                        {
                            this.setDate1(lastYear.fromDate);
                            this.setDate2(lastYear.toDate);
                            onDateRangeChanged();
                        }
                    }
                    break;

            }
        }
        bool _ignoreEvents = false;
        void periodSelector2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(_ignoreEvents)
                return;
            _ignoreEvents = true;
            try
            {
                if (this.datePicker2 != null)
                    this.datePicker2.DateTime = periodSelector2.selectedPeriod.toDate;
            }
            finally
            {
                _ignoreEvents = false;
            }
        }

        void periodSelector1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents)
                return;
            _ignoreEvents = true;
            try
            {
                if (this.datePicker1 != null)
                    setDate2(periodSelector1.selectedPeriod.fromDate);
            }
            finally
            {
                _ignoreEvents = false;
            }
        }

        void datePicker2_DateTimeChanged(object sender, EventArgs e)
        {
            fixPeriodSelectorsForDates();
        }

        void datePicker1_DateTimeChanged(object sender, EventArgs e)
        {
            fixPeriodSelectorsForDates();
        }
        public DateTime getDate2()
        {
            if (time2Selector != null)
            {
                switch (time2Selector.SelectedIndex)
                {
                    case 1:
                        return new DateTime(datePicker2.DateTime.Year, datePicker2.DateTime.Month, datePicker2.DateTime.Day, 23, 58, 0);
                    case 2:
                        return new DateTime(datePicker2.DateTime.Year, datePicker2.DateTime.Month, datePicker2.DateTime.Day, 23, 59, 59);
                }
            }
            return datePicker2.DateTime.AddDays(1);
        }
        public void setDate2(DateTime value)
        {
            if (datePicker2 == null)
                return;
            if (time2Selector != null)
            {
                if (value == new DateTime(datePicker2.DateTime.Year, datePicker2.DateTime.Month, datePicker2.DateTime.Day, 23, 58, 0))
                {
                    datePicker2.DateTime = value.Date.Date;
                    time2Selector.SelectedIndex = 1;
                    return;
                }
                
                if (value == new DateTime(datePicker2.DateTime.Year, datePicker2.DateTime.Month, datePicker2.DateTime.Day, 23, 59, 59))
                {
                    datePicker2.DateTime = value.Date.Date;
                    time2Selector.SelectedIndex = 2;
                    return;
                }
            }
            datePicker2.DateTime=value.AddDays(-1).Date;
        }

        
        private void fixPeriodSelectorsForDates()
        {

            bool selected = false;
            if (periodSelector1 != null)
            {
                foreach (AccountingPeriod prd in periodSelector1.Properties.Items)
                {
                    if (prd.fromDate == datePicker1.DateTime)
                    {
                        selected = true;
                        periodSelector1.selectedPeriod = prd;
                        break;
                    }
                }
                if (!selected)
                    periodSelector1.selectedPeriod = null;
            }
            selected = false;
            DateTime date2 = getDate2();
            if (periodSelector2 != null)
            {
                foreach (AccountingPeriod prd in periodSelector2.Properties.Items)
                {
                    if (prd.toDate == date2)
                    {
                        selected = true;
                        periodSelector2.selectedPeriod = prd;
                        break;
                    }
                }
                if (!selected)
                    periodSelector2.selectedPeriod = null;
            }

        }

        public DateTime getDate1()
        {
            if (datePicker1 == null)
                return DateTime.Now;
            return datePicker1.DateTime;
        }
        public void setDate1(DateTime value)
        {
            if (datePicker1 == null)
                return;
            datePicker1.DateTime = value;
        }
        public void setDateRange(DateTime date1,DateTime date2)
        {
            setDate1(date1);
            setDate2(date2);
        }


    }
}
