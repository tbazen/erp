﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    class DocumentBasicFieldsController
    {
        protected BNDualCalendar _calendar;
        protected MemoEdit _memoShortDescription;
        protected TextEdit _textReference;

        protected List<DocumentSerialPlaceHolder> _serial = new List<DocumentSerialPlaceHolder>();
        protected List<string> _serialFields = new List<string>();
        public DocumentBasicFieldsController(BNDualCalendar cal
            , TextEdit txtRef
            
            ,MemoEdit memoShortDescription)
        {
            _calendar = cal;
            _memoShortDescription = memoShortDescription;
            _textReference = txtRef;
            
        }
        public virtual void setDocumentData(AccountDocument doc)
        {

            if (_textReference != null)
            {
                doc.PaperRef = _textReference.Text;
            }
            doc.DocumentDate = _calendar.DateTime;
            doc.ShortDescription = _memoShortDescription.Text;
            for (int i = 0; i < _serial.Count; i++)
            {
                doc.GetType().GetField(_serialFields[i]).SetValue(doc,_serial[i].getReference());
            }
        }
        public virtual void setControlData(AccountDocument doc)
        {
            if (_textReference != null)
            _textReference.Text=doc.PaperRef;
            _calendar.DateTime=doc.DocumentDate;
            _memoShortDescription.Text=doc.ShortDescription;

            for (int i = 0; i < _serial.Count; i++)
            {
                _serial[i].setReference(doc.AccountDocumentID, doc.GetType().GetField(_serialFields[i]).GetValue(doc) as DocumentTypedReference);
            }
        }
    }

    class PurchaseBasicFieldController : DocumentBasicFieldsController
    {
        CheckEdit _referenceOption;
        public PurchaseBasicFieldController(BNDualCalendar cal
            , TextEdit txtRef
            ,CheckEdit referenceOption
            ,MemoEdit memoShortDescription)
            :base(cal,txtRef,memoShortDescription)
        {
            this._referenceOption = referenceOption;
            this._referenceOption.CheckedChanged += new EventHandler(_referenceOption_CheckedChanged);
            _referenceOption_CheckedChanged(null, null);
        }

        void _referenceOption_CheckedChanged(object sender, EventArgs e)
        {
            base._textReference.Enabled = _referenceOption.Checked;
        }
        public override void setDocumentData(AccountDocument doc)
        {
            base.setDocumentData(doc);
            ((Purchase2Document)doc).invoiced = _referenceOption.Checked;

        }
        public override void setControlData(AccountDocument doc)
        {
            base.setControlData(doc);
            _referenceOption.Checked = ((Purchase2Document)doc).invoiced;
        }
    }
}
