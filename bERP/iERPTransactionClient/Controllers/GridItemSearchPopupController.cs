﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class GridItemSearchPopupController
    {
        DevExpress.XtraGrid.Views.Grid.GridView gridView;
        DevExpress.XtraGrid.Columns.GridColumn colItem,colName;
        ItemSearchPopup _itemPopup;
        public GridItemSearchPopupController(DevExpress.XtraGrid.Views.Grid.GridView gridView, string itemCol, string nameCol)
        {
            this.gridView = gridView;
            colItem = gridView.Columns[itemCol];
            colName = gridView.Columns[nameCol];
            _itemPopup = new ItemSearchPopup();
            _itemPopup.ItemSelected += _itemPopup_ItemSelected;
            gridView.CellValueChanging += gridView_CellValueChanging;
            gridView.CellValueChanged += gridView_CellValueChanged;

        }
        void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column==colItem)
            {
                string code = e.Value as string;
                TransactionItems titem;
                if (!string.IsNullOrEmpty(code) && (titem = iERPTransactionClient.GetTransactionItems(code)) != null)
                {
                    gridView.SetRowCellValue(e.RowHandle, colName, titem.Name);
                }
                _itemPopup.Hide();
            }
        }

        void _itemPopup_ItemSelected(object sender, EventArgs e)
        {
            gridView.SetFocusedValue(_itemPopup.selectedItem.Code);
            
        }

        void gridView_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column==colItem)
            {
                if (!_itemPopup.Visible)
                {
                    _itemPopup.Show();
                    DevExpress.XtraEditors.BaseEdit be = gridView.ActiveEditor;
                    _itemPopup.Location = be.PointToScreen(new System.Drawing.Point(2, be.Height + 5));
                    _itemPopup.Show();
                    gridView.Focus();
                }
                _itemPopup.setSearch(e.Value.ToString(), PopupSearchType.itemCode);
            }
        }
    }
}
