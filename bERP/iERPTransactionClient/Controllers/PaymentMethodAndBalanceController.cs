﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    class AccountLabelPairBase
    {
        public int accountID;
        public LabelControl labelControl;
        public double balance;
        public AccountLabelPairBase(int aid, LabelControl l)
        {
            this.accountID = aid;
            this.labelControl = l;
        }
    }
    class AccountLabelPair:AccountLabelPairBase
    {
        public AccountLabelPair(int aid, LabelControl l)
            : base(aid, l)
        {
        }

        public void Update(DateTime time, int costCenterID)
        {
            if (accountID == -1)
            {
                labelControl.Text = "";
                balance = 0;
            }
            else
            {
                balance = AccountingClient.GetNetCostCenterAccountBalanceAsOf(costCenterID, accountID, time);
                labelControl.Text = AccountBase.FormatBalance(balance);
            }
        }


        
    }
    class CsAccountLabelPair : AccountLabelPairBase
    {
        public CsAccountLabelPair(int aid, LabelControl l)
            : base(aid, l)
        {
        }


        public void Update(DateTime time)
        {
            if (accountID == -1)
            {
                if(labelControl!=null)
                    labelControl.Text = "";
                balance = 0;
            }
            else
            {
                balance = AccountingClient.GetNetBalanceAsOf(accountID, time);
                if (labelControl != null)
                    labelControl.Text = AccountBase.FormatBalance(balance);
            }
        }
    }

    public partial class PaymentMethodAndBalanceController
    {
        public event EventHandler PaymentInformationChanged;
        void onPaymentInformationChanged(Control c)
        {
            if (PaymentInformationChanged != null)
                PaymentInformationChanged(c, null);
        }
        PaymentTypeSelector _paymentTypeSelector = null;
        //bank account
        BankAccountPlaceholder _bankAccounts = null;
        LabelControl _labelBankBalance = null;
        LayoutControlItem _layoutBankBalance = null;
        LayoutControlItem _layoutBankAccounts = null;
        //cash account
        CashAccountPlaceholder _cashAccounts = null;
        LabelControl _lableCashBalance = null;
        LayoutControlItem _layoutCashBalance = null;
        LayoutControlItem _layoutCashAccounts = null;
        
        //references
        LayoutControlItem _layoutServiceCharge = null;
        LayoutControlItem _layoutReference = null;
        LayoutControlItem _layoutServiceChargePaidBy = null;

        BNDualCalendar _calendar;

        CostCenterPlaceHolder _costCenterPlaceHolder;
        
        List<AccountLabelPairBase> _watchAccounts=new List<AccountLabelPairBase>();

        public PaymentMethodAndBalanceController(
            PaymentTypeSelector paymentTypeSelector
            , LayoutControlItem layoutBankAccounts
            , BankAccountPlaceholder bankAccounts
            , LayoutControlItem layoutBankBalance
            , LabelControl labelBankBalance
            , LayoutControlItem layoutCashAccounts
            , CashAccountPlaceholder cashAccounts
            , LayoutControlItem layoutCashBalance
            , LabelControl lableCashBalance
            , LayoutControlItem layoutServiceCharge
            , LayoutControlItem layoutReference
            , BNDualCalendar calendar
            , ActivationParameter activation)
            :this(paymentTypeSelector,layoutBankAccounts,bankAccounts,layoutBankBalance
            ,labelBankBalance,layoutCashAccounts,cashAccounts,layoutCashBalance,lableCashBalance
            ,layoutServiceCharge,layoutReference,calendar,null,activation)
        {

        }
        public PaymentMethodAndBalanceController(
            PaymentTypeSelector paymentTypeSelector
            , LayoutControlItem layoutBankAccounts
            , BankAccountPlaceholder bankAccounts
            , LayoutControlItem layoutBankBalance
            , LabelControl labelBankBalance
            , LayoutControlItem layoutCashAccounts
            , CashAccountPlaceholder cashAccounts
            , LayoutControlItem layoutCashBalance
            , LabelControl lableCashBalance
            , LayoutControlItem layoutServiceCharge
            , LayoutControlItem layoutReference
            , BNDualCalendar calendar
            , LayoutControlItem layoutServiceChargePaidBy
            , ActivationParameter activation)
        {
            this.PaymentTypeSelector = paymentTypeSelector;
            this.BankAccounts = bankAccounts;
            this.LabelBankBalance = labelBankBalance;
            this.LayoutBankBalance = layoutBankBalance;
            this.LayoutBankAccounts = layoutBankAccounts;

            this.CashAccounts = cashAccounts;
            this.LableCashBalance = lableCashBalance;
            this.LayoutCashAccounts = layoutCashAccounts;
            this.LayoutCashBalance = layoutCashBalance;

            this.LayoutServiceCharge = layoutServiceCharge;
            this.LayoutReference = layoutReference;
            this.Calendar = calendar;
            this._layoutServiceChargePaidBy = layoutServiceChargePaidBy;

            if (activation != null)
            {
                try
                {
                    IgnoreEvents();
                    if (activation.saveWithNoPayment)
                    {
                        paymentTypeSelector.Enabled = false;
                        paymentTypeSelector.PaymentMethod = BizNetPaymentMethod.None;
                    }
                    else
                        this.PaymentTypeSelector.PaymentMethod = activation.paymentMethod;

                    _cashAccounts.Enabled=_bankAccounts.Enabled=
                        !activation.fixAssetAccountID && !activation.saveWithNoPayment;
                    _paymentTypeSelector.Enabled=!activation.fixPaymentMethod && !activation.saveWithNoPayment;

                    if (activation.assetAccountID != -1)
                    {
                        this.assetAccountID = activation.assetAccountID;
                    }
                }
                finally
                {
                    AcceptEvents();
                }
            }
        }
        public void AddCostCenterAccountItem(int csAccountID, LabelControl label)
        {
            if (csAccountID == -1)
                return;

            foreach (AccountLabelPairBase i in _watchAccounts)
            {
                CsAccountLabelPair apair = i as CsAccountLabelPair;
                if (apair == null || apair.accountID != csAccountID)
                    throw new Exception("It is not allowed to add an account twice to the payment method controller");
            }
            CsAccountLabelPair pair;
            _watchAccounts.Add(pair=new CsAccountLabelPair(csAccountID, label));
            pair.Update(this.dateTime);
        }
        public void ChangeCostCenterAccountItem(int oldCsAccountID, int newCsAccountID)
        {
            foreach (AccountLabelPairBase i in _watchAccounts)
            {
                CsAccountLabelPair apair = i as CsAccountLabelPair;
                if (apair == null || apair.accountID != newCsAccountID)
                    throw new Exception("It is not allowed to add an account twice to the payment method controller");
            }
            foreach (AccountLabelPairBase i in _watchAccounts)
            {
                CsAccountLabelPair pair = i as CsAccountLabelPair;
                if (pair == null || pair.accountID != oldCsAccountID)
                    continue;
                pair.accountID = newCsAccountID;
                pair.Update(this.dateTime);
                break;
            }
        }
        public void AddAccountItem(int accountID, LabelControl label)
        {
            if (accountID == -1)
                return;
            foreach (AccountLabelPairBase i in _watchAccounts)
            {
                AccountLabelPair apair = i as AccountLabelPair;
                if (apair == null || apair.accountID != accountID)
                    throw new Exception("It is not allowed to add an account twice to the payment method controller");
            }
            AccountLabelPair pair;
            _watchAccounts.Add(pair=new AccountLabelPair(accountID, label));
            pair.Update(this.dateTime,this.costCenterID);
        }
        public void ChangeAccountItem(int oldAccountID, int newAccountID)
        {
            foreach (AccountLabelPairBase i in _watchAccounts)
            {
                AccountLabelPair apair = i as AccountLabelPair;
                if (apair == null || apair.accountID != newAccountID)
                    throw new Exception("It is not allowed to add an account twice to the payment method controller");
            }
            foreach (AccountLabelPairBase i in _watchAccounts)
            {
                AccountLabelPair pair= i as AccountLabelPair;
                if (pair == null || pair.accountID != oldAccountID)
                    continue;
                pair.accountID = newAccountID;
                pair.Update(this.dateTime, this.costCenterID);
                break;
            }
        }
        public CostCenterPlaceHolder CostCenterPlaceHolder
        {
            get { return _costCenterPlaceHolder; }
            set 
            { 

                if (_costCenterPlaceHolder == value)
                    return;
                if (_costCenterPlaceHolder != null)
                    _costCenterPlaceHolder.AccountChanged -= new EventHandler(CostCenterChanged);
                _costCenterPlaceHolder = value;
                _costCenterPlaceHolder.AccountChanged += new EventHandler(CostCenterChanged);

            }
        }

        public LayoutControlItem LayoutServiceChargePaidBy
        {
            get
            {
                return _layoutServiceChargePaidBy;
            }
            set
            {
                _layoutServiceChargePaidBy = value;
            }
        }

        public LayoutControlItem LayoutServiceCharge
        {
            get { return _layoutServiceCharge; }
            set { _layoutServiceCharge = value; }
        }

        public LayoutControlItem LayoutReference
        {
            get { return _layoutReference; }
            set { _layoutReference = value; }
        }

        public DateTime dateTime
        {
            get
            {
                if (_calendar == null)
                    return DateTime.Now;
                return _calendar.DateTime;
            }
        }
        public BNDualCalendar Calendar
        {
            get { return _calendar; }
            set
            {
                if (_calendar == value)
                    return;
                if (_calendar != null)
                    _calendar.DateTimeChanged -= new EventHandler(DateChanged);
                _calendar = value;
                _calendar.DateTimeChanged += new EventHandler(DateChanged);
            }
        }



        public BankAccountPlaceholder BankAccounts
        {
            get { return _bankAccounts; }
            set
            {
                if (_bankAccounts == value)
                    return;
                if (_bankAccounts != null)
                    _bankAccounts.SelectedIndexChanged -= new EventHandler(BankAccountSelectionIndexChanged);
                _bankAccounts = value;
                _bankAccounts.SelectedIndexChanged += new EventHandler(BankAccountSelectionIndexChanged);
            }
        }



        public LabelControl LabelBankBalance
        {
            get { return _labelBankBalance; }
            set { _labelBankBalance = value; }
        }

        public LayoutControlItem LayoutBankBalance
        {
            get { return _layoutBankBalance; }
            set { _layoutBankBalance = value; }
        }

        public LayoutControlItem LayoutBankAccounts
        {
            get { return _layoutBankAccounts; }
            set { _layoutBankAccounts = value; }
        }

        public CashAccountPlaceholder CashAccounts
        {
            get { return _cashAccounts; }
            set
            {
                if (_cashAccounts == value)
                    return;
                if (_cashAccounts != null)
                    _cashAccounts.SelectedIndexChanged -= new EventHandler(CashAccountChanged);
                _cashAccounts = value;
                _cashAccounts.SelectedIndexChanged += new EventHandler(CashAccountChanged);
            }
        }


        public LabelControl LableCashBalance
        {
            get { return _lableCashBalance; }
            set { _lableCashBalance = value; }
        }

        public LayoutControlItem LayoutCashBalance
        {
            get { return _layoutCashBalance; }
            set { _layoutCashBalance = value; }
        }

        public LayoutControlItem LayoutCashAccounts
        {
            get { return _layoutCashAccounts; }
            set { _layoutCashAccounts = value; }
        }

        public PaymentTypeSelector PaymentTypeSelector
        {
            get { return _paymentTypeSelector; }
            set
            {
                if (_paymentTypeSelector == value)
                    return;
                if (_paymentTypeSelector != null)
                    _paymentTypeSelector.SelectedIndexChanged -= new EventHandler(CashAccountChanged);
                _paymentTypeSelector = value;
                _paymentTypeSelector.SelectedIndexChanged += new EventHandler(PaymentTypeChanged);
            }
        }

        
        public string PaymentSource
        {
            get
            {
                if (_paymentTypeSelector == null)
                    return "";
                return " by " + PaymentMethodHelper.GetPaymentTypeText(_paymentTypeSelector.PaymentMethod);

            }
        }

        public int assetAccountID
        {
            get
            {
                if(_paymentTypeSelector==null)
                    return -1;
                if (PaymentMethodHelper.IsPaymentMethodCash(_paymentTypeSelector.PaymentMethod))
                    if (_cashAccounts != null)
                        return _cashAccounts.cashCsAccountID;
                if (PaymentMethodHelper.IsPaymentMethodBank(_paymentTypeSelector.PaymentMethod))
                    if (_bankAccounts!= null)
                        return _bankAccounts.mainCsAccountID;
                return -1;

            }
            set
            {
                if (_paymentTypeSelector == null)
                    return;
                if (PaymentMethodHelper.IsPaymentMethodCash(_paymentTypeSelector.PaymentMethod))
                {
                    if (_cashAccounts != null)
                    {
                        _cashAccounts.cashCsAccountID = value;
                    }
                    return;
                }
                if (PaymentMethodHelper.IsPaymentMethodBank(_paymentTypeSelector.PaymentMethod))
                    if (_bankAccounts != null)
                        _bankAccounts.mainCsAccountID = value;
                return;
            }
        }
        
        public double assetBalance()
        {
            int ac = assetAccountID;
            if (ac == -1)
                return 0;
            DateTime time = _calendar == null ? DateTime.Now : _calendar.DateTime;
            return AccountingClient.GetNetBalanceAsOf(assetAccountID, time);
        }
        public bool validatePaymentDocuemntReference(string chq,string cpo,string tran)
        {
            if (PaymentMethodHelper.IsPaymentMethodWithReference(this.PaymentTypeSelector.PaymentMethod))
            {
                if (string.IsNullOrEmpty(PaymentMethodHelper.selectReference(this.PaymentTypeSelector.PaymentMethod,chq,cpo,tran)))
                {
                    MessageBox.ShowErrorMessage("Please enter Check No, CPO No or Transfer Reference");
                    return false;
                }
            }
            return true;
        }
        internal bool validateAssetAcountSelected()
        {
            if (this.assetAccountID == -1)
            {
                MessageBox.ShowErrorMessage("Please select a cash or bank account");
                return false;
            }
            return true;
        }

        public int costCenterID
        {
            get
            {
                return _costCenterPlaceHolder == null ? CostCenter.ROOT_COST_CENTER : _costCenterPlaceHolder.GetAccountID();
            }
        }
        internal double getBalance(LabelControl labelStaffPaymentBal)
        {
            foreach (AccountLabelPairBase pair in _watchAccounts)
            {
                if (pair.labelControl == labelStaffPaymentBal)
                    return pair.balance;
            }
            return 0;
        }
        internal double getCsAccountBalance(int csAccountID)
        {
            foreach (AccountLabelPairBase pair in _watchAccounts)
            {
                if (pair is CsAccountLabelPair)
                {
                    if (pair.accountID== csAccountID)
                        return pair.balance;
                }
            }
            return 0;
        }
        internal double getAccountBalance(int accountID)
        {
            foreach (AccountLabelPairBase pair in _watchAccounts)
            {
                if (pair is AccountLabelPair)
                {
                    if (pair.accountID == accountID)
                        return pair.balance;
                }
            }
            return 0;
        }
    }
}
