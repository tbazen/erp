﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    class AccountLabelPairBaseNative
    {
        public int accountID;
        public Label labelControl;
        public double balance;
        public AccountLabelPairBaseNative(int aid, Label l)
        {
            this.accountID = aid;
            this.labelControl = l;
        }
    }
    class AccountLabelPairNative:AccountLabelPairBaseNative
    {
        public AccountLabelPairNative(int aid, Label l)
            : base(aid, l)
        {
        }

        public void Update(DateTime time, int costCenterID)
        {
            if (accountID == -1)
            {
                labelControl.Text = "";
                balance = 0;
            }
            else
            {
                balance = AccountingClient.GetNetCostCenterAccountBalanceAsOf(costCenterID, accountID, time);
                labelControl.Text = AccountBase.FormatBalance(balance);
            }
        }


        
    }
    class CsAccountLabelPairNative : AccountLabelPairBaseNative
    {
        public CsAccountLabelPairNative(int aid, Label l)
            : base(aid, l)
        {
        }


        public void Update(DateTime time)
        {
            if (accountID == -1)
            {
                if(labelControl!=null)
                    labelControl.Text = "";
                balance = 0;
            }
            else
            {
                balance = AccountingClient.GetNetBalanceAsOf(accountID, time);
                if (labelControl != null)
                    labelControl.Text = AccountBase.FormatBalance(balance);
            }
        }
    }

    public partial class PaymentMethodAndBalanceControllerNative
    {
        public event EventHandler PaymentInformationChanged;
        void onPaymentInformationChanged(Control c)
        {
            if (PaymentInformationChanged != null)
                PaymentInformationChanged(c, null);
        }
        PaymentTypeSelectorNative _paymentTypeSelector = null;
        //bank account
        BankAccountPlaceholderNative _bankAccounts = null;
        Label _labelBankBalance = null;
        //System.Windows.Forms.Panel _layoutBankBalance = null;
        System.Windows.Forms.Panel _layoutBankAccounts = null;
        //cash account
        CashAccountPlaceholderNative _cashAccounts = null;
        Label _lableCashBalance = null;
        //System.Windows.Forms.Panel _layoutCashBalance = null;
        System.Windows.Forms.Panel _layoutCashAccounts = null;

        //references
        System.Windows.Forms.Panel _layoutServiceCharge = null;
        System.Windows.Forms.Panel _layoutReference = null;
        //System.Windows.Forms.Panel _layoutServiceChargePaidBy = null;
        Label _labelRefName;

        INTAPS.Ethiopic.DualCalendar _calendar;

        CostCenterPlaceHolder _costCenterPlaceHolder;
        
        List<AccountLabelPairBaseNative> _watchAccounts=new List<AccountLabelPairBaseNative>();

       
        public PaymentMethodAndBalanceControllerNative(
            PaymentTypeSelectorNative paymentTypeSelector
            , System.Windows.Forms.Panel layoutBankAccounts
            , BankAccountPlaceholderNative bankAccounts
            , Label labelBankBalance
            , System.Windows.Forms.Panel layoutCashAccounts
            , CashAccountPlaceholderNative cashAccounts
            , Label lableCashBalance
            , System.Windows.Forms.Panel layoutServiceCharge
            , System.Windows.Forms.Panel layoutReference
            ,Label labelRefName
            , INTAPS.Ethiopic.DualCalendar calendar
            , ActivationParameter activation)
        {
            this.PaymentTypeSelector = paymentTypeSelector;
            this.BankAccounts = bankAccounts;
            this.LabelBankBalance = labelBankBalance;
            this.LayoutBankAccounts = layoutBankAccounts;

            this.CashAccounts = cashAccounts;
            this.LableCashBalance = lableCashBalance;
            this.LayoutCashAccounts = layoutCashAccounts;

            this.LayoutServiceCharge = layoutServiceCharge;
            this.LayoutReference = layoutReference;
            this.Calendar = calendar;
            this._labelRefName = labelRefName;

            if (activation != null)
            {
                try
                {
                    IgnoreEvents();
                    this.PaymentTypeSelector.PaymentMethod = activation.paymentMethod;
                    _cashAccounts.Enabled=_bankAccounts.Enabled=!activation.fixAssetAccountID;
                    _paymentTypeSelector.Enabled=!activation.fixPaymentMethod;
                    _paymentTypeSelector.PaymentMethod = BizNetPaymentMethod.Check;
                    if (activation.assetAccountID != -1)
                    {
                        this.assetAccountID = activation.assetAccountID;
                    }
                }
                finally
                {
                    AcceptEvents();
                }
            }
            updateReferenceLabel();
        }
        public void AddCostCenterAccountItem(int csAccountID, Label label)
        {
            if (csAccountID == -1)
                return;

            foreach (AccountLabelPairBaseNative i in _watchAccounts)
            {
                CsAccountLabelPairNative apair = i as CsAccountLabelPairNative;
                if (apair == null || apair.accountID != csAccountID)
                    throw new Exception("It is not allowed to add an account twice to the payment method controller");
            }
            CsAccountLabelPairNative pair;
            _watchAccounts.Add(pair=new CsAccountLabelPairNative(csAccountID, label));
            pair.Update(this.dateTime);
        }
        public void ChangeCostCenterAccountItem(int oldCsAccountID, int newCsAccountID)
        {
            foreach (AccountLabelPairBaseNative i in _watchAccounts)
            {
                CsAccountLabelPairNative apair = i as CsAccountLabelPairNative;
                if (apair == null || apair.accountID != newCsAccountID)
                    throw new Exception("It is not allowed to add an account twice to the payment method controller");
            }
            foreach (AccountLabelPairBaseNative i in _watchAccounts)
            {
                CsAccountLabelPairNative pair = i as CsAccountLabelPairNative;
                if (pair == null || pair.accountID != oldCsAccountID)
                    continue;
                pair.accountID = newCsAccountID;
                pair.Update(this.dateTime);
                break;
            }
        }
        public void AddAccountItem(int accountID, Label label)
        {
            if (accountID == -1)
                return;
            foreach (AccountLabelPairBaseNative i in _watchAccounts)
            {
                AccountLabelPairNative apair = i as AccountLabelPairNative;
                if (apair == null || apair.accountID != accountID)
                    throw new Exception("It is not allowed to add an account twice to the payment method controller");
            }
            AccountLabelPairNative pair;
            _watchAccounts.Add(pair=new AccountLabelPairNative(accountID, label));
            pair.Update(this.dateTime,this.costCenterID);
        }
        public void ChangeAccountItem(int oldAccountID, int newAccountID)
        {
            foreach (AccountLabelPairBaseNative i in _watchAccounts)
            {
                AccountLabelPairNative apair = i as AccountLabelPairNative;
                if (apair == null || apair.accountID != newAccountID)
                    throw new Exception("It is not allowed to add an account twice to the payment method controller");
            }
            foreach (AccountLabelPairBaseNative i in _watchAccounts)
            {
                AccountLabelPairNative pair= i as AccountLabelPairNative;
                if (pair == null || pair.accountID != oldAccountID)
                    continue;
                pair.accountID = newAccountID;
                pair.Update(this.dateTime, this.costCenterID);
                break;
            }
        }
        public CostCenterPlaceHolder CostCenterPlaceHolder
        {
            get { return _costCenterPlaceHolder; }
            set 
            { 

                if (_costCenterPlaceHolder == value)
                    return;
                if (_costCenterPlaceHolder != null)
                    _costCenterPlaceHolder.AccountChanged -= new EventHandler(CostCenterChanged);
                _costCenterPlaceHolder = value;
                _costCenterPlaceHolder.AccountChanged += new EventHandler(CostCenterChanged);

            }
        }

       

        public System.Windows.Forms.Panel LayoutServiceCharge
        {
            get { return _layoutServiceCharge; }
            set { _layoutServiceCharge = value; }
        }

        public System.Windows.Forms.Panel LayoutReference
        {
            get { return _layoutReference; }
            set { _layoutReference = value; }
        }

        public DateTime dateTime
        {
            get
            {
                if (_calendar == null)
                    return DateTime.Now;
                return _calendar.DateTime;
            }
        }
        public INTAPS.Ethiopic.DualCalendar Calendar
        {
            get { return _calendar; }
            set
            {
                if (_calendar == value)
                    return;
                if (_calendar != null)
                    _calendar.DateTimeChanged -= new EventHandler(DateChanged);
                _calendar = value;
                _calendar.DateTimeChanged += new EventHandler(DateChanged);
            }
        }



        public BankAccountPlaceholderNative BankAccounts
        {
            get { return _bankAccounts; }
            set
            {
                if (_bankAccounts == value)
                    return;
                if (_bankAccounts != null)
                    _bankAccounts.SelectedIndexChanged -= new EventHandler(BankAccountSelectionIndexChanged);
                _bankAccounts = value;
                _bankAccounts.SelectedIndexChanged += new EventHandler(BankAccountSelectionIndexChanged);
            }
        }



        public Label LabelBankBalance
        {
            get { return _labelBankBalance; }
            set { _labelBankBalance = value; }
        }

       
        public System.Windows.Forms.Panel LayoutBankAccounts
        {
            get { return _layoutBankAccounts; }
            set { _layoutBankAccounts = value; }
        }

        public CashAccountPlaceholderNative CashAccounts
        {
            get { return _cashAccounts; }
            set
            {
                if (_cashAccounts == value)
                    return;
                if (_cashAccounts != null)
                    _cashAccounts.SelectedIndexChanged -= new EventHandler(CashAccountChanged);
                _cashAccounts = value;
                _cashAccounts.SelectedIndexChanged += new EventHandler(CashAccountChanged);
            }
        }


        public Label LableCashBalance
        {
            get { return _lableCashBalance; }
            set { _lableCashBalance = value; }
        }

       

        public System.Windows.Forms.Panel LayoutCashAccounts
        {
            get { return _layoutCashAccounts; }
            set { _layoutCashAccounts = value; }
        }

        public PaymentTypeSelectorNative PaymentTypeSelector
        {
            get { return _paymentTypeSelector; }
            set
            {
                if (_paymentTypeSelector == value)
                    return;
                if (_paymentTypeSelector != null)
                    _paymentTypeSelector.SelectedIndexChanged -= new EventHandler(CashAccountChanged);
                _paymentTypeSelector = value;
                _paymentTypeSelector.SelectedIndexChanged += new EventHandler(PaymentTypeChanged);
            }
        }

        
        public string PaymentSource
        {
            get
            {
                if (_paymentTypeSelector == null)
                    return "";
                return " by " + PaymentMethodHelper.GetPaymentTypeText(_paymentTypeSelector.PaymentMethod);

            }
        }

        public int assetAccountID
        {
            get
            {
                if(_paymentTypeSelector==null)
                    return -1;
                if (PaymentMethodHelper.IsPaymentMethodCash(_paymentTypeSelector.PaymentMethod))
                    if (_cashAccounts != null)
                        return _cashAccounts.cashCsAccountID;
                if (PaymentMethodHelper.IsPaymentMethodBank(_paymentTypeSelector.PaymentMethod))
                    if (_bankAccounts!= null)
                        return _bankAccounts.mainCsAccountID;
                return -1;

            }
            set
            {
                if (_paymentTypeSelector == null)
                    return;
                if (PaymentMethodHelper.IsPaymentMethodCash(_paymentTypeSelector.PaymentMethod))
                {
                    if (_cashAccounts != null)
                    {
                        _cashAccounts.cashCsAccountID = value;
                    }
                    return;
                }
                if (PaymentMethodHelper.IsPaymentMethodBank(_paymentTypeSelector.PaymentMethod))
                    if (_bankAccounts != null)
                        _bankAccounts.mainCsAccountID = value;
                return;
            }
        }
        
        public double assetBalance()
        {
            int ac = assetAccountID;
            if (ac == -1)
                return 0;
            DateTime time = _calendar == null ? DateTime.Now : _calendar.DateTime;
            return AccountingClient.GetNetBalanceAsOf(assetAccountID, time);
        }
        public bool validatePaymentDocuemntReference(string chq,string cpo,string tran)
        {
            if (PaymentMethodHelper.IsPaymentMethodWithReference(this.PaymentTypeSelector.PaymentMethod))
            {
                if (string.IsNullOrEmpty(PaymentMethodHelper.selectReference(this.PaymentTypeSelector.PaymentMethod,chq,cpo,tran)))
                {
                    MessageBox.ShowErrorMessage("Please enter Check No, CPO No or Transfer Reference");
                    return false;
                }
            }
            return true;
        }
        internal bool validateAssetAcountSelected()
        {
            if (this.assetAccountID == -1)
            {
                MessageBox.ShowErrorMessage("Please select a cash or bank account");
                return false;
            }
            return true;
        }

        public int costCenterID
        {
            get
            {
                return _costCenterPlaceHolder == null ? CostCenter.ROOT_COST_CENTER : _costCenterPlaceHolder.GetAccountID();
            }
        }
        internal double getBalance(Label labelStaffPaymentBal)
        {
            foreach (AccountLabelPairBaseNative pair in _watchAccounts)
            {
                if (pair.labelControl == labelStaffPaymentBal)
                    return pair.balance;
            }
            return 0;
        }
        internal double getCsAccountBalance(int csAccountID)
        {
            foreach (AccountLabelPairBaseNative pair in _watchAccounts)
            {
                if (pair is CsAccountLabelPairNative)
                {
                    if (pair.accountID== csAccountID)
                        return pair.balance;
                }
            }
            return 0;
        }
        internal double getAccountBalance(int accountID)
        {
            foreach (AccountLabelPairBaseNative pair in _watchAccounts)
            {
                if (pair is AccountLabelPairNative)
                {
                    if (pair.accountID == accountID)
                        return pair.balance;
                }
            }
            return 0;
        }
        public BizNetPaymentMethod paymentMethod
        {
            get
            {
                if (this.PaymentTypeSelector == null)
                    return BizNetPaymentMethod.Cash;
                return this.PaymentTypeSelector.PaymentMethod;
            }
        }
       
        
        


    }
}
