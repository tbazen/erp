﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Utils;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    partial class PaymentMethodAndBalanceControllerNative
    {
        bool _ignoreEvents=false;
        public void IgnoreEvents()
        {
            _ignoreEvents = true;
        }
        public void AcceptEvents()
        {
            _ignoreEvents = false;
            updateAssetBalance();
            updateAccountLabelPairBalances();
            showHideLayouts();
        }
        void CostCenterChanged(object sender, EventArgs e)
        {
            foreach (AccountLabelPairBaseNative i in _watchAccounts)
            {
                AccountLabelPairNative pair = i as AccountLabelPairNative;
                if (i == null)
                    continue;
                pair.Update(this.dateTime, this.costCenterID);
            }
            onPaymentInformationChanged(_costCenterPlaceHolder);
        }
        void BankAccountSelectionIndexChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents)
                return;
            updateAssetBalance();
            onPaymentInformationChanged(_bankAccounts);
        }
        void CashAccountChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents)
                return;
            updateAssetBalance();
            onPaymentInformationChanged(_cashAccounts);
        }
        void DateChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents)
                return;
            updateAssetBalance();
            updateAccountLabelPairBalances();
            onPaymentInformationChanged(_calendar);
        }

        private void updateAccountLabelPairBalances()
        {
            foreach (AccountLabelPairBaseNative i in _watchAccounts)
            {
                if (i is AccountLabelPairNative)
                {
                    ((AccountLabelPairNative)(i)).Update(this.dateTime, this.costCenterID);
                }
                else
                {
                    ((CsAccountLabelPairNative)(i)).Update(this.dateTime);
                }
            }

        }

        private void updateAssetBalance()
        {

            if (PaymentMethodHelper.IsPaymentMethodBank(_paymentTypeSelector.PaymentMethod))
            {
                if (_bankAccounts == null)
                    return;
                if (_bankAccounts.bankAccount == null)
                    _labelBankBalance.Text = "Select a bank account";
                else
                    _labelBankBalance.Text = AccountBase.FormatBalance(AccountingClient.GetNetBalanceAsOf(_bankAccounts.bankAccount.mainCsAccount, this.dateTime));
            }
            else if (PaymentMethodHelper.IsPaymentMethodCash(_paymentTypeSelector.PaymentMethod))
            {
                if (_cashAccounts == null)
                    return;
                if (_cashAccounts.cashCsAccountID == -1)
                    _lableCashBalance.Text = "Select a cash account";
                else
                    _lableCashBalance.Text = AccountBase.FormatBalance(AccountingClient.GetNetBalanceAsOf(_cashAccounts.cashCsAccountID, this.dateTime));
            }
        }
        private void updateReferenceLabel()
        {

            if (PaymentMethodHelper.IsPaymentMethodWithReference(_paymentTypeSelector.PaymentMethod))
            {

                if (this._labelRefName== null)
                    return;
                this._labelRefName.Text = PaymentMethodHelper.GetPaymentTypeReferenceName(_paymentTypeSelector.PaymentMethod);
            }
        }

        void PaymentTypeChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents)
                return;
            showHideLayouts();
            updateReferenceLabel();
            updateAssetBalance();
            onPaymentInformationChanged(_paymentTypeSelector);
        }

        private void showHideLayouts()
        {
            if (_paymentTypeSelector == null)
                return;
            BizNetPaymentMethod method = _paymentTypeSelector.PaymentMethod;
            if (_layoutBankAccounts!= null)
                _layoutBankAccounts.Visible = PaymentMethodHelper.IsPaymentMethodBank(method) ;
            
            if(_layoutCashAccounts != null)
                _layoutCashAccounts.Visible = PaymentMethodHelper.IsPaymentMethodCash(method);
            
            if(_layoutServiceCharge!=null)
                _layoutServiceCharge.Visible = PaymentMethodHelper.IsPaymentMethodWithServiceCharge(method);
            
           
            if(_layoutReference!=null)
                _layoutReference.Visible = PaymentMethodHelper.IsPaymentMethodWithReference(method);
            
            if(_layoutReference!=null)
                _layoutReference.Text = PaymentMethodHelper.GetPaymentTypeReferenceName(method) + ": ";
        }

    }
}
