﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Data;
using DevExpress.XtraGrid.Views.Base;

namespace BIZNET.iERP.Client
{
    interface IItemPickerPopup
    {
        event EventHandler ItemSelected;
        void selectTopItem();
    }
    abstract class PopupController<PickerType> where PickerType:Form,IItemPickerPopup,new()
    {
        protected DevExpress.XtraGrid.GridControl _grid;
        protected DevExpress.XtraGrid.Views.Grid.GridView _gridView;
        PickerType _popup = null;
        DevExpress.XtraGrid.Columns.GridColumn trackCol = null;
        DataRow trackRow = null;
        public PickerType Popup
        {
            get { return _popup; }
        }
        public PopupController(DevExpress.XtraGrid.GridControl grid, DevExpress.XtraGrid.Views.Grid.GridView gridView)
        {
            _grid = grid;
            _gridView = gridView;
            _gridView.CellValueChanging += new CellValueChangedEventHandler(gridView_CellValueChanging);
            _gridView.CellValueChanged += new CellValueChangedEventHandler(_gridView_CellValueChanged);
        }

        void _gridView_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (isColumnSearchable(e.Column))
            {
                if (popupShown)
                {
                    _popup.selectTopItem();
                }
            }
        }

        bool popupShown
        {
            get
            {
                return _popup != null && _popup.Visible;
            }
        }
        
        void trackPopup(DataRow row, DevExpress.XtraGrid.Columns.GridColumn col, string code, Point p)
        {
            if (!isColumnSearchable(col))
                return;
            
            if (_popup == null)
            {
                _popup = new PickerType();
                _popup.ItemSelected += new EventHandler(_popup_ItemSelected);
            }
            if (!popupShown)
            {
                _popup.Show();
                _popup.Location = p;
            }
            trackCol = col;
            trackRow = row;
            setSearch(row,col, code);
        }
        protected abstract bool isColumnSearchable(DevExpress.XtraGrid.Columns.GridColumn col);
        protected abstract void setSearch(DataRow row, DevExpress.XtraGrid.Columns.GridColumn col, string value);
        protected abstract void itemSelected(DataRow row,DevExpress.XtraGrid.Columns.GridColumn col);
        void _popup_ItemSelected(object sender, EventArgs e)
        {
            _gridView.HideEditor();
            _popup.Hide();
            itemSelected(trackRow, trackCol);
        }

        private void gridView_CellValueChanging(object sender, CellValueChangedEventArgs e)
        {
            DevExpress.XtraEditors.BaseEdit be = _gridView.ActiveEditor;
            if (be == null)
                return;
            Point p = be.PointToScreen(new Point(2, be.Height + 5));
            DataRow editRow = _gridView.GetDataRow(e.RowHandle);
            if (editRow == null)
                return;
            trackPopup(editRow,e.Column, e.Value.ToString(), p);
        }

    }
}
