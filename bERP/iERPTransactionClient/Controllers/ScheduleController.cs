﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Windows.Forms;

namespace BIZNET.iERP.Client
{
    public delegate ActivationParameter ScheduleControllerBeforeActivationHandler(ScheduleController source);
    public class ScheduleController
    {
        public event ScheduleControllerBeforeActivationHandler BeforeActivation;
        SimpleButton _buttonScheduleSettlement;
        LabelControl _labelSchedule;
        int []_docTypes;
        string _buttonText;
        ActivationParameter _par=null;

        public ScheduleController(SimpleButton button, LabelControl label,int[] docTypes)
        {
            initialize(button, label, docTypes);
        }

        private void initialize(SimpleButton button, LabelControl label, int[] docTypes)
        {
            _buttonScheduleSettlement = button;
            _labelSchedule = label;
            _docTypes = docTypes;
            _buttonScheduleSettlement.Click += new EventHandler(_buttonScheduleSettlement_Click);
            _par = new ActivationParameter();
            if(_labelSchedule!=null)
                _labelSchedule.Text = "No schedule";
            _buttonText = button.Text;
        }

        public ScheduleController(SimpleButton button, LabelControl label, params Type[] docTypes)
        {
            int[] docTypeIDS = new int[docTypes.Length];
            for (int i = 0; i < docTypes.Length; i++)
                docTypeIDS[i] = AccountingClient.GetDocumentTypeByType(docTypes[i]).id;
            initialize(button, label, docTypeIDS);
        }

        void SetActivationParameter(ActivationParameter par)
        {
            _par = par;
        }
        void _buttonScheduleSettlement_Click(object sender, EventArgs e)
        {
            AccountDocument[] schedule = _buttonScheduleSettlement.Tag as AccountDocument[];
            if (BeforeActivation != null)
                _par = BeforeActivation(this);
            DocumentScheduler doc = new DocumentScheduler(schedule,_docTypes, -1, false, false, 0, 0, _par);

            if (doc.ShowDialog(_buttonScheduleSettlement.FindForm()) == DialogResult.OK)
            {
                setSchedule(doc.scheduledDocs.ToArray());
            }
        }
        public AccountDocument[] getScheduledDocuments()
        {
            return _buttonScheduleSettlement.Tag as AccountDocument[];
        }
        public List<T> getScheduledDocumentsByType<T>() where T:AccountDocument
        {
            List<T> ret = new List<T>();
            if (_buttonScheduleSettlement.Tag == null)
                return ret;
            foreach (AccountDocument doc in _buttonScheduleSettlement.Tag as AccountDocument[])
            {
                T t = doc as T;
                if (t != null)
                    ret.Add(t);
            }
            return ret;
        }
        private void setSchedule(AccountDocument[] scheduledSettlement)
        {
            if (scheduledSettlement == null || scheduledSettlement.Length == 0)
            {
                if (_labelSchedule == null)
                    _buttonScheduleSettlement.Text =_buttonText;
                else
                    _labelSchedule.Text = "";
                    

                _buttonScheduleSettlement.Tag = null;
            }
            else
            {
                if (_labelSchedule == null)
                    _buttonScheduleSettlement.Text = _buttonText + " (" + scheduledSettlement.Length + ")";
                else
                    _labelSchedule.Text = scheduledSettlement.Length + " documents(s) scheduled";
                _buttonScheduleSettlement.Tag = scheduledSettlement;
            }
        }
        public bool visible
        {
            get
            {
                return _buttonScheduleSettlement.Visible;
            }
            set
            {
                _buttonScheduleSettlement.Visible = value;
                if (_labelSchedule != null)
                    _labelSchedule.Visible = value;
            }
        }
        public bool enabled
        {
            get
            {
                return _buttonScheduleSettlement.Enabled;
            }
            set
            {
                _buttonScheduleSettlement.Enabled = value;
                if (_labelSchedule != null)
                    _labelSchedule.Enabled = value;
            }
        }



        internal void setScheduledDocuments(AccountDocument[] accountDocument)
        {
            setSchedule(accountDocument);
        }
        internal void setScheduledDocuments(int [] accountDocumentIDs)
        {
            AccountDocument[] accountDocument = new AccountDocument[accountDocumentIDs.Length];
            for (int i = 0; i < accountDocumentIDs.Length; i++)
                accountDocument[i]=AccountingClient.GetAccountDocument(accountDocumentIDs[i], true);
            setSchedule(accountDocument);
        }
    }
}
