﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    public class SettleFullAmountController
    {
        public event EventHandler ValueChanged;
        TextEdit _text;
        CheckEdit _check;
        double _fullAmount = 0;
        bool _ignoreEvent = false;
        public double FullAmount
        {
            get { return _fullAmount; }
            set
            {
                if (AccountBase.AmountEqual(_fullAmount, value))
                    return;
                _fullAmount = value;
                if (_check.Checked)
                {
                    _ignoreEvent=true;
                    try
                    {
                        _text.Text = TSConstants.FormatBirr(_fullAmount);
                    }
                    finally
                    {
                        _ignoreEvent = false;
                    }

                }
            }
        }
        public double effectiveAmount
        {
            get
            {
                if (_check.Checked)
                    return _fullAmount;
                double val;
                if (!double.TryParse(_text.Text, out val))
                    return 0;
                return val;
            }
            set
            {
                _text.Text = TSConstants.FormatBirr(value);
            }
        }
        public SettleFullAmountController(TextEdit text, CheckEdit check)
        {
            _text = text;
            _check = check;
            if(_check!=null)
                _check.CheckedChanged += new EventHandler(_check_CheckedChanged);
            if(text!=null)
                text.TextChanged += new EventHandler(text_TextChanged);
        }

        void text_TextChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            onValueChanged();
        }

        void _check_CheckedChanged(object sender, EventArgs e)
        {
            if (_check.Checked)
            {
                _text.Text = TSConstants.FormatBirr(_fullAmount);
                _text.Enabled = false;
                onValueChanged();
            }
            else
            {
                _text.Text = _fullAmount.ToString(); ;
                _text.Enabled = true;
            }
            
        }

        private void onValueChanged()
        {
            if (ValueChanged != null)
                ValueChanged(this, null);
        }

        public bool Enabled
        {
            get { return _check.Enabled; }
            set
            {
                _check.Enabled = value;
                _text.Enabled = value && !_check.Checked;
            }
        }

        internal bool Validate()
        {
            if(_check.Checked)
                return true;
            if(AccountBase.AmountGreater(effectiveAmount,_fullAmount))
            {
                MessageBox.ShowErrorMessage("Amount can't be more than "+TSConstants.FormatBirr( _fullAmount));
                return false;
            }
            return true;
        }

        public bool hasPositiveValue {
            get 
            {
                return AccountBase.AmountGreater(effectiveAmount, 0);
            }
        }
        public bool hasNoneZeroValue
        {
            get
            {
                return !AccountBase.AmountEqual(effectiveAmount, 0);
            }
        }

    }
}
