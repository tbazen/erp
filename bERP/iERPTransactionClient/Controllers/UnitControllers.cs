﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class UnitControllerDXGridComboColumn
    {
        DevExpress.XtraEditors.Repository.RepositoryItemComboBox _combo;
        DevExpress.XtraGrid.Columns.GridColumn _unitColumn= null;
        DevExpress.XtraGrid.Columns.GridColumn _quantiyColumn = null;
        DevExpress.XtraGrid.Columns.GridColumn _unitPriceColumn= null;
        DevExpress.XtraGrid.Columns.GridColumn _itemCodeColumn= null;
        DevExpress.XtraGrid.Views.Grid.GridView _gridView = null;
        INTAPS.CachedObject<string, UnitConvertor> _convertor;
        public UnitControllerDXGridComboColumn(
            DevExpress.XtraGrid.Views.Grid.GridView gridView,
            DevExpress.XtraGrid.Columns.GridColumn unitColumn,
            DevExpress.XtraEditors.Repository.RepositoryItemComboBox combo,
            DevExpress.XtraGrid.Columns.GridColumn itemCodeColumn,
            DevExpress.XtraGrid.Columns.GridColumn quantiyColumn,
            DevExpress.XtraGrid.Columns.GridColumn unitPriceColumn
            )
        {
            _gridView = gridView;
            _unitColumn = unitColumn;
            _combo = combo;
            _itemCodeColumn = itemCodeColumn;
            _quantiyColumn = quantiyColumn;
            _unitPriceColumn = unitPriceColumn;

            _combo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            loadUnits();
            
            _combo.SelectedIndexChanged += selectionChanged;

            if (_unitPriceColumn != null || _quantiyColumn != null)
            {
                //Auto conversion code not implemented
                _gridView.CellValueChanging += _gridView_CellValueChanging;
                _gridView.CellValueChanged+=_gridView_CellValueChanged;
                _convertor = new INTAPS.CachedObject<string, UnitConvertor>(itemCode => iERPTransactionClient.getItemUnitConvertor(itemCode));
            }

        }

        private void loadUnits()
        {
            _combo.Items.Clear();
            foreach (MeasureUnit u in iERPTransactionClient.GetMeasureUnits())
            {
                _combo.Items.Add(u);
            }
        }
        public void reloadUnits()
        {
            loadUnits();
        }
        MeasureUnit oldUnit= null;
        void _gridView_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column != _unitColumn)
                return;

            oldUnit = _gridView.GetRowCellValue(e.RowHandle, _unitColumn) as MeasureUnit;
            
        }

        void _gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            MeasureUnit unit = oldUnit;
            oldUnit = null;
            if (e.Column != _unitColumn)
                return;
            
            if (unit == null)
                return;
            MeasureUnit newUnit = e.Value as MeasureUnit;
            if (unit == null || newUnit == null)
                return;
            string itemCode = _gridView.GetRowCellValue(e.RowHandle, _itemCodeColumn) as string;
            TransactionItems item = iERPTransactionClient.GetTransactionItems(itemCode);
            if (item == null)
                return;
            UnitConvertor conv = _convertor[itemCode.ToUpper()];
            if (_quantiyColumn != null)
            {
                object orginalValue = _gridView.GetRowCellValue(e.RowHandle, _quantiyColumn);
                if (orginalValue is double)
                {
                    if (!conv.canConvert(unit.ID, newUnit.ID))
                    {
                        _gridView.SetRowCellValue(e.RowHandle, _quantiyColumn, 0);
                    }
                    else
                        _gridView.SetRowCellValue(e.RowHandle, _quantiyColumn, conv.convert(unit.ID, newUnit.ID, (double)orginalValue));
                }
            }
            if (_unitPriceColumn!= null)
            {
                object orginalValue = _gridView.GetRowCellValue(e.RowHandle, _unitPriceColumn);
                if (orginalValue is double)
                {
                    if (!conv.canConvert(newUnit.ID,unit.ID))
                    {
                        _gridView.SetRowCellValue(e.RowHandle, _unitPriceColumn, 0);
                    }
                    else
                        _gridView.SetRowCellValue(e.RowHandle, _unitPriceColumn, conv.convert(newUnit.ID, unit.ID, (double)orginalValue));
                }
            }

        }

        void selectionChanged(object sender, EventArgs e)
        {
            
        }
        public void bindQuantityColumn(DevExpress.XtraGrid.Columns.GridColumn quantiyColumn)
        {
            _quantiyColumn = quantiyColumn;
        }
    }
}
