﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIZNET.iERP.Client
{
    class AEToggleButton:DevExpress.XtraEditors.CheckButton
    {
        System.Drawing.Font selectedFont, unselectedFont;
        public AEToggleButton()
        {
            selectedFont = new System.Drawing.Font("Aria", 10, System.Drawing.FontStyle.Bold);
            unselectedFont = new System.Drawing.Font("Aria", 8, System.Drawing.FontStyle.Regular);
            this.Appearance.Font = unselectedFont;
            this.Appearance.ForeColor = System.Drawing.Color.Black;
        }
        protected override void RaiseCheckedChanged()
        {
            if (this.Checked)
            {
                this.Appearance.ForeColor = System.Drawing.Color.Blue;
                this.Appearance.Font = selectedFont;
            }
            else
            {
                this.Appearance.ForeColor = System.Drawing.Color.Black;
                this.Appearance.Font = unselectedFont;
            }
            base.RaiseCheckedChanged();
        }
    }
}
