﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class AttachmentPlaceHolder : UserControl
    {
        int documentID=-1;
        public AttachmentPlaceHolder()
        {
            InitializeComponent();
            setDocument(-1);
        }
        public void setDocument(int documentID)
        {
            this.documentID = documentID;
            buttonAddAttachment.Enabled = AccountingClient.GetAccountDocument(this.documentID,false)!=null;
            loadAttachments();
        }
        private void buttonAddAttachment_Click(object sender, EventArgs e)
        {
            VisualBrowser vb = new VisualBrowser(documentID,-1, VisualBrowser.VisualBrowserMode.Create);
            if(vb.ShowDialog(this.ParentForm)==DialogResult.OK)
            {
                loadAttachments();
            }
            
        }

        private void loadAttachments()
        {
            tree.Nodes.Clear();
            if (documentID == -1)
                return;
            AttachedFileGroup[] attachments = iERPTransactionClient.getAttachedFileGroupForDocument(documentID);
            if(attachments.Length>0)
            {
                if(attachments.Length==1)
                {
                    TreeNode node = new TreeNode(attachments[0].title);
                    node.Tag = null;
                    tree.Nodes.Add(node);
                }
                else
                {
                    TreeNode node = new TreeNode(attachments.Length+" attachments");
                    node.Tag = null;
                    tree.Nodes.Add(node);
                    foreach(AttachedFileGroup g in attachments)
                    {
                        TreeNode chld= new TreeNode(g.title);
                        chld.Tag = g;
                        node.Nodes.Add(chld);
                    }
                    node.Expand();
                }
            }
        }

        private void tree_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            
        }

        internal void setDocument(AccountDocument doc)
        {
            if (doc == null)
                setDocument(-1);
            else
                setDocument(doc.AccountDocumentID);
        }

        private void tree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Tag is AttachedFileGroup)
            {
                VisualBrowser vb = new VisualBrowser(documentID, ((AttachedFileGroup)e.Node.Tag).id, VisualBrowser.VisualBrowserMode.View);
                vb.Show(this.ParentForm);
            }
            else
            {
                VisualBrowser vb = new VisualBrowser(documentID, -1, VisualBrowser.VisualBrowserMode.View);
                vb.Show(this.ParentForm);

            }
        }
    }
}
