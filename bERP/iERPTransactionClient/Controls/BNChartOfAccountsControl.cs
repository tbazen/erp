using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public class BNChartOfAccountsControl:INTAPS.UI.Windows.ObjectTreeSimpleTyped<Account>
    {

        protected override Account[] GetObjectChilds(Account obj)
        {
            int N;
            return INTAPS.Accounting.Client.AccountingClient.GetChildAcccount<Account>(obj==null?-1:obj.id,0,-1,out N);
        }

        protected override int GetObjectID(Account obj)
        {
            return obj == null ? -1 : obj.id;
        }

        protected override int GetObjectPID(Account obj)
        {
            return obj.id;
        }

        protected override string GetObjectString(Account obj)
        {
            return obj.NameCode;
        }
    }
}
