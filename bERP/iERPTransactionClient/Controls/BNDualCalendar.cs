﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BIZNET.iERP.Client
{
    public partial class BNDualCalendar : DevExpress.XtraEditors.XtraUserControl
    {
        INTAPS.Ethiopic.DualCalendarController _cont;
        public BNDualCalendar()
        {
            InitializeComponent();
            _cont = new INTAPS.Ethiopic.DualCalendarController(this);
            _cont.DateTimeChanged += new EventHandler(_cont_DateTimeChanged);
        }

        void _cont_DateTimeChanged(object sender, EventArgs e)
        {
            if (DateTimeChanged != null)
                DateTimeChanged(this, e);
        }
        public event EventHandler DateTimeChanged;
        
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public DateTime DateTime { get { return _cont.DateTime; } set { _cont.DateTime = value; } }

        public bool ShowEthiopian { get { return _cont.ShowEthiopian; } set { _cont.ShowEthiopian = value; } }
        public bool ShowGregorian { get { return _cont.ShowGregorian; } set { _cont.ShowGregorian = value; } }
        public bool ShowTime { get { return _cont.ShowTime; } set { _cont.ShowTime = value; } }
        public bool VerticalLayout { get { return _cont.VerticalLayout; } set { _cont.VerticalLayout = value; } }
    }
}
