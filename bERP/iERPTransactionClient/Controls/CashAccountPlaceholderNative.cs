﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.Accounting.Client;
using System.ComponentModel;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;

namespace BIZNET.iERP.Client
{
    public class CashAccountPlaceholderNative : System.Windows.Forms.ComboBox,IEnumerator<CashAccount>,IEnumerable<CashAccount>
    {
        bool _includeExpenseAdvance = true;

        public bool IncludeExpenseAdvance
        {
            get { return _includeExpenseAdvance; }
            set {

                if (_includeExpenseAdvance != value)
                {
                    _includeExpenseAdvance = value;
                    loadItems();
                }
            }
        }

        CashAccount[] m_cashOnHandAccounts;
        void loadItems()
        {
            this.Items.Clear();
            foreach (CashAccount c in m_cashOnHandAccounts = iERPTransactionClient.GetAllCashAccounts(_includeExpenseAdvance))
                this.Items.Add(c.name);
            if (this.Items.Count > 0)
                this.SelectedIndex = 0;
        }
        public CashAccountPlaceholderNative()
        {
            base.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            if(AccountingClient.IsConnected)
                loadItems();
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int cashCsAccountID
        {
            get
            {
                if (this.SelectedIndex == -1)
                    return -1;
                return m_cashOnHandAccounts[this.SelectedIndex].csAccountID;
            }
            set
            {
                if (value == cashCsAccountID)
                    return;
                int i = 0;
                foreach (CashAccount ac in m_cashOnHandAccounts)
                {
                    if (ac.csAccountID == value)
                    {
                        this.SelectedIndex = i;
                        return;
                    }
                    i++;
                }
                this.SelectedIndex = -1;
            }
        }
        int index = -1;
        public CashAccount Current
        {
            get
            {
                if (index == -1) return null;
                return m_cashOnHandAccounts[index];
            }
        }

        object System.Collections.IEnumerator.Current
        {
            get { return this.Current; }
        }

        public bool MoveNext()
        {
            index++;
            return index < m_cashOnHandAccounts.Length;
        }

        public IEnumerator<CashAccount> GetEnumerator()
        {
            index = -1;
            return this;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this;
        }

        public void Reset()
        {
            index = -1;
        }
    }
    public class BankAccountPlaceholderNative : System.Windows.Forms.ComboBox
    {
        BankAccountInfo[] _bankAccounts;
        void loadItems()
        {
            foreach (BankAccountInfo c in _bankAccounts = iERPTransactionClient.GetAllBankAccounts())
                this.Items.Add(c.ToString());
            if (this.Items.Count > 0)
                this.SelectedIndex = 0;
        }
        public BankAccountPlaceholderNative()
        {
            this.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            if (AccountingClient.IsConnected)
                loadItems();
        }
        
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public BankAccountInfo bankAccount
        {
            get
            {
                if (this.SelectedIndex == -1)
                    return null;
                return _bankAccounts[this.SelectedIndex];
            }
            set
            {
                if (value == null)
                    this.mainCsAccountID = -1;
                else
                    this.mainCsAccountID = value.mainCsAccount;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int mainCsAccountID
        {
            get
            {
                if (this.SelectedIndex == -1)
                    return -1;
                return _bankAccounts[this.SelectedIndex].mainCsAccount;
            }
            set
            {
                int i = 0;
                foreach (BankAccountInfo ac in _bankAccounts)
                {
                    if (ac.mainCsAccount == value)
                    {
                        this.SelectedIndex = i;
                        return;
                    }
                    i++;
                }
                this.SelectedIndex = -1;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int bankServiceChargeAccountID
        {
            get
            {
                if (this.SelectedIndex == -1)
                    return -1;
                return _bankAccounts[this.SelectedIndex].bankServiceChargeCsAccountID;
            }
            set
            {
                int i = 0;
                foreach (BankAccountInfo ac in _bankAccounts)
                {
                    if (ac.bankServiceChargeCsAccountID == value)
                    {
                        this.SelectedIndex = i;
                        return;
                    }
                    i++;
                }
                this.SelectedIndex = -1;
            }
        }

    }
}

