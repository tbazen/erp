﻿namespace BIZNET.iERP.Client
{
    partial class DocumentSerialPlaceHolder
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonTypes = new DevExpress.XtraEditors.SimpleButton();
            this.textRefence = new DevExpress.XtraEditors.TextEdit();
            this.linkOtherDocuments = new System.Windows.Forms.LinkLabel();
            this.checkPrimary = new DevExpress.XtraEditors.CheckEdit();
            this.contextTypes = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.textRefence.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPrimary.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonTypes
            // 
            this.buttonTypes.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.buttonTypes.Appearance.Options.UseBackColor = true;
            this.buttonTypes.Appearance.Options.UseTextOptions = true;
            this.buttonTypes.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.buttonTypes.Location = new System.Drawing.Point(3, 3);
            this.buttonTypes.Name = "buttonTypes";
            this.buttonTypes.Size = new System.Drawing.Size(180, 22);
            this.buttonTypes.TabIndex = 0;
            this.buttonTypes.Text = "simpleButton1";
            this.buttonTypes.Click += new System.EventHandler(this.buttonTypes_Click);
            // 
            // textRefence
            // 
            this.textRefence.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textRefence.Location = new System.Drawing.Point(189, 5);
            this.textRefence.Name = "textRefence";
            this.textRefence.Size = new System.Drawing.Size(196, 20);
            this.textRefence.TabIndex = 2;
            this.textRefence.EditValueChanged += new System.EventHandler(this.textRefence_EditValueChanged);
            this.textRefence.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textRefence_KeyDown);
            this.textRefence.Leave += new System.EventHandler(this.textRefence_Leave);
            // 
            // linkOtherDocuments
            // 
            this.linkOtherDocuments.AutoSize = true;
            this.linkOtherDocuments.Location = new System.Drawing.Point(186, 26);
            this.linkOtherDocuments.Name = "linkOtherDocuments";
            this.linkOtherDocuments.Size = new System.Drawing.Size(53, 13);
            this.linkOtherDocuments.TabIndex = 3;
            this.linkOtherDocuments.TabStop = true;
            this.linkOtherDocuments.Text = "linkLabel1";
            this.linkOtherDocuments.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkOtherDocuments_LinkClicked);
            // 
            // checkPrimary
            // 
            this.checkPrimary.Location = new System.Drawing.Point(3, 26);
            this.checkPrimary.Name = "checkPrimary";
            this.checkPrimary.Properties.Caption = "Primary";
            this.checkPrimary.Size = new System.Drawing.Size(67, 19);
            this.checkPrimary.TabIndex = 4;
            // 
            // contextTypes
            // 
            this.contextTypes.Name = "contextTypes";
            this.contextTypes.Size = new System.Drawing.Size(61, 4);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonTypes);
            this.panel1.Controls.Add(this.linkOtherDocuments);
            this.panel1.Controls.Add(this.checkPrimary);
            this.panel1.Controls.Add(this.textRefence);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(398, 48);
            this.panel1.TabIndex = 6;
            // 
            // DocumentSerialPlaceHolder
            // 
            this.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel1);
            this.Name = "DocumentSerialPlaceHolder";
            this.Size = new System.Drawing.Size(398, 131);
            ((System.ComponentModel.ISupportInitialize)(this.textRefence.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPrimary.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton buttonTypes;
        private System.Windows.Forms.ContextMenuStrip contextTypes;
        private DevExpress.XtraEditors.TextEdit textRefence;
        private System.Windows.Forms.LinkLabel linkOtherDocuments;
        private DevExpress.XtraEditors.CheckEdit checkPrimary;
        private System.Windows.Forms.Panel panel1;

    }
}
