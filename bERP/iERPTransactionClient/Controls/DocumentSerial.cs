﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class DocumentSerialPlaceHolder : DevExpress.XtraEditors.XtraUserControl
    {
        const int OTHER_REF_WIDTH = 90;
        const int OTHER_REF_HEIGHT = 20;
        DocumentSerialType _currentType = null;
        List<DocumentSerialType> _types=null;
        List<Control> otherReferences = new List<Control>();
        int _docID = -1;
        public DocumentSerialPlaceHolder()
        {

            InitializeComponent();
            _ignoreEvents = true;
            setKeys();
            this.setReference(-1, null);
            Label lc = new Label();
            lc.Text = "Other Refs.:";
            lc.AutoSize = false;
            lc.Width = OTHER_REF_WIDTH;
            lc.Height = OTHER_REF_HEIGHT;
            otherReferences.Add(lc);
            this.Controls.Add(lc);
            _ignoreEvents = false;
        }
        public void setKeys(Type type, string field)
        {
            object [] atr= type.GetField(field).GetCustomAttributes(typeof(DocumentTypedReferenceFieldAttribute),true);
            if(atr==null || atr.Length==0)
                throw new INTAPS.ClientServer.ServerUserMessage("DocumentTypedReferenceFieldAttribute not set for field "+field+" of "+type);
            DocumentTypedReferenceFieldAttribute da = atr[0] as DocumentTypedReferenceFieldAttribute;
            setKeys(da.types);
        }
        public void setKeys(params string[] keys)
        {
            _types=new List<INTAPS.Accounting.DocumentSerialType>();
            string serialsList = "";
            foreach(string k in keys)
            {
                if (serialsList == "")
                    serialsList = k;
                else
                    serialsList += "," + k;
                DocumentSerialType type = AccountingClient.getDocumentSerialType(k);
                if(type==null)
                    continue;
                _types.Add(type);
            }
            buttonTypes.Enabled = _types.Count>1;
            textRefence.Enabled = _types.Count > 0;
            if (_types.Count == 0)
            {
                buttonTypes.Text = "Configure document serials:" + serialsList;
            }
            else
            {
                setType(_types[0]);
                foreach(ToolStripItem item in contextTypes.Items)
                    item.Click -= new EventHandler(item_Click);                    
                foreach (DocumentSerialType t in _types)
                {
                    ToolStripItem item = contextTypes.Items.Add(t.name);
                    item.Tag = t;
                    item.Click += new EventHandler(item_Click);                    
                }
            }
        }

        private void setType(DocumentSerialType documentSerialType)
        {
            _currentType = documentSerialType;
            if (documentSerialType == null)
            {
                if (_types.Count != 1)
                    buttonTypes.Text = "Select Type";
            }
            else
                buttonTypes.Text = documentSerialType.name;
        }
        public DocumentTypedReference getReference()
        {

            if (_currentType == null)
                return null;
            if (string.IsNullOrEmpty(textRefence.Text))
                return null;
            DocumentTypedReference ret=new DocumentTypedReference(_currentType.id, textRefence.Text,checkPrimary.Checked);
            string r;
            if (_currentType.serialType == DocumentSerializationMode.Serialized)
            {
                int ser = ret.serial;
                if (ser == -1)
                    r = "";
                else
                    r = AccountingClient.formatSerialNo(_currentType.id,ser);
                ret.reference=r;
            }
            return ret;
        }
        bool _ignoreEvents = false;
        public void setReference(int docID,DocumentTypedReference reference)
        {
            _ignoreEvents = true;
            try
            {
                _docID = docID;
                updateOtherLink(reference);
                if (reference == null)
                {
                    textRefence.Text = "";
                    return;
                }
                checkPrimary.Checked = reference.primary;
                DocumentSerialType exists = null;
                foreach (DocumentSerialType t in _types)
                {
                    if (t.id == reference.typeID)
                    {
                        exists = t;
                        break;
                    }
                }
                if (exists == null)
                {
                    setType(AccountingClient.getDocumentSerialType(reference.typeID));
                }
                else
                {
                    setType(exists);
                }
                textRefence.Text = reference.reference;
                for (int i = otherReferences.Count - 1; i > 0; i--)
                {
                    this.Controls.Remove(otherReferences[i]);
                    otherReferences.Remove(otherReferences[i]);
                }
                if (docID > 0)
                {
                    foreach (DocumentSerial ds in INTAPS.Accounting.Client.AccountingClient.getDocumentSerials(docID))
                    {
                        if (reference != null && ds.typeID == reference.typeID && ds.reference.Equals(reference.reference))
                            continue;
                        DocumentSerialType st = INTAPS.Accounting.Client.AccountingClient.getDocumentSerialType(ds.typeID);
                        LinkLabel ll = new LinkLabel();
                        ll.Tag = ds;
                        ll.Text = st.prefix + ":" + ds.reference;
                        ll.Tag = ds;
                        ll.Width = OTHER_REF_WIDTH;
                        ll.Height = OTHER_REF_HEIGHT;

                        ll.Click += new EventHandler(ll_Click);
                        ll.AutoSize = false;
                        ll.AutoEllipsis = true;
                        otherReferences.Add(ll);
                        this.Controls.Add(ll);
                    }
                }
                layoutOtherReferences();
            }
            finally
            {
                _ignoreEvents = false;
            }
        }
        protected override void OnLayout(LayoutEventArgs e)
        {
            base.OnLayout(e);
            if (_ignoreEvents)
                return;
            layoutOtherReferences();
        }
        void layoutOtherReferences()
        {
            int nw = this.Width / OTHER_REF_WIDTH;
            int nh = otherReferences.Count / nw + (otherReferences.Count % nw == 0 ? 0 : 1);
            for (int i = 0; i < otherReferences.Count; i++)
            {
                int x = (i % nw) * OTHER_REF_WIDTH;
                int y = i / nw * OTHER_REF_HEIGHT;
                otherReferences[i].Left = x;
                otherReferences[i].Top = (this.Height-nh*OTHER_REF_HEIGHT)+ y;
            }
            otherReferences[0].Visible = otherReferences.Count > 1;
        }
        void ll_Click(object sender, EventArgs e)
        {
            DocumentSerial ds = ((Control)sender).Tag as DocumentSerial;
            JournalExplorer je = new JournalExplorer(new DocumentTypedReference[] { new DocumentTypedReference(ds.typeID, ds.reference, ds.primaryDocument) });
            je.Show(this);

        }
        void item_Click(object sender, EventArgs e)
        {
            ToolStripItem item = sender as ToolStripItem;
            setType(item.Tag as DocumentSerialType);
            checkPrimary.Checked = false;
            updateOtherLink(getReference());
        }

        private void buttonTypes_Click(object sender, EventArgs e)
        {
            contextTypes.Show(buttonTypes.PointToScreen(new Point(0, buttonTypes.Height + 3)));
        }
        void updateOtherLink(DocumentTypedReference tref)
        {
            if (tref == null)
            {
                linkOtherDocuments.Text = "";
                linkOtherDocuments.Visible = false;
                checkPrimary.Enabled = true;
            }
            else
            {
                int[] list = AccountingClient.getDocumentListByTypedReference(tref);
                linkOtherDocuments.Visible = true;
                linkOtherDocuments.Tag = list;
                if (list.Length == 0)
                {
                    linkOtherDocuments.Text = "Click to Void";
                    linkOtherDocuments.Enabled = true;
                    checkPrimary.Checked = true;
                    checkPrimary.Enabled = false;
                }
                else
                {
                    bool found = false;
                    foreach (int d in list)
                    {
                        if (d == _docID)
                        {
                            found = true;
                            break;
                        }
                    }
                    int n = found ? list.Length - 1 : list.Length;
                    if (n == 0)
                    {
                        linkOtherDocuments.Text = "No other document";
                        checkPrimary.Checked = true;
                        checkPrimary.Enabled = false;
                    }
                    else
                    {
                        linkOtherDocuments.Text = n + " other document(s)";
                        checkPrimary.Enabled = true;
                    }
                    linkOtherDocuments.Enabled = n > 0;
                }

            }
        }
        private void textRefence_Leave(object sender, EventArgs e)
        {
            updateDisplay();
        }

        private void updateDisplay()
        {
            _ignoreEvents = true;
            try
            {
                DocumentTypedReference tref = this.getReference();
                updateOtherLink(tref);
                if (tref == null)
                    textRefence.Text = "";
                else 
                    textRefence.Text = tref.reference;
            }
            finally
            {
                _ignoreEvents = false;
            }
        }
        public override Size GetPreferredSize(Size proposedSize)
        {
            return new Size(proposedSize.Width, 100);
        }
        private void linkOtherDocuments_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            int[] docs = linkOtherDocuments.Tag as int[];
            if (docs == null)
                return;
            if (docs.Length == 0)
            {
                if (MessageBox.ShowWarningMessage("Are you sure you want to void " + _currentType.name + " " + getReference().reference) != DialogResult.Yes)
                    return;
                _ignoreEvents = true;
                try
                {
                    AccountingClient.voidSerialNo(DateTime.Now, getReference(), "");
                    textRefence.Text = "";
                    updateOtherLink(null);
                }
                catch (Exception ex)
                {

                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Failed to void", ex);
                }
                finally
                {
                    _ignoreEvents = false;
                }
            }
            else
            {
                JournalExplorer je=new JournalExplorer(new DocumentTypedReference[]{new DocumentTypedReference(_currentType.id,getReference().reference,false)});
                je.Show(this);
            }
        }

        private void textRefence_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                updateDisplay();
                textRefence.SelectAll();
            }
        }

        private void textRefence_EditValueChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents)
                return;
            checkPrimary.Checked = false;
            linkOtherDocuments.Text = "Press Enter to Check";
            linkOtherDocuments.Enabled = false;

        }
        public void setAttachments(AttachedFileGroup[] attachments)
        {

        }
    }
}
