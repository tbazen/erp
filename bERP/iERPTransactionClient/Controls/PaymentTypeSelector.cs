﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace BIZNET.iERP.Client
{
    public class PaymentTypeSelector:DevExpress.XtraEditors.ComboBoxEdit
    {
        List<BizNetPaymentMethod> _paymentMethods;
        bool _cash = true;
        bool _check = true;
        bool _bankTransferFromAccount = true;
        bool _bankTransferFromCash = true;
        bool _cpoFromBank = true;
        bool _cpoFromCash = true;
        bool _credit = false;

        void ReloadItems()
        {
            if (!INTAPS.Accounting.Client.AccountingClient.IsConnected)
                return;
            _paymentMethods=new List<BizNetPaymentMethod>();
            if (_cash)
                _paymentMethods.Add(BizNetPaymentMethod.Cash);
            if (_check)
                _paymentMethods.Add(BizNetPaymentMethod.Check);
            if(_bankTransferFromAccount)
                _paymentMethods.Add(BizNetPaymentMethod.BankTransferFromBankAccount);
            if(_bankTransferFromCash)
                _paymentMethods.Add(BizNetPaymentMethod.BankTransferFromCash);
            if (_cpoFromBank)
                _paymentMethods.Add(BizNetPaymentMethod.CPOFromBankAccount);
            if (_cpoFromCash)
                _paymentMethods.Add(BizNetPaymentMethod.CPOFromCash);
            if(_credit)
                _paymentMethods.Add(BizNetPaymentMethod.Credit);
            Properties.Items.Clear();
            foreach(BizNetPaymentMethod method in _paymentMethods)
                Properties.Items.Add(PaymentMethodHelper.GetPaymentTypeText(method));
                
        }
        public bool Include_Cash
        {
            get { return _cash; }
            set { _cash = value; ReloadItems(); }
        }

        public bool Include_Check
        {
            get { return _check; }
            set { _check = value; ReloadItems(); }
        }

        public bool Include_BankTransferFromAccount
        {
            get { return _bankTransferFromAccount; }
            set { _bankTransferFromAccount = value; ReloadItems(); }
        }

        public bool Include_BankTransferFromCash
        {
            get { return _bankTransferFromCash; }
            set { _bankTransferFromCash = value; ReloadItems(); }
        }

        public bool Include_CpoFromBank
        {
            get { return _cpoFromBank; }
            set { _cpoFromBank = value; ReloadItems(); }
        }

        public bool Include_CpoFromCash
        {
            get { return _cpoFromCash; }
            set { _cpoFromCash = value; ReloadItems(); }
        }

        public bool Include_Credit
        {
            get { return _credit; }
            set { _credit = value; ReloadItems(); }
        }
        //
        public PaymentTypeSelector()
        {
            _paymentMethods = new List<BizNetPaymentMethod>();
            base.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            ReloadItems();
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public BizNetPaymentMethod PaymentMethod
        {
            get
            {
                if (this.SelectedIndex == -1)
                    return BizNetPaymentMethod.None;
                return _paymentMethods[this.SelectedIndex];
            }
            set
            {
                int index = _paymentMethods.IndexOf(value);
                this.SelectedIndex = index;                    
            }
        }
        
    }
}
