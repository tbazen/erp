﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.Payroll.Client;
using INTAPS.Payroll;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    class PayrollSheetFormatSelector : DevExpress.XtraEditors.ComboBoxEdit
    {
        public PayrollSheetFormatSelector()
        {
            base.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            if (AccountingClient.IsConnected)
                loadData();
        }
        public void loadData()
        {
            object[] formats=PayrollClient.GetSystemParameters(new string[]{"PayrollColumnFormula","PayrollColumnFormulaTwo","PayrollColumnFormulaThree"});
            foreach(object o in formats)
            {
                if(o is PayrollSheetFormat)
                    this.Properties.Items.Add(o);
            }
            if(this.Properties.Items.Count>0)
                this.SelectedIndex=0;
        }
        public PayrollSheetFormat getSelectedFormat()
        {
            return this.SelectedItem as PayrollSheetFormat;
        }
    }
}
