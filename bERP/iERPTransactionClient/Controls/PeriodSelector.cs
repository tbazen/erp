﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    public class PeriodSelector : DevExpress.XtraEditors.ComboBoxEdit
    {
        const string PAYROLL_ACCOUNTING_PERIOD_TYPE = "AP_Payroll";
        List<AccountingPeriod> _periods;
        string _periodType = "AP_Accounting_Month";

        public string PeriodType
        {
            get { return _periodType; }
            set
            {
                if (_periodType.Equals(value))
                    return;
                _periodType = value;
                if (INTAPS.Accounting.Client.AccountingClient.IsConnected) 
                    ReloadItems();
            }
        }


        void ReloadItems()
        {
            Properties.Items.Clear();
            _periods = new List<AccountingPeriod>();
            int nPastYears=1;
            string strPastYears = System.Configuration.ConfigurationManager.AppSettings["nPastYears"];
            if(!string.IsNullOrEmpty(strPastYears))
            {
                if (!int.TryParse(strPastYears, out nPastYears))
                    nPastYears = 1;
            }
            if (_periodType.ToUpper() == PAYROLL_ACCOUNTING_PERIOD_TYPE.ToUpper())
            {
                for (int i = nPastYears; i >= 1;i-- )
                    _periods.AddRange(INTAPS.Payroll.Client.PayrollClient.GetPayPeriods(INTAPS.Payroll.Client.PayrollClient.CurrentYear - i, INTAPS.Payroll.Client.PayrollClient.Ethiopian));
                _periods.AddRange(INTAPS.Payroll.Client.PayrollClient.GetPayPeriods(INTAPS.Payroll.Client.PayrollClient.CurrentYear,INTAPS.Payroll.Client.PayrollClient.Ethiopian));
                _periods.AddRange(INTAPS.Payroll.Client.PayrollClient.GetPayPeriods(INTAPS.Payroll.Client.PayrollClient.CurrentYear+1, INTAPS.Payroll.Client.PayrollClient.Ethiopian));
            }
            else
            {
                for (int i = nPastYears; i >= 1; i--)
                    _periods.AddRange(iERPTransactionClient.GetAccountingPeriodsInFiscalYear(_periodType, Globals.CurrentFiscalYear - i));
                _periods.AddRange(iERPTransactionClient.GetAccountingPeriodsInFiscalYear(_periodType, Globals.CurrentFiscalYear));
                _periods.AddRange(iERPTransactionClient.GetAccountingPeriodsInFiscalYear(_periodType, Globals.CurrentFiscalYear+1));
            }
            foreach (AccountingPeriod prd in _periods)
                this.Properties.Items.Add(prd.name);
            int j = 0;
            DateTime now=DateTime.Now;
            foreach (AccountingPeriod pr in _periods)
            {
                if (pr.InPeriod(now))
                {
                    this.SelectedIndex = j;
                    break;
                }
                j++;
            }
        }

        public PeriodSelector()
        {
            _periods = new List<AccountingPeriod>();
            base.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            if (INTAPS.Accounting.Client.AccountingClient.IsConnected)
                ReloadItems();
        }
        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [System.ComponentModel.Browsable(false)]
        public AccountingPeriod selectedPeriod
        {
            get
            {
                if (this.SelectedIndex == -1)
                    return null;
                return _periods[this.SelectedIndex];
            }
            set
            {
                if (value == null)
                    this.SelectedIndex = -1;
                else
                {
                    int index = 0;
                    foreach (AccountingPeriod prd in _periods)
                    {
                        if (value.id == prd.id)
                        {
                            this.SelectedIndex = index;
                            break;
                        }
                        index++;

                    }
                }
            }

        }

        [System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        [System.ComponentModel.Browsable(false)]

        public int selectedPeriodID
        {
            get
            {
                if (this.SelectedIndex == -1)
                    return -1;
                return _periods[this.SelectedIndex].id;
            }
            set
            {
                if (value == -1)
                    this.SelectedIndex = -1;
                else
                {
                    int index = 0;
                    foreach (AccountingPeriod prd in _periods)
                    {
                        if (value == prd.id)
                        {
                            this.SelectedIndex = index;
                            break;
                        }
                        index++;

                    }
                }
            }

        }


        internal void setByDate(DateTime dateTime)
        {
            int index=0;
            foreach(AccountingPeriod p in _periods)
            {
                if(p.InPeriod(dateTime))
                {
                    this.SelectedIndex=index;
                    break;
                }
                index++;
            }
        }

        
    }
}
