﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.Accounting.Client;
using System.ComponentModel;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;

namespace BIZNET.iERP.Client
{
    public class StorePlaceholder : DevExpress.XtraEditors.ComboBoxEdit
    {
        StoreInfo[] _stores;
        void loadItems()
        {
            foreach (StoreInfo c in _stores = iERPTransactionClient.GetAllStores())
                Properties.Items.Add(c.description+"-"+c.code);
            if (Properties.Items.Count > 0)
                this.SelectedIndex = 0;
        }
        public StorePlaceholder()
        {
            base.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            if (AccountingClient.IsConnected)
                loadItems();
        }
        
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public StoreInfo bankAccount
        {
            get
            {
                if (this.SelectedIndex == -1)
                    return null;
                return _stores[this.SelectedIndex];
            }
            set
            {
                if (value == null)
                    this.storeID = -1;
                else
                    this.storeID = value.costCenterID;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int storeID
        {
            get
            {
                if (this.SelectedIndex == -1)
                    return -1;
                return _stores[this.SelectedIndex].costCenterID;
            }
            set
            {
                int i = 0;
                foreach (StoreInfo ac in _stores)
                {
                    if (ac.costCenterID == value)
                    {
                        this.SelectedIndex = i;
                        return;
                    }
                    i++;
                }
                this.SelectedIndex = -1;
            }
        }

       

    }
}

