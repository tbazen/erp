﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Columns;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using DevExpress.XtraEditors.Controls;

namespace BIZNET.iERP.Client
{
    public partial class AdjustmentForm : XtraForm
    {
        int inputVATAccountID
            , collectedWithHoldingTaxAccountID
            , outPutVATAccountID
            , paidWithHoldingTaxAccountID
            , laborIncomeTaxAccountID
            , IncomeTaxLiabilityAccount
            , staffPensionPayableAccountID;
        DataTable _inputVATTable;
        DataTable _outputVATTable;
        DataTable _paidWHTable;
        DataTable _collectedWHTable;
        Dictionary<DataRow, DataRow> _inputVATLookup;
        Dictionary<DataRow, DataRow> _outVATLookup;
        Dictionary<DataRow, DataRow> _paidWHLookup;
        Dictionary<DataRow, DataRow> _collectedWHLookup;
        
        InputVatController _inputVATPopup;
        OutputVatController _ouputVATPopup;
        OutputVatController _paidWHPopup;
        InputVatController _collectedWHPopup;
        
        class InputVatController : PopupController<TradeRelationSearchPopup>
        {
            AdjustmentForm _parent;
            public InputVatController(AdjustmentForm parent, DevExpress.XtraGrid.GridControl grid, DevExpress.XtraGrid.Views.Grid.GridView gridView)
                :base(grid,gridView)
            {
                _parent = parent;
            }
            protected override void setSearch(DataRow row, GridColumn col, string value)
            {
                switch (col.FieldName)
                {
                    case "Supplier":
                        base.Popup.setSearch(value, TradeRelationType.Supplier);
                        break;
                }
            }

            protected override void itemSelected(DataRow row, GridColumn col)
            {
                row["Supplier"] = Popup.selectedRelation.NameCode;
                row["TIN"] = Popup.selectedRelation.TIN;
                row["RelationCode"] = Popup.selectedRelation;
                base._gridView.FocusedColumn = base._gridView.Columns["Receipt No"];
            }

            protected override bool isColumnSearchable(GridColumn col)
            {
                return col.FieldName.Equals("Supplier") || col.FieldName.Equals("TIN");
            }
        }
        class OutputVatController : PopupController<TradeRelationSearchPopup>
        {
            AdjustmentForm _parent;
            public OutputVatController(AdjustmentForm parent, DevExpress.XtraGrid.GridControl grid, DevExpress.XtraGrid.Views.Grid.GridView gridView)
                : base(grid, gridView)
            {
                _parent = parent;
            }
            protected override void setSearch(DataRow row, GridColumn col, string value)
            {
                switch (col.FieldName)
                {
                    case "Customer":
                        base.Popup.setSearch(value, TradeRelationType.Customer);
                        break;
                }
            }

            protected override void itemSelected(DataRow row, GridColumn col)
            {
                row["Customer"] = Popup.selectedRelation.NameCode;
                row["TIN"] = Popup.selectedRelation.TIN;
                row["RelationCode"] = Popup.selectedRelation;
                base._gridView.FocusedColumn = base._gridView.Columns["Receipt No"];
            }

            protected override bool isColumnSearchable(GridColumn col)
            {
                return col.FieldName.Equals("Customer") || col.FieldName.Equals("TIN");
            }
        }
        void setInputVatSupplier(TradeRelation supplier)
        {
        }
        int[] allTaxSettings;
        DataTable[] allTables;
        Dictionary<DataRow, DataRow>[] allLookups;
        GridView[] allViews;
        string[] allRelationLabels;
        void initializeDeclarationSupport()
        {
            
            loadTaxAccountSettings();
            _inputVATPopup = new InputVatController(this, gridInputVAT, gridViewInputVAT);
            _ouputVATPopup = new OutputVatController(this, gridOutputVAT, gridViewOuputVat);
            _paidWHPopup = new OutputVatController(this, gridPaidWH, gridViewPaidWH);
            _collectedWHPopup = new InputVatController(this, gridCollectedWH, gridViewCollectedWH);
            prepareArrays();
            createDeclarationTables();
            buildDeclarationTables();

        }

        private void prepareArrays()
        {
            //The following array are important part of the algorithm for synchronizing the declaration grids with the main grid
            //1. the order of items in this array should be the same as the tab pages 
            //2. all of the arrays must be the same size
            allTaxSettings = new int[] { inputVATAccountID, outPutVATAccountID, paidWithHoldingTaxAccountID, collectedWithHoldingTaxAccountID };
            allTables = new DataTable[] { 
                _inputVATTable=new DataTable("Table")
                , _outputVATTable=new DataTable("Table")
                , _paidWHTable=new DataTable("Table")
                , _collectedWHTable=new DataTable("Table")
            };
            allLookups = new Dictionary<DataRow, DataRow>[] { 
                _inputVATLookup=new Dictionary<DataRow,DataRow>()
                , _outVATLookup=new Dictionary<DataRow,DataRow>()
                , _paidWHLookup=new Dictionary<DataRow,DataRow>()
                , _collectedWHLookup=new Dictionary<DataRow,DataRow>()
            };
            allViews = new GridView[] { gridViewInputVAT, gridViewOuputVat, gridViewPaidWH, gridViewCollectedWH };

            allRelationLabels = new string[] { "Supplier", "Customer", "Customer", "Supplier" };
        }

        void loadTaxAccountSettings()
        {
            inputVATAccountID=iERPTransactionSystemParametrsClient.inputVATAccountID;
            collectedWithHoldingTaxAccountID=iERPTransactionSystemParametrsClient.collectedWithHoldingTaxAccountID;
            outPutVATAccountID=iERPTransactionSystemParametrsClient.outPutVATAccountID;
            paidWithHoldingTaxAccountID=iERPTransactionSystemParametrsClient.paidWithHoldingTaxAccountID;
            laborIncomeTaxAccountID=iERPTransactionSystemParametrsClient.laborIncomeTaxAccountID;
            IncomeTaxLiabilityAccount = (int)INTAPS.Payroll.Client.PayrollClient.GetSystemParameter("IncomeTaxLiabilityAccount");
            staffPensionPayableAccountID = (int)INTAPS.Payroll.Client.PayrollClient.GetSystemParameter("staffPensionPayableAccountID");
        }
        void buildDeclarationTables()
        {
            
            for (int i = 0; i < allTaxSettings.Length; i++)
            {
                allTables[i].Rows.Clear();
                allTables[i].AcceptChanges();
            }
            foreach (DataRow row in _entryTable.Rows)
            {
                int accountID = (int)row["accountID"];
                for (int i = 0; i < allTaxSettings.Length; i++)
                {
                    if (accountID == allTaxSettings[i])
                    {
                        DataRow r = allTables[i].Rows.Add(
                            row["accountID"]
                            , row["Account"]
                            , row["Debit"]
                            , row["Credit"]
                            ,null
                            ,null
                            ,null
                            ,null
                            ,"Service"
                            );
                        allLookups[i].Add(row, r);
                    }
                }
            }

            fixPageVisibility();
        }

        void updateDeclarationTables(DataRow changedRow)
        {
            int accountID = (int)changedRow["accountID"];
            for (int i = 0; i < allTaxSettings.Length; i++)
            {
                if (allTaxSettings[i] == -1)
                    continue;
                bool updateAccountInformation = false;
                DataRow thisRow = null;
                if (allLookups[i].ContainsKey(changedRow))
                {
                    thisRow = allLookups[i][changedRow];
                    int thisAccountID = (int)thisRow["accountID"];
                    if (thisAccountID != accountID)
                    {
                        allLookups[i].Remove(changedRow);
                        allTables[i].Rows.Remove(thisRow);
                    }
                    else
                    {
                        updateAccountInformation = true;
                    }
                }

                if (accountID != allTaxSettings[i])
                    continue;

                if (updateAccountInformation)
                {
                    thisRow["Debit"] = changedRow["Debit"];
                    thisRow["Credit"] = changedRow["Credit"];
                }
                else
                {
                    DataRow r = allTables[i].Rows.Add(
                        changedRow["accountID"]
                        , changedRow["Account"]
                        , changedRow["Debit"]
                        , changedRow["Credit"]
                        , null
                            , null
                            , null
                            , null
                            , "Service");
                    allLookups[i].Add(changedRow, r);
                }
            }
            fixPageVisibility();

        }
        void fixPageVisibility()
        {
            for (int i = 0; i < allTaxSettings.Length; i++)
            {
                tabControl.TabPages[i+1].PageVisible = allTables[i].Rows.Count > 0;
            }
        }
        void deleteFromDeclarationTables(DataRow deletedRow)
        {
            for (int i = 0; i < allTaxSettings.Length; i++)
            {
                if (allLookups[i].ContainsKey(deletedRow))
                {
                    DataRow thisRow = allLookups[i][deletedRow];
                    allLookups[i].Remove(deletedRow);
                    allTables[i].Rows.Remove(thisRow);
                    break;
                }
            }
            fixPageVisibility();
        }
        void createDeclarationTables()
        {
            repositoryItemComboBox1.Items.AddRange(new string[]{"Service","Good"});
            for (int i = 0; i < allTables.Length; i++)
            {
                allTables[i].Columns.Add("accountID", typeof(int));
                allTables[i].Columns.Add("Account", typeof(string));
                allTables[i].Columns.Add("Debit", typeof(double));
                allTables[i].Columns.Add("Credit", typeof(double));
                allTables[i].Columns.Add(allRelationLabels[i], typeof(string));
                allTables[i].Columns.Add("TIN", typeof(string));
                allTables[i].Columns.Add("Receipt No", typeof(string));
                allTables[i].Columns.Add("RelationCode", typeof(TradeRelation));
                allTables[i].Columns.Add("Good/Service", typeof(string));
                allViews[i].GridControl.DataSource =allTables[i];
                foreach (GridColumn col in allViews[i].Columns)
                {
                    col.OptionsColumn.AllowEdit = col.AbsoluteIndex > 3;
                    if (!col.OptionsColumn.AllowEdit)
                        col.AppearanceCell.BackColor = Color.LightGray;
                }
                allViews[i].Columns["accountID"].Visible = false;
                allViews[i].Columns["RelationCode"].Visible = false;
                allViews[i].Columns["Good/Service"].ColumnEdit = repositoryItemComboBox1;

            }

        }
        void clearDeclarationData()
        {
            for(int i=0;i<allTables.Length;i++)
            {
                allTables[i].Rows.Clear();
                allLookups[i].Clear();
            }
        }
        void getDeclarationData(DataRow row, AdjustmentAccount adj)
        {
            foreach (Dictionary<DataRow, DataRow> lookup in allLookups)
            {
                if (lookup.ContainsKey(row))
                {
                    DataRow thisRow = lookup[row];
                    adj.relation=thisRow["RelationCode"] as TradeRelation;
                    adj.goodOrService = thisRow["Good/Service"].ToString().Equals( "Service" )? GoodOrService.Service : GoodOrService.Good;
                    adj.receiptNo = thisRow["Receipt No"] as string;
                    break;
                }
            }
            
        }
        void setDeclarationData(DataRow row, AdjustmentAccount adj)
        {
            int i=0;
            foreach (Dictionary<DataRow, DataRow> lookup in allLookups)
            {
                if (lookup.ContainsKey(row))
                {
                    DataRow thisRow = lookup[row];
                    thisRow["RelationCode"] = adj.relation;
                    if (adj.relation != null)
                    {
                        thisRow[allRelationLabels[i]] = adj.relation.NameCode;
                        thisRow["TIN"] = adj.relation.TIN;
                        thisRow["Good/Service"] = adj.goodOrService == GoodOrService.Service ? "Service" : "Good";
                        thisRow["Receipt No"] =adj.receiptNo;
                    }
                    break;
                }
                i++;
            }
        }

    }
}