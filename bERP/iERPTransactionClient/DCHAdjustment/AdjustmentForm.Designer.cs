﻿namespace BIZNET.iERP.Client
{
    partial class AdjustmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.voucher = new BIZNET.iERP.Client.DocumentSerialPlaceHolder();
            this.dateDocument = new BIZNET.iERP.Client.BNDualCalendar();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnPost = new DevExpress.XtraEditors.SimpleButton();
            this.addEntry = new DevExpress.XtraEditors.SimpleButton();
            this.btnRemove = new DevExpress.XtraEditors.SimpleButton();
            this.txtAdditionalInfo = new DevExpress.XtraEditors.MemoEdit();
            this.gridEntry = new DevExpress.XtraGrid.GridControl();
            this.gridViewEntry = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.validateControls = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.pageEntries = new DevExpress.XtraTab.XtraTabPage();
            this.pageInputVAT = new DevExpress.XtraTab.XtraTabPage();
            this.gridInputVAT = new DevExpress.XtraGrid.GridControl();
            this.gridViewInputVAT = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.pageOuputVAT = new DevExpress.XtraTab.XtraTabPage();
            this.gridOutputVAT = new DevExpress.XtraGrid.GridControl();
            this.gridViewOuputVat = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.pagePaidWH = new DevExpress.XtraTab.XtraTabPage();
            this.gridPaidWH = new DevExpress.XtraGrid.GridControl();
            this.gridViewPaidWH = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemButtonEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.pageCollectedWH = new DevExpress.XtraTab.XtraTabPage();
            this.gridCollectedWH = new DevExpress.XtraGrid.GridControl();
            this.gridViewCollectedWH = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemButtonEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdditionalInfo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validateControls)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.pageEntries.SuspendLayout();
            this.pageInputVAT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridInputVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInputVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            this.pageOuputVAT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridOutputVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOuputVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            this.pagePaidWH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPaidWH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPaidWH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit4)).BeginInit();
            this.pageCollectedWH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCollectedWH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCollectedWH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.voucher);
            this.layoutControl1.Controls.Add(this.dateDocument);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(383, 169, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(750, 102);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // voucher
            // 
            this.voucher.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.voucher.Appearance.Options.UseBackColor = true;
            this.voucher.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.voucher.Location = new System.Drawing.Point(12, 28);
            this.voucher.Name = "voucher";
            this.voucher.Size = new System.Drawing.Size(361, 62);
            this.voucher.TabIndex = 11;
            // 
            // dateDocument
            // 
            this.dateDocument.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateDocument.Location = new System.Drawing.Point(377, 28);
            this.dateDocument.Name = "dateDocument";
            this.dateDocument.ShowEthiopian = true;
            this.dateDocument.ShowGregorian = true;
            this.dateDocument.ShowTime = true;
            this.dateDocument.Size = new System.Drawing.Size(361, 63);
            this.dateDocument.TabIndex = 10;
            this.dateDocument.VerticalLayout = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(750, 102);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.dateDocument;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(365, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(365, 82);
            this.layoutControlItem5.Text = "Date:";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(50, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.voucher;
            this.layoutControlItem1.CustomizationFormText = "Reference";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(365, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(365, 41);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(365, 82);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "Reference";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(50, 13);
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.btnClose);
            this.panelControl1.Controls.Add(this.btnPost);
            this.panelControl1.Controls.Add(this.addEntry);
            this.panelControl1.Controls.Add(this.btnRemove);
            this.panelControl1.Controls.Add(this.txtAdditionalInfo);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 396);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(750, 105);
            this.panelControl1.TabIndex = 7;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(3, 6);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 13);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Note:";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(670, 78);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnPost
            // 
            this.btnPost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPost.Location = new System.Drawing.Point(580, 79);
            this.btnPost.Name = "btnPost";
            this.btnPost.Size = new System.Drawing.Size(75, 23);
            this.btnPost.TabIndex = 0;
            this.btnPost.Text = "&Post";
            this.btnPost.Click += new System.EventHandler(this.btnPost_Click);
            // 
            // addEntry
            // 
            this.addEntry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.addEntry.Location = new System.Drawing.Point(120, 75);
            this.addEntry.Name = "addEntry";
            this.addEntry.Size = new System.Drawing.Size(102, 26);
            this.addEntry.TabIndex = 0;
            this.addEntry.Text = "&Add Entry";
            this.addEntry.Click += new System.EventHandler(this.addEntry_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRemove.Location = new System.Drawing.Point(12, 75);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(102, 26);
            this.btnRemove.TabIndex = 0;
            this.btnRemove.Text = "&Remove Entries";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // txtAdditionalInfo
            // 
            this.txtAdditionalInfo.Location = new System.Drawing.Point(36, 6);
            this.txtAdditionalInfo.Name = "txtAdditionalInfo";
            this.txtAdditionalInfo.Size = new System.Drawing.Size(698, 62);
            this.txtAdditionalInfo.TabIndex = 5;
            // 
            // gridEntry
            // 
            this.gridEntry.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEntry.Location = new System.Drawing.Point(0, 0);
            this.gridEntry.MainView = this.gridViewEntry;
            this.gridEntry.Name = "gridEntry";
            this.gridEntry.Padding = new System.Windows.Forms.Padding(3);
            this.gridEntry.Size = new System.Drawing.Size(744, 266);
            this.gridEntry.TabIndex = 6;
            this.gridEntry.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewEntry});
            this.gridEntry.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridControlAccountAdjustment_ProcessGridKey);
            // 
            // gridViewEntry
            // 
            this.gridViewEntry.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewEntry.Appearance.GroupPanel.ForeColor = System.Drawing.Color.DarkViolet;
            this.gridViewEntry.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewEntry.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewEntry.GridControl = this.gridEntry;
            this.gridViewEntry.GroupPanelText = "Enter Account Code to Debit or Credit";
            this.gridViewEntry.Name = "gridViewEntry";
            this.gridViewEntry.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewEntry.OptionsCustomization.AllowFilter = false;
            this.gridViewEntry.OptionsCustomization.AllowGroup = false;
            this.gridViewEntry.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewEntry.OptionsCustomization.AllowSort = false;
            this.gridViewEntry.OptionsSelection.MultiSelect = true;
            this.gridViewEntry.OptionsView.ShowFooter = true;
            this.gridViewEntry.HiddenEditor += new System.EventHandler(this.gridViewAccountAdjustment_HiddenEditor);
            this.gridViewEntry.ShownEditor += new System.EventHandler(this.gridViewAccountAdjustment_ShownEditor);
            this.gridViewEntry.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewAccountAdjustment_CellValueChanged);
            this.gridViewEntry.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewAccountAdjustment_CellValueChanging);
            this.gridViewEntry.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewAccountAdjustment_InvalidRowException);
            this.gridViewEntry.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewAccountAdjustment_ValidateRow);
            this.gridViewEntry.LostFocus += new System.EventHandler(this.gridViewAccountAdjustment_LostFocus);
            // 
            // validateControls
            // 
            this.validateControls.ValidateHiddenControls = false;
            // 
            // tabControl
            // 
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 102);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedTabPage = this.pageEntries;
            this.tabControl.Size = new System.Drawing.Size(750, 294);
            this.tabControl.TabIndex = 8;
            this.tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.pageEntries,
            this.pageInputVAT,
            this.pageOuputVAT,
            this.pagePaidWH,
            this.pageCollectedWH});
            // 
            // pageEntries
            // 
            this.pageEntries.Controls.Add(this.gridEntry);
            this.pageEntries.Name = "pageEntries";
            this.pageEntries.Size = new System.Drawing.Size(744, 266);
            this.pageEntries.Text = "Entries";
            // 
            // pageInputVAT
            // 
            this.pageInputVAT.Controls.Add(this.gridInputVAT);
            this.pageInputVAT.Name = "pageInputVAT";
            this.pageInputVAT.Size = new System.Drawing.Size(744, 266);
            this.pageInputVAT.Text = "Input VAT";
            // 
            // gridInputVAT
            // 
            this.gridInputVAT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridInputVAT.Location = new System.Drawing.Point(0, 0);
            this.gridInputVAT.MainView = this.gridViewInputVAT;
            this.gridInputVAT.Name = "gridInputVAT";
            this.gridInputVAT.Padding = new System.Windows.Forms.Padding(3);
            this.gridInputVAT.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.gridInputVAT.Size = new System.Drawing.Size(744, 266);
            this.gridInputVAT.TabIndex = 7;
            this.gridInputVAT.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewInputVAT});
            // 
            // gridViewInputVAT
            // 
            this.gridViewInputVAT.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewInputVAT.Appearance.GroupPanel.ForeColor = System.Drawing.Color.DarkViolet;
            this.gridViewInputVAT.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewInputVAT.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewInputVAT.GridControl = this.gridInputVAT;
            this.gridViewInputVAT.GroupPanelText = "Enter Additional Information for Input VAT";
            this.gridViewInputVAT.Name = "gridViewInputVAT";
            this.gridViewInputVAT.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewInputVAT.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewInputVAT.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewInputVAT.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewInputVAT.OptionsCustomization.AllowFilter = false;
            this.gridViewInputVAT.OptionsCustomization.AllowGroup = false;
            this.gridViewInputVAT.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewInputVAT.OptionsCustomization.AllowSort = false;
            this.gridViewInputVAT.OptionsSelection.MultiSelect = true;
            this.gridViewInputVAT.OptionsView.ShowFooter = true;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            // 
            // pageOuputVAT
            // 
            this.pageOuputVAT.Controls.Add(this.gridOutputVAT);
            this.pageOuputVAT.Name = "pageOuputVAT";
            this.pageOuputVAT.Size = new System.Drawing.Size(744, 266);
            this.pageOuputVAT.Text = "Ouput VAT";
            // 
            // gridOutputVAT
            // 
            this.gridOutputVAT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridOutputVAT.Location = new System.Drawing.Point(0, 0);
            this.gridOutputVAT.MainView = this.gridViewOuputVat;
            this.gridOutputVAT.Name = "gridOutputVAT";
            this.gridOutputVAT.Padding = new System.Windows.Forms.Padding(3);
            this.gridOutputVAT.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit2});
            this.gridOutputVAT.Size = new System.Drawing.Size(744, 266);
            this.gridOutputVAT.TabIndex = 8;
            this.gridOutputVAT.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewOuputVat});
            // 
            // gridViewOuputVat
            // 
            this.gridViewOuputVat.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewOuputVat.Appearance.GroupPanel.ForeColor = System.Drawing.Color.DarkViolet;
            this.gridViewOuputVat.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewOuputVat.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewOuputVat.GridControl = this.gridOutputVAT;
            this.gridViewOuputVat.GroupPanelText = "Enter Additional Information for Output VAT";
            this.gridViewOuputVat.Name = "gridViewOuputVat";
            this.gridViewOuputVat.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewOuputVat.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewOuputVat.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewOuputVat.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewOuputVat.OptionsCustomization.AllowFilter = false;
            this.gridViewOuputVat.OptionsCustomization.AllowGroup = false;
            this.gridViewOuputVat.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewOuputVat.OptionsCustomization.AllowSort = false;
            this.gridViewOuputVat.OptionsSelection.MultiSelect = true;
            this.gridViewOuputVat.OptionsView.ShowFooter = true;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.AutoHeight = false;
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            // 
            // pagePaidWH
            // 
            this.pagePaidWH.Controls.Add(this.gridPaidWH);
            this.pagePaidWH.Name = "pagePaidWH";
            this.pagePaidWH.Size = new System.Drawing.Size(744, 266);
            this.pagePaidWH.Text = "Paid Witholding";
            // 
            // gridPaidWH
            // 
            this.gridPaidWH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPaidWH.Location = new System.Drawing.Point(0, 0);
            this.gridPaidWH.MainView = this.gridViewPaidWH;
            this.gridPaidWH.Name = "gridPaidWH";
            this.gridPaidWH.Padding = new System.Windows.Forms.Padding(3);
            this.gridPaidWH.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit4});
            this.gridPaidWH.Size = new System.Drawing.Size(744, 266);
            this.gridPaidWH.TabIndex = 9;
            this.gridPaidWH.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPaidWH});
            // 
            // gridViewPaidWH
            // 
            this.gridViewPaidWH.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewPaidWH.Appearance.GroupPanel.ForeColor = System.Drawing.Color.DarkViolet;
            this.gridViewPaidWH.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewPaidWH.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewPaidWH.GridControl = this.gridPaidWH;
            this.gridViewPaidWH.GroupPanelText = "Enter Additional Information for Paid Witholding Tax";
            this.gridViewPaidWH.Name = "gridViewPaidWH";
            this.gridViewPaidWH.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewPaidWH.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewPaidWH.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewPaidWH.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewPaidWH.OptionsCustomization.AllowFilter = false;
            this.gridViewPaidWH.OptionsCustomization.AllowGroup = false;
            this.gridViewPaidWH.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewPaidWH.OptionsCustomization.AllowSort = false;
            this.gridViewPaidWH.OptionsSelection.MultiSelect = true;
            this.gridViewPaidWH.OptionsView.ShowFooter = true;
            // 
            // repositoryItemButtonEdit4
            // 
            this.repositoryItemButtonEdit4.AutoHeight = false;
            this.repositoryItemButtonEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit4.Name = "repositoryItemButtonEdit4";
            // 
            // pageCollectedWH
            // 
            this.pageCollectedWH.Controls.Add(this.gridCollectedWH);
            this.pageCollectedWH.Name = "pageCollectedWH";
            this.pageCollectedWH.Size = new System.Drawing.Size(744, 266);
            this.pageCollectedWH.Text = "Collected Witholding";
            // 
            // gridCollectedWH
            // 
            this.gridCollectedWH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCollectedWH.Location = new System.Drawing.Point(0, 0);
            this.gridCollectedWH.MainView = this.gridViewCollectedWH;
            this.gridCollectedWH.Name = "gridCollectedWH";
            this.gridCollectedWH.Padding = new System.Windows.Forms.Padding(3);
            this.gridCollectedWH.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit3,
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2});
            this.gridCollectedWH.Size = new System.Drawing.Size(744, 266);
            this.gridCollectedWH.TabIndex = 9;
            this.gridCollectedWH.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCollectedWH});
            // 
            // gridViewCollectedWH
            // 
            this.gridViewCollectedWH.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewCollectedWH.Appearance.GroupPanel.ForeColor = System.Drawing.Color.DarkViolet;
            this.gridViewCollectedWH.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewCollectedWH.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewCollectedWH.GridControl = this.gridCollectedWH;
            this.gridViewCollectedWH.GroupPanelText = "Enter Additional Information for Collected Witholding Tax";
            this.gridViewCollectedWH.Name = "gridViewCollectedWH";
            this.gridViewCollectedWH.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewCollectedWH.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewCollectedWH.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewCollectedWH.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewCollectedWH.OptionsCustomization.AllowFilter = false;
            this.gridViewCollectedWH.OptionsCustomization.AllowGroup = false;
            this.gridViewCollectedWH.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewCollectedWH.OptionsCustomization.AllowSort = false;
            this.gridViewCollectedWH.OptionsSelection.MultiSelect = true;
            this.gridViewCollectedWH.OptionsView.ShowFooter = true;
            // 
            // repositoryItemButtonEdit3
            // 
            this.repositoryItemButtonEdit3.AutoHeight = false;
            this.repositoryItemButtonEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit3.Name = "repositoryItemButtonEdit3";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // AdjustmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 501);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "AdjustmentForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Adjustment Form";
            this.Load += new System.EventHandler(this.AdjustmentForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdditionalInfo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validateControls)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.pageEntries.ResumeLayout(false);
            this.pageInputVAT.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridInputVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInputVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            this.pageOuputVAT.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridOutputVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOuputVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            this.pagePaidWH.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridPaidWH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPaidWH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit4)).EndInit();
            this.pageCollectedWH.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCollectedWH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCollectedWH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.MemoEdit txtAdditionalInfo;
        private DevExpress.XtraGrid.GridControl gridEntry;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewEntry;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnPost;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validateControls;
        private DevExpress.XtraEditors.SimpleButton btnRemove;
        private BIZNET.iERP.Client.BNDualCalendar dateDocument;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DocumentSerialPlaceHolder voucher;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraTab.XtraTabPage pageEntries;
        private DevExpress.XtraTab.XtraTabPage pageInputVAT;
        private DevExpress.XtraTab.XtraTabPage pageOuputVAT;
        private DevExpress.XtraTab.XtraTabPage pageCollectedWH;
        private DevExpress.XtraTab.XtraTabPage pagePaidWH;
        private DevExpress.XtraGrid.GridControl gridInputVAT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewInputVAT;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.GridControl gridOutputVAT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewOuputVat;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private DevExpress.XtraGrid.GridControl gridCollectedWH;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCollectedWH;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit3;
        private DevExpress.XtraGrid.GridControl gridPaidWH;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPaidWH;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton addEntry;
    }
}