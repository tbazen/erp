using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
namespace BIZNET.iERP.Client
{
    public class DCHAdjustment : bERPClientDocumentHandler
    {
        public DCHAdjustment()
            : base(typeof(AdjustmentForm))
        {
        }
        
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            AdjustmentForm f = (AdjustmentForm)editor;
            f.LoadData((AdjustmentDocument)doc);
        }

    }
}
