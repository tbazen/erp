﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS;
namespace BIZNET.iERP.Client
{
    public partial class AssignToCustodianForm : DevExpress.XtraEditors.XtraForm
    {
        AssignToCustodianDocument _doc = null;
        DataTable gridData;
        Dictionary<DataRow, AssignToCustodianDocument.AssignmentItem> items;
        IAccountingClient _client;
        public AssignToCustodianForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            _client = client;
            initializeGrid();
            updateButtonStatus();
        }
        void initializeGrid()
        {
            items = new Dictionary<DataRow, AssignToCustodianDocument.AssignmentItem>();
            gridData = new DataTable("data");
            gridData.Columns.Add("Item Code");
            gridData.Columns.Add("Property Code");
            gridData.Columns.Add("Type");
            gridData.Columns.Add("Name");
            gridData.Columns.Add("Aquired Year");
            gridData.Columns.Add("Cost Center");
            gridControl.DataSource = gridData;
        }
        private void addFixedAssetToGrid(Property prop,object extraData,int costCenterID)
        {
            PropertyType type = iERPTransactionClient.getPropertyType(prop.propertyTypeID);
            DataRow row= gridData.Rows.Add(prop.parentItemCode,
                prop.propertyCode,
                type == null ? null : type.name,
                prop.name,
                prop.acquireDate.ToShortDateString(),
                AccountingClient.GetAccount<CostCenter>(costCenterID).NameCode
                );
            items.Add(row, new AssignToCustodianDocument.AssignmentItem()
            {
                existingPropertyID=-1,
                newProperty=prop,
                newPropertyCostCenterID=costCenterID,
                newPropertyData=extraData
            }
            );
        }
        private void addPropertyToGrid(Property prop,int costCenterID)
        {
            PropertyType type = iERPTransactionClient.getPropertyType(prop.propertyTypeID);

            DataRow row = gridData.Rows.Add(prop.parentItemCode,
                prop.propertyCode,
                type == null ? null : type.name,
                prop.name,
                prop.acquireDate.ToShortDateString(),
                AccountingClient.GetAccount<CostCenter>(costCenterID).NameCode
                );
            items.Add(row, new AssignToCustodianDocument.AssignmentItem()
            {
                existingPropertyID = prop.id,
                newProperty = null,
                newPropertyCostCenterID = -1,
                newPropertyData = null,
            }
            );
        }
        private void setFixedAssetToGridRow(DataRow row, Property prop, object extraData, int costCenterID)
        {
            PropertyType type = iERPTransactionClient.getPropertyType(prop.propertyTypeID);
            row[0] = prop.parentItemCode;
            row[1] = prop.propertyCode;
            row[2] = type == null ? null : type.name;
            row[3] = prop.name;
            row[4] = prop.acquireDate.ToShortDateString();
            row[4] = AccountingClient.GetAccount<CostCenter>(costCenterID).NameCode;
            items[row]=new AssignToCustodianDocument.AssignmentItem()
            {
                existingPropertyID = -1,
                newProperty = prop,
                newPropertyCostCenterID = costCenterID,
                newPropertyData = extraData
            };
        }
        private void setPropertyToGridRow(DataRow row, Property prop,int costCenterID)
        {
            PropertyType type = iERPTransactionClient.getPropertyType(prop.propertyTypeID);
            row[0] = prop.parentItemCode;
            row[1] = prop.propertyCode;
            row[2] = type == null ? null : type.name;
            row[3] = prop.name;
            row[4] = prop.acquireDate.ToShortDateString();
            row[4] = AccountingClient.GetAccount<CostCenter>(costCenterID).NameCode;
            items[row] = new AssignToCustodianDocument.AssignmentItem()
            {
                existingPropertyID = prop.id,
                newProperty = null,
                newPropertyCostCenterID = -1,
                newPropertyData = null
            };
        }
        void updateButtonStatus()
        {
            buttonDelete.Enabled = buttonEdit.Enabled = (gridView.GetSelectedRows().Length == 1);
        }

        internal void LoadData(AssignToCustodianDocument doc)
        {
            _doc=doc;
            if (doc == null)
                return;
            dateDocument.DateTime = doc.DocumentDate;
            costCenterPlaceholder.SetByID(doc.custodianID);
            
            foreach (AssignToCustodianDocument.AssignmentItem item in doc.items)
            {
                if (item.existingPropertyID == -1)
                    addFixedAssetToGrid(item.newProperty, item.newPropertyData, item.newPropertyCostCenterID);
                else
                {
                    object d;
                    Property p=iERPTransactionClient.getProperty(item.existingPropertyID,out d);
                    AccountBalance bal=iERPTransactionClient.verifyAndGetPropertyQuantityBalanceBalance(doc.DocumentDate, p.itemCode);
                    addPropertyToGrid(p,AccountingClient.GetCostCenterAccount(bal.AccountID).costCenterID);
                }
            }
        }

        private void gridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            updateButtonStatus();
        }

        private void buttonAddFixedAssetItem_Click(object sender, EventArgs e)
        {
            
            Property prop=new Property();
            RegisterProperty f=new RegisterProperty(prop,null,-1,dateDocument.DateTime,true);
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                addFixedAssetToGrid(prop,f.extraData,f.costCenterID);
            }
        }

        private void gridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            updateButtonStatus();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            int [] rows=gridView.GetSelectedRows();
            if(rows.Length==0)
                return;
            DataRow row = gridView.GetDataRow(rows[0]);
            AssignToCustodianDocument.AssignmentItem item = items[row];
            if (item.existingPropertyID == -1)
            {
                Property prop = item.newProperty.clone();
                RegisterProperty p = new RegisterProperty(prop, item.newPropertyData,item.newPropertyCostCenterID,  dateDocument.DateTime, true);
                if (p.ShowDialog() == DialogResult.OK)
                {
                    setFixedAssetToGridRow(row, prop, p.extraData, p.costCenterID);
                }
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            foreach (int row in gridView.GetSelectedRows())
            {
                DataRow drow = gridView.GetDataRow(row);
                gridData.Rows.Remove(drow);
                items.Remove(drow);
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                gridData.AcceptChanges();
                int docID = _doc == null ? -1 : _doc.AccountDocumentID;
                _doc = new AssignToCustodianDocument();
                _doc.AccountDocumentID = docID;
                _doc.DocumentDate = dateDocument.DateTime;
                _doc.items = new AssignToCustodianDocument.AssignmentItem[gridData.Rows.Count];
                _doc.custodianID = costCenterPlaceholder.GetAccountID();
                if (_doc.custodianID == -1)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserWarning("Please select custodian");
                    return;
                }
                int i = 0;
                foreach (DataRow row in gridData.Rows)
                {
                    _doc.items[i++] = items[row];
                }
                _client.PostGenericDocument(_doc);
                Close();

            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAddProperty_Click(object sender, EventArgs e)
        {
            PickProperty f = new PickProperty();
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                object d;
                Property prop=iERPTransactionClient.getProperty(f.pickedPropertyID, out d);
                AccountBalance[] bal = iERPTransactionClient.getMaterialBalance(dateDocument.DateTime, prop.propertyCode, new int[] { TransactionItem.MATERIAL_QUANTITY });
                if (bal.Length != 1 || !AccountBase.AmountEqual(bal[0].DebitBalance, 1))
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Property {0}  has invalid balance. Balance found in {1} cost centers.\nPick another property of check your data.".format(prop.propertyCode, bal.Length));
                }

                addPropertyToGrid(prop, AccountingClient.GetCostCenterAccount(bal[0].AccountID).costCenterID);
            }
        }
    }
}