using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHAssignToCustodian : bERPClientDocumentHandler
    {
        public DCHAssignToCustodian()
            : base(typeof(AssignToCustodianForm))
        {
        }
        
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            AssignToCustodianForm f = (AssignToCustodianForm)editor;
            f.LoadData((AssignToCustodianDocument)doc);
        }
    }
}
