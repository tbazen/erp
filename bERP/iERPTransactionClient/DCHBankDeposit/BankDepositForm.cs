﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;

namespace BIZNET.iERP.Client
{
    public partial class BankDepositForm : DevExpress.XtraEditors.XtraForm
    {
        BankDepositDocument m_doc = null;
        private bool newDocument = true;
        protected IAccountingClient _client;
        protected ActivationParameter _activation;

        public BankDepositForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            voucher.setKeys(typeof(BankToBankDocument), "voucher");

            _activation = activation;
            _client = client;
            
            txtAmount.LostFocus += Control_LostFocus;
            txtReference.LostFocus += Control_LostFocus;
            InitializeValidationRules();
        }

        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validationBankDeposit.Validate(control);
        }

        private void InitializeValidationRules()
        {
            NonEmptyNumericValidationRule depositAmountValidation = new NonEmptyNumericValidationRule();
            depositAmountValidation.ErrorText = "Deposit amount cannot be blank and cannot contain a value of zero";
            depositAmountValidation.ErrorType = ErrorType.Default;
            validationBankDeposit.SetValidationRule(txtAmount, depositAmountValidation);
        }
        

        internal void LoadData(BankDepositDocument bankDepositDocument)
        {
            attachments.setDocument(bankDepositDocument);

            m_doc = bankDepositDocument;
            
            if (bankDepositDocument == null)
            {
                ResetControls();
            }
            else
            {
                PopulateControlsForUpdate(bankDepositDocument);
                newDocument = false;
                SetCashAccountBalance();
                SetBankAccountBalance();
            }
        }
        private void ResetControls()
        {
            dateDeposit.DateTime = DateTime.Now;
            cmbCasher.SelectedIndex = cmbCasher.Properties.Items.Count > 0 ? 0 : -1;
            cmbCasher_SelectedIndexChanged(null, null);
            cmbBankAccount.SelectedIndex = cmbBankAccount.Properties.Items.Count > 0 ? 0 : -1;
            cmbBankAccount_SelectedIndexChanged(null, null);
            txtAmount.Text = "";
            txtReference.Text = "";
            voucher.setReference(-1, null);
            txtNote.Text = "";
        }
        private void PopulateControlsForUpdate(BankDepositDocument bankDepositDocument)
        {
            dateDeposit.DateTime = bankDepositDocument.DocumentDate;
            cmbCasher.cashCsAccountID = bankDepositDocument.casherAccountID;
            cmbBankAccount.mainCsAccountID = bankDepositDocument.bankAccountID;
            txtAmount.Text = TSConstants.FormatBirr(bankDepositDocument.amount);
            txtReference.Text = bankDepositDocument.PaperRef;
            txtNote.Text = bankDepositDocument.ShortDescription;
            voucher.setReference(bankDepositDocument.AccountDocumentID, bankDepositDocument.voucher);

        }



        private void buttonOk_Click(object sender, EventArgs e)
        {
            //MaterialManagmentClient is used for sending document 
            //from the client side to the framework
            try
            {
                if (validationBankDeposit.Validate())
                {
                    if (voucher.getReference() == null)
                    {
                        MessageBox.ShowErrorMessage("Enter valid reference");
                        return;
                    }
                    if (!ValidateCashAccount() || !ValidateBankAccount())
                        return;
                    int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;
                    BankDepositDocument newDoc = new BankDepositDocument();
                    newDoc.AccountDocumentID = docID;
                    newDoc.DocumentDate = dateDeposit.DateTime;
                    newDoc.casherAccountID = cmbCasher.cashCsAccountID;
                    newDoc.bankAccountID = cmbBankAccount.mainCsAccountID;
                    newDoc.amount = double.Parse(txtAmount.Text);
                    newDoc.PaperRef = txtReference.Text;
                    newDoc.voucher = voucher.getReference();
                    newDoc.ShortDescription = txtNote.Text;
                    newDoc.AccountDocumentID=_client.PostGenericDocument(newDoc);
                    if (!(_client is DocumentScheduler))
                    {
                        string message = docID == -1 ? "Bank Deposit successfully saved!" : "Bank Deposit successfully updated!";
                        MessageBox.ShowSuccessMessage(message);
                    }
                    if(docID==-1 && iERPTransactionClient.promptAttachments)
                    {
                        if(PromptAddAttachment.promptAddAttachment(newDoc.AccountDocumentID, this)==DialogResult.OK)
                        {
                            attachments.setDocument(newDoc.AccountDocumentID);
                            return;
                        }
                    }
                    if (docID == -1)
                        ResetControls();
                    else
                        Close();
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        public bool ValidateCashAccount()
        {
            if (cmbCasher.SelectedIndex != -1)
            {
                return true;
            }
            else
            {
                MessageBox.ShowErrorMessage("No cash accounts found.\nPlease configure cash account, set balance to it in opening transactions setup and try again!");
                return false;
            }
        }


        public bool ValidateBankAccount()
        {

            if (cmbBankAccount.SelectedIndex == -1)
            {
                MessageBox.ShowErrorMessage("No bank accounts found.\nPlease configure bank account and try again!");
                return false;
            }
            else
            {
                return true;
            }
        }

        private void cmbCasher_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetCashAccountBalance();

        }

        private void SetCashAccountBalance()
        {
            if (cmbCasher.SelectedIndex != -1)
            {
                double balance = GetCashAccountBalance();
                labelCashBalance.Text = String.Format("Current Balance: {0} Birr", TSConstants.FormatBirr(balance));
            }
            else
                labelCashBalance.Text = "Current Balance: 0.00 Birr";
        }
        private double GetCashAccountBalance()
        {
            double balance = INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(cmbCasher.cashCsAccountID,dateDeposit.DateTime);
            return balance;
        }

        private void cmbBankAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetBankAccountBalance();
        }

        private void SetBankAccountBalance()
        {
            if (cmbBankAccount.SelectedIndex != -1)
            {
                double balance = GetBankAccountBalance();
                labelBankBalance.Text = String.Format("Current Balance: {0} Birr", TSConstants.FormatBirr(balance));

            }
            else
                labelBankBalance.Text = "Current Balance: 0.00 Birr";
        }

        private double GetBankAccountBalance()
        {
            double balance = INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(cmbBankAccount.mainCsAccountID,dateDeposit.DateTime);
            return balance;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dateDeposit_DateTimeChanged(object sender, EventArgs e)
        {
            SetCashAccountBalance();
            SetBankAccountBalance();
        }
    }
}
