using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHBankDeposit : bERPClientDocumentHandler
    {
        public DCHBankDeposit()
            : base(typeof(BankDepositForm))
        {
        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            BankDepositForm f = (BankDepositForm)editor;
            f.LoadData((BankDepositDocument)doc);
        }
    }
}
