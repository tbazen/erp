﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;

namespace BIZNET.iERP.Client
{
    public partial class BankServiceChargePayment : XtraForm
    {
        BankServiceChargePaymentDocument m_doc = null;
        private bool newDocument=true;
        private string paymentSource;
        private int m_ServiceChargeAccountID;
        protected IAccountingClient _client;
        protected ActivationParameter _activation;
        public BankServiceChargePayment(IAccountingClient client,ActivationParameter activation)
        {
            InitializeComponent();
            voucher.setKeys(typeof(BankServiceChargePaymentDocument), "voucher");
            _activation = activation;
            _client = client;

           if (!Globals.DocumentWithPaymentTypeActivated)
               Globals.CurrentPaymentMethod = BizNetPaymentMethod.None;

            txtAmount.LostFocus += Control_LostFocus;
            txtReference.LostFocus += Control_LostFocus;
            InitializeValidationRules();
        }
        
        private void PrepareControlsLayoutForCurrentPaymentMethod()
        {
            switch (Globals.CurrentPaymentMethod)
            {
                case BizNetPaymentMethod.Check:
                    paymentSource = " by Check";
                    break;

                case BizNetPaymentMethod.None:
                    break;
                default:
                    break;
            }                       
        }
        private void SetFormTitle()
        {
            string title = "Bank Service Charge Payment ";

            BankAccountInfo bankAccount = iERPTransactionClient.GetBankAccount(Globals.CurrentMainAccountID);

            lblTitle.Text = "<size=14><color=#360087><b>" + title + "</b></color></size>" +
                "<size=14><color=#C30013><b>" + "from " + bankAccount.bankName + "</b></color></size>" +
                "<size=14><color=#360087><b>" + paymentSource + "</b></color></size>";
        }
        private void SetBankServiceChargeBalance()
        {
            //int serviceChargeAccountID = (int)iERPTransactionClient.GetSystemParameters(new string[] { "bankFeeExpenseAccount" })[0];
            BankAccountInfo bankInfo = iERPTransactionClient.GetBankAccount(Globals.CurrentMainAccountID);
            int serviceChargeAccountID = bankInfo.bankServiceChargeCsAccountID;
            CostCenterAccount serviceChargeAccount = INTAPS.Accounting.Client.AccountingClient.GetCostCenterAccount(serviceChargeAccountID);
            double balance = 0;
            if (serviceChargeAccount != null)
            {
                m_ServiceChargeAccountID = serviceChargeAccount.id;
                balance = INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(m_ServiceChargeAccountID, datePayment.DateTime);
                labelServiceChargePaymentBal.Text = TSConstants.FormatBirr(balance) + " Birr";
            }
            else
            {
                labelServiceChargePaymentBal.Text = "No service charge account found to calculate balance";
            }
        }
       
        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validationStaffReceivable.Validate(control);
        }

        private void InitializeValidationRules()
        {
            InitializePaymentAmountValidation();
        }

        private void InitializePaymentAmountValidation()
        {
            NonEmptyNumericValidationRule depositAmountValidation = new NonEmptyNumericValidationRule();
            depositAmountValidation.ErrorText = "Payment amount cannot be blank and cannot contain a value of zero";
            depositAmountValidation.ErrorType = ErrorType.Default;
            validationStaffReceivable.SetValidationRule(txtAmount, depositAmountValidation);
        }
        
        internal void LoadData(BankServiceChargePaymentDocument serviceChargePaymentDoc)
        {
            m_doc = serviceChargePaymentDoc;
            if (serviceChargePaymentDoc == null)
            {
                ResetControls();
            }
            else
            {
                Globals.CurrentPaymentMethod = m_doc.paymentMethod;
                Globals.CurrentMainAccountID = m_doc.assetAccountID;
                Globals.DocumentWithPaymentTypeActivated = false;
                SetBankServiceChargeBalance();
                PrepareControlsLayoutForCurrentPaymentMethod();
                newDocument = false;
                PopulateControlsForUpdate(serviceChargePaymentDoc);
                SetBankAccountBalance();
            }
        }

        private void ResetControls()
        {
            datePayment.DateTime = DateTime.Now;
            if (m_doc != null)
                SetBankServiceChargeBalance();
            txtAmount.Text = "";
            txtReference.Text = "";
            txtNote.Text = "";
            voucher.setReference(-1, null);
        }

        private void PopulateControlsForUpdate(BankServiceChargePaymentDocument serviceChargePaymentDoc)
        {
            datePayment.DateTime = serviceChargePaymentDoc.DocumentDate;
            txtReference.Text = serviceChargePaymentDoc.PaperRef;
            txtAmount.Text = TSConstants.FormatBirr(serviceChargePaymentDoc.amount);
            txtNote.Text = serviceChargePaymentDoc.ShortDescription;
            voucher.setReference(serviceChargePaymentDoc.AccountDocumentID, serviceChargePaymentDoc.voucher);
        }

        private void SetBankAccountBalance()
        {
            double balance = INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(Globals.CurrentMainAccountID,datePayment.DateTime);
            labelBankBalance.Text = String.Format("{0} Birr", TSConstants.FormatBirr(balance));
        }
     
        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (validationStaffReceivable.Validate())
                {
                    if (!ValidateAccounts())
                        return;
                    int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;
                    BankServiceChargePaymentDocument newDoc = new BankServiceChargePaymentDocument();
                    newDoc.AccountDocumentID = docID;
                    newDoc.DocumentDate = datePayment.DateTime;
                    newDoc.PaperRef = txtReference.Text;
                    newDoc.paymentMethod = Globals.CurrentPaymentMethod;
                    newDoc.assetAccountID = Globals.CurrentMainAccountID;
                    newDoc.amount = double.Parse(txtAmount.Text);
                    newDoc.ShortDescription = txtNote.Text;
                    newDoc.voucher = voucher.getReference();
                    
                    _client.PostGenericDocument(newDoc);
                    string message = docID == -1 ? "Payment successfully saved!" : "Payment successfully updated!";
                    MessageBox.ShowSuccessMessage(message);
                    Close();
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }
            
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private bool ValidateAccounts()
        {
            bool validation=false;
            switch (Globals.CurrentPaymentMethod)
            {
                case BizNetPaymentMethod.Check:
                case BizNetPaymentMethod.CPOFromBankAccount:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    validation = ValidateBankAccount();
                    break;
                case BizNetPaymentMethod.None:
                    break;
                default:
                    break;
            }
            return validation;
        }

        public bool ValidateBankAccount()
        {

            double balance = GetBankAccountBalance();
                if (!newDocument)
                {
                    if (m_doc.assetAccountID== Globals.CurrentMainAccountID)
                        balance = (m_doc.amount + balance) - double.Parse(txtAmount.Text);
                }

                if (balance == 0)
                {
                    if (newDocument)
                    {
                        MessageBox.ShowErrorMessage("The selected bank account balance is empty.\nPlease make sure that the account balance has been set on opening transactions setup and try again!");
                        return false;
                    }
                    else
                        return true;
                }
                else
                {
                    if (newDocument)
                    {
                        if (double.Parse(txtAmount.Text) > balance)
                        {
                            MessageBox.ShowErrorMessage("The withdrawal amount cannot be greater than the selected bank account balance");
                            return false;
                        }
                        else
                            return true;
                    }
                    else
                    {
                        if (m_doc.assetAccountID== Globals.CurrentMainAccountID)
                        {
                            if (balance < 0)
                            {
                                MessageBox.ShowErrorMessage("The withdrawal amount cannot be greater than the selected bank account balance");
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        else
                        {
                            if (double.Parse(txtAmount.Text) > balance)
                            {
                                MessageBox.ShowErrorMessage("The withdrawal amount cannot be greater than the selected bank account balance");
                                return false;
                            }
                            else
                                return true;
                        }
                    }
                }
           
        }
      
        private double GetBankAccountBalance()
        {
            return INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(Globals.CurrentMainAccountID, datePayment.DateTime);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BankServiceChargePayment_Load(object sender, EventArgs e)
        {
            if (Globals.DocumentWithPaymentTypeActivated)
            {
                SetBankServiceChargeBalance();
                SetBankAccountBalance();
                PrepareControlsLayoutForCurrentPaymentMethod();
            }
            SetFormTitle();
        }

        private void datePayment_DateTimeChanged(object sender, EventArgs e)
        {
            SetBankAccountBalance();
            SetBankServiceChargeBalance();
        }

    }
}
