using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHBankServiceChargePayment : bERPClientDocumentHandler
    {
        public DCHBankServiceChargePayment()
            : base(typeof(BankServiceChargePayment))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            BankServiceChargePayment f = (BankServiceChargePayment)editor;

            f.LoadData((BankServiceChargePaymentDocument)doc);
        }
    }
}
