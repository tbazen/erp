﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;

namespace BIZNET.iERP.Client
{
    public partial class BankToBankForm : XtraForm
    {
        BankToBankDocument m_doc = null;
        private bool newDocument=true;
        IAccountingClient _client;
        ActivationParameter _activation;

        public BankToBankForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            voucher.setKeys(typeof(BankToBankDocument), "voucher");

            _client = client;
            _activation = activation;
            dateDocument.LostFocus += Control_LostFocus;
            txtAmount.LostFocus += Control_LostFocus;
            InitializeValidationRules();
            SetFromBankAccountBalance();
            SetToBankAccountBalance();
        }

        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validationBankToBank.Validate(control);
        }

        private void InitializeValidationRules()
        {
            NonEmptyNumericValidationRule amountValidatin = new NonEmptyNumericValidationRule();
            amountValidatin.ErrorText = "Amount cannot be blank and cannot contain a value of zero";
            amountValidatin.ErrorType = ErrorType.Default;
            validationBankToBank.SetValidationRule(txtAmount, amountValidatin);
        }
        internal void LoadData(BankToBankDocument bankDocument)
        {
            m_doc = bankDocument;
            if (bankDocument == null)
            {
                ResetControls();
            }
            else
            {
                PopulateControlsForUpdate(bankDocument);
                newDocument = false;
                SetToBankAccountBalance();
                SetFromBankAccountBalance();
            }
        }

        private void ResetControls()
        {
            dateDocument.DateTime = DateTime.Now;
            txtAmount.Text = "";
            txtNote.Text = "";
            voucher.setReference(-1, null);
        }

        private void PopulateControlsForUpdate(BankToBankDocument bankDocument)
        {
            dateDocument.DateTime = bankDocument.DocumentDate;
            cmbFromBankAccount.mainCsAccountID = bankDocument.fromBankAccountID;
            cmbToBankAccount.mainCsAccountID = bankDocument.toBankAccountID;
            txtAmount.Text = TSConstants.FormatBirr(bankDocument.amount);
            voucher.setReference(bankDocument.AccountDocumentID, bankDocument.voucher);
            txtNote.Text = bankDocument.ShortDescription;
        }


        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (validationBankToBank.Validate())
                {
                    if (cmbToBankAccount.mainCsAccountID == cmbFromBankAccount.mainCsAccountID)
                    {
                        MessageBox.ShowErrorMessage("Select different bank account");
                        return;
                    }
                    if (voucher.getReference() == null)
                    {
                        MessageBox.ShowErrorMessage("Enter valid reference");
                        return;
                    }
                    if (!ValidateToBankAccount() || !ValidateFromBankAccount())
                        return;
                    int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;
                    BankToBankDocument newDoc = new BankToBankDocument();
                    newDoc.AccountDocumentID = docID;
                    newDoc.DocumentDate = dateDocument.DateTime;
                    newDoc.toBankAccountID = cmbToBankAccount.mainCsAccountID;
                    newDoc.fromBankAccountID = cmbFromBankAccount.mainCsAccountID;
                    newDoc.amount = double.Parse(txtAmount.Text);
                    newDoc.voucher = voucher.getReference();
                    newDoc.ShortDescription = txtNote.Text;

                    _client.PostGenericDocument(newDoc);
                    if (!(_client is DocumentScheduler))
                    {
                        string message = docID == -1 ? "Bank transfer successfully saved!" : "Bank transfer successfully updated!";
                        MessageBox.ShowSuccessMessage(message);
                    }
                    if (docID == -1)
                        ResetControls();
                    else
                        Close();
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        public bool ValidateToBankAccount()
        {
            if (cmbToBankAccount.SelectedIndex == -1)
            {
                MessageBox.ShowErrorMessage("No destination bank accounts found.");
                return false;
            }
            else
            {
                return true;
            }
        
        }

        public bool ValidateFromBankAccount()
        {

            if (cmbFromBankAccount.SelectedIndex != -1)
            {
                return true;
            }
            else
            {
                MessageBox.ShowErrorMessage("No bank accounts found.");
                return false;
            }
        }

        private void SetToBankAccountBalance()
        {
            if (cmbToBankAccount.SelectedIndex != -1)
            {
                double balance = GetToBankAccountBalance();
                labelToBankBalance.Text = String.Format("Current Balance: {0} Birr", TSConstants.FormatBirr(balance));
            }
            else
                labelToBankBalance.Text = "Current Balance: 0.00 Birr";
        }
        
        private void SetFromBankAccountBalance()
        {
            if (cmbFromBankAccount.SelectedIndex != -1)
            {
                double balance = GetFromBankAccountBalance();
                labelFromBankBalance.Text = String.Format("Current Balance: {0} Birr", TSConstants.FormatBirr(balance));

            }
            else
                labelFromBankBalance.Text = "Current Balance: 0.00 Birr";
        }

        private double GetFromBankAccountBalance()
        {
            return INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(cmbFromBankAccount.mainCsAccountID,dateDocument.DateTime);
        }

        private double GetToBankAccountBalance()
        {
            return INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(cmbToBankAccount.mainCsAccountID, dateDocument.DateTime);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbFromBankAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetFromBankAccountBalance();
        }

        private void cmbToBankAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetToBankAccountBalance();
        }

        private void dateDocument_DateTimeChanged(object sender, EventArgs e)
        {
            SetFromBankAccountBalance();
            SetToBankAccountBalance();
        }
    }
}
