using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHBankToBank : bERPClientDocumentHandler
    {
        public DCHBankToBank()
            : base(typeof(BankToBankForm))
        {
        }
        
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            BankToBankForm f = (BankToBankForm)editor;
            f.LoadData((BankToBankDocument)doc);
        }
    }
}
