﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;

namespace BIZNET.iERP.Client
{
    public partial class BankWithdrawalForm : XtraForm
    {
        BankWithdrawalDocument m_doc = null;
        private bool newDocument=true;
        protected IAccountingClient _client;
        protected ActivationParameter _activation;
        public BankWithdrawalForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            voucher.setKeys(typeof(BankWithdrawalDocument), "voucher");

            _activation = activation;
            _client = client;
            dateWithdrawal.LostFocus += Control_LostFocus;
            txtAmount.LostFocus += Control_LostFocus;
            txtReference.LostFocus += Control_LostFocus;
            InitializeValidationRules();
        }

        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validationBankDeposit.Validate(control);
        }

        private void InitializeValidationRules()
        {
            NonEmptyNumericValidationRule depositAmountValidation = new NonEmptyNumericValidationRule();
            depositAmountValidation.ErrorText = "Deposit amount cannot be blank and cannot contain a value of zero";
            depositAmountValidation.ErrorType = ErrorType.Default;
            validationBankDeposit.SetValidationRule(txtAmount, depositAmountValidation);
        }

        internal void LoadData(BankWithdrawalDocument bankWithdrawalDocument)
        {
            m_doc = bankWithdrawalDocument;
            if (bankWithdrawalDocument == null)
            {
                ResetControls();
            }
            else
            {
                PopulateControlsForUpdate(bankWithdrawalDocument);
                newDocument = false;
                SetCashAccountBalance();
                SetBankAccountBalance();
            }
        }
        private void ResetControls()
        {
            dateWithdrawal.DateTime = DateTime.Now;
            cmbCasher.SelectedIndex = cmbCasher.Properties.Items.Count > 0 ? 0 : -1;
            cmbCasher_SelectedIndexChanged(null, null);
            cmbBankAccount.SelectedIndex = cmbBankAccount.Properties.Items.Count > 0 ? 0 : -1;
            cmbBankAccount_SelectedIndexChanged(null, null);
            txtAmount.Text = "";
            txtReference.Text = "";
            txtNote.Text = "";
            voucher.setReference(-1, null);
        }
        private void PopulateControlsForUpdate(BankWithdrawalDocument bankWithdrawalDocument)
        {
            dateWithdrawal.DateTime = bankWithdrawalDocument.DocumentDate;
            voucher.setReference(bankWithdrawalDocument.AccountDocumentID, bankWithdrawalDocument.voucher);
            cmbCasher.cashCsAccountID = bankWithdrawalDocument.casherAccountID;
            cmbBankAccount.mainCsAccountID = bankWithdrawalDocument.bankAccountID;
            txtAmount.Text = TSConstants.FormatBirr(bankWithdrawalDocument.amount);
            txtReference.Text = bankWithdrawalDocument.chekNumber;
            txtNote.Text = bankWithdrawalDocument.ShortDescription;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (validationBankDeposit.Validate())
                {
                    if (voucher.getReference() == null)
                    {
                        MessageBox.ShowErrorMessage("Enter valid reference");
                        return;
                    }
                    if (!ValidateCashAccount() || !ValidateBankAccount())
                        return;
                    int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;

                    BankWithdrawalDocument newDoc = new BankWithdrawalDocument();
                    newDoc.AccountDocumentID = docID;
                    newDoc.DocumentDate = dateWithdrawal.DateTime;
                    newDoc.casherAccountID = cmbCasher.cashCsAccountID;
                    newDoc.bankAccountID = cmbBankAccount.mainCsAccountID;
                    newDoc.amount = double.Parse(txtAmount.Text);
                    newDoc.chekNumber = txtReference.Text;
                    newDoc.ShortDescription = txtNote.Text;
                    newDoc.voucher = voucher.getReference(); 
                    _client.PostGenericDocument(newDoc);
                    if (!(_client is DocumentScheduler))
                    {
                        string message = docID == -1 ? "Bank Withdrawal successfully saved!" : "Bank Withdrawal successfully updated!";
                        MessageBox.ShowSuccessMessage(message);
                    }
                   if (docID == -1)
                       ResetControls();
                   else
                       Close();
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        public bool ValidateCashAccount()
        {
            if (cmbCasher.SelectedIndex == -1)
            {
                MessageBox.ShowErrorMessage("No cash accounts found.\nPlease configure cash account and try again!");
                return false;
            }
            else
            {
                return true;
            }
        
        }


        public bool ValidateBankAccount()
        {

            if (cmbBankAccount.SelectedIndex != -1)
                return true;
            else
            {
                MessageBox.ShowErrorMessage("No bank accounts found.\nPlease configure bank account, set balance to it in opening transactions setup and try again!");
                return false;
            }
        }

        private void cmbCasher_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetCashAccountBalance();
        }

        private void SetCashAccountBalance()
        {
            if (cmbCasher.SelectedIndex != -1)
            {
                double balance = GetCashAccountBalance();
                labelCashBalance.Text = String.Format("Current Balance: {0} Birr", TSConstants.FormatBirr(balance));
            }
            else
                labelCashBalance.Text = "Current Balance: 0.00 Birr";
        }
        private double GetCashAccountBalance()
        {
            return INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(cmbCasher.cashCsAccountID, dateWithdrawal.DateTime);
        }

        private void cmbBankAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetBankAccountBalance();
        }

        private void SetBankAccountBalance()
        {
            if (cmbBankAccount.SelectedIndex != -1)
            {
                double balance = GetBankAccountBalance();
                labelBankBalance.Text = String.Format("Current Balance: {0} Birr", TSConstants.FormatBirr(balance));

            }
            else
                labelBankBalance.Text = "Current Balance: 0.00 Birr";
        }

        private double GetBankAccountBalance()
        {
            return INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(cmbBankAccount.mainCsAccountID, dateWithdrawal.DateTime);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dateWithdrawal_DateTimeChanged(object sender, EventArgs e)
        {
            SetBankAccountBalance();
            SetCashAccountBalance();
        }
    }
}
