using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHBankWithdrawal : bERPClientDocumentHandler
    {
        public DCHBankWithdrawal()
            : base(typeof(BankWithdrawalForm))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            BankWithdrawalForm f = (BankWithdrawalForm)editor;
            f.LoadData((BankWithdrawalDocument)doc);
        }

    }
}
