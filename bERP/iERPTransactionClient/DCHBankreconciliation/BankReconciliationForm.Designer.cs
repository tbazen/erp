﻿namespace BIZNET.iERP.Client
{
    partial class BankReconciliationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BankReconciliationForm));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.labelPreviousReconciliation = new DevExpress.XtraEditors.LabelControl();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.textBalance = new DevExpress.XtraEditors.TextEdit();
            this.periodSelector = new BIZNET.iERP.Client.PeriodSelector();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutPeriodSelector = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBankBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.browserController = new INTAPS.UI.HTML.BowserController();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.controlBrowser = new INTAPS.UI.HTML.ControlBrowser();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.dxValidationProvider = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBalance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodSelector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodSelector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBankBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.browserController.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.labelPreviousReconciliation);
            this.layoutControl1.Controls.Add(this.gridControl);
            this.layoutControl1.Controls.Add(this.textBalance);
            this.layoutControl1.Controls.Add(this.periodSelector);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(652, 387);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // labelPreviousReconciliation
            // 
            this.labelPreviousReconciliation.Location = new System.Drawing.Point(12, 42);
            this.labelPreviousReconciliation.Name = "labelPreviousReconciliation";
            this.labelPreviousReconciliation.Size = new System.Drawing.Size(496, 29);
            this.labelPreviousReconciliation.StyleController = this.layoutControl1;
            this.labelPreviousReconciliation.TabIndex = 9;
            this.labelPreviousReconciliation.Text = "##";
            // 
            // gridControl
            // 
            this.gridControl.Location = new System.Drawing.Point(12, 91);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(628, 284);
            this.gridControl.TabIndex = 4;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsCustomization.AllowColumnMoving = false;
            this.gridView.OptionsCustomization.AllowFilter = false;
            this.gridView.OptionsCustomization.AllowGroup = false;
            this.gridView.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView.OptionsCustomization.AllowSort = false;
            this.gridView.OptionsView.ShowGroupPanel = false;
            // 
            // textBalance
            // 
            this.textBalance.Location = new System.Drawing.Point(409, 15);
            this.textBalance.Name = "textBalance";
            this.textBalance.Properties.EditFormat.FormatString = "N";
            this.textBalance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textBalance.Size = new System.Drawing.Size(228, 20);
            this.textBalance.StyleController = this.layoutControl1;
            this.textBalance.TabIndex = 3;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "This value is not valid";
            this.dxValidationProvider.SetValidationRule(this.textBalance, conditionValidationRule1);
            this.textBalance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBalance_KeyDown);
            this.textBalance.Validated += new System.EventHandler(this.textBalance_Validated);
            // 
            // periodSelector
            // 
            this.periodSelector.Location = new System.Drawing.Point(93, 15);
            this.periodSelector.Name = "periodSelector";
            this.periodSelector.PeriodType = "AP_Accounting_Month";
            this.periodSelector.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.periodSelector.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.periodSelector.Size = new System.Drawing.Size(228, 20);
            this.periodSelector.StyleController = this.layoutControl1;
            this.periodSelector.TabIndex = 1;
            this.periodSelector.SelectedIndexChanged += new System.EventHandler(this.periodSelector_SelectedIndexChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutPeriodSelector,
            this.layoutControlItem4,
            this.layoutBankBalance,
            this.layoutControlItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(652, 387);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutPeriodSelector
            // 
            this.layoutPeriodSelector.Control = this.periodSelector;
            this.layoutPeriodSelector.CustomizationFormText = "Reconcile Upto:";
            this.layoutPeriodSelector.Location = new System.Drawing.Point(0, 0);
            this.layoutPeriodSelector.Name = "layoutPeriodSelector";
            this.layoutPeriodSelector.Size = new System.Drawing.Size(316, 30);
            this.layoutPeriodSelector.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutPeriodSelector.Text = "Reconcile Upto:";
            this.layoutPeriodSelector.TextSize = new System.Drawing.Size(75, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControl;
            this.layoutControlItem4.CustomizationFormText = "Transactions";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 63);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(400, 40);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(632, 304);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "Transactions";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(75, 13);
            // 
            // layoutBankBalance
            // 
            this.layoutBankBalance.Control = this.textBalance;
            this.layoutBankBalance.CustomizationFormText = "Bank Balance";
            this.layoutBankBalance.Location = new System.Drawing.Point(316, 0);
            this.layoutBankBalance.Name = "layoutBankBalance";
            this.layoutBankBalance.Size = new System.Drawing.Size(316, 30);
            this.layoutBankBalance.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutBankBalance.Text = "Bank Balance";
            this.layoutBankBalance.TextSize = new System.Drawing.Size(75, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.labelPreviousReconciliation;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(500, 50);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(500, 30);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(632, 33);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // browserController
            // 
            this.browserController.AutoSize = false;
            this.browserController.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1});
            this.browserController.Location = new System.Drawing.Point(0, 0);
            this.browserController.Name = "browserController";
            this.browserController.ShowBackForward = false;
            this.browserController.ShowRefresh = false;
            this.browserController.Size = new System.Drawing.Size(319, 38);
            this.browserController.TabIndex = 10;
            this.browserController.Text = "bowserController1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(113, 35);
            this.toolStripButton1.Text = "Save Reconciliation";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // controlBrowser
            // 
            this.controlBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlBrowser.Location = new System.Drawing.Point(0, 38);
            this.controlBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.controlBrowser.Name = "controlBrowser";
            this.controlBrowser.Size = new System.Drawing.Size(319, 349);
            this.controlBrowser.StyleSheetFile = "berp.css";
            this.controlBrowser.TabIndex = 0;
            // 
            // timer
            // 
            this.timer.Interval = 200;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.layoutControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.controlBrowser);
            this.splitContainerControl1.Panel2.Controls.Add(this.browserController);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(976, 387);
            this.splitContainerControl1.SplitterPosition = 652;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // BankReconciliationForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(976, 387);
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "BankReconciliationForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bank Reconciliation Form";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBalance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.periodSelector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPeriodSelector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBankBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.browserController.ResumeLayout(false);
            this.browserController.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private PeriodSelector periodSelector;
        private DevExpress.XtraLayout.LayoutControlItem layoutPeriodSelector;
        private INTAPS.UI.HTML.ControlBrowser controlBrowser;
        private DevExpress.XtraEditors.TextEdit textBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutBankBalance;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.LabelControl labelPreviousReconciliation;
        private INTAPS.UI.HTML.BowserController browserController;
        public DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.Timer timer;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidationProvider;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
    }
}