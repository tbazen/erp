﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Threading;
using DevExpress.XtraLayout.Utils;

namespace BIZNET.iERP.Client
{
    public partial class BankReconciliationForm : DevExpress.XtraEditors.XtraForm
    {
        BankReconciliationDocument _pervReconciliation = null;
        BankReconciliationDocument _doc = null;
        AccountDocument[] _tranDocuments = null;
        List<AccountTransaction> _trans = null;
        DCHBankreconciliation.BankReconciliationData.LedgerDataTable _ledger;
        protected IAccountingClient _client;
        protected ActivationParameter _activation;
        BankAccountInfo _bankAccount;
        Thread _updateThread = null;
        bool _updateReport = false;
        string _html = null;
        public BankReconciliationForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            _activation = activation;
            _client = client;
            initializeGrid();

            setBankAcount(_activation.assetAccountID);
            loadLedger();
            
            browserController.SetBrowser(controlBrowser);
        }

        private void startUpdateThread()
        {
            _updateReport = false;
            _updateThread = new Thread(delegate()
                {
                    _html = buildHTML();
                });
            _updateThread.Start();
            timer.Enabled = true;
        }

        private void updatePage()
        {
            controlBrowser.LoadTextPage("Bank Reconciliation", buildHTML());
        }

        private void setBankAcount(int bankID)
        {
            if (bankID == -1)
                _bankAccount = null;
            else
                _bankAccount=iERPTransactionClient.GetBankAccount(bankID);
        }

        void loadLedger()
        {
            _ledger.Clear();
            if(_bankAccount==null)
            {
                _pervReconciliation=null;
                _trans = null;
                _tranDocuments = null;
                textBalance.Enabled = false;
                layoutBankBalance.Text = "Select Period";
                labelPreviousReconciliation.Text="";
                return;
            }
            
            textBalance.Enabled = true;
            layoutBankBalance.Text = "Bank balance as of " + AccountBase.FormatDate(periodSelector.selectedPeriod.toDate.AddDays(-1));
            if (_pervReconciliation == null)
                labelPreviousReconciliation.Text = "First reconciliation";
            else
                labelPreviousReconciliation.Text = "Bank Account Reconciled upto " + AccountBase.FormatDate(_pervReconciliation.DocumentDate.AddDays(-1)); 
            int N;
            AccountBalance bal;
            _trans = new List<AccountTransaction>();
            if(_pervReconciliation!=null)
                foreach (int t in _pervReconciliation.outstandingTransactions)
                {
                    AccountTransaction transaction = AccountingClient.GetTransaction(t);
                    if (transaction != null)
                        _trans.Add(transaction);
                }

            _trans.AddRange(AccountingClient.GetTransactions(_bankAccount.mainCsAccount, 0
                , true,
                _pervReconciliation == null ? periodSelector.selectedPeriod.fromDate.AddDays(-1) : _pervReconciliation.DocumentDate
                , periodSelector.selectedPeriod.toDate, 0, -1, out N, out bal));
            int rowIndex = 0;
            _tranDocuments = new AccountDocument[_trans.Count];
            foreach (AccountTransaction t in _trans)
            {
                AccountDocument doc = AccountingClient.GetAccountDocument(t.documentID, true);
                string r;
                string note;
                if (doc == null)
                {
                    note = r = "[System error docID:" + t.documentID;
                }
                else
                {
                    if ((doc is IStandardAttributeDocument) && ((IStandardAttributeDocument)doc).hasInstrument)
                        r = ((IStandardAttributeDocument)doc).getInstrument(false);
                    else
                        r = doc.PaperRef;
                    note = AccountingClient.GetDocumentTypeByID(doc.DocumentTypeID).name + "\n" + doc.ShortDescription;
                }
                _tranDocuments[rowIndex] = doc;
                bool reconciled=true;
                if (_doc == null)
                    reconciled = false;
                else 
                    foreach(int tranID in _doc.outstandingTransactions)
                        if (tranID == t.ID)
                        {
                            reconciled = false;
                            break;
                        }
                _ledger.AddLedgerRow(t.ID, AccountBase.FormatDate(t.execTime), r,
                    AccountBase.FormatAmount(AccountBase.AmountGreater(t.Amount, 0) ? t.Amount : 0)
                    , AccountBase.FormatAmount(AccountBase.AmountLess(t.Amount, 0) ? -t.Amount : 0)
                    , AccountBase.FormatBalance(t.NewDebit - t.NewCredit), reconciled,note, t.documentID
                    ,rowIndex
                    );
                rowIndex++;

            }
        }
        public string buildHTML()
        {
            try
            {
                double bankBalance;
                if (!double.TryParse(textBalance.Text, out bankBalance))
                    bankBalance = 0;
                bERPHtmlTable table = new bERPHtmlTable();
                bERPHtmlTableRowGroup body = new bERPHtmlTableRowGroup(TableRowGroupType.body);
                table.groups.Add(body);
                List<DCHBankreconciliation.BankReconciliationData.LedgerRow> outstandingWithdrawal = new List<DCHBankreconciliation.BankReconciliationData.LedgerRow>();
                List<DCHBankreconciliation.BankReconciliationData.LedgerRow> outstandingDeposit = new List<DCHBankreconciliation.BankReconciliationData.LedgerRow>();
                double outstandingDepositTotal = 0;
                double outstandingWithdrawalTotal = 0;
                if (_ledger != null)
                {
                    foreach (DCHBankreconciliation.BankReconciliationData.LedgerRow row in _ledger)
                    {
                        AccountTransaction tran = _trans[row.rowIndex];
                        if (!row.Reconciled)
                        {
                            if (AccountBase.AmountGreater(tran.Amount, 0))
                            {
                                outstandingDeposit.Add(row);
                                outstandingDepositTotal += tran.Amount;
                            }
                            else if (AccountBase.AmountLess(tran.Amount, 0))
                            {
                                outstandingWithdrawal.Add(row);
                                outstandingWithdrawalTotal += -tran.Amount;
                            }
                        }
                    }

                    bERPHtmlBuilder.htmlAddRow(body
                        , new bERPHtmlTableCell("Balance as per Bank, " + AccountBase.FormatDate(periodSelector.selectedPeriod.toDate.AddDays(-1)))
                        , new bERPHtmlTableCell("")
                        , new bERPHtmlTableCell(AccountBase.FormatBalance(bankBalance), "CurrencyCell")).css = "Section_Header_1";

                    bERPHtmlBuilder.htmlAddRow(body, new bERPHtmlTableCell("Add: Deposit in Transit", 3)).css = "Section_Header_2";

                    int i = 0;
                    foreach (DCHBankreconciliation.BankReconciliationData.LedgerRow row in outstandingDeposit)
                    {
                        AccountDocument doc = _tranDocuments[row.rowIndex];
                        string reference = doc == null ? "" : doc.PaperRef;
                        bERPHtmlBuilder.htmlAddRow(body
                            , new bERPHtmlTableCell(reference + " issued on " + AccountBase.FormatDate(doc.DocumentDate))
                            , new bERPHtmlTableCell(AccountBase.FormatBalance(_trans[row.rowIndex].Amount), "CurrencyCell")
                            , new bERPHtmlTableCell("")).css = i % 2 == 0 ? "Even_row" : "Odd_row";
                        i++;
                    }
                    bERPHtmlBuilder.htmlAddRow(body
                        , new bERPHtmlTableCell("")
                        , new bERPHtmlTableCell("")
                        , new bERPHtmlTableCell(TDType.body, AccountBase.FormatBalance(outstandingDepositTotal), "CurrencyCell")).css = "Section_Header_2";
                    bERPHtmlBuilder.htmlAddRow(body
                        , new bERPHtmlTableCell("")
                        , new bERPHtmlTableCell("")
                        , new bERPHtmlTableCell(AccountBase.FormatBalance(bankBalance + outstandingDepositTotal), "CurrencyCell")).css = "Section_Header_1";


                    bERPHtmlBuilder.htmlAddRow(body, new bERPHtmlTableCell("Less: Outstanding withdrawal", 3)).css = "Section_Header_2";
                    i = 0;
                    foreach (DCHBankreconciliation.BankReconciliationData.LedgerRow row in outstandingWithdrawal)
                    {
                        AccountDocument doc = _tranDocuments[row.rowIndex];
                        string reference;
                        if (doc == null)
                            reference = "[System Error]";
                        else
                        {
                            IStandardAttributeDocument atr = doc as IStandardAttributeDocument;
                            if (atr == null || !atr.hasInstrument)
                                reference = doc.PaperRef;
                            else
                                reference = atr.getInstrument(false);
                        }
                        bERPHtmlBuilder.htmlAddRow(body
                            , new bERPHtmlTableCell(reference + " issued on " + AccountBase.FormatDate(doc.DocumentDate))
                            , new bERPHtmlTableCell(AccountBase.FormatBalance(-_trans[row.rowIndex].Amount), "CurrencyCell")
                            , new bERPHtmlTableCell("")).css = i % 2 == 0 ? "Even_row" : "Odd_row";
                        i++;
                    }

                    bERPHtmlBuilder.htmlAddRow(body
                        , new bERPHtmlTableCell("")
                        , new bERPHtmlTableCell("")
                        , new bERPHtmlTableCell(TDType.body, AccountBase.FormatBalance(outstandingWithdrawalTotal), "CurrencyCell")).css = "Section_Header_2";
                    double adjustedBankBalance = bankBalance + outstandingDepositTotal - outstandingWithdrawalTotal;
                    bERPHtmlBuilder.htmlAddRow(body
                        , new bERPHtmlTableCell("Adjusted Bank Balance")
                        , new bERPHtmlTableCell("")
                        , new bERPHtmlTableCell(AccountBase.FormatBalance(adjustedBankBalance), "CurrencyCell")).css = "Section_Header_1";

                    double bookBalance = AccountingClient.GetNetBalanceAsOf(_bankAccount.mainCsAccount, periodSelector.selectedPeriod.toDate);
                    bERPHtmlBuilder.htmlAddRow(body
                        , new bERPHtmlTableCell("Balance per the Books")
                        , new bERPHtmlTableCell("")
                        , new bERPHtmlTableCell(AccountBase.FormatBalance(bookBalance), "CurrencyCell")).css = "Section_Header_1";
                    if (AccountBase.AmountEqual(bookBalance, adjustedBankBalance))
                        bERPHtmlBuilder.htmlAddRow(body, new bERPHtmlTableCell("Reconciled", 3)
                                ).css = "Section_Header_1";
                    else
                        bERPHtmlBuilder.htmlAddRow(body
                            , new bERPHtmlTableCell("Unreconciled Amount")
                            , new bERPHtmlTableCell(AccountBase.FormatBalance(bookBalance - adjustedBankBalance), 2)
                                ).css = "Section_Header_1";

                    
                }
                
                StringBuilder builder = new StringBuilder();
                generateTitle(builder);
                table.build(builder);
                return builder.ToString();
            }
            catch (Exception ex)
            {
                return System.Web.HttpUtility.HtmlEncode(ex.Message + "\n" + ex.StackTrace);
            }
        }
        bool _ignoreEvent = false;
        void loadSavedData(BankReconciliationDocument doc)
        {
            _ignoreEvent = true;
            try
            {
                _doc = doc;
                _pervReconciliation = doc.previosReconciationDocument == -1 ? null : AccountingClient.GetAccountDocument(doc.previosReconciationDocument, true) as BankReconciliationDocument;
                periodSelector.setByDate(_doc.DocumentDate.AddDays(-1));
                textBalance.Text = AccountBase.FormatBalance(_doc.bankBalance);
            }
            finally
            {
                _ignoreEvent = false;
            }

        }
        public void LoadData(BankReconciliationDocument doc)
        {
            if (doc == null)
            {
                periodSelector_SelectedIndexChanged(null, null);
                return;
            }

            setBankAcount(doc.bankID);
            layoutPeriodSelector.Visibility = LayoutVisibility.Never;
            loadSavedData(doc);
            loadLedger();
            updatePage();
        }

        void initializeGrid()
        {
            _ledger = new DCHBankreconciliation.BankReconciliationData.LedgerDataTable();
            gridControl.DataSource = _ledger;

            gridView.Columns[_ledger.IDColumn.Ordinal].Visible = false;
            gridView.Columns[_ledger.DateColumn.Ordinal].OptionsColumn.AllowEdit = false;
            gridView.Columns[_ledger.ReferenceColumn.Ordinal].OptionsColumn.AllowEdit = false;
            gridView.Columns[_ledger.DebitColumn.Ordinal].OptionsColumn.AllowEdit = false;
            gridView.Columns[_ledger.CreditColumn.Ordinal].OptionsColumn.AllowEdit = false;
            gridView.Columns[_ledger.BalanceColumn.Ordinal].OptionsColumn.AllowEdit = false;
            gridView.Columns[_ledger.NoteColumn.Ordinal].OptionsColumn.AllowEdit = false;
            gridView.Columns[_ledger.ReconciledColumn.Ordinal].OptionsColumn.AllowEdit = true;
            gridView.Columns[_ledger.documentIDColumn.Ordinal].Visible = false;
            gridView.Columns[_ledger.rowIndexColumn.Ordinal].Visible = false;

            gridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridView_CellValueChanged);
            gridView.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridView_CellValueChanging);
        }

        void gridView_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            try
            {
                _ledger[gridView.GetDataSourceRowIndex(e.RowHandle)].Reconciled = (bool)e.Value;
                updatePage();
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            
        }

        private void periodSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent)
                return;
            BankReconciliationDocument rec = iERPTransactionClient.getLastBankReconciliation(_bankAccount.mainCsAccount, periodSelector.selectedPeriod.toDate);
            if (rec != null)
            {
                if (periodSelector.selectedPeriod.InPeriod(rec.DocumentDate.AddDays(-1)))
                {
                    loadSavedData(rec);
                }
                else
                {
                    _pervReconciliation = rec;
                    _doc = null;
                }
            }
            else
            {
                _pervReconciliation = null;
                _doc = null;
            }
            loadLedger();
            updatePage();

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (!_updateThread.IsAlive)
            {
                timer.Enabled = false;
                controlBrowser.LoadTextPage("Bank Reconciliation", _html);
                if (_updateReport)
                    startUpdateThread();
            }

        }

        private void textBalance_Validated(object sender, EventArgs e)
        {
            updatePage();
        }
        private void generateTitle(StringBuilder builder)
        {
            TSConstants.addCompanyNameLine(iERPTransactionClient.GetCompanyProfile(), builder);
            builder.Append("<h2>Bank Reconciliation</h2>");
            builder.Append("<h2>" + System.Web.HttpUtility.HtmlEncode(_bankAccount.BankBranchAccount) + "</h2>");
            builder.Append(string.Format("<span class='SubTitle'><b>As of</b> <u>{0}</u></span>", AccountBase.FormatDate(periodSelector.selectedPeriod.toDate.AddDays(-1))));
        }

        private void textBalance_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                updatePage();
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if(!dxValidationProvider.Validate())
                    return;
                BankReconciliationDocument rec=new BankReconciliationDocument();
                rec.DocumentDate = periodSelector.selectedPeriod.toDate;
                rec.PaperRef = periodSelector.selectedPeriod.name;
                rec.bankBalance=double.Parse(textBalance.Text);
                rec.bankID = _bankAccount.mainCsAccount;
                List<int> unreconciled=new List<int>();
                foreach(DCHBankreconciliation.BankReconciliationData.LedgerRow row in _ledger)
                {
                    if(!row.Reconciled)
                        unreconciled.Add(row.ID);
                }
                rec.outstandingTransactions=unreconciled.ToArray();
                rec.AccountDocumentID = _doc == null ? -1 : _doc.AccountDocumentID;
                rec.ShortDescription = "";
                rec.AccountDocumentID=_client.PostGenericDocument(rec);
                _doc = rec;
                MessageBox.ShowSuccessMessage("Reconciliation succesfully saved.");
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }
    }

}