﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIZNET.iERP.Client
{
   
    class DCHBankReconciliation : bERPClientDocumentHandler
    {
        public DCHBankReconciliation()
            : base(typeof(BankReconciliationForm))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            BankReconciliationForm f = (BankReconciliationForm)editor;
            f.LoadData((BankReconciliationDocument)doc);
        }
    }
}
