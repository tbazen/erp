﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Columns;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using DevExpress.XtraEditors.Controls;

namespace BIZNET.iERP.Client
{
    public partial class BookClosingForm : XtraForm
    {
        private BookClosingDocument m_doc;
        IAccountingClient _accountingClient;

        public BookClosingForm(IAccountingClient client, ActivationParameter pars)
        {
            InitializeComponent();
            voucher.setKeys(typeof(BookClosingDocument), "voucher");
            _accountingClient = client;
            dateDocument.DateTime = DateTime.Now;
            
        }







        internal void LoadData(BookClosingDocument adjustmentDoc)
        {
            m_doc = adjustmentDoc;

            if (adjustmentDoc != null)
            {
                PopulateGridForUpdate(adjustmentDoc);
            }
            else
            {
            }
        }

        private void PopulateGridForUpdate(BookClosingDocument adjustmentDoc)
        {
            dateDocument.DateTime = adjustmentDoc.DocumentDate;
            txtAdditionalInfo.Text = adjustmentDoc.ShortDescription;
            voucher.setReference(adjustmentDoc.AccountDocumentID, adjustmentDoc.voucher);
        }

       


      

        private void btnPost_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validateControls.Validate())
                    return;
                if (voucher.getReference() == null)
                {
                    MessageBox.ShowErrorMessage("Enter valid reference");
                    return;
                }
                if(txtAdditionalInfo.Text.Trim().Equals(""))
                {
                    MessageBox.ShowErrorMessage("Type description for the book closing transaction");
                    return;
                }

                int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;
                BookClosingDocument adjDoc = new BookClosingDocument();
                adjDoc.AccountDocumentID = docID;
                adjDoc.voucher = voucher.getReference();
                adjDoc.ShortDescription = txtAdditionalInfo.Text;
                adjDoc.DocumentDate = dateDocument.DateTime;
                if (adjDoc.IsFutureDate)
                {
                    if (MessageBox.ShowWarningMessage("Are you sure you want to post future transaction?") != DialogResult.Yes)
                        return;
                    adjDoc.scheduled = true;
                    adjDoc.materialized = false;
                }
                _accountingClient.PostGenericDocument(adjDoc);
                string message = docID == -1 ? "Book closing successfully posted!" : "Book closing successfully updated";
                MessageBox.ShowSuccessMessage(message);
                if (docID == -1)
                {
                    ResetControls();
                }
                else
                {
                    Close();
                }
            }
            catch (Exception ex)
            {
                string exception = ex.InnerException == null ? ex.Message : ex.InnerException.Message + "\n" + ex.Message;
                MessageBox.ShowErrorMessage(exception);
            }
        }
        
        private void ResetControls()
        {
            dateDocument.DateTime = DateTime.Now;
            voucher.setReference(-1, null);
            txtAdditionalInfo.Text = "";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}