using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
namespace BIZNET.iERP.Client
{
    public class DCHBookClosing : bERPClientDocumentHandler
    {
        public DCHBookClosing()
            : base(typeof(BookClosingForm))
        {
        }
        
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            BookClosingForm f = (BookClosingForm)editor;
            f.LoadData((BookClosingDocument)doc);
        }

    }
}
