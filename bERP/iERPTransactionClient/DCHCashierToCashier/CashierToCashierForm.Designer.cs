﻿namespace BIZNET.iERP.Client
{
    partial class CashierToCashierForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cmbFromCashier = new BIZNET.iERP.Client.CashAccountPlaceholder();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.voucher = new BIZNET.iERP.Client.DocumentSerialPlaceHolder();
            this.dateDocument = new BIZNET.iERP.Client.BNDualCalendar();
            this.labelToCashBalance = new DevExpress.XtraEditors.LabelControl();
            this.labelFromCashBalance = new DevExpress.XtraEditors.LabelControl();
            this.txtNote = new DevExpress.XtraEditors.MemoEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.cmbToCashier = new BIZNET.iERP.Client.CashAccountPlaceholder();
            this.txtAmount = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutFromCashierBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlToCashierBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.validationCashierToCashier = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.cmbFromCashier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbToCashier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFromCashierBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlToCashierBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationCashierToCashier)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbFromCashier
            // 
            this.cmbFromCashier.Location = new System.Drawing.Point(123, 98);
            this.cmbFromCashier.Name = "cmbFromCashier";
            this.cmbFromCashier.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbFromCashier.Properties.ImmediatePopup = true;
            this.cmbFromCashier.Size = new System.Drawing.Size(355, 20);
            this.cmbFromCashier.StyleController = this.layoutControl1;
            this.cmbFromCashier.TabIndex = 1;
            this.cmbFromCashier.SelectedIndexChanged += new System.EventHandler(this.cmbFromCashier_SelectedIndexChanged);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.voucher);
            this.layoutControl1.Controls.Add(this.dateDocument);
            this.layoutControl1.Controls.Add(this.labelToCashBalance);
            this.layoutControl1.Controls.Add(this.labelFromCashBalance);
            this.layoutControl1.Controls.Add(this.txtNote);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.cmbFromCashier);
            this.layoutControl1.Controls.Add(this.cmbToCashier);
            this.layoutControl1.Controls.Add(this.txtAmount);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(180, 335, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(706, 302);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // voucher
            // 
            this.voucher.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.voucher.Appearance.Options.UseBackColor = true;
            this.voucher.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.voucher.Location = new System.Drawing.Point(7, 23);
            this.voucher.Name = "voucher";
            this.voucher.Size = new System.Drawing.Size(344, 68);
            this.voucher.TabIndex = 10;
            // 
            // dateDocument
            // 
            this.dateDocument.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateDocument.Location = new System.Drawing.Point(358, 26);
            this.dateDocument.Name = "dateDocument";
            this.dateDocument.ShowEthiopian = true;
            this.dateDocument.ShowGregorian = true;
            this.dateDocument.ShowTime = true;
            this.dateDocument.Size = new System.Drawing.Size(338, 63);
            this.dateDocument.TabIndex = 9;
            this.dateDocument.VerticalLayout = true;
            this.dateDocument.DateTimeChanged += new System.EventHandler(this.dateDocument_DateTimeChanged);
            // 
            // labelToCashBalance
            // 
            this.labelToCashBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelToCashBalance.Location = new System.Drawing.Point(485, 125);
            this.labelToCashBalance.Name = "labelToCashBalance";
            this.labelToCashBalance.Size = new System.Drawing.Size(214, 26);
            this.labelToCashBalance.StyleController = this.layoutControl1;
            this.labelToCashBalance.TabIndex = 8;
            this.labelToCashBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // labelFromCashBalance
            // 
            this.labelFromCashBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFromCashBalance.Location = new System.Drawing.Point(485, 95);
            this.labelFromCashBalance.Name = "labelFromCashBalance";
            this.labelFromCashBalance.Size = new System.Drawing.Size(214, 26);
            this.labelFromCashBalance.StyleController = this.layoutControl1;
            this.labelFromCashBalance.TabIndex = 7;
            this.labelFromCashBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(123, 188);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(573, 66);
            this.txtNote.StyleController = this.layoutControl1;
            this.txtNote.TabIndex = 6;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.buttonCancel);
            this.panelControl1.Controls.Add(this.buttonOk);
            this.panelControl1.Location = new System.Drawing.Point(7, 261);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(692, 34);
            this.panelControl1.TabIndex = 5;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(628, 6);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(59, 23);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(550, 6);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(62, 23);
            this.buttonOk.TabIndex = 4;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // cmbToCashier
            // 
            this.cmbToCashier.Location = new System.Drawing.Point(123, 128);
            this.cmbToCashier.Name = "cmbToCashier";
            this.cmbToCashier.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbToCashier.Properties.ImmediatePopup = true;
            this.cmbToCashier.Size = new System.Drawing.Size(355, 20);
            this.cmbToCashier.StyleController = this.layoutControl1;
            this.cmbToCashier.TabIndex = 1;
            this.cmbToCashier.SelectedIndexChanged += new System.EventHandler(this.cmbToCashier_SelectedIndexChanged);
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(123, 158);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtAmount.Properties.Mask.EditMask = "#,########0.00;";
            this.txtAmount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtAmount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtAmount.Size = new System.Drawing.Size(573, 20);
            this.txtAmount.StyleController = this.layoutControl1;
            this.txtAmount.TabIndex = 2;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem7,
            this.layoutControlItem6,
            this.layoutFromCashierBalance,
            this.layoutControlToCashierBalance,
            this.layoutControlItem8,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(706, 302);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.cmbFromCashier;
            this.layoutControlItem2.CustomizationFormText = "Cash Source:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 88);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(129, 30);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(478, 30);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem2.Text = "From Cashier Account:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(109, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.cmbToCashier;
            this.layoutControlItem3.CustomizationFormText = "Bank Account:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 118);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(478, 30);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "To Cashier Account:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(109, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtAmount;
            this.layoutControlItem4.CustomizationFormText = "Amount:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 148);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(696, 30);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem4.Text = "Amount:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(109, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.panelControl1;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 254);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(696, 38);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtNote;
            this.layoutControlItem6.CustomizationFormText = "Note:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 178);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(696, 76);
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem6.Text = "Note:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(109, 13);
            // 
            // layoutFromCashierBalance
            // 
            this.layoutFromCashierBalance.Control = this.labelFromCashBalance;
            this.layoutFromCashierBalance.CustomizationFormText = "layoutControlCashBalance";
            this.layoutFromCashierBalance.Location = new System.Drawing.Point(478, 88);
            this.layoutFromCashierBalance.MaxSize = new System.Drawing.Size(218, 30);
            this.layoutFromCashierBalance.MinSize = new System.Drawing.Size(218, 30);
            this.layoutFromCashierBalance.Name = "layoutFromCashierBalance";
            this.layoutFromCashierBalance.Size = new System.Drawing.Size(218, 30);
            this.layoutFromCashierBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutFromCashierBalance.Text = "layoutFromCashierBalance";
            this.layoutFromCashierBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutFromCashierBalance.TextToControlDistance = 0;
            this.layoutFromCashierBalance.TextVisible = false;
            // 
            // layoutControlToCashierBalance
            // 
            this.layoutControlToCashierBalance.Control = this.labelToCashBalance;
            this.layoutControlToCashierBalance.CustomizationFormText = "layoutControlItem8";
            this.layoutControlToCashierBalance.Location = new System.Drawing.Point(478, 118);
            this.layoutControlToCashierBalance.MaxSize = new System.Drawing.Size(218, 30);
            this.layoutControlToCashierBalance.MinSize = new System.Drawing.Size(218, 30);
            this.layoutControlToCashierBalance.Name = "layoutControlToCashierBalance";
            this.layoutControlToCashierBalance.Size = new System.Drawing.Size(218, 30);
            this.layoutControlToCashierBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlToCashierBalance.Text = "layoutControlItem8";
            this.layoutControlToCashierBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlToCashierBalance.TextToControlDistance = 0;
            this.layoutControlToCashierBalance.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.voucher;
            this.layoutControlItem8.CustomizationFormText = "Document Reference";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(348, 88);
            this.layoutControlItem8.Text = "Document Reference";
            this.layoutControlItem8.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(109, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dateDocument;
            this.layoutControlItem1.CustomizationFormText = "Date:";
            this.layoutControlItem1.Location = new System.Drawing.Point(348, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 88);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(119, 88);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(348, 88);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Text = "Date:";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(109, 13);
            // 
            // CashierToCashierForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 302);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "CashierToCashierForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cashier To Cashier Form";
            ((System.ComponentModel.ISupportInitialize)(this.cmbFromCashier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbToCashier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFromCashierBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlToCashierBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationCashierToCashier)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CashAccountPlaceholder cmbFromCashier;
        private CashAccountPlaceholder cmbToCashier;
        private DevExpress.XtraEditors.TextEdit txtAmount;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.MemoEdit txtNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.LabelControl labelFromCashBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutFromCashierBalance;
        private DevExpress.XtraEditors.LabelControl labelToCashBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlToCashierBalance;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validationCashierToCashier;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private BIZNET.iERP.Client.BNDualCalendar dateDocument;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DocumentSerialPlaceHolder voucher;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
    }
}