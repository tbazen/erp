﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;

namespace BIZNET.iERP.Client
{
    public partial class CashierToCashierForm : DevExpress.XtraEditors.XtraForm
    {
        CashierToCashierDocument m_doc = null;
        private bool newDocument = true;
        IAccountingClient _client;
        ActivationParameter _activation;
        public CashierToCashierForm(IAccountingClient client,ActivationParameter activation)
        {
            InitializeComponent();
            voucher.setKeys(typeof(CashierToCashierDocument), "voucher");

            _client = client;
            _activation = activation;
            dateDocument.LostFocus += Control_LostFocus;
            txtAmount.LostFocus += Control_LostFocus;
            InitializeValidationRules();
            SetFromCashAccountBalance();
            SetToCashAccountBalance();
        }

        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validationCashierToCashier.Validate(control);
        }

        private void InitializeValidationRules()
        {
            NonEmptyNumericValidationRule amountValidation = new NonEmptyNumericValidationRule();
            amountValidation.ErrorText = "Amount cannot be blank and cannot contain a value of zero";
            amountValidation.ErrorType = ErrorType.Default;
            validationCashierToCashier.SetValidationRule(txtAmount, amountValidation);
        }

        internal void LoadData(CashierToCashierDocument cashierToCashierDoc)
        {
            m_doc = cashierToCashierDoc;
            if (cashierToCashierDoc == null)
            {
                ResetControls();
            }
            else
            {
                PopulateControlsForUpdate(cashierToCashierDoc);
                newDocument = false;
                SetFromCashAccountBalance();
                SetToCashAccountBalance();
            }
        }
        private void ResetControls()
        {
            dateDocument.DateTime = DateTime.Now;
            txtAmount.Text = "";
            txtNote.Text = "";
            voucher.setReference(-1, null);
        }
        private void PopulateControlsForUpdate(CashierToCashierDocument cashierToCashierDoc)
        {
            dateDocument.DateTime = cashierToCashierDoc.DocumentDate;
            cmbFromCashier.cashCsAccountID = cashierToCashierDoc.fromCashierAccountID;
            cmbToCashier.cashCsAccountID = cashierToCashierDoc.toCashierAccountID;
            txtAmount.Text = TSConstants.FormatBirr(cashierToCashierDoc.amount);
            voucher.setReference(cashierToCashierDoc.AccountDocumentID, cashierToCashierDoc.voucher);
            txtNote.Text = cashierToCashierDoc.ShortDescription;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (validationCashierToCashier.Validate())
                {
                    if (cmbToCashier.cashCsAccountID== cmbFromCashier.cashCsAccountID)
                    {
                        MessageBox.ShowErrorMessage("Select different cash accounts");
                        return;
                    }
                    if (voucher.getReference() == null)
                    {
                        MessageBox.ShowErrorMessage("Enter valid reference");
                        return;
                    }
                    if (!ValidateFromCashierAccount() || !ValidateToCashierAccount())
                        return;
                    int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;
                    m_doc = new CashierToCashierDocument();
                    m_doc.AccountDocumentID = docID;
                    m_doc.DocumentDate = dateDocument.DateTime;
                    m_doc.fromCashierAccountID = cmbFromCashier.cashCsAccountID;
                    m_doc.toCashierAccountID = cmbToCashier.cashCsAccountID;
                    m_doc.amount = double.Parse(txtAmount.Text);
                    m_doc.voucher = voucher.getReference();
                    m_doc.ShortDescription = txtNote.Text;
                    
                    _client.PostGenericDocument(m_doc);
                    if (!(_client is DocumentScheduler))
                    {
                        string message = docID == -1 ? "Cash transfer successfully saved!" : "Cash transfer successfully updated!";
                        MessageBox.ShowSuccessMessage(message);
                    }
                    if (docID == -1)
                        ResetControls();
                    else
                        Close();
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        public bool ValidateFromCashierAccount()
        {
            if (cmbFromCashier.SelectedIndex != -1)
            {
                return true;
            }
            else
            {
                MessageBox.ShowErrorMessage("No cash accounts found.");
                return false;
            }
        }


        public bool ValidateToCashierAccount()
        {

            if (cmbToCashier.SelectedIndex == -1)
            {
                MessageBox.ShowErrorMessage("No destination cash accounts found.");
                return false;
            }
            else
            {
                return true;
            }
        }

       
        private void SetFromCashAccountBalance()
        {
            if (cmbFromCashier.SelectedIndex != -1)
            {
                double balance = GetFromCashAccountBalance();
                labelFromCashBalance.Text = String.Format("Current Balance: {0} Birr", TSConstants.FormatBirr(balance));
            }
            else
                labelFromCashBalance.Text = "Current Balance: 0.00 Birr";
        }
        private double GetFromCashAccountBalance()
        {
            return INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(cmbFromCashier.cashCsAccountID, dateDocument.DateTime);
        }

       
        private void SetToCashAccountBalance()
        {
            if (cmbToCashier.SelectedIndex != -1)
            {
                double balance = GetToCashAccountBalance();
                labelToCashBalance.Text = String.Format("Current Balance: {0} Birr", TSConstants.FormatBirr(balance));

            }
            else
                labelToCashBalance.Text = "Current Balance: 0.00 Birr";
        }

        private double GetToCashAccountBalance()
        {
            return INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(cmbToCashier.cashCsAccountID, dateDocument.DateTime);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbToCashier_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetToCashAccountBalance();
        }

        private void cmbFromCashier_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetFromCashAccountBalance();
        }

        private void dateDocument_DateTimeChanged(object sender, EventArgs e)
        {
            SetToCashAccountBalance();
            SetFromCashAccountBalance();

        }
    }
}
