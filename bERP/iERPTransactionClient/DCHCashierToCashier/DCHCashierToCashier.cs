using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHCashierToCashier : bERPClientDocumentHandler
    {
        public DCHCashierToCashier()
            : base(typeof(CashierToCashierForm))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            CashierToCashierForm f = (CashierToCashierForm)editor;
            f.LoadData((CashierToCashierDocument)doc);
        }
    }
}
