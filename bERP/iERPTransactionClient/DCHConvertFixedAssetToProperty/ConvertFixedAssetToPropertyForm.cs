﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    public partial class ConvertFixedAssetToPropertyForm : DevExpress.XtraEditors.XtraForm
    {
        ConvertFixedAssetToPropertyDocument _doc = null;
        DataTable gridData;
        Dictionary<DataRow, ConvertFixedAssetToPropertyDocument.ConvertFixedAssetToPropertyItem> items;
        IAccountingClient _client;
        public ConvertFixedAssetToPropertyForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            _client = client;
            initializeGrid();
            updateButtonStatus();
        }
        void initializeGrid()
        {
            items = new Dictionary<DataRow, ConvertFixedAssetToPropertyDocument.ConvertFixedAssetToPropertyItem>();
            gridData = new DataTable("data");
            gridData.Columns.Add("Item Code");
            gridData.Columns.Add("Property Code");
            gridData.Columns.Add("Type");
            gridData.Columns.Add("Name");
            gridData.Columns.Add("Aquired Year");
            gridData.Columns.Add("Balance");
            gridControl.DataSource = gridData;
        }
        private void addToDataGrid(Property prop,object extraData)
        {
            PropertyType type = iERPTransactionClient.getPropertyType(prop.propertyTypeID);
            DataRow row= gridData.Rows.Add(prop.parentItemCode,
                prop.propertyCode,
                type == null ? null : type.name,
                prop.name,
                prop.acquireDate.ToShortDateString()
                );
            items.Add(row, new ConvertFixedAssetToPropertyDocument.ConvertFixedAssetToPropertyItem()
            {
                costCenterID=costCenterPlaceholder.GetAccountID(),
                property=prop,
                propertyData=extraData,
            }
            );
        }
        private void setToDataGridRow(DataRow row, Property prop, object extraData)
        {
            PropertyType type = iERPTransactionClient.getPropertyType(prop.propertyTypeID);
            row[0] = prop.parentItemCode;
            row[1] = prop.propertyCode;
            row[2] = type == null ? null : type.name;
            row[3] = prop.name;
            row[4] = prop.acquireDate.ToShortDateString();
            items[row]=new ConvertFixedAssetToPropertyDocument.ConvertFixedAssetToPropertyItem()
            {
                costCenterID = costCenterPlaceholder.GetAccountID(),
                property = prop,
                propertyData = extraData,
            };
        }
        void updateButtonStatus()
        {
            buttonDelete.Enabled = buttonEdit.Enabled = (gridView.GetSelectedRows().Length == 1);
        }

        internal void LoadData(ConvertFixedAssetToPropertyDocument doc)
        {
            _doc=doc;
            if (doc == null)
                return;
            dateDocument.DateTime = doc.DocumentDate;
            int costCenterID = -1;
            bool first = true;
            foreach (ConvertFixedAssetToPropertyDocument.ConvertFixedAssetToPropertyItem item in doc.items)
            {
                if (first)
                {
                    costCenterPlaceholder.SetByID(item.costCenterID);
                    costCenterID = item.costCenterID;
                    first=false;
                }
                else
                {
                    if (costCenterID != item.costCenterID)
                        throw new INTAPS.ClientServer.ServerUserMessage("It is not allowed to mix cost centers");
                }
                addToDataGrid(item.property, item.propertyData);
            }
        }

        private void gridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            updateButtonStatus();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            int costCenterID = costCenterPlaceholder.GetAccountID();
            if (costCenterID == -1)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Select cost center");
                return;
            }
            if (INTAPS.Accounting.Client.AccountingClient.GetAccount<CostCenter>(costCenterID).childCount>0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Select leaf cost center");
                return;
            }

            
            Property prop=new Property();
            RegisterProperty f=new RegisterProperty(prop,null,-1,DateTime.Now,false);
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                addToDataGrid(prop,f.extraData);
            }
        }

        private void gridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            updateButtonStatus();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            int [] rows=gridView.GetSelectedRows();
            if(rows.Length==0)
                return;
            DataRow row = gridView.GetDataRow(rows[0]);
            ConvertFixedAssetToPropertyDocument.ConvertFixedAssetToPropertyItem item = items[row];
            Property p = item.property.clone();
            RegisterProperty f = new RegisterProperty(p, item.propertyData,-1,DateTime.Now,false);
            if(f.ShowDialog(this)==DialogResult.OK)
            {
                setToDataGridRow(row,p, f.extraData);
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            foreach (int row in gridView.GetSelectedRows())
            {
                DataRow drow = gridView.GetDataRow(row);
                gridData.Rows.Remove(drow);
                items.Remove(drow);
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                gridData.AcceptChanges();
                int docID = _doc == null ? -1 : _doc.AccountDocumentID;
                _doc = new ConvertFixedAssetToPropertyDocument();
                _doc.AccountDocumentID = docID;
                _doc.DocumentDate = dateDocument.DateTime;
                _doc.items = new ConvertFixedAssetToPropertyDocument.ConvertFixedAssetToPropertyItem[gridData.Rows.Count];
                int i = 0;
                foreach (DataRow row in gridData.Rows)
                {
                    _doc.items[i++] = items[row];
                }
                _client.PostGenericDocument(_doc);
                Close();

            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}