using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHConvertFixedAssetToProperty : bERPClientDocumentHandler
    {
        public DCHConvertFixedAssetToProperty()
            : base(typeof(ConvertFixedAssetToPropertyForm))
        {
        }
        
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            ConvertFixedAssetToPropertyForm f = (ConvertFixedAssetToPropertyForm)editor;
            f.LoadData((ConvertFixedAssetToPropertyDocument)doc);
        }
    }
}
