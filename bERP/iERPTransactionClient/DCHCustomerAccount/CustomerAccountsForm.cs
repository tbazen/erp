﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;
using DevExpress.XtraEditors.Controls;

namespace BIZNET.iERP.Client
{
    abstract partial class CustomerAccountForm : XtraForm
    {
        //data
        protected TradeRelation _customer;
        protected bool _newDocument = true;
        protected CustomerAccountDocument _doc = null;
        protected TaxRates _taxRates;
        
        //UI logic
        protected IAccountingClient _client;
        protected ActivationParameter _activation;
        protected PaymentMethodAndBalanceController _paymentController;
        protected ScheduleController _schedule;
        
        public CustomerAccountForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            HideTaxCalculationControls();
            _taxRates = new TaxRates();
            _taxRates.CommonVATRate = (double)iERPTransactionClient.GetSystemParamter("CommonVATRate");
            _taxRates.WHServicesTaxableRate = (double)iERPTransactionClient.GetSystemParamter("WHServicesTaxableRate");
            _activation = activation;
            _client = client;

            setCustomer(activation.objectCode);
            _paymentController = new PaymentMethodAndBalanceController(paymentTypeSelector, layoutControlBankAccount, cmbBankAccount, layoutControlBankBalance, labelBankBalance, layoutControlCashAccount, cmbCasher, layoutControlCashBalance, labelCashBalance, layoutControlServiceCharge, layoutControlSlipRef, datePayment, _activation);
            _paymentController.AddCostCenterAccountItem(getCustomerAccountID(), labelCustomerPaymentBal);


            _schedule = new ScheduleController(buttonScheduleSettlement, labelSchedule, new int[]{
                    AccountingClient.GetDocumentTypeByType(typeof(CustomerAdvanceReturnDocument)).id
                    ,AccountingClient.GetDocumentTypeByType(typeof(Sell2Document)).id
            });
            _schedule.visible = false;
            _schedule.BeforeActivation += delegate(ScheduleController cont)
            {
                ActivationParameter ret = new ActivationParameter();
                ret.objectCode = _customer == null ? "" : _customer.Code;
                ret.paymentMethod = paymentTypeSelector.PaymentMethod;
                ret.assetAccountID = _paymentController.assetAccountID;
                return ret;
            };

            
            
            //AUDIT: Why is this not done through the VS from designer
            txtServiceCharge.LostFocus += Control_LostFocus;
            txtAmount.LostFocus += Control_LostFocus;
            txtReference.LostFocus += Control_LostFocus;
            PrepareControlsLayoutBasedOnDocumentType();
            InitializeValidationRules();
            ResetControls();
            SetFormTitle();
        }

        private void HideTaxCalculationControls()
        {
            layoutCalculateVAT.HideToCustomization();
            layoutCalculateWithholding.HideToCustomization();
            layoutGroupTaxCalculation.HideToCustomization();
        }
        protected abstract int relevantAccountID { get; }
        protected abstract void PrepareControlsLayoutBasedOnDocumentType();
        private int getCustomerAccountID()
        {
            if (_customer == null)
                return -1;
            CostCenterAccount csa = AccountingClient.GetCostCenterAccount(iERPTransactionClient.mainCostCenterID,
                relevantAccountID
                );
            if (csa == null)
                return -1;
            return csa.id;
        }

        private void setCustomer(string code)
        {
            if (code == null)
                _customer = null;
            else 
                _customer = iERPTransactionClient.GetCustomer(code);
        }
        
        //methods for handling variations between document types
        protected abstract CustomerAccountDocument CreateDocumentInstance();
        protected abstract string getTitle(bool transitive);
        protected virtual void setAdditionalFormData(CustomerAccountDocument customerAccountDocument)
        {

        }
        protected virtual void getAdditionalFormData(CustomerAccountDocument oldCustomerAccountDocument, CustomerAccountDocument customerAccountDocument)
        {

        }
        protected virtual bool validateData(int assetAccountID)
        {
            return true;
        }
        
        


        //methods for common logic
        bool isSchedulingMode
        {
            get
            {
                return _client is DocumentScheduler;
            }
        }
        
      
        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validationCustomerPayable.Validate(control);
        }

        private void InitializeValidationRules()
        {
            InitializeDepositAmountValidation();
        }

        private void InitializeDepositAmountValidation()
        {
            NonEmptyNumericValidationRule depositAmountValidation = new NonEmptyNumericValidationRule();
            depositAmountValidation.ErrorText = "Deposit amount cannot be blank and cannot contain a value of zero";
            depositAmountValidation.ErrorType = ErrorType.Default;
            validationCustomerPayable.SetValidationRule(txtAmount, depositAmountValidation);
        }

        protected virtual void loadAdditionlData(CustomerAccountDocument customerAccountDocument)
        {

        }
        internal  void LoadData(CustomerAccountDocument customerAccountDocument)
        {
            _doc = customerAccountDocument;
            if (customerAccountDocument == null)
            {
                ResetControls();
            }
            else
            {
                setCustomer(_doc.customerCode);
                _newDocument = false;
                try
                {
                    _paymentController.IgnoreEvents();
                    _paymentController.PaymentTypeSelector.PaymentMethod = _doc.paymentMethod;
                    PopulateControlsForUpdate(customerAccountDocument);
                    setAdditionalFormData(customerAccountDocument);
                    _paymentController.AddCostCenterAccountItem(getCustomerAccountID(), labelCustomerPaymentBal);
                    setAdditionalFormData(customerAccountDocument);
                }
                finally
                {
                    _paymentController.AcceptEvents();
                }
                SetFormTitle();
            }
        }

        
        private void ResetControls()
        {
            datePayment.DateTime = DateTime.Now;
            txtAmount.Text = "";            
            txtReference.Text =isSchedulingMode?"Unknown":"";
            txtNote.Text = "";
           // txtPaperReference.Text = isSchedulingMode ? "Unknown" : "";
            voucher.setReference(-1, null);
        }
        private void PopulateControlsForUpdate(CustomerAccountDocument customerAccountDocument)
        {
            datePayment.DateTime = customerAccountDocument.DocumentDate;
            //txtPaperReference.Text = customerAccountDocument.PaperRef;
            SetDocumentReference(customerAccountDocument);
            _paymentController.assetAccountID=customerAccountDocument.assetAccountID;
            txtAmount.Text = TSConstants.FormatBirr(customerAccountDocument.amount);
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(paymentTypeSelector.PaymentMethod))
                txtServiceCharge.Text = customerAccountDocument.serviceChargeAmount.ToString("N");
            SetReferenceNumber(_doc, false);
            txtNote.Text = customerAccountDocument.ShortDescription;
        }

        private void SetDocumentReference(CustomerAccountDocument customerAccountDocument)
        {
            if (customerAccountDocument is CustomerPayableDocument)
            {
                CustomerPayableDocument cusPayable=customerAccountDocument as CustomerPayableDocument;
                voucher.setReference(cusPayable.AccountDocumentID, cusPayable.voucher);
            }
            else if (customerAccountDocument is CustomerReceivableDocument)
            {
                CustomerReceivableDocument cusReceivable = customerAccountDocument as CustomerReceivableDocument;
                voucher.setReference(cusReceivable.AccountDocumentID, cusReceivable.voucher);
            }
            else if (customerAccountDocument is CustomerAdvanceReturnDocument)
            {
                CustomerAdvanceReturnDocument cusReturn = customerAccountDocument as CustomerAdvanceReturnDocument;
                voucher.setReference(cusReturn.AccountDocumentID, cusReturn.voucher);
            }
            else if (customerAccountDocument is BondPaymentDocument)
            {
                BondPaymentDocument bondPayment = customerAccountDocument as BondPaymentDocument;
                voucher.setReference(bondPayment.AccountDocumentID, bondPayment.voucher);
            }
            else if (customerAccountDocument is BondReturnDocument)
            {
                BondReturnDocument bondReturn = customerAccountDocument as BondReturnDocument;
                voucher.setReference(bondReturn.AccountDocumentID, bondReturn.voucher);
            }

        }


        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (_customer== null)
                {
                    MessageBox.ShowErrorMessage("BUG: TradeRelation not set");
                    return;
                }
                if (validationCustomerPayable.Validate())
                {
                    int docID = _doc == null ? -1 : _doc.AccountDocumentID;
                    CustomerAccountDocument newDoc = CreateDocumentInstance();
                    newDoc.AccountDocumentID = docID;
                    newDoc.customerCode = _customer.Code;
                    newDoc.DocumentDate = datePayment.DateTime;
                    newDoc.paymentMethod = paymentTypeSelector.PaymentMethod;
                    newDoc.assetAccountID = _paymentController.assetAccountID;
                    newDoc.amount = double.Parse(txtAmount.Text);
                    newDoc.checkNumber = newDoc.slipReferenceNo = newDoc.cpoNumber = this.txtReference.Text;
                    if (layoutControlSlipRef.Visibility == LayoutVisibility.Always)
                    {
                        bool updateDoc = _doc == null ? false : true;
                        SetReferenceNumber(newDoc, updateDoc);
                    }
                    newDoc.ShortDescription = txtNote.Text;
                    if (layoutControlServiceCharge.Visibility == LayoutVisibility.Always && txtServiceCharge.Text != "")
                    {
                        using (ServiceChargePayment serviceCharge = new ServiceChargePayment(_customer.Name))
                        {
                            serviceCharge.StartPosition = FormStartPosition.CenterScreen;
                            serviceCharge.ShowInTaskbar = false;
                            if (serviceCharge.ShowDialog() == DialogResult.OK)
                            {
                                newDoc.serviceChargePayer = serviceCharge.ServiceChargePayer;
                                double serviceChargeAmount = txtServiceCharge.Text == "" ? 0 : double.Parse(txtServiceCharge.Text);
                                newDoc.serviceChargeAmount = serviceChargeAmount;
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                    getAdditionalFormData(_doc, newDoc);

                    if (!isSchedulingMode && newDoc.IsFutureDate) //BETSU:replaced _doc with newdoc. It used to null for new doc
                    {
                        if (MessageBox.ShowWarningMessage("Are you sure you want to post future transaction?") != DialogResult.Yes)
                            return;
                        _doc.scheduled = true;
                        _doc.materialized = false;
                    }
                    _client.PostGenericDocument(newDoc);
                    if (!isSchedulingMode)
                    {
                        string message = docID == -1 ? "successfully saved!" : "successfully updated!";
                        MessageBox.ShowSuccessMessage(message);
                    }
                    _doc = newDoc;
                    Close();
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }
            
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }



        private void SetReferenceNumber(CustomerAccountDocument newDoc,bool updateDoc)
        {
            switch (paymentTypeSelector.PaymentMethod)
            {
                case BizNetPaymentMethod.Check:
                    if (_newDocument)
                        newDoc.checkNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = newDoc.checkNumber;
                        else
                            newDoc.checkNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.CPOFromBankAccount:
                    if (_newDocument)
                        newDoc.cpoNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = newDoc.cpoNumber;
                        else
                            newDoc.cpoNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.BankTransferFromCash:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    if (_newDocument)
                        newDoc.slipReferenceNo = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = newDoc.slipReferenceNo;
                        else
                            newDoc.slipReferenceNo = txtReference.Text;
                    break;
                default:
                    break;
            }
        }
        
        private double GetCashAccountBalance()
        {
            return INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(cmbCasher.cashCsAccountID, datePayment.DateTime);
        }

        

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void SetFormTitle()
        {
            string title = getTitle(true);
            string customerName = _customer == null ? "[Not Set]" : _customer.Name;
            lblTitle.Text = "<size=14><color=#360087><b>" + title + "</b></color></size>" +
                (_customer == null ? " [Not Set] " : "<size=14><color=#C30013><b>" + customerName+ "</b></color></size>") +
                "<size=14><color=#360087><b>" + _paymentController.PaymentSource+ "</b></color></size>";
        }


        

    }
}
