﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;
using DevExpress.XtraEditors.Controls;
using INTAPS.UI.ButtonGrid;

namespace BIZNET.iERP.Client
{
    partial class CustomerBondReturn : XtraForm
    {
        private bool newDocument = true;
        private BondReturnDocument m_doc = null;
        private TradeRelation _customer=null;
        private BondPaymentDocument _bondDocument = null;
        private bool _returnBond = true;
        
        
//UI logic
        protected IAccountingClient _client;
        protected ActivationParameter _activation;
        protected PaymentMethodAndBalanceController _paymentController;
        protected ScheduleController _schedule;

        public CustomerBondReturn(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            _activation = activation;
            _client = client;

            _paymentController = new PaymentMethodAndBalanceController(paymentTypeSelector, layoutControlBankAccount, cmbBankAccount, layoutControlBankBalance, labelBankBalance, layoutControlCashAccount, cmbCasher, layoutControlCashBalance, labelCashBalance, layoutControlServiceCharge, layoutControlSlipRef, datePayment, _activation);
            setCustomer(_activation.objectCode);
            setBondPaymentDocument(_activation.bondPaymentDocument);
            SetFormTitle();
            
            _paymentController.AddCostCenterAccountItem(getCustomerAccountID(), labelCustomerPaymentBal);

            txtPaperReference.LostFocus += Control_LostFocus;
            txtServiceCharge.LostFocus += Control_LostFocus;
            txtReference.LostFocus += Control_LostFocus;
        }
        private int getCustomerAccountID()
        {
            if (_customer == null)
                return -1;
            return AccountingClient.GetCostCenterAccount(iERPTransactionClient.mainCostCenterID,
                _customer.PaidBondAccountID
                ).id;

        }

        private void setCustomer(string code)
        {
            if (code == null)
                _customer = null;
            else
                _customer = iERPTransactionClient.GetCustomer(code);
        }
        private void setBondPaymentDocument(int docID)
        {
            if (docID == -1)
                _bondDocument = null;
            else
                _bondDocument = (BondPaymentDocument)INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(docID, true);
        }
        private void setBondPaymentDocument(BondPaymentDocument doc)
        {
            _bondDocument = doc;
        }
        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validationBondReturn.Validate(control);
        }
        
        internal void LoadData(BondReturnDocument bondReturnDocument)
        {
            m_doc = bondReturnDocument;
            if (bondReturnDocument == null)
            {
                btnUndoReturn.Visible = false;
                ResetControls();
            }
            else
            {
                
                btnUndoReturn.Visible = true;
                try
                {
                    _paymentController.IgnoreEvents();
                    _paymentController.PaymentTypeSelector.PaymentMethod = m_doc.paymentMethod;
                    setCustomer(m_doc.customerCode);
                    setBondPaymentDocument(m_doc.bondPaymentDocumentID);
                    newDocument = false;
                    PopulateControlsForUpdate(bondReturnDocument);
                }
                finally
                {
                    _paymentController.AcceptEvents();
                }
                SetFormTitle();
            }
        }
        private void ResetControls()
        {
            datePayment.DateTime = DateTime.Now;
            txtReference.Text = "";
            txtNote.Text = "";
            txtPaperReference.Text = txtServiceCharge.Text = "";
        }
        private void PopulateControlsForUpdate(BondReturnDocument bondReturnDocument)
        {
            datePayment.DateTime = bondReturnDocument.DocumentDate;
            txtPaperReference.Text = bondReturnDocument.PaperRef;
            _paymentController.assetAccountID = bondReturnDocument.assetAccountID;
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(_paymentController.PaymentTypeSelector.PaymentMethod))
                txtServiceCharge.Text = bondReturnDocument.serviceChargeAmount.ToString("N");
            SetReferenceNumber(false);
            txtNote.Text = bondReturnDocument.ShortDescription;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (validationBondReturn.Validate())
                {
                    int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;
                    m_doc = new BondReturnDocument();
                    m_doc.AccountDocumentID = docID;
                    m_doc.customerCode = _customer.Code;
                    m_doc.bondPaymentDocumentID = _bondDocument.AccountDocumentID;
                    m_doc.returnBond = _returnBond;
                    m_doc.DocumentDate = datePayment.DateTime;
                    m_doc.PaperRef = txtPaperReference.Text;
                    m_doc.paymentMethod = _paymentController.PaymentTypeSelector.PaymentMethod;
                    m_doc.assetAccountID = _paymentController.assetAccountID;
                    m_doc.amount = _bondDocument.amount;
                    if (PaymentMethodHelper.IsPaymentMethodWithReference(_paymentController.PaymentTypeSelector.PaymentMethod))
                    {
                        bool updateDoc = m_doc == null ? false : true;
                        SetReferenceNumber(updateDoc);
                    }
                    m_doc.ShortDescription = txtNote.Text;
                    if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(_paymentController.PaymentTypeSelector.PaymentMethod))
                    {

                        using (ServiceChargePayment serviceCharge = new ServiceChargePayment(_customer.Name))
                        {
                            serviceCharge.StartPosition = FormStartPosition.CenterScreen;
                            serviceCharge.ShowInTaskbar = false;
                            if (serviceCharge.ShowDialog() == DialogResult.OK)
                            {
                                m_doc.serviceChargePayer = serviceCharge.ServiceChargePayer;
                                double serviceChargeAmount = txtServiceCharge.Text == "" ? 0 : double.Parse(txtServiceCharge.Text);
                                m_doc.serviceChargeAmount = serviceChargeAmount;
                            }
                            else
                            {
                                return;
                            }
                        }
                    }

                    INTAPS.Accounting.Client.AccountingClient.PostGenericDocument(m_doc);
                    string message = docID == -1 ? "successfully saved!" : "successfully updated!";
                    MessageBox.ShowSuccessMessage(message);
                    if (docID == -1)
                        RefreshMainButtonGrid();
                    Close();

                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }
            
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }
        private static void RefreshMainButtonGrid()
        {
            foreach (Control control in INTAPS.UI.UIFormApplicationBase.MainForm.Controls)
            {
                if (control is SplitContainerControl)
                {
                    Control buttonGridControl = ((SplitContainerControl)control).Panel1.Controls[0];
                    ((ButtonGrid)buttonGridControl).UpdateLayoutInPlace();
                    break;
                }
            }
        }


        private void SetReferenceNumber(bool updateDoc)
        {
            switch (Globals.CurrentPaymentMethod)
            {
                case BizNetPaymentMethod.Check:
                    if (newDocument)
                        m_doc.checkNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.checkNumber;
                        else
                            m_doc.checkNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.CPOFromBankAccount:
                    if (newDocument)
                        m_doc.cpoNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.cpoNumber;
                        else
                            m_doc.cpoNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.BankTransferFromCash:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    if (newDocument)
                        m_doc.slipReferenceNo = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.slipReferenceNo;
                        else
                            m_doc.slipReferenceNo = txtReference.Text;
                    break;
                default:
                    break;
            }
        }

        
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        
        private void SetFormTitle()
        {
            string title = "Bond Return by ";
            string customerName = _customer == null ? " [Not Set] " : _customer.Name;
          
            lblTitle.Text = "<size=14><color=#360087><b>" + title + "</b></color></size>" +
                "<size=14><color=#C30013><b>" + customerName + "</b></color></size>" +
                "<size=14><color=#360087><b>" + _paymentController.PaymentSource + "</b></color></size>";
        }

            

        private void btnUndoReturn_Click(object sender, EventArgs e)
        {
            if (MessageBox.ShowWarningMessage("Are you sure you want to undo return of this Bond?") == DialogResult.Yes)
            {
                _returnBond = false;
                buttonOk_Click(null, null);
            }
        }

    }
}
