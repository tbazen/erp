﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.Accounting;
using System.Drawing;

namespace BIZNET.iERP.Client
{
    class CustomerAdvancePaymentForm:CustomerAccountForm
    {
        public CustomerAdvancePaymentForm(IAccountingClient client, ActivationParameter param)
            : base(client, param)
        {
            voucher.setKeys(typeof(CustomerPayableDocument), "voucher");
            txtAmount.TextChanged += new EventHandler(txtAmount_TextChanged);
            chkCalculateVAT.CheckedChanged += new EventHandler(chkCalculateVAT_CheckedChanged);
            chkCalculateWithholding.CheckedChanged += new EventHandler(chkCalculateWithholding_CheckedChanged);
        }

        void chkCalculateWithholding_CheckedChanged(object sender, EventArgs e)
        {
            layoutGroupTaxCalculation.RestoreFromCustomization(layoutControlAmount, DevExpress.XtraLayout.Utils.InsertType.Bottom);

            if (!chkCalculateVAT.Checked)
            {
                layoutVAT.HideToCustomization();
                layoutWithheldVAT.HideToCustomization();
            }
            if (!chkCalculateWithholding.Checked)
            {
                txtWithholding.Text = "";
                layoutWithholding.HideToCustomization();
                CalculateTotalCash();
            }
            else
            {
                if (layoutWithholding.IsHidden)
                    layoutWithholding.RestoreFromCustomization(chkCalculateVAT.Checked ? layoutVAT : layoutTotalCash, DevExpress.XtraLayout.Utils.InsertType.Top);
                txtWithholding.Text = (Math.Round(double.Parse(txtAmount.Text) * _taxRates.WHServicesTaxableRate, 2) * -1).ToString();
                CalculateTotalCash();
            }
            if (!chkCalculateVAT.Checked && !chkCalculateWithholding.Checked)
            {
                txtTotalCash.Text = "";
                layoutGroupTaxCalculation.HideToCustomization();
            }
        }

        void chkCalculateVAT_CheckedChanged(object sender, EventArgs e)
        {
            layoutGroupTaxCalculation.RestoreFromCustomization(layoutControlAmount, DevExpress.XtraLayout.Utils.InsertType.Bottom);
            
            if (!chkCalculateVAT.Checked)
            {
                txtVAT.Text = "";
                layoutVAT.HideToCustomization();
                layoutWithheldVAT.HideToCustomization();
                CalculateTotalCash();
            }
            else
            {
                if (layoutVAT.IsHidden)
                    layoutVAT.RestoreFromCustomization(chkCalculateWithholding.Checked?layoutWithholding: layoutTotalCash, DevExpress.XtraLayout.Utils.InsertType.Top);
                txtVAT.Text = Math.Round(double.Parse(txtAmount.Text) * _taxRates.CommonVATRate, 2).ToString();
                if (_customer.IsVatAgent)
                {
                    layoutWithheldVAT.RestoreFromCustomization(layoutVAT, DevExpress.XtraLayout.Utils.InsertType.Bottom);
                    txtWithheldVAT.Text = (Math.Round(double.Parse(txtAmount.Text) * _taxRates.CommonVATRate, 2) * -1).ToString();
                }
                CalculateTotalCash();
            }
            if (!chkCalculateWithholding.Checked)
                layoutWithholding.HideToCustomization();
            if (!chkCalculateVAT.Checked && !chkCalculateWithholding.Checked)
            {
                txtTotalCash.Text = "";
                layoutGroupTaxCalculation.HideToCustomization();
            }

        }

        private void CalculateTotalCash()
        {
            double amount, vat, withholding,withheldVat;
            amount = string.IsNullOrEmpty(txtAmount.Text) ? 0 : double.Parse(txtAmount.Text);
            vat = string.IsNullOrEmpty(txtVAT.Text) ? 0 : double.Parse(txtVAT.Text);
            withheldVat = string.IsNullOrEmpty(txtWithheldVAT.Text) ? 0 : double.Parse(txtWithheldVAT.Text);
            withholding = string.IsNullOrEmpty(txtWithholding.Text) ? 0 : double.Parse(txtWithholding.Text);
            double totalCash = amount + vat + withheldVat + withholding;
            txtTotalCash.Text = Math.Round(totalCash, 2).ToString();
        }

        void txtAmount_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtAmount.Text))
            {
                layoutCalculateVAT.HideToCustomization();
                layoutCalculateWithholding.HideToCustomization();
            }
            else
            {
                layoutCalculateVAT.RestoreFromCustomization(layoutControlAmount, DevExpress.XtraLayout.Utils.InsertType.Right);
                layoutCalculateWithholding.RestoreFromCustomization(layoutCalculateVAT, DevExpress.XtraLayout.Utils.InsertType.Right);
            }
        }
        protected override void getAdditionalFormData(CustomerAccountDocument oldcustomerAccountDocument, CustomerAccountDocument customerAccountDocument)
        {
            ((CustomerPayableDocument)customerAccountDocument).scheduledSettlement = _schedule.getScheduledDocuments();
            ((CustomerPayableDocument)customerAccountDocument).calculateVAT = chkCalculateVAT.Checked;
            ((CustomerPayableDocument)customerAccountDocument).calculateWithholding = chkCalculateWithholding.Checked;
            ((CustomerPayableDocument)customerAccountDocument).taxRates = _taxRates;
            ((CustomerPayableDocument)customerAccountDocument).totalCash = txtTotalCash.Text == "" || double.Parse(txtTotalCash.Text) == 0 ? double.Parse(txtAmount.Text) : double.Parse(txtTotalCash.Text);
            ((CustomerPayableDocument)customerAccountDocument).voucher = voucher.getReference();
        }
        protected override CustomerAccountDocument CreateDocumentInstance()
        {
           // return new CustomerPayableDocument();
            CustomerPayableDocument payable = new CustomerPayableDocument();
            payable.voucher = voucher.getReference();
            return payable;
        }
        protected override string getTitle(bool transitive)
        {
            return "Advance Payment by ";
        }
        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "TradeRelation Advance Payment Form";
            layoutControlCustomerBalance.Text = "Total Advances Collected:";
            _schedule.visible = true;
        }
        protected override bool validateData(int assetAccountID)
        {
            bool customerHasDebt = AccountBase.AmountGreater(_paymentController.getBalance(labelCustomerPaymentBal), 0);
            if (customerHasDebt)
            {
                MessageBox.ShowErrorMessage("You cannot receive advance from the customer now as the customer has pending credits that are not settled yet.\nPlease settle the credit and try again!");
                return false;
            }
            return true;
        }
        protected override void setAdditionalFormData(CustomerAccountDocument customerAccountDocument)
        {
            _schedule.setScheduledDocuments(((CustomerPayableDocument)customerAccountDocument).scheduledSettlement);
            _taxRates = ((CustomerPayableDocument)customerAccountDocument).taxRates;
            chkCalculateVAT.Checked = ((CustomerPayableDocument)customerAccountDocument).calculateVAT;
            chkCalculateWithholding.Checked = ((CustomerPayableDocument)customerAccountDocument).calculateWithholding;

            voucher.setReference(customerAccountDocument.AccountDocumentID, ((CustomerPayableDocument)customerAccountDocument).voucher);
        }
        
        protected override int relevantAccountID
        {
            get { return _customer == null ? -1 : _customer.PayableAccountID; }
        }
    }
    class CustomerReceivableForm : CustomerAccountForm
    {
        public CustomerReceivableForm(IAccountingClient client, ActivationParameter param)
            : base(client, param)
        {
            voucher.setKeys(typeof(CustomerReceivableDocument), "voucher");
        }
        protected override int relevantAccountID
        {
            get { return _customer == null ? -1 : _customer.ReceivableAccountID; }
        }

        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "TradeRelation Credit Settlement Form";
            layoutControlCustomerBalance.Text = "Total Credits:";
            layoutControlAmount.Text = "Payment Amount:";
            layoutRetention.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
        }
        protected override bool validateData(int assetAccountID)
        {
            double balance = _paymentController.getBalance(labelCustomerPaymentBal);
            if (double.Parse(txtAmount.Text) > balance && !(_client is DocumentScheduler))
            {
                MessageBox.ShowErrorMessage("The payment amount cannot be greater than the credit amount!");
                return false;
            }
            return true;
        }
        
        protected override string getTitle(bool transitive)
        {
            return "Credit Settlement for ";
        }
        protected override void setAdditionalFormData(CustomerAccountDocument customerAccountDocument)
        {
            CustomerReceivableDocument doc = (CustomerReceivableDocument)customerAccountDocument;
            checkRetention.Checked = doc.retention;
            voucher.setReference(doc.AccountDocumentID,doc.voucher);
        }
        protected override void getAdditionalFormData(CustomerAccountDocument oldCustomerAccountDocument, CustomerAccountDocument customerAccountDocument)
        {
            ((CustomerReceivableDocument)customerAccountDocument).voucher = voucher.getReference();
        }
        protected override CustomerAccountDocument CreateDocumentInstance()
        {
            return new CustomerReceivableDocument();
        }
    }
    class CustomerAdvanceReturnForm : CustomerAccountForm
    {
        public CustomerAdvanceReturnForm(IAccountingClient client, ActivationParameter param)
            : base(client, param)
        {
            voucher.setKeys(typeof(CustomerAdvanceReturnDocument), "voucher");
        }
        protected override int relevantAccountID
        {
            get { return _customer == null ? -1 : _customer.PayableAccountID; }
        }
        
        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "TradeRelation Advance Return Form";
            layoutControlCustomerBalance.Text = "Total Advances Collected:";
            layoutControlAmount.Text = "Return Amount:";           
        }
        
        protected override bool validateData(int assetAccountID)
        {
            return true;
        }
        protected override string getTitle(bool transitive)
        {
            return "Advance Return for ";
        }
        protected override void getAdditionalFormData(CustomerAccountDocument oldCustomerAccountDocument, CustomerAccountDocument customerAccountDocument)
        {
            ((CustomerAdvanceReturnDocument)customerAccountDocument).voucher = voucher.getReference();
        }
        protected override void setAdditionalFormData(CustomerAccountDocument customerAccountDocument)
        {
            voucher.setReference(customerAccountDocument.AccountDocumentID, ((CustomerAdvanceReturnDocument)customerAccountDocument).voucher);
        }
        protected override CustomerAccountDocument CreateDocumentInstance()
        {
            return new CustomerAdvanceReturnDocument();
        }
    }
    class BondPaymentForm : CustomerAccountForm
    {
        public BondPaymentForm(IAccountingClient client, ActivationParameter param)
            : base(client, param)
        {
            voucher.setKeys(typeof(BondPaymentDocument), "voucher");
        }
        protected override int relevantAccountID
        {
            get { return _customer == null ? -1 : _customer.PaidBondAccountID; }
        }

        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "Bond Payment Form";
            layoutControlCustomerBalance.Text = "Total Bond Paid:";
            layoutControlAmount.Text = "Payment Amount:";
        }
        protected override bool validateData(int assetAccountID)
        {
            return true;
        }
        protected override CustomerAccountDocument CreateDocumentInstance()
        {
           // return new BondPaymentDocument();
            BondPaymentDocument bondPayment = new BondPaymentDocument();
            bondPayment.voucher = voucher.getReference();
            return bondPayment;
        }
        protected override string getTitle(bool transitive)
        {
            return "Bond Payment for ";
        }
        protected override void getAdditionalFormData(CustomerAccountDocument oldCustomerAccountDocument, CustomerAccountDocument customerAccountDocument)
        {
            ((BondPaymentDocument)customerAccountDocument).voucher = voucher.getReference();
        }
        protected override void setAdditionalFormData(CustomerAccountDocument customerAccountDocument)
        {
            voucher.setReference(customerAccountDocument.AccountDocumentID, ((BondPaymentDocument)customerAccountDocument).voucher);
        }
    }
    class BondPaymentReturnForm : CustomerAccountForm
    {
        public BondPaymentReturnForm(IAccountingClient client, ActivationParameter param)
            : base(client, param)
        {
            voucher.setKeys(typeof(BondReturnDocument), "voucher");
        }
        protected override int relevantAccountID
        {
            get { return _customer == null ? -1 : _customer.PaidBondAccountID; }
        }

        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "Bond Return Form";
            layoutControlCustomerBalance.Text = "Total Bond Paid:";
            layoutControlAmount.Text = "Payment Amount:";
        }
        protected override CustomerAccountDocument CreateDocumentInstance()
        {
           // return new BondReturnDocument();
            BondReturnDocument bondReturn = new BondReturnDocument();
            
            return bondReturn;
        }

        protected override void getAdditionalFormData(CustomerAccountDocument oldcustomerAccountDocument,CustomerAccountDocument customerAccountDocument)
        {
            if (oldcustomerAccountDocument == null)
                ((BondReturnDocument)customerAccountDocument).bondPaymentDocumentID = _activation.bondPaymentDocument.AccountDocumentID;
            else
                ((BondReturnDocument)customerAccountDocument).bondPaymentDocumentID = ((BondReturnDocument)oldcustomerAccountDocument).bondPaymentDocumentID;
            ((BondReturnDocument)customerAccountDocument).voucher = voucher.getReference();
        }
        protected override void setAdditionalFormData(CustomerAccountDocument customerAccountDocument)
        {
            voucher.setReference(customerAccountDocument.AccountDocumentID, ((BondReturnDocument)customerAccountDocument).voucher);
        }
        protected override string getTitle(bool transitive)
        {
            return "Bond Return for ";
        }
    }
}
