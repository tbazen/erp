using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    class DCHCustomerPayable : bERPClientDocumentHandler
    {
        public DCHCustomerPayable()
            : base(typeof(CustomerAdvancePaymentForm))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            CustomerAccountForm f = (CustomerAccountForm)editor;
            f.LoadData((CustomerAccountDocument)doc);
        }
    }

    class DCHCustomerReceivable : bERPClientDocumentHandler
    {
        public DCHCustomerReceivable()
            : base(typeof(CustomerReceivableForm))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            CustomerAccountForm f = (CustomerAccountForm)editor;

            f.LoadData((CustomerAccountDocument)doc);
        }
    }

    class DCHCustomerAdvanceReturn : bERPClientDocumentHandler
    {
        public DCHCustomerAdvanceReturn()
            : base(typeof(CustomerAdvanceReturnForm))
        {
        }
       public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            CustomerAccountForm f = (CustomerAccountForm)editor;

            f.LoadData((CustomerAccountDocument)doc);
        }
    }

    class DCHBondPayment : bERPClientDocumentHandler
    {
         public DCHBondPayment()
            : base(typeof(BondPaymentForm))
        {
        }
       public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            CustomerAccountForm f = (CustomerAccountForm)editor;

            f.LoadData((CustomerAccountDocument)doc);
        }
    }

    class DCHBondReturn : bERPClientDocumentHandler
    {
        public DCHBondReturn()
            : base(typeof(BondPaymentReturnForm))
        {
        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            BondPaymentReturnForm f = (BondPaymentReturnForm)editor;

            f.LoadData((BondReturnDocument)doc);
        }
    }
}
