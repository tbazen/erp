using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHFixedAssetDepreciation : bERPClientDocumentHandler
    {
        public DCHFixedAssetDepreciation()
            : base(typeof(FixedAssetDepreciationForm))
        {
        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            ((FixedAssetDepreciationForm)editor).LoadData((FixedAssetDepreciationDocument)doc);
        }
    }

}
