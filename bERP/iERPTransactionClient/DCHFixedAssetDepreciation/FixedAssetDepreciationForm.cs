﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class FixedAssetDepreciationForm : DevExpress.XtraEditors.XtraForm
    {
        FixedAssetDepreciationDocument _doc;
        IAccountingClient _client;
        ActivationParameter _activation;
        bool _ignoreEvents = false;
        public FixedAssetDepreciationForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            htmlExportButton.setBrowser(browser);
            _client = client;
            _activation = activation;
            periodSelector.SelectedIndexChanged += delegate(object source, EventArgs e)
            {
                if (_ignoreEvents)
                    return;
                loadDepreciation();
            };
        }



        public double getUnitPrice(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            return 0;
        }

        public double getBalance(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            double val = AccountingClient.GetNetBalanceAsOf2(row.costCenterID, item.originalFixedAssetAccountID, periodSelector.selectedPeriod.toDate);
            return val;
        }

        internal void LoadData(FixedAssetDepreciationDocument depDocument)
        {
            _doc = depDocument;
            if (depDocument == null)
            {
                loadDepreciation();
            }
            else
            {
                pupulateData(depDocument);
            }
        }

        private void loadDepreciation()
        {
            browser.LoadTextPage("", "<h1>Loading depreciation .. please wait</h1>");
            enableForm(false);
            new System.Threading.Thread(delegate()
                {
                    int doc = iERPTransactionClient.getDepreciationDocument(periodSelector.selectedPeriod.fromDate);
                    FixedAssetDepreciationDocument faddoc;
                    if (doc != -1)
                        faddoc = AccountingClient.GetAccountDocument(doc, true) as FixedAssetDepreciationDocument;
                    else
                        faddoc = iERPTransactionClient.previewDepreciation(periodSelector.selectedPeriod.toDate);
                    if (!this.IsDisposed)
                    {
                        this.Invoke(new INTAPS.UI.HTML.ProcessSingleParameter<FixedAssetDepreciationDocument>(
                            delegate(FixedAssetDepreciationDocument d)
                            {
                                pupulateData(d);
                                enableForm(true);
                            }), faddoc);
                    }
                }).Start();
        }

        private void enableForm(bool enabled)
        {
            periodSelector.Enabled = enabled;
            buttonOk.Enabled = enabled;
        }
        double getBookValue(TransactionDocumentItem ditem,DateTime date)
        {
            TransactionItems item = iERPTransactionClient.GetTransactionItems(ditem.code);
            double val = AccountingClient.GetNetBalanceAsOf2(ditem.costCenterID, item.originalFixedAssetAccountID, date);
            val += AccountingClient.GetNetBalanceAsOf2(ditem.costCenterID, item.accumulatedDepreciationAccountID, date);
            return val;

        }
        TransactionDocumentItem[] items=null;
        private void pupulateData(FixedAssetDepreciationDocument fixedAssetDepreciationDocument)
        {
            _ignoreEvents = true;
            try
            {
                items = fixedAssetDepreciationDocument.items;
                periodSelector.setByDate(fixedAssetDepreciationDocument.DocumentDate.AddDays(-1));
                browser.LoadTextPage("Depreciation", AccountingClient.GetDocumentHTML(fixedAssetDepreciationDocument));
            }
            finally
            {
                _ignoreEvents = false;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {

                if (!dxValidationProvider.Validate())
                    return;

                int docID = _doc == null ? -1 : _doc.AccountDocumentID;
                _doc = new FixedAssetDepreciationDocument();
                _doc.AccountDocumentID = docID;
                
                _doc.ShortDescription = memoNote.Text;

                _doc.DocumentDate = periodSelector.selectedPeriod.toDate;
                _doc.PaperRef = periodSelector.selectedPeriod.name;

                //List<TransactionDocumentItem> items = new List<TransactionDocumentItem>();
                //foreach (ItemGridControl.ItemGridData.ItemTableRow row in itemGrid.data)
                //{
                //    if (!string.IsNullOrEmpty(row.code))
                //        items.Add(ItemGrid.toDocumentItem(row));
                //}
                //_doc.items = items.ToArray();
                _doc.items = items;
                _doc.verifyClientDocument = true;
                _doc.AccountDocumentID = _client.PostGenericDocument(_doc);
                if (!(_client is DocumentScheduler))
                {
                    MessageBox.ShowSuccessMessage("Depreciation succesfully registered");
                }

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public bool hasUnitPrice(TransactionItems item)
        {
            return true;
        }

        public bool queryDeleteRow(ItemGridControl.ItemGridData.ItemTableRow row)
        {
            return true;
        }
        public void afterComit(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {

        }

    }
}

