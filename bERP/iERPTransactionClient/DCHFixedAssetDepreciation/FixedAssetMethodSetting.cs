﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BIZNET.iERP.Client
{
    public partial class FixedAssetMethodSetting : DevExpress.XtraEditors.XtraForm
    {
        public FixedAssetMethodSetting(FixedAssetSetting setting)
        {
            InitializeComponent();
        }
        void save(FixedAssetSetting  newSetting)
        {
            iERPTransactionClient.saveFixedAssetRuleSetting(newSetting.typeID, newSetting);
        }
    }
}