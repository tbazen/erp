using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHFixedAssetDisposal : bERPClientDocumentHandler
    {
        public DCHFixedAssetDisposal()
            : base(typeof(FixedAssetDisposalForm))
        {
        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            ((FixedAssetDisposalForm)editor).LoadData((FixedAssetDisposalDocument)doc);
        }
    }

}
