﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class FixedAssetDisposalForm : DevExpress.XtraEditors.XtraForm,IItemGridController
    {
        FixedAssetDisposalDocument _doc;
        DocumentBasicFieldsController _docFields;
        IAccountingClient _client;
        ActivationParameter _activation;
        TextEdit _textReference;
        public FixedAssetDisposalForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            voucher.setKeys(typeof(FixedAssetDisposalDocument), "voucher");
            _textReference = new TextEdit();
            _client = client;
            _activation = activation;
            
            
            itemGrid.Controller = this;
            itemGrid.initializeGrid();
            _docFields = new DocumentBasicFieldsController(dateTransaction, _textReference, memoNote);
            DateTime now=DateTime.Now;
            dateTransaction.DateTime = new DateTime(now.Year, now.Month, now.Day, 22, 0, 0);
        }

        public double getUnitPrice(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            double totalQuantity = AccountingClient.GetNetCostCenterAccountBalanceAsOf(row.costCenterID, item.originalFixedAssetAccountID, TransactionItem.MATERIAL_QUANTITY, dateTransaction.DateTime);
            double totalValue = AccountingClient.GetNetCostCenterAccountBalanceAsOf(row.costCenterID, item.originalFixedAssetAccountID, TransactionItem.DEFAULT_CURRENCY, dateTransaction.DateTime);
            totalValue += AccountingClient.GetNetCostCenterAccountBalanceAsOf(row.costCenterID, item.accumulatedDepreciationAccountID, TransactionItem.DEFAULT_CURRENCY, dateTransaction.DateTime);
            if (_doc != null && _doc.DocumentDate < dateTransaction.DateTime && row.costCenterID == row.costCenterID)
            {
                TransactionDocumentItem indocItem = TransactionDocumentItem.sumByItem(_doc.items, item.Code, -1);
                totalQuantity += indocItem.quantity;
                totalValue += indocItem.price;
            }

            if (AccountBase.AmountEqual(totalQuantity, 0))
                return 0;
            return totalValue / totalQuantity;
        }

        public double getBalance(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            if (_doc != null && _doc.DocumentDate < dateTransaction.DateTime && row.costCenterID == row.costCenterID)
            {
                TransactionDocumentItem indocItem = TransactionDocumentItem.sumByItem(_doc.items, item.Code, -1);
                double ret = AccountingClient.GetNetCostCenterAccountBalanceAsOf(row.costCenterID, item.originalFixedAssetAccountID, TransactionItem.MATERIAL_QUANTITY, dateTransaction.DateTime);
                return ret + indocItem.quantity;
            }
            return AccountingClient.GetNetCostCenterAccountBalanceAsOf(row.costCenterID, item.originalFixedAssetAccountID, TransactionItem.MATERIAL_QUANTITY, dateTransaction.DateTime);
        }

        internal void LoadData(FixedAssetDisposalDocument doc)
        {
            _doc = doc;
            if (doc != null)
            {
                voucher.setReference(doc.AccountDocumentID, doc.voucher);
                if (doc.voucher != null)
                    _textReference.Text = doc.voucher.reference;
                _docFields.setControlData(_doc);
                foreach (TransactionDocumentItem item in _doc.items)
                {
                    itemGrid.Add(item);
                }
                itemGrid.updateBalances();
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (!dxValidationProvider.Validate())
                    return;
                if (voucher.getReference() == null)
                {
                    MessageBox.ShowErrorMessage("Enter valid reference");
                    return;
                }
                int docID = _doc == null ? -1 : _doc.AccountDocumentID;
                _doc = new FixedAssetDisposalDocument();
                _doc.AccountDocumentID = docID;
                _doc.voucher = voucher.getReference();
                _textReference.Text = _doc.voucher.reference;
                _docFields.setDocumentData(_doc);
                List<TransactionDocumentItem> items = new List<TransactionDocumentItem>();
                foreach (ItemGridControl.ItemGridData.ItemTableRow row in itemGrid.data)
                {
                    if (!string.IsNullOrEmpty(row.code))
                    {
                        TransactionDocumentItem di = ItemGrid.toDocumentItem(row);
                        items.Add(di);
                    }
                }
                _doc.items = items.ToArray();
               
                _doc.AccountDocumentID=_client.PostGenericDocument(_doc);
                if(!(_client is DocumentScheduler))
                {
                    MessageBox.ShowSuccessMessage("Store issue succesfully registered");
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public bool hasUnitPrice(TransactionItems item)
        {
            return true;
        }

        public bool queryCellChange(ItemGridControl.ItemGridData.ItemTableRow row, DataColumn column, object oldValue)
        {
            return true;
        }

        public bool queryDeleteRow(ItemGridControl.ItemGridData.ItemTableRow row)
        {
            return true;
        }

        public void afterComit(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {

        }

        private void dateTransaction_DateTimeChanged(object sender, EventArgs e)
        {
            itemGrid.updateBalances();
        }

        private void storePlaceholder_SelectedIndexChanged(object sender, EventArgs e)
        {
            itemGrid.updateBalances();
        }

    }
}

