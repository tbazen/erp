using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHGroupPayroll : bERPClientDocumentHandler
    {
        public DCHGroupPayroll()
            : base(typeof(GroupPayrollForm))
        {
        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            GroupPayrollForm f = (GroupPayrollForm)editor;

            f.LoadData((GroupPayrollDocument)doc);
        }
    }
}
