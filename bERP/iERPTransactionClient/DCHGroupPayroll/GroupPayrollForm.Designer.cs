﻿namespace BIZNET.iERP.Client
{
    partial class GroupPayrollForm 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule3 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule4 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule5 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtOverTime = new DevExpress.XtraEditors.TextEdit();
            this.txtUnclaimedSalary = new DevExpress.XtraEditors.TextEdit();
            this.txtTaxableGrossSalary = new DevExpress.XtraEditors.TextEdit();
            this.txtGrossSalary = new DevExpress.XtraEditors.TextEdit();
            this.txtDescription = new DevExpress.XtraEditors.TextEdit();
            this.chkAccountAsCost = new DevExpress.XtraEditors.CheckEdit();
            this.labelBankBalance = new DevExpress.XtraEditors.LabelControl();
            this.labelCashBalance = new DevExpress.XtraEditors.LabelControl();
            this.txtServiceCharge = new DevExpress.XtraEditors.TextEdit();
            this.chkDeclareTax = new DevExpress.XtraEditors.CheckEdit();
            this.txtSalary = new DevExpress.XtraEditors.TextEdit();
            this.txtTotalEmployerPension = new DevExpress.XtraEditors.TextEdit();
            this.cmbTaxCenter = new DevExpress.XtraEditors.ComboBoxEdit();
            this.costCenterPlaceHolder = new INTAPS.Accounting.Client.CostCenterPlaceHolder();
            this.txtTotalIncomeTax = new DevExpress.XtraEditors.TextEdit();
            this.txtNote = new DevExpress.XtraEditors.MemoEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.txtReference = new DevExpress.XtraEditors.TextEdit();
            this.txtTotalEmployeePension = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlNote = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAccountAsCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutNetPay = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutGrossSalary = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTaxableGrossSalary = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlSlipRef = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlServiceCharge = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlCashBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlBankBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.validationGroupPayoll = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.chkUnclaimed = new DevExpress.XtraEditors.CheckEdit();
            this.layoutChkUnclaimed = new DevExpress.XtraLayout.LayoutControlItem();
            this.voucher = new BIZNET.iERP.Client.DocumentSerialPlaceHolder();
            this.cmbBankAccount = new BIZNET.iERP.Client.BankAccountPlaceholder();
            this.cmbCasher = new BIZNET.iERP.Client.CashAccountPlaceholder();
            this.paymentTypeSelector = new BIZNET.iERP.Client.PaymentTypeSelector();
            this.cmbPeriod = new BIZNET.iERP.Client.PeriodSelector();
            this.datePayment = new BIZNET.iERP.Client.BNDualCalendar();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlCashAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlBankAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnclaimedSalary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxableGrossSalary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrossSalary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAccountAsCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceCharge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeclareTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalEmployerPension.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTaxCenter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIncomeTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalEmployeePension.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAccountAsCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutNetPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGrossSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTaxableGrossSalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSlipRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlServiceCharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationGroupPayoll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUnclaimed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutChkUnclaimed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBankAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCasher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentTypeSelector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.chkUnclaimed);
            this.layoutControl1.Controls.Add(this.txtOverTime);
            this.layoutControl1.Controls.Add(this.txtUnclaimedSalary);
            this.layoutControl1.Controls.Add(this.voucher);
            this.layoutControl1.Controls.Add(this.txtTaxableGrossSalary);
            this.layoutControl1.Controls.Add(this.txtGrossSalary);
            this.layoutControl1.Controls.Add(this.txtDescription);
            this.layoutControl1.Controls.Add(this.chkAccountAsCost);
            this.layoutControl1.Controls.Add(this.labelBankBalance);
            this.layoutControl1.Controls.Add(this.labelCashBalance);
            this.layoutControl1.Controls.Add(this.txtServiceCharge);
            this.layoutControl1.Controls.Add(this.cmbBankAccount);
            this.layoutControl1.Controls.Add(this.cmbCasher);
            this.layoutControl1.Controls.Add(this.chkDeclareTax);
            this.layoutControl1.Controls.Add(this.paymentTypeSelector);
            this.layoutControl1.Controls.Add(this.txtSalary);
            this.layoutControl1.Controls.Add(this.txtTotalEmployerPension);
            this.layoutControl1.Controls.Add(this.cmbTaxCenter);
            this.layoutControl1.Controls.Add(this.cmbPeriod);
            this.layoutControl1.Controls.Add(this.costCenterPlaceHolder);
            this.layoutControl1.Controls.Add(this.datePayment);
            this.layoutControl1.Controls.Add(this.txtTotalIncomeTax);
            this.layoutControl1.Controls.Add(this.txtNote);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.txtReference);
            this.layoutControl1.Controls.Add(this.txtTotalEmployeePension);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(447, 217, 250, 350);
            this.layoutControl1.OptionsView.HighlightFocusedItem = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(808, 661);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            this.layoutControl1.LayoutUpdate += new System.EventHandler(this.layoutControl1_LayoutUpdate);
            // 
            // txtOverTime
            // 
            this.txtOverTime.Location = new System.Drawing.Point(589, 394);
            this.txtOverTime.Name = "txtOverTime";
            this.txtOverTime.Properties.Mask.EditMask = "#,########0.00;";
            this.txtOverTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtOverTime.Size = new System.Drawing.Size(190, 20);
            this.txtOverTime.StyleController = this.layoutControl1;
            this.txtOverTime.TabIndex = 36;
            // 
            // txtUnclaimedSalary
            // 
            this.txtUnclaimedSalary.Location = new System.Drawing.Point(198, 364);
            this.txtUnclaimedSalary.Name = "txtUnclaimedSalary";
            this.txtUnclaimedSalary.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtUnclaimedSalary.Properties.Mask.EditMask = "#,########0.00;";
            this.txtUnclaimedSalary.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtUnclaimedSalary.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtUnclaimedSalary.Size = new System.Drawing.Size(199, 20);
            this.txtUnclaimedSalary.StyleController = this.layoutControl1;
            this.txtUnclaimedSalary.TabIndex = 35;
            // 
            // txtTaxableGrossSalary
            // 
            this.txtTaxableGrossSalary.Location = new System.Drawing.Point(589, 304);
            this.txtTaxableGrossSalary.Name = "txtTaxableGrossSalary";
            this.txtTaxableGrossSalary.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtTaxableGrossSalary.Properties.Mask.EditMask = "#,########0.00;";
            this.txtTaxableGrossSalary.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTaxableGrossSalary.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTaxableGrossSalary.Size = new System.Drawing.Size(190, 20);
            this.txtTaxableGrossSalary.StyleController = this.layoutControl1;
            this.txtTaxableGrossSalary.TabIndex = 33;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "Taxable Gross salary cannot be empty";
            this.validationGroupPayoll.SetValidationRule(this.txtTaxableGrossSalary, conditionValidationRule1);
            // 
            // txtGrossSalary
            // 
            this.txtGrossSalary.Location = new System.Drawing.Point(198, 304);
            this.txtGrossSalary.Name = "txtGrossSalary";
            this.txtGrossSalary.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtGrossSalary.Properties.Mask.EditMask = "#,########0.00;";
            this.txtGrossSalary.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGrossSalary.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtGrossSalary.Size = new System.Drawing.Size(199, 20);
            this.txtGrossSalary.StyleController = this.layoutControl1;
            this.txtGrossSalary.TabIndex = 32;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "Please enter Gross Salary and try again";
            this.validationGroupPayoll.SetValidationRule(this.txtGrossSalary, conditionValidationRule2);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(192, 98);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(593, 20);
            this.txtDescription.StyleController = this.layoutControl1;
            this.txtDescription.TabIndex = 31;
            conditionValidationRule3.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule3.ErrorText = "Please enter general description of the payroll";
            this.validationGroupPayoll.SetValidationRule(this.txtDescription, conditionValidationRule3);
            this.txtDescription.EditValueChanged += new System.EventHandler(this.txtDescription_EditValueChanged);
            // 
            // chkAccountAsCost
            // 
            this.chkAccountAsCost.Location = new System.Drawing.Point(16, 244);
            this.chkAccountAsCost.Name = "chkAccountAsCost";
            this.chkAccountAsCost.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.chkAccountAsCost.Properties.Caption = "Account As Cost";
            this.chkAccountAsCost.Size = new System.Drawing.Size(763, 21);
            this.chkAccountAsCost.StyleController = this.layoutControl1;
            this.chkAccountAsCost.TabIndex = 30;
            // 
            // labelBankBalance
            // 
            this.labelBankBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelBankBalance.Location = new System.Drawing.Point(564, 512);
            this.labelBankBalance.Name = "labelBankBalance";
            this.labelBankBalance.Size = new System.Drawing.Size(218, 26);
            this.labelBankBalance.StyleController = this.layoutControl1;
            this.labelBankBalance.TabIndex = 29;
            this.labelBankBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // labelCashBalance
            // 
            this.labelCashBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelCashBalance.Location = new System.Drawing.Point(564, 482);
            this.labelCashBalance.Name = "labelCashBalance";
            this.labelCashBalance.Size = new System.Drawing.Size(218, 26);
            this.labelCashBalance.StyleController = this.layoutControl1;
            this.labelCashBalance.TabIndex = 28;
            this.labelCashBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // txtServiceCharge
            // 
            this.txtServiceCharge.Location = new System.Drawing.Point(198, 545);
            this.txtServiceCharge.Name = "txtServiceCharge";
            this.txtServiceCharge.Size = new System.Drawing.Size(581, 20);
            this.txtServiceCharge.StyleController = this.layoutControl1;
            this.txtServiceCharge.TabIndex = 27;
            // 
            // chkDeclareTax
            // 
            this.chkDeclareTax.Location = new System.Drawing.Point(16, 213);
            this.chkDeclareTax.Name = "chkDeclareTax";
            this.chkDeclareTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.chkDeclareTax.Properties.Appearance.Options.UseFont = true;
            this.chkDeclareTax.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.chkDeclareTax.Properties.Caption = "Add taxes to Declaration (consider income and pension taxes to be added to declar" +
                "ation and paid later)";
            this.chkDeclareTax.Size = new System.Drawing.Size(763, 21);
            this.chkDeclareTax.StyleController = this.layoutControl1;
            this.chkDeclareTax.TabIndex = 23;
            this.chkDeclareTax.CheckedChanged += new System.EventHandler(this.chkDeclareTax_CheckedChanged);
            // 
            // txtSalary
            // 
            this.txtSalary.Location = new System.Drawing.Point(198, 334);
            this.txtSalary.Name = "txtSalary";
            this.txtSalary.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtSalary.Properties.Mask.EditMask = "#,########0.00;";
            this.txtSalary.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSalary.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtSalary.Size = new System.Drawing.Size(199, 20);
            this.txtSalary.StyleController = this.layoutControl1;
            this.txtSalary.TabIndex = 21;
            conditionValidationRule4.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule4.ErrorText = "Total Salary cannot be empty";
            this.validationGroupPayoll.SetValidationRule(this.txtSalary, conditionValidationRule4);
            // 
            // txtTotalEmployerPension
            // 
            this.txtTotalEmployerPension.Location = new System.Drawing.Point(198, 394);
            this.txtTotalEmployerPension.Name = "txtTotalEmployerPension";
            this.txtTotalEmployerPension.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtTotalEmployerPension.Properties.Mask.EditMask = "#,########0.00;";
            this.txtTotalEmployerPension.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalEmployerPension.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotalEmployerPension.Size = new System.Drawing.Size(199, 20);
            this.txtTotalEmployerPension.StyleController = this.layoutControl1;
            this.txtTotalEmployerPension.TabIndex = 20;
            // 
            // cmbTaxCenter
            // 
            this.cmbTaxCenter.Location = new System.Drawing.Point(571, 183);
            this.cmbTaxCenter.Name = "cmbTaxCenter";
            this.cmbTaxCenter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTaxCenter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbTaxCenter.Size = new System.Drawing.Size(208, 20);
            this.cmbTaxCenter.StyleController = this.layoutControl1;
            this.cmbTaxCenter.TabIndex = 19;
            // 
            // costCenterPlaceHolder
            // 
            this.costCenterPlaceHolder.account = null;
            this.costCenterPlaceHolder.AllowAdd = false;
            this.costCenterPlaceHolder.Location = new System.Drawing.Point(198, 183);
            this.costCenterPlaceHolder.Name = "costCenterPlaceHolder";
            this.costCenterPlaceHolder.OnlyLeafAccount = false;
            this.costCenterPlaceHolder.Size = new System.Drawing.Size(181, 20);
            this.costCenterPlaceHolder.TabIndex = 17;
            // 
            // txtTotalIncomeTax
            // 
            this.txtTotalIncomeTax.Location = new System.Drawing.Point(589, 334);
            this.txtTotalIncomeTax.Name = "txtTotalIncomeTax";
            this.txtTotalIncomeTax.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtTotalIncomeTax.Properties.Mask.EditMask = "#,########0.00;";
            this.txtTotalIncomeTax.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalIncomeTax.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotalIncomeTax.Size = new System.Drawing.Size(190, 20);
            this.txtTotalIncomeTax.StyleController = this.layoutControl1;
            this.txtTotalIncomeTax.TabIndex = 15;
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(192, 611);
            this.txtNote.Name = "txtNote";
            this.txtNote.Properties.ReadOnly = true;
            this.txtNote.Size = new System.Drawing.Size(593, 33);
            this.txtNote.StyleController = this.layoutControl1;
            this.txtNote.TabIndex = 6;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.buttonCancel);
            this.panelControl1.Controls.Add(this.buttonOk);
            this.panelControl1.Location = new System.Drawing.Point(7, 651);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(781, 20);
            this.panelControl1.TabIndex = 5;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(717, -4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(59, 23);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(639, -4);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(62, 23);
            this.buttonOk.TabIndex = 4;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // txtReference
            // 
            this.txtReference.Location = new System.Drawing.Point(198, 575);
            this.txtReference.Name = "txtReference";
            this.txtReference.Properties.Mask.EditMask = "\\w.*";
            this.txtReference.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtReference.Size = new System.Drawing.Size(581, 20);
            this.txtReference.StyleController = this.layoutControl1;
            this.txtReference.TabIndex = 2;
            conditionValidationRule5.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule5.ErrorText = "Slip Reference cannot be empty";
            this.validationGroupPayoll.SetValidationRule(this.txtReference, conditionValidationRule5);
            // 
            // txtTotalEmployeePension
            // 
            this.txtTotalEmployeePension.Location = new System.Drawing.Point(589, 364);
            this.txtTotalEmployeePension.Name = "txtTotalEmployeePension";
            this.txtTotalEmployeePension.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtTotalEmployeePension.Properties.Mask.EditMask = "#,########0.00;";
            this.txtTotalEmployeePension.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalEmployeePension.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotalEmployeePension.Size = new System.Drawing.Size(190, 20);
            this.txtTotalEmployeePension.StyleController = this.layoutControl1;
            this.txtTotalEmployeePension.TabIndex = 2;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlNote,
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlItem13,
            this.layoutControlItem12,
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(795, 678);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.panelControl1;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 644);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(785, 24);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlNote
            // 
            this.layoutControlNote.Control = this.txtNote;
            this.layoutControlNote.CustomizationFormText = "Note:";
            this.layoutControlNote.Location = new System.Drawing.Point(0, 601);
            this.layoutControlNote.MaxSize = new System.Drawing.Size(785, 43);
            this.layoutControlNote.MinSize = new System.Drawing.Size(785, 43);
            this.layoutControlNote.Name = "layoutControlNote";
            this.layoutControlNote.Size = new System.Drawing.Size(785, 43);
            this.layoutControlNote.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlNote.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlNote.Text = "Note:";
            this.layoutControlNote.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "Payroll Information";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutAccountAsCost,
            this.layoutNetPay,
            this.layoutGrossSalary,
            this.layoutTaxableGrossSalary,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem14,
            this.layoutChkUnclaimed,
            this.layoutControlItem11,
            this.layoutControlAmount});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 118);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup2.Size = new System.Drawing.Size(785, 302);
            this.layoutControlGroup2.Text = "Payroll Information";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.chkDeclareTax;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(773, 31);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.costCenterPlaceHolder;
            this.layoutControlItem3.CustomizationFormText = "Cost Center:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(373, 30);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "Cost Center:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutAccountAsCost
            // 
            this.layoutAccountAsCost.Control = this.chkAccountAsCost;
            this.layoutAccountAsCost.CustomizationFormText = "Account as Cost";
            this.layoutAccountAsCost.Location = new System.Drawing.Point(0, 91);
            this.layoutAccountAsCost.Name = "layoutAccountAsCost";
            this.layoutAccountAsCost.Size = new System.Drawing.Size(773, 31);
            this.layoutAccountAsCost.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutAccountAsCost.Text = "Account as Cost";
            this.layoutAccountAsCost.TextSize = new System.Drawing.Size(0, 0);
            this.layoutAccountAsCost.TextToControlDistance = 0;
            this.layoutAccountAsCost.TextVisible = false;
            // 
            // layoutNetPay
            // 
            this.layoutNetPay.Control = this.txtSalary;
            this.layoutNetPay.CustomizationFormText = "Total Salary:";
            this.layoutNetPay.Location = new System.Drawing.Point(0, 181);
            this.layoutNetPay.Name = "layoutNetPay";
            this.layoutNetPay.Size = new System.Drawing.Size(391, 30);
            this.layoutNetPay.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutNetPay.Text = "Total Net Pay:";
            this.layoutNetPay.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutGrossSalary
            // 
            this.layoutGrossSalary.Control = this.txtGrossSalary;
            this.layoutGrossSalary.CustomizationFormText = "Total Gross Salary:";
            this.layoutGrossSalary.Location = new System.Drawing.Point(0, 151);
            this.layoutGrossSalary.Name = "layoutGrossSalary";
            this.layoutGrossSalary.Size = new System.Drawing.Size(391, 30);
            this.layoutGrossSalary.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutGrossSalary.Text = "Total Gross Salary:";
            this.layoutGrossSalary.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutTaxableGrossSalary
            // 
            this.layoutTaxableGrossSalary.Control = this.txtTaxableGrossSalary;
            this.layoutTaxableGrossSalary.CustomizationFormText = "Total Taxable Gross Salary:";
            this.layoutTaxableGrossSalary.Location = new System.Drawing.Point(391, 151);
            this.layoutTaxableGrossSalary.MaxSize = new System.Drawing.Size(692, 30);
            this.layoutTaxableGrossSalary.MinSize = new System.Drawing.Size(382, 30);
            this.layoutTaxableGrossSalary.Name = "layoutTaxableGrossSalary";
            this.layoutTaxableGrossSalary.Size = new System.Drawing.Size(382, 30);
            this.layoutTaxableGrossSalary.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutTaxableGrossSalary.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutTaxableGrossSalary.Text = "Total Taxable Gross Salary:";
            this.layoutTaxableGrossSalary.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.cmbTaxCenter;
            this.layoutControlItem5.CustomizationFormText = "Tax Center:";
            this.layoutControlItem5.Location = new System.Drawing.Point(373, 30);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(400, 30);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem5.Text = "Tax Center:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txtUnclaimedSalary;
            this.layoutControlItem11.CustomizationFormText = "Total Unclaimed Salary:";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 211);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(392, 30);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(212, 30);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(391, 30);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem11.Text = "Total Unclaimed Salary:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtTotalIncomeTax;
            this.layoutControlItem6.CustomizationFormText = "Paid To:";
            this.layoutControlItem6.Location = new System.Drawing.Point(391, 181);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(382, 30);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(382, 30);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(382, 30);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem6.Text = "Total Income Tax:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutControlAmount
            // 
            this.layoutControlAmount.Control = this.txtTotalEmployeePension;
            this.layoutControlAmount.CustomizationFormText = "Amount:";
            this.layoutControlAmount.Location = new System.Drawing.Point(391, 211);
            this.layoutControlAmount.Name = "layoutControlAmount";
            this.layoutControlAmount.Size = new System.Drawing.Size(382, 30);
            this.layoutControlAmount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlAmount.Text = "Total Employee Pension Contribution:";
            this.layoutControlAmount.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtTotalEmployerPension;
            this.layoutControlItem8.CustomizationFormText = "Total Employer Pension Contribution:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 241);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(391, 30);
            this.layoutControlItem8.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem8.Text = "Total Employer Pension Contribution:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txtOverTime;
            this.layoutControlItem14.CustomizationFormText = "Total Over Time:";
            this.layoutControlItem14.Location = new System.Drawing.Point(391, 241);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(382, 30);
            this.layoutControlItem14.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem14.Text = "Total Over Time:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.CustomizationFormText = "Payment Information:";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlSlipRef,
            this.layoutControlCashAccount,
            this.layoutControlBankAccount,
            this.layoutControlServiceCharge,
            this.layoutControlCashBalance,
            this.layoutControlBankBalance});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 420);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup3.Size = new System.Drawing.Size(785, 181);
            this.layoutControlGroup3.Text = "Payment Information:";
            // 
            // layoutControlSlipRef
            // 
            this.layoutControlSlipRef.Control = this.txtReference;
            this.layoutControlSlipRef.CustomizationFormText = "Slip Reference:";
            this.layoutControlSlipRef.Location = new System.Drawing.Point(0, 120);
            this.layoutControlSlipRef.Name = "layoutControlSlipRef";
            this.layoutControlSlipRef.Size = new System.Drawing.Size(773, 30);
            this.layoutControlSlipRef.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlSlipRef.Text = "Slip Reference:";
            this.layoutControlSlipRef.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutControlServiceCharge
            // 
            this.layoutControlServiceCharge.Control = this.txtServiceCharge;
            this.layoutControlServiceCharge.CustomizationFormText = "Service Charge:";
            this.layoutControlServiceCharge.Location = new System.Drawing.Point(0, 90);
            this.layoutControlServiceCharge.Name = "layoutControlServiceCharge";
            this.layoutControlServiceCharge.Size = new System.Drawing.Size(773, 30);
            this.layoutControlServiceCharge.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlServiceCharge.Text = "Service Charge:";
            this.layoutControlServiceCharge.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutControlCashBalance
            // 
            this.layoutControlCashBalance.Control = this.labelCashBalance;
            this.layoutControlCashBalance.CustomizationFormText = "layoutControlItem15";
            this.layoutControlCashBalance.Location = new System.Drawing.Point(551, 30);
            this.layoutControlCashBalance.MaxSize = new System.Drawing.Size(222, 30);
            this.layoutControlCashBalance.MinSize = new System.Drawing.Size(222, 30);
            this.layoutControlCashBalance.Name = "layoutControlCashBalance";
            this.layoutControlCashBalance.Size = new System.Drawing.Size(222, 30);
            this.layoutControlCashBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlCashBalance.Text = "layoutControlCashBalance";
            this.layoutControlCashBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlCashBalance.TextToControlDistance = 0;
            this.layoutControlCashBalance.TextVisible = false;
            // 
            // layoutControlBankBalance
            // 
            this.layoutControlBankBalance.Control = this.labelBankBalance;
            this.layoutControlBankBalance.CustomizationFormText = "layoutControlItem16";
            this.layoutControlBankBalance.Location = new System.Drawing.Point(551, 60);
            this.layoutControlBankBalance.MaxSize = new System.Drawing.Size(222, 30);
            this.layoutControlBankBalance.MinSize = new System.Drawing.Size(222, 30);
            this.layoutControlBankBalance.Name = "layoutControlBankBalance";
            this.layoutControlBankBalance.Size = new System.Drawing.Size(222, 30);
            this.layoutControlBankBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlBankBalance.Text = "layoutControlBankBalance";
            this.layoutControlBankBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlBankBalance.TextToControlDistance = 0;
            this.layoutControlBankBalance.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.txtDescription;
            this.layoutControlItem13.CustomizationFormText = "Description:";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 88);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(785, 30);
            this.layoutControlItem13.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem13.Text = "Description:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(179, 13);
            // 
            // validationGroupPayoll
            // 
            this.validationGroupPayoll.ValidateHiddenControls = false;
            // 
            // chkUnclaimed
            // 
            this.chkUnclaimed.Location = new System.Drawing.Point(16, 275);
            this.chkUnclaimed.Name = "chkUnclaimed";
            this.chkUnclaimed.Properties.Caption = "Full Unclaimed";
            this.chkUnclaimed.Size = new System.Drawing.Size(763, 19);
            this.chkUnclaimed.StyleController = this.layoutControl1;
            this.chkUnclaimed.TabIndex = 37;
            this.chkUnclaimed.CheckedChanged += new System.EventHandler(this.chkUnclaimed_CheckedChanged);
            // 
            // layoutChkUnclaimed
            // 
            this.layoutChkUnclaimed.Control = this.chkUnclaimed;
            this.layoutChkUnclaimed.CustomizationFormText = "Full Unclaimed";
            this.layoutChkUnclaimed.Location = new System.Drawing.Point(0, 122);
            this.layoutChkUnclaimed.Name = "layoutChkUnclaimed";
            this.layoutChkUnclaimed.Size = new System.Drawing.Size(773, 29);
            this.layoutChkUnclaimed.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutChkUnclaimed.Text = "Full Unclaimed";
            this.layoutChkUnclaimed.TextSize = new System.Drawing.Size(0, 0);
            this.layoutChkUnclaimed.TextToControlDistance = 0;
            this.layoutChkUnclaimed.TextVisible = false;
            // 
            // voucher
            // 
            this.voucher.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.voucher.Appearance.Options.UseBackColor = true;
            this.voucher.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.voucher.Location = new System.Drawing.Point(7, 23);
            this.voucher.Name = "voucher";
            this.voucher.Size = new System.Drawing.Size(373, 68);
            this.voucher.TabIndex = 34;
            // 
            // cmbBankAccount
            // 
            this.cmbBankAccount.Location = new System.Drawing.Point(198, 515);
            this.cmbBankAccount.Name = "cmbBankAccount";
            this.cmbBankAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbBankAccount.Properties.ImmediatePopup = true;
            this.cmbBankAccount.Size = new System.Drawing.Size(359, 20);
            this.cmbBankAccount.StyleController = this.layoutControl1;
            this.cmbBankAccount.TabIndex = 26;
            // 
            // cmbCasher
            // 
            this.cmbCasher.Location = new System.Drawing.Point(198, 485);
            this.cmbCasher.Name = "cmbCasher";
            this.cmbCasher.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCasher.Properties.ImmediatePopup = true;
            this.cmbCasher.Size = new System.Drawing.Size(359, 20);
            this.cmbCasher.StyleController = this.layoutControl1;
            this.cmbCasher.TabIndex = 25;
            // 
            // paymentTypeSelector
            // 
            this.paymentTypeSelector.Include_BankTransferFromAccount = true;
            this.paymentTypeSelector.Include_BankTransferFromCash = true;
            this.paymentTypeSelector.Include_Cash = true;
            this.paymentTypeSelector.Include_Check = true;
            this.paymentTypeSelector.Include_CpoFromBank = true;
            this.paymentTypeSelector.Include_CpoFromCash = true;
            this.paymentTypeSelector.Include_Credit = false;
            this.paymentTypeSelector.Location = new System.Drawing.Point(198, 455);
            this.paymentTypeSelector.Name = "paymentTypeSelector";
            this.paymentTypeSelector.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.paymentTypeSelector.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.paymentTypeSelector.Size = new System.Drawing.Size(581, 20);
            this.paymentTypeSelector.StyleController = this.layoutControl1;
            this.paymentTypeSelector.TabIndex = 22;
            // 
            // cmbPeriod
            // 
            this.cmbPeriod.Location = new System.Drawing.Point(198, 153);
            this.cmbPeriod.Name = "cmbPeriod";
            this.cmbPeriod.PeriodType = "AP_Payroll";
            this.cmbPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPeriod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbPeriod.Size = new System.Drawing.Size(581, 20);
            this.cmbPeriod.StyleController = this.layoutControl1;
            this.cmbPeriod.TabIndex = 18;
            this.cmbPeriod.SelectedIndexChanged += new System.EventHandler(this.cmbPeriod_SelectedIndexChanged);
            // 
            // datePayment
            // 
            this.datePayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.datePayment.Location = new System.Drawing.Point(387, 26);
            this.datePayment.Name = "datePayment";
            this.datePayment.ShowEthiopian = true;
            this.datePayment.ShowGregorian = true;
            this.datePayment.ShowTime = true;
            this.datePayment.Size = new System.Drawing.Size(398, 63);
            this.datePayment.TabIndex = 16;
            this.datePayment.VerticalLayout = true;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.cmbPeriod;
            this.layoutControlItem4.CustomizationFormText = "Period:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(773, 30);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem4.Text = "Period:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.paymentTypeSelector;
            this.layoutControlItem9.CustomizationFormText = "Payment Method:";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(773, 30);
            this.layoutControlItem9.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem9.Text = "Payment Method:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutControlCashAccount
            // 
            this.layoutControlCashAccount.Control = this.cmbCasher;
            this.layoutControlCashAccount.CustomizationFormText = "Payment from Cash Account:";
            this.layoutControlCashAccount.Location = new System.Drawing.Point(0, 30);
            this.layoutControlCashAccount.MaxSize = new System.Drawing.Size(551, 30);
            this.layoutControlCashAccount.MinSize = new System.Drawing.Size(551, 30);
            this.layoutControlCashAccount.Name = "layoutControlCashAccount";
            this.layoutControlCashAccount.Size = new System.Drawing.Size(551, 30);
            this.layoutControlCashAccount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlCashAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlCashAccount.Text = "Payment from Cash Account:";
            this.layoutControlCashAccount.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutControlBankAccount
            // 
            this.layoutControlBankAccount.Control = this.cmbBankAccount;
            this.layoutControlBankAccount.CustomizationFormText = "Payment from Bank Account:";
            this.layoutControlBankAccount.Location = new System.Drawing.Point(0, 60);
            this.layoutControlBankAccount.MaxSize = new System.Drawing.Size(551, 30);
            this.layoutControlBankAccount.MinSize = new System.Drawing.Size(551, 30);
            this.layoutControlBankAccount.Name = "layoutControlBankAccount";
            this.layoutControlBankAccount.Size = new System.Drawing.Size(551, 30);
            this.layoutControlBankAccount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlBankAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlBankAccount.Text = "Payment from Bank Account:";
            this.layoutControlBankAccount.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.voucher;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(377, 88);
            this.layoutControlItem12.Text = "Document Reference";
            this.layoutControlItem12.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(179, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.datePayment;
            this.layoutControlItem2.CustomizationFormText = "Date:";
            this.layoutControlItem2.Location = new System.Drawing.Point(377, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 88);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(189, 88);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(408, 88);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem2.Text = "Date:";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(179, 13);
            // 
            // GroupPayrollForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(808, 661);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "GroupPayrollForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Group Payroll Form";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtOverTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnclaimedSalary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxableGrossSalary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrossSalary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAccountAsCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceCharge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeclareTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalEmployerPension.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTaxCenter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalIncomeTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalEmployeePension.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAccountAsCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutNetPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGrossSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTaxableGrossSalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSlipRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlServiceCharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationGroupPayoll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUnclaimed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutChkUnclaimed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBankAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCasher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentTypeSelector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtTotalEmployeePension;
        private DevExpress.XtraEditors.TextEdit txtReference;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlSlipRef;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.MemoEdit txtNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlNote;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validationGroupPayoll;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraEditors.TextEdit txtTotalIncomeTax;
        private BIZNET.iERP.Client.BNDualCalendar datePayment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private INTAPS.Accounting.Client.CostCenterPlaceHolder costCenterPlaceHolder;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private PeriodSelector cmbPeriod;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.ComboBoxEdit cmbTaxCenter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.TextEdit txtTotalEmployerPension;
        private DevExpress.XtraEditors.TextEdit txtSalary;
        private PaymentTypeSelector paymentTypeSelector;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.CheckEdit chkDeclareTax;
        private BankAccountPlaceholder cmbBankAccount;
        private CashAccountPlaceholder cmbCasher;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCashAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlBankAccount;
        private DevExpress.XtraEditors.TextEdit txtServiceCharge;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlServiceCharge;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutNetPay;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.LabelControl labelBankBalance;
        private DevExpress.XtraEditors.LabelControl labelCashBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCashBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlBankBalance;
        private DevExpress.XtraEditors.CheckEdit chkAccountAsCost;
        private DevExpress.XtraLayout.LayoutControlItem layoutAccountAsCost;
        private DevExpress.XtraEditors.TextEdit txtDescription;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.TextEdit txtGrossSalary;
        private DevExpress.XtraLayout.LayoutControlItem layoutGrossSalary;
        private DevExpress.XtraEditors.TextEdit txtTaxableGrossSalary;
        private DevExpress.XtraLayout.LayoutControlItem layoutTaxableGrossSalary;
        private DocumentSerialPlaceHolder voucher;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.TextEdit txtUnclaimedSalary;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.TextEdit txtOverTime;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.CheckEdit chkUnclaimed;
        private DevExpress.XtraLayout.LayoutControlItem layoutChkUnclaimed;
    }
}