﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;

namespace BIZNET.iERP.Client
{
    public partial class GroupPayrollForm : XtraForm
    {
        GroupPayrollDocument m_doc = null;
        private bool newDocument=true;
        private IAccountingClient _client;
        private ActivationParameter _activation;
        private PaymentMethodAndBalanceController _paymentController;
        public GroupPayrollForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            voucher.setKeys(typeof(GroupPayrollDocument), "voucher");

            _client = client;
            _activation = activation;
            _paymentController = new PaymentMethodAndBalanceController(paymentTypeSelector, layoutControlBankAccount, cmbBankAccount, layoutControlBankBalance, labelBankBalance, layoutControlCashAccount, cmbCasher, layoutControlCashBalance, labelCashBalance, layoutControlServiceCharge, layoutControlSlipRef, datePayment, _activation);

            txtTotalIncomeTax.LostFocus += Control_LostFocus;
            txtTotalEmployerPension.LostFocus += Control_LostFocus;
            txtTotalEmployeePension.LostFocus += Control_LostFocus;
            txtSalary.LostFocus += Control_LostFocus;
           
            txtReference.LostFocus += Control_LostFocus;
            InitializeValidationRules();
            LoadTaxCenters();
            costCenterPlaceHolder.SetByID(Globals.currentCostCenterID);
            cmbPeriod.SelectedIndex = 0;
            layoutGrossSalary.HideToCustomization();
            layoutTaxableGrossSalary.HideToCustomization();
            chkDeclareTax.Checked = true;
        }
       

       
        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validationGroupPayoll.Validate(control);
        }

        private void InitializeValidationRules()
        {
            InitializePaymentAmountValidation();
        }

        private void InitializePaymentAmountValidation()
        {
            NonEmptyNumericValidationRule amountValidation = new NonEmptyNumericValidationRule();
            amountValidation.ErrorText = "Amount cannot be blank and cannot contain a value of zero";
            amountValidation.ErrorType = ErrorType.Default;
            validationGroupPayoll.SetValidationRule(txtGrossSalary, amountValidation);
            validationGroupPayoll.SetValidationRule(txtTaxableGrossSalary, amountValidation);
            validationGroupPayoll.SetValidationRule(txtSalary, amountValidation);
        }
        private void LoadTaxCenters()
        {
            TaxCenter[] taxCenter = iERPTransactionClient.GetTaxCenters();
            foreach (TaxCenter center in taxCenter)
            {
                cmbTaxCenter.Properties.Items.Add(center);
            }
            cmbTaxCenter.SelectedIndex = 0;
        }
        private void SetPayrollTaxCenter(int taxCenter)
        {
            int i = 0;
            foreach (TaxCenter center in cmbTaxCenter.Properties.Items)
            {
                if (center.ID == taxCenter)
                {
                    cmbTaxCenter.SelectedIndex = i;
                    break;
                }
                i++;
            }
        }
        internal void LoadData(GroupPayrollDocument groupPayrollDocument)
        {
            m_doc = groupPayrollDocument;
            if (groupPayrollDocument == null)
            {
                ResetControls();
            }
            else
            {
                _paymentController.IgnoreEvents();
                newDocument = false;
                try
                {
                    paymentTypeSelector.PaymentMethod = m_doc.paymentMethod;
                    PopulateControlsForUpdate(groupPayrollDocument);
                }
                finally
                {
                    _paymentController.AcceptEvents();
                }

            }
        }
        private void ResetControls()
        {
            datePayment.DateTime = DateTime.Now;
            txtTotalEmployeePension.Text = "";
            txtTotalEmployerPension.Text = "";
            txtTotalIncomeTax.Text = "";
            txtSalary.Text = "";
            txtReference.Text = "";
            txtNote.Text = "";
            voucher.setReference(-1, null);
        }
        private void PopulateControlsForUpdate(GroupPayrollDocument groupPayrollDocument)
        {
            datePayment.DateTime = groupPayrollDocument.DocumentDate;
            voucher.setReference(groupPayrollDocument.AccountDocumentID, groupPayrollDocument.voucher);
            _paymentController.assetAccountID = groupPayrollDocument.assetAccountID;
            txtDescription.Text = groupPayrollDocument.description;
            SetPayrollTaxCenter(groupPayrollDocument.taxCenter);
            costCenterPlaceHolder.SetByID(groupPayrollDocument.costCenterID);
            cmbPeriod.selectedPeriodID = groupPayrollDocument.periodID;
            chkDeclareTax.Checked = groupPayrollDocument.declareTax;
            chkAccountAsCost.Checked = groupPayrollDocument.accountAsCost;
            chkAccountAsCost.Enabled = false;
            chkUnclaimed.Checked = groupPayrollDocument.fullUnclaimed;
            txtGrossSalary.Text = groupPayrollDocument.grossSalaryTotalAmount == 0 ? "" : groupPayrollDocument.grossSalaryTotalAmount.ToString("N");
            txtTaxableGrossSalary.Text = groupPayrollDocument.taxableGrossSalaryTotalAmount == 0 ? "" : groupPayrollDocument.taxableGrossSalaryTotalAmount.ToString("N");
            txtTotalIncomeTax.Text = groupPayrollDocument.incomeTaxTotalAmount == 0 ? "" : groupPayrollDocument.incomeTaxTotalAmount.ToString("N");
            txtTotalEmployeePension.Text = groupPayrollDocument.employeesTotalPensionAmount == 0 ? "" : groupPayrollDocument.employeesTotalPensionAmount.ToString("N");
            txtTotalEmployerPension.Text = groupPayrollDocument.employerTotalPensionAmount == 0 ? "" : groupPayrollDocument.employerTotalPensionAmount.ToString("N");
            txtOverTime.Text = groupPayrollDocument.overTimeTotalAmount == 0 ? "" : groupPayrollDocument.overTimeTotalAmount.ToString("N");
            txtUnclaimedSalary.Text = groupPayrollDocument.unclaimedSalary == 0 ? "" : groupPayrollDocument.unclaimedSalary.ToString("N");
            txtSalary.Text = groupPayrollDocument.totalSalary.ToString("N");
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(paymentTypeSelector.PaymentMethod))
                txtServiceCharge.Text = groupPayrollDocument.serviceChargeAmount.ToString("N");
            SetReferenceNumber(false);
            txtNote.Text = groupPayrollDocument.ShortDescription;
            if (groupPayrollDocument.declareTax)
            {
                int incomeTaxDeclarationType = AccountingClient.GetDocumentTypeByType(typeof(IncomeTaxDeclarationDocument)).id;
                int pensionTaxDeclarationType = AccountingClient.GetDocumentTypeByType(typeof(PensionTaxDeclarationDocument)).id;
                string periodType = (string)iERPTransactionClient.GetSystemParamter("taxDeclarationPeriods");
                AccountingPeriod[] declarationPeriods = iERPTransactionClient.GetTouchedPeriods(periodType, cmbPeriod.selectedPeriod.fromDate, cmbPeriod.selectedPeriod.toDate);
                bool incomeTaxDeclared = false;
                bool pensionTaxDeclared = false;
                if (declarationPeriods != null && declarationPeriods.Length > 0)
                {
                    foreach (AccountingPeriod period in declarationPeriods)
                    {
                        if (IsTaxDeclaredForPeriod(period.id, incomeTaxDeclarationType))
                        {
                            incomeTaxDeclared = true;
                        }
                        if (IsTaxDeclaredForPeriod(period.id, pensionTaxDeclarationType))
                        {
                            pensionTaxDeclared = true;
                        }
                    }

                    if (incomeTaxDeclared)
                        DisableIncomeTaxControls();

                    if (pensionTaxDeclared)
                        DisablePensionTaxControls();
                }
            }
            //Make the unclaimed salary text box read only if payment has began on the group payroll for the unclaimed ones
            double totalPaid = iERPTransactionClient.GetGroupPayrollTotalSalaryPaid(cmbPeriod.selectedPeriodID, ((TaxCenter)cmbTaxCenter.SelectedItem).ID);
            if (Account.AmountGreater(totalPaid, 0))
                txtUnclaimedSalary.Properties.ReadOnly = true;
        }

        private void DisableIncomeTaxControls()
        {
            DisableCommonControls();
            txtGrossSalary.Properties.ReadOnly = true;
            txtTaxableGrossSalary.Properties.ReadOnly = true;
            txtTotalIncomeTax.Properties.ReadOnly = true;
        }

        private void DisablePensionTaxControls()
        {
            DisableCommonControls();
            txtTotalEmployerPension.Properties.ReadOnly = true;
            txtTotalEmployeePension.Properties.ReadOnly = true;
        }
        private void DisableCommonControls()
        {
            datePayment.Enabled = false;
            txtDescription.Properties.ReadOnly = true;
            cmbPeriod.Enabled = false;
            costCenterPlaceHolder.Enabled = false;
            cmbTaxCenter.Enabled = false;
            chkDeclareTax.Enabled = chkAccountAsCost.Enabled = false;
            txtSalary.Properties.ReadOnly = true;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (voucher.getReference() == null)
                {
                    MessageBox.ShowErrorMessage("Enter valid reference");
                    return;
                }
                if (costCenterPlaceHolder.account == null)
                {
                    MessageBox.ShowErrorMessage("Please pick cost center and try again!");
                    return;
                }
                CostCenter cs = costCenterPlaceHolder.account;
                if (cs.childCount > 1)
                {
                    MessageBox.ShowErrorMessage("Pick cost center with no child!");
                    return;
                }
                if (DateTime.Compare(datePayment.DateTime, DateTime.Now) > 0)
                {
                    MessageBox.ShowErrorMessage("Date cannot be greater than today");
                    return;
                }
                if (chkUnclaimed.Checked && txtUnclaimedSalary.Text == "")
                {
                    MessageBox.ShowErrorMessage("Please enter total unclaimed salary and try again or discard the full unclamied option");
                    return;
                }
                if (validationGroupPayoll.Validate())
                {
                    int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;

                    m_doc = new GroupPayrollDocument();
                    m_doc.AccountDocumentID = docID;
                    m_doc.description = txtDescription.Text;
                    m_doc.costCenterID = costCenterPlaceHolder.GetAccountID();
                    m_doc.periodID = cmbPeriod.selectedPeriodID;
                    m_doc.taxCenter = ((TaxCenter)cmbTaxCenter.SelectedItem).ID;
                    m_doc.declareTax = chkDeclareTax.Checked;
                    m_doc.accountAsCost = chkAccountAsCost.Checked;
                    m_doc.fullUnclaimed = chkUnclaimed.Checked;
                    m_doc.DocumentDate = datePayment.DateTime;
                    m_doc.voucher = voucher.getReference(); 
                    m_doc.assetAccountID = _paymentController.assetAccountID;
                    m_doc.paymentMethod = _paymentController.PaymentTypeSelector.PaymentMethod;
                    m_doc.grossSalaryTotalAmount = txtGrossSalary.Text == "" ? 0 : double.Parse(txtGrossSalary.Text);
                    m_doc.taxableGrossSalaryTotalAmount = txtTaxableGrossSalary.Text == "" ? 0 : double.Parse(txtTaxableGrossSalary.Text);
                    m_doc.incomeTaxTotalAmount = txtTotalIncomeTax.Text == "" ? 0 : double.Parse(txtTotalIncomeTax.Text);
                    m_doc.employeesTotalPensionAmount = txtTotalEmployeePension.Text == "" ? 0 : double.Parse(txtTotalEmployeePension.Text);
                    m_doc.employerTotalPensionAmount = txtTotalEmployerPension.Text == "" ? 0 : double.Parse(txtTotalEmployerPension.Text);
                    m_doc.overTimeTotalAmount = txtOverTime.Text == "" ? 0 : double.Parse(txtOverTime.Text);
                    m_doc.totalSalary = txtSalary.Text == "" ? 0 : double.Parse(txtSalary.Text);
                    m_doc.unclaimedSalary = txtUnclaimedSalary.Text == "" ? 0 : double.Parse(txtUnclaimedSalary.Text);
                    if (!chkUnclaimed.Checked)
                    {
                        if (PaymentMethodHelper.IsPaymentMethodWithReference(_paymentController.PaymentTypeSelector.PaymentMethod))
                        {
                            bool updateDoc = m_doc == null ? false : true;
                            SetReferenceNumber(updateDoc);
                        }
                    }
                    m_doc.ShortDescription = txtNote.Text;
                    if (!chkUnclaimed.Checked)
                    {
                        if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(paymentTypeSelector.PaymentMethod) && txtServiceCharge.Text != "")
                        {
                            m_doc.serviceChargePayer = ServiceChargePayer.Company;
                            double serviceChargeAmount = txtServiceCharge.Text == "" ? 0 : double.Parse(txtServiceCharge.Text);
                            m_doc.serviceChargeAmount = serviceChargeAmount;
                        }
                    }
                    _client.PostGenericDocument(m_doc);
                    string message = docID == -1 ? "Payment successfully saved!" : "Payment successfully updated!";
                    MessageBox.ShowSuccessMessage(message);
                    Close();
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }
            
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private bool IsTaxDeclaredForPeriod(int periodID, int declarationType)
        {
            TaxDeclaration declaration = iERPTransactionClient.GetTaxDeclaration(declarationType, periodID);
            if (declaration == null)
                return false;
            else
                return true;
        }
        
        private void SetReferenceNumber(bool updateDoc)
        {
            switch (_paymentController.PaymentTypeSelector.PaymentMethod)
            {
                case BizNetPaymentMethod.Check:
                    if (newDocument)
                        m_doc.checkNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.checkNumber;
                        else
                            m_doc.checkNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.BankTransferFromCash:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    if (newDocument) 
                        m_doc.slipReferenceNo = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.slipReferenceNo;
                        else
                            m_doc.slipReferenceNo = txtReference.Text;
                    break;
                default:
                    break;
            }
        }


        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtDescription_EditValueChanged(object sender, EventArgs e)
        {
            if (txtDescription.Text == "")
                txtNote.Text = "";
            else
            {
                RefreshNoteDescription();
            }
        }

        private void RefreshNoteDescription()
        {
            string prefix = "Payroll of ";
            string period = " for " + cmbPeriod.selectedPeriod.name;
            txtNote.Text = String.Concat(prefix, txtDescription.Text, period);
        }

        private void chkDeclareTax_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDeclareTax.Checked)
            {
                layoutGrossSalary.RestoreFromCustomization(layoutChkUnclaimed, InsertType.Bottom);
                layoutTaxableGrossSalary.RestoreFromCustomization(layoutGrossSalary, InsertType.Right);
            }
            else
            {
                layoutGrossSalary.HideToCustomization();
                layoutTaxableGrossSalary.HideToCustomization();
                txtGrossSalary.Text = txtTaxableGrossSalary.Text = "";
            }
        }

        private void layoutControl1_LayoutUpdate(object sender, EventArgs e)
        {
            try
            {
                if (layoutControl1.Root.MinSize != null)
                    this.Size = new Size(Size.Width, layoutControl1.Root.MinSize.Height + 50);
            }
            catch { }
        }

        private void cmbPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshNoteDescription();
        }

        private void chkUnclaimed_CheckedChanged(object sender, EventArgs e)
        {
            if (chkUnclaimed.Checked)
            {
                chkDeclareTax.Checked = true;
                chkDeclareTax.Enabled = false;
                layoutControlGroup3.HideToCustomization();
                txtSalary.Text = "";
                layoutNetPay.HideToCustomization();
            }
            else
            {
                chkDeclareTax.Enabled = true;
                layoutControlGroup3.RestoreFromCustomization(layoutControlGroup2, InsertType.Bottom);
                layoutNetPay.RestoreFromCustomization(layoutGrossSalary, InsertType.Bottom);
            }
        }
    }
}
