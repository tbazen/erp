using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHGroupPayrollPayment : bERPClientDocumentHandler
    {
        public DCHGroupPayrollPayment()
            : base(typeof(GroupPayrollPaymentForm))
        {
        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            GroupPayrollPaymentForm f = (GroupPayrollPaymentForm)editor;

            f.LoadData((GroupPayrollPaymentDocument)doc);
        }
    }
}
