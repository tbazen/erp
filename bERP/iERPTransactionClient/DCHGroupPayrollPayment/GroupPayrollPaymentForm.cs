﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;

namespace BIZNET.iERP.Client
{
    public partial class GroupPayrollPaymentForm : XtraForm
    {
        GroupPayrollPaymentDocument m_doc = null;
       
        private bool newDocument=true;
        private double m_TotalUnclaimedSalary;
        private IAccountingClient _client;
        private ActivationParameter _activation;
        private PaymentMethodAndBalanceController _paymentController;
        public GroupPayrollPaymentForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            voucher.setKeys(typeof(GroupPayrollPaymentDocument), "voucher");

            _client = client;
            _activation = activation;
            _paymentController = new PaymentMethodAndBalanceController(paymentTypeSelector, layoutControlBankAccount, cmbBankAccount, layoutControlBankBalance, labelBankBalance, layoutControlCashAccount, cmbCasher, layoutControlCashBalance, labelCashBalance, layoutServiceCharge, layoutControlSlipRef, datePayment, _activation);

            txtAmount.LostFocus += Control_LostFocus;
            txtReference.LostFocus += Control_LostFocus;

            InitializeValidationRules();
            LoadTaxCenters();
            cmbPeriod.SelectedIndex = 0;
        }
        private void LoadTaxCenters()
        {
            TaxCenter[] taxCenter = iERPTransactionClient.GetTaxCenters();
            foreach (TaxCenter center in taxCenter)
            {
                cmbTaxCenter.Properties.Items.Add(center);
            }
            cmbTaxCenter.SelectedIndex = 0;
        }
        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validationGroupPayroll.Validate(control);
        }
        

        private void InitializeValidationRules()
        {
            InitializePaymentAmountValidation();
        }

        private void InitializePaymentAmountValidation()
        {
            NonEmptyNumericValidationRule amountValidation = new NonEmptyNumericValidationRule();
            amountValidation.ErrorText = "Amount cannot be blank and cannot contain a value of zero";
            amountValidation.ErrorType = ErrorType.Default;
            validationGroupPayroll.SetValidationRule(txtAmount, amountValidation);
        }
        private void SetPayrollTaxCenter(int taxCenter)
        {
            int i = 0;
            foreach (TaxCenter center in cmbTaxCenter.Properties.Items)
            {
                if (center.ID == taxCenter)
                {
                    cmbTaxCenter.SelectedIndex = i;
                    break;
                }
                i++;
            }
        }
        internal void LoadData(GroupPayrollPaymentDocument groupPayrollPaymentDocument)
        {
            m_doc = groupPayrollPaymentDocument;
            if (groupPayrollPaymentDocument == null)
            {
                ResetControls();
            }
            else
            {
                _paymentController.IgnoreEvents();
                newDocument = false;
                try
                {
                    _paymentController.PaymentTypeSelector.PaymentMethod = m_doc.paymentMethod;
                    PopulateControlsForUpdate(groupPayrollPaymentDocument);
                }
                finally
                {
                    _paymentController.AcceptEvents();
                }

            }
        }
        private void ResetControls()
        {
            datePayment.DateTime = DateTime.Now;
            txtReference.Text = "";
            txtNote.Text = "";
            voucher.setReference(-1, null);
        }
        private void PopulateControlsForUpdate(GroupPayrollPaymentDocument groupPayrollPaymentDocument)
        {
            datePayment.DateTime = groupPayrollPaymentDocument.DocumentDate;
            txtAmount.Text = TSConstants.FormatBirr(groupPayrollPaymentDocument.amount);
            voucher.setReference(groupPayrollPaymentDocument.AccountDocumentID, groupPayrollPaymentDocument.voucher);
            _paymentController.assetAccountID = groupPayrollPaymentDocument.assetAccountID;
            SetPayrollTaxCenter(groupPayrollPaymentDocument.taxCenter);
            cmbPeriod.selectedPeriodID = groupPayrollPaymentDocument.periodID;
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(paymentTypeSelector.PaymentMethod))
                txtServiceCharge.Text = groupPayrollPaymentDocument.serviceChargeAmount.ToString("N");
            SetReferenceNumber(false);
            txtNote.Text = groupPayrollPaymentDocument.ShortDescription;
        }


        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (voucher.getReference() == null)
                {
                    MessageBox.ShowErrorMessage("Enter valid reference");
                    return;
                }
               
                if (validationGroupPayroll.Validate())
                {
                    if (Account.AmountGreater(double.Parse(txtAmount.Text), m_TotalUnclaimedSalary))
                    {
                        MessageBox.ShowErrorMessage("Payment amount cannot be greater than the total unpaid salary");
                        return;
                    }
                    
                    //else
                    //{
                    //    MessageBox.ShowErrorMessage("Group payroll not found for the selected period and tax center");
                    //    return;
                    //}
                    int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;

                    m_doc = new GroupPayrollPaymentDocument();
                    m_doc.AccountDocumentID = docID;
                    m_doc.amount = double.Parse(txtAmount.Text);
                    m_doc.periodID = cmbPeriod.selectedPeriodID;
                    m_doc.taxCenter = ((TaxCenter)cmbTaxCenter.SelectedItem).ID;
                    m_doc.DocumentDate = datePayment.DateTime;
                    m_doc.voucher = voucher.getReference(); 
                    m_doc.assetAccountID = _paymentController.assetAccountID;
                    m_doc.paymentMethod = _paymentController.PaymentTypeSelector.PaymentMethod;
                    if (PaymentMethodHelper.IsPaymentMethodWithReference(_paymentController.PaymentTypeSelector.PaymentMethod))
                    {
                        bool updateDoc = m_doc == null ? false : true;
                        SetReferenceNumber(updateDoc);
                    }
                    m_doc.ShortDescription = txtNote.Text;
                    if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(paymentTypeSelector.PaymentMethod) && txtServiceCharge.Text != "")
                    {
                        m_doc.serviceChargePayer = ServiceChargePayer.Company;
                        double serviceChargeAmount = txtServiceCharge.Text == "" ? 0 : double.Parse(txtServiceCharge.Text);
                        m_doc.serviceChargeAmount = serviceChargeAmount;
                    }
                    _client.PostGenericDocument(m_doc);
                    string message = docID == -1 ? "Payment successfully saved!" : "Payment successfully updated!";
                    MessageBox.ShowSuccessMessage(message);
                    Close();
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }
            
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void SetReferenceNumber(bool updateDoc)
        {
            switch (_paymentController.PaymentTypeSelector.PaymentMethod)
            {
                case BizNetPaymentMethod.Check:
                    if (newDocument)
                        m_doc.checkNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.checkNumber;
                        else
                            m_doc.checkNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.BankTransferFromCash:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    if (newDocument) 
                        m_doc.slipReferenceNo = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.slipReferenceNo;
                        else
                            m_doc.slipReferenceNo = txtReference.Text;
                    break;
                default:
                    break;
            }
        }


        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            double totalUnclaimed = iERPTransactionClient.GetGroupPayrollUnclaimed(cmbPeriod.selectedPeriodID, ((TaxCenter)cmbTaxCenter.SelectedItem).ID);
            GetTotalUnclaimedSalary(totalUnclaimed);
            labelGroupPaymentBal.Text = m_TotalUnclaimedSalary.ToString("N") + " Birr";
        }

        private void GetTotalUnclaimedSalary(double groupPayrollTotalUnclaimed)
        {
            double totalPaid = iERPTransactionClient.GetGroupPayrollTotalSalaryPaid(cmbPeriod.selectedPeriodID, ((TaxCenter)cmbTaxCenter.SelectedItem).ID);
            m_TotalUnclaimedSalary = groupPayrollTotalUnclaimed - totalPaid;
            if (!newDocument)
                m_TotalUnclaimedSalary += m_doc.amount;
        }

        private void cmbTaxCenter_SelectedIndexChanged(object sender, EventArgs e)
        {
            double totalUnclaimed = iERPTransactionClient.GetGroupPayrollUnclaimed(cmbPeriod.selectedPeriodID, ((TaxCenter)cmbTaxCenter.SelectedItem).ID);
            GetTotalUnclaimedSalary(totalUnclaimed);
            labelGroupPaymentBal.Text = m_TotalUnclaimedSalary.ToString("N") + " Birr";

        }

    }
}
