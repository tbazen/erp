using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    class DCHFixedAssetInventory : bERPClientDocumentHandler
    {
        public DCHFixedAssetInventory()
            : base(typeof(FixedAssetInventoryForm))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            FixedAssetInventoryForm f = (FixedAssetInventoryForm)editor;
            f.LoadData((FixedAssetInventoryDocument)doc);
        }
    }
}
