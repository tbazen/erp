using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    class DCHInternalService : bERPClientDocumentHandler
    {
        public DCHInternalService()
            : base(typeof(InternalServiceForm))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            InternalServiceForm f = (InternalServiceForm)editor;
            f.LoadData((InternalServiceDocument)doc);
        }
    }
}
