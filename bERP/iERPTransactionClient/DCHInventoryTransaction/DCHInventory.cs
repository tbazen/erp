using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    class DCHInventory : bERPClientDocumentHandler
    {
        public DCHInventory()
            : base(typeof(InventoryForm))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            InventoryForm f = (InventoryForm)editor;
            f.LoadData((InventoryDocument)doc);
        }
    }
}
