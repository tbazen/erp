using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    class DCHReturnToStore : bERPClientDocumentHandler
    {
        public DCHReturnToStore()
            : base(typeof(ReturnToStoreForm))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            ReturnToStoreForm f = (ReturnToStoreForm)editor;
            f.LoadData((ReturnToStoreDocument)doc);
        }
    }
}
