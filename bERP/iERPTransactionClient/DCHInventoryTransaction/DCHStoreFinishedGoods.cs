using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    class DCHStoreFinishedGoods : bERPClientDocumentHandler
    {
        public DCHStoreFinishedGoods()
            : base(typeof(StoreFinishedGoodsForm))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            StoreFinishedGoodsForm f = (StoreFinishedGoodsForm)editor;
            f.LoadData((StoreFinishedGoodsDocument)doc);
        }
    }
}
