using System;
using System.Collections.Generic;
using System.Text;
using BIZNET.iERP.Client.DCHInventoryTransaction;

namespace BIZNET.iERP.Client
{
    class DCHStoreIssue : bERPClientDocumentHandler
    {
        public DCHStoreIssue()
            : base(typeof(StoreIssueForm))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            StoreIssueForm f = (StoreIssueForm)editor;
            f.LoadData((StoreIssueDocument)doc);
        }
    }
}
