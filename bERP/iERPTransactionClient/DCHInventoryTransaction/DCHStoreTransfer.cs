using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    class DCHStoreTransfer : bERPClientDocumentHandler
    {
        public DCHStoreTransfer()
            : base(typeof(StoreTransferForm))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            StoreTransferForm f = (StoreTransferForm)editor;
            f.LoadData((StoreTransferDocument)doc);
        }
    }
}
