﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS;
namespace BIZNET.iERP.Client
{
    public partial class FixedAssetInventoryForm : DevExpress.XtraEditors.XtraForm
    {
        FixedAssetInventoryDocument _doc;
        DocumentBasicFieldsController _docFields;
        IAccountingClient _client;
        ActivationParameter _activation;
        TextEdit _textReference;
        DataTable dataTable;
        
        public FixedAssetInventoryForm(IAccountingClient client,ActivationParameter activation)
        {
            InitializeComponent();
            _ignoreEvents = true;
            voucher.setKeys(typeof(InventoryDocument), "voucher");
            _textReference = new TextEdit();
            _client = client;
            _activation = activation;
            _docFields = new DocumentBasicFieldsController(dateTransaction, _textReference, memoNote);
            initGrid();
            _ignoreEvents = false;
            
        }
        void initGrid()
        {
            dataTable = new DataTable();
            dataTable.Columns.Add("itemCode", typeof(string));
            dataTable.Columns.Add("itemName", typeof(string));
            dataTable.Columns.Add("quantity", typeof(double));
            dataTable.Columns.Add("orgVal", typeof(double));
            dataTable.Columns.Add("acDep", typeof(double));

            gridControl.DataSource = dataTable;
            gridView.Columns["itemCode"].Caption = "Code";
            
            gridView.Columns["itemName"].Caption = "Description";
            gridView.Columns["itemName"].OptionsColumn.ReadOnly = true;

            gridView.Columns["quantity"].Caption = "Quantity";
            gridView.Columns["orgVal"].Caption = "Original Value";
            gridView.Columns["acDep"].Caption = "Accumulated Depreciation";
            new GridItemSearchPopupController(gridView, "itemCode", "itemName");
        }

        
        
        internal void LoadData(FixedAssetInventoryDocument inventoryDocument)
        {
            _doc = inventoryDocument;
            _ignoreEvents = true;
            try
            {
                if (inventoryDocument != null)
                {
                    voucher.setReference(inventoryDocument.AccountDocumentID, inventoryDocument.voucher);
                    if (inventoryDocument.voucher != null)
                        _textReference.Text = inventoryDocument.voucher.reference;
                    _docFields.setControlData(_doc);
                        costCenter.SetByID(_doc.costCenterID);
                    foreach (FixedAssetInventoryItem item in _doc.items)
                    {
                        TransactionItems titme=iERPTransactionClient.GetTransactionItems(item.itemCode);
                        dataTable.Rows.Add(titme.Code, titme.Name,item.quantity, item.orginalValue, item.accumulatedDepreciation);
                    }
                }
            }
            finally
            {
                _ignoreEvents = false;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (!dxValidationProvider.Validate())
                    return;
                if (voucher.getReference() == null)
                {
                    MessageBox.ShowErrorMessage("Enter valid reference");
                    return;
                }
                int docID = _doc == null ? -1 : _doc.AccountDocumentID;
                _doc = new FixedAssetInventoryDocument();
                _doc.AccountDocumentID = docID;
                _doc.voucher = voucher.getReference();
                _textReference.Text = _doc.voucher.reference;
                _docFields.setDocumentData(_doc);
                List<FixedAssetInventoryItem> items = new List<FixedAssetInventoryItem>();
                    _doc.costCenterID= costCenter.GetAccountID();
                    foreach (DataRow row in dataTable.Rows)
                    {
                        string code=row["itemCode"] as string;

                        if (!string.IsNullOrEmpty(code))
                        {
                            double q;
                            double orgVal;
                            double acDep;
                            if (!(row["quantity"] is double))
                            {
                                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter quantity for {0}".format(code));
                                return;
                            }
                            q = (double)row["quantity"];
                            if (!(row["orgVal"] is double))
                            {
                                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter orginval value for {0}".format(code));
                                return;
                            }
                            orgVal = (double)row["orgVal"];
                            if (row["acDep"] is double)
                                acDep = (double)row["acDep"];
                            else
                                acDep = 0;
                            items.Add(new FixedAssetInventoryItem()
                            {
                                itemCode=code,
                                orginalValue=orgVal,
                                accumulatedDepreciation=acDep,
                                quantity=q
                            });
                        }
                    }
                _doc.items = items.ToArray();
               
                _doc.AccountDocumentID=_client.PostGenericDocument(_doc);
                if(!(_client is DocumentScheduler))
                {
                    MessageBox.ShowSuccessMessage("Inventory succesfully registered");
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        public bool hasUnitPrice(TransactionItems item)
        {
            return _doc == null;
        }


        public bool queryCellChange(ItemGridControl.ItemGridData.ItemTableRow row, DataColumn column, object oldValue)
        {
            return true;
        }


        public bool queryDeleteRow(ItemGridControl.ItemGridData.ItemTableRow row)
        {
            return true;
        }

        public void afterComit(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {

        }

        void updateBalances()
        {
            
        }
        bool _ignoreEvents=false;
        private void dateTransaction_DateTimeChanged(object sender, EventArgs e)
        {
        }

    }
}

