﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class InventoryForm : DevExpress.XtraEditors.XtraForm,IItemGridController
    {
        InventoryDocument _doc;
        DocumentBasicFieldsController _docFields;
        IAccountingClient _client;
        ActivationParameter _activation;
        TextEdit _textReference;
        bool _fixedAssetMode;
        public InventoryForm(IAccountingClient client,ActivationParameter activation,bool faMode)
        {
            InitializeComponent();
            _ignoreEvents = true;
            voucher.setKeys(typeof(InventoryDocument), "voucher");
            _textReference = new TextEdit();
            _client = client;
            _activation = activation;
            
            if (_activation.storeID != -1)
                storePlaceholder.storeID = _activation.storeID;
            
            itemGrid.Controller = this;
            itemGrid.initializeGrid();
            _docFields = new DocumentBasicFieldsController(dateTransaction, _textReference, memoNote);
            this.fixedAssetMode = faMode;
            _ignoreEvents = false;
        }
        public InventoryForm(IAccountingClient client, ActivationParameter activation)
            :this(client,activation,false)
        {
        }
        public bool fixedAssetMode
        {
            set
            {
                _fixedAssetMode = value;
                layoutStore.Visibility = _fixedAssetMode ? DevExpress.XtraLayout.Utils.LayoutVisibility.Never : DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layoutCostCenter.Visibility = _fixedAssetMode ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                this.Text = _fixedAssetMode ? "Fixed Asset Inventory" : "Store Inventory";
            }
        }
        public double getUnitPrice(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            if (_doc != null)
            {
                foreach (TransactionDocumentItem it in _doc.items)
                {
                    if (it.code.Equals(row.code, StringComparison.CurrentCultureIgnoreCase))
                    {
                        return it.unitPrice;
                    }
                }
            }
            CostCenterAccount cas = AccountingClient.GetCostCenterAccount(storePlaceholder.storeID, item.inventoryAccountID);
            if (cas == null)
                return 0;
            double totalQuantity = AccountingClient.GetNetBalanceAsOf(cas.id, TransactionItem.MATERIAL_QUANTITY, dateTransaction.DateTime);
            double totalValue= AccountingClient.GetNetBalanceAsOf(cas.id, TransactionItem.DEFAULT_CURRENCY, dateTransaction.DateTime);

            if (AccountBase.AmountEqual(totalQuantity, 0))
                return 0;
            return totalValue / totalQuantity;
        }

        public double getBalance(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            if (_doc != null)
            {
                foreach (TransactionDocumentItem it in _doc.items)
                {
                    if (it.code.Equals(row.code, StringComparison.CurrentCultureIgnoreCase))
                    {
                        return it.quantity;
                    }
                }
            }
            CostCenterAccount cas=AccountingClient.GetCostCenterAccount(storePlaceholder.storeID, item.inventoryAccountID);
            if (cas == null)
                return 0;
            return AccountingClient.GetNetBalanceAsOf(cas.id, TransactionItem.MATERIAL_QUANTITY, dateTransaction.DateTime);
        }

        internal void LoadData(InventoryDocument inventoryDocument)
        {
            _doc = inventoryDocument;
            _ignoreEvents = true;
            try
            {
                if (inventoryDocument != null)
                {
                    this.fixedAssetMode = inventoryDocument.fixedAsset;
                    voucher.setReference(inventoryDocument.AccountDocumentID, inventoryDocument.voucher);
                    if (inventoryDocument.voucher != null)
                        _textReference.Text = inventoryDocument.voucher.reference;
                    _docFields.setControlData(_doc);
                    if (_fixedAssetMode)
                        costCenter.SetByID(_doc.storeID);
                    else
                        storePlaceholder.storeID = _doc.storeID;
                    foreach (TransactionDocumentItem item in _doc.items)
                    {
                        itemGrid.Add(item);
                    }
                    itemGrid.updateBalances();
                }
            }
            finally
            {
                _ignoreEvents = false;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (!dxValidationProvider.Validate())
                    return;
                if (voucher.getReference() == null)
                {
                    MessageBox.ShowErrorMessage("Enter valid reference");
                    return;
                }
                int docID = _doc == null ? -1 : _doc.AccountDocumentID;
                _doc = new InventoryDocument();
                _doc.AccountDocumentID = docID;
                _doc.voucher = voucher.getReference();
                _doc.fixedAsset = _fixedAssetMode;
                _textReference.Text = _doc.voucher.reference;
                _docFields.setDocumentData(_doc);
                List<TransactionDocumentItem> items = new List<TransactionDocumentItem>();
                if (_fixedAssetMode)
                    _doc.storeID = costCenter.GetAccountID();
                else
                    _doc.storeID = storePlaceholder.storeID;
                foreach (ItemGridControl.ItemGridData.ItemTableRow row in itemGrid.data)
                {
                    if (!string.IsNullOrEmpty(row.code))
                        items.Add(ItemGrid.toDocumentItem(row));
                }
                _doc.items = items.ToArray();
               
                _doc.AccountDocumentID=_client.PostGenericDocument(_doc);
                if(!(_client is DocumentScheduler))
                {
                    MessageBox.ShowSuccessMessage("Inventory succesfully registered");
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        public bool hasUnitPrice(TransactionItems item)
        {
            return _doc == null;
        }


        public bool queryCellChange(ItemGridControl.ItemGridData.ItemTableRow row, DataColumn column, object oldValue)
        {
            return true;
        }


        public bool queryDeleteRow(ItemGridControl.ItemGridData.ItemTableRow row)
        {
            return true;
        }

        public void afterComit(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {

        }

        void updateBalances()
        {
            
        }
        bool _ignoreEvents=false;
        private void dateTransaction_DateTimeChanged(object sender, EventArgs e)
        {
        }

        private void storePlaceholder_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents) return;
            itemGrid.updateBalances();
        }

        private void dateTransaction_Leave(object sender, EventArgs e)
        {
            if (_ignoreEvents) return;
            itemGrid.updateBalances();
        }

    }
}

