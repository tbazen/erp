﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class RecalculateInventoryWeightedAverage : DevExpress.XtraEditors.XtraForm
    {
        public RecalculateInventoryWeightedAverage()
        {
            InitializeComponent();
        }

        private void buttonRecalculate_Click(object sender, EventArgs e)
        {
            buttonRecalculate.Enabled = false;
            timer.Start();
            new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(
                delegate(object par)
                {
                    Exception error;
                    try
                    {
                        iERPTransactionClient.recalculateInventoryWeightedAverage();
                        error = null;
                    }
                    catch (Exception ex)
                    {
                        error = ex;
                    }
                    this.Invoke(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                        {
                            buttonRecalculate.Enabled = true;
                            timer.Stop();
                            if (error == null)
                            {
                                MessageBox.ShowSuccessMessage("Recalcualtion done successfully.");
                            }
                            else
                            {
                                MessageBox.ShowException(error);
                            }
                        }
                    ));
                }
            )).Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            string msg;
            double p=Math.Round(100*AccountingClient.GetProccessProgress(out msg));
            labelControl1.Text = msg;
            if (p < 0)
                p = 0;
            if (p > 100)
                p = 100;
            progress.Value = (int)p;
        }
    }
}