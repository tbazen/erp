﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class StoreTransferForm : DevExpress.XtraEditors.XtraForm,IItemGridController
    {
        StoreTransferDocument _doc;
        DocumentBasicFieldsController _docFields;
        IAccountingClient _client;
        ActivationParameter _activation;
        TextEdit _textReference;
        public StoreTransferForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            voucher.setKeys(typeof(StoreTransferDocument), "voucher");
            _textReference = new TextEdit();
            _client = client;
            _activation = activation;
            
            if (_activation.storeID != -1)
                storeSource.storeID = _activation.storeID;
            
            itemGrid.Controller = this;
            itemGrid.initializeGrid();
            _docFields = new DocumentBasicFieldsController(dateTransaction, _textReference, memoNote);
        }

        public double getUnitPrice(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            bool hasQuantity;
            return AccountingClient.GetAverageUnitPrice(storeSource.storeID, item.inventoryAccountID, dateTransaction.DateTime,out hasQuantity);
        }

        public double getBalance(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            return Math.Round( AccountingClient.GetNetCostCenterAccountBalanceAsOf(storeSource.storeID, item.inventoryAccountID, TransactionItem.MATERIAL_QUANTITY, dateTransaction.DateTime)
                + AccountingClient.GetNetCostCenterAccountBalanceAsOf(storeSource.storeID, item.finishedGoodAccountID, TransactionItem.MATERIAL_QUANTITY, dateTransaction.DateTime),1);
        }

        internal void LoadData(StoreTransferDocument doc)
        {
            _doc = doc;
            if (doc != null)
            {
                voucher.setReference(doc.AccountDocumentID, doc.voucher);
                if (doc.voucher != null)
                    _textReference.Text = doc.voucher.reference;
                _docFields.setControlData(_doc);
                storeSource.storeID = _doc.sourceStoreID;
                storeDestination.storeID = _doc.destinationStoreID;
                foreach (TransactionDocumentItem item in _doc.items)
                {
                    itemGrid.Add(item);
                }
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (!dxValidationProvider.Validate())
                    return;
                if (voucher.getReference() == null)
                {
                    MessageBox.ShowErrorMessage("Enter valid reference");
                    return;
                }
                if (storeDestination.storeID == storeSource.storeID)
                {
                    MessageBox.ShowErrorMessage("Source and destination stores must be the different");
                }
                int docID = _doc == null ? -1 : _doc.AccountDocumentID;
                _doc = new StoreTransferDocument();
                _doc.AccountDocumentID = docID;
                _doc.voucher = voucher.getReference();
                _textReference.Text = _doc.voucher.reference;
                _docFields.setDocumentData(_doc);
                List<TransactionDocumentItem> items = new List<TransactionDocumentItem>();
                _doc.sourceStoreID= storeSource.storeID;
                _doc.destinationStoreID= storeDestination.storeID;
                foreach (ItemGridControl.ItemGridData.ItemTableRow row in itemGrid.data)
                {
                    if (!string.IsNullOrEmpty(row.code))
                        items.Add(ItemGrid.toDocumentItem(row));
                }
                _doc.items = items.ToArray();
               
                _doc.AccountDocumentID=_client.PostGenericDocument(_doc);
                if(!(_client is DocumentScheduler))
                {
                    MessageBox.ShowSuccessMessage("Store transfer succesfully registered");
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        public bool hasUnitPrice(TransactionItems item)
        {
            return true;
        }


        public bool queryCellChange(ItemGridControl.ItemGridData.ItemTableRow row, DataColumn column, object oldValue)
        {
            return true;
        }


        public bool queryDeleteRow(ItemGridControl.ItemGridData.ItemTableRow row)
        {
            return true;
        }

        public void afterComit(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {

        }

        private void dateTransaction_DateTimeChanged(object sender, EventArgs e)
        {
            itemGrid.updateBalances();
        }

        private void storeSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            itemGrid.updateBalances();
        }

    }
}

