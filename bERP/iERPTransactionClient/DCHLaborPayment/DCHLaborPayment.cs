using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHLaborPayment : bERPClientDocumentHandler
    {
        public DCHLaborPayment()
            : base(typeof(LaborPaymentForm))
        {
        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            LaborPaymentForm f = (LaborPaymentForm)editor;

            f.LoadData((LaborPaymentDocument)doc);
        }
    }
}
