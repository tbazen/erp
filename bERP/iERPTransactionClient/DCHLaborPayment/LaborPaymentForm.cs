﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;

namespace BIZNET.iERP.Client
{
    public partial class LaborPaymentForm : XtraForm
    {
        LaborPaymentDocument m_doc = null;
        CashAccount[] m_cashOnHandAccounts;
        private BankAccountInfo[] m_BankAccounts;
        private bool m_ValidateCash;
        private bool newDocument=true;
        private string paymentSource;

        private IAccountingClient _client;
        private ActivationParameter _activation;
        private PaymentMethodAndBalanceController _paymentController;
        private int m_laborExpenseAccountID;
        public LaborPaymentForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            voucher.setKeys(typeof(LaborPaymentDocument), "voucher");
            _client = client;
            _activation = activation;
            _paymentController = new PaymentMethodAndBalanceController(paymentTypeSelector, layoutControlBankAccount, cmbBankAccount, layoutControlBankBalance, labelBankBalance, layoutControlCashAccount, cmbCasher, layoutControlCashBalance, labelCashBalance,new DevExpress.XtraLayout.LayoutControlItem(), layoutControlSlipRef, datePayment, _activation);
            if (costCenterPlaceHolder.account != null)
                _paymentController.AddCostCenterAccountItem(GetLaborExpenseAccountID(), labelLaborPaymentBal);

            txtAmount.LostFocus += Control_LostFocus;
            txtReference.LostFocus += Control_LostFocus;
            InitializeValidationRules();
            LoadTaxCenters();
        }
        private void LoadTaxCenters()
        {
            TaxCenter[] taxCenter = iERPTransactionClient.GetTaxCenters();
            foreach (TaxCenter center in taxCenter)
            {
                cmbTaxCenter.Properties.Items.Add(center);
            }
            cmbTaxCenter.SelectedIndex = 0;
        }
        private void SetPayrollTaxCenter(int taxCenter)
        {
            int i = 0;
            foreach (TaxCenter center in cmbTaxCenter.Properties.Items)
            {
                if (center.ID == taxCenter)
                {
                    cmbTaxCenter.SelectedIndex = i;
                    break;
                }
                i++;
            }
        }

        private int GetLaborExpenseAccountID()
        {

            int laborExpenseAccountID = Convert.ToInt32(iERPTransactionClient.GetSystemParameters(new string[] { "laborExpenseAccountID" })[0]);
            //AUDIT: Why leaf account?
            //account = GenericAccountingClient<IAccountingService>.GetLeafAccounts(laborExpenseAccountID)[0];
            CostCenterAccount account = INTAPS.Accounting.Client.AccountingClient.GetCostCenterAccount(costCenterPlaceHolder.GetAccountID(),laborExpenseAccountID);
            //double balance = 0;
            if (account != null)
            {
                return account.id;
            }
            else
            {
                return -1;

            }
           
        }
        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validationLaborPayment.Validate(control);
        }

        private void InitializeValidationRules()
        {
            InitializePaymentAmountValidation();
        }

        private void InitializePaymentAmountValidation()
        {
            NonEmptyNumericValidationRule amountValidation = new NonEmptyNumericValidationRule();
            amountValidation.ErrorText = "Amount cannot be blank and cannot contain a value of zero";
            amountValidation.ErrorType = ErrorType.Default;
            validationLaborPayment.SetValidationRule(txtAmount, amountValidation);
        }
        
        internal void LoadData(LaborPaymentDocument laborPaymentDocument)
        {
            m_doc = laborPaymentDocument;
            if (laborPaymentDocument == null)
            {
                ResetControls();
            }
            else
            {
                _paymentController.IgnoreEvents();
                newDocument = false;
                try
                {
                    _paymentController.PaymentTypeSelector.PaymentMethod = m_doc.paymentMethod;
                    PopulateControlsForUpdate(laborPaymentDocument);
                    _paymentController.AddCostCenterAccountItem(GetLaborExpenseAccountID(), labelLaborPaymentBal);
                }
                finally
                {
                    _paymentController.AcceptEvents();
                }

            }
        }
        private void ResetControls()
        {
            datePayment.DateTime = DateTime.Now;
            txtAmount.Text = "";
            txtReference.Text = "";
            txtNote.Text = "";
            voucher.setReference(-1, null);
        }
        private void PopulateControlsForUpdate(LaborPaymentDocument laborPaymentDocument)
        {
            datePayment.DateTime = laborPaymentDocument.DocumentDate;
            _paymentController.assetAccountID = laborPaymentDocument.assetAccountID;
            costCenterPlaceHolder.SetByID(laborPaymentDocument.costCenterID);
            SetPayrollTaxCenter(laborPaymentDocument.taxCenter);
            txtPaidTo.Text = laborPaymentDocument.paidTo;
            txtAmount.Text = TSConstants.FormatBirr(laborPaymentDocument.amount);
            SetReferenceNumber(false);
            txtNote.Text = laborPaymentDocument.ShortDescription;
            voucher.setReference(laborPaymentDocument.AccountDocumentID, laborPaymentDocument.voucher);
        }


        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (costCenterPlaceHolder.account == null)
                {
                    MessageBox.ShowErrorMessage("Please pick cost center and try again!");
                    return;
                }
                CostCenter cs = costCenterPlaceHolder.account;
                if (cs.childCount > 1)
                {
                    MessageBox.ShowErrorMessage("Pick cost center with no child!");
                    return;
                }
                if (voucher.getReference() == null)
                {
                    MessageBox.ShowErrorMessage("Please provide reference!");
                    return;
                }
                if (validationLaborPayment.Validate())
                {
                    int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;

                    m_doc = new LaborPaymentDocument();
                    m_doc.AccountDocumentID = docID;
                    m_doc.costCenterID = costCenterPlaceHolder.GetAccountID();
                    m_doc.taxCenter = ((TaxCenter)cmbTaxCenter.SelectedItem).ID;
                    m_doc.DocumentDate = datePayment.DateTime;
                    m_doc.voucher = voucher.getReference();
                    m_doc.assetAccountID = _paymentController.assetAccountID;
                    m_doc.paymentMethod = _paymentController.PaymentTypeSelector.PaymentMethod;
                    m_doc.paidTo = txtPaidTo.Text;
                    m_doc.amount = double.Parse(txtAmount.Text);
                    if (PaymentMethodHelper.IsPaymentMethodWithReference(_paymentController.PaymentTypeSelector.PaymentMethod))
                    {
                        bool updateDoc = m_doc == null ? false : true;
                        SetReferenceNumber(updateDoc);
                    }
                    m_doc.ShortDescription = txtNote.Text;

                    _client.PostGenericDocument(m_doc);
                    string message = docID == -1 ? "Payment successfully saved!" : "Payment successfully updated!";
                    MessageBox.ShowSuccessMessage(message);
                    Close();
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }
            
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void SetReferenceNumber(bool updateDoc)
        {
            switch (_paymentController.PaymentTypeSelector.PaymentMethod)
            {
                case BizNetPaymentMethod.Check:
                    if (newDocument)
                        m_doc.checkNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.checkNumber;
                        else
                            m_doc.checkNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.BankTransferFromCash:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    if (newDocument) 
                        m_doc.slipReferenceNo = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.slipReferenceNo;
                        else
                            m_doc.slipReferenceNo = txtReference.Text;
                    break;
                default:
                    break;
            }
        }


        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void costCenterPlaceHolder_AccountChanged(object sender, EventArgs e)
        {
            double balance = AccountingClient.GetNetBalanceAsOf(GetLaborExpenseAccountID(), datePayment.DateTime);
                labelLaborPaymentBal.Text = AccountBase.FormatBalance(balance);
        }

    }
}
