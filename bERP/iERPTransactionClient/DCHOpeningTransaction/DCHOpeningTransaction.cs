using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHOpeningTransaction : INTAPS.Accounting.IGenericDocumentClientHandler
    {
        INTAPS.Accounting.IAccountingClient _client = null;
        public void setAccountingClient(INTAPS.Accounting.IAccountingClient client)
        {
            _client = client;
        }
        #region IGenericDocumentClientHandler Members

        public bool CanEdit
        {
            get
            {
                return true;
            }
        }

        public object CreateEditor(bool embeded)
        {
            //return new OpeningTransactionForm();
            return new OpeningTransactionWizard();
        }

        public INTAPS.Accounting.AccountDocument GetEditorDocument(object editor)
        {
            OpeningTransactionWizard openingTranForm = (OpeningTransactionWizard)editor;
            return null;
        }

        public void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            //OpeningTransactionForm openingTranForm = (OpeningTransactionForm)editor;
            OpeningTransactionWizard tranWizard = (OpeningTransactionWizard)editor;
            //openingTranForm.LoadData(doc as OpeningTransactionDocument);
            OpeningTransactionWizard.m_doc = doc as OpeningTransactionDocument;
        }

        public bool VerifyUserEntry(object editor, out string error)
        {
            //OpeningTransactionForm f = (OpeningTransactionForm)editor;
            OpeningTransactionWizard tranWizard = (OpeningTransactionWizard)editor;
            error = null;
            return true;
        }

        #endregion
    }
}
