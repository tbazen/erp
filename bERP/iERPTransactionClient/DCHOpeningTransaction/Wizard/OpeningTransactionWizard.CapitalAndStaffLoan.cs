using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Data;
using INTAPS.Payroll.Client;

namespace BIZNET.iERP.Client
{
    public partial class OpeningTransactionWizard
    {
        private void InitializeCapitalAndStaffLoanAccSettings()
        {
            wizPageCapitalAndStaffLoan.Text = "Step 1 - Setup Capital,Staff Loan and Accumulated VAT Accounts";
            wizPageCapitalAndStaffLoan.DescriptionText = "Please enter balance amounts for the corresponding Capital, Staff Loan and Accumulated VAT Accounts.";

            PrepareCapitalAndStaffLoanAccDataTable();

        }

        private void PrepareCapitalAndStaffLoanAccDataTable()
        {
            PrepareCapitalAccountDataTable();
            PrepareStaffLoanAccountDataTable();
        }

        private void PrepareCapitalAccountDataTable()
        {
            dataTableCapitalAccount.Columns.Add("AccountID", typeof(int));
            dataTableCapitalAccount.Columns.Add("Account", typeof(string));
            dataTableCapitalAccount.Columns.Add("Account Name", typeof(string));
            dataTableCapitalAccount.Columns.Add("Balance", typeof(double));
            gridCapital.DataSource = dataTableCapitalAccount;
            gridViewCapital.Columns["AccountID"].Visible = false;
            gridViewCapital.Columns["Account"].OptionsColumn.AllowEdit = false;
            gridViewCapital.Columns["Account Name"].OptionsColumn.AllowEdit = false;
        }

        private void PrepareStaffLoanAccountDataTable()
        {
            dataTableStaffLoanAccount.Columns.Add("AccountID", typeof(int));
            dataTableStaffLoanAccount.Columns.Add("Staff Loan Account", typeof(string));
            dataTableStaffLoanAccount.Columns.Add("Account Name", typeof(string));
            dataTableStaffLoanAccount.Columns.Add("Balance", typeof(double));
            gridStaffLoan.DataSource = dataTableStaffLoanAccount;
            gridViewStaffLoan.Columns["AccountID"].Visible = false;
            gridViewStaffLoan.Columns["Staff Loan Account"].OptionsColumn.AllowEdit = false;
            gridViewStaffLoan.Columns["Account Name"].OptionsColumn.AllowEdit = false;
        }

        private void InitializeCapitalAndStaffLoanAccInplaceEditors()
        {
            InitializeAccGridInpaceTextEditor(gridViewCapital);
            InitializeAccGridInpaceTextEditor(gridViewStaffLoan);
        }

        private void LoadCapitalAndStaffLoanAccData()
        {
            LoadCapitalAccounts();
            LoadStaffLoanAccounts();
        }

        private void LoadCapitalAccounts()
        {
            object[] configuredAccountIDs = iERPTransactionClient.GetSystemParameters(new string[] { "capitalAccount" });
            if (configuredAccountIDs.Length == 0 || !(configuredAccountIDs[0] is int))
            {
                MessageBox.ShowErrorMessage("Capital Accounts Configuration error!\n Please make sure you've configured the accounts first in accounts configuration form.");
                return;
            }
            object accumulatedVATID = iERPTransactionClient.GetSystemParamter("accumulatedReceivableVATAccountID");
            if (!(accumulatedVATID is int))
            {
                MessageBox.ShowErrorMessage("Accumulated VAT Account Configuration error!\n Please make sure you've configured the accounts first in accounts configuration form.");
                return;
            }
            Account[] capitalAccounts = INTAPS.Accounting.Client.AccountingClient.GetLeafAccounts<Account>(Convert.ToInt32(configuredAccountIDs[0]));
            bool isShareHolderVisible = ShareHoldersLoanVisible();
            foreach (Account account in capitalAccounts)
            {
                if(isShareHolderVisible)
                {
                    if (account.id == Convert.ToInt32(iERPTransactionClient.GetSystemParamter("WithdrawalAccountID")))
                    {
                        continue;
                    }
                }
                else //Withdrawal account
                {
                    if (account.id == (int)PayrollClient.GetSystemParameters(new string[] { "OwnersEquityAccount" })[0]
                        || IsChildShareHolderAccount(account))
                    {
                        continue;
                    }
                }

                double balance = 0;
                balance = GetCapitalAccountBalanceForNonEmptyDocument(account, balance);
                AddCapitalAccount(account, balance);
            }
            AddAccumulatedVATAccount((int)accumulatedVATID);
        }

        private bool IsChildShareHolderAccount(Account childAccount)
        {
            if (childAccount.PID == (int)PayrollClient.GetSystemParameters(new string[] { "OwnersEquityAccount" })[0])
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private double GetCapitalAccountBalanceForNonEmptyDocument(Account account, double balance)
        {
            if (m_doc != null)
            {
                for (int i = 0; i < m_doc.capitalAccountID.Length; i++)
                {
                    if (m_doc.capitalAccountID[i] == account.id)
                    {
                        if (i < m_doc.capitals.Length)
                            balance = m_doc.capitals[i];
                        break;
                    }
                }
            }
            return balance;
        }

        private void AddCapitalAccount(Account account, double balance)
        {
            DataRow row = dataTableCapitalAccount.NewRow();
            row[0] = account.id;
            row[1] = account.Code;
            row[2] = account.Name + " (Capital)";
            row[3] = balance;
            dataTableCapitalAccount.Rows.Add(row);
            gridCapital.DataSource = dataTableCapitalAccount;
        }

        private void AddAccumulatedVATAccount(int accountID)
        {
            Account acc = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(accountID);
            DataRow row = dataTableCapitalAccount.NewRow();
            row[0] = accountID;
            row[1] = acc.Code;
            row[2] = acc.Name;
            if (m_doc != null)
                row[3] = m_doc.accumulatedVATBalance;
            dataTableCapitalAccount.Rows.Add(row);
            gridCapital.DataSource = dataTableCapitalAccount;
        }
        private void LoadStaffLoanAccounts()
        {
            object[] configuredAccountIDs = PayrollClient.GetSystemParameters(new string[] { "StaffLoanAccount" });
            if (configuredAccountIDs.Length == 0 || !(configuredAccountIDs[0] is int))
            {
                MessageBox.ShowErrorMessage("Staff Loan Accounts Configuration error!\n Please make sure you've configured the accounts first in accounts configuration form.");
                return;
            }
            Account[] staffLoanAccounts = INTAPS.Accounting.Client.AccountingClient.GetLeafAccounts<Account>((int)configuredAccountIDs[0]);
            foreach (Account account in staffLoanAccounts)
            {
                double balance = 0;
                balance = GetStaffLoanAccountBalanceForNonEmptyDocument(account, balance);
                AddStaffLoanAccount(account, balance);
            }
        }

        private double GetStaffLoanAccountBalanceForNonEmptyDocument(Account account, double balance)
        {
            if (m_doc != null)
            {
                for (int i = 0; i < m_doc.staffLoanAccountID.Length; i++)
                {
                    if (m_doc.staffLoanAccountID[i] == account.id)
                    {
                        if (i < m_doc.staffLoans.Length)
                            balance = m_doc.staffLoans[i];
                        break;
                    }
                }
            }
            return balance;
        }

        private void AddStaffLoanAccount(Account account, double balance)
        {
            DataRow row = dataTableStaffLoanAccount.NewRow();
            row[0] = account.id;
            row[1] = account.Code;
            row[2] = account.Name;
            row[3] = balance;
            dataTableStaffLoanAccount.Rows.Add(row);
            gridStaffLoan.DataSource = dataTableStaffLoanAccount;
        }

        private void CollectCapitalAndStaffLoanAccountData()
        {
            CollectCapitalAccountData();
            CollectStaffLoanAccountData();
        }
        private void CollectCapitalAccountData()
        {
            m_doc.capitalAccountID = new int[dataTableCapitalAccount.Rows.Count - 1];
            m_doc.capitals = new double[dataTableCapitalAccount.Rows.Count - 1];

            for (int i = 0; i < gridViewCapital.DataRowCount - 1; i++)
            {

                double balance = (double)gridViewCapital.GetRowCellValue(i, "Balance");
                m_doc.capitalAccountID[i] = (int)gridViewCapital.GetRowCellValue(i, "AccountID");
                m_doc.capitals[i] = balance;
            }
            m_doc.accumulatedVATBalance = (double)gridViewCapital.GetRowCellValue(gridViewCapital.DataRowCount - 1, "Balance");
        }
        private void CollectStaffLoanAccountData()
        {
            m_doc.staffLoanAccountID = new int[dataTableStaffLoanAccount.Rows.Count];
            m_doc.staffLoans = new double[dataTableStaffLoanAccount.Rows.Count];

            for (int i = 0; i < gridViewStaffLoan.DataRowCount; i++)
            {

                double balance = (double)gridViewStaffLoan.GetRowCellValue(i, "Balance");
                m_doc.staffLoanAccountID[i] = (int)gridViewStaffLoan.GetRowCellValue(i, "AccountID");
                m_doc.staffLoans[i] = balance;

            }
        }



    }
}
