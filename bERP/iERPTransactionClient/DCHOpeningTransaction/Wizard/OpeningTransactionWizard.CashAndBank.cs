using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Data;

namespace BIZNET.iERP.Client
{
    public partial class OpeningTransactionWizard
    {
        private void InitializeCashAndBankAccSettings()
        {
            wizPageCashAndBank.Text = "Step 1 - Setup Cash on hand and Bank Accounts";
            wizPageCashAndBank.DescriptionText = "Please enter balance amounts for the corresponding Cash on hand and Bank Accounts.";

            PrepareCashAndBankAccDataTable();

        }
        private void PrepareCashAndBankAccDataTable()
        {
            PrepareCashAccountDataTable();
            PrepareBankAccountDataTable();
        }
        private void PrepareCashAccountDataTable()
        {
            dataTableCashAccount.Columns.Add("AccountID", typeof(int));
            dataTableCashAccount.Columns.Add("Cash Account", typeof(string));
            dataTableCashAccount.Columns.Add("Account Name", typeof(string));
            dataTableCashAccount.Columns.Add("Balance", typeof(double));
            gridCashOnHand.DataSource = dataTableCashAccount;
            gridViewCashOnHand.Columns["AccountID"].Visible = false;
            gridViewCashOnHand.Columns["Cash Account"].OptionsColumn.AllowEdit = false;
            gridViewCashOnHand.Columns["Account Name"].OptionsColumn.AllowEdit = false;
        }
        private void PrepareBankAccountDataTable()
        {
            dataTableBankAccount.Columns.Add("AccountID", typeof(int));
            dataTableBankAccount.Columns.Add("Bank Name", typeof(string));
            dataTableBankAccount.Columns.Add("Bank Account", typeof(string));
            dataTableBankAccount.Columns.Add("Balance", typeof(double));
            gridBank.DataSource = dataTableBankAccount;
            gridViewBank.Columns["AccountID"].Visible = false;
            gridViewBank.Columns["Bank Name"].OptionsColumn.AllowEdit = false;
            gridViewBank.Columns["Bank Account"].OptionsColumn.AllowEdit = false;
        }
        private void InitializeCashAndBankAccInplaceEditors()
        {
            InitializeAccGridInpaceTextEditor(gridViewCashOnHand);
            InitializeAccGridInpaceTextEditor(gridViewBank);
        }
        private void LoadCashAndBankAccData()
        {
            LoadCashAccounts();
            LoadBankAccounts();
        }
        private void LoadCashAccounts()
        {

            CashAccount[] cashAccounts = iERPTransactionClient.GetAllCashAccounts(true);
            foreach (CashAccount account in cashAccounts)
            {
                double balance = 0;
                balance = GetCashAccountBalanceForNonEmptyDocument(account, balance);
                AddCashAccount(account, balance);
            }
        }
        private double GetCashAccountBalanceForNonEmptyDocument(CashAccount accounts, double balance)
        {
            if (m_doc != null)
            {
                for (int i = 0; i < m_doc.cashOnHandsAccountID.Length; i++)
                {
                    if (m_doc.cashOnHandsAccountID[i] == accounts.csAccountID)
                    {
                        if (i < m_doc.cashOnHands.Length)
                            balance = m_doc.cashOnHands[i];
                        break;
                    }
                }
            }
            return balance;
        }

        private void AddCashAccount(CashAccount account, double balance)
        {
            DataRow row = dataTableCashAccount.NewRow();
            row[0] = account.csAccountID;
            row[1] = account.code;
            row[2] = account.name;
            row[3] = balance;
            dataTableCashAccount.Rows.Add(row);
            gridCashOnHand.DataSource = dataTableCashAccount;
        }

        private void LoadBankAccounts()
        {
            BankAccountInfo[] bankAccounts = iERPTransactionClient.GetAllBankAccounts();
            foreach (BankAccountInfo account in bankAccounts)
            {
                double balance = 0;
                balance = GetBankAccountBalanceForNonEmptyDocument(account, balance);
                AddBankAccount(account, balance);
            }
        }
        private double GetBankAccountBalanceForNonEmptyDocument(BankAccountInfo account, double bal)
        {
            if (m_doc != null)
            {
                for (int i = 0; i < m_doc.bankAccounts.Length; i++)
                {
                    if (m_doc.bankAccounts[i] == account.mainCsAccount)
                    {
                        if (i < m_doc.bankBalances.Length)
                            bal = m_doc.bankBalances[i];
                        break;
                    }
                }
            }
            return bal;
        }
        private void AddBankAccount(BankAccountInfo account, double balance)
        {
            DataRow row = dataTableBankAccount.NewRow();
            row[0] = account.mainCsAccount;
            row[1] = account.bankName;
            row[2] = account.bankAccountName;
            row[3] = balance;
            dataTableBankAccount.Rows.Add(row);
            gridBank.DataSource = dataTableBankAccount;
        }

        private void CollectCashAndBankAccountData()
        {
            CollectCashAccountData();
            CollectBankAccountData();
        }

        private void CollectCashAccountData()
        {
            m_doc.cashOnHandsAccountID = new int[dataTableCashAccount.Rows.Count];
            m_doc.cashOnHands = new double[dataTableCashAccount.Rows.Count];

            for (int i = 0; i < gridViewCashOnHand.DataRowCount; i++)
            {
                double amount = (double)gridViewCashOnHand.GetRowCellValue(i, "Balance");
                m_doc.cashOnHandsAccountID[i] = (int)gridViewCashOnHand.GetRowCellValue(i, "AccountID");
                m_doc.cashOnHands[i] = amount;
            }

        }
        private void CollectBankAccountData()
        {
            m_doc.bankAccounts = new int[dataTableBankAccount.Rows.Count];
            m_doc.bankBalances = new double[dataTableBankAccount.Rows.Count];

            for (int i = 0; i < gridViewBank.DataRowCount; i++)
            {

                double amount = (double)gridViewBank.GetRowCellValue(i, "Balance");
                m_doc.bankAccounts[i] = (int)gridViewBank.GetRowCellValue(i, "AccountID");
                m_doc.bankBalances[i] = amount;

            }
        }

    }
}
