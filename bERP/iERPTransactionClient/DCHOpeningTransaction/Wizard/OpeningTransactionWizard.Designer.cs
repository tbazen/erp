﻿namespace BIZNET.iERP.Client
{
    partial class OpeningTransactionWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OpeningTransactionWizard));
            this.wizardOpenTransaction = new DevExpress.XtraWizard.WizardControl();
            this.wizPageWelcome = new DevExpress.XtraWizard.WelcomeWizardPage();
            this.wizPageCashAndBank = new DevExpress.XtraWizard.WizardPage();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.dateDocDate = new BIZNET.iERP.Client.BNDualCalendar();
            this.gridBank = new DevExpress.XtraGrid.GridControl();
            this.gridViewBank = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridCashOnHand = new DevExpress.XtraGrid.GridControl();
            this.gridViewCashOnHand = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wizPageCompletion = new DevExpress.XtraWizard.CompletionWizardPage();
            this.wizPageCapitalAndStaffLoan = new DevExpress.XtraWizard.WizardPage();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.gridCapital = new DevExpress.XtraGrid.GridControl();
            this.gridViewCapital = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridStaffLoan = new DevExpress.XtraGrid.GridControl();
            this.gridViewStaffLoan = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.costCenterPlaceHolder = new INTAPS.Accounting.Client.CostCenterPlaceHolder();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.wizardOpenTransaction)).BeginInit();
            this.wizardOpenTransaction.SuspendLayout();
            this.wizPageCashAndBank.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridBank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCashOnHand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCashOnHand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.wizPageCapitalAndStaffLoan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCapital)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCapital)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridStaffLoan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStaffLoan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // wizardOpenTransaction
            // 
            this.wizardOpenTransaction.Appearance.ExteriorPage.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wizardOpenTransaction.Appearance.ExteriorPage.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.wizardOpenTransaction.Appearance.ExteriorPage.Options.UseFont = true;
            this.wizardOpenTransaction.Appearance.ExteriorPage.Options.UseForeColor = true;
            this.wizardOpenTransaction.Appearance.ExteriorPageTitle.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wizardOpenTransaction.Appearance.ExteriorPageTitle.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.wizardOpenTransaction.Appearance.ExteriorPageTitle.Options.UseFont = true;
            this.wizardOpenTransaction.Appearance.ExteriorPageTitle.Options.UseForeColor = true;
            this.wizardOpenTransaction.Appearance.Page.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wizardOpenTransaction.Appearance.Page.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.wizardOpenTransaction.Appearance.Page.Options.UseFont = true;
            this.wizardOpenTransaction.Appearance.Page.Options.UseForeColor = true;
            this.wizardOpenTransaction.Appearance.PageTitle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wizardOpenTransaction.Appearance.PageTitle.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.wizardOpenTransaction.Appearance.PageTitle.Options.UseFont = true;
            this.wizardOpenTransaction.Appearance.PageTitle.Options.UseForeColor = true;
            this.wizardOpenTransaction.Controls.Add(this.label1);
            this.wizardOpenTransaction.Controls.Add(this.costCenterPlaceHolder);
            this.wizardOpenTransaction.Controls.Add(this.wizPageWelcome);
            this.wizardOpenTransaction.Controls.Add(this.wizPageCashAndBank);
            this.wizardOpenTransaction.Controls.Add(this.wizPageCompletion);
            this.wizardOpenTransaction.Controls.Add(this.wizPageCapitalAndStaffLoan);
            this.wizardOpenTransaction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wizardOpenTransaction.Image = ((System.Drawing.Image)(resources.GetObject("wizardOpenTransaction.Image")));
            this.wizardOpenTransaction.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.wizardOpenTransaction.Location = new System.Drawing.Point(0, 0);
            this.wizardOpenTransaction.Name = "wizardOpenTransaction";
            this.wizardOpenTransaction.Pages.AddRange(new DevExpress.XtraWizard.BaseWizardPage[] {
            this.wizPageWelcome,
            this.wizPageCashAndBank,
            this.wizPageCapitalAndStaffLoan,
            this.wizPageCompletion});
            this.wizardOpenTransaction.ShowHeaderImage = true;
            this.wizardOpenTransaction.Size = new System.Drawing.Size(663, 518);
            this.wizardOpenTransaction.SelectedPageChanging += new DevExpress.XtraWizard.WizardPageChangingEventHandler(this.wizardOpenTransaction_SelectedPageChanging);
            this.wizardOpenTransaction.CancelClick += new System.ComponentModel.CancelEventHandler(this.wizardOpenTransaction_CancelClick);
            this.wizardOpenTransaction.FinishClick += new System.ComponentModel.CancelEventHandler(this.wizardOpenTransaction_FinishClick);
            // 
            // wizPageWelcome
            // 
            this.wizPageWelcome.Name = "wizPageWelcome";
            this.wizPageWelcome.Size = new System.Drawing.Size(446, 379);
            // 
            // wizPageCashAndBank
            // 
            this.wizPageCashAndBank.Controls.Add(this.layoutControl1);
            this.wizPageCashAndBank.Name = "wizPageCashAndBank";
            this.wizPageCashAndBank.Size = new System.Drawing.Size(631, 373);
            this.wizPageCashAndBank.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wizPageCashAndBank_PageValidating);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.dateDocDate);
            this.layoutControl1.Controls.Add(this.gridBank);
            this.layoutControl1.Controls.Add(this.gridCashOnHand);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(507, 264, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(631, 373);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // dateDocDate
            // 
            this.dateDocDate.DateTime = new System.DateTime(2013, 3, 19, 9, 38, 47, 194);
            this.dateDocDate.Location = new System.Drawing.Point(51, 7);
            this.dateDocDate.Name = "dateDocDate";
            this.dateDocDate.ShowEthiopian = true;
            this.dateDocDate.ShowGregorian = true;
            this.dateDocDate.ShowTime = true;
            this.dateDocDate.Size = new System.Drawing.Size(576, 21);
            this.dateDocDate.TabIndex = 6;
            this.dateDocDate.VerticalLayout = false;
            // 
            // gridBank
            // 
            this.gridBank.Location = new System.Drawing.Point(19, 189);
            this.gridBank.MainView = this.gridViewBank;
            this.gridBank.Name = "gridBank";
            this.gridBank.Size = new System.Drawing.Size(593, 165);
            this.gridBank.TabIndex = 5;
            this.gridBank.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBank});
            // 
            // gridViewBank
            // 
            this.gridViewBank.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewBank.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Indigo;
            this.gridViewBank.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewBank.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewBank.GridControl = this.gridBank;
            this.gridViewBank.GroupPanelText = "Bank Accounts";
            this.gridViewBank.Name = "gridViewBank";
            // 
            // gridCashOnHand
            // 
            this.gridCashOnHand.Location = new System.Drawing.Point(19, 43);
            this.gridCashOnHand.MainView = this.gridViewCashOnHand;
            this.gridCashOnHand.Name = "gridCashOnHand";
            this.gridCashOnHand.Size = new System.Drawing.Size(593, 118);
            this.gridCashOnHand.TabIndex = 4;
            this.gridCashOnHand.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCashOnHand});
            // 
            // gridViewCashOnHand
            // 
            this.gridViewCashOnHand.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewCashOnHand.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Indigo;
            this.gridViewCashOnHand.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewCashOnHand.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewCashOnHand.GridControl = this.gridCashOnHand;
            this.gridViewCashOnHand.GroupPanelText = "Cash on hand Accounts";
            this.gridViewCashOnHand.Name = "gridViewCashOnHand";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.emptySpaceItem1,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(631, 373);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "Cash on Hand";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(621, 146);
            this.layoutControlGroup2.Text = "Cash on Hand";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridCashOnHand;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(597, 122);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.CustomizationFormText = "Bank Balances";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 170);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(621, 193);
            this.layoutControlGroup3.Text = "Bank Balances";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridBank;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(597, 169);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(10, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.dateDocDate;
            this.layoutControlItem3.CustomizationFormText = "Date:";
            this.layoutControlItem3.Location = new System.Drawing.Point(10, 0);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(135, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(611, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Date:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(30, 13);
            // 
            // wizPageCompletion
            // 
            this.wizPageCompletion.Name = "wizPageCompletion";
            this.wizPageCompletion.Size = new System.Drawing.Size(446, 379);
            // 
            // wizPageCapitalAndStaffLoan
            // 
            this.wizPageCapitalAndStaffLoan.Controls.Add(this.layoutControl2);
            this.wizPageCapitalAndStaffLoan.Name = "wizPageCapitalAndStaffLoan";
            this.wizPageCapitalAndStaffLoan.Size = new System.Drawing.Size(631, 373);
            this.wizPageCapitalAndStaffLoan.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wizPageCapitalAndStaffLoan_PageValidating);
            this.wizPageCapitalAndStaffLoan.PageCommit += new System.EventHandler(this.wizPageCapitalAndStaffLoan_PageCommit);
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.gridCapital);
            this.layoutControl2.Controls.Add(this.gridStaffLoan);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(507, 264, 250, 350);
            this.layoutControl2.Root = this.layoutControlGroup4;
            this.layoutControl2.Size = new System.Drawing.Size(631, 373);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // gridCapital
            // 
            this.gridCapital.Location = new System.Drawing.Point(19, 19);
            this.gridCapital.MainView = this.gridViewCapital;
            this.gridCapital.Name = "gridCapital";
            this.gridCapital.Size = new System.Drawing.Size(593, 158);
            this.gridCapital.TabIndex = 6;
            this.gridCapital.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCapital});
            // 
            // gridViewCapital
            // 
            this.gridViewCapital.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewCapital.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Indigo;
            this.gridViewCapital.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewCapital.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewCapital.GridControl = this.gridCapital;
            this.gridViewCapital.GroupPanelText = "Capital and Accumulated VAT";
            this.gridViewCapital.Name = "gridViewCapital";
            // 
            // gridStaffLoan
            // 
            this.gridStaffLoan.Location = new System.Drawing.Point(19, 205);
            this.gridStaffLoan.MainView = this.gridViewStaffLoan;
            this.gridStaffLoan.Name = "gridStaffLoan";
            this.gridStaffLoan.Size = new System.Drawing.Size(593, 149);
            this.gridStaffLoan.TabIndex = 5;
            this.gridStaffLoan.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewStaffLoan});
            // 
            // gridViewStaffLoan
            // 
            this.gridViewStaffLoan.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewStaffLoan.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Indigo;
            this.gridViewStaffLoan.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewStaffLoan.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewStaffLoan.GridControl = this.gridStaffLoan;
            this.gridViewStaffLoan.GroupPanelText = "Staff Loan";
            this.gridViewStaffLoan.Name = "gridViewStaffLoan";
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6,
            this.layoutControlGroup7});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "Root";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup4.Size = new System.Drawing.Size(631, 373);
            this.layoutControlGroup4.Text = "Root";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup6.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup6.CustomizationFormText = "Bank Balances";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 186);
            this.layoutControlGroup6.Name = "layoutControlGroup3";
            this.layoutControlGroup6.Size = new System.Drawing.Size(621, 177);
            this.layoutControlGroup6.Text = "Staff Loan Accounts";
            this.layoutControlGroup6.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridStaffLoan;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem2";
            this.layoutControlItem5.Size = new System.Drawing.Size(597, 153);
            this.layoutControlItem5.Text = "layoutControlItem2";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup7.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup7.CustomizationFormText = "Capital Accounts";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup4";
            this.layoutControlGroup7.Size = new System.Drawing.Size(621, 186);
            this.layoutControlGroup7.Text = "Capital Accounts";
            this.layoutControlGroup7.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.gridCapital;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem3";
            this.layoutControlItem6.Size = new System.Drawing.Size(597, 162);
            this.layoutControlItem6.Text = "layoutControlItem3";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // costCenterPlaceHolder
            // 
            this.costCenterPlaceHolder.account = null;
            this.costCenterPlaceHolder.AllowAdd = false;
            this.costCenterPlaceHolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.costCenterPlaceHolder.Location = new System.Drawing.Point(429, 50);
            this.costCenterPlaceHolder.Name = "costCenterPlaceHolder";
            this.costCenterPlaceHolder.Size = new System.Drawing.Size(214, 21);
            this.costCenterPlaceHolder.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(346, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Cost Center:";
            // 
            // OpeningTransactionWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(663, 518);
            this.Controls.Add(this.wizardOpenTransaction);
            this.Name = "OpeningTransactionWizard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OpeningTransactoinWizard";
            this.Load += new System.EventHandler(this.OpeningTransactionWizard_Load);
            ((System.ComponentModel.ISupportInitialize)(this.wizardOpenTransaction)).EndInit();
            this.wizardOpenTransaction.ResumeLayout(false);
            this.wizardOpenTransaction.PerformLayout();
            this.wizPageCashAndBank.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridBank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCashOnHand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCashOnHand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.wizPageCapitalAndStaffLoan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCapital)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCapital)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridStaffLoan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStaffLoan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraWizard.WizardControl wizardOpenTransaction;
        private DevExpress.XtraWizard.WelcomeWizardPage wizPageWelcome;
        private DevExpress.XtraWizard.WizardPage wizPageCashAndBank;
        private DevExpress.XtraWizard.CompletionWizardPage wizPageCompletion;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraGrid.GridControl gridBank;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewBank;
        private DevExpress.XtraGrid.GridControl gridCashOnHand;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCashOnHand;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraWizard.WizardPage wizPageCapitalAndStaffLoan;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraGrid.GridControl gridCapital;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCapital;
        private DevExpress.XtraGrid.GridControl gridStaffLoan;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewStaffLoan;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private BIZNET.iERP.Client.BNDualCalendar dateDocDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private System.Windows.Forms.Label label1;
        private INTAPS.Accounting.Client.CostCenterPlaceHolder costCenterPlaceHolder;
    }
}