using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraWizard;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Repository;
using INTAPS.Payroll.Client;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using DevExpress.XtraEditors.DXErrorProvider;

namespace BIZNET.iERP.Client
{
    public partial class OpeningTransactionWizard : XtraForm
    {
        private bool m_CashAndBankPageSettingsInitialized;
        private bool m_CapitalAndStaffLoanPageSettingsInitialized;
        public static OpeningTransactionDocument m_doc;
        private DataTable dataTableCashAccount;
        private DataTable dataTableBankAccount;
        private DataTable dataTableCapitalAccount;
        private DataTable dataTableStaffLoanAccount;

        public OpeningTransactionWizard()
        {
            InitializeComponent();
            dateDocDate.DateTime = DateTime.Now;
            InitializeDataTables();
        }

        private void InitializeDataTables()
        {
            dataTableCashAccount = new DataTable();
            dataTableBankAccount = new DataTable();
            dataTableCapitalAccount = new DataTable();
            dataTableStaffLoanAccount = new DataTable();
        }

        private void InitializeWelcomePage()
        {
            wizPageWelcome.IntroductionText = @"This wizard will assist you setup your initial company transactions
 through a serious of simple steps.";
            wizPageWelcome.ProceedText = "To proceed with configuration, click Next.";
        }

        private void wizardOpenTransaction_SelectedPageChanging(object sender, DevExpress.XtraWizard.WizardPageChangingEventArgs e)
        {
            if (e.Direction == Direction.Forward)
            {
                SetupCashAndBankWizardPage(e);
                SetupCapitalAndStaffLoanWizardPage(e);
                SetupCompletionPage(e);
            }
            else
            {
                ChangeWizPagesNextButtonText(e);
            }
        }

        private void ChangeWizPagesNextButtonText(DevExpress.XtraWizard.WizardPageChangingEventArgs e)
        {
            if (e.Page == wizPageCashAndBank)
            {
                wizardOpenTransaction.NextText = "&Next";
            }
            if (e.Page == wizPageCapitalAndStaffLoan)
            {
                wizardOpenTransaction.NextText = "&Save";
            }
            if (e.Page == wizPageCompletion)
            {
                wizardOpenTransaction.NextText = "&Finish";
            }
        }

        private void SetupCompletionPage(DevExpress.XtraWizard.WizardPageChangingEventArgs e)
        {
            if (e.Page == wizPageCompletion)
            {
                wizardOpenTransaction.NextText = "&Finish";
            }
        }

        private void SetupCapitalAndStaffLoanWizardPage(DevExpress.XtraWizard.WizardPageChangingEventArgs e)
        {
            if (e.Page == wizPageCapitalAndStaffLoan)
            {
                wizardOpenTransaction.NextText = "&Save";
                if (!m_CapitalAndStaffLoanPageSettingsInitialized)
                {
                    InitializeCapitalAndStaffLoanAccSettings();
                    InitializeCapitalAndStaffLoanAccInplaceEditors();
                    LoadCapitalAndStaffLoanAccData();
                    m_CapitalAndStaffLoanPageSettingsInitialized = true;
                }

            }
        }

        private void SetupCashAndBankWizardPage(DevExpress.XtraWizard.WizardPageChangingEventArgs e)
        {
            if (e.Page == wizPageCashAndBank)
            {
                wizardOpenTransaction.NextText = "&Next";
                if (!m_CashAndBankPageSettingsInitialized)
                {
                    InitializeCashAndBankAccSettings();
                    InitializeCashAndBankAccInplaceEditors();
                    LoadCashAndBankAccData();
                    m_CashAndBankPageSettingsInitialized = true;
                }

            }
        }

        private void InitializeAccGridInpaceTextEditor(GridView gridViewAccount)
        {
            RepositoryItemTextEdit txtBalance = new RepositoryItemTextEdit();
            txtBalance.Mask.EditMask = "#,########0.00;";
            txtBalance.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            txtBalance.Mask.UseMaskAsDisplayFormat = true;
            gridViewAccount.GridControl.RepositoryItems.Add(txtBalance);
            gridViewAccount.Columns["Balance"].ColumnEdit = txtBalance;

        }
        private bool ShareHoldersLoanVisible()
        {
            bool showShareHolderLoan = false;
            CompanyProfile profile = iERPTransactionClient.GetCompanyProfile();
            if (profile == null)
            {
                MessageBox.ShowErrorMessage("Company profile not found! Please configure your company profile and try again!");
            }
            else
            {
                switch (profile.BusinessEntity)
                {
                    case BusinessEntity.SoleProprietorship:
                        showShareHolderLoan = false;
                        break;
                    case BusinessEntity.Partnership:
                    case BusinessEntity.GeneralPartnership:
                    case BusinessEntity.LimitedPartnership:
                    case BusinessEntity.ShareCompany:
                    case BusinessEntity.PrivateLimitedCompany:
                    case BusinessEntity.JointVenture:
                        showShareHolderLoan = true;
                        break;
                    default:
                        break;
                }
            }
            return showShareHolderLoan;
        }

        private void wizPageCapitalAndStaffLoan_PageCommit(object sender, EventArgs e)
        {
            wizPageCompletion.AllowBack = false;
            wizPageCompletion.Text = "Opening Account Transactions setup successfully completed!";
            wizPageCompletion.FinishText = "You've successfully finished setting up your account transactions.";
        }

        private void wizPageCapitalAndStaffLoan_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            try
            {
                if (e.Direction == Direction.Forward)
                {
                    if (ValidateEmptyBalance(gridViewCapital) && ValidateEmptyBalance(gridViewStaffLoan))
                    {
                        CollectAccountData();
                        INTAPS.Accounting.Client.AccountingClient.PostGenericDocument(m_doc);
                        e.Valid = true;
                    }
                    else
                    {
                        e.Valid = false;
                    }
                }
            }
            catch (Exception ex)
            {
                e.Valid = false;
                e.ErrorText = String.Format("{0}\n{1}", ex.Message, string.IsNullOrEmpty(ex.InnerException.Message) ? "" : ex.InnerException.Message);
            }
        }
        private bool ValidateEmptyBalance(GridView gridView)
        {
            bool validation = true;
            for (int i = 0; i < gridView.DataRowCount; i++)
            {
                if (gridView.GetRowCellValue(i, "Balance") is DBNull)
                {
                    gridView.FocusedRowHandle = i;
                    gridView.SetColumnError(gridView.Columns["Balance"], "Balance cannot be empty", ErrorType.Default);
                    validation = false;
                }

            }
            return validation;
        }
        private void CollectAccountData()
        {
            int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;
            m_doc = new OpeningTransactionDocument();
            m_doc.AccountDocumentID = docID;
            CollectCashAndBankAccountData();
            CollectCapitalAndStaffLoanAccountData();
            m_doc.DocumentDate = dateDocDate.DateTime;
            m_doc.PaperRef = "Opening Transaction";
            m_doc.ShortDescription = "Opening Transaction";
            m_doc.costCenterID = costCenterPlaceHolder.GetAccountID();
        }

        private void wizardOpenTransaction_FinishClick(object sender, CancelEventArgs e)
        {
            Close();
        }

        private void wizardOpenTransaction_CancelClick(object sender, CancelEventArgs e)
        {
            Close();
        }

        private void OpeningTransactionWizard_Load(object sender, EventArgs e)
        {
            if (m_doc != null)
            {
                wizardOpenTransaction.SelectedPage = wizPageCashAndBank;
                dateDocDate.DateTime = m_doc.DocumentDate;
            }
            else
                InitializeWelcomePage();
        }

        private void wizPageCashAndBank_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            if(!ValidateEmptyBalance(gridViewCashOnHand) || !ValidateEmptyBalance(gridViewBank))
            {
                e.Valid = false;
            }
        }


    }
}
