using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHPayToCashier : bERPClientDocumentHandler
    {
        public DCHPayToCashier()
            : base(typeof(PayToCashierForm))
        {
        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            PayToCashierForm f = (PayToCashierForm)editor;

            f.LoadData((PayToCashierDocument)doc);
        }
    }
}
