﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;

namespace BIZNET.iERP.Client
{
    public partial class PayToCashierForm : XtraForm
    {
        PayToCashierDocument m_doc = null;
        CashAccount[] m_cashOnHandAccounts;
        private BankAccountInfo[] m_BankAccounts;
        private bool m_ValidateCash;
        private bool newDocument=true;
        private string paymentSource;
        private double m_TotalUnclaimedSalary;
        private IAccountingClient _client;
        private ActivationParameter _activation;
        private PaymentMethodAndBalanceController _paymentController;
        public PayToCashierForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            voucher.setKeys(typeof(PayToCashierDocument), "voucher");
            _client = client;
            _activation = activation;
            _paymentController = new PaymentMethodAndBalanceController(paymentTypeSelector, layoutControlBankAccount, cmbBankAccount, layoutControlBankBalance, labelBankBalance, layoutControlCashAccount, cmbCasher, layoutControlCashBalance, labelCashBalance, layoutServiceCharge, layoutControlSlipRef, datePayment, _activation);
            paymentTypeSelector.PaymentMethod = BizNetPaymentMethod.BankTransferFromBankAccount;
            cmbToCashAccount_SelectedIndexChanged(null, null);

            txtAmount.LostFocus += Control_LostFocus;
            txtReference.LostFocus += Control_LostFocus;

            InitializeValidationRules();
        }
        void Control_LostFocus(object sender, EventArgs e)
        {
            try{
            Control control = (Control)sender;
            validationPayToCashier.Validate(control);
            }
            catch { }
        }

        private void InitializeValidationRules()
        {
            InitializePaymentAmountValidation();
        }

        private void InitializePaymentAmountValidation()
        {
            NonEmptyNumericValidationRule amountValidation = new NonEmptyNumericValidationRule();
            amountValidation.ErrorText = "Amount cannot be blank and cannot contain a value of zero";
            amountValidation.ErrorType = ErrorType.Default;
            validationPayToCashier.SetValidationRule(txtAmount, amountValidation);
        }
        internal void LoadData(PayToCashierDocument payToCashierDoc)
        {
            m_doc = payToCashierDoc;
            if (payToCashierDoc == null)
            {
                ResetControls();
            }
            else
            {
                _paymentController.IgnoreEvents();
                newDocument = false;
                try
                {
                    _paymentController.PaymentTypeSelector.PaymentMethod = m_doc.paymentMethod;
                    PopulateControlsForUpdate(payToCashierDoc);
                }
                finally
                {
                    _paymentController.AcceptEvents();
                }

            }
        }
        private void ResetControls()
        {
            datePayment.DateTime = DateTime.Now;
            txtReference.Text = "";
            txtNote.Text = "";
            voucher.setReference(-1, null);
        }
        private void PopulateControlsForUpdate(PayToCashierDocument payToCashierDocument)
        {
            datePayment.DateTime = payToCashierDocument.DocumentDate;
            txtAmount.Text = TSConstants.FormatBirr(payToCashierDocument.amount);
            voucher.setReference(payToCashierDocument.AccountDocumentID, payToCashierDocument.voucher);
            _paymentController.assetAccountID = payToCashierDocument.assetAccountID;
            cmbToCashAccount.cashCsAccountID = payToCashierDocument.destinationCashAccID;
            cmbToCashAccount_SelectedIndexChanged(null, null);
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(paymentTypeSelector.PaymentMethod))
                txtServiceCharge.Text = payToCashierDocument.serviceChargeAmount.ToString("N");
            SetReferenceNumber(false);
            txtNote.Text = payToCashierDocument.ShortDescription;
        }


        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (voucher.getReference() == null)
                {
                    MessageBox.ShowErrorMessage("Enter valid reference");
                    return;
                }
               
                if (validationPayToCashier.Validate())
                {
                    
                    int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;

                    m_doc = new PayToCashierDocument();
                    m_doc.AccountDocumentID = docID;
                    m_doc.amount = double.Parse(txtAmount.Text);
                    m_doc.DocumentDate = datePayment.DateTime;
                    m_doc.voucher = voucher.getReference(); 
                    m_doc.assetAccountID = _paymentController.assetAccountID;
                    m_doc.destinationCashAccID = cmbToCashAccount.cashCsAccountID;
                    m_doc.paymentMethod = _paymentController.PaymentTypeSelector.PaymentMethod;
                    if (PaymentMethodHelper.IsPaymentMethodWithReference(_paymentController.PaymentTypeSelector.PaymentMethod))
                    {
                        bool updateDoc = m_doc == null ? false : true;
                        SetReferenceNumber(updateDoc);
                    }
                    m_doc.ShortDescription = txtNote.Text;
                    if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(paymentTypeSelector.PaymentMethod) && txtServiceCharge.Text != "")
                    {
                        m_doc.serviceChargePayer = ServiceChargePayer.Company;
                        double serviceChargeAmount = txtServiceCharge.Text == "" ? 0 : double.Parse(txtServiceCharge.Text);
                        m_doc.serviceChargeAmount = serviceChargeAmount;
                    }
                    _client.PostGenericDocument(m_doc);
                    string message = docID == -1 ? "Payment successfully saved!" : "Payment successfully updated!";
                    MessageBox.ShowSuccessMessage(message);
                    Close();
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }
            
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void SetReferenceNumber(bool updateDoc)
        {
            switch (_paymentController.PaymentTypeSelector.PaymentMethod)
            {
                case BizNetPaymentMethod.Check:
                    if (newDocument)
                        m_doc.checkNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.checkNumber;
                        else
                            m_doc.checkNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.CPOFromBankAccount:
                    if (newDocument)
                        m_doc.cpoNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.cpoNumber;
                        else
                            m_doc.cpoNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.BankTransferFromCash:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    if (newDocument)
                        m_doc.slipReferenceNo = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.slipReferenceNo;
                        else
                            m_doc.slipReferenceNo = txtReference.Text;
                    break;
                default:
                    break;
            }
        }


        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbToCashAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblToCashBalance.Text = AccountingClient.GetNetBalanceAsOf(cmbToCashAccount.cashCsAccountID, datePayment.DateTime).ToString("N");
        }

        private void datePayment_DateTimeChanged(object sender, EventArgs e)
        {
            lblToCashBalance.Text = AccountingClient.GetNetBalanceAsOf(cmbToCashAccount.cashCsAccountID, datePayment.DateTime).ToString("N");
        }

    }
}
