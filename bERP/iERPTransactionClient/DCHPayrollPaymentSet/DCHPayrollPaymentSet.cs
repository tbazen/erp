using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHPayrollPaymentSet : bERPClientDocumentHandler
    {
        public DCHPayrollPaymentSet()
            : base(typeof(PayPayroll2))
        {
        }
       
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            PayPayroll2 f = (PayPayroll2)editor;

            f.LoadData((PayrollPaymentSetDocument)doc);
        }
    }
}
