using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHPenality : bERPClientDocumentHandler
    {
        public DCHPenality()
            : base(typeof(PenalityPaymentForm))
        {
        }
       
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            PenalityPaymentForm f = (PenalityPaymentForm)editor;

            f.LoadData((PenalityDocument)doc);
        }
    }
}
