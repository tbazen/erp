﻿namespace BIZNET.iERP.Client
{
    partial class PenalityPaymentForm 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.cmbBankAccount = new BIZNET.iERP.Client.BankAccountPlaceholder();
            this.cmbCasher = new BIZNET.iERP.Client.CashAccountPlaceholder();
            this.paymentTypeSelector = new BIZNET.iERP.Client.PaymentTypeSelector();
            this.costCenterPlaceHolder = new INTAPS.Accounting.Client.CostCenterPlaceHolder();
            this.datePayment = new BIZNET.iERP.Client.BNDualCalendar();
            this.labelTotal = new DevExpress.XtraEditors.LabelControl();
            this.txtExemption = new DevExpress.XtraEditors.TextEdit();
            this.txtOtherPenality = new DevExpress.XtraEditors.TextEdit();
            this.txtInterest = new DevExpress.XtraEditors.TextEdit();
            this.txtPaymentVoucherNo = new DevExpress.XtraEditors.TextEdit();
            this.txtServiceCharge = new DevExpress.XtraEditors.TextEdit();
            this.labelBankBalance = new DevExpress.XtraEditors.LabelControl();
            this.labelCashBalance = new DevExpress.XtraEditors.LabelControl();
            this.txtNote = new DevExpress.XtraEditors.MemoEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.txtReference = new DevExpress.XtraEditors.TextEdit();
            this.txtPenality = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutPenality = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlNote = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlBankBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlCashBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlVoucher = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutInterest = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutOtherPenality = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutExemptions = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlSlipRef = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlServiceCharge = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlCashAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlBankAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.validationPenality = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBankAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCasher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentTypeSelector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExemption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOtherPenality.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInterest.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentVoucherNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceCharge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPenality.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPenality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlVoucher)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOtherPenality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExemptions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSlipRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlServiceCharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationPenality)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.cmbBankAccount);
            this.layoutControl1.Controls.Add(this.cmbCasher);
            this.layoutControl1.Controls.Add(this.paymentTypeSelector);
            this.layoutControl1.Controls.Add(this.costCenterPlaceHolder);
            this.layoutControl1.Controls.Add(this.datePayment);
            this.layoutControl1.Controls.Add(this.labelTotal);
            this.layoutControl1.Controls.Add(this.txtExemption);
            this.layoutControl1.Controls.Add(this.txtOtherPenality);
            this.layoutControl1.Controls.Add(this.txtInterest);
            this.layoutControl1.Controls.Add(this.txtPaymentVoucherNo);
            this.layoutControl1.Controls.Add(this.txtServiceCharge);
            this.layoutControl1.Controls.Add(this.labelBankBalance);
            this.layoutControl1.Controls.Add(this.labelCashBalance);
            this.layoutControl1.Controls.Add(this.txtNote);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.lblTitle);
            this.layoutControl1.Controls.Add(this.txtReference);
            this.layoutControl1.Controls.Add(this.txtPenality);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(136, 199, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(737, 509);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // cmbBankAccount
            // 
            this.cmbBankAccount.Location = new System.Drawing.Point(154, 192);
            this.cmbBankAccount.Name = "cmbBankAccount";
            this.cmbBankAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbBankAccount.Properties.ImmediatePopup = true;
            this.cmbBankAccount.Size = new System.Drawing.Size(383, 20);
            this.cmbBankAccount.StyleController = this.layoutControl1;
            this.cmbBankAccount.TabIndex = 23;
            // 
            // cmbCasher
            // 
            this.cmbCasher.Location = new System.Drawing.Point(154, 162);
            this.cmbCasher.Name = "cmbCasher";
            this.cmbCasher.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCasher.Properties.ImmediatePopup = true;
            this.cmbCasher.Size = new System.Drawing.Size(383, 20);
            this.cmbCasher.StyleController = this.layoutControl1;
            this.cmbCasher.TabIndex = 22;
            // 
            // paymentTypeSelector
            // 
            this.paymentTypeSelector.Include_BankTransferFromAccount = true;
            this.paymentTypeSelector.Include_BankTransferFromCash = true;
            this.paymentTypeSelector.Include_Cash = true;
            this.paymentTypeSelector.Include_Check = true;
            this.paymentTypeSelector.Include_CpoFromBank = true;
            this.paymentTypeSelector.Include_CpoFromCash = true;
            this.paymentTypeSelector.Include_Credit = false;
            this.paymentTypeSelector.Location = new System.Drawing.Point(154, 132);
            this.paymentTypeSelector.Name = "paymentTypeSelector";
            this.paymentTypeSelector.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.paymentTypeSelector.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.paymentTypeSelector.Size = new System.Drawing.Size(573, 20);
            this.paymentTypeSelector.StyleController = this.layoutControl1;
            this.paymentTypeSelector.TabIndex = 21;
            // 
            // costCenterPlaceHolder
            // 
            this.costCenterPlaceHolder.account = null;
            this.costCenterPlaceHolder.AllowAdd = false;
            this.costCenterPlaceHolder.Location = new System.Drawing.Point(517, 102);
            this.costCenterPlaceHolder.Name = "costCenterPlaceHolder";
            this.costCenterPlaceHolder.OnlyLeafAccount = false;
            this.costCenterPlaceHolder.Size = new System.Drawing.Size(210, 20);
            this.costCenterPlaceHolder.TabIndex = 20;
            // 
            // datePayment
            // 
            this.datePayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.datePayment.Location = new System.Drawing.Point(154, 69);
            this.datePayment.Name = "datePayment";
            this.datePayment.ShowEthiopian = true;
            this.datePayment.ShowGregorian = true;
            this.datePayment.ShowTime = true;
            this.datePayment.Size = new System.Drawing.Size(596, 21);
            this.datePayment.TabIndex = 19;
            this.datePayment.VerticalLayout = false;
            // 
            // labelTotal
            // 
            this.labelTotal.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelTotal.Location = new System.Drawing.Point(154, 402);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(47, 13);
            this.labelTotal.StyleController = this.layoutControl1;
            this.labelTotal.TabIndex = 18;
            this.labelTotal.Text = "0.00 Birr";
            // 
            // txtExemption
            // 
            this.txtExemption.Location = new System.Drawing.Point(154, 372);
            this.txtExemption.Name = "txtExemption";
            this.txtExemption.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtExemption.Properties.DisplayFormat.FormatString = "#,########0.00;";
            this.txtExemption.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtExemption.Properties.EditFormat.FormatString = "#,########0.00;";
            this.txtExemption.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtExemption.Properties.Mask.EditMask = "#,########0.00;";
            this.txtExemption.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtExemption.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtExemption.Size = new System.Drawing.Size(573, 20);
            this.txtExemption.StyleController = this.layoutControl1;
            this.txtExemption.TabIndex = 17;
            this.txtExemption.EditValueChanged += new System.EventHandler(this.txtExemption_EditValueChanged);
            // 
            // txtOtherPenality
            // 
            this.txtOtherPenality.Location = new System.Drawing.Point(154, 342);
            this.txtOtherPenality.Name = "txtOtherPenality";
            this.txtOtherPenality.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtOtherPenality.Properties.DisplayFormat.FormatString = "#,########0.00;";
            this.txtOtherPenality.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtOtherPenality.Properties.EditFormat.FormatString = "#,########0.00;";
            this.txtOtherPenality.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtOtherPenality.Properties.Mask.EditMask = "#,########0.00;";
            this.txtOtherPenality.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtOtherPenality.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtOtherPenality.Size = new System.Drawing.Size(573, 20);
            this.txtOtherPenality.StyleController = this.layoutControl1;
            this.txtOtherPenality.TabIndex = 16;
            this.txtOtherPenality.EditValueChanged += new System.EventHandler(this.txtOtherPenality_EditValueChanged);
            // 
            // txtInterest
            // 
            this.txtInterest.Location = new System.Drawing.Point(154, 312);
            this.txtInterest.Name = "txtInterest";
            this.txtInterest.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtInterest.Properties.DisplayFormat.FormatString = "#,########0.00;";
            this.txtInterest.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtInterest.Properties.EditFormat.FormatString = "#,########0.00;";
            this.txtInterest.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtInterest.Properties.Mask.EditMask = "#,########0.00;";
            this.txtInterest.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtInterest.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtInterest.Size = new System.Drawing.Size(573, 20);
            this.txtInterest.StyleController = this.layoutControl1;
            this.txtInterest.TabIndex = 15;
            this.txtInterest.EditValueChanged += new System.EventHandler(this.txtInterest_EditValueChanged);
            // 
            // txtPaymentVoucherNo
            // 
            this.txtPaymentVoucherNo.Location = new System.Drawing.Point(154, 102);
            this.txtPaymentVoucherNo.Name = "txtPaymentVoucherNo";
            this.txtPaymentVoucherNo.Properties.Mask.EditMask = "\\w.*";
            this.txtPaymentVoucherNo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtPaymentVoucherNo.Size = new System.Drawing.Size(209, 20);
            this.txtPaymentVoucherNo.StyleController = this.layoutControl1;
            this.txtPaymentVoucherNo.TabIndex = 14;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "Payment voucher number cannot be blank";
            this.validationPenality.SetValidationRule(this.txtPaymentVoucherNo, conditionValidationRule1);
            // 
            // txtServiceCharge
            // 
            this.txtServiceCharge.Location = new System.Drawing.Point(154, 252);
            this.txtServiceCharge.Name = "txtServiceCharge";
            this.txtServiceCharge.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtServiceCharge.Properties.Mask.EditMask = "#,########0.00;";
            this.txtServiceCharge.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtServiceCharge.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtServiceCharge.Size = new System.Drawing.Size(573, 20);
            this.txtServiceCharge.StyleController = this.layoutControl1;
            this.txtServiceCharge.TabIndex = 12;
            // 
            // labelBankBalance
            // 
            this.labelBankBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBankBalance.Location = new System.Drawing.Point(544, 189);
            this.labelBankBalance.Name = "labelBankBalance";
            this.labelBankBalance.Size = new System.Drawing.Size(186, 26);
            this.labelBankBalance.StyleController = this.layoutControl1;
            this.labelBankBalance.TabIndex = 8;
            this.labelBankBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // labelCashBalance
            // 
            this.labelCashBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCashBalance.Location = new System.Drawing.Point(544, 159);
            this.labelCashBalance.Name = "labelCashBalance";
            this.labelCashBalance.Size = new System.Drawing.Size(186, 26);
            this.labelCashBalance.StyleController = this.layoutControl1;
            this.labelCashBalance.TabIndex = 7;
            this.labelCashBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(154, 425);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(573, 46);
            this.txtNote.StyleController = this.layoutControl1;
            this.txtNote.TabIndex = 6;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.buttonCancel);
            this.panelControl1.Controls.Add(this.buttonOk);
            this.panelControl1.Location = new System.Drawing.Point(7, 478);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(723, 24);
            this.panelControl1.TabIndex = 5;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(659, 0);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(59, 23);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(581, 0);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(62, 23);
            this.buttonOk.TabIndex = 4;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AllowHtmlString = true;
            this.lblTitle.Appearance.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lblTitle.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(112)))));
            this.lblTitle.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblTitle.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lblTitle.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.lblTitle.Location = new System.Drawing.Point(5, 5);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(727, 59);
            this.lblTitle.StyleController = this.layoutControl1;
            this.lblTitle.TabIndex = 13;
            this.lblTitle.Text = "Penality Payment";
            // 
            // txtReference
            // 
            this.txtReference.Location = new System.Drawing.Point(154, 222);
            this.txtReference.Name = "txtReference";
            this.txtReference.Properties.Mask.EditMask = "\\w.*";
            this.txtReference.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtReference.Size = new System.Drawing.Size(573, 20);
            this.txtReference.StyleController = this.layoutControl1;
            this.txtReference.TabIndex = 2;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "Slip Reference cannot be empty";
            this.validationPenality.SetValidationRule(this.txtReference, conditionValidationRule2);
            // 
            // txtPenality
            // 
            this.txtPenality.Location = new System.Drawing.Point(154, 282);
            this.txtPenality.Name = "txtPenality";
            this.txtPenality.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtPenality.Properties.DisplayFormat.FormatString = "#,########0.00;";
            this.txtPenality.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtPenality.Properties.EditFormat.FormatString = "#,########0.00;";
            this.txtPenality.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtPenality.Properties.Mask.EditMask = "#,########0.00;";
            this.txtPenality.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPenality.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtPenality.Size = new System.Drawing.Size(573, 20);
            this.txtPenality.StyleController = this.layoutControl1;
            this.txtPenality.TabIndex = 2;
            this.txtPenality.EditValueChanged += new System.EventHandler(this.txtPenality_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutPenality,
            this.layoutControlItem7,
            this.layoutControlNote,
            this.layoutControlBankBalance,
            this.layoutControlCashBalance,
            this.layoutControlGroup2,
            this.layoutControlVoucher,
            this.layoutInterest,
            this.layoutOtherPenality,
            this.layoutExemptions,
            this.layoutControlItem5,
            this.layoutControlSlipRef,
            this.layoutControlServiceCharge,
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlCashAccount,
            this.layoutControlBankAccount});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(737, 509);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutPenality
            // 
            this.layoutPenality.Control = this.txtPenality;
            this.layoutPenality.CustomizationFormText = "Amount:";
            this.layoutPenality.Location = new System.Drawing.Point(0, 272);
            this.layoutPenality.Name = "layoutPenality";
            this.layoutPenality.Size = new System.Drawing.Size(727, 30);
            this.layoutPenality.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutPenality.Text = "Penailty:";
            this.layoutPenality.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.panelControl1;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 471);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(727, 28);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlNote
            // 
            this.layoutControlNote.Control = this.txtNote;
            this.layoutControlNote.CustomizationFormText = "Note:";
            this.layoutControlNote.Location = new System.Drawing.Point(0, 415);
            this.layoutControlNote.MinSize = new System.Drawing.Size(193, 26);
            this.layoutControlNote.Name = "layoutControlNote";
            this.layoutControlNote.Size = new System.Drawing.Size(727, 56);
            this.layoutControlNote.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlNote.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlNote.Text = "Note:";
            this.layoutControlNote.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlBankBalance
            // 
            this.layoutControlBankBalance.Control = this.labelBankBalance;
            this.layoutControlBankBalance.CustomizationFormText = "layoutControlItem8";
            this.layoutControlBankBalance.Location = new System.Drawing.Point(537, 182);
            this.layoutControlBankBalance.MaxSize = new System.Drawing.Size(190, 30);
            this.layoutControlBankBalance.MinSize = new System.Drawing.Size(190, 30);
            this.layoutControlBankBalance.Name = "layoutControlBankBalance";
            this.layoutControlBankBalance.Size = new System.Drawing.Size(190, 30);
            this.layoutControlBankBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlBankBalance.Text = "layoutControlItem8";
            this.layoutControlBankBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlBankBalance.TextToControlDistance = 0;
            this.layoutControlBankBalance.TextVisible = false;
            // 
            // layoutControlCashBalance
            // 
            this.layoutControlCashBalance.Control = this.labelCashBalance;
            this.layoutControlCashBalance.CustomizationFormText = "layoutControlCashBalance";
            this.layoutControlCashBalance.Location = new System.Drawing.Point(537, 152);
            this.layoutControlCashBalance.MaxSize = new System.Drawing.Size(190, 30);
            this.layoutControlCashBalance.MinSize = new System.Drawing.Size(190, 30);
            this.layoutControlCashBalance.Name = "layoutControlCashBalance";
            this.layoutControlCashBalance.Size = new System.Drawing.Size(190, 30);
            this.layoutControlCashBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlCashBalance.Text = "layoutControlCashBalance";
            this.layoutControlCashBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlCashBalance.TextToControlDistance = 0;
            this.layoutControlCashBalance.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(727, 59);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.Color.MidnightBlue;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.Control = this.lblTitle;
            this.layoutControlItem2.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(900, 59);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(596, 59);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem2.Size = new System.Drawing.Size(727, 59);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlVoucher
            // 
            this.layoutControlVoucher.Control = this.txtPaymentVoucherNo;
            this.layoutControlVoucher.CustomizationFormText = "Payment Voucher Number:";
            this.layoutControlVoucher.Location = new System.Drawing.Point(0, 92);
            this.layoutControlVoucher.Name = "layoutControlVoucher";
            this.layoutControlVoucher.Size = new System.Drawing.Size(363, 30);
            this.layoutControlVoucher.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlVoucher.Text = "Document Number:";
            this.layoutControlVoucher.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutInterest
            // 
            this.layoutInterest.Control = this.txtInterest;
            this.layoutInterest.CustomizationFormText = "Interest";
            this.layoutInterest.Location = new System.Drawing.Point(0, 302);
            this.layoutInterest.Name = "layoutInterest";
            this.layoutInterest.Size = new System.Drawing.Size(727, 30);
            this.layoutInterest.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutInterest.Text = "Interest:";
            this.layoutInterest.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutOtherPenality
            // 
            this.layoutOtherPenality.Control = this.txtOtherPenality;
            this.layoutOtherPenality.CustomizationFormText = "Other Penality";
            this.layoutOtherPenality.Location = new System.Drawing.Point(0, 332);
            this.layoutOtherPenality.Name = "layoutOtherPenality";
            this.layoutOtherPenality.Size = new System.Drawing.Size(727, 30);
            this.layoutOtherPenality.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutOtherPenality.Text = "Other Penality:";
            this.layoutOtherPenality.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutExemptions
            // 
            this.layoutExemptions.Control = this.txtExemption;
            this.layoutExemptions.CustomizationFormText = "Exemptions";
            this.layoutExemptions.Location = new System.Drawing.Point(0, 362);
            this.layoutExemptions.Name = "layoutExemptions";
            this.layoutExemptions.Size = new System.Drawing.Size(727, 30);
            this.layoutExemptions.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutExemptions.Text = "Exemptions:";
            this.layoutExemptions.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.labelTotal;
            this.layoutControlItem5.CustomizationFormText = "Total Payment:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 392);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(727, 23);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem5.Text = "Total Payment:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlSlipRef
            // 
            this.layoutControlSlipRef.Control = this.txtReference;
            this.layoutControlSlipRef.CustomizationFormText = "Slip Reference:";
            this.layoutControlSlipRef.Location = new System.Drawing.Point(0, 212);
            this.layoutControlSlipRef.Name = "layoutControlSlipRef";
            this.layoutControlSlipRef.Size = new System.Drawing.Size(727, 30);
            this.layoutControlSlipRef.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlSlipRef.Text = "Slip Reference:";
            this.layoutControlSlipRef.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlServiceCharge
            // 
            this.layoutControlServiceCharge.Control = this.txtServiceCharge;
            this.layoutControlServiceCharge.CustomizationFormText = "Service Charge:";
            this.layoutControlServiceCharge.Location = new System.Drawing.Point(0, 242);
            this.layoutControlServiceCharge.Name = "layoutControlServiceCharge";
            this.layoutControlServiceCharge.Size = new System.Drawing.Size(727, 30);
            this.layoutControlServiceCharge.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlServiceCharge.Text = "Service Charge:";
            this.layoutControlServiceCharge.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.datePayment;
            this.layoutControlItem1.CustomizationFormText = "Date:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 59);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(727, 33);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Text = "Date:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.costCenterPlaceHolder;
            this.layoutControlItem3.CustomizationFormText = "Cost Center:";
            this.layoutControlItem3.Location = new System.Drawing.Point(363, 92);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(364, 30);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "Cost Center:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.paymentTypeSelector;
            this.layoutControlItem4.CustomizationFormText = "Payment Method:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 122);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(727, 30);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem4.Text = "Payment Method:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlCashAccount
            // 
            this.layoutControlCashAccount.Control = this.cmbCasher;
            this.layoutControlCashAccount.CustomizationFormText = "Payment from Cash Account:";
            this.layoutControlCashAccount.Location = new System.Drawing.Point(0, 152);
            this.layoutControlCashAccount.Name = "layoutControlCashAccount";
            this.layoutControlCashAccount.Size = new System.Drawing.Size(537, 30);
            this.layoutControlCashAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlCashAccount.Text = "Payment from Cash Account:";
            this.layoutControlCashAccount.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlBankAccount
            // 
            this.layoutControlBankAccount.Control = this.cmbBankAccount;
            this.layoutControlBankAccount.CustomizationFormText = "Payment from Bank Account:";
            this.layoutControlBankAccount.Location = new System.Drawing.Point(0, 182);
            this.layoutControlBankAccount.Name = "layoutControlBankAccount";
            this.layoutControlBankAccount.Size = new System.Drawing.Size(537, 30);
            this.layoutControlBankAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlBankAccount.Text = "Payment from Bank Account:";
            this.layoutControlBankAccount.TextSize = new System.Drawing.Size(140, 13);
            // 
            // validationPenality
            // 
            this.validationPenality.ValidateHiddenControls = false;
            // 
            // PenalityPaymentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(737, 509);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "PenalityPaymentForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Penality Payment Form";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbBankAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCasher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentTypeSelector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExemption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOtherPenality.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInterest.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentVoucherNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceCharge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPenality.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPenality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlVoucher)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOtherPenality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExemptions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSlipRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlServiceCharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationPenality)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtPenality;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutPenality;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.MemoEdit txtNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlNote;
        private DevExpress.XtraEditors.LabelControl labelCashBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCashBalance;
        private DevExpress.XtraEditors.LabelControl labelBankBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlBankBalance;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validationPenality;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraEditors.TextEdit txtServiceCharge;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlServiceCharge;
        private DevExpress.XtraEditors.LabelControl lblTitle;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit txtPaymentVoucherNo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlVoucher;
        private DevExpress.XtraEditors.TextEdit txtInterest;
        private DevExpress.XtraLayout.LayoutControlItem layoutInterest;
        private DevExpress.XtraEditors.LabelControl labelTotal;
        private DevExpress.XtraEditors.TextEdit txtExemption;
        private DevExpress.XtraEditors.TextEdit txtOtherPenality;
        private DevExpress.XtraLayout.LayoutControlItem layoutOtherPenality;
        private DevExpress.XtraLayout.LayoutControlItem layoutExemptions;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private BIZNET.iERP.Client.BNDualCalendar datePayment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private INTAPS.Accounting.Client.CostCenterPlaceHolder costCenterPlaceHolder;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private PaymentTypeSelector paymentTypeSelector;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private CashAccountPlaceholder cmbCasher;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCashAccount;
        private BankAccountPlaceholder cmbBankAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlBankAccount;
        private DevExpress.XtraEditors.TextEdit txtReference;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlSlipRef;
    }
}