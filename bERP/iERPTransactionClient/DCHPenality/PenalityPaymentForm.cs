﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;
using DevExpress.XtraEditors.Controls;

namespace BIZNET.iERP.Client
{
    public partial class PenalityPaymentForm : XtraForm
    {
        PenalityDocument m_doc = null;
        CashAccount[] m_cashOnHandAccounts;
        private BankAccountInfo[] m_BankAccounts;
        private double m_TotalPayment;
        private bool m_ValidateCash;
        private bool newDocument=true;
        private string paymentSource;

        private IAccountingClient _client;
        private ActivationParameter _activation;
        private PaymentMethodAndBalanceController _paymentController;
        public PenalityPaymentForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            _client = client;
            _activation = activation;
            _paymentController = new PaymentMethodAndBalanceController(paymentTypeSelector, layoutControlBankAccount, cmbBankAccount, layoutControlBankBalance, labelBankBalance, layoutControlCashAccount, cmbCasher, layoutControlCashBalance, labelCashBalance, layoutControlServiceCharge, layoutControlSlipRef, datePayment, _activation);
            _paymentController.PaymentInformationChanged += new EventHandler(_paymentMethodController_PaymentInformationChanged);

            SetFormTitle();

            txtPaymentVoucherNo.LostFocus += Control_LostFocus;
            txtReference.LostFocus += Control_LostFocus;
        }
       

        private void SetFormTitle()
        {

            string title = "Penality Payment ";


            lblTitle.Text = "<size=14><color=#360087><b>" + title + "</b></color></size>" +
                "<size=14><color=#360087><b>" + _paymentController.PaymentSource + "</b></color></size>";
        }
        void _paymentMethodController_PaymentInformationChanged(object sender, EventArgs e)
        {
            SetFormTitle();
        }
       
        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validationPenality.Validate(control);
        }

        internal void LoadData(PenalityDocument penalityDocument)
        {
            m_doc = penalityDocument;
            if (penalityDocument == null)
            {
                ResetControls();
            }
            else
            {
                _paymentController.IgnoreEvents();
                newDocument = false;
                try
                {
                    _paymentController.PaymentTypeSelector.PaymentMethod = m_doc.paymentMethod;
                    PopulateControlsForUpdate(penalityDocument);
                }
                finally
                {
                    _paymentController.AcceptEvents();
                }
                SetFormTitle();
            }
        }
        private void ResetControls()
        {
            datePayment.DateTime = DateTime.Now;
            txtReference.Text = "";
            txtNote.Text = "";
            txtPaymentVoucherNo.Text = txtServiceCharge.Text = "";
            txtPenality.Text = txtInterest.Text = txtOtherPenality.Text = txtExemption.Text = "";
        }
        private void PopulateControlsForUpdate(PenalityDocument penalityDocument)
        {
            datePayment.DateTime = penalityDocument.DocumentDate;
            costCenterPlaceHolder.SetByID(penalityDocument.costCenterID);
            txtPaymentVoucherNo.Text = penalityDocument.PaperRef;
            txtPenality.Text = penalityDocument.penality.ToString("N");
            txtInterest.Text = penalityDocument.interest.ToString("N");
            txtOtherPenality.Text = penalityDocument.unaccountedTax.ToString("N");
            txtExemption.Text = penalityDocument.deduction.ToString("N");
            _paymentController.assetAccountID = penalityDocument.assetAccountID;
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(_paymentController.PaymentTypeSelector.PaymentMethod))
                txtServiceCharge.Text = penalityDocument.serviceChargeAmount.ToString("N");
            SetReferenceNumber(false);
            txtNote.Text = penalityDocument.ShortDescription;
        }
       
        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (costCenterPlaceHolder.account == null)
                {
                    MessageBox.ShowErrorMessage("Please pick cost center and try again!");
                    return;
                }
                CostCenter cs = costCenterPlaceHolder.account;
                if (cs.childCount > 1)
                {
                    MessageBox.ShowErrorMessage("Pick cost center with no child!");
                    return;
                }
                if (validationPenality.Validate())
                {
                    int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;

                    m_doc = new PenalityDocument();
                    m_doc.AccountDocumentID = docID;
                    m_doc.costCenterID = costCenterPlaceHolder.GetAccountID();
                    m_doc.DocumentDate = datePayment.DateTime;
                    m_doc.PaperRef = txtPaymentVoucherNo.Text;
                    m_doc.paymentMethod = _paymentController.PaymentTypeSelector.PaymentMethod;
                    m_doc.penality = txtPenality.Text == "" ? 0 : double.Parse(txtPenality.Text);
                    m_doc.interest = txtInterest.Text == "" ? 0 : double.Parse(txtInterest.Text);
                    m_doc.unaccountedTax = txtOtherPenality.Text == "" ? 0 : double.Parse(txtOtherPenality.Text);
                    m_doc.deduction = txtExemption.Text == "" ? 0 : double.Parse(txtExemption.Text);
                    m_doc.assetAccountID = _paymentController.assetAccountID;
                    if (PaymentMethodHelper.IsPaymentMethodWithReference(_paymentController.PaymentTypeSelector.PaymentMethod))
                    {
                        bool updateDoc = m_doc == null ? false : true;
                        SetReferenceNumber(updateDoc);
                    }
                    m_doc.ShortDescription = txtNote.Text;
                    if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(_paymentController.PaymentTypeSelector.PaymentMethod))
                    {
                        m_doc.serviceChargePayer = ServiceChargePayer.Company;
                        double serviceChargeAmount = txtServiceCharge.Text == "" ? 0 : double.Parse(txtServiceCharge.Text);
                        m_doc.serviceChargeAmount = serviceChargeAmount;
                    }
                    _client.PostGenericDocument(m_doc);
                    string message = docID == -1 ? "Penality Payment successfully saved!" : "Penality Payment successfully updated!";
                    MessageBox.ShowSuccessMessage(message);
                    Close();
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }
            
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void SetReferenceNumber(bool updateDoc)
        {
            switch (paymentTypeSelector.PaymentMethod)
            {
                
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.CPOFromBankAccount:
                    if (newDocument)
                        m_doc.cpoNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.cpoNumber;
                        else
                            m_doc.cpoNumber = txtReference.Text;
                    break;
                
                default:
                    break;
            }
        }


        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private double CalculateTotalPayment()
        {
            double penality = txtPenality.Text == "" ? 0 : double.Parse(txtPenality.Text);
            double interest = txtInterest.Text == "" ? 0 : double.Parse(txtInterest.Text);
            double otherPenality = txtOtherPenality.Text == "" ? 0 : double.Parse(txtOtherPenality.Text);
            double exemption = txtExemption.Text == "" ? 0 : double.Parse(txtExemption.Text);

            m_TotalPayment = (penality + interest + otherPenality) - exemption;
            labelTotal.Text = TSConstants.FormatBirr(Math.Round(m_TotalPayment,2)) + " Birr";
            return m_TotalPayment;
        }

        private void txtInterest_EditValueChanged(object sender, EventArgs e)
        {
            CalculateTotalPayment();
        }

        private void txtOtherPenality_EditValueChanged(object sender, EventArgs e)
        {
            CalculateTotalPayment();
        }

        private void txtExemption_EditValueChanged(object sender, EventArgs e)
        {
            double penality = txtPenality.Text == "" ? 0 : double.Parse(txtPenality.Text);
            double interest = txtInterest.Text == "" ? 0 : double.Parse(txtInterest.Text);
            double otherPenality = txtOtherPenality.Text == "" ? 0 : double.Parse(txtOtherPenality.Text);
            double totalDeclaredAmount = penality + interest + otherPenality;
            InitializeExemptionValidation(totalDeclaredAmount);
            if (validationPenality.Validate(txtExemption))
                CalculateTotalPayment();
        }

        private void InitializeExemptionValidation(double totalDeclaredAmount)
        {
            TaxExemptionValidationRule exemptionValidation = new TaxExemptionValidationRule(totalDeclaredAmount);
            exemptionValidation.ErrorType = ErrorType.Default;
            exemptionValidation.ErrorText = "The exemption amount you entered will make total payment negative. Please correct it and try again!";
            validationPenality.SetValidationRule(txtExemption, exemptionValidation);
        }

        private void txtPenality_EditValueChanged(object sender, EventArgs e)
        {
            CalculateTotalPayment();
        }

    }
}
