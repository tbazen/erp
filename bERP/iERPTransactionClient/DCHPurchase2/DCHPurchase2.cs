﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHPurchase2: bERPClientDocumentHandler
    {
        public DCHPurchase2()
            : base(typeof(Purchase2Form<Purchase2Document>))
        {
        }
        public DCHPurchase2(Type t)
            : base(t)
        {
        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            Purchase2Form<Purchase2Document> f = (Purchase2Form<Purchase2Document>)editor;
            f.LoadData((Purchase2Document)doc);
        }
    }
}
