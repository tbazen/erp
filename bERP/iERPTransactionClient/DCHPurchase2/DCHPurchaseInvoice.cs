﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHPurchaseInvoice : bERPClientDocumentHandler
    {
        public DCHPurchaseInvoice()
            : base(typeof(PurchaseInvoiceForm))
        {
        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            ((PurchaseInvoiceForm)editor).LoadData((PurchaseInvoiceDocument)doc);
        }
    }
}
