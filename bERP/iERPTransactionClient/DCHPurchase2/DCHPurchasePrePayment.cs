using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHPurchasePrePayment : bERPClientDocumentHandler
    {
        public DCHPurchasePrePayment()
            : base(typeof(PurchasePrePaymentForm))
        {
        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            ((PurchasePrePaymentForm)editor).LoadData((PurchasePrePaymentDocument)doc);
        }
    }

}
