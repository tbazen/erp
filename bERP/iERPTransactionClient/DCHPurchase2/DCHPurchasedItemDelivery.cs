using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHPurchasedItemDelivery : bERPClientDocumentHandler
    {
        public DCHPurchasedItemDelivery()
            : base(typeof(PurchasedItemDeliveryForm))
        {
        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            ((PurchasedItemDeliveryForm)editor).LoadData((PurchasedItemDeliveryDocument)doc);
        }
    }

}
