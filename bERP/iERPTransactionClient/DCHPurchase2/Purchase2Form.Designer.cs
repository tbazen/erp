﻿namespace BIZNET.iERP.Client
{
    partial class Purchase2Form<PurchaseType>
        where PurchaseType:Purchase2Document, new()
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textRetention = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textVATWithholdingReceiptNo = new DevExpress.XtraEditors.TextEdit();
            this.textVATBase = new DevExpress.XtraEditors.TextEdit();
            this.textWHTNo = new DevExpress.XtraEditors.TextEdit();
            this.textWHBase = new DevExpress.XtraEditors.TextEdit();
            this.checkFullVAT = new DevExpress.XtraEditors.CheckEdit();
            this.checkWitholdFull = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutVATBase = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutFullVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutWHBase = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutFullWH = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutWHTRNo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutVWHRef = new DevExpress.XtraLayout.LayoutControlItem();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.paymentTypeSelector = new BIZNET.iERP.Client.PaymentTypeSelector();
            this.memoNote = new DevExpress.XtraEditors.MemoEdit();
            this.checkPayFullAmount = new DevExpress.XtraEditors.CheckEdit();
            this.cashAccountPlaceholder = new BIZNET.iERP.Client.CashAccountPlaceholder();
            this.textPaidAmount = new DevExpress.XtraEditors.TextEdit();
            this.bankAccountPlaceholder = new BIZNET.iERP.Client.BankAccountPlaceholder();
            this.textTransferReference = new DevExpress.XtraEditors.TextEdit();
            this.textCredit = new DevExpress.XtraEditors.TextEdit();
            this.labelCashBalance = new DevExpress.XtraEditors.LabelControl();
            this.labelBankBalance = new DevExpress.XtraEditors.LabelControl();
            this.textServiceCharge = new DevExpress.XtraEditors.TextEdit();
            this.textSettleAdvancePayment = new DevExpress.XtraEditors.TextEdit();
            this.checkSettleAll = new DevExpress.XtraEditors.CheckEdit();
            this.checAllCredit = new DevExpress.XtraEditors.CheckEdit();
            this.checkRelationSettled = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutSettleAdvance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutSettleMaxAdvance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTaxation = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCollectedCash = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTransferRefernce = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutServiceCharge = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutServiceChargeBy = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCashAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCashBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBankAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBankBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCredit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutFullCredit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRetention = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCollectFullAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonCredit = new DevExpress.XtraEditors.SimpleButton();
            this.buttonDeliveries = new DevExpress.XtraEditors.SimpleButton();
            this.buttonPrepayments = new DevExpress.XtraEditors.SimpleButton();
            this.buttonSetInvoice = new DevExpress.XtraEditors.SimpleButton();
            this.itemSummaryGrid = new BIZNET.iERP.Client.ItemSummaryGrid();
            this.gridViewSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelPaymentInstruction = new DevExpress.XtraEditors.LabelControl();
            this.voucher = new BIZNET.iERP.Client.DocumentSerialPlaceHolder();
            this.textReference = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.checkInvoiced = new DevExpress.XtraEditors.CheckEdit();
            this.itemGrid = new BIZNET.iERP.Client.ItemGrid();
            this.gridViewItems = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelCustomerInformation = new DevExpress.XtraEditors.LabelControl();
            this.dateTransaction = new BIZNET.iERP.Client.BNDualCalendar();
            this.linkEditSupplier = new DevExpress.XtraEditors.HyperLinkEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRelationInformation = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.buttonPrev = new DevExpress.XtraEditors.SimpleButton();
            this.buttonNext = new DevExpress.XtraEditors.SimpleButton();
            this.dxValidationProvider = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            ((System.ComponentModel.ISupportInitialize)(this.textRetention.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textVATWithholdingReceiptNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textVATBase.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWHTNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWHBase.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkFullVAT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkWitholdFull.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVATBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFullVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWHBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFullWH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWHTRNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVWHRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentTypeSelector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPayFullAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cashAccountPlaceholder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textPaidAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bankAccountPlaceholder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textTransferReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCredit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textServiceCharge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSettleAdvancePayment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSettleAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checAllCredit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkRelationSettled.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSettleAdvance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSettleMaxAdvance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTaxation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCollectedCash)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTransferRefernce)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServiceCharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServiceChargeBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCashAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCashBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBankBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFullCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRetention)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCollectFullAmount)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.itemSummaryGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkInvoiced.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkEditSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRelationInformation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // textRetention
            // 
            this.textRetention.Location = new System.Drawing.Point(157, 182);
            this.textRetention.Name = "textRetention";
            this.textRetention.Properties.Mask.EditMask = "#,########0.00;";
            this.textRetention.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textRetention.Size = new System.Drawing.Size(163, 20);
            this.textRetention.StyleController = this.layoutControl3;
            this.textRetention.TabIndex = 33;
            this.textRetention.EditValueChanged += new System.EventHandler(this.textRetention_EditValueChanged);
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.groupControl1);
            this.layoutControl3.Controls.Add(this.labelControl3);
            this.layoutControl3.Controls.Add(this.paymentTypeSelector);
            this.layoutControl3.Controls.Add(this.memoNote);
            this.layoutControl3.Controls.Add(this.checkPayFullAmount);
            this.layoutControl3.Controls.Add(this.cashAccountPlaceholder);
            this.layoutControl3.Controls.Add(this.textPaidAmount);
            this.layoutControl3.Controls.Add(this.textRetention);
            this.layoutControl3.Controls.Add(this.bankAccountPlaceholder);
            this.layoutControl3.Controls.Add(this.textTransferReference);
            this.layoutControl3.Controls.Add(this.textCredit);
            this.layoutControl3.Controls.Add(this.labelCashBalance);
            this.layoutControl3.Controls.Add(this.labelBankBalance);
            this.layoutControl3.Controls.Add(this.textServiceCharge);
            this.layoutControl3.Controls.Add(this.textSettleAdvancePayment);
            this.layoutControl3.Controls.Add(this.checkSettleAll);
            this.layoutControl3.Controls.Add(this.checAllCredit);
            this.layoutControl3.Controls.Add(this.checkRelationSettled);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(773, 380);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.BorderColor = System.Drawing.Color.Black;
            this.groupControl1.Appearance.Options.UseBorderColor = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.layoutControl1);
            this.groupControl1.Location = new System.Drawing.Point(12, 44);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(749, 104);
            this.groupControl1.TabIndex = 40;
            this.groupControl1.Text = "Taxation";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textVATWithholdingReceiptNo);
            this.layoutControl1.Controls.Add(this.textVATBase);
            this.layoutControl1.Controls.Add(this.textWHTNo);
            this.layoutControl1.Controls.Add(this.textWHBase);
            this.layoutControl1.Controls.Add(this.checkFullVAT);
            this.layoutControl1.Controls.Add(this.checkWitholdFull);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 24);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(745, 78);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textVATWithholdingReceiptNo
            // 
            this.textVATWithholdingReceiptNo.Location = new System.Drawing.Point(143, 42);
            this.textVATWithholdingReceiptNo.Name = "textVATWithholdingReceiptNo";
            this.textVATWithholdingReceiptNo.Size = new System.Drawing.Size(129, 20);
            this.textVATWithholdingReceiptNo.StyleController = this.layoutControl1;
            this.textVATWithholdingReceiptNo.TabIndex = 40;
            // 
            // textVATBase
            // 
            this.textVATBase.Location = new System.Drawing.Point(143, 14);
            this.textVATBase.Name = "textVATBase";
            this.textVATBase.Properties.Mask.EditMask = "#,########0.00;";
            this.textVATBase.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textVATBase.Size = new System.Drawing.Size(129, 20);
            this.textVATBase.StyleController = this.layoutControl1;
            this.textVATBase.TabIndex = 35;
            // 
            // textWHTNo
            // 
            this.textWHTNo.Location = new System.Drawing.Point(525, 39);
            this.textWHTNo.Name = "textWHTNo";
            this.textWHTNo.Size = new System.Drawing.Size(208, 20);
            this.textWHTNo.StyleController = this.layoutControl1;
            this.textWHTNo.TabIndex = 29;
            // 
            // textWHBase
            // 
            this.textWHBase.Location = new System.Drawing.Point(525, 12);
            this.textWHBase.Name = "textWHBase";
            this.textWHBase.Properties.Mask.EditMask = "#,########0.00;";
            this.textWHBase.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textWHBase.Size = new System.Drawing.Size(112, 20);
            this.textWHBase.StyleController = this.layoutControl1;
            this.textWHBase.TabIndex = 38;
            // 
            // checkFullVAT
            // 
            this.checkFullVAT.Location = new System.Drawing.Point(280, 14);
            this.checkFullVAT.Name = "checkFullVAT";
            this.checkFullVAT.Properties.Caption = "Full Invoice";
            this.checkFullVAT.Size = new System.Drawing.Size(110, 19);
            this.checkFullVAT.StyleController = this.layoutControl1;
            this.checkFullVAT.TabIndex = 36;
            // 
            // checkWitholdFull
            // 
            this.checkWitholdFull.Location = new System.Drawing.Point(643, 14);
            this.checkWitholdFull.Name = "checkWitholdFull";
            this.checkWitholdFull.Properties.Caption = "Full Invoice";
            this.checkWitholdFull.Size = new System.Drawing.Size(88, 19);
            this.checkWitholdFull.StyleController = this.layoutControl1;
            this.checkWitholdFull.TabIndex = 39;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutVATBase,
            this.layoutFullVAT,
            this.layoutWHBase,
            this.layoutFullWH,
            this.layoutWHTRNo,
            this.layoutVWHRef});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(745, 78);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutVATBase
            // 
            this.layoutVATBase.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutVATBase.AppearanceItemCaption.Options.UseFont = true;
            this.layoutVATBase.Control = this.textVATBase;
            this.layoutVATBase.CustomizationFormText = "VAT Base";
            this.layoutVATBase.Location = new System.Drawing.Point(0, 0);
            this.layoutVATBase.Name = "layoutVATBase";
            this.layoutVATBase.Size = new System.Drawing.Size(266, 28);
            this.layoutVATBase.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutVATBase.Text = "VAT Base";
            this.layoutVATBase.TextSize = new System.Drawing.Size(126, 13);
            // 
            // layoutFullVAT
            // 
            this.layoutFullVAT.Control = this.checkFullVAT;
            this.layoutFullVAT.CustomizationFormText = "layoutControlItem17";
            this.layoutFullVAT.Location = new System.Drawing.Point(266, 0);
            this.layoutFullVAT.Name = "layoutFullVAT";
            this.layoutFullVAT.Size = new System.Drawing.Size(118, 58);
            this.layoutFullVAT.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutFullVAT.Text = "layoutFullVAT";
            this.layoutFullVAT.TextSize = new System.Drawing.Size(0, 0);
            this.layoutFullVAT.TextToControlDistance = 0;
            this.layoutFullVAT.TextVisible = false;
            // 
            // layoutWHBase
            // 
            this.layoutWHBase.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutWHBase.AppearanceItemCaption.Options.UseFont = true;
            this.layoutWHBase.Control = this.textWHBase;
            this.layoutWHBase.CustomizationFormText = "Witholding Base";
            this.layoutWHBase.Location = new System.Drawing.Point(384, 0);
            this.layoutWHBase.Name = "layoutWHBase";
            this.layoutWHBase.Size = new System.Drawing.Size(245, 27);
            this.layoutWHBase.Text = "Witholding Base";
            this.layoutWHBase.TextSize = new System.Drawing.Size(126, 13);
            // 
            // layoutFullWH
            // 
            this.layoutFullWH.Control = this.checkWitholdFull;
            this.layoutFullWH.CustomizationFormText = "layoutControlItem19";
            this.layoutFullWH.Location = new System.Drawing.Point(629, 0);
            this.layoutFullWH.Name = "layoutFullWH";
            this.layoutFullWH.Size = new System.Drawing.Size(96, 27);
            this.layoutFullWH.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutFullWH.Text = "layoutFullWH";
            this.layoutFullWH.TextSize = new System.Drawing.Size(0, 0);
            this.layoutFullWH.TextToControlDistance = 0;
            this.layoutFullWH.TextVisible = false;
            // 
            // layoutWHTRNo
            // 
            this.layoutWHTRNo.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutWHTRNo.AppearanceItemCaption.Options.UseFont = true;
            this.layoutWHTRNo.Control = this.textWHTNo;
            this.layoutWHTRNo.CustomizationFormText = "Witholding Receipt No.";
            this.layoutWHTRNo.Location = new System.Drawing.Point(384, 27);
            this.layoutWHTRNo.Name = "layoutWHTRNo";
            this.layoutWHTRNo.Size = new System.Drawing.Size(341, 31);
            this.layoutWHTRNo.Text = "Witholding Receipt No.";
            this.layoutWHTRNo.TextSize = new System.Drawing.Size(126, 13);
            // 
            // layoutVWHRef
            // 
            this.layoutVWHRef.Control = this.textVATWithholdingReceiptNo;
            this.layoutVWHRef.CustomizationFormText = "VAT WH Receipt No.";
            this.layoutVWHRef.Location = new System.Drawing.Point(0, 28);
            this.layoutVWHRef.Name = "layoutVWHRef";
            this.layoutVWHRef.Size = new System.Drawing.Size(266, 30);
            this.layoutVWHRef.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutVWHRef.Text = "VAT WH Receipt No.";
            this.layoutVWHRef.TextSize = new System.Drawing.Size(126, 13);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.CornflowerBlue;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(12, 12);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(749, 28);
            this.labelControl3.StyleController = this.layoutControl3;
            this.labelControl3.TabIndex = 34;
            this.labelControl3.Text = "Payment and Tax";
            // 
            // paymentTypeSelector
            // 
            this.paymentTypeSelector.Include_BankTransferFromAccount = true;
            this.paymentTypeSelector.Include_BankTransferFromCash = true;
            this.paymentTypeSelector.Include_Cash = true;
            this.paymentTypeSelector.Include_Check = true;
            this.paymentTypeSelector.Include_CpoFromBank = true;
            this.paymentTypeSelector.Include_CpoFromCash = true;
            this.paymentTypeSelector.Include_Credit = true;
            this.paymentTypeSelector.Location = new System.Drawing.Point(157, 210);
            this.paymentTypeSelector.Name = "paymentTypeSelector";
            this.paymentTypeSelector.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.paymentTypeSelector.Properties.Items.AddRange(new object[] {
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Credit"});
            this.paymentTypeSelector.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.paymentTypeSelector.Size = new System.Drawing.Size(602, 20);
            this.paymentTypeSelector.StyleController = this.layoutControl3;
            this.paymentTypeSelector.TabIndex = 4;
            // 
            // memoNote
            // 
            this.memoNote.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.memoNote.Location = new System.Drawing.Point(12, 336);
            this.memoNote.Name = "memoNote";
            this.memoNote.Size = new System.Drawing.Size(749, 32);
            this.memoNote.StyleController = this.layoutControl3;
            this.memoNote.TabIndex = 17;
            this.memoNote.EditValueChanged += new System.EventHandler(this.memoNote_EditValueChanged);
            // 
            // checkPayFullAmount
            // 
            this.checkPayFullAmount.Location = new System.Drawing.Point(631, 182);
            this.checkPayFullAmount.Name = "checkPayFullAmount";
            this.checkPayFullAmount.Properties.Caption = "Pay Full Amount";
            this.checkPayFullAmount.Size = new System.Drawing.Size(128, 19);
            this.checkPayFullAmount.StyleController = this.layoutControl3;
            this.checkPayFullAmount.TabIndex = 31;
            // 
            // cashAccountPlaceholder
            // 
            this.cashAccountPlaceholder.IncludeExpenseAdvance = true;
            this.cashAccountPlaceholder.Location = new System.Drawing.Point(157, 238);
            this.cashAccountPlaceholder.Name = "cashAccountPlaceholder";
            this.cashAccountPlaceholder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cashAccountPlaceholder.Properties.HighlightedItemStyle = DevExpress.XtraEditors.HighlightStyle.Skinned;
            this.cashAccountPlaceholder.Properties.ImmediatePopup = true;
            this.cashAccountPlaceholder.Size = new System.Drawing.Size(225, 20);
            this.cashAccountPlaceholder.StyleController = this.layoutControl3;
            this.cashAccountPlaceholder.TabIndex = 6;
            // 
            // textPaidAmount
            // 
            this.textPaidAmount.Location = new System.Drawing.Point(471, 182);
            this.textPaidAmount.Name = "textPaidAmount";
            this.textPaidAmount.Properties.Mask.EditMask = "#,########0.00;";
            this.textPaidAmount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textPaidAmount.Size = new System.Drawing.Size(152, 20);
            this.textPaidAmount.StyleController = this.layoutControl3;
            this.textPaidAmount.TabIndex = 30;
            // 
            // bankAccountPlaceholder
            // 
            this.bankAccountPlaceholder.Location = new System.Drawing.Point(157, 266);
            this.bankAccountPlaceholder.Name = "bankAccountPlaceholder";
            this.bankAccountPlaceholder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.bankAccountPlaceholder.Properties.ImmediatePopup = true;
            this.bankAccountPlaceholder.Size = new System.Drawing.Size(225, 20);
            this.bankAccountPlaceholder.StyleController = this.layoutControl3;
            this.bankAccountPlaceholder.TabIndex = 7;
            // 
            // textTransferReference
            // 
            this.textTransferReference.Location = new System.Drawing.Point(157, 294);
            this.textTransferReference.Name = "textTransferReference";
            this.textTransferReference.Size = new System.Drawing.Size(138, 20);
            this.textTransferReference.StyleController = this.layoutControl3;
            this.textTransferReference.TabIndex = 10;
            // 
            // textCredit
            // 
            this.textCredit.Location = new System.Drawing.Point(594, 154);
            this.textCredit.Name = "textCredit";
            this.textCredit.Properties.Mask.EditMask = "#,########0.00;";
            this.textCredit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textCredit.Size = new System.Drawing.Size(86, 20);
            this.textCredit.StyleController = this.layoutControl3;
            this.textCredit.TabIndex = 15;
            // 
            // labelCashBalance
            // 
            this.labelCashBalance.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelCashBalance.Location = new System.Drawing.Point(390, 238);
            this.labelCashBalance.Name = "labelCashBalance";
            this.labelCashBalance.Size = new System.Drawing.Size(369, 20);
            this.labelCashBalance.StyleController = this.layoutControl3;
            this.labelCashBalance.TabIndex = 27;
            this.labelCashBalance.Text = "###";
            // 
            // labelBankBalance
            // 
            this.labelBankBalance.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelBankBalance.Location = new System.Drawing.Point(390, 266);
            this.labelBankBalance.Name = "labelBankBalance";
            this.labelBankBalance.Size = new System.Drawing.Size(369, 13);
            this.labelBankBalance.StyleController = this.layoutControl3;
            this.labelBankBalance.TabIndex = 28;
            this.labelBankBalance.Text = "###";
            // 
            // textServiceCharge
            // 
            this.textServiceCharge.Location = new System.Drawing.Point(446, 294);
            this.textServiceCharge.Name = "textServiceCharge";
            this.textServiceCharge.Properties.Mask.EditMask = "#,########0.00;";
            this.textServiceCharge.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textServiceCharge.Size = new System.Drawing.Size(124, 20);
            this.textServiceCharge.StyleController = this.layoutControl3;
            this.textServiceCharge.TabIndex = 11;
            // 
            // textSettleAdvancePayment
            // 
            this.textSettleAdvancePayment.Location = new System.Drawing.Point(157, 154);
            this.textSettleAdvancePayment.Name = "textSettleAdvancePayment";
            this.textSettleAdvancePayment.Properties.Mask.EditMask = "#,########0.00;";
            this.textSettleAdvancePayment.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textSettleAdvancePayment.Size = new System.Drawing.Size(138, 20);
            this.textSettleAdvancePayment.StyleController = this.layoutControl3;
            this.textSettleAdvancePayment.TabIndex = 13;
            // 
            // checkSettleAll
            // 
            this.checkSettleAll.Location = new System.Drawing.Point(303, 154);
            this.checkSettleAll.Name = "checkSettleAll";
            this.checkSettleAll.Properties.Caption = "Settle Maximum Amount";
            this.checkSettleAll.Size = new System.Drawing.Size(140, 19);
            this.checkSettleAll.StyleController = this.layoutControl3;
            this.checkSettleAll.TabIndex = 14;
            // 
            // checAllCredit
            // 
            this.checAllCredit.Location = new System.Drawing.Point(688, 154);
            this.checAllCredit.Name = "checAllCredit";
            this.checAllCredit.Properties.Caption = "Full Credit";
            this.checAllCredit.Size = new System.Drawing.Size(71, 19);
            this.checAllCredit.StyleController = this.layoutControl3;
            this.checAllCredit.TabIndex = 16;
            // 
            // checkRelationSettled
            // 
            this.checkRelationSettled.Location = new System.Drawing.Point(578, 294);
            this.checkRelationSettled.Name = "checkRelationSettled";
            this.checkRelationSettled.Properties.Caption = "Paid by Supplier";
            this.checkRelationSettled.Size = new System.Drawing.Size(181, 19);
            this.checkRelationSettled.StyleController = this.layoutControl3;
            this.checkRelationSettled.TabIndex = 12;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutSettleAdvance,
            this.layoutSettleMaxAdvance,
            this.layoutControlItem32,
            this.layoutTaxation,
            this.layoutCollectedCash,
            this.layoutControlItem10,
            this.layoutTransferRefernce,
            this.layoutServiceCharge,
            this.layoutServiceChargeBy,
            this.layoutCashAccount,
            this.layoutCashBalance,
            this.layoutBankAccount,
            this.layoutBankBalance,
            this.layoutCredit,
            this.layoutFullCredit,
            this.layoutControlItem15,
            this.layoutRetention,
            this.layoutCollectFullAmount});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(773, 380);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutSettleAdvance
            // 
            this.layoutSettleAdvance.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutSettleAdvance.AppearanceItemCaption.Options.UseFont = true;
            this.layoutSettleAdvance.Control = this.textSettleAdvancePayment;
            this.layoutSettleAdvance.CustomizationFormText = "Settle Advance Payment";
            this.layoutSettleAdvance.Location = new System.Drawing.Point(0, 140);
            this.layoutSettleAdvance.Name = "layoutSettleAdvance";
            this.layoutSettleAdvance.Size = new System.Drawing.Size(289, 28);
            this.layoutSettleAdvance.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutSettleAdvance.Text = "Settle Advance Payment";
            this.layoutSettleAdvance.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutSettleMaxAdvance
            // 
            this.layoutSettleMaxAdvance.Control = this.checkSettleAll;
            this.layoutSettleMaxAdvance.CustomizationFormText = "layoutControlItem8";
            this.layoutSettleMaxAdvance.Location = new System.Drawing.Point(289, 140);
            this.layoutSettleMaxAdvance.Name = "layoutSettleMaxAdvance";
            this.layoutSettleMaxAdvance.Size = new System.Drawing.Size(148, 28);
            this.layoutSettleMaxAdvance.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutSettleMaxAdvance.Text = "layoutSettleMaxAdvance";
            this.layoutSettleMaxAdvance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutSettleMaxAdvance.TextToControlDistance = 0;
            this.layoutSettleMaxAdvance.TextVisible = false;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.labelControl3;
            this.layoutControlItem32.CustomizationFormText = "layoutControlItem32";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem32.MaxSize = new System.Drawing.Size(0, 32);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(22, 32);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(753, 32);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Text = "layoutControlItem32";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem32.TextToControlDistance = 0;
            this.layoutControlItem32.TextVisible = false;
            // 
            // layoutTaxation
            // 
            this.layoutTaxation.Control = this.groupControl1;
            this.layoutTaxation.CustomizationFormText = "layoutControlItem15";
            this.layoutTaxation.Location = new System.Drawing.Point(0, 32);
            this.layoutTaxation.MaxSize = new System.Drawing.Size(0, 108);
            this.layoutTaxation.MinSize = new System.Drawing.Size(104, 108);
            this.layoutTaxation.Name = "layoutTaxation";
            this.layoutTaxation.Size = new System.Drawing.Size(753, 108);
            this.layoutTaxation.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutTaxation.Text = "layoutTaxation";
            this.layoutTaxation.TextSize = new System.Drawing.Size(0, 0);
            this.layoutTaxation.TextToControlDistance = 0;
            this.layoutTaxation.TextVisible = false;
            // 
            // layoutCollectedCash
            // 
            this.layoutCollectedCash.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutCollectedCash.AppearanceItemCaption.Options.UseFont = true;
            this.layoutCollectedCash.Control = this.textPaidAmount;
            this.layoutCollectedCash.CustomizationFormText = "Paid Amount";
            this.layoutCollectedCash.Location = new System.Drawing.Point(314, 168);
            this.layoutCollectedCash.Name = "layoutCollectedCash";
            this.layoutCollectedCash.Size = new System.Drawing.Size(303, 28);
            this.layoutCollectedCash.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutCollectedCash.Text = "Paid Amount";
            this.layoutCollectedCash.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.Control = this.paymentTypeSelector;
            this.layoutControlItem10.CustomizationFormText = "Payment Method";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 196);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(753, 28);
            this.layoutControlItem10.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem10.Text = "Payment Method";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutTransferRefernce
            // 
            this.layoutTransferRefernce.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutTransferRefernce.AppearanceItemCaption.Options.UseFont = true;
            this.layoutTransferRefernce.Control = this.textTransferReference;
            this.layoutTransferRefernce.CustomizationFormText = "Transfer Ref.";
            this.layoutTransferRefernce.Location = new System.Drawing.Point(0, 280);
            this.layoutTransferRefernce.Name = "layoutTransferRefernce";
            this.layoutTransferRefernce.Size = new System.Drawing.Size(289, 28);
            this.layoutTransferRefernce.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutTransferRefernce.Text = "Transfer Ref.";
            this.layoutTransferRefernce.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutServiceCharge
            // 
            this.layoutServiceCharge.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutServiceCharge.AppearanceItemCaption.Options.UseFont = true;
            this.layoutServiceCharge.Control = this.textServiceCharge;
            this.layoutServiceCharge.CustomizationFormText = "Bank Service Charge";
            this.layoutServiceCharge.Location = new System.Drawing.Point(289, 280);
            this.layoutServiceCharge.Name = "layoutServiceCharge";
            this.layoutServiceCharge.Size = new System.Drawing.Size(275, 28);
            this.layoutServiceCharge.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutServiceCharge.Text = "Bank Service Charge";
            this.layoutServiceCharge.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutServiceChargeBy
            // 
            this.layoutServiceChargeBy.Control = this.checkRelationSettled;
            this.layoutServiceChargeBy.CustomizationFormText = "layoutControlItem22";
            this.layoutServiceChargeBy.Location = new System.Drawing.Point(564, 280);
            this.layoutServiceChargeBy.Name = "layoutServiceChargeBy";
            this.layoutServiceChargeBy.Size = new System.Drawing.Size(189, 28);
            this.layoutServiceChargeBy.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutServiceChargeBy.Text = "layoutServiceChargeBy";
            this.layoutServiceChargeBy.TextSize = new System.Drawing.Size(0, 0);
            this.layoutServiceChargeBy.TextToControlDistance = 0;
            this.layoutServiceChargeBy.TextVisible = false;
            // 
            // layoutCashAccount
            // 
            this.layoutCashAccount.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutCashAccount.AppearanceItemCaption.Options.UseFont = true;
            this.layoutCashAccount.Control = this.cashAccountPlaceholder;
            this.layoutCashAccount.CustomizationFormText = "Cash Account";
            this.layoutCashAccount.Location = new System.Drawing.Point(0, 224);
            this.layoutCashAccount.MinSize = new System.Drawing.Size(202, 28);
            this.layoutCashAccount.Name = "layoutCashAccount";
            this.layoutCashAccount.Size = new System.Drawing.Size(376, 28);
            this.layoutCashAccount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutCashAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutCashAccount.Text = "Cash Account";
            this.layoutCashAccount.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutCashBalance
            // 
            this.layoutCashBalance.Control = this.labelCashBalance;
            this.layoutCashBalance.CustomizationFormText = "layoutControlItem17";
            this.layoutCashBalance.Location = new System.Drawing.Point(376, 224);
            this.layoutCashBalance.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutCashBalance.MinSize = new System.Drawing.Size(18, 28);
            this.layoutCashBalance.Name = "layoutCashBalance";
            this.layoutCashBalance.Size = new System.Drawing.Size(377, 28);
            this.layoutCashBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutCashBalance.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutCashBalance.Text = "layoutCashBalance";
            this.layoutCashBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutCashBalance.TextToControlDistance = 0;
            this.layoutCashBalance.TextVisible = false;
            // 
            // layoutBankAccount
            // 
            this.layoutBankAccount.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutBankAccount.AppearanceItemCaption.Options.UseFont = true;
            this.layoutBankAccount.Control = this.bankAccountPlaceholder;
            this.layoutBankAccount.CustomizationFormText = "Bank Account";
            this.layoutBankAccount.Location = new System.Drawing.Point(0, 252);
            this.layoutBankAccount.Name = "layoutBankAccount";
            this.layoutBankAccount.Size = new System.Drawing.Size(376, 28);
            this.layoutBankAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutBankAccount.Text = "Bank Account";
            this.layoutBankAccount.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutBankBalance
            // 
            this.layoutBankBalance.Control = this.labelBankBalance;
            this.layoutBankBalance.CustomizationFormText = "layoutControlItem19";
            this.layoutBankBalance.Location = new System.Drawing.Point(376, 252);
            this.layoutBankBalance.Name = "layoutBankBalance";
            this.layoutBankBalance.Size = new System.Drawing.Size(377, 28);
            this.layoutBankBalance.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutBankBalance.Text = "layoutBankBalance";
            this.layoutBankBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutBankBalance.TextToControlDistance = 0;
            this.layoutBankBalance.TextVisible = false;
            // 
            // layoutCredit
            // 
            this.layoutCredit.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutCredit.AppearanceItemCaption.Options.UseFont = true;
            this.layoutCredit.Control = this.textCredit;
            this.layoutCredit.CustomizationFormText = "Supplier Credit";
            this.layoutCredit.Location = new System.Drawing.Point(437, 140);
            this.layoutCredit.Name = "layoutCredit";
            this.layoutCredit.Size = new System.Drawing.Size(237, 28);
            this.layoutCredit.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutCredit.Text = "Supplier Credit";
            this.layoutCredit.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutFullCredit
            // 
            this.layoutFullCredit.Control = this.checAllCredit;
            this.layoutFullCredit.CustomizationFormText = "layoutControlItem12";
            this.layoutFullCredit.Location = new System.Drawing.Point(674, 140);
            this.layoutFullCredit.Name = "layoutFullCredit";
            this.layoutFullCredit.Size = new System.Drawing.Size(79, 28);
            this.layoutFullCredit.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutFullCredit.Text = "layoutFullCredit";
            this.layoutFullCredit.TextSize = new System.Drawing.Size(0, 0);
            this.layoutFullCredit.TextToControlDistance = 0;
            this.layoutFullCredit.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.memoNote;
            this.layoutControlItem15.CustomizationFormText = "Additional Notes";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 308);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(144, 36);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(753, 52);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "Additional Notes";
            this.layoutControlItem15.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutRetention
            // 
            this.layoutRetention.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutRetention.AppearanceItemCaption.Options.UseFont = true;
            this.layoutRetention.Control = this.textRetention;
            this.layoutRetention.CustomizationFormText = "Retention";
            this.layoutRetention.Location = new System.Drawing.Point(0, 168);
            this.layoutRetention.Name = "layoutRetention";
            this.layoutRetention.Size = new System.Drawing.Size(314, 28);
            this.layoutRetention.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutRetention.Text = "Retention";
            this.layoutRetention.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutCollectFullAmount
            // 
            this.layoutCollectFullAmount.Control = this.checkPayFullAmount;
            this.layoutCollectFullAmount.CustomizationFormText = "layoutControlItem6";
            this.layoutCollectFullAmount.Location = new System.Drawing.Point(617, 168);
            this.layoutCollectFullAmount.Name = "layoutCollectFullAmount";
            this.layoutCollectFullAmount.Size = new System.Drawing.Size(136, 28);
            this.layoutCollectFullAmount.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutCollectFullAmount.Text = "layoutCollectFullAmount";
            this.layoutCollectFullAmount.TextSize = new System.Drawing.Size(0, 0);
            this.layoutCollectFullAmount.TextToControlDistance = 0;
            this.layoutCollectFullAmount.TextVisible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.buttonCredit);
            this.flowLayoutPanel1.Controls.Add(this.buttonDeliveries);
            this.flowLayoutPanel1.Controls.Add(this.buttonPrepayments);
            this.flowLayoutPanel1.Controls.Add(this.buttonSetInvoice);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(514, 128);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(287, 57);
            this.flowLayoutPanel1.TabIndex = 20;
            // 
            // buttonCredit
            // 
            this.buttonCredit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCredit.Location = new System.Drawing.Point(161, 3);
            this.buttonCredit.Name = "buttonCredit";
            this.buttonCredit.Size = new System.Drawing.Size(123, 23);
            this.buttonCredit.TabIndex = 5;
            this.buttonCredit.Text = "&Credit Settlement";
            this.buttonCredit.Visible = false;
            // 
            // buttonDeliveries
            // 
            this.buttonDeliveries.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDeliveries.Location = new System.Drawing.Point(32, 3);
            this.buttonDeliveries.Name = "buttonDeliveries";
            this.buttonDeliveries.Size = new System.Drawing.Size(123, 23);
            this.buttonDeliveries.TabIndex = 5;
            this.buttonDeliveries.Text = "&Delivery";
            this.buttonDeliveries.Visible = false;
            // 
            // buttonPrepayments
            // 
            this.buttonPrepayments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonPrepayments.Location = new System.Drawing.Point(161, 32);
            this.buttonPrepayments.Name = "buttonPrepayments";
            this.buttonPrepayments.Size = new System.Drawing.Size(123, 23);
            this.buttonPrepayments.TabIndex = 5;
            this.buttonPrepayments.Text = "&Prepayment";
            this.buttonPrepayments.Visible = false;
            this.buttonPrepayments.Click += new System.EventHandler(this.buttonPrepayments_Click);
            // 
            // buttonSetInvoice
            // 
            this.buttonSetInvoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSetInvoice.Location = new System.Drawing.Point(32, 32);
            this.buttonSetInvoice.Name = "buttonSetInvoice";
            this.buttonSetInvoice.Size = new System.Drawing.Size(123, 23);
            this.buttonSetInvoice.TabIndex = 5;
            this.buttonSetInvoice.Text = "&Invoice";
            this.buttonSetInvoice.Visible = false;
            // 
            // itemSummaryGrid
            // 
            this.itemSummaryGrid.Location = new System.Drawing.Point(23, 34);
            this.itemSummaryGrid.MainView = this.gridViewSummary;
            this.itemSummaryGrid.Name = "itemSummaryGrid";
            this.itemSummaryGrid.Size = new System.Drawing.Size(402, 180);
            this.itemSummaryGrid.TabIndex = 5;
            this.itemSummaryGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSummary});
            // 
            // gridViewSummary
            // 
            this.gridViewSummary.GridControl = this.itemSummaryGrid;
            this.gridViewSummary.GroupPanelText = "Payment Summary";
            this.gridViewSummary.Name = "gridViewSummary";
            // 
            // labelPaymentInstruction
            // 
            this.labelPaymentInstruction.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPaymentInstruction.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelPaymentInstruction.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelPaymentInstruction.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelPaymentInstruction.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelPaymentInstruction.Location = new System.Drawing.Point(442, 12);
            this.labelPaymentInstruction.Name = "labelPaymentInstruction";
            this.labelPaymentInstruction.Size = new System.Drawing.Size(340, 62);
            this.labelPaymentInstruction.TabIndex = 19;
            this.labelPaymentInstruction.Text = "#####";
            // 
            // voucher
            // 
            this.voucher.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.voucher.Appearance.Options.UseBackColor = true;
            this.voucher.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.voucher.Location = new System.Drawing.Point(14, 62);
            this.voucher.Name = "voucher";
            this.voucher.Size = new System.Drawing.Size(367, 80);
            this.voucher.TabIndex = 32;
            // 
            // textReference
            // 
            this.textReference.Location = new System.Drawing.Point(509, 177);
            this.textReference.Name = "textReference";
            this.textReference.Size = new System.Drawing.Size(250, 20);
            this.textReference.StyleController = this.layoutControl2;
            this.textReference.TabIndex = 1;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.checkInvoiced);
            this.layoutControl2.Controls.Add(this.itemGrid);
            this.layoutControl2.Controls.Add(this.labelControl1);
            this.layoutControl2.Controls.Add(this.labelCustomerInformation);
            this.layoutControl2.Controls.Add(this.dateTransaction);
            this.layoutControl2.Controls.Add(this.linkEditSupplier);
            this.layoutControl2.Controls.Add(this.textReference);
            this.layoutControl2.Controls.Add(this.voucher);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(593, 204, 250, 350);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(773, 380);
            this.layoutControl2.TabIndex = 33;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // checkIncoiced
            // 
            this.checkInvoiced.EditValue = true;
            this.checkInvoiced.Location = new System.Drawing.Point(389, 150);
            this.checkInvoiced.Name = "checkIncoiced";
            this.checkInvoiced.Properties.Caption = "Invoiced";
            this.checkInvoiced.Size = new System.Drawing.Size(370, 19);
            this.checkInvoiced.StyleController = this.layoutControl2;
            this.checkInvoiced.TabIndex = 35;
            this.checkInvoiced.CheckedChanged += new System.EventHandler(this.checkIncoiced_CheckedChanged);
            // 
            // itemGrid
            // 
            this.itemGrid.AllowPriceEdit = false;
            this.itemGrid.AllowRepeatedItem = false;
            this.itemGrid.AllowUnitPriceEdit = true;
            this.itemGrid.AutoUnitPrice = false;
            this.itemGrid.CurrentBalanceCaption = "Current Balance";
            this.itemGrid.DirectExpenseCaption = "Direct Issue?";
            this.itemGrid.GroupByCostCenter = false;
            this.itemGrid.Location = new System.Drawing.Point(12, 235);
            this.itemGrid.MainView = this.gridViewItems;
            this.itemGrid.Name = "itemGrid";
            this.itemGrid.Padding = new System.Windows.Forms.Padding(3);
            this.itemGrid.PrepaidCaption = "Prepaid?";
            this.itemGrid.PriceCaption = "Price";
            this.itemGrid.QuantityCaption = "Quantity";
            this.itemGrid.ReadOnly = false;
            this.itemGrid.ShowCostCenterColumn = true;
            this.itemGrid.ShowCurrentBalanceColumn = false;
            this.itemGrid.ShowDirectExpenseColumn = true;
            this.itemGrid.ShowPrepaid = true;
            this.itemGrid.ShowPrice = false;
            this.itemGrid.ShowQuantity = true;
            this.itemGrid.ShowRate = false;
            this.itemGrid.ShowRemitedColumn = false;
            this.itemGrid.ShowUnit = true;
            this.itemGrid.ShowUnitPrice = true;
            this.itemGrid.Size = new System.Drawing.Size(749, 133);
            this.itemGrid.TabIndex = 3;
            this.itemGrid.UnitPriceCaption = "Unit Price";
            this.itemGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewItems});
            // 
            // gridViewItems
            // 
            this.gridViewItems.GridControl = this.itemGrid;
            this.gridViewItems.Name = "gridViewItems";
            this.gridViewItems.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewItems.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewItems.OptionsCustomization.AllowFilter = false;
            this.gridViewItems.OptionsCustomization.AllowGroup = false;
            this.gridViewItems.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewItems.OptionsCustomization.AllowSort = false;
            this.gridViewItems.OptionsView.ShowGroupPanel = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.CornflowerBlue;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(749, 28);
            this.labelControl1.StyleController = this.layoutControl2;
            this.labelControl1.TabIndex = 33;
            this.labelControl1.Text = "Transaction Detail";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // labelCustomerInformation
            // 
            this.labelCustomerInformation.AllowHtmlString = true;
            this.labelCustomerInformation.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelCustomerInformation.Location = new System.Drawing.Point(14, 166);
            this.labelCustomerInformation.Name = "labelCustomerInformation";
            this.labelCustomerInformation.Padding = new System.Windows.Forms.Padding(3);
            this.labelCustomerInformation.Size = new System.Drawing.Size(367, 19);
            this.labelCustomerInformation.StyleController = this.layoutControl2;
            this.labelCustomerInformation.TabIndex = 15;
            this.labelCustomerInformation.Text = "###";
            // 
            // dateTransaction
            // 
            this.dateTransaction.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateTransaction.Location = new System.Drawing.Point(389, 62);
            this.dateTransaction.Name = "dateTransaction";
            this.dateTransaction.ShowEthiopian = true;
            this.dateTransaction.ShowGregorian = true;
            this.dateTransaction.ShowTime = true;
            this.dateTransaction.Size = new System.Drawing.Size(370, 63);
            this.dateTransaction.TabIndex = 0;
            this.dateTransaction.VerticalLayout = true;
            // 
            // linkEditSupplier
            // 
            this.linkEditSupplier.EditValue = "Edit";
            this.linkEditSupplier.Location = new System.Drawing.Point(14, 193);
            this.linkEditSupplier.Name = "linkEditSupplier";
            this.linkEditSupplier.Size = new System.Drawing.Size(367, 20);
            this.linkEditSupplier.StyleController = this.layoutControl2;
            this.linkEditSupplier.TabIndex = 34;
            this.linkEditSupplier.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.linkEditSupplier_OpenLink);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem13,
            this.layoutControlItem5,
            this.layoutControlItem4,
            this.layoutRelationInformation,
            this.layoutControlItem3,
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(773, 380);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.Control = this.voucher;
            this.layoutControlItem9.CustomizationFormText = "Reference";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 32);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(0, 104);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(125, 104);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(375, 104);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem9.Text = "Reference";
            this.layoutControlItem9.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.Control = this.dateTransaction;
            this.layoutControlItem13.CustomizationFormText = "Date";
            this.layoutControlItem13.Location = new System.Drawing.Point(375, 32);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(0, 104);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(125, 104);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(378, 104);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem13.Text = "Date";
            this.layoutControlItem13.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.labelControl1;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 32);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(14, 32);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(753, 32);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.itemGrid;
            this.layoutControlItem4.CustomizationFormText = "Items";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 207);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(753, 153);
            this.layoutControlItem4.Text = "Items";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutRelationInformation
            // 
            this.layoutRelationInformation.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutRelationInformation.AppearanceItemCaption.Options.UseFont = true;
            this.layoutRelationInformation.Control = this.labelCustomerInformation;
            this.layoutRelationInformation.CustomizationFormText = "Supplier Information";
            this.layoutRelationInformation.Location = new System.Drawing.Point(0, 136);
            this.layoutRelationInformation.Name = "layoutRelationInformation";
            this.layoutRelationInformation.Size = new System.Drawing.Size(375, 43);
            this.layoutRelationInformation.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutRelationInformation.Text = "Supplier Information";
            this.layoutRelationInformation.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutRelationInformation.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.textReference;
            this.layoutControlItem3.CustomizationFormText = "Supplier Invoice No.";
            this.layoutControlItem3.Location = new System.Drawing.Point(375, 163);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(378, 44);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem3.Text = "Supplier Invoice No.";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.linkEditSupplier;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 179);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(375, 28);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.checkInvoiced;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(375, 136);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(378, 27);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.buttonCancel);
            this.panelControl1.Controls.Add(this.buttonPrev);
            this.panelControl1.Controls.Add(this.buttonNext);
            this.panelControl1.Controls.Add(this.itemSummaryGrid);
            this.panelControl1.Controls.Add(this.labelPaymentInstruction);
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 386);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(801, 219);
            this.panelControl1.TabIndex = 18;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(23, 12);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(125, 13);
            this.labelControl4.TabIndex = 21;
            this.labelControl4.Text = "Transaction Summary";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(737, 191);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(59, 23);
            this.buttonCancel.TabIndex = 6;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonPrev
            // 
            this.buttonPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrev.Location = new System.Drawing.Point(565, 191);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(80, 23);
            this.buttonPrev.TabIndex = 5;
            this.buttonPrev.Text = "<<Previous";
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNext.Location = new System.Drawing.Point(651, 191);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(80, 23);
            this.buttonNext.TabIndex = 5;
            this.buttonNext.Text = "Next>>";
            this.buttonNext.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // dxValidationProvider
            // 
            this.dxValidationProvider.ValidateHiddenControls = false;
            // 
            // tabControl
            // 
            this.tabControl.AppearancePage.Header.BackColor = System.Drawing.Color.Silver;
            this.tabControl.AppearancePage.Header.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabControl.AppearancePage.Header.Options.UseBackColor = true;
            this.tabControl.AppearancePage.Header.Options.UseBorderColor = true;
            this.tabControl.AppearancePage.HeaderActive.BackColor = System.Drawing.Color.DimGray;
            this.tabControl.AppearancePage.HeaderActive.BackColor2 = System.Drawing.Color.White;
            this.tabControl.AppearancePage.HeaderActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabControl.AppearancePage.HeaderActive.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl.AppearancePage.HeaderActive.ForeColor = System.Drawing.Color.Blue;
            this.tabControl.AppearancePage.HeaderActive.Options.UseBackColor = true;
            this.tabControl.AppearancePage.HeaderActive.Options.UseBorderColor = true;
            this.tabControl.AppearancePage.HeaderActive.Options.UseFont = true;
            this.tabControl.AppearancePage.HeaderActive.Options.UseForeColor = true;
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.HeaderButtons = DevExpress.XtraTab.TabButtons.Next;
            this.tabControl.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.tabControl.HeaderOrientation = DevExpress.XtraTab.TabOrientation.Vertical;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedTabPage = this.xtraTabPage1;
            this.tabControl.Size = new System.Drawing.Size(801, 386);
            this.tabControl.TabIndex = 1;
            this.tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage3});
            this.tabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.tabControl_SelectedPageChanged);
            this.tabControl.Click += new System.EventHandler(this.tabControl_Click);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.layoutControl2);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(773, 380);
            this.xtraTabPage1.Text = "Details";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.layoutControl3);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(773, 380);
            this.xtraTabPage3.Text = "Payment";
            // 
            // Purchase2Form
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(801, 605);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panelControl1);
            this.Name = "Purchase2Form";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Purchase of Goods/Services";
            ((System.ComponentModel.ISupportInitialize)(this.textRetention.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textVATWithholdingReceiptNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textVATBase.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWHTNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWHBase.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkFullVAT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkWitholdFull.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVATBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFullVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWHBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFullWH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWHTRNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVWHRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentTypeSelector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPayFullAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cashAccountPlaceholder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textPaidAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bankAccountPlaceholder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textTransferReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCredit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textServiceCharge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSettleAdvancePayment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSettleAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checAllCredit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkRelationSettled.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSettleAdvance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSettleMaxAdvance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTaxation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCollectedCash)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTransferRefernce)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServiceCharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServiceChargeBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCashAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCashBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBankBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFullCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRetention)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCollectFullAmount)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.itemSummaryGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkInvoiced.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkEditSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRelationInformation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private BNDualCalendar dateTransaction;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        protected ItemGrid itemGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewItems;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonNext;
        private DevExpress.XtraEditors.MemoEdit memoNote;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidationProvider;
        private DevExpress.XtraEditors.TextEdit textReference;
        private PaymentTypeSelector paymentTypeSelector;
        private DevExpress.XtraEditors.TextEdit textSettleAdvancePayment;
        private DevExpress.XtraEditors.CheckEdit checkSettleAll;
        private DevExpress.XtraEditors.LabelControl labelCustomerInformation;
        private ItemSummaryGrid itemSummaryGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSummary;
        private DevExpress.XtraEditors.CheckEdit checAllCredit;
        private DevExpress.XtraEditors.TextEdit textCredit;
        private DevExpress.XtraEditors.SimpleButton buttonCredit;
        private DevExpress.XtraEditors.LabelControl labelPaymentInstruction;
        private DevExpress.XtraEditors.TextEdit textTransferReference;
        private DevExpress.XtraEditors.TextEdit textServiceCharge;
        private BankAccountPlaceholder bankAccountPlaceholder;
        private CashAccountPlaceholder cashAccountPlaceholder;
        private DevExpress.XtraEditors.SimpleButton buttonPrepayments;
        private DevExpress.XtraEditors.CheckEdit checkRelationSettled;
        private DevExpress.XtraEditors.LabelControl labelBankBalance;
        private DevExpress.XtraEditors.LabelControl labelCashBalance;
        private DevExpress.XtraEditors.SimpleButton buttonDeliveries;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.TextEdit textWHTNo;
        private DevExpress.XtraEditors.TextEdit textPaidAmount;
        private DevExpress.XtraEditors.CheckEdit checkPayFullAmount;
        private DevExpress.XtraEditors.TextEdit textRetention;
        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutRelationInformation;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutCashAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutCashBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutBankAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutBankBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutTransferRefernce;
        private DevExpress.XtraLayout.LayoutControlItem layoutServiceCharge;
        private DevExpress.XtraLayout.LayoutControlItem layoutServiceChargeBy;
        private DevExpress.XtraLayout.LayoutControlItem layoutSettleAdvance;
        private DevExpress.XtraLayout.LayoutControlItem layoutSettleMaxAdvance;
        private DevExpress.XtraLayout.LayoutControlItem layoutCredit;
        private DevExpress.XtraLayout.LayoutControlItem layoutFullCredit;
        private DevExpress.XtraLayout.LayoutControlItem layoutRetention;
        private DevExpress.XtraLayout.LayoutControlItem layoutCollectedCash;
        private DevExpress.XtraLayout.LayoutControlItem layoutCollectFullAmount;
        private DevExpress.XtraEditors.TextEdit textVATBase;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraEditors.SimpleButton buttonPrev;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutVATBase;
        private DevExpress.XtraLayout.LayoutControlItem layoutFullVAT;
        private DevExpress.XtraLayout.LayoutControlItem layoutWHTRNo;
        private DevExpress.XtraLayout.LayoutControlItem layoutFullWH;
        private DevExpress.XtraLayout.LayoutControlItem layoutWHBase;
        private DevExpress.XtraLayout.LayoutControlItem layoutTaxation;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.HyperLinkEdit linkEditSupplier;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.CheckEdit checkInvoiced;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        protected DocumentSerialPlaceHolder voucher;
        private DevExpress.XtraEditors.TextEdit textVATWithholdingReceiptNo;
        private DevExpress.XtraLayout.LayoutControlItem layoutVWHRef;
        public DevExpress.XtraEditors.TextEdit textWHBase;
        protected DevExpress.XtraEditors.CheckEdit checkFullVAT;
        protected DevExpress.XtraEditors.CheckEdit checkWitholdFull;
        private DevExpress.XtraEditors.SimpleButton buttonSetInvoice;
    }
}