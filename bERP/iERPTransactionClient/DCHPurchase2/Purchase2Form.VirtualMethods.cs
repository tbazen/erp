﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using DevExpress.XtraLayout.Utils;

namespace BIZNET.iERP.Client
{
    public partial class Purchase2Form<PurchaseType> where PurchaseType : Purchase2Document, new()
    {
        public virtual string getFormTitle()
        {
            return "Purchase";
        }
        public virtual string getTradeRelationType()
        {
            return "Supplier";
        }
        public virtual List<TradeTransactionPaymentField> getPaymentFields()
        {
            return new List<TradeTransactionPaymentField>(((TradeTransactionPaymentField[])Enum.GetValues(typeof(TradeTransactionPaymentField))));
        }
        public virtual string getAdditionalRelationInfo(TradeRelation rel, DateTime date)
        {
            if (rel.PayableAccountID != -1)
            {
                double bal = AccountingClient.GetNetCostCenterAccountBalanceAsOf(iERPTransactionClient.mainCostCenterID, rel.ReceivableAccountID, dateTransaction.DateTime);
                if (AccountBase.AmountEqual(bal, 0) && !iERPTransactionClient.AllowNegativeTransactionPost())
                {
                    _advanceSettlement.Enabled = false;
                    return "";
                }
                else
                {
                    _advanceSettlement.Enabled = true;
                    return "\nAdvance paid:" + TSConstants.FormatBirr(bal);
                }

            }
            return "";
        }
    }
}