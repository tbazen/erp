﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using DevExpress.XtraLayout.Utils;

namespace BIZNET.iERP.Client
{
    
    public partial class Purchase2Form<PurchaseType> : DevExpress.XtraEditors.XtraForm,IItemGridController
        where PurchaseType:Purchase2Document, new()
    {
        PurchaseType _doc=null;
        
        DocumentBasicFieldsController _docFields;
        protected IAccountingClient _client;
        ActivationParameter _activation;
        PaymentMethodAndBalanceController _paymentController;
        SettleFullAmountController _creditPayment;
        protected SettleFullAmountController _advanceSettlement;
        SettleFullAmountController _paidAmount;
        SettleFullAmountController _vatBase;
        SettleFullAmountController _whBase;

        TradeRelation _supplier=null;
        
        ScheduleController _deliverySchedule;
        ScheduleController _creditSettlementSchedule;
        ScheduleController _prepamentSettlementSchedule;
        ScheduleController _invoiceSchedule;
        
        bool _ignoreEvents = false;

        public Purchase2Form(IAccountingClient client, ActivationParameter activation)
        {
            _ignoreEvents = true;
            try
            {
                InitializeComponent();
                configureForm();
                updatePrevNextButton();
                _client = client;
                _activation = activation;

                voucher.setKeys(typeof(PurchaseType), "paymentVoucher");

                _paymentController = new PaymentMethodAndBalanceController(
                    paymentTypeSelector
                    , layoutBankAccount
                    , bankAccountPlaceholder
                    , layoutBankBalance
                    , labelBankBalance
                    , layoutCashAccount
                    , cashAccountPlaceholder
                    , layoutCashBalance
                    , labelCashBalance
                    , layoutServiceCharge
                    , layoutTransferRefernce
                    , dateTransaction
                    , layoutServiceChargeBy
                    , _activation);
                //dissable reference and date if requested by activation parameter
                dateTransaction.Enabled = !activation.disableDate;
                voucher.Enabled = !activation.disableReference;
                _paymentController.PaymentInformationChanged += new EventHandler(_paymentController_PaymentInformationChanged);
                _docFields = new PurchaseBasicFieldController(dateTransaction, textReference,checkInvoiced, memoNote);

                _advanceSettlement = new SettleFullAmountController(textSettleAdvancePayment, checkSettleAll);
                _advanceSettlement.ValueChanged += new EventHandler(_advanceSettlement_ValueChanged);
                
                _creditPayment = new SettleFullAmountController(textCredit, checAllCredit);
                _creditPayment.FullAmount = 0;
                _creditPayment.ValueChanged += new EventHandler(_creditPayment_ValueChanged);

                _paidAmount = new SettleFullAmountController(textPaidAmount, checkPayFullAmount);
                checkPayFullAmount.Checked = true;
                _paidAmount.ValueChanged += new EventHandler(_paidAmount_ValueChanged);

                _vatBase = new SettleFullAmountController(textVATBase, checkFullVAT);
                checkFullVAT.Checked = true;
                _vatBase.ValueChanged += new EventHandler(_vatBase_ValueChanged);

                _whBase = new SettleFullAmountController(textWHBase, checkWitholdFull);
                checkWitholdFull.Checked = true;
                _whBase.ValueChanged += new EventHandler(_whBase_ValueChanged);

                itemGrid.initializeGrid();
                itemGrid.Controller = this;
                itemSummaryGrid.initializeGrid();
                setSupplier(_activation.objectCode);
                updateSummaryTable();


                _deliverySchedule = new ScheduleController(
                    buttonDeliveries, null
                    , typeof(PurchasedItemDeliveryDocument));

                _deliverySchedule.BeforeActivation += delegate(ScheduleController source)
                {
                    ActivationParameter act = new ActivationParameter();
                    act.purchaseDocument = buildPurchaseDocument();
                    return act;
                };

                _prepamentSettlementSchedule = new ScheduleController(
                    buttonPrepayments, null
                    , typeof(PurchasePrePaymentDocument));
                _prepamentSettlementSchedule.BeforeActivation += delegate(ScheduleController source)
                {
                    ActivationParameter act = new ActivationParameter();
                    act.purchaseDocument = buildPurchaseDocument();
                    return act;
                };

                _creditSettlementSchedule = new ScheduleController(
                    buttonCredit, null
                    , typeof(PurchaseInvoiceDocument));
                _creditSettlementSchedule.BeforeActivation += delegate(ScheduleController source)
                {
                    ActivationParameter act = new ActivationParameter();
                    act.purchaseDocument = buildPurchaseDocument();
                    act.objectCode = _supplier == null ? null : _supplier.Code;
                    return act;
                };

                _invoiceSchedule
                    = new ScheduleController(
                buttonSetInvoice, null
                , typeof(PurchaseInvoiceDocument));
                _invoiceSchedule.BeforeActivation += delegate(ScheduleController source)
                {
                    ActivationParameter act = new ActivationParameter();
                    act.purchaseDocument = buildPurchaseDocument();
                    act.objectCode = _supplier == null ? null : _supplier.Code;
                    return act;
                };

            }
            finally
            {
                _ignoreEvents = false;
            }
        }

        private void configureForm()
        {
            this.Text = getFormTitle();
            layoutRelationInformation.Text = getTradeRelationType() + " Information";
            linkEditSupplier.Text = "Edit " + getTradeRelationType();
            List<TradeTransactionPaymentField> fields = getPaymentFields();
            layoutSettleAdvance.Visibility = layoutSettleMaxAdvance.Visibility
                = fields.Contains(TradeTransactionPaymentField.SettleAdvancePayment) ? LayoutVisibility.Always : LayoutVisibility.Never;
            layoutCredit.Visibility = layoutFullCredit.Visibility
                = fields.Contains(TradeTransactionPaymentField.Credit) ? LayoutVisibility.Always : LayoutVisibility.Never;
            layoutRetention.Visibility
                = fields.Contains(TradeTransactionPaymentField.Retention) ? LayoutVisibility.Always : LayoutVisibility.Never;
            layoutCollectedCash.Visibility = layoutCollectFullAmount.Visibility
                = fields.Contains(TradeTransactionPaymentField.CollectedCash) ? LayoutVisibility.Always : LayoutVisibility.Never;

        }

        void _whBase_ValueChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents) return;
            updateSummaryTable(sender);
        }

        void _vatBase_ValueChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents) return;
            updateSummaryTable();
        }

        void _paidAmount_ValueChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents) return;
            updateSummaryTable();
        }
        public bool interactiveValidateAmount()
        {
            return true;
        }
        private PurchaseType buildPurchaseDocument()
        {
            PurchaseType ret = new PurchaseType();
            ret.replaceAttachments = true;
            ret.paymentVoucher = voucher.getReference();
            ret.AccountDocumentID = _doc == null ? -1 : _doc.AccountDocumentID;
            _docFields.setDocumentData(ret);
            ret.paymentMethod = _paymentController.PaymentTypeSelector.PaymentMethod;
            if (!double.TryParse(textRetention.Text, out ret.retention))
                ret.retention = 0;
           
            ret.assetAccountID = _paymentController.assetAccountID;
            double serviceCharge;
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(paymentTypeSelector.PaymentMethod) && textServiceCharge.Text != "")
            {
                if (!double.TryParse(textServiceCharge.Text, out serviceCharge))
                    serviceCharge = 0;
                ret.serviceChargeAmount = serviceCharge;
                ret.serviceChargePayer = checkRelationSettled.Checked ? ServiceChargePayer.Employee : ServiceChargePayer.Company;
            }
            ret.settleAdvancePayment = _advanceSettlement.effectiveAmount;
            ret.supplierCredit = _creditPayment.effectiveAmount;
            ret.withholdingReceiptNo = textWHTNo.Text;
            ret.vatWithholdingReceiptNo = textVATWithholdingReceiptNo.Text;
            ret.relationCode = _supplier.Code;
            TransactionItems[] items;
            TransactionDocumentItem[] ditems;
            extractGridData(out items,out ditems);
            ret.items = ditems;
            double taxable;
            bool vatApplies, whApplies;
            double[] pwt;
            ret.taxImposed=Purchase2Document.getTaxes(_supplier, iERPTransactionClient.GetCompanyProfile(), ditems, items, iERPTransactionClient.getTaxRates()
                ,!checkFullVAT.Checked,_vatBase.effectiveAmount
                ,!checkWitholdFull.Checked,_whBase.effectiveAmount
                , out ret.amount, out taxable
                , out vatApplies, out whApplies,out pwt);
            ret.deliveries = buttonDeliveries.Visible? _deliverySchedule.getScheduledDocumentsByType<PurchasedItemDeliveryDocument>().ToArray():new PurchasedItemDeliveryDocument[0];
            ret.creditSettlements = buttonCredit.Visible ? _creditSettlementSchedule.getScheduledDocumentsByType<SupplierCreditSettlement2Document>().ToArray() : new SupplierCreditSettlement2Document[0];
            ret.prepayments = buttonPrepayments.Visible?_prepamentSettlementSchedule.getScheduledDocumentsByType<PurchasePrePaymentDocument>().ToArray():new PurchasePrePaymentDocument[0];
            ret.invoices= buttonSetInvoice.Visible ? _invoiceSchedule.getScheduledDocumentsByType<PurchaseInvoiceDocument>().ToArray() : new PurchaseInvoiceDocument[0];
            ret.paidAmount = _paidAmount.effectiveAmount;
            ret.manualVATBase = checkFullVAT.Checked ? -1d : _vatBase.effectiveAmount;
            ret.manualWithholdBase= checkWitholdFull.Checked ? -1d : _whBase.effectiveAmount;
            
            return ret;
        }


        void _creditPayment_ValueChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents) return;
            updateSummaryTable();
            showHideCreditButton();
        }

        void _advanceSettlement_ValueChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents) return;
            updateSummaryTable();
        }

        void _paymentController_PaymentInformationChanged(object sender, EventArgs e)
        {
            updateSupplierInfo();
        }

        protected virtual void setSupplier(string code)
        {
            if (string.IsNullOrEmpty(code))
                _supplier = null;
            else
            {
                _supplier = iERPTransactionClient.GetRelation(code);
                if (_supplier == null)
                {
                    MessageBox.ShowErrorMessage("Supplier with code " + code + " is not found in the database");
                    return;
                }
                CostCenterAccount csAccount = AccountingClient.GetCostCenterAccount(iERPTransactionClient.mainCostCenterID, _supplier.ReceivableAccountID);
                if (csAccount != null)
                    _paymentController.AddCostCenterAccountItem(csAccount.id, null);
            }
            updateSupplierInfo();
        }

        private void updateSupplierInfo()
        {
            if (_supplier == null)
            {
                _advanceSettlement.Enabled = false;
                labelCustomerInformation.Text = "";
                linkEditSupplier.Enabled = false;
            }
            else
            {
                string info = _supplier.Name;
                linkEditSupplier.Enabled = true;
               labelCustomerInformation.Text = info+getAdditionalRelationInfo(_supplier,dateTransaction.DateTime);
            }
        }
        internal virtual void LoadData(PurchaseType purchase2Document)
        {
            
                _doc = purchase2Document;
                if (_doc == null)
                    return;
                _docFields.setControlData(purchase2Document);
                voucher.setReference(_doc.AccountDocumentID, _doc.paymentVoucher);
                setSupplier(purchase2Document.relationCode);
                textWHTNo.Text = purchase2Document.withholdingReceiptNo;
                textVATWithholdingReceiptNo.Text = purchase2Document.vatWithholdingReceiptNo;
                try
                {
                    _ignoreEvents = true;
                    _paymentController.IgnoreEvents();
                    _paymentController.PaymentTypeSelector.PaymentMethod = _doc.paymentMethod;
                    _paymentController.assetAccountID = _doc.assetAccountID;
                    if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(_doc.paymentMethod))
                        textServiceCharge.Text = _doc.serviceChargeAmount.ToString("N");
                    SetReferenceNumber(false);
                    foreach (TransactionDocumentItem item in _doc.items)
                    {
                        itemGrid.Add(itemGrid.toItemGridTableRow(item));
                    }
                    if (!AccountBase.AmountEqual(_doc.settleAdvancePayment, 0))
                        _advanceSettlement.effectiveAmount = _doc.settleAdvancePayment;
                    if (!AccountBase.AmountEqual(_doc.supplierCredit, 0))
                        _creditPayment.effectiveAmount = _doc.supplierCredit;

                    _creditSettlementSchedule.setScheduledDocuments(_doc.creditSettlementIDs);
                    _deliverySchedule.setScheduledDocuments(_doc.deliverieIDs);
                    _prepamentSettlementSchedule.setScheduledDocuments(_doc.prepaymentsIDs);
                    _invoiceSchedule.setScheduledDocuments(_doc.invoiceIDs);
                    textRetention.Text = AccountBase.AmountEqual(_doc.retention, 0) ? "" : AccountBase.FormatAmount(_doc.retention);
                    checkPayFullAmount.Checked = false;
                    _paidAmount.effectiveAmount = _doc.paidAmount;
                    checkFullVAT.Checked = AccountBase.AmountLess(_doc.manualVATBase, 0);
                    if (!AccountBase.AmountLess(_doc.manualVATBase, 0))
                        _vatBase.effectiveAmount = _doc.manualVATBase;
                    checkWitholdFull.Checked = AccountBase.AmountLess(_doc.manualWithholdBase, 0);
                    if (!AccountBase.AmountLess(_doc.manualWithholdBase, 0))
                        _whBase.effectiveAmount = _doc.manualWithholdBase;
                    double cash = updateSummaryTable();
                    checkPayFullAmount.Checked = AccountBase.AmountEqual(cash, _doc.paidAmount);
                    showHideDeliveryButton();
                    showHidPrePaymentButton();
                    showHideCreditButton();
                }
                finally
                {
                    _paymentController.AcceptEvents();
                    _ignoreEvents = false;
                }
           
        }

        private void SetReferenceNumber(bool updateDoc)
        {
            switch (paymentTypeSelector.PaymentMethod)
            {
                case BizNetPaymentMethod.Check:
                    if (_doc == null)
                        _doc.checkNumber = textTransferReference.Text;
                    else
                        if (!updateDoc)
                            textTransferReference.Text = _doc.checkNumber;
                        else
                            _doc.checkNumber = textTransferReference.Text;
                    break;
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.CPOFromBankAccount:
                    if (_doc == null)
                        _doc.cpoNumber = textTransferReference.Text;
                    else
                        if (!updateDoc)
                            textTransferReference.Text = _doc.cpoNumber;
                        else
                            _doc.cpoNumber = textTransferReference.Text;
                    break;
                case BizNetPaymentMethod.BankTransferFromCash:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    if (_doc == null)
                        _doc.transferReference = textTransferReference.Text;
                    else
                        if (!updateDoc)
                            textTransferReference.Text = _doc.transferReference;
                        else
                            _doc.transferReference = textTransferReference.Text;
                    break;
                default:
                    break;
            }
        }

        private void showHideDeliveryButton()
        {
            bool show = false;
            foreach (ItemGridControl.ItemGridData.ItemTableRow row in itemGrid)
            {
                TransactionItems item = itemGrid.GetItem(row);
                if (item == null)
                    continue;
                if ((item.IsInventoryItem || item.IsFixedAssetItem)&& !row.directExpense)
                {
                    show = true;
                    break;
                }
            }
            if (show ^ buttonDeliveries.Visible)
            {
                if (!show)
                    _deliverySchedule.setScheduledDocuments(new AccountDocument[0]);
                buttonDeliveries.Visible = show;
            }
        }

        private void showHidPrePaymentButton()
        {
            bool show = false;
            foreach (ItemGridControl.ItemGridData.ItemTableRow row in itemGrid)
            {
                TransactionItems item = itemGrid.GetItem(row);
                if (item == null)
                    continue;
                if (row.prepaid)
                {
                    show = true;
                    break;
                }
            }
            if (show ^ buttonPrepayments.Visible)
                buttonPrepayments.Visible = show;

        }
        private void showHideCreditButton()
        {
            bool show = _creditPayment.hasNoneZeroValue;
            if (show ^ buttonCredit.Visible)
            {
                if (!show)
                    _creditSettlementSchedule.setScheduledDocuments(new AccountDocument[0]);
                buttonCredit.Visible = show;
            }
        }

        void extractGridData(out TransactionItems[] items, out TransactionDocumentItem[] docItems)
        {
            items = new TransactionItems[itemGrid.Count];
            docItems = new TransactionDocumentItem[itemGrid.Count];
            int i = 0;
            foreach (ItemGridControl.ItemGridData.ItemTableRow row in itemGrid)
            {
                items[i] = itemGrid.GetItem(row);
                docItems[i] = ItemGrid.toDocumentItem(row);
                docItems[i].price = docItems[i].quantity * docItems[i].unitPrice;
                i++;
            }
        }
        protected double updateSummaryTable(object sender=null)
        {
            itemSummaryGrid.data.Clear();
            double totalPurchase = 0;
            double totalTaxable = 0;
            TransactionItems[] items;
            TransactionDocumentItem[] docItems;
            extractGridData(out items, out docItems);
            bool vatApplies, whApplies;
            double[] pwt;
            TaxImposed [] taxes= Purchase2Document.getTaxes(_supplier,
                iERPTransactionClient.GetCompanyProfile()
                ,docItems,items
                ,iERPTransactionClient.getTaxRates()
                , !checkFullVAT.Checked, _vatBase.effectiveAmount
                , !checkWitholdFull.Checked, _whBase.effectiveAmount
                , out totalPurchase
                ,out totalTaxable
                , out vatApplies, out whApplies,out pwt
                );
            
            _whBase.FullAmount = totalTaxable;
            _vatBase.FullAmount = totalTaxable;

            double netPayment;
            bool hasWH = false;
            bool hasVWH = false;
            double withHolding = 0;
            if (!AccountBase.AmountEqual(totalTaxable, 0) || (taxes.Length == 1 && taxes[0].TaxType == TaxType.WithHoldingTax))
            {
                int order = 20;
                double addedTax = 0;
                foreach (TaxImposed tax in taxes)
                {
                    itemSummaryGrid.setSummary("tax_" + tax.TaxType, TaxImposed.TaxTypeName(tax.TaxType), tax.TaxValue, order);
                    order += 10;
                    addedTax += tax.TaxValue;
                    if (tax.TaxType == TaxType.WithHoldingTax)
                    {
                        withHolding = tax.TaxValue;
                        hasWH = true;
                        _whBase.FullAmount = tax.TaxBaseValue;
                    }
                    if (tax.TaxType == TaxType.VAT)
                    {
                        _vatBase.FullAmount = tax.TaxBaseValue;                        
                    }
                    if (tax.TaxType == TaxType.VATWitholding)
                    {
                        hasVWH = true;
                    }
                }
                itemSummaryGrid.setSummary("totalPrice", "Total Before Tax", totalPurchase, 10);
                netPayment = totalPurchase + addedTax;
                order += 10;
            }
            else
                netPayment = totalPurchase;

            layoutWHBase.Visibility = layoutFullWH.Visibility = whApplies || sender != null ? LayoutVisibility.Always : LayoutVisibility.Never;
            layoutWHTRNo.Visibility = hasWH && double.Parse(textWHBase.Text)!=0 ? LayoutVisibility.Always : LayoutVisibility.Never;
            layoutFullVAT.Visibility = layoutVATBase.Visibility = vatApplies ? LayoutVisibility.Always : LayoutVisibility.Never;
            layoutVWHRef.Visibility = hasVWH? LayoutVisibility.Always : LayoutVisibility.Never;
            layoutTaxation.Visibility = (whApplies || sender!=null) || vatApplies? LayoutVisibility.Always : LayoutVisibility.Never;
            bool credits = false;
            bool settlment = false;
            bool retention = false;
            if (_advanceSettlement.hasNoneZeroValue)
            {
                credits=true;
                itemSummaryGrid.setSummary("advance", "Less Advance Payment", -_advanceSettlement.effectiveAmount, 80);
            }

            if (_creditPayment.hasNoneZeroValue)
            {
                settlment = true;
                itemSummaryGrid.setSummary("credit", "Less Supplier Credit", -_creditPayment.effectiveAmount, 90);
            }
            double retained;

            if (double.TryParse(textRetention.Text, out retained) && AccountBase.AmountGreater(retained, 0))
            {
                retention = true;
                itemSummaryGrid.setSummary("retention", "Retention", -retained, 90);
            }
            else
                retained = 0;

            double cash=netPayment - _advanceSettlement.effectiveAmount - _creditPayment.effectiveAmount-retained;
            
            if (credits || settlment || retention)
            {
                itemSummaryGrid.setSummary("net", "Net Amount", netPayment, 70);
                itemSummaryGrid.setSummary("cash", "Cash", cash, 100);
            }
            else
            {
                itemSummaryGrid.setSummary("cash", "Cash", netPayment, 70);
            }
            _paidAmount.FullAmount = cash;
            if (hasWH)
                labelPaymentInstruction.Text = string.Format("Pay {0}{1} \nPrepare Witholding Receipt of amount {2}({3}) for {4}"
                    , AccountBase.AmountEqual(_paidAmount.effectiveAmount, 0) ? "nothing" : TSConstants.FormatBirr(_paidAmount.effectiveAmount)
                    , AccountBase.AmountEqual(_paidAmount.effectiveAmount, 0) ? "" : "(" + CurrencyTranslator.TranslateCurrency((decimal)_paidAmount.effectiveAmount) + ")"
                    , TSConstants.FormatBirr(-withHolding)
                    , CurrencyTranslator.TranslateCurrency(-(decimal)withHolding)
                    , _supplier.Name + (string.IsNullOrEmpty(_supplier.TIN) ? "" : " -TIN:" + _supplier.TIN)
                    );
            else
                labelPaymentInstruction.Text = string.Format("Pay {0}({1})"
                    , TSConstants.FormatBirr(_paidAmount.effectiveAmount)
                    , CurrencyTranslator.TranslateCurrency((decimal)_paidAmount.effectiveAmount)
                    );
            if (_supplier != null)
            {
                CostCenterAccount csAccount = AccountingClient.GetCostCenterAccount(iERPTransactionClient.mainCostCenterID, _supplier.ReceivableAccountID);
                if (csAccount != null)
                {
                    _advanceSettlement.FullAmount = Math.Min(netPayment, _paymentController.getCsAccountBalance(csAccount.id));
                }
                _creditPayment.FullAmount = netPayment - _advanceSettlement.effectiveAmount;
            }
            return cash;
        }

        public double getUnitPrice(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            throw new NotImplementedException();
        }

        public double getBalance(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            return 0;
        }

        public bool hasUnitPrice(TransactionItems item)
        {
            throw new NotImplementedException();
        }

        public bool queryCellChange(ItemGridControl.ItemGridData.ItemTableRow row, DataColumn column, object oldValue)
        {
            if (_ignoreEvents) return true;
            TransactionItems item = itemGrid.GetItem(row);
            if (item == null)
                return true;
            if (column == itemGrid.data.priceColumn
                || column == itemGrid.data.unitPriceColumn
                || column == itemGrid.data.quantityColumn
                )
            {
                updateSummaryTable();
                showHideDeliveryButton();
                return true;
            }
            else if (column == itemGrid.data.prepaidColumn)
            {
                
                if (!item.IsExpenseItem || item.IsInventoryItem)
                    return false;
                showHidPrePaymentButton();
                return true;
            }
            else if (column == itemGrid.data.codeColumn)
            {
                if (!item.IsExpenseItem && !item.IsFixedAssetItem)
                    return false;
                showHideDeliveryButton();
                return true;
            }
            else if (column == itemGrid.data.directExpenseColumn)
            {
                if (!item.IsInventoryItem && !item.IsFixedAssetItem)
                    return false;
                showHideDeliveryButton();
                return true;
            }
            return true;
        }


        public bool queryDeleteRow(ItemGridControl.ItemGridData.ItemTableRow row)
        {
            if (_ignoreEvents) return true;
            updateSummaryTable();
            return true;
        }
        public void afterComit(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {

        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (tabControl.SelectedTabPageIndex == tabControl.TabPages.Count - 1)
                save();
            else
            {
                if (!dxValidationProvider.Validate())
                    return;
                tabControl.SelectedTabPageIndex++;
            }
        }

        private void save()
        {
            try
            {
                if (!dxValidationProvider.Validate())
                    return;
                if (!(_client is DocumentScheduler))
                {
                    if (voucher.Enabled && voucher.getReference() == null)
                    {
                        MessageBox.ShowErrorMessage("Please enter reference");
                        return;
                    }
                    if (checkInvoiced.Checked)
                    {
                        if (textReference.Text.Trim().Equals(""))
                        {
                            MessageBox.ShowErrorMessage("Please enter supplier reference");
                            return;
                        }
                        if (layoutWHBase.Visibility == LayoutVisibility.Always && layoutWHTRNo.Visibility == LayoutVisibility.Always)
                        {
                            if (textWHTNo.Text.Trim().Equals(""))
                            {
                                MessageBox.ShowErrorMessage("Please enter tax withholding receipt no");
                                return;
                            }
                        }
                        if (layoutVWHRef.Visibility == LayoutVisibility.Always)
                        {
                            if (textVATWithholdingReceiptNo.Text.Trim().Equals(""))
                            {
                                MessageBox.ShowErrorMessage("Please enter vat withholding receipt no");
                                return;
                            }
                        }
                    }
                }
                
                double retained;
                if (!textRetention.Text.Equals(""))
                    if (!double.TryParse(textRetention.Text, out retained) || AccountBase.AmountLess(retained, 0))
                    {
                        MessageBox.ShowErrorMessage("Enter valid retention amount or leave retention field blank");
                        return;
                    }
                _doc = buildPurchaseDocument();


                bool updateDoc = _doc == null ? false : true;
                SetReferenceNumber(updateDoc);
                if (!_activation.saveWithNoPayment && !_paymentController.validatePaymentDocuemntReference(_doc.checkNumber, _doc.cpoNumber, _doc.transferReference))
                    return;
                if (IsTaxAuthorityUnclaimableItemMixed(_doc.items))
                {
                    MessageBox.ShowErrorMessage("You cannot mix tax authority unclaimable items with other items!");
                    return;
                }
                bool post = true;
                /*if (iERPTransactionClient.AllowNegativeTransactionPost())
                {
                    post = TransactionValidator.ValidateNegativeTransactionPost(_doc.assetAccountID, _doc.amount, paymentTypeSelector
                        , textServiceCharge, _doc.serviceChargePayer, _doc.DocumentDate);
                }*/
                if (post)
                {
                    _doc.AccountDocumentID = _client.PostGenericDocument(_doc);
                    if (!(_client is DocumentScheduler))
                    {
                        MessageBox.ShowSuccessMessage("Document succesfully posted");
                    }
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private bool IsTaxAuthorityUnclaimableItemMixed(TransactionDocumentItem[] transactionDocumentItem)
        {
            int unclaimable, claimable;
            unclaimable = claimable = 0;
            foreach (TransactionDocumentItem item in transactionDocumentItem)
            {
                TransactionItems titem = iERPTransactionClient.GetTransactionItems(item.code);
                if (titem.ExpenseType == ExpenseType.TaxAuthorityUnclaimable)
                {
                    unclaimable++;
                    if (claimable > 0)
                        return true;
                }
                else
                {
                    claimable++;
                    if (unclaimable > 0)
                        return true;
                }

            }
            return false;
        }

        private bool ValidateNegativeTransactionPost(double assetBalance, bool post)
        {
            double payAmount = _doc.amount;
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(paymentTypeSelector.PaymentMethod) && textServiceCharge.Text != "")
            {
                if (_doc.serviceChargePayer == ServiceChargePayer.Company)
                    payAmount = _doc.amount + double.Parse(textServiceCharge.Text);
            }
            if (Account.AmountGreater(payAmount, assetBalance))
            {
                if (MessageBox.ShowWarningMessage("There is no sufficient money in the selected payment account so posting will cause negative balance. Are you sure you want to continue?") == DialogResult.Yes)
                    post = true;
                else
                    post = false;
            }
            else
                post = true;
            return post;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        void updatePrevNextButton()
        {
            
            buttonNext.Text = tabControl.SelectedTabPageIndex == tabControl.TabPages.Count - 1 ? "Finish" : "Next>>";
            buttonPrev.Enabled = tabControl.SelectedTabPageIndex > 0;
        }
       

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            tabControl.SelectedTabPageIndex--;
        }

        private void tabControl_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            updatePrevNextButton();
        }

        private void textRetention_EditValueChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents) return;
            updateSummaryTable();
        }

        private void memoNote_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void tabControl_Click(object sender, EventArgs e)
        {

        }

        private void linkEditSupplier_OpenLink(object sender, DevExpress.XtraEditors.Controls.OpenLinkEventArgs e)
        {
            RegisterRelation rr = new RegisterRelation(_supplier.Code);
            if (rr.ShowDialog(this) == DialogResult.OK)
            {
                setSupplier(_supplier.coaCode);
                updateSummaryTable();
            }
        }

        private void buttonPrepayments_Click(object sender, EventArgs e)
        {

        }

        private void checkIncoiced_CheckedChanged(object sender, EventArgs e)
        {
            buttonSetInvoice.Visible = !checkInvoiced.Checked;
        }

    }
}