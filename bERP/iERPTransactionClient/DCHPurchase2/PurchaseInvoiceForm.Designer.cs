﻿namespace BIZNET.iERP.Client
{
    partial class PurchaseInvoiceForm
        
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.checkWitholdFull = new DevExpress.XtraEditors.CheckEdit();
            this.textWHTNo = new DevExpress.XtraEditors.TextEdit();
            this.textVATWithholdingReceiptNo = new DevExpress.XtraEditors.TextEdit();
            this.textWHBase = new DevExpress.XtraEditors.TextEdit();
            this.textVATBase = new DevExpress.XtraEditors.TextEdit();
            this.checkFullVAT = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutVATBase = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutFullVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutWHBase = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutWHTRNo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutFullWH = new DevExpress.XtraLayout.LayoutControlItem();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.memoNote = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTaxation = new DevExpress.XtraLayout.LayoutControlItem();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonCredit = new DevExpress.XtraEditors.SimpleButton();
            this.buttonDeliveries = new DevExpress.XtraEditors.SimpleButton();
            this.buttonPrepayments = new DevExpress.XtraEditors.SimpleButton();
            this.itemSummaryGrid = new BIZNET.iERP.Client.ItemSummaryGrid();
            this.gridViewSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelPaymentInstruction = new DevExpress.XtraEditors.LabelControl();
            this.textReference = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.itemGrid = new BIZNET.iERP.Client.ItemGrid();
            this.gridViewItems = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelCustomerInformation = new DevExpress.XtraEditors.LabelControl();
            this.dateTransaction = new BIZNET.iERP.Client.BNDualCalendar();
            this.linkEditSupplier = new DevExpress.XtraEditors.HyperLinkEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRelationInformation = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.buttonPrev = new DevExpress.XtraEditors.SimpleButton();
            this.buttonNext = new DevExpress.XtraEditors.SimpleButton();
            this.dxValidationProvider = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkWitholdFull.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWHTNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textVATWithholdingReceiptNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWHBase.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textVATBase.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkFullVAT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVATBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFullVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWHBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWHTRNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFullWH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTaxation)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.itemSummaryGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.itemGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkEditSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRelationInformation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.groupControl2);
            this.layoutControl3.Controls.Add(this.labelControl3);
            this.layoutControl3.Controls.Add(this.memoNote);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(800, 384);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.layoutControl4);
            this.groupControl2.Location = new System.Drawing.Point(12, 44);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(776, 105);
            this.groupControl2.TabIndex = 41;
            this.groupControl2.Text = "Taxation";
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.checkWitholdFull);
            this.layoutControl4.Controls.Add(this.textWHTNo);
            this.layoutControl4.Controls.Add(this.textVATWithholdingReceiptNo);
            this.layoutControl4.Controls.Add(this.textWHBase);
            this.layoutControl4.Controls.Add(this.textVATBase);
            this.layoutControl4.Controls.Add(this.checkFullVAT);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl4.Location = new System.Drawing.Point(2, 21);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.Root = this.layoutControlGroup4;
            this.layoutControl4.Size = new System.Drawing.Size(772, 82);
            this.layoutControl4.TabIndex = 0;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // checkWitholdFull
            // 
            this.checkWitholdFull.Location = new System.Drawing.Point(683, 12);
            this.checkWitholdFull.Name = "checkWitholdFull";
            this.checkWitholdFull.Properties.Caption = "Full Invoice";
            this.checkWitholdFull.Size = new System.Drawing.Size(77, 19);
            this.checkWitholdFull.StyleController = this.layoutControl4;
            this.checkWitholdFull.TabIndex = 39;
            // 
            // textWHTNo
            // 
            this.textWHTNo.Location = new System.Drawing.Point(466, 42);
            this.textWHTNo.Name = "textWHTNo";
            this.textWHTNo.Size = new System.Drawing.Size(211, 20);
            this.textWHTNo.StyleController = this.layoutControl4;
            this.textWHTNo.TabIndex = 29;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "This value is not valid";
            this.dxValidationProvider.SetValidationRule(this.textWHTNo, conditionValidationRule2);
            // 
            // textVATWithholdingReceiptNo
            // 
            this.textVATWithholdingReceiptNo.Location = new System.Drawing.Point(81, 42);
            this.textVATWithholdingReceiptNo.Name = "textVATWithholdingReceiptNo";
            this.textVATWithholdingReceiptNo.Size = new System.Drawing.Size(228, 20);
            this.textVATWithholdingReceiptNo.StyleController = this.layoutControl4;
            this.textVATWithholdingReceiptNo.TabIndex = 40;
            // 
            // textWHBase
            // 
            this.textWHBase.Location = new System.Drawing.Point(466, 14);
            this.textWHBase.Name = "textWHBase";
            this.textWHBase.Properties.Mask.EditMask = "#,########0.00;";
            this.textWHBase.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textWHBase.Size = new System.Drawing.Size(211, 20);
            this.textWHBase.StyleController = this.layoutControl4;
            this.textWHBase.TabIndex = 38;
            // 
            // textVATBase
            // 
            this.textVATBase.Location = new System.Drawing.Point(81, 14);
            this.textVATBase.Name = "textVATBase";
            this.textVATBase.Properties.Mask.EditMask = "#,########0.00;";
            this.textVATBase.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textVATBase.Size = new System.Drawing.Size(228, 20);
            this.textVATBase.StyleController = this.layoutControl4;
            this.textVATBase.TabIndex = 35;
            // 
            // checkFullVAT
            // 
            this.checkFullVAT.Location = new System.Drawing.Point(315, 12);
            this.checkFullVAT.Name = "checkFullVAT";
            this.checkFullVAT.Properties.Caption = "Full Invoice";
            this.checkFullVAT.Size = new System.Drawing.Size(78, 19);
            this.checkFullVAT.StyleController = this.layoutControl4;
            this.checkFullVAT.TabIndex = 36;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutVATBase,
            this.layoutFullVAT,
            this.layoutWHBase,
            this.layoutWHTRNo,
            this.layoutFullWH});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(772, 82);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textVATWithholdingReceiptNo;
            this.layoutControlItem2.CustomizationFormText = "VAT WH No:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(303, 34);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem2.Text = "VAT WH No:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(64, 13);
            // 
            // layoutVATBase
            // 
            this.layoutVATBase.Control = this.textVATBase;
            this.layoutVATBase.CustomizationFormText = "VAT Base";
            this.layoutVATBase.Location = new System.Drawing.Point(0, 0);
            this.layoutVATBase.Name = "layoutVATBase";
            this.layoutVATBase.Size = new System.Drawing.Size(303, 28);
            this.layoutVATBase.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutVATBase.Text = "VAT Base";
            this.layoutVATBase.TextSize = new System.Drawing.Size(64, 13);
            // 
            // layoutFullVAT
            // 
            this.layoutFullVAT.Control = this.checkFullVAT;
            this.layoutFullVAT.CustomizationFormText = "layoutFullVAT";
            this.layoutFullVAT.Location = new System.Drawing.Point(303, 0);
            this.layoutFullVAT.Name = "layoutFullVAT";
            this.layoutFullVAT.Size = new System.Drawing.Size(82, 62);
            this.layoutFullVAT.Text = "layoutFullVAT";
            this.layoutFullVAT.TextSize = new System.Drawing.Size(0, 0);
            this.layoutFullVAT.TextToControlDistance = 0;
            this.layoutFullVAT.TextVisible = false;
            // 
            // layoutWHBase
            // 
            this.layoutWHBase.Control = this.textWHBase;
            this.layoutWHBase.CustomizationFormText = "layoutControlItem9";
            this.layoutWHBase.Location = new System.Drawing.Point(385, 0);
            this.layoutWHBase.Name = "layoutWHBase";
            this.layoutWHBase.Size = new System.Drawing.Size(286, 28);
            this.layoutWHBase.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutWHBase.Text = "WH Tax Base";
            this.layoutWHBase.TextSize = new System.Drawing.Size(64, 13);
            // 
            // layoutWHTRNo
            // 
            this.layoutWHTRNo.Control = this.textWHTNo;
            this.layoutWHTRNo.CustomizationFormText = "Tax WH No:";
            this.layoutWHTRNo.Location = new System.Drawing.Point(385, 28);
            this.layoutWHTRNo.Name = "layoutWHTRNo";
            this.layoutWHTRNo.Size = new System.Drawing.Size(286, 34);
            this.layoutWHTRNo.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutWHTRNo.Text = "Tax WH No:";
            this.layoutWHTRNo.TextSize = new System.Drawing.Size(64, 13);
            // 
            // layoutFullWH
            // 
            this.layoutFullWH.Control = this.checkWitholdFull;
            this.layoutFullWH.CustomizationFormText = "layoutFullWH";
            this.layoutFullWH.Location = new System.Drawing.Point(671, 0);
            this.layoutFullWH.Name = "layoutFullWH";
            this.layoutFullWH.Size = new System.Drawing.Size(81, 62);
            this.layoutFullWH.Text = "layoutFullWH";
            this.layoutFullWH.TextSize = new System.Drawing.Size(0, 0);
            this.layoutFullWH.TextToControlDistance = 0;
            this.layoutFullWH.TextVisible = false;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.CornflowerBlue;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(12, 12);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(776, 28);
            this.labelControl3.StyleController = this.layoutControl3;
            this.labelControl3.TabIndex = 34;
            this.labelControl3.Text = "Taxes";
            // 
            // memoNote
            // 
            this.memoNote.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.memoNote.Location = new System.Drawing.Point(12, 169);
            this.memoNote.Name = "memoNote";
            this.memoNote.Size = new System.Drawing.Size(776, 203);
            this.memoNote.StyleController = this.layoutControl3;
            this.memoNote.TabIndex = 17;
            this.memoNote.EditValueChanged += new System.EventHandler(this.memoNote_EditValueChanged);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem32,
            this.layoutControlItem15,
            this.layoutTaxation});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(800, 384);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.labelControl3;
            this.layoutControlItem32.CustomizationFormText = "layoutControlItem32";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem32.MaxSize = new System.Drawing.Size(0, 32);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(22, 32);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(780, 32);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Text = "layoutControlItem32";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem32.TextToControlDistance = 0;
            this.layoutControlItem32.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.memoNote;
            this.layoutControlItem15.CustomizationFormText = "Additional Notes";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 141);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(144, 36);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(780, 223);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "Additional Notes";
            this.layoutControlItem15.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutTaxation
            // 
            this.layoutTaxation.Control = this.groupControl2;
            this.layoutTaxation.CustomizationFormText = "layoutControlItem6";
            this.layoutTaxation.Location = new System.Drawing.Point(0, 32);
            this.layoutTaxation.Name = "layoutTaxation";
            this.layoutTaxation.Size = new System.Drawing.Size(780, 109);
            this.layoutTaxation.Text = "layoutTaxation";
            this.layoutTaxation.TextSize = new System.Drawing.Size(0, 0);
            this.layoutTaxation.TextToControlDistance = 0;
            this.layoutTaxation.TextVisible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.buttonCredit);
            this.flowLayoutPanel1.Controls.Add(this.buttonDeliveries);
            this.flowLayoutPanel1.Controls.Add(this.buttonPrepayments);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(541, 93);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(287, 57);
            this.flowLayoutPanel1.TabIndex = 20;
            // 
            // buttonCredit
            // 
            this.buttonCredit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCredit.Location = new System.Drawing.Point(161, 3);
            this.buttonCredit.Name = "buttonCredit";
            this.buttonCredit.Size = new System.Drawing.Size(123, 23);
            this.buttonCredit.TabIndex = 5;
            this.buttonCredit.Text = "&Credit Settlement";
            this.buttonCredit.Visible = false;
            // 
            // buttonDeliveries
            // 
            this.buttonDeliveries.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDeliveries.Location = new System.Drawing.Point(32, 3);
            this.buttonDeliveries.Name = "buttonDeliveries";
            this.buttonDeliveries.Size = new System.Drawing.Size(123, 23);
            this.buttonDeliveries.TabIndex = 5;
            this.buttonDeliveries.Text = "&Delivery";
            this.buttonDeliveries.Visible = false;
            // 
            // buttonPrepayments
            // 
            this.buttonPrepayments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonPrepayments.Location = new System.Drawing.Point(32, 32);
            this.buttonPrepayments.Name = "buttonPrepayments";
            this.buttonPrepayments.Size = new System.Drawing.Size(252, 23);
            this.buttonPrepayments.TabIndex = 5;
            this.buttonPrepayments.Text = "&Expense Schedule for Prepayment";
            this.buttonPrepayments.Visible = false;
            // 
            // itemSummaryGrid
            // 
            this.itemSummaryGrid.Location = new System.Drawing.Point(23, 34);
            this.itemSummaryGrid.MainView = this.gridViewSummary;
            this.itemSummaryGrid.Name = "itemSummaryGrid";
            this.itemSummaryGrid.Size = new System.Drawing.Size(369, 116);
            this.itemSummaryGrid.TabIndex = 5;
            this.itemSummaryGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSummary});
            // 
            // gridViewSummary
            // 
            this.gridViewSummary.GridControl = this.itemSummaryGrid;
            this.gridViewSummary.GroupPanelText = "Payment Summary";
            this.gridViewSummary.Name = "gridViewSummary";
            // 
            // labelPaymentInstruction
            // 
            this.labelPaymentInstruction.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPaymentInstruction.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelPaymentInstruction.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelPaymentInstruction.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelPaymentInstruction.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelPaymentInstruction.Location = new System.Drawing.Point(407, 12);
            this.labelPaymentInstruction.Name = "labelPaymentInstruction";
            this.labelPaymentInstruction.Size = new System.Drawing.Size(375, 62);
            this.labelPaymentInstruction.TabIndex = 19;
            this.labelPaymentInstruction.Text = "#####";
            // 
            // textReference
            // 
            this.textReference.Location = new System.Drawing.Point(134, 46);
            this.textReference.Name = "textReference";
            this.textReference.Size = new System.Drawing.Size(261, 20);
            this.textReference.StyleController = this.layoutControl2;
            this.textReference.TabIndex = 1;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.itemGrid);
            this.layoutControl2.Controls.Add(this.labelControl1);
            this.layoutControl2.Controls.Add(this.labelCustomerInformation);
            this.layoutControl2.Controls.Add(this.dateTransaction);
            this.layoutControl2.Controls.Add(this.linkEditSupplier);
            this.layoutControl2.Controls.Add(this.textReference);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(593, 204, 250, 350);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(800, 384);
            this.layoutControl2.TabIndex = 33;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // itemGrid
            // 
            this.itemGrid.AllowPriceEdit = false;
            this.itemGrid.AllowRepeatedItem = false;
            this.itemGrid.AllowUnitPriceEdit = true;
            this.itemGrid.AutoUnitPrice = false;
            this.itemGrid.CurrentBalanceCaption = "Current Balance";
            this.itemGrid.DirectExpenseCaption = "Direct Issue?";
            this.itemGrid.GroupByCostCenter = false;
            this.itemGrid.Location = new System.Drawing.Point(12, 159);
            this.itemGrid.MainView = this.gridViewItems;
            this.itemGrid.Name = "itemGrid";
            this.itemGrid.Padding = new System.Windows.Forms.Padding(3);
            this.itemGrid.PrepaidCaption = "Prepaid?";
            this.itemGrid.PriceCaption = "Price";
            this.itemGrid.QuantityCaption = "Quantity";
            this.itemGrid.ReadOnly = false;
            this.itemGrid.ShowCostCenterColumn = false;
            this.itemGrid.ShowCurrentBalanceColumn = false;
            this.itemGrid.ShowDirectExpenseColumn = false;
            this.itemGrid.ShowPrepaid = false;
            this.itemGrid.ShowPrice = false;
            this.itemGrid.ShowQuantity = true;
            this.itemGrid.ShowRate = false;
            this.itemGrid.ShowRemitedColumn = false;
            this.itemGrid.ShowUnitPrice = true;
            this.itemGrid.Size = new System.Drawing.Size(776, 213);
            this.itemGrid.TabIndex = 3;
            this.itemGrid.UnitPriceCaption = "Unit Price";
            this.itemGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewItems});
            // 
            // gridViewItems
            // 
            this.gridViewItems.GridControl = this.itemGrid;
            this.gridViewItems.Name = "gridViewItems";
            this.gridViewItems.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewItems.OptionsCustomization.AllowFilter = false;
            this.gridViewItems.OptionsCustomization.AllowGroup = false;
            this.gridViewItems.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewItems.OptionsCustomization.AllowSort = false;
            this.gridViewItems.OptionsView.ShowGroupPanel = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.CornflowerBlue;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(776, 28);
            this.labelControl1.StyleController = this.layoutControl2;
            this.labelControl1.TabIndex = 33;
            this.labelControl1.Text = "Transaction Detail";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // labelCustomerInformation
            // 
            this.labelCustomerInformation.AllowHtmlString = true;
            this.labelCustomerInformation.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelCustomerInformation.Location = new System.Drawing.Point(14, 90);
            this.labelCustomerInformation.Name = "labelCustomerInformation";
            this.labelCustomerInformation.Padding = new System.Windows.Forms.Padding(3);
            this.labelCustomerInformation.Size = new System.Drawing.Size(381, 19);
            this.labelCustomerInformation.StyleController = this.layoutControl2;
            this.labelCustomerInformation.TabIndex = 15;
            this.labelCustomerInformation.Text = "###";
            // 
            // dateTransaction
            // 
            this.dateTransaction.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateTransaction.Location = new System.Drawing.Point(403, 62);
            this.dateTransaction.Name = "dateTransaction";
            this.dateTransaction.ShowEthiopian = true;
            this.dateTransaction.ShowGregorian = true;
            this.dateTransaction.ShowTime = true;
            this.dateTransaction.Size = new System.Drawing.Size(383, 63);
            this.dateTransaction.TabIndex = 0;
            this.dateTransaction.VerticalLayout = true;
            // 
            // linkEditSupplier
            // 
            this.linkEditSupplier.EditValue = "Edit";
            this.linkEditSupplier.Location = new System.Drawing.Point(14, 117);
            this.linkEditSupplier.Name = "linkEditSupplier";
            this.linkEditSupplier.Size = new System.Drawing.Size(381, 20);
            this.linkEditSupplier.StyleController = this.layoutControl2;
            this.linkEditSupplier.TabIndex = 34;
            this.linkEditSupplier.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.linkEditSupplier_OpenLink);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.layoutControlItem5,
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutRelationInformation,
            this.layoutControlItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(800, 384);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.Control = this.dateTransaction;
            this.layoutControlItem13.CustomizationFormText = "Date";
            this.layoutControlItem13.Location = new System.Drawing.Point(389, 32);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(125, 44);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(391, 99);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem13.Text = "Date";
            this.layoutControlItem13.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.labelControl1;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 32);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(14, 32);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(780, 32);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.itemGrid;
            this.layoutControlItem4.CustomizationFormText = "Items";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 131);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(780, 233);
            this.layoutControlItem4.Text = "Items";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.textReference;
            this.layoutControlItem3.CustomizationFormText = "Supplier Invoice No.";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 32);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(389, 28);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem3.Text = "Invoice Number:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutRelationInformation
            // 
            this.layoutRelationInformation.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutRelationInformation.AppearanceItemCaption.Options.UseFont = true;
            this.layoutRelationInformation.Control = this.labelCustomerInformation;
            this.layoutRelationInformation.CustomizationFormText = "Supplier Information";
            this.layoutRelationInformation.Location = new System.Drawing.Point(0, 60);
            this.layoutRelationInformation.Name = "layoutRelationInformation";
            this.layoutRelationInformation.Size = new System.Drawing.Size(389, 43);
            this.layoutRelationInformation.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutRelationInformation.Text = "Supplier Information";
            this.layoutRelationInformation.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutRelationInformation.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.linkEditSupplier;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 103);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(389, 28);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.buttonCancel);
            this.panelControl1.Controls.Add(this.buttonPrev);
            this.panelControl1.Controls.Add(this.buttonNext);
            this.panelControl1.Controls.Add(this.itemSummaryGrid);
            this.panelControl1.Controls.Add(this.labelPaymentInstruction);
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 390);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(828, 184);
            this.panelControl1.TabIndex = 18;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Location = new System.Drawing.Point(23, 12);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(125, 13);
            this.labelControl4.TabIndex = 21;
            this.labelControl4.Text = "Transaction Summary";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(764, 156);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(59, 23);
            this.buttonCancel.TabIndex = 6;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonPrev
            // 
            this.buttonPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrev.Location = new System.Drawing.Point(592, 156);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(80, 23);
            this.buttonPrev.TabIndex = 5;
            this.buttonPrev.Text = "<<Previous";
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNext.Location = new System.Drawing.Point(678, 156);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(80, 23);
            this.buttonNext.TabIndex = 5;
            this.buttonNext.Text = "Next>>";
            this.buttonNext.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // dxValidationProvider
            // 
            this.dxValidationProvider.ValidateHiddenControls = false;
            // 
            // tabControl
            // 
            this.tabControl.AppearancePage.Header.BackColor = System.Drawing.Color.Silver;
            this.tabControl.AppearancePage.Header.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabControl.AppearancePage.Header.Options.UseBackColor = true;
            this.tabControl.AppearancePage.Header.Options.UseBorderColor = true;
            this.tabControl.AppearancePage.HeaderActive.BackColor = System.Drawing.Color.DimGray;
            this.tabControl.AppearancePage.HeaderActive.BackColor2 = System.Drawing.Color.White;
            this.tabControl.AppearancePage.HeaderActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tabControl.AppearancePage.HeaderActive.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl.AppearancePage.HeaderActive.ForeColor = System.Drawing.Color.Blue;
            this.tabControl.AppearancePage.HeaderActive.Options.UseBackColor = true;
            this.tabControl.AppearancePage.HeaderActive.Options.UseBorderColor = true;
            this.tabControl.AppearancePage.HeaderActive.Options.UseFont = true;
            this.tabControl.AppearancePage.HeaderActive.Options.UseForeColor = true;
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.HeaderButtons = DevExpress.XtraTab.TabButtons.Next;
            this.tabControl.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.tabControl.HeaderOrientation = DevExpress.XtraTab.TabOrientation.Vertical;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedTabPage = this.xtraTabPage1;
            this.tabControl.Size = new System.Drawing.Size(828, 390);
            this.tabControl.TabIndex = 1;
            this.tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage3});
            this.tabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.tabControl_SelectedPageChanged);
            this.tabControl.Click += new System.EventHandler(this.tabControl_Click);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.layoutControl2);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(800, 384);
            this.xtraTabPage1.Text = "Details";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.layoutControl3);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(800, 384);
            this.xtraTabPage3.Text = "Tax";
            // 
            // PurchaseInvoiceForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(828, 574);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panelControl1);
            this.Name = "PurchaseInvoiceForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Invoice on Purchase";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkWitholdFull.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWHTNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textVATWithholdingReceiptNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWHBase.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textVATBase.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkFullVAT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVATBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFullVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWHBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWHTRNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFullWH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTaxation)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.itemSummaryGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.itemGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkEditSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRelationInformation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private BNDualCalendar dateTransaction;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        protected ItemGrid itemGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewItems;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonNext;
        private DevExpress.XtraEditors.MemoEdit memoNote;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidationProvider;
        private DevExpress.XtraEditors.TextEdit textReference;
        private DevExpress.XtraEditors.LabelControl labelCustomerInformation;
        private ItemSummaryGrid itemSummaryGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSummary;
        private DevExpress.XtraEditors.SimpleButton buttonCredit;
        private DevExpress.XtraEditors.LabelControl labelPaymentInstruction;
        private DevExpress.XtraEditors.SimpleButton buttonPrepayments;
        private DevExpress.XtraEditors.SimpleButton buttonDeliveries;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.TextEdit textWHTNo;
        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutRelationInformation;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.CheckEdit checkFullVAT;
        private DevExpress.XtraEditors.TextEdit textVATBase;
        private DevExpress.XtraEditors.CheckEdit checkWitholdFull;
        private DevExpress.XtraEditors.TextEdit textWHBase;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraEditors.SimpleButton buttonPrev;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.HyperLinkEdit linkEditSupplier;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit textVATWithholdingReceiptNo;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutVATBase;
        private DevExpress.XtraLayout.LayoutControlItem layoutFullVAT;
        private DevExpress.XtraLayout.LayoutControlItem layoutWHBase;
        private DevExpress.XtraLayout.LayoutControlItem layoutWHTRNo;
        private DevExpress.XtraLayout.LayoutControlItem layoutFullWH;
        private DevExpress.XtraLayout.LayoutControlItem layoutTaxation;
    }
}