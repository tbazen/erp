﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using DevExpress.XtraLayout.Utils;

namespace BIZNET.iERP.Client
{
    
    public partial class PurchaseInvoiceForm: DevExpress.XtraEditors.XtraForm,IItemGridController
    {
        PurchaseInvoiceDocument _doc=null;
        
        DocumentBasicFieldsController _docFields;
        IAccountingClient _client;
        ActivationParameter _activation;
        SettleFullAmountController _vatBase;
        SettleFullAmountController _whBase;

        TradeRelation _supplier=null;
        
        bool _ignoreEvents = false;
        Purchase2Document _purchaseDocument;
        public PurchaseInvoiceForm(IAccountingClient client, ActivationParameter activation)
        {
            _ignoreEvents = true;
            try
            {
                InitializeComponent();
                configureForm();
                updatePrevNextButton();
                _client = client;
                _activation = activation;
                _purchaseDocument = activation.purchaseDocument;
                _docFields = new DocumentBasicFieldsController(dateTransaction, textReference,memoNote);


                _vatBase = new SettleFullAmountController(textVATBase, checkFullVAT);
                checkFullVAT.Checked = true;
                _vatBase.ValueChanged += new EventHandler(_vatBase_ValueChanged);

                _whBase = new SettleFullAmountController(textWHBase, checkWitholdFull);
                checkWitholdFull.Checked = true;
                _whBase.ValueChanged += new EventHandler(_whBase_ValueChanged);
                setSupplier(_activation.objectCode);
                itemGrid.initializeGrid();
                itemGrid.Controller = this;
                itemSummaryGrid.initializeGrid();
                
                if (_purchaseDocument != null)
                {
                    List<TransactionDocumentItem> remaining= new List<TransactionDocumentItem>();
                    remaining.AddRange(_purchaseDocument.items);
                    foreach (int inv in _purchaseDocument.invoiceIDs)
                    {
                        PurchaseInvoiceDocument invoice = AccountingClient.GetAccountDocument(inv, true) as PurchaseInvoiceDocument;
                        remaining = TransactionDocumentItem.subTract(remaining, invoice.items);
                    }
                    foreach (TransactionDocumentItem item in remaining)
                        itemGrid.Add(itemGrid.toItemGridTableRow(item));
                }
                
                updateSummaryTable();
            }
            finally
            {
                _ignoreEvents = false;
            }
        }

        private void configureForm()
        {
            this.Text = getFormTitle();
            layoutRelationInformation.Text = getTradeRelationType() + " Information";
            linkEditSupplier.Text = "Edit " + getTradeRelationType();
            List<TradeTransactionPaymentField> fields = getPaymentFields();
        }

        void _whBase_ValueChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents) return;
            updateSummaryTable(sender);
        }

        void _vatBase_ValueChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents) return;
            updateSummaryTable();
        }

        void _paidAmount_ValueChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents) return;
            updateSummaryTable();
        }
        public bool interactiveValidateAmount()
        {
            return true;
        }
        private PurchaseInvoiceDocument buildInvoiceDocument()
        {
            PurchaseInvoiceDocument ret = new PurchaseInvoiceDocument();
            ret.AccountDocumentID = _doc == null ? -1 : _doc.AccountDocumentID;
            _docFields.setDocumentData(ret);

            ret.withholdingReceiptNo = textWHTNo.Text;
            ret.vatWithholdingReceiptNo = textVATWithholdingReceiptNo.Text;
            TransactionItems[] items;
            TransactionDocumentItem[] ditems;
            extractGridData(out items, out ditems);
            ret.items = ditems;
            double taxable;
            bool vatApplies, whApplies;
            double[] pwt;
            double totalAmount;
            ret.taxImposed = Purchase2Document.getTaxes(_supplier, iERPTransactionClient.GetCompanyProfile(), ditems, items, iERPTransactionClient.getTaxRates()
                , !checkFullVAT.Checked, _vatBase.effectiveAmount
                , !checkWitholdFull.Checked, _whBase.effectiveAmount
                , out totalAmount, out taxable
                , out vatApplies, out whApplies, out pwt);
            ret.manualVATBase = checkFullVAT.Checked ? -1d : _vatBase.effectiveAmount;
            ret.manualWithholdBase = checkWitholdFull.Checked ? -1d : _whBase.effectiveAmount;
            ret.purchaseTransactionDocumentID = _purchaseDocument.AccountDocumentID;
            return ret;
        }

        protected virtual void setSupplier(string code)
        {
            if (string.IsNullOrEmpty(code))
                _supplier = null;
            else
            {
                _supplier = iERPTransactionClient.GetRelation(code);
                if (_supplier == null)
                {
                    MessageBox.ShowErrorMessage("Supplier with code " + code + " is not found in the database");
                    return;
                }
            }
            updateSupplierInfo();
        }

        private void updateSupplierInfo()
        {
            if (_supplier == null)
            {
                labelCustomerInformation.Text = "";
                linkEditSupplier.Enabled = false;
            }
            else
            {
                string info = _supplier.Name;
                linkEditSupplier.Enabled = true;
               labelCustomerInformation.Text = info+getAdditionalRelationInfo(_supplier,dateTransaction.DateTime);
            }
        }
        public virtual void LoadData(PurchaseInvoiceDocument invoice)
        {
            
                _doc = invoice;
                if (_doc == null)
                    return;
                _docFields.setControlData(invoice);
                _purchaseDocument = _activation.purchaseDocument;
                setSupplier(_purchaseDocument.relationCode);
                textWHTNo.Text = invoice.withholdingReceiptNo;
                textVATWithholdingReceiptNo.Text = invoice.vatWithholdingReceiptNo;
                try
                {
                    _ignoreEvents = true;
                    itemGrid.Clear();
                    foreach (TransactionDocumentItem item in _doc.items)
                    {
                        itemGrid.Add(itemGrid.toItemGridTableRow(item));
                    }
                    checkFullVAT.Checked = AccountBase.AmountLess(_doc.manualVATBase, 0);
                    if (!AccountBase.AmountLess(_doc.manualVATBase, 0))
                        _vatBase.effectiveAmount = _doc.manualVATBase;
                    checkWitholdFull.Checked = AccountBase.AmountLess(_doc.manualWithholdBase, 0);
                    if (!AccountBase.AmountLess(_doc.manualWithholdBase, 0))
                        _whBase.effectiveAmount = _doc.manualWithholdBase;
                    double cash = updateSummaryTable();
                }
                finally
                {
                    _ignoreEvents = false;
                }
           
        }

        void extractGridData(out TransactionItems[] items, out TransactionDocumentItem[] docItems)
        {
            items = new TransactionItems[itemGrid.Count];
            docItems = new TransactionDocumentItem[itemGrid.Count];
            int i = 0;
            foreach (ItemGridControl.ItemGridData.ItemTableRow row in itemGrid)
            {
                items[i] = itemGrid.GetItem(row);
                docItems[i] = ItemGrid.toDocumentItem(row);
                docItems[i].price = docItems[i].quantity * docItems[i].unitPrice;
                i++;
            }
        }
        private double updateSummaryTable(object sender = null)
        {
            itemSummaryGrid.data.Clear();
            double totalPurchase = 0;
            double totalTaxable = 0;
            TransactionItems[] items;
            TransactionDocumentItem[] docItems;
            extractGridData(out items, out docItems);
            bool vatApplies, whApplies;
            double[] pwt;
            TaxImposed[] taxes = Purchase2Document.getTaxes(_supplier,
                iERPTransactionClient.GetCompanyProfile()
                , docItems, items
                , iERPTransactionClient.getTaxRates()
                , !checkFullVAT.Checked, _vatBase.effectiveAmount
                , !checkWitholdFull.Checked, _whBase.effectiveAmount
                , out totalPurchase
                , out totalTaxable
                , out vatApplies, out whApplies, out pwt
                );

            _whBase.FullAmount = totalTaxable;
            _vatBase.FullAmount = totalTaxable;

            double netPayment;
            bool hasWH = false;
            double withHolding = 0;
            if (!AccountBase.AmountEqual(totalTaxable, 0) || (taxes.Length == 1 && taxes[0].TaxType == TaxType.WithHoldingTax))
            {
                int order = 20;
                double addedTax = 0;
                foreach (TaxImposed tax in taxes)
                {
                    itemSummaryGrid.setSummary("tax_" + tax.TaxType, TaxImposed.TaxTypeName(tax.TaxType), tax.TaxValue, order);
                    order += 10;
                    addedTax += tax.TaxValue;
                    if (tax.TaxType == TaxType.WithHoldingTax)
                    {
                        withHolding = tax.TaxValue;
                        hasWH = true;
                        _whBase.FullAmount = tax.TaxBaseValue;
                    }
                    if (tax.TaxType == TaxType.VAT)
                    {
                        _vatBase.FullAmount = tax.TaxBaseValue;
                    }
                }
                itemSummaryGrid.setSummary("totalPrice", "Total Before Tax", totalPurchase, 10);
                netPayment = totalPurchase + addedTax;
                order += 10;
            }
            else
                netPayment = totalPurchase;

            layoutWHTRNo.Visibility = hasWH ? LayoutVisibility.Always : LayoutVisibility.Never;
            layoutVATBase.Visibility = layoutFullWH.Visibility = whApplies || sender != null ? LayoutVisibility.Always : LayoutVisibility.Never;
            layoutFullVAT.Visibility = layoutVATBase.Visibility = vatApplies ? LayoutVisibility.Always : LayoutVisibility.Never;
            layoutTaxation.Visibility = (whApplies || sender != null) || vatApplies ? LayoutVisibility.Always : LayoutVisibility.Never;
            double cash = netPayment;


            itemSummaryGrid.setSummary("cash", "Cash", netPayment, 70);

            if (hasWH)
                labelPaymentInstruction.Text = string.Format("Prepare Witholding Receipt of amount {0}({1}) for {2}"
                    , TSConstants.FormatBirr(-withHolding)
                    , CurrencyTranslator.TranslateCurrency(-(decimal)withHolding)
                    , _supplier.Name + (string.IsNullOrEmpty(_supplier.TIN) ? "" : " -TIN:" + _supplier.TIN)
                    );
            else
                labelPaymentInstruction.Text = "";


            return cash;
        }

        public double getUnitPrice(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            throw new NotImplementedException();
        }

        public double getBalance(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            return 0;
        }

        public bool hasUnitPrice(TransactionItems item)
        {
            throw new NotImplementedException();
        }

        public bool queryCellChange(ItemGridControl.ItemGridData.ItemTableRow row, DataColumn column, object oldValue)
        {
            if (_ignoreEvents) return true;
            TransactionItems item = itemGrid.GetItem(row);
            if (item == null)
                return true;
            if (column == itemGrid.data.priceColumn
                || column == itemGrid.data.unitPriceColumn
                || column == itemGrid.data.quantityColumn
                )
            {
                updateSummaryTable();
                return true;
            }
            return true;
        }


        public bool queryDeleteRow(ItemGridControl.ItemGridData.ItemTableRow row)
        {
            if (_ignoreEvents) return true;
            updateSummaryTable();
            return true;
        }
        public void afterComit(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {

        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (tabControl.SelectedTabPageIndex == tabControl.TabPages.Count - 1)
                save();
            else
            {
                if (!dxValidationProvider.Validate())
                    return;
                tabControl.SelectedTabPageIndex++;
            }
        }

        private void save()
        {
            try
            {
                if (!dxValidationProvider.Validate())
                    return;
                if (!(_client is DocumentScheduler))
                {

                    if (textReference.Text.Trim().Equals(""))
                    {
                        MessageBox.ShowErrorMessage("Please enter supplier reference");
                        return;
                    }
                }
                _doc = buildInvoiceDocument();
                bool updateDoc = _doc == null ? false : true;
                if (IsTaxAuthorityUnclaimableItemMixed(_doc.items))
                {
                    MessageBox.ShowErrorMessage("You cannot mix tax authority unclaimable items with other items!");
                    return;
                }
                _doc.AccountDocumentID = _client.PostGenericDocument(_doc);
                if (!(_client is DocumentScheduler))
                {
                    MessageBox.ShowSuccessMessage("Document succesfully posted");
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private bool IsTaxAuthorityUnclaimableItemMixed(TransactionDocumentItem[] transactionDocumentItem)
        {
            int unclaimable, claimable;
            unclaimable = claimable = 0;
            foreach (TransactionDocumentItem item in transactionDocumentItem)
            {
                TransactionItems titem = iERPTransactionClient.GetTransactionItems(item.code);
                if (titem.ExpenseType == ExpenseType.TaxAuthorityUnclaimable)
                {
                    unclaimable++;
                    if (claimable > 0)
                        return true;
                }
                else
                {
                    claimable++;
                    if (unclaimable > 0)
                        return true;
                }

            }
            return false;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        void updatePrevNextButton()
        {
            
            buttonNext.Text = tabControl.SelectedTabPageIndex == tabControl.TabPages.Count - 1 ? "Finish" : "Next>>";
            buttonPrev.Enabled = tabControl.SelectedTabPageIndex > 0;
        }
       

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            tabControl.SelectedTabPageIndex--;
        }

        private void tabControl_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            updatePrevNextButton();
        }

        private void textRetention_EditValueChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents) return;
            updateSummaryTable();
        }

        private void memoNote_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void tabControl_Click(object sender, EventArgs e)
        {

        }

        private void linkEditSupplier_OpenLink(object sender, DevExpress.XtraEditors.Controls.OpenLinkEventArgs e)
        {
            RegisterRelation rr = new RegisterRelation(_supplier.Code);
            if (rr.ShowDialog(this) == DialogResult.OK)
            {
                setSupplier(_supplier.coaCode);
                updateSummaryTable();
            }
        }

    }
}