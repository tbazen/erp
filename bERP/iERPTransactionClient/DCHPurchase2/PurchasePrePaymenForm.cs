﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class PurchasePrePaymentForm : DevExpress.XtraEditors.XtraForm, IItemGridController
    {
        PurchasePrePaymentDocument _doc;
        DocumentBasicFieldsController _docFields;
        IAccountingClient _client;
        ActivationParameter _activation;
        Purchase2Document _purchaseDocument;
        bool _editMode = false;
        public PurchasePrePaymentForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            _client = client;
            _activation = activation;
            itemGrid.Controller = this;
            itemGrid.initializeGrid();
            voucher.setKeys(typeof(PurchasePrePaymentDocument), "voucher");
            _docFields = new DocumentBasicFieldsController(dateTransaction, null, memoNote);
            setPurhcaseDocument(activation.purchaseDocument);
            reloadItemsFromPurhcase();
        }
        bool generationMode
        {
            get
            {
                return (_client is DocumentScheduler) && !_editMode;
            }
        }
        private void setPurhcaseDocument(Purchase2Document doc)
        {
            _purchaseDocument = doc;
            if (_purchaseDocument != null)
                voucher.setReference(_purchaseDocument.AccountDocumentID, _purchaseDocument.paymentVoucher);
        }

        private void reloadItemsFromPurhcase()
        {
            itemGrid.Clear();
            if (_purchaseDocument != null)
                foreach (TransactionDocumentItem item in _purchaseDocument.items)
                {
                    TransactionDocumentItem citem = item.clone();
                    if (citem.prepaid)
                    {
                        itemGrid.Add(itemGrid.toItemGridTableRow(item));
                        itemGrid[itemGrid.Count - 1].quantity = getBalance(null,itemGrid.GetItem(itemGrid[itemGrid.Count - 1]));
                    }
                }
        }

        public double getUnitPrice(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            foreach (TransactionDocumentItem ditem in _activation.purchaseDocument.items)
            {
                if (TransactionItems.codeEqual(item.Code, ditem.code))
                {
                    return ditem.unitPrice;
                }
            }
            throw new Exception("Item not found in the original document");
        }

        public double getBalance(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            double orderBalance = 0;
            if (_purchaseDocument != null)
            {
                foreach (TransactionDocumentItem ditem in _purchaseDocument.items)
                {
                    if (TransactionItems.codeEqual(item.Code, ditem.code))
                    {
                        orderBalance = ditem.quantity;
                        break;
                    }
                }
            }
            PurchasePrePaymentDocument[] deliveries;
            if (_client is DocumentScheduler)
            {
                deliveries = ((DocumentScheduler)_client).GetListedSchedules<PurchasePrePaymentDocument>().ToArray();
            }
            else
                deliveries = _purchaseDocument.prepayments;
            foreach (PurchasePrePaymentDocument doc in deliveries)
            {
                if (doc.DocumentDate <= dateTransaction.DateTime && doc!=_doc)
                {
                    foreach (TransactionDocumentItem ditem in doc.items)
                    {
                        if (TransactionItems.codeEqual(item.Code, ditem.code))
                        {
                            orderBalance -= ditem.quantity;
                            break;
                        }
                    }
                }
            }
            return orderBalance;
        }

        internal void LoadData(PurchasePrePaymentDocument prePaymentDocument)
        {
            _doc = prePaymentDocument;
            if (prePaymentDocument != null)
            {
                setPurhcaseDocument(_activation.purchaseDocument);
                _editMode = true;
                _docFields.setControlData(_doc);
                voucher.setReference(_doc.AccountDocumentID, _doc.voucher);
                
                itemGrid.Clear();
                foreach (TransactionDocumentItem item in _doc.items)
                {
                    itemGrid.Add(item);
                }
                layoutDuration.Visibility = layoutPeriodType.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (!dxValidationProvider.Validate())
                    return;
                int nMonths;
                if (generationMode && int.TryParse(textDuration.Text, out  nMonths))
                {
                    bool ethiopian = comboPeriodType.SelectedIndex == 0;
                    DateTime date = dateTransaction.DateTime;
                    DocumentSerialType sert = AccountingClient.getDocumentSerialType("GEN");
                    string parentReference = _activation.purchaseDocument == null ? null : _activation.purchaseDocument.paymentVoucher.reference;
                    if (sert == null)
                    {
                        MessageBox.ShowErrorMessage("Please configure GEN document reference type");
                        return;
                    }
                    for (int i = 0; i < nMonths; i++)
                    {
                        if (ethiopian)
                        {
                            INTAPS.Ethiopic.EtGrDate etDate = INTAPS.Ethiopic.EtGrDate.ToEth(new INTAPS.Ethiopic.EtGrDate(date));
                            if (etDate.Month < 12)
                                etDate.Month++;
                            else if (etDate.Month == 12)
                            {
                                etDate.Month = 1;
                                etDate.Year++;
                            }
                            else //pagumen
                            {
                                etDate.Year++;
                                etDate.Month = 1;
                                etDate.Day = 30;
                            }
                            date = INTAPS.Ethiopic.EtGrDate.ToGrig(etDate).GridDate;
                        }
                        else
                            date = date.AddMonths(1);

                        PurchasePrePaymentDocument thisDoc = new PurchasePrePaymentDocument();
                        thisDoc.AccountDocumentID = -1;
                        _docFields.setDocumentData(thisDoc);
                        thisDoc.voucher = new DocumentTypedReference(sert.id, parentReference + "-" + (i + 1), true);
                        thisDoc.PaperRef = thisDoc.voucher.reference;
                        thisDoc.DocumentDate = date;
                        List<TransactionDocumentItem> items = new List<TransactionDocumentItem>();
                        foreach (ItemGridControl.ItemGridData.ItemTableRow row in itemGrid.data)
                        {
                            if (!string.IsNullOrEmpty(row.code))
                            {
                                TransactionDocumentItem docItem = ItemGrid.toDocumentItem(row);
                                docItem.quantity = docItem.quantity / (double)nMonths;
                                docItem.price = docItem.quantity * docItem.unitPrice;
                                items.Add(docItem);
                            }
                        }
                        thisDoc.items = items.ToArray();
                        thisDoc.AccountDocumentID = _client.PostGenericDocument(thisDoc);
                    }
                }
                else
                {
                    int docID = _doc == null ? -1 : _doc.AccountDocumentID;
                    PurchasePrePaymentDocument newDoc = new PurchasePrePaymentDocument();
                    newDoc.AccountDocumentID = docID;
                    _docFields.setDocumentData(newDoc);
                    List<TransactionDocumentItem> items = new List<TransactionDocumentItem>();
                    foreach (ItemGridControl.ItemGridData.ItemTableRow row in itemGrid.data)
                    {
                        if (!string.IsNullOrEmpty(row.code))
                            items.Add(ItemGrid.toDocumentItem(row));
                    }
                    newDoc.items = items.ToArray();
                    newDoc.voucher = voucher.getReference();
                    newDoc.PaperRef = newDoc.voucher == null ? "" : newDoc.voucher.reference;
                    newDoc.AccountDocumentID = _client.PostGenericDocument(newDoc);
                    if (!(_client is DocumentScheduler))
                    {
                        MessageBox.ShowSuccessMessage("Prepaid expense");
                    }
                    _doc = newDoc;
                }
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        public bool hasUnitPrice(TransactionItems item)
        {
            return true;
        }


        public bool queryCellChange(ItemGridControl.ItemGridData.ItemTableRow row, DataColumn column, object oldValue)
        {
            if (_purchaseDocument != null)
            {
                if (column == itemGrid.data.codeColumn)
                {
                    foreach (TransactionDocumentItem docitem in _purchaseDocument.items)
                    {
                        if (TransactionItems.codeEqual(docitem.code, row.code))
                            return true;
                    }
                    return false;
                }
            }
            return true;
        }


        public bool queryDeleteRow(ItemGridControl.ItemGridData.ItemTableRow row)
        {
            return true;
        }
        public void afterComit(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {

        }

    }
}

