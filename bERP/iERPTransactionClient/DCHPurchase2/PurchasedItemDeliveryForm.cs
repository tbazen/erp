﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Client
{
    public partial class PurchasedItemDeliveryForm : DevExpress.XtraEditors.XtraForm, IItemGridController
    {
        PurchasedItemDeliveryDocument _doc;
        DocumentBasicFieldsController _docFields;
        IAccountingClient _client;
        ActivationParameter _activation;
        Purchase2Document _purchaseDocument;
        bool _quantityModified;
        public PurchasedItemDeliveryForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            voucher.setKeys(typeof(PurchasedItemDeliveryDocument), "voucher");
            _client = client;
            _activation = activation;
            
            if (_activation.storeID != -1)
                storePlaceholder.storeID = _activation.storeID;
            itemGrid.Controller = this;
            itemGrid.initializeGrid();
            _docFields = new DocumentBasicFieldsController(dateTransaction, null, memoNote);
            setPurhcaseDocument(activation.purchaseDocument);
            reloadItemsFromPurhcase();
        }

        private void setPurhcaseDocument(Purchase2Document doc)
        {
            _purchaseDocument = doc;
        }

        private void reloadItemsFromPurhcase()
        {
            itemGrid.Clear();
            if (_purchaseDocument != null)
                foreach (TransactionDocumentItem item in _purchaseDocument.items)
                {
                    TransactionDocumentItem citem = item.clone();
                    TransactionItems titem = iERPTransactionClient.GetTransactionItems(item.code);
                    if (titem.IsInventoryItem || titem.IsFixedAssetItem)
                    {
                        itemGrid.Add(itemGrid.toItemGridTableRow(item));
                        itemGrid[itemGrid.Count - 1].quantity = getBalance(null,titem);
                    }
                }
        }

        public double getUnitPrice(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            if (_activation.purchaseDocument != null)
            {
                foreach (TransactionDocumentItem ditem in _activation.purchaseDocument.items)
                {
                    if (TransactionItems.codeEqual(item.Code, ditem.code))
                    {
                        return ditem.unitPrice;
                    }
                }
            }
            throw new Exception("Item not found in the original document");
        }

        public double getBalance(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            double orderBalance = 0;
            if (_purchaseDocument != null)
            {
                foreach (TransactionDocumentItem ditem in _purchaseDocument.items)
                {
                    if (TransactionItems.codeEqual(item.Code, ditem.code))
                    {
                        orderBalance = ditem.quantity;
                        break;
                    }
                }
            }
            PurchasedItemDeliveryDocument[] deliveries;
            if (_client is DocumentScheduler)
            {
                deliveries = ((DocumentScheduler)_client).GetListedSchedules<PurchasedItemDeliveryDocument>().ToArray();
            }
            else
                deliveries = _purchaseDocument.deliveries;
            foreach (PurchasedItemDeliveryDocument doc in deliveries)
            {
                if (doc.DocumentDate <= dateTransaction.DateTime && doc!=_doc)
                {
                    foreach (TransactionDocumentItem ditem in doc.items)
                    {
                        if (TransactionItems.codeEqual(item.Code, ditem.code))
                        {
                            orderBalance -= ditem.quantity;
                            break;
                        }
                    }
                }
            }
            return orderBalance;
        }

        internal void LoadData(PurchasedItemDeliveryDocument deliveryDocument)
        {
            _doc = deliveryDocument;
            if (deliveryDocument != null)
            {
                voucher.setReference(deliveryDocument.AccountDocumentID, deliveryDocument.voucher);
                _docFields.setControlData(_doc);
                int storeID = -1;
                foreach (TransactionDocumentItem item in deliveryDocument.items)
                {
                    if (storeID == -1)
                        storeID = item.costCenterID;
                    else
                        if (storeID != item.costCenterID)
                        {
                            throw new ServerUserMessage("Items delivery to multiple stores not supported with single delivery document");
                        }
                }
                storePlaceholder.storeID = storeID;
                /*if (deliveryDocument.orderDocumentID != -1)//AUDIT: specifically coded for scheduling
                    setPurhcaseDocument(AccountingClient.GetAccountDocument(deliveryDocument.orderDocumentID, true) as Purchase2Document);
                 */
                setPurhcaseDocument(_activation.purchaseDocument);
                itemGrid.Clear();
                foreach (TransactionDocumentItem item in _doc.items)
                {
                    itemGrid.Add(item);
                }
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (!dxValidationProvider.Validate())
                    return;
                if (!(_client is DocumentScheduler))
                {
                    if (voucher.getReference() == null)
                    {
                        MessageBox.ShowErrorMessage("Enter valid reference");
                        return;
                    }
                }
                int docID = _doc == null ? -1 : _doc.AccountDocumentID;
                _doc = new PurchasedItemDeliveryDocument();
                _doc.AccountDocumentID = docID;
                _doc.voucher = voucher.getReference();
                _docFields.setDocumentData(_doc);
                List<TransactionDocumentItem> items = new List<TransactionDocumentItem>();
                foreach (ItemGridControl.ItemGridData.ItemTableRow row in itemGrid.data)
                {
                    if (!string.IsNullOrEmpty(row.code))
                        items.Add(ItemGrid.toDocumentItem(row));
                }
                _doc.items = items.ToArray();
                foreach (TransactionDocumentItem ditems in _doc.items)
                {
                    ditems.costCenterID = storePlaceholder.storeID;
                }
                _doc.AccountDocumentID=_client.PostGenericDocument(_doc);
                if(!(_client is DocumentScheduler))
                {
                    MessageBox.ShowSuccessMessage("Order succesfully registered");
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        public bool hasUnitPrice(TransactionItems item)
        {
            return true;
        }


        public bool queryCellChange(ItemGridControl.ItemGridData.ItemTableRow row, DataColumn column, object oldValue)
        {
            if (_purchaseDocument != null)
            {
                if (column == itemGrid.data.codeColumn)
                {
                    foreach (TransactionDocumentItem docitem in _purchaseDocument.items)
                    {
                        if (TransactionItems.codeEqual(docitem.code, row.code))
                            return true;
                    }
                    return false;
                }
            }
            return true;
        }


        public bool queryDeleteRow(ItemGridControl.ItemGridData.ItemTableRow row)
        {
            return true;
        }
        public void afterComit(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {

        }

    }
}

