using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHReceiveFromCashier : bERPClientDocumentHandler
    {
        public DCHReceiveFromCashier()
            : base(typeof(ReceiveFromCashierForm))
        {
        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            ReceiveFromCashierForm f = (ReceiveFromCashierForm)editor;

            f.LoadData((ReceiveFromCashierDocument)doc);
        }
    }
}
