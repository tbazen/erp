﻿namespace BIZNET.iERP.Client
{
    partial class ReceiveFromCashierForm 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.lblToBankBalance = new DevExpress.XtraEditors.LabelControl();
            this.txtAmount = new DevExpress.XtraEditors.TextEdit();
            this.txtServiceCharge = new DevExpress.XtraEditors.TextEdit();
            this.labelBankBalance = new DevExpress.XtraEditors.LabelControl();
            this.labelCashBalance = new DevExpress.XtraEditors.LabelControl();
            this.txtNote = new DevExpress.XtraEditors.MemoEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.txtReference = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlSlipRef = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlNote = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlBankBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlCashBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutServiceCharge = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.validationPayToCashier = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.cmbToBankAccount = new BIZNET.iERP.Client.BankAccountPlaceholder();
            this.voucher = new BIZNET.iERP.Client.DocumentSerialPlaceHolder();
            this.cmbBankAccount = new BIZNET.iERP.Client.BankAccountPlaceholder();
            this.paymentTypeSelector = new BIZNET.iERP.Client.PaymentTypeSelector();
            this.datePayment = new BIZNET.iERP.Client.BNDualCalendar();
            this.cmbCasher = new BIZNET.iERP.Client.CashAccountPlaceholder();
            this.layoutControlCashAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlBankAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceCharge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSlipRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServiceCharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationPayToCashier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbToBankAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBankAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentTypeSelector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCasher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.cmbToBankAccount);
            this.layoutControl1.Controls.Add(this.lblToBankBalance);
            this.layoutControl1.Controls.Add(this.txtAmount);
            this.layoutControl1.Controls.Add(this.txtServiceCharge);
            this.layoutControl1.Controls.Add(this.voucher);
            this.layoutControl1.Controls.Add(this.cmbBankAccount);
            this.layoutControl1.Controls.Add(this.paymentTypeSelector);
            this.layoutControl1.Controls.Add(this.datePayment);
            this.layoutControl1.Controls.Add(this.labelBankBalance);
            this.layoutControl1.Controls.Add(this.labelCashBalance);
            this.layoutControl1.Controls.Add(this.txtNote);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.cmbCasher);
            this.layoutControl1.Controls.Add(this.txtReference);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(447, 217, 250, 350);
            this.layoutControl1.OptionsView.HighlightFocusedItem = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(755, 398);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // lblToBankBalance
            // 
            this.lblToBankBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToBankBalance.Location = new System.Drawing.Point(519, 185);
            this.lblToBankBalance.Name = "lblToBankBalance";
            this.lblToBankBalance.Size = new System.Drawing.Size(229, 26);
            this.lblToBankBalance.StyleController = this.layoutControl1;
            this.lblToBankBalance.TabIndex = 27;
            this.lblToBankBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(178, 218);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtAmount.Properties.Mask.EditMask = "#,########0.00;";
            this.txtAmount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtAmount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtAmount.Size = new System.Drawing.Size(567, 20);
            this.txtAmount.StyleController = this.layoutControl1;
            this.txtAmount.TabIndex = 25;
            // 
            // txtServiceCharge
            // 
            this.txtServiceCharge.Location = new System.Drawing.Point(178, 278);
            this.txtServiceCharge.Name = "txtServiceCharge";
            this.txtServiceCharge.Size = new System.Drawing.Size(567, 20);
            this.txtServiceCharge.StyleController = this.layoutControl1;
            this.txtServiceCharge.TabIndex = 23;
            // 
            // labelBankBalance
            // 
            this.labelBankBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBankBalance.Location = new System.Drawing.Point(519, 155);
            this.labelBankBalance.Name = "labelBankBalance";
            this.labelBankBalance.Size = new System.Drawing.Size(229, 26);
            this.labelBankBalance.StyleController = this.layoutControl1;
            this.labelBankBalance.TabIndex = 8;
            this.labelBankBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // labelCashBalance
            // 
            this.labelCashBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCashBalance.Location = new System.Drawing.Point(519, 125);
            this.labelCashBalance.Name = "labelCashBalance";
            this.labelCashBalance.Size = new System.Drawing.Size(229, 26);
            this.labelCashBalance.StyleController = this.layoutControl1;
            this.labelCashBalance.TabIndex = 7;
            this.labelCashBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(178, 308);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(567, 51);
            this.txtNote.StyleController = this.layoutControl1;
            this.txtNote.TabIndex = 6;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.buttonCancel);
            this.panelControl1.Controls.Add(this.buttonOk);
            this.panelControl1.Location = new System.Drawing.Point(7, 366);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(741, 25);
            this.panelControl1.TabIndex = 5;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(677, 1);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(59, 23);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(599, 1);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(62, 23);
            this.buttonOk.TabIndex = 4;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // txtReference
            // 
            this.txtReference.Location = new System.Drawing.Point(178, 248);
            this.txtReference.Name = "txtReference";
            this.txtReference.Properties.Mask.EditMask = "\\w.*";
            this.txtReference.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtReference.Size = new System.Drawing.Size(567, 20);
            this.txtReference.StyleController = this.layoutControl1;
            this.txtReference.TabIndex = 2;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "Slip Reference cannot be empty";
            this.validationPayToCashier.SetValidationRule(this.txtReference, conditionValidationRule1);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlSlipRef,
            this.layoutControlItem7,
            this.layoutControlNote,
            this.layoutControlBankBalance,
            this.layoutControlCashAccount,
            this.layoutControlCashBalance,
            this.layoutControlItem4,
            this.layoutControlBankAccount,
            this.layoutControlItem5,
            this.layoutControlItem2,
            this.layoutServiceCharge,
            this.layoutControlItem8,
            this.layoutControlItem3,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(755, 398);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlSlipRef
            // 
            this.layoutControlSlipRef.Control = this.txtReference;
            this.layoutControlSlipRef.CustomizationFormText = "Slip Reference:";
            this.layoutControlSlipRef.Location = new System.Drawing.Point(0, 238);
            this.layoutControlSlipRef.Name = "layoutControlSlipRef";
            this.layoutControlSlipRef.Size = new System.Drawing.Size(745, 30);
            this.layoutControlSlipRef.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlSlipRef.Text = "Slip Reference:";
            this.layoutControlSlipRef.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.panelControl1;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 359);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(745, 29);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlNote
            // 
            this.layoutControlNote.Control = this.txtNote;
            this.layoutControlNote.CustomizationFormText = "Note:";
            this.layoutControlNote.Location = new System.Drawing.Point(0, 298);
            this.layoutControlNote.MinSize = new System.Drawing.Size(193, 26);
            this.layoutControlNote.Name = "layoutControlNote";
            this.layoutControlNote.Size = new System.Drawing.Size(745, 61);
            this.layoutControlNote.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlNote.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlNote.Text = "Note:";
            this.layoutControlNote.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlBankBalance
            // 
            this.layoutControlBankBalance.Control = this.labelBankBalance;
            this.layoutControlBankBalance.CustomizationFormText = "layoutControlItem8";
            this.layoutControlBankBalance.Location = new System.Drawing.Point(512, 148);
            this.layoutControlBankBalance.MaxSize = new System.Drawing.Size(233, 30);
            this.layoutControlBankBalance.MinSize = new System.Drawing.Size(233, 30);
            this.layoutControlBankBalance.Name = "layoutControlBankBalance";
            this.layoutControlBankBalance.Size = new System.Drawing.Size(233, 30);
            this.layoutControlBankBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlBankBalance.Text = "layoutControlItem8";
            this.layoutControlBankBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlBankBalance.TextToControlDistance = 0;
            this.layoutControlBankBalance.TextVisible = false;
            // 
            // layoutControlCashBalance
            // 
            this.layoutControlCashBalance.Control = this.labelCashBalance;
            this.layoutControlCashBalance.CustomizationFormText = "layoutControlCashBalance";
            this.layoutControlCashBalance.Location = new System.Drawing.Point(512, 118);
            this.layoutControlCashBalance.MaxSize = new System.Drawing.Size(233, 30);
            this.layoutControlCashBalance.MinSize = new System.Drawing.Size(233, 30);
            this.layoutControlCashBalance.Name = "layoutControlCashBalance";
            this.layoutControlCashBalance.Size = new System.Drawing.Size(233, 30);
            this.layoutControlCashBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlCashBalance.Text = "layoutControlCashBalance";
            this.layoutControlCashBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlCashBalance.TextToControlDistance = 0;
            this.layoutControlCashBalance.TextVisible = false;
            // 
            // layoutServiceCharge
            // 
            this.layoutServiceCharge.Control = this.txtServiceCharge;
            this.layoutServiceCharge.CustomizationFormText = "Service Charge";
            this.layoutServiceCharge.Location = new System.Drawing.Point(0, 268);
            this.layoutServiceCharge.Name = "layoutServiceCharge";
            this.layoutServiceCharge.Size = new System.Drawing.Size(745, 30);
            this.layoutServiceCharge.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutServiceCharge.Text = "Service Charge";
            this.layoutServiceCharge.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtAmount;
            this.layoutControlItem8.CustomizationFormText = "Amount to Pay:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 208);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(745, 30);
            this.layoutControlItem8.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem8.Text = "Amount:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.lblToBankBalance;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(512, 178);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(233, 30);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(233, 30);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(233, 30);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // validationPayToCashier
            // 
            this.validationPayToCashier.ValidateHiddenControls = false;
            // 
            // cmbToBankAccount
            // 
            this.cmbToBankAccount.Location = new System.Drawing.Point(178, 188);
            this.cmbToBankAccount.Name = "cmbToBankAccount";
            this.cmbToBankAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbToBankAccount.Properties.ImmediatePopup = true;
            this.cmbToBankAccount.Size = new System.Drawing.Size(334, 20);
            this.cmbToBankAccount.StyleController = this.layoutControl1;
            this.cmbToBankAccount.TabIndex = 28;
            this.cmbToBankAccount.SelectedIndexChanged += new System.EventHandler(this.bankAccountPlaceholder1_SelectedIndexChanged);
            // 
            // voucher
            // 
            this.voucher.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.voucher.Appearance.Options.UseBackColor = true;
            this.voucher.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.voucher.Location = new System.Drawing.Point(7, 23);
            this.voucher.Name = "voucher";
            this.voucher.Size = new System.Drawing.Size(368, 68);
            this.voucher.TabIndex = 20;
            // 
            // cmbBankAccount
            // 
            this.cmbBankAccount.Location = new System.Drawing.Point(178, 158);
            this.cmbBankAccount.Name = "cmbBankAccount";
            this.cmbBankAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbBankAccount.Properties.ImmediatePopup = true;
            this.cmbBankAccount.Size = new System.Drawing.Size(334, 20);
            this.cmbBankAccount.StyleController = this.layoutControl1;
            this.cmbBankAccount.TabIndex = 19;
            // 
            // paymentTypeSelector
            // 
            this.paymentTypeSelector.Include_BankTransferFromAccount = false;
            this.paymentTypeSelector.Include_BankTransferFromCash = true;
            this.paymentTypeSelector.Include_Cash = true;
            this.paymentTypeSelector.Include_Check = false;
            this.paymentTypeSelector.Include_CpoFromBank = false;
            this.paymentTypeSelector.Include_CpoFromCash = true;
            this.paymentTypeSelector.Include_Credit = false;
            this.paymentTypeSelector.Location = new System.Drawing.Point(178, 98);
            this.paymentTypeSelector.Name = "paymentTypeSelector";
            this.paymentTypeSelector.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.paymentTypeSelector.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.paymentTypeSelector.Size = new System.Drawing.Size(567, 20);
            this.paymentTypeSelector.StyleController = this.layoutControl1;
            this.paymentTypeSelector.TabIndex = 18;
            // 
            // datePayment
            // 
            this.datePayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.datePayment.Location = new System.Drawing.Point(382, 26);
            this.datePayment.Name = "datePayment";
            this.datePayment.ShowEthiopian = true;
            this.datePayment.ShowGregorian = true;
            this.datePayment.ShowTime = true;
            this.datePayment.Size = new System.Drawing.Size(363, 63);
            this.datePayment.TabIndex = 16;
            this.datePayment.VerticalLayout = true;
            this.datePayment.DateTimeChanged += new System.EventHandler(this.datePayment_DateTimeChanged);
            // 
            // cmbCasher
            // 
            this.cmbCasher.Location = new System.Drawing.Point(178, 128);
            this.cmbCasher.Name = "cmbCasher";
            this.cmbCasher.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCasher.Properties.ImmediatePopup = true;
            this.cmbCasher.Size = new System.Drawing.Size(334, 20);
            this.cmbCasher.StyleController = this.layoutControl1;
            this.cmbCasher.TabIndex = 1;
            // 
            // layoutControlCashAccount
            // 
            this.layoutControlCashAccount.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlCashAccount.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlCashAccount.Control = this.cmbCasher;
            this.layoutControlCashAccount.CustomizationFormText = "Cash Source:";
            this.layoutControlCashAccount.Location = new System.Drawing.Point(0, 118);
            this.layoutControlCashAccount.MinSize = new System.Drawing.Size(129, 30);
            this.layoutControlCashAccount.Name = "layoutControlCashAccount";
            this.layoutControlCashAccount.Size = new System.Drawing.Size(512, 30);
            this.layoutControlCashAccount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlCashAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlCashAccount.Text = "Cash Account";
            this.layoutControlCashAccount.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.Control = this.paymentTypeSelector;
            this.layoutControlItem4.CustomizationFormText = "Payment Method:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 88);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(745, 30);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem4.Text = "Payment Method:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlBankAccount
            // 
            this.layoutControlBankAccount.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlBankAccount.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlBankAccount.Control = this.cmbBankAccount;
            this.layoutControlBankAccount.CustomizationFormText = "Payment from Bank Account:";
            this.layoutControlBankAccount.Location = new System.Drawing.Point(0, 148);
            this.layoutControlBankAccount.Name = "layoutControlBankAccount";
            this.layoutControlBankAccount.Size = new System.Drawing.Size(512, 30);
            this.layoutControlBankAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlBankAccount.Text = "Payment from Bank Account:";
            this.layoutControlBankAccount.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.voucher;
            this.layoutControlItem5.CustomizationFormText = "Document Reference";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(372, 88);
            this.layoutControlItem5.Text = "Document Reference";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.datePayment;
            this.layoutControlItem2.CustomizationFormText = "Date:";
            this.layoutControlItem2.Location = new System.Drawing.Point(372, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 88);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(150, 88);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(373, 88);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem2.Text = "Date:";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(164, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.cmbToBankAccount;
            this.layoutControlItem1.CustomizationFormText = "Payment To Bank Account:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 178);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(512, 30);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Text = "Deposit To Bank Account:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(164, 13);
            // 
            // ReceiveFromCashierForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(755, 398);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ReceiveFromCashierForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Receive From Cashier Form";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceCharge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSlipRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServiceCharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationPayToCashier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbToBankAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBankAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentTypeSelector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCasher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CashAccountPlaceholder cmbCasher;
        private DevExpress.XtraEditors.TextEdit txtReference;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCashAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlSlipRef;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.MemoEdit txtNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlNote;
        private DevExpress.XtraEditors.LabelControl labelCashBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCashBalance;
        private DevExpress.XtraEditors.LabelControl labelBankBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlBankBalance;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validationPayToCashier;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private BIZNET.iERP.Client.BNDualCalendar datePayment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private PaymentTypeSelector paymentTypeSelector;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private BankAccountPlaceholder cmbBankAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlBankAccount;
        private DocumentSerialPlaceHolder voucher;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.TextEdit txtServiceCharge;
        private DevExpress.XtraLayout.LayoutControlItem layoutServiceCharge;
        private DevExpress.XtraEditors.TextEdit txtAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.LabelControl lblToBankBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private BankAccountPlaceholder cmbToBankAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
    }
}