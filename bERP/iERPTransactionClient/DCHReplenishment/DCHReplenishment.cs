using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHReplenishment : bERPClientDocumentHandler
    {
        public DCHReplenishment()
            : base(typeof(ReplenishmentForm))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            ReplenishmentForm f = (ReplenishmentForm)editor;
            f.LoadData((ReplenishmentDocument)doc);
        }
    }
}
