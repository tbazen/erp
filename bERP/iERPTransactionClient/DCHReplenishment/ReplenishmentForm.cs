﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;

namespace BIZNET.iERP.Client
{
    public partial class ReplenishmentForm : XtraForm
    {
        ReplenishmentDocument m_doc = null;
        CashAccount[] m_cashOnHandAccounts;
        private BankAccountInfo[] m_BankAccounts;
        private bool newDocument=true;
        private string paymentSource;
        private int m_WithdrawalAccountID;
        private double m_WithdrawalBalance;

        private IAccountingClient _client;
        private ActivationParameter _activation;
        private PaymentMethodAndBalanceController _paymentController;

        public ReplenishmentForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            voucher.setKeys(typeof(ReplenishmentDocument), "voucher");

            _client = client;
            _activation = activation;
            _paymentController = new PaymentMethodAndBalanceController(paymentTypeSelector, layoutControlBankAccount, cmbBankAccount, layoutControlBankBalance, labelBankBalance, layoutControlCashAccount, cmbCasher, layoutControlCashBalance, labelCashBalance, layoutControlServiceCharge, layoutControlSlipRef, datePayment, _activation);
            _paymentController.PaymentInformationChanged += new EventHandler(_paymentMethodController_PaymentInformationChanged);
            SetFormTitle();

            txtServiceCharge.LostFocus += Control_LostFocus;
            txtAmount.LostFocus += Control_LostFocus;
            txtReference.LostFocus += Control_LostFocus;
            InitializeValidationRules();
        }
        private void SetFormTitle()
        {
            string title = "Owner replenishment";
            lblTitle.Text = "<size=14><color=#360087><b>" + title + "</b></color></size>" +
                "<size=14><color=#360087><b>" + _paymentController.PaymentSource + "</b></color></size>";
        }
        void _paymentMethodController_PaymentInformationChanged(object sender, EventArgs e)
        {
            SetFormTitle();
        }

        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validationStaffReturn.Validate(control);
        }

        private void InitializeValidationRules()
        {
            InitializeAmountValidation();
        }

        private void InitializeAmountValidation()
        {
            NonEmptyNumericValidationRule depositAmountValidation = new NonEmptyNumericValidationRule();
            depositAmountValidation.ErrorText = "Amount cannot be blank and cannot contain a value of zero";
            depositAmountValidation.ErrorType = ErrorType.Default;
            validationStaffReturn.SetValidationRule(txtAmount, depositAmountValidation);
        }
        
       internal void LoadData(ReplenishmentDocument replenishmentDocument)
        {
            m_doc = replenishmentDocument;
            if (replenishmentDocument == null)
            {
                ResetControls();
            }
            else
            {
                _paymentController.IgnoreEvents();
                newDocument = false;

                try
                {
                    _paymentController.PaymentTypeSelector.PaymentMethod = m_doc.paymentMethod;
                    PopulateControlsForUpdate(replenishmentDocument);
                }
                finally
                {
                    _paymentController.AcceptEvents();
                }
                SetFormTitle();
            }
        }
        private void ResetControls()
        {
            datePayment.DateTime = DateTime.Now;
            txtAmount.Text = "";
            txtReference.Text = "";
            txtNote.Text = "";
            txtServiceCharge.Text = "";
            voucher.setReference(-1, null);
        }
        private void PopulateControlsForUpdate(ReplenishmentDocument replenishmentDocument)
        {
            datePayment.DateTime = replenishmentDocument.DocumentDate;
            voucher.setReference(replenishmentDocument.AccountDocumentID, replenishmentDocument.voucher);
            _paymentController.assetAccountID = replenishmentDocument.assetAccountID;
            txtAmount.Text = TSConstants.FormatBirr(replenishmentDocument.amount);
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(_paymentController.PaymentTypeSelector.PaymentMethod))
                txtServiceCharge.Text = replenishmentDocument.serviceChargeAmount.ToString("N");
            SetReferenceNumber(false);
            txtNote.Text = replenishmentDocument.ShortDescription;
        }


       private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (voucher.getReference() == null)
                {
                    MessageBox.ShowErrorMessage("Enter valid reference");
                    return;
                }
                if (validationStaffReturn.Validate())
                {
                    int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;

                    m_doc = new ReplenishmentDocument();
                    m_doc.AccountDocumentID = docID;
                    m_doc.DocumentDate = datePayment.DateTime;
                    m_doc.voucher = voucher.getReference(); 
                    m_doc.paymentMethod = _paymentController.PaymentTypeSelector.PaymentMethod;
                    m_doc.assetAccountID = _paymentController.assetAccountID;
                    m_doc.amount = double.Parse(txtAmount.Text);
                    if (PaymentMethodHelper.IsPaymentMethodWithReference(_paymentController.PaymentTypeSelector.PaymentMethod))
                    {
                        bool updateDoc = m_doc == null ? false : true;
                        SetReferenceNumber(updateDoc);
                    }
                    m_doc.ShortDescription = txtNote.Text;
                    if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(_paymentController.PaymentTypeSelector.PaymentMethod))
                    {
                        m_doc.serviceChargePayer = ServiceChargePayer.Company;
                        double serviceChargeAmount = txtServiceCharge.Text == "" ? 0 : double.Parse(txtServiceCharge.Text);
                        m_doc.serviceChargeAmount = serviceChargeAmount;
                    }
                    _client.PostGenericDocument(m_doc);
                    string message = docID == -1 ? "successfully saved!" : "successfully updated!";
                    MessageBox.ShowSuccessMessage(message);
                    Close();
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }
            
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void SetReferenceNumber(bool updateDoc)
        {
            switch (paymentTypeSelector.PaymentMethod)
            {
                case BizNetPaymentMethod.Check:
                    if (newDocument)
                        m_doc.checkNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.checkNumber;
                        else
                            m_doc.checkNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.CPOFromBankAccount:
                    if (newDocument)
                        m_doc.cpoNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.cpoNumber;
                        else
                            m_doc.cpoNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.BankTransferFromCash:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    if (newDocument) 
                        m_doc.slipReferenceNo = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.slipReferenceNo;
                        else
                            m_doc.slipReferenceNo = txtReference.Text;
                    break;
                default:
                    break;
            }
        }


        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
