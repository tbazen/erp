﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIZNET.iERP.Client
{
    class DCHSell2: bERPClientDocumentHandler
    {
        public DCHSell2()
            : base(typeof(Sell2Form<Sell2Document>))
        {
        }

        public DCHSell2(Type t)
            : base(t)
        {
        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            Sell2Form<Sell2Document> f = (Sell2Form<Sell2Document>)editor;
            f.LoadData((Sell2Document)doc);
        }
    }
}
