using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHSellPrePayment : bERPClientDocumentHandler
    {
        public DCHSellPrePayment()
            : base(typeof(SellPrePaymentForm))
        {
        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            ((SellPrePaymentForm)editor).LoadData((SellPrePaymentDocument)doc);
        }
    }

}
