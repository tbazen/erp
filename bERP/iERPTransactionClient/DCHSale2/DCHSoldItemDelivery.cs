using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHSoldItemDelivery : bERPClientDocumentHandler
    {
        public DCHSoldItemDelivery()
            : base(typeof(SoldtemDeliveryForm))
        {
        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            ((SoldtemDeliveryForm)editor).LoadData((SoldItemDeliveryDocument)doc);
        }
    }

}
