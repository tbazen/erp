﻿namespace BIZNET.iERP.Client
{
    partial class Sell2Form<SellDocumentType> where SellDocumentType : Sell2Document, new()
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.checkCollectFullAmount = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.textRetention = new DevExpress.XtraEditors.TextEdit();
            this.groupTaxation = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.textVATBase = new DevExpress.XtraEditors.TextEdit();
            this.textWHBase = new DevExpress.XtraEditors.TextEdit();
            this.checkFullVAT = new DevExpress.XtraEditors.CheckEdit();
            this.checkFullWH = new DevExpress.XtraEditors.CheckEdit();
            this.textWHTNo = new DevExpress.XtraEditors.TextEdit();
            this.textWHVNo = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutVATBase = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutFullVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutWHBase = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutFullWH = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutWHTRNo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutVATWH = new DevExpress.XtraLayout.LayoutControlItem();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.paymentTypeSelector = new BIZNET.iERP.Client.PaymentTypeSelector();
            this.cashAccountPlaceholder = new BIZNET.iERP.Client.CashAccountPlaceholder();
            this.checFullCredit = new DevExpress.XtraEditors.CheckEdit();
            this.labelCashBalance = new DevExpress.XtraEditors.LabelControl();
            this.memoNote = new DevExpress.XtraEditors.MemoEdit();
            this.textTransferReference = new DevExpress.XtraEditors.TextEdit();
            this.textCredit = new DevExpress.XtraEditors.TextEdit();
            this.bankAccountPlaceholder = new BIZNET.iERP.Client.BankAccountPlaceholder();
            this.checkSettleMaxiumAdvance = new DevExpress.XtraEditors.CheckEdit();
            this.labelBankBalance = new DevExpress.XtraEditors.LabelControl();
            this.textSettleAdvancePayment = new DevExpress.XtraEditors.TextEdit();
            this.textServiceCharge = new DevExpress.XtraEditors.TextEdit();
            this.checkRelationSettled = new DevExpress.XtraEditors.CheckEdit();
            this.textCollectedCash = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutServiceCharge = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutServiceChargeBy = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutSettleAdvance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutSettleMaxAdvance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTaxation = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCredit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutFullCredit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCashAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCashBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBankAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBankBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCollectedCash = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCollectFullAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRetention = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTransferRefernce = new DevExpress.XtraLayout.LayoutControlItem();
            this.itemSummaryGrid = new BIZNET.iERP.Client.ItemSummaryGrid();
            this.gridViewSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.buttonPrev = new DevExpress.XtraEditors.SimpleButton();
            this.buttonNext = new DevExpress.XtraEditors.SimpleButton();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonCredit = new DevExpress.XtraEditors.SimpleButton();
            this.buttonPrepayments = new DevExpress.XtraEditors.SimpleButton();
            this.buttonDeliveries = new DevExpress.XtraEditors.SimpleButton();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.buttonPrintAttachment = new DevExpress.XtraEditors.SimpleButton();
            this.labelPaymentInstruction = new DevExpress.XtraEditors.LabelControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.linkEditSupplier = new DevExpress.XtraEditors.HyperLinkEdit();
            this.itemGrid = new BIZNET.iERP.Client.ItemGrid();
            this.gridViewItems = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.voucher = new BIZNET.iERP.Client.DocumentSerialPlaceHolder();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateTransaction = new BIZNET.iERP.Client.BNDualCalendar();
            this.labelCustomerInformation = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutRelationInformation = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dxValidationProvider = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.dxValidationProviderWH = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.dxValidationProviderWHV = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            ((System.ComponentModel.ISupportInitialize)(this.checkCollectFullAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textRetention.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupTaxation)).BeginInit();
            this.groupTaxation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textVATBase.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWHBase.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkFullVAT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkFullWH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWHTNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWHVNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVATBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFullVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWHBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFullWH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWHTRNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVATWH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentTypeSelector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cashAccountPlaceholder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checFullCredit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textTransferReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCredit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bankAccountPlaceholder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSettleMaxiumAdvance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSettleAdvancePayment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textServiceCharge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkRelationSettled.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCollectedCash.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServiceCharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServiceChargeBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSettleAdvance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSettleMaxAdvance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTaxation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFullCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCashAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCashBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBankBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCollectedCash)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCollectFullAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRetention)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTransferRefernce)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemSummaryGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.linkEditSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRelationInformation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProviderWH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProviderWHV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkCollectFullAmount
            // 
            this.checkCollectFullAmount.Location = new System.Drawing.Point(592, 170);
            this.checkCollectFullAmount.Name = "checkCollectFullAmount";
            this.checkCollectFullAmount.Properties.Caption = "Collect Full Amount";
            this.checkCollectFullAmount.Size = new System.Drawing.Size(199, 19);
            this.checkCollectFullAmount.StyleController = this.layoutControl3;
            this.checkCollectFullAmount.TabIndex = 33;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.textRetention);
            this.layoutControl3.Controls.Add(this.groupTaxation);
            this.layoutControl3.Controls.Add(this.labelControl3);
            this.layoutControl3.Controls.Add(this.paymentTypeSelector);
            this.layoutControl3.Controls.Add(this.cashAccountPlaceholder);
            this.layoutControl3.Controls.Add(this.checFullCredit);
            this.layoutControl3.Controls.Add(this.labelCashBalance);
            this.layoutControl3.Controls.Add(this.memoNote);
            this.layoutControl3.Controls.Add(this.textTransferReference);
            this.layoutControl3.Controls.Add(this.textCredit);
            this.layoutControl3.Controls.Add(this.checkCollectFullAmount);
            this.layoutControl3.Controls.Add(this.bankAccountPlaceholder);
            this.layoutControl3.Controls.Add(this.checkSettleMaxiumAdvance);
            this.layoutControl3.Controls.Add(this.labelBankBalance);
            this.layoutControl3.Controls.Add(this.textSettleAdvancePayment);
            this.layoutControl3.Controls.Add(this.textServiceCharge);
            this.layoutControl3.Controls.Add(this.checkRelationSettled);
            this.layoutControl3.Controls.Add(this.textCollectedCash);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup4;
            this.layoutControl3.Size = new System.Drawing.Size(805, 344);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // textRetention
            // 
            this.textRetention.Location = new System.Drawing.Point(157, 170);
            this.textRetention.Name = "textRetention";
            this.textRetention.Properties.Mask.EditMask = "#,########0.00;";
            this.textRetention.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textRetention.Size = new System.Drawing.Size(153, 20);
            this.textRetention.StyleController = this.layoutControl3;
            this.textRetention.TabIndex = 43;
            this.textRetention.EditValueChanged += new System.EventHandler(this.textRetention_EditValueChanged);
            // 
            // groupTaxation
            // 
            this.groupTaxation.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupTaxation.AppearanceCaption.Options.UseFont = true;
            this.groupTaxation.Controls.Add(this.layoutControl4);
            this.groupTaxation.Location = new System.Drawing.Point(14, 40);
            this.groupTaxation.Name = "groupTaxation";
            this.groupTaxation.Size = new System.Drawing.Size(777, 94);
            this.groupTaxation.TabIndex = 42;
            this.groupTaxation.Text = "Taxation";
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.textVATBase);
            this.layoutControl4.Controls.Add(this.textWHBase);
            this.layoutControl4.Controls.Add(this.checkFullVAT);
            this.layoutControl4.Controls.Add(this.checkFullWH);
            this.layoutControl4.Controls.Add(this.textWHTNo);
            this.layoutControl4.Controls.Add(this.textWHVNo);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl4.Location = new System.Drawing.Point(2, 21);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.Root = this.layoutControlGroup5;
            this.layoutControl4.Size = new System.Drawing.Size(773, 71);
            this.layoutControl4.TabIndex = 0;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // textVATBase
            // 
            this.textVATBase.Location = new System.Drawing.Point(154, 14);
            this.textVATBase.Name = "textVATBase";
            this.textVATBase.Properties.Mask.EditMask = "#,########0.00;";
            this.textVATBase.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textVATBase.Size = new System.Drawing.Size(106, 20);
            this.textVATBase.StyleController = this.layoutControl4;
            this.textVATBase.TabIndex = 35;
            // 
            // textWHBase
            // 
            this.textWHBase.Location = new System.Drawing.Point(532, 12);
            this.textWHBase.Name = "textWHBase";
            this.textWHBase.Properties.Mask.EditMask = "#,########0.00;";
            this.textWHBase.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textWHBase.Size = new System.Drawing.Size(103, 20);
            this.textWHBase.StyleController = this.layoutControl4;
            this.textWHBase.TabIndex = 38;
            // 
            // checkFullVAT
            // 
            this.checkFullVAT.Location = new System.Drawing.Point(268, 14);
            this.checkFullVAT.Name = "checkFullVAT";
            this.checkFullVAT.Properties.Caption = "Full Invoice";
            this.checkFullVAT.Size = new System.Drawing.Size(118, 19);
            this.checkFullVAT.StyleController = this.layoutControl4;
            this.checkFullVAT.TabIndex = 36;
            // 
            // checkFullWH
            // 
            this.checkFullWH.Location = new System.Drawing.Point(641, 14);
            this.checkFullWH.Name = "checkFullWH";
            this.checkFullWH.Properties.Caption = "Full Invoice";
            this.checkFullWH.Size = new System.Drawing.Size(118, 19);
            this.checkFullWH.StyleController = this.layoutControl4;
            this.checkFullWH.TabIndex = 39;
            // 
            // textWHTNo
            // 
            this.textWHTNo.Location = new System.Drawing.Point(534, 41);
            this.textWHTNo.Name = "textWHTNo";
            this.textWHTNo.Size = new System.Drawing.Size(225, 20);
            this.textWHTNo.StyleController = this.layoutControl4;
            this.textWHTNo.TabIndex = 30;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "This value is not valid";
            this.dxValidationProviderWH.SetValidationRule(this.textWHTNo, conditionValidationRule1);
            // 
            // textWHVNo
            // 
            this.textWHVNo.Location = new System.Drawing.Point(154, 42);
            this.textWHVNo.Name = "textWHVNo";
            this.textWHVNo.Size = new System.Drawing.Size(232, 20);
            this.textWHVNo.StyleController = this.layoutControl4;
            this.textWHVNo.TabIndex = 31;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "This value is not valid";
            this.dxValidationProviderWHV.SetValidationRule(this.textWHVNo, conditionValidationRule2);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutVATBase,
            this.layoutFullVAT,
            this.layoutWHBase,
            this.layoutFullWH,
            this.layoutWHTRNo,
            this.layoutVATWH});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup1";
            this.layoutControlGroup5.Size = new System.Drawing.Size(773, 76);
            this.layoutControlGroup5.Text = "layoutControlGroup1";
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutVATBase
            // 
            this.layoutVATBase.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutVATBase.AppearanceItemCaption.Options.UseFont = true;
            this.layoutVATBase.Control = this.textVATBase;
            this.layoutVATBase.CustomizationFormText = "VAT Base";
            this.layoutVATBase.Location = new System.Drawing.Point(0, 0);
            this.layoutVATBase.Name = "layoutVATBase";
            this.layoutVATBase.Size = new System.Drawing.Size(254, 28);
            this.layoutVATBase.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutVATBase.Text = "VAT Base";
            this.layoutVATBase.TextSize = new System.Drawing.Size(137, 13);
            // 
            // layoutFullVAT
            // 
            this.layoutFullVAT.Control = this.checkFullVAT;
            this.layoutFullVAT.CustomizationFormText = "layoutControlItem17";
            this.layoutFullVAT.Location = new System.Drawing.Point(254, 0);
            this.layoutFullVAT.Name = "layoutFullVAT";
            this.layoutFullVAT.Size = new System.Drawing.Size(126, 28);
            this.layoutFullVAT.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutFullVAT.Text = "layoutFullVAT";
            this.layoutFullVAT.TextSize = new System.Drawing.Size(0, 0);
            this.layoutFullVAT.TextToControlDistance = 0;
            this.layoutFullVAT.TextVisible = false;
            // 
            // layoutWHBase
            // 
            this.layoutWHBase.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutWHBase.AppearanceItemCaption.Options.UseFont = true;
            this.layoutWHBase.Control = this.textWHBase;
            this.layoutWHBase.CustomizationFormText = "Witholding Base";
            this.layoutWHBase.Location = new System.Drawing.Point(380, 0);
            this.layoutWHBase.Name = "layoutWHBase";
            this.layoutWHBase.Size = new System.Drawing.Size(247, 27);
            this.layoutWHBase.Text = "Witholding Base";
            this.layoutWHBase.TextSize = new System.Drawing.Size(137, 13);
            // 
            // layoutFullWH
            // 
            this.layoutFullWH.Control = this.checkFullWH;
            this.layoutFullWH.CustomizationFormText = "layoutControlItem19";
            this.layoutFullWH.Location = new System.Drawing.Point(627, 0);
            this.layoutFullWH.Name = "layoutFullWH";
            this.layoutFullWH.Size = new System.Drawing.Size(126, 27);
            this.layoutFullWH.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutFullWH.Text = "layoutFullWH";
            this.layoutFullWH.TextSize = new System.Drawing.Size(0, 0);
            this.layoutFullWH.TextToControlDistance = 0;
            this.layoutFullWH.TextVisible = false;
            // 
            // layoutWHTRNo
            // 
            this.layoutWHTRNo.Control = this.textWHTNo;
            this.layoutWHTRNo.CustomizationFormText = "Witholding Receipt No.";
            this.layoutWHTRNo.Location = new System.Drawing.Point(380, 27);
            this.layoutWHTRNo.Name = "layoutWHTRNo";
            this.layoutWHTRNo.Size = new System.Drawing.Size(373, 29);
            this.layoutWHTRNo.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutWHTRNo.Text = "Witholding Receipt No.";
            this.layoutWHTRNo.TextSize = new System.Drawing.Size(137, 13);
            // 
            // layoutVATWH
            // 
            this.layoutVATWH.Control = this.textWHVNo;
            this.layoutVATWH.CustomizationFormText = "VAT Withhodling Receipt No.";
            this.layoutVATWH.Location = new System.Drawing.Point(0, 28);
            this.layoutVATWH.Name = "layoutVATWH";
            this.layoutVATWH.Size = new System.Drawing.Size(380, 28);
            this.layoutVATWH.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutVATWH.Text = "VAT Withhodling Receipt No.";
            this.layoutVATWH.TextSize = new System.Drawing.Size(137, 13);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.CornflowerBlue;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(12, 12);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(781, 22);
            this.labelControl3.StyleController = this.layoutControl3;
            this.labelControl3.TabIndex = 35;
            this.labelControl3.Text = "Taxation, Payment && Related Transactions";
            // 
            // paymentTypeSelector
            // 
            this.paymentTypeSelector.Include_BankTransferFromAccount = true;
            this.paymentTypeSelector.Include_BankTransferFromCash = true;
            this.paymentTypeSelector.Include_Cash = true;
            this.paymentTypeSelector.Include_Check = true;
            this.paymentTypeSelector.Include_CpoFromBank = true;
            this.paymentTypeSelector.Include_CpoFromCash = true;
            this.paymentTypeSelector.Include_Credit = true;
            this.paymentTypeSelector.Location = new System.Drawing.Point(157, 198);
            this.paymentTypeSelector.Name = "paymentTypeSelector";
            this.paymentTypeSelector.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.paymentTypeSelector.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.paymentTypeSelector.Size = new System.Drawing.Size(179, 20);
            this.paymentTypeSelector.StyleController = this.layoutControl3;
            this.paymentTypeSelector.TabIndex = 4;
            // 
            // cashAccountPlaceholder
            // 
            this.cashAccountPlaceholder.IncludeExpenseAdvance = true;
            this.cashAccountPlaceholder.Location = new System.Drawing.Point(487, 198);
            this.cashAccountPlaceholder.Name = "cashAccountPlaceholder";
            this.cashAccountPlaceholder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cashAccountPlaceholder.Properties.ImmediatePopup = true;
            this.cashAccountPlaceholder.Size = new System.Drawing.Size(107, 20);
            this.cashAccountPlaceholder.StyleController = this.layoutControl3;
            this.cashAccountPlaceholder.TabIndex = 6;
            // 
            // checFullCredit
            // 
            this.checFullCredit.Location = new System.Drawing.Point(720, 142);
            this.checFullCredit.Name = "checFullCredit";
            this.checFullCredit.Properties.Caption = "Full Credit";
            this.checFullCredit.Size = new System.Drawing.Size(71, 19);
            this.checFullCredit.StyleController = this.layoutControl3;
            this.checFullCredit.TabIndex = 16;
            this.checFullCredit.CheckedChanged += new System.EventHandler(this.checAllCredit_CheckedChanged);
            // 
            // labelCashBalance
            // 
            this.labelCashBalance.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelCashBalance.Location = new System.Drawing.Point(602, 198);
            this.labelCashBalance.Name = "labelCashBalance";
            this.labelCashBalance.Size = new System.Drawing.Size(189, 13);
            this.labelCashBalance.StyleController = this.layoutControl3;
            this.labelCashBalance.TabIndex = 27;
            this.labelCashBalance.Text = "###";
            // 
            // memoNote
            // 
            this.memoNote.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoNote.Location = new System.Drawing.Point(12, 296);
            this.memoNote.Name = "memoNote";
            this.memoNote.Size = new System.Drawing.Size(781, 36);
            this.memoNote.StyleController = this.layoutControl3;
            this.memoNote.TabIndex = 17;
            // 
            // textTransferReference
            // 
            this.textTransferReference.Location = new System.Drawing.Point(157, 254);
            this.textTransferReference.Name = "textTransferReference";
            this.textTransferReference.Size = new System.Drawing.Size(168, 20);
            this.textTransferReference.StyleController = this.layoutControl3;
            this.textTransferReference.TabIndex = 10;
            // 
            // textCredit
            // 
            this.textCredit.Location = new System.Drawing.Point(616, 142);
            this.textCredit.Name = "textCredit";
            this.textCredit.Properties.Mask.EditMask = "#,########0.00;";
            this.textCredit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textCredit.Size = new System.Drawing.Size(96, 20);
            this.textCredit.StyleController = this.layoutControl3;
            this.textCredit.TabIndex = 15;
            // 
            // bankAccountPlaceholder
            // 
            this.bankAccountPlaceholder.Location = new System.Drawing.Point(487, 226);
            this.bankAccountPlaceholder.Name = "bankAccountPlaceholder";
            this.bankAccountPlaceholder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.bankAccountPlaceholder.Properties.ImmediatePopup = true;
            this.bankAccountPlaceholder.Size = new System.Drawing.Size(107, 20);
            this.bankAccountPlaceholder.StyleController = this.layoutControl3;
            this.bankAccountPlaceholder.TabIndex = 7;
            // 
            // checkSettleMaxiumAdvance
            // 
            this.checkSettleMaxiumAdvance.Location = new System.Drawing.Point(283, 142);
            this.checkSettleMaxiumAdvance.Name = "checkSettleMaxiumAdvance";
            this.checkSettleMaxiumAdvance.Properties.Caption = "Settle Maximum Amount";
            this.checkSettleMaxiumAdvance.Size = new System.Drawing.Size(182, 19);
            this.checkSettleMaxiumAdvance.StyleController = this.layoutControl3;
            this.checkSettleMaxiumAdvance.TabIndex = 14;
            // 
            // labelBankBalance
            // 
            this.labelBankBalance.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelBankBalance.Location = new System.Drawing.Point(602, 226);
            this.labelBankBalance.Name = "labelBankBalance";
            this.labelBankBalance.Size = new System.Drawing.Size(189, 13);
            this.labelBankBalance.StyleController = this.layoutControl3;
            this.labelBankBalance.TabIndex = 28;
            this.labelBankBalance.Text = "###";
            // 
            // textSettleAdvancePayment
            // 
            this.textSettleAdvancePayment.Location = new System.Drawing.Point(157, 142);
            this.textSettleAdvancePayment.Name = "textSettleAdvancePayment";
            this.textSettleAdvancePayment.Properties.Mask.EditMask = "#,########0.00;";
            this.textSettleAdvancePayment.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textSettleAdvancePayment.Size = new System.Drawing.Size(118, 20);
            this.textSettleAdvancePayment.StyleController = this.layoutControl3;
            this.textSettleAdvancePayment.TabIndex = 13;
            this.textSettleAdvancePayment.EditValueChanged += new System.EventHandler(this.textSettleAdvancePayment_EditValueChanged);
            // 
            // textServiceCharge
            // 
            this.textServiceCharge.Location = new System.Drawing.Point(476, 254);
            this.textServiceCharge.Name = "textServiceCharge";
            this.textServiceCharge.Properties.Mask.EditMask = "#,########0.00;";
            this.textServiceCharge.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textServiceCharge.Size = new System.Drawing.Size(174, 20);
            this.textServiceCharge.StyleController = this.layoutControl3;
            this.textServiceCharge.TabIndex = 11;
            // 
            // checkRelationSettled
            // 
            this.checkRelationSettled.Location = new System.Drawing.Point(658, 254);
            this.checkRelationSettled.Name = "checkRelationSettled";
            this.checkRelationSettled.Properties.Caption = "Paid by Customer";
            this.checkRelationSettled.Size = new System.Drawing.Size(133, 19);
            this.checkRelationSettled.StyleController = this.layoutControl3;
            this.checkRelationSettled.TabIndex = 12;
            // 
            // textCollectedCash
            // 
            this.textCollectedCash.Location = new System.Drawing.Point(461, 170);
            this.textCollectedCash.Name = "textCollectedCash";
            this.textCollectedCash.Properties.Mask.EditMask = "#,########0.00;";
            this.textCollectedCash.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textCollectedCash.Size = new System.Drawing.Size(123, 20);
            this.textCollectedCash.StyleController = this.layoutControl3;
            this.textCollectedCash.TabIndex = 32;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem16,
            this.layoutServiceCharge,
            this.layoutServiceChargeBy,
            this.layoutSettleAdvance,
            this.layoutSettleMaxAdvance,
            this.layoutTaxation,
            this.layoutCredit,
            this.layoutFullCredit,
            this.layoutCashAccount,
            this.layoutCashBalance,
            this.layoutBankAccount,
            this.layoutBankBalance,
            this.layoutCollectedCash,
            this.layoutCollectFullAmount,
            this.layoutRetention,
            this.layoutControlItem12,
            this.layoutTransferRefernce});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(805, 344);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.labelControl3;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(785, 26);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.Control = this.paymentTypeSelector;
            this.layoutControlItem16.CustomizationFormText = "Payment Method";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 184);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(330, 56);
            this.layoutControlItem16.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem16.Text = "Payment Method";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutServiceCharge
            // 
            this.layoutServiceCharge.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutServiceCharge.AppearanceItemCaption.Options.UseFont = true;
            this.layoutServiceCharge.Control = this.textServiceCharge;
            this.layoutServiceCharge.CustomizationFormText = "Bank Service Charge";
            this.layoutServiceCharge.Location = new System.Drawing.Point(319, 240);
            this.layoutServiceCharge.Name = "layoutServiceCharge";
            this.layoutServiceCharge.Size = new System.Drawing.Size(325, 28);
            this.layoutServiceCharge.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutServiceCharge.Text = "Bank Service Charge";
            this.layoutServiceCharge.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutServiceChargeBy
            // 
            this.layoutServiceChargeBy.Control = this.checkRelationSettled;
            this.layoutServiceChargeBy.CustomizationFormText = "layoutServiceChargeBy";
            this.layoutServiceChargeBy.Location = new System.Drawing.Point(644, 240);
            this.layoutServiceChargeBy.Name = "layoutServiceChargeBy";
            this.layoutServiceChargeBy.Size = new System.Drawing.Size(141, 28);
            this.layoutServiceChargeBy.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutServiceChargeBy.Text = "layoutServiceChargeBy";
            this.layoutServiceChargeBy.TextSize = new System.Drawing.Size(0, 0);
            this.layoutServiceChargeBy.TextToControlDistance = 0;
            this.layoutServiceChargeBy.TextVisible = false;
            // 
            // layoutSettleAdvance
            // 
            this.layoutSettleAdvance.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutSettleAdvance.AppearanceItemCaption.Options.UseFont = true;
            this.layoutSettleAdvance.Control = this.textSettleAdvancePayment;
            this.layoutSettleAdvance.CustomizationFormText = "Settle Advance Payment";
            this.layoutSettleAdvance.Location = new System.Drawing.Point(0, 128);
            this.layoutSettleAdvance.Name = "layoutSettleAdvance";
            this.layoutSettleAdvance.Size = new System.Drawing.Size(269, 28);
            this.layoutSettleAdvance.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutSettleAdvance.Text = "Settle Advance Payment";
            this.layoutSettleAdvance.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutSettleMaxAdvance
            // 
            this.layoutSettleMaxAdvance.Control = this.checkSettleMaxiumAdvance;
            this.layoutSettleMaxAdvance.CustomizationFormText = "layoutControlItem8";
            this.layoutSettleMaxAdvance.Location = new System.Drawing.Point(269, 128);
            this.layoutSettleMaxAdvance.Name = "layoutSettleMaxAdvance";
            this.layoutSettleMaxAdvance.Size = new System.Drawing.Size(190, 28);
            this.layoutSettleMaxAdvance.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutSettleMaxAdvance.Text = "layoutSettleMaxAdvance";
            this.layoutSettleMaxAdvance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutSettleMaxAdvance.TextToControlDistance = 0;
            this.layoutSettleMaxAdvance.TextVisible = false;
            // 
            // layoutTaxation
            // 
            this.layoutTaxation.Control = this.groupTaxation;
            this.layoutTaxation.CustomizationFormText = "layoutControlItem24";
            this.layoutTaxation.Location = new System.Drawing.Point(0, 26);
            this.layoutTaxation.MinSize = new System.Drawing.Size(104, 24);
            this.layoutTaxation.Name = "layoutTaxation";
            this.layoutTaxation.Size = new System.Drawing.Size(785, 102);
            this.layoutTaxation.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutTaxation.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutTaxation.Text = "layoutTaxation";
            this.layoutTaxation.TextSize = new System.Drawing.Size(0, 0);
            this.layoutTaxation.TextToControlDistance = 0;
            this.layoutTaxation.TextVisible = false;
            // 
            // layoutCredit
            // 
            this.layoutCredit.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutCredit.AppearanceItemCaption.Options.UseFont = true;
            this.layoutCredit.Control = this.textCredit;
            this.layoutCredit.CustomizationFormText = "Credit";
            this.layoutCredit.Location = new System.Drawing.Point(459, 128);
            this.layoutCredit.Name = "layoutCredit";
            this.layoutCredit.Size = new System.Drawing.Size(247, 28);
            this.layoutCredit.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutCredit.Text = "Customer Credit";
            this.layoutCredit.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutFullCredit
            // 
            this.layoutFullCredit.Control = this.checFullCredit;
            this.layoutFullCredit.CustomizationFormText = "layoutControlItem11";
            this.layoutFullCredit.Location = new System.Drawing.Point(706, 128);
            this.layoutFullCredit.Name = "layoutFullCredit";
            this.layoutFullCredit.Size = new System.Drawing.Size(79, 28);
            this.layoutFullCredit.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutFullCredit.Text = "layoutFullCredit";
            this.layoutFullCredit.TextSize = new System.Drawing.Size(0, 0);
            this.layoutFullCredit.TextToControlDistance = 0;
            this.layoutFullCredit.TextVisible = false;
            // 
            // layoutCashAccount
            // 
            this.layoutCashAccount.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutCashAccount.AppearanceItemCaption.Options.UseFont = true;
            this.layoutCashAccount.Control = this.cashAccountPlaceholder;
            this.layoutCashAccount.CustomizationFormText = "Cash Account";
            this.layoutCashAccount.Location = new System.Drawing.Point(330, 184);
            this.layoutCashAccount.Name = "layoutCashAccount";
            this.layoutCashAccount.Size = new System.Drawing.Size(258, 28);
            this.layoutCashAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutCashAccount.Text = "Cash Account";
            this.layoutCashAccount.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutCashBalance
            // 
            this.layoutCashBalance.Control = this.labelCashBalance;
            this.layoutCashBalance.CustomizationFormText = "layoutCashBalance";
            this.layoutCashBalance.Location = new System.Drawing.Point(588, 184);
            this.layoutCashBalance.Name = "layoutCashBalance";
            this.layoutCashBalance.Size = new System.Drawing.Size(197, 28);
            this.layoutCashBalance.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutCashBalance.Text = "layoutCashBalance";
            this.layoutCashBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutCashBalance.TextToControlDistance = 0;
            this.layoutCashBalance.TextVisible = false;
            // 
            // layoutBankAccount
            // 
            this.layoutBankAccount.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutBankAccount.AppearanceItemCaption.Options.UseFont = true;
            this.layoutBankAccount.Control = this.bankAccountPlaceholder;
            this.layoutBankAccount.CustomizationFormText = "Bank Account";
            this.layoutBankAccount.Location = new System.Drawing.Point(330, 212);
            this.layoutBankAccount.Name = "layoutBankAccount";
            this.layoutBankAccount.Size = new System.Drawing.Size(258, 28);
            this.layoutBankAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutBankAccount.Text = "Bank Account";
            this.layoutBankAccount.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutBankBalance
            // 
            this.layoutBankBalance.Control = this.labelBankBalance;
            this.layoutBankBalance.CustomizationFormText = "layoutControlItem19";
            this.layoutBankBalance.Location = new System.Drawing.Point(588, 212);
            this.layoutBankBalance.Name = "layoutBankBalance";
            this.layoutBankBalance.Size = new System.Drawing.Size(197, 28);
            this.layoutBankBalance.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutBankBalance.Text = "layoutBankBalance";
            this.layoutBankBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutBankBalance.TextToControlDistance = 0;
            this.layoutBankBalance.TextVisible = false;
            // 
            // layoutCollectedCash
            // 
            this.layoutCollectedCash.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutCollectedCash.AppearanceItemCaption.Options.UseFont = true;
            this.layoutCollectedCash.Control = this.textCollectedCash;
            this.layoutCollectedCash.CustomizationFormText = "Collected Cash";
            this.layoutCollectedCash.Location = new System.Drawing.Point(304, 156);
            this.layoutCollectedCash.Name = "layoutCollectedCash";
            this.layoutCollectedCash.Size = new System.Drawing.Size(274, 28);
            this.layoutCollectedCash.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutCollectedCash.Text = "Collected Cash";
            this.layoutCollectedCash.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutCollectFullAmount
            // 
            this.layoutCollectFullAmount.Control = this.checkCollectFullAmount;
            this.layoutCollectFullAmount.CustomizationFormText = "layoutControlItem6";
            this.layoutCollectFullAmount.Location = new System.Drawing.Point(578, 156);
            this.layoutCollectFullAmount.Name = "layoutCollectFullAmount";
            this.layoutCollectFullAmount.Size = new System.Drawing.Size(207, 28);
            this.layoutCollectFullAmount.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutCollectFullAmount.Text = "layoutCollectFullAmount";
            this.layoutCollectFullAmount.TextSize = new System.Drawing.Size(0, 0);
            this.layoutCollectFullAmount.TextToControlDistance = 0;
            this.layoutCollectFullAmount.TextVisible = false;
            // 
            // layoutRetention
            // 
            this.layoutRetention.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutRetention.AppearanceItemCaption.Options.UseFont = true;
            this.layoutRetention.Control = this.textRetention;
            this.layoutRetention.CustomizationFormText = "Retintion";
            this.layoutRetention.Location = new System.Drawing.Point(0, 156);
            this.layoutRetention.Name = "layoutRetention";
            this.layoutRetention.Size = new System.Drawing.Size(304, 28);
            this.layoutRetention.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutRetention.Text = "Retention";
            this.layoutRetention.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.memoNote;
            this.layoutControlItem12.CustomizationFormText = "Additional Notes";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 268);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(122, 36);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(785, 56);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "Additional Notes";
            this.layoutControlItem12.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutTransferRefernce
            // 
            this.layoutTransferRefernce.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutTransferRefernce.AppearanceItemCaption.Options.UseFont = true;
            this.layoutTransferRefernce.Control = this.textTransferReference;
            this.layoutTransferRefernce.CustomizationFormText = "Transfer Refernce";
            this.layoutTransferRefernce.Location = new System.Drawing.Point(0, 240);
            this.layoutTransferRefernce.Name = "layoutTransferRefernce";
            this.layoutTransferRefernce.Size = new System.Drawing.Size(319, 28);
            this.layoutTransferRefernce.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutTransferRefernce.Text = "Transfer Refernce";
            this.layoutTransferRefernce.TextSize = new System.Drawing.Size(140, 13);
            // 
            // itemSummaryGrid
            // 
            this.itemSummaryGrid.Location = new System.Drawing.Point(23, 10);
            this.itemSummaryGrid.MainView = this.gridViewSummary;
            this.itemSummaryGrid.Name = "itemSummaryGrid";
            this.itemSummaryGrid.Size = new System.Drawing.Size(279, 137);
            this.itemSummaryGrid.TabIndex = 5;
            this.itemSummaryGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSummary});
            // 
            // gridViewSummary
            // 
            this.gridViewSummary.GridControl = this.itemSummaryGrid;
            this.gridViewSummary.Name = "gridViewSummary";
            this.gridViewSummary.OptionsView.ShowGroupPanel = false;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.buttonPrev);
            this.panelControl1.Controls.Add(this.buttonNext);
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Controls.Add(this.buttonCancel);
            this.panelControl1.Controls.Add(this.buttonPrintAttachment);
            this.panelControl1.Controls.Add(this.labelPaymentInstruction);
            this.panelControl1.Controls.Add(this.itemSummaryGrid);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 350);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(833, 184);
            this.panelControl1.TabIndex = 18;
            // 
            // buttonPrev
            // 
            this.buttonPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrev.Location = new System.Drawing.Point(587, 156);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(80, 23);
            this.buttonPrev.TabIndex = 22;
            this.buttonPrev.Text = "<<Previous";
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNext.Location = new System.Drawing.Point(673, 156);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(80, 23);
            this.buttonNext.TabIndex = 21;
            this.buttonNext.Text = "Next>>";
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.buttonCredit);
            this.flowLayoutPanel1.Controls.Add(this.buttonPrepayments);
            this.flowLayoutPanel1.Controls.Add(this.buttonDeliveries);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(532, 56);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(296, 56);
            this.flowLayoutPanel1.TabIndex = 20;
            // 
            // buttonCredit
            // 
            this.buttonCredit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCredit.Location = new System.Drawing.Point(170, 3);
            this.buttonCredit.Name = "buttonCredit";
            this.buttonCredit.Size = new System.Drawing.Size(123, 23);
            this.buttonCredit.TabIndex = 5;
            this.buttonCredit.Text = "&Credit Settlement";
            this.buttonCredit.Visible = false;
            // 
            // buttonPrepayments
            // 
            this.buttonPrepayments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonPrepayments.Location = new System.Drawing.Point(41, 3);
            this.buttonPrepayments.Name = "buttonPrepayments";
            this.buttonPrepayments.Size = new System.Drawing.Size(123, 23);
            this.buttonPrepayments.TabIndex = 5;
            this.buttonPrepayments.Text = "&Schedule Earning";
            this.buttonPrepayments.Visible = false;
            // 
            // buttonDeliveries
            // 
            this.buttonDeliveries.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDeliveries.Location = new System.Drawing.Point(170, 32);
            this.buttonDeliveries.Name = "buttonDeliveries";
            this.buttonDeliveries.Size = new System.Drawing.Size(123, 23);
            this.buttonDeliveries.TabIndex = 6;
            this.buttonDeliveries.Text = "&Delivery";
            this.buttonDeliveries.Visible = false;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(765, 156);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(59, 23);
            this.buttonCancel.TabIndex = 6;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonPrintAttachment
            // 
            this.buttonPrintAttachment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrintAttachment.Location = new System.Drawing.Point(673, 118);
            this.buttonPrintAttachment.Name = "buttonPrintAttachment";
            this.buttonPrintAttachment.Size = new System.Drawing.Size(155, 23);
            this.buttonPrintAttachment.TabIndex = 5;
            this.buttonPrintAttachment.Text = "&Print Attachment";
            this.buttonPrintAttachment.Click += new System.EventHandler(this.buttonPrintAttachment_Click);
            // 
            // labelPaymentInstruction
            // 
            this.labelPaymentInstruction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPaymentInstruction.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelPaymentInstruction.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelPaymentInstruction.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.labelPaymentInstruction.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelPaymentInstruction.Location = new System.Drawing.Point(314, 10);
            this.labelPaymentInstruction.Name = "labelPaymentInstruction";
            this.labelPaymentInstruction.Padding = new System.Windows.Forms.Padding(2);
            this.labelPaymentInstruction.Size = new System.Drawing.Size(510, 40);
            this.labelPaymentInstruction.TabIndex = 19;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.linkEditSupplier);
            this.layoutControl2.Controls.Add(this.itemGrid);
            this.layoutControl2.Controls.Add(this.voucher);
            this.layoutControl2.Controls.Add(this.labelControl1);
            this.layoutControl2.Controls.Add(this.dateTransaction);
            this.layoutControl2.Controls.Add(this.labelCustomerInformation);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup3;
            this.layoutControl2.Size = new System.Drawing.Size(805, 344);
            this.layoutControl2.TabIndex = 35;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // linkEditSupplier
            // 
            this.linkEditSupplier.Location = new System.Drawing.Point(406, 123);
            this.linkEditSupplier.Name = "linkEditSupplier";
            this.linkEditSupplier.Size = new System.Drawing.Size(385, 20);
            this.linkEditSupplier.StyleController = this.layoutControl2;
            this.linkEditSupplier.TabIndex = 36;
            this.linkEditSupplier.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.linkEditSupplier_OpenLink);
            // 
            // itemGrid
            // 
            this.itemGrid.AllowPriceEdit = false;
            this.itemGrid.AllowRepeatedItem = false;
            this.itemGrid.AllowUnitPriceEdit = true;
            this.itemGrid.AutoUnitPrice = true;
            this.itemGrid.CurrentBalanceCaption = "Current Balance";
            this.itemGrid.DirectExpenseCaption = "Direct Issue?";
            this.itemGrid.GroupByCostCenter = false;
            this.itemGrid.Location = new System.Drawing.Point(12, 180);
            this.itemGrid.MainView = this.gridViewItems;
            this.itemGrid.Name = "itemGrid";
            this.itemGrid.Padding = new System.Windows.Forms.Padding(3);
            this.itemGrid.PrepaidCaption = "Unearned";
            this.itemGrid.PriceCaption = "Price";
            this.itemGrid.QuantityCaption = "Quantity";
            this.itemGrid.ReadOnly = false;
            this.itemGrid.ShowCostCenterColumn = true;
            this.itemGrid.ShowCurrentBalanceColumn = true;
            this.itemGrid.ShowDirectExpenseColumn = false;
            this.itemGrid.ShowPrepaid = true;
            this.itemGrid.ShowPrice = false;
            this.itemGrid.ShowQuantity = true;
            this.itemGrid.ShowRate = false;
            this.itemGrid.ShowRemitedColumn = true;
            this.itemGrid.ShowUnit = true;
            this.itemGrid.ShowUnitPrice = true;
            this.itemGrid.Size = new System.Drawing.Size(781, 152);
            this.itemGrid.TabIndex = 3;
            this.itemGrid.UnitPriceCaption = "Unit Price";
            this.itemGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewItems});
            // 
            // gridViewItems
            // 
            this.gridViewItems.GridControl = this.itemGrid;
            this.gridViewItems.Name = "gridViewItems";
            this.gridViewItems.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewItems.OptionsCustomization.AllowFilter = false;
            this.gridViewItems.OptionsCustomization.AllowGroup = false;
            this.gridViewItems.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewItems.OptionsCustomization.AllowSort = false;
            this.gridViewItems.OptionsView.ShowGroupPanel = false;
            // 
            // voucher
            // 
            this.voucher.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.voucher.Appearance.Options.UseBackColor = true;
            this.voucher.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.voucher.Location = new System.Drawing.Point(12, 54);
            this.voucher.Name = "voucher";
            this.voucher.Size = new System.Drawing.Size(294, 63);
            this.voucher.TabIndex = 35;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.CornflowerBlue;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(781, 22);
            this.labelControl1.StyleController = this.layoutControl2;
            this.labelControl1.TabIndex = 34;
            this.labelControl1.Text = "General Sales Invoice Information";
            // 
            // dateTransaction
            // 
            this.dateTransaction.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateTransaction.Location = new System.Drawing.Point(310, 54);
            this.dateTransaction.Name = "dateTransaction";
            this.dateTransaction.ShowEthiopian = true;
            this.dateTransaction.ShowGregorian = true;
            this.dateTransaction.ShowTime = true;
            this.dateTransaction.Size = new System.Drawing.Size(483, 63);
            this.dateTransaction.TabIndex = 0;
            this.dateTransaction.VerticalLayout = true;
            // 
            // labelCustomerInformation
            // 
            this.labelCustomerInformation.AllowHtmlString = true;
            this.labelCustomerInformation.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelCustomerInformation.Location = new System.Drawing.Point(14, 139);
            this.labelCustomerInformation.Name = "labelCustomerInformation";
            this.labelCustomerInformation.Padding = new System.Windows.Forms.Padding(3);
            this.labelCustomerInformation.Size = new System.Drawing.Size(384, 19);
            this.labelCustomerInformation.StyleController = this.layoutControl2;
            this.labelCustomerInformation.TabIndex = 15;
            this.labelCustomerInformation.Text = "###";
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem5,
            this.layoutControlItem13,
            this.layoutRelationInformation,
            this.layoutControlItem3,
            this.layoutControlItem2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(805, 344);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.labelControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(785, 26);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.dateTransaction;
            this.layoutControlItem5.CustomizationFormText = "Date";
            this.layoutControlItem5.Location = new System.Drawing.Point(298, 26);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 83);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(130, 83);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(487, 83);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "Date";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(126, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.Control = this.voucher;
            this.layoutControlItem13.CustomizationFormText = "Reference";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(0, 83);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(130, 83);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(298, 83);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "Reference";
            this.layoutControlItem13.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(126, 13);
            // 
            // layoutRelationInformation
            // 
            this.layoutRelationInformation.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutRelationInformation.AppearanceItemCaption.Options.UseFont = true;
            this.layoutRelationInformation.Control = this.labelCustomerInformation;
            this.layoutRelationInformation.CustomizationFormText = "Customer Information";
            this.layoutRelationInformation.Location = new System.Drawing.Point(0, 109);
            this.layoutRelationInformation.Name = "layoutRelationInformation";
            this.layoutRelationInformation.Size = new System.Drawing.Size(392, 43);
            this.layoutRelationInformation.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutRelationInformation.Text = "Customer Information";
            this.layoutRelationInformation.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutRelationInformation.TextSize = new System.Drawing.Size(126, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.itemGrid;
            this.layoutControlItem3.CustomizationFormText = "Items";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 152);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(785, 172);
            this.layoutControlItem3.Text = "Items";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(126, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.linkEditSupplier;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(392, 109);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(393, 43);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // dxValidationProvider
            // 
            this.dxValidationProvider.ValidateHiddenControls = false;
            // 
            // dxValidationProviderWH
            // 
            this.dxValidationProviderWH.ValidateHiddenControls = false;
            // 
            // dxValidationProviderWHV
            // 
            this.dxValidationProviderWHV.ValidateHiddenControls = false;
            // 
            // tabControl
            // 
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedTabPage = this.xtraTabPage1;
            this.tabControl.Size = new System.Drawing.Size(833, 350);
            this.tabControl.TabIndex = 1;
            this.tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage3});
            this.tabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.tabControl_SelectedPageChanged);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.layoutControl2);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(805, 344);
            this.xtraTabPage1.Text = "Details";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.layoutControl3);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(805, 344);
            this.xtraTabPage3.Text = "Payment";
            // 
            // Sell2Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(833, 534);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panelControl1);
            this.Name = "Sell2Form";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.checkCollectFullAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textRetention.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupTaxation)).EndInit();
            this.groupTaxation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textVATBase.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWHBase.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkFullVAT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkFullWH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWHTNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWHVNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVATBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFullVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWHBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFullWH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWHTRNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVATWH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentTypeSelector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cashAccountPlaceholder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checFullCredit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textTransferReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCredit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bankAccountPlaceholder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSettleMaxiumAdvance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSettleAdvancePayment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textServiceCharge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkRelationSettled.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCollectedCash.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServiceCharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServiceChargeBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSettleAdvance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSettleMaxAdvance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTaxation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFullCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCashAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCashBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBankBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCollectedCash)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCollectFullAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRetention)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTransferRefernce)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemSummaryGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.linkEditSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutRelationInformation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProviderWH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProviderWHV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private BNDualCalendar dateTransaction;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private ItemGrid itemGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewItems;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.MemoEdit memoNote;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidationProvider;
        private PaymentTypeSelector paymentTypeSelector;
        private DevExpress.XtraEditors.TextEdit textSettleAdvancePayment;
        private DevExpress.XtraEditors.CheckEdit checkSettleMaxiumAdvance;
        private DevExpress.XtraEditors.LabelControl labelCustomerInformation;
        private ItemSummaryGrid itemSummaryGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSummary;
        private DevExpress.XtraEditors.CheckEdit checFullCredit;
        private DevExpress.XtraEditors.TextEdit textCredit;
        private DevExpress.XtraEditors.SimpleButton buttonCredit;
        private DevExpress.XtraEditors.LabelControl labelPaymentInstruction;
        private DevExpress.XtraEditors.TextEdit textTransferReference;
        private DevExpress.XtraEditors.TextEdit textServiceCharge;
        private BankAccountPlaceholder bankAccountPlaceholder;
        private CashAccountPlaceholder cashAccountPlaceholder;
        private DevExpress.XtraEditors.SimpleButton buttonPrepayments;
        private DevExpress.XtraEditors.CheckEdit checkRelationSettled;
        private DevExpress.XtraEditors.LabelControl labelBankBalance;
        private DevExpress.XtraEditors.LabelControl labelCashBalance;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.TextEdit textWHTNo;
        private DevExpress.XtraEditors.TextEdit textWHVNo;
        private DevExpress.XtraEditors.SimpleButton buttonPrintAttachment;
        private DevExpress.XtraEditors.CheckEdit checkCollectFullAmount;
        private DevExpress.XtraEditors.TextEdit textCollectedCash;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidationProviderWH;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidationProviderWHV;
        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DocumentSerialPlaceHolder voucher;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutRelationInformation;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraEditors.TextEdit textVATBase;
        private DevExpress.XtraEditors.TextEdit textWHBase;
        private DevExpress.XtraEditors.CheckEdit checkFullVAT;
        private DevExpress.XtraEditors.CheckEdit checkFullWH;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutVATBase;
        private DevExpress.XtraLayout.LayoutControlItem layoutFullVAT;
        private DevExpress.XtraLayout.LayoutControlItem layoutWHBase;
        private DevExpress.XtraLayout.LayoutControlItem layoutFullWH;
        private DevExpress.XtraLayout.LayoutControlItem layoutWHTRNo;
        private DevExpress.XtraLayout.LayoutControlItem layoutVATWH;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutCashAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutCashBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutBankAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutBankBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutServiceCharge;
        private DevExpress.XtraLayout.LayoutControlItem layoutServiceChargeBy;
        private DevExpress.XtraLayout.LayoutControlItem layoutCollectedCash;
        private DevExpress.XtraLayout.LayoutControlItem layoutCollectFullAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutSettleAdvance;
        private DevExpress.XtraLayout.LayoutControlItem layoutSettleMaxAdvance;
        private DevExpress.XtraLayout.LayoutControlItem layoutCredit;
        private DevExpress.XtraLayout.LayoutControlItem layoutFullCredit;
        private DevExpress.XtraLayout.LayoutControlItem layoutTransferRefernce;
        private DevExpress.XtraEditors.GroupControl groupTaxation;
        private DevExpress.XtraLayout.LayoutControlItem layoutTaxation;
        private DevExpress.XtraEditors.TextEdit textRetention;
        private DevExpress.XtraLayout.LayoutControlItem layoutRetention;
        private DevExpress.XtraEditors.SimpleButton buttonPrev;
        private DevExpress.XtraEditors.SimpleButton buttonNext;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.HyperLinkEdit linkEditSupplier;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SimpleButton buttonDeliveries;
    }
}