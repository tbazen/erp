﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using DevExpress.XtraLayout.Utils;
using BIZNET.iERP.TypedDataSets;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Client
{
    public enum TradeTransactionPaymentField
    {
        VATBase,
        WHBase,
        SettleAdvancePayment,
        Credit,
        Retention,
        CollectedCash
    }
    public partial class Sell2Form<SellDocumentType> : DevExpress.XtraEditors.XtraForm,IItemGridController
        where SellDocumentType : Sell2Document, new()
    {
        public virtual string getFormTitle()
        {
            return "Sells";
        }
        public virtual string getTradeRelationType()
        {
            return "Customer";
        }
        public virtual List<TradeTransactionPaymentField> getPaymentFields()
        {
            return new List<TradeTransactionPaymentField>(((TradeTransactionPaymentField[])Enum.GetValues(typeof(TradeTransactionPaymentField))));
        }
        public virtual string getAdditionalRelationInfo(TradeRelation rel, DateTime date)
        {
            if (_customer.PayableAccountID != -1)
            {
                double bal = AccountingClient.GetNetCostCenterAccountBalanceAsOf(iERPTransactionClient.mainCostCenterID, _customer.PayableAccountID, dateTransaction.DateTime);
                if (AccountBase.AmountEqual(bal, 0))
                {
                    _advanceSettlement.Enabled = false;
                    return "";
                }
                else
                {
                    _advanceSettlement.Enabled = true;
                    return "\nAdvance paid:" + TSConstants.FormatBirr(bal);
                }

            }
            return "";
        }
   }
}