﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using DevExpress.XtraLayout.Utils;
using BIZNET.iERP.TypedDataSets;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Client
{
    public partial class Sell2Form<SellDocumentType> : DevExpress.XtraEditors.XtraForm, IItemGridController
        where SellDocumentType : Sell2Document, new()
    {
        protected SellDocumentType _doc = null;

        DocumentBasicFieldsController _docFields;
        protected IAccountingClient _client;
        protected ActivationParameter _activation;
        PaymentMethodAndBalanceController _paymentController;
        protected SettleFullAmountController _creditPayment;
        protected SettleFullAmountController _advanceSettlement;
        protected SettleFullAmountController _paidAmount;
        protected SettleFullAmountController _vatBase;
        protected SettleFullAmountController _whBase;

        protected TradeRelation _customer = null;
        
        ScheduleController _deliverySchedule;
        protected ScheduleController _creditSettlmentSchedule;
        protected ScheduleController _prepamentSettlementSchedule;
        public Sell2Form(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            configureForm();
            tabControl_SelectedPageChanged(null, null);
            voucher.setKeys(typeof(SellDocumentType), "salesVoucher");
            _client = client;
            _activation = activation;
            _paymentController = new PaymentMethodAndBalanceController(
                paymentTypeSelector
                , layoutBankAccount
                , bankAccountPlaceholder
                , layoutBankBalance
                , labelBankBalance
                , layoutCashAccount
                , cashAccountPlaceholder
                , layoutCashBalance
                , labelCashBalance
                , layoutServiceCharge
                , layoutTransferRefernce
                , dateTransaction
                , layoutServiceChargeBy
                , _activation);
            _paymentController.PaymentInformationChanged += new EventHandler(_paymentController_PaymentInformationChanged);
            _docFields = new DocumentBasicFieldsController(dateTransaction, null, memoNote);

            _advanceSettlement = new SettleFullAmountController(textSettleAdvancePayment, checkSettleMaxiumAdvance);
            _advanceSettlement.ValueChanged += new EventHandler(_advanceSettlement_ValueChanged);
            _creditPayment = new SettleFullAmountController(textCredit, checFullCredit);
            _creditPayment.FullAmount = 0;
            _creditPayment.ValueChanged += new EventHandler(_creditPayment_ValueChanged);

            _paidAmount = new SettleFullAmountController(textCollectedCash, checkCollectFullAmount);
            checkCollectFullAmount.Checked = true;
            _paidAmount.ValueChanged += new EventHandler(_paidAmount_ValueChanged);

            _vatBase = new SettleFullAmountController(textVATBase, checkFullVAT);
            checkFullVAT.Checked = true;
            _vatBase.ValueChanged += new EventHandler(_vatBase_ValueChanged);

            _whBase = new SettleFullAmountController(textWHBase, checkFullWH);
            checkFullWH.Checked = true;
            _whBase.ValueChanged += new EventHandler(_whBase_ValueChanged);



            itemGrid.initializeGrid();
            itemGrid.Controller = this;
            itemSummaryGrid.initializeGrid();
            setCustomer(_activation.objectCode);
            updateSummaryTable();

            _deliverySchedule = new ScheduleController(
                buttonDeliveries, null
                , typeof(SoldItemDeliveryDocument));

            _deliverySchedule.BeforeActivation += delegate(ScheduleController source)
            {
                ActivationParameter act = new ActivationParameter();
                act.sellDocument = buildSellsDocument();
                return act;
            };

            _prepamentSettlementSchedule = new ScheduleController(
                buttonPrepayments, null
                , typeof(SellPrePaymentDocument));
            _prepamentSettlementSchedule.BeforeActivation += delegate(ScheduleController source)
            {
                ActivationParameter act = new ActivationParameter();
                act.sellDocument = buildSellsDocument();
                return act;
            };

            _creditSettlmentSchedule = new ScheduleController(
                buttonCredit, null
                , typeof(CustomerCreditSettlement2Document));
            _creditSettlmentSchedule.BeforeActivation += delegate(ScheduleController source)
            {
                ActivationParameter act = new ActivationParameter();
                act.sellDocument = buildSellsDocument();
                act.objectCode = _customer == null ? null : _customer.Code;
                return act;
            };
            if (IsScheduleMode())
            {
                textWHTNo.Text = "Scheduled";
                textWHVNo.Text = "Scheduled";
            }

        }
        private void configureForm()
        {
            this.Text = getFormTitle();
            layoutRelationInformation.Text = getTradeRelationType() + " Information";
            linkEditSupplier.Text = "Edit " + getTradeRelationType();
            List<TradeTransactionPaymentField> fields = getPaymentFields();
            layoutSettleAdvance.Visibility = layoutSettleMaxAdvance.Visibility
                = fields.Contains(TradeTransactionPaymentField.SettleAdvancePayment) ? LayoutVisibility.Always : LayoutVisibility.Never;
            layoutCredit.Visibility = layoutFullCredit.Visibility
                = fields.Contains(TradeTransactionPaymentField.Credit) ? LayoutVisibility.Always : LayoutVisibility.Never;
            layoutRetention.Visibility
                = fields.Contains(TradeTransactionPaymentField.Retention) ? LayoutVisibility.Always : LayoutVisibility.Never;
            layoutCollectedCash.Visibility = layoutCollectFullAmount.Visibility
                = fields.Contains(TradeTransactionPaymentField.CollectedCash) ? LayoutVisibility.Always : LayoutVisibility.Never;
        }
        void _whBase_ValueChanged(object sender, EventArgs e)
        {
            updateSummaryTable();
        }

        void _vatBase_ValueChanged(object sender, EventArgs e)
        {
            updateSummaryTable();
        }

        void _paidAmount_ValueChanged(object sender, EventArgs e)
        {
            updateSummaryTable();
        }
        public bool interactiveValidateAmount()
        {
            return true;
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            Properties.Settings.Default.Save();
            base.OnClosing(e);
        }
        private SellDocumentType buildSellsDocument()
        {
            SellDocumentType ret = new SellDocumentType();
            ret.replaceAttachments = true;
            ret.salesVoucher = voucher.getReference();
            ret.PaperRef = ret.salesVoucher == null ? "" : ret.salesVoucher.reference;
            
            ret.AccountDocumentID = _doc == null ? -1 : _doc.AccountDocumentID;
            _docFields.setDocumentData(ret);
            if (!double.TryParse(textRetention.Text, out ret.retention))
                ret.retention = 0;
            ret.paymentMethod = _paymentController.PaymentTypeSelector.PaymentMethod;
            ret.checkNumber = textTransferReference.Text;
            ret.cpoNumber = textTransferReference.Text;
            ret.transferReference = textTransferReference.Text;
            ret.assetAccountID = _paymentController.assetAccountID;
            double serviceCharge;
            if (!double.TryParse(textServiceCharge.Text, out serviceCharge))
                serviceCharge = 0;
            ret.serviceChargeAmount = serviceCharge;
            ret.serviceChargePayer = checkRelationSettled.Checked ? ServiceChargePayer.Employee : ServiceChargePayer.Company;
            ret.settleAdvancePayment = _advanceSettlement.effectiveAmount;
            ret.supplierCredit = _creditPayment.effectiveAmount;
            ret.withholdingReceiptNo = textWHTNo.Text;
            ret.relationCode = _customer.Code;
            TransactionItems[] items;
            TransactionDocumentItem[] ditems;
            extractGridData(out items, out ditems);
            ret.items = ditems;

            double taxable;
            bool vatApplies, whApplies;
            ret.taxImposed = Sell2Document.getTaxes(iERPTransactionClient.GetCompanyProfile()
                , _customer
                , ditems, items, iERPTransactionClient.getTaxRates()
                , !checkFullVAT.Checked, _vatBase.effectiveAmount
                , !checkFullWH.Checked, _whBase.effectiveAmount
                , out ret.amount
                , out taxable
                , out vatApplies, out whApplies
                );
            ret.deliveries = buttonDeliveries.Visible ? _deliverySchedule.getScheduledDocumentsByType<SoldItemDeliveryDocument>().ToArray() : new SoldItemDeliveryDocument[0];
            ret.creditSettlements = _creditSettlmentSchedule.getScheduledDocumentsByType<CustomerReceivableDocument>().ToArray();
            ret.prepayments = _prepamentSettlementSchedule.getScheduledDocumentsByType<SellPrePaymentDocument>().ToArray();
            ret.withHeldVatReceiptNo = textWHVNo.Text;
            ret.paidAmount = _paidAmount.effectiveAmount;

            ret.manualVATBase = checkFullVAT.Checked ? -1d : _vatBase.effectiveAmount;
            ret.manualWithholdBase = checkFullWH.Checked ? -1d : _whBase.effectiveAmount;

            return ret;
        }


        void _creditPayment_ValueChanged(object sender, EventArgs e)
        {
            updateSummaryTable();
            showHideCreditButton();
        }

        void _advanceSettlement_ValueChanged(object sender, EventArgs e)
        {
            updateSummaryTable();
        }

        void _paymentController_PaymentInformationChanged(object sender, EventArgs e)
        {
            updateSupplierInfo();
        }

        private void setCustomer(string code)
        {
            if (string.IsNullOrEmpty(code))
            {
                _customer = null;
                linkEditSupplier.Visible = false;
            }
            else
            {
                linkEditSupplier.Visible = true;
                _customer = iERPTransactionClient.GetRelation(code);
                _paymentController.AddCostCenterAccountItem(GetOrCreateCostCenterAccount(iERPTransactionClient.mainCostCenterID, _customer.PayableAccountID).id, null);
            }
            updateSupplierInfo();
        }
        private CostCenterAccount GetOrCreateCostCenterAccount(int costCenterID, int accountID)
        {
            CostCenterAccount ret = AccountingClient.GetCostCenterAccount(costCenterID, accountID);
            if (ret != null)
                return ret;
            return AccountingClient.GetCostCenterAccount(AccountingClient.CreateCostCenterAccount(costCenterID, accountID));
        }

        private void updateSupplierInfo()
        {
            if (_customer == null)
            {
                _advanceSettlement.Enabled = false;
                labelCustomerInformation.Text = "";
            }
            else
            {
                string info = _customer.Name;
                info += getAdditionalRelationInfo(_customer, dateTransaction.DateTime);
                labelCustomerInformation.Text = info;
            }
        }
        internal void LoadData(SellDocumentType sellDocument)
        {

            _doc = sellDocument;
            buttonPrintAttachment.Visible = !IsScheduleMode() && _doc != null && _doc.attachmentNumber > 0;
            if (_doc == null)
                return;
            voucher.setReference(sellDocument.AccountDocumentID, sellDocument.salesVoucher);

            _docFields.setControlData(sellDocument);
            setCustomer(sellDocument.relationCode);
            textWHTNo.Text = sellDocument.withholdingReceiptNo;
            textWHVNo.Text = sellDocument.withHeldVatReceiptNo;
            try
            {
                _paymentController.IgnoreEvents();
                _paymentController.PaymentTypeSelector.PaymentMethod = _doc.paymentMethod;
                _paymentController.assetAccountID = _doc.assetAccountID;
                foreach (TransactionDocumentItem item in _doc.items)
                {
                    itemGrid.Add(itemGrid.toItemGridTableRow(item));
                }
                if (!AccountBase.AmountEqual(_doc.settleAdvancePayment, 0))
                    _advanceSettlement.effectiveAmount = _doc.settleAdvancePayment;
                if (!AccountBase.AmountEqual(_doc.supplierCredit, 0))
                    _creditPayment.effectiveAmount = _doc.supplierCredit;
                _creditSettlmentSchedule.setScheduledDocuments(_doc.creditSettlementIDs);
                _deliverySchedule.setScheduledDocuments(_doc.deliverieIDs);
                _prepamentSettlementSchedule.setScheduledDocuments(_doc.prepaymentsIDs);
                textRetention.Text = AccountBase.AmountEqual(_doc.retention, 0) ? "" : AccountBase.FormatAmount(_doc.retention);

                checkFullVAT.Checked = AccountBase.AmountLess(sellDocument.manualVATBase, 0);
                if (!AccountBase.AmountLess(_doc.manualVATBase, 0))
                    _vatBase.effectiveAmount = _doc.manualVATBase;
                checkFullWH.Checked = AccountBase.AmountLess(sellDocument.manualWithholdBase, 0);
                if (!AccountBase.AmountLess(_doc.manualWithholdBase, 0))
                    _whBase.effectiveAmount = _doc.manualWithholdBase;

                checkCollectFullAmount.Checked = false;
                _paidAmount.effectiveAmount = _doc.paidAmount;
                double cash = updateSummaryTable();
                checkCollectFullAmount.Checked = AccountBase.AmountEqual(cash, _doc.paidAmount);
                showHideDeliveryButton();
                showHidPrePaymentButton();

            }
            finally
            {
                _paymentController.AcceptEvents();
            }
        }



        private void showHideDeliveryButton()
        {
            bool show = false;
            foreach (ItemGridControl.ItemGridData.ItemTableRow row in itemGrid)
            {
                TransactionItems item = itemGrid.GetItem(row);
                if (item == null)
                    continue;
                if ((item.IsInventoryItem || item.IsFixedAssetItem) && !row.directExpense)
                {
                    show = true;
                    break;
                }
            }
            if (show ^ buttonDeliveries.Visible)
            {
                if (!show)
                    _deliverySchedule.setScheduledDocuments(new AccountDocument[0]);
                buttonDeliveries.Visible = show;
            }
        }
        private void showHidPrePaymentButton()
        {
            bool show = false;
            foreach (ItemGridControl.ItemGridData.ItemTableRow row in itemGrid)
            {
                TransactionItems item = itemGrid.GetItem(row);
                if (item == null)
                    continue;
                if (row.prepaid)
                {
                    show = true;
                    break;
                }
            }
            if (show ^ buttonPrepayments.Visible)
                buttonPrepayments.Visible = show;

        }
        private void showHideCreditButton()
        {
            bool show = _creditPayment.hasNoneZeroValue;
            if (show ^ buttonCredit.Visible)
                buttonCredit.Visible = show;

        }

        void extractGridData(out TransactionItems[] items, out TransactionDocumentItem[] docItems)
        {
            items = new TransactionItems[itemGrid.Count];
            docItems = new TransactionDocumentItem[itemGrid.Count];
            int i = 0;
            foreach (ItemGridControl.ItemGridData.ItemTableRow row in itemGrid)
            {
                items[i] = itemGrid.GetItem(row);
                docItems[i] = ItemGrid.toDocumentItem(row);
                docItems[i].price = docItems[i].quantity * docItems[i].unitPrice;
                i++;
            }
        }
        private double updateSummaryTable()
        {
            itemSummaryGrid.data.Clear();
            if (_customer == null)
                return 0;
            try
            {
                double totalPurchase = 0;
                double totalTaxable = 0;
                TransactionItems[] items;
                TransactionDocumentItem[] docItems;
                extractGridData(out items, out docItems);
                bool vatApplies, whApplies;
                TaxImposed[] taxes = Sell2Document.getTaxes(
                    iERPTransactionClient.GetCompanyProfile()
                    , _customer
                    , docItems, items
                    , iERPTransactionClient.getTaxRates()
                                    , !checkFullVAT.Checked, _vatBase.effectiveAmount
                    , !checkFullWH.Checked, _whBase.effectiveAmount

                    , out totalPurchase
                    , out totalTaxable
                    , out vatApplies, out whApplies
                    );

                double netPayment;
                bool hasWH = false;

                double withHolding = 0;
                bool hasVATWH = false;
                double withheldVat = 0;


                if (!AccountBase.AmountEqual(totalTaxable, 0))
                {
                    int order = 20;
                    double addedTax = 0;
                    foreach (TaxImposed tax in taxes)
                    {
                        if (tax.TaxType == TaxType.WithHoldingTax)
                        {
                            withHolding = tax.TaxValue;
                            hasWH = true;
                            _whBase.FullAmount = tax.TaxBaseValue;
                        }
                        if (tax.TaxType == TaxType.VATWitholding)
                        {
                            withheldVat = tax.TaxValue;
                            hasVATWH = true;
                        }
                        if (tax.TaxType == TaxType.VAT)
                        {
                            _vatBase.FullAmount = tax.TaxBaseValue;
                        }
                        itemSummaryGrid.setSummary("tax_" + tax.TaxType, TaxImposed.TaxTypeName(tax.TaxType), tax.TaxValue, order);
                        order += 10;
                        addedTax += tax.TaxValue;

                    }
                    itemSummaryGrid.setSummary("totalPrice", "Total Before Tax", totalPurchase, 10);
                    netPayment = totalPurchase + addedTax;
                    order += 10;
                }
                else
                    netPayment = totalPurchase;
                layoutWHTRNo.Visibility = hasWH ? LayoutVisibility.Always : LayoutVisibility.Never;
                layoutVATWH.Visibility = hasVATWH ? LayoutVisibility.Always : LayoutVisibility.Never;

                layoutWHBase.Visibility = layoutFullWH.Visibility = whApplies ? LayoutVisibility.Always : LayoutVisibility.Never;
                layoutFullVAT.Visibility = layoutVATBase.Visibility = vatApplies ? LayoutVisibility.Always : LayoutVisibility.Never;
                layoutTaxation.Visibility = whApplies || vatApplies ? LayoutVisibility.Always : LayoutVisibility.Never;

                bool credits = false;
                bool settlment = false;
                bool retention = false;
                if (_advanceSettlement.hasNoneZeroValue)
                {
                    credits = true;
                    itemSummaryGrid.setSummary("advance", "Less Advance Payment", -_advanceSettlement.effectiveAmount, 80);
                }
                if (_customer != null)
                {
                    _advanceSettlement.FullAmount = Math.Min(netPayment, _paymentController.getCsAccountBalance(GetOrCreateCostCenterAccount(iERPTransactionClient.mainCostCenterID, _customer.PayableAccountID).id));
                    _creditPayment.FullAmount = netPayment - _advanceSettlement.effectiveAmount;
                }
                if (_creditPayment.hasNoneZeroValue)
                {
                    settlment = true;
                    itemSummaryGrid.setSummary("credit", "Less TradeRelation Credit", -_creditPayment.effectiveAmount, 90);
                }

                double retained;

                if (double.TryParse(textRetention.Text, out retained) && AccountBase.AmountGreater(retained, 0))
                {
                    retention = true;
                    itemSummaryGrid.setSummary("retention", "Retention", -retained, 90);
                }
                else
                    retained = 0;

                double cash = netPayment - _advanceSettlement.effectiveAmount - _creditPayment.effectiveAmount - retained;
                _paidAmount.FullAmount = cash;
                if (credits || settlment || retention)
                {
                    itemSummaryGrid.setSummary("net", "Net Amount", netPayment, 70);
                    itemSummaryGrid.setSummary("cash", "Payment", cash, 100);
                }
                else
                {
                    itemSummaryGrid.setSummary("cash", "Payment", netPayment, 70);
                }
                string message = string.Format("Receive {0}({1})"
                        , TSConstants.FormatBirr(_paidAmount.effectiveAmount)
                        , CurrencyTranslator.TranslateCurrency((decimal)_paidAmount.effectiveAmount)
                        );

                if (hasWH)
                    labelPaymentInstruction.Text = string.Format("\nGet Witholding Receipt for amount {0}({1}) from {2}"
                        , TSConstants.FormatBirr(-withHolding)
                        , CurrencyTranslator.TranslateCurrency(-(decimal)withHolding)
                        , _customer.Name + (_customer.hasTINNumber ? "" : " -TIN:" + _customer.TIN)
                        );

                if (hasVATWH)
                    labelPaymentInstruction.Text = string.Format("\nGet Witheld VAT Receipt for amount {0}({1}) from {2}"
                        , TSConstants.FormatBirr(-withheldVat)
                        , CurrencyTranslator.TranslateCurrency(-(decimal)withheldVat)
                        , _customer.Name + (_customer.hasTINNumber ? "" : " -TIN:" + _customer.TIN)
                        );

                labelPaymentInstruction.Text = message;

                labelPaymentInstruction.ForeColor = Color.Black;
                return cash;
            }
            catch (ServerUserMessage msg)
            {
                itemSummaryGrid.data.Clear();
                labelPaymentInstruction.ForeColor = Color.Red;
                labelPaymentInstruction.Text = msg.Message;
                return 0;
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
                return 0;
            }


        }

        public double getUnitPrice(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            bool q;
            if (item.IsInventoryItem)
                return AccountingClient.GetAverageUnitPrice(row.costCenterID, item.inventoryAccountID, dateTransaction.DateTime, out q);
            if (item.IsSalesItem)
                return AccountingClient.GetAverageUnitPrice(row.costCenterID, item.finishedWorkAccountID, dateTransaction.DateTime, out q);

            return 0;
        }

        public double getBalance(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            if (item.IsInventoryItem)
                return AccountingClient.GetNetCostCenterAccountBalanceAsOf(1, item.inventoryAccountID, TransactionItem.MATERIAL_QUANTITY, dateTransaction.DateTime);
            if (item.IsSalesItem)
                return AccountingClient.GetNetCostCenterAccountBalanceAsOf(row.costCenterID, item.finishedWorkAccountID, TransactionItem.MATERIAL_QUANTITY, dateTransaction.DateTime);

            return 0;
        }

        public bool hasUnitPrice(TransactionItems item)
        {
            return false;
        }

        public bool queryCellChange(ItemGridControl.ItemGridData.ItemTableRow row, DataColumn column, object oldValue)
        {
            TransactionItems item = itemGrid.GetItem(row);
            if (item == null)
                return true;
            buttonPrintAttachment.Visible = false;
            if (column == itemGrid.data.priceColumn
                || column == itemGrid.data.unitPriceColumn
                || column == itemGrid.data.quantityColumn
                )
            {
                updateSummaryTable();
                showHideDeliveryButton();
                return true;
            }
            else if (column == itemGrid.data.prepaidColumn)
            {

                if (!item.IsSalesItem || item.IsInventoryItem)
                    return false;
                showHidPrePaymentButton();
                return true;
            }
            else if (column == itemGrid.data.codeColumn)
            {
                if (!item.IsSalesItem && !item.IsFixedAssetItem)
                    return false;
                showHideDeliveryButton();
                return true;
            }
            return true;
        }


        public bool queryDeleteRow(ItemGridControl.ItemGridData.ItemTableRow row)
        {
            updateSummaryTable();
            return true;
        }
        void postDocument(bool prepareAttacment)
        {
            bool isNewDocument = _doc == null;
            SellDocumentType sellsDoc = buildSellsDocument();

            //confirm sheduling with the user
            if (!IsScheduleMode() && sellsDoc.IsFutureDate)
            {
                if (MessageBox.ShowWarningMessage(
                    "Are you sure you want to post future transactions?"
                    ) != DialogResult.Yes)
                    return;
                sellsDoc.scheduled = true;
                sellsDoc.materialized = false;
            }

            //set next attachment number
            int attachmentNumber = -1;
            if (prepareAttacment)
            {
                if (isNewDocument || _doc.attachmentNumber == -1)
                {
                    object attachNo = iERPTransactionClient.GetSystemParamter("AttachmentNumber");
                    if ((int)attachNo == 0)
                        attachmentNumber = 1;
                    else
                    {
                        attachmentNumber = (int)attachNo;
                    }
                    sellsDoc.attachmentNumber = attachmentNumber;
                }
                else
                    attachmentNumber = _doc.attachmentNumber;
            }

            //confirm scheduling with the user
            if (!IsScheduleMode() && sellsDoc.IsFutureDate)
            {

                if (MessageBox.ShowWarningMessage(
                    "Are you sure you want to post future transactions?"
                    ) != DialogResult.Yes)
                    return;
                _doc.scheduled = true;
                _doc.materialized = false;
            }

            //post document
            sellsDoc.AccountDocumentID = _client.PostGenericDocument(sellsDoc);

            //prepare the attachment
            if (prepareAttacment)
            {
                prepareSalesAttachment(sellsDoc, attachmentNumber);
                if (isNewDocument)
                {
                    int attachNo = Convert.ToInt32(iERPTransactionClient.GetSystemParamter("AttachmentNumber"));
                    iERPTransactionClient.SetSystemParameters(new string[] { "AttachmentNumber" }, new object[] { attachNo + 1 });
                }
            }

            _doc = sellsDoc;

            //show success message if not in schedule mode
            if (!IsScheduleMode())
            {
                string message = isNewDocument ? "Sales successfully saved!" : "Sales successfully updated!";
                MessageBox.ShowSuccessMessage(message);
            }

            this.Close();
        }

        private bool IsScheduleMode()
        {
            return _client is DocumentScheduler;
        }

        private void stepSave()
        {
            try
            {
                if (!dxValidationProvider.Validate())
                    return;
                double retained;
                if (!textRetention.Text.Equals(""))
                    if (!double.TryParse(textRetention.Text, out retained) || AccountBase.AmountLess(retained, 0))
                    {
                        MessageBox.ShowErrorMessage("Enter valid retention amount or leave retention field blank");
                        return;
                    }

                ThreeButtonDialogBox db = new ThreeButtonDialogBox();
                if (IsScheduleMode())
                    postDocument(false);
                else
                {
                    switch (db.ShowDialog(this))
                    {
                        case ThreeButtonDialogResult.Button_Cancel:
                            return;
                        case ThreeButtonDialogResult.Button_Accept2:
                            postDocument(false);
                            break;
                        case ThreeButtonDialogResult.Button_Accept1:
                            postDocument(true);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }
        public void afterComit(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool IsManualVATUsed(out double manualVATBase)
        {
            manualVATBase = 0;
            if (!checkFullVAT.Checked)
            {

                manualVATBase = _vatBase.effectiveAmount;
                return true;
            }
            return false;
        }
        private void prepareSalesAttachment(SellDocumentType sellsDoc, int attachmentNumber)
        {
            Attachment attachmentData = new Attachment();
            CompanyProfile profile = (CompanyProfile)iERPTransactionClient.GetSystemParamter("companyProfile");
            TradeRelation customer = iERPTransactionClient.GetRelation(sellsDoc.relationCode);
            string customerName = customer.Name;
            string customerTIN = customer.TIN;
            string customerAddress = customer.Address;
            if (profile == null)
            {
                MessageBox.ShowErrorMessage("No company profile data found. Please configure company profile and try again");
                return;
            }
            attachmentData.Summary.AddSummaryRow(sellsDoc.DocumentDate.ToShortDateString(), profile.Name, profile.Logo
                , profile.TIN, profile.VATNumber, profile.Region, profile.Zone, profile.Kebele
                , profile.HouseNumber, profile.Telephone, profile.MobilePhone, profile.Fax
                , profile.Email, sellsDoc.PaperRef, CodeFormatter.FormatAttachmentNumber(attachmentNumber), profile.MachineNumber
                , customerName, customerTIN, customerAddress, "", 0, 0, 0, 0, 0, 0, sellsDoc.AccountDocumentID);

            int i = 0;
            double subtotal = 0;
            double totalTaxable = 0;
            double manualVATBase = 0;
            if (!IsManualVATUsed(out manualVATBase))
            {
                foreach (TransactionDocumentItem item in sellsDoc.items)
                {

                    TransactionItems titm = iERPTransactionClient.GetTransactionItems(item.code);
                    string remark = titm.TaxStatus == ItemTaxStatus.Taxable ? "Taxable" : (item.remitted ? "Remitted" : "Non-Taxable");

                    if (titm.TaxStatus == ItemTaxStatus.Taxable && !item.remitted)
                        totalTaxable += item.unitPrice * item.quantity;

                    string itemName = item.name;
                    double total = Math.Round(item.quantity * item.unitPrice, 2);
                    attachmentData.ItemsDetail.AddItemsDetailRow(i + 1, item.code, itemName
                        , item.quantity, item.unitPrice, total, remark, attachmentData.Summary[0]);
                    subtotal += total;

                    i++;
                }
            }
            else
            {
                totalTaxable = GetTotalTaxableBase(sellsDoc.items);
                double prorateRate = manualVATBase / totalTaxable;
                foreach (TransactionDocumentItem item in sellsDoc.items)
                {
                    string itemName = item.name;
                    double total = Math.Round(item.quantity * item.unitPrice, 2);
                    double newUnitPrice = 0;
                    TransactionItems titm = iERPTransactionClient.GetTransactionItems(item.code);
                    string remark = titm.TaxStatus == ItemTaxStatus.Taxable ? "Taxable" : (item.remitted ? "Remitted" : "Non-Taxable");
                    if (titm.TaxStatus == ItemTaxStatus.Taxable && !item.remitted)
                    {
                        double newTotal = GetItemProratedPrice(prorateRate, total, item.quantity, out newUnitPrice);
                        attachmentData.ItemsDetail.AddItemsDetailRow(i + 1, item.code, itemName
                            , item.quantity, newUnitPrice, newTotal, remark, attachmentData.Summary[0]);
                        subtotal += newTotal;
                    }
                    else
                    {
                        attachmentData.ItemsDetail.AddItemsDetailRow(i + 1, item.code, itemName
                        , item.quantity, item.unitPrice, total, remark, attachmentData.Summary[0]);
                        subtotal += total;
                    }
                    i++;
                }
                totalTaxable = manualVATBase;
            }

            string taxName = "Tax";
            double taxValue = 0, withHolding, netPayment, grandTotal;
            taxValue = withHolding = netPayment = grandTotal = 0;
            double taxWithholding = 0;

            foreach (TaxImposed taxImposed in sellsDoc.taxImposed)
            {
                if (taxImposed.TaxType == TaxType.VAT || taxImposed.TaxType == TaxType.VATWitholding)
                {
                    taxName = "VAT";
                    taxValue += taxImposed.TaxValue;
                    if (customer.IsVatAgent && taxImposed.TaxValue > 0)
                        taxWithholding = taxValue;
                }
                else if (taxImposed.TaxType == TaxType.TOT)
                {
                    taxName = "TOT";
                    taxValue = taxImposed.TaxValue;
                }
                else if (taxImposed.TaxType == TaxType.WithHoldingTax)
                {
                    withHolding = -taxImposed.TaxValue;
                }
            }
            if (Properties.Settings.Default.PaperKindIndex == 0) //A4 paper format
            {
                grandTotal = subtotal + taxValue;
                netPayment = grandTotal - withHolding - (taxWithholding != 0 ? taxValue : 0);
                attachmentData.Summary[0].AmountInWord = CurrencyTranslator.TranslateCurrency(decimal.Parse(netPayment.ToString()));
                attachmentData.Summary[0].SubTotal = subtotal;
                attachmentData.Summary[0].TaxableTotal = totalTaxable;
                attachmentData.Summary[0].TaxPayable = taxWithholding == 0 ? taxValue : taxWithholding;
                attachmentData.Summary[0].GrandTotal = grandTotal;
                attachmentData.Summary[0].Withholding = withHolding;
                attachmentData.Summary[0].NetPayment = netPayment;
                attachmentData.Summary[0].AttachmentNo = attachmentNumber == 0 ? "" : CodeFormatter.FormatAttachmentNumber(attachmentNumber);
                Attachment_A4 A4_Attachment = new Attachment_A4(taxName, taxWithholding);
                A4_Attachment.DataSource = attachmentData;
                PrintFormViewer viewer = new PrintFormViewer();
                viewer.LoadReport(A4_Attachment);
                viewer.ShowDialog();
            }
            else //A5 paper format
            {
                grandTotal = subtotal + taxValue;
                netPayment = grandTotal - withHolding - (taxWithholding != 0 ? taxValue : 0);
                attachmentData.Summary[0].AmountInWord = CurrencyTranslator.TranslateCurrency(decimal.Parse(netPayment.ToString()));
                attachmentData.Summary[0].SubTotal = subtotal;
                attachmentData.Summary[0].TaxableTotal = totalTaxable;
                attachmentData.Summary[0].TaxPayable = taxWithholding == 0 ? taxValue : taxWithholding;
                attachmentData.Summary[0].GrandTotal = grandTotal;
                attachmentData.Summary[0].Withholding = withHolding;
                attachmentData.Summary[0].NetPayment = netPayment;
                attachmentData.Summary[0].AttachmentNo = attachmentNumber == 0 ? "" : CodeFormatter.FormatAttachmentNumber(attachmentNumber);
                Attachment_A5 A5_Attachment = new Attachment_A5(taxName, taxWithholding);
                A5_Attachment.DataSource = attachmentData;
                PrintFormViewer viewer = new PrintFormViewer();
                viewer.LoadReport(A5_Attachment);
                viewer.ShowDialog();
            }
        }

        private double GetItemProratedPrice(double prorateRate, double total, double quantity, out double unitPrice)
        {
            double totalPrice = total * prorateRate;
            unitPrice = totalPrice / quantity;
            return totalPrice;
        }

        private double GetTotalTaxableBase(TransactionDocumentItem[] transactionDocumentItems)
        {
            double totalTaxable = 0;
            foreach (TransactionDocumentItem item in transactionDocumentItems)
            {
                TransactionItems titm = iERPTransactionClient.GetTransactionItems(item.code);
                if (titm.TaxStatus == ItemTaxStatus.Taxable && !item.remitted)
                    totalTaxable += item.unitPrice * item.quantity;
            }
            return totalTaxable;
        }

        private void buttonPrintAttachment_Click(object sender, EventArgs e)
        {
            try
            {
                if (!dxValidationProvider.Validate())
                    return;
                prepareSalesAttachment(_doc, _doc.attachmentNumber);
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void txtWithholdBase_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                updateSummaryTable();
        }

        private void txtManualVATBase_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                updateSummaryTable();
        }

        private void checAllCredit_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textSettleAdvancePayment_EditValueChanged(object sender, EventArgs e)
        {
            updateSummaryTable();
        }

        private void textRetention_EditValueChanged(object sender, EventArgs e)
        {
            updateSummaryTable();
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {

            if (tabControl.SelectedTabPageIndex == tabControl.TabPages.Count - 1)
                stepSave();
            else
            {
                tabControl.SelectedTabPageIndex++;
            }
        }

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            tabControl.SelectedTabPageIndex--;
        }

        private void tabControl_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            buttonNext.Text = tabControl.SelectedTabPageIndex == tabControl.TabPages.Count - 1 ? "Finish" : "Next>>";
            buttonPrev.Enabled = tabControl.SelectedTabPageIndex > 0;
        }

        private void linkEditSupplier_OpenLink(object sender, DevExpress.XtraEditors.Controls.OpenLinkEventArgs e)
        {
            RegisterRelation rr = new RegisterRelation(_customer.Code);
            if (rr.ShowDialog(this) == DialogResult.OK)
            {
                setCustomer(_customer.coaCode);
                updateSummaryTable();
            }

        }

    }
}