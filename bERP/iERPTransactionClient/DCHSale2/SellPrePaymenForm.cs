﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class SellPrePaymentForm : DevExpress.XtraEditors.XtraForm, IItemGridController
    {
        SellPrePaymentDocument _doc;
        DocumentBasicFieldsController _docFields;
        IAccountingClient _client;
        ActivationParameter _activation;
        Sell2Document _sellDocument;
        public SellPrePaymentForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            _client = client;
            _activation = activation;
            itemGrid.Controller = this;
            itemGrid.initializeGrid();
            _docFields = new DocumentBasicFieldsController(dateTransaction, textReference, memoNote);
            setSellDocument(activation.sellDocument);
            reloadItemsFromSell();
        }
        private void setSellDocument(Sell2Document doc)
        {
            _sellDocument = doc;
        }

        private void reloadItemsFromSell()
        {
            itemGrid.Clear();
            if (_sellDocument != null)
                foreach (TransactionDocumentItem item in _sellDocument.items)
                {
                    TransactionDocumentItem citem = item.clone();
                    if (citem.prepaid)
                    {
                        itemGrid.Add(itemGrid.toItemGridTableRow(item));
                        double earnedPrice ;
                        double earnedQuantity = getEarnedQuantity(itemGrid.GetItem(itemGrid[itemGrid.Count - 1]),out earnedPrice);
                        if (AccountBase.AmountGreater(earnedQuantity, 0))
                        {
                            double unitPrice = earnedPrice / earnedQuantity;
                            ItemGridControl.ItemGridData.ItemTableRow row = itemGrid[itemGrid.Count - 1];
                            if (AccountBase.AmountEqual(unitPrice, item.unitPrice))
                            {
                                row.quantity = item.quantity - earnedQuantity;
                                row.price = row.unitPrice * row.quantity;
                            }
                            else if (AccountBase.AmountEqual(earnedQuantity, item.quantity))
                            {
                                row.unitPrice = item.unitPrice - unitPrice;
                                row.price = row.unitPrice * row.quantity;
                            }
                        }
                        
                    }
                }
        }

        public double getUnitPrice(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            foreach (TransactionDocumentItem ditem in _activation.purchaseDocument.items)
            {
                if (TransactionItems.codeEqual(item.Code, ditem.code))
                {
                    return ditem.unitPrice;
                }
            }
            throw new Exception("Item not found in the original document");
        }
        public double getBalance(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            return 0;
        }
        public double getEarnedQuantity(TransactionItems item,out double price)
        {
            double earnedQuantity = 0;
            double earnedPrice = 0;
            SellPrePaymentDocument[] earnings;
            if (_client is DocumentScheduler)
            {
                earnings = ((DocumentScheduler)_client).GetListedSchedules<SellPrePaymentDocument>().ToArray();
            }
            else
                earnings = _sellDocument.prepayments;
            foreach (SellPrePaymentDocument doc in earnings)
            {
                if (doc.DocumentDate <= dateTransaction.DateTime && doc!=_doc)
                {
                    foreach (TransactionDocumentItem ditem in doc.items)
                    {
                        if (TransactionItems.codeEqual(item.Code, ditem.code))
                        {
                            earnedQuantity += ditem.quantity;
                            earnedPrice += ditem.price;
                            break;
                        }
                    }
                }
            }
            price = earnedPrice;
            return earnedQuantity;
        }

        internal void LoadData(SellPrePaymentDocument prePaymentDocument)
        {
            _doc = prePaymentDocument;
            if (prePaymentDocument != null)
            {
                _docFields.setControlData(_doc);
                setSellDocument(_activation.sellDocument);
                itemGrid.Clear();
                foreach (TransactionDocumentItem item in _doc.items)
                {
                    itemGrid.Add(item);
                }
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {

                if (!dxValidationProvider.Validate())
                    return;

                int docID = _doc == null ? -1 : _doc.AccountDocumentID;
                _doc = new SellPrePaymentDocument();
                _doc.AccountDocumentID = docID;
                _docFields.setDocumentData(_doc);
                List<TransactionDocumentItem> items = new List<TransactionDocumentItem>();
                foreach (ItemGridControl.ItemGridData.ItemTableRow row in itemGrid.data)
                {
                    if (!string.IsNullOrEmpty(row.code))
                        items.Add(ItemGrid.toDocumentItem(row));
                }
                _doc.items = items.ToArray();
                _doc.AccountDocumentID = _client.PostGenericDocument(_doc);
                if (!(_client is DocumentScheduler))
                {
                    MessageBox.ShowSuccessMessage("Document succesfully posted");
                }
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        public bool hasUnitPrice(TransactionItems item)
        {
            return true;
        }


        public bool queryCellChange(ItemGridControl.ItemGridData.ItemTableRow row, DataColumn column, object oldValue)
        {
            if (_sellDocument != null)
            {
                if (column == itemGrid.data.codeColumn)
                {
                    foreach (TransactionDocumentItem docitem in _sellDocument.items)
                    {
                        if (TransactionItems.codeEqual(docitem.code, row.code))
                            return true;
                    }
                    return false;
                }
            }
            return true;
        }


        public bool queryDeleteRow(ItemGridControl.ItemGridData.ItemTableRow row)
        {
            return true;
        }
        public void afterComit(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {

        }

    }
}

