﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Client
{
    public partial class SoldtemDeliveryForm : DevExpress.XtraEditors.XtraForm, IItemGridController
    {
        SoldItemDeliveryDocument _doc;
        DocumentBasicFieldsController _docFields;
        IAccountingClient _client;
        ActivationParameter _activation;
        Sell2Document _sellDocument;
        bool _quantityModified;
        public SoldtemDeliveryForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            voucher.setKeys(typeof(SoldItemDeliveryDocument), "voucher");
            _client = client;
            _activation = activation;
            
            if (_activation.storeID != -1)
                storePlaceholder.storeID = _activation.storeID;
            itemGrid.Controller = this;
            itemGrid.initializeGrid();
            _docFields = new DocumentBasicFieldsController(dateTransaction, null, memoNote);
            setSellDocument(activation.sellDocument);
            reloadItemsFromPurhcase();
        }

        private void setSellDocument(Sell2Document doc)
        {
            _sellDocument = doc;
        }

        private void reloadItemsFromPurhcase()
        {
            itemGrid.Clear();
            if (_sellDocument != null)
                foreach (TransactionDocumentItem item in _sellDocument.items)
                {
                    TransactionDocumentItem citem = item.clone();
                    TransactionItems titem = iERPTransactionClient.GetTransactionItems(item.code);
                    if (titem.IsInventoryItem || titem.IsFixedAssetItem)
                    {
                        itemGrid.Add(itemGrid.toItemGridTableRow(item));
                        itemGrid[itemGrid.Count - 1].quantity = getBalance(null,titem);
                    }
                }
        }

        public double getUnitPrice(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            if (_activation.sellDocument!= null)
            {
                foreach (TransactionDocumentItem ditem in _activation.sellDocument.items)
                {
                    if (TransactionItems.codeEqual(item.Code, ditem.code))
                    {
                        return ditem.unitPrice;
                    }
                }
            }
            throw new Exception("Item not found in the original document");
        }

        public double getBalance(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            double orderBalance = 0;
            if (_sellDocument != null)
            {
                foreach (TransactionDocumentItem ditem in _sellDocument.items)
                {
                    if (TransactionItems.codeEqual(item.Code, ditem.code))
                    {
                        orderBalance = ditem.quantity;
                        break;
                    }
                }
            }
            SoldItemDeliveryDocument[] deliveries;
            if (_client is DocumentScheduler)
            {
                deliveries = ((DocumentScheduler)_client).GetListedSchedules<SoldItemDeliveryDocument>().ToArray();
            }
            else
                deliveries = _sellDocument.deliveries;
            foreach (SoldItemDeliveryDocument doc in deliveries)
            {
                if (doc.DocumentDate <= dateTransaction.DateTime && doc!=_doc)
                {
                    foreach (TransactionDocumentItem ditem in doc.items)
                    {
                        if (TransactionItems.codeEqual(item.Code, ditem.code))
                        {
                            orderBalance -= ditem.quantity;
                            break;
                        }
                    }
                }
            }
            return orderBalance;
        }

        internal void LoadData(SoldItemDeliveryDocument deliveryDocument)
        {
            _doc = deliveryDocument;
            if (deliveryDocument != null)
            {
                voucher.setReference(deliveryDocument.AccountDocumentID, deliveryDocument.voucher);
                _docFields.setControlData(_doc);
                int storeID = -1;
                foreach (TransactionDocumentItem item in deliveryDocument.items)
                {
                    if (storeID == -1)
                        storeID = item.costCenterID;
                    else
                        if (storeID != item.costCenterID)
                        {
                            throw new ServerUserMessage("Items delivery from stores not supported with single delivery document");
                        }
                }
                storePlaceholder.storeID = storeID;
                setSellDocument(_activation.sellDocument);
                itemGrid.Clear();
                foreach (TransactionDocumentItem item in _doc.items)
                {
                    itemGrid.Add(item);
                }
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (!dxValidationProvider.Validate())
                    return;
                if (!(_client is DocumentScheduler))
                {
                    if (voucher.getReference() == null)
                    {
                        MessageBox.ShowErrorMessage("Enter valid reference");
                        return;
                    }
                }
                int docID = _doc == null ? -1 : _doc.AccountDocumentID;
                _doc = new SoldItemDeliveryDocument();
                _doc.AccountDocumentID = docID;
                _doc.voucher = voucher.getReference();
                _doc.PaperRef = _doc.voucher.reference;
                _docFields.setDocumentData(_doc);
                List<TransactionDocumentItem> items = new List<TransactionDocumentItem>();
                foreach (ItemGridControl.ItemGridData.ItemTableRow row in itemGrid.data)
                {
                    if (!string.IsNullOrEmpty(row.code))
                        items.Add(ItemGrid.toDocumentItem(row));
                }
                _doc.items = items.ToArray();
                foreach (TransactionDocumentItem ditems in _doc.items)
                {
                    ditems.costCenterID = storePlaceholder.storeID;
                }
                _doc.AccountDocumentID=_client.PostGenericDocument(_doc);
                if(!(_client is DocumentScheduler))
                {
                    MessageBox.ShowSuccessMessage("Order succesfully registered");
                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        public bool hasUnitPrice(TransactionItems item)
        {
            return true;
        }


        public bool queryCellChange(ItemGridControl.ItemGridData.ItemTableRow row, DataColumn column, object oldValue)
        {
            if (_sellDocument != null)
            {
                if (column == itemGrid.data.codeColumn)
                {
                    foreach (TransactionDocumentItem docitem in _sellDocument.items)
                    {
                        if (TransactionItems.codeEqual(docitem.code, row.code))
                            return true;
                    }
                    return false;
                }
            }
            return true;
        }


        public bool queryDeleteRow(ItemGridControl.ItemGridData.ItemTableRow row)
        {
            return true;
        }
        public void afterComit(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {

        }

    }
}

