﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BIZNET.iERP.Client
{
   enum ThreeButtonDialogResult
   {
       Button_Accept1,
       Button_Accept2,
       Button_Cancel,
   }
    partial class ThreeButtonDialogBox : DevExpress.XtraEditors.XtraForm
    {

        ThreeButtonDialogResult buttonID = ThreeButtonDialogResult.Button_Cancel;
        public ThreeButtonDialogBox()
        {
            InitializeComponent();
        }
        public new ThreeButtonDialogResult ShowDialog()
        {
            base.ShowDialog();
            return buttonID;
        }
        public new ThreeButtonDialogResult ShowDialog(IWin32Window owner)
        {
            base.ShowDialog(owner);
            return buttonID;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            buttonID = ThreeButtonDialogResult.Button_Cancel;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            buttonID = ThreeButtonDialogResult.Button_Accept2;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buttonID = ThreeButtonDialogResult.Button_Accept1;
            this.Close();
        }

    }

}