﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace BIZNET.iERP.Client
{
    public partial class Attachment_A4 : DevExpress.XtraReports.UI.XtraReport
    {
        public Attachment_A4(string taxName, double withHeldVAT)
        {
            InitializeComponent();
            lblTaxName.Text = taxName;
            if (withHeldVAT == 0)
            {
                lblWitheldVAT.Visible = txtWtihheldVAT.Visible = false;
                lblWithholding.LocationF = new PointF(lblWithholding.LocationF.X, 233.47F);
                txtWithholding.LocationF = new PointF(txtWithholding.LocationF.X, 233.68F);
                lblNetPayment.LocationF = new PointF(lblNetPayment.LocationF.X, 291.89F);
                txtPayment.LocationF = new PointF(txtPayment.LocationF.X, 291.89F);
            }
            else
            {
                txtWtihheldVAT.Text = withHeldVAT.ToString("N");
            }
        }

    }
}
