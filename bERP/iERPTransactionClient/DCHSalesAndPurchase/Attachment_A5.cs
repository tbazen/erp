﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace BIZNET.iERP.Client
{
    public partial class Attachment_A5 : DevExpress.XtraReports.UI.XtraReport
    {
        public Attachment_A5(string taxName, double withHeldVAT)
        {
            InitializeComponent();
            lblTaxName.Text = taxName;
            if (withHeldVAT == 0)
            {
                this.xrTable4.DeleteRow(xrTableRow19);
            }
            else
            {
                txtWithheldVAT.Text = withHeldVAT.ToString("N");
            }
        }

    }
}
