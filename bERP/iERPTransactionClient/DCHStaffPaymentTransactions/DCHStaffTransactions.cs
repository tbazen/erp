using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHStaffPaymentTransactionBase : bERPClientDocumentHandler
    {
        public DCHStaffPaymentTransactionBase(Type formType)
            : base(formType)
        {
        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            StaffPaymentTransactionFormBase f = (StaffPaymentTransactionFormBase)editor;

            f.LoadData((StaffReceivableAccountDocument)doc);
        }
    }
    public class DCHExpenseAdvance : DCHStaffPaymentTransactionBase
    {
        public DCHExpenseAdvance():base(typeof(ExpenseAdvanceForm))
        {
        }
    
    }

    public class DCHShortTermLoan : DCHStaffPaymentTransactionBase
    {
        public DCHShortTermLoan()
            : base(typeof(ShortTermLoanForm))
        {
        }
       
    }
    public class DCHLongTermLoan : DCHStaffPaymentTransactionBase
    {
        public DCHLongTermLoan()
            : base(typeof(LongTermLoanForm))
        {

        }
    }

    public class DCHUnclaimedSalary : DCHStaffPaymentTransactionBase
    {
        public DCHUnclaimedSalary()
            : base(typeof(SalaryPaymentForm))
        {
        }

    }
    public class DCHStaffLoanPayment : DCHStaffPaymentTransactionBase
    {
        public DCHStaffLoanPayment()
            : base(typeof(StaffLoanReturnForm))
        {
        }

    }

    public class DCHShareHoldersLoan  : DCHStaffPaymentTransactionBase
    {
        public DCHShareHoldersLoan()
            : base(typeof(LoanToShareHolderForm))
        {
        }
    }
}
