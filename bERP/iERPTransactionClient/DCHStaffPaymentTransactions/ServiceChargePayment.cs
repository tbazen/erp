﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BIZNET.iERP.Client
{
    public partial class ServiceChargePayment : DevExpress.XtraEditors.XtraForm
    {
        private ServiceChargePayer m_ServiceChargePayer = ServiceChargePayer.None;

        public ServiceChargePayment(string employeeNameWithID)
        {
            InitializeComponent();
            object[] vals = iERPTransactionClient.GetSystemParameters(new string[] { "companyProfile" });
            if (vals != null)
            {
                if (vals.Length > 0 && vals[0] != null)
                {
                    CompanyProfile loadedProfile = (CompanyProfile)vals[0];
                    btnFromCompany.Text = loadedProfile.Name + " paid service charge";
                }
            }
            btnFromEmployee.Text = employeeNameWithID + " paid bank service charge";
        }


        public ServiceChargePayer ServiceChargePayer
        {
            get
            {
                return m_ServiceChargePayer;
            }
          
        }
        private void btnFromCompany_Click(object sender, EventArgs e)
        {
            m_ServiceChargePayer = ServiceChargePayer.Company;
            DialogResult = DialogResult.OK;
        }

        private void btnFromEmployee_Click(object sender, EventArgs e)
        {
            m_ServiceChargePayer = ServiceChargePayer.Employee;
            DialogResult = DialogResult.OK;
        }

        private void ServiceChargePayment_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult != DialogResult.OK)
                DialogResult = DialogResult.Cancel;
        }
    }
}