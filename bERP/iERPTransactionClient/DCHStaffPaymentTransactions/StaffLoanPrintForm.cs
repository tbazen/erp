﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace BIZNET.iERP.Client.DCHStaffPaymentTransactions
{
    public partial class StaffLoanPrintForm : DevExpress.XtraReports.UI.XtraReport
    {
        public StaffLoanPrintForm()
        {
            InitializeComponent();
        }

        public void ChangeReportFormatToShareHolder()
        {
            txtName.Text = "Share Holder Name:";
            lblMonthlyReturn.Visible = txtMonthlyReturn.Visible = false;
            lblPaymentStartPeriod.Visible = txtStartPeriod.Visible = false;
            lblEmpSignature.Text = "Share Holder Signature:___________________________";
        }
        public void ChangeReportFormatToLoanFromStaff()
        {
            txtName.Text = "Staff Name:";
            lblMonthlyReturn.Visible = txtMonthlyReturn.Visible = false;
            lblPaymentStartPeriod.Visible = txtStartPeriod.Visible = false;
            lblEmpSignature.Text = "Company Signature:___________________________";
        }
    }
}
