﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;
using BIZNET.iERP.Client.DCHStaffPaymentTransactions;

namespace BIZNET.iERP.Client
{
    public abstract partial class StaffPaymentTransactionFormBase : XtraForm
    {
        void SetFormTitle()
        {
            if (m_Employee != null)
                employeeNameWithID = m_Employee.EmployeeNameID;
            else
                employeeNameWithID = "";
            lblTitle.Text = "<size=14><color=#360087><b>" + getTitle() + "</b></color></size>" +
                "<size=14><color=#C30013><b>" + employeeNameWithID + "</b></color></size>" +
                "<size=14><color=#360087><b>" + _paymentMethodController.PaymentSource + "</b></color></size>";
        }
        
        int getEmployeeAccountID(int costCenterID)
        {
            int aid = accountID;
            if (aid == -1)
                return -1;
            if (m_Employee == null)
                return -1;
            CostCenterAccount csa= AccountingClient.GetCostCenterAccount(costCenterID, iERPTransactionClient.GetEmployeeAccountID(m_Employee, accountID));
            if (csa == null)
                return -1;
            return csa.id;
        }

        void setEmployee(int empID)
        {
            if (empID == -1)
            {
                m_Employee = null;
            }
            else
            {
                m_Employee = PayrollClient.GetEmployee(empID);
            }
        }

        internal void LoadData(StaffReceivableAccountDocument staffReceivableAccountDoc)
        {
            m_doc = staffReceivableAccountDoc;
            if (staffReceivableAccountDoc == null)
            {
                btnPrint.Visible = false;
                ResetControls();
            }
            else
            {
                btnPrint.Visible = IsPrintable;
                newDocument = false;
                setEmployee(m_doc.employeeID);
                //SetFormTitle();
                try
                {
                    _paymentMethodController.IgnoreEvents();
                    comboPaymentType.PaymentMethod = m_doc.paymentMethod;
                    PopulateControlsForUpdate(staffReceivableAccountDoc);
                    SetFormTitle();
                    _paymentMethodController.AddCostCenterAccountItem(getEmployeeAccountID(CostCenter.ROOT_COST_CENTER), labelStaffPaymentBal);
                    _faAmount.FullAmount = _paymentMethodController.getBalance(labelStaffPaymentBal);
                }
                finally
                {
                    _paymentMethodController.AcceptEvents();
                    onPeriodSelectionChanged();
                }
            }

        }
        private void ResetControls()
        {
            datePayment.DateTime = DateTime.Now;
            txtAmount.Text = "";
            txtMonthlyReturn.Text = "";
            txtReference.Text = "";
            txtNote.Text = "";
            voucher.setReference(-1, null);
        }
        protected virtual void PopulateControlsForUpdate(StaffReceivableAccountDocument staffReceivableAccountDoc)
        {
            datePayment.DateTime = staffReceivableAccountDoc.DocumentDate;
            voucher.setReference(staffReceivableAccountDoc.AccountDocumentID, staffReceivableAccountDoc.voucher);
            _paymentMethodController.assetAccountID = staffReceivableAccountDoc.assetAccountID;

            
            txtAmount.Text = TSConstants.FormatBirr(staffReceivableAccountDoc.amount);
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(comboPaymentType.PaymentMethod))
                txtServiceCharge.Text = staffReceivableAccountDoc.serviceChargeAmount.ToString("N");
            SetReferenceNumber(false);
            txtNote.Text = staffReceivableAccountDoc.ShortDescription;

        }

    }
}
