﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;
using BIZNET.iERP.Client.DCHStaffPaymentTransactions;

namespace BIZNET.iERP.Client
{
    abstract partial class StaffPaymentTransactionFormBase : XtraForm
    {
        protected virtual bool IsPrintable
        {
            get
            {
                return false;
            }
        }

        private void SetReferenceNumber(bool updateDoc)
        {
            switch (comboPaymentType.PaymentMethod)
            {
                case BizNetPaymentMethod.Check:
                    if (newDocument)
                        m_doc.checkNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.checkNumber;
                        else
                            m_doc.checkNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.CPOFromBankAccount:
                    if (newDocument)
                        m_doc.cpoNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.cpoNumber;
                        else
                            m_doc.cpoNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.BankTransferFromCash:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    if (newDocument)
                        m_doc.slipReferenceNo = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.slipReferenceNo;
                        else
                            m_doc.slipReferenceNo = txtReference.Text;
                    break;
                default:
                    break;
            }
        }

        private void ShowPrintForm()
        {
            try
            {
                CompanyProfile prof = iERPTransactionClient.GetSystemParamter("companyProfile") as CompanyProfile;
                PrintFormViewer p = new PrintFormViewer();
                StaffLoanPrintForm printLoan = new StaffLoanPrintForm();
                if (m_doc is ShortTermLoanDocument)
                {
                    printLoan.lblTitle.Text = "Staff Short Term Loan";
                }
                else if (m_doc is LongTermLoanDocument)
                {
                    printLoan.lblTitle.Text = "Staff Long Term Loan";
                }
                else if (m_doc is ShareHoldersLoanDocument)
                {
                    printLoan.lblTitle.Text = "Share Holder Loan";
                    printLoan.ChangeReportFormatToShareHolder();
                }
                StaffLoanData data = new StaffLoanData();
                data.StaffLoan.AddStaffLoanRow(m_doc.AccountDocumentID
                    , m_doc.PaperRef
                    , m_doc.DocumentDate.ToShortDateString()
                    , PayrollClient.GetEmployee(m_doc.employeeID).EmployeeNameID
                    , m_doc.amount, m_doc.monthlyReturn
                    , cmbPeriod.SelectedItem != null ? cmbPeriod.SelectedItem.ToString() : ""
                    , prof.Name
                    , prof.Logo);
                printLoan.DataSource = data;
                p.LoadReport(printLoan);

                p.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            ShowPrintForm();
        }
    }
}
