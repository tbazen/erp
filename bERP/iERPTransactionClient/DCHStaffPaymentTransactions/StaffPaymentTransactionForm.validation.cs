﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;
using BIZNET.iERP.Client.DCHStaffPaymentTransactions;

namespace BIZNET.iERP.Client
{
    abstract partial class StaffPaymentTransactionFormBase : XtraForm
    {
        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validationStaffReceivable.Validate(control);
        }

        protected virtual void InitializeValidationRules()
        {
            InitializeMonthlyReturnAmountValidation();
        }
        protected virtual bool additionalValidation()
        {
            return true;
        }

        private void InitializeMonthlyReturnAmountValidation()
        {
            NonEmptyNumericValidationRule monthlyReturnAmountValidation = new NonEmptyNumericValidationRule();
            monthlyReturnAmountValidation.ErrorText = "Monthly return amount cannot be blank and cannot contain a value of zero";
            monthlyReturnAmountValidation.ErrorType = ErrorType.Default;
            validationStaffReceivable.SetValidationRule(txtMonthlyReturn, monthlyReturnAmountValidation);
        }
    }
}
