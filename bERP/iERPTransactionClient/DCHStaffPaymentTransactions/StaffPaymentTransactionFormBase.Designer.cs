﻿namespace BIZNET.iERP.Client
{
    partial class StaffPaymentTransactionFormBase 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.buttonResetSchedule = new DevExpress.XtraEditors.SimpleButton();
            this.gridLoanList = new DevExpress.XtraGrid.GridControl();
            this.gridViewLoanList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.voucher = new BIZNET.iERP.Client.DocumentSerialPlaceHolder();
            this.datePayment = new BIZNET.iERP.Client.BNDualCalendar();
            this.checkSettleFullAmount = new DevExpress.XtraEditors.CheckEdit();
            this.comboPaymentType = new BIZNET.iERP.Client.PaymentTypeSelector();
            this.lblPeriodUnclaimed = new DevExpress.XtraEditors.LabelControl();
            this.txtMonthlyReturn = new DevExpress.XtraEditors.TextEdit();
            this.cmbPeriod = new BIZNET.iERP.Client.PeriodSelector();
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.txtServiceCharge = new DevExpress.XtraEditors.TextEdit();
            this.labelStaffPaymentBal = new DevExpress.XtraEditors.LabelControl();
            this.labelBankBalance = new DevExpress.XtraEditors.LabelControl();
            this.labelCashBalance = new DevExpress.XtraEditors.LabelControl();
            this.txtNote = new DevExpress.XtraEditors.MemoEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelScheduledDocuments = new DevExpress.XtraEditors.LabelControl();
            this.buttonSchedule = new DevExpress.XtraEditors.SimpleButton();
            this.btnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.cmbCasher = new BIZNET.iERP.Client.CashAccountPlaceholder();
            this.cmbBankAccount = new BIZNET.iERP.Client.BankAccountPlaceholder();
            this.txtReference = new DevExpress.XtraEditors.TextEdit();
            this.txtAmount = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutgroupLoansList = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutGroupStaff = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlCashAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlCashBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlBankAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlBankBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlReceivableBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlPeriod = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlPeriodUnclaimed = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlMonthlyReturn = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlSettleFullAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlNote = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlServiceCharge = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlSlipRef = new DevExpress.XtraLayout.LayoutControlItem();
            this.validationStaffReceivable = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLoanList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLoanList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSettleFullAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboPaymentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonthlyReturn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceCharge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCasher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBankAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutgroupLoansList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupStaff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlReceivableBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPeriodUnclaimed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMonthlyReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSettleFullAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlServiceCharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSlipRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationStaffReceivable)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.panelControl2);
            this.layoutControl1.Controls.Add(this.gridLoanList);
            this.layoutControl1.Controls.Add(this.voucher);
            this.layoutControl1.Controls.Add(this.datePayment);
            this.layoutControl1.Controls.Add(this.checkSettleFullAmount);
            this.layoutControl1.Controls.Add(this.comboPaymentType);
            this.layoutControl1.Controls.Add(this.lblPeriodUnclaimed);
            this.layoutControl1.Controls.Add(this.txtMonthlyReturn);
            this.layoutControl1.Controls.Add(this.cmbPeriod);
            this.layoutControl1.Controls.Add(this.lblTitle);
            this.layoutControl1.Controls.Add(this.txtServiceCharge);
            this.layoutControl1.Controls.Add(this.labelStaffPaymentBal);
            this.layoutControl1.Controls.Add(this.labelBankBalance);
            this.layoutControl1.Controls.Add(this.labelCashBalance);
            this.layoutControl1.Controls.Add(this.txtNote);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.cmbCasher);
            this.layoutControl1.Controls.Add(this.cmbBankAccount);
            this.layoutControl1.Controls.Add(this.txtReference);
            this.layoutControl1.Controls.Add(this.txtAmount);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(447, 217, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(779, 574);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.buttonResetSchedule);
            this.panelControl2.Location = new System.Drawing.Point(19, 100);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(741, 42);
            this.panelControl2.TabIndex = 25;
            // 
            // buttonResetSchedule
            // 
            this.buttonResetSchedule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonResetSchedule.Location = new System.Drawing.Point(623, 8);
            this.buttonResetSchedule.Name = "buttonResetSchedule";
            this.buttonResetSchedule.Size = new System.Drawing.Size(104, 29);
            this.buttonResetSchedule.TabIndex = 7;
            this.buttonResetSchedule.Text = "&Reset Schedule";
            // 
            // gridLoanList
            // 
            this.gridLoanList.Location = new System.Drawing.Point(19, 146);
            this.gridLoanList.MainView = this.gridViewLoanList;
            this.gridLoanList.Name = "gridLoanList";
            this.gridLoanList.Size = new System.Drawing.Size(741, 361);
            this.gridLoanList.TabIndex = 24;
            this.gridLoanList.ToolTipController = this.toolTipController1;
            this.gridLoanList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLoanList});
            // 
            // gridViewLoanList
            // 
            this.gridViewLoanList.GridControl = this.gridLoanList;
            this.gridViewLoanList.Name = "gridViewLoanList";
            this.gridViewLoanList.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewLoanList.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewLoanList.OptionsView.ShowGroupPanel = false;
            // 
            // toolTipController1
            // 
            this.toolTipController1.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.toolTipController1_GetActiveObjectInfo);
            // 
            // voucher
            // 
            this.voucher.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.voucher.Appearance.Options.UseBackColor = true;
            this.voucher.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.voucher.Location = new System.Drawing.Point(19, 116);
            this.voucher.Name = "voucher";
            this.voucher.Size = new System.Drawing.Size(363, 68);
            this.voucher.TabIndex = 23;
            // 
            // datePayment
            // 
            this.datePayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.datePayment.Location = new System.Drawing.Point(389, 119);
            this.datePayment.Name = "datePayment";
            this.datePayment.ShowEthiopian = true;
            this.datePayment.ShowGregorian = true;
            this.datePayment.ShowTime = true;
            this.datePayment.Size = new System.Drawing.Size(368, 63);
            this.datePayment.TabIndex = 22;
            this.datePayment.VerticalLayout = true;
            // 
            // checkSettleFullAmount
            // 
            this.checkSettleFullAmount.Location = new System.Drawing.Point(529, 371);
            this.checkSettleFullAmount.Name = "checkSettleFullAmount";
            this.checkSettleFullAmount.Properties.Caption = "Settle Full Amount";
            this.checkSettleFullAmount.Size = new System.Drawing.Size(228, 19);
            this.checkSettleFullAmount.StyleController = this.layoutControl1;
            this.checkSettleFullAmount.TabIndex = 21;
            // 
            // comboPaymentType
            // 
            this.comboPaymentType.Include_BankTransferFromAccount = true;
            this.comboPaymentType.Include_BankTransferFromCash = true;
            this.comboPaymentType.Include_Cash = true;
            this.comboPaymentType.Include_Check = true;
            this.comboPaymentType.Include_CpoFromBank = true;
            this.comboPaymentType.Include_CpoFromCash = true;
            this.comboPaymentType.Include_Credit = false;
            this.comboPaymentType.Location = new System.Drawing.Point(168, 191);
            this.comboPaymentType.Name = "comboPaymentType";
            this.comboPaymentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboPaymentType.Properties.Items.AddRange(new object[] {
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash"});
            this.comboPaymentType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboPaymentType.Size = new System.Drawing.Size(589, 20);
            this.comboPaymentType.StyleController = this.layoutControl1;
            this.comboPaymentType.TabIndex = 20;
            // 
            // lblPeriodUnclaimed
            // 
            this.lblPeriodUnclaimed.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeriodUnclaimed.Location = new System.Drawing.Point(517, 308);
            this.lblPeriodUnclaimed.Name = "lblPeriodUnclaimed";
            this.lblPeriodUnclaimed.Size = new System.Drawing.Size(227, 26);
            this.lblPeriodUnclaimed.StyleController = this.layoutControl1;
            this.lblPeriodUnclaimed.TabIndex = 18;
            this.lblPeriodUnclaimed.Text = "Unclaimed Amount: 0.00 Birr";
            // 
            // txtMonthlyReturn
            // 
            this.txtMonthlyReturn.Location = new System.Drawing.Point(168, 341);
            this.txtMonthlyReturn.Name = "txtMonthlyReturn";
            this.txtMonthlyReturn.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtMonthlyReturn.Properties.Mask.EditMask = "#,########0.00;";
            this.txtMonthlyReturn.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtMonthlyReturn.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtMonthlyReturn.Size = new System.Drawing.Size(589, 20);
            this.txtMonthlyReturn.StyleController = this.layoutControl1;
            this.txtMonthlyReturn.TabIndex = 16;
            this.txtMonthlyReturn.Validated += new System.EventHandler(this.txtMonthlyReturn_Validated);
            // 
            // cmbPeriod
            // 
            this.cmbPeriod.Location = new System.Drawing.Point(168, 311);
            this.cmbPeriod.Name = "cmbPeriod";
            this.cmbPeriod.PeriodType = "AP_Payroll";
            this.cmbPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPeriod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbPeriod.Size = new System.Drawing.Size(342, 20);
            this.cmbPeriod.StyleController = this.layoutControl1;
            this.cmbPeriod.TabIndex = 15;
            this.cmbPeriod.SelectedIndexChanged += new System.EventHandler(this.cmbPeriod_SelectedIndexChanged);
            // 
            // lblTitle
            // 
            this.lblTitle.AllowHtmlString = true;
            this.lblTitle.Appearance.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lblTitle.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(112)))));
            this.lblTitle.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblTitle.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lblTitle.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.lblTitle.Location = new System.Drawing.Point(5, 5);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(769, 59);
            this.lblTitle.StyleController = this.layoutControl1;
            this.lblTitle.TabIndex = 13;
            this.lblTitle.Text = "Staff Payment Form for ";
            // 
            // txtServiceCharge
            // 
            this.txtServiceCharge.Location = new System.Drawing.Point(168, 431);
            this.txtServiceCharge.Name = "txtServiceCharge";
            this.txtServiceCharge.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtServiceCharge.Properties.Mask.EditMask = "#,########0.00;";
            this.txtServiceCharge.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtServiceCharge.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtServiceCharge.Size = new System.Drawing.Size(589, 20);
            this.txtServiceCharge.StyleController = this.layoutControl1;
            this.txtServiceCharge.TabIndex = 12;
            // 
            // labelStaffPaymentBal
            // 
            this.labelStaffPaymentBal.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStaffPaymentBal.Location = new System.Drawing.Point(168, 281);
            this.labelStaffPaymentBal.Name = "labelStaffPaymentBal";
            this.labelStaffPaymentBal.Size = new System.Drawing.Size(452, 20);
            this.labelStaffPaymentBal.StyleController = this.layoutControl1;
            this.labelStaffPaymentBal.TabIndex = 11;
            this.labelStaffPaymentBal.Text = "Current Balance: 0.00 Birr";
            // 
            // labelBankBalance
            // 
            this.labelBankBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBankBalance.Location = new System.Drawing.Point(531, 248);
            this.labelBankBalance.Name = "labelBankBalance";
            this.labelBankBalance.Size = new System.Drawing.Size(229, 26);
            this.labelBankBalance.StyleController = this.layoutControl1;
            this.labelBankBalance.TabIndex = 8;
            this.labelBankBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // labelCashBalance
            // 
            this.labelCashBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCashBalance.Location = new System.Drawing.Point(531, 218);
            this.labelCashBalance.Name = "labelCashBalance";
            this.labelCashBalance.Size = new System.Drawing.Size(229, 26);
            this.labelCashBalance.StyleController = this.layoutControl1;
            this.labelCashBalance.TabIndex = 7;
            this.labelCashBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(168, 461);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(589, 43);
            this.txtNote.StyleController = this.layoutControl1;
            this.txtNote.TabIndex = 6;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.labelScheduledDocuments);
            this.panelControl1.Controls.Add(this.buttonSchedule);
            this.panelControl1.Controls.Add(this.btnPrint);
            this.panelControl1.Controls.Add(this.buttonCancel);
            this.panelControl1.Controls.Add(this.buttonOk);
            this.panelControl1.Location = new System.Drawing.Point(7, 523);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(765, 44);
            this.panelControl1.TabIndex = 5;
            // 
            // labelScheduledDocuments
            // 
            this.labelScheduledDocuments.Location = new System.Drawing.Point(260, 16);
            this.labelScheduledDocuments.Name = "labelScheduledDocuments";
            this.labelScheduledDocuments.Size = new System.Drawing.Size(63, 13);
            this.labelScheduledDocuments.TabIndex = 6;
            this.labelScheduledDocuments.Text = "labelControl1";
            // 
            // buttonSchedule
            // 
            this.buttonSchedule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSchedule.Location = new System.Drawing.Point(116, 13);
            this.buttonSchedule.Name = "buttonSchedule";
            this.buttonSchedule.Size = new System.Drawing.Size(128, 23);
            this.buttonSchedule.TabIndex = 5;
            this.buttonSchedule.Text = "&Schedule Settlement";
            this.buttonSchedule.Visible = false;
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrint.Location = new System.Drawing.Point(6, 13);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(104, 23);
            this.btnPrint.TabIndex = 5;
            this.btnPrint.Text = "&Print Form";
            this.btnPrint.Visible = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(701, 15);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(59, 23);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(623, 15);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(62, 23);
            this.buttonOk.TabIndex = 4;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // cmbCasher
            // 
            this.cmbCasher.Location = new System.Drawing.Point(168, 221);
            this.cmbCasher.Name = "cmbCasher";
            this.cmbCasher.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCasher.Properties.ImmediatePopup = true;
            this.cmbCasher.Size = new System.Drawing.Size(356, 20);
            this.cmbCasher.StyleController = this.layoutControl1;
            this.cmbCasher.TabIndex = 1;
            // 
            // cmbBankAccount
            // 
            this.cmbBankAccount.Location = new System.Drawing.Point(168, 251);
            this.cmbBankAccount.Name = "cmbBankAccount";
            this.cmbBankAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbBankAccount.Properties.ImmediatePopup = true;
            this.cmbBankAccount.Size = new System.Drawing.Size(356, 20);
            this.cmbBankAccount.StyleController = this.layoutControl1;
            this.cmbBankAccount.TabIndex = 1;
            // 
            // txtReference
            // 
            this.txtReference.Location = new System.Drawing.Point(168, 401);
            this.txtReference.Name = "txtReference";
            this.txtReference.Properties.Mask.EditMask = "\\w.*";
            this.txtReference.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtReference.Size = new System.Drawing.Size(589, 20);
            this.txtReference.StyleController = this.layoutControl1;
            this.txtReference.TabIndex = 2;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "Slip Reference cannot be empty";
            this.validationStaffReceivable.SetValidationRule(this.txtReference, conditionValidationRule2);
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(168, 371);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtAmount.Properties.Mask.EditMask = "#,########0.00;";
            this.txtAmount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtAmount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtAmount.Size = new System.Drawing.Size(351, 20);
            this.txtAmount.StyleController = this.layoutControl1;
            this.txtAmount.TabIndex = 2;
            this.txtAmount.Validated += new System.EventHandler(this.txtAmount_Validated);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlGroup2,
            this.tabbedControlGroup1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(779, 574);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.panelControl1;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 516);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(769, 48);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(769, 59);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.Color.MidnightBlue;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.Control = this.lblTitle;
            this.layoutControlItem2.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(900, 59);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(596, 59);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem2.Size = new System.Drawing.Size(769, 59);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 59);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutGroupStaff;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(769, 457);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGroupStaff,
            this.layoutgroupLoansList});
            this.tabbedControlGroup1.Text = "tabbedControlGroup1";
            // 
            // layoutgroupLoansList
            // 
            this.layoutgroupLoansList.CustomizationFormText = "Loans List";
            this.layoutgroupLoansList.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem8});
            this.layoutgroupLoansList.Location = new System.Drawing.Point(0, 0);
            this.layoutgroupLoansList.Name = "layoutgroupLoansList";
            this.layoutgroupLoansList.Size = new System.Drawing.Size(745, 411);
            this.layoutgroupLoansList.Text = "Loan Return Schedule Detail";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridLoanList;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 46);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(745, 365);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.panelControl2;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(745, 46);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutGroupStaff
            // 
            this.layoutGroupStaff.CustomizationFormText = " Staff Loan";
            this.layoutGroupStaff.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlCashAccount,
            this.layoutControlCashBalance,
            this.layoutControlBankAccount,
            this.layoutControlBankBalance,
            this.layoutControlReceivableBalance,
            this.layoutControlPeriod,
            this.layoutControlPeriodUnclaimed,
            this.layoutControlMonthlyReturn,
            this.layoutControlAmount,
            this.layoutControlSettleFullAmount,
            this.layoutControlNote,
            this.layoutControlSlipRef,
            this.layoutControlServiceCharge});
            this.layoutGroupStaff.Location = new System.Drawing.Point(0, 0);
            this.layoutGroupStaff.Name = "layoutGroupStaff";
            this.layoutGroupStaff.Size = new System.Drawing.Size(745, 411);
            this.layoutGroupStaff.Text = " Loan Schedule";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.voucher;
            this.layoutControlItem1.CustomizationFormText = "Document Reference";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(147, 41);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(367, 88);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "Document Reference";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.datePayment;
            this.layoutControlItem3.CustomizationFormText = "Date:";
            this.layoutControlItem3.Location = new System.Drawing.Point(367, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 88);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(257, 88);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(378, 88);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "Date:";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.comboPaymentType;
            this.layoutControlItem4.CustomizationFormText = "Payment Method";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 88);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(745, 30);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem4.Text = "Payment Method";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlCashAccount
            // 
            this.layoutControlCashAccount.Control = this.cmbCasher;
            this.layoutControlCashAccount.CustomizationFormText = "Cash Source:";
            this.layoutControlCashAccount.Location = new System.Drawing.Point(0, 118);
            this.layoutControlCashAccount.MinSize = new System.Drawing.Size(129, 30);
            this.layoutControlCashAccount.Name = "layoutControlCashAccount";
            this.layoutControlCashAccount.Size = new System.Drawing.Size(512, 30);
            this.layoutControlCashAccount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlCashAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlCashAccount.Text = "Payment from Cash Account:";
            this.layoutControlCashAccount.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlCashBalance
            // 
            this.layoutControlCashBalance.Control = this.labelCashBalance;
            this.layoutControlCashBalance.CustomizationFormText = "layoutControlCashBalance";
            this.layoutControlCashBalance.Location = new System.Drawing.Point(512, 118);
            this.layoutControlCashBalance.MaxSize = new System.Drawing.Size(233, 30);
            this.layoutControlCashBalance.MinSize = new System.Drawing.Size(233, 30);
            this.layoutControlCashBalance.Name = "layoutControlCashBalance";
            this.layoutControlCashBalance.Size = new System.Drawing.Size(233, 30);
            this.layoutControlCashBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlCashBalance.Text = "layoutControlCashBalance";
            this.layoutControlCashBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlCashBalance.TextToControlDistance = 0;
            this.layoutControlCashBalance.TextVisible = false;
            // 
            // layoutControlBankAccount
            // 
            this.layoutControlBankAccount.Control = this.cmbBankAccount;
            this.layoutControlBankAccount.CustomizationFormText = "Bank Account:";
            this.layoutControlBankAccount.Location = new System.Drawing.Point(0, 148);
            this.layoutControlBankAccount.MinSize = new System.Drawing.Size(196, 30);
            this.layoutControlBankAccount.Name = "layoutControlBankAccount";
            this.layoutControlBankAccount.Size = new System.Drawing.Size(512, 30);
            this.layoutControlBankAccount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlBankAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlBankAccount.Text = "Payment from Bank Account:";
            this.layoutControlBankAccount.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlBankBalance
            // 
            this.layoutControlBankBalance.Control = this.labelBankBalance;
            this.layoutControlBankBalance.CustomizationFormText = "layoutControlItem8";
            this.layoutControlBankBalance.Location = new System.Drawing.Point(512, 148);
            this.layoutControlBankBalance.MaxSize = new System.Drawing.Size(233, 30);
            this.layoutControlBankBalance.MinSize = new System.Drawing.Size(233, 30);
            this.layoutControlBankBalance.Name = "layoutControlBankBalance";
            this.layoutControlBankBalance.Size = new System.Drawing.Size(233, 30);
            this.layoutControlBankBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlBankBalance.Text = "layoutControlItem8";
            this.layoutControlBankBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlBankBalance.TextToControlDistance = 0;
            this.layoutControlBankBalance.TextVisible = false;
            // 
            // layoutControlReceivableBalance
            // 
            this.layoutControlReceivableBalance.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlReceivableBalance.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlReceivableBalance.Control = this.labelStaffPaymentBal;
            this.layoutControlReceivableBalance.CustomizationFormText = "layoutControlExpenseAdvance";
            this.layoutControlReceivableBalance.Location = new System.Drawing.Point(0, 178);
            this.layoutControlReceivableBalance.MaxSize = new System.Drawing.Size(608, 30);
            this.layoutControlReceivableBalance.MinSize = new System.Drawing.Size(608, 30);
            this.layoutControlReceivableBalance.Name = "layoutControlReceivableBalance";
            this.layoutControlReceivableBalance.Size = new System.Drawing.Size(745, 30);
            this.layoutControlReceivableBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlReceivableBalance.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlReceivableBalance.Text = "Total Staff Payment Paid:";
            this.layoutControlReceivableBalance.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlPeriod
            // 
            this.layoutControlPeriod.Control = this.cmbPeriod;
            this.layoutControlPeriod.CustomizationFormText = "Payment Start Month:";
            this.layoutControlPeriod.Location = new System.Drawing.Point(0, 208);
            this.layoutControlPeriod.MaxSize = new System.Drawing.Size(498, 30);
            this.layoutControlPeriod.MinSize = new System.Drawing.Size(498, 30);
            this.layoutControlPeriod.Name = "layoutControlPeriod";
            this.layoutControlPeriod.Size = new System.Drawing.Size(498, 30);
            this.layoutControlPeriod.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlPeriod.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlPeriod.Text = "Payment Start Month:";
            this.layoutControlPeriod.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlPeriodUnclaimed
            // 
            this.layoutControlPeriodUnclaimed.Control = this.lblPeriodUnclaimed;
            this.layoutControlPeriodUnclaimed.CustomizationFormText = "layoutControlPeriodUnclaimed";
            this.layoutControlPeriodUnclaimed.Location = new System.Drawing.Point(498, 208);
            this.layoutControlPeriodUnclaimed.MaxSize = new System.Drawing.Size(231, 30);
            this.layoutControlPeriodUnclaimed.MinSize = new System.Drawing.Size(231, 30);
            this.layoutControlPeriodUnclaimed.Name = "layoutControlPeriodUnclaimed";
            this.layoutControlPeriodUnclaimed.Size = new System.Drawing.Size(247, 30);
            this.layoutControlPeriodUnclaimed.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlPeriodUnclaimed.Text = "layoutControlPeriodUnclaimed";
            this.layoutControlPeriodUnclaimed.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlPeriodUnclaimed.TextToControlDistance = 0;
            this.layoutControlPeriodUnclaimed.TextVisible = false;
            // 
            // layoutControlMonthlyReturn
            // 
            this.layoutControlMonthlyReturn.Control = this.txtMonthlyReturn;
            this.layoutControlMonthlyReturn.CustomizationFormText = "Monthly Return Amount:";
            this.layoutControlMonthlyReturn.Location = new System.Drawing.Point(0, 238);
            this.layoutControlMonthlyReturn.Name = "layoutControlMonthlyReturn";
            this.layoutControlMonthlyReturn.Size = new System.Drawing.Size(745, 30);
            this.layoutControlMonthlyReturn.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlMonthlyReturn.Text = "Monthly Return Amount:";
            this.layoutControlMonthlyReturn.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlAmount
            // 
            this.layoutControlAmount.Control = this.txtAmount;
            this.layoutControlAmount.CustomizationFormText = "Amount:";
            this.layoutControlAmount.Location = new System.Drawing.Point(0, 268);
            this.layoutControlAmount.Name = "layoutControlAmount";
            this.layoutControlAmount.Size = new System.Drawing.Size(507, 30);
            this.layoutControlAmount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlAmount.Text = "Payment Amount:";
            this.layoutControlAmount.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlSettleFullAmount
            // 
            this.layoutControlSettleFullAmount.Control = this.checkSettleFullAmount;
            this.layoutControlSettleFullAmount.CustomizationFormText = "layoutControlItem5";
            this.layoutControlSettleFullAmount.Location = new System.Drawing.Point(507, 268);
            this.layoutControlSettleFullAmount.Name = "layoutControlSettleFullAmount";
            this.layoutControlSettleFullAmount.Size = new System.Drawing.Size(238, 30);
            this.layoutControlSettleFullAmount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlSettleFullAmount.Text = "layoutControlSettleFullAmount";
            this.layoutControlSettleFullAmount.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlSettleFullAmount.TextToControlDistance = 0;
            this.layoutControlSettleFullAmount.TextVisible = false;
            // 
            // layoutControlNote
            // 
            this.layoutControlNote.Control = this.txtNote;
            this.layoutControlNote.CustomizationFormText = "Note:";
            this.layoutControlNote.Location = new System.Drawing.Point(0, 358);
            this.layoutControlNote.MinSize = new System.Drawing.Size(193, 26);
            this.layoutControlNote.Name = "layoutControlNote";
            this.layoutControlNote.Size = new System.Drawing.Size(745, 53);
            this.layoutControlNote.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlNote.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlNote.Text = "Note:";
            this.layoutControlNote.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlServiceCharge
            // 
            this.layoutControlServiceCharge.Control = this.txtServiceCharge;
            this.layoutControlServiceCharge.CustomizationFormText = "Service Charge:";
            this.layoutControlServiceCharge.Location = new System.Drawing.Point(0, 328);
            this.layoutControlServiceCharge.Name = "layoutControlServiceCharge";
            this.layoutControlServiceCharge.Size = new System.Drawing.Size(745, 30);
            this.layoutControlServiceCharge.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlServiceCharge.Text = "Service Charge:";
            this.layoutControlServiceCharge.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlSlipRef
            // 
            this.layoutControlSlipRef.Control = this.txtReference;
            this.layoutControlSlipRef.CustomizationFormText = "Slip Reference:";
            this.layoutControlSlipRef.Location = new System.Drawing.Point(0, 298);
            this.layoutControlSlipRef.Name = "layoutControlSlipRef";
            this.layoutControlSlipRef.Size = new System.Drawing.Size(745, 30);
            this.layoutControlSlipRef.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlSlipRef.Text = "Slip Reference:";
            this.layoutControlSlipRef.TextSize = new System.Drawing.Size(143, 13);
            // 
            // validationStaffReceivable
            // 
            this.validationStaffReceivable.ValidateHiddenControls = false;
            // 
            // StaffPaymentTransactionFormBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 574);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "StaffPaymentTransactionFormBase";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Staff Receivable Form";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridLoanList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLoanList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSettleFullAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboPaymentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonthlyReturn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceCharge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCasher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBankAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutgroupLoansList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroupStaff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlReceivableBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPeriodUnclaimed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMonthlyReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSettleFullAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlServiceCharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSlipRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationStaffReceivable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CashAccountPlaceholder cmbCasher;
        private BankAccountPlaceholder cmbBankAccount;
        protected DevExpress.XtraEditors.TextEdit txtAmount;
        private DevExpress.XtraEditors.TextEdit txtReference;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.MemoEdit txtNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.LabelControl labelCashBalance;
        private DevExpress.XtraEditors.LabelControl labelBankBalance;
        protected DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validationStaffReceivable;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraEditors.LabelControl labelStaffPaymentBal;
        private DevExpress.XtraEditors.TextEdit txtServiceCharge;
        private DevExpress.XtraEditors.LabelControl lblTitle;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        protected PeriodSelector cmbPeriod;
        protected DevExpress.XtraEditors.TextEdit txtMonthlyReturn;
        protected DevExpress.XtraEditors.SimpleButton btnPrint;
        private BIZNET.iERP.Client.BNDualCalendar datePayment;
        protected DevExpress.XtraEditors.LabelControl lblPeriodUnclaimed;
        private PaymentTypeSelector comboPaymentType;
        private DevExpress.XtraEditors.CheckEdit checkSettleFullAmount;
        protected DevExpress.XtraEditors.SimpleButton buttonSchedule;
        private DevExpress.XtraEditors.LabelControl labelScheduledDocuments;
        private DocumentSerialPlaceHolder voucher;
        protected DevExpress.XtraLayout.LayoutControlGroup layoutGroupStaff;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlCashAccount;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlCashBalance;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlBankAccount;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlBankBalance;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlReceivableBalance;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlPeriod;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlPeriodUnclaimed;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlMonthlyReturn;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlAmount;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlSettleFullAmount;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlNote;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlServiceCharge;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlSlipRef;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        protected DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        protected DevExpress.XtraLayout.LayoutControlGroup layoutgroupLoansList;
        protected DevExpress.XtraGrid.GridControl gridLoanList;
        protected DevExpress.XtraGrid.Views.Grid.GridView gridViewLoanList;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        protected internal DevExpress.XtraEditors.SimpleButton buttonResetSchedule;
        private DevExpress.Utils.ToolTipController toolTipController1;
    }
}