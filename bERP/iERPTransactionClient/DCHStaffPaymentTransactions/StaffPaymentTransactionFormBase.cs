﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;
using BIZNET.iERP.Client.DCHStaffPaymentTransactions;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;

namespace BIZNET.iERP.Client
{
    abstract partial class StaffPaymentTransactionFormBase : XtraForm
    {
        protected StaffReceivableAccountDocument m_doc = null;
        protected bool newDocument = true;
        protected Employee m_Employee;
        private string employeeNameWithID;
        IAccountingClient _client;
        ActivationParameter _activation;

        protected SettleFullAmountController _faAmount;
        protected PaymentMethodAndBalanceController _paymentMethodController;
        protected ScheduleController _schedule;
        public StaffPaymentTransactionFormBase(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            layoutgroupLoansList.Visibility = LayoutVisibility.Never;
            _client = client;
            _activation = activation;

            voucher.setKeys(typeof(StaffReceivableAccountDocument), "voucher");

            _faAmount = new SettleFullAmountController(txtAmount, checkSettleFullAmount);
            _paymentMethodController = new PaymentMethodAndBalanceController(comboPaymentType, layoutControlBankAccount, cmbBankAccount, layoutControlBankBalance, labelBankBalance, layoutControlCashAccount, cmbCasher, layoutControlCashBalance, labelCashBalance, layoutControlServiceCharge, layoutControlSlipRef, datePayment, _activation);
            _paymentMethodController.PaymentInformationChanged += new EventHandler(_paymentMethodController_PaymentInformationChanged);
            setEmployee(_activation.employeeID);
            _paymentMethodController.AddCostCenterAccountItem(getEmployeeAccountID(CostCenter.ROOT_COST_CENTER), labelStaffPaymentBal);

            _schedule = new ScheduleController(buttonSchedule, labelScheduledDocuments, new int[]{
                AccountingClient.GetDocumentTypeByType(typeof(StaffPerDiemDocument)).id
                ,AccountingClient.GetDocumentTypeByType(typeof(Purchase2Document)).id
                ,AccountingClient.GetDocumentTypeByType(typeof(ExpenseAdvanceReturnDocument)).id
            });
            _schedule.visible = false;
            _schedule.BeforeActivation += new ScheduleControllerBeforeActivationHandler(_schedule_BeforeActivation);
            
            txtServiceCharge.LostFocus += Control_LostFocus;
            txtMonthlyReturn.LostFocus += Control_LostFocus;
            txtAmount.LostFocus += Control_LostFocus;
            txtReference.LostFocus += Control_LostFocus;
            PrepareControlsLayoutBasedOnDocumentType();
            

            SetFormTitle();
            _faAmount.FullAmount = _paymentMethodController.getBalance(labelStaffPaymentBal);
            InitializeValidationRules();
            if (activation.employeeID != -1)
                onPeriodSelectionChanged();

        }

        ActivationParameter _schedule_BeforeActivation(ScheduleController source)
        {
            ActivationParameter ret = new ActivationParameter();
            ret.employeeID = m_Employee == null ? -1 : m_Employee.id;
            int empAccountID = AccountingClient.GetCostCenterAccount((int)iERPTransactionClient.GetSystemParamter("mainCostCenterID"),  iERPTransactionClient.GetEmployeeAccountID(m_Employee, this.accountID)).id;
            foreach (CashAccount ca in cmbCasher)
            {
                if (ca.csAccountID == empAccountID)
                {
                    ret.assetAccountID = ca.csAccountID;
                    ret.paymentMethod = BizNetPaymentMethod.Cash;
                    ret.fixPaymentMethod = true;
                    ret.fixAssetAccountID = true;
                    return ret;
                }
            }
            return ret;
        }

        protected abstract void PrepareControlsLayoutBasedOnDocumentType();
        protected abstract string getTitle();
        protected abstract int accountID { get; }
        protected abstract StaffReceivableAccountDocument createDocumentInstance();
        
        protected virtual void onPeriodSelectionChanged() { }
        protected virtual void onAmountChanged() { }
        protected virtual void onReturnAmountChange() { }
        protected void HideStaffLoanLayoutControls()
        {
            layoutControlPeriod.HideToCustomization();
            layoutControlMonthlyReturn.HideToCustomization();
        }
        protected virtual void retrieveAdditionalControlData()
        {
        }
        protected virtual void onPaymentInformationChanged()
        {
            SetFormTitle();
        }
        void _paymentMethodController_PaymentInformationChanged(object sender, EventArgs e)
        {
            onPaymentInformationChanged();
        }
        protected virtual void cmbPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                onPeriodSelectionChanged();
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
                cmbPeriod.SelectedIndex = -1;
            }
        }
        void txtAmount_EditValueChanged(object sender, EventArgs e)
        {
            
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (validationStaffReceivable.Validate()
                    )
                {
                    if (!additionalValidation())
                        return;
                    if (!_paymentMethodController.validateAssetAcountSelected())
                        return;

                    int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;

                    m_doc = createDocumentInstance();
                    m_doc.AccountDocumentID = docID;
                    m_doc.employeeID = m_Employee.id;
                    m_doc.DocumentDate = datePayment.DateTime;
                    m_doc.PaperRef = voucher.getReference().reference;
                    m_doc.voucher = voucher.getReference();
                    m_doc.paymentMethod = comboPaymentType.PaymentMethod;
                    m_doc.assetAccountID = _paymentMethodController.assetAccountID;
                    m_doc.amount = _faAmount.effectiveAmount;
                    bool updateDoc = m_doc == null ? false : true;
                    SetReferenceNumber(updateDoc);
                    retrieveAdditionalControlData();
                    
                    m_doc.ShortDescription = txtNote.Text;
                    if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(comboPaymentType.PaymentMethod) && txtServiceCharge.Text != "")
                    {
                        using (ServiceChargePayment serviceCharge = new ServiceChargePayment(employeeNameWithID))
                        {
                            serviceCharge.StartPosition = FormStartPosition.CenterScreen;
                            serviceCharge.ShowInTaskbar = false;
                            if (serviceCharge.ShowDialog() == DialogResult.OK)
                            {
                                m_doc.serviceChargePayer = serviceCharge.ServiceChargePayer;
                                double serviceChargeAmount = txtServiceCharge.Text == "" ? 0 : double.Parse(txtServiceCharge.Text);
                                m_doc.serviceChargeAmount = serviceChargeAmount;
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                    int id = _client.PostGenericDocument(m_doc);
                    if (!(_client is DocumentScheduler))
                    {
                        if(IsPrintable)
                            ShowPrintForm();
                        string message = docID == -1 ? "Payment successfully saved!" : "Payment successfully updated!";
                        MessageBox.ShowSuccessMessage(message);
                    }
                    Close();
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }

            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }


        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        protected virtual void txtMonthlyReturn_EditValueChanged(object sender, EventArgs e)
        {
            
        }

        private void txtAmount_EditValueChanged_1(object sender, EventArgs e)
        {
            
        }

        private void txtMonthlyReturn_Validated(object sender, EventArgs e)
        {
            onReturnAmountChange();
        }

        private void txtAmount_Validated(object sender, EventArgs e)
        {
            onAmountChanged();
        }

        private void toolTipController1_GetActiveObjectInfo(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.SelectedControl != gridLoanList)
                return;

            GridHitInfo hitInfo = gridViewLoanList.CalcHitInfo(e.ControlMousePosition);

            if (hitInfo.InRow == false)
                return;

            SuperToolTipSetupArgs toolTipArgs = new SuperToolTipSetupArgs();
          

            //Get the data from this row
            DataRow drCurrentRow = gridViewLoanList.GetDataRow(hitInfo.RowHandle);
            bool exception = (bool)gridViewLoanList.GetRowCellValue(hitInfo.RowHandle, "Exception?");
            if (drCurrentRow != null && exception)
            {
                toolTipArgs.Title.Text = "Incorrect Return Amount";
                string BodyText = "The loan return amount you modified should not exceed the scheduled loan return amount";
                toolTipArgs.Contents.Text = BodyText;
            }
            else if (drCurrentRow != null && !exception)
            {
                toolTipArgs.Contents.Text = "";
                toolTipArgs.Contents.Text = "";
            }

            e.Info = new ToolTipControlInfo();
            e.Info.Object = hitInfo.HitTest.ToString() + hitInfo.RowHandle.ToString();
            e.Info.ToolTipType = ToolTipType.SuperTip;
            e.Info.SuperTip = new SuperToolTip();
            e.Info.SuperTip.Setup(toolTipArgs);
        }

    }
}
