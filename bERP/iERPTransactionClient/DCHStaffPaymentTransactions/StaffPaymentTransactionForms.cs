﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using INTAPS.Payroll;
using DevExpress.XtraLayout.Utils;
using INTAPS.Accounting;
using System.Data;
using System.Drawing;
using DevExpress.XtraGrid;

namespace BIZNET.iERP.Client
{
    class ExpenseAdvanceForm : StaffPaymentTransactionFormBase
    {
        public ExpenseAdvanceForm(IAccountingClient client, ActivationParameter param)
            : base(client, param)
        {
        }
        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "Staff Purchase Advance Payment Form";
            layoutControlReceivableBalance.Text = "Unaccounted Purchase Advance:";
            layoutGroupStaff.Text = "Purchase Advance Data";
            HideStaffLoanLayoutControls();
            layoutControlPeriodUnclaimed.HideToCustomization();
            layoutControlSettleFullAmount.HideToCustomization();
            _schedule.visible = true;
        }
        protected override void retrieveAdditionalControlData()
        {
            ((ExpenseAdvanceDocument)m_doc).schedules = _schedule.getScheduledDocuments();
        }
        protected override void PopulateControlsForUpdate(StaffReceivableAccountDocument staffReceivableAccountDoc)
        {
            base.PopulateControlsForUpdate(staffReceivableAccountDoc);
            _schedule.setScheduledDocuments(((ExpenseAdvanceDocument)staffReceivableAccountDoc).schedules);
        }
        protected override string getTitle()
        {
            return "Purchase Advance Payment for ";
        }

        protected override int accountID
        {
            get { return (int)PayrollClient.GetSystemParameters(new string[] { "staffExpenseAdvanceAccountID" })[0]; }
        }

        protected override StaffReceivableAccountDocument createDocumentInstance()
        {
            return new ExpenseAdvanceDocument();
        }
        
    }
    public class StaffLoanFormBase: StaffPaymentTransactionFormBase
    {
        DataTable _loanScheduleData = null;
        public StaffLoanFormBase(IAccountingClient client, ActivationParameter param)
            : base(client, param)
        {
            _loanScheduleData = new DataTable("Data");
            _loanScheduleData.Columns.Add("Period", typeof(string));
            _loanScheduleData.Columns.Add("Return Amount", typeof(double));
            _loanScheduleData.Columns.Add("PeriodID", typeof(int));
            _loanScheduleData.Columns.Add("Exception?", typeof(bool));

            gridLoanList.DataSource = _loanScheduleData;
            gridViewLoanList.Columns["PeriodID"].Visible= false;
            gridViewLoanList.Columns["Exception?"].Visible = false;
            gridViewLoanList.Columns["Period"].OptionsColumn.AllowEdit= false;
            gridViewLoanList.Columns["Return Amount"].AppearanceCell.TextOptions.HAlignment=DevExpress.Utils.HorzAlignment.Far;
            gridViewLoanList.Columns["Return Amount"].SummaryItem.DisplayFormat = "Total = {0:n2}";
            gridViewLoanList.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewLoanList_CellValueChanged);
            buttonResetSchedule.Click += new EventHandler(buttonResetSchedule_Click);
        }

        void buttonResetSchedule_Click(object sender, EventArgs e)
        {
            _IgnoreEvents = true;
            try
            {
                _loanScheduleData.Rows.Clear();
                refreshReturnSchedule(null);
            }
            finally
            {
                _IgnoreEvents = false;
            }
        }

        void gridViewLoanList_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            DataRow row=gridViewLoanList.GetDataRow(e.RowHandle);
             row[1]=e.Value;
             if (_IgnoreEvents) return;
             _IgnoreEvents = true;
             try
             {
                 refreshReturnSchedule(row);
             }
             finally
             {
                 _IgnoreEvents = false;
             }
        }
        bool _IgnoreEvents = false;
        StaffLoandDocumentBase.LoanReturnItem[] collectExceptions(double loanAmount,double returnAmount,DataRow editedRow)
        {
            List<StaffLoandDocumentBase.LoanReturnItem> exp = new List<StaffLoandDocumentBase.LoanReturnItem>();
            int index=0;
            foreach (DataRow row in _loanScheduleData.Rows)
            {
                double ra = (double)row[1];
                int periodID = (int)row[2];
                if (!AccountBase.AmountEqual(ra, returnAmount) && 
                    (index<_loanScheduleData.Rows.Count-1 || editedRow==row)
                    )
                {
                    StaffLoandDocumentBase.LoanReturnItem ex = new StaffLoandDocumentBase.LoanReturnItem { returnAmount = ra, periodID = periodID };
                    exp.Add(ex);
                }
                index++;
            }
            return exp.ToArray();
        }
        
        protected override void onReturnAmountChange()
        {
            if (_IgnoreEvents) return;
            _IgnoreEvents = true;
            try
            {
                _loanScheduleData.Rows.Clear();
                refreshReturnSchedule(null);
                base.onReturnAmountChange();
            }
            finally
            {
                _IgnoreEvents = false;
            }
        }
        protected override void onAmountChanged()
        {
            if (_IgnoreEvents) return;
            _IgnoreEvents = true;
            try
            {
                _loanScheduleData.Rows.Clear();
                refreshReturnSchedule(null);
                base.onAmountChanged();
            }
            finally
            {
                _IgnoreEvents = false;
            }
        }

        protected override void onPeriodSelectionChanged()
        {
            if (_IgnoreEvents) return;
            _IgnoreEvents=true;
            try
            {
                PayrollPeriod period = PayrollClient.GetPayPeriod(cmbPeriod.selectedPeriodID);
                if (_loanScheduleData != null)
                {
                    foreach (DataRow row in _loanScheduleData.Rows)
                    {
                        row[2] = period.id;
                        row[0] = PayrollClient.GetPayPeriod(period.id).name;
                        period = PayrollClient.getNextPeriod(period.id);
                    }
                }
            }
            finally
            {
                _IgnoreEvents = false;
            }
            base.onPeriodSelectionChanged();
        }
        private void refreshReturnSchedule(DataRow editedRow)
        {

            double la, ra;
            if (double.TryParse(base.txtAmount.Text, out la)
                && double.TryParse(base.txtMonthlyReturn.Text, out ra))
            {
                StaffLoandDocumentBase.LoanReturnItem[] exp = collectExceptions(la, ra, editedRow);
                populateLoanSchedule(new StaffLoandDocumentBase { returnExceptions = exp, amount = la, monthlyReturn = ra, periodID = base.cmbPeriod.selectedPeriodID });
            }
            else
                _loanScheduleData.Rows.Clear();

        }
        protected override bool IsPrintable
        {
            get
            {
                return true;
            }
        }

        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            layoutControlAmount.Text = "Loan Amount";
            layoutGroupStaff.Text = "Loan Schedule";
            if (!newDocument)
                btnPrint.Visible = true;
            layoutgroupLoansList.Visibility = LayoutVisibility.Always;
            layoutControlPeriodUnclaimed.HideToCustomization();
            layoutControlSettleFullAmount.HideToCustomization();

        }
        protected override void retrieveAdditionalControlData()
        {
            m_doc.periodID = cmbPeriod.selectedPeriodID;
            m_doc.monthlyReturn = double.Parse(txtMonthlyReturn.Text);
            ((StaffLoandDocumentBase)m_doc).returnExceptions = collectExceptions(m_doc.amount, m_doc.monthlyReturn, null);
        }
        void populateLoanSchedule(StaffLoandDocumentBase doc)
        {
            txtMonthlyReturn.Enabled =txtAmount.Enabled = doc.returnExceptions.Length == 0;
            buttonResetSchedule.Enabled = !txtAmount.Enabled;
            double directReturn = 0;
            foreach (int rdocID in doc.directReturnDocumentID)
                directReturn += ((StaffLoanReturnDocument)INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(rdocID, true)).amount;
            BIZNET.iERP.StaffLoandDocumentBase.LoanReturnItem[] schedule = doc.getReturnSchedule(delegate(int periodID)
            {
                PayrollPeriod p = PayrollClient.getNextPeriod(periodID);
                if (p == null)
                    return -1;
                return p.id;
            },directReturn);

            int index = 0;
            foreach (BIZNET.iERP.StaffLoandDocumentBase.LoanReturnItem s in schedule)
            {
                DataRow row;
                if (index < _loanScheduleData.Rows.Count)
                    row = _loanScheduleData.Rows[index];
                else
                    row = _loanScheduleData.Rows.Add();
                row[0] = PayrollClient.GetPayPeriod(s.periodID).name;
                row[1] = s.returnAmount;
                row[2] = s.periodID;
                row[3] = s.returnAmount > doc.monthlyReturn;
                index++;
            }
            while (index <_loanScheduleData.Rows.Count)
            {
                _loanScheduleData.Rows.RemoveAt(index);
            }
            for (int i = 0; i < _loanScheduleData.Rows.Count; i++)
            {
                bool exception = !AccountBase.AmountEqual(doc.monthlyReturn, (double)_loanScheduleData.Rows[i][1]);
            }
            DevExpress.XtraGrid.StyleFormatCondition condition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            condition1.Appearance.BackColor = Color.IndianRed;
            condition1.Appearance.Options.UseBackColor = true;
            condition1.Condition = FormatConditionEnum.Expression;
            condition1.Expression = "[Exception?] =='True'";
            gridViewLoanList.FormatConditions.Add(condition1);
            gridViewLoanList.RefreshData();

        }
    
        protected override void PopulateControlsForUpdate(StaffReceivableAccountDocument staffReceivableAccountDoc)
        {
            _IgnoreEvents = true;
            try
            {
                cmbPeriod.selectedPeriodID = staffReceivableAccountDoc.periodID;
                txtMonthlyReturn.Text = TSConstants.FormatBirr(staffReceivableAccountDoc.monthlyReturn);
                populateLoanSchedule(staffReceivableAccountDoc as StaffLoandDocumentBase);
                base.PopulateControlsForUpdate(staffReceivableAccountDoc);
            }
            finally
            {
                _IgnoreEvents = false;
            }

        }
        protected override string getTitle()
        {
            return "Long Term Loan Payment for ";
        }

        protected override int accountID
        {
            get { return (int)PayrollClient.GetSystemParameters(new string[] { "staffLongTermLoanAccountID" })[0]; }
        }

        protected override StaffReceivableAccountDocument createDocumentInstance()
        {
            return new LongTermLoanDocument();
        }

        
        
    }
    public class LongTermLoanForm : StaffLoanFormBase
    {
        public LongTermLoanForm(IAccountingClient client, ActivationParameter param)
            : base(client, param)
        {
        }
        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "Staff Long Term Loan Payment Form";
            layoutControlReceivableBalance.Text = "Current Long-term Loan Balance:";
            base.PrepareControlsLayoutBasedOnDocumentType();
            
        }
        protected override void retrieveAdditionalControlData()
        {
            base.retrieveAdditionalControlData();
            ((StaffLoandDocumentBase)m_doc).directReturnDocumentID = null;
        }
        protected override string getTitle()
        {
            return "Long Term Loan Payment for ";
        }

        protected override int accountID
        {
            get { return (int)PayrollClient.GetSystemParameters(new string[] { "staffLongTermLoanAccountID" })[0]; }
        }

        protected override StaffReceivableAccountDocument createDocumentInstance()
        {
            return new LongTermLoanDocument();
        }
    }
    class ShortTermLoanForm : StaffLoanFormBase
    {
        public ShortTermLoanForm(IAccountingClient client, ActivationParameter param)
            : base(client, param)
        {
        }
        protected override void retrieveAdditionalControlData()
        {
            base.retrieveAdditionalControlData();
            ((StaffLoandDocumentBase)m_doc).directReturnDocumentID = null;
        }
        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "Staff Short Term Loan Payment Form";
            layoutControlReceivableBalance.Text = "Current Shor-term Loan Balance:";            
            base.PrepareControlsLayoutBasedOnDocumentType();            
        }

        protected override string getTitle()
        {
            return "Short Term Loan Payment for ";
        }

        protected override int accountID
        {
            get { return (int)PayrollClient.GetSystemParameters(new string[] { "staffSalaryAdvanceAccountID" })[0]; }
        }

        protected override StaffReceivableAccountDocument createDocumentInstance()
        {
            return new ShortTermLoanDocument();
        }
    }
    class SalaryPaymentForm : StaffPaymentTransactionFormBase
    {
        public SalaryPaymentForm(IAccountingClient client, ActivationParameter param)
            : base(client, param)
        {
        }
        double periodUnclaimed = 0;
        double netPay = 0;

        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "Staff  Salary Payment Form";
            layoutControlReceivableBalance.Text = "Current Unclaimed Salary:";
            layoutControlPeriod.Text = "Period";
            layoutGroupStaff.Text = "Salay Payment Data";
            layoutControlMonthlyReturn.HideToCustomization();
        }
        protected override void retrieveAdditionalControlData()
        {
            m_doc.periodID = cmbPeriod.selectedPeriodID;

        }
        protected override string getTitle()
        {
            return "Salary Payment for ";
        }

        protected override int accountID
        {
            get { return (int)PayrollClient.GetSystemParameters(new string[] { "UnclaimedSalaryAccount" })[0]; }
        }
        protected override void InitializeValidationRules()
        {
            base.InitializeValidationRules();
        }
        protected override void onPaymentInformationChanged()
        {
            base.onPaymentInformationChanged();
            onPeriodSelectionChanged();
        }
        protected override void onPeriodSelectionChanged()
        {
            string message = "";
            if (cmbPeriod.selectedPeriodID != -1)
            {
                double paidAmount = iERPTransactionClient.GetEmployeePaidSalaryTotal(m_Employee.id, cmbPeriod.selectedPeriodID);
                PayrollDocument payDoc = PayrollClient.GetPayroll(m_Employee.id, cmbPeriod.selectedPeriodID);

                if (payDoc != null)
                {
                    if (payDoc.Posted)
                    {
                        periodUnclaimed = payDoc.NetPay == paidAmount ? 0 : payDoc.NetPay - paidAmount;
                        netPay = payDoc.NetPay;
                        message = periodUnclaimed == 0 ? "Paid " + TSConstants.FormatBirr(netPay) + "" : "Unpaid Salary amount: " + TSConstants.FormatBirr(payDoc.NetPay - paidAmount) + " Birr";
                        if (layoutControlPeriodUnclaimed.IsHidden)
                            layoutControlPeriodUnclaimed.RestoreFromCustomization(layoutControlPeriod, InsertType.Right);
                        if (periodUnclaimed == 0 && (m_doc == null || cmbPeriod.selectedPeriodID != m_doc.periodID))  //paid and new document
                            _faAmount.Enabled = false;
                        else
                        {
                            _faAmount.Enabled = true;
                        }
                        lblPeriodUnclaimed.Text = message;
                        _faAmount.FullAmount = netPay;
                        return;
                    }
                }
            }
            _faAmount.FullAmount = 0;
            lblPeriodUnclaimed.Text = "";
        }
        protected override bool additionalValidation()
        {
            if (!_faAmount.Validate())
            {
                return false;
            }
            if (cmbPeriod.SelectedIndex != -1)
            {
                if (periodUnclaimed == 0 && (m_doc == null || cmbPeriod.selectedPeriodID != m_doc.periodID)) // if salary is paid for the selected period and it's new document
                {
                    MessageBox.ShowErrorMessage("Salary is paid for the selected period. Please select another period and try again!");
                    return false;
                }
            }
            else
            {
                MessageBox.ShowErrorMessage("Please select period for which you want to pay salary and try again!");
                return false;
            }
            return true;
        }
        protected override StaffReceivableAccountDocument createDocumentInstance()
        {
            return new UnclaimedSalaryDocument();
        }
    }
    class StaffLoanReturnForm : StaffPaymentTransactionFormBase
    {
        public StaffLoanReturnForm(IAccountingClient client, ActivationParameter param)
            : base(client, param)
        {
        }
        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "Staff  Loan Return Form";
            layoutControlReceivableBalance.Text = "Current Loan Balance:";
            layoutGroupStaff.Text = "Loan Return Data";
            HideStaffLoanLayoutControls();
            layoutControlPeriodUnclaimed.HideToCustomization();
        }

        protected override string getTitle()
        {
            return "Staff Loan Return for ";
        }

        protected override int accountID
        {
            get { return (int)PayrollClient.GetSystemParameters(new string[] { "LoanFromStaffAccountID" })[0]; }
        }
        protected override bool additionalValidation()
        {
            if (!_faAmount.Validate())
            {
                return false;
            }
            return true;
        }
        protected override StaffReceivableAccountDocument createDocumentInstance()
        {
            return new StaffLoanPaymentDocument();
        }
    }
    class LoanToShareHolderForm : StaffPaymentTransactionFormBase
    {
        public LoanToShareHolderForm(IAccountingClient client, ActivationParameter param)
            : base(client, param)
        {
        }
        
        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "Loan to Share Holder";
            layoutControlReceivableBalance.Text = "Current Share-holder's Loan Balance:";
            layoutControlAmount.Text = "Loan Amount";
            layoutGroupStaff.Text = "Loan Data";
            HideStaffLoanLayoutControls();
            if (!newDocument)
                btnPrint.Visible = true;
            layoutControlPeriodUnclaimed.HideToCustomization();

            layoutControlSettleFullAmount.HideToCustomization();
        }
        protected override bool IsPrintable
        {
            get
            {
                return true;
            }
        }
        protected override string getTitle()
        {
            return "Share Holder Loan Payment for ";
        }

        protected override int accountID
        {
            get { return (int)PayrollClient.GetSystemParameters(new string[] { "ShareHoldersLoanAccountID" })[0]; }
        }
        protected override StaffReceivableAccountDocument createDocumentInstance()
        {
            return new ShareHoldersLoanDocument();
        }
    }
}
