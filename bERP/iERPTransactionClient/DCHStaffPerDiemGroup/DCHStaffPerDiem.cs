using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHStaffPerDiem : bERPClientDocumentHandler
    {

        public DCHStaffPerDiem()
            : base(typeof(StaffPerDiemForm))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            StaffPerDiemForm f = (StaffPerDiemForm)editor;

            f.LoadData((StaffPerDiemDocument)doc);
        }

    }

}
