﻿namespace BIZNET.iERP.Client
{
    partial class StaffPerDiemForm 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule3 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnPickCostCenter = new INTAPS.Accounting.Client.CostCenterPlaceHolder();
            this.paymentTypeSelector = new BIZNET.iERP.Client.PaymentTypeSelector();
            this.dateReturn = new BIZNET.iERP.Client.BNDualCalendar();
            this.dateLeaving = new BIZNET.iERP.Client.BNDualCalendar();
            this.voucher = new BIZNET.iERP.Client.DocumentSerialPlaceHolder();
            this.datePayment = new BIZNET.iERP.Client.BNDualCalendar();
            this.txtPlaceOfDestination = new DevExpress.XtraEditors.TextEdit();
            this.txtUnitPerDimeAmount = new DevExpress.XtraEditors.TextEdit();
            this.txtDayAdjustment = new DevExpress.XtraEditors.TextEdit();
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.txtServiceCharge = new DevExpress.XtraEditors.TextEdit();
            this.labelStaffPaymentBal = new DevExpress.XtraEditors.LabelControl();
            this.labelBankBalance = new DevExpress.XtraEditors.LabelControl();
            this.labelCashBalance = new DevExpress.XtraEditors.LabelControl();
            this.txtNote = new DevExpress.XtraEditors.MemoEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.buttonPrintForm = new DevExpress.XtraEditors.SimpleButton();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.cmbCasher = new BIZNET.iERP.Client.CashAccountPlaceholder();
            this.cmbBankAccount = new BIZNET.iERP.Client.BankAccountPlaceholder();
            this.txtReference = new DevExpress.XtraEditors.TextEdit();
            this.txtAmount = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlSlipRef = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlNote = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlBankAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlBankBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlCashAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlCashBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlReceivableBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlServiceCharge = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.validationStaffReceivable = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.validateUnitPerDime = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.paymentTypeSelector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlaceOfDestination.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitPerDimeAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayAdjustment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceCharge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCasher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBankAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSlipRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlReceivableBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlServiceCharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationStaffReceivable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validateUnitPerDime)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnPickCostCenter);
            this.layoutControl1.Controls.Add(this.paymentTypeSelector);
            this.layoutControl1.Controls.Add(this.dateReturn);
            this.layoutControl1.Controls.Add(this.dateLeaving);
            this.layoutControl1.Controls.Add(this.voucher);
            this.layoutControl1.Controls.Add(this.datePayment);
            this.layoutControl1.Controls.Add(this.txtPlaceOfDestination);
            this.layoutControl1.Controls.Add(this.txtUnitPerDimeAmount);
            this.layoutControl1.Controls.Add(this.txtDayAdjustment);
            this.layoutControl1.Controls.Add(this.lblTitle);
            this.layoutControl1.Controls.Add(this.txtServiceCharge);
            this.layoutControl1.Controls.Add(this.labelStaffPaymentBal);
            this.layoutControl1.Controls.Add(this.labelBankBalance);
            this.layoutControl1.Controls.Add(this.labelCashBalance);
            this.layoutControl1.Controls.Add(this.txtNote);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.cmbCasher);
            this.layoutControl1.Controls.Add(this.cmbBankAccount);
            this.layoutControl1.Controls.Add(this.txtReference);
            this.layoutControl1.Controls.Add(this.txtAmount);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(447, 217, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(758, 618);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnPickCostCenter
            // 
            this.btnPickCostCenter.account = null;
            this.btnPickCostCenter.AllowAdd = false;
            this.btnPickCostCenter.Location = new System.Drawing.Point(528, 157);
            this.btnPickCostCenter.Name = "btnPickCostCenter";
            this.btnPickCostCenter.OnlyLeafAccount = false;
            this.btnPickCostCenter.Size = new System.Drawing.Size(220, 20);
            this.btnPickCostCenter.TabIndex = 24;
            // 
            // paymentTypeSelector
            // 
            this.paymentTypeSelector.Include_BankTransferFromAccount = true;
            this.paymentTypeSelector.Include_BankTransferFromCash = true;
            this.paymentTypeSelector.Include_Cash = true;
            this.paymentTypeSelector.Include_Check = true;
            this.paymentTypeSelector.Include_CpoFromBank = true;
            this.paymentTypeSelector.Include_CpoFromCash = true;
            this.paymentTypeSelector.Include_Credit = false;
            this.paymentTypeSelector.Location = new System.Drawing.Point(154, 157);
            this.paymentTypeSelector.Name = "paymentTypeSelector";
            this.paymentTypeSelector.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.paymentTypeSelector.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.paymentTypeSelector.Size = new System.Drawing.Size(220, 20);
            this.paymentTypeSelector.StyleController = this.layoutControl1;
            this.paymentTypeSelector.TabIndex = 23;
            // 
            // dateReturn
            // 
            this.dateReturn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateReturn.Location = new System.Drawing.Point(154, 300);
            this.dateReturn.Name = "dateReturn";
            this.dateReturn.ShowEthiopian = true;
            this.dateReturn.ShowGregorian = true;
            this.dateReturn.ShowTime = false;
            this.dateReturn.Size = new System.Drawing.Size(411, 21);
            this.dateReturn.TabIndex = 22;
            this.dateReturn.VerticalLayout = false;
            this.dateReturn.DateTimeChanged += new System.EventHandler(this.dateReturn_EditValueChanged);
            // 
            // dateLeaving
            // 
            this.dateLeaving.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateLeaving.Location = new System.Drawing.Point(154, 270);
            this.dateLeaving.Name = "dateLeaving";
            this.dateLeaving.ShowEthiopian = true;
            this.dateLeaving.ShowGregorian = true;
            this.dateLeaving.ShowTime = false;
            this.dateLeaving.Size = new System.Drawing.Size(411, 21);
            this.dateLeaving.TabIndex = 21;
            this.dateLeaving.VerticalLayout = false;
            this.dateLeaving.DateTimeChanged += new System.EventHandler(this.dateLeaving_EditValueChanged);
            // 
            // voucher
            // 
            this.voucher.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.voucher.Appearance.Options.UseBackColor = true;
            this.voucher.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.voucher.Location = new System.Drawing.Point(7, 82);
            this.voucher.Name = "voucher";
            this.voucher.Size = new System.Drawing.Size(370, 68);
            this.voucher.TabIndex = 25;
            // 
            // datePayment
            // 
            this.datePayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.datePayment.Location = new System.Drawing.Point(384, 85);
            this.datePayment.Name = "datePayment";
            this.datePayment.ShowEthiopian = true;
            this.datePayment.ShowGregorian = true;
            this.datePayment.ShowTime = true;
            this.datePayment.Size = new System.Drawing.Size(364, 63);
            this.datePayment.TabIndex = 20;
            this.datePayment.VerticalLayout = true;
            // 
            // txtPlaceOfDestination
            // 
            this.txtPlaceOfDestination.Location = new System.Drawing.Point(154, 330);
            this.txtPlaceOfDestination.Name = "txtPlaceOfDestination";
            this.txtPlaceOfDestination.Size = new System.Drawing.Size(594, 20);
            this.txtPlaceOfDestination.StyleController = this.layoutControl1;
            this.txtPlaceOfDestination.TabIndex = 19;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "Please enter place of destination";
            this.validationStaffReceivable.SetValidationRule(this.txtPlaceOfDestination, conditionValidationRule1);
            // 
            // txtUnitPerDimeAmount
            // 
            this.txtUnitPerDimeAmount.Location = new System.Drawing.Point(154, 360);
            this.txtUnitPerDimeAmount.Name = "txtUnitPerDimeAmount";
            this.txtUnitPerDimeAmount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtUnitPerDimeAmount.Properties.Mask.EditMask = "#,########0.00;";
            this.txtUnitPerDimeAmount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtUnitPerDimeAmount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtUnitPerDimeAmount.Size = new System.Drawing.Size(594, 20);
            this.txtUnitPerDimeAmount.StyleController = this.layoutControl1;
            this.txtUnitPerDimeAmount.TabIndex = 18;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "Please enter unit per diem amount";
            this.validationStaffReceivable.SetValidationRule(this.txtUnitPerDimeAmount, conditionValidationRule2);
            this.txtUnitPerDimeAmount.EditValueChanged += new System.EventHandler(this.txtUnitPerDimeAmount_EditValueChanged);
            // 
            // txtDayAdjustment
            // 
            this.txtDayAdjustment.Location = new System.Drawing.Point(154, 390);
            this.txtDayAdjustment.Name = "txtDayAdjustment";
            this.txtDayAdjustment.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtDayAdjustment.Properties.Mask.EditMask = "#,########0.00;";
            this.txtDayAdjustment.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDayAdjustment.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtDayAdjustment.Size = new System.Drawing.Size(594, 20);
            this.txtDayAdjustment.StyleController = this.layoutControl1;
            this.txtDayAdjustment.TabIndex = 17;
            this.txtDayAdjustment.EditValueChanged += new System.EventHandler(this.txtDayAdjustment_EditValueChanged);
            // 
            // lblTitle
            // 
            this.lblTitle.AllowHtmlString = true;
            this.lblTitle.Appearance.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lblTitle.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(112)))));
            this.lblTitle.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblTitle.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lblTitle.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.lblTitle.Location = new System.Drawing.Point(5, 5);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(748, 59);
            this.lblTitle.StyleController = this.layoutControl1;
            this.lblTitle.TabIndex = 13;
            this.lblTitle.Text = "Staff Payment Form for ";
            // 
            // txtServiceCharge
            // 
            this.txtServiceCharge.Location = new System.Drawing.Point(154, 450);
            this.txtServiceCharge.Name = "txtServiceCharge";
            this.txtServiceCharge.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtServiceCharge.Properties.Mask.EditMask = "#,########0.00;";
            this.txtServiceCharge.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtServiceCharge.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtServiceCharge.Size = new System.Drawing.Size(594, 20);
            this.txtServiceCharge.StyleController = this.layoutControl1;
            this.txtServiceCharge.TabIndex = 12;
            // 
            // labelStaffPaymentBal
            // 
            this.labelStaffPaymentBal.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStaffPaymentBal.Location = new System.Drawing.Point(154, 247);
            this.labelStaffPaymentBal.Name = "labelStaffPaymentBal";
            this.labelStaffPaymentBal.Size = new System.Drawing.Size(143, 13);
            this.labelStaffPaymentBal.StyleController = this.layoutControl1;
            this.labelStaffPaymentBal.TabIndex = 11;
            this.labelStaffPaymentBal.Text = "Current Balance: 0.00 Birr";
            // 
            // labelBankBalance
            // 
            this.labelBankBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBankBalance.Location = new System.Drawing.Point(522, 214);
            this.labelBankBalance.Name = "labelBankBalance";
            this.labelBankBalance.Size = new System.Drawing.Size(229, 26);
            this.labelBankBalance.StyleController = this.layoutControl1;
            this.labelBankBalance.TabIndex = 8;
            this.labelBankBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // labelCashBalance
            // 
            this.labelCashBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCashBalance.Location = new System.Drawing.Point(522, 184);
            this.labelCashBalance.Name = "labelCashBalance";
            this.labelCashBalance.Size = new System.Drawing.Size(229, 26);
            this.labelCashBalance.StyleController = this.layoutControl1;
            this.labelCashBalance.TabIndex = 7;
            this.labelCashBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(154, 510);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(594, 67);
            this.txtNote.StyleController = this.layoutControl1;
            this.txtNote.TabIndex = 6;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.buttonCancel);
            this.panelControl1.Controls.Add(this.buttonPrintForm);
            this.panelControl1.Controls.Add(this.buttonOk);
            this.panelControl1.Location = new System.Drawing.Point(7, 584);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(744, 27);
            this.panelControl1.TabIndex = 5;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(680, 3);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(59, 23);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonPrintForm
            // 
            this.buttonPrintForm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrintForm.Location = new System.Drawing.Point(25, 1);
            this.buttonPrintForm.Name = "buttonPrintForm";
            this.buttonPrintForm.Size = new System.Drawing.Size(94, 23);
            this.buttonPrintForm.TabIndex = 4;
            this.buttonPrintForm.Text = "&Print Form";
            this.buttonPrintForm.Visible = false;
            this.buttonPrintForm.Click += new System.EventHandler(this.buttonPrintForm_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(602, 3);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(62, 23);
            this.buttonOk.TabIndex = 4;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // cmbCasher
            // 
            this.cmbCasher.Location = new System.Drawing.Point(154, 187);
            this.cmbCasher.Name = "cmbCasher";
            this.cmbCasher.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCasher.Properties.ImmediatePopup = true;
            this.cmbCasher.Size = new System.Drawing.Size(361, 20);
            this.cmbCasher.StyleController = this.layoutControl1;
            this.cmbCasher.TabIndex = 1;
            // 
            // cmbBankAccount
            // 
            this.cmbBankAccount.Location = new System.Drawing.Point(154, 217);
            this.cmbBankAccount.Name = "cmbBankAccount";
            this.cmbBankAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbBankAccount.Properties.ImmediatePopup = true;
            this.cmbBankAccount.Size = new System.Drawing.Size(361, 20);
            this.cmbBankAccount.StyleController = this.layoutControl1;
            this.cmbBankAccount.TabIndex = 1;
            // 
            // txtReference
            // 
            this.txtReference.Location = new System.Drawing.Point(154, 480);
            this.txtReference.Name = "txtReference";
            this.txtReference.Properties.Mask.EditMask = "\\w.*";
            this.txtReference.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtReference.Size = new System.Drawing.Size(594, 20);
            this.txtReference.StyleController = this.layoutControl1;
            this.txtReference.TabIndex = 2;
            conditionValidationRule3.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule3.ErrorText = "Slip Reference cannot be empty";
            this.validationStaffReceivable.SetValidationRule(this.txtReference, conditionValidationRule3);
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(154, 420);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Properties.Mask.EditMask = "#,########0.00;";
            this.txtAmount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtAmount.Properties.ReadOnly = true;
            this.txtAmount.Size = new System.Drawing.Size(594, 20);
            this.txtAmount.StyleController = this.layoutControl1;
            this.txtAmount.TabIndex = 2;
            this.txtAmount.EditValueChanged += new System.EventHandler(this.txtAmount_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlAmount,
            this.layoutControlSlipRef,
            this.layoutControlItem7,
            this.layoutControlNote,
            this.layoutControlBankAccount,
            this.layoutControlBankBalance,
            this.layoutControlCashAccount,
            this.layoutControlCashBalance,
            this.layoutControlReceivableBalance,
            this.layoutControlServiceCharge,
            this.layoutControlGroup2,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem9,
            this.layoutControlItem8});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(758, 618);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlAmount
            // 
            this.layoutControlAmount.Control = this.txtAmount;
            this.layoutControlAmount.CustomizationFormText = "Amount:";
            this.layoutControlAmount.Location = new System.Drawing.Point(0, 410);
            this.layoutControlAmount.Name = "layoutControlAmount";
            this.layoutControlAmount.Size = new System.Drawing.Size(748, 30);
            this.layoutControlAmount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlAmount.Text = "Total Per Diem Amount:";
            this.layoutControlAmount.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlSlipRef
            // 
            this.layoutControlSlipRef.Control = this.txtReference;
            this.layoutControlSlipRef.CustomizationFormText = "Slip Reference:";
            this.layoutControlSlipRef.Location = new System.Drawing.Point(0, 470);
            this.layoutControlSlipRef.Name = "layoutControlSlipRef";
            this.layoutControlSlipRef.Size = new System.Drawing.Size(748, 30);
            this.layoutControlSlipRef.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlSlipRef.Text = "Slip Reference:";
            this.layoutControlSlipRef.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.panelControl1;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 577);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(748, 31);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlNote
            // 
            this.layoutControlNote.Control = this.txtNote;
            this.layoutControlNote.CustomizationFormText = "Note:";
            this.layoutControlNote.Location = new System.Drawing.Point(0, 500);
            this.layoutControlNote.MinSize = new System.Drawing.Size(193, 26);
            this.layoutControlNote.Name = "layoutControlNote";
            this.layoutControlNote.Size = new System.Drawing.Size(748, 77);
            this.layoutControlNote.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlNote.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlNote.Text = "Note:";
            this.layoutControlNote.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlBankAccount
            // 
            this.layoutControlBankAccount.Control = this.cmbBankAccount;
            this.layoutControlBankAccount.CustomizationFormText = "Bank Account:";
            this.layoutControlBankAccount.Location = new System.Drawing.Point(0, 207);
            this.layoutControlBankAccount.MinSize = new System.Drawing.Size(196, 30);
            this.layoutControlBankAccount.Name = "layoutControlBankAccount";
            this.layoutControlBankAccount.Size = new System.Drawing.Size(515, 30);
            this.layoutControlBankAccount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlBankAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlBankAccount.Text = "Payment from Bank Account:";
            this.layoutControlBankAccount.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlBankBalance
            // 
            this.layoutControlBankBalance.Control = this.labelBankBalance;
            this.layoutControlBankBalance.CustomizationFormText = "layoutControlItem8";
            this.layoutControlBankBalance.Location = new System.Drawing.Point(515, 207);
            this.layoutControlBankBalance.MaxSize = new System.Drawing.Size(233, 30);
            this.layoutControlBankBalance.MinSize = new System.Drawing.Size(233, 30);
            this.layoutControlBankBalance.Name = "layoutControlBankBalance";
            this.layoutControlBankBalance.Size = new System.Drawing.Size(233, 30);
            this.layoutControlBankBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlBankBalance.Text = "layoutControlItem8";
            this.layoutControlBankBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlBankBalance.TextToControlDistance = 0;
            this.layoutControlBankBalance.TextVisible = false;
            // 
            // layoutControlCashAccount
            // 
            this.layoutControlCashAccount.Control = this.cmbCasher;
            this.layoutControlCashAccount.CustomizationFormText = "Cash Source:";
            this.layoutControlCashAccount.Location = new System.Drawing.Point(0, 177);
            this.layoutControlCashAccount.MinSize = new System.Drawing.Size(129, 30);
            this.layoutControlCashAccount.Name = "layoutControlCashAccount";
            this.layoutControlCashAccount.Size = new System.Drawing.Size(515, 30);
            this.layoutControlCashAccount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlCashAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlCashAccount.Text = "Payment from Cash Account:";
            this.layoutControlCashAccount.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlCashBalance
            // 
            this.layoutControlCashBalance.Control = this.labelCashBalance;
            this.layoutControlCashBalance.CustomizationFormText = "layoutControlCashBalance";
            this.layoutControlCashBalance.Location = new System.Drawing.Point(515, 177);
            this.layoutControlCashBalance.MaxSize = new System.Drawing.Size(233, 30);
            this.layoutControlCashBalance.MinSize = new System.Drawing.Size(233, 30);
            this.layoutControlCashBalance.Name = "layoutControlCashBalance";
            this.layoutControlCashBalance.Size = new System.Drawing.Size(233, 30);
            this.layoutControlCashBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlCashBalance.Text = "layoutControlCashBalance";
            this.layoutControlCashBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlCashBalance.TextToControlDistance = 0;
            this.layoutControlCashBalance.TextVisible = false;
            // 
            // layoutControlReceivableBalance
            // 
            this.layoutControlReceivableBalance.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlReceivableBalance.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlReceivableBalance.Control = this.labelStaffPaymentBal;
            this.layoutControlReceivableBalance.CustomizationFormText = "layoutControlExpenseAdvance";
            this.layoutControlReceivableBalance.Location = new System.Drawing.Point(0, 237);
            this.layoutControlReceivableBalance.Name = "layoutControlReceivableBalance";
            this.layoutControlReceivableBalance.Size = new System.Drawing.Size(748, 23);
            this.layoutControlReceivableBalance.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlReceivableBalance.Text = "Perdiem Expense: ";
            this.layoutControlReceivableBalance.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlServiceCharge
            // 
            this.layoutControlServiceCharge.Control = this.txtServiceCharge;
            this.layoutControlServiceCharge.CustomizationFormText = "Service Charge:";
            this.layoutControlServiceCharge.Location = new System.Drawing.Point(0, 440);
            this.layoutControlServiceCharge.Name = "layoutControlServiceCharge";
            this.layoutControlServiceCharge.Size = new System.Drawing.Size(748, 30);
            this.layoutControlServiceCharge.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlServiceCharge.Text = "Service Charge:";
            this.layoutControlServiceCharge.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(748, 59);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.Color.MidnightBlue;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.Control = this.lblTitle;
            this.layoutControlItem2.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(900, 59);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(596, 59);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem2.Size = new System.Drawing.Size(748, 59);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtDayAdjustment;
            this.layoutControlItem4.CustomizationFormText = "Number of days:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 380);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(748, 30);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem4.Text = "Number of days:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtUnitPerDimeAmount;
            this.layoutControlItem5.CustomizationFormText = "Unit Per Dime Amount:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 350);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(748, 30);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem5.Text = "Unit Per Dime Amount:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtPlaceOfDestination;
            this.layoutControlItem6.CustomizationFormText = "Place of Destination:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 320);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(748, 30);
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem6.Text = "Place of Destination:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dateLeaving;
            this.layoutControlItem1.CustomizationFormText = "Leave Date:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 260);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(254, 30);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(748, 30);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Text = "Leave Date:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.dateReturn;
            this.layoutControlItem3.CustomizationFormText = "Return Date:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 290);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(254, 30);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(748, 30);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "Return Date:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnPickCostCenter;
            this.layoutControlItem10.CustomizationFormText = "Cost Center:";
            this.layoutControlItem10.Location = new System.Drawing.Point(374, 147);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(374, 30);
            this.layoutControlItem10.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem10.Text = "Cost Center:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.voucher;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 59);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(104, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(374, 88);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "Document Reference";
            this.layoutControlItem11.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.paymentTypeSelector;
            this.layoutControlItem9.CustomizationFormText = "Payment Type";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 147);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(374, 30);
            this.layoutControlItem9.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem9.Text = "Payment Type";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.datePayment;
            this.layoutControlItem8.CustomizationFormText = "Date:";
            this.layoutControlItem8.Location = new System.Drawing.Point(374, 59);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 88);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(150, 88);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(374, 88);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem8.Text = "Date:";
            this.layoutControlItem8.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(140, 13);
            // 
            // validationStaffReceivable
            // 
            this.validationStaffReceivable.ValidateHiddenControls = false;
            // 
            // validateUnitPerDime
            // 
            this.validateUnitPerDime.ValidateHiddenControls = false;
            // 
            // StaffPerDiemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(758, 618);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "StaffPerDiemForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Staff Per Diem Form";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.paymentTypeSelector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlaceOfDestination.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitPerDimeAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayAdjustment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceCharge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbCasher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBankAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSlipRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlReceivableBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlServiceCharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationStaffReceivable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validateUnitPerDime)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CashAccountPlaceholder cmbCasher;
        private BankAccountPlaceholder cmbBankAccount;
        private DevExpress.XtraEditors.TextEdit txtAmount;
        private DevExpress.XtraEditors.TextEdit txtReference;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCashAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlBankAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlSlipRef;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.MemoEdit txtNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlNote;
        private DevExpress.XtraEditors.LabelControl labelCashBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCashBalance;
        private DevExpress.XtraEditors.LabelControl labelBankBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlBankBalance;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validationStaffReceivable;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraEditors.LabelControl labelStaffPaymentBal;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlReceivableBalance;
        private DevExpress.XtraEditors.TextEdit txtServiceCharge;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlServiceCharge;
        private DevExpress.XtraEditors.LabelControl lblTitle;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit txtDayAdjustment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit txtUnitPerDimeAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.TextEdit txtPlaceOfDestination;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.SimpleButton buttonPrintForm;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validateUnitPerDime;
        private BIZNET.iERP.Client.BNDualCalendar datePayment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private BIZNET.iERP.Client.BNDualCalendar dateLeaving;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private BIZNET.iERP.Client.BNDualCalendar dateReturn;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private PaymentTypeSelector paymentTypeSelector;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private INTAPS.Accounting.Client.CostCenterPlaceHolder btnPickCostCenter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DocumentSerialPlaceHolder voucher;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
    }
}