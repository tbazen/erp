﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;
using BIZNET.iERP.Client.DCHStaffPerDiemGroup;


namespace BIZNET.iERP.Client
{
    public partial class StaffPerDiemForm : XtraForm
    {
        StaffPerDiemDocument m_doc = null;
        private bool newDocument = true;
        private Employee m_Employee;
        private PerDiemDayAdjustmentValidationRule m_AdjustmentValidation;
        private int m_perDiemAccountID;
        private string employeeNameWithID;
        IAccountingClient _client;
        ActivationParameter _activation;
        PaymentMethodAndBalanceController _paymentController;
        public StaffPerDiemForm(IAccountingClient client,ActivationParameter activation)
        {
            InitializeComponent();
            _activation = activation;
            _client = client;
           
            voucher.setKeys(typeof(StaffReceivableAccountDocument), "voucher");
            _paymentController = new PaymentMethodAndBalanceController(paymentTypeSelector, layoutControlBankAccount, cmbBankAccount, layoutControlBankBalance, labelBankBalance, layoutControlCashAccount, cmbCasher, layoutControlCashBalance, labelCashBalance, layoutControlServiceCharge, layoutControlSlipRef, datePayment, _activation);
            txtServiceCharge.LostFocus += Control_LostFocus;
            txtAmount.LostFocus += Control_LostFocus;   
            txtReference.LostFocus += Control_LostFocus;
            txtPlaceOfDestination.LostFocus += Control_LostFocus;
            this.btnPickCostCenter.SetByID(Globals.currentCostCenterID);
            try
            {
                this.voucher.Enabled = !activation.disableReference;
                this.datePayment.Enabled = !activation.disableDate;
                _paymentController.IgnoreEvents();
                _paymentController.PaymentTypeSelector.PaymentMethod= _activation.paymentMethod;
                _paymentController.assetAccountID= _activation.assetAccountID;
                dateLeaving.DateTime = DateTime.Now;
                dateReturn.DateTime = DateTime.Now;
                setEmployee(_activation.employeeID);
                _paymentController.AddCostCenterAccountItem(getEmployeeAccountID(CostCenter.ROOT_COST_CENTER), labelStaffPaymentBal);
                InitializeValidationRules();
            }
            finally
            {
                _paymentController.AcceptEvents();
            }

            SetFormTitle();
        }
        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validateUnitPerDime.Validate(control);
            validationStaffReceivable.Validate(control);
        }
        void setEmployee(int empID)
        {
            if (empID == -1)
            {
                m_Employee = null;
            }
            else
            {
                m_Employee = PayrollClient.GetEmployee(empID);
            }
        }

        int accountID
        {
            get
            {
                return (int)PayrollClient.GetSystemParameters(new string[] { "staffPerDiemAccountID" })[0];
            }
        }

        int getEmployeeAccountID(int costCenterID)
        {
            int aid = accountID;
            if (aid == -1)
                return -1;
            if (m_Employee == null)
                return -1;
            CostCenterAccount csa = AccountingClient.GetCostCenterAccount(costCenterID, iERPTransactionClient.GetEmployeeAccountID(m_Employee, accountID));
            if (csa == null)
                return -1;
            return csa.id;
        }

        private void LoadDefaultPerDiemAmount(double grossSalary)
        {
            double perDiem = grossSalary * 0.04;
            double maxPerDiemAmount = 150;
            if (perDiem > 150)
            {
                txtAmount.Text = TSConstants.FormatBirr(maxPerDiemAmount);
                txtUnitPerDimeAmount.Text = TSConstants.FormatBirr(maxPerDiemAmount);
            }
            else
            {
                txtAmount.Text = TSConstants.FormatBirr(perDiem);
                txtUnitPerDimeAmount.Text = TSConstants.FormatBirr(perDiem);
            }
        }
        
        private void SetFormTitle()
        {
            if (m_Employee != null)
                employeeNameWithID = m_Employee.EmployeeNameID;
            else
                employeeNameWithID = " [not set] ";
            string title = "Per Diem Payment for ";


            lblTitle.Text = "<size=14><color=#360087><b>" + title + "</b></color></size>" +
                "<size=14><color=#C30013><b>" + employeeNameWithID + "</b></color></size>" +
                "<size=14><color=#360087><b>" + _paymentController.PaymentSource+ "</b></color></size>";
        }
        private void InitializeValidationRules()
        {
            InitializePaymentAmountValidation();

        }

        private void InitializePaymentAmountValidation()
        {
            NonEmptyNumericValidationRule perDiemAmountValidation = new NonEmptyNumericValidationRule();
            perDiemAmountValidation.ErrorText = "Amount cannot be blank and cannot contain a value of zero";
            perDiemAmountValidation.ErrorType = ErrorType.Default;
            validationStaffReceivable.SetValidationRule(txtAmount, perDiemAmountValidation);
        }

        private void InitializeDayAdjustmentValidation()
        {

            TimeSpan span = dateReturn.DateTime.Subtract(dateLeaving.DateTime);
            double noOfDays = span.TotalDays;

            m_AdjustmentValidation = new PerDiemDayAdjustmentValidationRule(noOfDays);
            m_AdjustmentValidation.ErrorType = ErrorType.Default;
            validationStaffReceivable.SetValidationRule(txtDayAdjustment, m_AdjustmentValidation);

        }
        

        internal void LoadData(StaffPerDiemDocument staffPerDiemDocument)
        {
            m_doc = staffPerDiemDocument;
            if (staffPerDiemDocument == null)
            {
                buttonPrintForm.Visible = false;
                int defaultCostCenterID = Convert.ToInt32(iERPTransactionClient.GetSystemParamter("mainCostCenterID"));
                if (defaultCostCenterID == 0)
                {
                    MessageBox.ShowErrorMessage("Configuration inconsistency: Head Quarter main cost center is not set in settings. Please set it and try again!");
                    return;
                }
                btnPickCostCenter.SetByID(defaultCostCenterID);
                ResetControls();
            }
            else
            {
                try
                {
                    _paymentController.IgnoreEvents();
                    buttonPrintForm.Visible = true;
                    _paymentController.PaymentTypeSelector.PaymentMethod=m_doc.paymentMethod;
                    setEmployee(m_doc.employeeID);
                    newDocument = false;
                    PopulateControlsForUpdate(staffPerDiemDocument);
                    _paymentController.AddCostCenterAccountItem(getEmployeeAccountID(CostCenter.ROOT_COST_CENTER), labelStaffPaymentBal);

                }
                finally
                {
                    _paymentController.AcceptEvents();
                }
                SetFormTitle();
            }
        }
        private void ResetControls()
        {
            datePayment.DateTime = DateTime.Now;
            txtAmount.Text = "";
            txtReference.Text = "";
            txtNote.Text = "";
            dateLeaving.DateTime = DateTime.Now;
            dateReturn.DateTime = DateTime.Now;
            txtDayAdjustment.Text = "";
            voucher.setReference(-1, null);
        }
        private void PopulateControlsForUpdate(StaffPerDiemDocument staffPerDiemDocument)
        {
            datePayment.DateTime = staffPerDiemDocument.DocumentDate;
            int defaultCostCenterID = Convert.ToInt32(iERPTransactionClient.GetSystemParamter("mainCostCenterID"));
            if (defaultCostCenterID == 0)
            {
                MessageBox.ShowErrorMessage("Configuration inconsistency: Head Quarter main cost center is not set in settings. Please set it and try again!");
                return;
            }
            int costCenterID = staffPerDiemDocument.costCenterID == 0 ? defaultCostCenterID : staffPerDiemDocument.costCenterID;
            btnPickCostCenter.SetByID(costCenterID);
            voucher.setReference(staffPerDiemDocument.AccountDocumentID, staffPerDiemDocument.voucher);
            dateLeaving.DateTime = staffPerDiemDocument.leaveDate;
            dateReturn.DateTime = staffPerDiemDocument.returnDate;
            txtPlaceOfDestination.Text = staffPerDiemDocument.placeOfDestination;
            txtDayAdjustment.Text = staffPerDiemDocument.dayAdjustment.ToString();
            _paymentController.assetAccountID = staffPerDiemDocument.assetAccountID;
            txtUnitPerDimeAmount.Text = TSConstants.FormatBirr(staffPerDiemDocument.unitPerdiem);
            txtAmount.Text = TSConstants.FormatBirr(staffPerDiemDocument.amount);
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(staffPerDiemDocument.paymentMethod))
                txtServiceCharge.Text = staffPerDiemDocument.serviceChargeAmount.ToString("N");
            SetReferenceNumber(false);
            txtNote.Text = staffPerDiemDocument.ShortDescription;
        }

        


        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (validationStaffReceivable.Validate())
                {
                    if (dateLeaving.DateTime.CompareTo(dateReturn.DateTime) >= 0)
                    {
                        MessageBox.ShowErrorMessage("Date of leaving cannot be equal to or greater than date of return. Please adjust dates and try again!");
                        return;
                    }
                    if (btnPickCostCenter.account == null)
                    {
                        MessageBox.ShowErrorMessage("Please pick cost center!");
                        return;
                    }
                    CostCenter costCenter = AccountingClient.GetAccount<CostCenter>(btnPickCostCenter.GetAccountID());
                    if (costCenter.childCount > 1)
                    {
                        MessageBox.ShowErrorMessage("Please pick cost center with no child");
                    }
                    if (!ValidateEmployeeCostCenter(costCenter.id))
                    {
                        MessageBox.ShowErrorMessage("Please pick a project/branch or project/branch division cost center!");
                        return;
                    }
                    int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;

                    m_doc = new StaffPerDiemDocument();
                    m_doc.AccountDocumentID = docID;
                    m_doc.costCenterID = costCenter.id;
                    m_doc.employeeID = m_Employee.id;
                    m_doc.DocumentDate = datePayment.DateTime;
                    m_doc.voucher = voucher.getReference();
                    m_doc.paymentMethod = paymentTypeSelector.PaymentMethod;
                    m_doc.leaveDate = dateLeaving.DateTime.Date;
                    m_doc.returnDate = dateReturn.DateTime.Date;
                    m_doc.placeOfDestination = txtPlaceOfDestination.Text;
                    m_doc.dayAdjustment = txtDayAdjustment.Text == "" ? 1 : double.Parse(txtDayAdjustment.Text);
                    m_doc.unitPerdiem = double.Parse(txtUnitPerDimeAmount.Text);
                    m_doc.assetAccountID = _paymentController.assetAccountID;
                    m_doc.amount = double.Parse(txtAmount.Text);

                    if (!layoutControlSlipRef.IsHidden)
                    {
                        bool updateDoc = m_doc == null ? false : true;
                        SetReferenceNumber(updateDoc);
                    }
                    m_doc.ShortDescription = txtNote.Text;
                    if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(_paymentController.PaymentTypeSelector.PaymentMethod))
                    {
                        using (ServiceChargePayment serviceCharge = new ServiceChargePayment(employeeNameWithID))
                        {
                            serviceCharge.StartPosition = FormStartPosition.CenterScreen;
                            serviceCharge.ShowInTaskbar = false;
                            if (serviceCharge.ShowDialog() == DialogResult.OK)
                            {
                                m_doc.serviceChargePayer = serviceCharge.ServiceChargePayer;
                                double serviceChargeAmount = txtServiceCharge.Text == "" ? 0 : double.Parse(txtServiceCharge.Text);
                                m_doc.serviceChargeAmount = serviceChargeAmount;
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                    _client.PostGenericDocument(m_doc);
                    if (!(_client is DocumentScheduler))
                    {
                        ShowPrintForm();
                        string message = docID == -1 ? "Payment successfully saved!" : "Payment successfully updated!";
                        MessageBox.ShowSuccessMessage(message);
                    }
                    Close();
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }

            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }
        private bool ValidateEmployeeCostCenter(int costCenterID)
        {
            Project[] projects = iERPTransactionClient.GetAllProjects();
            foreach (Project project in projects)
            {
                if (project.costCenterID == costCenterID)
                    return true;
                foreach (int divCostCenter in project.projectData.projectDivisions)
                {
                    if (costCenterID == divCostCenter)
                        return true;
                }
            }
            return false;
        }
        private void SetReferenceNumber(bool updateDoc)
        {
            switch (paymentTypeSelector.PaymentMethod)
            {
                case BizNetPaymentMethod.Check:
                    if (newDocument)
                        m_doc.checkNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.checkNumber;
                        else
                            m_doc.checkNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.CPOFromBankAccount:
                    if (newDocument)
                        m_doc.cpoNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.cpoNumber;
                        else
                            m_doc.cpoNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.BankTransferFromCash:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    if (newDocument)
                        m_doc.slipReferenceNo = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.slipReferenceNo;
                        else
                            m_doc.slipReferenceNo = txtReference.Text;
                    break;
                default:
                    break;
            }
        }


        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        
        private void txtAmount_EditValueChanged(object sender, EventArgs e)
        {
            validationStaffReceivable.Validate(txtAmount);
        }

        private void txtDayAdjustment_EditValueChanged(object sender, EventArgs e)
        {
            if (txtDayAdjustment.Text != "")
            {
                if (double.Parse(txtDayAdjustment.Text) == 0)
                    LoadDefaultPerDiemAmount(m_Employee.grossSalary);
                else
                {
                    // if (validationStaffReceivable.Validate(dateLeaving))
                    //{
                    InitializeDayAdjustmentValidation();
                    bool validate = validationStaffReceivable.Validate(txtDayAdjustment);

                    if (validate)
                    {
                        if (txtUnitPerDimeAmount.Text != "")
                        {
                            double perDiemAmount = double.Parse(txtUnitPerDimeAmount.Text);
                            txtAmount.Text = Math.Round(perDiemAmount * double.Parse(txtDayAdjustment.Text), 2).ToString();
                        }
                    }
                    //}
                }
            }
            else
            {
                //dateLeaving.DateTime = dateReturn.DateTime = DateTime.Now;
                if (m_Employee != null)
                    LoadDefaultPerDiemAmount(m_Employee.grossSalary);
            }
        }

        private void txtUnitPerDimeAmount_EditValueChanged(object sender, EventArgs e)
        {
            InitializeUnitPerDiemValidation();
            validateUnitPerDime.Validate();
            //{
                if (txtUnitPerDimeAmount.Text != "")
                {
                    if (double.Parse(txtUnitPerDimeAmount.Text) != 0)
                    {
                        if (txtDayAdjustment.Text != "")
                        {
                            if (double.Parse(txtDayAdjustment.Text) != 0)
                            {
                                double perDiemAmount = double.Parse(txtUnitPerDimeAmount.Text);
                                txtAmount.Text = Math.Round(perDiemAmount * double.Parse(txtDayAdjustment.Text), 2).ToString();

                            }
                        }
                    }
                }
           // }
        }

        private void InitializeUnitPerDiemValidation()
        {
            if (m_Employee != null)
            {
                PerDiemAmountValidationRule unitPerDiemAmountValidation = new PerDiemAmountValidationRule(m_Employee.grossSalary);
                unitPerDiemAmountValidation.ErrorText = "This per diem amount will be added to the gross salary and taxed since the amount either exceeds 4% of the gross salary or is greater than 150 birr";
                unitPerDiemAmountValidation.ErrorType = ErrorType.Warning;
                validateUnitPerDime.SetValidationRule(txtUnitPerDimeAmount, unitPerDiemAmountValidation);
            }
        }

        private void dateLeaving_EditValueChanged(object sender, EventArgs e)
        {
            TimeSpan span;
            if (dateReturn.DateTime.Date.CompareTo(dateLeaving.DateTime.Date) > 0)
            {
                span = dateReturn.DateTime.Date.Subtract(dateLeaving.DateTime.Date);
                txtDayAdjustment.Text = span.TotalDays.ToString();
            }
            else
            {
                MessageBox.ShowErrorMessage("Date of leaving cannot be equal to or greater than date of return. Please adjust dates and try again!");
            }
        }

        private void dateReturn_EditValueChanged(object sender, EventArgs e)
        {
            TimeSpan span;
            if (dateReturn.DateTime.Date.CompareTo(dateLeaving.DateTime.Date) > 0)
            {
                span = dateReturn.DateTime.Subtract(dateLeaving.DateTime);
                txtDayAdjustment.Text = span.TotalDays.ToString();
            }
            else
            {
                MessageBox.ShowErrorMessage("Date of leaving cannot be equal to or greater than date of return. Please adjust dates and try again!");
            }
        }

        private void buttonPrintForm_Click(object sender, EventArgs e)
        {
            try
            {
                //if (m_doc == null || m_doc.AccountDocumentID == -1)
                //{
                //    MessageBox.ShowErrorMessage("Save the document first");
                //    return;
                //}
                ShowPrintForm();
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void ShowPrintForm()
        {
            CompanyProfile prof = iERPTransactionClient.GetSystemParamter("companyProfile") as CompanyProfile;
            PrintFormViewer p = new PrintFormViewer();
            StaffPerdiemPrintForm pf = new StaffPerdiemPrintForm();
            StaffPerdiemData data = new StaffPerdiemData();
            data.Data.AddDataRow(prof.Name, m_doc.DocumentDate.ToShortDateString(), m_doc.voucher==null?"":m_doc.voucher.reference, PayrollClient.GetEmployee(m_doc.employeeID).EmployeeNameID
                , m_doc.leaveDate.ToShortDateString()
                , m_doc.returnDate.ToShortDateString()
                , Math.Round(m_doc.dayAdjustment, 2).ToString("#,#0.00")
                , m_doc.placeOfDestination
                , Math.Round(m_doc.amount / m_doc.dayAdjustment, 2).ToString("#,#0.00")
                , Math.Round(m_doc.amount, 2).ToString("#,#0.00")
                , prof.Logo);
            pf.DataSource = data;
            p.LoadReport(pf);
            p.ShowDialog();
        }

    }
}
