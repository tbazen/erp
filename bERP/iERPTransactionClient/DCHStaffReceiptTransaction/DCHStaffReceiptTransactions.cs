using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHStaffReceiptTransactionBase : bERPClientDocumentHandler
    {
        public DCHStaffReceiptTransactionBase(Type formType)
            : base(formType)
        {
        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            StaffReceiptTransactionFormBase f = (StaffReceiptTransactionFormBase)editor;
            f.LoadData((StaffReturnDocument)doc);
        }
    }
    public class DCHExpenseAdvanceReturn : DCHStaffReceiptTransactionBase
    {
        public DCHExpenseAdvanceReturn()
            : base(typeof(ExpenseAdvanceReturnForm))
        {
        }
    }

    public class DCHShortTermLoanReturn : DCHStaffReceiptTransactionBase
    {
        public DCHShortTermLoanReturn()
            : base(typeof(ShortTermLoanReturnForm))
        {
        }
    }
    public class DCHLongTermLoanReturn : DCHStaffReceiptTransactionBase
    {
        public DCHLongTermLoanReturn()
            : base(typeof(LongTermLoanReturnForm))
        {
        }
    }
    public class DCHLoanFromStaff : DCHStaffReceiptTransactionBase
    {
        public DCHLoanFromStaff()
            : base(typeof(LoanFromStaffForm))
        {
        }
    }
    public class DCHShareHoldersLoanReturn : DCHStaffReceiptTransactionBase
    {
        public DCHShareHoldersLoanReturn()
            : base(typeof(ShareHoldersLoanReturnForm))
        {
        }
    }
}
