﻿namespace BIZNET.iERP.Client
{
    partial class StaffReceiptTransactionFormBase 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.hyperLinkView = new DevExpress.XtraEditors.HyperLinkEdit();
            this.hyperLinkRemove = new DevExpress.XtraEditors.HyperLinkEdit();
            this.hyperLinkPickDoc = new DevExpress.XtraEditors.HyperLinkEdit();
            this.labelReturnFrom = new DevExpress.XtraEditors.LabelControl();
            this.voucher = new BIZNET.iERP.Client.DocumentSerialPlaceHolder();
            this.checkSettleFullAmount = new DevExpress.XtraEditors.CheckEdit();
            this.paymentTypeSelector = new BIZNET.iERP.Client.PaymentTypeSelector();
            this.datePayment = new BIZNET.iERP.Client.BNDualCalendar();
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.txtServiceCharge = new DevExpress.XtraEditors.TextEdit();
            this.labelStaffPaymentBal = new DevExpress.XtraEditors.LabelControl();
            this.labelBankBalance = new DevExpress.XtraEditors.LabelControl();
            this.labelCashBalance = new DevExpress.XtraEditors.LabelControl();
            this.txtNote = new DevExpress.XtraEditors.MemoEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.cmbCasher = new BIZNET.iERP.Client.CashAccountPlaceholder();
            this.cmbBankAccount = new BIZNET.iERP.Client.BankAccountPlaceholder();
            this.txtReference = new DevExpress.XtraEditors.TextEdit();
            this.txtAmount = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutAmountLabel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlNote = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlBankAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlBankBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlCashAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlCashBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlReceivableBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layotControlPaymentType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlSettleFullAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlSlipRef = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlServiceCharge = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutReturnFromLabel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutReturnFromPick = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutReturnFromRemove = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutReturnFromView = new DevExpress.XtraLayout.LayoutControlItem();
            this.validationStaffReturn = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkView.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkRemove.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkPickDoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSettleFullAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentTypeSelector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceCharge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCasher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBankAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAmountLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlReceivableBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layotControlPaymentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSettleFullAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSlipRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlServiceCharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutReturnFromLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutReturnFromPick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutReturnFromRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutReturnFromView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationStaffReturn)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.hyperLinkView);
            this.layoutControl1.Controls.Add(this.hyperLinkRemove);
            this.layoutControl1.Controls.Add(this.hyperLinkPickDoc);
            this.layoutControl1.Controls.Add(this.labelReturnFrom);
            this.layoutControl1.Controls.Add(this.voucher);
            this.layoutControl1.Controls.Add(this.checkSettleFullAmount);
            this.layoutControl1.Controls.Add(this.paymentTypeSelector);
            this.layoutControl1.Controls.Add(this.datePayment);
            this.layoutControl1.Controls.Add(this.lblTitle);
            this.layoutControl1.Controls.Add(this.txtServiceCharge);
            this.layoutControl1.Controls.Add(this.labelStaffPaymentBal);
            this.layoutControl1.Controls.Add(this.labelBankBalance);
            this.layoutControl1.Controls.Add(this.labelCashBalance);
            this.layoutControl1.Controls.Add(this.txtNote);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.cmbCasher);
            this.layoutControl1.Controls.Add(this.cmbBankAccount);
            this.layoutControl1.Controls.Add(this.txtReference);
            this.layoutControl1.Controls.Add(this.txtAmount);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(447, 217, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(738, 523);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // hyperLinkView
            // 
            this.hyperLinkView.EditValue = "Open";
            this.hyperLinkView.Location = new System.Drawing.Point(502, 307);
            this.hyperLinkView.Name = "hyperLinkView";
            this.hyperLinkView.Size = new System.Drawing.Size(226, 20);
            this.hyperLinkView.StyleController = this.layoutControl1;
            this.hyperLinkView.TabIndex = 22;
            this.hyperLinkView.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.hyperLinkView_OpenLink);
            // 
            // hyperLinkRemove
            // 
            this.hyperLinkRemove.EditValue = "Remove";
            this.hyperLinkRemove.Location = new System.Drawing.Point(502, 337);
            this.hyperLinkRemove.Name = "hyperLinkRemove";
            this.hyperLinkRemove.Size = new System.Drawing.Size(226, 20);
            this.hyperLinkRemove.StyleController = this.layoutControl1;
            this.hyperLinkRemove.TabIndex = 21;
            this.hyperLinkRemove.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.hyperLinkRemove_OpenLink);
            // 
            // hyperLinkPickDoc
            // 
            this.hyperLinkPickDoc.EditValue = "Pick";
            this.hyperLinkPickDoc.Location = new System.Drawing.Point(502, 367);
            this.hyperLinkPickDoc.Name = "hyperLinkPickDoc";
            this.hyperLinkPickDoc.Size = new System.Drawing.Size(226, 20);
            this.hyperLinkPickDoc.StyleController = this.layoutControl1;
            this.hyperLinkPickDoc.TabIndex = 20;
            this.hyperLinkPickDoc.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.hyperLinkPickDoc_OpenLink);
            // 
            // labelReturnFrom
            // 
            this.labelReturnFrom.AutoEllipsis = true;
            this.labelReturnFrom.Location = new System.Drawing.Point(156, 307);
            this.labelReturnFrom.Name = "labelReturnFrom";
            this.labelReturnFrom.Size = new System.Drawing.Size(336, 80);
            this.labelReturnFrom.StyleController = this.layoutControl1;
            this.labelReturnFrom.TabIndex = 19;
            this.labelReturnFrom.Text = "labelControl1";
            // 
            // voucher
            // 
            this.voucher.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.voucher.Appearance.Options.UseBackColor = true;
            this.voucher.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.voucher.Location = new System.Drawing.Point(7, 82);
            this.voucher.Name = "voucher";
            this.voucher.Size = new System.Drawing.Size(368, 68);
            this.voucher.TabIndex = 18;
            // 
            // checkSettleFullAmount
            // 
            this.checkSettleFullAmount.Location = new System.Drawing.Point(481, 277);
            this.checkSettleFullAmount.Name = "checkSettleFullAmount";
            this.checkSettleFullAmount.Properties.Caption = "Settle Full Amount";
            this.checkSettleFullAmount.Size = new System.Drawing.Size(247, 19);
            this.checkSettleFullAmount.StyleController = this.layoutControl1;
            this.checkSettleFullAmount.TabIndex = 17;
            // 
            // paymentTypeSelector
            // 
            this.paymentTypeSelector.Include_BankTransferFromAccount = true;
            this.paymentTypeSelector.Include_BankTransferFromCash = true;
            this.paymentTypeSelector.Include_Cash = true;
            this.paymentTypeSelector.Include_Check = true;
            this.paymentTypeSelector.Include_CpoFromBank = true;
            this.paymentTypeSelector.Include_CpoFromCash = true;
            this.paymentTypeSelector.Include_Credit = false;
            this.paymentTypeSelector.Location = new System.Drawing.Point(156, 157);
            this.paymentTypeSelector.Name = "paymentTypeSelector";
            this.paymentTypeSelector.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.paymentTypeSelector.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.paymentTypeSelector.Size = new System.Drawing.Size(572, 20);
            this.paymentTypeSelector.StyleController = this.layoutControl1;
            this.paymentTypeSelector.TabIndex = 16;
            // 
            // datePayment
            // 
            this.datePayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.datePayment.Location = new System.Drawing.Point(382, 85);
            this.datePayment.Name = "datePayment";
            this.datePayment.ShowEthiopian = true;
            this.datePayment.ShowGregorian = true;
            this.datePayment.ShowTime = true;
            this.datePayment.Size = new System.Drawing.Size(346, 63);
            this.datePayment.TabIndex = 15;
            this.datePayment.VerticalLayout = true;
            // 
            // lblTitle
            // 
            this.lblTitle.AllowHtmlString = true;
            this.lblTitle.Appearance.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lblTitle.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(112)))));
            this.lblTitle.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblTitle.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lblTitle.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.lblTitle.Location = new System.Drawing.Point(5, 5);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(728, 59);
            this.lblTitle.StyleController = this.layoutControl1;
            this.lblTitle.TabIndex = 13;
            this.lblTitle.Text = "Staff Return Form for ";
            // 
            // txtServiceCharge
            // 
            this.txtServiceCharge.Location = new System.Drawing.Point(156, 466);
            this.txtServiceCharge.Name = "txtServiceCharge";
            this.txtServiceCharge.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtServiceCharge.Properties.Mask.EditMask = "#,########0.00;";
            this.txtServiceCharge.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtServiceCharge.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtServiceCharge.Size = new System.Drawing.Size(572, 20);
            this.txtServiceCharge.StyleController = this.layoutControl1;
            this.txtServiceCharge.TabIndex = 12;
            // 
            // labelStaffPaymentBal
            // 
            this.labelStaffPaymentBal.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStaffPaymentBal.Location = new System.Drawing.Point(156, 247);
            this.labelStaffPaymentBal.Name = "labelStaffPaymentBal";
            this.labelStaffPaymentBal.Size = new System.Drawing.Size(452, 20);
            this.labelStaffPaymentBal.StyleController = this.layoutControl1;
            this.labelStaffPaymentBal.TabIndex = 11;
            this.labelStaffPaymentBal.Text = "Current Balance: 0.00 Birr";
            // 
            // labelBankBalance
            // 
            this.labelBankBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBankBalance.Location = new System.Drawing.Point(502, 214);
            this.labelBankBalance.Name = "labelBankBalance";
            this.labelBankBalance.Size = new System.Drawing.Size(229, 26);
            this.labelBankBalance.StyleController = this.layoutControl1;
            this.labelBankBalance.TabIndex = 8;
            this.labelBankBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // labelCashBalance
            // 
            this.labelCashBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCashBalance.Location = new System.Drawing.Point(502, 184);
            this.labelCashBalance.Name = "labelCashBalance";
            this.labelCashBalance.Size = new System.Drawing.Size(229, 26);
            this.labelCashBalance.StyleController = this.layoutControl1;
            this.labelCashBalance.TabIndex = 7;
            this.labelCashBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(156, 427);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(572, 29);
            this.txtNote.StyleController = this.layoutControl1;
            this.txtNote.TabIndex = 6;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnPrint);
            this.panelControl1.Controls.Add(this.buttonCancel);
            this.panelControl1.Controls.Add(this.buttonOk);
            this.panelControl1.Location = new System.Drawing.Point(7, 493);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(724, 23);
            this.panelControl1.TabIndex = 5;
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(0, 3);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(104, 23);
            this.btnPrint.TabIndex = 6;
            this.btnPrint.Text = "&Print Form";
            this.btnPrint.Visible = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(660, 1);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(59, 23);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(582, 1);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(62, 23);
            this.buttonOk.TabIndex = 4;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // cmbCasher
            // 
            this.cmbCasher.Location = new System.Drawing.Point(156, 187);
            this.cmbCasher.Name = "cmbCasher";
            this.cmbCasher.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCasher.Properties.ImmediatePopup = true;
            this.cmbCasher.Size = new System.Drawing.Size(339, 20);
            this.cmbCasher.StyleController = this.layoutControl1;
            this.cmbCasher.TabIndex = 1;
            // 
            // cmbBankAccount
            // 
            this.cmbBankAccount.Location = new System.Drawing.Point(156, 217);
            this.cmbBankAccount.Name = "cmbBankAccount";
            this.cmbBankAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbBankAccount.Properties.ImmediatePopup = true;
            this.cmbBankAccount.Size = new System.Drawing.Size(339, 20);
            this.cmbBankAccount.StyleController = this.layoutControl1;
            this.cmbBankAccount.TabIndex = 1;
            // 
            // txtReference
            // 
            this.txtReference.Location = new System.Drawing.Point(156, 397);
            this.txtReference.Name = "txtReference";
            this.txtReference.Properties.Mask.EditMask = "\\w.*";
            this.txtReference.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtReference.Size = new System.Drawing.Size(572, 20);
            this.txtReference.StyleController = this.layoutControl1;
            this.txtReference.TabIndex = 2;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "Slip Reference cannot be empty";
            this.validationStaffReturn.SetValidationRule(this.txtReference, conditionValidationRule1);
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(156, 277);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtAmount.Properties.Mask.EditMask = "#,########0.00;";
            this.txtAmount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtAmount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtAmount.Size = new System.Drawing.Size(315, 20);
            this.txtAmount.StyleController = this.layoutControl1;
            this.txtAmount.TabIndex = 2;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutAmountLabel,
            this.layoutControlItem7,
            this.layoutControlNote,
            this.layoutControlBankAccount,
            this.layoutControlBankBalance,
            this.layoutControlCashAccount,
            this.layoutControlCashBalance,
            this.layoutControlReceivableBalance,
            this.layoutControlGroup2,
            this.layotControlPaymentType,
            this.layoutControlSettleFullAmount,
            this.layoutControlItem3,
            this.layoutControlItem1,
            this.layoutControlSlipRef,
            this.layoutControlServiceCharge,
            this.layoutReturnFromLabel,
            this.layoutReturnFromPick,
            this.layoutReturnFromRemove,
            this.layoutReturnFromView});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(738, 523);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutAmountLabel
            // 
            this.layoutAmountLabel.Control = this.txtAmount;
            this.layoutAmountLabel.CustomizationFormText = "Amount:";
            this.layoutAmountLabel.Location = new System.Drawing.Point(0, 267);
            this.layoutAmountLabel.Name = "layoutAmountLabel";
            this.layoutAmountLabel.Size = new System.Drawing.Size(471, 30);
            this.layoutAmountLabel.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutAmountLabel.Text = "Return Amount:";
            this.layoutAmountLabel.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.panelControl1;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 486);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(728, 27);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlNote
            // 
            this.layoutControlNote.Control = this.txtNote;
            this.layoutControlNote.CustomizationFormText = "Note:";
            this.layoutControlNote.Location = new System.Drawing.Point(0, 417);
            this.layoutControlNote.MinSize = new System.Drawing.Size(193, 26);
            this.layoutControlNote.Name = "layoutControlNote";
            this.layoutControlNote.Size = new System.Drawing.Size(728, 39);
            this.layoutControlNote.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlNote.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlNote.Text = "Note:";
            this.layoutControlNote.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlBankAccount
            // 
            this.layoutControlBankAccount.Control = this.cmbBankAccount;
            this.layoutControlBankAccount.CustomizationFormText = "Bank Account:";
            this.layoutControlBankAccount.Location = new System.Drawing.Point(0, 207);
            this.layoutControlBankAccount.MinSize = new System.Drawing.Size(196, 30);
            this.layoutControlBankAccount.Name = "layoutControlBankAccount";
            this.layoutControlBankAccount.Size = new System.Drawing.Size(495, 30);
            this.layoutControlBankAccount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlBankAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlBankAccount.Text = "Bank Account:";
            this.layoutControlBankAccount.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlBankBalance
            // 
            this.layoutControlBankBalance.Control = this.labelBankBalance;
            this.layoutControlBankBalance.CustomizationFormText = "layoutControlItem8";
            this.layoutControlBankBalance.Location = new System.Drawing.Point(495, 207);
            this.layoutControlBankBalance.MaxSize = new System.Drawing.Size(233, 30);
            this.layoutControlBankBalance.MinSize = new System.Drawing.Size(233, 30);
            this.layoutControlBankBalance.Name = "layoutControlBankBalance";
            this.layoutControlBankBalance.Size = new System.Drawing.Size(233, 30);
            this.layoutControlBankBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlBankBalance.Text = "layoutControlItem8";
            this.layoutControlBankBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlBankBalance.TextToControlDistance = 0;
            this.layoutControlBankBalance.TextVisible = false;
            // 
            // layoutControlCashAccount
            // 
            this.layoutControlCashAccount.Control = this.cmbCasher;
            this.layoutControlCashAccount.CustomizationFormText = "Cash Source:";
            this.layoutControlCashAccount.Location = new System.Drawing.Point(0, 177);
            this.layoutControlCashAccount.MinSize = new System.Drawing.Size(129, 30);
            this.layoutControlCashAccount.Name = "layoutControlCashAccount";
            this.layoutControlCashAccount.Size = new System.Drawing.Size(495, 30);
            this.layoutControlCashAccount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlCashAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlCashAccount.Text = "Cash Account:";
            this.layoutControlCashAccount.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlCashBalance
            // 
            this.layoutControlCashBalance.Control = this.labelCashBalance;
            this.layoutControlCashBalance.CustomizationFormText = "layoutControlCashBalance";
            this.layoutControlCashBalance.Location = new System.Drawing.Point(495, 177);
            this.layoutControlCashBalance.MaxSize = new System.Drawing.Size(233, 30);
            this.layoutControlCashBalance.MinSize = new System.Drawing.Size(233, 30);
            this.layoutControlCashBalance.Name = "layoutControlCashBalance";
            this.layoutControlCashBalance.Size = new System.Drawing.Size(233, 30);
            this.layoutControlCashBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlCashBalance.Text = "layoutControlCashBalance";
            this.layoutControlCashBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlCashBalance.TextToControlDistance = 0;
            this.layoutControlCashBalance.TextVisible = false;
            // 
            // layoutControlReceivableBalance
            // 
            this.layoutControlReceivableBalance.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlReceivableBalance.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlReceivableBalance.Control = this.labelStaffPaymentBal;
            this.layoutControlReceivableBalance.CustomizationFormText = "layoutControlExpenseAdvance";
            this.layoutControlReceivableBalance.Location = new System.Drawing.Point(0, 237);
            this.layoutControlReceivableBalance.MaxSize = new System.Drawing.Size(608, 30);
            this.layoutControlReceivableBalance.MinSize = new System.Drawing.Size(608, 30);
            this.layoutControlReceivableBalance.Name = "layoutControlReceivableBalance";
            this.layoutControlReceivableBalance.Size = new System.Drawing.Size(728, 30);
            this.layoutControlReceivableBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlReceivableBalance.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlReceivableBalance.Text = "Total Staff Payment Paid:";
            this.layoutControlReceivableBalance.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(728, 59);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.BackColor = System.Drawing.Color.LightSteelBlue;
            this.layoutControlItem2.AppearanceItemCaption.BackColor2 = System.Drawing.Color.LightSteelBlue;
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.Color.MidnightBlue;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.Control = this.lblTitle;
            this.layoutControlItem2.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(900, 59);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(596, 59);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem2.Size = new System.Drawing.Size(728, 59);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layotControlPaymentType
            // 
            this.layotControlPaymentType.Control = this.paymentTypeSelector;
            this.layotControlPaymentType.CustomizationFormText = "Payment Method:";
            this.layotControlPaymentType.Location = new System.Drawing.Point(0, 147);
            this.layotControlPaymentType.Name = "layotControlPaymentType";
            this.layotControlPaymentType.Size = new System.Drawing.Size(728, 30);
            this.layotControlPaymentType.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layotControlPaymentType.Text = "Payment Method:";
            this.layotControlPaymentType.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlSettleFullAmount
            // 
            this.layoutControlSettleFullAmount.Control = this.checkSettleFullAmount;
            this.layoutControlSettleFullAmount.CustomizationFormText = "layoutControlSettleFullAmount";
            this.layoutControlSettleFullAmount.Location = new System.Drawing.Point(471, 267);
            this.layoutControlSettleFullAmount.Name = "layoutControlSettleFullAmount";
            this.layoutControlSettleFullAmount.Size = new System.Drawing.Size(257, 30);
            this.layoutControlSettleFullAmount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlSettleFullAmount.Text = "layoutControlSettleFullAmount";
            this.layoutControlSettleFullAmount.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlSettleFullAmount.TextToControlDistance = 0;
            this.layoutControlSettleFullAmount.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.voucher;
            this.layoutControlItem3.CustomizationFormText = "Document Reference";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 59);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(372, 88);
            this.layoutControlItem3.Text = "Document Reference";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.datePayment;
            this.layoutControlItem1.CustomizationFormText = "Date:";
            this.layoutControlItem1.Location = new System.Drawing.Point(372, 59);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 88);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(153, 88);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(356, 88);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Text = "Date:";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlSlipRef
            // 
            this.layoutControlSlipRef.Control = this.txtReference;
            this.layoutControlSlipRef.CustomizationFormText = "Slip Reference:";
            this.layoutControlSlipRef.Location = new System.Drawing.Point(0, 387);
            this.layoutControlSlipRef.Name = "layoutControlSlipRef";
            this.layoutControlSlipRef.Size = new System.Drawing.Size(728, 30);
            this.layoutControlSlipRef.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlSlipRef.Text = "Slip Reference:";
            this.layoutControlSlipRef.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutControlServiceCharge
            // 
            this.layoutControlServiceCharge.Control = this.txtServiceCharge;
            this.layoutControlServiceCharge.CustomizationFormText = "Service Charge:";
            this.layoutControlServiceCharge.Location = new System.Drawing.Point(0, 456);
            this.layoutControlServiceCharge.Name = "layoutControlServiceCharge";
            this.layoutControlServiceCharge.Size = new System.Drawing.Size(728, 30);
            this.layoutControlServiceCharge.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlServiceCharge.Text = "Service Charge:";
            this.layoutControlServiceCharge.TextSize = new System.Drawing.Size(143, 13);
            // 
            // layoutReturnFromLabel
            // 
            this.layoutReturnFromLabel.Control = this.labelReturnFrom;
            this.layoutReturnFromLabel.CustomizationFormText = "Retrun From Loan:";
            this.layoutReturnFromLabel.Location = new System.Drawing.Point(0, 297);
            this.layoutReturnFromLabel.MinSize = new System.Drawing.Size(219, 23);
            this.layoutReturnFromLabel.Name = "layoutReturnFromLabel";
            this.layoutReturnFromLabel.Size = new System.Drawing.Size(492, 90);
            this.layoutReturnFromLabel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutReturnFromLabel.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutReturnFromLabel.Text = "Retrun From Loan:";
            this.layoutReturnFromLabel.TextSize = new System.Drawing.Size(143, 13);
            this.layoutReturnFromLabel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutReturnFromPick
            // 
            this.layoutReturnFromPick.Control = this.hyperLinkPickDoc;
            this.layoutReturnFromPick.CustomizationFormText = "layoutReturnFromPick";
            this.layoutReturnFromPick.Location = new System.Drawing.Point(492, 357);
            this.layoutReturnFromPick.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutReturnFromPick.MinSize = new System.Drawing.Size(60, 30);
            this.layoutReturnFromPick.Name = "layoutReturnFromPick";
            this.layoutReturnFromPick.Size = new System.Drawing.Size(236, 30);
            this.layoutReturnFromPick.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutReturnFromPick.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutReturnFromPick.Text = "layoutReturnFromPick";
            this.layoutReturnFromPick.TextSize = new System.Drawing.Size(0, 0);
            this.layoutReturnFromPick.TextToControlDistance = 0;
            this.layoutReturnFromPick.TextVisible = false;
            this.layoutReturnFromPick.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutReturnFromRemove
            // 
            this.layoutReturnFromRemove.Control = this.hyperLinkRemove;
            this.layoutReturnFromRemove.CustomizationFormText = "layoutControlItem4";
            this.layoutReturnFromRemove.Location = new System.Drawing.Point(492, 327);
            this.layoutReturnFromRemove.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutReturnFromRemove.MinSize = new System.Drawing.Size(60, 30);
            this.layoutReturnFromRemove.Name = "layoutReturnFromRemove";
            this.layoutReturnFromRemove.Size = new System.Drawing.Size(236, 30);
            this.layoutReturnFromRemove.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutReturnFromRemove.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutReturnFromRemove.Text = "layoutReturnFromRemove";
            this.layoutReturnFromRemove.TextSize = new System.Drawing.Size(0, 0);
            this.layoutReturnFromRemove.TextToControlDistance = 0;
            this.layoutReturnFromRemove.TextVisible = false;
            this.layoutReturnFromRemove.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutReturnFromView
            // 
            this.layoutReturnFromView.Control = this.hyperLinkView;
            this.layoutReturnFromView.CustomizationFormText = "layoutControlItem5";
            this.layoutReturnFromView.Location = new System.Drawing.Point(492, 297);
            this.layoutReturnFromView.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutReturnFromView.MinSize = new System.Drawing.Size(60, 30);
            this.layoutReturnFromView.Name = "layoutReturnFromView";
            this.layoutReturnFromView.Size = new System.Drawing.Size(236, 30);
            this.layoutReturnFromView.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutReturnFromView.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutReturnFromView.Text = "layoutReturnFromView";
            this.layoutReturnFromView.TextSize = new System.Drawing.Size(0, 0);
            this.layoutReturnFromView.TextToControlDistance = 0;
            this.layoutReturnFromView.TextVisible = false;
            this.layoutReturnFromView.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // validationStaffReturn
            // 
            this.validationStaffReturn.ValidateHiddenControls = false;
            // 
            // StaffReceiptTransactionFormBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(738, 523);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "StaffReceiptTransactionFormBase";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Staff Return Form";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkView.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkRemove.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hyperLinkPickDoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSettleFullAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentTypeSelector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceCharge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbCasher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBankAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAmountLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlReceivableBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layotControlPaymentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSettleFullAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSlipRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlServiceCharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutReturnFromLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutReturnFromPick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutReturnFromRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutReturnFromView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationStaffReturn)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CashAccountPlaceholder cmbCasher;
        private BankAccountPlaceholder cmbBankAccount;
        private DevExpress.XtraEditors.TextEdit txtAmount;
        private DevExpress.XtraEditors.TextEdit txtReference;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCashAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlBankAccount;
        protected DevExpress.XtraLayout.LayoutControlItem layoutAmountLabel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlSlipRef;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.MemoEdit txtNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlNote;
        private DevExpress.XtraEditors.LabelControl labelCashBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCashBalance;
        private DevExpress.XtraEditors.LabelControl labelBankBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlBankBalance;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validationStaffReturn;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraEditors.LabelControl labelStaffPaymentBal;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlReceivableBalance;
        private DevExpress.XtraEditors.TextEdit txtServiceCharge;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlServiceCharge;
        private DevExpress.XtraEditors.LabelControl lblTitle;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        protected DevExpress.XtraEditors.SimpleButton btnPrint;
        private BIZNET.iERP.Client.BNDualCalendar datePayment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private PaymentTypeSelector paymentTypeSelector;
        private DevExpress.XtraLayout.LayoutControlItem layotControlPaymentType;
        private DevExpress.XtraEditors.CheckEdit checkSettleFullAmount;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlSettleFullAmount;
        private DocumentSerialPlaceHolder voucher;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        protected DevExpress.XtraEditors.HyperLinkEdit hyperLinkPickDoc;
        protected DevExpress.XtraEditors.LabelControl labelReturnFrom;
        protected DevExpress.XtraLayout.LayoutControlItem layoutReturnFromLabel;
        protected DevExpress.XtraLayout.LayoutControlItem layoutReturnFromPick;
        private DevExpress.XtraEditors.HyperLinkEdit hyperLinkView;
        private DevExpress.XtraEditors.HyperLinkEdit hyperLinkRemove;
        private DevExpress.XtraLayout.LayoutControlItem layoutReturnFromRemove;
        private DevExpress.XtraLayout.LayoutControlItem layoutReturnFromView;
    }
}