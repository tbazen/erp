﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;

namespace BIZNET.iERP.Client
{
    public abstract partial class StaffReceiptTransactionFormBase : XtraForm
    {
        StaffReturnDocument m_doc = null;
        protected bool newDocument = true;
        private Employee m_Employee;
        private string employeeNameWithID;
        IAccountingClient _client;
        ActivationParameter _activation;
        PaymentMethodAndBalanceController _controller;
        protected SettleFullAmountController _settleFullAmount;
        public StaffReceiptTransactionFormBase(IAccountingClient client,ActivationParameter activation)
        {
            InitializeComponent();
            _client = client;
            _activation = activation;

            voucher.setKeys(typeof(StaffReturnDocument), "voucher");

            _settleFullAmount = new SettleFullAmountController(txtAmount, checkSettleFullAmount);
            _controller = new PaymentMethodAndBalanceController
            (paymentTypeSelector, layoutControlBankAccount, cmbBankAccount, layoutControlBankBalance, labelBankBalance, layoutControlCashAccount, cmbCasher, layoutControlCashBalance, labelCashBalance, layoutControlServiceCharge, layoutControlSlipRef, datePayment, _activation);
            setEmployee(_activation.employeeID);
            _controller.AddCostCenterAccountItem(getEmployeeAccountID(CostCenter.ROOT_COST_CENTER), labelStaffPaymentBal);
            _controller.PaymentInformationChanged += new EventHandler(_controller_PaymentInformationChanged);
            InitializeValidationRules();
            txtServiceCharge.LostFocus += Control_LostFocus;
            txtAmount.LostFocus += Control_LostFocus;
            txtReference.LostFocus += Control_LostFocus;
            PrepareControlsLayoutBasedOnDocumentType();
            try
            {
                _controller.IgnoreEvents();
                paymentTypeSelector.PaymentMethod = _activation.paymentMethod;
            }
            finally
            {
                _controller.AcceptEvents();
            }
            SetFormTitle();
            _settleFullAmount.FullAmount = _controller.getBalance(labelStaffPaymentBal);
        }

        void _controller_PaymentInformationChanged(object sender, EventArgs e)
        {
            _settleFullAmount.FullAmount = _controller.getBalance(labelStaffPaymentBal);
        }

        protected abstract void PrepareControlsLayoutBasedOnDocumentType();
        protected abstract string getTitle();
        protected abstract int accountID { get; }
        protected abstract StaffReturnDocument createDocumentInstance();
        protected abstract bool ValidateReturnAmount();
        protected virtual int pickDocumentTypeID
        {
            get
            {
                return -1;
            }
        }

        void setEmployee(int empID)
        {
            if (empID == -1)
            {
                m_Employee = null;
            }
            else
            {
                m_Employee = PayrollClient.GetEmployee(empID);
            }
        }
        int getEmployeeAccountID(int costCenterID)
        {
            int aid = accountID;
            if (aid == -1)
                return -1;
            if (m_Employee == null)
                return -1;
            CostCenterAccount csa = AccountingClient.GetCostCenterAccount(costCenterID, iERPTransactionClient.GetEmployeeAccountID(m_Employee, accountID));
            if (csa == null)
                return -1;
            return csa.id;
        }
        private void SetFormTitle()
        {

            employeeNameWithID = m_Employee==null?"": m_Employee.EmployeeNameID;

            lblTitle.Text = "<size=14><color=#360087><b>" + getTitle() + "</b></color></size>" +
                "<size=14><color=#C30013><b>" + employeeNameWithID + "</b></color></size>" +
                "<size=14><color=#360087><b>" + _controller.PaymentSource + "</b></color></size>";
        }
        
        
        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validationStaffReturn.Validate(control);
        }

        private void InitializeValidationRules()
        {
            InitializeAmountValidation();
        }

        private void InitializeAmountValidation()
        {
            NonEmptyNumericValidationRule depositAmountValidation = new NonEmptyNumericValidationRule();
            depositAmountValidation.ErrorText = "Amount cannot be blank and cannot contain a value of zero";
            depositAmountValidation.ErrorType = ErrorType.Default;
            validationStaffReturn.SetValidationRule(txtAmount, depositAmountValidation);
        }
       internal void LoadData(StaffReturnDocument staffReturnDocument)
        {
            m_doc = staffReturnDocument;
            if (staffReturnDocument == null)
            {
                btnPrint.Visible = false;
                ResetControls();
            }
            else
            {
                btnPrint.Visible = true;
                try
                {
                    _controller.IgnoreEvents();
                    _controller.PaymentTypeSelector.PaymentMethod=m_doc.paymentMethod;
                    setEmployee(m_doc.employeeID);
                    newDocument = false;
                    PopulateControlsForUpdate(staffReturnDocument);
                    _controller.AddCostCenterAccountItem(getEmployeeAccountID(CostCenter.ROOT_COST_CENTER), labelStaffPaymentBal);
                    _settleFullAmount.FullAmount = _controller.getBalance(labelStaffPaymentBal);
                    setReturnFrom(m_doc.loanDocumentID);
                }
                finally
                {
                    _controller.AcceptEvents();
                }
                SetFormTitle();
            }
        }
        private void ResetControls()
        {
            datePayment.DateTime = DateTime.Now;
            txtAmount.Text = "";
            txtReference.Text = "";
            txtNote.Text = "";
            voucher.setReference(-1, null);
            setReturnFrom(-1);
        }
        private void PopulateControlsForUpdate(StaffReturnDocument staffReturnDocument)
        {
            datePayment.DateTime = staffReturnDocument.DocumentDate;
            voucher.setReference(staffReturnDocument.AccountDocumentID, staffReturnDocument.voucher);
            _controller.assetAccountID = staffReturnDocument.assetAccountID;
            txtAmount.Text = TSConstants.FormatBirr(staffReturnDocument.amount);
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(_controller.PaymentTypeSelector.PaymentMethod))
                txtServiceCharge.Text = staffReturnDocument.serviceChargeAmount.ToString("N");
            SetReferenceNumber(false);
            txtNote.Text = staffReturnDocument.ShortDescription;
        }
       private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (validationStaffReturn.Validate())
                {
                    if (!ValidateReturnAmount())
                        return;
                    int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;

                    m_doc = createDocumentInstance();
                    m_doc.AccountDocumentID = docID;
                    m_doc.employeeID = m_Employee.id;
                    m_doc.DocumentDate = datePayment.DateTime;
                    m_doc.PaperRef = voucher.getReference().reference;
                    m_doc.voucher = voucher.getReference();
                    m_doc.paymentMethod = _controller.PaymentTypeSelector.PaymentMethod;
                    m_doc.assetAccountID = _controller.assetAccountID;
                    m_doc.amount = _settleFullAmount.effectiveAmount;
                    m_doc.loanDocumentID = (int)labelReturnFrom.Tag;
                    if (PaymentMethodHelper.IsPaymentMethodWithReference(_controller.PaymentTypeSelector.PaymentMethod))
                    {
                        bool updateDoc = m_doc == null ? false : true;
                        SetReferenceNumber(updateDoc);
                    }
                    m_doc.ShortDescription = txtNote.Text;
                    if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(_controller.PaymentTypeSelector.PaymentMethod))
                    {
                        using (ServiceChargePayment serviceCharge = new ServiceChargePayment(employeeNameWithID))
                        {
                            serviceCharge.StartPosition = FormStartPosition.CenterScreen;
                            serviceCharge.ShowInTaskbar = false;
                            if (serviceCharge.ShowDialog() == DialogResult.OK)
                            {
                                m_doc.serviceChargePayer = serviceCharge.ServiceChargePayer;
                                double serviceChargeAmount = txtServiceCharge.Text == "" ? 0 : double.Parse(txtServiceCharge.Text);
                                m_doc.serviceChargeAmount = serviceChargeAmount;
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                    int id = _client.PostGenericDocument(m_doc);
                    
                    if (!(_client is DocumentScheduler))
                    {
                        if (DocumentIsPrintable(m_doc))
                            ShowPrintForm();
                        string message = docID == -1 ? "successfully saved!" : "successfully updated!";
                        MessageBox.ShowSuccessMessage(message);
                    }
                    
                    Close();
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }
            
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

       protected virtual bool DocumentIsPrintable(StaffReturnDocument m_doc)
       {
           return false;
       }
       private void ShowPrintForm()
       {
           try
           {
               CompanyProfile prof = iERPTransactionClient.GetSystemParamter("companyProfile") as CompanyProfile;
               PrintFormViewer p = new PrintFormViewer();
               BIZNET.iERP.Client.DCHStaffPaymentTransactions.StaffLoanPrintForm printLoan = new BIZNET.iERP.Client.DCHStaffPaymentTransactions.StaffLoanPrintForm();
               if (m_doc is LoanFromStaffDocument)
               {
                   printLoan.lblTitle.Text = "Company Loan From Staff";
                   printLoan.ChangeReportFormatToLoanFromStaff();
               }
               BIZNET.iERP.Client.DCHStaffPaymentTransactions.StaffLoanData data = new BIZNET.iERP.Client.DCHStaffPaymentTransactions.StaffLoanData();
               data.StaffLoan.AddStaffLoanRow(m_doc.AccountDocumentID
                   , m_doc.PaperRef
                   , m_doc.DocumentDate.ToShortDateString()
                   , PayrollClient.GetEmployee(m_doc.employeeID).EmployeeNameID
                   , m_doc.amount,0
                   ,""
                   , prof.Name
                   , prof.Logo);
               printLoan.DataSource = data;
               p.LoadReport(printLoan);

               p.ShowDialog();
           }
           catch (Exception ex)
           {
               MessageBox.ShowException(ex);
           }
       }

        private void SetReferenceNumber(bool updateDoc)
        {
            switch (_controller.PaymentTypeSelector.PaymentMethod)
            {
                case BizNetPaymentMethod.Check:
                    if (newDocument)
                        m_doc.checkNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.checkNumber;
                        else
                            m_doc.checkNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.CPOFromBankAccount:
                    if (newDocument)
                        m_doc.cpoNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.cpoNumber;
                        else
                            m_doc.cpoNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.BankTransferFromCash:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    if (newDocument) 
                        m_doc.slipReferenceNo = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.slipReferenceNo;
                        else
                            m_doc.slipReferenceNo = txtReference.Text;
                    break;
                default:
                    break;
            }
        }
        
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            ShowPrintForm();
        }

        private void hyperLinkPickDoc_OpenLink(object sender, DevExpress.XtraEditors.Controls.OpenLinkEventArgs e)
        {
            DocumentBrowser docBrowser = new DocumentBrowser(true,this.pickDocumentTypeID);
            if (docBrowser.ShowDialog(this) == DialogResult.OK)
            {
                setReturnFrom(docBrowser.pickedDocument.AccountDocumentID);
            }
        }

        private void setReturnFrom(int docID)
        {
            labelReturnFrom.Tag = docID;
            LayoutVisibility visible= LayoutVisibility.Never;
            if (docID == -1)
                labelReturnFrom.Text = "";
            else
            {
                StaffLoandDocumentBase doc = INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(docID, true) as StaffLoandDocumentBase;
                if (doc == null)
                {
                    labelReturnFrom.Text = "DocID: " + docID + " not found in database";
                }
                else
                {
                    visible = LayoutVisibility.Always;
                    labelReturnFrom.Text = string.Format("Ref: {0} Amount: {1} Monthly Return: {2} Retrun Starts From: {3}"
                        , doc.voucher == null ? doc.PaperRef : doc.voucher.reference
                        , AccountBase.FormatAmount(doc.amount)
                        , AccountBase.FormatAmount(doc.monthlyReturn)
                        , PayrollClient.GetPayPeriod(doc.periodID).name);
                }
            }
            layoutReturnFromRemove.Visibility = layoutReturnFromView.Visibility = visible;
        }

        private void hyperLinkRemove_OpenLink(object sender, DevExpress.XtraEditors.Controls.OpenLinkEventArgs e)
        {
            setReturnFrom(-1);
        }

        private void hyperLinkView_OpenLink(object sender, DevExpress.XtraEditors.Controls.OpenLinkEventArgs e)
        {
            int docID = (int)labelReturnFrom.Tag;
            if(docID==-1)
                return;
            AccountDocument doc=AccountingClient.GetAccountDocument(docID,true);
            IGenericDocumentClientHandler handler= AccountingClient.GetDocumentHandler(doc.DocumentTypeID);
            Form f = handler.CreateEditor(false) as Form;
            handler.SetEditorDocument(f, doc);
            f.Show(this);
        }

       
    }
}
