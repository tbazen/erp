﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.Payroll.Client;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    class ExpenseAdvanceReturnForm : StaffReceiptTransactionFormBase
    {
        public ExpenseAdvanceReturnForm(IAccountingClient client, ActivationParameter param)
            : base(client, param)
        {
        }
        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "Staff Unused Purchase Advance Return Form";
            layoutControlReceivableBalance.Text = "Total Expense Advances Paid:";
        }

        protected override string getTitle()
        {
            return "Unused Purchase Advance return by ";
        }

        protected override int accountID
        {
            get { return (int)PayrollClient.GetSystemParameters(new string[] { "staffExpenseAdvanceAccountID" })[0]; }
        }

        protected override StaffReturnDocument createDocumentInstance()
        {
            return new ExpenseAdvanceReturnDocument();
        }

        protected override bool ValidateReturnAmount()
        {
            if (!_settleFullAmount.Validate())
                MessageBox.ShowErrorMessage("Amount cannot be greater than total of expense advances collected!");
            return true;
        }
    }

    class ShortTermLoanReturnForm : StaffReceiptTransactionFormBase
    {
        public ShortTermLoanReturnForm(IAccountingClient client, ActivationParameter param)
            : base(client, param)
        {
        }
        protected override int pickDocumentTypeID
        {
            get
            {
                return AccountingClient.GetDocumentTypeByType(typeof(ShortTermLoanDocument)).id;
            }
        }
        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "Staff Short Term Loan Settlement Form";
            layoutControlReceivableBalance.Text = "Total Short Term Loans Paid:";
            layoutReturnFromLabel.Visibility = base.layoutReturnFromPick.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
        }

        protected override string getTitle()
        {
            return "Short Term Loan Settlement by "; 
        }

        protected override int accountID
        {
            get { return (int)PayrollClient.GetSystemParameters(new string[] { "staffSalaryAdvanceAccountID" })[0]; }
        }

        protected override StaffReturnDocument createDocumentInstance()
        {
            return new ShortTermLoanReturnDocument();
        }

        protected override bool ValidateReturnAmount()
        {
            if (!_settleFullAmount.Validate())
                MessageBox.ShowErrorMessage("Amount cannot be greater than total of short term loans collected!");
            return true;
        }
        
    }

    class LongTermLoanReturnForm : StaffReceiptTransactionFormBase
    {
        public LongTermLoanReturnForm(IAccountingClient client, ActivationParameter param)
            : base(client, param)
        {
        }
        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "Staff Long Term Loan Settlement Form";
            layoutControlReceivableBalance.Text = "Total Long Term Loans Paid:";
            layoutReturnFromLabel.Visibility = base.layoutReturnFromPick.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
        }
        protected override int pickDocumentTypeID
        {
            get
            {
                DocumentType dt=AccountingClient.GetDocumentTypeByID(178);
                if (dt == null)
                    return AccountingClient.GetDocumentTypeByType(typeof(LongTermLoanDocument)).id;
                return 178;
            }
        }
        protected override string getTitle()
        {
            return "Long Term Loan Settlement by ";
        }

        protected override int accountID
        {
            get { return (int)PayrollClient.GetSystemParameters(new string[] { "staffLongTermLoanAccountID" })[0]; }
        }

        protected override StaffReturnDocument createDocumentInstance()
        {
            return new LongTermLoanReturnDocument();
        }

        protected override bool ValidateReturnAmount()
        {
            if (!_settleFullAmount.Validate())
                MessageBox.ShowErrorMessage("Amount cannot be greater than total of longer term loans collected!");
            return true;
        }
    }
    
    class LoanFromStaffForm : StaffReceiptTransactionFormBase
    {
        public LoanFromStaffForm(IAccountingClient client, ActivationParameter param)
            : base(client, param)
        {
        }
        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "Company Loan From Staff Form";
            layoutControlReceivableBalance.Text = "Total Loan:";
            layoutAmountLabel.Text = "Loan Amount:";
            if (!newDocument)
                btnPrint.Visible = true;
            layoutControlSettleFullAmount.HideToCustomization();
        }

        protected override string getTitle()
        {
            return "Company Loan from ";
        }

        protected override int accountID
        {
            get { return (int)PayrollClient.GetSystemParameters(new string[] { "LoanFromStaffAccountID" })[0]; }
        }
        protected override bool DocumentIsPrintable(StaffReturnDocument m_doc)
        {
            return true;
        }

        protected override StaffReturnDocument createDocumentInstance()
        {
            return new LoanFromStaffDocument();
        }

        protected override bool ValidateReturnAmount()
        {
            return true;
        }
    }

    class ShareHoldersLoanReturnForm : StaffReceiptTransactionFormBase
    {
        public ShareHoldersLoanReturnForm(IAccountingClient client, ActivationParameter param)
            : base(client, param)
        {
        }

        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "Share Holders Loan Settlement Form";
            layoutControlReceivableBalance.Text = "Total Share Holder Loans Paid:";
        }

        protected override string getTitle()
        {
            return "Share Holder Loan Settlement by ";
        }

        protected override int accountID
        {
            get { return (int)PayrollClient.GetSystemParameters(new string[] { "ShareHoldersLoanAccountID" })[0]; }
        }

        protected override StaffReturnDocument createDocumentInstance()
        {
            return new ShareHoldersLoanReturnDocument();
        }

        protected override bool ValidateReturnAmount()
        {
            if(!_settleFullAmount.Validate())
                MessageBox.ShowErrorMessage("Amount cannot be greater than total of share holder loans collected!");
            return true;
        }
    }
}
    

