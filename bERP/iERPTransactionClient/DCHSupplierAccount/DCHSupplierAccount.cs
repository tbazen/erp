using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    class DCHSupplierTransactionBase : bERPClientDocumentHandler
    {
        public DCHSupplierTransactionBase(Type formType)
            : base(formType)
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            SupplierTransactionFormBase f = (SupplierTransactionFormBase)editor;
            f.LoadData((SupplierAccountDocument)doc);
        }
    }
    class DCHSupplierReceivable : DCHSupplierTransactionBase
    {
        public DCHSupplierReceivable()
            : base(typeof(SupplierReceivableForm))
        {
        }
    }
    class DCHSupplierPayable : DCHSupplierTransactionBase
    {
        public DCHSupplierPayable()
            : base(typeof(SupplierPayableForm))
        {
        }

    }
    class DCHSupplierAdvanceReturn : DCHSupplierTransactionBase
    {
        public DCHSupplierAdvanceReturn()
            : base(typeof(SupplierAdvanceReturnForm))
        {
        }
    }
}
