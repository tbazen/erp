﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;
using DevExpress.XtraEditors.Controls;

namespace BIZNET.iERP.Client
{
    abstract partial class SupplierTransactionFormBase : XtraForm
    {
        //data
        protected bool _newDocument = true;
        protected SupplierAccountDocument _doc = null;
        protected TradeRelation _supplier;

        //UI logic
        protected IAccountingClient _client;
        protected ActivationParameter _activation;
        protected PaymentMethodAndBalanceController _paymentController;
        protected ScheduleController _schedule;

        public SupplierTransactionFormBase(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            _client = client;
            _activation = activation;

            voucher.setKeys(typeof(SupplierAccountDocument), "voucher");

            setSupplier(activation.objectCode);
            _paymentController = new PaymentMethodAndBalanceController(paymentTypeSelector, layoutControlBankAccount, cmbBankAccount, layoutControlBankBalance, labelBankBalance, layoutControlCashAccount, cmbCasher, layoutControlCashBalance, labelCashBalance, layoutControlServiceCharge, layoutControlSlipRef, datePayment, _activation);
            _paymentController.AddCostCenterAccountItem(getSupplierAccountID(), labelSupplierPaymentBal);


            _schedule = new ScheduleController(buttonScheduleSettlement, labelSchedule, new int[]{
                    AccountingClient.GetDocumentTypeByType(typeof(CustomerAdvanceReturnDocument)).id
                    ,AccountingClient.GetDocumentTypeByType(typeof(Sell2Document)).id
            });
            _schedule.visible = false;
            _schedule.BeforeActivation += delegate(ScheduleController cont)
            {
                ActivationParameter ret = new ActivationParameter();
                ret.objectCode = _supplier == null ? "" : _supplier.Code;
                ret.paymentMethod = paymentTypeSelector.PaymentMethod;
                ret.assetAccountID = _paymentController.assetAccountID;
                return ret;
            };
            PrepareControlsLayoutBasedOnDocumentType();

            SetFormTitle();

            txtServiceCharge.LostFocus += Control_LostFocus;
            txtAmount.LostFocus += Control_LostFocus;
            txtReference.LostFocus += Control_LostFocus;

            InitializeValidationRules();
        }
        
        protected abstract int relevantAccountID { get; }
        protected abstract void PrepareControlsLayoutBasedOnDocumentType();
        protected abstract SupplierAccountDocument CreateDocumentInstance();
        protected abstract string getTitle(bool transitive);
        
        protected virtual void setAdditionalFormData(SupplierAccountDocument customerAccountDocument)
        {

        }
        protected virtual void getAdditionalFormData(SupplierAccountDocument customerAccountDocument)
        {

        }
        protected virtual bool validateData(int assetAccountID)
        {
            return true;
        }

        private int getSupplierAccountID()
        {
            if (_supplier == null)
                return -1;
            CostCenterAccount csAccount = GetOrCreateCostCenterAccount(iERPTransactionClient.mainCostCenterID, relevantAccountID);
            return csAccount.id;

        }
        private CostCenterAccount GetOrCreateCostCenterAccount(int costCenterID, int accountID)
        {
            CostCenterAccount ret = AccountingClient.GetCostCenterAccount(costCenterID, accountID);
            if (ret != null)
                return ret;
            return AccountingClient.GetCostCenterAccount(AccountingClient.CreateCostCenterAccount(costCenterID, accountID));
        }
        private void setSupplier(string code)
        {
            if (code == null)
                _supplier = null;
            else
                _supplier = iERPTransactionClient.GetSupplier(code);
        }


        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validationSupplierReceivable.Validate(control);
        }

        private void InitializeValidationRules()
        {
            InitializeDepositAmountValidation();
        }

        private void InitializeDepositAmountValidation()
        {
            NonEmptyNumericValidationRule depositAmountValidation = new NonEmptyNumericValidationRule();
            depositAmountValidation.ErrorText = "Deposit amount cannot be blank and cannot contain a value of zero";
            depositAmountValidation.ErrorType = ErrorType.Default;
            validationSupplierReceivable.SetValidationRule(txtAmount, depositAmountValidation);
        }




        internal void LoadData(SupplierAccountDocument supplierAccountDocument)
        {
            _doc = supplierAccountDocument;
            if (supplierAccountDocument == null)
            {
                ResetControls();
            }
            else
            {
                _paymentController.IgnoreEvents();
                _newDocument = false;
                setSupplier(_doc.supplierCode);
                try
                {
                    _paymentController.PaymentTypeSelector.PaymentMethod = _doc.paymentMethod;
                    PopulateControlsForUpdate(supplierAccountDocument);
                    setAdditionalFormData(supplierAccountDocument);
                    _paymentController.AddCostCenterAccountItem(getSupplierAccountID(), labelSupplierPaymentBal);

                }
                finally
                {
                    _paymentController.AcceptEvents();
                }
                SetFormTitle();
            }
        }
        private void ResetControls()
        {
            datePayment.DateTime = DateTime.Now;
            txtAmount.Text = "";
            txtReference.Text = "";
            txtNote.Text = "";
            txtServiceCharge.Text = "";
            voucher.setReference(-1, null);
        }
        private void PopulateControlsForUpdate(SupplierAccountDocument supplierAccountDocument)
        {
            datePayment.DateTime = supplierAccountDocument.DocumentDate;
            voucher.setReference(supplierAccountDocument.AccountDocumentID, supplierAccountDocument.voucher);
            _paymentController.assetAccountID = supplierAccountDocument.assetAccountID;
            txtAmount.Text = TSConstants.FormatBirr(supplierAccountDocument.amount);
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(_paymentController.PaymentTypeSelector.PaymentMethod))
                txtServiceCharge.Text = supplierAccountDocument.serviceChargeAmount.ToString("N");
            SetReferenceNumber(_doc, false);
            txtNote.Text = supplierAccountDocument.ShortDescription;
        }

        
        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (validationSupplierReceivable.Validate())
                {
                    validateData(_paymentController.assetAccountID);
                    

                    int docID = _doc == null ? -1 : _doc.AccountDocumentID;
                    
                    SupplierAccountDocument newDoc = CreateDocumentInstance();
                    newDoc.AccountDocumentID = docID;
                    newDoc.supplierCode = _supplier.Code;
                    newDoc.DocumentDate = datePayment.DateTime;
                    newDoc.PaperRef = voucher.getReference().reference;
                    newDoc.voucher = voucher.getReference();
                    newDoc.paymentMethod = _paymentController.PaymentTypeSelector.PaymentMethod;
                    newDoc.assetAccountID = _paymentController.assetAccountID;
                    newDoc.amount = double.Parse(txtAmount.Text);
                    if (PaymentMethodHelper.IsPaymentMethodWithReference(_paymentController.PaymentTypeSelector.PaymentMethod))
                    {
                        bool updateDoc = newDoc == null ? false : true;
                        SetReferenceNumber(newDoc, updateDoc);
                    }
                    newDoc.ShortDescription = txtNote.Text;
                    getAdditionalFormData(newDoc);
                    if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(_paymentController.PaymentTypeSelector.PaymentMethod))
                    {
                        using (ServiceChargePayment serviceCharge = new ServiceChargePayment(_supplier.Name))
                        {
                            serviceCharge.StartPosition = FormStartPosition.CenterScreen;
                            serviceCharge.ShowInTaskbar = false;
                            if (serviceCharge.ShowDialog() == DialogResult.OK)
                            {
                                newDoc.serviceChargePayer = serviceCharge.ServiceChargePayer;
                                double serviceChargeAmount = txtServiceCharge.Text == "" ? 0 : double.Parse(txtServiceCharge.Text);
                                newDoc.serviceChargeAmount = serviceChargeAmount;
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                    if(!(_client is ScheduleController) && newDoc.IsFutureDate)
                        if (_doc == null || !_doc.scheduled)
                        {
                            if (MessageBox.ShowWarningMessage("Are you sure you want to schedule this document?") != DialogResult.Yes)
                                return;
                            newDoc.scheduled = true;
                        }
                        //else
                        //{
                        //    newDoc.scheduled = true;
                        //}

                    bool post = true;
                    if (iERPTransactionClient.AllowNegativeTransactionPost())
                    {
                        post = TransactionValidator.ValidateNegativeTransactionPost(newDoc.assetAccountID, newDoc.amount, paymentTypeSelector
                            , txtServiceCharge, newDoc.serviceChargePayer, newDoc.DocumentDate);
                    }
                    if (post)
                    {
                        newDoc.AccountDocumentID = _client.PostGenericDocument(newDoc);
                        _doc = newDoc;
                        if (!(_client is DocumentScheduler))
                        {
                            string message = docID == -1 ? "Payment successfully saved!" : "Payment successfully updated!";
                            MessageBox.ShowSuccessMessage(message);
                        }
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }

            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        


        private void SetReferenceNumber(SupplierAccountDocument doc, bool updateDoc)
        {
            switch (_paymentController.PaymentTypeSelector.PaymentMethod)
            {
                case BizNetPaymentMethod.Check:
                    if (_newDocument)
                        doc.checkNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = _doc.checkNumber;
                        else
                            doc.checkNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.CPOFromBankAccount:
                    if (_newDocument)
                        doc.cpoNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = _doc.cpoNumber;
                        else
                            doc.cpoNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.BankTransferFromCash:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    if (_newDocument)
                        doc.slipReferenceNo = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = _doc.slipReferenceNo;
                        else
                            doc.slipReferenceNo = txtReference.Text;
                    break;
                default:
                    break;
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void SetFormTitle()
        {
            string supplierName = _supplier == null ? " [Not Set] " : _supplier.Name;
            lblTitle.Text = "<size=14><color=#360087><b>" + getTitle(true) + "</b></color></size>" +
                "<size=14><color=#C30013><b>" + supplierName+ "</b></color></size>" +
                "<size=14><color=#360087><b>" + _paymentController.PaymentSource + "</b></color></size>";
        }
        

    }
}
