﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.Accounting;
using DevExpress.XtraLayout.Utils;

namespace BIZNET.iERP.Client
{
    class SupplierReceivableForm : SupplierTransactionFormBase
    {
        public SupplierReceivableForm(IAccountingClient client, ActivationParameter activation)
            : base(client, activation)
        {
        }
        protected override bool validateData(int assetAccountID)
        {
            return true;
        }
        protected override int relevantAccountID
        {
            get {
                return _supplier == null ? -1 : _supplier.ReceivableAccountID;
            }
        }

        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "TradeRelation Advance Payment Form";
            layoutControlSupplierBalance.Text = "Total Advances Paid:";
            layoutControlAmount.Text = "Payment Amount:";

        }

        protected override SupplierAccountDocument CreateDocumentInstance()
        {
            return new SupplierReceivableDocument();
        }

        protected override string getTitle(bool transitive)
        {
            return "Advance Payment Form for ";
        }
    }
    class SupplierPayableForm : SupplierTransactionFormBase
    {
        public SupplierPayableForm(IAccountingClient client, ActivationParameter activation)
            : base(client, activation)
        {
        }

        protected override int relevantAccountID
        {
            get { return _supplier == null ? -1 : _supplier.PayableAccountID; }
        }

        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "TradeRelation Credit Payment Settlement Form";
            layoutControlSupplierBalance.Text = "Total Credits:";
            layoutControlAmount.Text = "Payment Amount:";
        }

        protected override SupplierAccountDocument CreateDocumentInstance()
        {
            return new SupplierPayableDocument();
        }
        protected override void setAdditionalFormData(SupplierAccountDocument supplierAccountDocument)
        {
            base.setAdditionalFormData(supplierAccountDocument);
        }
        protected override void getAdditionalFormData(SupplierAccountDocument customerAccountDocument)
        {
            base.getAdditionalFormData(customerAccountDocument);
        }
        protected override string getTitle(bool transitive)
        {
            return "Credit Payment Settlement Form from ";
        }
    }
    class SupplierAdvanceReturnForm : SupplierTransactionFormBase
    {
        public SupplierAdvanceReturnForm(IAccountingClient client, ActivationParameter activation)
            : base(client, activation)
        {
        }
        protected override int relevantAccountID
        {
            get { return _supplier == null ? -1 : _supplier.ReceivableAccountID; }
        }

        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "TradeRelation Advance Return Form";
            layoutControlSupplierBalance.Text = "Total Advanced Paid:";
            layoutControlAmount.Text = "Return Amount:";
        }

        protected override SupplierAccountDocument CreateDocumentInstance()
        {
            return new SupplierAdvanceReturnDocument();
        }

        protected override string getTitle(bool transitive)
        {
            return "Advance Return Form from ";
        }
    }
}
