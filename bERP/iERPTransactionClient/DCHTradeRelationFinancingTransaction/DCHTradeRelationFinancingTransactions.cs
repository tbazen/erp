﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIZNET.iERP.Client
{
    class DCHSupplierAdvancePayment2 : DCHPurchase2
    {
        public DCHSupplierAdvancePayment2()
            : base(typeof(SupplierAdvancePayment2From))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            SupplierAdvancePayment2From f = (SupplierAdvancePayment2From)editor;
            f.LoadData((SupplierAdvancePayment2Document)doc);
        }
    }
    class DCHSupplierRetentionSettlement : DCHPurchase2
    {
        public DCHSupplierRetentionSettlement()
            : base(typeof(SupplierRetentionSettlementForm))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            SupplierRetentionSettlementForm f = (SupplierRetentionSettlementForm)editor;
            f.LoadData((SupplierRetentionSettlementDocument)doc);
        }
    }
    class DCHSupplierCreditSettlement : DCHPurchase2
    {
        public DCHSupplierCreditSettlement()
            : base(typeof(SupplierCreditSettlement2Form))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            SupplierCreditSettlement2Form f = (SupplierCreditSettlement2Form)editor;
            f.LoadData((SupplierCreditSettlement2Document)doc);
        }
    }
    class DCHSupplierAdvanceReturn2 : DCHSell2
    {
        public DCHSupplierAdvanceReturn2()
            : base(typeof(SupplierAdvanceReturn2Form))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            SupplierAdvanceReturn2Form f = (SupplierAdvanceReturn2Form)editor;
            f.LoadData((SupplierAdvanceReturn2Document)doc);
        }
    }
    class DCHCustomerAdvancePayment2 : DCHSell2
    {
        public DCHCustomerAdvancePayment2()
            : base(typeof(CustomerAdvancePayment2Form))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            CustomerAdvancePayment2Form f = (CustomerAdvancePayment2Form)editor;
            f.LoadData((CustomerAdvancePayment2Document)doc);
        }
    }
    class DCHCustomerRetentionSettlement : DCHSell2
    {
        public DCHCustomerRetentionSettlement()
            : base(typeof(CustomerRetentionSettlementForm))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            CustomerRetentionSettlementForm f = (CustomerRetentionSettlementForm)editor;
            f.LoadData((CustomerRetentionSettlementDocument)doc);
        }
    }
    class DCHCustomerCreditSettlement2 : DCHSell2
    {
        public DCHCustomerCreditSettlement2()
            : base(typeof(CustomerCreditSettlementForm))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            CustomerCreditSettlementForm f = (CustomerCreditSettlementForm)editor;
            f.LoadData((CustomerCreditSettlement2Document)doc);
        }
    }
    class DCHCustomerAdvanceReturn2 : DCHPurchase2
    {
        public DCHCustomerAdvanceReturn2()
            : base(typeof(CustomerAdvanceReturn2Form))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            CustomerAdvanceReturn2Form f = (CustomerAdvanceReturn2Form)editor;
            f.LoadData((CustomerAdvanceReturn2Document)doc);
        }
    }
}
