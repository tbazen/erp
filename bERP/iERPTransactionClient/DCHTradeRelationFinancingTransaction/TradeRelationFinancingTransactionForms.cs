﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public class SupplierAdvancePayment2From : Purchase2Form<SupplierAdvancePayment2Document>
    {
        public SupplierAdvancePayment2From(IAccountingClient client, ActivationParameter activation)
            : base(client, activation)
        {
        }
        public override string getTradeRelationType()
        {
            return "Supplier";
        }
        public override string getFormTitle()
        {
            return "Supplier Advance Payment";
        }
        public override List<TradeTransactionPaymentField> getPaymentFields()
        {
            List<TradeTransactionPaymentField> ret = base.getPaymentFields();
            ret.Remove(TradeTransactionPaymentField.SettleAdvancePayment);
            return ret;
        }
        public override string getAdditionalRelationInfo(TradeRelation rel, DateTime date)
        {
            if (rel.PayableAccountID != -1)
            {
                double bal = AccountingClient.GetNetCostCenterAccountBalanceAsOf(iERPTransactionClient.mainCostCenterID, rel.ReceivableAccountID, date);
                if (AccountBase.AmountEqual(bal, 0) && !iERPTransactionClient.AllowNegativeTransactionPost())
                {
                    _advanceSettlement.Enabled = false;
                    return "";
                }
                else
                {
                    _advanceSettlement.Enabled = true;
                    return "\nAdvance Paid:" + TSConstants.FormatBirr(bal);
                }

            }
            return "";
        }
    }
    public class SupplierRetentionSettlementForm : Purchase2Form<SupplierRetentionSettlementDocument>
    {
        public SupplierRetentionSettlementForm(IAccountingClient client, ActivationParameter activation)
            : base(client, activation)
        {
        }
        public override string getTradeRelationType()
        {
            return "Supplier";
        }
        public override string getFormTitle()
        {
            return "Supplier Retention Settlement";
        }
        public override List<TradeTransactionPaymentField> getPaymentFields()
        {
            List<TradeTransactionPaymentField> ret = base.getPaymentFields();
            ret.Remove(TradeTransactionPaymentField.Retention);
            return ret;
        }
        public override string getAdditionalRelationInfo(TradeRelation rel, DateTime date)
        {
            if (rel.PayableAccountID != -1)
            {
                double bal = AccountingClient.GetNetCostCenterAccountBalanceAsOf(iERPTransactionClient.mainCostCenterID, rel.RetentionPayableAccountID, date);
                if (AccountBase.AmountEqual(bal, 0) && !iERPTransactionClient.AllowNegativeTransactionPost())
                {
                    _advanceSettlement.Enabled = false;
                    return "";
                }
                else
                {
                    _advanceSettlement.Enabled = true;
                    return "\nRetation Paid:" + TSConstants.FormatBirr(bal);
                }

            }
            return "";
        }
    }
    public class SupplierCreditSettlement2Form : Purchase2Form<SupplierCreditSettlement2Document>
    {
        public SupplierCreditSettlement2Form(IAccountingClient client, ActivationParameter activation)
            : base(client, activation)
        {
        }
        public override string getTradeRelationType()
        {
            return "Supplier";
        }
        public override string getFormTitle()
        {
            return "Supplier Credit Settlement";
        }
        public override List<TradeTransactionPaymentField> getPaymentFields()
        {
            List<TradeTransactionPaymentField> ret = base.getPaymentFields();
            ret.Remove(TradeTransactionPaymentField.Credit);
            return ret;
        }
        public override string getAdditionalRelationInfo(TradeRelation rel, DateTime date)
        {
            if (rel.PayableAccountID != -1)
            {
                double bal = AccountingClient.GetNetCostCenterAccountBalanceAsOf(iERPTransactionClient.mainCostCenterID, rel.ReceivableAccountID, date);
                if (AccountBase.AmountEqual(bal, 0) && !iERPTransactionClient.AllowNegativeTransactionPost())
                {
                    _advanceSettlement.Enabled = false;
                    return "";
                }
                else
                {
                    _advanceSettlement.Enabled = true;
                    return "\nCredit Taken:" + TSConstants.FormatBirr(bal);
                }

            }
            return "";
        }
    }
    public class SupplierAdvanceReturn2Form : Sell2Form<SupplierAdvanceReturn2Document>
    {
        public SupplierAdvanceReturn2Form(IAccountingClient client, ActivationParameter activation)
            : base(client, activation)
        {
        }
        public override string getTradeRelationType()
        {
            return "Supplier";
        }
        public override string getFormTitle()
        {
            return "Supplier Advance Return";
        }
        public override List<TradeTransactionPaymentField> getPaymentFields()
        {
            List<TradeTransactionPaymentField> ret= base.getPaymentFields();
            ret.Remove(TradeTransactionPaymentField.SettleAdvancePayment);
            return ret;
        }
        public override string getAdditionalRelationInfo(TradeRelation rel, DateTime date)
        {
            if (rel.PayableAccountID != -1)
            {
                double bal = AccountingClient.GetNetCostCenterAccountBalanceAsOf(iERPTransactionClient.mainCostCenterID, rel.ReceivableAccountID, date);
                if (AccountBase.AmountEqual(bal, 0))
                {
                    _advanceSettlement.Enabled = false;
                    return "";
                }
                else
                {
                    _advanceSettlement.Enabled = true;
                    return "\nAdvance Paid:" + TSConstants.FormatBirr(bal);
                }

            }
            return "";
        }
    }
    public class CustomerAdvancePayment2Form : Sell2Form<CustomerAdvancePayment2Document>
    {
        public CustomerAdvancePayment2Form(IAccountingClient client, ActivationParameter activation)
            : base(client, activation)
        {
        }
        public override string getTradeRelationType()
        {
            return "Customer";
        }
        public override string getFormTitle()
        {
            return "Customer Advance Payment";
        }
        public override List<TradeTransactionPaymentField> getPaymentFields()
        {
            List<TradeTransactionPaymentField> ret = base.getPaymentFields();
            ret.Remove(TradeTransactionPaymentField.SettleAdvancePayment);
            return ret;
        }
        public override string getAdditionalRelationInfo(TradeRelation rel, DateTime date)
        {
            if (rel.PayableAccountID != -1)
            {
                double bal = AccountingClient.GetNetCostCenterAccountBalanceAsOf(iERPTransactionClient.mainCostCenterID, rel.PayableAccountID, date);
                if (AccountBase.AmountEqual(bal, 0))
                {
                    _advanceSettlement.Enabled = false;
                    return "";
                }
                else
                {
                    _advanceSettlement.Enabled = true;
                    return "\nAdvance Paid:" + TSConstants.FormatBirr(bal);
                }

            }
            return "";
        }
    }
    public class CustomerRetentionSettlementForm : Sell2Form<CustomerRetentionSettlementDocument>
    {
        public CustomerRetentionSettlementForm(IAccountingClient client, ActivationParameter activation)
            : base(client, activation)
        {
        }
        public override string getTradeRelationType()
        {
            return "Customer";
        }
        public override string getFormTitle()
        {
            return "Customer Retention Settlement";
        }
        public override List<TradeTransactionPaymentField> getPaymentFields()
        {
            List<TradeTransactionPaymentField> ret = base.getPaymentFields();
            ret.Remove(TradeTransactionPaymentField.Retention);
            return ret;
        }
        public override string getAdditionalRelationInfo(TradeRelation rel,DateTime date)
        {
            if (_customer.PayableAccountID != -1)
            {
                double bal = AccountingClient.GetNetCostCenterAccountBalanceAsOf(iERPTransactionClient.mainCostCenterID, rel.RetentionReceivableAccountID, date);
                if (AccountBase.AmountEqual(bal, 0))
                {
                    _advanceSettlement.Enabled = false;
                    return "";
                }
                else
                {
                    _advanceSettlement.Enabled = true;
                    return "\nRetained Amount:" + TSConstants.FormatBirr(bal);
                }

            }
            return "";
        }
    }
    public class CustomerCreditSettlementForm : Sell2Form<CustomerCreditSettlement2Document>
    {
        public CustomerCreditSettlementForm(IAccountingClient client, ActivationParameter activation)
            : base(client, activation)
        {
        }
        public override string getTradeRelationType()
        {
            return "Customer";
        }
        public override string getFormTitle()
        {
            return "Customer Credit Settlement";
        }
        public override List<TradeTransactionPaymentField> getPaymentFields()
        {
            List<TradeTransactionPaymentField> ret = base.getPaymentFields();
            ret.Remove(TradeTransactionPaymentField.Credit);
            return ret;
        }
        public override string getAdditionalRelationInfo(TradeRelation rel, DateTime date)
        {
            if (rel.PayableAccountID != -1)
            {
                double bal = AccountingClient.GetNetCostCenterAccountBalanceAsOf(iERPTransactionClient.mainCostCenterID, rel.ReceivableAccountID, date);
                if (AccountBase.AmountEqual(bal, 0))
                {
                    _advanceSettlement.Enabled = false;
                    return "";
                }
                else
                {
                    _advanceSettlement.Enabled = true;
                    return "\nCredit Given:" + TSConstants.FormatBirr(bal);
                }

            }
            return "";
        }
    }
    public class CustomerAdvanceReturn2Form : Purchase2Form<CustomerAdvanceReturn2Document>
    {
        public CustomerAdvanceReturn2Form(IAccountingClient client, ActivationParameter activation)
            : base(client, activation)
        {
        }
        public override string getTradeRelationType()
        {
            return "Customer";
        }
        public override string getFormTitle()
        {
            return "Customer Advance Return";
        }
        public override List<TradeTransactionPaymentField> getPaymentFields()
        {
            List<TradeTransactionPaymentField> ret = base.getPaymentFields();
            ret.Remove(TradeTransactionPaymentField.SettleAdvancePayment);
            return ret;
        }
        public override string getAdditionalRelationInfo(TradeRelation rel, DateTime date)
        {
            if (rel.PayableAccountID != -1)
            {
                double bal = AccountingClient.GetNetCostCenterAccountBalanceAsOf(iERPTransactionClient.mainCostCenterID, rel.PayableAccountID, date);
                if (AccountBase.AmountEqual(bal, 0) && !iERPTransactionClient.AllowNegativeTransactionPost())
                {
                    _advanceSettlement.Enabled = false;
                    return "";
                }
                else
                {
                    _advanceSettlement.Enabled = true;
                    return "\nAdvance Paid:" + TSConstants.FormatBirr(bal);
                }

            }
            return "";
        }
    }
}
