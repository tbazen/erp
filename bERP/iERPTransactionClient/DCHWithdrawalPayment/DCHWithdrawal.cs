using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHWithdrawal : bERPClientDocumentHandler
    {
        public DCHWithdrawal()
            : base(typeof(WithdrawalForm))
        {
        }
       
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            WithdrawalForm f = (WithdrawalForm)editor;

            f.LoadData((WithdrawalDocument)doc);
        }
    }
}
