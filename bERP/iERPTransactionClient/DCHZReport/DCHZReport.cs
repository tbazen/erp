using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public class DCHZReport : bERPClientDocumentHandler
    {
        public DCHZReport()
            : base(typeof(ZReportForm))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            ZReportForm f = (ZReportForm)editor;
            f.LoadData((ZReportDocument)doc);
        }

    }
}
