﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using INTAPS.Accounting.Client;
using DevExpress.XtraGrid;
using DevExpress.Data;
using DevExpress.XtraSplashScreen;

namespace BIZNET.iERP.Client
{
    public partial class ZReportBrowser : DevExpress.XtraEditors.XtraForm
    {
        private DataTable m_ZTable;
        SplashScreenManager waitScreen;
        public ZReportBrowser()
        {
            InitializeComponent();
            waitScreen = new SplashScreenManager(this, typeof(global::BIZNET.iERP.Client.WaitForm1), true, true);
            comboPeriod.SelectedValueChanged += comboPeriod_SelectedValueChanged;
            LoadPeriods();
        }

        void comboPeriod_SelectedValueChanged(object sender, EventArgs e)
        {
            waitScreen.ShowWaitForm();
            LoadZReports((AccountingPeriod)((ComboBoxEdit)sender).SelectedItem);
            waitScreen.CloseWaitForm();
        }

        

        private void InitializeDataTable()
        {
            m_ZTable = new DataTable();
            m_ZTable.Columns.Add("Date", typeof(string));
            m_ZTable.Columns.Add("Reference No.", typeof(string));
            m_ZTable.Columns.Add("Taxable Amount", typeof(double));
            m_ZTable.Columns.Add("Non-Taxable Amount", typeof(double));
            m_ZTable.Columns.Add("Tax", typeof(string));
            m_ZTable.Columns.Add("Grand Total", typeof(double));
            m_ZTable.Columns.Add("DocID", typeof(int));

            gridControlZReports.DataSource = m_ZTable;

            gridViewZReports.Columns["Taxable Amount"].DisplayFormat.FormatType = FormatType.Numeric;
            gridViewZReports.Columns["Taxable Amount"].DisplayFormat.FormatString = "N";

            gridViewZReports.Columns["Non-Taxable Amount"].DisplayFormat.FormatType = FormatType.Numeric;
            gridViewZReports.Columns["Non-Taxable Amount"].DisplayFormat.FormatString = "N";

            gridViewZReports.Columns["Grand Total"].DisplayFormat.FormatType = FormatType.Numeric;
            gridViewZReports.Columns["Grand Total"].DisplayFormat.FormatString = "N";

            gridViewZReports.Columns["DocID"].Visible = false;

            GridColumnSummaryItem grandTotalSummary = new GridColumnSummaryItem(SummaryItemType.Sum, "Grand Total", "Grand Total = {0:n2}");
            gridViewZReports.Columns["Grand Total"].Summary.Add(grandTotalSummary);
            gridViewZReports.OptionsView.ShowFooter = true;


        }
        private void LoadPeriods()
        {
            string text = (string)iERPTransactionClient.GetSystemParamter("taxDeclarationPeriods");
            AccountingPeriod[] periods = iERPTransactionClient.GetAccountingPeriodsInFiscalYear(text, Globals.CurrentFiscalYear - 1);
            comboPeriod.Items.AddRange(periods);
            periods = iERPTransactionClient.GetAccountingPeriodsInFiscalYear(text, Globals.CurrentFiscalYear);
            comboPeriod.Items.AddRange(periods);
            int index = 0;
            foreach (AccountingPeriod p in comboPeriod.Items)
            {
                if (Globals.CurrentTaxDeclarationPeriod != null)
                {
                    if (p.id == Globals.CurrentTaxDeclarationPeriod.id)
                    {
                        break;
                    }
                }
                index++;
            }
            if (index < comboPeriod.Items.Count)
            {
                if (btnEditPeriod.EditValue != null)
                    btnEditPeriod.EditValue = comboPeriod.Items[index];
            }
        }
        private void LoadZReports(AccountingPeriod period)
        {
            m_ZTable.Rows.Clear();
            ZReportDocument[] report = iERPTransactionClient.GetZReport(period);
            if (report != null)
            {
                foreach (ZReportDocument doc in report)
                {
                    DataRow row = m_ZTable.NewRow();
                    waitScreen.SetWaitFormDescription("Loading Z-Report: " + doc.PaperRef);
                    row[0] = doc.DocumentDate.ToShortDateString();
                    row[1] = doc.PaperRef;
                    if (Account.AmountGreater(doc.taxImposed.TaxBaseValue, 0))
                        row[2] = doc.taxImposed.TaxBaseValue;
                    if (Account.AmountGreater(doc.nonTaxableAmount, 0))
                        row[3] = doc.nonTaxableAmount;
                    if (Account.AmountGreater(doc.taxImposed.TaxValue, 0))
                        row[4] = String.Format("{0} = {1:N}", doc.taxImposed.TaxType, doc.taxImposed.TaxValue);
                    row[5] = doc.grandTotal;
                    row[6] = doc.AccountDocumentID;

                    m_ZTable.Rows.Add(row);
                    Application.DoEvents();
                }
                gridControlZReports.DataSource = m_ZTable;
                gridControlZReports.RefreshDataSource();
                if (gridViewZReports.DataRowCount > 0)
                    gridViewZReports.SelectRow(0);
            }
        }

        private void gridViewZReports_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewZReports.IsRowSelected(e.FocusedRowHandle))
            {
                btnEdit.Enabled = true;
                btnDelete.Enabled = true;
            }
            else
            {
                btnEdit.Enabled = false;
                btnDelete.Enabled = false;
            }
        }

        private void btnEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ActivationParameter activation = new ActivationParameter();
            IAccountingClient client = AccountingClient.AccountingClientDefaultInstance;
            ZReportForm zreport = new ZReportForm(client, activation);
            int docID=(int)gridViewZReports.GetRowCellValue(gridViewZReports.FocusedRowHandle,"DocID");
            ZReportDocument document = (ZReportDocument)INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(docID, true);
            zreport.LoadData(document);
            if (zreport.ShowDialog(this) == DialogResult.OK)
            {
                waitScreen.ShowWaitForm();
                LoadZReports((AccountingPeriod)btnEditPeriod.EditValue);
                waitScreen.CloseWaitForm();
            }
        }
       
        private void ZReportBrowser_Load(object sender, EventArgs e)
        {
            InitializeDataTable();
            waitScreen.ShowWaitForm();
            LoadZReports((AccountingPeriod)btnEditPeriod.EditValue);
            waitScreen.CloseWaitForm();
        }

        private void btnDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int docID = (int)gridViewZReports.GetRowCellValue(gridViewZReports.FocusedRowHandle, "DocID");
            if (MessageBox.ShowWarningMessage("Are you sure you want to delete the selected Z-Report?") == DialogResult.Yes)
            {
                INTAPS.Accounting.Client.AccountingClient.DeleteAccountDocument(docID);
                waitScreen.ShowWaitForm();
                LoadZReports((AccountingPeriod)btnEditPeriod.EditValue);
                waitScreen.CloseWaitForm();
            }
        }
    }
}