﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;
using DevExpress.XtraEditors.Controls;

namespace BIZNET.iERP.Client
{
    public partial class ZReportForm : XtraForm
    {
        ZReportDocument m_doc = null;
        CashAccount[] m_cashOnHandAccounts;
        private BankAccountInfo[] m_BankAccounts;
        private bool m_ValidateCash = false;
        private bool newDocument=true;
        private string paymentSource;
        private double m_SalesTaxableAmount = 0;
        private double m_SalesNonTaxableAmount = 0;
        private double m_SalesTax = 0;
        private IAccountingClient _client;
        private ActivationParameter _activation;
        private PaymentMethodAndBalanceController _paymentController;

        private double m_NetTaxableAmount = 0;
        private double m_NetNonTaxableAmount = 0;
        public ZReportForm(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            voucher.setKeys(typeof(ZReportDocument), "voucher");
            _client = client;
            _activation = activation;
            _paymentController = new PaymentMethodAndBalanceController(paymentTypeSelector, layoutControlBankAccount, cmbBankAccount, layoutControlBankBalance, labelBankBalance, layoutControlCashAccount, cmbCasher, layoutControlCashBalance, labelCashBalance, layoutControlServiceCharge, layoutControlSlipRef, datePayment, _activation);
            _paymentController.PaymentInformationChanged += new EventHandler(_paymentMethodController_PaymentInformationChanged);

            //if (!Globals.DocumentWithPaymentTypeActivated)
            //    Globals.CurrentPaymentMethod = BizNetPaymentMethod.None;
            SetFormTitle();
            txtReference.LostFocus += Control_LostFocus;
            datePayment.DateTime = DateTime.Now;
            LoadSales(datePayment.DateTime.Date);
            CalculateNetSales();
        }

        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validationZReport.Validate(control);
        }

        
        internal void LoadData(ZReportDocument zReportDocument)
        {
            m_doc = zReportDocument;
            if (zReportDocument == null)
            {
                ResetControls();
            }
            else
            {
                _paymentController.IgnoreEvents();
                newDocument = false;
                //Globals.CurrentPaymentMethod = m_doc.paymentMethod;
                //Globals.DocumentWithPaymentTypeActivated = false;
                //PrepareControlsLayoutForCurrentPaymentMethod();
                try
                {
                    _paymentController.PaymentTypeSelector.PaymentMethod = m_doc.paymentMethod;
                    PopulateControlsForUpdate(zReportDocument);
                }
                finally
                {
                    _paymentController.AcceptEvents();
                }
                SetFormTitle();
            }
        }
        private void ResetControls()
        {
            datePayment.DateTime = DateTime.Now;
            txtTaxableAmount.Text = "";
            txtNonTaxableAmount.Text = "";
            txtReference.Text = "";
            txtNote.Text = "";
            voucher.setReference(-1, null);
        }
        void _paymentMethodController_PaymentInformationChanged(object sender, EventArgs e)
        {
            SetFormTitle();
        }
        private void PopulateControlsForUpdate(ZReportDocument zReportDocument)
        {
            datePayment.DateTime = zReportDocument.DocumentDate;
            _paymentController.assetAccountID = zReportDocument.assetAccountID;
            voucher.setReference(zReportDocument.AccountDocumentID, zReportDocument.voucher);
            txtTaxableAmount.Text = TSConstants.FormatBirr(zReportDocument.taxImposed.TaxBaseValue);
            txtNonTaxableAmount.Text = TSConstants.FormatBirr(zReportDocument.nonTaxableAmount);
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(_paymentController.PaymentTypeSelector.PaymentMethod))
                txtServiceCharge.Text = zReportDocument.serviceChargeAmount.ToString("N");
            SetReferenceNumber(false);
            txtNote.Text = zReportDocument.ShortDescription;
        }
        
        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (voucher.getReference() == null)
                {
                    MessageBox.ShowErrorMessage("Enter valid reference");
                    return;
                }
                if (validationZReport.Validate())
                {
                    if (ValidateAmounts())
                    {
                        if (datePayment.DateTime.CompareTo(DateTime.Now) > 0)
                        {
                            MessageBox.ShowErrorMessage("You cannot enter Z-Report for future dates");
                            return;
                        }
                        int docID = m_doc == null ? -1 : m_doc.AccountDocumentID;
                        m_doc = new ZReportDocument();
                        m_doc.AccountDocumentID = docID;
                        m_doc.DocumentDate = datePayment.DateTime;
                        m_doc.voucher = voucher.getReference(); 
                        m_doc.paymentMethod = _paymentController.PaymentTypeSelector.PaymentMethod;
                        CalculateTax();
                        m_doc.assetAccountID = _paymentController.assetAccountID;
                        m_doc.grandTotal = m_doc.NetTaxableAmount + m_doc.NetNonTaxableAmount + m_doc.NetTaxAmount;
                        if (PaymentMethodHelper.IsPaymentMethodWithReference(_paymentController.PaymentTypeSelector.PaymentMethod))
                        {
                            bool updateDoc = m_doc == null ? false : true;
                            SetReferenceNumber(updateDoc);
                        }
                        m_doc.ShortDescription = txtNote.Text;
                        if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(_paymentController.PaymentTypeSelector.PaymentMethod))
                        {
                            m_doc.serviceChargePayer = ServiceChargePayer.Company;
                            double serviceChargeAmount = txtServiceCharge.Text == "" ? 0 : double.Parse(txtServiceCharge.Text);
                            m_doc.serviceChargeAmount = serviceChargeAmount;
                        }

                        _client.PostGenericDocument(m_doc);
                        string message = docID == -1 ? "successfully saved!" : "successfully updated!";
                        MessageBox.ShowSuccessMessage(message);
                        if (docID != -1)
                            this.DialogResult = DialogResult.OK;
                        if (!this.IsDisposed)
                            Close();
                    }
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }
            
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void CalculateTax()
        {
            try
            {
                TaxImposed taxImposed = new TaxImposed();
                taxImposed.TaxBaseValue = double.Parse(txtTaxableAmount.Text);
                if (Account.AmountGreater(taxImposed.TaxBaseValue, 0))
                {
                    CompanyProfile profile = (CompanyProfile)iERPTransactionClient.GetSystemParamter("companyProfile");
                    if (profile.TaxRegistrationType == TaxRegisrationType.VAT)
                    {
                        double vatRate = (double)iERPTransactionClient.GetSystemParamter("CommonVATRate");
                        taxImposed.TaxType = TaxType.VAT;
                        taxImposed.TaxValue = taxImposed.TaxBaseValue * vatRate;
                    }
                    else if (profile.TaxRegistrationType == TaxRegisrationType.TOT)
                    {//Only for goods selling company
                        double totRate = (double)iERPTransactionClient.GetSystemParamter("GoodsTaxableRate");
                        taxImposed.TaxType = TaxType.TOT;
                        taxImposed.TaxValue = taxImposed.TaxBaseValue * totRate;
                    }
                }
                m_doc.taxImposed = taxImposed;
                m_doc.nonTaxableAmount = double.Parse(txtNonTaxableAmount.Text);
                m_doc.salesTaxableAmount = m_SalesTaxableAmount;
                m_doc.salesNonTaxableAmount = m_SalesNonTaxableAmount;
                m_doc.salesTax = m_SalesTax;
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);   
            }
        }
        private bool ValidateAmounts()
        {
            double taxableAmount, nonTaxableAmount;
            if (txtTaxableAmount.Text == "")
            {
                txtTaxableAmount.Text = 0.ToString();
            }
            if (txtNonTaxableAmount.Text == "")
            {
                txtNonTaxableAmount.Text = 0.ToString();
            }
            if (double.TryParse(txtTaxableAmount.Text, out taxableAmount))
            {
                if (double.TryParse(txtNonTaxableAmount.Text, out nonTaxableAmount))
                {
                    if (Account.AmountEqual(taxableAmount, 0) && Account.AmountEqual(nonTaxableAmount, 0))
                    {
                        MessageBox.ShowErrorMessage("Please enter amount for either taxable amount or non-taxable amount and try again!");
                        return false;
                    }
                }
            }
            return true;
        }

       
        private void SetReferenceNumber(bool updateDoc)
        {
            switch (_paymentController.PaymentTypeSelector.PaymentMethod)
            {
                case BizNetPaymentMethod.Check:
                    if (newDocument)
                        m_doc.checkNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.checkNumber;
                        else
                            m_doc.checkNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.CPOFromBankAccount:
                    if (newDocument)
                        m_doc.cpoNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.cpoNumber;
                        else
                            m_doc.cpoNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.BankTransferFromCash:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    if (newDocument)
                        m_doc.slipReferenceNo = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = m_doc.slipReferenceNo;
                        else
                            m_doc.slipReferenceNo = txtReference.Text;
                    break;
                default:
                    break;
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        
        private void SetFormTitle()
        {
            string title = "Z-Report Sales Entry";

          
            lblTitle.Text = "<size=14><color=#360087><b>" + title + "</b></color></size>" +
                "<size=14><color=#360087><b>" + _paymentController.PaymentSource + "</b></color></size>";
        }

        private void LoadSales(DateTime date)
        {
            if (newDocument)
                m_SalesTaxableAmount = iERPTransactionClient.GetSalesTaxableAndNonTaxable(date, out m_SalesNonTaxableAmount, out m_SalesTax);
            else if (!newDocument && DateTime.Compare(m_doc.DocumentDate.Date, datePayment.DateTime.Date) != 0)
                m_SalesTaxableAmount = iERPTransactionClient.GetSalesTaxableAndNonTaxable(date, out m_SalesNonTaxableAmount, out m_SalesTax);

            else
            {
                m_SalesTaxableAmount = m_doc.salesTaxableAmount;
                m_SalesNonTaxableAmount = m_doc.salesNonTaxableAmount;
                m_SalesTax = m_doc.salesTax;
            }
            lblSalesTaxable.Text = m_SalesTaxableAmount.ToString("N");
            lblSalesNonTaxable.Text = m_SalesNonTaxableAmount.ToString("N");
        }
        private void CalculateNetSales()
        {
            double taxable, nonTaxable;
            taxable = nonTaxable = 0;
            if (txtTaxableAmount.Text != "")
            {
                taxable = double.Parse(txtTaxableAmount.Text);
            }
            if(txtNonTaxableAmount.Text!="")
            {
                nonTaxable = double.Parse(txtNonTaxableAmount.Text);
            }
            m_NetNonTaxableAmount = Math.Round(Math.Abs(m_SalesNonTaxableAmount - nonTaxable), 2);
            m_NetTaxableAmount = Math.Round(Math.Abs(m_SalesTaxableAmount - taxable), 2);

            lblNetNonTaxable.Text = m_NetNonTaxableAmount.ToString("N");
            lblNetTaxable.Text = m_NetTaxableAmount.ToString("N");
        }


        private void txtTaxableAmount_EditValueChanged(object sender, EventArgs e)
        {
            CalculateNetSales();
        }

        private void txtNonTaxableAmount_EditValueChanged(object sender, EventArgs e)
        {
            CalculateNetSales();
        }

        private void datePayment_DateTimeChanged(object sender, EventArgs e)
        {
            LoadSales(datePayment.DateTime.Date);
            CalculateNetSales();
        }
    }
}
