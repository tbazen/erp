using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.UI.HTML;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Web;

namespace BIZNET.iERP.Client
{
    public class CashDashBoardWidget:IHTMLBuilder
    {
        string m_outerHtml=null;
        string m_pathPrefix;
        int m_costCenterID = CostCenter.ROOT_COST_CENTER;
        public CashDashBoardWidget(string pathPrefix)
        {
            m_pathPrefix = pathPrefix;
        }
        public int costCenterID
        {
            get
            {
                return m_costCenterID;
            }
            set
            {
                m_costCenterID = value;
            }
        }
        #region IHTMLBuilder Members
        public void Build()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div><span class='Section_Header_2'></span><br/><table>");
            try
            {
                BankAccountInfo[] ba = iERPTransactionClient.GetAllBankAccounts();
                if (ba.Length > 0)
                {
                    if (ba.Length == 1)
                        sb.Append("<tr><td>" + HttpUtility.HtmlEncode("Cash at " + ba[0].ToString()) + "</td><td>" + INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(ba[0].mainCsAccount, DateTime.Now).ToString("#,#0.00") + "</td></tr>");
                    else
                    {
                        sb.Append("<tr><td colspan=2 class='Section_Header_1'>Cash at Bank</td></tr>");
                        double total = 0;
                        int index = 0;
                        for (int i = 0; i < ba.Length; i++)
                        {
                            double bal = INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(ba[i].mainCsAccount, DateTime.Now);
                            if (!Account.AmountEqual(bal, 0))
                            {
                                sb.Append(string.Format(
                                    "<tr class='{0}'><td >{1}</td><td class='currencyCell'>{2}</td></tr>"
                                , index % 2 == 0 ? "Even_Row" : "Odd_Row"
                                    , HttpUtility.HtmlEncode(ba[i].ToString())
                                , bal.ToString("#,#0.00")
                                ));
                                total += bal;
                                index++;
                            }
                        }
                        sb.Append("<tr class='Section_Header_1'><td class='currencyCell'>Total:</td><td class='currencyCell'>" + total.ToString("#,#0.00") + "</td></tr>");
                    }

                }




                CashAccount[] cashAccounts = iERPTransactionClient.GetAllCashAccounts(true);
                if (cashAccounts.Length > 0)
                {
                    

                    if (cashAccounts.Length == 1)
                        sb.Append("<tr class='Section_Header_1'><td>" + HttpUtility.HtmlEncode(cashAccounts[0].name) + "</td><td class='currencyCell'>" + INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(cashAccounts[0].csAccountID, DateTime.Now).ToString("#,#0.00") + "</td></tr>");
                    else
                    {

                        double total = 0;
                        sb.Append("<tr class='Section_Header_1'><td colspan=2>Cash at Hand</td></tr>");
                        int itemcount = 0;
                        string[] prefix = iERPTransactionClient.GetSystemParamter("summaryRoots") as string[];

                        for (int i = 0; i < cashAccounts.Length; i++)
                        {
                            Account ac = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(INTAPS.Accounting.Client.AccountingClient.GetCostCenterAccount(cashAccounts[i].csAccountID).accountID);
                            bool skip = false;
                            foreach(string p in prefix)
                            {
                                if(ac.Code.IndexOf(p)==0)
                                {
                                    skip = true;
                                    break;
                                }
                            }
                            if (skip)
                                continue;
                            double bal = INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(cashAccounts[i].csAccountID, DateTime.Now);
                            if (!Account.AmountEqual(bal, 0))
                            {
                                sb.Append(string.Format(
                                    "<tr class={0}><td>{1}</td><td class='currencyCell'>{2}</td></tr>"
                                    , itemcount % 2 == 0 ? "Even_Row" : "Odd_Row"
                                    , HttpUtility.HtmlEncode(cashAccounts[i].name)
                                    , bal.ToString("#,#0.00")
                                    ));
                                itemcount++;
                            }
                            total += bal;
                        }
                        if (itemcount > 1)
                            sb.Append("<tr class='Section_Header_1'><td class='currencyCell'>Total:</td><td class='currencyCell'>" + total.ToString("#,#0.00") + "</td></tr>");
                    }
                }
                m_outerHtml = sb.ToString();
            }
            catch (Exception ex)
            {
                m_outerHtml = HttpUtility.HtmlEncode(ex.Message) + "<br/>" + HttpUtility.HtmlEncode(ex.StackTrace);
            }
        }


        public string GetOuterHtml()
        {
            return m_outerHtml;
        }

        public bool ProcessUrl(string path, System.Collections.Hashtable query)
        {
            return false;
        }

        public void SetHost(IHTMLDocumentHost host)
        {
            
        }

        public string WindowCaption
        {
            get { return "Cash status"; }
        }

        #endregion
    }

    public class DashboardBuilder : IHTMLBuilder
    {
        #region IHTMLBuilder Members
        string m_outerHTML = null;
        CashDashBoardWidget m_cashWidget;

        public void Build()
        {
            m_cashWidget = new CashDashBoardWidget("cash");
            m_cashWidget.Build();
            m_outerHTML = "<div id='cash'>" + m_cashWidget.GetOuterHtml() + "</div>";
        }
        public string GetOuterHtml()
        {
            if (m_outerHTML == null)
                Build();
            return m_outerHTML;
        }

        public bool ProcessUrl(string path, System.Collections.Hashtable query)
        {
            if (m_cashWidget.ProcessUrl(path, query))
                return true;
            return false;
        }

        public void SetHost(INTAPS.UI.HTML.IHTMLDocumentHost host)
        {
            
        }

        public string WindowCaption
        {
            get { return "Dash board"; }
        }

        #endregion
    }
}
