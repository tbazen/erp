﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace BIZNET.iERP.Client.DeclarationDesign
{
    public partial class PensionTaxSummary : DevExpress.XtraReports.UI.XtraReport
    {
        public PensionTaxSummary()
        {
            InitializeComponent();
        }
        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);
            subResigned.ReportSource.DataSource = this.DataSource;
        }

    }
}
