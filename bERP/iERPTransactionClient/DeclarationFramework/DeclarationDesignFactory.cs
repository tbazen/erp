using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace BIZNET.iERP.Client
{
    public class DeclarationDesignFactory
    {
        static Assembly designAssembly;
        static DeclarationDesignFactory()
        {
            try
            {
                bool appKeyExists = CheckDeclarationTemplateKeyInAppConfig(System.Configuration.ConfigurationManager.AppSettings.Keys);
                if (!appKeyExists)
                    throw new Exception("'DeclarationTemplateAssembly' key not found in Client Application Configuration File");

                string declarationAssembly = System.Configuration.ConfigurationManager.AppSettings["DeclarationTemplateAssembly"] + ".dll";

                if (!System.IO.File.Exists(declarationAssembly))
                    throw new Exception("Declaration Template Assembly not found. Check Client Application Configuration");

                designAssembly = Assembly.Load(declarationAssembly.Replace(".dll",""));
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private static bool CheckDeclarationTemplateKeyInAppConfig(System.Collections.Specialized.NameObjectCollectionBase.KeysCollection keysCollection)
        {
            foreach (string key in keysCollection)
            {
                if (key.Equals("DeclarationTemplateAssembly"))
                    return true;
            }
            return false;
        }
        public DeclarationDesignFactory()
        {
            
        }

        internal static DevExpress.XtraReports.UI.XtraReport createDesignObject(string typeName)
        {
            object obj=Activator.CreateInstance( designAssembly.GetType(typeName));
            return obj as DevExpress.XtraReports.UI.XtraReport;
        }
    }
}
