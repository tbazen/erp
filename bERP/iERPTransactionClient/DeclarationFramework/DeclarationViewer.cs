﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BIZNET.iERP.Client
{
    public partial class DeclarationViewer : DevExpress.XtraEditors.XtraForm
    {
        IDeclarationSource m_source;
        public DeclarationViewer(IDeclarationSource source)
        {
            InitializeComponent();
            LoadSummary(source);
        }
        
        DevExpress.XtraPrinting.Control.PrintControl CreateReportViewer(int index)
        {
            DevExpress.XtraReports.UI.XtraReport report = m_source.GetPageReport(index);
            DevExpress.XtraPrinting.Control.PrintControl viewer = new DevExpress.XtraPrinting.Control.PrintControl();
            report.CreateDocument();
            viewer.PrintingSystem = report.PrintingSystem;
            report.PrintingSystem.ShowMarginsWarning = false;            
            viewer.Dock = DockStyle.Fill;
            return viewer;
        }
        
        void LoadSummary(IDeclarationSource source)
        {
            m_source = source;
            this.Text = m_source.SummaryTitle;
            if (m_source.PageCount == 0)
                return;
            if (m_source.PageCount == 1)
            {
                DevExpress.XtraPrinting.Control.PrintControl cont = CreateReportViewer(0);
                printBarManager1.PrintControl = cont;
                this.Controls.Add(cont);
                return;
            }
            int n = m_source.PageCount;
            DevExpress.XtraTab.XtraTabControl tabc = new DevExpress.XtraTab.XtraTabControl();
            
            tabc.Dock = DockStyle.Fill;
            this.Controls.Add(tabc);
            for (int i = 0; i < n; i++)
            {
                DevExpress.XtraTab.XtraTabPage page = new DevExpress.XtraTab.XtraTabPage();
                page.Text = m_source.GetPageTitle(i);
                tabc.TabPages.Add(page);
                page.Controls.Add(CreateReportViewer(i));
            }
            tabc_SelectedPageChanged(tabc, new DevExpress.XtraTab.TabPageChangedEventArgs(null,tabc.TabPages[0]));
            tabc.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(tabc_SelectedPageChanged);
        }

        void tabc_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            printBarManager1.PrintControl = (DevExpress.XtraPrinting.Control.PrintControl)e.Page.Controls[0];

        }
    }
}