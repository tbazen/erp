using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace BIZNET.iERP.Client
{
    public enum PageSideType
    {
        None,
        Front,
        Back,
    }

    public interface IDeclarationSource
    {
        string SummaryTitle { get;}
        int PageCount { get;}
        string GetPageTitle(int index);
        DataSet GetPageDataSet(int index);
        PageSideType GetPageSideType(int index);
        DevExpress.XtraReports.UI.XtraReport GetPageReport(int index);
    }
}
