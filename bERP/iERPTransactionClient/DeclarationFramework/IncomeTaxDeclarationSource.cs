using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Data;

namespace BIZNET.iERP.Client
{
    public class IncomeTaxDeclarationSummarySource:IDeclarationSource
    {
        TypedDataSets.IncomeTaxDeclarationData[] m_data;
        TypedDataSets.IncomeTaxDeclarationData[] m_resignedContinuation;
        AccountingPeriod m_period;
        const int CONT_FIRST_PAGE = 6;
        const int CONT_OTHER_PAGES = 15;
        const int RESIGNED_FIRST_PAGE = 4;
        const int RESIGNED_OTHER_PAGES = 20;
        public IncomeTaxDeclarationSummarySource(int periodID, TypedDataSets.IncomeTaxDeclarationData data)
        {
            if (data != null)
            {
                m_period = iERPTransactionClient.GetAccountingPeriod((string)iERPTransactionClient.GetSystemParamter("taxDeclarationPeriods"), periodID);
                int extra = data.Continuation.Count - CONT_FIRST_PAGE;
                m_data = new BIZNET.iERP.TypedDataSets.IncomeTaxDeclarationData[
                    1 + (extra / CONT_OTHER_PAGES) + (extra % CONT_OTHER_PAGES > 0 ? 1 : 0)];
                m_data[0] = new BIZNET.iERP.TypedDataSets.IncomeTaxDeclarationData();
                m_data[0].Summary.Rows.Add(data.Summary.Rows[0].ItemArray);

                int k = 0;
                int NItems = data.Continuation.Count;
                int periodIDIndex = m_data[0].Continuation.PeriodIDColumn.Ordinal;
                for (int j = 0; j < CONT_FIRST_PAGE; j++)
                {
                    if (k < NItems)
                    {
                        TypedDataSets.IncomeTaxDeclarationData.ContinuationRow row = (TypedDataSets.IncomeTaxDeclarationData.ContinuationRow)m_data[0].Continuation.Rows.Add(data.Continuation.Rows[k].ItemArray);
                    }
                    else
                    {
                        object[] vals = new object[data.Continuation.Columns.Count];
                        vals[periodIDIndex] = periodID;
                        m_data[0].Continuation.Rows.Add(vals);
                    }
                    k++;
                }

                for (int i = 1; i < m_data.Length; i++)
                {
                    m_data[i] = new BIZNET.iERP.TypedDataSets.IncomeTaxDeclarationData();
                    m_data[i].Summary.Rows.Add(data.Summary.Rows[0].ItemArray);
                    for (int j = 0; j < CONT_OTHER_PAGES; j++)
                    {
                        if (k < NItems)
                        {
                            TypedDataSets.IncomeTaxDeclarationData.ContinuationRow row = (TypedDataSets.IncomeTaxDeclarationData.ContinuationRow)m_data[i].Continuation.Rows.Add(data.Continuation.Rows[k].ItemArray);
                        }
                        else
                        {
                            object[] vals = new object[data.Continuation.Columns.Count];
                            vals[periodIDIndex] = periodID;
                            m_data[i].Continuation.Rows.Add(vals);
                        }
                        k++;
                    }
                }

                double contTotalTax = 0;
                double contTotalTaxable = 0;
                string birr, cents;
                for (int i = m_data.Length - 1; i >= 0; i--)
                {
                    if (!Account.AmountEqual(contTotalTax, 0))
                    {
                        m_data[i].Summary[0].ContTax = contTotalTax;
                    }
                    if (!Account.AmountEqual(contTotalTaxable, 0))
                    {
                        m_data[i].Summary[0].ContTaxable = contTotalTaxable;
                    }
                    foreach (TypedDataSets.IncomeTaxDeclarationData.ContinuationRow row in m_data[i].Continuation)
                    {
                        if (row.IsNull(m_data[i].Continuation.TaxColumn))
                            continue;
                        contTotalTax += row.Tax;
                        contTotalTaxable += row.TotalTaxable;
                    }
                    if (!Account.AmountEqual(contTotalTaxable, 0))
                    {
                        m_data[i].Summary[0].TotalTaxable = contTotalTaxable;
                    }
                    if (!Account.AmountEqual(contTotalTax, 0))
                    {
                        m_data[i].Summary[0].TotalTax = contTotalTax;
                    }

                    m_data[i].Summary[0].N_Page = m_data.Length;
                    m_data[i].Summary[0].Page = (i + 1).ToString();
                }



                extra = data.ResignedEmployees.Count - RESIGNED_FIRST_PAGE;
                m_resignedContinuation = new BIZNET.iERP.TypedDataSets.IncomeTaxDeclarationData[
                    (extra / RESIGNED_OTHER_PAGES) + (extra % RESIGNED_OTHER_PAGES > 0 ? 1 : 0)];
                k = 0;
                NItems = data.ResignedEmployees.Count;
                periodIDIndex = m_data[0].ResignedEmployees.PeriodIDColumn.Ordinal;
                for (int j = 0; j < RESIGNED_FIRST_PAGE; j++)
                {
                    if (k < NItems)
                    {
                        TypedDataSets.IncomeTaxDeclarationData.ResignedEmployeesRow row = (TypedDataSets.IncomeTaxDeclarationData.ResignedEmployeesRow)m_data[0].ResignedEmployees.Rows.Add(data.ResignedEmployees.Rows[k].ItemArray);
                    }
                    else
                    {
                        object[] vals = new object[data.ResignedEmployees.Columns.Count];
                        vals[periodIDIndex] = periodID;
                        m_data[0].ResignedEmployees.Rows.Add(vals);
                    }
                    k++;
                }
                for (int i = 0; i < m_resignedContinuation.Length; i++)
                {
                    m_resignedContinuation[i] = new BIZNET.iERP.TypedDataSets.IncomeTaxDeclarationData();
                    m_resignedContinuation[i].Summary.Rows.Add(data.Summary.Rows[0].ItemArray);
                    for (int j = 0; j < CONT_OTHER_PAGES; j++)
                    {
                        if (k < NItems)
                        {
                            TypedDataSets.IncomeTaxDeclarationData.ResignedEmployeesRow row = (TypedDataSets.IncomeTaxDeclarationData.ResignedEmployeesRow)m_resignedContinuation[i].ResignedEmployees.Rows.Add(data.ResignedEmployees.Rows[k].ItemArray);
                        }
                        else
                        {
                            object[] vals = new object[data.Continuation.Columns.Count];
                            vals[periodIDIndex] = periodID;
                            m_data[i].ResignedEmployees.Rows.Add(vals);

                        }
                        k++;
                    }
                }
                UpdateZeroValuesToNull();
            }
        }
       
        private void UpdateZeroValuesToNull()
        {
            double value;
            int i;
            foreach (TypedDataSets.IncomeTaxDeclarationData data in m_data)
            {
                i = 0;
                foreach (TypedDataSets.IncomeTaxDeclarationData.ContinuationRow row in data.Continuation)
                {
                    for (int j = 0; j < data.Continuation.Columns.Count; j++)
                    {
                        if (double.TryParse(data.Continuation[i][data.Continuation.Columns[j]].ToString(), out value))
                        {
                            if (Account.AmountEqual(value, 0))
                            {
                                data.Continuation[i][data.Continuation.Columns[j]] = DBNull.Value;
                            }
                        }
                    }
                    i++;
                }
                i = 0;
                foreach (TypedDataSets.IncomeTaxDeclarationData.SummaryRow row in data.Summary)
                {
                    for (int j = 0; j < data.Summary.Columns.Count; j++)
                    {
                        if (double.TryParse(data.Summary[i][data.Summary.Columns[j]].ToString(), out value))
                        {
                            if (Account.AmountEqual(value, 0))
                            {
                                data.Summary[i][data.Summary.Columns[j]] = DBNull.Value;
                            }
                        }
                    }
                    i++;
                }
            }
        }
       
        #region ISummarySource Members

        public string SummaryTitle
        {
            get 
            {
                if (m_period != null)
                    return "Income tax declaration for " + m_period.name;
                else
                    return "Income tax declaration";
            }
        }

        public int PageCount
        {
            get { return m_data==null?0: m_data.Length+m_resignedContinuation.Length; }
        }

        public string GetPageTitle(int index)
        {
            if (index == 0)
                return "Summary";
            if(index<m_data.Length)
                return "Continuation " + index;
            return "Resigned Employees Continuation " + (index - m_data.Length+1);
        }

        public System.Data.DataSet GetPageDataSet(int index)
        {
            if(index<m_data.Length)
                return m_data[index];
            return m_resignedContinuation[index - m_data.Length];
        }

        public PageSideType GetPageSideType(int index)
        {
            return PageSideType.None;
        }

        public DevExpress.XtraReports.UI.XtraReport GetPageReport(int index)
        {
            DataSet data = this.GetPageDataSet(index);
            if (index == 0)
            {
                //DeclarationDesign.IncomeTaxSummary ret = new DeclarationDesign.IncomeTaxSummary();
                DevExpress.XtraReports.UI.XtraReport ret = DeclarationDesignFactory.createDesignObject("BIZNET.iERP.Client.DeclarationDesign.IncomeTaxSummary");
                ret.DataSource = data;
                //ret.DataSource = data;
                return ret;
            }
            else if (index < m_data.Length)
            {
                //DeclarationDesign.IncomeTaxContinuation ret = new DeclarationDesign.IncomeTaxContinuation();
                //ret.DataSource = data;
                //return ret;
                //DeclarationDesign.IncomeTaxSummary ret = new DeclarationDesign.IncomeTaxSummary();
                DevExpress.XtraReports.UI.XtraReport ret = DeclarationDesignFactory.createDesignObject("BIZNET.iERP.Client.DeclarationDesign.IncomeTaxContinuation");
                ret.DataSource = data;
                //ret.DataSource = data;
                return ret;
            }
            else
            {
                throw new Exception("Continuation employees report not ready");
            }
        }

        #endregion
    }
    
    
}
