using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Data;

namespace BIZNET.iERP.Client
{
    public class PensionTaxDeclarationSummarySource:IDeclarationSource
    {
        TypedDataSets.PensionTaxDeclarationData[] m_data;
        TypedDataSets.PensionTaxDeclarationData[] m_resignedContinuation;
        AccountingPeriod m_period;
        const int CONT_FIRST_PAGE = 6;
        const int CONT_OTHER_PAGES = 15;
        const int RESIGNED_FIRST_PAGE = 4;
        const int RESIGNED_OTHER_PAGES = 20;
        public PensionTaxDeclarationSummarySource(int periodID, TypedDataSets.PensionTaxDeclarationData data)
        {
            if (data != null)
            {
                m_period = iERPTransactionClient.GetAccountingPeriod((string)iERPTransactionClient.GetSystemParamter("taxDeclarationPeriods"), periodID);
                int extra = data.Continuation.Count - CONT_FIRST_PAGE;
                m_data = new BIZNET.iERP.TypedDataSets.PensionTaxDeclarationData[
                    1 + (extra / CONT_OTHER_PAGES) + (extra % CONT_OTHER_PAGES > 0 ? 1 : 0)];
                m_data[0] = new BIZNET.iERP.TypedDataSets.PensionTaxDeclarationData();
                m_data[0].Summary.Rows.Add(data.Summary.Rows[0].ItemArray);

                int k = 0;
                int NItems = data.Continuation.Count;
                int periodIDIndex = m_data[0].Continuation.PeriodIDColumn.Ordinal;
                for (int j = 0; j < CONT_FIRST_PAGE; j++)
                {
                    if (k < NItems)
                    {
                        TypedDataSets.PensionTaxDeclarationData.ContinuationRow row = (TypedDataSets.PensionTaxDeclarationData.ContinuationRow)m_data[0].Continuation.Rows.Add(data.Continuation.Rows[k].ItemArray);
                    }
                    else
                    {
                        object[] vals = new object[data.Continuation.Columns.Count];
                        vals[periodIDIndex] = periodID;
                        m_data[0].Continuation.Rows.Add(vals);
                    }
                    k++;
                }
                for (int i = 1; i < m_data.Length; i++)
                {
                    m_data[i] = new BIZNET.iERP.TypedDataSets.PensionTaxDeclarationData();
                    m_data[i].Summary.Rows.Add(data.Summary.Rows[0].ItemArray);
                    for (int j = 0; j < CONT_OTHER_PAGES; j++)
                    {
                        if (k < NItems)
                        {
                            TypedDataSets.PensionTaxDeclarationData.ContinuationRow row = (TypedDataSets.PensionTaxDeclarationData.ContinuationRow)m_data[i].Continuation.Rows.Add(data.Continuation.Rows[k].ItemArray);
                        }
                        else
                        {
                            object[] vals = new object[data.Continuation.Columns.Count];
                            vals[periodIDIndex] = periodID;
                            m_data[i].Continuation.Rows.Add(vals);

                        }
                        k++;
                    }
                }

                double ContEmployerContribution = 0;
                double ContEmloyeeContribution = 0;
                double ContTotalContributions = 0;
                double contTotalSalary = 0;
                string birr, cents;
                for (int i = m_data.Length - 1; i >= 0; i--)
                {
                    if (!Account.AmountEqual(ContEmployerContribution, 0))
                    {
                        m_data[i].Summary[0].ContEmployerContribution = ContEmployerContribution;
                    }
                    if (!Account.AmountEqual(ContEmloyeeContribution, 0))
                    {
                        m_data[i].Summary[0].ContEmloyeeContribution = ContEmloyeeContribution;
                    }
                    if (!Account.AmountEqual(ContTotalContributions, 0))
                    {
                        m_data[i].Summary[0].ContTotalContributions = ContTotalContributions;
                    }

                    if (!Account.AmountEqual(contTotalSalary, 0))
                    {
                        m_data[i].Summary[0].ContSalary = contTotalSalary;
                    }
                    foreach (TypedDataSets.PensionTaxDeclarationData.ContinuationRow row in m_data[i].Continuation)
                    {
                        if (row.IsNull(m_data[i].Continuation.TotalContributionColumn))
                            continue;
                        ContEmployerContribution += row.EmployerContribution;
                        ContEmloyeeContribution += row.EmployeeContribution;
                        ContTotalContributions += row.TotalContribution;
                        contTotalSalary += row.Salary;
                    }
                    if (!Account.AmountEqual(contTotalSalary, 0))
                    {
                        m_data[i].Summary[0].TotalSalary = contTotalSalary;
                    }
                    if (!Account.AmountEqual(ContEmployerContribution, 0))
                    {
                        m_data[i].Summary[0].TotalEmployerContribution = ContEmployerContribution;
                    }
                    if (!Account.AmountEqual(ContEmloyeeContribution, 0))
                    {
                        m_data[i].Summary[0].TotalEmployeeContribution = ContEmloyeeContribution;
                    }
                    if (!Account.AmountEqual(ContTotalContributions, 0))
                    {
                        m_data[i].Summary[0].TotalContributions = ContTotalContributions;
                    }

                    m_data[i].Summary[0].N_Page = m_data.Length;
                    m_data[i].Summary[0].Page = (i + 1).ToString();
                }



                extra = data.ResignedEmployees.Count - RESIGNED_FIRST_PAGE;
                m_resignedContinuation = new BIZNET.iERP.TypedDataSets.PensionTaxDeclarationData[
                    (extra / RESIGNED_OTHER_PAGES) + (extra % RESIGNED_OTHER_PAGES > 0 ? 1 : 0)];
                k = 0;
                NItems = data.ResignedEmployees.Count;
                periodIDIndex = m_data[0].ResignedEmployees.PeriodIDColumn.Ordinal;
                for (int j = 0; j < RESIGNED_FIRST_PAGE; j++)
                {
                    if (k < NItems)
                    {
                        TypedDataSets.PensionTaxDeclarationData.ResignedEmployeesRow row = (TypedDataSets.PensionTaxDeclarationData.ResignedEmployeesRow)m_data[0].ResignedEmployees.Rows.Add(data.ResignedEmployees.Rows[k].ItemArray);
                    }
                    else
                    {
                        object[] vals = new object[data.ResignedEmployees.Columns.Count];
                        vals[periodIDIndex] = periodID;
                        m_data[0].ResignedEmployees.Rows.Add(vals);
                    }
                    k++;
                }
                for (int i = 0; i < m_resignedContinuation.Length; i++)
                {
                    m_resignedContinuation[i] = new BIZNET.iERP.TypedDataSets.PensionTaxDeclarationData();
                    m_resignedContinuation[i].Summary.Rows.Add(data.Summary.Rows[0].ItemArray);
                    for (int j = 0; j < CONT_OTHER_PAGES; j++)
                    {
                        if (k < NItems)
                        {
                            TypedDataSets.PensionTaxDeclarationData.ResignedEmployeesRow row = (TypedDataSets.PensionTaxDeclarationData.ResignedEmployeesRow)m_resignedContinuation[i].ResignedEmployees.Rows.Add(data.ResignedEmployees.Rows[k].ItemArray);
                        }
                        else
                        {
                            object[] vals = new object[data.Continuation.Columns.Count];
                            vals[periodIDIndex] = periodID;
                            m_data[i].ResignedEmployees.Rows.Add(vals);

                        }
                        k++;
                    }
                }
                UpdateZeroValuesToNull();
            }
        }
        private void UpdateZeroValuesToNull()
        {
            double value;
            int i;
            foreach (TypedDataSets.PensionTaxDeclarationData data in m_data)
            {
                i = 0;
                foreach (TypedDataSets.PensionTaxDeclarationData.SummaryRow row in data.Summary)
                {
                    for (int j = 0; j < data.Summary.Columns.Count; j++)
                    {
                        if (double.TryParse(data.Summary[i][data.Summary.Columns[j]].ToString(), out value))
                        {
                            if (Account.AmountEqual(value, 0))
                            {
                                data.Summary[i][data.Summary.Columns[j]] = DBNull.Value;
                            }
                        }
                    }
                    i++;
                }
            }
        }

        #region ISummarySource Members

        public string SummaryTitle
        {
            get 
            {
                if (m_period != null)
                    return "Pension tax declaration for " + m_period.name;
                else
                    return "Pension tax declaration";
            }
        }

        public int PageCount
        {
            get { return m_data==null?0: m_data.Length+m_resignedContinuation.Length; }
        }

        public string GetPageTitle(int index)
        {
            if (index == 0)
                return "Summary";
            if(index<m_data.Length)
                return "Continuation " + index;
            return "Resigned Employees Continuation " + (index - m_data.Length+1);
        }

        public System.Data.DataSet GetPageDataSet(int index)
        {
            if(index<m_data.Length)
                return m_data[index];
            return m_resignedContinuation[index - m_data.Length];
        }

        public PageSideType GetPageSideType(int index)
        {
            return PageSideType.None;
        }

        public DevExpress.XtraReports.UI.XtraReport GetPageReport(int index)
        {
            DataSet data = this.GetPageDataSet(index);
            if (index == 0)
            {
                //DeclarationDesign.PensionTaxSummary ret = new DeclarationDesign.PensionTaxSummary();
                //ret.DataSource = data;
                //return ret;
                DevExpress.XtraReports.UI.XtraReport ret = DeclarationDesignFactory.createDesignObject("BIZNET.iERP.Client.DeclarationDesign.PensionTaxSummary");
                ret.DataSource = data;
                //ret.DataSource = data;
                return ret;
            }
            else if (index < m_data.Length)
            {
                //DeclarationDesign.PensionTaxContinuation ret = new DeclarationDesign.PensionTaxContinuation();
                //ret.DataSource = data;
                //return ret;
                DevExpress.XtraReports.UI.XtraReport ret = DeclarationDesignFactory.createDesignObject("BIZNET.iERP.Client.DeclarationDesign.PensionTaxContinuation");
                ret.DataSource = data;
                //ret.DataSource = data;
                return ret;
            }
            else
            {
                throw new Exception("Continuation employees report not ready");
            }
        }

        #endregion
    }
    
    
}
