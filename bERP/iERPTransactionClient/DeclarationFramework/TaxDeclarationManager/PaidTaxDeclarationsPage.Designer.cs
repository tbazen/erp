﻿namespace BIZNET.iERP.Client
{
    partial class PaidTaxDeclarationsPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PaidTaxDeclarationsPage));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnUndoPay = new DevExpress.XtraBars.BarButtonItem();
            this.btnPreview = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.gridControlPaidDeclaration = new DevExpress.XtraGrid.GridControl();
            this.gridViewPaidDeclaration = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPaidDeclaration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPaidDeclaration)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem2,
            this.barButtonItem3,
            this.btnUndoPay,
            this.btnPreview});
            this.barManager1.MaxItemId = 6;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnUndoPay, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnPreview, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar1.OptionsBar.AllowCollapse = true;
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // btnUndoPay
            // 
            this.btnUndoPay.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUndoPay.Appearance.Options.UseFont = true;
            this.btnUndoPay.Caption = "&Undo Payment";
            this.btnUndoPay.Enabled = false;
            this.btnUndoPay.Glyph = ((System.Drawing.Image)(resources.GetObject("btnUndoPay.Glyph")));
            this.btnUndoPay.Id = 4;
            this.btnUndoPay.Name = "btnUndoPay";
            this.btnUndoPay.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnUndoPay_ItemClick);
            // 
            // btnPreview
            // 
            this.btnPreview.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPreview.Appearance.Options.UseFont = true;
            this.btnPreview.Caption = "&Preview";
            this.btnPreview.Enabled = false;
            this.btnPreview.Glyph = ((System.Drawing.Image)(resources.GetObject("btnPreview.Glyph")));
            this.btnPreview.Id = 5;
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPreview_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(685, 39);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 492);
            this.barDockControlBottom.Size = new System.Drawing.Size(685, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 39);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 453);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(685, 39);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 453);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "&Pay";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "&Delete";
            this.barButtonItem3.Id = 2;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // gridControlPaidDeclaration
            // 
            this.gridControlPaidDeclaration.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPaidDeclaration.Location = new System.Drawing.Point(0, 39);
            this.gridControlPaidDeclaration.MainView = this.gridViewPaidDeclaration;
            this.gridControlPaidDeclaration.MenuManager = this.barManager1;
            this.gridControlPaidDeclaration.Name = "gridControlPaidDeclaration";
            this.gridControlPaidDeclaration.Size = new System.Drawing.Size(685, 453);
            this.gridControlPaidDeclaration.TabIndex = 4;
            this.gridControlPaidDeclaration.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPaidDeclaration});
            // 
            // gridViewPaidDeclaration
            // 
            this.gridViewPaidDeclaration.GridControl = this.gridControlPaidDeclaration;
            this.gridViewPaidDeclaration.Name = "gridViewPaidDeclaration";
            this.gridViewPaidDeclaration.OptionsBehavior.Editable = false;
            this.gridViewPaidDeclaration.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewPaidDeclaration.OptionsCustomization.AllowFilter = false;
            this.gridViewPaidDeclaration.OptionsCustomization.AllowGroup = false;
            this.gridViewPaidDeclaration.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewPaidDeclaration.OptionsCustomization.AllowRowSizing = true;
            this.gridViewPaidDeclaration.OptionsCustomization.AllowSort = false;
            this.gridViewPaidDeclaration.OptionsView.ShowGroupPanel = false;
            this.gridViewPaidDeclaration.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewPaidDeclaration_FocusedRowChanged);
            // 
            // PaidTaxDeclarationsPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlPaidDeclaration);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "PaidTaxDeclarationsPage";
            this.Size = new System.Drawing.Size(685, 492);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPaidDeclaration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPaidDeclaration)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem btnUndoPay;
        private DevExpress.XtraGrid.GridControl gridControlPaidDeclaration;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPaidDeclaration;
        private DevExpress.XtraBars.BarButtonItem btnPreview;
    }
}
