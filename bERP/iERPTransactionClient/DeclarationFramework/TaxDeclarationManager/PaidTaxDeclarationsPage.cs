﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class PaidTaxDeclarationsPage : DevExpress.XtraEditors.XtraUserControl
    {
        TaxDeclaration[] m_TaxDeclaration;
        DataTable m_PaidDecTable;
        bool m_VATDocument = false;
        public PaidTaxDeclarationsPage()
        {
            InitializeComponent();
            m_PaidDecTable = new DataTable();
            PreparePaidDecDataTable();
            ChangeMainMenuCaptionsForVATDocument();
            LoadPaidDeclarations();
        }

        private void PreparePaidDecDataTable()
        {
            m_PaidDecTable.Columns.Add("ID", typeof(int));
            m_PaidDecTable.Columns.Add("Period", typeof(string));
            m_PaidDecTable.Columns.Add("Paid Amount", typeof(double));
            m_PaidDecTable.Columns.Add("Paid Date", typeof(string));
            m_PaidDecTable.Columns.Add("Tax Center", typeof(TaxCenter));
            m_PaidDecTable.Columns.Add("Paid Document ID", typeof(int));
            m_PaidDecTable.Columns.Add("Document Number", typeof(string));
            gridControlPaidDeclaration.DataSource = m_PaidDecTable;
            gridViewPaidDeclaration.Columns["ID"].Visible = false;
            gridViewPaidDeclaration.Columns["Paid Document ID"].Visible = false;
            gridViewPaidDeclaration.Columns["Paid Amount"].DisplayFormat.FormatType = FormatType.Numeric;
            gridViewPaidDeclaration.Columns["Paid Amount"].DisplayFormat.FormatString = "N";

        }
        private void ChangeMainMenuCaptionsForVATDocument()
        {
            if (TaxDeclarationManager.DeclarationTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(VATDeclarationDocument)).id)
            {
                btnUndoPay.Caption = "&Undo Confirmation";
                m_VATDocument = true;
            }
        }
        public void LoadPaidDeclarations()
        {
            m_PaidDecTable.Clear();
            m_TaxDeclaration = iERPTransactionClient.GetTaxDeclarations(TaxDeclarationManager.DeclarationTypeID, -1, -1);
            foreach (TaxDeclaration declaration in m_TaxDeclaration)
            {
                if (declaration.payDocumentID != -1)
                {
                    DataRow row = m_PaidDecTable.NewRow();
                    row[0] = declaration.id;
                    row[1] = iERPTransactionClient.GetAccountingPeriod((string)iERPTransactionClient.GetSystemParamter("taxDeclarationPeriods"), declaration.periodID).name;
                    row[2] = declaration.paidAmount;
                    row[3] = declaration.payDate.ToShortDateString();
                    row[4] = iERPTransactionClient.GetTaxCenter(declaration.TaxCenter);
                    row[5] = declaration.payDocumentID;
                    row[6] = AccountingClient.GetAccountDocument(declaration.payDocumentID, true).PaperRef;
                    m_PaidDecTable.Rows.Add(row);
                }
            }
            gridControlPaidDeclaration.DataSource = m_PaidDecTable;
            gridControlPaidDeclaration.RefreshDataSource();
        }

        private void btnPreview_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                int rowHandle = gridViewPaidDeclaration.FocusedRowHandle;
                int declarationID = (int)gridViewPaidDeclaration.GetRowCellValue(rowHandle, "ID");
                TaxDeclaration declaration = iERPTransactionClient.GetTaxDeclaration(declarationID);
                DataSet data = declaration.report;
                int periodID = declaration.periodID;
                DocumentType documentType = INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByID(TaxDeclarationManager.DeclarationTypeID);
                IDeclarationSource source = TaxDeclarationSummarySourceFactory.CreateSummarySource(documentType.GetTypeObject(), periodID, data);
                DeclarationViewer viewer = new DeclarationViewer(source);
                viewer.Show(this);
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void gridViewPaidDeclaration_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewPaidDeclaration.SelectedRowsCount == 1)
            {
                btnUndoPay.Enabled = true;
                btnPreview.Enabled = true;
            }
            else
            {
                btnUndoPay.Enabled = false;
                btnPreview.Enabled = false;
            }
        }

        private void btnUndoPay_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (MessageBox.ShowWarningMessage("Are you sure you want to undo the selected tax declaration payment?") == DialogResult.Yes)
                {
                    int rowHandle = gridViewPaidDeclaration.FocusedRowHandle;
                    int documentID = (int)gridViewPaidDeclaration.GetRowCellValue(rowHandle, "Paid Document ID");
                    INTAPS.Accounting.Client.AccountingClient.DeleteGenericDocument(documentID);
                    if (TaxDeclarationManager.TaxDeclarationManagerForm != null)
                        TaxDeclarationManager.TaxDeclarationManagerForm.ReloadSavedAndPaid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

    }
}
