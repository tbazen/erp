﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.UI.ButtonGrid;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class PaymentSelector : DevExpress.XtraEditors.XtraForm
    {
        private double m_TotalPayment = 0;
        private TaxDeclarationFormBase m_DeclarationForm = null;
        private ActivationParameter m_Activation;
        private ButtonGridBodyButtonItem cashItem;
        private ButtonGridBodyButtonItem checkItem;
        private ButtonGridBodyButtonItem bankTransferFromBankItem;
        private ButtonGridBodyButtonItem bankTransferFromCashItem;
        private ButtonGridBodyButtonItem CPOItem;
       
        public PaymentSelector(double paymentAmount)
        {
            m_TotalPayment = paymentAmount;
            InitializeComponent();
            InitializeButtonGrid();
        }
        ButtonGrid buttonGrid;
        public static ButtonGrid CreateButtonGrid()
        {
            ButtonGrid ret = new ButtonGrid();
            ret.Dock = System.Windows.Forms.DockStyle.Fill;

            ret.ShowLeafNode = false;

            //global default style
            ret.RootStyle.Add(StyleType.FontBrush, Brushes.Black);
            ret.RootStyleButtonBody.Add(StyleType.FillBrush, Brushes.White);

            //header buttons global style
            ret.RootStylesCollectionHeaderButton.DefaultStyle.Add(StyleType.FillBrush, System.Drawing.Brushes.LightGray);
            ret.RootStylesCollectionHeaderButton.DefaultStyle.Add(StyleType.Font, new System.Drawing.Font("Arial", 8.0f));
            ret.RootStylesCollectionHeaderButton.DefaultStyle.Add(StyleType.StringFormat
                , StringFormatExtensions.GetStringFormat(StringAlignment.Center, StringAlignment.Center
                , StringFormatFlags.NoWrap, StringTrimming.EllipsisCharacter));

            ret.RootStylesCollectionHeaderButton.SnapStyle.Add(StyleType.Font, new System.Drawing.Font("Arial", 8.0f));
            ret.RootStylesCollectionHeaderButton.SnapStyle.Add(StyleType.StringFormat
                , StringFormatExtensions.GetStringFormat(StringAlignment.Center, StringAlignment.Center
                , StringFormatFlags.NoWrap, StringTrimming.EllipsisCharacter));

            ret.RootStylesCollectionHeaderButton.SelectStyle.Add(StyleType.ButtonWidth, 200);
            ret.RootStylesCollectionHeaderButton.SelectStyle.Add(StyleType.Font, new System.Drawing.Font("Arial", 10.0f, FontStyle.Bold));
            ret.RootStylesCollectionHeaderButton.SelectStyle.Add(StyleType.StringFormat
                    , StringFormatExtensions.GetStringFormat(StringAlignment.Center, StringAlignment.Center
                    , StringFormatFlags.NoWrap, StringTrimming.EllipsisCharacter));

            //page selector buttons
            ret.RootStylesCollectionNavigatorButton.DefaultStyle.Add(StyleType.ButtonWidth, 40);
            ret.RootStylesCollectionNavigatorButton.DefaultStyle.Add(StyleType.ButtonHeight, 20);

            ret.RootStylesCollectionNavigatorButton.SelectStyle.Add(StyleType.ButtonWidth, 40);
            ret.RootStylesCollectionNavigatorButton.SelectStyle.Add(StyleType.ButtonHeight, 20);
            ret.RootStylesCollectionNavigatorButton.SelectStyle.Add(StyleType.FillBrush, Brushes.RoyalBlue);
            ret.RootStylesCollectionNavigatorButton.SelectStyle.Add(StyleType.BroderPen, Pens.Brown);

            ret.RootStylesCollectionNavigatorButton.SnapStyle.Add(StyleType.ButtonWidth, 40);
            ret.RootStylesCollectionNavigatorButton.SnapStyle.Add(StyleType.ButtonHeight, 20);
            ret.RootStylesCollectionNavigatorButton.SnapStyle.Add(StyleType.FillBrush, Brushes.RoyalBlue);

            ret.FilterBoxBackColor = Color.LightGray;
            ret.FilterLabel.ForeColor = Color.Black;
            ret.FilterBox.Width = 400;
            ret.BackColor = Color.FromArgb(0xD9, 0xE0, 0xE0);
            return ret;
        }
        void InitializeButtonGrid()
        {
            buttonGrid = CreateButtonGrid();
            this.Controls.Add(buttonGrid);
            buttonGrid.Dock = DockStyle.Fill;
            buttonGrid.BringToFront();
            buttonGrid.LeafButtonClicked += buttonGrid_LeafButtonClicked;

            this.Size = new System.Drawing.Size(700, 283);
            InitializePayrollPaymengGridButtons();
            AddPayrollPaymentGridButtons();


            buttonGrid.setFilterBox();
            buttonGrid.UpdateLayout();
        }

        private void InitializePayrollPaymengGridButtons()
        {
            cashItem = ButtonGridHelper.CreateButton("", "Cash");
            cashItem.Tag = new FormActivator(this, typeof(PaymentInfoDialog), false, BizNetPaymentMethod.Cash, m_TotalPayment,DateTime.Now);

            checkItem = ButtonGridHelper.CreateButton("", "Check");
            checkItem.Tag = new FormActivator(this, typeof(PaymentInfoDialog), false, BizNetPaymentMethod.Check, m_TotalPayment, DateTime.Now);

            bankTransferFromBankItem = ButtonGridHelper.CreateButton("", "Bank Transfer from Bank");
            bankTransferFromBankItem.Tag = new FormActivator(this, typeof(PaymentInfoDialog), false, BizNetPaymentMethod.BankTransferFromBankAccount, m_TotalPayment, DateTime.Now);

            bankTransferFromCashItem = ButtonGridHelper.CreateButton("", "Bank Transfer from Cash");
            bankTransferFromCashItem.Tag = new FormActivator(this, typeof(PaymentInfoDialog), false, BizNetPaymentMethod.BankTransferFromCash, m_TotalPayment, DateTime.Now); 
        }
        private void AddPayrollPaymentGridButtons()
        {
            buttonGrid.AddButtonGridItem(buttonGrid.rootButtonItem,
                new ArrayChildSource(buttonGrid
                , buttonGrid.rootButtonItem
                , cashItem
                , checkItem
                , bankTransferFromBankItem
                , bankTransferFromCashItem));
        }
        void buttonGrid_LeafButtonClicked(ButtonGrid t, ButtonGridItem z)
        {
            ButtonGridBodyButtonItem b = z as ButtonGridBodyButtonItem;
            if (b == null)
                return;
            IiERPCommandActivator cmd = b.Tag as IiERPCommandActivator;
            if (cmd == null)
                return;
            try
            {
                cmd.Activate();
                //if (cmd.Activate() == DialogResult.OK)
                //    Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error activating this item", ex);
            }
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            buttonGrid.handleKeyDown(new KeyEventArgs(keyData));
            return base.ProcessCmdKey(ref msg, keyData);
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            buttonGrid.handleKeyDown(e);
            base.OnKeyDown(e);
        }
    }
}