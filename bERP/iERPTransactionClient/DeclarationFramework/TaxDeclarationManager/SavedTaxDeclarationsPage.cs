﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using DevExpress.Utils;

namespace BIZNET.iERP.Client
{
    public partial class SavedTaxDeclarationsPage : DevExpress.XtraEditors.XtraUserControl
    {
        TaxDeclaration[] m_TaxDeclaration;
        DataTable m_TaxDecTable;
        TaxDeclarationManager m_manager;
        bool m_VATDocument = false;
        public SavedTaxDeclarationsPage(TaxDeclarationManager manager)
        {
            m_manager = manager;
            InitializeComponent();
            m_TaxDecTable = new DataTable();
            PrepareTaxDecDataTable();
            ChangeMainMenuCaptionsForVATDocument();
            LoadSavedTaxDeclarations();
        }

        private void PrepareTaxDecDataTable()
        {
            m_TaxDecTable.Columns.Add("ID", typeof(int));
            m_TaxDecTable.Columns.Add("Period", typeof(string));
            m_TaxDecTable.Columns.Add("Declaration Date", typeof(string));
            m_TaxDecTable.Columns.Add("Declared Amount", typeof(double));
            m_TaxDecTable.Columns.Add("Tax Center", typeof(TaxCenter));
            m_TaxDecTable.Columns.Add("Period ID", typeof(int));
            gridControlSavedDeclaration.DataSource = m_TaxDecTable;
            gridViewSavedDeclaration.Columns["ID"].Visible = false;
            gridViewSavedDeclaration.Columns["Period ID"].Visible = false;

            gridViewSavedDeclaration.Columns["Declared Amount"].DisplayFormat.FormatType = FormatType.Numeric;
            gridViewSavedDeclaration.Columns["Declared Amount"].DisplayFormat.FormatString = "N";
        }

        private void ChangeMainMenuCaptionsForVATDocument()
        {
            if (TaxDeclarationManager.DeclarationTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(VATDeclarationDocument)).id)
            {
                btnPay.Caption = "&Confirm";
                m_VATDocument = true;
            }
        }
        public void LoadSavedTaxDeclarations()
        {
            m_TaxDecTable.Clear();
            m_TaxDeclaration = iERPTransactionClient.GetTaxDeclarations(TaxDeclarationManager.DeclarationTypeID, -1, -1);
            foreach (TaxDeclaration declaration in m_TaxDeclaration)
            {
                if (declaration.payDocumentID == -1) //Add unpaid declarations
                {
                    DataRow row = m_TaxDecTable.NewRow();
                    row[0] = declaration.id;
                    row[1] = iERPTransactionClient.GetAccountingPeriod((string)iERPTransactionClient.GetSystemParamter("taxDeclarationPeriods"), declaration.periodID).name;
                    row[2] = declaration.declarationDate.ToShortDateString();
                    row[3] = declaration.paidAmount;
                    row[4] = iERPTransactionClient.GetTaxCenter(declaration.TaxCenter);
                    row[5] = declaration.periodID;
                    m_TaxDecTable.Rows.Add(row);
                }
            }
            gridControlSavedDeclaration.DataSource = m_TaxDecTable;
            gridControlSavedDeclaration.RefreshDataSource();
        }

        private void btnPay_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int rowHandle = gridViewSavedDeclaration.FocusedRowHandle;
            double declaredAmount = (double)gridViewSavedDeclaration.GetRowCellValue(rowHandle, "Declared Amount");
            int declarationID = (int)gridViewSavedDeclaration.GetRowCellValue(rowHandle, "ID");
            int periodID = (int)gridViewSavedDeclaration.GetRowCellValue(rowHandle, "Period ID");
            ActivationParameter activation = new ActivationParameter();
            activation.CurrentTaxDeclarationPeriod = iERPTransactionClient.GetAccountingPeriod((string)iERPTransactionClient.GetSystemParamter("taxDeclarationPeriods"), periodID);
            activation.CurrentDeclaredAmount = declaredAmount;
            activation.CurrentTaxDeclarationID = declarationID;
            if (!m_VATDocument)
            {
                if (TaxDeclarationManager.DeclarationTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(IncomeTaxDeclarationDocument)).id)
                {
                    IncomeTaxDeclarationForm incomeDeclaration = new IncomeTaxDeclarationForm(AccountingClient.AccountingClientDefaultInstance, activation);
                    incomeDeclaration.LoadData(null);
                    incomeDeclaration.ShowDialog(TaxDeclarationManager.TaxDeclarationManagerForm);
                }
                else if (TaxDeclarationManager.DeclarationTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(PensionTaxDeclarationDocument)).id)
                {
                    PensionTaxDeclarationForm declaration = new PensionTaxDeclarationForm(AccountingClient.AccountingClientDefaultInstance, activation);
                    declaration.LoadData(null);
                    declaration.ShowDialog(TaxDeclarationManager.TaxDeclarationManagerForm);
                }
                else if (TaxDeclarationManager.DeclarationTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(WithholdingTaxDeclarationDocument)).id)
                {
                    WithholdingTaxDeclarationForm declaration = new WithholdingTaxDeclarationForm(AccountingClient.AccountingClientDefaultInstance, activation);
                    declaration.LoadData(null);
                    declaration.ShowDialog(TaxDeclarationManager.TaxDeclarationManagerForm);
                }
                else if (TaxDeclarationManager.DeclarationTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(VATWithholdingDeclarationDocument)).id)
                {
                    VATWithholdingDeclarationForm declaration = new VATWithholdingDeclarationForm(AccountingClient.AccountingClientDefaultInstance, activation);
                    declaration.LoadData(null);
                    declaration.ShowDialog(TaxDeclarationManager.TaxDeclarationManagerForm);
                }
            }
            else
            {
                if (iERPTransactionClient.IsPreviousDeclarationPeriodConfirmed(TaxDeclarationManager.DeclarationTypeID, periodID))
                {
                    BIZNET.iERP.TypedDataSets.VATDeclarationData data = (BIZNET.iERP.TypedDataSets.VATDeclarationData)iERPTransactionClient.GetTaxDeclaration(declarationID).report;
                    double vatCredit, otherCredit, carryForwardCredit, accumulatedCredit;
                    vatCredit = otherCredit = carryForwardCredit = accumulatedCredit = 0;
                    double vatChargeForCurrPeriod, totalInputTax;

                    vatChargeForCurrPeriod = data.OutputTaxComputation[0].IsVATForCurrentPeriodNull() ? 0 : data.OutputTaxComputation[0].VATForCurrentPeriod;
                    totalInputTax = data.InputTaxComputation[0].IsTotalTaxValueNull() ? 0 : data.InputTaxComputation[0].TotalTaxValue;
                    vatCredit = totalInputTax - vatChargeForCurrPeriod;

                    if (!data.InputTaxComputation[0].IsOtherCreditNull())
                    {
                        otherCredit = data.InputTaxComputation[0].OtherCredit;
                    }
                    if (!data.InputTaxComputation[0].IsCarryForwardCreditNull())
                    {
                        carryForwardCredit = data.InputTaxComputation[0].CarryForwardCredit;
                    }
                    if (!data.InputTaxComputation[0].IsPreviousMonthCreditNull())
                    {
                        accumulatedCredit = data.InputTaxComputation[0].PreviousMonthCredit;
                    }
                    activation.CurrentPeriodCredit = vatCredit + otherCredit;
                    activation.CarryCreditForward = carryForwardCredit;
                    activation.accumulatedCredit = accumulatedCredit;

                    VATDeclarationForm form = new VATDeclarationForm(AccountingClient.AccountingClientDefaultInstance, activation);
                    form.LoadData(null);
                    form.ShowDialog(TaxDeclarationManager.TaxDeclarationManagerForm);

                }
                else
                {
                    MessageBox.ShowErrorMessage(String.Format("You cannot confirm '{0}' VAT declaration since you have not confirmed the previous month declaration.\nPlease make sure you have confirmed the previous month declaration and try again!", activation.CurrentTaxDeclarationPeriod.name));
                }
            }
        }

        private void gridViewSavedDeclaration_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewSavedDeclaration.SelectedRowsCount == 1)
            {
                btnEdit.Enabled = true;
                btnPay.Enabled = true;
                btnDelete.Enabled = true;
                btnPreview.Enabled = true;
            }
            else
            {
                btnEdit.Enabled = false;
                btnPay.Enabled = false;
                btnDelete.Enabled = false;
                btnPreview.Enabled = false;
            }
        }

        private void btnDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (MessageBox.ShowWarningMessage("Are you sure you want to delete the selected tax declaration?") == DialogResult.Yes)
                {
                    int rowHandle = gridViewSavedDeclaration.FocusedRowHandle;
                    int declarationID = (int)gridViewSavedDeclaration.GetRowCellValue(rowHandle, "ID");
                    iERPTransactionClient.DeleteDeclaration(declarationID);
                    LoadSavedTaxDeclarations();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void btnEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int rowHandle = gridViewSavedDeclaration.FocusedRowHandle;
            int declarationID = (int)gridViewSavedDeclaration.GetRowCellValue(rowHandle, "ID");
            TaxDeclarationWizard wizard = new TaxDeclarationWizard(declarationID);
            wizard.Show(this);
        }

        private void btnPreview_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int rowHandle = gridViewSavedDeclaration.FocusedRowHandle;
            int declarationID = (int)gridViewSavedDeclaration.GetRowCellValue(rowHandle, "ID");
            TaxDeclaration declaration = iERPTransactionClient.GetTaxDeclaration(declarationID);
            DataSet data = declaration.report;
            int periodID = declaration.periodID;
            DocumentType documentType = INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByID(TaxDeclarationManager.DeclarationTypeID);
            IDeclarationSource source = TaxDeclarationSummarySourceFactory.CreateSummarySource(documentType.GetTypeObject(), periodID, data);
            DeclarationViewer viewer = new DeclarationViewer(source);
            viewer.Show(this);

        }
    }
}
