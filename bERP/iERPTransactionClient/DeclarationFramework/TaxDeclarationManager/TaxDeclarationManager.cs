﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class TaxDeclarationManager : DevExpress.XtraEditors.XtraForm
    {
        public static TaxDeclarationManager instance = null;
        public static int DeclarationTypeID;
        private SavedTaxDeclarationsPage m_SavedTaxDeclaragePage;
        private PaidTaxDeclarationsPage m_PaidTaxDeclaragePage;
        public static TaxDeclarationManager TaxDeclarationManagerForm;
        
        public TaxDeclarationManager(int delcarationType)
        {
            InitializeComponent();
            DeclarationTypeID = delcarationType;
            m_SavedTaxDeclaragePage = new SavedTaxDeclarationsPage(this);
            m_PaidTaxDeclaragePage = new PaidTaxDeclarationsPage();
            btnSavedDeclaration_LinkClicked(null, null);
            SetTaxDeclarationTitle();
            TaxDeclarationManager.instance = this;
            
        }
        public void ReloadSavedAndPaid()
        {
            if (m_SavedTaxDeclaragePage != null)
                m_SavedTaxDeclaragePage.LoadSavedTaxDeclarations();
            if (m_PaidTaxDeclaragePage != null)
                m_PaidTaxDeclaragePage.LoadPaidDeclarations();
        }
        
        private void SetTaxDeclarationTitle()
        {
            DocumentType documentType = INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByID(DeclarationTypeID);
            if (documentType.GetTypeObject() == typeof(WithholdingTaxDeclarationDocument))
            {
                labelTitle.Text = "Withholding Tax Declaration";
            }
            else if (documentType.GetTypeObject() == typeof(IncomeTaxDeclarationDocument))
            {
                labelTitle.Text = "Income Tax Declaration";
            }
            else if (documentType.GetTypeObject() == typeof(PensionTaxDeclarationDocument))
            {
                labelTitle.Text = "Pension Contribution Declaration";
                btnSavedDeclaration.Caption = "Saved Pension Contributions";
                btnPaidDeclaration.Caption = "Paid Pension Contributions";
            }
            else if (documentType.GetTypeObject() == typeof(VATDeclarationDocument))
            {
                labelTitle.Text = "Value Added Tax Declaration";
            }
            else if (documentType.GetTypeObject() == typeof(VATWithholdingDeclarationDocument))
            {
                labelTitle.Text = "VAT Withholding Declaration";
            }
        }

        private void btnNewTaxDeclaration_Click(object sender, EventArgs e)
        {
            TaxDeclarationWizard declaration = new TaxDeclarationWizard();
            declaration.Show();
        }

        private void btnSavedDeclaration_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            AddSavedDeclarationPage();
        }

        private void AddSavedDeclarationPage()
        {
            panConfigPages.Controls.Clear();
            panConfigPages.Controls.Add(m_SavedTaxDeclaragePage);
            m_SavedTaxDeclaragePage.Dock = DockStyle.Fill;
        }

        private void btnPaidDeclaration_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            AddPaidDeclarationPage();
        }

        private void AddPaidDeclarationPage()
        {
            panConfigPages.Controls.Clear();
            panConfigPages.Controls.Add(m_PaidTaxDeclaragePage);
            m_PaidTaxDeclaragePage.Dock = DockStyle.Fill;
        }
    }
}