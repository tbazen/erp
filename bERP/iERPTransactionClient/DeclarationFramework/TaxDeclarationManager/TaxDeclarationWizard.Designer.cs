﻿namespace BIZNET.iERP.Client
{
    partial class TaxDeclarationWizard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TaxDeclarationWizard));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.wizardSaveDeclaration = new DevExpress.XtraWizard.WizardControl();
            this.wizPageDocumentList = new DevExpress.XtraWizard.WizardPage();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.cmbPeriod = new BIZNET.iERP.Client.PeriodSelector();
            this.dateDeclaration = new BIZNET.iERP.Client.BNDualCalendar();
            this.gridControlCandidates = new DevExpress.XtraGrid.GridControl();
            this.gridViewCandidates = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.chkSelectAll = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wizPageDeclarationPreview = new DevExpress.XtraWizard.WizardPage();
            this.panPreview = new DevExpress.XtraEditors.PanelControl();
            this.panNavigate = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cmbTaxCenters = new DevExpress.XtraEditors.ComboBoxEdit();
            this.previewBar3 = new DevExpress.XtraPrinting.Preview.PreviewBar();
            ((System.ComponentModel.ISupportInitialize)(this.wizardSaveDeclaration)).BeginInit();
            this.wizardSaveDeclaration.SuspendLayout();
            this.wizPageDocumentList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCandidates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCandidates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.wizPageDeclarationPreview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panNavigate)).BeginInit();
            this.panNavigate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTaxCenters.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // wizardSaveDeclaration
            // 
            this.wizardSaveDeclaration.Appearance.ExteriorPage.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wizardSaveDeclaration.Appearance.ExteriorPage.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.wizardSaveDeclaration.Appearance.ExteriorPage.Options.UseFont = true;
            this.wizardSaveDeclaration.Appearance.ExteriorPage.Options.UseForeColor = true;
            this.wizardSaveDeclaration.Appearance.ExteriorPageTitle.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wizardSaveDeclaration.Appearance.ExteriorPageTitle.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.wizardSaveDeclaration.Appearance.ExteriorPageTitle.Options.UseFont = true;
            this.wizardSaveDeclaration.Appearance.ExteriorPageTitle.Options.UseForeColor = true;
            this.wizardSaveDeclaration.Appearance.Page.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wizardSaveDeclaration.Appearance.Page.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.wizardSaveDeclaration.Appearance.Page.Options.UseFont = true;
            this.wizardSaveDeclaration.Appearance.Page.Options.UseForeColor = true;
            this.wizardSaveDeclaration.Appearance.PageTitle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wizardSaveDeclaration.Appearance.PageTitle.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.wizardSaveDeclaration.Appearance.PageTitle.Options.UseFont = true;
            this.wizardSaveDeclaration.Appearance.PageTitle.Options.UseForeColor = true;
            this.wizardSaveDeclaration.Controls.Add(this.wizPageDocumentList);
            this.wizardSaveDeclaration.Controls.Add(this.wizPageDeclarationPreview);
            this.wizardSaveDeclaration.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wizardSaveDeclaration.Image = ((System.Drawing.Image)(resources.GetObject("wizardSaveDeclaration.Image")));
            this.wizardSaveDeclaration.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.wizardSaveDeclaration.Location = new System.Drawing.Point(0, 0);
            this.wizardSaveDeclaration.Name = "wizardSaveDeclaration";
            this.wizardSaveDeclaration.Pages.AddRange(new DevExpress.XtraWizard.BaseWizardPage[] {
            this.wizPageDocumentList,
            this.wizPageDeclarationPreview});
            this.wizardSaveDeclaration.ShowHeaderImage = true;
            this.wizardSaveDeclaration.Size = new System.Drawing.Size(852, 613);
            this.wizardSaveDeclaration.SelectedPageChanging += new DevExpress.XtraWizard.WizardPageChangingEventHandler(this.wizardSaveDeclaration_SelectedPageChanging);
            this.wizardSaveDeclaration.CancelClick += new System.ComponentModel.CancelEventHandler(this.wizardSaveDeclaration_CancelClick);
            // 
            // wizPageDocumentList
            // 
            this.wizPageDocumentList.Controls.Add(this.layoutControl1);
            this.wizPageDocumentList.Name = "wizPageDocumentList";
            this.wizPageDocumentList.Size = new System.Drawing.Size(820, 468);
            this.wizPageDocumentList.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wizPageDocumentList_PageValidating);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.cmbPeriod);
            this.layoutControl1.Controls.Add(this.dateDeclaration);
            this.layoutControl1.Controls.Add(this.gridControlCandidates);
            this.layoutControl1.Controls.Add(this.chkSelectAll);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(820, 468);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // cmbPeriod
            // 
            this.cmbPeriod.Location = new System.Drawing.Point(623, 4);
            this.cmbPeriod.Name = "cmbPeriod";
            this.cmbPeriod.PeriodType = "AP_Tax_Declaration";
            this.cmbPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPeriod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbPeriod.Size = new System.Drawing.Size(193, 20);
            this.cmbPeriod.StyleController = this.layoutControl1;
            this.cmbPeriod.TabIndex = 9;
            this.cmbPeriod.SelectedIndexChanged += new System.EventHandler(this.cmbPeriod_SelectedIndexChanged);
            // 
            // dateDeclaration
            // 
            this.dateDeclaration.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateDeclaration.Location = new System.Drawing.Point(39, 2);
            this.dateDeclaration.Name = "dateDeclaration";
            this.dateDeclaration.ShowEthiopian = true;
            this.dateDeclaration.ShowGregorian = true;
            this.dateDeclaration.ShowTime = false;
            this.dateDeclaration.Size = new System.Drawing.Size(411, 21);
            this.dateDeclaration.TabIndex = 8;
            this.dateDeclaration.VerticalLayout = false;
            // 
            // gridControlCandidates
            // 
            this.gridControlCandidates.Location = new System.Drawing.Point(2, 30);
            this.gridControlCandidates.MainView = this.gridViewCandidates;
            this.gridControlCandidates.Name = "gridControlCandidates";
            this.gridControlCandidates.Size = new System.Drawing.Size(816, 413);
            this.gridControlCandidates.TabIndex = 7;
            this.gridControlCandidates.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCandidates});
            // 
            // gridViewCandidates
            // 
            this.gridViewCandidates.GridControl = this.gridControlCandidates;
            this.gridViewCandidates.Name = "gridViewCandidates";
            this.gridViewCandidates.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewCandidates.OptionsCustomization.AllowFilter = false;
            this.gridViewCandidates.OptionsCustomization.AllowGroup = false;
            this.gridViewCandidates.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewCandidates.OptionsFind.AlwaysVisible = true;
            this.gridViewCandidates.OptionsFind.FindDelay = 100;
            this.gridViewCandidates.OptionsFind.FindMode = DevExpress.XtraEditors.FindMode.Always;
            this.gridViewCandidates.OptionsFind.ShowFindButton = false;
            this.gridViewCandidates.OptionsView.ShowGroupPanel = false;
            this.gridViewCandidates.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewCandidates_CustomRowCellEdit);
            // 
            // chkSelectAll
            // 
            this.chkSelectAll.Location = new System.Drawing.Point(2, 447);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSelectAll.Properties.Appearance.Options.UseFont = true;
            this.chkSelectAll.Properties.Caption = "Select All";
            this.chkSelectAll.Size = new System.Drawing.Size(816, 19);
            this.chkSelectAll.StyleController = this.layoutControl1;
            this.chkSelectAll.TabIndex = 6;
            this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(820, 468);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.chkSelectAll;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 445);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(820, 23);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControlCandidates;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(820, 417);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.dateDeclaration;
            this.layoutControlItem1.CustomizationFormText = "Date:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(582, 28);
            this.layoutControlItem1.Text = "Date:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(34, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.cmbPeriod;
            this.layoutControlItem2.CustomizationFormText = "Period:";
            this.layoutControlItem2.Location = new System.Drawing.Point(582, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(238, 28);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem2.Text = "Period:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(34, 13);
            // 
            // wizPageDeclarationPreview
            // 
            this.wizPageDeclarationPreview.Controls.Add(this.panPreview);
            this.wizPageDeclarationPreview.Controls.Add(this.panNavigate);
            this.wizPageDeclarationPreview.Name = "wizPageDeclarationPreview";
            this.wizPageDeclarationPreview.Size = new System.Drawing.Size(820, 468);
            this.wizPageDeclarationPreview.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wizPageDeclarationPreview_PageValidating);
            this.wizPageDeclarationPreview.PageCommit += new System.EventHandler(this.wizPageDeclarationPreview_PageCommit);
            // 
            // panPreview
            // 
            this.panPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panPreview.Location = new System.Drawing.Point(0, 32);
            this.panPreview.Name = "panPreview";
            this.panPreview.Size = new System.Drawing.Size(820, 436);
            this.panPreview.TabIndex = 0;
            // 
            // panNavigate
            // 
            this.panNavigate.Controls.Add(this.labelControl1);
            this.panNavigate.Controls.Add(this.cmbTaxCenters);
            this.panNavigate.Dock = System.Windows.Forms.DockStyle.Top;
            this.panNavigate.Location = new System.Drawing.Point(0, 0);
            this.panNavigate.Name = "panNavigate";
            this.panNavigate.Size = new System.Drawing.Size(820, 32);
            this.panNavigate.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(355, 8);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(94, 18);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Tax Centers:";
            // 
            // cmbTaxCenters
            // 
            this.cmbTaxCenters.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbTaxCenters.Location = new System.Drawing.Point(455, 5);
            this.cmbTaxCenters.Name = "cmbTaxCenters";
            this.cmbTaxCenters.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTaxCenters.Properties.Appearance.Options.UseFont = true;
            this.cmbTaxCenters.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.SpinRight),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.SpinLeft, "", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.cmbTaxCenters.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbTaxCenters.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.comboBoxEdit1_Properties_ButtonClick);
            this.cmbTaxCenters.Size = new System.Drawing.Size(362, 22);
            this.cmbTaxCenters.TabIndex = 1;
            this.cmbTaxCenters.SelectedIndexChanged += new System.EventHandler(this.cmbTaxCenters_SelectedIndexChanged);
            this.cmbTaxCenters.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.cmbTaxCenters_QueryPopUp);
            // 
            // previewBar3
            // 
            this.previewBar3.BarName = "Main Menu";
            this.previewBar3.DockCol = 0;
            this.previewBar3.DockRow = 0;
            this.previewBar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.previewBar3.OptionsBar.MultiLine = true;
            this.previewBar3.OptionsBar.UseWholeRow = true;
            this.previewBar3.Text = "Main Menu";
            // 
            // TaxDeclarationWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 613);
            this.Controls.Add(this.wizardSaveDeclaration);
            this.Name = "TaxDeclarationWizard";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.TaxDeclarationWizard_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.wizardSaveDeclaration)).EndInit();
            this.wizardSaveDeclaration.ResumeLayout(false);
            this.wizPageDocumentList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCandidates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCandidates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.wizPageDeclarationPreview.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panNavigate)).EndInit();
            this.panNavigate.ResumeLayout(false);
            this.panNavigate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTaxCenters.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraWizard.WizardControl wizardSaveDeclaration;
        private DevExpress.XtraWizard.WizardPage wizPageDocumentList;
        private DevExpress.XtraWizard.WizardPage wizPageDeclarationPreview;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraPrinting.Preview.PreviewBar previewBar3;
        private DevExpress.XtraEditors.CheckEdit chkSelectAll;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.GridControl gridControlCandidates;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCandidates;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private BIZNET.iERP.Client.BNDualCalendar dateDeclaration;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.PanelControl panPreview;
        private DevExpress.XtraEditors.PanelControl panNavigate;
        private DevExpress.XtraEditors.ComboBoxEdit cmbTaxCenters;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private PeriodSelector cmbPeriod;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    }
}
