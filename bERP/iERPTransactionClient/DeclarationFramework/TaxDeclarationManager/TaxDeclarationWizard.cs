﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraWizard;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Payroll;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraRichEdit.API.Word;
using DevExpress.XtraSplashScreen;
using INTAPS.Payroll.Client;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Client
{
    public partial class TaxDeclarationWizard : XtraForm
    {
        private bool m_CandidateDocsLoaded = false;
        private IDeclarationSource m_source;
        //private int[] m_DocumentIDs;
        private int[] m_RejectedDocumentIDs;
        private DevExpress.XtraReports.UI.XtraReport m_Report;
        private int m_TaxDeclarationID = -1;
        private TaxDeclaration m_declarationToUpdate;
        private DataTable m_CandidatesTable;
        RepositoryItemCheckEdit rejectCheckMark, disabledRejectCheckMark, selectCheckMark;
        bool m_VATDocument = false;
        SplashScreenManager waitScreen;
        public TaxDeclarationWizard()
        {
            InitializeComponent();
            waitScreen = new SplashScreenManager(this, typeof(global::BIZNET.iERP.Client.WaitForm1), true, true);
            rejectCheckMark = new RepositoryItemCheckEdit();
            disabledRejectCheckMark = new RepositoryItemCheckEdit();
            selectCheckMark = new RepositoryItemCheckEdit();
            selectCheckMark.CheckedChanged += Selection_Changed;
            disabledRejectCheckMark.ReadOnly = true;
            gridControlCandidates.RepositoryItems.Add(rejectCheckMark);
            gridControlCandidates.RepositoryItems.Add(disabledRejectCheckMark);
            gridControlCandidates.RepositoryItems.Add(selectCheckMark);
            m_VATDocument = IsDeclarationVATDocument();
            SetupDocumentListsWizardPage(new WizardPageChangingEventArgs(null, wizPageDocumentList,Direction.Forward));
            cmbPeriod.selectedPeriod = iERPTransactionClient.GetAccountingPeriod(cmbPeriod.PeriodType, cmbPeriod.selectedPeriod.fromDate.AddDays(-1));
        }

        public TaxDeclarationWizard(int declarationID)
            : this()
        {
            m_TaxDeclarationID = declarationID;
            m_declarationToUpdate = iERPTransactionClient.GetTaxDeclaration(m_TaxDeclarationID);
            
        }
       
        private void PrepareDataTable()
        {
            m_CandidatesTable = new DataTable();
            m_CandidatesTable.Columns.Add("Select", typeof(bool));
            m_CandidatesTable.Columns.Add("Reference No.", typeof(string));
            m_CandidatesTable.Columns.Add("Name", typeof(string));
            m_CandidatesTable.Columns.Add("TIN",typeof(string));
            m_CandidatesTable.Columns.Add("Document Source", typeof(string));
            m_CandidatesTable.Columns.Add("Tax Amount", typeof(double));
            m_CandidatesTable.Columns.Add("Date", typeof(DateTime));
            m_CandidatesTable.Columns.Add("DocID", typeof(int));
            m_CandidatesTable.Columns.Add("Tax Center", typeof(TaxCenter));
            if (m_TaxDeclarationID != -1 && m_VATDocument)
            {
                m_CandidatesTable.Columns.Add("Reject", typeof(bool));
            }

            gridControlCandidates.DataSource = m_CandidatesTable;
            gridViewCandidates.Columns["DocID"].Visible = false;
            gridViewCandidates.Columns["Reference No."].OptionsColumn.AllowEdit = false;
            gridViewCandidates.Columns["Name"].OptionsColumn.AllowEdit = true;
            gridViewCandidates.Columns["TIN"].OptionsColumn.AllowEdit = false;
            gridViewCandidates.Columns["Document Source"].OptionsColumn.AllowEdit = false;
            gridViewCandidates.Columns["Tax Amount"].OptionsColumn.AllowEdit = false;
            gridViewCandidates.Columns["Date"].OptionsColumn.AllowEdit = false;
            gridViewCandidates.Columns["Tax Center"].OptionsColumn.AllowEdit = true;
            if (m_TaxDeclarationID != -1 && m_VATDocument)
            {
                gridViewCandidates.Columns["Reject"].OptionsColumn.AllowSize = false;
                gridViewCandidates.Columns["Reject"].OptionsColumn.FixedWidth = true;
                gridViewCandidates.Columns["Reject"].Width = 40;
            }

            gridViewCandidates.Columns["Select"].OptionsColumn.AllowSize = false;
            gridViewCandidates.Columns["Select"].OptionsColumn.FixedWidth = true;
            gridViewCandidates.Columns["Select"].Width = 40;
            gridViewCandidates.Columns["Select"].ColumnEdit = selectCheckMark;
            
        }

        private void wizardSaveDeclaration_SelectedPageChanging(object sender, DevExpress.XtraWizard.WizardPageChangingEventArgs e)
        {
            if (e.Direction == Direction.Forward)
            {
                SetupDocumentListsWizardPage(e);
                SetupDeclarationPreviewWizardPage(e);
            }
            else
            {
              ChangeWizPagesNextButtonText(e);
            }
        }

        private void SetupDocumentListsWizardPage(WizardPageChangingEventArgs e)
        {
            if (e.Page == wizPageDocumentList)
            {
                wizardSaveDeclaration.NextText = "&Preview";

                if (!m_CandidateDocsLoaded)
                {
                    wizPageDocumentList.Text = "Step 1 - Select documents to Save for Declaration";
                    wizPageDocumentList.DescriptionText = "Please select date, period and check on documents to save for declaration and click 'Preview'";
                    dateDeclaration.DateTime = DateTime.Now;
                    PrepareDataTable();
                    LoadPeriods();
                    if (m_TaxDeclarationID != -1)
                    {
                        string declarationPeriod = (string)iERPTransactionClient.GetSystemParamter("taxDeclarationPeriods");
                        AccountingPeriod period = iERPTransactionClient.GetAccountingPeriod(declarationPeriod, m_declarationToUpdate.periodID);
                        cmbPeriod.selectedPeriod= period;
                    }
                    //LoadCandidateDocuments();
                    m_CandidateDocsLoaded = true;
                }
           
            }
        }

        void Selection_Changed(object sender, EventArgs e)
        {
            gridViewCandidates.CloseEditor();
            bool check = (bool)gridViewCandidates.GetRowCellValue(gridViewCandidates.FocusedRowHandle, "Select");
            if (!check)
                gridViewCandidates.SetRowCellValue(gridViewCandidates.FocusedRowHandle, "Reject", false);
        }

        private void LoadPeriods()
        {
            string text = (string)iERPTransactionClient.GetSystemParamter("taxDeclarationPeriods");
            //AccountingPeriod[] periods = iERPTransactionClient.GetAccountingPeriodsInFiscalYear(text, Globals.CurrentFiscalYear-2);
            //cmbPeriod.Properties.Items.AddRange(periods);
            //periods = iERPTransactionClient.GetAccountingPeriodsInFiscalYear(text, Globals.CurrentFiscalYear - 1);
            //cmbPeriod.Properties.Items.AddRange(periods);
            //periods = iERPTransactionClient.GetAccountingPeriodsInFiscalYear(text, Globals.CurrentFiscalYear);
            //cmbPeriod.Properties.Items.AddRange(periods);
            //int index = 0;
            //foreach (AccountingPeriod p in cmbPeriod.Properties.Items)
            //{
            //    if (Globals.CurrentTaxDeclarationPeriod != null)
            //    {
            //        if (p.id == Globals.CurrentTaxDeclarationPeriod.id)
            //        {
            //            break;
            //        }
            //        index++;
            //    }
            //}
            //if (index < cmbPeriod.Properties.Items.Count)
            //{
            //    if (cmbPeriod.EditValue != null)
            //        cmbPeriod.EditValue = cmbPeriod.Properties.Items[index];
            //}
        }

        int[] taxCenterIDs;
        private void LoadCandidateDocuments()
        {
            try
            {
                int declarationType = TaxDeclarationManager.DeclarationTypeID;
                double[] taxAmount;
                int[] documentIDs = null;

                waitScreen.ShowWaitForm();
                waitScreen.SetWaitFormDescription("Loading Documents");
                if (m_TaxDeclarationID == -1) //New declaration
                {
                    documentIDs = iERPTransactionClient.GetCandidatesForDeclaration(declarationType, dateDeclaration.DateTime.Date, cmbPeriod.selectedPeriodID, out taxCenterIDs, out taxAmount);
                }
                else
                {
                    if (m_declarationToUpdate.periodID == cmbPeriod.selectedPeriodID)
                    {
                        dateDeclaration.DateTime = m_declarationToUpdate.declarationDate;
                        documentIDs = m_declarationToUpdate.declaredDocuments;
                        if (m_VATDocument)
                            m_RejectedDocumentIDs = m_declarationToUpdate.rejectedDocuments;
                        taxAmount = GetTaxAmountFromTaxDeclarationReport(documentIDs, m_declarationToUpdate.report);
                    }
                    else
                    {
                        documentIDs = iERPTransactionClient.GetCandidatesForDeclaration(declarationType, dateDeclaration.DateTime.Date, cmbPeriod.selectedPeriodID, out taxCenterIDs, out taxAmount);
                    }
                }
                m_CandidatesTable.Rows.Clear();

                for (int i = 0; i < documentIDs.Length; i++)
                {
                    AccountDocument doc = INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(documentIDs[i], false);
                    string columnName, name, tin;
                    DataRow row = m_CandidatesTable.NewRow();
                    bool check = m_TaxDeclarationID == -1 ? false : true;
                    row[0] = check;
                   // row[1] = doc.PaperRef;
                    row[1] = AccountingClient.getDocumentSerials(doc.AccountDocumentID)[0].reference;
                    string description = GetDocumentDescription(doc, out columnName, out name, out tin);
                    waitScreen.SetWaitFormDescription("Loading Documents: " + name);
                    HideColumns(columnName, tin);
                    if (columnName != null)
                    {
                        gridViewCandidates.Columns[2].Caption = columnName;
                        row[2] = name;
                        gridViewCandidates.RefreshData();
                    }
                    if (tin != null)
                    {
                        row[3] = tin;
                    }
                    row[4] = description;
                    row[5] = TSConstants.FormatBirr(taxAmount[i]);
                    row[6] = doc.DocumentDate;
                    row[7] = doc.AccountDocumentID;
                    row[8] = new TaxCenter();// GetTaxCenter(doc, false);

                    if (m_TaxDeclarationID != -1 && m_VATDocument)
                    {
                        row[9] = IsDocumentRejected(documentIDs[i]);
                    }
                    m_CandidatesTable.Rows.Add(row);
                    //System.Threading.Thread.Sleep(25);
                }
                gridViewCandidates.Columns["Date"].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                gridControlCandidates.DataSource = m_CandidatesTable;
                gridControlCandidates.RefreshDataSource();
                if (m_TaxDeclarationID != -1 && m_declarationToUpdate.periodID == cmbPeriod.selectedPeriodID)
                {
                    AddOtherCandidateDocumentsForUpdate(declarationType, ref taxAmount, ref documentIDs);
                }
                else
                {
                    chkSelectAll.Checked = true;
                    chkSelectAll_CheckedChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                if (waitScreen.IsSplashFormVisible)
                    waitScreen.CloseWaitForm();
                MessageBox.ShowErrorMessage(ex.Message);
            }
            finally
            {
                if (waitScreen.IsSplashFormVisible)
                    waitScreen.CloseWaitForm();
            }
        }

        private TaxCenter GetTaxCenter(AccountDocument doc,bool newCandidate)
        {
            TaxCenter center = null;

            if (newCandidate)
            {
                center = GetTaxCenterForNewDeclaration(doc);
                return center;
            }
            else
            {
                if (m_TaxDeclarationID != -1)
                {
                    if (m_declarationToUpdate.periodID == cmbPeriod.selectedPeriodID)
                    {
                        center = iERPTransactionClient.GetTaxCenter(m_declarationToUpdate.TaxCenter);
                    }
                    else
                        center = GetTaxCenterForNewDeclaration(doc);
                }
                else
                {
                    center = GetTaxCenterForNewDeclaration(doc);
                }
                return center;
            }
        }

        private static TaxCenter GetTaxCenterForNewDeclaration(AccountDocument doc)
        {
            TaxCenter center = null;
            object[] vals = iERPTransactionClient.GetSystemParameters(new string[] { "companyProfile" });
            if (TaxDeclarationManager.DeclarationTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(IncomeTaxDeclarationDocument)).id
                || TaxDeclarationManager.DeclarationTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(PensionTaxDeclarationDocument)).id)
            {
                AccountDocument pDoc = INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(doc.AccountDocumentID, true);
                if (pDoc is PayrollSetDocument)
                {
                    foreach (PayrollDocument payroll in ((PayrollSetDocument)pDoc).payrolls)
                    {
                        TaxCenter thisCenter=null;
                        if (payroll != null)
                        {
                            Employee emp = PayrollClient.GetEmployee(payroll.employee.id);
                            thisCenter = iERPTransactionClient.GetTaxCenter(emp.TaxCenter);
                        }
                        else
                        {
                            if (vals != null)
                            {
                                if (vals.Length > 0 && vals[0] != null)
                                {
                                    CompanyProfile loadedProfile = (CompanyProfile)vals[0];
                                    thisCenter = iERPTransactionClient.GetTaxCenter(loadedProfile.VATTaxCenter);
                                }
                            }
                        }
                        if (center == null)
                            center = thisCenter;
                        else
                        {
                            if (center.ID != thisCenter.ID)
                                throw new ServerUserMessage("Mixed tax center is not supported in a single payroll");
                        }
                    }
                }
                else if (pDoc is GroupPayrollDocument)
                {
                    GroupPayrollDocument gpayroll=pDoc as GroupPayrollDocument;
                    center = iERPTransactionClient.GetTaxCenter(gpayroll.taxCenter);
                }
               
            }
            
            else
            {

                if (vals != null)
                {
                    if (vals.Length > 0 && vals[0] != null)
                    {
                        CompanyProfile loadedProfile = (CompanyProfile)vals[0];
                        if (TaxDeclarationManager.DeclarationTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(WithholdingTaxDeclarationDocument)).id)
                        {
                            center = iERPTransactionClient.GetTaxCenter(loadedProfile.WithholdingTaxCenter);
                        }
                        else
                        {
                            center = iERPTransactionClient.GetTaxCenter(loadedProfile.VATTaxCenter);
                        }
                    }
                }
            }
            return center;
        }

        private void HideColumns(string columnName, string tin)
        {
            if (columnName == null)
            {
                gridViewCandidates.Columns["Name"].Visible = false;
                gridViewCandidates.RefreshData();
            }
            if (tin == null)
            {
                gridViewCandidates.Columns["TIN"].Visible = false;
                gridViewCandidates.RefreshData();
            }
        }

        private bool IsDocumentRejected(int docID)
        {
            bool rejected = false;
            if (m_RejectedDocumentIDs != null)
            {
                foreach (int doc in m_RejectedDocumentIDs)
                {
                    if (docID == doc)
                    {
                        rejected = true;
                        return rejected;
                    }
                }
            }
            return rejected;
        }

        private static double[] GetTaxAmountFromTaxDeclarationReport(int[] documentIDs, DataSet report)
        {
            double[] taxAmount;
            List<double> loadedTaxAmount = new List<double>();
            DocumentType docType = INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByID(TaxDeclarationManager.DeclarationTypeID);
            if (docType.GetTypeObject() == typeof(WithholdingTaxDeclarationDocument))
            {
                TypedDataSets.WithholdingTaxDeclarationData data = (TypedDataSets.WithholdingTaxDeclarationData)report;
                foreach (TypedDataSets.WithholdingTaxDeclarationData.ContinuationRow row in data.Continuation)
                {
                    loadedTaxAmount.Add(Math.Abs(row.Tax));
                }
            }
            else if (docType.GetTypeObject() == typeof(VATWithholdingDeclarationDocument))
            {
                TypedDataSets.VATWithholdingDeclarationData data = (TypedDataSets.VATWithholdingDeclarationData)report;
                foreach (TypedDataSets.VATWithholdingDeclarationData.ContinuationRow row in data.Continuation)
                {
                    loadedTaxAmount.Add(Math.Abs(row.Tax));
                }
            }
            else if (docType.GetTypeObject() == typeof(IncomeTaxDeclarationDocument))
            {
                TypedDataSets.IncomeTaxDeclarationData data = (TypedDataSets.IncomeTaxDeclarationData)report;
                foreach (TypedDataSets.IncomeTaxDeclarationData.ContinuationRow row in data.Continuation)
                {
                    loadedTaxAmount.Add(row.Tax);
                }
            }
            else if (docType.GetTypeObject() == typeof(PensionTaxDeclarationDocument))
            {
                TypedDataSets.PensionTaxDeclarationData data = (TypedDataSets.PensionTaxDeclarationData)report;
                foreach (TypedDataSets.PensionTaxDeclarationData.ContinuationRow row in data.Continuation)
                {
                    loadedTaxAmount.Add(row.TotalContribution);
                }
            }
            else if (docType.GetTypeObject() == typeof(VATDeclarationDocument))
            {
                double totalTranAmount;
                foreach (int docID in documentIDs)
                {
                    totalTranAmount = 0;
                    AccountDocument doc = INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(docID, true);
                    if (doc.DocumentTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(Purchase2Document)).id)
                    {
                        Purchase2Document pDoc = doc as Purchase2Document;
                        foreach (TaxImposed tax in pDoc.taxImposed)
                        {
                            if (tax.TaxType == TaxType.VAT)
                            {
                                loadedTaxAmount.Add(tax.TaxValue);
                                break;
                            }
                        }
                    }
                    else if (doc.DocumentTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(Sell2Document)).id)
                    {
                        Sell2Document salesDoc = doc as Sell2Document;
                        foreach (TaxImposed tax in salesDoc.taxImposed)
                        {
                            if (tax.TaxType == TaxType.VAT)
                            {
                                loadedTaxAmount.Add(tax.TaxValue);
                                break;
                            }
                        }
                    }
                    else if (doc.DocumentTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(ZReportDocument)).id)
                    {
                        ZReportDocument zreport = doc as ZReportDocument;

                        if (zreport.taxImposed.TaxType == TaxType.VAT)
                        {
                            loadedTaxAmount.Add(zreport.taxImposed.TaxValue);
                        }
                    }
                    else if (doc.DocumentTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(AdjustmentDocument)).id)
                    {
                        AccountTransaction[] transactions = INTAPS.Accounting.Client.AccountingClient.GetTransactionOfDocument(docID);
                        foreach (AccountTransaction tran in transactions)
                        {
                            if (tran.AccountID == Convert.ToInt32(iERPTransactionClient.GetSystemParamter("inputVATAccountID")))
                            {
                                if (Account.AmountGreater(tran.Amount, 0))
                                {
                                    totalTranAmount += tran.Amount;
                                }
                            }
                        }
                        if (Account.AmountGreater(totalTranAmount, 0))
                        {
                            loadedTaxAmount.Add(totalTranAmount);
                        }
                    }
                }
                
            }
            taxAmount = loadedTaxAmount.ToArray();
            return taxAmount;
        }

        private void AddOtherCandidateDocumentsForUpdate(int declarationType, ref double[] taxAmount, ref int[] documentIDs)
        {
            documentIDs = iERPTransactionClient.GetCandidatesForDeclaration(declarationType, dateDeclaration.DateTime.Date, cmbPeriod.selectedPeriodID, out taxCenterIDs, out taxAmount);
            for (int i = 0; i < documentIDs.Length; i++)
            {
                AccountDocument doc = INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(documentIDs[i], false);
                string columnName, name, tin;
                DataRow row = m_CandidatesTable.NewRow();
                row[0] = false;
                row[1] = doc.PaperRef;
                string description = GetDocumentDescription(doc, out columnName, out name, out tin);
                HideColumns(columnName, tin);
                if (columnName != null)
                {
                    gridViewCandidates.Columns[2].Caption = columnName;
                    row[2] = name;
                    gridViewCandidates.RefreshData();
                }
                if (tin != null)
                {
                    row[3] = tin;
                }
                row[4] = description;
                row[5] = TSConstants.FormatBirr(taxAmount[i]);
                row[6] = doc.DocumentDate.ToShortDateString();
                row[7] = doc.AccountDocumentID;
                row[8] =new TaxCenter();// GetTaxCenter(doc, true);

                if (m_TaxDeclarationID != -1 && m_VATDocument)
                {
                    row[9] = false;
                }
                m_CandidatesTable.Rows.Add(row);
            }
            gridControlCandidates.DataSource = m_CandidatesTable;

        }

        private string GetDocumentDescription(AccountDocument doc, out string columnName, out string name,out string tin)
        {
            string description=null;
            columnName = name = tin = null;
            if (doc.DocumentTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(Purchase2Document)).id)
            {
                Purchase2Document pDoc = INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(doc.AccountDocumentID, true) as Purchase2Document;
                columnName = "Name";
                TradeRelation supplier = iERPTransactionClient.GetSupplier(pDoc.relationCode);
                if (supplier != null)
                {
                    name = supplier.Name;
                    tin = supplier.TIN;
                }
                else
                {
                    name = "Supplier Not Found in Database";
                    tin = "Supplier Not Found in Database";
                }
                description= "Purchase Receipt";
            }
            else if (doc.DocumentTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(SupplierAdvancePayment2Document)).id)
            {
                SupplierAdvancePayment2Document pDoc = INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(doc.AccountDocumentID, true) as SupplierAdvancePayment2Document;
                columnName = "Name";
                TradeRelation supplier = iERPTransactionClient.GetSupplier(pDoc.relationCode);
                name = supplier.Name;
                tin = supplier.TIN;
                description = "Supplier Advance Payment";
            }
            else if (doc.DocumentTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(Sell2Document)).id)
            {
                Sell2Document sDoc = INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(doc.AccountDocumentID, true) as Sell2Document;
                columnName = "Name";
                TradeRelation customer = iERPTransactionClient.GetCustomer(sDoc.relationCode);
                name = customer.Name;
                tin = customer.TIN;
                description= "Sales Receipt";
            }
            else if (doc.DocumentTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(CustomerAdvancePayment2Document)).id)
            {
                CustomerAdvancePayment2Document sDoc = INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(doc.AccountDocumentID, true) as CustomerAdvancePayment2Document;
                columnName = "Name";
                TradeRelation customer = iERPTransactionClient.GetCustomer(sDoc.relationCode);
                name = customer.Name;
                tin = customer.TIN;
                description = "Customer Advance Payment";
            }
            else if (doc.DocumentTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(ZReportDocument)).id)
            {
                description = "Z-Report";
            }
            else if (doc.DocumentTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(AdjustmentDocument)).id)
            {
                description = "Other Credits";
            }
            else if (doc.DocumentTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(PayrollSetDocument)).id)
            {
                PayrollSetDocument payroll = INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(doc.AccountDocumentID, true) as PayrollSetDocument;

                columnName = "Name";
                name = payroll.title;
                description = "Payroll Document";
            }
            else if (doc.DocumentTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(LaborPaymentDocument)).id)
            {
                columnName = "Name";
                LaborPaymentDocument labDoc = INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(doc.AccountDocumentID, true) as LaborPaymentDocument;
                name = labDoc.paidTo;
                description = "Labor Payment Document";
            }
            else if (doc.DocumentTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(GroupPayrollDocument)).id)
            {
                columnName = "Name";
                GroupPayrollDocument gpDoc = INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(doc.AccountDocumentID, true) as GroupPayrollDocument;
                name = gpDoc.description;
                description = "Group Payroll Document";
            }
            return description;
        }

        private bool IsDeclarationVATDocument()
        {
            bool vatDoc = false;
            if (TaxDeclarationManager.DeclarationTypeID == INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByType(typeof(VATDeclarationDocument)).id)
            {
                vatDoc = true;
                return vatDoc;
            }
            return vatDoc;
        }
        private void SetupDeclarationPreviewWizardPage(WizardPageChangingEventArgs e)
        {
            if (e.Page == wizPageDeclarationPreview)
            {
                string buttonText = m_TaxDeclarationID == -1 ? "&Save" : "&Update";
                wizardSaveDeclaration.NextText = buttonText;
                wizardSaveDeclaration.FinishText = buttonText;
                string text = m_TaxDeclarationID == -1 ? "Step 2 - Preview and Save Declaration" : "Step 2 - Preview and Update Declaration";
                wizPageDeclarationPreview.Text = text;
                wizPageDeclarationPreview.DescriptionText = "";

                PopulateTaxCenterComboBox();

                GeneratePreview(((TaxCenter)cmbTaxCenters.SelectedItem).ID);

            }

        }

        private void GeneratePreview(int taxCenter)
        {
            int[] documents = getSelectedDocuments();
             int[] rejectedDocuments=null;
           // m_DocumentIDs = docIDs.ToArray();
            if (m_VATDocument && m_TaxDeclarationID != -1)
               rejectedDocuments = GetRejectedDocumentIDs(taxCenter);
            double totalAmount;
            DataSet data = iERPTransactionClient.PreviewDeclaration(TaxDeclarationManager.DeclarationTypeID, dateDeclaration.DateTime, cmbPeriod.selectedPeriodID, taxCenter, documents, rejectedDocuments, out totalAmount, m_TaxDeclarationID == -1 ? true : false);
            DocumentType documentType = INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByID(TaxDeclarationManager.DeclarationTypeID);
            IDeclarationSource source = TaxDeclarationSummarySourceFactory.CreateSummarySource(documentType.GetTypeObject(), cmbPeriod.selectedPeriodID, data);
            if (!waitScreen.IsSplashFormVisible)
                waitScreen.ShowWaitForm();
            waitScreen.SetWaitFormDescription("Generating Preview");
            LoadDeclarationPreview(source);
            if (waitScreen.IsSplashFormVisible)
                waitScreen.CloseWaitForm();
        }

        private void PopulateTaxCenterComboBox()
        {
            waitScreen.ShowWaitForm();
            waitScreen.SetWaitFormDescription("Loading Tax Centers");
            cmbTaxCenters.Properties.Items.Clear();
            TaxCenter[] centers = GetSelectedDocumentsTaxCenters();
            foreach (TaxCenter center in centers)
            {
                cmbTaxCenters.Properties.Items.Add(center);
            }
            cmbTaxCenters.SelectedIndex = 0;
            waitScreen.CloseWaitForm();
        }

        private void LoadDeclarationPreview(IDeclarationSource source)
        {
            m_source = source;
            this.Text = m_source.SummaryTitle;
            if (m_source.PageCount == 0)
                return;
            if (m_source.PageCount == 1)
            {
                DevExpress.XtraPrinting.Control.PrintControl cont = CreateReportViewer(0);
                //printBarManager1.PrintControl = cont;
                //this.Controls.Add(cont);
               // wizPageDeclarationPreview.Controls.Clear();
                wizPageDeclarationPreview.Controls["panPreview"].Controls.Clear();
                wizPageDeclarationPreview.Controls["panPreview"].Controls.Add(cont);
                //wizPageDeclarationPreview.Controls.Add(cont);
                return;
            }
            int n = m_source.PageCount;
            DevExpress.XtraTab.XtraTabControl tabc = new DevExpress.XtraTab.XtraTabControl();

            tabc.Dock = DockStyle.Fill;
           // this.Controls.Add(tabc);
           // wizPageDeclarationPreview.Controls.Clear();
           // wizPageDeclarationPreview.Controls.Add(tabc);
            wizPageDeclarationPreview.Controls["panPreview"].Controls.Clear();
            wizPageDeclarationPreview.Controls["panPreview"].Controls.Add(tabc);

            for (int i = 0; i < n; i++)
            {
                DevExpress.XtraTab.XtraTabPage page = new DevExpress.XtraTab.XtraTabPage();
                page.Text = m_source.GetPageTitle(i);
                tabc.TabPages.Add(page);
                page.Controls.Add(CreateReportViewer(i));
            }
            tabc_SelectedPageChanged(tabc, new DevExpress.XtraTab.TabPageChangedEventArgs(null, tabc.TabPages[0]));
            tabc.SelectedPageChanged += tabc_SelectedPageChanged;
        }


        DevExpress.XtraPrinting.Control.PrintControl CreateReportViewer(int index)
        {
            m_Report = m_source.GetPageReport(index);
            DevExpress.XtraPrinting.Control.PrintControl viewer = new DevExpress.XtraPrinting.Control.PrintControl();
            m_Report.CreateDocument();
            viewer.PrintingSystem = m_Report.PrintingSystem;
            m_Report.PrintingSystem.ShowMarginsWarning = false;
            viewer.Dock = DockStyle.Fill;
            return viewer;
        }
        void tabc_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
           // printBarManager1.PrintControl = (DevExpress.XtraPrinting.Control.PrintControl)e.Page.Controls[0];

        }
        
        private void ChangeWizPagesNextButtonText(DevExpress.XtraWizard.WizardPageChangingEventArgs e)
        {
            if (e.Page == wizPageDocumentList)
            {
                wizardSaveDeclaration.NextText = "&Preview";
            }
            if (e.Page == wizPageDeclarationPreview)
            {
                string buttonText = m_TaxDeclarationID == -1 ? "&Save" : "&Update";
                wizardSaveDeclaration.NextText = buttonText;
            }
           
        }

        private void wizPageDocumentList_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            try
            {
                if (e.Direction == Direction.Forward)
                {
                    if (!m_VATDocument)
                    {
                        int count = CountSelected();
                        if (count > 0)
                        {
                            e.Valid = true;
                        }
                        else
                        {
                            e.Valid = false;
                            e.ErrorText = "Please select documents to preview declaration and try again!";
                        }
                    }
                    else
                    {
                        e.Valid = true;
                    }
                }
            }
            catch (Exception ex)
            {
                e.Valid = false;
                e.ErrorText = String.Format("{0}\n{1}", ex.Message, ex.InnerException.Message);
            }
        }

        private int CountSelected()
        {
            int count = 0;
            for (int i = 0; i < gridViewCandidates.DataRowCount; i++)
            {
                if ((bool)gridViewCandidates.GetRowCellValue(i, "Select") == true)
                    count++;
            }
            return count;
        }

        private void wizPageDeclarationPreview_PageCommit(object sender, EventArgs e)
        {
            //ShowPrintDialogBox();
            Close();
        }

        private void ShowPrintDialogBox()
        {
            const string printOptionMessage = "Would you like to view and print the tax declaration form you just saved?";
            if (MessageBox.ShowWarningMessage(printOptionMessage) == DialogResult.Yes)
            {
                using (DevExpress.XtraReports.UI.ReportPrintTool printTool = new DevExpress.XtraReports.UI.ReportPrintTool(m_Report))
                {
                    printTool.PrintDialog();
                }
            }
        }

        private void wizPageDeclarationPreview_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            try
            {
                if (e.Direction == Direction.Forward)
                {
                    if (m_TaxDeclarationID == -1 ||
                        (m_TaxDeclarationID != -1
                        && m_declarationToUpdate.periodID != cmbPeriod.selectedPeriodID)
                        && iERPTransactionClient.CountPaidTaxDeclarations(TaxDeclarationManager.DeclarationTypeID) > 0
                        )
                    {

                       foreach(int taxCenterID in taxCenterIDs)
                        {
                            int taxDeclarationID = iERPTransactionClient.SaveTaxDeclaration(TaxDeclarationManager.DeclarationTypeID, dateDeclaration.DateTime, cmbPeriod.selectedPeriodID, getSelectedDocuments(), taxCenterID);
                        }
                        if (m_VATDocument && gridViewCandidates.DataRowCount == 0)
                        {
                            object[] vals = iERPTransactionClient.GetSystemParameters(new string[] { "companyProfile" });

                            CompanyProfile loadedProfile = (CompanyProfile)vals[0];
                            TaxCenter vatTaxCenter = iERPTransactionClient.GetTaxCenter(loadedProfile.VATTaxCenter);
                            int[] documents = getSelectedDocuments();
                            iERPTransactionClient.SaveTaxDeclaration(TaxDeclarationManager.DeclarationTypeID, dateDeclaration.DateTime, cmbPeriod.selectedPeriodID, documents, vatTaxCenter.ID);
                        }
                        MessageBox.ShowSuccessMessage("Declaration successfully saved!");
                        if (TaxDeclarationManager.TaxDeclarationManagerForm != null)
                            TaxDeclarationManager.TaxDeclarationManagerForm.ReloadSavedAndPaid();
                        e.Valid = true;
                    }
                    if (m_TaxDeclarationID != -1)
                    {
                        TaxDeclaration updatedDeclaration = iERPTransactionClient.GetTaxDeclaration(m_TaxDeclarationID);
                        int[] documents = getSelectedDocuments();
                        updatedDeclaration.declaredDocuments = documents;
                        updatedDeclaration.periodID = cmbPeriod.selectedPeriodID;
                        updatedDeclaration.declarationDate = dateDeclaration.DateTime;
                        if (m_TaxDeclarationID != -1 && m_VATDocument)
                        {
                            int[] rejectedDocs = GetRejectedDocumentIDs(updatedDeclaration.TaxCenter);
                            updatedDeclaration.rejectedDocuments = rejectedDocs;
                        }
                        iERPTransactionClient.UpdateDeclaration(updatedDeclaration);
                        MessageBox.ShowSuccessMessage("Declaration successfully updated!");
                        if (TaxDeclarationManager.TaxDeclarationManagerForm != null)
                            TaxDeclarationManager.TaxDeclarationManagerForm.ReloadSavedAndPaid();
                        e.Valid = true;
                    }
                }
            }
            catch (Exception ex)
            {
                e.Valid = false;
                e.ErrorText = String.Format("{0}\n{1}", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
            }
        }

        private int[] GetRejectedDocumentIDs(int taxCenter)
        {
            List<int> documentIDs = new List<int>();
            for (int i = 0; i < gridViewCandidates.DataRowCount; i++)
            {
                bool rejected = (bool)gridViewCandidates.GetRowCellValue(i, "Reject");
                int center = ((TaxCenter)gridViewCandidates.GetRowCellValue(i, "Tax Center")).ID;
                if (rejected && taxCenter == center)
                    documentIDs.Add((int)gridViewCandidates.GetRowCellValue(i, "DocID"));
            }
            return documentIDs.ToArray();
        }

        private void wizardSaveDeclaration_CancelClick(object sender, CancelEventArgs e)
        {
            Close();
        }


        private void TaxDeclarationWizard_Shown(object sender, EventArgs e)
        {
            //wizardSaveDeclaration.SelectedPage = wizPageDocumentList;
            WizardPageChangingEventArgs eventArgs = new WizardPageChangingEventArgs(null, wizPageDocumentList, Direction.Forward);
            wizardSaveDeclaration_SelectedPageChanging(null, eventArgs);
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (gridViewCandidates.DataRowCount > 0)
            {
                if (chkSelectAll.Checked)
                {
                    SelectAllDocuments();
                }
                else
                    ClearSelection();
            }
        }

        private void SelectAllDocuments()
        {
            for (int i = 0; i < gridViewCandidates.DataRowCount; i++)
            {
                gridViewCandidates.SetRowCellValue(i, "Select", true);
            }
        }

        private void ClearSelection()
        {
            for (int i = 0; i < gridViewCandidates.DataRowCount; i++)
            {
                gridViewCandidates.SetRowCellValue(i, "Select", false);
            }
        }

        private void cmbPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCandidateDocuments();
        }

        private void gridViewCandidates_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.Column.FieldName == "Reject")
            {

                GridView gv = sender as GridView;
                bool saved = (bool)gv.GetRowCellValue(e.RowHandle, "Select");
                if (saved)
                {
                    if (m_declarationToUpdate.periodID == cmbPeriod.selectedPeriodID)
                        e.RepositoryItem = rejectCheckMark;
                    else
                        e.RepositoryItem = disabledRejectCheckMark;
                }
                else
                {
                    e.RepositoryItem = disabledRejectCheckMark;
                }
            }
        }
        
        private TaxCenter[] GetSelectedDocumentsTaxCenters()
        {

            TaxCenter[] ret = new TaxCenter[taxCenterIDs.Length];
            for (int i = 0; i < ret.Length; i++)
            {
                ret[i] = iERPTransactionClient.GetTaxCenter(taxCenterIDs[i]);
            }
            return ret;
            /*object[] vals = iERPTransactionClient.GetSystemParameters(new string[] { "companyProfile" });

            CompanyProfile loadedProfile = (CompanyProfile)vals[0];
            TaxCenter vatTaxCenter = iERPTransactionClient.GetTaxCenter(loadedProfile.VATTaxCenter);

            List<TaxCenter> taxCenters = new List<TaxCenter>();
            for (int i = 0; i < gridViewCandidates.RowCount; i++)
            {
                bool selected = (bool)gridViewCandidates.GetRowCellValue(i, "Select");
                TaxCenter center = (TaxCenter)gridViewCandidates.GetRowCellValue(i, "Tax Center");
                if (selected)
                    taxCenters.Add(center);
            }
            if (taxCenters.Count == 0)
                taxCenters.Add(vatTaxCenter);
            return taxCenters.ToArray();*/
        }

        private int[] getSelectedDocuments()
        {
            List<int> docIDs = new List<int>();
            for (int i = 0; i < gridViewCandidates.DataRowCount; i++)
            {
                bool selected = (bool)gridViewCandidates.GetRowCellValue(i, "Select");
                int docID = (int)gridViewCandidates.GetRowCellValue(i, "DocID");
                if (selected )
                {
                    docIDs.Add(docID);
                }
            }
            return docIDs.ToArray();
        }
        private void comboBoxEdit1_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.SpinRight) //Navigate Next
            {
                if (cmbTaxCenters.SelectedIndex != cmbTaxCenters.Properties.Items.Count - 1)
                {
                    e.Button.Enabled = true;
                    cmbTaxCenters.Properties.Buttons[1].Enabled = true;
                    cmbTaxCenters.SelectedIndex = cmbTaxCenters.SelectedIndex + 1;
                   GeneratePreview(((TaxCenter)cmbTaxCenters.SelectedItem).ID);
                }
                else
                {
                    e.Button.Enabled = false;
                    if (cmbTaxCenters.SelectedIndex - 1 >= 0)
                        cmbTaxCenters.Properties.Buttons[1].Enabled = true;
                }
            }
            else  //Naviage previous
            {

                if (cmbTaxCenters.SelectedIndex != 0)
                {
                    e.Button.Enabled = true;
                    cmbTaxCenters.Properties.Buttons[0].Enabled = true;
                    cmbTaxCenters.SelectedIndex = cmbTaxCenters.SelectedIndex - 1;
                    GeneratePreview(((TaxCenter)cmbTaxCenters.SelectedItem).ID);
                }
                else
                {
                    e.Button.Enabled = false;
                    if (cmbTaxCenters.SelectedIndex + 1 <= cmbTaxCenters.Properties.Items.Count - 1)
                        cmbTaxCenters.Properties.Buttons[0].Enabled = true;
                }
            }
        }

        private void cmbTaxCenters_QueryPopUp(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
        }

        private void cmbTaxCenters_SelectedIndexChanged(object sender, EventArgs e)
        {
           // GeneratePreview(((TaxCenter)cmbTaxCenters.SelectedItem).ID);
        }

    }
}
