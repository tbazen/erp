using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Data;

namespace BIZNET.iERP.Client
{
    public static class TaxDeclarationSummarySourceFactory
    {
        public static IDeclarationSource CreateSummarySource(Type declarationDocumentType, int periodID, DataSet data)
        {
            if (declarationDocumentType == typeof(WithholdingTaxDeclarationDocument))
            {
                return new WithholdingTaxDeclarationSummarySource(periodID, (TypedDataSets.WithholdingTaxDeclarationData)data);
            }
            else if (declarationDocumentType == typeof(VATWithholdingDeclarationDocument))
            {
                return new VATWithholdingDeclarationSummarySource(periodID, (TypedDataSets.VATWithholdingDeclarationData)data);
            }
            else if (declarationDocumentType == typeof(IncomeTaxDeclarationDocument))
            {
                return new IncomeTaxDeclarationSummarySource(periodID, (TypedDataSets.IncomeTaxDeclarationData)data);
            }
            else if (declarationDocumentType == typeof(PensionTaxDeclarationDocument))
            {
                return new PensionTaxDeclarationSummarySource(periodID, (TypedDataSets.PensionTaxDeclarationData)data);
            }
            else if (declarationDocumentType == typeof(VATDeclarationDocument))
            {
                return new VATDeclarationSource(periodID, (TypedDataSets.VATDeclarationData)data);
            }
            throw new Exception("Summary source not defined for this declaration type:" + declarationDocumentType);
        }
    }
}
