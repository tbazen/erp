using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Data;
using DevExpress.XtraReports.UI;

namespace BIZNET.iERP.Client
{
    public class VATDeclarationSource:IDeclarationSource
    {
        TypedDataSets.VATDeclarationData m_data;
        AccountingPeriod m_period;
        double m_VATChargeForThisPeriod, m_TotalInputTax;
        List<object> reportList = new List<object>();
        List<object> reportTitles = new List<object>();
        int m_pageCount;
        public VATDeclarationSource(int periodID, TypedDataSets.VATDeclarationData data)
        {
            m_data = data;
            reportList.AddRange(new object[] { new DeclarationDesign.VATFrongPage(), new DeclarationDesign.VATBackPage()
                , new DeclarationDesign.LocalPurchaseGoodReport(), new DeclarationDesign.LocalPurchaseServiceReport(), new DeclarationDesign.VATAgentCustomersSalesReport() });

            reportTitles.AddRange(new string[]{"Front", "Bank", "Local Good Purchase Report", "Local Service Purchase Report",
                "VAT Agent Customers Sales Report"});

            m_VATChargeForThisPeriod = m_data.OutputTaxComputation[0].VATForCurrentPeriod;
            m_TotalInputTax = m_data.InputTaxComputation[0].TotalTaxValue;
            UpdateZeroValuesToNull();

            PopulateOutputTaxTable();

            PopulateInputTaxTable();

            m_period = iERPTransactionClient.GetAccountingPeriod((string)iERPTransactionClient.GetSystemParamter("taxDeclarationPeriods"), periodID);
            m_pageCount = GetPageCount();
        }

        private void UpdateZeroValuesToNull()
        {
            UpdateOutPutValuesToNull();
            UpdateInputValuesToNull();
            UpdatePurchaseReportValuesToNull();
            UpdateSalesReportValuesToNull();
        }
        private void UpdateOutPutValuesToNull()
        {
            double value;
            foreach (DataColumn column in m_data.OutputTaxComputation.Columns)
            {
                if (double.TryParse(m_data.OutputTaxComputation[0][column].ToString(), out value))
                {
                    if (Account.AmountEqual(value, 0))
                    {
                        m_data.OutputTaxComputation[0][column] = DBNull.Value;
                    }
                }
            }
        }
        private void UpdateInputValuesToNull()
        {
            double value;
            foreach (DataColumn column in m_data.InputTaxComputation.Columns)
            {
                if (double.TryParse(m_data.InputTaxComputation[0][column].ToString(), out value))
                {
                    if (Account.AmountEqual(value, 0))
                    {
                        m_data.InputTaxComputation[0][column] = DBNull.Value;
                    }
                }
            }
        }
        private void UpdatePurchaseReportValuesToNull()
        {
            foreach (TypedDataSets.VATDeclarationData.LocalPurchaseReportRow row in m_data.LocalPurchaseReport)
            {
                if (Account.AmountEqual(row.PurchaseBaseValue, 0))
                    row.SetPurchaseBaseValueNull();
                if (Account.AmountEqual(row.VAT, 0))
                    row.SetVATNull();
                if (Account.AmountEqual(row.WithholdingTax, 0))
                    row.SetWithholdingTaxNull();
            }
            foreach (TypedDataSets.VATDeclarationData.LocalPurchaseServiceReportRow row in m_data.LocalPurchaseServiceReport)
            {
                if (Account.AmountEqual(row.PurchaseBaseValue, 0))
                    row.SetPurchaseBaseValueNull();
                if (Account.AmountEqual(row.VAT, 0))
                    row.SetVATNull();
                if (Account.AmountEqual(row.WithholdingTax, 0))
                    row.SetWithholdingTaxNull();
            }
            foreach (TypedDataSets.VATDeclarationData.NoTINLocalPurchaseGoodReportRow row in m_data.NoTINLocalPurchaseGoodReport)
            {
                if (Account.AmountEqual(row.PurchaseBaseValue, 0))
                    row.SetPurchaseBaseValueNull();
                if (Account.AmountEqual(row.WithholdingTax, 0))
                    row.SetWithholdingTaxNull();
            }
            foreach (TypedDataSets.VATDeclarationData.NoTINLocalPurchaseServiceReportRow row in m_data.NoTINLocalPurchaseServiceReport)
            {
                if (Account.AmountEqual(row.PurchaseBaseValue, 0))
                    row.SetPurchaseBaseValueNull();
                if (Account.AmountEqual(row.WithholdingTax, 0))
                    row.SetWithholdingTaxNull();
            }
        }
        private bool IsLocalGoodPurchaseReportEmpty()
        {
            if (m_data.LocalPurchaseReport.Rows.Count > 0)
            {
                if (m_data.LocalPurchaseReport.Rows[0]["PurchaseBaseValue"] == DBNull.Value)
                    return true;
            }
            else
                return true;
            return false;
        }
        private bool IsLocalServicePurchaseReportEmpty()
        {
            if (m_data.LocalPurchaseServiceReport.Rows.Count > 0)
            {
                if (m_data.LocalPurchaseServiceReport.Rows[0]["PurchaseBaseValue"] == DBNull.Value)
                    return true;
            }
            else
                return true;
            return false;
        }
        private bool IsSaleseReportEmpty()
        {
            if (m_data.VATAgentCustomersSalesReport.Rows.Count > 0)
            {
                if (m_data.VATAgentCustomersSalesReport.Rows[0]["SalesBaseValue"] == DBNull.Value)
                    return true;
            }
            else
                return true;
            return false;
        }
        private void UpdateSalesReportValuesToNull()
        {
            foreach (TypedDataSets.VATDeclarationData.VATAgentCustomersSalesReportRow row in m_data.VATAgentCustomersSalesReport)
            {
                if (Account.AmountEqual(row.SalesBaseValue, 0) || Account.AmountLess(row.SalesBaseValue, 0))
                    row.SetSalesBaseValueNull();
                if (Account.AmountEqual(row.VAT, 0) || Account.AmountLess(row.VAT, 0))
                    row.SetVATNull();
            }
        }

        
        private void PopulateInputTaxTable()
        {
            string birr, cents;
            if (!m_data.InputTaxComputation[0].IsLocalPurchaseValueNull())
            {
                GetBirrCent(m_data.InputTaxComputation[0].LocalPurchaseValue, out birr, out cents);
                m_data.InputTaxComputation[0].LocalPurchaseValueBirr = birr;
                m_data.InputTaxComputation[0].LocalPurchaseValueCent = cents;
            }
            if (!m_data.InputTaxComputation[0].IsVATOnLocalPurchaseNull())
            {
                GetBirrCent(m_data.InputTaxComputation[0].VATOnLocalPurchase, out birr, out cents);
                m_data.InputTaxComputation[0].VATOnLocalPurchaseBirr = birr;
                m_data.InputTaxComputation[0].VATOnLocalPurchaseCent = cents;
            }
            if (!m_data.InputTaxComputation[0].IsImportedPurchaseValueNull())
            {
                GetBirrCent(m_data.InputTaxComputation[0].ImportedPurchaseValue, out birr, out cents);
                m_data.InputTaxComputation[0].ImportedPurchaseValueBirr = birr;
                m_data.InputTaxComputation[0].ImportedPurchaseValueCent = cents;
            }
            if (!m_data.InputTaxComputation[0].IsVATOnImportedPurchaseNull())
            {
                GetBirrCent(m_data.InputTaxComputation[0].VATOnImportedPurchase, out birr, out cents);
                m_data.InputTaxComputation[0].VATOnImportedPurchaseBirr = birr;
                m_data.InputTaxComputation[0].VATOnImportedPurchaseCent = cents;
            }
            if (!m_data.InputTaxComputation[0].IsGeneralExpenseValueNull())
            {
                GetBirrCent(m_data.InputTaxComputation[0].GeneralExpenseValue, out birr, out cents);
                m_data.InputTaxComputation[0].GeneralExpenseValueBirr = birr;
                m_data.InputTaxComputation[0].GeneralExpenseValueCent = cents;
            }
            if (!m_data.InputTaxComputation[0].IsVATOnGeneralExpenseNull())
            {
                GetBirrCent(m_data.InputTaxComputation[0].VATOnGeneralExpense, out birr, out cents);
                m_data.InputTaxComputation[0].VATOnGeneralExpenseBirr = birr;
                m_data.InputTaxComputation[0].VATOnGeneralExpenseCent = cents;
            }
            if (!m_data.InputTaxComputation[0].IsNonVATPurchaseValueNull())
            {
                GetBirrCent(m_data.InputTaxComputation[0].NonVATPurchaseValue, out birr, out cents);
                m_data.InputTaxComputation[0].NonVATPurchaseValueBirr = birr;
                m_data.InputTaxComputation[0].NonVATPurchaseValueCent = cents;
            }
            if (!m_data.InputTaxComputation[0].IsTotalValueNull())
            {
                GetBirrCent(m_data.InputTaxComputation[0].TotalValue, out birr, out cents);
                m_data.InputTaxComputation[0].TotalValueBirr = birr;
                m_data.InputTaxComputation[0].TotalValueCent = cents;
            }
            if (!m_data.InputTaxComputation[0].IsTaxableValueNull())
            {
                GetBirrCent(m_data.InputTaxComputation[0].TaxableValue, out birr, out cents);
                m_data.InputTaxComputation[0].TaxableValueBirr = birr;
                m_data.InputTaxComputation[0].TaxableValueCent = cents;
            }
            if (!m_data.InputTaxComputation[0].IsTotalTaxValueNull())
            {
                GetBirrCent(m_data.InputTaxComputation[0].TotalTaxValue, out birr, out cents);
                m_data.InputTaxComputation[0].TotalTaxValueBirr = birr;
                m_data.InputTaxComputation[0].TotalTaxValueCent = cents;
            }
            if (!m_data.InputTaxComputation[0].IsNetVATNull())
            {
                double netVat = !m_data.OutputTaxComputation[0].IsGovVATOnLocalSalesNull() ? m_data.InputTaxComputation[0].NetVAT + m_data.OutputTaxComputation[0].GovVATOnLocalSales : m_data.InputTaxComputation[0].NetVAT;
                GetBirrCent(netVat, out birr, out cents);
                m_data.InputTaxComputation[0].NetVATBirr = birr;
                m_data.InputTaxComputation[0].NetVATCent = cents;
            }
            else
            {
                if (Account.AmountGreater(m_VATChargeForThisPeriod, m_TotalInputTax))
                {
                    GetBirrCent(m_VATChargeForThisPeriod - m_TotalInputTax, out birr, out cents);
                    m_data.InputTaxComputation[0].NetVATBirr = birr;
                    m_data.InputTaxComputation[0].NetVATCent = cents;
                }
            }
            if (!m_data.InputTaxComputation[0].IsVATCreditNull())
            {
                GetBirrCent(m_data.InputTaxComputation[0].VATCredit, out birr, out cents);
                m_data.InputTaxComputation[0].VATCreditBirr = birr;
                m_data.InputTaxComputation[0].VATCreditCent = cents;
            }
            if (!m_data.InputTaxComputation[0].IsOtherCreditNull())
            {
                GetBirrCent(m_data.InputTaxComputation[0].OtherCredit, out birr, out cents);
                m_data.InputTaxComputation[0].OtherCreditBirr = birr;
                m_data.InputTaxComputation[0].OtherCreditCent = cents;
            }
            if (!m_data.InputTaxComputation[0].IsPreviousMonthCreditNull())
            {
                GetBirrCent(m_data.InputTaxComputation[0].PreviousMonthCredit, out birr, out cents);
                m_data.InputTaxComputation[0].PreviousMonthCreditBirr = birr; //Accumulated Credit
                m_data.InputTaxComputation[0].PreviousMonthCreditCent = cents;
            }
            if (!m_data.InputTaxComputation[0].IsTotalPayableAmountNull())
            {
                GetBirrCent(m_data.InputTaxComputation[0].TotalPayableAmount, out birr, out cents);
                m_data.InputTaxComputation[0].TotalPayableAmountBirr = birr;
                m_data.InputTaxComputation[0].TotalPayableAmountCent = cents;
            }
            if (!m_data.InputTaxComputation[0].IsCarryForwardCreditNull())
            {
                GetBirrCent(m_data.InputTaxComputation[0].CarryForwardCredit, out birr, out cents);
                m_data.InputTaxComputation[0].CarryForwardCreditBirr = birr;
                m_data.InputTaxComputation[0].CarryForwardCreditCent = cents;
            }
        }

        private void PopulateOutputTaxTable()
        {
            string birr, cents;
            if (!m_data.OutputTaxComputation[0].IsTaxableLocalSalesItemsValueNull())
            {
                GetBirrCent(m_data.OutputTaxComputation[0].TaxableLocalSalesItemsValue, out birr, out cents);
                m_data.OutputTaxComputation[0].TaxableLocalSalesItemsValueBirr = birr;
                m_data.OutputTaxComputation[0].TaxableLocalSalesItemsValueCent = cents;
            }
            if (!m_data.OutputTaxComputation[0].IsVATOnLocalSalesNull())
            {
                GetBirrCent(m_data.OutputTaxComputation[0].VATOnLocalSales, out birr, out cents);
                m_data.OutputTaxComputation[0].VATOnLocalSalesBirr = birr;
                m_data.OutputTaxComputation[0].VATOnLocalSalesCent = cents;
            }
            if (!m_data.OutputTaxComputation[0].IsGovVATOnLocalSalesNull())
            {
                GetBirrCent(m_data.OutputTaxComputation[0].GovVATOnLocalSales, out birr, out cents);
                m_data.OutputTaxComputation[0].GovVATOnLocalSalesBirr = "Gov " + birr;
                m_data.OutputTaxComputation[0].GovVATOnLocalSalesCent = cents;
            }
            if (!m_data.OutputTaxComputation[0].IsExportSalesItemsValueNull())
            {
                GetBirrCent(m_data.OutputTaxComputation[0].ExportSalesItemsValue, out birr, out cents);
                m_data.OutputTaxComputation[0].ExportSalesItemsValueBirr = birr;
                m_data.OutputTaxComputation[0].ExportSalesItemsValueCent = cents;
            }
            if (!m_data.OutputTaxComputation[0].IsVATExemptedItemsValueNull())
            {
                GetBirrCent(m_data.OutputTaxComputation[0].VATExemptedItemsValue, out birr, out cents);
                m_data.OutputTaxComputation[0].VATExemptedItemsValueBirr = birr;
                m_data.OutputTaxComputation[0].VATExemptedItemsValueCent = cents;
            }
            if (!m_data.OutputTaxComputation[0].IsRemittedItemsValueNull())
            {
                GetBirrCent(m_data.OutputTaxComputation[0].RemittedItemsValue, out birr, out cents);
                m_data.OutputTaxComputation[0].RemittedItemsValueBirr = birr;
                m_data.OutputTaxComputation[0].RemittedItemsValueCent = cents;
            }
            if (!m_data.OutputTaxComputation[0].IsTotaValueNull())
            {
                GetBirrCent(m_data.OutputTaxComputation[0].TotaValue, out birr, out cents);
                m_data.OutputTaxComputation[0].TotaValueBirr = birr;
                m_data.OutputTaxComputation[0].TotaValueCent = cents;
            }
            if (!m_data.OutputTaxComputation[0].IsVATForCurrentPeriodNull())
            {
                GetBirrCent(m_data.OutputTaxComputation[0].VATForCurrentPeriod, out birr, out cents);
                m_data.OutputTaxComputation[0].VATForCurrentPeriodBirr = birr;
                m_data.OutputTaxComputation[0].VATForCurrentPeriodCent = cents;
            }
        }
        void GetBirrCent(double amount, out string birr, out string cents)
        {
            amount = Math.Round(amount, 2);
            birr= Math.Floor(amount).ToString("#,#0");
            cents= ((amount - Math.Floor(amount))*100).ToString("00");
        }
        private int GetPageCount()
        {
            int page = 5;
            if (IsLocalGoodPurchaseReportEmpty())
            {
                reportList.Remove(new DeclarationDesign.LocalPurchaseGoodReport());
                reportTitles.Remove("Local Good Purchase Report");
                page = page - 1;
            }
            if (IsLocalServicePurchaseReportEmpty())
            {
                reportList.Remove(new DeclarationDesign.LocalPurchaseServiceReport());
                reportTitles.Remove("Local Service Purchase Report");
                page = page - 1;
            }
            if (IsSaleseReportEmpty())
            {
                reportList.Remove(new DeclarationDesign.VATAgentCustomersSalesReport());
                reportTitles.Remove("VAT Agent Customers Sales Report");
                page = page - 1;
            }
            return page;
        }
        #region ISummarySource Members

        public string SummaryTitle
        {
            get 
            {
                return "Value Added Tax declaration for " + m_period.name;
            }
        }

        public int PageCount
        {

            get { return 7; }
        }

        public string GetPageTitle(int index)
        {
            if (index == 0)
                return "Front";
            else if (index == 1)
                return "Back";
            else if (index == 2)
                return "Local Good Purchase Report";
            else if (index == 3)
                return "Local Service Purchase Report";
            else if (index == 4)
                return "No TIN Local Good Purchase Report";
            else if (index == 5)
                return "No TIN Local Service Purchase Report";
            else 
                return "VAT Agent Customers Sales Report";
        }

        public System.Data.DataSet GetPageDataSet(int index)
        {
            return m_data;
        }

        public PageSideType GetPageSideType(int index)
        {
            if (index == 0)
                return PageSideType.Front;
            else if (index == 1)
                return PageSideType.Back;
            else
                return PageSideType.None;
        }

        public DevExpress.XtraReports.UI.XtraReport GetPageReport(int index)
        {
            DevExpress.XtraReports.UI.XtraReport ret;
            if (index == 0)
            {
                ret = DeclarationDesignFactory.createDesignObject("BIZNET.iERP.Client.DeclarationDesign.VATFrongPage");
                object picNeal = ret.GetType().GetField("picNeal").GetValue(ret);
                object picNotNeal = ret.GetType().GetField("picNotNeal").GetValue(ret);
                if (m_data.Summary[0].IsNealDeclaration)
                {
                    picNeal.GetType().GetProperty("Visible").SetValue(picNeal, true, new object[0]);
                    picNotNeal.GetType().GetProperty("Visible").SetValue(picNotNeal, false, new object[0]);
                    //((BIZNET.iERP.Client.DeclarationDesign.VATFrongPage)ret).picNeal.Visible = true;
                    //((BIZNET.iERP.Client.DeclarationDesign.VATFrongPage)ret).picNotNeal.Visible = false;
                }
                else
                {
                    picNeal.GetType().GetProperty("Visible").SetValue(picNeal, false, new object[0]);
                    picNotNeal.GetType().GetProperty("Visible").SetValue(picNotNeal, true, new object[0]);
                    //((BIZNET.iERP.Client.DeclarationDesign.VATFrongPage)ret).picNeal.Visible = false;
                    //((BIZNET.iERP.Client.DeclarationDesign.VATFrongPage)ret).picNotNeal.Visible = true;
                }
            }
            else if (index == 1)
                ret = DeclarationDesignFactory.createDesignObject("BIZNET.iERP.Client.DeclarationDesign.VATBackPage");
            else if (index == 2)
                ret = DeclarationDesignFactory.createDesignObject("BIZNET.iERP.Client.DeclarationDesign.LocalPurchaseGoodReport");
            else if (index == 3)
                ret = DeclarationDesignFactory.createDesignObject("BIZNET.iERP.Client.DeclarationDesign.LocalPurchaseServiceReport");
            else if (index == 4)
                ret = DeclarationDesignFactory.createDesignObject("BIZNET.iERP.Client.DeclarationDesign.NoTINLocalPurchaseGoodReport");
            else if (index == 5)
                ret = DeclarationDesignFactory.createDesignObject("BIZNET.iERP.Client.DeclarationDesign.NoTINLocalPurchaseServiceReport");
            else
                ret = DeclarationDesignFactory.createDesignObject("BIZNET.iERP.Client.DeclarationDesign.VATAgentCustomersSalesReport");
            ret.DataSource = this.GetPageDataSet(index);
            return ret;
        }

        #endregion
    }
    
    
}
