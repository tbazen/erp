using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Data;

namespace BIZNET.iERP.Client
{
    public class WithholdingTaxDeclarationSummarySource:IDeclarationSource
    {
        TypedDataSets.WithholdingTaxDeclarationData[] m_data;
        AccountingPeriod m_period;
        public WithholdingTaxDeclarationSummarySource(int periodID,TypedDataSets.WithholdingTaxDeclarationData data)
        {
            int extra=data.Continuation.Count-6;
            m_data = new BIZNET.iERP.TypedDataSets.WithholdingTaxDeclarationData[
                1 + (extra / 16) + (extra % 16 > 0 ? 1 : 0)];
            m_data[0] = new BIZNET.iERP.TypedDataSets.WithholdingTaxDeclarationData();
            m_data[0].Summary.Rows.Add(data.Summary.Rows[0].ItemArray);
            
            int k=0;
            int NItems=data.Continuation.Count;
            int periodIDIndex = m_data[0].Continuation.PeriodIDColumn.Ordinal;
            for (int j = 0; j < 6; j++)
            {
                if (k < NItems)
                {
                    TypedDataSets.WithholdingTaxDeclarationData.ContinuationRow row = (TypedDataSets.WithholdingTaxDeclarationData.ContinuationRow)m_data[0].Continuation.Rows.Add(data.Continuation.Rows[k].ItemArray);
                    FixBirrCents(row);
                }
                else
                {
                    object[] vals = new object[data.Continuation.Columns.Count];
                    vals[periodIDIndex] = periodID;
                    m_data[0].Continuation.Rows.Add(vals);
                }
                k++;
            }
            for (int i = 1; i < m_data.Length; i++)
            {
                m_data[i] = new BIZNET.iERP.TypedDataSets.WithholdingTaxDeclarationData();
                m_data[i].Summary.Rows.Add(data.Summary.Rows[0].ItemArray);
                for (int j = 0; j < 16; j++)
                {
                    if (k < NItems)
                    {
                        TypedDataSets.WithholdingTaxDeclarationData.ContinuationRow row =(TypedDataSets.WithholdingTaxDeclarationData.ContinuationRow)m_data[i].Continuation.Rows.Add(data.Continuation.Rows[k].ItemArray);
                        FixBirrCents(row);
                    }
                    else
                    {
                        object[] vals = new object[data.Continuation.Columns.Count];
                        vals[periodIDIndex] = periodID;
                        m_data[i].Continuation.Rows.Add(vals);

                    }
                    k++;
                }
            }

            double contTotalTax=0;
            double contTotalTaxable = 0;
            string birr, cents;
            for (int i = m_data.Length - 1; i >= 0; i--)
            {
                if (!Account.AmountEqual(contTotalTax, 0))
                {
                    m_data[i].Summary[0].ContTax = contTotalTax;
                    GetBirrCent(contTotalTax,out birr,out cents);
                    m_data[i].Summary[0].ContTaxBirr=birr;
                    m_data[i].Summary[0].ContTaxCent=cents;
                }
                if (!Account.AmountEqual(contTotalTaxable, 0))
                {
                    m_data[i].Summary[0].ContTaxable = contTotalTaxable;
                    GetBirrCent(contTotalTaxable, out birr, out cents);
                    m_data[i].Summary[0].ContTaxableBirr = birr;
                    m_data[i].Summary[0].ContTaxableCent= cents;
                }
                foreach (TypedDataSets.WithholdingTaxDeclarationData.ContinuationRow row in m_data[i].Continuation)
                {
                    if (row.IsNull(m_data[i].Continuation.TaxColumn))
                        continue;
                    contTotalTax += row.Tax;
                    contTotalTaxable += row.Taxable;
                }
                if (!Account.AmountEqual(contTotalTaxable, 0))
                {
                    m_data[i].Summary[0].TotalTaxable = contTotalTaxable;
                    GetBirrCent(contTotalTaxable, out birr, out cents);
                    m_data[i].Summary[0].TotalTaxableBirr = birr;
                    m_data[i].Summary[0].TotalTaxableCent = cents;
                }
                if (!Account.AmountEqual(contTotalTax, 0))
                {
                    m_data[i].Summary[0].TotalTax = contTotalTax;
                    GetBirrCent(contTotalTax, out birr, out cents);
                    m_data[i].Summary[0].TotalTaxBirr = birr;
                    m_data[i].Summary[0].TotalTaxCent = cents;
                }
                
                m_data[i].Summary[0].N_Page = m_data.Length;
                m_data[i].Summary[0].Page = (i + 1).ToString();
            }
            m_period = iERPTransactionClient.GetAccountingPeriod((string)iERPTransactionClient.GetSystemParamter( "taxDeclarationPeriods"),periodID);

        }
        void GetBirrCent(double amount, out string birr, out string cents)
        {
            birr = Math.Abs(Math.Floor(amount)).ToString("#,#0");
            cents= ((Math.Abs(amount) -Math.Abs(Math.Floor(amount)))*100).ToString("00");
        }
        private static void FixBirrCents(TypedDataSets.WithholdingTaxDeclarationData.ContinuationRow row)
        {
            row.TaxableBirr =Math.Abs(Math.Floor(row.Taxable)).ToString("#,#0");
            row.TaxableCents = ((Math.Abs(row.Taxable) - Math.Abs(Math.Floor(row.Taxable))) * 100).ToString("00");
            row.TaxBirr =Math.Abs( Math.Floor(row.Tax)).ToString("#,#0");
            row.TaxCents = ((Math.Abs(row.Tax) - Math.Abs(Math.Floor(row.Tax))) * 100).ToString("00");
        }
        #region ISummarySource Members

        public string SummaryTitle
        {
            get 
            {
                return "Withholding tax declaration for " + m_period.name;
            }
        }

        public int PageCount
        {
            get { return m_data.Length; }
        }

        public string GetPageTitle(int index)
        {
            if (index == 0)
                return "Summary";
            return "Continuation " + index;
        }

        public System.Data.DataSet GetPageDataSet(int index)
        {
            return m_data[index];
        }

        public PageSideType GetPageSideType(int index)
        {
            return PageSideType.None;
        }

        public DevExpress.XtraReports.UI.XtraReport GetPageReport(int index)
        {
            DevExpress.XtraReports.UI.XtraReport ret;
            if(index==0)
                ret = DeclarationDesignFactory.createDesignObject("BIZNET.iERP.Client.DeclarationDesign.WithholdingTaxSummary");
            else
                ret = DeclarationDesignFactory.createDesignObject("BIZNET.iERP.Client.DeclarationDesign.WithholdingTaxContinuation");
            ret.DataSource = this.GetPageDataSet(index);
            return ret;
        }

        #endregion
    }
    
    
}
