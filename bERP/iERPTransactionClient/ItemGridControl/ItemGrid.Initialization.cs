﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Utils.Menu;
using DevExpress.XtraGrid;
using System.Data;
using DevExpress.Utils;
using DevExpress.Data;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    public partial class ItemGrid 
    {

        DXMenuItem[] menuItems;
        void InitializeMenuItems()
        {
            DXMenuItem itemDelete = new DXMenuItem("Remove", ItemDelete_Click);
            menuItems = new DXMenuItem[] { itemDelete };
        }

        private void ItemDelete_Click(object sender, System.EventArgs e)
        {
            foreach (int index in gridViewItems.GetSelectedRows())
            {
                int dIndex = gridViewItems.GetDataSourceRowIndex(index);
                if (dIndex == _itemsTable.Rows.Count - 1)
                    continue;
                RemoveAt(dIndex);
            }
        }

        bool newRowEnabled
        {
            get
            {
                return !_readOnly;
            }
        }
        public void initializeGrid()
        {
            this.gridViewItems = this.ViewCollection[0] as DevExpress.XtraGrid.Views.Grid.GridView;
            this.gridViewItems.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewItems_CellValueChanged);
            this.gridViewItems.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewItems_CellValueChanging);
            _itemsTable = new ItemGridControl.ItemGridData.ItemTableDataTable();
            InitializeMenuItems();
            this.gridViewItems.PopupMenuShowing+=gridViewItems_PopupMenuShowing;
            this.DataSource = _itemsTable;
            foreach(DevExpress.XtraGrid.Columns.GridColumn col in gridViewItems.Columns)
                col.SortOrder = ColumnSortOrder.None;

            //cost center
            gridViewItems.Columns[_itemsTable.costCenterColumn.Ordinal].Caption = "Accouting Center";
            gridViewItems.Columns[_itemsTable.costCenterColumn.Ordinal].Visible = _showCostCenterColumn;
            gridViewItems.Columns[_itemsTable.costCenterColumn.Ordinal].OptionsColumn.AllowEdit =!_readOnly;
            if (_groupByCostCenter)
                gridViewItems.Columns[_itemsTable.costCenterColumn.Ordinal].Group();
            //cost center ID
            gridViewItems.Columns[_itemsTable.costCenterIDColumn.Ordinal].Visible= false;

            //item code
            gridViewItems.Columns[_itemsTable.codeColumn.Ordinal].Caption = "Code";
            gridViewItems.Columns[_itemsTable.codeColumn.Ordinal].Visible = true;
            gridViewItems.Columns[_itemsTable.codeColumn.Ordinal].OptionsColumn.AllowEdit = !_readOnly;
            
            
            //item name
            gridViewItems.Columns[_itemsTable.nameColumn.Ordinal].Caption = "Description";
            gridViewItems.Columns[_itemsTable.nameColumn.Ordinal].Visible = true;
            gridViewItems.Columns[_itemsTable.nameColumn.Ordinal].OptionsColumn.AllowEdit = true;

            //Current Balance
            gridViewItems.Columns[_itemsTable.currentBalanceColumn.Ordinal].Caption = _currentBalanceCaption;
            gridViewItems.Columns[_itemsTable.currentBalanceColumn.Ordinal].Visible = _showCurrentBalanceColumn;
            gridViewItems.Columns[_itemsTable.currentBalanceColumn.Ordinal].OptionsColumn.AllowEdit = false;

            //quantity
            gridViewItems.Columns[_itemsTable.quantityColumn.Ordinal].Caption = _quantityCaption;
            gridViewItems.Columns[_itemsTable.quantityColumn.Ordinal].Visible = _showQuantity;
            gridViewItems.Columns[_itemsTable.quantityColumn.Ordinal].OptionsColumn.AllowEdit = !_readOnly;
            gridViewItems.Columns[_itemsTable.quantityColumn.Ordinal].DisplayFormat.FormatType = FormatType.Numeric;
            gridViewItems.Columns[_itemsTable.quantityColumn.Ordinal].DisplayFormat.FormatString = "N";


            //unit
            DevExpress.XtraEditors.Repository.RepositoryItemComboBox combo=new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            gridViewItems.GridControl.RepositoryItems.Add(combo);
            gridViewItems.Columns[_itemsTable.unitColumn.Ordinal].ColumnEdit = combo;
            gridViewItems.Columns[_itemsTable.unitColumn.Ordinal].Caption = _unitCaption;
            gridViewItems.Columns[_itemsTable.unitColumn.Ordinal].Visible = _showUnit;
            gridViewItems.Columns[_itemsTable.unitColumn.Ordinal].OptionsColumn.AllowEdit = true;
            new UnitControllerDXGridComboColumn(
                gridViewItems,
                gridViewItems.Columns[_itemsTable.unitColumn.Ordinal],
                combo,
                gridViewItems.Columns[_itemsTable.codeColumn.Ordinal],
                gridViewItems.Columns[_itemsTable.quantityColumn.Ordinal],
                gridViewItems.Columns[_itemsTable.unitPriceColumn.Ordinal]
                );

            //rate
            gridViewItems.Columns[_itemsTable.rateColumn.Ordinal].Caption = "Depreciation Rate";
            gridViewItems.Columns[_itemsTable.rateColumn.Ordinal].Visible = _showRate;
            gridViewItems.Columns[_itemsTable.rateColumn.Ordinal].OptionsColumn.AllowEdit = false;
            gridViewItems.Columns[_itemsTable.rateColumn.Ordinal].DisplayFormat.FormatType = FormatType.Numeric;
            gridViewItems.Columns[_itemsTable.rateColumn.Ordinal].DisplayFormat.FormatString = "N";
            //unit price
            gridViewItems.Columns[_itemsTable.unitPriceColumn.Ordinal].Caption = _unitPriceCaption;
            gridViewItems.Columns[_itemsTable.unitPriceColumn.Ordinal].Visible = _showUnitPrice;
            gridViewItems.Columns[_itemsTable.unitPriceColumn.Ordinal].OptionsColumn.AllowEdit = !_readOnly && _allowUnitPriceEdit;
            gridViewItems.Columns[_itemsTable.unitPriceColumn.Ordinal].DisplayFormat.FormatType = FormatType.Numeric;
            gridViewItems.Columns[_itemsTable.unitPriceColumn.Ordinal].DisplayFormat.FormatString = "N";


            //Price
            gridViewItems.Columns[_itemsTable.priceColumn.Ordinal].Caption = _priceCaption;
            gridViewItems.Columns[_itemsTable.priceColumn.Ordinal].Visible = _showPrice;
            gridViewItems.Columns[_itemsTable.priceColumn.Ordinal].OptionsColumn.AllowEdit = !_readOnly && _allowPriceEdit;
            gridViewItems.Columns[_itemsTable.priceColumn.Ordinal].DisplayFormat.FormatType = FormatType.Numeric;
            gridViewItems.Columns[_itemsTable.priceColumn.Ordinal].DisplayFormat.FormatString = "N";


            //Remitted
            gridViewItems.Columns[_itemsTable.remittedColumn.Ordinal].Caption = "Exempted?";
            gridViewItems.Columns[_itemsTable.remittedColumn.Ordinal].Visible = _showRemitedColumn;
            gridViewItems.Columns[_itemsTable.remittedColumn.Ordinal].OptionsColumn.AllowEdit = !_readOnly;

            //directExpense
            gridViewItems.Columns[_itemsTable.directExpenseColumn.Ordinal].Caption = _directExpenseCaption;
            gridViewItems.Columns[_itemsTable.directExpenseColumn.Ordinal].Visible = _showDirectExpenseColumn;
            gridViewItems.Columns[_itemsTable.directExpenseColumn.Ordinal].OptionsColumn.AllowEdit = !_readOnly;
            
            //pre paid
            gridViewItems.Columns[_itemsTable.prepaidColumn.Ordinal].Caption = _prepaidCaption;
            gridViewItems.Columns[_itemsTable.prepaidColumn.Ordinal].Visible = _showPrepaid;
            gridViewItems.Columns[_itemsTable.prepaidColumn.Ordinal].OptionsColumn.AllowEdit = !_readOnly;
            
            gridViewItems.OptionsView.ShowGroupPanel=false;
            
                
            _itemsTable.Rows.Add();
        }

    }
}
