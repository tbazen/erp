﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Utils.Menu;
using DevExpress.XtraGrid;
using System.Data;
using DevExpress.Utils;
using DevExpress.Data;
using DevExpress.XtraGrid.Views.Grid;
using INTAPS.Accounting;
using BIZNET.iERP.Client.Items;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using System.ComponentModel;
using BIZNET.iERP.Client.ItemGridControl;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraEditors;
namespace BIZNET.iERP.Client
{
    public interface IItemGridController
    {
        double getUnitPrice(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item);
        double getBalance(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item);
        bool hasUnitPrice(TransactionItems item);
        bool queryCellChange(ItemGridControl.ItemGridData.ItemTableRow row, DataColumn column, object oldValue);
        bool queryDeleteRow(ItemGridControl.ItemGridData.ItemTableRow row);
        void afterComit(ItemGridControl.ItemGridData.ItemTableRow row,TransactionItems item);
    }
    
    public delegate void ItemGridCellChangedHandler(ItemGrid source,DataColumn column,object oldValue);
    public partial class ItemGrid : GridControl,IEnumerable<ItemGridControl.ItemGridData.ItemTableRow>,IList<ItemGridControl.ItemGridData.ItemTableRow>
    {
        public event ItemGridCellChangedHandler CellChanged;
        
        //data
        ItemGridControl.ItemGridData.ItemTableDataTable _itemsTable;
        
        //view refernce, it is created by the designer
        DevExpress.XtraGrid.Views.Grid.GridView gridViewItems;
        
        IItemGridController _controller = null;
        //state varaiable
        CostCenter _lastCostCenter = null;
        int _editRow = -1;
        ItemGridControl.ItemGridData.ItemTableRow _originalRow = null;

        //configuration options
        bool _showRemitedColumn = false;
        bool _showDirectExpenseColumn = false;
        bool _showCostCenterColumn = true;
        bool _showCurrentBalanceColumn = true;
        bool _showPrepaid = true;
        bool _showUnitPrice = true;
        bool _showRate = false;
        bool _showPrice = false;
        bool _showQuantity = false;
        bool _showUnit = true;
        bool _readOnly = false;
        bool _groupByCostCenter = false;

        
        
        bool _allowPriceEdit = false;
        bool _allowUnitPriceEdit = false;
        bool _autoUnitPrice = true;
        bool _allowRepeatedItem = false;
        string _priceCaption = "Price";
        string _unitPriceCaption = "Unit Price";
        string _quantityCaption = "Quantity";
        string _unitCaption= "Unit";
        string _prepaidCaption = "Prepaid?";
        string _directExpenseCaption = "Direct Issue?";
        string _currentBalanceCaption = "Current Balance";

        public string CurrentBalanceCaption
        {
            get { return _currentBalanceCaption; }
            set { _currentBalanceCaption = value; }
        }
        public string DirectExpenseCaption
        {
            get { return _directExpenseCaption; }
            set { _directExpenseCaption = value; }
        }
        public bool ShowCostCenterColumn
        {
            get { return _showCostCenterColumn; }
            set { _showCostCenterColumn = value; }
        }
        public bool ShowDirectExpenseColumn
        {
            get { return _showDirectExpenseColumn; }
            set { _showDirectExpenseColumn = value; }
        }
        public bool ShowRemitedColumn
        {
            get { return _showRemitedColumn; }
            set { _showRemitedColumn = value; }
        }
        public bool ShowCurrentBalanceColumn
        {
            get { return _showCurrentBalanceColumn; }
            set { _showCurrentBalanceColumn = value; }
        }
        public bool ShowPrepaid
        {
            get { return _showPrepaid; }
            set { _showPrepaid = value; }
        }
        public bool ShowUnitPrice
        {
            get { return _showUnitPrice; }
            set { _showUnitPrice = value; }
        }
        public bool ShowQuantity
        {
            get { return _showQuantity; }
            set { _showQuantity = value; }
        }
        public bool ShowUnit
        {
            get { return _showUnit; }
            set { _showUnit= value; }
        }
        public bool ShowRate
        {
            get { return _showRate; }
            set { _showRate = value; }
        }
        
        public bool ShowPrice
        {
            get { return _showPrice; }
            set { _showPrice = value; }
        }

        public string PriceCaption
        {
            get { return _priceCaption; }
            set { _priceCaption = value; }
        }
        public string UnitPriceCaption
        {
            get { return _unitPriceCaption; }
            set { _unitPriceCaption = value; }
        }
        public string QuantityCaption
        {
            get { return _quantityCaption; }
            set { _quantityCaption = value; }
        }

        public string PrepaidCaption
        {
            get { return _prepaidCaption; }
            set { _prepaidCaption = value; }
        }

        public bool AllowPriceEdit
        {
            get { return _allowPriceEdit; }
            set { _allowPriceEdit = value; }
        }
        public bool AllowUnitPriceEdit
        {
            get { return _allowUnitPriceEdit; }
            set { _allowUnitPriceEdit = value; }
        }
        public bool AutoUnitPrice
        {
            get { return _autoUnitPrice; }
            set { _autoUnitPrice = value; }
        }
        public bool AllowRepeatedItem
        {
            get { return _allowRepeatedItem; }
            set { _allowRepeatedItem = value; }
        }
        public bool ReadOnly
        {
            get { return _readOnly; }
            set { _readOnly = value; }
        }
        public bool GroupByCostCenter
        {
            get { return _groupByCostCenter; }
            set { _groupByCostCenter = value; }
        }
        public ItemGrid()
        {
            _spopup.ItemSelected += new EventHandler(_spopup_ItemSelected);
            
        }

        
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public IItemGridController Controller
        {
            get { return _controller; }
            set { _controller = value; }
        }
        
        public ItemGridControl.ItemGridData.ItemTableDataTable data
        {
            get
            {
                return _itemsTable;
            }
        }
        
        bool isRowEmpty(ItemGridControl.ItemGridData.ItemTableRow row)
        {
            if (!_showCostCenterColumn)
                return string.IsNullOrEmpty(row.code);
            return string.IsNullOrEmpty(row.code) || string.IsNullOrEmpty(row.costCenter);
        }
        ItemSearchPopup _spopup = new ItemSearchPopup();
        bool _pupShown = false;
        void _spopup_ItemSelected(object sender, EventArgs e)
        {
            gridViewItems.HideEditor();
            ItemGridControl.ItemGridData.ItemTableRow row = gridViewItems.GetDataRow(_editRow) as ItemGridControl.ItemGridData.ItemTableRow;
            if (_spopup.searchType == PopupSearchType.costCenter)
            {
                row.costCenter = _spopup.selectedCostCenter.Code;
                //setCostCenter(row, (CostCenter)_spopup.selectedCostCenter);
                gridViewItems_CellValueChanged(null,
                    new DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs(_editRow,
                    gridViewItems.Columns[_itemsTable.costCenterColumn.Ordinal], row.costCenter));
            }
            else
            {
                row.code = _spopup.selectedItem.Code;
                if (row.costCenterID < 1)
                    if (_showCostCenterColumn)
                        setCostCenter(row, _lastCostCenter == null ? AccountingClient.GetAccount<CostCenter>(Globals.currentCostCenterID) : _lastCostCenter);
                gridViewItems_CellValueChanged(null,
                    new DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs(_editRow,
                        gridViewItems.Columns[_itemsTable.codeColumn.Ordinal], row.code));
            }

        }

        void gridViewItems_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (_editRow != e.RowHandle)
            {
                _editRow = e.RowHandle;
                _originalRow = _itemsTable.NewRow() as ItemGridControl.ItemGridData.ItemTableRow;
                _originalRow.ItemArray = gridViewItems.GetDataRow(e.RowHandle).ItemArray;
            }
            if (e.Column.AbsoluteIndex == _itemsTable.prepaidColumn.Ordinal
                || e.Column.AbsoluteIndex == _itemsTable.directExpenseColumn.Ordinal
                )
            {
                gridViewItems.GetDataRow(e.RowHandle)[e.Column.AbsoluteIndex] = e.Value;
                gridViewItems_CellValueChanged(sender, e);
                return;
            }


            if (e.Column.AbsoluteIndex == _itemsTable.codeColumn.Ordinal
                || e.Column.AbsoluteIndex == _itemsTable.nameColumn.Ordinal
                || e.Column.AbsoluteIndex == _itemsTable.costCenterColumn.Ordinal
                )
            {

                BaseEdit be = gridViewItems.ActiveEditor;
                if (!_pupShown)
                {
                    _spopup.Location = be.PointToScreen(new System.Drawing.Point(2, be.Height+5));
                    _spopup.Show();
                    _pupShown = true;
                    be.Focus();
                }
                if(e.Column.AbsoluteIndex == _itemsTable.codeColumn.Ordinal)
                    _spopup.setSearch(be.Text,PopupSearchType.itemCode);
                else if(e.Column.AbsoluteIndex == _itemsTable.nameColumn.Ordinal)
                    _spopup.setSearch(be.Text,PopupSearchType.itemName);
                else if(e.Column.AbsoluteIndex == _itemsTable.costCenterColumn.Ordinal)
                    _spopup.setSearch(be.Text, PopupSearchType.costCenter);
            }
        }
        

        bool queryController(ItemGridControl.ItemGridData.ItemTableRow row, DataColumn column)
        {
            if (_controller != null)
            {
                if (!_controller.queryCellChange(row, column, _originalRow[column.Ordinal]))
                {
                    row[column.Ordinal] = _originalRow[column.Ordinal];
                    return false;
                }
            }
            return true;
        }

        void gridViewItems_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (_pupShown)
            {
                _pupShown = false;
                _spopup.Hide();
            }

            _editRow = -1;
            ItemGridControl.ItemGridData.ItemTableRow row = gridViewItems.GetDataRow(e.RowHandle) as ItemGridControl.ItemGridData.ItemTableRow;
            DataColumn column=_itemsTable.Columns[e.Column.AbsoluteIndex];

            if (sender != null)
            {
                if (e.Column.AbsoluteIndex == _itemsTable.nameColumn.Ordinal
                    || e.Column.AbsoluteIndex == _itemsTable.codeColumn.Ordinal
                    )
                {
                    TransactionItems nameItem = _spopup.getTopItem();
                    if (nameItem != null)
                    {
                        row.code = nameItem.Code;
                        row.name = nameItem.Description;
                        gridViewItems_CellValueChanged(null,

                            new DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs(e.RowHandle
                                , gridViewItems.Columns[_itemsTable.codeColumn.Ordinal], row.code)
                            );
                        return;
                    }
                }
                else if (e.Column.AbsoluteIndex == _itemsTable.costCenterColumn.Ordinal)
                {
                    CostCenter nameItem = (CostCenter)_spopup.getTopAccountItem();
                    if (nameItem != null)
                    {
                        row.costCenter = nameItem.Code;
                    }
                }
                if (isRowEmpty(row))
                {
                    if (column != _itemsTable.codeColumn && column != _itemsTable.costCenterColumn)
                    {
                        row[column.Ordinal] = _originalRow[column.Ordinal];
                        return;
                    }
                }
            }
            if (!queryController(row, column))
                    return;

            TransactionItems item = iERPTransactionClient.GetTransactionItems(row.code);
            if (e.Column.AbsoluteIndex == _itemsTable.codeColumn.Ordinal)
            {

                try
                {
                    if (item == null)
                    {
                        MessageBox.ShowErrorMessage(row.code + " is not valid");
                        row.code = _originalRow.code;
                        return;
                    }
                    if (_allowRepeatedItem)
                    {
                        foreach (ItemGridControl.ItemGridData.ItemTableRow i in _itemsTable)
                        {
                            if (string.IsNullOrEmpty(i.code))
                                continue;
                            if(i.code.ToUpper().Equals(item.Code.ToUpper()))
                            {
                                MessageBox.ShowErrorMessage(row.code + " is already entered");
                                row.code = _originalRow.code;
                                return;
                            }
                        }
                    }
                    setRowItem(row, item);
                    gridViewItems.FocusedColumn = gridViewItems.Columns[_itemsTable.quantityColumn.Ordinal];
                }
                catch (Exception ex)
                {
                    MessageBox.ShowException(ex);
                    row.code = _originalRow.code;
                    return;
                }
            }
            else if (e.Column.AbsoluteIndex == _itemsTable.unitPriceColumn.Ordinal
              || e.Column.AbsoluteIndex == _itemsTable.quantityColumn.Ordinal
              )
            {
                setRowBalance(row);
                if (e.Column.AbsoluteIndex == _itemsTable.quantityColumn.Ordinal)
                    if (_allowUnitPriceEdit)
                    {
                        gridViewItems.FocusedColumn = gridViewItems.Columns[_itemsTable.unitPriceColumn.Ordinal];
                        gridViewItems.SelectCell(gridViewItems.GetRowHandle(gridViewItems.GetFocusedDataSourceRowIndex()), gridViewItems.FocusedColumn);
                    }
                    else
                    {
                        gridViewItems.MoveNext();
                        gridViewItems.FocusedColumn = gridViewItems.Columns[_itemsTable.codeColumn.Ordinal];
                    }
                else
                {
                    gridViewItems.MoveNext();
                    gridViewItems.FocusedColumn = gridViewItems.Columns[_itemsTable.codeColumn.Ordinal];
                }
            }
            else if (e.Column.AbsoluteIndex == _itemsTable.costCenterColumn.Ordinal)
            {
                try
                {
                    CostCenter cs = AccountingClient.GetAccount<CostCenter>(row.costCenter);
                    if (cs == null)
                    {
                        MessageBox.ShowErrorMessage(row.costCenter + " is not valid accounting center code");
                        row.costCenter = _originalRow.costCenter;
                        return;
                    }
                    setCostCenter(row, cs);
                    setBalance(row, item);
                    if(isRowEmpty(row))
                        gridViewItems.FocusedColumn = gridViewItems.Columns[_itemsTable.codeColumn.Ordinal];
                    else
                        gridViewItems.FocusedColumn = gridViewItems.Columns[_itemsTable.quantityColumn.Ordinal];
                }
                catch (Exception ex)
                {
                    MessageBox.ShowException(ex);
                    row.costCenter = _originalRow.costCenter;
                    return;
                }
            }
            if (_controller != null)
                _controller.afterComit(row, item);
            checkNewRow(row);
        }
        
        private void checkNewRow(ItemGridControl.ItemGridData.ItemTableRow row)
        {
            if (!newRowEnabled)
                return;
            int rowIndex = _itemsTable.Rows.IndexOf(row);
            if (rowIndex == _itemsTable.Count - 2)
            {
                if (isRowEmpty(row) && isRowEmpty(_itemsTable[rowIndex + 1]))
                {
                    _itemsTable.Rows.RemoveAt(rowIndex + 1);
                }
            }
            else if (rowIndex == _itemsTable.Count - 1)
            {
                if (!isRowEmpty(row))//it means this row is commited
                {
                    _itemsTable.Rows.Add();
                }
            }
        }
        
        private void setRowBalance(ItemGridControl.ItemGridData.ItemTableRow row)
        {
            row.price = row.quantity *row.unitPrice*row.rate;
        }
        
        protected override bool ProcessGridKeys(System.Windows.Forms.KeyEventArgs keys, bool onlyEvent)
        {
//            if (keys.KeyCode == Keys.Delete)
//            {
//                foreach (int index in gridViewItems.GetSelectedRows())
//                {
//                    int dIndex = gridViewItems.GetDataSourceRowIndex(index);
//                    if (dIndex == _itemsTable.Rows.Count - 1)
//                        continue;
//                    RemoveAt(dIndex);
//                }
//            }
              return base.ProcessGridKeys(keys, onlyEvent);
        }

        private void gridViewItems_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.HitInfo.InRow)
            {
                GridView view = sender as GridView;
                view.FocusedRowHandle = e.HitInfo.RowHandle;

                foreach (DXMenuItem item in menuItems)
                    e.Menu.Items.Add(item);
            }
        }
        void setRowItem(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            if (item == null)
            {
                row.code = null;
                row.name = null;
                row.quantity = 0;
                row.price = 0;
                row.currentBalance = 0;
                row.unitPrice = 0;
            }
            else
            {
                row.code = item.Code;
                row.name = item.Name;
                row.quantity = 0;
                row.price = 0;
                
                
                if (row.costCenterID < 2)
                {
                    if (_lastCostCenter == null)
                        _lastCostCenter = AccountingClient.GetAccount<CostCenter>(Globals.currentCostCenterID);
                    setCostCenter(row, _lastCostCenter);
                }
                setBalance(row, item);
            }
        }
        
        private void setBalance(ItemGridControl.ItemGridData.ItemTableRow row, TransactionItems item)
        {
            if (_controller != null)
            {
                if (item == null)
                {
                    row.currentBalance = 0;
                    row.unitPrice = 0;
                }
                else
                {
                    if (_showCurrentBalanceColumn)
                    {
                        row.currentBalance = _controller.getBalance(row, item);
                    }
                    if (_autoUnitPrice && (_controller != null && _controller.hasUnitPrice(item)))
                    {
                        row.unitPrice = _controller.getUnitPrice(row,item);
                    }
                }
            }
        }
        
        private void setCostCenter(ItemGridControl.ItemGridData.ItemTableRow row, CostCenter costCenter)
        {
            row.costCenter = costCenter.CodeName;
            row.costCenterID = costCenter.id;
            _lastCostCenter = costCenter;
        }
        
        public  ItemGridControl.ItemGridData.ItemTableRow toItemGridTableRow(TransactionDocumentItem doc)
        {
            ItemGridControl.ItemGridData.ItemTableRow ret = _itemsTable.NewItemTableRow();
            ret.code=doc.code;
            ret.costCenterID = doc.costCenterID;
            ret.costCenter=AccountingClient.GetAccount<CostCenter>(doc.costCenterID).CodeName;
            ret.unitPrice = doc.unitPrice;
            ret.quantity = doc.quantity;
            ret.price = doc.price;
            ret.name= doc.name;
            ret.remitted = doc.remitted;
            ret.directExpense = doc.directExpense;
            ret.prepaid = doc.prepaid;
            ret.rate = doc.rate;
            ret.unit=iERPTransactionClient.GetMeasureUnit(doc.unitID==-1?iERPTransactionClient.GetTransactionItems(doc.code).MeasureUnitID:doc.unitID);
            return ret;
        }
        

        public static TransactionDocumentItem toDocumentItem(ItemGridControl.ItemGridData.ItemTableRow row)
        {
            TransactionDocumentItem ret = new TransactionDocumentItem();
            ret.costCenterID = row.costCenterID;
            ret.code = row.code;
            ret.name = row.name;
            ret.quantity = row.quantity;
            ret.unitPrice = row.unitPrice;
            ret.rate = row.rate;
            ret.price = row.price;
            ret.directExpense = row.directExpense;
            ret.remitted = row.remitted;
            ret.prepaid = row.prepaid;
            ret.unitID = row.unit is MeasureUnit ? ((MeasureUnit)row.unit).ID : -1;
            return ret;
        }

        public void Add(TransactionDocumentItem item)
        {
            Add(toItemGridTableRow(item));
        }

        class ItemGridEnumartor : IEnumerator<ItemGridControl.ItemGridData.ItemTableRow>
        {
            IEnumerator<ItemGridControl.ItemGridData.ItemTableRow> _enum;
            ItemGrid _grid;
            public ItemGridEnumartor(ItemGrid grid)
            {
                _grid = grid;
                _enum = grid._itemsTable.GetEnumerator();
            }
            
            public ItemGridControl.ItemGridData.ItemTableRow Current
            {
                get {

                    return _enum.Current;
                }
            }

            public void Dispose()
            {
                _enum.Dispose();
            }

            object System.Collections.IEnumerator.Current
            {
                get { return this.Current; }
            }

            public bool MoveNext()
            {
                if (_grid.newRowEnabled)
                {
                    bool ret = _enum.MoveNext();
                    if (!ret)
                        throw new Exception("BUG: grid enumartor end not expected");
                    if (_grid.isNewRow(_enum.Current))
                        return false;
                    return true;
                }
                else
                    return _enum.MoveNext();
            }

            public void Reset()
            {
                _enum.Reset();
            }
        }
        
        public IEnumerator<ItemGridControl.ItemGridData.ItemTableRow> GetEnumerator()
        {
            return new ItemGridEnumartor(this);
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public int IndexOf(ItemGridControl.ItemGridData.ItemTableRow item)
        {
            return _itemsTable.Rows.IndexOf(item);
        }

        public void Insert(int index, ItemGridControl.ItemGridData.ItemTableRow item)
        {
            if (newRowEnabled && index == _itemsTable.Rows.Count - 1)
                Add(item);
            else
                _itemsTable.Rows.InsertAt(item, index);
        }

        public void RemoveAt(int index)
        {
            if (newRowEnabled && index == _itemsTable.Rows.Count - 1)
                throw new Exception("It is not allowed to remve the new row");
            ItemGridControl.ItemGridData.ItemTableRow row = _itemsTable[index];
            _itemsTable.Rows.RemoveAt(index);
            if (_controller != null)
            {
                if (!_controller.queryDeleteRow(row))
                {
                    _itemsTable.Rows.InsertAt(row, index);
                }
            }

        }

        public ItemGridControl.ItemGridData.ItemTableRow this[int index]
        {
            get
            {
                return _itemsTable[index];
            }
            set
            {
                
            }
        }

        public void Add(ItemGridControl.ItemGridData.ItemTableRow item)
        {
            TransactionItems titem = iERPTransactionClient.GetTransactionItems(item.code);
            setBalance(item, titem);
            if (newRowEnabled)
            {
                _itemsTable.Rows.InsertAt(item, _itemsTable.Rows.Count - 1);
            }
            else
            {
                _itemsTable.Rows.Add(item);
            }
        }

        public void Clear()
        {
            while (_itemsTable.Count > (newRowEnabled?1:0))
            {
                _itemsTable.Rows.RemoveAt(0);
            }
        }

        public bool Contains(ItemGridControl.ItemGridData.ItemTableRow item)
        {
            bool ret = _itemsTable.Rows.Contains(item);
            if (!ret)
                return false;
            return !isNewRow(item);
        }

        public void CopyTo(ItemGridControl.ItemGridData.ItemTableRow[] array, int arrayIndex)
        {
            throw new Exception("Copy to is not supported");
        }

        public int Count
        {
            get { return newRowEnabled? _itemsTable.Count-1 :_itemsTable.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(ItemGridControl.ItemGridData.ItemTableRow item)
        {
            
            if (isNewRow(item))
                throw new Exception("New row can't be removed");
            int index = _itemsTable.Rows.IndexOf(item);
            if (index == -1)
                return false;
            RemoveAt(index);
            return true;
        }

        internal bool isNewRow(ItemGridControl.ItemGridData.ItemTableRow itemTableRow)
        {
            return newRowEnabled &&  _itemsTable.Rows.IndexOf(itemTableRow) == _itemsTable.Rows.Count-1;
        }

        internal TransactionItems GetItem(ItemGridControl.ItemGridData.ItemTableRow row)
        {
            return iERPTransactionClient.GetTransactionItems(row.code);
        }

        public void updateBalances()
        {
            foreach (ItemGridControl.ItemGridData.ItemTableRow row in _itemsTable)
            {
                if (isNewRow(row))
                    continue;
                TransactionItems item=iERPTransactionClient.GetTransactionItems(row.code);
                if (item == null)
                    continue;
                if(_showUnitPrice)
                    row.unitPrice = _controller.getUnitPrice(row,item );
                if (_showCurrentBalanceColumn)
                    row.currentBalance = _controller.getBalance(row, item);
            }
        }
    }
}
