﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Client
{
    class ItemGridRow : IEquatable<ItemGridRow>
    {
        public TransactionItems item;
        public int costCenterID = CostCenter.ROOT_COST_CENTER;
        public double unitPrice;
        public double quantity;
        public double price;
        public string editedItemName;
        public bool remited;
        public bool directExpense;

        public bool Equals(ItemGridRow other)
        {
            if (other == null)
                return false;
            if (this.item == null && other.item == null)
                return true;
            if (this.item == null || other.item == null)
                return false;
            return item.Code.Equals(other.item.Code);
        }
        public ItemGridRow()
        {
        }
        public ItemGridRow(TransactionDocumentItem doc)
        {
            this.item = iERPTransactionClient.GetTransactionItems(doc.code);
            this.costCenterID = doc.costCenterID;
            this.unitPrice = doc.unitPrice;
            this.quantity = doc.quantity;
            this.price = doc.price;
            this.editedItemName = doc.name;
            this.remited = doc.remitted;
            this.directExpense = doc.directExpense;
        }

        internal TransactionDocumentItem getDocumentItem()
        {
            TransactionDocumentItem ret = new TransactionDocumentItem();
            if (this.item == null)
                throw new ServerUserMessage("Item not selected");
            ret.code = this.item.Code;
            ret.name = editedItemName;
            ret.quantity = this.quantity;
            ret.unitPrice = this.unitPrice;
            ret.price = this.price;
            ret.directExpense = this.directExpense;
            ret.remitted = this.remited;
            ret.costCenterID = this.costCenterID;
            return ret;
        }
    }
}
