﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Threading;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
   
    public enum PopupSearchType
    {
        itemCode,
        itemName,
        costCenter,
        account
    }
    [Flags]
    public enum TransactionItemType
    {
        All=Inventory|FixedAsset|SalesItems,
        Inventory=1,
        FixedAsset=2,
        SalesItems=4,
    }
    public partial class ItemSearchPopup : DevExpress.XtraEditors.XtraForm
    {
        public event EventHandler ItemSelected;
        public TransactionItems selectedItem = null;
        
        string _query;
        PopupSearchType _searchType;

        bool _upateSeach=false;
        Thread _searchThread = null;
        TransactionItems[] _itemSearchResult = null;
        ItemCategory[] _itemCats = null;

        AccountBase[] _costCenterSearchResult = null;
        AccountBase[] _parents = null;
        Exception _ex = null;

        TransactionItemType _includeItemType;

        
        public AccountBase selectedCostCenter;
        public ItemSearchPopup()
        {
            InitializeComponent();
            base.SetStyle(ControlStyles.Selectable, false);
            labelStatus.Text = "Ready";
        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams ret = base.CreateParams;
                ret.Style = (int)Flags.WindowStyles.WS_THICKFRAME |
                   (int)Flags.WindowStyles.WS_CHILD;
                ret.ExStyle |= (int)Flags.WindowStyles.WS_EX_NOACTIVATE |
                   (int)Flags.WindowStyles.WS_EX_TOOLWINDOW;
                ret.X = this.Location.X;
                ret.Y = this.Location.Y;

                return ret;
            }
        } 
        public TransactionItems getTopItem()
        {
            if (_itemSearchResult == null || _itemSearchResult.Length == 0)
                return null;
            return _itemSearchResult[0];
        }
        public void setSearchTransactionItemType(TransactionItemType type)
        {
            _includeItemType = type;
        }
        public AccountBase getTopAccountItem()
        {
            if (_costCenterSearchResult == null || _costCenterSearchResult.Length == 0)
                return null;
            return _costCenterSearchResult[0];
        }
        public void setSearch(string q, PopupSearchType byCode)
        {
            _query = q;
            _searchType=byCode;
            _upateSeach = true;
            buttonNewItem.Visible = _searchType == PopupSearchType.itemCode
                || _searchType == PopupSearchType.itemName;
            if (_searchThread == null || !_searchThread.IsAlive)
            {
                startSeach();
            }
        }
        public PopupSearchType searchType
        {
            get { return _searchType; }
        }

        private void startSeach()
        {
            _upateSeach = false;
            labelStatus.Text= "Searching..";
            
            _searchThread = new Thread(delegate()
                {

                    try
                    {
                        int N;
                        if (_searchType == PopupSearchType.costCenter || _searchType == PopupSearchType.account)
                        {
                            _costCenterSearchResult = _searchType == PopupSearchType.costCenter ? (AccountBase[])AccountingClient.SearchAccount<CostCenter>(_query, 0, 30, out N) : AccountingClient.SearchAccount<Account>(_query, 0, 30, out N);
                            _parents = _searchType == PopupSearchType.costCenter ? (AccountBase[])new CostCenter[_costCenterSearchResult.Length] : new Account[_costCenterSearchResult.Length];
                            for (int i = 0; i < _costCenterSearchResult.Length; i++)
                                _parents[i] = _costCenterSearchResult[i].PID == -1 ? null :
                                    (_searchType == PopupSearchType.costCenter ? (AccountBase)AccountingClient.GetAccount<CostCenter>(_costCenterSearchResult[i].PID) : AccountingClient.GetAccount<Account>(_costCenterSearchResult[i].PID));
                        }
                        else
                        {
                            string[] fields = new string[] { _searchType == PopupSearchType.itemCode ? "code" : "name" };
                            object[] vals = new string[] { _query };
                            List<TransactionItems> result = new List<TransactionItems>();

                            result.AddRange(iERPTransactionClient.SearchTransactionItems(0, 30, vals, fields, out N));
                            _itemSearchResult = result.ToArray();
                            _itemCats = new ItemCategory[_itemSearchResult.Length];
                            for (int i = 0; i < _itemSearchResult.Length; i++)
                                _itemCats[i] = iERPTransactionClient.GetItemCategory(_itemSearchResult[i].categoryID);
                        }
                        INTAPS.UI.UIFormApplicationBase.runInUIThread(new INTAPS.UI.HTML.ProcessParameterless(searchResultReady));
                        _ex = null;
                    }
                    catch (Exception ex)
                    {
                        _ex = ex;
                    }
                });
            _searchThread.Start();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void searchResultReady()
        {
            if (_ex != null)
            {
                MessageBox.ShowException(_ex);
            }
            else
            {
                listView.Items.Clear();
                if (_searchType == PopupSearchType.account)
                    listView.Columns[2].Text = "Parent Account";
                else if (_searchType == PopupSearchType.costCenter)
                    listView.Columns[2].Text = "Parent Cost Center";
                else
                    listView.Columns[2].Text = "Category";
                int i = 0;
                if (_searchType == PopupSearchType.costCenter || _searchType == PopupSearchType.account)
                {
                    foreach (AccountBase item in _costCenterSearchResult)
                    {
                        ListViewItem li = new ListViewItem(item.Code);
                        li.Tag = item;
                        li.SubItems.Add(item.Name);
                        li.SubItems.Add(_parents[i] == null ? "" : _parents[i].NameCode);
                        listView.Items.Add(li);
                        i++;
                    }
                }
                else
                {

                    foreach (TransactionItems item in _itemSearchResult)
                    {
                        ListViewItem li = new ListViewItem(item.Code);
                        li.Tag = item;
                        li.SubItems.Add(item.Name);
                        li.SubItems.Add(_itemCats[i] == null ? "" : _itemCats[i].description);
                        listView.Items.Add(li);
                        i++;
                    }
                }
            }
            if (_upateSeach)
                startSeach();
            else
                labelStatus.Text = "Ready";

        }
        bool _ignoreEvent = false;
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent)
                return;
            _ignoreEvent = true;
            try
            {
                if (listView.SelectedItems.Count > 0)
                {
                    ListViewItem selected = listView.SelectedItems[0];
                    if (_searchType == PopupSearchType.costCenter || _searchType == PopupSearchType.account)
                    {
                        selectedCostCenter = selected.Tag as AccountBase;
                        selectedItem = null;
                    }
                    else
                    {
                        selectedCostCenter = null;
                        selectedItem = selected.Tag as TransactionItems;
                    }
                    if (ItemSelected != null)
                        ItemSelected(this, null);
                }
            }
            finally
            {
                _ignoreEvent = false;
            }
        }
        RegisterItem itemEditor;
        private void buttonNewItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ItemCategoryPicker catPicker = new ItemCategoryPicker();
            if (catPicker.ShowDialog(this) != DialogResult.OK)
                return;
            itemEditor = new RegisterItem(catPicker.category.ID);
            if (itemEditor.ShowDialog(this) == DialogResult.OK)
            {
                selectedItem = iERPTransactionClient.GetTransactionItems(itemEditor.ItemCode);
                if (ItemSelected != null)
                {
                    ItemSelected(this, null);
                }
            }
        }

    }
    public static class Flags
    {
        [Flags]
        public enum WindowStyles : uint
        {
            WS_OVERLAPPED = 0x00000000,
            WS_POPUP = 0x80000000,
            WS_CHILD = 0x40000000,
            WS_MINIMIZE = 0x20000000,
            WS_VISIBLE = 0x10000000,
            WS_DISABLED = 0x08000000,
            WS_CLIPSIBLINGS = 0x04000000,
            WS_CLIPCHILDREN = 0x02000000,
            WS_MAXIMIZE = 0x01000000,
            WS_BORDER = 0x00800000,
            WS_DLGFRAME = 0x00400000,
            WS_VSCROLL = 0x00200000,
            WS_HSCROLL = 0x00100000,
            WS_SYSMENU = 0x00080000,
            WS_THICKFRAME = 0x00040000,
            WS_GROUP = 0x00020000,
            WS_TABSTOP = 0x00010000,

            WS_MINIMIZEBOX = 0x00020000,
            WS_MAXIMIZEBOX = 0x00010000,

            WS_CAPTION = WS_BORDER | WS_DLGFRAME,
            WS_TILED = WS_OVERLAPPED,
            WS_ICONIC = WS_MINIMIZE,
            WS_SIZEBOX = WS_THICKFRAME,
            WS_TILEDWINDOW = WS_OVERLAPPEDWINDOW,

            WS_OVERLAPPEDWINDOW = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,
            WS_POPUPWINDOW = WS_POPUP | WS_BORDER | WS_SYSMENU,
            WS_CHILDWINDOW = WS_CHILD,

            //Extended Window Styles

            WS_EX_DLGMODALFRAME = 0x00000001,
            WS_EX_NOPARENTNOTIFY = 0x00000004,
            WS_EX_TOPMOST = 0x00000008,
            WS_EX_ACCEPTFILES = 0x00000010,
            WS_EX_TRANSPARENT = 0x00000020,

            //#if(WINVER >= 0x0400)

            WS_EX_MDICHILD = 0x00000040,
            WS_EX_TOOLWINDOW = 0x00000080,
            WS_EX_WINDOWEDGE = 0x00000100,
            WS_EX_CLIENTEDGE = 0x00000200,
            WS_EX_CONTEXTHELP = 0x00000400,

            WS_EX_RIGHT = 0x00001000,
            WS_EX_LEFT = 0x00000000,
            WS_EX_RTLREADING = 0x00002000,
            WS_EX_LTRREADING = 0x00000000,
            WS_EX_LEFTSCROLLBAR = 0x00004000,
            WS_EX_RIGHTSCROLLBAR = 0x00000000,

            WS_EX_CONTROLPARENT = 0x00010000,
            WS_EX_STATICEDGE = 0x00020000,
            WS_EX_APPWINDOW = 0x00040000,

            WS_EX_OVERLAPPEDWINDOW = (WS_EX_WINDOWEDGE | WS_EX_CLIENTEDGE),
            WS_EX_PALETTEWINDOW = (WS_EX_WINDOWEDGE | WS_EX_TOOLWINDOW | WS_EX_TOPMOST),

            //#endif /* WINVER >= 0x0400 */

            //#if(WIN32WINNT >= 0x0500)

            WS_EX_LAYERED = 0x00080000,

            //#endif /* WIN32WINNT >= 0x0500 */

            //#if(WINVER >= 0x0500)

            WS_EX_NOINHERITLAYOUT = 0x00100000, // Disable inheritence of mirroring by children
            WS_EX_LAYOUTRTL = 0x00400000, // Right to left mirroring

            //#endif /* WINVER >= 0x0500 */

            //#if(WIN32WINNT >= 0x0500)

            WS_EX_COMPOSITED = 0x02000000,
            WS_EX_NOACTIVATE = 0x08000000

            //#endif /* WIN32WINNT >= 0x0500 */

        }
    }
}