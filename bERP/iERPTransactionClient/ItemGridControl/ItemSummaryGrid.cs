﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraGrid;
using DevExpress.Utils;
using System.Drawing;

namespace BIZNET.iERP.Client
{
    
    class ItemSummaryGrid:GridControl
    {
        ItemGridControl.ItemSummaryData.SummaryTableDataTable _summaryTable;
        DevExpress.XtraGrid.Views.Grid.GridView gridViewSummaries;
        public ItemSummaryGrid()
        {
        }
        public void initializeGrid()
        {
            _summaryTable = new ItemGridControl.ItemSummaryData.SummaryTableDataTable();
            this.gridViewSummaries = this.ViewCollection[0] as DevExpress.XtraGrid.Views.Grid.GridView;
            this.DataSource = _summaryTable;

            this.gridViewSummaries.Columns[_summaryTable.orderColumn.Ordinal].Visible = false;
            this.gridViewSummaries.Columns[_summaryTable.orderColumn.Ordinal].Caption = "";
            this.gridViewSummaries.Columns[_summaryTable.orderColumn.Ordinal].SortIndex=0;

            this.gridViewSummaries.Columns[_summaryTable.keyColumn.Ordinal].Visible = false;
            this.gridViewSummaries.Columns[_summaryTable.keyColumn.Ordinal].Caption = "";

            this.gridViewSummaries.Columns[_summaryTable.labelColumn.Ordinal].Visible = true;
            this.gridViewSummaries.Columns[_summaryTable.labelColumn.Ordinal].Caption = "Summary";
            this.gridViewSummaries.Columns[_summaryTable.labelColumn.Ordinal].OptionsColumn.AllowEdit = false;
            
            this.gridViewSummaries.Columns[_summaryTable.amountColumn.Ordinal].Visible = true;
            this.gridViewSummaries.Columns[_summaryTable.amountColumn.Ordinal].Caption = "Value";
            this.gridViewSummaries.Columns[_summaryTable.amountColumn.Ordinal].OptionsColumn.AllowEdit = false;
            this.gridViewSummaries.Columns[_summaryTable.amountColumn.Ordinal].DisplayFormat.FormatType = FormatType.Numeric;
            this.gridViewSummaries.Columns[_summaryTable.amountColumn.Ordinal].DisplayFormat.FormatString = "N";

            this.gridViewSummaries.OptionsView.ShowColumnHeaders = false;
            this.gridViewSummaries.OptionsView.ShowGroupPanel = false;

            StyleFormatCondition condition = new StyleFormatCondition();
            condition.Appearance.Options.UseForeColor = true;
            condition.Appearance.ForeColor = Color.FromArgb(0xFF, 0xFF, 0x00, 0x00);
            condition.Condition = FormatConditionEnum.Expression;
            condition.Expression = "[amount]<0";
            condition.ApplyToRow = true;
            this.gridViewSummaries.FormatConditions.Add(condition);
            
        }
        public void setSummary(string key, string label,double value, int order)
        {
            foreach (ItemGridControl.ItemSummaryData.SummaryTableRow row in _summaryTable)
            {
                if (row.key == key)
                {
                    row.amount = value;
                    row.order = order;
                    row.label = label;
                    return;
                }
            };
            _summaryTable.AddSummaryTableRow(key, label, value, order);
        }
        public ItemGridControl.ItemSummaryData.SummaryTableDataTable data
        {
            get
            {
                return _summaryTable;
            }
        }
    }
}
