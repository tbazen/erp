﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS;
using INTAPS.Accounting;
namespace BIZNET.iERP.Client
{
    public partial class ConversionFactorEditor : DevExpress.XtraEditors.XtraForm
    {
        DataTable _data;
        string itemCode;
        int categoryID;
        MeasureUnit _baseUnit=null;
        public ConversionFactorEditor(string itemCode,int categoryID)
        {
            InitializeComponent();
            this.itemCode = itemCode;
            this.categoryID = categoryID;
            initGrid();
            if (!string.IsNullOrEmpty(itemCode))
            {
                TransactionItems item = iERPTransactionClient.GetTransactionItems(itemCode);
                this.Text = "Conversion factors for item: {0}".format(item.NameCode);
                _baseUnit = iERPTransactionClient.GetMeasureUnit(item.MeasureUnitID);
            }
            else if (categoryID != -1)
            {
                this.Text = "Conversion factors for category: {0}".format(iERPTransactionClient.GetItemCategory(categoryID).NameCode);
            }
            else
                this.Text = "Global conversion factors";

            foreach(UnitCoversionFactor f in iERPTransactionClient.getConversionFactors(itemCode,categoryID))
            {
                _data.Rows.Add(iERPTransactionClient.GetMeasureUnit(f.baseUnitID), iERPTransactionClient.GetMeasureUnit(f.conversionUnitID), f.conversionFactor);
            }
        }
        UnitControllerDXGridComboColumn c1, c2;
        void initGrid()
        {
            _data = new DataTable("data");
            _data.Columns.Add("unit1",typeof(object));
            _data.Columns.Add("unit2", typeof(object));
            _data.Columns.Add("factor",typeof(double));
            
            c1=new UnitControllerDXGridComboColumn(gridView,colUnit1,comboUnit1,null,null,null);
            c2=new UnitControllerDXGridComboColumn(gridView, colUnit2, comboUnit2, null,null, null);
            gridControl.DataSource = _data;
            colUnit1.OptionsColumn.AllowEdit= _baseUnit == null;
            _data.RowChanged += _data_RowChanged;
            
        }
        bool _ignoreEvent = false;
        void _data_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            if (_ignoreEvent)
                return;
            if (_baseUnit != null)
            {
                _ignoreEvent = true;
                e.Row[0] = _baseUnit;
                _ignoreEvent = false;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAddRow_Click(object sender, EventArgs e)
        {
            _data.Rows.Add();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                List<UnitCoversionFactor> factors = new List<UnitCoversionFactor>();
                int index = 1;
                foreach(DataRow row in _data.Rows)
                {
                    MeasureUnit unit1 = row[0] as MeasureUnit;
                    MeasureUnit unit2 = row[1] as MeasureUnit;
                    if (unit1 == null || unit2 == null || !(row[2] is double))
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Row {0} is not complete".format(index));
                        return;
                    }
                    double factor=(double)row[2];
                    if(!AccountBase.AmountGreater(factor,0))
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Conversion factor of row {0} is not valid".format(index));
                        return;
                    }

                    if(unit1.ID==unit2.ID)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Row {0} is has same unit1 and init2".format(index));
                        return;
                    }
                    foreach(UnitCoversionFactor f in factors)
                    {
                        if(f.baseUnitID==unit1.ID && f.conversionUnitID==unit2.ID)
                        {
                            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Row {0} is duplicate".format(index));
                            return;
                        }
                    }
                    factors.Add(new UnitCoversionFactor(){
                        itemScope=itemCode,
                        categoryScope=categoryID,
                        baseUnitID=unit1.ID,
                        conversionUnitID=unit2.ID,
                        conversionFactor=factor,
                   });
                }
                iERPTransactionClient.setConvesionFactors(itemCode, categoryID, factors.ToArray());
                this.Close();
            }
            catch(Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }

        private void removeItem_Click(object sender, EventArgs e)
        {
            gridView.DeleteSelectedRows();
        }

        private void buttonNewUnit_Click(object sender, EventArgs e)
        {
            string unit;
            if (bERPInputBox.showInputBox("New Measuring Unit", "Type Name of Measuring Unit", ""
                , delegate(string u, out string err)
                {
                    MeasureUnit mu = iERPTransactionClient.GetMeasureUnit(u);
                    if (mu == null)
                    {
                        err = null;
                        return true;
                    }
                    err = "The unit is already defined";
                    return false;
                }
                , out unit))
            {
                try
                {
                    iERPTransactionClient.RegisterMeasureUnit(new MeasureUnit() { ID = -1, Name = unit });
                    c1.reloadUnits();
                    c2.reloadUnits();
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
                }
            }
        }
    }
}