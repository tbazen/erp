﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    public partial class CostCentersPicker : DevExpress.XtraEditors.XtraForm
    {
        private List<int> m_CostCenters;
        public CostCentersPicker()
        {
            InitializeComponent();
            costCenterTree1.LoadData(CostCenter.ROOT_COST_CENTER);
            costCenterTree1.ExpandAll();
            m_CostCenters = new List<int>();
            
        }
        public int[] CostCenters
        {
            get
            {
                return m_CostCenters.ToArray();
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddCostCenterNode(costCenterTree1.Nodes);
            this.DialogResult = DialogResult.OK;
        }

        private void AddCostCenterNode(TreeNodeCollection nodes)
        {
            foreach (TreeNode n in nodes)
            {
                if (n.Checked)
                {
                    m_CostCenters.Add(((CostCenter)n.Tag).id);
                }
                AddCostCenterNode(n.Nodes);
            }
        }
    }
}