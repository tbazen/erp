﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Nodes;
using System.Collections;
using System.Xml;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using DevExpress.XtraLayout.Utils;

namespace BIZNET.iERP.Client
{
    public partial class ItemCategoryEditor : DevExpress.XtraEditors.XtraForm
    {
        public delegate void NewCategoryAddedHandler(ItemCategory category);
        public delegate void CategoryUpdatedHandler(ItemCategory updatedCategory);

        public event NewCategoryAddedHandler NewCategoryAdded;
        public event CategoryUpdatedHandler CategoryUpdated;
        
        private string m_ParentCategoryName;

        private bool m_Editable = false;
        private int m_CategID = -1;
        private int m_ParentCategID = -1;
        public ItemCategoryEditor(string parentCategoryName,int parentCategID, bool editCategory)
        {
            InitializeComponent();
            chkExpenseItem.Checked = true;
            txtCategoryName.LostFocus += new EventHandler(Control_LostFocus);
            if (parentCategoryName == null)
            {
                layoutParentCategory.HideToCustomization();
            }
            txtCategoryName.Focus();
            m_ParentCategoryName = parentCategoryName;
            m_ParentCategID=parentCategID;
            txtParentCategoryName.Text = m_ParentCategoryName ?? "";
        }
        
        void Control_LostFocus(object sender, EventArgs e)
        {
            validationCategory.Validate((Control)sender);
        }
        public ItemCategoryEditor(ItemCategory itemCategory, string parentCategName,int parentCategID, bool editCategory)
            :
            this(parentCategName,parentCategID, editCategory)
        {
            m_Editable = editCategory;
            m_CategID = itemCategory.ID;
            m_ParentCategID = itemCategory.PID;
            txtParentCategoryName.Text = m_ParentCategoryName;
            txtCategoryName.Text = itemCategory.description;
            txtCode.Text = itemCategory.Code;
            TransactionItems[] items = iERPTransactionClient.GetItemsInCategory(m_CategID);
            if (items != null)
            {
                if (items.Length > 0)
                    txtCode.Enabled = false;
            }
            accExpenseAccount.SetByID(itemCategory.expenseAccountPID);
            accDirectCostAccount.SetByID(itemCategory.directCostAccountPID);
            accPrePaidExpAccountHolder.SetByID(itemCategory.prePaidExpenseAccountPID);
            accSalesAccount.SetByID(itemCategory.salesAccountPID);
            accFinishedWork.SetByID(itemCategory.finishedWorkAccountPID);
            accUnearnedAccountPlaceHolder.SetByID(itemCategory.unearnedRevenueAccountPID);
            accGeneralInvAccount.SetByID(itemCategory.generalInventoryAccountPID);
            accFinishedGoodInvAccount.SetByID(itemCategory.finishedGoodsAccountPID);
            accPendingOrderAccountPlaceHolder.SetByID(itemCategory.pendingOrderAccountPID);
            accPendingDeliveryPlaceHolder.SetByID(itemCategory.pendingDeliveryAcountPID);
            accAccumFixedAssetHolder.SetByID(itemCategory.accumulatedDepreciationAccountPID);
            accOrigFixedAssetHolder.SetByID(itemCategory.originalFixedAssetAccountPID);
            accDeprecFixedAssetHolder.SetByID(itemCategory.depreciationAccountPID);
            //accExpenseAccount.Enabled = !CategoryAccountHasTransaction(m_CategID, itemCategory.expenseAccountPID, ItemAccountType.ExpenseAccount);
            //accDirectCostAccount.Enabled = !CategoryAccountHasTransaction(m_CategID, itemCategory.directCostAccountPID, ItemAccountType.DirectCostAccount);
            //accPrePaidExpAccountHolder.Enabled = !CategoryAccountHasTransaction(m_CategID, itemCategory.prePaidExpenseAccountPID, ItemAccountType.PrePaidExpenseAccount);
            //accSalesAccount.Enabled = !CategoryAccountHasTransaction(m_CategID, itemCategory.salesAccountPID, ItemAccountType.SalesAccount);
            //accExpenseForFixedAssetHolder.Enabled = !CategoryAccountHasTransaction(m_CategID, itemCategory.expenseAccountPID, ItemAccountType.ExpenseAccount);
            //accFinishedWork.Enabled = !CategoryAccountHasTransaction(m_CategID, itemCategory.finishedWorkAccountPID, ItemAccountType.FinishedWorkAccount);
            //accUnearnedAccountPlaceHolder.Enabled = !CategoryAccountHasTransaction(m_CategID, itemCategory.unearnedRevenueAccountPID, ItemAccountType.UnearnedRevenueAccount);
            //accGeneralInvAccount.Enabled = !CategoryAccountHasTransaction(m_CategID, itemCategory.generalInventoryAccountPID, ItemAccountType.InventoryAccount);
            //accFinishedGoodInvAccount.Enabled = !CategoryAccountHasTransaction(m_CategID, itemCategory.finishedGoodsAccountPID, ItemAccountType.FinishedGoodsAccount);
            //accPendingOrderAccountPlaceHolder.Enabled = !CategoryAccountHasTransaction(m_CategID, itemCategory.pendingOrderAccountPID, ItemAccountType.PendingOrderAccount);
            //accPendingDeliveryPlaceHolder.Enabled = !CategoryAccountHasTransaction(m_CategID, itemCategory.pendingDeliveryAcountPID, ItemAccountType.PendingDeliveryAccount);
            //accOrigFixedAssetHolder.Enabled = !CategoryAccountHasTransaction(m_CategID, itemCategory.originalFixedAssetAccountPID, ItemAccountType.OriginalFixedAssetAccount);
            //accAccumFixedAssetHolder.Enabled = !CategoryAccountHasTransaction(m_CategID, itemCategory.accumulatedDepreciationAccountPID, ItemAccountType.AccumulatedFixedAssetAccount);
            //accDeprecFixedAssetHolder.Enabled = !CategoryAccountHasTransaction(m_CategID, itemCategory.depreciationAccountPID, ItemAccountType.DepreciationFixedAssetAccount);
            autoCheckItemType(itemCategory);
        }

        private void autoCheckItemType(ItemCategory itemCategory)
        {
            chkExpenseItem.Checked =
            itemCategory.expenseAccountPID > 0
                || itemCategory.directCostAccountPID > 0
                || itemCategory.prePaidExpenseAccountPID > 0;

            chkSalesItem.Checked =
                itemCategory.salesAccountPID > 0
                || itemCategory.unearnedRevenueAccountPID > 0
                || itemCategory.finishedWorkAccountPID > 0;

            chkFixedAsset.Checked =
                itemCategory.accumulatedDepreciationAccountPID > 0
                || itemCategory.originalFixedAssetAccountPID > 0
                || itemCategory.depreciationAccountPID > 0;
            chkInventory.Checked =
                (itemCategory.generalInventoryAccountPID > 0
                || itemCategory.finishedGoodsAccountPID > 0)
                 && !chkFixedAsset.Checked;

        }

        public void HideExpenseAccountPlaceHolder()
        {
            layoutExpenseAccountHolder.HideToCustomization();
            layoutDirectCostAccountHolder.HideToCustomization();
            layoutPrePaidExpenseAccount.HideToCustomization();
        }
        public void HideInventoryAccountPlaceHolder()
        {
            layoutGeneralInventory.HideToCustomization();
            layoutFinishedInventory.HideToCustomization();
            layoutPendingOrder.HideToCustomization();
            layoutPendingDelivery.HideToCustomization();
        }
        public void HideFixedAssetAccountPlaceHolder()
        {
            layoutAccumDepreciation.HideToCustomization();
            layoutOriginalFixedAsset.HideToCustomization();
            layoutDepreciation.HideToCustomization();
        }
        public void HideSalesAccountPlaceHolder()
        {
            layoutSalesAccountHolder.HideToCustomization();
            layoutUnearnedRevenueAccountPlaceHolder.HideToCustomization();
            layoutFinishedWork.HideToCustomization();
        }

        public void ShowExpenseAccountPlaceHolder()
        {
            if (layoutExpenseAccountHolder.IsHidden)
                layoutExpenseAccountHolder.RestoreFromCustomization(layoutCategoryCode, InsertType.Bottom);
            if (layoutDirectCostAccountHolder.IsHidden)
                layoutDirectCostAccountHolder.RestoreFromCustomization(layoutExpenseAccountHolder, InsertType.Bottom);
            if (layoutPrePaidExpenseAccount.IsHidden)
                layoutPrePaidExpenseAccount.RestoreFromCustomization(layoutDirectCostAccountHolder, InsertType.Bottom);
        }
        public void ShowInventoryAccountPlaceHolder()
        {
            if (layoutPendingDelivery.IsHidden)
                layoutPendingDelivery.RestoreFromCustomization(layoutConfirmPanel, InsertType.Top);
            if (layoutPendingOrder.IsHidden)
                layoutPendingOrder.RestoreFromCustomization(layoutPendingDelivery, InsertType.Top);
            if (layoutFinishedInventory.IsHidden)
                layoutFinishedInventory.RestoreFromCustomization(layoutPendingOrder, InsertType.Top);
            if (layoutGeneralInventory.IsHidden)
                layoutGeneralInventory.RestoreFromCustomization(layoutFinishedInventory, InsertType.Top);
        }
        public void ShowSalesAccountPlaceHolder()
        {
            if (layoutSalesAccountHolder.IsHidden)
                layoutSalesAccountHolder.RestoreFromCustomization(layoutCategoryCode, InsertType.Bottom);
            if (layoutUnearnedRevenueAccountPlaceHolder.IsHidden)
                layoutUnearnedRevenueAccountPlaceHolder.RestoreFromCustomization(layoutSalesAccountHolder, InsertType.Bottom);
            if (layoutFinishedWork.IsHidden)
                layoutFinishedWork.RestoreFromCustomization(layoutUnearnedRevenueAccountPlaceHolder, InsertType.Bottom);

        }
        public void ShowFixedAssetAccountPlaceHolder()
        {
            if (layoutAccumDepreciation.IsHidden)
                layoutAccumDepreciation.RestoreFromCustomization(layoutCategoryCode, InsertType.Bottom);
            if (layoutOriginalFixedAsset.IsHidden)
                layoutOriginalFixedAsset.RestoreFromCustomization(layoutAccumDepreciation, InsertType.Bottom);
            if (layoutDepreciation.IsHidden)
                layoutDepreciation.RestoreFromCustomization(layoutOriginalFixedAsset, InsertType.Bottom);
            if (layoutPendingOrder.IsHidden)
                layoutPendingOrder.RestoreFromCustomization(layoutOriginalFixedAsset, InsertType.Bottom);
            if (layoutPendingDelivery.IsHidden)
                layoutPendingDelivery.RestoreFromCustomization(layoutPendingOrder, InsertType.Bottom);
        }
        private void ClearData()
        {
            btnAdd.Text = "&Add";
        }
        
        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            if (validationCategory.Validate())
            {
                if (ValidateEmptyAccountPlaceHolders())
                {
                    if (m_ParentCategID != -1)
                    {
                        if (!ValidateParentAccountsOfAccountHolders())
                            return;
                    }
                    ItemCategory category = new ItemCategory();
                    category.ID = m_CategID;
                    category.PID = m_ParentCategID;
                    category.description = txtCategoryName.Text;
                    category.Code = txtCode.Text;
                    category.expenseAccountPID = accExpenseAccount.GetAccountID();
                    category.directCostAccountPID = accDirectCostAccount.GetAccountID();
                    category.prePaidExpenseAccountPID = accPrePaidExpAccountHolder.GetAccountID();
                    category.salesAccountPID = accSalesAccount.GetAccountID();
                    category.finishedWorkAccountPID = accFinishedWork.GetAccountID();
                    category.unearnedRevenueAccountPID = accUnearnedAccountPlaceHolder.GetAccountID();
                    category.generalInventoryAccountPID = accGeneralInvAccount.GetAccountID();
                    category.finishedGoodsAccountPID = accFinishedGoodInvAccount.GetAccountID();
                    category.pendingOrderAccountPID = accPendingOrderAccountPlaceHolder.GetAccountID();
                    category.pendingDeliveryAcountPID = accPendingDeliveryPlaceHolder.GetAccountID();
                    category.originalFixedAssetAccountPID = accOrigFixedAssetHolder.GetAccountID();
                    category.accumulatedDepreciationAccountPID = accAccumFixedAssetHolder.GetAccountID();
                    category.depreciationAccountPID = accDeprecFixedAssetHolder.GetAccountID();
                    if (m_Editable)
                    {
                        FireCategoryUpdated(category);
                        Close();
                    }
                    else
                    {
                        FireNewCategoryAdded(category);
                        //ClearControls();
                        validationCategory.RemoveControlError(txtCategoryName);
                        validationCategory.RemoveControlError(txtCode);
                    }
                }
            }
        }

        private bool ValidateParentAccountsOfAccountHolders()
        {
            ItemCategory category = iERPTransactionClient.GetItemCategory(m_ParentCategID);

            if (ValidateExpenseAcccountHolder(category)
                && ValidateDirectCostAcccountHolder(category)
                && ValidatePrePaidExpenseAcccountHolder(category)
                && ValidateSalesAcccountHolder(category)
                && ValidateFinishedWorkAcccountHolder(category)
                && ValidateUnearnedRevenueAcccountHolder(category)
                && ValidateGeneralInvAcccountHolder(category)
                && ValidateFinishedInvAcccountHolder(category)
                && ValidatePendingOrderAcccountHolder(category)
                && ValidatePendingDeliveryAcccountHolder(category)
                && ValidateOriginalFixedAssetAcccountHolder(category)
                && ValidateAccumFixedAssetAcccountHolder(category)
                && ValidateDepreciationAccountHolder(category))
                return true;
            return false;
        }

        private bool ValidateExpenseAcccountHolder(ItemCategory category)
        {
            if (!layoutExpenseAccountHolder.IsHidden && accExpenseAccount.account!=null)
            {
                Account expenseAccount = AccountingClient.GetAccount<Account>(accExpenseAccount.GetAccountID());
                if (expenseAccount.PID == category.expenseAccountPID || accExpenseAccount.GetAccountID() == category.expenseAccountPID)
                    return true;
                else
                {
                    MessageBox.ShowErrorMessage("The expense account you're choosing should either be the same expense account of the parent category or a child account of the parent category!");
                    return false;
                }
            }
            return true;
        }

        private bool ValidateDirectCostAcccountHolder(ItemCategory category)
        {
            if (!layoutDirectCostAccountHolder.IsHidden && accDirectCostAccount.account!=null)
            {
                Account directCostAccount = AccountingClient.GetAccount<Account>(accDirectCostAccount.GetAccountID());
                if (directCostAccount.PID == category.directCostAccountPID || accDirectCostAccount.GetAccountID() == category.directCostAccountPID)
                    return true;
                else
                {
                    MessageBox.ShowErrorMessage("The direct cost account you're choosing should either be the same cost account of the parent category or a child account of the parent category!");
                    return false;
                }
            }
            return true;
        }

        private bool ValidatePrePaidExpenseAcccountHolder(ItemCategory category)
        {
            if (!layoutPrePaidExpenseAccount.IsHidden && accPrePaidExpAccountHolder.account != null)
            {
                Account expenseAccount = AccountingClient.GetAccount<Account>(accPrePaidExpAccountHolder.GetAccountID());
                if (expenseAccount.PID == category.prePaidExpenseAccountPID || accPrePaidExpAccountHolder.GetAccountID() == category.prePaidExpenseAccountPID)
                    return true;
                else
                {
                    MessageBox.ShowErrorMessage("The pre paid expense account you're choosing should either be the same pre paid expense account of the parent category or a child account of the parent category!");
                    return false;
                }
            }
            return true;
        }


        private bool ValidateSalesAcccountHolder(ItemCategory category)
        {
            if (!layoutSalesAccountHolder.IsHidden && accSalesAccount.account!=null)
            {
                Account salesAccount = AccountingClient.GetAccount<Account>(accSalesAccount.GetAccountID());
                if (salesAccount.PID == category.salesAccountPID || accSalesAccount.GetAccountID() == category.salesAccountPID)
                    return true;
                else
                {
                    MessageBox.ShowErrorMessage("The sales account you're choosing should either be the same sales account of the parent category or a child account of the parent category!");
                    return false;
                }
            }
            return true;
        }

        private bool ValidateFinishedWorkAcccountHolder(ItemCategory category)
        {
            if (!layoutFinishedWork.IsHidden && accFinishedWork.account != null)
            {
                Account salesAccount = AccountingClient.GetAccount<Account>(accFinishedWork.GetAccountID());
                if (salesAccount.PID == category.finishedWorkAccountPID || accFinishedWork.GetAccountID() == category.finishedWorkAccountPID)
                    return true;
                else
                {
                    MessageBox.ShowErrorMessage("The finished work account you're choosing should either be the same finished work account of the parent category or a child account of the parent category!");
                    return false;
                }
            }
            return true;
        }

        private bool ValidateUnearnedRevenueAcccountHolder(ItemCategory category)
        {
            if (!layoutUnearnedRevenueAccountPlaceHolder.IsHidden && accUnearnedAccountPlaceHolder.account != null)
            {
                Account salesAccount = AccountingClient.GetAccount<Account>(accUnearnedAccountPlaceHolder.GetAccountID());
                if (salesAccount.PID == category.unearnedRevenueAccountPID || accUnearnedAccountPlaceHolder.GetAccountID() == category.unearnedRevenueAccountPID)
                    return true;
                else
                {
                    MessageBox.ShowErrorMessage("The unearned revenue account you're choosing should either be the same revenue account of the parent category or a child account of the parent category!");
                    return false;
                }
            }
            return true;
        }


        private bool ValidateGeneralInvAcccountHolder(ItemCategory category)
        {
            if (!layoutGeneralInventory.IsHidden && accGeneralInvAccount.account!=null)
            {
                Account invAccount = AccountingClient.GetAccount<Account>(accGeneralInvAccount.GetAccountID());
                if (invAccount.PID == category.generalInventoryAccountPID || accGeneralInvAccount.GetAccountID() == category.generalInventoryAccountPID)
                    return true;
                else
                {
                    MessageBox.ShowErrorMessage("The general inventory account you're choosing should either be the same general inventory account of the parent category or a child account of the parent category!");
                    return false;
                }
            }
            return true;
        }

        private bool ValidateFinishedInvAcccountHolder(ItemCategory category)
        {
            if (!layoutFinishedInventory.IsHidden && accFinishedGoodInvAccount.account!=null)
            {
                Account invAccount = AccountingClient.GetAccount<Account>(accFinishedGoodInvAccount.GetAccountID());
                if (invAccount.PID == category.finishedGoodsAccountPID || accFinishedGoodInvAccount.GetAccountID() == category.finishedGoodsAccountPID)
                    return true;
                else
                {
                    MessageBox.ShowErrorMessage("The finished good inventory account you're choosing should either be the same inventory account of the parent category or a child account of the parent category!");
                    return false;
                }
            }
            return true;
        }

        private bool ValidatePendingOrderAcccountHolder(ItemCategory category)
        {
            if (!layoutPendingOrder.IsHidden && accPendingOrderAccountPlaceHolder.account != null)
            {
                Account invAccount = AccountingClient.GetAccount<Account>(accPendingOrderAccountPlaceHolder.GetAccountID());
                if (invAccount.PID == category.pendingOrderAccountPID || accPendingOrderAccountPlaceHolder.GetAccountID() == category.pendingOrderAccountPID)
                    return true;
                else
                {
                    MessageBox.ShowErrorMessage("The pending order account you're choosing should either be the same pending order account of the parent category or a child account of the parent category!");
                    return false;
                }
            }
            return true;
        }

        private bool ValidatePendingDeliveryAcccountHolder(ItemCategory category)
        {
            if (!layoutPendingDelivery.IsHidden && accPendingDeliveryPlaceHolder.account != null)
            {
                Account invAccount = AccountingClient.GetAccount<Account>(accPendingDeliveryPlaceHolder.GetAccountID());
                if (invAccount.PID == category.pendingDeliveryAcountPID || accPendingDeliveryPlaceHolder.GetAccountID() == category.pendingDeliveryAcountPID)
                    return true;
                else
                {
                    MessageBox.ShowErrorMessage("The pending delivery account you're choosing should either be the same pending delivery account of the parent category or a child account of the parent category!");
                    return false;
                }
            }
            return true;
        }
        private bool ValidateOriginalFixedAssetAcccountHolder(ItemCategory category)
        {
            if (!layoutOriginalFixedAsset.IsHidden && accOrigFixedAssetHolder.account != null)
            {
                Account assetAccount = AccountingClient.GetAccount<Account>(accOrigFixedAssetHolder.GetAccountID());
                if (assetAccount.PID == category.originalFixedAssetAccountPID || accOrigFixedAssetHolder.GetAccountID() == category.originalFixedAssetAccountPID)
                    return true;
                else
                {
                    MessageBox.ShowErrorMessage("The original fixed asset account you're choosing should either be the same original fixed asset account of the parent category or a child account of the parent category!");
                    return false;
                }
            }
            return true;
        }

        private bool ValidateAccumFixedAssetAcccountHolder(ItemCategory category)
        {
            if (!layoutAccumDepreciation.IsHidden && accAccumFixedAssetHolder.account != null)
            {
                Account assetAccount = AccountingClient.GetAccount<Account>(accAccumFixedAssetHolder.GetAccountID());
                if (assetAccount.PID == category.accumulatedDepreciationAccountPID || accAccumFixedAssetHolder.GetAccountID() == category.accumulatedDepreciationAccountPID)
                    return true;
                else
                {
                    MessageBox.ShowErrorMessage("The accumulated fixed asset account you're choosing should either be the same accumulated fixed asset account of the parent category or a child account of the parent category!");
                    return false;
                }
            }
            return true;
        }

        private bool ValidateDepreciationAccountHolder(ItemCategory category)
        {
            if (!layoutDepreciation.IsHidden && accDeprecFixedAssetHolder.account != null)
            {
                Account depreciationAccount = AccountingClient.GetAccount<Account>(accDeprecFixedAssetHolder.GetAccountID());
                if (depreciationAccount.PID == category.depreciationAccountPID || accDeprecFixedAssetHolder.GetAccountID() == category.depreciationAccountPID)
                    return true;
                else
                {
                    MessageBox.ShowErrorMessage("The depreciation expense account you're choosing should either be the same depreciation account of the parent category or a child account of the parent category!");
                    return false;
                }
            }
            return true;
        }

        private bool ValidateEmptyAccountPlaceHolders()
        {
            if (!layoutExpenseAccountHolder.IsHidden && accExpenseAccount.account == null
                && (!layoutDirectCostAccountHolder.IsHidden && accDirectCostAccount.account == null))
            {
                MessageBox.ShowErrorMessage("Please choose expense account or direct cost account from the account picker and try again!");
                return false;
            }
            else if (!layoutPrePaidExpenseAccount.IsHidden && accPrePaidExpAccountHolder.account == null)
            {
                MessageBox.ShowErrorMessage("Please choose prepaid expense account from the account picker and try again!");
                return false;
            }
            else if (!layoutSalesAccountHolder.IsHidden && accSalesAccount.account == null)
            {
                MessageBox.ShowErrorMessage("Please choose sales account from the sales account picker and try again!");
                return false;
            }
            else if (!layoutUnearnedRevenueAccountPlaceHolder.IsHidden && accUnearnedAccountPlaceHolder.account == null)
            {
                MessageBox.ShowErrorMessage("Please choose unearned revenue account from the account picker and try again!");
                return false;
            }
            else if (!layoutOriginalFixedAsset.IsHidden && accOrigFixedAssetHolder.account == null
                || !layoutAccumDepreciation.IsHidden && accAccumFixedAssetHolder.account == null
                || !layoutDepreciation.IsHidden && accDeprecFixedAssetHolder.account == null)
            {
                MessageBox.ShowErrorMessage("Please choose original fixed asset account, accumulated depreciation account and depreciation expense account from account picker and try again!");
                return false;
            }
            return true;
        }

        public void ChangeButtonTextToUpdate()
        {
            btnAdd.Text = "&Update";
        }


        public void ClearControls()
        {
            txtCategoryName.Text = "";
            txtCode.Text="";
            txtCategoryName.Focus();
        }
        private void FireNewCategoryAdded(ItemCategory category)
        {
            if (NewCategoryAdded != null)
            {
                NewCategoryAdded(category);

            }
        }

        private void FireCategoryUpdated(ItemCategory category)
        {
            if (CategoryUpdated != null)
                CategoryUpdated(category);
        }



        private void navBarControl1_GroupCollapsed(object sender, DevExpress.XtraNavBar.NavBarGroupEventArgs e)
        {
            //layoutControlItem2.Size = new Size(layoutControlItem2.Size.Width, 34);
        }

        private void navBarControl1_GroupExpanded(object sender, DevExpress.XtraNavBar.NavBarGroupEventArgs e)
        {
           // layoutControlItem2.Size = new Size(layoutControlItem2.Size.Width, 69);
        }

        private void chkExpenseItem_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkInventory.Checked && !chkExpenseItem.Checked && !chkSalesItem.Checked
               && !chkFixedAsset.Checked)
            {
                chkExpenseItem.Checked = true;
            }
            if (chkExpenseItem.Checked)
            {
               // chkFixedAsset.Checked = false;
                ShowExpenseAccountPlaceHolder();
                if (!chkSalesItem.Checked)
                    HideSalesAccountPlaceHolder();
                if (!chkInventory.Checked && !chkFixedAsset.Checked)
                    HideInventoryAccountPlaceHolder();
                if (!chkFixedAsset.Checked)
                    HideFixedAssetAccountPlaceHolder();
            }
            else
                HideExpenseAccountPlaceHolder();
        }

        private void chkSalesItem_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkInventory.Checked && !chkExpenseItem.Checked && !chkSalesItem.Checked
               && !chkFixedAsset.Checked)
            {
                chkExpenseItem.Checked = true;
            }
            if (chkSalesItem.Checked)
            {
                ShowSalesAccountPlaceHolder();
                if (!chkExpenseItem.Checked)
                    HideExpenseAccountPlaceHolder();
                if (!chkFixedAsset.Checked)
                    HideFixedAssetAccountPlaceHolder();
                if (!chkInventory.Checked && !chkFixedAsset.Checked)
                    HideInventoryAccountPlaceHolder();
            }
            else
                HideSalesAccountPlaceHolder();
        }

        private void chkFixedAsset_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkInventory.Checked && !chkExpenseItem.Checked && !chkSalesItem.Checked
                 && !chkFixedAsset.Checked)
            {
                chkExpenseItem.Checked = true;
            }
            if (chkFixedAsset.Checked)
            {
               // chkExpenseItem.Checked = false;
                chkInventory.Checked = false;
                if (!chkExpenseItem.Checked)
                    HideExpenseAccountPlaceHolder();
                if (!chkSalesItem.Checked)
                    HideSalesAccountPlaceHolder();
                //if (!chkInventory.Checked)
                //    HideInventoryAccountPlaceHolder();
                ShowFixedAssetAccountPlaceHolder();
                ShowInventoryAccountPlaceHolder();
            }
            else
            {
                HideFixedAssetAccountPlaceHolder();
                HideInventoryAccountPlaceHolder();
            }
        }

        private void chkInventory_CheckedChanged(object sender, EventArgs e)
        {
            if (chkInventory.Checked && !chkSalesItem.Checked && !chkExpenseItem.Checked)
                chkExpenseItem.Checked = true;

            if (!chkInventory.Checked && !chkExpenseItem.Checked && !chkSalesItem.Checked
                && !chkFixedAsset.Checked)
            {
                chkExpenseItem.Checked = true;
            }
            if (chkInventory.Checked)
            {
                chkFixedAsset.Checked = false;
                ShowInventoryAccountPlaceHolder();
                if (!chkExpenseItem.Checked)
                    HideExpenseAccountPlaceHolder();
                if (!chkSalesItem.Checked)
                    HideSalesAccountPlaceHolder();
                if (!chkFixedAsset.Checked)
                    HideFixedAssetAccountPlaceHolder();
            }
            else
                HideInventoryAccountPlaceHolder();
        }

        private void layoutControl1_LayoutUpdate(object sender, EventArgs e)
        {
            try
            {
                if (layoutControl1.Root.MinSize != null)
                    this.Size = new Size(Size.Width, layoutControl1.Root.MinSize.Height + 50);
            }
            catch { }
        }
    }
}