﻿namespace BIZNET.iERP.Client
{
    partial class ItemCategoryEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemCategoryEditor));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.chkInventory = new DevExpress.XtraEditors.CheckEdit();
            this.chkFixedAsset = new DevExpress.XtraEditors.CheckEdit();
            this.accPendingDeliveryPlaceHolder = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.chkSalesItem = new DevExpress.XtraEditors.CheckEdit();
            this.accPendingOrderAccountPlaceHolder = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.chkExpenseItem = new DevExpress.XtraEditors.CheckEdit();
            this.accFinishedWork = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accUnearnedAccountPlaceHolder = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accPrePaidExpAccountHolder = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accDeprecFixedAssetHolder = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accAccumFixedAssetHolder = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accOrigFixedAssetHolder = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accFinishedGoodInvAccount = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accGeneralInvAccount = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accSalesAccount = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accDirectCostAccount = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accExpenseAccount = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.txtCode = new DevExpress.XtraEditors.TextEdit();
            this.txtParentCategoryName = new DevExpress.XtraEditors.TextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.txtCategoryName = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutConfirmPanel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutParentCategory = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCategoryCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutExpenseAccountHolder = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutDirectCostAccountHolder = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutSalesAccountHolder = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutGeneralInventory = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutFinishedInventory = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutOriginalFixedAsset = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAccumDepreciation = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutDepreciation = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPrePaidExpenseAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUnearnedRevenueAccountPlaceHolder = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutFinishedWork = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPendingOrder = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPendingDelivery = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.validationCategory = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkInventory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFixedAsset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpenseItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtParentCategoryName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategoryName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutConfirmPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutParentCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCategoryCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExpenseAccountHolder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDirectCostAccountHolder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSalesAccountHolder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGeneralInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFinishedInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOriginalFixedAsset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAccumDepreciation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDepreciation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPrePaidExpenseAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUnearnedRevenueAccountPlaceHolder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFinishedWork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPendingOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPendingDelivery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationCategory)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.chkInventory);
            this.layoutControl1.Controls.Add(this.chkFixedAsset);
            this.layoutControl1.Controls.Add(this.accPendingDeliveryPlaceHolder);
            this.layoutControl1.Controls.Add(this.chkSalesItem);
            this.layoutControl1.Controls.Add(this.accPendingOrderAccountPlaceHolder);
            this.layoutControl1.Controls.Add(this.chkExpenseItem);
            this.layoutControl1.Controls.Add(this.accFinishedWork);
            this.layoutControl1.Controls.Add(this.accUnearnedAccountPlaceHolder);
            this.layoutControl1.Controls.Add(this.accPrePaidExpAccountHolder);
            this.layoutControl1.Controls.Add(this.accDeprecFixedAssetHolder);
            this.layoutControl1.Controls.Add(this.accAccumFixedAssetHolder);
            this.layoutControl1.Controls.Add(this.accOrigFixedAssetHolder);
            this.layoutControl1.Controls.Add(this.accFinishedGoodInvAccount);
            this.layoutControl1.Controls.Add(this.accGeneralInvAccount);
            this.layoutControl1.Controls.Add(this.accSalesAccount);
            this.layoutControl1.Controls.Add(this.accDirectCostAccount);
            this.layoutControl1.Controls.Add(this.accExpenseAccount);
            this.layoutControl1.Controls.Add(this.txtCode);
            this.layoutControl1.Controls.Add(this.txtParentCategoryName);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.txtCategoryName);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(486, 174, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(418, 545);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            this.layoutControl1.LayoutUpdate += new System.EventHandler(this.layoutControl1_LayoutUpdate);
            // 
            // chkInventory
            // 
            this.chkInventory.Location = new System.Drawing.Point(317, 5);
            this.chkInventory.Name = "chkInventory";
            this.chkInventory.Properties.Caption = "Inventory Item";
            this.chkInventory.Size = new System.Drawing.Size(96, 19);
            this.chkInventory.StyleController = this.layoutControl1;
            this.chkInventory.TabIndex = 0;
            this.chkInventory.CheckedChanged += new System.EventHandler(this.chkInventory_CheckedChanged);
            // 
            // chkFixedAsset
            // 
            this.chkFixedAsset.Location = new System.Drawing.Point(198, 5);
            this.chkFixedAsset.Name = "chkFixedAsset";
            this.chkFixedAsset.Properties.Caption = "Fixed Asset Item";
            this.chkFixedAsset.Size = new System.Drawing.Size(115, 19);
            this.chkFixedAsset.StyleController = this.layoutControl1;
            this.chkFixedAsset.TabIndex = 0;
            this.chkFixedAsset.CheckedChanged += new System.EventHandler(this.chkFixedAsset_CheckedChanged);
            // 
            // accPendingDeliveryPlaceHolder
            // 
            this.accPendingDeliveryPlaceHolder.account = null;
            this.accPendingDeliveryPlaceHolder.AllowAdd = false;
            this.accPendingDeliveryPlaceHolder.Location = new System.Drawing.Point(181, 481);
            this.accPendingDeliveryPlaceHolder.Name = "accPendingDeliveryPlaceHolder";
            this.accPendingDeliveryPlaceHolder.OnlyLeafAccount = false;
            this.accPendingDeliveryPlaceHolder.Size = new System.Drawing.Size(229, 20);
            this.accPendingDeliveryPlaceHolder.TabIndex = 26;
            // 
            // chkSalesItem
            // 
            this.chkSalesItem.Location = new System.Drawing.Point(109, 5);
            this.chkSalesItem.Name = "chkSalesItem";
            this.chkSalesItem.Properties.Caption = "Sales Item";
            this.chkSalesItem.Size = new System.Drawing.Size(85, 19);
            this.chkSalesItem.StyleController = this.layoutControl1;
            this.chkSalesItem.TabIndex = 0;
            this.chkSalesItem.CheckedChanged += new System.EventHandler(this.chkSalesItem_CheckedChanged);
            // 
            // accPendingOrderAccountPlaceHolder
            // 
            this.accPendingOrderAccountPlaceHolder.account = null;
            this.accPendingOrderAccountPlaceHolder.AllowAdd = false;
            this.accPendingOrderAccountPlaceHolder.Location = new System.Drawing.Point(181, 451);
            this.accPendingOrderAccountPlaceHolder.Name = "accPendingOrderAccountPlaceHolder";
            this.accPendingOrderAccountPlaceHolder.OnlyLeafAccount = false;
            this.accPendingOrderAccountPlaceHolder.Size = new System.Drawing.Size(229, 20);
            this.accPendingOrderAccountPlaceHolder.TabIndex = 25;
            // 
            // chkExpenseItem
            // 
            this.chkExpenseItem.Location = new System.Drawing.Point(5, 5);
            this.chkExpenseItem.Name = "chkExpenseItem";
            this.chkExpenseItem.Properties.Caption = "Expense Item";
            this.chkExpenseItem.Size = new System.Drawing.Size(100, 19);
            this.chkExpenseItem.StyleController = this.layoutControl1;
            this.chkExpenseItem.TabIndex = 0;
            this.chkExpenseItem.CheckedChanged += new System.EventHandler(this.chkExpenseItem_CheckedChanged);
            // 
            // accFinishedWork
            // 
            this.accFinishedWork.account = null;
            this.accFinishedWork.AllowAdd = false;
            this.accFinishedWork.Location = new System.Drawing.Point(181, 241);
            this.accFinishedWork.Name = "accFinishedWork";
            this.accFinishedWork.OnlyLeafAccount = false;
            this.accFinishedWork.Size = new System.Drawing.Size(229, 20);
            this.accFinishedWork.TabIndex = 21;
            // 
            // accUnearnedAccountPlaceHolder
            // 
            this.accUnearnedAccountPlaceHolder.account = null;
            this.accUnearnedAccountPlaceHolder.AllowAdd = false;
            this.accUnearnedAccountPlaceHolder.Location = new System.Drawing.Point(181, 271);
            this.accUnearnedAccountPlaceHolder.Name = "accUnearnedAccountPlaceHolder";
            this.accUnearnedAccountPlaceHolder.OnlyLeafAccount = false;
            this.accUnearnedAccountPlaceHolder.Size = new System.Drawing.Size(229, 20);
            this.accUnearnedAccountPlaceHolder.TabIndex = 19;
            // 
            // accPrePaidExpAccountHolder
            // 
            this.accPrePaidExpAccountHolder.account = null;
            this.accPrePaidExpAccountHolder.AllowAdd = false;
            this.accPrePaidExpAccountHolder.Location = new System.Drawing.Point(181, 181);
            this.accPrePaidExpAccountHolder.Name = "accPrePaidExpAccountHolder";
            this.accPrePaidExpAccountHolder.OnlyLeafAccount = false;
            this.accPrePaidExpAccountHolder.Size = new System.Drawing.Size(229, 20);
            this.accPrePaidExpAccountHolder.TabIndex = 18;
            // 
            // accDeprecFixedAssetHolder
            // 
            this.accDeprecFixedAssetHolder.account = null;
            this.accDeprecFixedAssetHolder.AllowAdd = false;
            this.accDeprecFixedAssetHolder.Location = new System.Drawing.Point(181, 361);
            this.accDeprecFixedAssetHolder.Name = "accDeprecFixedAssetHolder";
            this.accDeprecFixedAssetHolder.OnlyLeafAccount = false;
            this.accDeprecFixedAssetHolder.Size = new System.Drawing.Size(229, 20);
            this.accDeprecFixedAssetHolder.TabIndex = 17;
            // 
            // accAccumFixedAssetHolder
            // 
            this.accAccumFixedAssetHolder.account = null;
            this.accAccumFixedAssetHolder.AllowAdd = false;
            this.accAccumFixedAssetHolder.Location = new System.Drawing.Point(181, 331);
            this.accAccumFixedAssetHolder.Name = "accAccumFixedAssetHolder";
            this.accAccumFixedAssetHolder.OnlyLeafAccount = false;
            this.accAccumFixedAssetHolder.Size = new System.Drawing.Size(229, 20);
            this.accAccumFixedAssetHolder.TabIndex = 16;
            // 
            // accOrigFixedAssetHolder
            // 
            this.accOrigFixedAssetHolder.account = null;
            this.accOrigFixedAssetHolder.AllowAdd = false;
            this.accOrigFixedAssetHolder.Location = new System.Drawing.Point(181, 301);
            this.accOrigFixedAssetHolder.Name = "accOrigFixedAssetHolder";
            this.accOrigFixedAssetHolder.OnlyLeafAccount = false;
            this.accOrigFixedAssetHolder.Size = new System.Drawing.Size(229, 20);
            this.accOrigFixedAssetHolder.TabIndex = 15;
            // 
            // accFinishedGoodInvAccount
            // 
            this.accFinishedGoodInvAccount.account = null;
            this.accFinishedGoodInvAccount.AllowAdd = false;
            this.accFinishedGoodInvAccount.Location = new System.Drawing.Point(181, 421);
            this.accFinishedGoodInvAccount.Name = "accFinishedGoodInvAccount";
            this.accFinishedGoodInvAccount.OnlyLeafAccount = false;
            this.accFinishedGoodInvAccount.Size = new System.Drawing.Size(229, 20);
            this.accFinishedGoodInvAccount.TabIndex = 14;
            // 
            // accGeneralInvAccount
            // 
            this.accGeneralInvAccount.account = null;
            this.accGeneralInvAccount.AllowAdd = false;
            this.accGeneralInvAccount.Location = new System.Drawing.Point(181, 391);
            this.accGeneralInvAccount.Name = "accGeneralInvAccount";
            this.accGeneralInvAccount.OnlyLeafAccount = false;
            this.accGeneralInvAccount.Size = new System.Drawing.Size(229, 20);
            this.accGeneralInvAccount.TabIndex = 13;
            // 
            // accSalesAccount
            // 
            this.accSalesAccount.account = null;
            this.accSalesAccount.AllowAdd = false;
            this.accSalesAccount.Location = new System.Drawing.Point(181, 211);
            this.accSalesAccount.Name = "accSalesAccount";
            this.accSalesAccount.OnlyLeafAccount = false;
            this.accSalesAccount.Size = new System.Drawing.Size(229, 20);
            this.accSalesAccount.TabIndex = 12;
            // 
            // accDirectCostAccount
            // 
            this.accDirectCostAccount.account = null;
            this.accDirectCostAccount.AllowAdd = false;
            this.accDirectCostAccount.Location = new System.Drawing.Point(181, 151);
            this.accDirectCostAccount.Name = "accDirectCostAccount";
            this.accDirectCostAccount.OnlyLeafAccount = false;
            this.accDirectCostAccount.Size = new System.Drawing.Size(229, 20);
            this.accDirectCostAccount.TabIndex = 11;
            // 
            // accExpenseAccount
            // 
            this.accExpenseAccount.account = null;
            this.accExpenseAccount.AllowAdd = false;
            this.accExpenseAccount.Location = new System.Drawing.Point(181, 121);
            this.accExpenseAccount.Name = "accExpenseAccount";
            this.accExpenseAccount.OnlyLeafAccount = false;
            this.accExpenseAccount.Size = new System.Drawing.Size(229, 20);
            this.accExpenseAccount.TabIndex = 10;
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(181, 91);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(229, 20);
            this.txtCode.StyleController = this.layoutControl1;
            this.txtCode.TabIndex = 9;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "Category code cannot be empty";
            this.validationCategory.SetValidationRule(this.txtCode, conditionValidationRule1);
            // 
            // txtParentCategoryName
            // 
            this.txtParentCategoryName.Location = new System.Drawing.Point(181, 31);
            this.txtParentCategoryName.Name = "txtParentCategoryName";
            this.txtParentCategoryName.Properties.ReadOnly = true;
            this.txtParentCategoryName.Size = new System.Drawing.Size(229, 20);
            this.txtParentCategoryName.StyleController = this.layoutControl1;
            this.txtParentCategoryName.TabIndex = 8;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnClose);
            this.panelControl1.Controls.Add(this.btnAdd);
            this.panelControl1.Location = new System.Drawing.Point(5, 508);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(408, 32);
            this.panelControl1.TabIndex = 7;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(330, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Location = new System.Drawing.Point(252, 4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "&Add";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtCategoryName
            // 
            this.txtCategoryName.Location = new System.Drawing.Point(181, 61);
            this.txtCategoryName.Name = "txtCategoryName";
            this.txtCategoryName.Properties.Mask.EditMask = "[0-9a-zA-Z].*";
            this.txtCategoryName.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtCategoryName.Size = new System.Drawing.Size(229, 20);
            this.txtCategoryName.StyleController = this.layoutControl1;
            this.txtCategoryName.TabIndex = 6;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "category name cannot be empty";
            this.validationCategory.SetValidationRule(this.txtCategoryName, conditionValidationRule2);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutConfirmPanel,
            this.layoutControlItem3,
            this.layoutParentCategory,
            this.layoutCategoryCode,
            this.layoutExpenseAccountHolder,
            this.layoutDirectCostAccountHolder,
            this.layoutSalesAccountHolder,
            this.layoutGeneralInventory,
            this.layoutFinishedInventory,
            this.layoutOriginalFixedAsset,
            this.layoutAccumDepreciation,
            this.layoutDepreciation,
            this.layoutPrePaidExpenseAccount,
            this.layoutUnearnedRevenueAccountPlaceHolder,
            this.layoutFinishedWork,
            this.layoutPendingOrder,
            this.layoutPendingDelivery,
            this.layoutControlItem1,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(418, 545);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutConfirmPanel
            // 
            this.layoutConfirmPanel.Control = this.panelControl1;
            this.layoutConfirmPanel.CustomizationFormText = "layoutControlItem4";
            this.layoutConfirmPanel.Location = new System.Drawing.Point(0, 503);
            this.layoutConfirmPanel.Name = "layoutConfirmPanel";
            this.layoutConfirmPanel.Size = new System.Drawing.Size(412, 36);
            this.layoutConfirmPanel.Text = "layoutConfirmPanel";
            this.layoutConfirmPanel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutConfirmPanel.TextToControlDistance = 0;
            this.layoutConfirmPanel.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtCategoryName;
            this.layoutControlItem3.CustomizationFormText = "Name";
            this.layoutControlItem3.FillControlToClientArea = false;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 53);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(412, 30);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "Category Name";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(170, 13);
            // 
            // layoutParentCategory
            // 
            this.layoutParentCategory.Control = this.txtParentCategoryName;
            this.layoutParentCategory.CustomizationFormText = "Parent Unit:";
            this.layoutParentCategory.FillControlToClientArea = false;
            this.layoutParentCategory.Location = new System.Drawing.Point(0, 23);
            this.layoutParentCategory.Name = "layoutParentCategory";
            this.layoutParentCategory.Size = new System.Drawing.Size(412, 30);
            this.layoutParentCategory.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutParentCategory.Text = "Parent Category Name:";
            this.layoutParentCategory.TextSize = new System.Drawing.Size(170, 13);
            // 
            // layoutCategoryCode
            // 
            this.layoutCategoryCode.Control = this.txtCode;
            this.layoutCategoryCode.CustomizationFormText = "Category Code:";
            this.layoutCategoryCode.Location = new System.Drawing.Point(0, 83);
            this.layoutCategoryCode.Name = "layoutCategoryCode";
            this.layoutCategoryCode.Size = new System.Drawing.Size(412, 30);
            this.layoutCategoryCode.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutCategoryCode.Text = "Category Code:";
            this.layoutCategoryCode.TextSize = new System.Drawing.Size(170, 13);
            // 
            // layoutExpenseAccountHolder
            // 
            this.layoutExpenseAccountHolder.Control = this.accExpenseAccount;
            this.layoutExpenseAccountHolder.CustomizationFormText = "Expense Account:";
            this.layoutExpenseAccountHolder.Location = new System.Drawing.Point(0, 113);
            this.layoutExpenseAccountHolder.Name = "layoutExpenseAccountHolder";
            this.layoutExpenseAccountHolder.Size = new System.Drawing.Size(412, 30);
            this.layoutExpenseAccountHolder.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutExpenseAccountHolder.Text = "Expense Account:";
            this.layoutExpenseAccountHolder.TextSize = new System.Drawing.Size(170, 13);
            // 
            // layoutDirectCostAccountHolder
            // 
            this.layoutDirectCostAccountHolder.Control = this.accDirectCostAccount;
            this.layoutDirectCostAccountHolder.CustomizationFormText = "Direct Cost Account:";
            this.layoutDirectCostAccountHolder.Location = new System.Drawing.Point(0, 143);
            this.layoutDirectCostAccountHolder.Name = "layoutDirectCostAccountHolder";
            this.layoutDirectCostAccountHolder.Size = new System.Drawing.Size(412, 30);
            this.layoutDirectCostAccountHolder.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutDirectCostAccountHolder.Text = "Direct Cost Account:";
            this.layoutDirectCostAccountHolder.TextSize = new System.Drawing.Size(170, 13);
            // 
            // layoutSalesAccountHolder
            // 
            this.layoutSalesAccountHolder.Control = this.accSalesAccount;
            this.layoutSalesAccountHolder.CustomizationFormText = "Sales Account:";
            this.layoutSalesAccountHolder.Location = new System.Drawing.Point(0, 203);
            this.layoutSalesAccountHolder.Name = "layoutSalesAccountHolder";
            this.layoutSalesAccountHolder.Size = new System.Drawing.Size(412, 30);
            this.layoutSalesAccountHolder.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutSalesAccountHolder.Text = "Sales Account:";
            this.layoutSalesAccountHolder.TextSize = new System.Drawing.Size(170, 13);
            // 
            // layoutGeneralInventory
            // 
            this.layoutGeneralInventory.Control = this.accGeneralInvAccount;
            this.layoutGeneralInventory.CustomizationFormText = "General Inventory Account:";
            this.layoutGeneralInventory.Location = new System.Drawing.Point(0, 383);
            this.layoutGeneralInventory.Name = "layoutGeneralInventory";
            this.layoutGeneralInventory.Size = new System.Drawing.Size(412, 30);
            this.layoutGeneralInventory.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutGeneralInventory.Text = "General Inventory Account:";
            this.layoutGeneralInventory.TextSize = new System.Drawing.Size(170, 13);
            // 
            // layoutFinishedInventory
            // 
            this.layoutFinishedInventory.Control = this.accFinishedGoodInvAccount;
            this.layoutFinishedInventory.CustomizationFormText = "Finished Good Inventory Account:";
            this.layoutFinishedInventory.Location = new System.Drawing.Point(0, 413);
            this.layoutFinishedInventory.Name = "layoutFinishedInventory";
            this.layoutFinishedInventory.Size = new System.Drawing.Size(412, 30);
            this.layoutFinishedInventory.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutFinishedInventory.Text = "Finished Good Inventory Account:";
            this.layoutFinishedInventory.TextSize = new System.Drawing.Size(170, 13);
            // 
            // layoutOriginalFixedAsset
            // 
            this.layoutOriginalFixedAsset.Control = this.accOrigFixedAssetHolder;
            this.layoutOriginalFixedAsset.CustomizationFormText = "Original Fixed Asset Account:";
            this.layoutOriginalFixedAsset.Location = new System.Drawing.Point(0, 293);
            this.layoutOriginalFixedAsset.Name = "layoutOriginalFixedAsset";
            this.layoutOriginalFixedAsset.Size = new System.Drawing.Size(412, 30);
            this.layoutOriginalFixedAsset.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutOriginalFixedAsset.Text = "Original Fixed Asset Account:";
            this.layoutOriginalFixedAsset.TextSize = new System.Drawing.Size(170, 13);
            // 
            // layoutAccumDepreciation
            // 
            this.layoutAccumDepreciation.Control = this.accAccumFixedAssetHolder;
            this.layoutAccumDepreciation.CustomizationFormText = "Accumulated Depreciation Account:";
            this.layoutAccumDepreciation.Location = new System.Drawing.Point(0, 323);
            this.layoutAccumDepreciation.Name = "layoutAccumDepreciation";
            this.layoutAccumDepreciation.Size = new System.Drawing.Size(412, 30);
            this.layoutAccumDepreciation.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutAccumDepreciation.Text = "Accumulated Depreciation Account:";
            this.layoutAccumDepreciation.TextSize = new System.Drawing.Size(170, 13);
            // 
            // layoutDepreciation
            // 
            this.layoutDepreciation.Control = this.accDeprecFixedAssetHolder;
            this.layoutDepreciation.CustomizationFormText = "Depreciation Expense Account:";
            this.layoutDepreciation.Location = new System.Drawing.Point(0, 353);
            this.layoutDepreciation.Name = "layoutDepreciation";
            this.layoutDepreciation.Size = new System.Drawing.Size(412, 30);
            this.layoutDepreciation.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutDepreciation.Text = "Depreciation Expense Account:";
            this.layoutDepreciation.TextSize = new System.Drawing.Size(170, 13);
            // 
            // layoutPrePaidExpenseAccount
            // 
            this.layoutPrePaidExpenseAccount.Control = this.accPrePaidExpAccountHolder;
            this.layoutPrePaidExpenseAccount.CustomizationFormText = "Pre Paid Expense Account:";
            this.layoutPrePaidExpenseAccount.Location = new System.Drawing.Point(0, 173);
            this.layoutPrePaidExpenseAccount.Name = "layoutPrePaidExpenseAccount";
            this.layoutPrePaidExpenseAccount.Size = new System.Drawing.Size(412, 30);
            this.layoutPrePaidExpenseAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutPrePaidExpenseAccount.Text = "Pre Paid Expense Account:";
            this.layoutPrePaidExpenseAccount.TextSize = new System.Drawing.Size(170, 13);
            // 
            // layoutUnearnedRevenueAccountPlaceHolder
            // 
            this.layoutUnearnedRevenueAccountPlaceHolder.Control = this.accUnearnedAccountPlaceHolder;
            this.layoutUnearnedRevenueAccountPlaceHolder.CustomizationFormText = "Unearned Revenue Account:";
            this.layoutUnearnedRevenueAccountPlaceHolder.Location = new System.Drawing.Point(0, 263);
            this.layoutUnearnedRevenueAccountPlaceHolder.Name = "layoutUnearnedRevenueAccountPlaceHolder";
            this.layoutUnearnedRevenueAccountPlaceHolder.Size = new System.Drawing.Size(412, 30);
            this.layoutUnearnedRevenueAccountPlaceHolder.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutUnearnedRevenueAccountPlaceHolder.Text = "Unearned Revenue Account:";
            this.layoutUnearnedRevenueAccountPlaceHolder.TextSize = new System.Drawing.Size(170, 13);
            // 
            // layoutFinishedWork
            // 
            this.layoutFinishedWork.Control = this.accFinishedWork;
            this.layoutFinishedWork.CustomizationFormText = "Finished Work Account:";
            this.layoutFinishedWork.Location = new System.Drawing.Point(0, 233);
            this.layoutFinishedWork.Name = "layoutFinishedWork";
            this.layoutFinishedWork.Size = new System.Drawing.Size(412, 30);
            this.layoutFinishedWork.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutFinishedWork.Text = "Finished Work Account:";
            this.layoutFinishedWork.TextSize = new System.Drawing.Size(170, 13);
            // 
            // layoutPendingOrder
            // 
            this.layoutPendingOrder.Control = this.accPendingOrderAccountPlaceHolder;
            this.layoutPendingOrder.CustomizationFormText = "Pending Order Account:";
            this.layoutPendingOrder.Location = new System.Drawing.Point(0, 443);
            this.layoutPendingOrder.Name = "layoutPendingOrder";
            this.layoutPendingOrder.Size = new System.Drawing.Size(412, 30);
            this.layoutPendingOrder.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutPendingOrder.Text = "Pending Order Account:";
            this.layoutPendingOrder.TextSize = new System.Drawing.Size(170, 13);
            // 
            // layoutPendingDelivery
            // 
            this.layoutPendingDelivery.Control = this.accPendingDeliveryPlaceHolder;
            this.layoutPendingDelivery.CustomizationFormText = "Pending Delivery Account:";
            this.layoutPendingDelivery.Location = new System.Drawing.Point(0, 473);
            this.layoutPendingDelivery.Name = "layoutPendingDelivery";
            this.layoutPendingDelivery.Size = new System.Drawing.Size(412, 30);
            this.layoutPendingDelivery.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutPendingDelivery.Text = "Pending Delivery Account:";
            this.layoutPendingDelivery.TextSize = new System.Drawing.Size(170, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.chkExpenseItem;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(104, 23);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(104, 23);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(104, 23);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.chkSalesItem;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(104, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(89, 23);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(89, 23);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(89, 23);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.chkFixedAsset;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(193, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(119, 23);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(119, 23);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(119, 23);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.chkInventory;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(312, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(100, 23);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(100, 23);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(100, 23);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Org.bmp");
            this.imageList1.Images.SetKeyName(1, "User.bmp");
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup2.Size = new System.Drawing.Size(419, 383);
            this.layoutControlGroup2.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Text = "All Chart of Accounts";
            // 
            // validationCategory
            // 
            this.validationCategory.ValidateHiddenControls = false;
            // 
            // ItemCategoryEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(418, 545);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ItemCategoryEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Item Category Editor";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkInventory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFixedAsset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpenseItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtParentCategoryName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCategoryName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutConfirmPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutParentCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCategoryCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExpenseAccountHolder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDirectCostAccountHolder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutSalesAccountHolder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGeneralInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFinishedInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOriginalFixedAsset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAccumDepreciation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDepreciation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPrePaidExpenseAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUnearnedRevenueAccountPlaceHolder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFinishedWork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPendingOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPendingDelivery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationCategory)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit txtCategoryName;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutConfirmPanel;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.TextEdit txtParentCategoryName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutParentCategory;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validationCategory;
        private DevExpress.XtraEditors.TextEdit txtCode;
        private DevExpress.XtraLayout.LayoutControlItem layoutCategoryCode;
        private INTAPS.Accounting.Client.AccountPlaceHolder accFinishedGoodInvAccount;
        private INTAPS.Accounting.Client.AccountPlaceHolder accGeneralInvAccount;
        private INTAPS.Accounting.Client.AccountPlaceHolder accSalesAccount;
        private INTAPS.Accounting.Client.AccountPlaceHolder accDirectCostAccount;
        private INTAPS.Accounting.Client.AccountPlaceHolder accExpenseAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutExpenseAccountHolder;
        private DevExpress.XtraLayout.LayoutControlItem layoutDirectCostAccountHolder;
        private DevExpress.XtraLayout.LayoutControlItem layoutSalesAccountHolder;
        private DevExpress.XtraLayout.LayoutControlItem layoutGeneralInventory;
        private DevExpress.XtraLayout.LayoutControlItem layoutFinishedInventory;
        private INTAPS.Accounting.Client.AccountPlaceHolder accDeprecFixedAssetHolder;
        private INTAPS.Accounting.Client.AccountPlaceHolder accAccumFixedAssetHolder;
        private INTAPS.Accounting.Client.AccountPlaceHolder accOrigFixedAssetHolder;
        private DevExpress.XtraLayout.LayoutControlItem layoutOriginalFixedAsset;
        private DevExpress.XtraLayout.LayoutControlItem layoutAccumDepreciation;
        private DevExpress.XtraLayout.LayoutControlItem layoutDepreciation;
        private INTAPS.Accounting.Client.AccountPlaceHolder accPrePaidExpAccountHolder;
        private DevExpress.XtraLayout.LayoutControlItem layoutPrePaidExpenseAccount;
        private INTAPS.Accounting.Client.AccountPlaceHolder accUnearnedAccountPlaceHolder;
        private DevExpress.XtraLayout.LayoutControlItem layoutUnearnedRevenueAccountPlaceHolder;
        private INTAPS.Accounting.Client.AccountPlaceHolder accFinishedWork;
        private DevExpress.XtraLayout.LayoutControlItem layoutFinishedWork;
        private INTAPS.Accounting.Client.AccountPlaceHolder accPendingDeliveryPlaceHolder;
        private INTAPS.Accounting.Client.AccountPlaceHolder accPendingOrderAccountPlaceHolder;
        private DevExpress.XtraLayout.LayoutControlItem layoutPendingOrder;
        private DevExpress.XtraLayout.LayoutControlItem layoutPendingDelivery;
        private DevExpress.XtraEditors.CheckEdit chkInventory;
        private DevExpress.XtraEditors.CheckEdit chkFixedAsset;
        private DevExpress.XtraEditors.CheckEdit chkSalesItem;
        private DevExpress.XtraEditors.CheckEdit chkExpenseItem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
    }
}