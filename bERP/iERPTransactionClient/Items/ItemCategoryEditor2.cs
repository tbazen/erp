﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Nodes;
using System.Collections;
using System.Xml;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using DevExpress.XtraLayout.Utils;

namespace BIZNET.iERP.Client
{
    public partial class ItemCategoryEditor2 : DevExpress.XtraEditors.XtraForm
    {
        public delegate void NewCategoryAddedHandler(ItemCategory category);
        public delegate void CategoryUpdatedHandler(ItemCategory updatedCategory);

        public event NewCategoryAddedHandler NewCategoryAdded;
        public event CategoryUpdatedHandler CategoryUpdated;
        
        private string m_ParentCategoryName;

        private bool m_Editable = false;
        private int m_CategID = -1;
        private int m_ParentCategID = -1;
        public ItemCategoryEditor2(string parentCategoryName,int parentCategID, bool editCategory)
        {
            InitializeComponent();
            buttonConversionFactors.Visible = false;
            chkExpenseItem.Checked = false;
            txtCategoryName.LostFocus += new EventHandler(Control_LostFocus);
            if (parentCategoryName == null)
            {
                layoutParentCategory.HideToCustomization();
            }
            txtCategoryName.Focus();
            m_ParentCategoryName = parentCategoryName;
            m_ParentCategID=parentCategID;
            txtParentCategoryName.Text = m_ParentCategoryName ?? "";
            loadAccountControls();
        }
        List<AccountPlaceHolder> accountControls = new List<AccountPlaceHolder>();
        Dictionary<AccountPlaceHolder, AIParentAccountFieldAttribute> accountAttributes = new Dictionary<AccountPlaceHolder, AIParentAccountFieldAttribute>();
        Dictionary<AccountPlaceHolder, DevExpress.XtraLayout.LayoutControlItem> accountLayoutControls = new Dictionary<AccountPlaceHolder, DevExpress.XtraLayout.LayoutControlItem>();
        void loadAccountControls()
        {
            SortedDictionary<AIParentAccountFieldAttribute, System.Reflection.FieldInfo> fields = new SortedDictionary<AIParentAccountFieldAttribute, System.Reflection.FieldInfo>();
            foreach (System.Reflection.FieldInfo fi in typeof(ItemCategory).GetFields())
            {
                object[] atr = fi.GetCustomAttributes(typeof(AIParentAccountFieldAttribute), true);
                if (atr == null || atr.Length == 0)
                    continue;
                AIParentAccountFieldAttribute patr = (AIParentAccountFieldAttribute)atr[0];
                
                fields.Add(patr,fi);
                
            }
            
            foreach (KeyValuePair<AIParentAccountFieldAttribute,System.Reflection.FieldInfo> a in fields)
            {
                AccountPlaceHolder ap=new AccountPlaceHolder();
                ap.Tag = a.Value;
                DevExpress.XtraLayout.LayoutControlItem item;
                accountLayoutControls.Add(ap,item=layoutControlGroup.AddItem(a.Key.name,ap ));
                if(!a.Key.isSummary)
                    item.AppearanceItemCaption.Font = new Font(item.AppearanceItemCaption.Font, FontStyle.Bold);
                accountControls.Add(ap);
                accountAttributes.Add(ap, a.Key);
            }
        }
        void Control_LostFocus(object sender, EventArgs e)
        {
            validationCategory.Validate((Control)sender);
        }
        public ItemCategoryEditor2(ItemCategory itemCategory, string parentCategName, int parentCategID, bool editCategory)
            :
            this(parentCategName,parentCategID, editCategory)
        {
            m_Editable = editCategory;
            m_CategID = itemCategory.ID;
            m_ParentCategID = itemCategory.PID;
            txtParentCategoryName.Text = m_ParentCategoryName;
            txtCategoryName.Text = itemCategory.description;
            txtCode.Text = itemCategory.Code;
            TransactionItems[] items = iERPTransactionClient.GetItemsInCategory(m_CategID);
            if (items != null)
            {
                if (items.Length > 0)
                    txtCode.Enabled = false;
            }
            foreach (AccountPlaceHolder ap in accountControls)
            {
                ap.SetByID( (int)((System.Reflection.FieldInfo)ap.Tag).GetValue(itemCategory));
            }
            autoCheckItemType(itemCategory);
            showHideAccountControl();
            buttonConversionFactors.Visible = true;
        }

        private void autoCheckItemType(ItemCategory itemCategory)
        {
            foreach (CheckEdit chk in new CheckEdit[] { chkExpenseItem, chkSalesItem, chkFixedAsset, chkInventory })
            {
                string cat = chk.Tag as string;
                bool found = false;
                foreach (AccountPlaceHolder ap in accountControls)
                {
                    int ac = (int)((System.Reflection.FieldInfo)ap.Tag).GetValue(itemCategory);
                    if (ac != -1)
                    {
                        if (INTAPS.ArrayExtension.contains(accountAttributes[ap].groups, cat))
                        {
                            found = true;
                            break;
                        }
                    }
                }
                if (found)
                    chk.Checked = true;
            }
        }


        private void ClearData()
        {
            btnAdd.Text = "&Add";
        }
        
        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            try
            {
                if (validationCategory.Validate())
                {
                    if (ValidateEmptyAccountPlaceHolders())
                    {
                        if (m_ParentCategID != -1)
                        {
                            if (!ValidateParentAccountsOfAccountHolders())
                                return;
                        }
                        ItemCategory category = new ItemCategory();
                        category.ID = m_CategID;
                        category.PID = m_ParentCategID;
                        category.description = txtCategoryName.Text;
                        category.Code = txtCode.Text;
                        foreach (AccountPlaceHolder ap in accountControls)
                            ((System.Reflection.FieldInfo)ap.Tag).SetValue(category, ap.GetAccountID());
                        if (m_Editable)
                        {
                            FireCategoryUpdated(category);
                            Close();
                        }
                        else
                        {
                            FireNewCategoryAdded(category);
                            Close();
                            //ClearControls();
                            validationCategory.RemoveControlError(txtCategoryName);
                            validationCategory.RemoveControlError(txtCode);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private bool ValidateParentAccountsOfAccountHolders()
        {
            ItemCategory category = iERPTransactionClient.GetItemCategory(m_ParentCategID);

            return true;
        }

        private bool ValidateEmptyAccountPlaceHolders()
        {
            /*if (!layoutExpenseAccountHolder.IsHidden && accExpenseAccount.account == null
                && (!layoutDirectCostAccountHolder.IsHidden && accDirectCostAccount.account == null))
            {
                MessageBox.ShowErrorMessage("Please choose expense account or direct cost account from the account picker and try again!");
                return false;
            }
            else if (!layoutPrePaidExpenseAccount.IsHidden && accPrePaidExpAccountHolder.account == null)
            {
                MessageBox.ShowErrorMessage("Please choose prepaid expense account from the account picker and try again!");
                return false;
            }
            else if (!layoutSalesAccountHolder.IsHidden && accSalesAccount.account == null)
            {
                MessageBox.ShowErrorMessage("Please choose sales account from the sales account picker and try again!");
                return false;
            }
            else if (!layoutUnearnedRevenueAccountPlaceHolder.IsHidden && accUnearnedAccountPlaceHolder.account == null)
            {
                MessageBox.ShowErrorMessage("Please choose unearned revenue account from the account picker and try again!");
                return false;
            }
            else if (!layoutOriginalFixedAsset.IsHidden && accOrigFixedAssetHolder.account == null
                || !layoutAccumDepreciation.IsHidden && accAccumFixedAssetHolder.account == null
                || !layoutDepreciation.IsHidden && accDeprecFixedAssetHolder.account == null)
            {
                MessageBox.ShowErrorMessage("Please choose original fixed asset account, accumulated depreciation account and depreciation expense account from account picker and try again!");
                return false;
            }*/
            return true;
        }

        public void ChangeButtonTextToUpdate()
        {
            btnAdd.Text = "&Update";
        }


        public void ClearControls()
        {
            txtCategoryName.Text = "";
            txtCode.Text="";
            txtCategoryName.Focus();
        }
        private void FireNewCategoryAdded(ItemCategory category)
        {
            if (NewCategoryAdded != null)
            {
                NewCategoryAdded(category);

            }
        }

        private void FireCategoryUpdated(ItemCategory category)
        {
            if (CategoryUpdated != null)
                CategoryUpdated(category);
        }



        private void navBarControl1_GroupCollapsed(object sender, DevExpress.XtraNavBar.NavBarGroupEventArgs e)
        {
            //layoutControlItem2.Size = new Size(layoutControlItem2.Size.Width, 34);
        }

        private void navBarControl1_GroupExpanded(object sender, DevExpress.XtraNavBar.NavBarGroupEventArgs e)
        {
           // layoutControlItem2.Size = new Size(layoutControlItem2.Size.Width, 69);
        }
        void showHideAccountControl()
        {

            foreach (AccountPlaceHolder ap in accountControls)
            {
                string[] groups = accountAttributes[ap].groups;
                bool found = false;
                foreach (CheckEdit chk in new CheckEdit[] { chkExpenseItem, chkSalesItem, chkFixedAsset, chkInventory })
                {
                    if (chk.Checked)
                    {
                        string cat = (string)chk.Tag;
                        if (INTAPS.ArrayExtension.contains(groups, cat))
                        {
                            found = true;
                            break;
                        }
                    }
                }
                if (found)
                {
                    accountLayoutControls[ap].Visibility = LayoutVisibility.Always;
                }
                else
                    accountLayoutControls[ap].Visibility = LayoutVisibility.Never;

            }
        }
        private void chkExpenseItem_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkInventory.Checked && !chkExpenseItem.Checked && !chkSalesItem.Checked
               && !chkFixedAsset.Checked)
            {
                chkExpenseItem.Checked = true;
            }
            showHideAccountControl();
        }

        private void chkSalesItem_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkInventory.Checked && !chkExpenseItem.Checked && !chkSalesItem.Checked
               && !chkFixedAsset.Checked)
            {
                chkExpenseItem.Checked = true;
            }
            showHideAccountControl();
        }

        private void chkFixedAsset_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkInventory.Checked && !chkExpenseItem.Checked && !chkSalesItem.Checked
                 && !chkFixedAsset.Checked)
            {
                chkExpenseItem.Checked = true;
            }
            showHideAccountControl();
        }

        private void chkInventory_CheckedChanged(object sender, EventArgs e)
        {
            if (chkInventory.Checked && !chkSalesItem.Checked && !chkExpenseItem.Checked)
                chkExpenseItem.Checked = true;

            showHideAccountControl();
        }

        private void layoutControl1_LayoutUpdate(object sender, EventArgs e)
        {
            try
            {
                if (layoutControl1.Root.MinSize != null)
                    this.Size = new Size(Size.Width, layoutControl1.Root.MinSize.Height + 50);
            }
            catch { }
        }

        private void buttonConversionFactors_Click(object sender, EventArgs e)
        {
            ConversionFactorEditor editor = new ConversionFactorEditor("",this.m_CategID);
            editor.ShowDialog(this);
        }
    }
}