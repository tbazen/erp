﻿namespace BIZNET.iERP.Client
{
    partial class ItemCategoryEditor2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule3 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemCategoryEditor2));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.chkInventory = new DevExpress.XtraEditors.CheckEdit();
            this.chkFixedAsset = new DevExpress.XtraEditors.CheckEdit();
            this.chkSalesItem = new DevExpress.XtraEditors.CheckEdit();
            this.chkExpenseItem = new DevExpress.XtraEditors.CheckEdit();
            this.txtCode = new DevExpress.XtraEditors.TextEdit();
            this.txtParentCategoryName = new DevExpress.XtraEditors.TextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.txtCategoryName = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutConfirmPanel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutParentCategory = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCategoryCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.validationCategory = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.buttonConversionFactors = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkInventory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFixedAsset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpenseItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtParentCategoryName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategoryName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutConfirmPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutParentCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCategoryCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationCategory)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.chkInventory);
            this.layoutControl1.Controls.Add(this.chkFixedAsset);
            this.layoutControl1.Controls.Add(this.chkSalesItem);
            this.layoutControl1.Controls.Add(this.chkExpenseItem);
            this.layoutControl1.Controls.Add(this.txtCode);
            this.layoutControl1.Controls.Add(this.txtParentCategoryName);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.txtCategoryName);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(486, 174, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup;
            this.layoutControl1.Size = new System.Drawing.Size(753, 164);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            this.layoutControl1.LayoutUpdate += new System.EventHandler(this.layoutControl1_LayoutUpdate);
            // 
            // chkInventory
            // 
            this.chkInventory.Location = new System.Drawing.Point(317, 5);
            this.chkInventory.Name = "chkInventory";
            this.chkInventory.Properties.Caption = "Inventory Item";
            this.chkInventory.Size = new System.Drawing.Size(96, 19);
            this.chkInventory.StyleController = this.layoutControl1;
            this.chkInventory.TabIndex = 0;
            this.chkInventory.Tag = "INV";
            this.chkInventory.CheckedChanged += new System.EventHandler(this.chkInventory_CheckedChanged);
            // 
            // chkFixedAsset
            // 
            this.chkFixedAsset.Location = new System.Drawing.Point(198, 5);
            this.chkFixedAsset.Name = "chkFixedAsset";
            this.chkFixedAsset.Properties.Caption = "Fixed Asset Item";
            this.chkFixedAsset.Size = new System.Drawing.Size(115, 19);
            this.chkFixedAsset.StyleController = this.layoutControl1;
            this.chkFixedAsset.TabIndex = 0;
            this.chkFixedAsset.Tag = "FAS";
            this.chkFixedAsset.CheckedChanged += new System.EventHandler(this.chkFixedAsset_CheckedChanged);
            // 
            // chkSalesItem
            // 
            this.chkSalesItem.Location = new System.Drawing.Point(109, 5);
            this.chkSalesItem.Name = "chkSalesItem";
            this.chkSalesItem.Properties.Caption = "Sales Item";
            this.chkSalesItem.Size = new System.Drawing.Size(85, 19);
            this.chkSalesItem.StyleController = this.layoutControl1;
            this.chkSalesItem.TabIndex = 0;
            this.chkSalesItem.Tag = "SLS";
            this.chkSalesItem.CheckedChanged += new System.EventHandler(this.chkSalesItem_CheckedChanged);
            // 
            // chkExpenseItem
            // 
            this.chkExpenseItem.Location = new System.Drawing.Point(5, 5);
            this.chkExpenseItem.Name = "chkExpenseItem";
            this.chkExpenseItem.Properties.Caption = "Expense Item";
            this.chkExpenseItem.Size = new System.Drawing.Size(100, 19);
            this.chkExpenseItem.StyleController = this.layoutControl1;
            this.chkExpenseItem.TabIndex = 0;
            this.chkExpenseItem.Tag = "EXP";
            this.chkExpenseItem.CheckedChanged += new System.EventHandler(this.chkExpenseItem_CheckedChanged);
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(125, 91);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(620, 20);
            this.txtCode.StyleController = this.layoutControl1;
            this.txtCode.TabIndex = 9;
            conditionValidationRule3.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule3.ErrorText = "Category code cannot be empty";
            this.validationCategory.SetValidationRule(this.txtCode, conditionValidationRule3);
            // 
            // txtParentCategoryName
            // 
            this.txtParentCategoryName.Location = new System.Drawing.Point(125, 31);
            this.txtParentCategoryName.Name = "txtParentCategoryName";
            this.txtParentCategoryName.Properties.ReadOnly = true;
            this.txtParentCategoryName.Size = new System.Drawing.Size(620, 20);
            this.txtParentCategoryName.StyleController = this.layoutControl1;
            this.txtParentCategoryName.TabIndex = 8;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.buttonConversionFactors);
            this.panelControl1.Controls.Add(this.btnClose);
            this.panelControl1.Controls.Add(this.btnAdd);
            this.panelControl1.Location = new System.Drawing.Point(5, 118);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(743, 41);
            this.panelControl1.TabIndex = 7;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(665, 13);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Cancel";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Location = new System.Drawing.Point(587, 13);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Ok";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtCategoryName
            // 
            this.txtCategoryName.Location = new System.Drawing.Point(125, 61);
            this.txtCategoryName.Name = "txtCategoryName";
            this.txtCategoryName.Properties.Mask.EditMask = "[0-9a-zA-Z].*";
            this.txtCategoryName.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtCategoryName.Size = new System.Drawing.Size(620, 20);
            this.txtCategoryName.StyleController = this.layoutControl1;
            this.txtCategoryName.TabIndex = 6;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "category name cannot be empty";
            this.validationCategory.SetValidationRule(this.txtCategoryName, conditionValidationRule1);
            // 
            // layoutControlGroup
            // 
            this.layoutControlGroup.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup.GroupBordersVisible = false;
            this.layoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutConfirmPanel,
            this.layoutControlItem3,
            this.layoutParentCategory,
            this.layoutCategoryCode,
            this.layoutControlItem1,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6});
            this.layoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup.Name = "layoutControlGroup";
            this.layoutControlGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup.Size = new System.Drawing.Size(753, 164);
            this.layoutControlGroup.Text = "layoutControlGroup";
            this.layoutControlGroup.TextVisible = false;
            // 
            // layoutConfirmPanel
            // 
            this.layoutConfirmPanel.Control = this.panelControl1;
            this.layoutConfirmPanel.CustomizationFormText = "layoutControlItem4";
            this.layoutConfirmPanel.Location = new System.Drawing.Point(0, 113);
            this.layoutConfirmPanel.Name = "layoutConfirmPanel";
            this.layoutConfirmPanel.Size = new System.Drawing.Size(747, 45);
            this.layoutConfirmPanel.Text = "layoutConfirmPanel";
            this.layoutConfirmPanel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutConfirmPanel.TextToControlDistance = 0;
            this.layoutConfirmPanel.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtCategoryName;
            this.layoutControlItem3.CustomizationFormText = "Name";
            this.layoutControlItem3.FillControlToClientArea = false;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 53);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(747, 30);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "Category Name";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(114, 13);
            // 
            // layoutParentCategory
            // 
            this.layoutParentCategory.Control = this.txtParentCategoryName;
            this.layoutParentCategory.CustomizationFormText = "Parent Unit:";
            this.layoutParentCategory.FillControlToClientArea = false;
            this.layoutParentCategory.Location = new System.Drawing.Point(0, 23);
            this.layoutParentCategory.Name = "layoutParentCategory";
            this.layoutParentCategory.Size = new System.Drawing.Size(747, 30);
            this.layoutParentCategory.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutParentCategory.Text = "Parent Category Name:";
            this.layoutParentCategory.TextSize = new System.Drawing.Size(114, 13);
            // 
            // layoutCategoryCode
            // 
            this.layoutCategoryCode.Control = this.txtCode;
            this.layoutCategoryCode.CustomizationFormText = "Category Code:";
            this.layoutCategoryCode.Location = new System.Drawing.Point(0, 83);
            this.layoutCategoryCode.Name = "layoutCategoryCode";
            this.layoutCategoryCode.Size = new System.Drawing.Size(747, 30);
            this.layoutCategoryCode.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutCategoryCode.Text = "Category Code:";
            this.layoutCategoryCode.TextSize = new System.Drawing.Size(114, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.chkExpenseItem;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(104, 23);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(104, 23);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(104, 23);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.chkSalesItem;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(104, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(89, 23);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(89, 23);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(89, 23);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.chkFixedAsset;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(193, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(119, 23);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(119, 23);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(119, 23);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.chkInventory;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(312, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(100, 23);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(100, 23);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(435, 23);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Org.bmp");
            this.imageList1.Images.SetKeyName(1, "User.bmp");
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup2.Size = new System.Drawing.Size(419, 383);
            this.layoutControlGroup2.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Text = "All Chart of Accounts";
            // 
            // validationCategory
            // 
            this.validationCategory.ValidateHiddenControls = false;
            // 
            // buttonConversionFactors
            // 
            this.buttonConversionFactors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonConversionFactors.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonConversionFactors.Appearance.Options.UseFont = true;
            this.buttonConversionFactors.Location = new System.Drawing.Point(7, 10);
            this.buttonConversionFactors.Name = "buttonConversionFactors";
            this.buttonConversionFactors.Size = new System.Drawing.Size(129, 29);
            this.buttonConversionFactors.TabIndex = 4;
            this.buttonConversionFactors.Text = "&Conversion Factors";
            this.buttonConversionFactors.Click += new System.EventHandler(this.buttonConversionFactors_Click);
            // 
            // ItemCategoryEditor2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(753, 164);
            this.Controls.Add(this.layoutControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ItemCategoryEditor2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Item Category Editor";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkInventory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFixedAsset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpenseItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtParentCategoryName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCategoryName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutConfirmPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutParentCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCategoryCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationCategory)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup;
        private DevExpress.XtraEditors.TextEdit txtCategoryName;
        private DevExpress.XtraEditors.TextEdit txtParentCategoryName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutParentCategory;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validationCategory;
        private DevExpress.XtraEditors.TextEdit txtCode;
        private DevExpress.XtraLayout.LayoutControlItem layoutCategoryCode;
        private DevExpress.XtraEditors.CheckEdit chkInventory;
        private DevExpress.XtraEditors.CheckEdit chkFixedAsset;
        private DevExpress.XtraEditors.CheckEdit chkSalesItem;
        private DevExpress.XtraEditors.CheckEdit chkExpenseItem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraLayout.LayoutControlItem layoutConfirmPanel;
        private DevExpress.XtraEditors.SimpleButton buttonConversionFactors;
    }
}