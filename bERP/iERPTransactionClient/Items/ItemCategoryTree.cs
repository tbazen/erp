﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIZNET.iERP.Client
{
    class ItemCategoryTree : DevExpress.XtraTreeList.TreeList
    {
        private DevExpress.XtraTreeList.Columns.TreeListColumn colCode;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colName;

        public void initializeControl()
        {
            this.colCode = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colName = new DevExpress.XtraTreeList.Columns.TreeListColumn();

            // 
            // colCode
            // 
            this.colCode.Caption = "Code";
            this.colCode.FieldName = "Code";
            this.colCode.Name = "colCode";
            this.colCode.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            this.colCode.Visible = true;
            this.colCode.VisibleIndex = 0;
            this.colCode.Width = 103;
            // 
            // colName
            // 
            this.colName.Caption = "Name";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 1;
            this.colName.Width = 240;
            this.Columns.Clear();
            this.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[]
            {
                colCode
                ,colName
            });
            this.OptionsBehavior.Editable = false;
        }
        void loadCategories(int parentCategoryID, DevExpress.XtraTreeList.Nodes.TreeListNodes nodes)
        {
            foreach (ItemCategory cat in iERPTransactionClient.GetItemCategories(parentCategoryID))
            {
                DevExpress.XtraTreeList.Nodes.TreeListNode n = nodes.Add(cat.Code, cat.description);
                n.Tag = cat;
                loadCategories(cat.ID, n.Nodes);
            }
        }
        public void reloadCategories()
        {
            this.Nodes.Clear();
            loadCategories(-1, this.Nodes);
            this.ExpandAll();
        }
       
    }
}
