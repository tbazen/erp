﻿namespace BIZNET.iERP.Client
{
    partial class ItemManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemManager));
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.standaloneBarDockControl2 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.treeManager = new DevExpress.XtraTreeList.TreeList();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridManager = new DevExpress.XtraGrid.GridControl();
            this.gridViewManager = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barManager = new DevExpress.XtraBars.Bar();
            this.btnAddRootCateg = new DevExpress.XtraBars.BarButtonItem();
            this.btnAddSubCateg = new DevExpress.XtraBars.BarButtonItem();
            this.btnAddItem = new DevExpress.XtraBars.BarButtonItem();
            this.btnEdit = new DevExpress.XtraBars.BarButtonItem();
            this.btnDelete = new DevExpress.XtraBars.BarButtonItem();
            this.btnActivate = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonUnitConversion = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonRefreshItems = new DevExpress.XtraBars.BarButtonItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnFirst = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrevious = new DevExpress.XtraBars.BarButtonItem();
            this.txtCurrentPage = new DevExpress.XtraBars.BarStaticItem();
            this.txtTotalRecords = new DevExpress.XtraBars.BarStaticItem();
            this.btnNext = new DevExpress.XtraBars.BarButtonItem();
            this.btnLast = new DevExpress.XtraBars.BarButtonItem();
            this.standaloneBarDockControl3 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barCheckItem1 = new DevExpress.XtraBars.BarCheckItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutConverter1 = new DevExpress.XtraLayout.Converter.LayoutConverter(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.chkSalesItem = new DevExpress.XtraEditors.CheckEdit();
            this.chkFixedAssetItem = new DevExpress.XtraEditors.CheckEdit();
            this.txtCode = new DevExpress.XtraEditors.TextEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.btnSearch = new DevExpress.XtraEditors.SimpleButton();
            this.cmbExpenseType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.chkExpenseItem = new DevExpress.XtraEditors.CheckEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutExpenseType = new DevExpress.XtraLayout.LayoutControlItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFixedAssetItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbExpenseType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpenseItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExpenseType)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1074, 480);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.AppearanceTabPage.Header.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup2.AppearanceTabPage.Header.Options.UseFont = true;
            this.layoutControlGroup2.CaptionImage = global::BIZNET.iERP.Client.Properties.Resources.expense_info;
            this.layoutControlGroup2.CustomizationFormText = "TradeRelation General Information";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem1,
            this.layoutControlItem3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1068, 474);
            this.layoutControlGroup2.Text = "Ex General Information";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.standaloneBarDockControl2;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 50);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(5, 40);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(1068, 50);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // standaloneBarDockControl2
            // 
            this.standaloneBarDockControl2.CausesValidation = false;
            this.standaloneBarDockControl2.Location = new System.Drawing.Point(5, 5);
            this.standaloneBarDockControl2.Name = "standaloneBarDockControl2";
            this.standaloneBarDockControl2.Size = new System.Drawing.Size(1064, 46);
            this.standaloneBarDockControl2.Text = "standaloneBarDockControl2";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.treeManager;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 50);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(404, 424);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // treeManager
            // 
            this.treeManager.Appearance.FocusedRow.BackColor = System.Drawing.Color.PaleGreen;
            this.treeManager.Appearance.FocusedRow.Options.UseBackColor = true;
            this.treeManager.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeManager.Appearance.Row.Options.UseFont = true;
            this.treeManager.Location = new System.Drawing.Point(5, 55);
            this.treeManager.Name = "treeManager";
            this.treeManager.OptionsSelection.MultiSelect = true;
            this.treeManager.RowHeight = 25;
            this.treeManager.Size = new System.Drawing.Size(400, 420);
            this.treeManager.TabIndex = 11;
            this.treeManager.BeforeExpand += new DevExpress.XtraTreeList.BeforeExpandEventHandler(this.treeManager_BeforeExpand);
            this.treeManager.SelectionChanged += new System.EventHandler(this.treeManager_SelectionChanged);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridManager;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(404, 50);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(664, 424);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // gridManager
            // 
            this.gridManager.Location = new System.Drawing.Point(409, 55);
            this.gridManager.MainView = this.gridViewManager;
            this.gridManager.MenuManager = this.barManager1;
            this.gridManager.Name = "gridManager";
            this.gridManager.Size = new System.Drawing.Size(660, 420);
            this.gridManager.TabIndex = 13;
            this.gridManager.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewManager});
            this.gridManager.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridManager_ProcessGridKey);
            // 
            // gridViewManager
            // 
            this.gridViewManager.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightBlue;
            this.gridViewManager.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewManager.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewManager.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Indigo;
            this.gridViewManager.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewManager.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewManager.Appearance.SelectedRow.BackColor = System.Drawing.Color.SteelBlue;
            this.gridViewManager.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridViewManager.GridControl = this.gridManager;
            this.gridViewManager.GroupPanelText = "Items";
            this.gridViewManager.Name = "gridViewManager";
            this.gridViewManager.OptionsBehavior.Editable = false;
            this.gridViewManager.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewManager.OptionsCustomization.AllowFilter = false;
            this.gridViewManager.OptionsCustomization.AllowGroup = false;
            this.gridViewManager.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewManager.OptionsCustomization.AllowRowSizing = true;
            this.gridViewManager.OptionsCustomization.AllowSort = false;
            this.gridViewManager.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewManager_FocusedRowChanged);
            this.gridViewManager.DoubleClick += new System.EventHandler(this.gridViewManager_DoubleClick);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barManager,
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl2);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl3);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnAddItem,
            this.btnEdit,
            this.btnDelete,
            this.barCheckItem1,
            this.btnActivate,
            this.barStaticItem1,
            this.btnFirst,
            this.btnPrevious,
            this.txtTotalRecords,
            this.btnNext,
            this.btnLast,
            this.txtCurrentPage,
            this.btnAddRootCateg,
            this.btnAddSubCateg,
            this.barButtonUnitConversion,
            this.barButtonRefresh,
            this.barButtonRefreshItems});
            this.barManager1.MainMenu = this.barManager;
            this.barManager1.MaxItemId = 23;
            // 
            // barManager
            // 
            this.barManager.BarName = "Main menu";
            this.barManager.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Standalone;
            this.barManager.DockCol = 0;
            this.barManager.DockRow = 0;
            this.barManager.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.barManager.FloatLocation = new System.Drawing.Point(191, 430);
            this.barManager.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnAddRootCateg, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnAddSubCateg, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnAddItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnActivate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonUnitConversion),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonRefreshItems)});
            this.barManager.OptionsBar.MultiLine = true;
            this.barManager.OptionsBar.UseWholeRow = true;
            this.barManager.StandaloneBarDockControl = this.standaloneBarDockControl2;
            this.barManager.Text = "Main menu";
            // 
            // btnAddRootCateg
            // 
            this.btnAddRootCateg.Caption = "Add Root Category";
            this.btnAddRootCateg.Id = 17;
            this.btnAddRootCateg.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddRootCateg.ItemAppearance.Normal.Options.UseFont = true;
            this.btnAddRootCateg.Name = "btnAddRootCateg";
            this.btnAddRootCateg.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAddRootCateg_ItemClick);
            // 
            // btnAddSubCateg
            // 
            this.btnAddSubCateg.Caption = "Add Sub Category";
            this.btnAddSubCateg.Id = 18;
            this.btnAddSubCateg.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddSubCateg.ItemAppearance.Normal.Options.UseFont = true;
            this.btnAddSubCateg.Name = "btnAddSubCateg";
            this.btnAddSubCateg.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAddSubCateg_ItemClick);
            // 
            // btnAddItem
            // 
            this.btnAddItem.Caption = "&Add";
            this.btnAddItem.Glyph = global::BIZNET.iERP.Client.Properties.Resources.expense_add;
            this.btnAddItem.Id = 0;
            this.btnAddItem.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.ItemAppearance.Normal.Options.UseFont = true;
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAdd_ItemClick);
            // 
            // btnEdit
            // 
            this.btnEdit.Caption = "&Edit";
            this.btnEdit.Enabled = false;
            this.btnEdit.Glyph = global::BIZNET.iERP.Client.Properties.Resources.expense_edit;
            this.btnEdit.Id = 1;
            this.btnEdit.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.ItemAppearance.Normal.Options.UseFont = true;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEdit_ItemClick);
            // 
            // btnDelete
            // 
            this.btnDelete.Caption = "&Delete";
            this.btnDelete.Enabled = false;
            this.btnDelete.Glyph = global::BIZNET.iERP.Client.Properties.Resources.expense_remove;
            this.btnDelete.Id = 2;
            this.btnDelete.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ItemAppearance.Normal.Options.UseFont = true;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDelete_ItemClick);
            // 
            // btnActivate
            // 
            this.btnActivate.Caption = "Activate/Deactivate";
            this.btnActivate.Enabled = false;
            this.btnActivate.Glyph = global::BIZNET.iERP.Client.Properties.Resources.expense_activation;
            this.btnActivate.Id = 4;
            this.btnActivate.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActivate.ItemAppearance.Normal.Options.UseFont = true;
            this.btnActivate.Name = "btnActivate";
            this.btnActivate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnActivate_ItemClick);
            // 
            // barButtonUnitConversion
            // 
            this.barButtonUnitConversion.Caption = "Unit Convesion";
            this.barButtonUnitConversion.Id = 19;
            this.barButtonUnitConversion.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.barButtonUnitConversion.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonUnitConversion.Name = "barButtonUnitConversion";
            this.barButtonUnitConversion.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonRefresh
            // 
            this.barButtonRefresh.Caption = "Refresh Categories";
            this.barButtonRefresh.Id = 21;
            this.barButtonRefresh.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.barButtonRefresh.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonRefresh.Name = "barButtonRefresh";
            this.barButtonRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonRefresh_ItemClick);
            // 
            // barButtonRefreshItems
            // 
            this.barButtonRefreshItems.Caption = "Refresh Items";
            this.barButtonRefreshItems.Id = 22;
            this.barButtonRefreshItems.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.barButtonRefreshItems.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonRefreshItems.Name = "barButtonRefreshItems";
            this.barButtonRefreshItems.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonRefreshItems_ItemClick);
            // 
            // bar1
            // 
            this.bar1.BarName = "Search Results Menu";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Standalone;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(210, 255);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnFirst, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnPrevious, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.txtCurrentPage, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.txtTotalRecords),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNext, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnLast, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl3;
            this.bar1.Text = "Search Results Menu";
            // 
            // btnFirst
            // 
            this.btnFirst.Caption = "&First";
            this.btnFirst.Enabled = false;
            this.btnFirst.Glyph = ((System.Drawing.Image)(resources.GetObject("btnFirst.Glyph")));
            this.btnFirst.Id = 10;
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnFirst_ItemClick);
            // 
            // btnPrevious
            // 
            this.btnPrevious.Caption = "&Previous";
            this.btnPrevious.Enabled = false;
            this.btnPrevious.Glyph = ((System.Drawing.Image)(resources.GetObject("btnPrevious.Glyph")));
            this.btnPrevious.Id = 11;
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPrevious_ItemClick);
            // 
            // txtCurrentPage
            // 
            this.txtCurrentPage.Caption = "Page 1";
            this.txtCurrentPage.Id = 15;
            this.txtCurrentPage.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrentPage.ItemAppearance.Normal.Options.UseFont = true;
            this.txtCurrentPage.Name = "txtCurrentPage";
            this.txtCurrentPage.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // txtTotalRecords
            // 
            this.txtTotalRecords.Caption = "{No results found }";
            this.txtTotalRecords.Id = 12;
            this.txtTotalRecords.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalRecords.ItemAppearance.Normal.Options.UseFont = true;
            this.txtTotalRecords.Name = "txtTotalRecords";
            this.txtTotalRecords.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // btnNext
            // 
            this.btnNext.Caption = "&Next";
            this.btnNext.Enabled = false;
            this.btnNext.Glyph = ((System.Drawing.Image)(resources.GetObject("btnNext.Glyph")));
            this.btnNext.Id = 13;
            this.btnNext.Name = "btnNext";
            this.btnNext.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNext_ItemClick);
            // 
            // btnLast
            // 
            this.btnLast.Caption = "&Last";
            this.btnLast.Enabled = false;
            this.btnLast.Glyph = ((System.Drawing.Image)(resources.GetObject("btnLast.Glyph")));
            this.btnLast.Id = 14;
            this.btnLast.Name = "btnLast";
            this.btnLast.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLast_ItemClick);
            // 
            // standaloneBarDockControl3
            // 
            this.standaloneBarDockControl3.CausesValidation = false;
            this.standaloneBarDockControl3.Location = new System.Drawing.Point(5, 5);
            this.standaloneBarDockControl3.Name = "standaloneBarDockControl3";
            this.standaloneBarDockControl3.Size = new System.Drawing.Size(536, 43);
            this.standaloneBarDockControl3.Text = "standaloneBarDockControl3";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1074, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 632);
            this.barDockControlBottom.Size = new System.Drawing.Size(1074, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 632);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1074, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 632);
            // 
            // barCheckItem1
            // 
            this.barCheckItem1.Caption = "barCheckItem1";
            this.barCheckItem1.Id = 3;
            this.barCheckItem1.Name = "barCheckItem1";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "First";
            this.barStaticItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem1.Glyph")));
            this.barStaticItem1.Id = 5;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.gridManager);
            this.layoutControl1.Controls.Add(this.treeManager);
            this.layoutControl1.Controls.Add(this.standaloneBarDockControl2);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 152);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(333, 252, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1074, 480);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageSize = new System.Drawing.Size(24, 24);
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "expense_category.png");
            this.imageCollection1.Images.SetKeyName(1, "fixedasset_category.png");
            this.imageCollection1.Images.SetKeyName(2, "sales_category.png");
            this.imageCollection1.Images.SetKeyName(3, "property.ico");
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.chkSalesItem);
            this.layoutControl2.Controls.Add(this.chkFixedAssetItem);
            this.layoutControl2.Controls.Add(this.txtCode);
            this.layoutControl2.Controls.Add(this.txtName);
            this.layoutControl2.Controls.Add(this.panelControl2);
            this.layoutControl2.Controls.Add(this.cmbExpenseType);
            this.layoutControl2.Controls.Add(this.chkExpenseItem);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(567, 193, 250, 350);
            this.layoutControl2.Root = this.Root;
            this.layoutControl2.Size = new System.Drawing.Size(1074, 152);
            this.layoutControl2.TabIndex = 11;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // chkSalesItem
            // 
            this.chkSalesItem.Location = new System.Drawing.Point(138, 66);
            this.chkSalesItem.MenuManager = this.barManager1;
            this.chkSalesItem.Name = "chkSalesItem";
            this.chkSalesItem.Properties.Caption = "Sales Item";
            this.chkSalesItem.Size = new System.Drawing.Size(113, 19);
            this.chkSalesItem.StyleController = this.layoutControl2;
            this.chkSalesItem.TabIndex = 11;
            // 
            // chkFixedAssetItem
            // 
            this.chkFixedAssetItem.Location = new System.Drawing.Point(255, 66);
            this.chkFixedAssetItem.MenuManager = this.barManager1;
            this.chkFixedAssetItem.Name = "chkFixedAssetItem";
            this.chkFixedAssetItem.Properties.Caption = "Fixed Asset Item";
            this.chkFixedAssetItem.Size = new System.Drawing.Size(110, 19);
            this.chkFixedAssetItem.StyleController = this.layoutControl2;
            this.chkFixedAssetItem.TabIndex = 10;
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(478, 40);
            this.txtCode.MenuManager = this.barManager1;
            this.txtCode.Name = "txtCode";
            this.txtCode.Properties.Mask.EditMask = "[1-9]\\d*";
            this.txtCode.Size = new System.Drawing.Size(246, 20);
            this.txtCode.StyleController = this.layoutControl2;
            this.txtCode.TabIndex = 5;
            this.txtCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchKeyDown);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(82, 40);
            this.txtName.MenuManager = this.barManager1;
            this.txtName.Name = "txtName";
            this.txtName.Properties.Mask.EditMask = "[a-zA-Z].*";
            this.txtName.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtName.Size = new System.Drawing.Size(313, 20);
            this.txtName.StyleController = this.layoutControl2;
            this.txtName.TabIndex = 4;
            this.txtName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchKeyDown);
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.btnClear);
            this.panelControl2.Controls.Add(this.standaloneBarDockControl3);
            this.panelControl2.Controls.Add(this.btnSearch);
            this.panelControl2.Location = new System.Drawing.Point(5, 94);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1064, 53);
            this.panelControl2.TabIndex = 8;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.Location = new System.Drawing.Point(986, 6);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 35);
            this.btnClear.TabIndex = 0;
            this.btnClear.Text = "C&lear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(908, 7);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(72, 35);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.Text = "&Search";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // cmbExpenseType
            // 
            this.cmbExpenseType.EditValue = "All";
            this.cmbExpenseType.Location = new System.Drawing.Point(807, 40);
            this.cmbExpenseType.MenuManager = this.barManager1;
            this.cmbExpenseType.Name = "cmbExpenseType";
            this.cmbExpenseType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbExpenseType.Properties.Items.AddRange(new object[] {
            "Not directly related to production or service",
            "Directly related to production or service",
            "All"});
            this.cmbExpenseType.Size = new System.Drawing.Size(260, 20);
            this.cmbExpenseType.StyleController = this.layoutControl2;
            this.cmbExpenseType.TabIndex = 7;
            // 
            // chkExpenseItem
            // 
            this.chkExpenseItem.Location = new System.Drawing.Point(5, 66);
            this.chkExpenseItem.MenuManager = this.barManager1;
            this.chkExpenseItem.Name = "chkExpenseItem";
            this.chkExpenseItem.Properties.Caption = "Expense Item";
            this.chkExpenseItem.Size = new System.Drawing.Size(129, 19);
            this.chkExpenseItem.StyleController = this.layoutControl2;
            this.chkExpenseItem.TabIndex = 9;
            // 
            // Root
            // 
            this.Root.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Root.AppearanceGroup.Options.UseFont = true;
            this.Root.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Root.AppearanceItemCaption.Options.UseFont = true;
            this.Root.CaptionImage = global::BIZNET.iERP.Client.Properties.Resources.expense_search;
            this.Root.CustomizationFormText = "Search suppliers by following criterian";
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutName,
            this.layoutCode,
            this.layoutControlItem8,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutExpenseType});
            this.Root.Location = new System.Drawing.Point(0, 0);
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.Root.Size = new System.Drawing.Size(1074, 152);
            this.Root.Text = "Search";
            // 
            // layoutName
            // 
            this.layoutName.Control = this.txtName;
            this.layoutName.CustomizationFormText = "Name:";
            this.layoutName.Location = new System.Drawing.Point(0, 0);
            this.layoutName.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutName.MinSize = new System.Drawing.Size(93, 28);
            this.layoutName.Name = "layoutName";
            this.layoutName.Size = new System.Drawing.Size(396, 28);
            this.layoutName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutName.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutName.Text = "Name:";
            this.layoutName.TextSize = new System.Drawing.Size(72, 13);
            // 
            // layoutCode
            // 
            this.layoutCode.Control = this.txtCode;
            this.layoutCode.CustomizationFormText = "Code:";
            this.layoutCode.Location = new System.Drawing.Point(396, 0);
            this.layoutCode.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutCode.MinSize = new System.Drawing.Size(93, 28);
            this.layoutCode.Name = "layoutCode";
            this.layoutCode.Size = new System.Drawing.Size(329, 28);
            this.layoutCode.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutCode.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutCode.Text = "Code:";
            this.layoutCode.TextSize = new System.Drawing.Size(72, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.panelControl2;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.FillControlToClientArea = false;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 57);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(104, 57);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(1068, 57);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.chkExpenseItem;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(133, 28);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(133, 28);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(133, 28);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.chkFixedAssetItem;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(250, 28);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(114, 28);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(114, 28);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(818, 28);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.chkSalesItem;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(133, 28);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(117, 28);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(117, 28);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(117, 28);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutExpenseType
            // 
            this.layoutExpenseType.Control = this.cmbExpenseType;
            this.layoutExpenseType.CustomizationFormText = "VAT Registration Number:";
            this.layoutExpenseType.Location = new System.Drawing.Point(725, 0);
            this.layoutExpenseType.MaxSize = new System.Drawing.Size(343, 28);
            this.layoutExpenseType.MinSize = new System.Drawing.Size(93, 28);
            this.layoutExpenseType.Name = "layoutExpenseType";
            this.layoutExpenseType.Size = new System.Drawing.Size(343, 28);
            this.layoutExpenseType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutExpenseType.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutExpenseType.Text = "Expense Type:";
            this.layoutExpenseType.TextSize = new System.Drawing.Size(72, 13);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Unit Convesion";
            this.barButtonItem2.Id = 19;
            this.barButtonItem2.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.barButtonItem2.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // ItemManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1074, 632);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ItemManager";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Item Manager";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ItemManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFixedAssetItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbExpenseType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpenseItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExpenseType)).EndInit();
            this.ResumeLayout(false);

}

        #endregion


        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnAddItem;
        private DevExpress.XtraBars.BarButtonItem btnEdit;
        private DevExpress.XtraBars.BarButtonItem btnDelete;
        private DevExpress.XtraBars.BarButtonItem btnActivate;
        private DevExpress.XtraBars.BarCheckItem barCheckItem1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraLayout.Converter.LayoutConverter layoutConverter1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarButtonItem btnFirst;
        private DevExpress.XtraBars.BarButtonItem btnPrevious;
        private DevExpress.XtraBars.BarStaticItem txtTotalRecords;
        private DevExpress.XtraBars.BarButtonItem btnNext;
        private DevExpress.XtraBars.BarButtonItem btnLast;
        private DevExpress.XtraBars.BarStaticItem txtCurrentPage;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraBars.BarButtonItem btnAddRootCateg;
        private DevExpress.XtraBars.BarButtonItem btnAddSubCateg;
        private DevExpress.XtraTreeList.TreeList treeManager;
        private DevExpress.XtraGrid.GridControl gridManager;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewManager;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.BarButtonItem barButtonUnitConversion;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl3;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.CheckEdit chkSalesItem;
        private DevExpress.XtraEditors.CheckEdit chkFixedAssetItem;
        private DevExpress.XtraEditors.TextEdit txtCode;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private DevExpress.XtraEditors.SimpleButton btnSearch;
        private DevExpress.XtraEditors.ComboBoxEdit cmbExpenseType;
        private DevExpress.XtraEditors.CheckEdit chkExpenseItem;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutName;
        private DevExpress.XtraLayout.LayoutControlItem layoutCode;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutExpenseType;
        private DevExpress.XtraBars.BarButtonItem barButtonRefresh;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonRefreshItems;
    }
}