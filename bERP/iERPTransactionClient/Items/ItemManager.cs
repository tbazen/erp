﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using DevExpress.XtraLayout.Utils;
using System.Collections;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using DevExpress.XtraGrid;
using DevExpress.Utils;
using DevExpress.XtraLayout;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using System.Linq;
using BIZNET.iERP.Client.Items;
namespace BIZNET.iERP.Client
{
    public partial class ItemManager : XtraForm
    {
        private string[] m_Column;
        private object[] m_Criteria;
        private int pageIndex = 0;
        private const int PAGE_SIZE = 15;
        private int NoOfRecords;
        TreeListColumn[] columns;
        private const string CATEGORY_NODETYPE = "Category";
        private const string ITEM_NODETYPE = "Item";
        private int m_CategoryImageIndex = 0;
        private int m_propertyImageIndex = 3;
        private TreeListNode m_SelectedNode;
        private bool m_CategoryClicked = false;
        List<int> LoadedNodes;
        DataTable tableItems;
        DevExpress.XtraSplashScreen.SplashScreenManager waitScreen;
        private ItemCategoryEditor2 m_ItemCategoryInstance;
        public ItemManager()
        {
            InitializeComponent();
            columns = new TreeListColumn[7];
            tableItems = new DataTable();
            PrepareItemsTable();
            LoadedNodes = new List<int>();
            DisableBarButtonItems();
            CreateColumns();
            treeManager.StateImageList = imageCollection1;
            waitScreen = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::BIZNET.iERP.Client.WaitForm1), true, true);
            layoutExpenseType.HideToCustomization();
            LoadData();
        }

        private void PrepareItemsTable()
        {
            tableItems.Columns.Add("Code", typeof(string));
            tableItems.Columns.Add("Name", typeof(string));
            tableItems.Columns.Add("Unit", typeof (String));
            tableItems.Columns.Add("Type", typeof(string));
            tableItems.Columns.Add("Category Name", typeof(string));
            tableItems.Columns.Add("CategoryID", typeof(int));
            tableItems.Columns.Add("Activated", typeof(bool));
            tableItems.Columns.Add("itemCode", typeof(string));
            gridManager.DataSource = tableItems;
            gridViewManager.Columns["Activated"].Visible = false;
            gridViewManager.Columns["CategoryID"].Visible = false;
            gridViewManager.Columns["itemCode"].Visible = false;
        }

        private void DisableBarButtonItems()
        {
            btnAddSubCateg.Enabled = false;
            btnAddItem.Enabled = false;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
            btnActivate.Enabled = false;
        }

        private void CreateColumns()
        {
            #region ID Column
            columns[0] = new TreeListColumn();
            columns[0].AllowIncrementalSearch = false;
            columns[0].Caption = "ID";
            columns[0].FieldName = "ID";
            columns[0].Name = "colID";
            columns[0].UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.Integer;
            #endregion
            #region Code Column
            columns[1] = new TreeListColumn();
            columns[1].Caption = "Code";
            columns[1].FieldName = "Code";
            columns[1].MinWidth = 51;
            columns[1].Name = "colCode";
            columns[1].OptionsColumn.AllowEdit = false;
            columns[1].OptionsColumn.AllowMove = false;
            columns[1].OptionsColumn.AllowMoveToCustomizationForm = false;
            columns[1].OptionsColumn.AllowSort = false;
            columns[1].UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            columns[1].Visible = true;
            columns[1].VisibleIndex = 0;
            columns[1].Width = 70;
            #endregion

            #region Name Column
            columns[2] = new TreeListColumn();
            columns[2].Caption = "Name";
            columns[2].FieldName = "Name";
            columns[2].MinWidth = 51;
            columns[2].Name = "colName";
            columns[2].OptionsColumn.AllowEdit = false;
            columns[2].OptionsColumn.AllowMove = false;
            columns[2].OptionsColumn.AllowMoveToCustomizationForm = false;
            columns[2].OptionsColumn.AllowSort = false;
            columns[2].UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            columns[2].Visible = true;
            columns[2].VisibleIndex = 1;
            columns[2].Width = 300;
            #endregion

            #region Unit Column
            columns[3] = new TreeListColumn();
            columns[3].Caption = "Unit";
            columns[3].FieldName = "Unit";
            columns[3].MinWidth = 15;
            columns[3].Name = "colName";
            columns[3].OptionsColumn.AllowEdit = false;
            columns[3].OptionsColumn.AllowMove = false;
            columns[3].OptionsColumn.AllowMoveToCustomizationForm = false;
            columns[3].OptionsColumn.AllowSort = false;
            columns[3].UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            columns[3].Visible = true;
            columns[3].VisibleIndex = 1;
            columns[3].Width = 20;
            #endregion

            #region Type Column
            columns[4] = new TreeListColumn();
            columns[4].AllowIncrementalSearch = false;
            columns[4].Caption = "Type";
            columns[4].FieldName = "Type";
            columns[4].Name = "colType";
            columns[4].UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            #endregion

            #region Activation Status Column
            columns[5] = new TreeListColumn();
            columns[5].Caption = "Status";
            columns[5].FieldName = "Status";
            columns[5].MinWidth = 51;
            columns[5].Name = "colStatus";
            columns[5].UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.Boolean;
            #endregion

            #region Node Type Column
            columns[6] = new TreeListColumn();
            columns[6].Caption = "NodeType";
            columns[6].FieldName = "NodeType";
            columns[6].MinWidth = 51;
            columns[6].Name = "colNodeType";
            columns[6].UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            #endregion

            treeManager.Columns.AddRange(columns);
        }
        private void LoadData()
        {
            LoadedNodes.Clear();
            treeManager.Nodes.Clear();
            LoadChilds(treeManager.Nodes, -1);
            LoadChildOfChilds(treeManager.Nodes);
        }

      

        private void SearchItems()
        {
            try
            {
                TransactionItems[] items = iERPTransactionClient.SearchTransactionItems(pageIndex * PAGE_SIZE, PAGE_SIZE, m_Criteria, m_Column, out NoOfRecords);
                double NumberOfPages = Math.Ceiling(((double)NoOfRecords / (double)PAGE_SIZE));
                string pageInfo = String.Format(" of {0}", NumberOfPages);
                int currentPage = pageIndex + 1;
                txtCurrentPage.Caption = String.Format("Page {0}{1}", currentPage, pageInfo);
                txtTotalRecords.Caption = String.Format("{0} records found", NoOfRecords);
                ChangeNavigationsVisibility(NumberOfPages);
                PopulateItemSearchGrid(items);
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to retrieve items\n" + ex.Message);
            }
            finally
            {
                if (waitScreen.IsSplashFormVisible)
                    waitScreen.CloseWaitForm();
            }
        }

        private void PopulateItemSearchGrid(TransactionItems[] items)
        {
            try
            {
                tableItems.Clear();
                waitScreen.ShowWaitForm();
                foreach (TransactionItems item in items)
                {
                    DataRow row = tableItems.NewRow();
                    waitScreen.SetWaitFormDescription("Searching: " + item.Name);
                    PropertyItemCategory cat = iERPTransactionClient.getPropertyItemCategory(item.Code);
                    string itemType = GetItemTypeDescription(item);
                    row[0] = item.Code;
                    row[1] = item.Name+(cat==null?"":"(+)");
                    MeasureUnit mu = iERPTransactionClient.GetMeasureUnit(item.MeasureUnitID);
                    row[2] =mu==null?"":mu.Name;
                    row[3] = itemType;
                    row[4] = iERPTransactionClient.GetItemCategory(item.categoryID).description;
                    row[5] = item.categoryID;
                    row[6] = item.Activated ? true : false;
                    row[7] = item.Code;
                    tableItems.Rows.Add(row);
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
            finally
            {
                if (waitScreen.IsSplashFormVisible)
                    waitScreen.CloseWaitForm();
            }
            gridManager.DataSource = tableItems;
            gridManager.RefreshDataSource();
            ApplyFormatConditioning();
        }

        private string GetItemTypeDescription(TransactionItems item)
        {
            List<string> list = new List<string>();
            if (item.IsExpenseItem)
                list.Add(item.IsDirectCost ? "Direct Cost Item" : "Expense Item");
            if (item.IsSalesItem)
                list.Add("Sales Item");
            if (item.IsFixedAssetItem)
                list.Add("Fixed Asset Item");
            return string.Join(",", list.ToArray());
        }

        private void ApplyFormatConditioning()
        {
            DevExpress.XtraGrid.StyleFormatCondition condition = new DevExpress.XtraGrid.StyleFormatCondition();
            condition.Appearance.Options.UseBackColor = true;
            condition.Appearance.BackColor = Color.FromArgb(0x8F, 0xF2, 0x14, 0x43);
            condition.Condition = FormatConditionEnum.Expression;
            condition.Expression = "[Activated] != True";
            condition.ApplyToRow = true;
            gridViewManager.FormatConditions.Add(condition);
        }
        void LoadChilds(TreeListNodes nodes, int PID)
        {
            if (LoadedNodes.Contains(PID))
                return;
            LoadedNodes.Add(PID);

            ItemCategory[] categories = iERPTransactionClient.GetItemCategories(PID);
            int N;
            PropertyItemCategory[] propCat = iERPTransactionClient.getPropertyItemCategory(PID, 0, -1, out N);
            if (categories != null)
            {
                foreach (ItemCategory categ in categories)
                {
                    bool propertyCategory = false;
                    foreach(PropertyItemCategory c in propCat)
                    {
                        if(c.categorID==categ.ID)
                        {
                            propertyCategory = true;
                            break;
                        }
                    }
                    if (propertyCategory)
                        continue;
                    AddItemCategoryNode(nodes, categ);
                }
            }

        }
        void LoadChildOfChilds(TreeListNodes nodes)
        {
            foreach (TreeListNode n in nodes)
            {
                if (n.Tag is ItemCategory)
                    LoadChilds(n.Nodes, ((ItemCategory)n.Tag).ID);
            }
        }
        TreeListNode AddItemCategoryNode(TreeListNodes parent, ItemCategory category)
        {
            TreeListNode n = treeManager.AppendNode(new object[] { category.ID,
                               category.Code,
                        category.description,
                        String.Empty,
                        true,
                        CATEGORY_NODETYPE
                           }, parent.ParentNode, category);
            n.StateImageIndex = m_CategoryImageIndex;
            //n.Tag = category;
            return n;
        }
        
        private void AddItemNodes(TreeListNodes nodes, int categId)
        {
            TransactionItems[] items = iERPTransactionClient.GetItemsInCategory(categId);
            if (items != null)
            {
                foreach (TransactionItems item in items)
                {
                    AddItemNode(nodes, item);
                }
            }
        }
        void AddItemNode(TreeListNodes nodes, TransactionItems item)
        {
        }
        private void ChangeNavigationsVisibility(double NumberOfPages)
        {
            ChangeNavigatePreviousVisibility();
            ChangeNavigateNextVisibility(NumberOfPages);
        }
        private void ChangeNavigatePreviousVisibility()
        {
            btnPrevious.Enabled = pageIndex > 0;
        }
        private void ChangeNavigateNextVisibility(double NumberOfPages)
        {
            btnNext.Enabled = pageIndex < NumberOfPages - 1;
        }
        private void btnAdd_ItemClick(object sender, ItemClickEventArgs e)
        {
            m_CategoryClicked = false;
            if (treeManager.Selection.Count == 1) //Only one selected
            {
                m_SelectedNode = treeManager.Selection[0];
            }
            int categID = (int)m_SelectedNode[0];

            AddTransactionItem(categID);
        }
        private void AddTransactionItem(int categID)
        {

            using (RegisterItem registerItem = new RegisterItem(categID))
            {
                registerItem.ShowInTaskbar = false;
                registerItem.StartPosition = FormStartPosition.CenterScreen;
                registerItem.ItemAdded += ItemAdded;

                if (registerItem.ShowDialog() == DialogResult.Cancel)
                {
                    registerItem.ItemAdded -= ItemAdded;
                }
            }
        }
        void ItemAdded(string code)
        {
            try
            {
                TransactionItems item = iERPTransactionClient.GetTransactionItems(code);
                PopulateItemSearchGrid(new TransactionItems[] { item });
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
           
        }
        private void btnEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (treeManager.Focused)
            {
                ShowEditingForTree();
            }
            else if (gridManager.Focused)
            {
                ShowEditingForGrid();
            }
        }
        private void ShowEditingForGrid()
        {
            string code = (string)gridViewManager.GetRowCellValue(gridViewManager.FocusedRowHandle, "itemCode");
            int categID = (int)gridViewManager.GetRowCellValue(gridViewManager.FocusedRowHandle, "CategoryID");
            EditTransactionItem(code, categID);
        }
        private void EditTransactionItem(string code, int categID)
        {
            object extraData;
            Property prop = iERPTransactionClient.getPropertyByItemCode(code, out extraData);
            if (prop == null)
            {
                using (RegisterItem item = new RegisterItem(code, categID))
                {
                    item.StartPosition = FormStartPosition.CenterScreen;
                    item.ShowInTaskbar = false;
                    if (item.ShowDialog() == DialogResult.OK)
                    {
                        TransactionItems updatedItem = iERPTransactionClient.GetTransactionItems(code);
                        gridViewManager.SetRowCellValue(gridViewManager.FocusedRowHandle, "Name", updatedItem.Name);
                        gridViewManager.SetRowCellValue(gridViewManager.FocusedRowHandle, "Type", GetItemTypeDescription(updatedItem));
                    }
                }
            }
            else
            {
                RegisterProperty propForm = new RegisterProperty(prop.id);
                if(propForm.ShowDialog(this)==DialogResult.OK)
                {
                    SearchItems();
                }
            }
        }
        private void ShowEditingForTree()
        {
            TreeListNode editNode = treeManager.Selection[0];
            if ((string)editNode[5] == CATEGORY_NODETYPE) //If Node is category
            {
                ShowCategoryEditForm();
            }         
        }
        private void ShowCategoryEditForm()
        {
            if (m_SelectedNode != null)
            {
                ItemCategory category = iERPTransactionClient.GetItemCategory((int)m_SelectedNode[0]);
                string parentCategName = m_SelectedNode.ParentNode == null ? null : m_SelectedNode.ParentNode[2] as string;
                int parentCategID = m_SelectedNode.ParentNode == null ? -1 : (int)m_SelectedNode.ParentNode[0];
                using (ItemCategoryEditor2 categoryEditor = new ItemCategoryEditor2(category, parentCategName, parentCategID, true))
                {
                    
                    categoryEditor.StartPosition = FormStartPosition.CenterScreen;
                    categoryEditor.ShowInTaskbar = false;
                    categoryEditor.ChangeButtonTextToUpdate();
                    categoryEditor.CategoryUpdated += CategoryUpdated;
                    if (categoryEditor.ShowDialog() == DialogResult.Cancel)
                    {
                        categoryEditor.CategoryUpdated -= CategoryUpdated;
                    }
                }
            }
        }
        void CategoryUpdated(ItemCategory itemCateg)
        {
            try
            {
                int id = iERPTransactionClient.RegisterItemCategory(itemCateg);
                m_SelectedNode.SetValue(2, itemCateg.description);
                m_SelectedNode.SetValue(1, itemCateg.Code);
                treeManager.RefreshNode(m_SelectedNode);
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
            finally
            {
                if (waitScreen.IsSplashFormVisible)
                    waitScreen.CloseWaitForm();
            }
        }
        private void btnDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (treeManager.Focused && treeManager.Selection.Count > 0)
                    ShowDeleteForTree();
                else if (gridManager.IsFocused && gridViewManager.SelectedRowsCount > 0)
                    ShowDeleteForGrid();
            }
            catch(Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }
        private void ShowDeleteForGrid()
        {

            string code = (string)gridViewManager.GetRowCellValue(gridViewManager.FocusedRowHandle, "itemCode");
            if (MessageBox.ShowWarningMessage("Are you sure you want to delete the selected item?") == DialogResult.Yes)
            {
                iERPTransactionClient.DeleteTransactionItem(code);
                gridViewManager.DeleteSelectedRows();
            }
        }
        private void ShowDeleteForTree()
        {
            if ((string)m_SelectedNode[5] == CATEGORY_NODETYPE)
            {
                int categID = (int)m_SelectedNode[0];
                if (MessageBox.ShowWarningMessage("Are you sure you want to delete the selected category?") == DialogResult.Yes)
                {
                    iERPTransactionClient.DeleteItemCategory(categID);
                    treeManager.DeleteSelectedNodes();
                }
            }
        
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            btnFirst.Enabled = btnLast.Enabled = true;
            pageIndex = 0;
            ArrayList columns = new ArrayList();
            ArrayList criteria = new ArrayList();
            CollectSearchCriteria(columns, criteria);
            m_Column = new string[columns.Count];
            columns.CopyTo(m_Column);
            m_Criteria = new object[criteria.Count];
            criteria.CopyTo(m_Criteria);
            if (waitScreen == null)
                waitScreen = new DevExpress.XtraSplashScreen.SplashScreenManager();
           // waitScreen.ShowWaitForm();
            Search();
           // waitScreen.CloseWaitForm();
        }
        private void CollectSearchCriteria(ArrayList columns, ArrayList criteria)
        {
            if (txtCode.Text != "")
            {
                columns.Add("Code");
                criteria.Add(txtCode.Text);
            }
            if (txtName.Text != "")
            {
                columns.Add("Name");
                criteria.Add(txtName.Text);
            }
             if (chkExpenseItem.Checked)
            {
                columns.Add("IsExpenseItem");
                criteria.Add(true);
            }
             if (chkSalesItem.Checked)
             {
                 columns.Add("IsSalesItem");
                 criteria.Add(true);
             }
             if (chkFixedAssetItem.Checked)
             {
                 columns.Add("IsFixedAssetItem");
                 criteria.Add(true);
             }
            if (!layoutExpenseType.IsHidden && cmbExpenseType.SelectedIndex != 2)
            {
                columns.Add("IsDirectCost");
                criteria.Add(cmbExpenseType.SelectedIndex == 0 ? false : true);
            }
           
        }
        private void btnNext_ItemClick(object sender, ItemClickEventArgs e)
        {
            pageIndex++;
            Search();
        }
        private void btnPrevious_ItemClick(object sender, ItemClickEventArgs e)
        {
            pageIndex--;
            Search();
        }
        private void btnLast_ItemClick(object sender, ItemClickEventArgs e)
        {
            double NumberOfPages = Math.Ceiling(((double)NoOfRecords / (double)PAGE_SIZE));
            pageIndex = (int)NumberOfPages - 1;
            Search();
        }
        private void btnFirst_ItemClick(object sender, ItemClickEventArgs e)
        {
            pageIndex = 0;
            Search();
        }
        private void Search()
        {
            LoadedNodes.Clear();
            SearchItems();
        }
        private void ApplyManagerFormatConditioning()
        {
            StyleFormatCondition condition = new StyleFormatCondition();
            condition.Appearance.Options.UseBackColor = true;
            condition.Appearance.BackColor = Color.FromArgb(0x8F, 0xF2, 0x14, 0x43);
            condition.Condition = FormatConditionEnum.Expression;
            condition.Expression = "[Activated] == 'False'";
            condition.ApplyToRow = true;
            //gridViewManager.FormatConditions.Add(condition);
        }
        private void ApplyAccountInfoFormatConditioning()
        {
            StyleFormatCondition condition = new StyleFormatCondition();
            condition.Appearance.Options.UseBackColor = true;
            condition.Appearance.BackColor = Color.FromArgb(0x8F, 0xF2, 0x14, 0x43);
            condition.Condition = FormatConditionEnum.Expression;
            condition.Expression = "[Activated] != True";
            condition.ApplyToRow = true;
            //gridViewAccountInfo.FormatConditions.Add(condition);
        }
        private void btnActivate_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (gridViewManager.SelectedRowsCount > 0)
                ShowActivationForGrid();
        }

        private void ShowActivationForGrid()
        {
            string code = (string)gridViewManager.GetRowCellValue(gridViewManager.FocusedRowHandle, "Code");
            bool activated = (bool)gridViewManager.GetRowCellValue(gridViewManager.FocusedRowHandle, "Activated");

            ActivateOrDeactivateItem(code, activated);

        }

        private void ActivateOrDeactivateItem(string code, bool activated)
        {
            try
            {
                bool confirmed = false;
                if (activated)
                {
                    if (MessageBox.ShowWarningMessage("Are you sure you want to deactivate the selected item?") == DialogResult.Yes)
                    {
                        iERPTransactionClient.DeactivateTransactionItem(code);
                        confirmed = true;
                    }
                }
                else
                {
                    if (MessageBox.ShowWarningMessage("Are you sure you want to activate the selected item?") == DialogResult.Yes)
                    {
                        iERPTransactionClient.ActivateTransactionItem(code);
                        confirmed = true;
                    }
                }
                if (confirmed)
                {
                    string message = activated ? "item successfully deactivated" : "item successfully activated";
                    MessageBox.ShowSuccessMessage(message);
                    PopulateItemSearchGrid(new TransactionItems[] { iERPTransactionClient.GetTransactionItems(code) });
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to activate/deactivate item!\n" + ex.Message);

            }
            finally
            {
                if (waitScreen.IsSplashFormVisible)
                    waitScreen.CloseWaitForm();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtName.Text = "";
            cmbExpenseType.SelectedIndex = 2;
            txtCode.Text = "";
        }

        private void gridViewManager_DoubleClick(object sender, EventArgs e)
        {
            ShowEditingForGrid();
        }

        private void gridManager_ProcessGridKey(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ShowEditingForGrid();
            }
        }

        private void treeManager_SelectionChanged(object sender, EventArgs e)
        {
            m_SelectedNode = treeManager.Selection[0];
            btnAddRootCateg.Enabled = true;
            if (treeManager.Selection.Count == 1) //one node selected
            {
                btnAddSubCateg.Enabled = (string)treeManager.Selection[0][5] == CATEGORY_NODETYPE ? true : false;
                btnAddItem.Enabled = (string)treeManager.Selection[0][5] == CATEGORY_NODETYPE ? true : false;
                btnEdit.Enabled = (bool)m_SelectedNode[4];
                btnDelete.Enabled = true;
                btnActivate.Enabled = (string)treeManager.Selection[0][5] == ITEM_NODETYPE ? true : false;
                btnActivate.Caption = (bool)m_SelectedNode[4] ? "Deactivate" : "Activate";

                if ((string)treeManager.Selection[0][5] == CATEGORY_NODETYPE)
                {
                    if (m_SelectedNode.Tag != null)
                    {
                        TransactionItems[] items = iERPTransactionClient.GetItemsInCategory(((ItemCategory)m_SelectedNode.Tag).ID);
                        if (items != null)
                        {
                            PopulateItemSearchGrid(items);
                        }
                        else
                        {
                            ClearGridView();
                        }
                    }
                }
                else
                {
                    txtCode.Text = (string)m_SelectedNode[1];
                    btnSearch_Click(null, null);
                }
            }
            else //Multiple nodes selected
            {
                btnAddSubCateg.Enabled = false;
                btnAddItem.Enabled = false;
                btnEdit.Enabled = false;
                btnDelete.Enabled = false;
                btnActivate.Enabled = false;
            }

        }

        private TransactionItems[] FilterItemsByType(TransactionItems[] items, ItemType itemType)
        {
            List<TransactionItems> tranItems = new List<TransactionItems>();
            foreach (TransactionItems item in items)
            {
                if (itemType == ItemType.Expense)
                {
                    if (item.IsExpenseItem)
                        tranItems.Add(item);
                }
                if (itemType == ItemType.Sales)
                {
                    if (item.IsSalesItem)
                        tranItems.Add(item);
                }
                if (itemType == ItemType.FixedAsset)
                {
                    if (item.IsFixedAssetItem)
                        tranItems.Add(item);
                }
            }
            return tranItems.ToArray();
        }

        private void ClearGridView()
        {
            tableItems.Clear();
            gridManager.DataSource = tableItems;
            gridViewManager.RefreshData();
        }

        private void btnAddRootCateg_ItemClick(object sender, ItemClickEventArgs e)
        {
            m_CategoryClicked = true;
            m_SelectedNode = null;
            using (ItemCategoryEditor2 categEditor = new ItemCategoryEditor2(null, -1, false))
            {
                categEditor.StartPosition = FormStartPosition.CenterScreen;
                categEditor.ShowInTaskbar = false;
                categEditor.NewCategoryAdded += new ItemCategoryEditor2.NewCategoryAddedHandler(NewCategoryAdded);

                if (categEditor.ShowDialog() == DialogResult.Cancel)
                {
                    categEditor.NewCategoryAdded -= NewCategoryAdded;
                }
            }
        }
        void NewCategoryAdded(ItemCategory category)
        {
            int parentCategID = -1;
            if (m_CategoryClicked)
            {
                m_SelectedNode = null;
            }
            else
                parentCategID = (int)m_SelectedNode[0];
            try
            {
                category.PID = parentCategID;
                int categID = iERPTransactionClient.RegisterItemCategory(category);
                category.ID = categID;
                if (m_ItemCategoryInstance != null)
                    m_ItemCategoryInstance.ClearControls();
                if (categID != -1)
                {
                    treeManager.BeginUnboundLoad();

                    TreeListNode newNode = treeManager.AppendNode(
                           new object[] { categID,
                               category.Code,
                        category.description,
                        String.Empty,
                        true,
                        CATEGORY_NODETYPE
                           }, m_SelectedNode, category);
                     newNode.StateImageIndex = m_CategoryImageIndex;
                    // newNode.Tag = category;
                    newNode.Visible = true;
                    if (m_SelectedNode != null)
                    {
                        m_SelectedNode.ExpandAll();
                        treeManager.RefreshNode(m_SelectedNode);
                    }
                    treeManager.EndUnboundLoad();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
            finally
            {
                if (waitScreen.IsSplashFormVisible)
                    waitScreen.CloseWaitForm();
            }
        }

        private void btnAddSubCateg_ItemClick(object sender, ItemClickEventArgs e)
        {
            m_CategoryClicked = false;
            if (treeManager.Selection.Count == 1) //Only one selected
            {
                m_SelectedNode = treeManager.Selection[0];
            }
            string parentCategName = (string)m_SelectedNode[2];

            using (ItemCategoryEditor2 itemCategory = new ItemCategoryEditor2(parentCategName, (int)m_SelectedNode[0], false))
            {
                m_ItemCategoryInstance = itemCategory;
                itemCategory.StartPosition = FormStartPosition.CenterScreen;
                itemCategory.ShowInTaskbar = false;
                itemCategory.NewCategoryAdded += NewCategoryAdded;

                if (itemCategory.ShowDialog() == DialogResult.Cancel)
                {
                    itemCategory.NewCategoryAdded -= NewCategoryAdded;
                }
            }
        }

        private void treeManager_BeforeExpand(object sender, DevExpress.XtraTreeList.BeforeExpandEventArgs e)
        {
            LoadChildOfChilds(e.Node.Nodes);
            ApplyTreeFormatConditioning();
        }

        private void ApplyTreeFormatConditioning()
        {
            DevExpress.XtraTreeList.StyleFormatConditions.StyleFormatCondition condition = new DevExpress.XtraTreeList.StyleFormatConditions.StyleFormatCondition();
            condition.Column = treeManager.Columns[4];
            condition.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            condition.Appearance.Options.UseBackColor = true;
            condition.Appearance.BackColor = Color.FromArgb(0x8F, 0xF2, 0x14, 0x43);
            condition.Expression = "[Status] != True";
            condition.ApplyToRow = true;
            treeManager.FormatConditions.Add(condition);
        }

        private void gridViewManager_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewManager.SelectedRowsCount == 1)
            {
                btnEdit.Enabled = true;
                btnDelete.Enabled = true;
                btnActivate.Enabled = true;
                btnActivate.Caption = (bool)gridViewManager.GetRowCellValue(e.FocusedRowHandle, "Activated") ? "Deactivate" : "Activate";
            }
            else
            {
                btnEdit.Enabled = false;
                btnDelete.Enabled = treeManager.Selection.Count==1;
                btnActivate.Enabled = false;
            }
        }

        private void chkExpenseItem_CheckedChanged(object sender, EventArgs e)
        {
            if (chkExpenseItem.Checked)
            {
                if (layoutExpenseType.IsHidden)
                    layoutExpenseType.RestoreFromCustomization(layoutCode, InsertType.Right);
                chkFixedAssetItem.Checked = false;
            }
            else
                layoutExpenseType.HideToCustomization();
        }

        private void chkSalesItem_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSalesItem.Checked && !chkExpenseItem.Checked)
                layoutExpenseType.HideToCustomization();
        }

        private void chkFixedAssetItem_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFixedAssetItem.Checked)
            {
                chkExpenseItem.Checked = false;
            }
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            ConversionFactorEditor editor = new ConversionFactorEditor("", -1);
            editor.Show(this);
        }

        private void ItemManager_Load(object sender, EventArgs e)
        {

        }

        private void searchKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSearch_Click(null, null);
        }
        
        private void barButtonRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            int selectedCategoryID=treeManager.Selection.Count==0?-1: (int)treeManager.Selection[0].GetValue(0);
            LoadData();
            TreeListNode n=getNodeByCategoryID(selectedCategoryID);
            if (n != null)
            {
                treeManager.Selection.Clear();
                n.Selected = true;
                while (n.ParentNode != null)
                {
                    n.ParentNode.Expanded = true;
                    n = n.ParentNode;
                }
                treeManager_SelectionChanged(null, null);
            }

        }
        TreeListNode searchNode(TreeListNodes nodes,int categryID)
        {
            foreach (TreeListNode n in nodes)
            {
                if ((int)n.GetValue(0) == categryID)
                    return n;
                TreeListNode r = searchNode(n.Nodes, categryID);
                if (r != null)
                    return r;
            }
            return null;
        }
        private TreeListNode getNodeByCategoryID(int categoryID)
        {
            return searchNode(treeManager.Nodes, categoryID);
        }

        private void barButtonRefreshItems_ItemClick(object sender, ItemClickEventArgs e)
        {
            if(treeManager.Selection.Count>0)
                treeManager_SelectionChanged(null, null);
        }
    }
    public enum ItemManagerType
    {
        ExpenseItem,
        SalesItem,
        FixedAsset
    }
}