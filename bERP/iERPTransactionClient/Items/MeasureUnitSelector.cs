﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    class MeasureUnitSelector : ComboBoxEdit
    {

        public MeasureUnitSelector()
        {
            Properties.TextEditStyle = TextEditStyles.Standard;
            if (AccountingClient.IsConnected)
                LoadMeasureUnits();
        }

        private void LoadMeasureUnits()
        {
            MeasureUnit[] units = iERPTransactionClient.GetMeasureUnits();
            if (units != null)
            {
                foreach (MeasureUnit unit in units)
                {
                    Properties.Items.Add(unit);
                }
                if (Properties.Items.Count > 0)
                    SelectedIndex = 0;
            }
        }

    }
}
