﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERP.Client
{
    public partial class PickProperty : Form
    {
        public int pickedPropertyID = -1;
        public PickProperty()
        {
            InitializeComponent();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (propertyPlaceHolder.GetObjectID() == -1)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select a property");
                return;
            }
            pickedPropertyID = propertyPlaceHolder.GetObjectID();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
