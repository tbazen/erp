using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace BIZNET.iERP.Client
{
    public class PropertyFormAttribute : Attribute
    {
        public Type dataType;
    }
    public abstract class PropertyFormBase : UserControl
    {
        public abstract void setData(object data);
        public abstract object getData();
        public abstract string validate();
        static Dictionary<Type, Type> formTypeOfDataType = new Dictionary<Type, Type>();
        static PropertyFormBase()
        {
            foreach (Type t in System.Reflection.Assembly.GetCallingAssembly().GetTypes())
            {
                if (t.IsSubclassOf(typeof(PropertyFormBase)))
                {
                    if (!t.IsAbstract)
                    {
                        object[] atr = t.GetCustomAttributes(typeof(PropertyFormAttribute), false);
                        if (atr != null && atr.Length> 0)
                        {
                            formTypeOfDataType.Add(((PropertyFormAttribute)atr[0]).dataType, t);
                        }
                    }
                }
            }
        }
        public static PropertyFormBase getFormForDataType(Type t)
        {
            if (t == null)
                return null;
            if (!formTypeOfDataType.ContainsKey(t))
                return null;
            return Activator.CreateInstance(formTypeOfDataType[t]) as PropertyFormBase;
        }
    }
}
