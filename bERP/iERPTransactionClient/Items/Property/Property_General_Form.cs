﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERP.Client
{    
    [PropertyForm(dataType=typeof(Property_General))]
    public partial class Property_General_Form : PropertyFormBase
    {
        public Property_General_Form()
        {
            InitializeComponent();
        }
        public override void setData(object _prop)
        {
            Property_General prop = (Property_General)_prop;
            if (prop == null)
            {
                this.textMake.Clear();
                this.textModel.Clear();
                this.textSerial.Clear();
            }
            else
            {
                this.textMake.Text = prop.make;
                this.textModel.Text = prop.model;
                this.textSerial.Text = prop.serialNo;
            }
        }
        public override string validate()
        {
            return null;
        }

        public override object getData()
        {
            Property_General ret = new Property_General();
            ret.make = this.textMake.Text;
            ret.model = this.textModel.Text;
            ret.serialNo = this.textSerial.Text;
            return ret;
        }
    }
}
