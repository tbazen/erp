﻿namespace BIZNET.iERP.Client
{
    partial class Property_Vehicle_Form
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textPlateNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textModel = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textMake = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textChasisNo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textEngineNo = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textPlateNo
            // 
            this.textPlateNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textPlateNo.Location = new System.Drawing.Point(80, 55);
            this.textPlateNo.Name = "textPlateNo";
            this.textPlateNo.Size = new System.Drawing.Size(273, 20);
            this.textPlateNo.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Plate No:";
            // 
            // textModel
            // 
            this.textModel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textModel.Location = new System.Drawing.Point(80, 29);
            this.textModel.Name = "textModel";
            this.textModel.Size = new System.Drawing.Size(273, 20);
            this.textModel.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Model:";
            // 
            // textMake
            // 
            this.textMake.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textMake.Location = new System.Drawing.Point(80, 3);
            this.textMake.Name = "textMake";
            this.textMake.Size = new System.Drawing.Size(273, 20);
            this.textMake.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Make:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Chasis No:";
            // 
            // textChasisNo
            // 
            this.textChasisNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textChasisNo.Location = new System.Drawing.Point(80, 81);
            this.textChasisNo.Name = "textChasisNo";
            this.textChasisNo.Size = new System.Drawing.Size(273, 20);
            this.textChasisNo.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Engine No:";
            // 
            // textEngineNo
            // 
            this.textEngineNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEngineNo.Location = new System.Drawing.Point(80, 107);
            this.textEngineNo.Name = "textEngineNo";
            this.textEngineNo.Size = new System.Drawing.Size(273, 20);
            this.textEngineNo.TabIndex = 5;
            // 
            // Property_Vehicle_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.textEngineNo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textChasisNo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textPlateNo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textModel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textMake);
            this.Controls.Add(this.label1);
            this.Name = "Property_Vehicle_Form";
            this.Size = new System.Drawing.Size(356, 139);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textPlateNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textModel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textMake;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textChasisNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textEngineNo;
    }
}
