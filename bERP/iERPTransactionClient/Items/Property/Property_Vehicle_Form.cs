﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERP.Client
{
    [PropertyForm(dataType = typeof(Property_Vehicle))]
    public partial class Property_Vehicle_Form : PropertyFormBase
    {
        public Property_Vehicle_Form()
        {
            InitializeComponent();
        }
        public override void setData(object _prop)
        {
            Property_Vehicle prop = _prop as Property_Vehicle;
            if (prop == null)
            {
                this.textMake.Clear();
                this.textModel.Clear();
                this.textPlateNo.Clear();
                this.textChasisNo.Clear();
                this.textEngineNo.Clear();
            }
            else
            {
                this.textMake.Text = prop.make;
                this.textModel.Text = prop.model;
                this.textPlateNo.Text = prop.plateNo;
                this.textChasisNo.Text = prop.chasisNo;
                this.textEngineNo.Text = prop.engineNo;
            }

        }
        public override string validate()
        {
            return null;
        }

        public override object getData()
        {
            Property_Vehicle ret = new Property_Vehicle();
            ret.make = this.textMake.Text;
            ret.model = this.textModel.Text;
            ret.plateNo = this.textPlateNo.Text;
            ret.chasisNo = this.textChasisNo.Text;
            ret.engineNo = this.textEngineNo.Text;
            return ret;
        }
    }
}
