﻿namespace BIZNET.iERP.Client
{
    partial class RegisterItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegisterItem));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.buttonShowLedger = new DevExpress.XtraEditors.SimpleButton();
            this.buttonEditProperty = new DevExpress.XtraEditors.SimpleButton();
            this.buttonDeleteProperty = new DevExpress.XtraEditors.SimpleButton();
            this.gridManager = new DevExpress.XtraGrid.GridControl();
            this.gridViewManager = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txtItemCode = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.chkIsInventory = new DevExpress.XtraEditors.CheckEdit();
            this.chkFixedAssetItem = new DevExpress.XtraEditors.CheckEdit();
            this.chkSalesItem = new DevExpress.XtraEditors.CheckEdit();
            this.chkExpenseItem = new DevExpress.XtraEditors.CheckEdit();
            this.cmbDepreciationType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbGoodType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtUnitPrice = new DevExpress.XtraEditors.TextEdit();
            this.chkHasFixedUnitPrice = new DevExpress.XtraEditors.CheckEdit();
            this.cmbMeasureUnits = new BIZNET.iERP.Client.MeasureUnitSelector();
            this.gridAccountInfo = new DevExpress.XtraGrid.GridControl();
            this.gridViewAccountInfo = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.cmbInventoryType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbExpenseType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbServiceType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbItemType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbTaxStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbExpenseKind = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtDescription = new DevExpress.XtraEditors.MemoEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.buttonShowStockCard = new DevExpress.XtraEditors.SimpleButton();
            this.buttonAddProperty = new DevExpress.XtraEditors.SimpleButton();
            this.buttonConversionFactors = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutGeneralInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTaxStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutServiceType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMeasureUnit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutInventoryType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutExpenseKind = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutExpenseType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutGoodType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutFixedUnitPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutUnitPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutDepreciationType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAccountInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabProperties = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.validationExpenseItem = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsInventory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFixedAssetItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpenseItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDepreciationType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbGoodType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHasFixedUnitPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMeasureUnits.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAccountInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAccountInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbInventoryType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbExpenseType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbServiceType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbItemType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTaxStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbExpenseKind.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGeneralInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTaxStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServiceType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMeasureUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInventoryType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExpenseKind)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExpenseType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGoodType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFixedUnitPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUnitPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDepreciationType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAccountInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationExpenseItem)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.panelControl3);
            this.layoutControl1.Controls.Add(this.gridManager);
            this.layoutControl1.Controls.Add(this.txtItemCode);
            this.layoutControl1.Controls.Add(this.panelControl2);
            this.layoutControl1.Controls.Add(this.cmbDepreciationType);
            this.layoutControl1.Controls.Add(this.cmbGoodType);
            this.layoutControl1.Controls.Add(this.txtUnitPrice);
            this.layoutControl1.Controls.Add(this.chkHasFixedUnitPrice);
            this.layoutControl1.Controls.Add(this.cmbMeasureUnits);
            this.layoutControl1.Controls.Add(this.gridAccountInfo);
            this.layoutControl1.Controls.Add(this.cmbInventoryType);
            this.layoutControl1.Controls.Add(this.cmbExpenseType);
            this.layoutControl1.Controls.Add(this.cmbServiceType);
            this.layoutControl1.Controls.Add(this.cmbItemType);
            this.layoutControl1.Controls.Add(this.cmbTaxStatus);
            this.layoutControl1.Controls.Add(this.cmbExpenseKind);
            this.layoutControl1.Controls.Add(this.txtDescription);
            this.layoutControl1.Controls.Add(this.txtName);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(731, 148, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(704, 637);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.buttonShowLedger);
            this.panelControl3.Controls.Add(this.buttonEditProperty);
            this.panelControl3.Controls.Add(this.buttonDeleteProperty);
            this.panelControl3.Location = new System.Drawing.Point(11, 52);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(682, 44);
            this.panelControl3.TabIndex = 1;
            // 
            // buttonShowLedger
            // 
            this.buttonShowLedger.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonShowLedger.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonShowLedger.Appearance.Options.UseFont = true;
            this.buttonShowLedger.Enabled = false;
            this.buttonShowLedger.Location = new System.Drawing.Point(3, 12);
            this.buttonShowLedger.Name = "buttonShowLedger";
            this.buttonShowLedger.Size = new System.Drawing.Size(144, 29);
            this.buttonShowLedger.TabIndex = 2;
            this.buttonShowLedger.Text = "Show Property Ledger";
            this.buttonShowLedger.Click += new System.EventHandler(this.buttonShowLedger_Click);
            // 
            // buttonEditProperty
            // 
            this.buttonEditProperty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEditProperty.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEditProperty.Appearance.Options.UseFont = true;
            this.buttonEditProperty.Enabled = false;
            this.buttonEditProperty.Location = new System.Drawing.Point(480, 13);
            this.buttonEditProperty.Name = "buttonEditProperty";
            this.buttonEditProperty.Size = new System.Drawing.Size(92, 29);
            this.buttonEditProperty.TabIndex = 2;
            this.buttonEditProperty.Text = "Edit";
            this.buttonEditProperty.Click += new System.EventHandler(this.buttonEditProperty_Click);
            // 
            // buttonDeleteProperty
            // 
            this.buttonDeleteProperty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDeleteProperty.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDeleteProperty.Appearance.Options.UseFont = true;
            this.buttonDeleteProperty.Enabled = false;
            this.buttonDeleteProperty.Location = new System.Drawing.Point(578, 12);
            this.buttonDeleteProperty.Name = "buttonDeleteProperty";
            this.buttonDeleteProperty.Size = new System.Drawing.Size(92, 29);
            this.buttonDeleteProperty.TabIndex = 2;
            this.buttonDeleteProperty.Text = "Delete";
            this.buttonDeleteProperty.Click += new System.EventHandler(this.buttonDeleteProperty_Click);
            // 
            // gridManager
            // 
            this.gridManager.Location = new System.Drawing.Point(11, 100);
            this.gridManager.MainView = this.gridViewManager;
            this.gridManager.Name = "gridManager";
            this.gridManager.Size = new System.Drawing.Size(682, 469);
            this.gridManager.TabIndex = 2;
            this.gridManager.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewManager});
            this.gridManager.Click += new System.EventHandler(this.gridManager_Click);
            // 
            // gridViewManager
            // 
            this.gridViewManager.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightBlue;
            this.gridViewManager.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewManager.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewManager.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Indigo;
            this.gridViewManager.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewManager.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewManager.Appearance.SelectedRow.BackColor = System.Drawing.Color.SteelBlue;
            this.gridViewManager.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridViewManager.GridControl = this.gridManager;
            this.gridViewManager.GroupPanelText = "Items";
            this.gridViewManager.Name = "gridViewManager";
            this.gridViewManager.OptionsBehavior.Editable = false;
            this.gridViewManager.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewManager.OptionsCustomization.AllowFilter = false;
            this.gridViewManager.OptionsCustomization.AllowGroup = false;
            this.gridViewManager.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewManager.OptionsCustomization.AllowRowSizing = true;
            this.gridViewManager.OptionsCustomization.AllowSort = false;
            this.gridViewManager.OptionsView.ShowGroupPanel = false;
            this.gridViewManager.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewManager_FocusedRowChanged);
            // 
            // txtItemCode
            // 
            this.txtItemCode.Location = new System.Drawing.Point(137, 89);
            this.txtItemCode.Name = "txtItemCode";
            this.txtItemCode.Properties.ReadOnly = true;
            this.txtItemCode.Size = new System.Drawing.Size(553, 20);
            this.txtItemCode.StyleController = this.layoutControl1;
            this.txtItemCode.TabIndex = 20;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.chkIsInventory);
            this.panelControl2.Controls.Add(this.chkFixedAssetItem);
            this.panelControl2.Controls.Add(this.chkSalesItem);
            this.panelControl2.Controls.Add(this.chkExpenseItem);
            this.panelControl2.Location = new System.Drawing.Point(11, 52);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(682, 30);
            this.panelControl2.TabIndex = 19;
            // 
            // chkIsInventory
            // 
            this.chkIsInventory.Location = new System.Drawing.Point(349, 5);
            this.chkIsInventory.Name = "chkIsInventory";
            this.chkIsInventory.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIsInventory.Properties.Appearance.Options.UseFont = true;
            this.chkIsInventory.Properties.Caption = "Inventory Item";
            this.chkIsInventory.Size = new System.Drawing.Size(109, 21);
            this.chkIsInventory.TabIndex = 0;
            this.chkIsInventory.CheckedChanged += new System.EventHandler(this.chkInventoryItem_CheckedChanged);
            // 
            // chkFixedAssetItem
            // 
            this.chkFixedAssetItem.Location = new System.Drawing.Point(215, 5);
            this.chkFixedAssetItem.Name = "chkFixedAssetItem";
            this.chkFixedAssetItem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFixedAssetItem.Properties.Appearance.Options.UseFont = true;
            this.chkFixedAssetItem.Properties.Caption = "Fixed Asset Item";
            this.chkFixedAssetItem.Size = new System.Drawing.Size(128, 21);
            this.chkFixedAssetItem.TabIndex = 0;
            this.chkFixedAssetItem.CheckedChanged += new System.EventHandler(this.chkFixedAssetItem_CheckedChanged);
            // 
            // chkSalesItem
            // 
            this.chkSalesItem.Location = new System.Drawing.Point(115, 5);
            this.chkSalesItem.Name = "chkSalesItem";
            this.chkSalesItem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalesItem.Properties.Appearance.Options.UseFont = true;
            this.chkSalesItem.Properties.Caption = "Sales Item";
            this.chkSalesItem.Size = new System.Drawing.Size(94, 21);
            this.chkSalesItem.TabIndex = 0;
            this.chkSalesItem.CheckedChanged += new System.EventHandler(this.chkSalesItem_CheckedChanged);
            // 
            // chkExpenseItem
            // 
            this.chkExpenseItem.Location = new System.Drawing.Point(5, 5);
            this.chkExpenseItem.Name = "chkExpenseItem";
            this.chkExpenseItem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkExpenseItem.Properties.Appearance.Options.UseFont = true;
            this.chkExpenseItem.Properties.Caption = "Expense Item";
            this.chkExpenseItem.Size = new System.Drawing.Size(104, 21);
            this.chkExpenseItem.TabIndex = 0;
            this.chkExpenseItem.CheckedChanged += new System.EventHandler(this.chkExpenseItem_CheckedChanged);
            // 
            // cmbDepreciationType
            // 
            this.cmbDepreciationType.EditValue = "Building";
            this.cmbDepreciationType.Location = new System.Drawing.Point(137, 389);
            this.cmbDepreciationType.Name = "cmbDepreciationType";
            this.cmbDepreciationType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDepreciationType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbDepreciationType.Size = new System.Drawing.Size(553, 20);
            this.cmbDepreciationType.StyleController = this.layoutControl1;
            this.cmbDepreciationType.TabIndex = 18;
            // 
            // cmbGoodType
            // 
            this.cmbGoodType.EditValue = "None";
            this.cmbGoodType.Location = new System.Drawing.Point(137, 299);
            this.cmbGoodType.Name = "cmbGoodType";
            this.cmbGoodType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbGoodType.Properties.Items.AddRange(new object[] {
            "None",
            "Rawhide"});
            this.cmbGoodType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbGoodType.Size = new System.Drawing.Size(553, 20);
            this.cmbGoodType.StyleController = this.layoutControl1;
            this.cmbGoodType.TabIndex = 17;
            // 
            // txtUnitPrice
            // 
            this.txtUnitPrice.Location = new System.Drawing.Point(479, 239);
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtUnitPrice.Properties.Mask.EditMask = "#,########0.00;";
            this.txtUnitPrice.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtUnitPrice.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtUnitPrice.Size = new System.Drawing.Size(211, 20);
            this.txtUnitPrice.StyleController = this.layoutControl1;
            this.txtUnitPrice.TabIndex = 16;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "Please enter unit price";
            this.validationExpenseItem.SetValidationRule(this.txtUnitPrice, conditionValidationRule1);
            // 
            // chkHasFixedUnitPrice
            // 
            this.chkHasFixedUnitPrice.Location = new System.Drawing.Point(137, 239);
            this.chkHasFixedUnitPrice.Name = "chkHasFixedUnitPrice";
            this.chkHasFixedUnitPrice.Properties.Caption = "";
            this.chkHasFixedUnitPrice.Size = new System.Drawing.Size(209, 19);
            this.chkHasFixedUnitPrice.StyleController = this.layoutControl1;
            this.chkHasFixedUnitPrice.TabIndex = 15;
            this.chkHasFixedUnitPrice.CheckedChanged += new System.EventHandler(this.chkHasFixedUnitPrice_CheckedChanged);
            // 
            // cmbMeasureUnits
            // 
            this.cmbMeasureUnits.Location = new System.Drawing.Point(137, 359);
            this.cmbMeasureUnits.Name = "cmbMeasureUnits";
            this.cmbMeasureUnits.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbMeasureUnits.Size = new System.Drawing.Size(553, 20);
            this.cmbMeasureUnits.StyleController = this.layoutControl1;
            this.cmbMeasureUnits.TabIndex = 14;
            // 
            // gridAccountInfo
            // 
            this.gridAccountInfo.Location = new System.Drawing.Point(11, 52);
            this.gridAccountInfo.MainView = this.gridViewAccountInfo;
            this.gridAccountInfo.Name = "gridAccountInfo";
            this.gridAccountInfo.Size = new System.Drawing.Size(682, 517);
            this.gridAccountInfo.TabIndex = 13;
            this.gridAccountInfo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAccountInfo});
            // 
            // gridViewAccountInfo
            // 
            this.gridViewAccountInfo.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewAccountInfo.Appearance.GroupPanel.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.gridViewAccountInfo.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewAccountInfo.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewAccountInfo.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewAccountInfo.Appearance.GroupRow.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.gridViewAccountInfo.Appearance.GroupRow.Options.UseFont = true;
            this.gridViewAccountInfo.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridViewAccountInfo.GridControl = this.gridAccountInfo;
            this.gridViewAccountInfo.GroupPanelText = "Account Information";
            this.gridViewAccountInfo.Name = "gridViewAccountInfo";
            this.gridViewAccountInfo.OptionsBehavior.Editable = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowFilter = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowGroup = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowSort = false;
            this.gridViewAccountInfo.OptionsFind.FindDelay = 100;
            this.gridViewAccountInfo.OptionsFind.FindMode = DevExpress.XtraEditors.FindMode.Always;
            this.gridViewAccountInfo.OptionsMenu.EnableColumnMenu = false;
            this.gridViewAccountInfo.OptionsMenu.EnableFooterMenu = false;
            this.gridViewAccountInfo.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewAccountInfo.OptionsView.ShowGroupPanel = false;
            // 
            // cmbInventoryType
            // 
            this.cmbInventoryType.EditValue = "General Inventory";
            this.cmbInventoryType.Location = new System.Drawing.Point(137, 419);
            this.cmbInventoryType.Name = "cmbInventoryType";
            this.cmbInventoryType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbInventoryType.Properties.Items.AddRange(new object[] {
            "General Inventory",
            "Fiinish Goods Inventory"});
            this.cmbInventoryType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbInventoryType.Size = new System.Drawing.Size(553, 20);
            this.cmbInventoryType.StyleController = this.layoutControl1;
            this.cmbInventoryType.TabIndex = 6;
            // 
            // cmbExpenseType
            // 
            this.cmbExpenseType.EditValue = "Value of Local Purchases Input";
            this.cmbExpenseType.Location = new System.Drawing.Point(137, 179);
            this.cmbExpenseType.Name = "cmbExpenseType";
            this.cmbExpenseType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbExpenseType.Properties.Items.AddRange(new object[] {
            "Value of Local Purchases Input",
            "Value of General Expense Inputs",
            "Unclaimed Inputs",
            "Do Not Declare in VAT Form (Do Not Report to ERCA)"});
            this.cmbExpenseType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbExpenseType.Size = new System.Drawing.Size(553, 20);
            this.cmbExpenseType.StyleController = this.layoutControl1;
            this.cmbExpenseType.TabIndex = 7;
            // 
            // cmbServiceType
            // 
            this.cmbServiceType.EditValue = "Others";
            this.cmbServiceType.Location = new System.Drawing.Point(137, 329);
            this.cmbServiceType.Name = "cmbServiceType";
            this.cmbServiceType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbServiceType.Properties.Items.AddRange(new object[] {
            "Others",
            " Contractors",
            " Mill Services",
            " Tractors and Combined Harvesters"});
            this.cmbServiceType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbServiceType.Size = new System.Drawing.Size(553, 20);
            this.cmbServiceType.StyleController = this.layoutControl1;
            this.cmbServiceType.TabIndex = 8;
            // 
            // cmbItemType
            // 
            this.cmbItemType.EditValue = "Good";
            this.cmbItemType.Location = new System.Drawing.Point(137, 269);
            this.cmbItemType.Name = "cmbItemType";
            this.cmbItemType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbItemType.Properties.Items.AddRange(new object[] {
            "Good",
            "Service"});
            this.cmbItemType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbItemType.Size = new System.Drawing.Size(553, 20);
            this.cmbItemType.StyleController = this.layoutControl1;
            this.cmbItemType.TabIndex = 4;
            this.cmbItemType.SelectedIndexChanged += new System.EventHandler(this.cmbItemType_SelectedIndexChanged);
            // 
            // cmbTaxStatus
            // 
            this.cmbTaxStatus.EditValue = "Taxable";
            this.cmbTaxStatus.Location = new System.Drawing.Point(137, 209);
            this.cmbTaxStatus.Name = "cmbTaxStatus";
            this.cmbTaxStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTaxStatus.Properties.Items.AddRange(new object[] {
            "Taxable",
            "Non-Taxable"});
            this.cmbTaxStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbTaxStatus.Size = new System.Drawing.Size(553, 20);
            this.cmbTaxStatus.StyleController = this.layoutControl1;
            this.cmbTaxStatus.TabIndex = 3;
            // 
            // cmbExpenseKind
            // 
            this.cmbExpenseKind.EditValue = "Not directly related to production or service";
            this.cmbExpenseKind.Location = new System.Drawing.Point(137, 149);
            this.cmbExpenseKind.Name = "cmbExpenseKind";
            this.cmbExpenseKind.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbExpenseKind.Properties.Items.AddRange(new object[] {
            "Not directly related to production or service",
            "Directly related to production or service"});
            this.cmbExpenseKind.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbExpenseKind.Size = new System.Drawing.Size(553, 20);
            this.cmbExpenseKind.StyleController = this.layoutControl1;
            this.cmbExpenseKind.TabIndex = 2;
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(137, 449);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(553, 117);
            this.txtDescription.StyleController = this.layoutControl1;
            this.txtDescription.TabIndex = 9;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(137, 119);
            this.txtName.Name = "txtName";
            this.txtName.Properties.Mask.EditMask = "[0-9a-zA-Z].*";
            this.txtName.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtName.Size = new System.Drawing.Size(553, 20);
            this.txtName.StyleController = this.layoutControl1;
            this.txtName.TabIndex = 1;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "TradeRelation name cannot be empty";
            this.validationExpenseItem.SetValidationRule(this.txtName, conditionValidationRule2);
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Controls.Add(this.btnCancel);
            this.panelControl1.Controls.Add(this.btnOk);
            this.panelControl1.Location = new System.Drawing.Point(5, 579);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(694, 53);
            this.panelControl1.TabIndex = 3;
            // 
            // buttonShowStockCard
            // 
            this.buttonShowStockCard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonShowStockCard.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonShowStockCard.Appearance.Options.UseFont = true;
            this.buttonShowStockCard.Location = new System.Drawing.Point(236, 21);
            this.buttonShowStockCard.Name = "buttonShowStockCard";
            this.buttonShowStockCard.Size = new System.Drawing.Size(144, 29);
            this.buttonShowStockCard.TabIndex = 5;
            this.buttonShowStockCard.Text = "Show Stock Card";
            this.buttonShowStockCard.Click += new System.EventHandler(this.buttonShowStockCard_Click);
            // 
            // buttonAddProperty
            // 
            this.buttonAddProperty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonAddProperty.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddProperty.Appearance.Options.UseFont = true;
            this.buttonAddProperty.Location = new System.Drawing.Point(138, 21);
            this.buttonAddProperty.Name = "buttonAddProperty";
            this.buttonAddProperty.Size = new System.Drawing.Size(92, 29);
            this.buttonAddProperty.TabIndex = 4;
            this.buttonAddProperty.Text = "Add Property";
            this.buttonAddProperty.Visible = false;
            this.buttonAddProperty.Click += new System.EventHandler(this.buttonAddProperty_Click);
            // 
            // buttonConversionFactors
            // 
            this.buttonConversionFactors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonConversionFactors.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonConversionFactors.Appearance.Options.UseFont = true;
            this.buttonConversionFactors.Location = new System.Drawing.Point(3, 21);
            this.buttonConversionFactors.Name = "buttonConversionFactors";
            this.buttonConversionFactors.Size = new System.Drawing.Size(129, 29);
            this.buttonConversionFactors.TabIndex = 3;
            this.buttonConversionFactors.Text = "&Conversion Factors";
            this.buttonConversionFactors.Click += new System.EventHandler(this.buttonConversionFactors_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(614, 15);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 29);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Close";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Appearance.Options.UseFont = true;
            this.btnOk.Location = new System.Drawing.Point(533, 15);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 29);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "&Register";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1,
            this.layoutControlItem8});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(704, 637);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.tabbedControlGroup1.SelectedTabPage = this.layoutGeneralInfo;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(698, 574);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGeneralInfo,
            this.layoutAccountInfo,
            this.tabProperties});
            this.tabbedControlGroup1.Text = "tabbedControlGroup1";
            // 
            // layoutGeneralInfo
            // 
            this.layoutGeneralInfo.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutGeneralInfo.AppearanceGroup.Options.UseFont = true;
            this.layoutGeneralInfo.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutGeneralInfo.AppearanceItemCaption.Options.UseFont = true;
            this.layoutGeneralInfo.AppearanceTabPage.Header.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutGeneralInfo.AppearanceTabPage.Header.Options.UseFont = true;
            this.layoutGeneralInfo.CaptionImage = global::BIZNET.iERP.Client.Properties.Resources.expense_info;
            this.layoutGeneralInfo.CustomizationFormText = "layoutControlGroup3";
            this.layoutGeneralInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutName,
            this.layoutTaxStatus,
            this.layoutItemType,
            this.layoutDescription,
            this.layoutControlItem13,
            this.layoutServiceType,
            this.layoutMeasureUnit,
            this.layoutInventoryType,
            this.layoutExpenseKind,
            this.layoutExpenseType,
            this.layoutGoodType,
            this.layoutFixedUnitPrice,
            this.layoutUnitPrice,
            this.layoutDepreciationType,
            this.layoutItemCode});
            this.layoutGeneralInfo.Location = new System.Drawing.Point(0, 0);
            this.layoutGeneralInfo.Name = "layoutGeneralInfo";
            this.layoutGeneralInfo.Size = new System.Drawing.Size(686, 521);
            this.layoutGeneralInfo.Text = "General Information";
            // 
            // layoutName
            // 
            this.layoutName.Control = this.txtName;
            this.layoutName.CustomizationFormText = "TradeRelation Name:";
            this.layoutName.Location = new System.Drawing.Point(0, 64);
            this.layoutName.Name = "layoutName";
            this.layoutName.Size = new System.Drawing.Size(686, 30);
            this.layoutName.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutName.Text = "Name:";
            this.layoutName.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutTaxStatus
            // 
            this.layoutTaxStatus.Control = this.cmbTaxStatus;
            this.layoutTaxStatus.CustomizationFormText = "Tax Status:";
            this.layoutTaxStatus.Location = new System.Drawing.Point(0, 154);
            this.layoutTaxStatus.Name = "layoutTaxStatus";
            this.layoutTaxStatus.Size = new System.Drawing.Size(686, 30);
            this.layoutTaxStatus.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutTaxStatus.Text = "Tax Status:";
            this.layoutTaxStatus.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutItemType
            // 
            this.layoutItemType.Control = this.cmbItemType;
            this.layoutItemType.CustomizationFormText = "Item Type:";
            this.layoutItemType.Location = new System.Drawing.Point(0, 214);
            this.layoutItemType.Name = "layoutItemType";
            this.layoutItemType.Size = new System.Drawing.Size(686, 30);
            this.layoutItemType.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutItemType.Text = "Item Type:";
            this.layoutItemType.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutDescription
            // 
            this.layoutDescription.Control = this.txtDescription;
            this.layoutDescription.CustomizationFormText = "Description:";
            this.layoutDescription.Location = new System.Drawing.Point(0, 394);
            this.layoutDescription.MinSize = new System.Drawing.Size(113, 26);
            this.layoutDescription.Name = "layoutDescription";
            this.layoutDescription.Size = new System.Drawing.Size(686, 127);
            this.layoutDescription.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutDescription.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutDescription.Text = "Description:";
            this.layoutDescription.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.panelControl2;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(0, 34);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(104, 34);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(686, 34);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutServiceType
            // 
            this.layoutServiceType.Control = this.cmbServiceType;
            this.layoutServiceType.CustomizationFormText = "Service Type:";
            this.layoutServiceType.Location = new System.Drawing.Point(0, 274);
            this.layoutServiceType.Name = "layoutServiceType";
            this.layoutServiceType.Size = new System.Drawing.Size(686, 30);
            this.layoutServiceType.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutServiceType.Text = "Service Type:";
            this.layoutServiceType.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutMeasureUnit
            // 
            this.layoutMeasureUnit.Control = this.cmbMeasureUnits;
            this.layoutMeasureUnit.CustomizationFormText = "Measure Units:";
            this.layoutMeasureUnit.Location = new System.Drawing.Point(0, 304);
            this.layoutMeasureUnit.Name = "layoutMeasureUnit";
            this.layoutMeasureUnit.Size = new System.Drawing.Size(686, 30);
            this.layoutMeasureUnit.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutMeasureUnit.Text = "Measure Units:";
            this.layoutMeasureUnit.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutInventoryType
            // 
            this.layoutInventoryType.Control = this.cmbInventoryType;
            this.layoutInventoryType.CustomizationFormText = "Inventory Type:";
            this.layoutInventoryType.Location = new System.Drawing.Point(0, 364);
            this.layoutInventoryType.Name = "layoutInventoryType";
            this.layoutInventoryType.Size = new System.Drawing.Size(686, 30);
            this.layoutInventoryType.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutInventoryType.Text = "Inventory Type:";
            this.layoutInventoryType.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutExpenseKind
            // 
            this.layoutExpenseKind.Control = this.cmbExpenseKind;
            this.layoutExpenseKind.CustomizationFormText = "Category:";
            this.layoutExpenseKind.Location = new System.Drawing.Point(0, 94);
            this.layoutExpenseKind.Name = "layoutExpenseKind";
            this.layoutExpenseKind.Size = new System.Drawing.Size(686, 30);
            this.layoutExpenseKind.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutExpenseKind.Text = "Expense Type:";
            this.layoutExpenseKind.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutExpenseType
            // 
            this.layoutExpenseType.Control = this.cmbExpenseType;
            this.layoutExpenseType.CustomizationFormText = "Expense Type:";
            this.layoutExpenseType.Location = new System.Drawing.Point(0, 124);
            this.layoutExpenseType.Name = "layoutExpenseType";
            this.layoutExpenseType.Size = new System.Drawing.Size(686, 30);
            this.layoutExpenseType.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutExpenseType.Text = "Declare in VAT Form As:";
            this.layoutExpenseType.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutGoodType
            // 
            this.layoutGoodType.Control = this.cmbGoodType;
            this.layoutGoodType.CustomizationFormText = "Good Type:";
            this.layoutGoodType.Location = new System.Drawing.Point(0, 244);
            this.layoutGoodType.Name = "layoutGoodType";
            this.layoutGoodType.Size = new System.Drawing.Size(686, 30);
            this.layoutGoodType.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutGoodType.Text = "Good Type:";
            this.layoutGoodType.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutFixedUnitPrice
            // 
            this.layoutFixedUnitPrice.Control = this.chkHasFixedUnitPrice;
            this.layoutFixedUnitPrice.CustomizationFormText = "Item has fixed unit price:";
            this.layoutFixedUnitPrice.Location = new System.Drawing.Point(0, 184);
            this.layoutFixedUnitPrice.Name = "layoutFixedUnitPrice";
            this.layoutFixedUnitPrice.Size = new System.Drawing.Size(342, 30);
            this.layoutFixedUnitPrice.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutFixedUnitPrice.Text = "Item has fixed unit price:";
            this.layoutFixedUnitPrice.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutUnitPrice
            // 
            this.layoutUnitPrice.Control = this.txtUnitPrice;
            this.layoutUnitPrice.CustomizationFormText = "Unit Price:";
            this.layoutUnitPrice.Location = new System.Drawing.Point(342, 184);
            this.layoutUnitPrice.Name = "layoutUnitPrice";
            this.layoutUnitPrice.Size = new System.Drawing.Size(344, 30);
            this.layoutUnitPrice.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutUnitPrice.Text = "Unit Price:";
            this.layoutUnitPrice.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutDepreciationType
            // 
            this.layoutDepreciationType.Control = this.cmbDepreciationType;
            this.layoutDepreciationType.CustomizationFormText = "Depreciation Type:";
            this.layoutDepreciationType.Location = new System.Drawing.Point(0, 334);
            this.layoutDepreciationType.Name = "layoutDepreciationType";
            this.layoutDepreciationType.Size = new System.Drawing.Size(686, 30);
            this.layoutDepreciationType.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutDepreciationType.Text = "Depreciation Type:";
            this.layoutDepreciationType.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutItemCode
            // 
            this.layoutItemCode.Control = this.txtItemCode;
            this.layoutItemCode.CustomizationFormText = "Code:";
            this.layoutItemCode.Location = new System.Drawing.Point(0, 34);
            this.layoutItemCode.Name = "layoutItemCode";
            this.layoutItemCode.Size = new System.Drawing.Size(686, 30);
            this.layoutItemCode.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutItemCode.Text = "Code:";
            this.layoutItemCode.TextSize = new System.Drawing.Size(120, 13);
            // 
            // layoutAccountInfo
            // 
            this.layoutAccountInfo.AppearanceTabPage.Header.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutAccountInfo.AppearanceTabPage.Header.Options.UseFont = true;
            this.layoutAccountInfo.CaptionImage = global::BIZNET.iERP.Client.Properties.Resources.accounting_info;
            this.layoutAccountInfo.CustomizationFormText = "layoutControlGroup4";
            this.layoutAccountInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutAccountInfo.Location = new System.Drawing.Point(0, 0);
            this.layoutAccountInfo.Name = "layoutAccountInfo";
            this.layoutAccountInfo.Size = new System.Drawing.Size(686, 521);
            this.layoutAccountInfo.Text = "Account Information";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridAccountInfo;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(686, 521);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // tabProperties
            // 
            this.tabProperties.CaptionImage = global::BIZNET.iERP.Client.Properties.Resources.property;
            this.tabProperties.CustomizationFormText = "Properties";
            this.tabProperties.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.tabProperties.Location = new System.Drawing.Point(0, 0);
            this.tabProperties.Name = "layoutControlGroup2";
            this.tabProperties.Size = new System.Drawing.Size(686, 521);
            this.tabProperties.Text = "Properties";
            this.tabProperties.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridManager;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(686, 473);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.panelControl3;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(686, 48);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.panelControl1;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 574);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(104, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(698, 57);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // validationExpenseItem
            // 
            this.validationExpenseItem.ValidateHiddenControls = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.buttonConversionFactors);
            this.flowLayoutPanel1.Controls.Add(this.buttonAddProperty);
            this.flowLayoutPanel1.Controls.Add(this.buttonShowStockCard);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(413, 53);
            this.flowLayoutPanel1.TabIndex = 6;
            // 
            // RegisterItem
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(704, 637);
            this.Controls.Add(this.layoutControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "RegisterItem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Register Item";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkIsInventory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFixedAssetItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpenseItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDepreciationType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbGoodType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHasFixedUnitPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMeasureUnits.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAccountInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAccountInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbInventoryType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbExpenseType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbServiceType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbItemType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTaxStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbExpenseKind.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGeneralInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTaxStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutServiceType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMeasureUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInventoryType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExpenseKind)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExpenseType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGoodType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFixedUnitPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutUnitPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDepreciationType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAccountInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationExpenseItem)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.MemoEdit txtDescription;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validationExpenseItem;
        private DevExpress.XtraGrid.GridControl gridAccountInfo;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewAccountInfo;
        private DevExpress.XtraEditors.ComboBoxEdit cmbExpenseKind;
        private DevExpress.XtraEditors.ComboBoxEdit cmbTaxStatus;
        private DevExpress.XtraEditors.ComboBoxEdit cmbItemType;
        private DevExpress.XtraEditors.ComboBoxEdit cmbServiceType;
        private DevExpress.XtraEditors.ComboBoxEdit cmbExpenseType;
        private DevExpress.XtraEditors.ComboBoxEdit cmbInventoryType;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGeneralInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutName;
        private DevExpress.XtraLayout.LayoutControlItem layoutExpenseKind;
        private DevExpress.XtraLayout.LayoutControlItem layoutTaxStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemType;
        private DevExpress.XtraLayout.LayoutControlItem layoutInventoryType;
        private DevExpress.XtraLayout.LayoutControlItem layoutExpenseType;
        private DevExpress.XtraLayout.LayoutControlItem layoutServiceType;
        private DevExpress.XtraLayout.LayoutControlItem layoutDescription;
        private DevExpress.XtraLayout.LayoutControlGroup layoutAccountInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private MeasureUnitSelector cmbMeasureUnits;
        private DevExpress.XtraLayout.LayoutControlItem layoutMeasureUnit;
        private DevExpress.XtraEditors.TextEdit txtUnitPrice;
        private DevExpress.XtraEditors.CheckEdit chkHasFixedUnitPrice;
        private DevExpress.XtraLayout.LayoutControlItem layoutFixedUnitPrice;
        private DevExpress.XtraLayout.LayoutControlItem layoutUnitPrice;
        private DevExpress.XtraEditors.ComboBoxEdit cmbGoodType;
        private DevExpress.XtraLayout.LayoutControlItem layoutGoodType;
        private DevExpress.XtraEditors.ComboBoxEdit cmbDepreciationType;
        private DevExpress.XtraLayout.LayoutControlItem layoutDepreciationType;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.CheckEdit chkIsInventory;
        private DevExpress.XtraEditors.CheckEdit chkFixedAssetItem;
        private DevExpress.XtraEditors.CheckEdit chkSalesItem;
        private DevExpress.XtraEditors.CheckEdit chkExpenseItem;
        private DevExpress.XtraEditors.TextEdit txtItemCode;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemCode;
        private DevExpress.XtraEditors.SimpleButton buttonConversionFactors;
        private DevExpress.XtraLayout.LayoutControlGroup tabProperties;
        private DevExpress.XtraGrid.GridControl gridManager;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewManager;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton buttonEditProperty;
        private DevExpress.XtraEditors.SimpleButton buttonDeleteProperty;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SimpleButton buttonAddProperty;
        private DevExpress.XtraEditors.SimpleButton buttonShowLedger;
        private DevExpress.XtraEditors.SimpleButton buttonShowStockCard;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}