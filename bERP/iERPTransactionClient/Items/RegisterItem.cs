﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.IO;
using System.Drawing.Imaging;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraGrid;
using DevExpress.Utils;
using INTAPS.UI.ButtonGrid;
using INTAPS;
namespace BIZNET.iERP.Client
{
    public partial class RegisterItem : XtraForm
    {
        private string m_ItemCode;
        private DataTable accountInfoTable;
        private TransactionItems m_transactionItem;
        public delegate void ItemAddedHandler(string itemCode);
        public event ItemAddedHandler ItemAdded;
        private int m_CategoryID;
        private bool m_manualItemCode = false;
        private DataTable tableItems;
        public RegisterItem(int categoryID)
        {
            InitializeComponent();
            object manual = iERPTransactionClient.GetSystemParamter("manualItemCode");
            if (manual is DBNull)
                MessageBox.ShowErrorMessage("Error: Please Configure manualItemCode setting in system parameters.");
            else
                m_manualItemCode = (bool)manual;
                
            txtItemCode.Properties.ReadOnly = false;
            
            m_CategoryID = categoryID;
            chkExpenseItem.Checked = true;
            layoutServiceType.HideToCustomization();
            layoutInventoryType.HideToCustomization();
            layoutAccountInfo.HideToCustomization();
            txtName.LostFocus += Control_LostFocus;
            cmbDepreciationType.Properties.Items.AddRange(iERPTransactionClient.getAllFixedAssetRuleAttributes());
            buttonConversionFactors.Visible = false;
            buttonShowStockCard.Visible = false;
            cmbExpenseKind.SelectedIndex = 1;
        }
        public RegisterItem(string itemCode, int categoryID)
            : this(categoryID)
        {
            m_ItemCode = itemCode;
            txtItemCode.Properties.ReadOnly = true;

           // chkExpenseItem.Checked = false;
            accountInfoTable = new DataTable();
            PrepareAccountInfoTable();
            TransactionItems item = iERPTransactionClient.GetTransactionItems(m_ItemCode);
            if (item != null)
            {
                m_transactionItem = item;
                if (m_transactionItem.IsInventoryItem)
                    cmbInventoryType.Enabled = false;
                LoadItemForUpdate(m_transactionItem);
                layoutAccountInfo.RestoreFromCustomization(layoutGeneralInfo, InsertType.Right);
                DisplayItemAccountInformation(m_transactionItem);
            }
            buttonConversionFactors.Visible = true;
            buttonShowStockCard.Visible = item.IsInventoryItem ||item.IsFixedAssetItem;
            preparePropertiesGrid();
            loadProperties(itemCode);
            buttonAddProperty.Visible = item.IsFixedAssetItem;
        }
        public string ItemCode
        {
            get { return m_ItemCode; }
        }

        void preparePropertiesGrid()
        {
            tableItems = new DataTable();
            tableItems.Columns.Add("Property Code", typeof(string));
            tableItems.Columns.Add("Property Name", typeof(string));
            tableItems.Columns.Add("Acquire Date", typeof(string));
            tableItems.Columns.Add("Custodian", typeof(string));
            tableItems.Columns.Add("itemCode", typeof(string));
            tableItems.Columns.Add("id", typeof(int));
            tableItems.Columns.Add("itemData", typeof(object));
            gridManager.DataSource = tableItems;
            gridViewManager.Columns["itemCode"].Visible = false;
            gridViewManager.Columns["id"].Visible = false;
            gridViewManager.Columns["itemData"].Visible = false;
            updatePropertyButtonsStatus();
        }
        void loadProperties(string itemCode)
        {
            PropertyItemCategory itemCat= iERPTransactionClient.getPropertyItemCategory(itemCode);
            if(itemCat==null)
                return;
            tabProperties.Visibility = LayoutVisibility.Always;
            int N;
            tableItems.Rows.Clear();
            foreach(Property p in iERPTransactionClient.getPropertiesByParentItemCode(itemCode,0,-1,out N))
            {
                TransactionItems propertyItem = iERPTransactionClient.GetTransactionItems(p.itemCode);
                string custodian = null;
                foreach (CostCenterAccount csa in AccountingClient.GetCostCenterAccountsOf<Account>(propertyItem.originalFixedAssetAccountID))
                {
                    if (csa.isControlAccount)
                        continue;
                    if (!AccountBase.AmountEqual(AccountingClient.GetBalanceAsOf(csa.id, TransactionItem.MATERIAL_QUANTITY, DateTime.Now).DebitBalance,0))
                        custodian = INTAPS.StringExtensions.AppendOperand(custodian, ", ", AccountingClient.GetAccount<CostCenter>(csa.costCenterID).NameCode);
                }
                foreach (CostCenterAccount csa in AccountingClient.GetCostCenterAccountsOf<Account>(propertyItem.inventoryAccountID))
                {
                    if (csa.isControlAccount)
                        continue;
                    if (!AccountBase.AmountEqual(AccountingClient.GetBalanceAsOf(csa.id, TransactionItem.MATERIAL_QUANTITY, DateTime.Now).DebitBalance, 0))
                        custodian = INTAPS.StringExtensions.AppendOperand(custodian, ", ", AccountingClient.GetAccount<CostCenter>(csa.costCenterID).NameCode);
                }
                
                tableItems.Rows.Add(p.propertyCode, p.name, p.acquireDate.ToString("MMM dd,yyy"),custodian, p.itemCode, p.id,propertyItem);
            }
        }
        private void PrepareAccountInfoTable()
        {
            accountInfoTable.Columns.Add("Account Code", typeof(string));
            accountInfoTable.Columns.Add("Account Type", typeof(string));
            accountInfoTable.Columns.Add("Account Name", typeof(string));
            gridAccountInfo.DataSource = accountInfoTable;
        }

        private void LoadItemForUpdate(TransactionItems item)
        {
            chkExpenseItem.Checked = item.IsExpenseItem;
            chkSalesItem.Checked = item.IsSalesItem;
            chkFixedAssetItem.Checked = item.IsFixedAssetItem;
            chkIsInventory.Checked = item.IsInventoryItem;
            cmbExpenseKind.Enabled = false;
            txtName.Text = item.Name;
            txtItemCode.Text = item.Code;
            if (item.IsExpenseItem)
            {
                cmbExpenseKind.SelectedIndex = item.IsDirectCost ? 1 : 0;
                cmbExpenseType.SelectedIndex = (int)item.ExpenseType - 1;
            }
            if (item.IsFixedAssetItem)
                foreach(FixedAssetRuleAttribute a in cmbDepreciationType.Properties.Items)
                    if (a.id == item.DepreciationType)
                    {
                        cmbDepreciationType.SelectedItem = a;
                        break;
                    }
            if (item.IsSalesItem)
            {
                chkHasFixedUnitPrice.Checked = Account.AmountGreater(item.FixedUnitPrice, 0) ? true : false;
                txtUnitPrice.Text = TSConstants.FormatBirr(item.FixedUnitPrice);
                cmbGoodType.SelectedIndex = (int)item.GoodType;
            }
            cmbTaxStatus.SelectedIndex = (int)item.TaxStatus - 1;
            cmbItemType.SelectedIndex = (int)item.GoodOrService - 1;
            SetMeasureUnit(item.MeasureUnitID);
            if (item.IsInventoryItem || (item.IsFixedAssetItem && !layoutInventoryType.IsHidden))
                cmbInventoryType.SelectedIndex = (int)item.inventoryType;
            cmbServiceType.SelectedIndex = (int)item.ServiceType;
            txtDescription.Text = item.Description;
        }
        private void SetMeasureUnit(int measureUnitID)
        {
            int i = 0;
            foreach (MeasureUnit unit in cmbMeasureUnits.Properties.Items)
            {
                if (unit.ID == measureUnitID)
                {
                    cmbMeasureUnits.SelectedIndex = i;
                    break;
                }
                i++;
            }
        }
        private void DisplayItemAccountInformation(TransactionItems item)
        {
            accountInfoTable.Clear();
            foreach (System.Reflection.FieldInfo fi in typeof(TransactionItems).GetFields())
            {
                object[] atr = fi.GetCustomAttributes(typeof(AIChildOfAttribute), true);
                if (atr == null || atr.Length == 0)
                    continue;
                AIChildOfAttribute a = atr[0] as AIChildOfAttribute;
                int ac = (int)fi.GetValue(item);
                if (ac != -1)
                {
                    Account acnt=AccountingClient.GetAccount<Account>(ac);
                    if(acnt==null)
                        accountInfoTable.Rows.Add("Invalid account ID", ac.ToString(), "");
                    else
                        accountInfoTable.Rows.Add(acnt.Code, a.displayName, acnt.Name);

                }
            }
        }

        void Control_LostFocus(object sender, EventArgs e)
        {
            validationExpenseItem.Validate((Control)sender);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            save();
        }

        private void save()
        {
            try
            {
                if (!validationExpenseItem.Validate())
                {
                    MessageBox.ShowErrorMessage("Please check the errors marked in red and try again!");
                    return;
                }
                if(m_manualItemCode && string.IsNullOrEmpty(txtItemCode.Text))
                {
                    MessageBox.ShowErrorMessage("Please enter Item Code");
                    return;
                }
                if (!ValidateItemOptions())
                {
                    return;
                }
                TransactionItems transactionItem = new TransactionItems();
                if (m_transactionItem != null)
                    transactionItem = m_transactionItem;

                transactionItem.Code = m_manualItemCode ? txtItemCode.Text : m_ItemCode;
                transactionItem.IsExpenseItem = chkExpenseItem.Checked;
                transactionItem.IsSalesItem = chkSalesItem.Checked;
                transactionItem.IsFixedAssetItem = chkFixedAssetItem.Checked;
                transactionItem.categoryID = m_CategoryID;
                ItemCategory categ = iERPTransactionClient.GetItemCategory(m_CategoryID);
                transactionItem.Name = txtName.Text;
                if (chkExpenseItem.Checked)
                {
                    transactionItem.IsDirectCost = cmbExpenseKind.SelectedIndex == 0 ? false : true;
                    transactionItem.ExpenseType = (ExpenseType)cmbExpenseType.SelectedIndex + 1;
                }
                if(chkFixedAssetItem.Checked)
                    transactionItem.DepreciationType = ((FixedAssetRuleAttribute)cmbDepreciationType.SelectedItem).id;
                if (chkSalesItem.Checked)
                {
                    transactionItem.FixedUnitPrice = chkHasFixedUnitPrice.Checked ? double.Parse(txtUnitPrice.Text) : 0;
                    transactionItem.GoodType = cmbItemType.SelectedIndex == 1 ? GoodType.None : (GoodType)cmbGoodType.SelectedIndex;
                }
                transactionItem.TaxStatus = (ItemTaxStatus)cmbTaxStatus.SelectedIndex + 1;
                transactionItem.GoodOrService = (GoodOrService)cmbItemType.SelectedIndex + 1;
                if (cmbMeasureUnits.SelectedIndex == -1 && cmbMeasureUnits.Text != "")
                {
                    MeasureUnit unit = new MeasureUnit();
                    unit.ID = -1;
                    unit.Name = cmbMeasureUnits.Text;
                    int measureUnitID = iERPTransactionClient.RegisterMeasureUnit(unit);
                    transactionItem.MeasureUnitID = measureUnitID;
                }
                else
                    transactionItem.MeasureUnitID = ((MeasureUnit)cmbMeasureUnits.SelectedItem).ID;
                transactionItem.IsInventoryItem = chkIsInventory.Checked;
                if (chkIsInventory.Checked || (chkFixedAssetItem.Checked && (categ.generalInventoryAccountPID>0 || categ.finishedGoodsAccountPID>0)))
                    transactionItem.inventoryType = (InventoryType)cmbInventoryType.SelectedIndex;
                transactionItem.Description = txtDescription.Text;
                transactionItem.ServiceType = cmbItemType.SelectedIndex == 0 ? ServiceType.None : (ServiceType)cmbServiceType.SelectedIndex;
                string itemCode = iERPTransactionClient.RegisterTransactionItem(transactionItem, null);
                string message = m_ItemCode == null ? "Item successfully saved!" : "Item successfully updated";
                itemCode = m_manualItemCode ? txtItemCode.Text : itemCode;
                m_ItemCode = itemCode;
                MessageBox.ShowSuccessMessage(message);
                if (transactionItem.Code == null)
                {
                    if (ItemAdded != null)
                        ItemAdded(itemCode);
                    ResetControls();
                }
                else
                    Close();
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error occurred while trying to create item\n" + ex.Message);
            }
        }

        private bool ValidateItemOptions()
        {
            if (!chkExpenseItem.Checked && !chkSalesItem.Checked
                && !chkFixedAssetItem.Checked && !chkIsInventory.Checked)
            {
                MessageBox.ShowErrorMessage("Please choose one of the item types from the check boxes and try again!");
                return false;
            }
            if (chkIsInventory.Checked && !chkExpenseItem.Checked && !chkSalesItem.Checked)
            {
                MessageBox.ShowErrorMessage("You should select expense or sales item to go with the inventory option.!");
                return false;
            }
            return true;
        }
        private void ResetControls()
        {
            txtName.Text = "";
            txtDescription.Text = "";
            chkIsInventory.Checked = false;
            cmbExpenseKind.SelectedIndex = 0;
            cmbTaxStatus.SelectedIndex = 0;
            cmbItemType.SelectedIndex = 0;
            cmbServiceType.SelectedIndex = 0;
            cmbExpenseType.SelectedIndex = 0;
            cmbInventoryType.SelectedIndex = 0;
            cmbGoodType.SelectedIndex = 0;
            cmbDepreciationType.SelectedIndex = 0;
            chkHasFixedUnitPrice.Checked = false;
            txtUnitPrice.Text = "";
        }
        
        private void cmbItemType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbItemType.SelectedIndex == 1) //Service selected
            {
                if (layoutServiceType.IsHidden)
                {
                    layoutServiceType.RestoreFromCustomization(layoutItemType, InsertType.Bottom);
                    int newHeight = Size.Height + 30;
                }
                chkIsInventory.Checked = false;

            }
            else //Good
            {
                if (!layoutServiceType.IsHidden)
                {
                    layoutServiceType.HideToCustomization();
                    int newHeight = Size.Height - 30;
                }
            }
        }      

 
        private void chkExpenseItem_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkExpenseItem.Checked && !chkSalesItem.Checked && !chkFixedAssetItem.Checked
                && !chkIsInventory.Checked && string.IsNullOrEmpty(m_ItemCode))
            {
                MessageBox.ShowErrorMessage("You should at least select one option for the item type");
                chkExpenseItem.Checked = true;
            }
            if (chkExpenseItem.Checked)
            {
                ShowExpenseItemControls();
                if (!chkSalesItem.Checked)
                    HideSalesItemControls();
                if (!chkIsInventory.Checked && !chkFixedAssetItem.Checked)
                    HideInventoryItemControls();
                if (!chkFixedAssetItem.Checked)
                    HideFixedAssetControls();
            }
            else
                HideExpenseItemControls();
            
        }

        private void HideFixedAssetControls()
        {
            layoutDepreciationType.HideToCustomization();
            HideInventoryItemControls();
        }

        private void HideExpenseItemControls()
        {
            layoutExpenseKind.HideToCustomization();
            layoutExpenseType.HideToCustomization();
        }

        private void ShowExpenseItemControls()
        {
            if (layoutExpenseKind.IsHidden)
                layoutExpenseKind.RestoreFromCustomization(layoutName, InsertType.Bottom);
            if (layoutExpenseType.IsHidden)
                layoutExpenseType.RestoreFromCustomization(layoutExpenseKind, InsertType.Bottom);
        }

        private void HideSalesItemControls()
        {
            layoutFixedUnitPrice.HideToCustomization();
            layoutUnitPrice.HideToCustomization();
            layoutGoodType.HideToCustomization();
        }

        private void HideInventoryItemControls()
        {
            if (!chkIsInventory.Checked)
                layoutInventoryType.HideToCustomization();
        }

       

        private void chkSalesItem_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkSalesItem.Checked && !chkExpenseItem.Checked && !chkFixedAssetItem.Checked
                && !chkIsInventory.Checked)
            {
                chkExpenseItem.Checked = true;
            }
            if (chkSalesItem.Checked)
            {
                ShowSalesItemControls();
                if (!chkExpenseItem.Checked)
                    HideExpenseItemControls();
                if (!chkFixedAssetItem.Checked)
                    HideFixedAssetControls();
                if (!chkIsInventory.Checked && !chkFixedAssetItem.Checked)
                    HideInventoryItemControls();
            }
            else
                HideSalesItemControls();
            
        }

        private void ShowSalesItemControls()
        {
            if (layoutFixedUnitPrice.IsHidden)
                layoutFixedUnitPrice.RestoreFromCustomization(layoutTaxStatus, InsertType.Bottom);
            if (layoutGoodType.IsHidden)
                layoutGoodType.RestoreFromCustomization(layoutItemType, InsertType.Bottom);                    
        }

        private void chkFixedAssetItem_CheckedChanged(object sender, EventArgs e)
        {
            //if (chkFixedAssetItem.Checked && chkExpenseItem.Checked)
            //{
            //    chkExpenseItem.Checked = false;
            //}
            if (chkFixedAssetItem.Checked && chkIsInventory.Checked)
            {
                chkIsInventory.Checked = false;
            }
            if(!chkFixedAssetItem.Checked && !chkExpenseItem.Checked && !chkIsInventory.Checked &&
                !chkSalesItem.Checked)
            {
                chkExpenseItem.Checked = true;
            }
            if (chkFixedAssetItem.Checked)
            {
                cmbItemType.SelectedIndex = 0;
                cmbItemType.Properties.ReadOnly = true;
                ShowFixedAssetItemControls();
                //ShowInventoryItemControls();
                if (!chkExpenseItem.Checked)
                    HideExpenseItemControls();
                if (!chkSalesItem.Checked)
                    HideSalesItemControls();
                //if (!chkIsInventory.Checked)
                //    HideInventoryItemControls();
            }
            else
            {
                cmbItemType.Properties.ReadOnly = false;
                HideFixedAssetControls();
               // HideInventoryItemControls();
            }
        }

        private void ShowFixedAssetItemControls()
        {
            if (layoutDepreciationType.IsHidden)
                layoutDepreciationType.RestoreFromCustomization(layoutMeasureUnit, InsertType.Bottom);
            ItemCategory categ = iERPTransactionClient.GetItemCategory(m_CategoryID);
            if (categ.finishedGoodsAccountPID > 0 || categ.generalInventoryAccountPID > 0)
                ShowInventoryItemControls();
        }

        private void chkInventoryItem_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIsInventory.Checked && !chkSalesItem.Checked && !chkExpenseItem.Checked)
                chkExpenseItem.Checked = true;
            if (chkIsInventory.Checked && chkFixedAssetItem.Checked)
                chkFixedAssetItem.Checked = false;
            if (!chkIsInventory.Checked && !chkExpenseItem.Checked && !chkSalesItem.Checked
                && !chkFixedAssetItem.Checked)
            {
                chkExpenseItem.Checked = true;
            }
            if (chkIsInventory.Checked)
            {
                ShowInventoryItemControls();
                if (!chkExpenseItem.Checked)
                    HideExpenseItemControls();
                if (!chkSalesItem.Checked)
                    HideSalesItemControls();
                if (!chkFixedAssetItem.Checked)
                    HideFixedAssetControls();
            }
            else
                HideInventoryItemControls();

        }

        private void ShowInventoryItemControls()
        {
            if (layoutInventoryType.IsHidden)
                layoutInventoryType.RestoreFromCustomization(layoutDescription, InsertType.Top);
        }

        private void chkHasFixedUnitPrice_CheckedChanged(object sender, EventArgs e)
        {
            if (chkHasFixedUnitPrice.Checked)
            {
                if (layoutUnitPrice.IsHidden)
                {
                    layoutUnitPrice.RestoreFromCustomization(layoutFixedUnitPrice, InsertType.Right);
                }
            }
            else
            {
                layoutUnitPrice.HideToCustomization();
            }
        }

        private void buttonConversionFactors_Click(object sender, EventArgs e)
        {
            ConversionFactorEditor editor = new ConversionFactorEditor(this.ItemCode, -1);
            editor.ShowDialog(this);
        }

        private void gridManager_Click(object sender, EventArgs e)
        {

        }

        private void gridViewManager_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {

            updatePropertyButtonsStatus();
        }

        private void updatePropertyButtonsStatus()
        {
            buttonShowLedger.Enabled = buttonEditProperty.Enabled = buttonDeleteProperty.Enabled = (gridViewAccountInfo.SelectedRowsCount > 0);
        }

        private void buttonAddProperty_Click(object sender, EventArgs e)
        {
            RegisterProperty p = new RegisterProperty(m_ItemCode);
            if (p.ShowDialog(this) == DialogResult.OK)
            {
                loadProperties(m_ItemCode);
            }

        }

        private void buttonEditProperty_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow row = gridViewManager.GetDataRow(gridViewManager.FocusedRowHandle);
                int propertyID = (int)row["id"];
                RegisterProperty p = new RegisterProperty(propertyID);
                if (p.ShowDialog(this) == DialogResult.OK)
                {
                    loadProperties(m_ItemCode);
                }
            }
            catch(Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }

        private void buttonDeleteProperty_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.ShowWarningMessage("Are you sure you want to delete the selected property?") == DialogResult.Yes)
                {
                    DataRow row = gridViewManager.GetDataRow(gridViewManager.FocusedRowHandle);
                    int propertyID = (int)row["id"];
                    iERPTransactionClient.deleteProperty(propertyID);
                    loadProperties(m_ItemCode);
                }
            }
            catch(Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }

        private void buttonShowLedger_Click(object sender, EventArgs e)
        {
            int[] rows=gridViewManager.GetSelectedRows();
            if(rows.Length==0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserWarning("Please select a property");
                return;
            }
            TransactionItems titem=gridViewManager.GetDataRow(rows[0])["itemData"] as TransactionItems;
            if(titem==null)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserWarning("Please select a property");
                return;
            }

            int[] acs=new int[0];
            if(titem.inventoryAccountID!=-1)
                acs=INTAPS.ArrayExtension.appendToArray(acs,titem.inventoryAccountID);
            if(titem.originalFixedAssetAccountID!=-1)
                acs = INTAPS.ArrayExtension.appendToArray(acs, titem.originalFixedAssetAccountID);
            if(titem.accumulatedDepreciationAccountID!=-1)
                acs = INTAPS.ArrayExtension.appendToArray(acs, titem.accumulatedDepreciationAccountID);
            if(acs.Length==0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserWarning("There is no asset account for this item that can be used to generate legder");
                return;
            }
            new LedgerViewer(CostCenter.ROOT_COST_CENTER, acs,"Ledger of property: {0}".format(titem.NameCode)).Show(this);
        }

        private void buttonShowStockCard_Click(object sender, EventArgs e)
        {
            
            new StockCardViewer(this.m_transactionItem.Code, CostCenter.ROOT_COST_CENTER).Show(this);
        }

    }
}