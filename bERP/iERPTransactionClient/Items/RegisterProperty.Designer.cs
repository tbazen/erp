﻿namespace BIZNET.iERP.Client
{
    partial class RegisterProperty
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegisterProperty));
            this.validationExpenseItem = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.textName = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.itemPlaceHolder = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.checkDirectCost = new System.Windows.Forms.CheckBox();
            this.panelAdditionalData = new DevExpress.XtraEditors.PanelControl();
            this.textDescription = new DevExpress.XtraEditors.MemoEdit();
            this.comboPropertyType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dateAcquire = new BIZNET.iERP.Client.BNDualCalendar();
            this.textPropertyCode = new DevExpress.XtraEditors.TextEdit();
            this.gridAccountInfo = new DevExpress.XtraGrid.GridControl();
            this.gridViewAccountInfo = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutGeneralInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutFixedAssetItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.pageAdditionalData = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAccountInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.costCenterPlaceHolder = new INTAPS.Accounting.Client.CostCenterPlaceHolder();
            this.layoutCostCenter = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.validationExpenseItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelAdditionalData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboPropertyType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textPropertyCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAccountInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAccountInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGeneralInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFixedAssetItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageAdditionalData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAccountInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCostCenter)).BeginInit();
            this.SuspendLayout();
            // 
            // validationExpenseItem
            // 
            this.validationExpenseItem.ValidateHiddenControls = false;
            // 
            // textName
            // 
            this.textName.Location = new System.Drawing.Point(102, 163);
            this.textName.Name = "textName";
            this.textName.Properties.Mask.EditMask = "[0-9a-zA-Z].*";
            this.textName.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.textName.Size = new System.Drawing.Size(507, 20);
            this.textName.StyleController = this.layoutControl1;
            this.textName.TabIndex = 1;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "TradeRelation name cannot be empty";
            this.validationExpenseItem.SetValidationRule(this.textName, conditionValidationRule1);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.costCenterPlaceHolder);
            this.layoutControl1.Controls.Add(this.itemPlaceHolder);
            this.layoutControl1.Controls.Add(this.checkDirectCost);
            this.layoutControl1.Controls.Add(this.panelAdditionalData);
            this.layoutControl1.Controls.Add(this.textDescription);
            this.layoutControl1.Controls.Add(this.comboPropertyType);
            this.layoutControl1.Controls.Add(this.dateAcquire);
            this.layoutControl1.Controls.Add(this.textPropertyCode);
            this.layoutControl1.Controls.Add(this.gridAccountInfo);
            this.layoutControl1.Controls.Add(this.textName);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(731, 148, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(623, 385);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // itemPlaceHolder
            // 
            this.itemPlaceHolder.anobject = null;
            this.itemPlaceHolder.Location = new System.Drawing.Point(101, 46);
            this.itemPlaceHolder.Name = "itemPlaceHolder";
            this.itemPlaceHolder.showAdd = false;
            this.itemPlaceHolder.Size = new System.Drawing.Size(509, 20);
            this.itemPlaceHolder.TabIndex = 26;
            this.itemPlaceHolder.ObjectChanged += new System.EventHandler(this.itemPlaceHolder_ObjectChanged);
            // 
            // checkDirectCost
            // 
            this.checkDirectCost.Location = new System.Drawing.Point(11, 220);
            this.checkDirectCost.Name = "checkDirectCost";
            this.checkDirectCost.Size = new System.Drawing.Size(601, 20);
            this.checkDirectCost.TabIndex = 25;
            this.checkDirectCost.Text = "Account as Direct Cost";
            this.checkDirectCost.UseVisualStyleBackColor = true;
            // 
            // panelAdditionalData
            // 
            this.panelAdditionalData.Location = new System.Drawing.Point(11, 44);
            this.panelAdditionalData.Name = "panelAdditionalData";
            this.panelAdditionalData.Size = new System.Drawing.Size(601, 278);
            this.panelAdditionalData.TabIndex = 24;
            // 
            // textDescription
            // 
            this.textDescription.Location = new System.Drawing.Point(14, 263);
            this.textDescription.Name = "textDescription";
            this.textDescription.Size = new System.Drawing.Size(595, 56);
            this.textDescription.StyleController = this.layoutControl1;
            this.textDescription.TabIndex = 23;
            // 
            // comboPropertyType
            // 
            this.comboPropertyType.Location = new System.Drawing.Point(102, 103);
            this.comboPropertyType.Name = "comboPropertyType";
            this.comboPropertyType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboPropertyType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboPropertyType.Size = new System.Drawing.Size(507, 20);
            this.comboPropertyType.StyleController = this.layoutControl1;
            this.comboPropertyType.TabIndex = 22;
            // 
            // dateAcquire
            // 
            this.dateAcquire.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateAcquire.Location = new System.Drawing.Point(102, 193);
            this.dateAcquire.Name = "dateAcquire";
            this.dateAcquire.ShowEthiopian = false;
            this.dateAcquire.ShowGregorian = true;
            this.dateAcquire.ShowTime = false;
            this.dateAcquire.Size = new System.Drawing.Size(507, 21);
            this.dateAcquire.TabIndex = 21;
            this.dateAcquire.VerticalLayout = true;
            // 
            // textPropertyCode
            // 
            this.textPropertyCode.Location = new System.Drawing.Point(102, 133);
            this.textPropertyCode.Name = "textPropertyCode";
            this.textPropertyCode.Properties.ReadOnly = true;
            this.textPropertyCode.Size = new System.Drawing.Size(507, 20);
            this.textPropertyCode.StyleController = this.layoutControl1;
            this.textPropertyCode.TabIndex = 20;
            // 
            // gridAccountInfo
            // 
            this.gridAccountInfo.Location = new System.Drawing.Point(11, 44);
            this.gridAccountInfo.MainView = this.gridViewAccountInfo;
            this.gridAccountInfo.Name = "gridAccountInfo";
            this.gridAccountInfo.Size = new System.Drawing.Size(601, 278);
            this.gridAccountInfo.TabIndex = 13;
            this.gridAccountInfo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAccountInfo,
            this.gridView1});
            // 
            // gridViewAccountInfo
            // 
            this.gridViewAccountInfo.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewAccountInfo.Appearance.GroupPanel.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.gridViewAccountInfo.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewAccountInfo.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewAccountInfo.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewAccountInfo.Appearance.GroupRow.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.gridViewAccountInfo.Appearance.GroupRow.Options.UseFont = true;
            this.gridViewAccountInfo.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridViewAccountInfo.GridControl = this.gridAccountInfo;
            this.gridViewAccountInfo.GroupPanelText = "Account Information";
            this.gridViewAccountInfo.Name = "gridViewAccountInfo";
            this.gridViewAccountInfo.OptionsBehavior.Editable = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowFilter = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowGroup = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowSort = false;
            this.gridViewAccountInfo.OptionsFind.FindDelay = 100;
            this.gridViewAccountInfo.OptionsFind.FindMode = DevExpress.XtraEditors.FindMode.Always;
            this.gridViewAccountInfo.OptionsMenu.EnableColumnMenu = false;
            this.gridViewAccountInfo.OptionsMenu.EnableFooterMenu = false;
            this.gridViewAccountInfo.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewAccountInfo.OptionsView.ShowGroupPanel = false;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridAccountInfo;
            this.gridView1.Name = "gridView1";
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnCancel);
            this.panelControl1.Controls.Add(this.btnOk);
            this.panelControl1.Location = new System.Drawing.Point(5, 332);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(613, 48);
            this.panelControl1.TabIndex = 10;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(533, 11);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 29);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Close";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Appearance.Options.UseFont = true;
            this.btnOk.Location = new System.Drawing.Point(452, 11);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 29);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "&Register";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1,
            this.layoutControlItem8});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(623, 385);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.tabbedControlGroup1.SelectedTabPage = this.layoutGeneralInfo;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(617, 327);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutGeneralInfo,
            this.pageAdditionalData,
            this.layoutAccountInfo});
            this.tabbedControlGroup1.Text = "tabbedControlGroup1";
            // 
            // layoutGeneralInfo
            // 
            this.layoutGeneralInfo.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutGeneralInfo.AppearanceGroup.Options.UseFont = true;
            this.layoutGeneralInfo.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutGeneralInfo.AppearanceItemCaption.Options.UseFont = true;
            this.layoutGeneralInfo.AppearanceTabPage.Header.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutGeneralInfo.AppearanceTabPage.Header.Options.UseFont = true;
            this.layoutGeneralInfo.CaptionImage = global::BIZNET.iERP.Client.Properties.Resources.expense_info;
            this.layoutGeneralInfo.CustomizationFormText = "layoutControlGroup3";
            this.layoutGeneralInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutName,
            this.layoutItemCode,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem6,
            this.layoutFixedAssetItem,
            this.layoutCostCenter});
            this.layoutGeneralInfo.Location = new System.Drawing.Point(0, 0);
            this.layoutGeneralInfo.Name = "layoutGeneralInfo";
            this.layoutGeneralInfo.Size = new System.Drawing.Size(605, 282);
            this.layoutGeneralInfo.Text = "General Information";
            // 
            // layoutName
            // 
            this.layoutName.Control = this.textName;
            this.layoutName.CustomizationFormText = "TradeRelation Name:";
            this.layoutName.Location = new System.Drawing.Point(0, 116);
            this.layoutName.Name = "layoutName";
            this.layoutName.Size = new System.Drawing.Size(605, 30);
            this.layoutName.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutName.Text = "Name:";
            this.layoutName.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutItemCode
            // 
            this.layoutItemCode.Control = this.textPropertyCode;
            this.layoutItemCode.CustomizationFormText = "Code:";
            this.layoutItemCode.Location = new System.Drawing.Point(0, 86);
            this.layoutItemCode.Name = "layoutItemCode";
            this.layoutItemCode.Size = new System.Drawing.Size(605, 30);
            this.layoutItemCode.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutItemCode.Text = "Code:";
            this.layoutItemCode.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dateAcquire;
            this.layoutControlItem1.CustomizationFormText = "Acquire Date:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 146);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(182, 30);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(605, 30);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Text = "Acquire Date:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.comboPropertyType;
            this.layoutControlItem2.CustomizationFormText = "Property Type";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(605, 30);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem2.Text = "Property Type";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textDescription;
            this.layoutControlItem3.CustomizationFormText = "Description";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 200);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(605, 82);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "Description";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.checkDirectCost;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 176);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(605, 24);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutFixedAssetItem
            // 
            this.layoutFixedAssetItem.Control = this.itemPlaceHolder;
            this.layoutFixedAssetItem.CustomizationFormText = "Fixed Asset Item:";
            this.layoutFixedAssetItem.Location = new System.Drawing.Point(0, 0);
            this.layoutFixedAssetItem.Name = "layoutFixedAssetItem";
            this.layoutFixedAssetItem.Size = new System.Drawing.Size(605, 28);
            this.layoutFixedAssetItem.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutFixedAssetItem.Text = "Fixed Asset Item:";
            this.layoutFixedAssetItem.TextSize = new System.Drawing.Size(85, 13);
            // 
            // pageAdditionalData
            // 
            this.pageAdditionalData.AppearanceGroup.Options.UseFont = true;
            this.pageAdditionalData.AppearanceItemCaption.Options.UseFont = true;
            this.pageAdditionalData.CustomizationFormText = "XXX";
            this.pageAdditionalData.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.pageAdditionalData.Location = new System.Drawing.Point(0, 0);
            this.pageAdditionalData.Name = "pageAdditionalData";
            this.pageAdditionalData.Size = new System.Drawing.Size(605, 282);
            this.pageAdditionalData.Text = "XXXX";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.panelAdditionalData;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(605, 282);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutAccountInfo
            // 
            this.layoutAccountInfo.AppearanceTabPage.Header.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutAccountInfo.AppearanceTabPage.Header.Options.UseFont = true;
            this.layoutAccountInfo.CaptionImage = global::BIZNET.iERP.Client.Properties.Resources.accounting_info;
            this.layoutAccountInfo.CustomizationFormText = "layoutControlGroup4";
            this.layoutAccountInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutAccountInfo.Location = new System.Drawing.Point(0, 0);
            this.layoutAccountInfo.Name = "layoutAccountInfo";
            this.layoutAccountInfo.Size = new System.Drawing.Size(605, 282);
            this.layoutAccountInfo.Text = "Account Information";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridAccountInfo;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(605, 282);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.panelControl1;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 327);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 52);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(104, 52);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(617, 52);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // costCenterPlaceHolder
            // 
            this.costCenterPlaceHolder.account = null;
            this.costCenterPlaceHolder.AllowAdd = false;
            this.costCenterPlaceHolder.Location = new System.Drawing.Point(101, 74);
            this.costCenterPlaceHolder.Name = "costCenterPlaceHolder";
            this.costCenterPlaceHolder.OnlyLeafAccount = false;
            this.costCenterPlaceHolder.Size = new System.Drawing.Size(509, 20);
            this.costCenterPlaceHolder.TabIndex = 29;
            // 
            // layoutCostCenter
            // 
            this.layoutCostCenter.Control = this.costCenterPlaceHolder;
            this.layoutCostCenter.CustomizationFormText = "layoutCostCenter";
            this.layoutCostCenter.Location = new System.Drawing.Point(0, 28);
            this.layoutCostCenter.Name = "layoutCostCenter";
            this.layoutCostCenter.Size = new System.Drawing.Size(605, 28);
            this.layoutCostCenter.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutCostCenter.Text = "layoutCostCenter";
            this.layoutCostCenter.TextSize = new System.Drawing.Size(85, 13);
            // 
            // RegisterProperty
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(623, 385);
            this.Controls.Add(this.layoutControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "RegisterProperty";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Property Editor";
            ((System.ComponentModel.ISupportInitialize)(this.validationExpenseItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelAdditionalData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboPropertyType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textPropertyCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAccountInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAccountInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGeneralInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFixedAssetItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageAdditionalData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAccountInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCostCenter)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validationExpenseItem;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.TextEdit textName;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.MemoEdit textDescription;
        private DevExpress.XtraEditors.ComboBoxEdit comboPropertyType;
        private BNDualCalendar dateAcquire;
        private DevExpress.XtraEditors.TextEdit textPropertyCode;
        private DevExpress.XtraGrid.GridControl gridAccountInfo;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewAccountInfo;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGeneralInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutName;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemCode;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutAccountInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlGroup pageAdditionalData;
        private DevExpress.XtraEditors.PanelControl panelAdditionalData;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private System.Windows.Forms.CheckBox checkDirectCost;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private TransactionItemPlaceHolder itemPlaceHolder;
        private DevExpress.XtraLayout.LayoutControlItem layoutFixedAssetItem;
        private INTAPS.Accounting.Client.CostCenterPlaceHolder costCenterPlaceHolder;
        private DevExpress.XtraLayout.LayoutControlItem layoutCostCenter;
    }
}