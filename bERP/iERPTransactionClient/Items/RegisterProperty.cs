﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.IO;
using System.Drawing.Imaging;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraGrid;
using DevExpress.Utils;
using INTAPS.UI.ButtonGrid;

namespace BIZNET.iERP.Client
{
    public partial class RegisterProperty : XtraForm
    {
        string _parentItemCode;
        DataTable accountInfoTable;
        int _propertyID = -1;
        public int PropertyID
        {
            get { return _propertyID; }
        }
        bool manualCode;
        INTAPS.CachedObject<Type, PropertyFormBase> additionalDataPage = new INTAPS.CachedObject<Type, PropertyFormBase>
            (
            delegate(Type t)
            {
                PropertyFormBase f = PropertyFormBase.getFormForDataType(t);
                if (f != null)
                    f.Dock = DockStyle.Fill;
                return f;
            }
            );
        PropertyFormBase additionaDataForm = null;
        void initForm(string itemCode)
        {
            InitializeComponent();
            object manual = iERPTransactionClient.GetSystemParamter("manualPropertyCode");
            if (manual is DBNull)
                MessageBox.ShowErrorMessage("Error: Please Configure manualPropertyCode setting in system parameters.");
            else
                manualCode = (bool)manual;
            textPropertyCode.Properties.ReadOnly = !manualCode;
            setParentItemCode(itemCode);
            comboPropertyType.Properties.Items.AddRange(iERPTransactionClient.getAllPropertTypes());
            comboPropertyType.SelectedIndexChanged += new EventHandler(comboPropertyType_SelectedIndexChanged);
            comboPropertyType.SelectedIndex = 0;
            //if (!string.IsNullOrEmpty(itemCode))
            //{
            //    itemPlaceHolder.SetByID(itemCode);
            //    itemPlaceHolder.Enabled = false;
            //}
        }

        void comboPropertyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            PropertyType pt = comboPropertyType.SelectedItem as PropertyType;
            if (pt == null || additionalDataPage[pt.extraDataType] == null)
            {
                additionaDataForm = null;
                if (pageAdditionalData.Visibility != LayoutVisibility.Never)
                    pageAdditionalData.Visibility = LayoutVisibility.Never;
            }
            else
            {
                if (pageAdditionalData.Visibility != LayoutVisibility.Always)
                    pageAdditionalData.Visibility = LayoutVisibility.Always;

                if (panelAdditionalData.Controls.Count == 0)
                    panelAdditionalData.Controls.Add(additionaDataForm = additionalDataPage[pt.extraDataType]);
                if (additionalDataPage[pt.extraDataType] != panelAdditionalData.Controls[0])
                {
                    panelAdditionalData.Controls.RemoveAt(0);
                    panelAdditionalData.Controls.Add(additionaDataForm = additionalDataPage[pt.extraDataType]);
                }
                pageAdditionalData.Text = pt.name;
            }
        }
        Property _updateProp = null;
        public object extraData = null;
        public int costCenterID;
        bool _pickCostCenter=false;
        DateTime _time;
        public RegisterProperty(Property prop, object extraData, int costCenterID,DateTime time, bool pickCostCenter)
        {
            _time = time;
            _pickCostCenter = pickCostCenter;
            _updateProp = prop;
            initForm(prop.itemCode);
            layoutFixedAssetItem.Visibility = LayoutVisibility.Always;
            layoutCostCenter.Visibility = pickCostCenter ? LayoutVisibility.Always : LayoutVisibility.Never;
            itemPlaceHolder.SetByID(prop.parentItemCode);

            itemPlaceHolder_ObjectChanged(null, null);
            costCenterPlaceHolder.SetByID(costCenterID);
            setData(prop, extraData);
        }
        public RegisterProperty(string itemCode)
        {

            initForm(itemCode);
            layoutFixedAssetItem.Visibility = LayoutVisibility.Never;
        }
        public RegisterProperty(int propertyID)
        {
            this._propertyID = propertyID;
            object extraData;
            Property prop = iERPTransactionClient.getProperty(propertyID, out extraData);
            initForm(prop.parentItemCode);
            layoutFixedAssetItem.Visibility = LayoutVisibility.Never;
            setData(prop, extraData);
        }
        void setParentItemCode(string itemCode)
        {
            _parentItemCode = itemCode;
        }


        private void setData(Property prop, object extraData)
        {
            foreach (PropertyType pt in comboPropertyType.Properties.Items)
            {
                if (pt.ID == prop.propertyTypeID)
                {
                    comboPropertyType.SelectedItem = pt;
                    break;
                }
            }
            textPropertyCode.Text = prop.propertyCode;
            textName.Text = prop.name;
            textDescription.Text = prop.description;
            dateAcquire.DateTime = prop.acquireDate;
            checkDirectCost.Checked = prop.directCost;
            TransactionItems tranItem = iERPTransactionClient.GetTransactionItems(prop.itemCode);
            PrepareAccountInfoTable();
            DisplayItemAccountInformation(tranItem);
            if (additionaDataForm != null)
                additionaDataForm.setData(extraData);
        }
        private void PrepareAccountInfoTable()
        {
            accountInfoTable = new DataTable();
            accountInfoTable.Columns.Add("Account Code", typeof(string));
            accountInfoTable.Columns.Add("Account Type", typeof(string));
            accountInfoTable.Columns.Add("Account Name", typeof(string));
            gridAccountInfo.DataSource = accountInfoTable;
        }
        private void DisplayItemAccountInformation(TransactionItems item)
        {
            accountInfoTable.Clear();
            if (item != null)
            {
                foreach (System.Reflection.FieldInfo fi in typeof(TransactionItems).GetFields())
                {
                    object[] atr = fi.GetCustomAttributes(typeof(AIChildOfAttribute), true);
                    if (atr == null || atr.Length == 0)
                        continue;
                    AIChildOfAttribute a = atr[0] as AIChildOfAttribute;
                    int ac = (int)fi.GetValue(item);
                    if (ac != -1)
                    {
                        Account acnt = AccountingClient.GetAccount<Account>(ac);
                        if (acnt == null)
                            accountInfoTable.Rows.Add("Invalid account ID", ac.ToString(), "");
                        else
                            accountInfoTable.Rows.Add(acnt.Code, a.displayName, acnt.Name);

                    }
                }
            }
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (_pickCostCenter)
                {
                    if (costCenterPlaceHolder.GetAccountID()== -1)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select a cost center");
                        return;
                    }
                    costCenterID = costCenterPlaceHolder.GetAccountID();
                }
                Property prop = _updateProp == null ? new Property() : _updateProp;
                prop.id = _propertyID;
                prop.propertyTypeID = ((PropertyType)comboPropertyType.SelectedItem).ID;
                if (string.IsNullOrEmpty(_parentItemCode))
                {
                    prop.parentItemCode = itemPlaceHolder.GetObjectID();
                    if (string.IsNullOrEmpty(prop.parentItemCode))
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter item code");
                        return;
                    }
                }
                else
                    prop.parentItemCode = _parentItemCode;
                prop.propertyCode = manualCode ? textPropertyCode.Text.Trim() : "";
                prop.name = textName.Text.Trim();
                prop.description = textDescription.Text.Trim();
                prop.acquireDate = dateAcquire.DateTime;
                prop.directCost = checkDirectCost.Checked;
                if (string.IsNullOrEmpty(prop.name))
                {
                    MessageBox.ShowErrorMessage("Enter valid property name");
                    return;
                }
                if (manualCode && string.IsNullOrEmpty(prop.propertyCode))
                {
                    MessageBox.ShowErrorMessage("Enter valid property name");
                    return;
                }
                extraData = additionaDataForm.getData();
                if (_updateProp == null)
                {
                    if (prop.id == -1)
                        _propertyID = iERPTransactionClient.createProperty(prop, extraData);
                    else
                        iERPTransactionClient.updateProperty(prop, extraData);
                }
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error occurred while trying to saving property\n" + ex.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void itemPlaceHolder_ObjectChanged(object sender, EventArgs e)
        {
        
            
        }
    }
}