using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.UI.ButtonGrid;

namespace BIZNET.iERP.Client
{
    public class ArrayChildSource:INTAPS.UI.ButtonGrid.IButtonChildSource
    {
        List<INTAPS.UI.ButtonGrid.ButtonGridItem> _items;
        ButtonGrid _grid;
        ButtonGridItem _parentButton;
        public ArrayChildSource(ButtonGrid grid,ButtonGridItem parentButton, params ButtonGridItem[] items)
        {
            _items = new List<ButtonGridItem>();
            _items.AddRange(items);
            _grid = grid;
            _parentButton = parentButton;
            foreach (ButtonGridItem itm in items)
            {
                if(itm!=null)
                    _grid.RelateWithParent(parentButton, itm);
            }
        }
        #region IButtonChildSource Members

        

        public void SetQuery(string query)
        {
            
        }

        #endregion

        #region IEnumerable<ButtonGridItem> Members

        public IEnumerator<INTAPS.UI.ButtonGrid.ButtonGridItem> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        public bool Searchable
        {
            get
            {
                return false;
            }
        }
        public string searchLabel
        {
            get { throw new NotImplementedException(); }
        }
    }
}
