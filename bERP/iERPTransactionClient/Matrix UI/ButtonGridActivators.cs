﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERP.Client
{
    public class BGADocumentType : IiERPCommandActivator
    {
        bERPClientDocumentHandler _handler;
        public BGADocumentType(int docTypeID)
        {
            _handler = iERPTransactionClient.GetDocumentHandler(docTypeID);
        }
        #region IiERPCommandActivator Members

        protected virtual ActivationParameter getActivationParameter()
        {
            return new ActivationParameter();
        }
        public virtual DialogResult Activate()
        {
            _handler.setAccountingClient(INTAPS.Accounting.Client.AccountingClient.AccountingClientDefaultInstance);
            object e = _handler.CreateEditor(getActivationParameter());
            _handler.SetEditorDocument(e, null);
            if (e is Form)
            {
                ((Form)e).Show(INTAPS.UI.UIFormApplicationBase.MainForm);
            }
            return DialogResult.OK;
        }
        #endregion
    }

    public class BGABankAccount : BGADocumentType
    {

        int _bankID;
        public BGABankAccount(int docTypeID, int bankID)
            : base(docTypeID)
        {
            _bankID = bankID;
        }
        #region IiERPCommandActivator Members

        protected override ActivationParameter getActivationParameter()
        {
            ActivationParameter ret= base.getActivationParameter();
            ret.assetAccountID=_bankID;
            return ret;
        }
        #endregion
    }

}