using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.UI.ButtonGrid;
using System.Drawing;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    class KeyedButton:ButtonGridBodyButtonItem,IEquatable<KeyedButton>
    {
        public string key;
        public KeyedButton(string key):base(false)
        {
            this.key=key;
        }
        public KeyedButton(bool newLine)
            : base(newLine)
        {
            
        }
         

        #region IEquatable<KeyedButton> Members

        public bool Equals(KeyedButton other)
        {
            if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(other.key))
                return this == other;
            return this.key.Equals(other.key);
        }
        public override bool Equals(object obj)
        {
            if (!(obj is ButtonGridBodyButtonItem))
                return false;
            return this.Equals((KeyedButton)obj);
        }
        public override int GetHashCode()
        {
            return key.GetHashCode();
        }
        #endregion
    }
    public class ButtonGridHelper
    {
        public static Font font_title = new Font("Arial Rounded MT", 18);
        public static Font font_title_Bold = new Font("Arial Rounded MT", 18,FontStyle.Bold);
        public static Font font_normal_button= new Font("Arial Rounded MT", 10.0f,FontStyle.Bold);
        public const int BUTTON_WIDTH = 128;
        public const int BUTTON_HEIGHT = 128;
        public const int BUTTON_CMD_HEIGHT = 60;
        public const int JUMBO_BUTTON_WIDTH = 200;
        public const int JUMBO_BUTTON_HEIGHT = 240;

        public static Image LoadImageFromServer(int pictureID, int width, int height)
        {
            if (pictureID != -1)
            {
                try
                {
                    return INTAPS.Accounting.Client.AccountingClient.LoadImageFormated(pictureID, 1, false, width, height);
                }
                catch(Exception ex)
                {
                    Console.WriteLine(string.Format("Error loading image {0} returning null\n{1}", pictureID,ex.Message));
                    return null;
                }

            }
            return null;
        }
        public static ButtonElement<Image> AddLargeImage(ButtonGridItem item, Image img)
        {
            img = null;
            if (img == null)
                return null;
            
            if(item is ButtonGridBodyButtonItem)
            {
                ButtonGridBodyButtonItem button = (ButtonGridBodyButtonItem)item;
                return button.ApperanceBody.AddImage(img);
            }
            else if (item is ButtonGridGroupButtonItem)
            {
                ButtonGridGroupButtonItem button = (ButtonGridGroupButtonItem)item;
                return  button.Apperance.AddImage(img);
            }
            return null;
        }
        public static void AddLargeImageRightBottom(ButtonGridItem item, Image img)
        {
            if (img != null)
            {
                ButtonElement<Image> el = AddLargeImage(item,img);
                el.DefaultStyle.Add(StyleType.ImageAlignment, ImageAlignment.RightBottom);
            }
        }
        public static void AddLargeImageLeftTop(ButtonGridItem item, Image img)
        {
            if (img != null)
            {
                ButtonElement<Image> el = AddLargeImage(item,img);
                el.DefaultStyle.Add(StyleType.ImageAlignment, ImageAlignment.LeftTop);
                el.SelectStyle.Add(StyleType.ImageAlignment, ImageAlignment.LeftTop);
                el.SnapStyle.Add(StyleType.ImageAlignment, ImageAlignment.LeftTop);
            }
        }
        public static void AddLargeImageRightTop(ButtonGridItem item, Image img)
        {
            if (img != null)
            {
                ButtonElement<Image> el = AddLargeImage(item,img);
                el.DefaultStyle.Add(StyleType.ImageAlignment, ImageAlignment.RightTop);
                el.SelectStyle.Add(StyleType.ImageAlignment, ImageAlignment.RightTop);
                el.SnapStyle.Add(StyleType.ImageAlignment, ImageAlignment.RightTop);
            }
        }
        public static ButtonGridGroupButtonItem CreateGroupButton(string text)
        {
            ButtonGridGroupButtonItem btn = new ButtonGridGroupButtonItem();
            ButtonElement<string> el = btn.Apperance.AddText(text);
            el.DefaultStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormatVert(System.Drawing.StringAlignment.Near));
            el.DefaultStyle.Add(StyleType.Font, font_title);
            return btn;
        }
        //public static ButtonGridGroupButtonItem CreateGroupButton(string text, Image image)
        //{
        //    ButtonGridItem buttonGridItem = CreateGroupButton(text);
        //    AddLargeImageRightBottom(buttonGridItem, image);
        //}
        public static ButtonGridGroupButtonItem CreateGroupButton(string text,int pictureID)
        {
            ButtonGridGroupButtonItem btn = CreateGroupButton(text);
            AddLargeImageRightBottom(btn, LoadImageFromServer(pictureID, BUTTON_WIDTH, BUTTON_HEIGHT));
            return btn;
        }
        public static ButtonGridGroupButtonItem CreateModuleGroupButton(string number,string text)
        {
            ButtonGridGroupButtonItem btn = new ButtonGridGroupButtonItem();
            btn.Apperance.StylesCollection.DefaultStyle.Add(StyleType.CornerRadius, 0);
            ButtonElement<string> el = btn.Apperance.AddText(text);
            el.DefaultStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormatVert(System.Drawing.StringAlignment.Near));
            el.DefaultStyle.Add(StyleType.Font, font_title);
            el = btn.Apperance.AddText(number);
            el.DefaultStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(System.Drawing.StringAlignment.Near, System.Drawing.StringAlignment.Near));
            el.DefaultStyle.Add(StyleType.Font, font_title_Bold);
            return btn;
        }
       // static Pen ButtonFocusPen = new Pen(Color.FromArgb(0x70, 0x0F, 0x87), 2f);
        static Pen ButtonFocusPen = new Pen(Color.WhiteSmoke); // Button Border snap style color
            public static ButtonGridBodyButtonItem CreateButton(string key,string txt)
        {
            ButtonGridBodyButtonItem item = new KeyedButton(key);
            item.ApperanceBody.StylesCollection.DefaultStyle.Add(StyleType.ButtonWidth, BUTTON_WIDTH);
            item.ApperanceBody.StylesCollection.DefaultStyle.Add(StyleType.ButtonHeight, BUTTON_HEIGHT);
            item.ApperanceBody.StylesCollection.SelectStyle.Add(StyleType.ButtonWidth, BUTTON_WIDTH);
            item.ApperanceBody.StylesCollection.SelectStyle.Add(StyleType.ButtonHeight, BUTTON_HEIGHT);
            item.ApperanceBody.StylesCollection.SnapStyle.Add(StyleType.ButtonWidth, BUTTON_WIDTH);
            item.ApperanceBody.StylesCollection.SnapStyle.Add(StyleType.ButtonHeight, BUTTON_HEIGHT);
            item.ApperanceBody.StylesCollection.SnapStyle.Add(StyleType.BroderPen, ButtonFocusPen);

            ButtonElement<string> el = item.ApperanceBody.AddText(txt);
            el.DefaultStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Far, StringAlignment.Center));
            el.SnapStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Far, StringAlignment.Center));
            el.SelectStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Far, StringAlignment.Center));
            
            el.DefaultStyle.Add(StyleType.Font, font_normal_button);
            el.SnapStyle.Add(StyleType.Font, font_normal_button);
            el.SelectStyle.Add(StyleType.Font, font_normal_button);

            el=item.ApperanceHeader.AddText(txt);
            el.SelectStyle.Add(StyleType.FontBrush, Brushes.White);
            el.SelectStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Center,StringAlignment.Center));
            return item;            
        }
        public static ButtonGridBodyButtonItem CreateButton(string key, string txt, string cornerText)
        {
            ButtonGridBodyButtonItem item = CreateButton(key,txt);
            AddCornerText(item,cornerText);
            return item;

        }
        public static void AddCornerText(ButtonGridItem item, string cornerText)
        {
            ButtonElement<string> el = null;
            if (item is ButtonGridBodyButtonItem)
                el = ((ButtonGridBodyButtonItem)item).ApperanceBody.AddText(cornerText);
            else
                el = ((ButtonGridGroupButtonItem)item).Apperance.AddText(cornerText);

            el.DefaultStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Near, StringAlignment.Far));
            el.SnapStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Near, StringAlignment.Far));
            el.SelectStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Near, StringAlignment.Far));

            el.DefaultStyle.Add(StyleType.Font, font_normal_button);
            el.SnapStyle.Add(StyleType.Font, font_normal_button);
            el.SelectStyle.Add(StyleType.Font, font_normal_button);

            //el.DefaultStyle.Add(StyleType.Font, new Font("Arial Rounded MT", 8, FontStyle.Bold));
            //el.SnapStyle.Add(StyleType.Font, new Font("Arial Rounded MT", 8, FontStyle.Bold));
            //el.SelectStyle.Add(StyleType.Font, new Font("Arial Rounded MT", 8, FontStyle.Bold));

            el.DefaultStyle.Add(StyleType.FontBrush, Brushes.DarkGreen);
            el.SnapStyle.Add(StyleType.FontBrush, Brushes.DarkGreen);
            el.SelectStyle.Add(StyleType.FontBrush, Brushes.DarkGreen);
        }
        public static void AddMiddleText(ButtonGridBodyButtonItem item, string middleText)
        {
            ButtonElement<string> el = null;
            el = item.ApperanceBody.AddText(middleText);
            el.DefaultStyle.Add(StyleType.Font, font_normal_button);
            el.SnapStyle.Add(StyleType.Font, font_normal_button);
            el.SelectStyle.Add(StyleType.Font, font_normal_button);

            el.DefaultStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Center, StringAlignment.Center));
            el.SnapStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Center, StringAlignment.Center));
            el.SelectStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Center, StringAlignment.Center));
        }
        
        public static ButtonGridBodyButtonItem CreateButtonWithImage(string key, string txt, Image img)
        {
            ButtonGridBodyButtonItem item = CreateButton(key,txt);
            AddLargeImage(item, img);
            return item;
        }
        public static ButtonGridBodyButtonItem CreateButtonWithImage(string key, string txt, int img)
        {
            return CreateButtonWithImage(key, txt, LoadImageFromServer(img, BUTTON_WIDTH, BUTTON_HEIGHT));
        }
        public static ButtonGridBodyButtonItem CreateButtonWithColor(string key, string txt, Color color)
        {
            ButtonGridBodyButtonItem item = CreateButton(key,txt);
            SolidBrush sb = new SolidBrush(color);
            item.ApperanceBody.StylesCollection.DefaultStyle.Add(StyleType.FillBrush, sb);
            return item;
        }
        public static ButtonGridBodyButtonItem CreateButtonWithColor(string key, string txt, Color color, string cornerText)
        {
            ButtonGridBodyButtonItem item = CreateButtonWithColor(key,txt, color);
            AddCornerText(item, cornerText);
            return item;
        }
        public static ButtonGridBodyButtonItem CreateButtonWithColor(string key, string txt, Color color, string cornerText, Image img)
        {
            ButtonGridBodyButtonItem item = CreateButtonWithColor(key,txt, color, cornerText);
            AddLargeImage(item, img);
            return item;
        }
        public static ButtonGridBodyButtonItem CreateButtonWithColor(string key, string txt, Color color, string cornerText, int img)
        {
            return CreateButtonWithColor(key, txt, color, cornerText,LoadImageFromServer(img,BUTTON_WIDTH,BUTTON_HEIGHT));
        }
        internal static ButtonGridBodyButtonItem CreateCommandButton(string key,string txt)
        {
            ButtonGridBodyButtonItem item = CreateButton(key,txt);
            item.ApperanceBody.StylesCollection.DefaultStyle.Add(StyleType.ButtonHeight, BUTTON_CMD_HEIGHT);
            item.ApperanceBody.StylesCollection.SnapStyle.Add(StyleType.ButtonHeight, BUTTON_CMD_HEIGHT);
            item.ApperanceBody.StylesCollection.SelectStyle.Add(StyleType.ButtonHeight, BUTTON_CMD_HEIGHT);
            return item;
        }
        internal static ButtonGridBodyButtonItem CreateCommandButton(string key, string txt,string cornerText)
        {
            ButtonGridBodyButtonItem item = CreateCommandButton(key, txt);
            AddCornerText(item, cornerText);
            return item;
        }
        internal static void ReformatAsNoneCommand(ButtonGridItem item, Image image)
        {
            StylesCollection styles;
            if (item is ButtonGridBodyButtonItem)
                styles = ((ButtonGridBodyButtonItem)item).ApperanceBody.StylesCollection;
            else
                styles = ((ButtonGridGroupButtonItem)item).Apperance.StylesCollection;

            styles.DefaultStyle.Add(StyleType.ButtonHeight, BUTTON_HEIGHT);
            styles.SnapStyle.Add(StyleType.ButtonHeight, BUTTON_HEIGHT);
            styles.SelectStyle.Add(StyleType.ButtonHeight, BUTTON_HEIGHT);
            AddLargeImage(item, image);
        }
        internal static void ReformatAsNoneCommand(ButtonGridItem item, int image)
        {
            ReformatAsNoneCommand(item, LoadImageFromServer(image, BUTTON_WIDTH, BUTTON_HEIGHT));
        }

        internal static ButtonGridBodyButtonItem CreateJumboButton(string key, string title)
        {
            ButtonGridBodyButtonItem item = new KeyedButton(key);
            item.ApperanceBody.StylesCollection.DefaultStyle.Add(StyleType.ButtonWidth, JUMBO_BUTTON_WIDTH);
            item.ApperanceBody.StylesCollection.DefaultStyle.Add(StyleType.ButtonHeight, JUMBO_BUTTON_HEIGHT);
            item.ApperanceBody.StylesCollection.SelectStyle.Add(StyleType.ButtonWidth, JUMBO_BUTTON_WIDTH);
            item.ApperanceBody.StylesCollection.SelectStyle.Add(StyleType.ButtonHeight, JUMBO_BUTTON_HEIGHT);
            item.ApperanceBody.StylesCollection.SnapStyle.Add(StyleType.ButtonWidth, JUMBO_BUTTON_WIDTH);
            item.ApperanceBody.StylesCollection.SnapStyle.Add(StyleType.ButtonHeight, JUMBO_BUTTON_HEIGHT);

            ButtonElement<string> el = item.ApperanceBody.AddText(title);
            el.DefaultStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Far, StringAlignment.Center));
            el.SnapStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Far, StringAlignment.Center));
            el.SelectStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Far, StringAlignment.Center));
            Font f = new Font("Arial Rounded MT", 14f);
            el.DefaultStyle.Add(StyleType.Font, f);
            el.SnapStyle.Add(StyleType.Font, f);
            el.SelectStyle.Add(StyleType.Font, f);
            return item;     
        }

        internal static void SetButtonBackColor(ButtonGridBodyButtonItem item, Color color)
        {
            SolidBrush sb = new SolidBrush(color);
            item.ApperanceBody.StylesCollection.DefaultStyle.Add(StyleType.FillBrush, sb);
        }
        public static void SetHeaderModeText(ButtonGridBodyButtonItem item, string text)
        {
            item.ApperanceHeader.Texts.Clear();
            item.ApperanceHeader.AddText(text);
        }
    }
}
