using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using INTAPS.UI.ButtonGrid;

namespace BIZNET.iERP.Client
{
    abstract public class ObjectBGListBase
    {
        protected ButtonGrid _grid;
        protected ButtonGridItem _parentItem;
        protected List<ButtonGridItem> _items = null;
        public ObjectBGListBase(ButtonGrid grid, ButtonGridItem parentItem)
        {
            _grid = grid;
            _parentItem = parentItem;

        }
        protected void AddToParent(params ButtonGridItem[] items)
        {
            foreach (ButtonGridItem item in items)
            {
                if (_parentItem == null)
                    _grid.RelateWithParent(_grid.rootButtonItem, item);
                else
                    _grid.RelateWithParent(_parentItem, item);
            }
        }
    }
    class ObjectEditorActivator<ObjectType, FormType> : IiERPCommandActivator
        where FormType : Form
    {

        ObjectType _obj;
        public ObjectEditorActivator(ObjectType obj)
        {
            _obj = obj;
        }
        #region IButtonGridItemActivator Members

        public System.Windows.Forms.DialogResult Activate()
        {
            Form f = (Form)typeof(FormType).GetConstructor(new Type[] { typeof(ObjectType) }).Invoke(new object[] { _obj });
            return f.ShowDialog();
        }

        #endregion
    }

    abstract public class ObjectBGList<ItemType> : ObjectBGListBase, IButtonChildSource
    {
        public ObjectBGList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {

        }

        string _query;
        public abstract List<ItemType> LoadData(string query);
        public abstract ButtonGridItem[] CreateButton(ItemType obj);
        #region IButtonChildSource Members

        public void SetQuery(string query)
        {
            _query = query;
        }

        #endregion

        #region IEnumerable<ButtonGridItem> Members
        protected virtual void CreateButtonItems()
        {
            _items = new List<ButtonGridItem>();
            foreach (ItemType obj in LoadData(_query))
            {
                ButtonGridItem[] item;
                try
                {
                    item = CreateButton(obj);
                }
                catch (Exception ex)
                {
                    item = new ButtonGridItem[] { ButtonGridHelper.CreateButton("", ex.Message) };
                }
                AddToParent(item);
                _items.AddRange(item);
            }
        }
        public IEnumerator<ButtonGridItem> GetEnumerator()
        {
            CreateButtonItems();
            return _items.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        #endregion

        #region IButtonChildSource Members

        public abstract bool Searchable { get;}
        #endregion


        public abstract string searchLabel
        {
            get;
        }
    }
}
