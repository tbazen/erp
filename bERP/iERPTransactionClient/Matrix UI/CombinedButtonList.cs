using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.UI.ButtonGrid;

namespace BIZNET.iERP.Client
{
    class CombinedButtonList : IButtonChildSource,IEnumerator<ButtonGridItem>
    {
        IButtonChildSource[] _sources;
        int sourceIndex=-1;
        IEnumerator<ButtonGridItem> currentSource=null;
        public CombinedButtonList(params IButtonChildSource[] sources)
        {
            _sources = sources;
        }

        #region IButtonChildSource Members

        public bool Searchable
        {
            get 
            {
                foreach (IButtonChildSource src in _sources)
                    if (src.Searchable)
                        return true;
                return false;
            }
        }
        string _query = null;
        public void SetQuery(string query)
        {
            _query = query;
            foreach (IButtonChildSource s in _sources)
                s.SetQuery(_query);

        }
        #endregion

        #region IEnumerable<ButtonGridItem> Members

        public IEnumerator<ButtonGridItem> GetEnumerator()
        {
            sourceIndex = -1;
            return this;
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IEnumerator<ButtonGridItem> Members

        public ButtonGridItem Current
        {
            get 
            {
                return currentSource.Current;
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
        }

        #endregion

        #region IEnumerator Members

        object System.Collections.IEnumerator.Current
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public bool MoveNext()
        {
            if(sourceIndex==-1)
            {
                sourceIndex=0;
                currentSource=_sources[sourceIndex].GetEnumerator();
            }
            while (!currentSource.MoveNext())
            {
                sourceIndex++;
                if (sourceIndex < _sources.Length)
                    currentSource = currentSource = _sources[sourceIndex].GetEnumerator();
                else
                    return false;
            }
            return true;
        }

        public void Reset()
        {
            sourceIndex = -1;
        }

        #endregion


        public string searchLabel
        {
            get {
                string label=null;
                foreach (IButtonChildSource src in _sources)
                    if (src.Searchable)
                    {
                        if (label == null)
                            label = src.searchLabel;
                        else if (!label.Equals(src.searchLabel, StringComparison.CurrentCultureIgnoreCase))
                        {
                            label = null;
                            break;
                        }
                    }
                return label??"Search";
            }
        }
    }
}
