using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting;
using INTAPS.UI.ButtonGrid;

namespace BIZNET.iERP.Client
{
    public interface IiERPCommandActivator
    {
        DialogResult Activate();
    }
    public class EventActivator<ObjectType> : IiERPCommandActivator
    {
        public event GenericEventHandler<ButtonGridBodyButtonItem, ObjectType> Clicked;
        ButtonGridBodyButtonItem _item;
        ObjectType _data;
        public EventActivator(ButtonGridBodyButtonItem item, ObjectType data)
        {
            _item = item;
            _item.Tag = this;
            _data = data;
        }

        #region IButtonGridItemActivator Members

        public DialogResult Activate()
        {
            if (Clicked != null)
                Clicked(_item, _data);
            return DialogResult.OK;
        }

        #endregion
    }
    public class FormActivator : IiERPCommandActivator
    {
        Form owner;
        Type formType;
        bool m_dialog;
        object[] m_args;
        public FormActivator(Form formOwner, Type formType, bool dialog)
        {
            this.owner = formOwner;
            this.formType = formType;
            m_dialog = dialog;
        }
        public FormActivator(Form formOwner, Type formType, bool dialog, params object[] args)
        {
            this.owner = formOwner;
            this.formType = formType;
            m_dialog = dialog;
            m_args = args;
        }
        public DialogResult Activate()
        {
            if (m_dialog)
            {

                return ((Form)Activator.CreateInstance(formType)).ShowDialog(owner);
            }
            if (m_args == null)
                ((Form)Activator.CreateInstance(formType)).Show(owner);
            else
                ((Form)Activator.CreateInstance(formType, m_args)).Show(owner);
            return DialogResult.OK;
        }

    }
    public class DocumentActivator : IiERPCommandActivator
    {
        IGenericDocumentClientHandler handler;
        public DocumentActivator(IGenericDocumentClientHandler h)
        {
            handler = h;
        }
        #region IiERPCommandActivator Members

        public virtual DialogResult Activate()
        {
            handler.setAccountingClient(INTAPS.Accounting.Client.AccountingClient.AccountingClientDefaultInstance);
            object e = handler.CreateEditor(false);
            handler.SetEditorDocument(e, null);
            if (e is Form)
            {
                ((Form)e).Show(INTAPS.UI.UIFormApplicationBase.MainForm);
            }
            return DialogResult.OK;
        }
        #endregion
    }

    public class DocumentWithRelationActivator : IiERPCommandActivator
    {
        bERPClientDocumentHandler handler;
        TradeRelation _relation;
        public DocumentWithRelationActivator(bERPClientDocumentHandler h, TradeRelation relation)
        {
            handler = h;
            _relation = relation;
        }
        #region IiERPCommandActivator Members

        public virtual DialogResult Activate()
        {
            Globals.CurrentCode = _relation.Code;
            Globals.DocumentWithPaymentTypeActivated = true;
            handler.setAccountingClient(INTAPS.Accounting.Client.AccountingClient.AccountingClientDefaultInstance);
            object e = handler.CreateEditor(Globals.GetActivationParameter());
            handler.SetEditorDocument(e, null);
            if (e is Form)
            {
                ((Form)e).Show(INTAPS.UI.UIFormApplicationBase.MainForm as IWin32Window);
            }
            return DialogResult.OK;
        }
        #endregion
    }
    public class DocumentWithPaymentTypeActivator:IiERPCommandActivator
    {
        #region IiERPCommandActivator Members

        bERPClientDocumentHandler handler;
        BizNetPaymentMethod method;
        int employeeID;
        int bankMainAccountID;
        string code;
        PurchaseAndSalesData data;
        BondPaymentDocument bondDocument;
        Form m_parentForm=null;

        public Form ParentForm
        {
            get { return m_parentForm; }
            set { m_parentForm = value; }
        }
        public DocumentWithPaymentTypeActivator(bERPClientDocumentHandler h,BizNetPaymentMethod method,int accountID)
        {
            handler = h;
            this.method=method;
            this.employeeID = accountID;
            bankMainAccountID = accountID;
        }

        public DocumentWithPaymentTypeActivator(bERPClientDocumentHandler h, BizNetPaymentMethod method, string code)
        {
            handler = h;
            this.method = method;
            this.code = code;
        }
        public DocumentWithPaymentTypeActivator(bERPClientDocumentHandler h, BizNetPaymentMethod method, PurchaseAndSalesData data)
        {
            handler = h;
            this.method = method;
            this.data = data;
        }
        public DocumentWithPaymentTypeActivator(bERPClientDocumentHandler h, BizNetPaymentMethod method, BondPaymentDocument bondPaymentDocument)
        {
            handler = h;
            this.method = method;
            bondDocument = bondPaymentDocument;
        }

        public DialogResult Activate()
        {
            Globals.CurrentPaymentMethod = method;
            Globals.CurrentEmployeeID = employeeID;
            if (bondDocument == null)
                Globals.CurrentCode = this.code;
            else
                Globals.CurrentCode = bondDocument.customerCode;
            Globals.DocumentWithPaymentTypeActivated = true;
            Globals.CurrentPurchaseAndSalesData = data;
            Globals.CurrentBondPaymentDocument = bondDocument;
            Globals.CurrentMainAccountID = bankMainAccountID;
            handler.setAccountingClient(INTAPS.Accounting.Client.AccountingClient.AccountingClientDefaultInstance);
            object e = handler.CreateEditor(Globals.GetActivationParameter());
            handler.SetEditorDocument(e, null);
            if (e is Form)
            {
                ((Form)e).Show(INTAPS.UI.UIFormApplicationBase.MainForm as IWin32Window);
            }
            return DialogResult.OK;
        }
        #endregion
    }
}
