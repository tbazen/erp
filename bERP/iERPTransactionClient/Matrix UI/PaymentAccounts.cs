using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.UI.ButtonGrid;
using System.Drawing;
using System.IO;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    public class PaymentAccountsList : ObjectBGList<ButtonGridItem>
    {
        ButtonGrid buttonGrid;
        public PaymentAccountsList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        public override bool Searchable
        {
            get { return false; }
        }
        protected virtual bool canIncludeAccount(int csAccountID)
        {
            return true;
        }
        protected virtual ButtonGridItem CreateButtonItem(object account, Image buttonImage, PurchaseAndSalesData data)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage(((CashAccount)account).code, ((CashAccount)account).name, buttonImage);
            data.assetAccountID = ((CashAccount)account).csAccountID;
            if (data.itemTransactionType == ItemTransactionType.Purchase)
            {
                buttonGrid.AddButtonGridItem(button, new SuppliersPurchaseList(buttonGrid, button, data));
                ButtonGridHelper.SetHeaderModeText(button, "Select Supplier");

            }
            else
            {
                buttonGrid.AddButtonGridItem(button, new CustomersSalesList(buttonGrid, button, data));
                ButtonGridHelper.SetHeaderModeText(button, "Select Customer");
            }

            return button;
        }
        public override ButtonGridItem[] CreateButton(ButtonGridItem obj)
        {
            return new ButtonGridItem[] { obj };
        }
        public override List<ButtonGridItem> LoadData(string query)
        {
            return null;
        }
        protected virtual ButtonGridItem[] CreatePaymentButtonItems(string itemCaption)
        {
            return null;
        }


        public override string searchLabel
        {
            get { throw new NotImplementedException(); }
        }
    }

    public class CashAccountsList : PaymentAccountsList
    {
        ButtonGrid buttonGrid;
        PurchaseAndSalesData purchaseAndSalesData;
        public CashAccountsList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        public CashAccountsList(ButtonGrid grid,ButtonGridItem parentItem,PurchaseAndSalesData data)
            :base(grid,parentItem)
        {
            buttonGrid = grid;
            purchaseAndSalesData = data;
        }
        protected override bool canIncludeAccount(int csAccountID)
        {
            if (purchaseAndSalesData.itemTransactionType == ItemTransactionType.Purchase)
            {
                return Account.AmountGreater(INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(csAccountID, DateTime.Now), 0);
            }
            else
            {
                return true;
            }
        }

        public override List<ButtonGridItem> LoadData(string query)
        {
            List<ButtonGridItem> ret = new List<ButtonGridItem>();
            CashAccount[] cashAccounts = iERPTransactionClient.GetAllCashAccounts(true);
            ret.Add(ButtonGridHelper.CreateGroupButton("Cash Accounts"));
            foreach (CashAccount cashAccount in cashAccounts)
            {
                if (canIncludeAccount(cashAccount.csAccountID))
                {
                    ret.Add(CreateButtonItem(cashAccount, Properties.Resources.cash, purchaseAndSalesData));
                }
            }
            return ret;
        }

    }
    public class AllBankAccountsList : ObjectBGList<ButtonGridItem>
    {
        ButtonGrid buttonGrid;
        int _docType;
        public AllBankAccountsList(ButtonGrid grid, ButtonGridItem parentItem,int docType)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
            _docType = docType;
        }
        
        
        public override List<ButtonGridItem> LoadData(string query)
        {
            List<ButtonGridItem> ret = new List<ButtonGridItem>();
            BankAccountInfo[] bankAccounts = iERPTransactionClient.GetAllBankAccounts();
            ret.Add(ButtonGridHelper.CreateGroupButton("Bank Accounts"));
            foreach (BankAccountInfo bankAccount in bankAccounts)
            {
                ret.Add(CreateButtonItem(bankAccount, Properties.Resources.Check));
            }
            return ret;
        }

        protected ButtonGridItem CreateButtonItem(object account, Image buttonImage)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage(((BankAccountInfo)account).mainCsAccount.ToString(), ((BankAccountInfo)account).BankBranchAccount, buttonImage);
            button.Tag = new BGABankAccount(_docType,((BankAccountInfo)account).mainCsAccount);
            return button;
        }

        public override ButtonGridItem[] CreateButton(ButtonGridItem obj)
        {
            return new ButtonGridItem[] { obj };
        }

        public override bool Searchable
        {
            get { return false; }
        }

        public override string searchLabel
        {
            get { throw new NotImplementedException(); }
        }
    }
    
    public class BankAccountsList : PaymentAccountsList
    {
        ButtonGrid buttonGrid;
        PurchaseAndSalesData purchaseAndSalesData;

        public BankAccountsList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        public BankAccountsList(ButtonGrid grid,ButtonGridItem parentItem,PurchaseAndSalesData data)
            :base(grid,parentItem)
        {
            buttonGrid = grid;
            purchaseAndSalesData = data;
        }
        protected override bool canIncludeAccount(int csAccountID)
        {
            if (purchaseAndSalesData.itemTransactionType == ItemTransactionType.Purchase)
            {
                return AccountBase.AmountGreater(INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(csAccountID, DateTime.Now), 0);
            }
            else
            {
                return true;
            }
        }
        public override List<ButtonGridItem> LoadData(string query)
        {
            List<ButtonGridItem> ret = new List<ButtonGridItem>();
            //if (string.IsNullOrEmpty(query))
            //    return ret;
            BankAccountInfo[] bankAccounts = iERPTransactionClient.GetAllBankAccounts();
            ret.Add(ButtonGridHelper.CreateGroupButton("Bank Accounts"));
            foreach (BankAccountInfo bankAccount in bankAccounts)
            {
                if (canIncludeAccount(bankAccount.mainCsAccount))
                {
                    ret.Add(CreateButtonItem(bankAccount, Properties.Resources.Check, purchaseAndSalesData));
                }
            }
            return ret;
        }

        protected override ButtonGridItem CreateButtonItem(object account, Image buttonImage, PurchaseAndSalesData data)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage(((BankAccountInfo)account).mainCsAccount.ToString(), ((BankAccountInfo)account).bankName, buttonImage);
            data.assetAccountID = ((BankAccountInfo)account).mainCsAccount;
            if (data.itemTransactionType == ItemTransactionType.Purchase)
                buttonGrid.AddButtonGridItem(button, new SuppliersPurchaseList(buttonGrid, button, data));
            else
                buttonGrid.AddButtonGridItem(button, new CustomersSalesList(buttonGrid, button, data));
            return button;
        }
    }
    public class StaffPaymentMethodListsForPayment : PaymentAccountsList
    {
        ButtonGrid buttonGrid;
        ButtonGridItem parentGridItem;
        StaffPaymentListType staffPaymentListType;
        IButtonChildSource buttonItems;
        public StaffPaymentMethodListsForPayment(ButtonGrid grid, ButtonGridItem parentItem,StaffPaymentListType paymentListType)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
            parentGridItem = parentItem;
            staffPaymentListType = paymentListType;
           
        }
        public StaffPaymentMethodListsForPayment(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        public override List<ButtonGridItem> LoadData(string query)
        {
            List<ButtonGridItem> ret = new List<ButtonGridItem>();
            ButtonGridItem[] buttonGridItems = CreatePaymentButtonItems("");
            foreach (ButtonGridItem item in buttonGridItems)
            {
                ret.Add(item);
            }
            return ret;
        }

        protected override ButtonGridItem[] CreatePaymentButtonItems(string itemCaption)
        {
            ButtonGridBodyButtonItem itemCash;
            ButtonGridBodyButtonItem itemCheck, itemCPOFromCash, itemBankTransferFromCash;
            ButtonGridBodyButtonItem itemCPOFromBank, itemBankTransferFromBank;

            ButtonGridItem[] buttonGridItems = new ButtonGridItem[6];

            itemCash = ButtonGridHelper.CreateButtonWithImage("empCash" + staffPaymentListType, "Cash " + itemCaption, Properties.Resources.cash);
            RelateChildButtonItems(BizNetPaymentMethod.Cash, itemCash);
            buttonGrid.AddButtonGridItem(itemCash, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCash, "Select Staff");

            itemCheck = ButtonGridHelper.CreateButtonWithImage("empCheck" + staffPaymentListType, "Check " + itemCaption, Properties.Resources.Check);
            RelateChildButtonItems(BizNetPaymentMethod.Check, itemCheck);
            buttonGrid.AddButtonGridItem(itemCheck, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCheck, "Select Staff");

            itemCPOFromCash = ButtonGridHelper.CreateButtonWithImage("empCPOFromCash" + staffPaymentListType, "CPO From Cash " + itemCaption, Properties.Resources.cpocash);
            RelateChildButtonItems(BizNetPaymentMethod.CPOFromCash, itemCPOFromCash);
            buttonGrid.AddButtonGridItem(itemCPOFromCash, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCPOFromCash, "Select Staff");


            itemCPOFromBank = ButtonGridHelper.CreateButtonWithImage("empCPOFromBank" + staffPaymentListType, "CPO From Bank " + itemCaption, Properties.Resources.cpobank);
            RelateChildButtonItems(BizNetPaymentMethod.CPOFromBankAccount, itemCPOFromBank);
            buttonGrid.AddButtonGridItem(itemCPOFromBank, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCPOFromBank, "Select Staff");

            itemBankTransferFromCash = ButtonGridHelper.CreateButtonWithImage("empBankTransferFromCash" + staffPaymentListType, "Bank Transfer From Cash " + itemCaption, Properties.Resources.bank_transfer_cash);
            RelateChildButtonItems(BizNetPaymentMethod.BankTransferFromCash, itemBankTransferFromCash);
            buttonGrid.AddButtonGridItem(itemBankTransferFromCash, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemBankTransferFromCash, "Select Staff");

            itemBankTransferFromBank = ButtonGridHelper.CreateButtonWithImage("empBankTransferFromBank" + staffPaymentListType, "Bank Transfer From Bank " + itemCaption, Properties.Resources.bank_transfer);
            RelateChildButtonItems(BizNetPaymentMethod.BankTransferFromBankAccount, itemBankTransferFromBank);
            buttonGrid.AddButtonGridItem(itemBankTransferFromBank, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemBankTransferFromBank, "Select Staff");

            buttonGridItems[0] = itemCash;
            buttonGridItems[1] = itemCheck;
            buttonGridItems[2] = itemCPOFromCash;
            buttonGridItems[3] = itemCPOFromBank;
            buttonGridItems[4] = itemBankTransferFromCash;
            buttonGridItems[5] = itemBankTransferFromBank;

            return buttonGridItems;
        }

        private void RelateChildButtonItems(BizNetPaymentMethod paymentMethod, ButtonGridBodyButtonItem parentItem)
        {
            switch (staffPaymentListType)
            {
                case StaffPaymentListType.ExpenseAdvanceList:
                    StaffWithExpenseAdvanceList expenseAdvance = new StaffWithExpenseAdvanceList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = expenseAdvance;
                    break;
                case StaffPaymentListType.SalaryAdvanceList:
                    StaffWithSalaryAdvanceList salaryAdvance = new StaffWithSalaryAdvanceList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = salaryAdvance;
                    break;
                case StaffPaymentListType.LongTermLoanList:
                    StaffWithLongTermLoanList longTermLoan = new StaffWithLongTermLoanList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = longTermLoan;
                    break;
                case StaffPaymentListType.LongTermLoanWithServiceChargeList:
                    StaffWithLongTermLoanWithServiceChargeList longTermLoanWithServiceCharge = new StaffWithLongTermLoanWithServiceChargeList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = longTermLoanWithServiceCharge;
                    break;
                case StaffPaymentListType.UnclaimedSalaryList:
                    StaffWithUnclaimedSalaryList unclaimedSalary = new StaffWithUnclaimedSalaryList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = unclaimedSalary;
                    break;
                case StaffPaymentListType.StaffLoanPayment:
                    StaffLoanPaymentList staffLoanPayment = new StaffLoanPaymentList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = staffLoanPayment;
                    break;
                case StaffPaymentListType.PerDiemPayment:
                    StaffPerDiemPaymentList staffPerDiem = new StaffPerDiemPaymentList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = staffPerDiem;
                    break;
                case StaffPaymentListType.ShareHoldersLoanList:
                    ShareHoldersLoanList shareHoldersLoan = new ShareHoldersLoanList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = shareHoldersLoan;
                    break;
                default:
                    break;
            }
        }
    }
    public class StaffPaymentMethodListsForReturn : PaymentAccountsList
    {
        ButtonGrid buttonGrid;
        ButtonGridItem parentGridItem;
        StaffReturnListType staffReturnListType;
        IButtonChildSource buttonItems;
        public StaffPaymentMethodListsForReturn(ButtonGrid grid, ButtonGridItem parentItem, StaffReturnListType returnListType)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
            parentGridItem = parentItem;
            staffReturnListType = returnListType;

        }
        public StaffPaymentMethodListsForReturn(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        public override List<ButtonGridItem> LoadData(string query)
        {
            List<ButtonGridItem> ret = new List<ButtonGridItem>();
            ButtonGridItem[] buttonGridItems = CreatePaymentButtonItems("");
            foreach (ButtonGridItem item in buttonGridItems)
            {
                ret.Add(item);
            }
            return ret;
        }

        protected override ButtonGridItem[] CreatePaymentButtonItems(string itemCaption)
        {
            ButtonGridBodyButtonItem itemCash;
            ButtonGridBodyButtonItem itemCheck;
            ButtonGridBodyButtonItem itemCPOFromBank, itemBankTransferFromBank;

            ButtonGridItem[] buttonGridItems = new ButtonGridItem[4];

            itemCash = ButtonGridHelper.CreateButtonWithImage("empCash" + staffReturnListType, "Cash " + itemCaption, Properties.Resources.cash);
            RelateChildButtonItems(BizNetPaymentMethod.Cash, itemCash);
            buttonGrid.AddButtonGridItem(itemCash, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCash, "Select Staff");

            itemCheck = ButtonGridHelper.CreateButtonWithImage("empCheck" + staffReturnListType, "Check " + itemCaption, Properties.Resources.Check);
            RelateChildButtonItems(BizNetPaymentMethod.Check, itemCheck);
            buttonGrid.AddButtonGridItem(itemCheck, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCheck, "Select Staff");


            itemCPOFromBank = ButtonGridHelper.CreateButtonWithImage("empCPOFromBank" + staffReturnListType, "CPO From Bank " + itemCaption, Properties.Resources.cpobank);
            RelateChildButtonItems(BizNetPaymentMethod.CPOFromBankAccount, itemCPOFromBank);
            buttonGrid.AddButtonGridItem(itemCPOFromBank, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCPOFromBank, "Select Staff");

            itemBankTransferFromBank = ButtonGridHelper.CreateButtonWithImage("empBankTransferFromBank" + staffReturnListType, "Bank Transfer From Bank " + itemCaption, Properties.Resources.bank_transfer);
            RelateChildButtonItems(BizNetPaymentMethod.BankTransferFromBankAccount, itemBankTransferFromBank);
            buttonGrid.AddButtonGridItem(itemBankTransferFromBank, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemBankTransferFromBank, "Select Staff");

            buttonGridItems[0] = itemCash;
            buttonGridItems[1] = itemCheck;
            buttonGridItems[2] = itemCPOFromBank;
            buttonGridItems[3] = itemBankTransferFromBank;

            return buttonGridItems;
        }

        private void RelateChildButtonItems(BizNetPaymentMethod paymentMethod, ButtonGridBodyButtonItem parentItem)
        {
            
            switch (staffReturnListType)
            {
                case StaffReturnListType.ExpenseAdvanceReturnList:
                    StaffWithExpenseAdvanceReturnList expenseAdvance = new StaffWithExpenseAdvanceReturnList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = expenseAdvance;
                    break;
                case StaffReturnListType.ShortTermLoanReturnList:
                    StaffWithShortTermLoanReturnList shortTermLoan = new StaffWithShortTermLoanReturnList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = shortTermLoan;
                    break;
                case StaffReturnListType.LongTermLoanReturnList:
                    StaffWithLongTermLoanReturnList longTermLoan = new StaffWithLongTermLoanReturnList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = longTermLoan;
                    break;
                case StaffReturnListType.LoanFromStaff:
                    LoanFromStaffList loanFromStaff = new LoanFromStaffList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = loanFromStaff;
                    break;
                case StaffReturnListType.ShareHoldersLoanReturnList:
                    ShareHoldersLoanReturnList shareHoldersLoanReturn = new ShareHoldersLoanReturnList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = shareHoldersLoanReturn;
                    break;
                default:
                    break;
            }
        }
    }
    public class SupplierPaymentMethodLists: PaymentAccountsList
    {
        ButtonGrid buttonGrid;
        ButtonGridItem parentGridItem;
        SupplierListType supplierListType;
        IButtonChildSource buttonItems;
        public SupplierPaymentMethodLists(ButtonGrid grid, ButtonGridItem parentItem, SupplierListType supListType)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
            parentGridItem = parentItem;
            supplierListType = supListType;

        }
        public SupplierPaymentMethodLists(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        public override List<ButtonGridItem> LoadData(string query)
        {
            List<ButtonGridItem> ret = new List<ButtonGridItem>();
            ButtonGridItem[] buttonGridItems;
            if (supplierListType != SupplierListType.AdvanceReturnList)
                buttonGridItems = CreatePaymentButtonItems("");
            else
                buttonGridItems = CreatePaymentButtonItemsForReturn("");
            foreach (ButtonGridItem item in buttonGridItems)
            {
                ret.Add(item);
            }
            return ret;
        }

        protected override ButtonGridItem[] CreatePaymentButtonItems(string itemCaption)
        {
            ButtonGridBodyButtonItem itemCash;
            ButtonGridBodyButtonItem itemCheck, itemCPOFromCash, itemBankTransferFromCash;
            ButtonGridBodyButtonItem itemCPOFromBank, itemBankTransferFromBank;

            ButtonGridItem[] buttonGridItems = new ButtonGridItem[6];

            itemCash = ButtonGridHelper.CreateButtonWithImage("suppCash" + supplierListType, "Cash " + itemCaption, Properties.Resources.cash);
            RelateChildButtonItems(BizNetPaymentMethod.Cash, itemCash);
            buttonGrid.AddButtonGridItem(itemCash, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCash, "Select Supplier");

            itemCheck = ButtonGridHelper.CreateButtonWithImage("suppCheck" + supplierListType, "Check " + itemCaption, Properties.Resources.Check);
            RelateChildButtonItems(BizNetPaymentMethod.Check, itemCheck);
            buttonGrid.AddButtonGridItem(itemCheck, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCheck, "Select Supplier");

            itemCPOFromCash = ButtonGridHelper.CreateButtonWithImage("suppCPOFromCash" + supplierListType, "CPO From Cash " + itemCaption, Properties.Resources.cpocash);
            RelateChildButtonItems(BizNetPaymentMethod.CPOFromCash, itemCPOFromCash);
            buttonGrid.AddButtonGridItem(itemCPOFromCash, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCPOFromCash, "Select Supplier");


            itemCPOFromBank = ButtonGridHelper.CreateButtonWithImage("suppCPOFromBank" + supplierListType, "CPO From Bank " + itemCaption, Properties.Resources.cpobank);
            RelateChildButtonItems(BizNetPaymentMethod.CPOFromBankAccount, itemCPOFromBank);
            buttonGrid.AddButtonGridItem(itemCPOFromBank, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCPOFromBank, "Select Supplier");

            itemBankTransferFromCash = ButtonGridHelper.CreateButtonWithImage("suppBankTransferFromCash" + supplierListType, "Bank Transfer From Cash " + itemCaption, Properties.Resources.bank_transfer_cash);
            RelateChildButtonItems(BizNetPaymentMethod.BankTransferFromCash, itemBankTransferFromCash);
            buttonGrid.AddButtonGridItem(itemBankTransferFromCash, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemBankTransferFromCash, "Select Supplier");

            itemBankTransferFromBank = ButtonGridHelper.CreateButtonWithImage("suppBankTransferFromBank" + supplierListType, "Bank Transfer From Bank " + itemCaption, Properties.Resources.bank_transfer);
            RelateChildButtonItems(BizNetPaymentMethod.BankTransferFromBankAccount, itemBankTransferFromBank);
            buttonGrid.AddButtonGridItem(itemBankTransferFromBank, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemBankTransferFromBank, "Select Supplier");

            buttonGridItems[0] = itemCash;
            buttonGridItems[1] = itemCheck;
            buttonGridItems[2] = itemCPOFromCash;
            buttonGridItems[3] = itemCPOFromBank;
            buttonGridItems[4] = itemBankTransferFromCash;
            buttonGridItems[5] = itemBankTransferFromBank;

            return buttonGridItems;
        }

        private ButtonGridItem[] CreatePaymentButtonItemsForReturn(string itemCaption)
        {
            ButtonGridBodyButtonItem itemCash;
            ButtonGridBodyButtonItem itemCheck;
            ButtonGridBodyButtonItem itemCPOFromBank, itemBankTransferFromBank;

            ButtonGridItem[] buttonGridItems = new ButtonGridItem[4];

            itemCash = ButtonGridHelper.CreateButtonWithImage("suppCash" + supplierListType, "Cash " + itemCaption, Properties.Resources.cash);
            RelateChildButtonItems(BizNetPaymentMethod.Cash, itemCash);
            buttonGrid.AddButtonGridItem(itemCash, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCash, "Select Supplier");

            itemCheck = ButtonGridHelper.CreateButtonWithImage("suppCheck" + supplierListType, "Check " + itemCaption, Properties.Resources.Check);
            RelateChildButtonItems(BizNetPaymentMethod.Check, itemCheck);
            buttonGrid.AddButtonGridItem(itemCheck, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCheck, "Select Supplier");


            itemCPOFromBank = ButtonGridHelper.CreateButtonWithImage("suppCPOFromBank" + supplierListType, "CPO From Bank " + itemCaption, Properties.Resources.cpobank);
            RelateChildButtonItems(BizNetPaymentMethod.CPOFromBankAccount, itemCPOFromBank);
            buttonGrid.AddButtonGridItem(itemCPOFromBank, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCPOFromBank, "Select Supplier");

            itemBankTransferFromBank = ButtonGridHelper.CreateButtonWithImage("suppBankTransferFromBank" + supplierListType, "Bank Transfer From Bank " + itemCaption, Properties.Resources.bank_transfer);
            RelateChildButtonItems(BizNetPaymentMethod.BankTransferFromBankAccount, itemBankTransferFromBank);
            buttonGrid.AddButtonGridItem(itemBankTransferFromBank, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemBankTransferFromBank, "Select Supplier");

            buttonGridItems[0] = itemCash;
            buttonGridItems[1] = itemCheck;
            buttonGridItems[2] = itemCPOFromBank;
            buttonGridItems[3] = itemBankTransferFromBank;

            return buttonGridItems;
        }
        private void RelateChildButtonItems(BizNetPaymentMethod paymentMethod, ButtonGridBodyButtonItem parentItem)
        {
            switch (supplierListType)
            {
                case SupplierListType.AdvanceList:
                    SuppliersAdvanceList advanceList = new SuppliersAdvanceList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = advanceList;
                    break;
                case SupplierListType.PayableList:
                    SuppliersPayableList payableList = new SuppliersPayableList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = payableList;
                    break;
                case SupplierListType.AdvanceReturnList:
                    SuppliersAdvanceReturnList advanceReturnList = new SuppliersAdvanceReturnList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = advanceReturnList;
                    break;
            }
        }


    }
    public class CustomerPaymentMethodLists : PaymentAccountsList
    {
        ButtonGrid buttonGrid;
        ButtonGridItem parentGridItem;
        CustomerListType customerListType;
        IButtonChildSource buttonItems;
        public CustomerPaymentMethodLists(ButtonGrid grid, ButtonGridItem parentItem, CustomerListType custListType)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
            parentGridItem = parentItem;
            customerListType = custListType;

        }
        public CustomerPaymentMethodLists(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        public override List<ButtonGridItem> LoadData(string query)
        {
            List<ButtonGridItem> ret = new List<ButtonGridItem>();
            ButtonGridItem[] buttonGridItems;
            if (customerListType != CustomerListType.AdvanceReturnList && customerListType != CustomerListType.BondPaymentList
                && customerListType != CustomerListType.BondReturnList)
                buttonGridItems = CreatePaymentButtonItems("");
            else if (customerListType == CustomerListType.BondPaymentList)
            {
                buttonGridItems = CreatePaymentButtonItemsForBondPayment("");
            }
            else if (customerListType == CustomerListType.BondReturnList)
            {
                buttonGridItems = CreatePaymentButtonItemsForBondReturn("");
            }
            else
                buttonGridItems = CreatePaymentButtonItemsForReturn("");
            foreach (ButtonGridItem item in buttonGridItems)
            {
                ret.Add(item);
            }
            return ret;
        }

        protected override ButtonGridItem[] CreatePaymentButtonItems(string itemCaption)
        {
            ButtonGridBodyButtonItem itemCash;
            ButtonGridBodyButtonItem itemCheck;
            ButtonGridBodyButtonItem itemCPOFromBank, itemBankTransferFromBank;

            ButtonGridItem[] buttonGridItems = new ButtonGridItem[4];

            itemCash = ButtonGridHelper.CreateButtonWithImage("custCash" + customerListType, "Cash " + itemCaption, Properties.Resources.cash);
            RelateChildButtonItems(BizNetPaymentMethod.Cash, itemCash);
            buttonGrid.AddButtonGridItem(itemCash, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCash, "Select Customer");

            itemCheck = ButtonGridHelper.CreateButtonWithImage("custCheck" + customerListType, "Check " + itemCaption, Properties.Resources.Check);
            RelateChildButtonItems(BizNetPaymentMethod.Check, itemCheck);
            buttonGrid.AddButtonGridItem(itemCheck, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCheck, "Select Customer");


            itemCPOFromBank = ButtonGridHelper.CreateButtonWithImage("custCPOFromBank" + customerListType, "CPO From Bank " + itemCaption, Properties.Resources.cpobank);
            RelateChildButtonItems(BizNetPaymentMethod.CPOFromBankAccount, itemCPOFromBank);
            buttonGrid.AddButtonGridItem(itemCPOFromBank, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCPOFromBank, "Select Customer");

            itemBankTransferFromBank = ButtonGridHelper.CreateButtonWithImage("custBankTransferFromBank" + customerListType, "Bank Transfer From Bank " + itemCaption, Properties.Resources.bank_transfer);
            RelateChildButtonItems(BizNetPaymentMethod.BankTransferFromBankAccount, itemBankTransferFromBank);
            buttonGrid.AddButtonGridItem(itemBankTransferFromBank, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemBankTransferFromBank, "Select Customer");

            buttonGridItems[0] = itemCash;
            buttonGridItems[1] = itemCheck;
            buttonGridItems[2] = itemCPOFromBank;
            buttonGridItems[3] = itemBankTransferFromBank;

            return buttonGridItems;
           
        }

        private ButtonGridItem[] CreatePaymentButtonItemsForBondPayment(string itemCaption)
        {

            ButtonGridBodyButtonItem itemCPOFromCash;
            ButtonGridBodyButtonItem itemCPOFromBank;

            ButtonGridItem[] buttonGridItems = new ButtonGridItem[2];

            itemCPOFromCash = ButtonGridHelper.CreateButtonWithImage("custCPOFromCash" + customerListType, "CPO From Cash " + itemCaption, Properties.Resources.cpocash);
            RelateChildButtonItems(BizNetPaymentMethod.CPOFromCash, itemCPOFromCash);
            buttonGrid.AddButtonGridItem(itemCPOFromCash, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCPOFromCash, "Select customer");

            itemCPOFromBank = ButtonGridHelper.CreateButtonWithImage("custCPOFromBank" + customerListType, "CPO From Bank " + itemCaption, Properties.Resources.cpobank);
            RelateChildButtonItems(BizNetPaymentMethod.CPOFromBankAccount, itemCPOFromBank);
            buttonGrid.AddButtonGridItem(itemCPOFromBank, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCPOFromBank, "Select customer");

            buttonGridItems[0] = itemCPOFromCash;
            buttonGridItems[1] = itemCPOFromBank;

            return buttonGridItems;
        }
        private ButtonGridItem[] CreatePaymentButtonItemsForBondReturn(string itemCaption)
        {
            ButtonGridBodyButtonItem itemCash;
            ButtonGridBodyButtonItem itemCheck;
            ButtonGridBodyButtonItem itemBankTransferFromBank;

            ButtonGridItem[] buttonGridItems = new ButtonGridItem[3];

            itemCash = ButtonGridHelper.CreateButtonWithImage("bondCash" + customerListType, "Cash " + itemCaption, Properties.Resources.cash);
            RelateChildButtonItems(BizNetPaymentMethod.Cash, itemCash);
            buttonGrid.AddButtonGridItem(itemCash, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCash, "Select Bond");

            itemCheck = ButtonGridHelper.CreateButtonWithImage("bondCheck" + customerListType, "Check " + itemCaption, Properties.Resources.Check);
            RelateChildButtonItems(BizNetPaymentMethod.Check, itemCheck);
            buttonGrid.AddButtonGridItem(itemCheck, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCheck, "Select Bond");

            itemBankTransferFromBank = ButtonGridHelper.CreateButtonWithImage("bondBankTransfer" + customerListType, "Bank Transfer From Bank " + itemCaption, Properties.Resources.bank_transfer);
            RelateChildButtonItems(BizNetPaymentMethod.BankTransferFromBankAccount, itemBankTransferFromBank);
            buttonGrid.AddButtonGridItem(itemBankTransferFromBank, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemBankTransferFromBank, "Select Bond");

            buttonGridItems[0] = itemCash;
            buttonGridItems[1] = itemCheck;
            buttonGridItems[2] = itemBankTransferFromBank;

            return buttonGridItems;  
        }
        private ButtonGridItem[] CreatePaymentButtonItemsForReturn(string itemCaption)
        {
            ButtonGridBodyButtonItem itemCash;
            ButtonGridBodyButtonItem itemCheck, itemCPOFromCash, itemBankTransferFromCash;
            ButtonGridBodyButtonItem itemCPOFromBank, itemBankTransferFromBank;

            ButtonGridItem[] buttonGridItems = new ButtonGridItem[6];

            itemCash = ButtonGridHelper.CreateButtonWithImage("custCash" + customerListType, "Cash " + itemCaption, Properties.Resources.cash);
            RelateChildButtonItems(BizNetPaymentMethod.Cash, itemCash);
            buttonGrid.AddButtonGridItem(itemCash, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCash, "Select Customer");

            itemCheck = ButtonGridHelper.CreateButtonWithImage("custCheck" + customerListType, "Check " + itemCaption, Properties.Resources.Check);
            RelateChildButtonItems(BizNetPaymentMethod.Check, itemCheck);
            buttonGrid.AddButtonGridItem(itemCheck, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCheck, "Select Customer");

            itemCPOFromCash = ButtonGridHelper.CreateButtonWithImage("custCPOFromCash" + customerListType, "CPO From Cash " + itemCaption, Properties.Resources.cpocash);
            RelateChildButtonItems(BizNetPaymentMethod.CPOFromCash, itemCPOFromCash);
            buttonGrid.AddButtonGridItem(itemCPOFromCash, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCPOFromCash, "Select Customer");


            itemCPOFromBank = ButtonGridHelper.CreateButtonWithImage("custCPOFromBank" + customerListType, "CPO From Bank " + itemCaption, Properties.Resources.cpobank);
            RelateChildButtonItems(BizNetPaymentMethod.CPOFromBankAccount, itemCPOFromBank);
            buttonGrid.AddButtonGridItem(itemCPOFromBank, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemCPOFromBank, "Select Customer");

            itemBankTransferFromCash = ButtonGridHelper.CreateButtonWithImage("custBankTransferFromCash" + customerListType, "Bank Transfer From Cash " + itemCaption, Properties.Resources.bank_transfer_cash);
            RelateChildButtonItems(BizNetPaymentMethod.BankTransferFromCash, itemBankTransferFromCash);
            buttonGrid.AddButtonGridItem(itemBankTransferFromCash, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemBankTransferFromCash, "Select Customer");

            itemBankTransferFromBank = ButtonGridHelper.CreateButtonWithImage("custBankTransferFromBank" + customerListType, "Bank Transfer From Bank " + itemCaption, Properties.Resources.bank_transfer);
            RelateChildButtonItems(BizNetPaymentMethod.BankTransferFromBankAccount, itemBankTransferFromBank);
            buttonGrid.AddButtonGridItem(itemBankTransferFromBank, buttonItems);
            ButtonGridHelper.SetHeaderModeText(itemBankTransferFromBank, "Select Customer");

            buttonGridItems[0] = itemCash;
            buttonGridItems[1] = itemCheck;
            buttonGridItems[2] = itemCPOFromCash;
            buttonGridItems[3] = itemCPOFromBank;
            buttonGridItems[4] = itemBankTransferFromCash;
            buttonGridItems[5] = itemBankTransferFromBank;

            return buttonGridItems;
        }
        private void RelateChildButtonItems(BizNetPaymentMethod paymentMethod, ButtonGridBodyButtonItem parentItem)
        {   
            switch (customerListType)
            {
                case CustomerListType.PayableList:
                    CustomersPayableList payableList = new CustomersPayableList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = payableList;
                    break;
                case CustomerListType.CreditList:
                    CustomersCreditList creditList = new CustomersCreditList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = creditList;
                    break;
                case CustomerListType.AdvanceReturnList:
                    CustomersAdvanceReturnList advanceReturnList = new CustomersAdvanceReturnList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = advanceReturnList;
                    break;
                case CustomerListType.BondPaymentList:
                    CustomersBondPaymentList bondPaymentList = new CustomersBondPaymentList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = bondPaymentList;
                    break;
                case CustomerListType.BondReturnList:
                    CustomersBondReturnList bondReturnList = new CustomersBondReturnList(buttonGrid, parentItem, paymentMethod);
                    buttonItems = bondReturnList;
                    break;
            }
        }
    }
    public class PurchaseAndSalesPaymentLists:PaymentAccountsList
    {
        ButtonGrid buttonGrid;
        IButtonChildSource buttonItems;
        PurchaseAndSalesData purchaseAndSalesData;
        public PurchaseAndSalesPaymentLists(ButtonGrid grid, ButtonGridItem parentItem,PurchaseAndSalesData data)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
            purchaseAndSalesData = data;
        }
        public PurchaseAndSalesPaymentLists(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }

        public override List<ButtonGridItem> LoadData(string query)
        {
            List<ButtonGridItem> ret = new List<ButtonGridItem>();
            ButtonGridItem[] buttonGridItems;
            if (purchaseAndSalesData.itemTransactionType == ItemTransactionType.Purchase)
                buttonGridItems = CreatePaymentButtonItems("");
            else
                buttonGridItems = CreateSalesPaymentButtonItems("");
            foreach (ButtonGridItem item in buttonGridItems)
            {
                ret.Add(item);
            }
            return ret;
        }

        protected override ButtonGridItem[] CreatePaymentButtonItems(string itemCaption)
        {
          
            ButtonGridBodyButtonItem itemCash, itemCredit;
            ButtonGridBodyButtonItem itemCheck, itemCPOFromCash, itemBankTransferFromCash;
            ButtonGridBodyButtonItem itemCPOFromBank, itemBankTransferFromBank;

            ButtonGridItem[] buttonGridItems = new ButtonGridItem[7];

            //purchaseAndSalesData.ReceivableAccountID = receivableAccount;
            //purchaseAndSalesData.agentCode = supplierCode;

            itemCredit = ButtonGridHelper.CreateButtonWithImage("suppPurchCredit", "Credit " + itemCaption, Properties.Resources.credit);
            ButtonGridHelper.SetHeaderModeText(itemCredit, "Select Supplier");
            PurchaseAndSalesData creditData = purchaseAndSalesData;
            creditData.paymentMethod = BizNetPaymentMethod.Credit;
            RelateChildButtonItems(creditData, AccountType.None, itemCredit);
            buttonGrid.AddButtonGridItem(itemCredit, buttonItems);

            itemCash = ButtonGridHelper.CreateButtonWithImage("suppPurchCash", "Cash " + itemCaption, Properties.Resources.cash);
            PurchaseAndSalesData cashData = purchaseAndSalesData;
            cashData.paymentMethod = BizNetPaymentMethod.Cash;
            RelateChildButtonItems(cashData, AccountType.Cash, itemCash);
            buttonGrid.AddButtonGridItem(itemCash, buttonItems);

            itemCheck = ButtonGridHelper.CreateButtonWithImage("suppPurchCheck", "Check " + itemCaption, Properties.Resources.Check);
            PurchaseAndSalesData checkData = purchaseAndSalesData;
            checkData.paymentMethod = BizNetPaymentMethod.Check;
            RelateChildButtonItems(checkData, AccountType.Bank, itemCheck);
            buttonGrid.AddButtonGridItem(itemCheck, buttonItems);

            itemCPOFromCash = ButtonGridHelper.CreateButtonWithImage("suppPurchCPOFromCash", "CPO From Cash " + itemCaption, Properties.Resources.cpocash);
            PurchaseAndSalesData cpoFromCashData = purchaseAndSalesData;
            cpoFromCashData.paymentMethod = BizNetPaymentMethod.CPOFromCash;
            RelateChildButtonItems(cpoFromCashData, AccountType.Cash, itemCPOFromCash);
            buttonGrid.AddButtonGridItem(itemCPOFromCash, buttonItems);

            itemCPOFromBank = ButtonGridHelper.CreateButtonWithImage("suppPurchCPOFromBank", "CPO From Bank " + itemCaption, Properties.Resources.cpobank);
            PurchaseAndSalesData cpoFromBankData = purchaseAndSalesData;
            cpoFromBankData.paymentMethod = BizNetPaymentMethod.CPOFromBankAccount;
            RelateChildButtonItems(cpoFromBankData, AccountType.Bank, itemCPOFromBank);
            buttonGrid.AddButtonGridItem(itemCPOFromBank,buttonItems);

            itemBankTransferFromCash = ButtonGridHelper.CreateButtonWithImage("suppPurchBankTranFromCash", "Bank Transfer From Cash " + itemCaption, Properties.Resources.bank_transfer_cash);
            PurchaseAndSalesData bankTranFromCashData = purchaseAndSalesData;
            bankTranFromCashData.paymentMethod = BizNetPaymentMethod.BankTransferFromCash;
            RelateChildButtonItems(bankTranFromCashData, AccountType.Cash, itemBankTransferFromCash);
            buttonGrid.AddButtonGridItem(itemBankTransferFromCash, buttonItems);

            itemBankTransferFromBank = ButtonGridHelper.CreateButtonWithImage("suppPurchBankTranFromBank", "Bank Transfer From Bank " + itemCaption, Properties.Resources.bank_transfer);
            PurchaseAndSalesData bankTranFromBankData = purchaseAndSalesData;
            bankTranFromBankData.paymentMethod = BizNetPaymentMethod.BankTransferFromBankAccount;
            RelateChildButtonItems(bankTranFromBankData, AccountType.Bank, itemBankTransferFromBank);
            buttonGrid.AddButtonGridItem(itemBankTransferFromBank, buttonItems);

            buttonGridItems[0] = itemCredit;
            buttonGridItems[1] = itemCash;
            buttonGridItems[2] = itemCheck;
            buttonGridItems[3] = itemCPOFromCash;
            buttonGridItems[4] = itemCPOFromBank;
            buttonGridItems[5] = itemBankTransferFromCash;
            buttonGridItems[6] = itemBankTransferFromBank;

            return buttonGridItems;
        }

        private ButtonGridItem[] CreateSalesPaymentButtonItems(string itemCaption)
        {
            ButtonGridBodyButtonItem itemCash, itemCredit;
            ButtonGridBodyButtonItem itemCheck;
            ButtonGridBodyButtonItem itemCPOFromBank, itemBankTransferFromBank;

            ButtonGridItem[] buttonGridItems = new ButtonGridItem[5];

            //purchaseAndSalesData.ReceivableAccountID = receivableAccount;
            //purchaseAndSalesData.agentCode = supplierCode;

            itemCredit = ButtonGridHelper.CreateButtonWithImage("suppSalesCredit", "Credit " + itemCaption, Properties.Resources.credit);
            ButtonGridHelper.SetHeaderModeText(itemCredit, "Select Supplier");
            PurchaseAndSalesData creditData = purchaseAndSalesData;
            creditData.paymentMethod = BizNetPaymentMethod.Credit;
            RelateChildButtonItems(creditData, AccountType.None, itemCredit);
            buttonGrid.AddButtonGridItem(itemCredit, buttonItems);

            itemCash = ButtonGridHelper.CreateButtonWithImage("suppSalesCash", "Cash " + itemCaption, Properties.Resources.cash);
            PurchaseAndSalesData cashData = purchaseAndSalesData;
            cashData.paymentMethod = BizNetPaymentMethod.Cash;
            RelateChildButtonItems(cashData, AccountType.Cash, itemCash);
            buttonGrid.AddButtonGridItem(itemCash, buttonItems);

            itemCheck = ButtonGridHelper.CreateButtonWithImage("suppSalesCheck", "Check " + itemCaption, Properties.Resources.Check);
            PurchaseAndSalesData checkData = purchaseAndSalesData;
            checkData.paymentMethod = BizNetPaymentMethod.Check;
            RelateChildButtonItems(checkData, AccountType.Bank, itemCheck);
            buttonGrid.AddButtonGridItem(itemCheck, buttonItems);

            itemCPOFromBank = ButtonGridHelper.CreateButtonWithImage("suppSalesCPOFromBank", "CPO From Bank " + itemCaption, Properties.Resources.cpobank);
            PurchaseAndSalesData cpoFromBankData = purchaseAndSalesData;
            cpoFromBankData.paymentMethod = BizNetPaymentMethod.CPOFromBankAccount;
            RelateChildButtonItems(cpoFromBankData, AccountType.Bank, itemCPOFromBank);
            buttonGrid.AddButtonGridItem(itemCPOFromBank, buttonItems);

            itemBankTransferFromBank = ButtonGridHelper.CreateButtonWithImage("suppSalesBankTranFromBank", "Bank Transfer From Bank " + itemCaption, Properties.Resources.bank_transfer);
            PurchaseAndSalesData bankTranFromBankData = purchaseAndSalesData;
            bankTranFromBankData.paymentMethod = BizNetPaymentMethod.BankTransferFromBankAccount;
            RelateChildButtonItems(bankTranFromBankData, AccountType.Bank, itemBankTransferFromBank);
            buttonGrid.AddButtonGridItem(itemBankTransferFromBank, buttonItems);

            buttonGridItems[0] = itemCredit;
            buttonGridItems[1] = itemCash;
            buttonGridItems[2] = itemCheck;
            buttonGridItems[3] = itemCPOFromBank;
            buttonGridItems[4] = itemBankTransferFromBank;

            return buttonGridItems;
        }
        private void RelateChildButtonItems(PurchaseAndSalesData data, AccountType accountType, ButtonGridBodyButtonItem parentItem)
        {   
            switch (accountType)
            {
                case AccountType.Cash:
                    CashAccountsList cashList = new CashAccountsList(buttonGrid, parentItem, data);
                    buttonItems = cashList;
                    break;
                case AccountType.Bank:
                    BankAccountsList bankList = new BankAccountsList(buttonGrid, parentItem, data);
                    buttonItems = bankList;
                    break;
                case AccountType.None:
                    if (purchaseAndSalesData.itemTransactionType == ItemTransactionType.Purchase)
                        buttonItems = new SuppliersPurchaseList(buttonGrid, parentItem, data);
                    else
                        buttonItems = new CustomersSalesList(buttonGrid, parentItem, data);

                    break;
                default:
                    break;
            }
        }

    }
    public class LaborPaymentLists : PaymentAccountsList
    {
        ButtonGrid buttonGrid;

        public LaborPaymentLists(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        public override List<ButtonGridItem> LoadData(string query)
        {
            List<ButtonGridItem> ret = new List<ButtonGridItem>();
            ButtonGridItem[] buttonGridItems = CreatePaymentButtonItems("");
            foreach (ButtonGridItem item in buttonGridItems)
            {
                ret.Add(item);
            }
            return ret;
        }

        protected override ButtonGridItem[] CreatePaymentButtonItems(string itemCaption)
        {
            ButtonGridBodyButtonItem itemCash;
            ButtonGridBodyButtonItem itemCheck;

            ButtonGridItem[] buttonGridItems = new ButtonGridItem[2];

            itemCash = ButtonGridHelper.CreateButtonWithImage("laborCash", "Cash " + itemCaption, Properties.Resources.cash);
            itemCash.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(128), BizNetPaymentMethod.Cash, "");

            itemCheck = ButtonGridHelper.CreateButtonWithImage("laborBank", "Check " + itemCaption, Properties.Resources.Check);
            itemCheck.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(128), BizNetPaymentMethod.Check, "");

            buttonGridItems[0] = itemCash;
            buttonGridItems[1] = itemCheck;

            return buttonGridItems;
        }

    }
    public class PenalityPaymentLists : PaymentAccountsList
    {
        ButtonGrid buttonGrid;

        public PenalityPaymentLists(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        public override List<ButtonGridItem> LoadData(string query)
        {
            List<ButtonGridItem> ret = new List<ButtonGridItem>();
            ButtonGridItem[] buttonGridItems = CreatePaymentButtonItems("");
            foreach (ButtonGridItem item in buttonGridItems)
            {
                ret.Add(item);
            }
            return ret;
        }

        protected override ButtonGridItem[] CreatePaymentButtonItems(string itemCaption)
        {
            ButtonGridBodyButtonItem itemCash;
            ButtonGridBodyButtonItem itemCheck;
            ButtonGridBodyButtonItem itemCPOFromCash, itemBankTransferFromCash;
            ButtonGridBodyButtonItem itemCPOFromBank, itemBankTransferFromBank;

            ButtonGridItem[] buttonGridItems = new ButtonGridItem[6];

            itemCash = ButtonGridHelper.CreateButtonWithImage("penalityCash", "Cash " + itemCaption, Properties.Resources.cash);
            itemCash.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(140), BizNetPaymentMethod.Cash, "");

            itemCheck = ButtonGridHelper.CreateButtonWithImage("penalityBank", "Check " + itemCaption, Properties.Resources.Check);
            itemCheck.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(140), BizNetPaymentMethod.Check, "");

            itemCPOFromCash = ButtonGridHelper.CreateButtonWithImage("penalityCPOCash", "CPO From Cash " + itemCaption, Properties.Resources.cpocash);
            itemCPOFromCash.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(140), BizNetPaymentMethod.CPOFromCash, "");

            itemBankTransferFromCash = ButtonGridHelper.CreateButtonWithImage("penalityBankCash", "Bank Transfer From Cash " + itemCaption, Properties.Resources.bank_transfer_cash);
            itemBankTransferFromCash.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(140), BizNetPaymentMethod.BankTransferFromCash, "");

            itemCPOFromBank = ButtonGridHelper.CreateButtonWithImage("penalityCPOBank", "CPO From Bank " + itemCaption, Properties.Resources.cpobank);
            itemCPOFromBank.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(140), BizNetPaymentMethod.CPOFromBankAccount, "");

            itemBankTransferFromBank = ButtonGridHelper.CreateButtonWithImage("penalityBankFromBank", "Bank Transfer From Bank " + itemCaption, Properties.Resources.bank_transfer);
            itemBankTransferFromBank.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(140), BizNetPaymentMethod.BankTransferFromBankAccount, "");

            buttonGridItems[0] = itemCash;
            buttonGridItems[1] = itemCheck;
            buttonGridItems[2] = itemCPOFromCash;
            buttonGridItems[3] = itemBankTransferFromCash;
            buttonGridItems[4] = itemCPOFromBank;
            buttonGridItems[5] = itemBankTransferFromBank;

            return buttonGridItems;
        }

    }
    public class WithdrawalPaymentLists : PaymentAccountsList
    {
        ButtonGrid buttonGrid;

        public WithdrawalPaymentLists(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        public override List<ButtonGridItem> LoadData(string query)
        {
            List<ButtonGridItem> ret = new List<ButtonGridItem>();
            ButtonGridItem[] buttonGridItems = CreatePaymentButtonItems("");
            foreach (ButtonGridItem item in buttonGridItems)
            {
                ret.Add(item);
            }
            return ret;
        }

        protected override ButtonGridItem[] CreatePaymentButtonItems(string itemCaption)
        {
            ButtonGridBodyButtonItem itemCash;
            ButtonGridBodyButtonItem itemCheck, itemCPOFromCash, itemBankTransferFromCash;
            ButtonGridBodyButtonItem itemCPOFromBank, itemBankTransferFromBank;
            ButtonGridItem[] buttonGridItems = new ButtonGridItem[6];

            itemCash = ButtonGridHelper.CreateButtonWithImage("withdrawCash", "Cash " + itemCaption, Properties.Resources.cash);
            itemCash.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(138), BizNetPaymentMethod.Cash, "");

            itemCheck = ButtonGridHelper.CreateButtonWithImage("withdrawBank", "Check " + itemCaption, Properties.Resources.Check);
            itemCheck.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(138), BizNetPaymentMethod.Check, "");

            itemCPOFromCash = ButtonGridHelper.CreateButtonWithImage("withdrawCPOFromCash", "CPO From Cash " + itemCaption, Properties.Resources.cpocash);
            itemCPOFromCash.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(138), BizNetPaymentMethod.CPOFromCash, "");

            itemCPOFromBank = ButtonGridHelper.CreateButtonWithImage("withdrawCPOFromBank", "CPO From Bank " + itemCaption, Properties.Resources.cpobank);
            itemCPOFromBank.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(138), BizNetPaymentMethod.CPOFromBankAccount, "");

            itemBankTransferFromCash = ButtonGridHelper.CreateButtonWithImage("withdrawBankTransferFromCash", "Bank Transfer From Cash " + itemCaption, Properties.Resources.bank_transfer_cash);
            itemBankTransferFromCash.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(138), BizNetPaymentMethod.BankTransferFromCash, "");

            itemBankTransferFromBank = ButtonGridHelper.CreateButtonWithImage("withdrawBankTransferFromBank", "Bank Transfer From Bank " + itemCaption, Properties.Resources.bank_transfer);
            itemBankTransferFromBank.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(138), BizNetPaymentMethod.BankTransferFromBankAccount, "");

            buttonGridItems[0] = itemCash;
            buttonGridItems[1] = itemCheck;
            buttonGridItems[2] = itemCPOFromCash;
            buttonGridItems[3] = itemCPOFromBank;
            buttonGridItems[4] = itemBankTransferFromCash;
            buttonGridItems[5] = itemBankTransferFromBank;

            return buttonGridItems;
        }

    }

    public class WithdrawalReturnLists : PaymentAccountsList
    {
        ButtonGrid buttonGrid;

        public WithdrawalReturnLists(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        public override List<ButtonGridItem> LoadData(string query)
        {
            List<ButtonGridItem> ret = new List<ButtonGridItem>();
            ButtonGridItem[] buttonGridItems = CreatePaymentButtonItems("");
            foreach (ButtonGridItem item in buttonGridItems)
            {
                ret.Add(item);
            }
            return ret;
        }

        protected override ButtonGridItem[] CreatePaymentButtonItems(string itemCaption)
        {
            ButtonGridBodyButtonItem itemCash;
            ButtonGridBodyButtonItem itemCheck;
            ButtonGridBodyButtonItem itemCPOFromBank, itemBankTransferFromBank;
            ButtonGridItem[] buttonGridItems = new ButtonGridItem[4];

            itemCash = ButtonGridHelper.CreateButtonWithImage("withdrawCash", "Cash " + itemCaption, Properties.Resources.cash);
            itemCash.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(139), BizNetPaymentMethod.Cash, "");

            itemCheck = ButtonGridHelper.CreateButtonWithImage("withdrawBank", "Check " + itemCaption, Properties.Resources.Check);
            itemCheck.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(139), BizNetPaymentMethod.Check, "");

            itemCPOFromBank = ButtonGridHelper.CreateButtonWithImage("withdrawCPOFromBank", "CPO From Bank " + itemCaption, Properties.Resources.cpobank);
            itemCPOFromBank.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(139), BizNetPaymentMethod.CPOFromBankAccount, "");

            itemBankTransferFromBank = ButtonGridHelper.CreateButtonWithImage("withdrawBankTransferFromBank", "Bank Transfer From Bank " + itemCaption, Properties.Resources.bank_transfer);
            itemBankTransferFromBank.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(139), BizNetPaymentMethod.BankTransferFromBankAccount, "");

            buttonGridItems[0] = itemCash;
            buttonGridItems[1] = itemCheck;
            buttonGridItems[2] = itemCPOFromBank;
            buttonGridItems[3] = itemBankTransferFromBank;

            return buttonGridItems;
        }

    }
    public class TaxDeclarationPaymentLists : PaymentAccountsList
    {
        ButtonGrid buttonGrid;
        int documentTypeID;
        TaxDeclarationManager m_manager;
        public TaxDeclarationPaymentLists(TaxDeclarationManager manager, ButtonGrid grid, ButtonGridItem parentItem,int declarationTypeID)
            : base(grid, parentItem)
        {
            m_manager = manager;
            buttonGrid = grid;
            documentTypeID = declarationTypeID;
        }
        public override List<ButtonGridItem> LoadData(string query)
        {
            List<ButtonGridItem> ret = new List<ButtonGridItem>();
            ButtonGridItem[] buttonGridItems = CreatePaymentButtonItems("");
            foreach (ButtonGridItem item in buttonGridItems)
            {
                ret.Add(item);
            }
            return ret;
        }

        protected override ButtonGridItem[] CreatePaymentButtonItems(string itemCaption)
        {
            ButtonGridBodyButtonItem itemCash;
            ButtonGridBodyButtonItem itemCPOFromBank;

            ButtonGridItem[] buttonGridItems = new ButtonGridItem[2];

            itemCash = ButtonGridHelper.CreateButtonWithImage("declarationCash", "Cash " + itemCaption, Properties.Resources.cash);
            DocumentWithPaymentTypeActivator ac1 = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(documentTypeID), BizNetPaymentMethod.Cash, "");
            ac1.ParentForm = m_manager;
            itemCash.Tag = ac1;

            itemCPOFromBank = ButtonGridHelper.CreateButtonWithImage("declarationCPO", "CPO " + itemCaption, Properties.Resources.cpobank);
            DocumentWithPaymentTypeActivator ac2 = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(documentTypeID), BizNetPaymentMethod.CPOFromBankAccount, "");
            ac2.ParentForm = m_manager;
            itemCPOFromBank.Tag = ac2;

            buttonGridItems[0] = itemCash;
            buttonGridItems[1] = itemCPOFromBank;

            return buttonGridItems;
        }

    }
    public class BankServiceChargePaymentLists : PaymentAccountsList
    {
        ButtonGrid buttonGrid;

        public BankServiceChargePaymentLists(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        protected override bool canIncludeAccount(int csAccountID)
        {
            if (AccountBase.AmountGreater(INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(csAccountID, DateTime.Now), 0))
            {
                return true;
            }
            else
                return false;
        }
        public override List<ButtonGridItem> LoadData(string query)
        {
            List<ButtonGridItem> ret = new List<ButtonGridItem>();
            BankAccountInfo[] bankAccounts = iERPTransactionClient.GetAllBankAccounts();
            ret.Add(ButtonGridHelper.CreateGroupButton("Bank Accounts"));
            foreach (BankAccountInfo bankAccount in bankAccounts)
            {
                //if (canIncludeAccount(bankAccount.mainCsAccount))
                {
                    ret.Add(CreateButtonItem(bankAccount, Properties.Resources.Check));
                }
            }
            return ret;
        }

        private  ButtonGridItem CreateButtonItem(object account, Image buttonImage)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage(((BankAccountInfo)account).mainCsAccount.ToString(), ((BankAccountInfo)account).bankName, buttonImage);
            DocumentWithPaymentTypeActivator ac = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(122), BizNetPaymentMethod.Check, ((BankAccountInfo)account).mainCsAccount);
            button.Tag = ac;
            return button;
        }
    }
    public class ZReportPaymentLists : PaymentAccountsList
    {
        ButtonGrid buttonGrid;

        public ZReportPaymentLists(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        public override List<ButtonGridItem> LoadData(string query)
        {
            List<ButtonGridItem> ret = new List<ButtonGridItem>();
            ButtonGridItem[] buttonGridItems = CreatePaymentButtonItems("");
            foreach (ButtonGridItem item in buttonGridItems)
            {
                ret.Add(item);
            }
            return ret;
        }

        protected override ButtonGridItem[] CreatePaymentButtonItems(string itemCaption)
        {
            ButtonGridBodyButtonItem itemCash;
            ButtonGridBodyButtonItem itemCheck;
            ButtonGridBodyButtonItem itemCPOFromBank;
            ButtonGridBodyButtonItem itemBankTranFromBank;

            ButtonGridItem[] buttonGridItems = new ButtonGridItem[4];

            itemCash = ButtonGridHelper.CreateButtonWithImage("z-cash", "Cash " + itemCaption, Properties.Resources.cash);
            itemCash.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(135), BizNetPaymentMethod.Cash, "");

            itemCheck = ButtonGridHelper.CreateButtonWithImage("z-check", "Check " + itemCaption, Properties.Resources.Check);
            itemCheck.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(135), BizNetPaymentMethod.Check, "");

            itemCPOFromBank = ButtonGridHelper.CreateButtonWithImage("z-cpo", "CPO " + itemCaption, Properties.Resources.cpobank);
            itemCPOFromBank.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(135), BizNetPaymentMethod.CPOFromBankAccount, "");

            itemBankTranFromBank = ButtonGridHelper.CreateButtonWithImage("z-bank", "Bank Transfer " + itemCaption, Properties.Resources.bank_transfer);
            itemBankTranFromBank.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(135), BizNetPaymentMethod.BankTransferFromBankAccount, "");

            buttonGridItems[0] = itemCash;
            buttonGridItems[1] = itemCheck;
            buttonGridItems[2] = itemCPOFromBank;
            buttonGridItems[3] = itemBankTranFromBank;

            return buttonGridItems;
        }

    }
    public class GroupPayrollPaymentLists : PaymentAccountsList
    {
        ButtonGrid buttonGrid;

        public GroupPayrollPaymentLists(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        public override List<ButtonGridItem> LoadData(string query)
        {
            List<ButtonGridItem> ret = new List<ButtonGridItem>();
            ButtonGridItem[] buttonGridItems = CreatePaymentButtonItems("");
            foreach (ButtonGridItem item in buttonGridItems)
            {
                ret.Add(item);
            }
            return ret;
        }

        protected override ButtonGridItem[] CreatePaymentButtonItems(string itemCaption)
        {
            ButtonGridBodyButtonItem itemCash;
            ButtonGridBodyButtonItem itemCheck;
            ButtonGridBodyButtonItem itemCPOFromCash, itemBankTransferFromCash;
            ButtonGridBodyButtonItem itemCPOFromBank, itemBankTransferFromBank;

            ButtonGridItem[] buttonGridItems = new ButtonGridItem[6];

            itemCash = ButtonGridHelper.CreateButtonWithImage("groupPayrollCash", "Cash " + itemCaption, Properties.Resources.cash);
            itemCash.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(154), BizNetPaymentMethod.Cash, "");

            itemCheck = ButtonGridHelper.CreateButtonWithImage("groupPayrollBank", "Check " + itemCaption, Properties.Resources.Check);
            itemCheck.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(154), BizNetPaymentMethod.Check, "");

            itemCPOFromCash = ButtonGridHelper.CreateButtonWithImage("groupPayrollCPOCash", "CPO From Cash " + itemCaption, Properties.Resources.cpocash);
            itemCPOFromCash.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(154), BizNetPaymentMethod.CPOFromCash, "");

            itemBankTransferFromCash = ButtonGridHelper.CreateButtonWithImage("groupPayrollBankCash", "Bank Transfer From Cash " + itemCaption, Properties.Resources.bank_transfer_cash);
            itemBankTransferFromCash.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(154), BizNetPaymentMethod.BankTransferFromCash, "");

            itemCPOFromBank = ButtonGridHelper.CreateButtonWithImage("groupPayrollCPOBank", "CPO From Bank " + itemCaption, Properties.Resources.cpobank);
            itemCPOFromBank.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(154), BizNetPaymentMethod.CPOFromBankAccount, "");

            itemBankTransferFromBank = ButtonGridHelper.CreateButtonWithImage("groupPayrollBankFromBank", "Bank Transfer From Bank " + itemCaption, Properties.Resources.bank_transfer);
            itemBankTransferFromBank.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(154), BizNetPaymentMethod.BankTransferFromBankAccount, "");

            buttonGridItems[0] = itemCash;
            buttonGridItems[1] = itemCheck;
            buttonGridItems[2] = itemCPOFromCash;
            buttonGridItems[3] = itemBankTransferFromCash;
            buttonGridItems[4] = itemCPOFromBank;
            buttonGridItems[5] = itemBankTransferFromBank;

            return buttonGridItems;
        }

    }

}
