using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Payroll;

namespace BIZNET.iERP.Client
{
    public class DCHPayrollSet : bERPClientDocumentHandler
    {
        public DCHPayrollSet()
            : base(typeof(PreviewPayroll2))
        {
        }

        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            PreviewPayroll2 f = (PreviewPayroll2)editor;
            f.loadData((PayrollSetDocument)doc);
        }

    }
}
