﻿namespace BIZNET.iERP.Client
{
    partial class DatePickerDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.dtDate = new BIZNET.iERP.Client.BNDualCalendar();
            this.memoNote = new DevExpress.XtraEditors.MemoEdit();
            this.labelNoteLabel = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoNote.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnClose);
            this.panelControl1.Controls.Add(this.btnOk);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 122);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(292, 31);
            this.panelControl1.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(212, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(131, 5);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "&Ok";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // dtDate
            // 
            this.dtDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dtDate.Dock = System.Windows.Forms.DockStyle.Top;
            this.dtDate.Location = new System.Drawing.Point(0, 0);
            this.dtDate.Name = "dtDate";
            this.dtDate.ShowEthiopian = true;
            this.dtDate.ShowGregorian = true;
            this.dtDate.ShowTime = false;
            this.dtDate.Size = new System.Drawing.Size(292, 42);
            this.dtDate.TabIndex = 1;
            this.dtDate.VerticalLayout = true;
            // 
            // memoEdit1
            // 
            this.memoNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoNote.Location = new System.Drawing.Point(0, 61);
            this.memoNote.Name = "memoEdit1";
            this.memoNote.Size = new System.Drawing.Size(292, 61);
            this.memoNote.TabIndex = 2;
            // 
            // labelNoteLabel
            // 
            this.labelNoteLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelNoteLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelNoteLabel.Location = new System.Drawing.Point(0, 42);
            this.labelNoteLabel.Name = "labelNoteLabel";
            this.labelNoteLabel.Size = new System.Drawing.Size(292, 19);
            this.labelNoteLabel.TabIndex = 3;
            this.labelNoteLabel.Text = "Note";
            // 
            // DatePickerDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 153);
            this.Controls.Add(this.memoNote);
            this.Controls.Add(this.labelNoteLabel);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "DatePickerDialog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pick Date";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DatePickerDialog_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoNote.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private BNDualCalendar dtDate;
        private DevExpress.XtraEditors.MemoEdit memoNote;
        private DevExpress.XtraEditors.LabelControl labelNoteLabel;
    }
}