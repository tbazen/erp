﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BIZNET.iERP.Client
{
    public partial class DatePickerDialog : DevExpress.XtraEditors.XtraForm
    {
        private DateTime _DateTime;
        private string _note;
        public DatePickerDialog()
            : this(false,"",DateTime.Now)
        {
        }

        public DatePickerDialog(bool showNote,string note,DateTime date)
        {
            InitializeComponent();
            labelNoteLabel.Visible = memoNote.Visible = showNote;
            memoNote.Text = note;
        }
        public DateTime DateTime
        {
            get
            {
                return _DateTime;
            }
        }
        public string Note
        {
            get
            {
                return _note;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            _DateTime = dtDate.DateTime;
            _note = memoNote.Text;
            this.DialogResult = DialogResult.OK;
        }

        private void DatePickerDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult != DialogResult.OK)
                DialogResult = DialogResult.Cancel;
        }
    }
}