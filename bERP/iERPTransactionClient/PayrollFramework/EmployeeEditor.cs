﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;
using System.Collections;
using DevExpress.XtraEditors.DXErrorProvider;
using INTAPS.Payroll;
using INTAPS.Payroll.Client;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using System.IO;
using System.Drawing.Imaging;
using DevExpress.Utils;
using DevExpress.XtraGrid;

namespace BIZNET.iERP.Client
{
    public partial class EmployeeEditor : DevExpress.XtraEditors.XtraForm
    {
        public delegate void NewEmployeeAddedHandler(Employee newEmployee);
        public delegate void EmployeeUpdatedHandler(Employee updatedEmployee);
        public event NewEmployeeAddedHandler NewEmployeeAdded;
        public event EmployeeUpdatedHandler EmployeeUpdated;
        private CostSharingPayableValidationRule m_CostSharingPayableValidation;
        private TransportAllowanceValidationRule m_TransportAllowanceValidation;
        private double m_GrossSalary;
        private string m_Message;
        AccountPicker<Account> m_picker;
        Account[] m_Accounts = null;
        OrgUnit m_parentOrgUnit;
        bool m_Editable;
        int m_ID = -1;
        int m_CostCenterID = -1;
        EmployeeStatus m_Status;
        private DataTable accountInfoTable;
        private DataTable tableEmployees;
        const string REGULAR_TIME = "Regular Time";
        const string OVER_TIME = "Over Time";
        const string OTHERS = "Special Benefit/Deduction";
        const string PERMANENT_BENEFIT= "Permanent Benefit/Deduction";
        private Project[] m_Projects;

        public bool updated = false;
        public EmployeeEditor(int parentOrtUnitID)
        {
            initWithOrgUnitID(parentOrtUnitID);
            comboVersions.SelectedIndex = 0;
        }
        private void initWithOrgUnitID(int parentOrtUnitID)
        {
            InitializeComponent();
            PopulateLoginNames();
            dateEnrolled.DateTime = DateTime.Now;
            dateBirth.DateTime = DateTime.Now;
            PrepareWorkDataTable();
            tabbedControlGroup1.SelectedTabPageIndex = 0;
            layoutCostSharingPayable.HideToCustomization(); //Hide payable cost sharing textbox
            layoutCostSharingDebt.HideToCustomization();// Hide cost-sharing debt textbox
            layoutAccountInformation.HideToCustomization();
            layoutOtherEmployeeAccounts.HideToCustomization();
            Size = new Size(800, 688);
            txtEmployeeName.Focus();
            m_parentOrgUnit = PayrollClient.GetOrgUnit(parentOrtUnitID);
            txtUnit.Text = m_parentOrgUnit.Name;
            InitializeGrossSalaryValidation();
            txtEmployeeID.LostFocus += Control_LostFocus;
            txtEmployeeName.LostFocus += Control_LostFocus;
            txtGrossSalary.LostFocus += Control_LostFocus;
            txtCostsharingPayable.LostFocus += Control_LostFocus;
            m_Projects = iERPTransactionClient.GetAllProjects();
            LoadTaxCenters();
            if (comboPeriod.SelectedIndex != -1 && m_ID!=-1)
                DisplayEmployeeWorkData(m_ID);
            comboVersions.Properties.Items.Add(new VersionObject(){version=-1});
            fixVersionControls();
        }

        private void LoadOtherEmployeeAccounts(int[]accounts)
        {
            this.panelAccount.Controls.Clear();
            int[] otherAccounts;
            if (m_Accounts != null)
                otherAccounts = FilterOtherEmployeeAccounts(accounts);
            else
                otherAccounts = accounts;
            foreach (int account in otherAccounts)
            {
                Account ac = AccountingClient.GetAccount<Account>(account);
                if(ac!=null)
                this.AddObject(ac.PID);
            }
        }

        private void PopulateLoginNames()
        {
            comboLoginName.Properties.Items.AddRange(INTAPS.ClientServer.Client.SecurityClient.GetAllUsers());
        }

        private void LoadTaxCenters()
        {
            TaxCenter[] taxCenter = iERPTransactionClient.GetTaxCenters();
            foreach (TaxCenter center in taxCenter)
            {
                cmbTaxCenter.Properties.Items.Add(center);
            }
            cmbTaxCenter.SelectedIndex = 0;
        }

        private void InitializeGrossSalaryValidation()
        {
            #region Gross Salary Validation
            NonEmptyNumericValidationRule grossSalaryValidation = new NonEmptyNumericValidationRule();
            grossSalaryValidation.ErrorText = "Employee Gross Salary cannot be blank and cannot contain a value of zero";
            grossSalaryValidation.ErrorType = ErrorType.Default;

            dxValidationProvider1.SetValidationRule(txtGrossSalary, grossSalaryValidation);

            #endregion

        }

        private void InitializeCostSharingPayableValidation()
        {
            m_CostSharingPayableValidation = new CostSharingPayableValidationRule(GetGrossSalary());
            m_CostSharingPayableValidation.ErrorType = ErrorType.Default;
            dxValidationProvider1.SetValidationRule(txtCostsharingPayable, m_CostSharingPayableValidation);
        }

        private void InitializeTransportAllowanceValidation()
        {
            m_TransportAllowanceValidation = new TransportAllowanceValidationRule(GetGrossSalary());
            //  m_TransportAllowanceValidation.ErrorType = ErrorType.Default;
            validationTransportAllowance.SetValidationRule(txtTransportAllowance, m_TransportAllowanceValidation);
        }

        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            dxValidationProvider1.Validate(control);
        }
        class VersionObject
        {
            public long version;
            public override string ToString()
            {
                if(version==-1)
                    return "New Version";
                return new DateTime(version).ToString("MMM dd,yyyy hh:mm tt");
            }
        }
        void fixVersionControls()
        {

            VersionObject selectedVersion = comboVersions.SelectedItem as VersionObject;
            
            buttonDeleteVersion.Enabled=selectedVersion!=null && selectedVersion.version!=-1 && comboVersions.Properties.Items.Count>2;
            dateTimeVersion.Enabled = selectedVersion!=null && selectedVersion.version == -1;
            if (selectedVersion != null && selectedVersion.version != -1)
                dateTimeVersion.DateTime = new DateTime(selectedVersion.version);

        }
        public EmployeeEditor(int employeeID, long version, bool editEmployee)
        {
            Employee employee = PayrollClient.GetEmployee(employeeID,version);
            initWithOrgUnitID(employee.orgUnitID);
            btnAdd.Text = "Save";

            long[] versions = PayrollClient.getEmployeVersions(employeeID);
            int index = -1;
            int i=0;
            foreach (long v in versions)
            {
                comboVersions.Properties.Items.Add(new VersionObject() { version = v });
                if (v == employee.ticksFrom)
                {
                    index = i;
                }
                i++;
            }
            _ignoreDataChangeEvent = true;
            
            comboVersions.SelectedIndex = index+1;
            
            accountInfoTable = new DataTable();
            m_Editable = editEmployee;
            m_ID = employee.id;
            DisplayEmployeeAccountInformation(employee);
            ApplyAccountsFormatConditioning();
            LoadOtherEmployeeAccounts(employee.accounts);
            layoutAccountInformation.RestoreFromCustomization(layoutEmployeeProfile, InsertType.Right);
            layoutOtherEmployeeAccounts.RestoreFromCustomization(layoutWorkData, InsertType.Right);
            tabbedControlGroup1.SelectedTabPage = layoutEmployeeProfile;
            initWithEmployee(employee);
            
        }

        private void initWithEmployee(Employee employee)
        {
            m_CostCenterID = employee.costCenterID;
            m_Status = employee.status;
            txtUnit.Text = m_parentOrgUnit.Name;
            txtEmployeeID.Text = employee.employeeID;
            txtEmployeeName.Text = employee.employeeName;
            txtBankAccountNo.Text = employee.BankAccountNo;
            txtTIN.Text = employee.TIN;
            txtTelephone.Text = employee.telephone;
            dateBirth.DateTime = employee.birthDate;
            cmbSex.SelectedIndex = (int)employee.sex;
            txtAddress.Text = employee.address;
            dateEnrolled.DateTime = employee.enrollmentDate;
            cmbEmpType.SelectedIndex = (int)employee.employmentType;
            txtGrossSalary.Text = employee.grossSalary == 0d ? "" : employee.grossSalary.ToString("N");
            cmbCostSharingStatus.SelectedIndex = (int)employee.costSharingStatus;
            txtCostSharing.Text = employee.totalCostSharingDebt == 0d ? "" : employee.totalCostSharingDebt.ToString("N");
            txtTransportAllowance.Text = employee.transportAllowance == 0d ? "" : employee.transportAllowance.ToString("N");
            txtCostsharingPayable.Text = employee.costSharingPayableAmount == 0d ? "" : employee.costSharingPayableAmount.ToString("N");
            chkIsShareHolder.Checked = employee.shareHolder;
            radSalaryType.SelectedIndex = employee.salaryKind == SalaryKind.Monthly ? 0 : 1;
            costCenterPlaceHolder.SetByID(employee.costCenterID);
            checkAccountAsCost.Checked = employee.accountAsCost;
            m_parentOrgUnit = PayrollClient.GetOrgUnit(employee.orgUnitID);
            txtUnit.Text = m_parentOrgUnit.Name;
            SetEmployeeTaxCenter(employee);
            SetEmployeeImage(employee);

            if (comboPeriod.SelectedIndex != -1)
                DisplayEmployeeWorkData(m_ID);
                DisableAllControls(employee.status == EmployeeStatus.Fired);
            
            comboLoginName.Text = employee.loginName;
            textTitle.Text = employee.title;
            fixVersionControls();
            
            if (!m_Editable)
            {
                //adding mode
                ClearControls();
                comboVersions.SelectedIndex = 0;
                m_ID = -1;
            }
            accountExpense.SetByID(employee.expenseParentAccountID);
        }

        private void DisableAllControls(bool readOnly)
        {
            if (readOnly)
                layoutConfirm.Visibility = LayoutVisibility.Never;
            else
                layoutConfirm.Visibility = LayoutVisibility.Always;
            txtUnit.Properties.ReadOnly = readOnly;
            txtEmployeeID.Properties.ReadOnly = readOnly;
            txtEmployeeName.Properties.ReadOnly = readOnly;
            txtTIN.Properties.ReadOnly = readOnly;
            txtTelephone.Properties.ReadOnly = readOnly;
            dateBirth.Enabled = !readOnly;
            txtAddress.Properties.ReadOnly = readOnly;
            cmbSex.Properties.ReadOnly = readOnly;
            dateEnrolled.Enabled = !readOnly;
            cmbEmpType.Properties.ReadOnly = readOnly;
            txtGrossSalary.Properties.ReadOnly = readOnly;
            cmbCostSharingStatus.Properties.ReadOnly = readOnly;
            txtCostSharing.Properties.ReadOnly = readOnly;
            chkIsShareHolder.Enabled = !readOnly;
            radSalaryType.Enabled = !readOnly;
            costCenterPlaceHolder.Enabled = !readOnly;
            checkAccountAsCost.Enabled = !readOnly;
            cmbTaxCenter.Properties.ReadOnly = readOnly;
            btnBrowse.Enabled = !readOnly;
            btnClear.Enabled = !readOnly;
            gridViewWorkData.OptionsBehavior.Editable = !readOnly;
        }
        bool _ignoreDataChangeEvent = false;
        bool _workingDataChanged = false;
        bool _employeeProfileChanged = false;
        private void SetEmployeeTaxCenter(Employee employee)
        {
            int i = 0;
            foreach (TaxCenter center in cmbTaxCenter.Properties.Items)
            {
                if (center.ID == employee.TaxCenter)
                {
                    cmbTaxCenter.SelectedIndex = i;
                    break;
                }
                i++;
            }
        }
        CostCenter getCostCenter(Dictionary<int, CostCenter> costCenters, int costCenterID)
        {
            if (costCenters.ContainsKey(costCenterID))
                return costCenters[costCenterID];
            CostCenter cs = AccountingClient.GetAccount<CostCenter>(costCenterID);
            costCenters.Add(costCenterID, cs);
            return cs;
        }
        class CostCenterComparer : IComparer<CostCenter>
        {
            public int Compare(CostCenter x, CostCenter y)
            {
                return x.Code.CompareTo(y.Code);
            }
        }
        private void DisplayEmployeeAccountInformation(Employee employee)
        {

            accountInfoTable = new DataTable();
            
            Account[] accounts = new Account[]{
            GetEmployeeAccount(GetUnclaimedSalaryParentAccountID(), employee.accounts)
            ,GetEmployeeAccount(GetLoanFromStaffParentAccountID(), employee.accounts)
            ,GetEmployeeAccount(GetshortTermLoanParentAccountID(), employee.accounts)
            ,GetEmployeeAccount(GetExpenseAdvanceParentAccountID(), employee.accounts)
            ,GetEmployeeAccount(GetLongTermLoanParentAccountID(), employee.accounts)
            ,GetEmployeeAccount(GetShareHoldersLoanParentAccountID(), employee.accounts)
            ,GetEmployeeAccount(GetPensionPayableParentAccountID(), employee.accounts)
            ,GetEmployeeAccount(GetPayrollIncomeTaxParentAccountID(), employee.accounts)
            ,GetEmployeeAccount(GetCostSharingPayableParentAccountID(), employee.accounts)
            ,GetEmployeeAccount(GetOwnersEquityParentAccountID(), employee.accounts)
            };
            m_Accounts = accounts;
            string[] accountNames = new string[]{
                "Unclaimed Salary"
                ,"Loan to Company"
                ,"Short-term loan"
                ,"Purchase Advance"
                ,"Long-term Loan"
                ,"Shareholder Loan"
                ,"Undeclared Pension Deduction"
                ,"Undeclared Incometax Deduction"
                ,"Undeclared Cost Sharing Deduction"
                ,"Company Equity"
            };

            SortedDictionary<string, CostCenter> costCenters = new SortedDictionary<string, CostCenter>();
            Dictionary<string, double>[] balances = new Dictionary<string, double>[accounts.Length];
            for (int rowIndex = 0; rowIndex < accounts.Length; rowIndex++)
            {
                Dictionary<string, double> row = new Dictionary<string, double>();
                balances[rowIndex] = row;
                if (accounts[rowIndex] == null)
                    continue;
                double total = 0;
                bool zeroRow = true;
                foreach (CostCenterAccount csa in INTAPS.Accounting.Client.AccountingClient.GetCostCenterAccountsOf<Account>(accounts[rowIndex].id))
                {

                    if (csa.isControlAccount)
                        continue;
                    double bal = INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(csa.id, DateTime.Now);
                    /*if (AccountBase.AmountEqual(bal, 0))
                        continue;*/
                    zeroRow = false;
                    CostCenter cs = null;
                    foreach (KeyValuePair<string, CostCenter> kv in costCenters)
                    {
                        if (kv.Value.id == csa.costCenterID)
                        {
                            cs = kv.Value;
                            break;
                        }
                    }
                    if (cs == null)
                    {
                        cs = INTAPS.Accounting.Client.AccountingClient.GetAccount<CostCenter>(csa.costCenterID);
                        costCenters.Add(cs.Code, cs);
                    }
                    row.Add(cs.Code, bal);
                    total += bal;
                }
                if (zeroRow)
                    accounts[rowIndex] = null;
            }

            accountInfoTable.Columns.Add("Account Code", typeof(string));
            accountInfoTable.Columns.Add("Account Name", typeof(string));
            foreach (KeyValuePair<string, CostCenter> kv in costCenters)
            {
                accountInfoTable.Columns.Add(kv.Value.NameCode, typeof(double));
            }
            accountInfoTable.Columns.Add("Total", typeof(double));
            double[] colTotal = new double[costCenters.Count + 1];
            for (int i = 0; i < accounts.Length; i++)
            {

                if (accounts[i] == null)
                    continue;
                DataRow dataRow = accountInfoTable.NewRow();
                dataRow[0] = accounts[i].Code;
                dataRow[1] = accountNames[i];
                int j = 2;
                Dictionary<string, double> balanceRow = balances[i];
                double total = 0;

                foreach (KeyValuePair<string, CostCenter> kv in costCenters)
                {
                    double bal = balanceRow.ContainsKey(kv.Value.Code) ? balanceRow[kv.Value.Code] : 0;
                    total += bal;
                    colTotal[j-2] += bal;
                    dataRow[j] = bal;
                    j++;
                }
                dataRow[j] = total;
                colTotal[j-2] += total;
                accountInfoTable.Rows.Add(dataRow);
            }
            {
                DataRow dataRow = accountInfoTable.NewRow();
                dataRow[0] = "";
                dataRow[1] = "Total";
                int j = 2;
                foreach (KeyValuePair<string, CostCenter> kv in costCenters)
                {
                    dataRow[j] = colTotal[j-2];
                    j++;
                }
                dataRow[j] = colTotal[j-2];
                accountInfoTable.Rows.Add(dataRow);
            }
            gridAccountInfo.DataSource = accountInfoTable;
            for(int j=0;j<colTotal.Length;j++)
            {
                if (j + 2 <= gridViewAccountInfo.Columns.Count-1)
                {
                    gridViewAccountInfo.Columns[j + 2].DisplayFormat.FormatType = FormatType.Numeric;
                    gridViewAccountInfo.Columns[j + 2].DisplayFormat.FormatString = "N";
                }
            }
        }
        private void ApplyAccountsFormatConditioning()
        {
            DevExpress.XtraGrid.StyleFormatCondition condition = new DevExpress.XtraGrid.StyleFormatCondition();
            condition.Appearance.Options.UseBackColor = true;
            condition.Appearance.BackColor = Color.FromArgb(0x8F, 0xF2, 0x14, 0x43);
            condition.Condition = FormatConditionEnum.Expression;
            condition.Expression = "[Status] == 'Fired'";
            condition.ApplyToRow = true;
            gridViewAccountInfo.FormatConditions.Add(condition);
        }

        private int GetLongTermLoanParentAccountID()
        {
            int longTermLoanAccountID = (int)PayrollClient.GetSystemParameters(new string[] { "staffLongTermLoanAccountID" })[0];
            return longTermLoanAccountID;
        }
        private int GetExpenseAdvanceParentAccountID()
        {
            int expenseAdvanceParentAccountID = (int)PayrollClient.GetSystemParameters(new string[] { "staffExpenseAdvanceAccountID" })[0];
            return expenseAdvanceParentAccountID;
        }
        private int GetshortTermLoanParentAccountID()
        {
            int shortTermLoanAccountID = (int)PayrollClient.GetSystemParameters(new string[] { "staffSalaryAdvanceAccountID" })[0];
            return shortTermLoanAccountID;
        }
        private int GetUnclaimedSalaryParentAccountID()
        {
            int unclaimedSalaryParentAccountID = (int)PayrollClient.GetSystemParameters(new string[] { "UnclaimedSalaryAccount" })[0];
            return unclaimedSalaryParentAccountID;
        }
        private int GetPayrollIncomeTaxParentAccountID()
        {
            int payrollIncomeTaxParentAccountID = (int)PayrollClient.GetSystemParameters(new string[] { "staffIncomeTaxAccountID" })[0];
            return payrollIncomeTaxParentAccountID;
        }
        private int GetPensionPayableParentAccountID()
        {
            int pensionPayableParentAccountID = (int)PayrollClient.GetSystemParameters(new string[] { "staffPensionPayableAccountID" })[0];
            return pensionPayableParentAccountID;
        }
        private int GetCostSharingPayableParentAccountID()
        {
            int costSharingPayableParentAccountID = (int)PayrollClient.GetSystemParameters(new string[] { "staffCostSharingPayableAccountID" })[0];
            return costSharingPayableParentAccountID;
        }
        private int GetOwnersEquityParentAccountID()
        {
            int ownersEquityParentAccountID = (int)PayrollClient.GetSystemParameters(new string[] { "OwnersEquityAccount" })[0];
            return ownersEquityParentAccountID;
        }
        private int GetLoanFromStaffParentAccountID()
        {
            int loanFromStaffParentAccountID = (int)PayrollClient.GetSystemParameters(new string[] { "LoanFromStaffAccountID" })[0];
            return loanFromStaffParentAccountID;
        }
        private int GetPerDiemParentAccountID()
        {
            int perDiemParentAccountID = (int)PayrollClient.GetSystemParameters(new string[] { "staffPerDiemAccountID" })[0];
            return perDiemParentAccountID;
        }
        private int GetShareHoldersLoanParentAccountID()
        {
            int shareHoldersLoanParentAccountID = (int)PayrollClient.GetSystemParameters(new string[] { "ShareHoldersLoanAccountID" })[0];
            return shareHoldersLoanParentAccountID;
        }
        private Account GetEmployeeAccount(int parentAccountID, int[] accounts)
        {
            foreach (int ac in accounts)
            {
                Account acnt = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(ac);
                if (acnt != null)
                {
                    if (acnt.PID == parentAccountID)
                    {
                        return acnt;
                    }
                }
            }
            return null;

        }
        private int[] FilterOtherEmployeeAccounts(int[]allAccounts)
        {
            List<int>filteredAccounts=new List<int>();
            bool found = false;
            foreach (int allacc in allAccounts)
            {
                found = false;
                foreach (Account defacc in m_Accounts)
                {
                    if (defacc != null)
                    {
                        if (allacc == defacc.id)
                        {
                            found = true;
                            break;
                        }
                    }
                }
                if (!found)
                    filteredAccounts.Add(allacc);
            }
            return filteredAccounts.ToArray();
        }
        private void SetEmployeeImage(Employee employee)
        {
            INTAPS.ClientServer.ImageItem[] image = INTAPS.Accounting.Client.AccountingClient.GetImageItems(employee.picture);
            image = null;
            if (image != null)
            {
                if (image.Length > 0)
                {
                    byte[] bytes = INTAPS.Accounting.Client.AccountingClient.LoadImage(image[0].imageID, image[0].itemIndex, true, -1, -1).imageData;

                    using (MemoryStream stream = new MemoryStream(bytes))
                    {
                        picEmployeePic.Image = Image.FromStream(stream);
                    }

                }
            }
        }

        public double GetGrossSalary()
        {
            if (txtGrossSalary.Text != "")
            {
                if (double.Parse(txtGrossSalary.Text) != 0d)
                {
                    m_GrossSalary = double.Parse(txtGrossSalary.Text);
                    return m_GrossSalary;
                }
                else
                {
                    return 0d;
                }
            }
            else
            {
                return 0d;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            validationTransportAllowance.Validate();

            if (dxValidationProvider1.Validate())
            {
                if (dateBirth.DateTime.Date.CompareTo(DateTime.Now.Date) == 0 || dateBirth.DateTime.Date.CompareTo(dateEnrolled.DateTime.Date) == 0)
                {
                    MessageBox.ShowErrorMessage("Birth date can not be equal to the current or enrollment date");
                    return;
                }
                if (dateBirth.DateTime.Date.CompareTo(dateEnrolled.DateTime.Date) > 0)
                {
                    MessageBox.ShowErrorMessage("Birth date can not be greater than the enrollment date");
                    return;
                }
                if (dateEnrolled.DateTime.Date.CompareTo(DateTime.Now.Date) > 0)
                {
                    MessageBox.ShowErrorMessage("Enrollment date cannot be futured date");
                    return;
                }
                CostCenter cs = costCenterPlaceHolder.account;
                if (cs == null)
                {
                    MessageBox.ShowErrorMessage("Select cost center for the employee");
                    return;
                }
                if (cs.childCount > 1)
                {
                    MessageBox.ShowErrorMessage("Pick a cost center with no child");
                    return;
                }
                /*if (!ValidateEmployeeCostCenter(cs.id))
                {
                    MessageBox.ShowErrorMessage("Please pick a project/branch or project/branch division cost center!");
                    return;
                }*/
                //if (cs.id != (int)iERPTransactionClient.GetSystemParamter("mainCostCenterID")) //if cost center is not HQ
                //{
                //    bool validate = ValidateCostCenter(cs);
                //    if (!validate)
                //    {
                //        MessageBox.ShowErrorMessage("You can only pick cost centers which are project divisions. Please try again!");
                //        return;
                //    }
                //}
                Employee employee = new Employee();
                employee.id =m_Editable? m_ID:-1;
                employee.orgUnitID = m_parentOrgUnit.id;
                employee.employeeID = txtEmployeeID.Text;
                employee.employeeName = txtEmployeeName.Text;
                employee.TIN = txtTIN.Text;
                employee.enrollmentDate = dateEnrolled.DateTime;
                employee.telephone = txtTelephone.Text;
                employee.birthDate = dateBirth.DateTime;
                employee.sex = (Sex)cmbSex.SelectedIndex;
                employee.address = txtAddress.Text;
                employee.employmentType = (EmploymentType)cmbEmpType.SelectedIndex;
                employee.grossSalary = double.Parse(txtGrossSalary.Text);
                employee.transportAllowance = txtTransportAllowance.Text == "" ? 0d : double.Parse(txtTransportAllowance.Text);
                employee.totalCostSharingDebt = txtCostSharing.Text == "" ? 0d : double.Parse(txtCostSharing.Text);
                employee.costSharingPayableAmount = txtCostsharingPayable.Text == "" ? 0d : double.Parse(txtCostsharingPayable.Text);
                employee.costSharingStatus = (CostSharingStatus)cmbCostSharingStatus.SelectedIndex;
                employee.shareHolder = chkIsShareHolder.Checked;
                employee.status = !m_Editable ? EmployeeStatus.Enrolled : m_Status;
                employee.salaryKind = (SalaryKind)radSalaryType.SelectedIndex;
                employee.TaxCenter = ((TaxCenter)cmbTaxCenter.SelectedItem).ID;
                employee.costCenterID = cs.id;
                employee.accountAsCost = checkAccountAsCost.Checked;
                employee.loginName = comboLoginName.Text;
                employee.BankAccountNo = txtBankAccountNo.Text;
                employee.title = textTitle.Text;
                employee.expenseParentAccountID = accountExpense.GetAccountID();
                byte[] picture;
                picture = GetEmployeeImageInBytes();
                Data_RegularTime regularData;
                OverTimeData overtimeData;
                List<SingleValueData> singleData;
                List<int> singleFormulaID;
                try
                {
                    //version
                    VersionObject version = comboVersions.SelectedItem as VersionObject;
                    if (version.version == -1)
                        employee.ticksFrom = dateTimeVersion.DateTime.Ticks;
                    else
                        employee.ticksFrom = version.version;


                    getWorkingData(out regularData, out overtimeData, out singleData, out singleFormulaID);
                    int[] otherEmpAccounts = GetOtherEmployeeAccounts();

                    if (m_Editable) //Editing mode
                    {
                        iERPTransactionClient.RegisterEmployee(employee, picture, _workingDataChanged,otherEmpAccounts, comboPeriod.selectedPeriodID, regularData, overtimeData, singleData.ToArray(), singleFormulaID.ToArray());
                        FireEmployeeUpdated(employee);
                        Employee updatedEmp = PayrollClient.GetEmployee(employee.id);
                        DisplayEmployeeAccountInformation(updatedEmp);
                        if (version.version == -1)
                        {
                            int index = 0;

                            foreach (VersionObject v in comboVersions.Properties.Items)
                            {
                                if (v.version == -1)
                                {
                                    index++;
                                    continue;
                                }
                                if (v.version > employee.ticksFrom)
                                    break;
                                index++;
                            }
                            if (index == comboVersions.Properties.Items.Count)
                            {
                                comboVersions.Properties.Items.Add(new VersionObject() { version = employee.ticksFrom });
                            }
                            else
                                comboVersions.Properties.Items.Insert(index, new VersionObject() { version = employee.ticksFrom });
                            comboVersions.SelectedIndex = index;
                        }
                    }
                    else //Employee adding mode
                    {
                        employee.id = iERPTransactionClient.RegisterEmployee(employee, picture, _workingDataChanged,otherEmpAccounts, comboPeriod.selectedPeriodID, regularData, overtimeData, singleData.ToArray(), singleFormulaID.ToArray());
                        m_ID = employee.id;
                        FireNewEmployeeAdded(employee);
                        ClearControls();
                        dxValidationProvider1.RemoveControlError(txtEmployeeID);
                        dxValidationProvider1.RemoveControlError(txtEmployeeName);
                        //if (version.version == -1)
                        //{
                        //    comboVersions.Properties.Items.Add(new VersionObject() { version = employee.ticksFrom });
                        //    comboVersions.SelectedIndex = comboVersions.Properties.Items.Count - 1;
                        //}
                    }
                    layoutWorkData.Text = "Work Data";
                    _workingDataChanged = false;
                    updated = true;
                    MessageBox.ShowSuccessMessage("Employee successfully registered!");
                }
                catch (Exception ex)
                {
                    MessageBox.ShowException(ex);
                }
            }
            else
            {
                MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
            }
        }

        private int[] GetOtherEmployeeAccounts()
        {
            List<int> otherAccounts = new List<int>();
            foreach (Control cont in panelAccount.Controls)
            {
                if (cont is XtraPanel)
                {
                    otherAccounts.Add(((Account)((XtraPanel)cont).Tag).id);
                }
            }
            return otherAccounts.ToArray();
        }

        private bool ValidateEmployeeCostCenter(int costCenterID)
        {
            Project[] projects = iERPTransactionClient.GetAllProjects();
            foreach (Project project in projects)
            {
                if (project.costCenterID == costCenterID)
                    return true;
                foreach (int divCostCenter in project.projectData.projectDivisions)
                {
                    if (costCenterID == divCostCenter)
                        return true;
                }
            }
            return false;
        }


        private bool ValidateCostCenter(CostCenter cs)
        {
            //allow only picking from project division cost centers
            bool confirmed = false;
            foreach (Project project in m_Projects)
            {
                if (project.costCenterID == cs.id)
                {
                    confirmed = false;
                    break;
                }
                foreach (int divisionCostCenter in project.projectData.projectDivisions)
                {
                    if (cs.id == divisionCostCenter)
                    {
                        confirmed = true;
                        break;
                    }
                }
                if (confirmed)
                    break;
            }
            return confirmed;
        }
        private byte[] GetEmployeeImageInBytes()
        {
            byte[] picture = null;
            if (picEmployeePic.Image != null)
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    int newWidth = 128, newHeight = 128;
                    if (picEmployeePic.Image.Width > picEmployeePic.Image.Height)
                    {
                        newWidth = 128;
                        newHeight = 128 * picEmployeePic.Image.Height / picEmployeePic.Image.Width;
                    }
                    else if (picEmployeePic.Image.Width < picEmployeePic.Image.Height)
                    {
                        newHeight = 128;
                        newWidth = 128 * picEmployeePic.Image.Width / picEmployeePic.Image.Height;
                    }
                    using (Bitmap newPic = new Bitmap(picEmployeePic.Image, new Size(newWidth, newHeight)))
                    {
                        newPic.Save(stream, ImageFormat.Jpeg);
                    }
                    picture = stream.ToArray();
                }
            }
            return picture;
        }

        private void ClearControls()
        {
            txtEmployeeID.Text = "";
            txtEmployeeName.Text = "";
            txtTIN.Text = "";
            dateEnrolled.DateTime = DateTime.Now;
            txtTelephone.Text = "";
            txtAddress.Text = "";
            txtGrossSalary.Text = "";
            txtTransportAllowance.Text = "";
            txtCostSharing.Text = "";
            txtCostsharingPayable.Text = "";
            cmbEmpType.SelectedIndex = 0;
            cmbCostSharingStatus.SelectedIndex = 0;
            if (layoutCostSharingPayable.IsHidden)
                Size = new Size(800, 673);
            else
                Size = new Size(800, 688);
            if (cmbCostSharingStatus.SelectedIndex == 0)
            {
                Size = new Size(800, 673);
            }

            txtTransportAllowance.Properties.ReadOnly = true;
            chkIsShareHolder.Checked = false;
            picEmployeePic.Image = null;
            txtEmployeeID.Focus();
        }
        public void ChangeButtonTextToUpdate()
        {
            btnAdd.Text = "&Update";
        }
        private void FireNewEmployeeAdded(Employee employee)
        {
            if (NewEmployeeAdded != null)
            {
                NewEmployeeAdded(employee);
            }
        }

        private void FireEmployeeUpdated(Employee updatedEmployee)
        {
            if (EmployeeUpdated != null)
            {
                EmployeeUpdated(updatedEmployee);
            }
        }

        private void txtTransportAllowance_EditValueChanged(object sender, EventArgs e)
        {
            if (txtGrossSalary.Text != "" && txtTransportAllowance.Text != "")
            {
                double transportAllowance = double.Parse(txtTransportAllowance.Text);
                double taxableAmount;

                if (double.Parse(txtGrossSalary.Text) != 0) //If Gross salary is entered
                {
                    InitializeTransportAllowanceValidation();
                    validationTransportAllowance.Validate(txtTransportAllowance);
                    taxableAmount = TransportAllowanceValidationRule.ValidateTransportAllowance(double.Parse(txtGrossSalary.Text), transportAllowance, out m_Message);

                    if (layoutCostSharingPayable.IsHidden)
                        Size = new Size(800, 673);
                    else
                        Size = new Size(800, 688);

                    //if (cmbCostSharingStatus.SelectedIndex == 0)
                    //{
                    //    Size = new Size(559, 580);
                    //}
                }
            }
            else
            {
                if (txtTransportAllowance.Text == "")
                    validationTransportAllowance.RemoveControlError(txtTransportAllowance);
            }

        }

        private void txtGrossSalary_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtGrossSalary.Text != "")
                {
                    if (double.Parse(txtGrossSalary.Text) != 0)
                    {
                        txtTransportAllowance.Properties.ReadOnly = false;
                        txtCostSharing.Properties.ReadOnly = false;
                        if (cmbCostSharingStatus.SelectedIndex == 1) //Evidence not provided
                        {
                            double costSharingPayable = CostSharingPayableValidationRule.CalculateMaximumCostSharingPayable(double.Parse(txtGrossSalary.Text));
                            txtCostsharingPayable.Text = costSharingPayable.ToString("N");
                        }
                        if (cmbCostSharingStatus.SelectedIndex == 0) //Evidence provided
                        {
                            if (txtCostSharing.Text != "")
                            {
                                if (double.Parse(txtCostSharing.Text) != 0d)
                                {
                                    double costSharingPayable = CostSharingPayableValidationRule.CalculateLeastCostSharingPayable(double.Parse(txtGrossSalary.Text));
                                    txtCostsharingPayable.Text = costSharingPayable.ToString("N");
                                }
                            }
                        }
                    }
                    else
                    {
                        RearrangeControlForEmptyGrossSalary();
                    }
                }
                else
                {
                    RearrangeControlForEmptyGrossSalary();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.ToString());
            }

        }

        private void RearrangeControlForEmptyGrossSalary()
        {
            //txtTransportAllowance.Text = "";
            if (layoutCostSharingPayable.IsHidden)
                Size = new Size(800, 673);
            else
                Size = new Size(800, 688);
            txtTransportAllowance.Properties.ReadOnly = true;
            txtCostSharing.Text = "";
            txtCostSharing.Properties.ReadOnly = true;
            txtCostsharingPayable.Text = "";
            txtCostsharingPayable.Properties.ReadOnly = true;
            layoutCostSharingPayable.HideToCustomization();
            cmbCostSharingStatus.SelectedIndex = 2;
        }

        private void txtCostSharing_EditValueChanged(object sender, EventArgs e)
        {
            if (txtCostSharing.Text != "")
            {
                if (double.Parse(txtCostSharing.Text) > 0d)
                {
                    double payableAmount = CostSharingPayableValidationRule.CalculateLeastCostSharingPayable(double.Parse(txtGrossSalary.Text));
                    layoutCostSharingPayable.RestoreFromCustomization(layoutCostSharingDebt, InsertType.Bottom);
                    Size = new Size(800, 688);
                    txtCostsharingPayable.Text = payableAmount.ToString("N");
                    txtCostsharingPayable.Properties.ReadOnly = false;
                }
                else
                {
                    Size = new Size(800, 673);
                    txtCostsharingPayable.Text = "";
                    txtCostsharingPayable.Properties.ReadOnly = true;
                    layoutCostSharingPayable.HideToCustomization();
                }
            }
        }



        private void cmbCostSharingStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCostSharingStatus.SelectedIndex == 0) //Evidence provided
            {
                if (layoutCostSharingDebt.IsHidden)
                {
                    if (!layoutCostSharingPayable.IsHidden)
                    {
                        layoutCostSharingPayable.HideToCustomization();
                    }
                    layoutCostSharingDebt.RestoreFromCustomization(layoutCostSharingStatus, InsertType.Bottom);
                    txtCostSharing.Text = "";
                    txtCostsharingPayable.Text = "";
                    txtCostsharingPayable.Properties.ReadOnly = false;
                }
            }
            else if (cmbCostSharingStatus.SelectedIndex == 1) //Evidence not provided
            {
                layoutCostSharingDebt.HideToCustomization();
                txtCostsharingPayable.Properties.ReadOnly = true;
                if (layoutCostSharingPayable.IsHidden)
                {
                    layoutCostSharingPayable.RestoreFromCustomization(layoutCostSharingStatus, InsertType.Bottom);
                }
                txtCostSharing.Text = "";

                if (txtGrossSalary.Text != "")
                {
                    if (double.Parse(txtGrossSalary.Text) != 0d)
                    {
                        double payableAmount = CostSharingPayableValidationRule.CalculateMaximumCostSharingPayable(double.Parse(txtGrossSalary.Text));
                        txtCostsharingPayable.Text = payableAmount.ToString("N");
                    }
                    else
                    {
                        txtCostsharingPayable.Text = "";
                    }
                }
                else
                {
                    txtCostsharingPayable.Text = "";
                }

            }
            else //Not applicable
            {
                layoutCostSharingDebt.HideToCustomization();
                layoutCostSharingPayable.HideToCustomization();
                txtCostSharing.Text = "";
                txtCostsharingPayable.Text = "";
            }
        }

        private void EmployeeEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void txtCostsharingPayable_EditValueChanged(object sender, EventArgs e)
        {
            if (txtCostsharingPayable.Text != "")
            {
                InitializeCostSharingPayableValidation();
                dxValidationProvider1.Validate(txtCostsharingPayable);
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            picEmployeePic.LoadImage();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            picEmployeePic.Image = null;
        }

        private void EmployeeEditor_Shown(object sender, EventArgs e)
        {
            validationTransportAllowance.Validate();
        }

        private void radSalaryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!m_Editable)
                txtGrossSalary.Text = "";

            if (radSalaryType.SelectedIndex == 0)
            {
                txtGrossSalary.Properties.NullValuePrompt = "Enter Monthly Salary";
            }
            else
            {
                txtGrossSalary.Properties.NullValuePrompt = "Enter Daily Salary";
            }
        }

        private void PrepareWorkDataTable()
        {
            tableEmployees = new DataTable();
            tableEmployees.RowChanged += new DataRowChangeEventHandler(tableEmployees_RowChanged);
            tableEmployees.Columns.Add("Component", typeof(string));
            tableEmployees.Columns.Add("Data", typeof(string));
            tableEmployees.Columns.Add("Amount", typeof(double));
            tableEmployees.Columns.Add("FormulaID", typeof(int));
            tableEmployees.Columns.Add("Remark", typeof(string));

            gridControlWorkData.DataSource = tableEmployees;
            gridViewWorkData.Columns["Component"].OptionsColumn.AllowEdit = false;
            gridViewWorkData.Columns["Data"].OptionsColumn.AllowEdit = false;
            gridViewWorkData.Columns["FormulaID"].Visible = false;
            gridViewWorkData.Columns["Amount"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gridViewWorkData.Columns["Amount"].DisplayFormat.FormatString = "#####0.00;";
            gridViewWorkData.Columns["Component"].SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
            gridViewWorkData.Columns["Data"].SortOrder = DevExpress.Data.ColumnSortOrder.None;
            gridViewWorkData.Columns["Remark"].OptionsColumn.AllowEdit = false;

            gridViewWorkData.GroupFormat = String.Format("{0}{1}", "", "{1}");


        }

        void tableEmployees_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            if (_ignoreDataChangeEvent)
                return;
            _workingDataChanged = true;
            layoutWorkData.Text = "Work Data*";
            
        }

        private void DisplayEmployeeWorkData(int empID)
        {
            _ignoreDataChangeEvent = true;
            try
            {
                tableEmployees.Clear();

                #region Regular work data
                int regularFormulaID;
                Data_RegularTime regularTimeData = GetRegularTimeData(empID, out regularFormulaID);
                if (regularTimeData != null)
                {
                    #region Working Days
                    DataRow wd_row = tableEmployees.NewRow();
                    wd_row[0] = REGULAR_TIME;
                    wd_row[1] = "Working Days";
                    wd_row[2] = (double)regularTimeData.WorkingDays;
                    wd_row[3] = regularFormulaID;
                    tableEmployees.Rows.Add(wd_row);
                    #endregion
                    #region Working Hours
                    DataRow wh_row = tableEmployees.NewRow();
                    wh_row[0] = REGULAR_TIME;
                    wh_row[1] = "Working Hours";
                    wh_row[2] = (double)regularTimeData.WorkingHours;
                    wh_row[3] = regularFormulaID;
                    tableEmployees.Rows.Add(wh_row);
                    #endregion
                    #region Partial Working Hours
                    DataRow pwh_row = tableEmployees.NewRow();
                    pwh_row[0] = REGULAR_TIME;
                    pwh_row[1] = "Partial Working Hours";
                    pwh_row[2] = (double)regularTimeData.PartialWorkingHour;
                    pwh_row[3] = regularFormulaID;
                    tableEmployees.Rows.Add(pwh_row);
                    #endregion
                }
                #endregion

                #region Over Time data
                int overTimeFormulaID;
                OverTimeData overTimeData = GetOverTimeData(empID, out overTimeFormulaID);
                if (overTimeData != null)
                {
                    #region Off Hours
                    DataRow offhrs_row = tableEmployees.NewRow();
                    offhrs_row[0] = OVER_TIME;
                    offhrs_row[1] = "Off Hours";
                    offhrs_row[2] = overTimeData.offHours;
                    offhrs_row[3] = overTimeFormulaID;
                    tableEmployees.Rows.Add(offhrs_row);
                    #endregion
                    #region Late Hours
                    DataRow latehrs_row = tableEmployees.NewRow();
                    latehrs_row[0] = OVER_TIME;
                    latehrs_row[1] = "Late Hours";
                    latehrs_row[2] = overTimeData.lateHours;
                    latehrs_row[3] = overTimeFormulaID;
                    tableEmployees.Rows.Add(latehrs_row);
                    #endregion
                    #region Weekend Hours
                    DataRow weekendhrs_row = tableEmployees.NewRow();
                    weekendhrs_row[0] = OVER_TIME;
                    weekendhrs_row[1] = "Weekend Hours";
                    weekendhrs_row[2] = overTimeData.weekendHours;
                    weekendhrs_row[3] = overTimeFormulaID;
                    tableEmployees.Rows.Add(weekendhrs_row);
                    #endregion
                    #region Holiday Hours
                    DataRow holidayhrs_row = tableEmployees.NewRow();
                    holidayhrs_row[0] = OVER_TIME;
                    holidayhrs_row[1] = "Holiday Hours";
                    holidayhrs_row[2] = overTimeData.holidayHours;
                    holidayhrs_row[3] = overTimeFormulaID;
                    tableEmployees.Rows.Add(holidayhrs_row);
                    #endregion
                }
                #endregion

                #region General data
                string[] formulas;
                int[] formulaID;
                double[] values = GetGeneralData(empID, out formulas, out formulaID);
                if (values != null)
                {
                    int j = 0;
                    foreach (string name in formulas)
                    {
                        DataRow row = tableEmployees.NewRow();
                        row[0] = OTHERS;
                        row[1] = name;
                        row[2] = values[j];
                        row[3] = formulaID[j];
                        tableEmployees.Rows.Add(row);
                        j++;
                    }
                }
                string[] remark;
                values = GetPermanentBenefitData(empID, out formulas, out formulaID,out remark);
                if (values != null)
                {
                    int j = 0;
                    foreach (string name in formulas)
                    {
                        DataRow row = tableEmployees.NewRow();
                        row[0] = PERMANENT_BENEFIT;
                        row[1] = name;
                        row[2] = values[j];
                        row[3] = formulaID[j];
                        row[4] = remark[j];
                        tableEmployees.Rows.Add(row);
                        j++;
                    }
                }
                #endregion

                gridControlWorkData.DataSource = tableEmployees;
                gridViewWorkData.RefreshData();
                if (tableEmployees.Rows.Count > 0)
                {
                    gridViewWorkData.Columns["Component"].Group();
                    gridViewWorkData.ExpandAllGroups();
                }
                _workingDataChanged = false;
                layoutWorkData.Text = "Work Data";
            }
            finally
            {
                _ignoreDataChangeEvent = false;
            }
        }

        private Data_RegularTime GetRegularTimeData(int empID, out int formulaID)
        {
            Data_RegularTime data = null;
            formulaID = -1;
            int pcdID = (int)iERPTransactionClient.GetSystemParamter("RegularTimePayrollFormulaID");
            PayrollComponentFormula regularFormula = PayrollClient.GetPCFormula(pcdID);
            if (regularFormula == null)
                return null;
            PayrollComponentData[] ret = PayrollClient.GetPCData(pcdID, regularFormula.ID, AppliesToObjectType.Employee, empID, comboPeriod.selectedPeriodID, false);
            formulaID = regularFormula.ID;
            if (empID != -1)
            {
                if (ret.Length > 0)
                {
                    object regularTimeData = PayrollClient.GetPCAdditionalData(ret[0].ID);
                    if (regularTimeData != null)
                    {
                        data = (Data_RegularTime)regularTimeData;
                    }
                }
                else
                    data = new Data_RegularTime();
            }
            else
            {
                data = new Data_RegularTime();
            }

            return data;
        }

        private OverTimeData GetOverTimeData(int empID, out int formulaID)
        {
            OverTimeData data = null;
            formulaID = -1;
            int pcdID = (int)iERPTransactionClient.GetSystemParamter("OverTimePayrollFormulaID");

            PayrollComponentFormula overTimeFormula = PayrollClient.GetPCFormula(pcdID);
            if (overTimeFormula==null)
                return null;

            PayrollComponentData[] ret = PayrollClient.GetPCData(pcdID, overTimeFormula.ID, AppliesToObjectType.Employee, empID, comboPeriod.selectedPeriodID, false);
            formulaID = overTimeFormula.ID;
            if (empID != -1)
            {
                if (ret.Length > 0)
                {
                    object overTimeData = PayrollClient.GetPCAdditionalData(ret[0].ID);
                    if (overTimeData != null)
                    {
                        data = (OverTimeData)overTimeData;
                    }

                }
                else
                    data = new OverTimeData();
            }
            else
            {
                data = new OverTimeData();
            }
            return data;
        }

        private double[] GetGeneralData(int empID, out string[] formulas,out int[]formulaID)
        {
            List<double> amounts = new List<double>();
            List<string> names = new List<string>();
            List<int> ids = new List<int>();
            int pcdID = (int)iERPTransactionClient.GetSystemParamter("SingleValuePayrollComponentID");
            if (pcdID != 0)
            {
                PayrollComponentFormula[] singleValueFormula = PayrollClient.GetPCFormulae(pcdID);
                if (singleValueFormula.Length == 0)
                {
                    formulas = null;
                    formulaID = null;
                    return null;
                }

                int i = 0;
                foreach (PayrollComponentFormula formula in singleValueFormula)
                {
                    if (empID != -1)
                    {
                        PayrollComponentData[] ret = PayrollClient.GetPCData(pcdID, singleValueFormula[i].ID, AppliesToObjectType.Employee, empID, comboPeriod.selectedPeriodID, false);
                        if (ret.Length > 0)
                        {
                            object singleValueData = PayrollClient.GetPCAdditionalData(ret[0].ID);
                            if (singleValueData != null)
                            {
                                SingleValueData data = (SingleValueData)singleValueData;
                                amounts.Add(data.value);
                                names.Add(formula.Name);
                                ids.Add(formula.ID);
                            }
                        }
                        else
                        {
                            amounts.Add(0d);
                            names.Add(formula.Name);
                            ids.Add(formula.ID);
                        }
                    }
                    else
                    {
                        amounts.Add(0d);
                        names.Add(formula.Name);
                        ids.Add(formula.ID);
                    }
                    i++;
                }

            }
            formulas = names.ToArray();
            formulaID = ids.ToArray();
            return amounts.ToArray();
        }

        private double[] GetPermanentBenefitData(int empID, out string[] formulas, out int[] formulaID,out string[] remark)
        {
            List<double> amounts = new List<double>();
            List<string> names = new List<string>();
            List<int> ids = new List<int>();
            List<string> _remark = new List<string>();
            int pcdID = (int)iERPTransactionClient.GetSystemParamter("permanentBenefitComponentID");
            if (pcdID != 0)
            {
                PayrollComponentFormula[] permanentBenefitFormula = PayrollClient.GetPCFormulae(pcdID);
                if (permanentBenefitFormula.Length == 0)
                {
                    formulas = null;
                    formulaID = null;
                    remark = null;
                    return null;
                }

                int i = 0;
                foreach (PayrollComponentFormula formula in permanentBenefitFormula)
                {
                    if (empID != -1)
                    {
                        PayrollComponentData[] ret = PayrollClient.GetPCData(pcdID, permanentBenefitFormula[i].ID, AppliesToObjectType.Employee, empID, comboPeriod.selectedPeriodID, false);
                        if (ret.Length > 0)
                        {
                            object singleValueData = PayrollClient.GetPCAdditionalData(ret[0].ID);
                            if (singleValueData != null)
                            {
                                SingleValueData data = (SingleValueData)singleValueData;
                                amounts.Add(data.value);
                                names.Add(formula.Name);
                                ids.Add(formula.ID);
                                PayPeriodRange range = ret[0].periodRange;
                                if (range.FiniteRange)
                                    _remark.Add(PayrollClient.GetPayPeriod(ret[0].periodRange.PeriodFrom).name + " to " + PayrollClient.GetPayPeriod(ret[0].periodRange.PeriodTo).name);
                                else
                                    _remark.Add("Since " + PayrollClient.GetPayPeriod(ret[0].periodRange.PeriodFrom).name);
                            }
                        }
                        else
                        {
                            amounts.Add(0d);
                            names.Add(formula.Name);
                            ids.Add(formula.ID);
                            _remark.Add("");
                        }
                    }
                    else
                    {
                        amounts.Add(0d);
                        names.Add(formula.Name);
                        ids.Add(formula.ID);
                        _remark.Add("");
                    }
                    i++;
                }

            }
            formulas = names.ToArray();
            formulaID = ids.ToArray();
            remark = _remark.ToArray();
            return amounts.ToArray();
        }

        private double[] DeletePermanentBenefitData(int empID)
        {
            //List<double> amounts = new List<double>();
            //List<string> names = new List<string>();
            //List<int> ids = new List<int>();
            //List<string> _remark = new List<string>();
            //int pcdID = (int)iERPTransactionClient.GetSystemParamter("permanentBenefitComponentID");
            //if (pcdID != 0)
            //{
            //    PayrollComponentFormula[] permanentBenefitFormula = PayrollClient.GetPCFormulae(pcdID);

            //    int i = 0;
            //    foreach (PayrollComponentFormula formula in permanentBenefitFormula)
            //    {
            //        if (empID != -1)
            //        {
            //            PayrollComponentData[] ret = PayrollClient.GetPCData(pcdID, permanentBenefitFormula[i].ID, AppliesToObjectType.Employee, empID, comboPeriod.selectedPeriodID, false);
            //            if (ret.Length > 0)
            //            {
            //                object singleValueData = PayrollClient.GetPCAdditionalData(ret[0].ID);
            //                if (singleValueData != null)
            //                {
            //                    SingleValueData data = (SingleValueData)singleValueData;
            //                    amounts.Add(data.value);
            //                    names.Add(formula.Name);
            //                    ids.Add(formula.ID);
            //                    PayPeriodRange range = ret[0].periodRange;
            //                    if (range.FiniteRange)
            //                        _remark.Add(PayrollClient.GetPayPeriod(ret[0].periodRange.PeriodFrom).name + " to " + PayrollClient.GetPayPeriod(ret[0].periodRange.PeriodTo).name);
            //                    else
            //                        _remark.Add("Since " + PayrollClient.GetPayPeriod(ret[0].periodRange.PeriodFrom).name);
            //                }
            //            }
            //            else
            //            {
            //                amounts.Add(0d);
            //                names.Add(formula.Name);
            //                ids.Add(formula.ID);
            //                _remark.Add("");
            //            }
            //        }
            //        else
            //        {
            //            amounts.Add(0d);
            //            names.Add(formula.Name);
            //            ids.Add(formula.ID);
            //            _remark.Add("");
            //        }
            //        i++;
            //    }

            //}
            //formulas = names.ToArray();
            //formulaID = ids.ToArray();
            //remark = _remark.ToArray();
            //return amounts.ToArray();
            return new double[] { };
        }

        int _workDataPeriodID = -1;

        private void comboPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_ignoreDataChangeEvent)
                return;


            if (comboPeriod.SelectedIndex != -1)
            {
                int periodID = comboPeriod.selectedPeriodID;
                if (periodID == _workDataPeriodID)
                    return;
                if (_workingDataChanged)
                {
                    if (MessageBox.ShowWarningMessage("The changes you have made to the work data will be lost.\nAre you sure you want to continue?")
                        != DialogResult.Yes)
                    {
                        try
                        {
                            _ignoreDataChangeEvent = true;
                            comboPeriod.selectedPeriodID = _workDataPeriodID;
                            return;
                        }
                        finally
                        {
                            _ignoreDataChangeEvent = false;
                        }
                    }
                }
                _workDataPeriodID = periodID;
                DisplayEmployeeWorkData(m_ID);
            }

        }

        private void getWorkingData(out Data_RegularTime regularData, out OverTimeData overtimeData, out List<SingleValueData> singleData, out List<int> singleFormulaID)
        {
            regularData = new Data_RegularTime();
            overtimeData = new OverTimeData();
            singleData = new List<SingleValueData>();
            singleFormulaID = new List<int>();

            for (int i = 0; i < gridViewWorkData.DataRowCount; i++)
            {
                string component = (string)gridViewWorkData.GetRowCellValue(i, "Component");
                string data = (string)gridViewWorkData.GetRowCellValue(i, "Data");
                double amount = (double)gridViewWorkData.GetRowCellValue(i, "Amount");
                int formulaID = (int)gridViewWorkData.GetRowCellValue(i, "FormulaID");
                switch (component)
                {
                    case REGULAR_TIME:
                        if (data.Equals("Working Days"))
                            regularData.WorkingDays = amount;
                        else if (data.Equals("Working Hours"))
                            regularData.WorkingHours = amount;
                        else if (data.Equals("Partial Working Hours"))
                            regularData.PartialWorkingHour = amount;
                        break;

                    case OVER_TIME:
                        if (data.Equals("Off Hours"))
                            overtimeData.offHours = amount;
                        else if (data.Equals("Late Hours"))
                            overtimeData.lateHours = amount;
                        else if (data.Equals("Weekend Hours"))
                            overtimeData.weekendHours = amount;
                        else if (data.Equals("Holiday Hours"))
                            overtimeData.holidayHours = amount;
                        break;

                    case OTHERS:case PERMANENT_BENEFIT:
                        SingleValueData sdata = new SingleValueData();
                        sdata.value = amount;
                        singleData.Add(sdata);
                        singleFormulaID.Add(formulaID);
                        break;
                    default:
                        break;
                }
            }
        }

        private void EmployeeEditor_Load(object sender, EventArgs e)
        {

        }

        private void btnAddAccount_Click(object sender, EventArgs e)
        {
            if (m_picker == null)
                m_picker = CreatePicker();
            if (m_picker.ShowDialog() == DialogResult.OK)
            {
                AddObject(m_picker.selectedAccount.id);
            }
        }

        private AccountPicker<Account> CreatePicker()
        {
            AccountPicker<Account> ret;
            AccountTree acTree = new AccountTree();
            acTree.CostCenterAccountMode = false;
            ret = new AccountPicker<Account>(acTree) as AccountPicker<Account>;
            ret.OnlyLeafAccount = false;
            return ret;
        }
        private void AddObject(int accountID)
        {
            XtraPanel panel = new XtraPanel();
            AccountPlaceHolder box = new AccountPlaceHolder();
            box.AccountChanged += new EventHandler(box_AccountChanged);
            box.ReadOnly = true;
            box.Dock = DockStyle.Fill;
            try
            {
                    box.SetByID(accountID);
            }
            catch (Exception ex)
            {
                box.Text = ex.Message;
            }
            SimpleButton button = new SimpleButton();
            button.Height = box.Height;
            panel.Height = box.Height;
            button.Width = button.Height;
            button.Image = BIZNET.iERP.Client.Properties.Resources.deleteac1;
            button.ImageLocation = ImageLocation.MiddleCenter;
            button.Dock = DockStyle.Left;
            panel.Controls.Add(box);
            panel.Controls.Add(button);
            
            panel.Dock = DockStyle.Top;
            this.panelAccount.Controls.Add(panel);
            panel.BringToFront();
            button.Click += new EventHandler(this.DeleteButtonClick);
            Account account = AccountingClient.GetAccount<Account>(accountID);
            panel.Tag = account;
          
        }

        void box_AccountChanged(object sender, EventArgs e)
        {
            Account account = AccountingClient.GetAccount<Account>(((AccountPlaceHolder)sender).GetAccountID());
            ((XtraPanel)((AccountPlaceHolder)sender).Parent).Tag = account;
        }
        private void DeleteButtonClick(object sender, EventArgs e)
        {
            SimpleButton button = (SimpleButton)sender;
            Account account = (Account)((XtraPanel)button.Parent).Tag;
            if (MessageBox.ShowWarningMessage("Are you sure  you want to remove the employee from " + account.CodeName + "?")==DialogResult.Yes)
            {
                try
                {
                    int accountID = account.id;
                    int empAccount = iERPTransactionClient.GetEmployeeAccountID(PayrollClient.GetEmployee(m_ID), accountID);
                    if (empAccount != -1)
                        iERPTransactionClient.DeleteEmployeeAccount(m_ID, empAccount);
                    button.Click -= new EventHandler(this.DeleteButtonClick);
                    this.panelAccount.Controls.Remove(button.Parent);
                }
                catch (Exception exception)
                {
                    MessageBox.ShowErrorMessage("Failed to remove empoyee from " + ((Account)button.Tag).CodeName + "\n" + exception.Message);
                }
            }
        }

        private void btnPrintSlip_Click(object sender, EventArgs e)
        {
            try
            {
                PrintFormViewer viewer = new PrintFormViewer();
                PayrollSlip slip = new PayrollSlip(m_ID, comboPeriod.selectedPeriodID);
                viewer.LoadReport(slip);
                if (slip.IsPayrollGenerated)
                    viewer.ShowDialog();
                else
                    MessageBox.ShowErrorMessage("Payroll is not generated for selected period " + PayrollClient.GetPayPeriod(comboPeriod.selectedPeriodID).name);
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void comboVersions_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_ignoreDataChangeEvent)
                return;
            VersionObject version=(VersionObject)comboVersions.SelectedItem;
            if(version.version!=-1)
            {
                initWithEmployee(PayrollClient.GetEmployee(m_ID, version.version));
            }
            fixVersionControls();
        }

        private void buttonDeleteVersion_Click(object sender, EventArgs e)
        {
            VersionObject version = comboVersions.SelectedItem as VersionObject;
            if (version==null || version.version == -1)
                return;
            if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete the selected version? The data will be removed from the database imediately and you will not be able to undo it."))
                return;
            try
            {
                PayrollClient.deleteEmployeeVersion(m_ID, version.version);
                int index = comboVersions.SelectedIndex;
                comboVersions.Properties.Items.Remove(version);
                if (index == comboVersions.Properties.Items.Count)
                    comboVersions.SelectedIndex = index - 1;
                else
                    comboVersions.SelectedIndex = index;
            }
            catch(Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }

        private void txtBankAccountNo_EditValueChanged(object sender, EventArgs e)
        {
            employeeProfileChanged();
        }

        private void employeeProfileChanged()
        {
            if (_ignoreDataChangeEvent)
                return;
            _employeeProfileChanged = true;
            layoutEmployeeProfile.Text = "Employee Profile*";
        }
    }
}