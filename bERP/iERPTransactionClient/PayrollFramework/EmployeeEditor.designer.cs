﻿namespace BIZNET.iERP.Client
{
    partial class EmployeeEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmployeeEditor));
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.accountExpense = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.txtBankAccountNo = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.dateTimeVersion = new BIZNET.iERP.Client.BNDualCalendar();
            this.comboVersions = new DevExpress.XtraEditors.ComboBoxEdit();
            this.buttonDeleteVersion = new DevExpress.XtraEditors.SimpleButton();
            this.layoutVersion = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnPrintSlip = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnAddAccount = new DevExpress.XtraEditors.SimpleButton();
            this.textTitle = new DevExpress.XtraEditors.TextEdit();
            this.comboLoginName = new DevExpress.XtraEditors.ComboBoxEdit();
            this.checkAccountAsCost = new DevExpress.XtraEditors.CheckEdit();
            this.costCenterPlaceHolder = new INTAPS.Accounting.Client.CostCenterPlaceHolder();
            this.comboPeriod = new BIZNET.iERP.Client.PeriodSelector();
            this.gridControlWorkData = new DevExpress.XtraGrid.GridControl();
            this.gridViewWorkData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.cmbTaxCenter = new DevExpress.XtraEditors.ComboBoxEdit();
            this.radSalaryType = new DevExpress.XtraEditors.RadioGroup();
            this.dateEnrolled = new BIZNET.iERP.Client.BNDualCalendar();
            this.gridAccountInfo = new DevExpress.XtraGrid.GridControl();
            this.gridViewAccountInfo = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.btnBrowse = new DevExpress.XtraEditors.SimpleButton();
            this.picEmployeePic = new DevExpress.XtraEditors.PictureEdit();
            this.dateBirth = new BIZNET.iERP.Client.BNDualCalendar();
            this.cmbCostSharingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtEmployeeID = new DevExpress.XtraEditors.TextEdit();
            this.txtCostsharingPayable = new DevExpress.XtraEditors.TextEdit();
            this.cmbSex = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtUnit = new DevExpress.XtraEditors.TextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.txtCostSharing = new DevExpress.XtraEditors.TextEdit();
            this.txtTransportAllowance = new DevExpress.XtraEditors.TextEdit();
            this.txtGrossSalary = new DevExpress.XtraEditors.TextEdit();
            this.cmbEmpType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtAddress = new DevExpress.XtraEditors.TextEdit();
            this.txtTelephone = new DevExpress.XtraEditors.TextEdit();
            this.txtTIN = new DevExpress.XtraEditors.TextEdit();
            this.txtEmployeeName = new DevExpress.XtraEditors.TextEdit();
            this.chkIsShareHolder = new DevExpress.XtraEditors.CheckEdit();
            this.panelAccount = new DevExpress.XtraEditors.PanelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutEmployeeProfile = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCostSharingDebt = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCostSharingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCostSharingPayable = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAccountInformation = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutWorkData = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutOtherEmployeeAccounts = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutConfirm = new DevExpress.XtraLayout.LayoutControlItem();
            this.dxValidationProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.validationTransportAllowance = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccountNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboVersions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textTitle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboLoginName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAccountAsCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWorkData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWorkData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTaxCenter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSalaryType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAccountInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAccountInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picEmployeePic.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCostSharingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCostsharingPayable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSex.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCostSharing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransportAllowance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrossSalary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEmpType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelephone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsShareHolder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutEmployeeProfile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCostSharingDebt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCostSharingStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCostSharingPayable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAccountInformation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWorkData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOtherEmployeeAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutConfirm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationTransportAllowance)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Controls.Add(this.accountExpense);
            this.layoutControl1.Controls.Add(this.txtBankAccountNo);
            this.layoutControl1.Controls.Add(this.layoutControl2);
            this.layoutControl1.Controls.Add(this.btnPrintSlip);
            this.layoutControl1.Controls.Add(this.panelControl2);
            this.layoutControl1.Controls.Add(this.textTitle);
            this.layoutControl1.Controls.Add(this.comboLoginName);
            this.layoutControl1.Controls.Add(this.checkAccountAsCost);
            this.layoutControl1.Controls.Add(this.costCenterPlaceHolder);
            this.layoutControl1.Controls.Add(this.comboPeriod);
            this.layoutControl1.Controls.Add(this.gridControlWorkData);
            this.layoutControl1.Controls.Add(this.cmbTaxCenter);
            this.layoutControl1.Controls.Add(this.radSalaryType);
            this.layoutControl1.Controls.Add(this.dateEnrolled);
            this.layoutControl1.Controls.Add(this.gridAccountInfo);
            this.layoutControl1.Controls.Add(this.btnClear);
            this.layoutControl1.Controls.Add(this.btnBrowse);
            this.layoutControl1.Controls.Add(this.picEmployeePic);
            this.layoutControl1.Controls.Add(this.dateBirth);
            this.layoutControl1.Controls.Add(this.cmbCostSharingStatus);
            this.layoutControl1.Controls.Add(this.txtEmployeeID);
            this.layoutControl1.Controls.Add(this.txtCostsharingPayable);
            this.layoutControl1.Controls.Add(this.cmbSex);
            this.layoutControl1.Controls.Add(this.txtUnit);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.txtCostSharing);
            this.layoutControl1.Controls.Add(this.txtTransportAllowance);
            this.layoutControl1.Controls.Add(this.txtGrossSalary);
            this.layoutControl1.Controls.Add(this.cmbEmpType);
            this.layoutControl1.Controls.Add(this.txtAddress);
            this.layoutControl1.Controls.Add(this.txtTelephone);
            this.layoutControl1.Controls.Add(this.txtTIN);
            this.layoutControl1.Controls.Add(this.txtEmployeeName);
            this.layoutControl1.Controls.Add(this.chkIsShareHolder);
            this.layoutControl1.Controls.Add(this.panelAccount);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(654, 248, 347, 350);
            this.layoutControl1.OptionsView.HighlightFocusedItem = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(775, 674);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // accountExpense
            // 
            this.accountExpense.account = null;
            this.accountExpense.AllowAdd = false;
            this.accountExpense.Location = new System.Drawing.Point(594, 445);
            this.accountExpense.Name = "accountExpense";
            this.accountExpense.OnlyLeafAccount = false;
            this.accountExpense.Size = new System.Drawing.Size(161, 20);
            this.accountExpense.TabIndex = 52;
            // 
            // txtBankAccountNo
            // 
            this.txtBankAccountNo.Location = new System.Drawing.Point(86, 163);
            this.txtBankAccountNo.Name = "txtBankAccountNo";
            this.txtBankAccountNo.Size = new System.Drawing.Size(397, 20);
            this.txtBankAccountNo.StyleController = this.layoutControl1;
            this.txtBankAccountNo.TabIndex = 51;
            this.txtBankAccountNo.EditValueChanged += new System.EventHandler(this.txtBankAccountNo_EditValueChanged);
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.dateTimeVersion);
            this.layoutControl2.Controls.Add(this.comboVersions);
            this.layoutControl2.Controls.Add(this.buttonDeleteVersion);
            this.layoutControl2.Location = new System.Drawing.Point(11, 33);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(261, 237, 250, 350);
            this.layoutControl2.Root = this.layoutVersion;
            this.layoutControl2.Size = new System.Drawing.Size(753, 72);
            this.layoutControl2.TabIndex = 50;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // dateTimeVersion
            // 
            this.dateTimeVersion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateTimeVersion.Location = new System.Drawing.Point(314, 4);
            this.dateTimeVersion.Name = "dateTimeVersion";
            this.dateTimeVersion.ShowEthiopian = true;
            this.dateTimeVersion.ShowGregorian = true;
            this.dateTimeVersion.ShowTime = true;
            this.dateTimeVersion.Size = new System.Drawing.Size(305, 63);
            this.dateTimeVersion.TabIndex = 50;
            this.dateTimeVersion.VerticalLayout = true;
            // 
            // comboVersions
            // 
            this.comboVersions.Location = new System.Drawing.Point(44, 6);
            this.comboVersions.Name = "comboVersions";
            this.comboVersions.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboVersions.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboVersions.Size = new System.Drawing.Size(264, 20);
            this.comboVersions.StyleController = this.layoutControl2;
            this.comboVersions.TabIndex = 47;
            this.comboVersions.SelectedIndexChanged += new System.EventHandler(this.comboVersions_SelectedIndexChanged);
            // 
            // buttonDeleteVersion
            // 
            this.buttonDeleteVersion.Location = new System.Drawing.Point(623, 4);
            this.buttonDeleteVersion.Name = "buttonDeleteVersion";
            this.buttonDeleteVersion.Size = new System.Drawing.Size(126, 22);
            this.buttonDeleteVersion.StyleController = this.layoutControl2;
            this.buttonDeleteVersion.TabIndex = 49;
            this.buttonDeleteVersion.Text = "Delete Version";
            this.buttonDeleteVersion.Click += new System.EventHandler(this.buttonDeleteVersion_Click);
            // 
            // layoutVersion
            // 
            this.layoutVersion.CustomizationFormText = "layoutVersion";
            this.layoutVersion.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutVersion.GroupBordersVisible = false;
            this.layoutVersion.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem28,
            this.layoutControlItem30,
            this.layoutControlItem29});
            this.layoutVersion.Location = new System.Drawing.Point(0, 0);
            this.layoutVersion.Name = "layoutVersion";
            this.layoutVersion.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutVersion.Size = new System.Drawing.Size(753, 72);
            this.layoutVersion.Text = "layoutVersion";
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.comboVersions;
            this.layoutControlItem28.CustomizationFormText = "Version";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(310, 68);
            this.layoutControlItem28.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem28.Text = "Version";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(35, 13);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.buttonDeleteVersion;
            this.layoutControlItem30.CustomizationFormText = "layoutControlItem30";
            this.layoutControlItem30.Location = new System.Drawing.Point(619, 0);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(130, 68);
            this.layoutControlItem30.Text = "layoutControlItem30";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem30.TextToControlDistance = 0;
            this.layoutControlItem30.TextVisible = false;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.dateTimeVersion;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(310, 0);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(309, 68);
            this.layoutControlItem29.Text = "layoutControlItem29";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem29.TextToControlDistance = 0;
            this.layoutControlItem29.TextVisible = false;
            // 
            // btnPrintSlip
            // 
            this.btnPrintSlip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrintSlip.Appearance.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintSlip.Appearance.Options.UseFont = true;
            this.btnPrintSlip.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintSlip.Image")));
            this.btnPrintSlip.Location = new System.Drawing.Point(370, 33);
            this.btnPrintSlip.Name = "btnPrintSlip";
            this.btnPrintSlip.Size = new System.Drawing.Size(194, 44);
            this.btnPrintSlip.StyleController = this.layoutControl1;
            this.btnPrintSlip.TabIndex = 1;
            this.btnPrintSlip.Text = "&Print Payslip";
            this.btnPrintSlip.Click += new System.EventHandler(this.btnPrintSlip_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.btnAddAccount);
            this.panelControl2.Location = new System.Drawing.Point(11, 33);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(753, 43);
            this.panelControl2.TabIndex = 45;
            // 
            // btnAddAccount
            // 
            this.btnAddAccount.Image = global::BIZNET.iERP.Client.Properties.Resources.add;
            this.btnAddAccount.Location = new System.Drawing.Point(0, 1);
            this.btnAddAccount.Name = "btnAddAccount";
            this.btnAddAccount.Size = new System.Drawing.Size(142, 26);
            this.btnAddAccount.TabIndex = 0;
            this.btnAddAccount.Text = "Add Control Account";
            this.btnAddAccount.Click += new System.EventHandler(this.btnAddAccount_Click);
            // 
            // textTitle
            // 
            this.textTitle.Location = new System.Drawing.Point(345, 217);
            this.textTitle.Name = "textTitle";
            this.textTitle.Size = new System.Drawing.Size(140, 20);
            this.textTitle.StyleController = this.layoutControl1;
            this.textTitle.TabIndex = 44;
            // 
            // comboLoginName
            // 
            this.comboLoginName.Location = new System.Drawing.Point(318, 316);
            this.comboLoginName.Name = "comboLoginName";
            this.comboLoginName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboLoginName.Size = new System.Drawing.Size(165, 20);
            this.comboLoginName.StyleController = this.layoutControl1;
            this.comboLoginName.TabIndex = 43;
            // 
            // checkAccountAsCost
            // 
            this.checkAccountAsCost.Location = new System.Drawing.Point(263, 445);
            this.checkAccountAsCost.Name = "checkAccountAsCost";
            this.checkAccountAsCost.Properties.Caption = "Account as Cost";
            this.checkAccountAsCost.Size = new System.Drawing.Size(211, 19);
            this.checkAccountAsCost.StyleController = this.layoutControl1;
            this.checkAccountAsCost.TabIndex = 42;
            // 
            // costCenterPlaceHolder
            // 
            this.costCenterPlaceHolder.account = null;
            this.costCenterPlaceHolder.AllowAdd = false;
            this.costCenterPlaceHolder.Location = new System.Drawing.Point(123, 471);
            this.costCenterPlaceHolder.Name = "costCenterPlaceHolder";
            this.costCenterPlaceHolder.OnlyLeafAccount = false;
            this.costCenterPlaceHolder.Size = new System.Drawing.Size(258, 20);
            this.costCenterPlaceHolder.TabIndex = 41;
            // 
            // comboPeriod
            // 
            this.comboPeriod.Location = new System.Drawing.Point(66, 33);
            this.comboPeriod.Name = "comboPeriod";
            this.comboPeriod.PeriodType = "AP_Payroll";
            this.comboPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboPeriod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboPeriod.Size = new System.Drawing.Size(300, 20);
            this.comboPeriod.StyleController = this.layoutControl1;
            this.comboPeriod.TabIndex = 40;
            this.comboPeriod.SelectedIndexChanged += new System.EventHandler(this.comboPeriod_SelectedIndexChanged);
            // 
            // gridControlWorkData
            // 
            this.gridControlWorkData.Location = new System.Drawing.Point(11, 81);
            this.gridControlWorkData.MainView = this.gridViewWorkData;
            this.gridControlWorkData.Name = "gridControlWorkData";
            this.gridControlWorkData.Size = new System.Drawing.Size(753, 502);
            this.gridControlWorkData.TabIndex = 39;
            this.gridControlWorkData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWorkData});
            // 
            // gridViewWorkData
            // 
            this.gridViewWorkData.GridControl = this.gridControlWorkData;
            this.gridViewWorkData.Name = "gridViewWorkData";
            this.gridViewWorkData.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewWorkData.OptionsCustomization.AllowFilter = false;
            this.gridViewWorkData.OptionsCustomization.AllowGroup = false;
            this.gridViewWorkData.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewWorkData.OptionsCustomization.AllowSort = false;
            this.gridViewWorkData.OptionsView.ShowGroupPanel = false;
            // 
            // cmbTaxCenter
            // 
            this.cmbTaxCenter.Location = new System.Drawing.Point(443, 472);
            this.cmbTaxCenter.Name = "cmbTaxCenter";
            this.cmbTaxCenter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTaxCenter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbTaxCenter.Size = new System.Drawing.Size(313, 20);
            this.cmbTaxCenter.StyleController = this.layoutControl1;
            this.cmbTaxCenter.TabIndex = 38;
            // 
            // radSalaryType
            // 
            this.radSalaryType.EditValue = true;
            this.radSalaryType.Location = new System.Drawing.Point(79, 498);
            this.radSalaryType.Name = "radSalaryType";
            this.radSalaryType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Monthly"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Daily")});
            this.radSalaryType.Size = new System.Drawing.Size(303, 25);
            this.radSalaryType.StyleController = this.layoutControl1;
            this.radSalaryType.TabIndex = 37;
            this.radSalaryType.SelectedIndexChanged += new System.EventHandler(this.radSalaryType_SelectedIndexChanged);
            // 
            // dateEnrolled
            // 
            this.dateEnrolled.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateEnrolled.Location = new System.Drawing.Point(19, 392);
            this.dateEnrolled.Name = "dateEnrolled";
            this.dateEnrolled.ShowEthiopian = true;
            this.dateEnrolled.ShowGregorian = true;
            this.dateEnrolled.ShowTime = false;
            this.dateEnrolled.Size = new System.Drawing.Size(411, 21);
            this.dateEnrolled.TabIndex = 33;
            this.dateEnrolled.VerticalLayout = false;
            // 
            // gridAccountInfo
            // 
            this.gridAccountInfo.Location = new System.Drawing.Point(11, 33);
            this.gridAccountInfo.MainView = this.gridViewAccountInfo;
            this.gridAccountInfo.Name = "gridAccountInfo";
            this.gridAccountInfo.Size = new System.Drawing.Size(753, 550);
            this.gridAccountInfo.TabIndex = 31;
            this.gridAccountInfo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAccountInfo,
            this.gridView2});
            // 
            // gridViewAccountInfo
            // 
            this.gridViewAccountInfo.GridControl = this.gridAccountInfo;
            this.gridViewAccountInfo.Name = "gridViewAccountInfo";
            this.gridViewAccountInfo.OptionsBehavior.Editable = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowFilter = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowGroup = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowRowSizing = true;
            this.gridViewAccountInfo.OptionsCustomization.AllowSort = false;
            this.gridViewAccountInfo.OptionsView.ShowGroupPanel = false;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridAccountInfo;
            this.gridView2.Name = "gridView2";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(621, 323);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(143, 22);
            this.btnClear.StyleController = this.layoutControl1;
            this.btnClear.TabIndex = 29;
            this.btnClear.Text = "&Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(496, 323);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(121, 22);
            this.btnBrowse.StyleController = this.layoutControl1;
            this.btnBrowse.TabIndex = 28;
            this.btnBrowse.Text = "&Browse";
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // picEmployeePic
            // 
            this.picEmployeePic.Location = new System.Drawing.Point(496, 125);
            this.picEmployeePic.Name = "picEmployeePic";
            this.picEmployeePic.Properties.ShowMenu = false;
            this.picEmployeePic.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picEmployeePic.Size = new System.Drawing.Size(268, 194);
            this.picEmployeePic.StyleController = this.layoutControl1;
            this.picEmployeePic.TabIndex = 27;
            // 
            // dateBirth
            // 
            this.dateBirth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateBirth.Location = new System.Drawing.Point(18, 259);
            this.dateBirth.Name = "dateBirth";
            this.dateBirth.ShowEthiopian = true;
            this.dateBirth.ShowGregorian = true;
            this.dateBirth.ShowTime = false;
            this.dateBirth.Size = new System.Drawing.Size(411, 21);
            this.dateBirth.TabIndex = 32;
            this.dateBirth.VerticalLayout = false;
            // 
            // cmbCostSharingStatus
            // 
            this.cmbCostSharingStatus.EditValue = "Not applicable";
            this.cmbCostSharingStatus.Location = new System.Drawing.Point(491, 529);
            this.cmbCostSharingStatus.Name = "cmbCostSharingStatus";
            this.cmbCostSharingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCostSharingStatus.Properties.Items.AddRange(new object[] {
            "Cost-Sharing debt evidence provided",
            "Cost-Sharing debt evidence not provided",
            "Not applicable"});
            this.cmbCostSharingStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbCostSharingStatus.Size = new System.Drawing.Size(265, 20);
            this.cmbCostSharingStatus.StyleController = this.layoutControl1;
            this.cmbCostSharingStatus.TabIndex = 24;
            this.cmbCostSharingStatus.SelectedIndexChanged += new System.EventHandler(this.cmbCostSharingStatus_SelectedIndexChanged);
            // 
            // txtEmployeeID
            // 
            this.txtEmployeeID.Location = new System.Drawing.Point(56, 191);
            this.txtEmployeeID.Name = "txtEmployeeID";
            this.txtEmployeeID.Properties.Mask.EditMask = "\\w.+";
            this.txtEmployeeID.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtEmployeeID.Size = new System.Drawing.Size(192, 20);
            this.txtEmployeeID.StyleController = this.layoutControl1;
            this.txtEmployeeID.TabIndex = 23;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "Employee ID cannot be blank";
            this.dxValidationProvider1.SetValidationRule(this.txtEmployeeID, conditionValidationRule1);
            // 
            // txtCostsharingPayable
            // 
            this.txtCostsharingPayable.Location = new System.Drawing.Point(538, 555);
            this.txtCostsharingPayable.Name = "txtCostsharingPayable";
            this.txtCostsharingPayable.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtCostsharingPayable.Properties.Mask.EditMask = "#,########0.00;";
            this.txtCostsharingPayable.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCostsharingPayable.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtCostsharingPayable.Properties.ReadOnly = true;
            this.txtCostsharingPayable.Size = new System.Drawing.Size(218, 20);
            this.txtCostsharingPayable.StyleController = this.layoutControl1;
            this.txtCostsharingPayable.TabIndex = 22;
            this.txtCostsharingPayable.EditValueChanged += new System.EventHandler(this.txtCostsharingPayable_EditValueChanged);
            // 
            // cmbSex
            // 
            this.cmbSex.EditValue = "Male";
            this.cmbSex.Location = new System.Drawing.Point(43, 288);
            this.cmbSex.Name = "cmbSex";
            this.cmbSex.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSex.Properties.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.cmbSex.Size = new System.Drawing.Size(205, 20);
            this.cmbSex.StyleController = this.layoutControl1;
            this.cmbSex.TabIndex = 21;
            // 
            // txtUnit
            // 
            this.txtUnit.Location = new System.Drawing.Point(44, 135);
            this.txtUnit.Name = "txtUnit";
            this.txtUnit.Properties.ReadOnly = true;
            this.txtUnit.Size = new System.Drawing.Size(441, 20);
            this.txtUnit.StyleController = this.layoutControl1;
            this.txtUnit.TabIndex = 19;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnClose);
            this.panelControl1.Controls.Add(this.btnAdd);
            this.panelControl1.Location = new System.Drawing.Point(5, 593);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(765, 76);
            this.panelControl1.TabIndex = 18;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(677, 40);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(81, 31);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Location = new System.Drawing.Point(573, 40);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(85, 31);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "&Add";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtCostSharing
            // 
            this.txtCostSharing.Location = new System.Drawing.Point(136, 555);
            this.txtCostSharing.Name = "txtCostSharing";
            this.txtCostSharing.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtCostSharing.Properties.Mask.EditMask = "#,########0.00;";
            this.txtCostSharing.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCostSharing.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtCostSharing.Properties.ReadOnly = true;
            this.txtCostSharing.Size = new System.Drawing.Size(247, 20);
            this.txtCostSharing.StyleController = this.layoutControl1;
            this.txtCostSharing.TabIndex = 16;
            this.txtCostSharing.EditValueChanged += new System.EventHandler(this.txtCostSharing_EditValueChanged);
            // 
            // txtTransportAllowance
            // 
            this.txtTransportAllowance.Location = new System.Drawing.Point(124, 529);
            this.txtTransportAllowance.Name = "txtTransportAllowance";
            this.txtTransportAllowance.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtTransportAllowance.Properties.Mask.EditMask = "#,########0.00;";
            this.txtTransportAllowance.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTransportAllowance.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTransportAllowance.Properties.ReadOnly = true;
            this.txtTransportAllowance.Size = new System.Drawing.Size(259, 20);
            this.txtTransportAllowance.StyleController = this.layoutControl1;
            this.txtTransportAllowance.TabIndex = 13;
            this.txtTransportAllowance.EditValueChanged += new System.EventHandler(this.txtTransportAllowance_EditValueChanged);
            // 
            // txtGrossSalary
            // 
            this.txtGrossSalary.Location = new System.Drawing.Point(425, 498);
            this.txtGrossSalary.Name = "txtGrossSalary";
            this.txtGrossSalary.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtGrossSalary.Properties.Mask.EditMask = "#,########0.00;";
            this.txtGrossSalary.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGrossSalary.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtGrossSalary.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtGrossSalary.Size = new System.Drawing.Size(331, 20);
            this.txtGrossSalary.StyleController = this.layoutControl1;
            this.txtGrossSalary.TabIndex = 12;
            this.txtGrossSalary.EditValueChanged += new System.EventHandler(this.txtGrossSalary_EditValueChanged);
            // 
            // cmbEmpType
            // 
            this.cmbEmpType.EditValue = "Permanent";
            this.cmbEmpType.Location = new System.Drawing.Point(114, 418);
            this.cmbEmpType.Name = "cmbEmpType";
            this.cmbEmpType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbEmpType.Properties.Items.AddRange(new object[] {
            "Permanent",
            "Temporary"});
            this.cmbEmpType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbEmpType.Size = new System.Drawing.Size(642, 20);
            this.cmbEmpType.StyleController = this.layoutControl1;
            this.cmbEmpType.TabIndex = 11;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(64, 314);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Properties.Mask.EditMask = "\\w.+";
            this.txtAddress.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtAddress.Size = new System.Drawing.Size(184, 20);
            this.txtAddress.StyleController = this.layoutControl1;
            this.txtAddress.TabIndex = 9;
            // 
            // txtTelephone
            // 
            this.txtTelephone.Location = new System.Drawing.Point(313, 288);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Properties.Mask.EditMask = "(\\+251)\\d\\d\\d-\\d\\d-\\d\\d-\\d\\d";
            this.txtTelephone.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.txtTelephone.Size = new System.Drawing.Size(172, 20);
            this.txtTelephone.StyleController = this.layoutControl1;
            this.txtTelephone.TabIndex = 8;
            // 
            // txtTIN
            // 
            this.txtTIN.Location = new System.Drawing.Point(278, 191);
            this.txtTIN.Name = "txtTIN";
            this.txtTIN.Properties.Mask.EditMask = "\\d+";
            this.txtTIN.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTIN.Size = new System.Drawing.Size(207, 20);
            this.txtTIN.StyleController = this.layoutControl1;
            this.txtTIN.TabIndex = 5;
            // 
            // txtEmployeeName
            // 
            this.txtEmployeeName.Location = new System.Drawing.Point(52, 217);
            this.txtEmployeeName.Name = "txtEmployeeName";
            this.txtEmployeeName.Properties.Mask.EditMask = "[a-zA-Z].+";
            this.txtEmployeeName.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtEmployeeName.Size = new System.Drawing.Size(260, 20);
            this.txtEmployeeName.StyleController = this.layoutControl1;
            this.txtEmployeeName.TabIndex = 4;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "Employee name cannot be blank";
            this.dxValidationProvider1.SetValidationRule(this.txtEmployeeName, conditionValidationRule2);
            // 
            // chkIsShareHolder
            // 
            this.chkIsShareHolder.Location = new System.Drawing.Point(20, 445);
            this.chkIsShareHolder.Name = "chkIsShareHolder";
            this.chkIsShareHolder.Properties.Caption = "Shareholder";
            this.chkIsShareHolder.Size = new System.Drawing.Size(235, 19);
            this.chkIsShareHolder.StyleController = this.layoutControl1;
            this.chkIsShareHolder.TabIndex = 26;
            // 
            // panelAccount
            // 
            this.panelAccount.Location = new System.Drawing.Point(11, 97);
            this.panelAccount.Name = "panelAccount";
            this.panelAccount.Size = new System.Drawing.Size(753, 486);
            this.panelAccount.TabIndex = 46;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1,
            this.layoutConfirm});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(775, 674);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.tabbedControlGroup1.SelectedTabPage = this.layoutOtherEmployeeAccounts;
            this.tabbedControlGroup1.SelectedTabPageIndex = 3;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(769, 588);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutEmployeeProfile,
            this.layoutAccountInformation,
            this.layoutWorkData,
            this.layoutOtherEmployeeAccounts});
            this.tabbedControlGroup1.Text = "tabbedControlGroup1";
            // 
            // layoutEmployeeProfile
            // 
            this.layoutEmployeeProfile.CustomizationFormText = "Employee Profile";
            this.layoutEmployeeProfile.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlGroup3,
            this.layoutControlGroup2,
            this.layoutControlItem31});
            this.layoutEmployeeProfile.Location = new System.Drawing.Point(0, 0);
            this.layoutEmployeeProfile.Name = "layoutEmployeeProfile";
            this.layoutEmployeeProfile.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AutoSize;
            this.layoutEmployeeProfile.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutEmployeeProfile.Size = new System.Drawing.Size(757, 554);
            this.layoutEmployeeProfile.Text = "Employee Profile";
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.picEmployeePic;
            this.layoutControlItem12.CustomizationFormText = "Picture:";
            this.layoutControlItem12.Location = new System.Drawing.Point(485, 76);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(272, 214);
            this.layoutControlItem12.Text = "Picture:";
            this.layoutControlItem12.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(37, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.btnBrowse;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(485, 290);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(125, 26);
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.btnClear;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(610, 290);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(147, 26);
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Employment Information";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem19,
            this.layoutControlItem23,
            this.layoutControlItem7,
            this.layoutControlItem24,
            this.layoutCostSharingDebt,
            this.layoutCostSharingStatus,
            this.layoutCostSharingPayable,
            this.layoutControlItem11,
            this.layoutControlItem22,
            this.layoutControlItem25,
            this.layoutControlItem6,
            this.layoutControlItem33});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 316);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutControlGroup3.Size = new System.Drawing.Size(757, 238);
            this.layoutControlGroup3.Text = "Employment Information";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtTransportAllowance;
            this.layoutControlItem8.CustomizationFormText = "Transport Allowance:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 153);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(370, 26);
            this.layoutControlItem8.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem8.Text = "Transport Allowance:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.dateEnrolled;
            this.layoutControlItem19.CustomizationFormText = "Enrolled Date:";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(443, 42);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(443, 42);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(743, 42);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem19.Text = "Enrolled Date:";
            this.layoutControlItem19.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(68, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.radSalaryType;
            this.layoutControlItem23.CustomizationFormText = "Salary Type";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 122);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(369, 31);
            this.layoutControlItem23.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem23.Text = "Salary Type";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(57, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtGrossSalary;
            this.layoutControlItem7.CustomizationFormText = "Salary:";
            this.layoutControlItem7.Location = new System.Drawing.Point(369, 122);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(374, 31);
            this.layoutControlItem7.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem7.Text = "Salary:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(34, 13);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.cmbTaxCenter;
            this.layoutControlItem24.CustomizationFormText = "Tax Center";
            this.layoutControlItem24.Location = new System.Drawing.Point(367, 96);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(376, 26);
            this.layoutControlItem24.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem24.Text = "Tax Center";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutCostSharingDebt
            // 
            this.layoutCostSharingDebt.Control = this.txtCostSharing;
            this.layoutCostSharingDebt.CustomizationFormText = "Toal cost sharing debt:";
            this.layoutCostSharingDebt.Location = new System.Drawing.Point(0, 179);
            this.layoutCostSharingDebt.Name = "layoutCostSharingDebt";
            this.layoutCostSharingDebt.Size = new System.Drawing.Size(370, 26);
            this.layoutCostSharingDebt.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutCostSharingDebt.Text = "Toal Cost-Sharing debt:";
            this.layoutCostSharingDebt.TextSize = new System.Drawing.Size(114, 13);
            // 
            // layoutCostSharingStatus
            // 
            this.layoutCostSharingStatus.Control = this.cmbCostSharingStatus;
            this.layoutCostSharingStatus.CustomizationFormText = "Cost-Sharing status:";
            this.layoutCostSharingStatus.Location = new System.Drawing.Point(370, 153);
            this.layoutCostSharingStatus.Name = "layoutCostSharingStatus";
            this.layoutCostSharingStatus.Size = new System.Drawing.Size(373, 26);
            this.layoutCostSharingStatus.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutCostSharingStatus.Text = "Cost-Sharing status:";
            this.layoutCostSharingStatus.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutCostSharingPayable
            // 
            this.layoutCostSharingPayable.Control = this.txtCostsharingPayable;
            this.layoutCostSharingPayable.CustomizationFormText = "Cost-Sharing payable amount:";
            this.layoutCostSharingPayable.Location = new System.Drawing.Point(370, 179);
            this.layoutCostSharingPayable.Name = "layoutCostSharingPayable";
            this.layoutCostSharingPayable.Size = new System.Drawing.Size(373, 26);
            this.layoutCostSharingPayable.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutCostSharingPayable.Text = "Cost-Sharing payable amount:";
            this.layoutCostSharingPayable.TextSize = new System.Drawing.Size(146, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.chkIsShareHolder;
            this.layoutControlItem11.CustomizationFormText = " ";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 68);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(243, 25);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(243, 25);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(243, 28);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem11.Text = " ";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.costCenterPlaceHolder;
            this.layoutControlItem22.CustomizationFormText = "Current Cost Center:";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(367, 26);
            this.layoutControlItem22.Text = "Current Cost Center:";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.checkAccountAsCost;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(243, 68);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(219, 28);
            this.layoutControlItem25.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem25.Text = "layoutControlItem25";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextToControlDistance = 0;
            this.layoutControlItem25.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.cmbEmpType;
            this.layoutControlItem6.CustomizationFormText = "Employment Status";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 42);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(680, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(680, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(743, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem6.Text = "Employment Status";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.accountExpense;
            this.layoutControlItem33.CustomizationFormText = "Expense/Cost Account:";
            this.layoutControlItem33.Location = new System.Drawing.Point(462, 68);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(281, 28);
            this.layoutControlItem33.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem33.Text = "Expense/Cost Account";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(109, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Basic Information";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.lblAddress,
            this.layoutControlItem1,
            this.layoutControlItem14,
            this.layoutControlItem4,
            this.layoutControlItem15,
            this.layoutControlItem26,
            this.layoutControlItem13,
            this.layoutControlItem16,
            this.layoutControlItem2,
            this.layoutControlItem32});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 76);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup2.Size = new System.Drawing.Size(485, 240);
            this.layoutControlGroup2.Text = "Basic Information";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtTelephone;
            this.layoutControlItem5.CustomizationFormText = "Telephone:";
            this.layoutControlItem5.Location = new System.Drawing.Point(236, 153);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(237, 26);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem5.Text = "Telephone:";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(54, 13);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // lblAddress
            // 
            this.lblAddress.Control = this.txtAddress;
            this.lblAddress.CustomizationFormText = "Address:";
            this.lblAddress.Location = new System.Drawing.Point(0, 179);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(236, 30);
            this.lblAddress.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.lblAddress.Text = "Address:";
            this.lblAddress.TextSize = new System.Drawing.Size(43, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtEmployeeName;
            this.layoutControlItem1.CustomizationFormText = "Employee Name:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 82);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(300, 26);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem1.Text = "Name:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(31, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txtUnit;
            this.layoutControlItem14.CustomizationFormText = "Unit:";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(473, 26);
            this.layoutControlItem14.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem14.Text = "Unit:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(23, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.dateBirth;
            this.layoutControlItem4.CustomizationFormText = "Birth Date:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 108);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(473, 45);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(473, 45);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(473, 45);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem4.Text = "Birth Date:";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(52, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.cmbSex;
            this.layoutControlItem15.CustomizationFormText = "Sex:";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 153);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(236, 26);
            this.layoutControlItem15.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem15.Text = "Sex:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(22, 13);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.textTitle;
            this.layoutControlItem26.CustomizationFormText = "layoutControlItem26";
            this.layoutControlItem26.Location = new System.Drawing.Point(300, 82);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(173, 26);
            this.layoutControlItem26.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem26.Text = "Title:";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(24, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.comboLoginName;
            this.layoutControlItem13.CustomizationFormText = "Login Name:";
            this.layoutControlItem13.Location = new System.Drawing.Point(236, 179);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(237, 30);
            this.layoutControlItem13.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem13.Text = "Login Name:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(59, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.txtEmployeeID;
            this.layoutControlItem16.CustomizationFormText = "Employee Identification No.:";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(236, 26);
            this.layoutControlItem16.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem16.Text = "ID No.:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(35, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtTIN;
            this.layoutControlItem2.CustomizationFormText = "Tax Identification Number (TIN):";
            this.layoutControlItem2.Location = new System.Drawing.Point(236, 56);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(237, 26);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem2.Text = "TIN:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(21, 13);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.txtBankAccountNo;
            this.layoutControlItem32.CustomizationFormText = "Account No.:";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(473, 30);
            this.layoutControlItem32.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem32.Text = "Account No.:";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(63, 13);
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.layoutControl2;
            this.layoutControlItem31.CustomizationFormText = "layoutControlItem31";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem31.MaxSize = new System.Drawing.Size(0, 76);
            this.layoutControlItem31.MinSize = new System.Drawing.Size(308, 76);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(757, 76);
            this.layoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem31.Text = "layoutControlItem31";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem31.TextToControlDistance = 0;
            this.layoutControlItem31.TextVisible = false;
            // 
            // layoutAccountInformation
            // 
            this.layoutAccountInformation.CustomizationFormText = "Account Information";
            this.layoutAccountInformation.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutAccountInformation.Location = new System.Drawing.Point(0, 0);
            this.layoutAccountInformation.Name = "layoutAccountInformation";
            this.layoutAccountInformation.Size = new System.Drawing.Size(757, 554);
            this.layoutAccountInformation.Text = "Account Information";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridAccountInfo;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(757, 554);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutWorkData
            // 
            this.layoutWorkData.CustomizationFormText = "Employee Work Data";
            this.layoutWorkData.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem27,
            this.emptySpaceItem1});
            this.layoutWorkData.Location = new System.Drawing.Point(0, 0);
            this.layoutWorkData.Name = "layoutWorkData";
            this.layoutWorkData.Size = new System.Drawing.Size(757, 554);
            this.layoutWorkData.Text = "Work Data";
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.gridControlWorkData;
            this.layoutControlItem20.CustomizationFormText = "layoutControlItem20";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(757, 506);
            this.layoutControlItem20.Text = "layoutControlItem20";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextToControlDistance = 0;
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem21.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem21.Control = this.comboPeriod;
            this.layoutControlItem21.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem21.CustomizationFormText = "Period:";
            this.layoutControlItem21.FillControlToClientArea = false;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(109, 34);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(359, 48);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "Period:";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(50, 30);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.btnPrintSlip;
            this.layoutControlItem27.CustomizationFormText = "layoutControlItem27";
            this.layoutControlItem27.Location = new System.Drawing.Point(359, 0);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(142, 34);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(198, 48);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "layoutControlItem27";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem27.TextToControlDistance = 0;
            this.layoutControlItem27.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(557, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(200, 48);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutOtherEmployeeAccounts
            // 
            this.layoutOtherEmployeeAccounts.CustomizationFormText = "Other Employee Accounts";
            this.layoutOtherEmployeeAccounts.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem10});
            this.layoutOtherEmployeeAccounts.Location = new System.Drawing.Point(0, 0);
            this.layoutOtherEmployeeAccounts.Name = "layoutOtherEmployeeAccounts";
            this.layoutOtherEmployeeAccounts.Size = new System.Drawing.Size(757, 554);
            this.layoutOtherEmployeeAccounts.Text = "Other Employee Accounts";
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.panelControl2;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(757, 47);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.BackColor = System.Drawing.Color.Lavender;
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem10.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.Control = this.panelAccount;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 47);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(104, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(757, 507);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "Control Accounts";
            this.layoutControlItem10.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(107, 14);
            // 
            // layoutConfirm
            // 
            this.layoutConfirm.Control = this.panelControl1;
            this.layoutConfirm.CustomizationFormText = "layoutControlItem13";
            this.layoutConfirm.Location = new System.Drawing.Point(0, 588);
            this.layoutConfirm.Name = "layoutConfirm";
            this.layoutConfirm.Size = new System.Drawing.Size(769, 80);
            this.layoutConfirm.Text = "layoutConfirm";
            this.layoutConfirm.TextSize = new System.Drawing.Size(0, 0);
            this.layoutConfirm.TextToControlDistance = 0;
            this.layoutConfirm.TextVisible = false;
            // 
            // dxValidationProvider1
            // 
            this.dxValidationProvider1.ValidateHiddenControls = false;
            this.dxValidationProvider1.ValidationMode = DevExpress.XtraEditors.DXErrorProvider.ValidationMode.Manual;
            // 
            // validationTransportAllowance
            // 
            this.validationTransportAllowance.ValidateHiddenControls = false;
            // 
            // EmployeeEditor
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(775, 674);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "EmployeeEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Employee Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EmployeeEditor_FormClosing);
            this.Load += new System.EventHandler(this.EmployeeEditor_Load);
            this.Shown += new System.EventHandler(this.EmployeeEditor_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccountNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboVersions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textTitle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboLoginName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAccountAsCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWorkData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWorkData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTaxCenter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSalaryType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAccountInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAccountInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picEmployeePic.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCostSharingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCostsharingPayable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSex.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCostSharing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransportAllowance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrossSalary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEmpType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelephone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsShareHolder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutEmployeeProfile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCostSharingDebt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCostSharingStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCostSharingPayable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAccountInformation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWorkData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOtherEmployeeAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutConfirm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationTransportAllowance)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit txtEmployeeName;
        private DevExpress.XtraEditors.TextEdit txtTIN;
        private DevExpress.XtraEditors.TextEdit txtAddress;
        private DevExpress.XtraEditors.TextEdit txtTelephone;
        private DevExpress.XtraEditors.ComboBoxEdit cmbEmpType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.TextEdit txtGrossSalary;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.TextEdit txtCostSharing;
        private DevExpress.XtraEditors.TextEdit txtTransportAllowance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutCostSharingDebt;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutConfirm;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.TextEdit txtUnit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.ComboBoxEdit cmbSex;
        private DevExpress.XtraEditors.TextEdit txtCostsharingPayable;
        private DevExpress.XtraLayout.LayoutControlItem layoutCostSharingPayable;
        private DevExpress.XtraEditors.TextEdit txtEmployeeID;
        private DevExpress.XtraEditors.ComboBoxEdit cmbCostSharingStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutCostSharingStatus;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidationProvider1;
        private DevExpress.XtraEditors.CheckEdit chkIsShareHolder;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.PictureEdit picEmployeePic;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private DevExpress.XtraEditors.SimpleButton btnBrowse;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutEmployeeProfile;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem lblAddress;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlGroup layoutAccountInformation;
        private DevExpress.XtraGrid.GridControl gridAccountInfo;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewAccountInfo;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validationTransportAllowance;
        private BIZNET.iERP.Client.BNDualCalendar dateBirth;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private BIZNET.iERP.Client.BNDualCalendar dateEnrolled;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.RadioGroup radSalaryType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraEditors.ComboBoxEdit cmbTaxCenter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlGroup layoutWorkData;
        private DevExpress.XtraGrid.GridControl gridControlWorkData;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWorkData;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private PeriodSelector comboPeriod;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private INTAPS.Accounting.Client.CostCenterPlaceHolder costCenterPlaceHolder;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.CheckEdit checkAccountAsCost;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraEditors.ComboBoxEdit comboLoginName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.TextEdit textTitle;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlGroup layoutOtherEmployeeAccounts;
        private DevExpress.XtraEditors.PanelControl panelAccount;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.SimpleButton btnAddAccount;
        private DevExpress.XtraEditors.SimpleButton btnPrintSlip;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.ComboBoxEdit comboVersions;
        private DevExpress.XtraEditors.SimpleButton buttonDeleteVersion;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutVersion;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private BNDualCalendar dateTimeVersion;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraEditors.TextEdit txtBankAccountNo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountExpense;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
    }
}