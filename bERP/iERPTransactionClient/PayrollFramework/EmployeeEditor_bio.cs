﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;
using System.Collections;
using DevExpress.XtraEditors.DXErrorProvider;
using INTAPS.Payroll;
using INTAPS.Payroll.Client;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using System.IO;
using System.Drawing.Imaging;
using DevExpress.Utils;
using DevExpress.XtraGrid;

namespace BIZNET.iERP.Client
{
    public partial class EmployeeEditor_bio : DevExpress.XtraEditors.XtraForm
    {
        private CostSharingPayableValidationRule m_CostSharingPayableValidation;
        private TransportAllowanceValidationRule m_TransportAllowanceValidation;
        private double m_GrossSalary;
        
        OrgUnit _parentOrgUnit;
        int _employeeID = -1;
        EmployeeStatus _employeeStatus=EmployeeStatus.Enrolled;
        long _initVersion=-1;
        public EmployeeEditor_bio(int parentOrtUnitID)
        {
            InitializeComponent();
            initWithOrgUnitID(parentOrtUnitID);
        }
        public int employeeID
        {
            get
            {
                return _employeeID; 
            }
        }
        private void initWithOrgUnitID(int parentOrtUnitID)
        {
            
            PopulateLoginNames();
            dateEnrolled.DateTime = DateTime.Now;
            dateBirth.DateTime = DateTime.Now;
            layoutCostSharingPayable.HideToCustomization(); //Hide payable cost sharing textbox
            layoutCostSharingDebt.HideToCustomization();// Hide cost-sharing debt textbox
            txtEmployeeName.Focus();
            _parentOrgUnit = PayrollClient.GetOrgUnit(parentOrtUnitID);
            txtUnit.Text = _parentOrgUnit.Name;
            InitializeGrossSalaryValidation();
            txtEmployeeID.LostFocus += Control_LostFocus;
            txtEmployeeName.LostFocus += Control_LostFocus;
            txtGrossSalary.LostFocus += Control_LostFocus;
            txtCostsharingPayable.LostFocus += Control_LostFocus;
            LoadTaxCenters();
            costCenterPlaceHolder.SetByID(Globals.currentCostCenterID);
        }

        private void PopulateLoginNames()
        {
            try
            {
                comboLoginName.Properties.Items.AddRange(INTAPS.ClientServer.Client.SecurityClient.GetAllUsers());
            }
            catch
            {
                comboLoginName.Properties.Items.Clear();
            }
        }

        private void LoadTaxCenters()
        {
            TaxCenter[] taxCenter = iERPTransactionClient.GetTaxCenters();
            int companyTaxCenter=iERPTransactionClient.GetCompanyProfile().VATTaxCenter;            
            TaxCenter selected=null;
            foreach (TaxCenter center in taxCenter)
            {
                cmbTaxCenter.Properties.Items.Add(center);
                if(center.ID==companyTaxCenter)
                {
                    selected=center;
                }

            }
            if(selected==null && taxCenter.Length>0)
                cmbTaxCenter.SelectedIndex=0;
            else
                cmbTaxCenter.SelectedItem= selected;
        }

        private void InitializeGrossSalaryValidation()
        {
            #region Gross Salary Validation
            NonEmptyNumericValidationRule grossSalaryValidation = new NonEmptyNumericValidationRule();
            grossSalaryValidation.ErrorText = "Employee Gross Salary cannot be blank and cannot contain a value of zero";
            grossSalaryValidation.ErrorType = ErrorType.Default;

            dxValidationProvider1.SetValidationRule(txtGrossSalary, grossSalaryValidation);

            #endregion

        }

        private void InitializeCostSharingPayableValidation()
        {
            m_CostSharingPayableValidation = new CostSharingPayableValidationRule(getGrossSalary());
            m_CostSharingPayableValidation.ErrorType = ErrorType.Default;
            dxValidationProvider1.SetValidationRule(txtCostsharingPayable, m_CostSharingPayableValidation);
        }

        private void InitializeTransportAllowanceValidation()
        {
            m_TransportAllowanceValidation = new TransportAllowanceValidationRule(getGrossSalary());
            //  m_TransportAllowanceValidation.ErrorType = ErrorType.Default;
            validationTransportAllowance.SetValidationRule(txtTransportAllowance, m_TransportAllowanceValidation);
        }

        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            dxValidationProvider1.Validate(control);
        }
        public EmployeeEditor_bio(int employeeID, long version, bool SelectedEmployeeInfo)
        {
            InitializeComponent();
            if (SelectedEmployeeInfo)
                tabbedControlGroup2.SelectedTabPage = empInfoTab;
            _initVersion = version;
            _employeeID = employeeID;
            Employee employee = null;

            if (_initVersion == -1)
            {
                employee = PayrollClient.GetEmployee(employeeID, DateTime.Now.Ticks);
                dateTimeVersion.DateTime = DateTime.Now;
            }
            else
            {
                employee = PayrollClient.GetEmployee(employeeID, version);
                dateTimeVersion.DateTime = new DateTime(_initVersion);
                dateTimeVersion.Enabled = false;
            }
            initWithOrgUnitID(employee.orgUnitID);
            btnAdd.Text = "Save";
            _ignoreDataChangeEvent = true;
            initWithEmployee(employee);
        }

        private void initWithEmployee(Employee employee)
        {
            _employeeStatus = employee.status;
            txtUnit.Text = _parentOrgUnit.Name;
            txtEmployeeID.Text = employee.employeeID;
            txtEmployeeName.Text = employee.employeeName;
            txtBankAccountNo.Text = employee.BankAccountNo;
            txtTIN.Text = employee.TIN;
            txtTelephone.Text = employee.telephone;
            dateBirth.DateTime = employee.birthDate;
            cmbSex.SelectedIndex = (int)employee.sex;
            txtAddress.Text = employee.address;
            dateEnrolled.DateTime = employee.enrollmentDate;
            cmbEmpType.SelectedIndex = (int)employee.employmentType;
            txtGrossSalary.Text = employee.grossSalary == 0d ? "" : employee.grossSalary.ToString("N");
            cmbCostSharingStatus.SelectedIndex = (int)employee.costSharingStatus;
            txtCostSharing.Text = employee.totalCostSharingDebt == 0d ? "" : employee.totalCostSharingDebt.ToString("N");
            txtTransportAllowance.Text = employee.transportAllowance == 0d ? "" : employee.transportAllowance.ToString("N");
            txtCostsharingPayable.Text = employee.costSharingPayableAmount == 0d ? "" : employee.costSharingPayableAmount.ToString("N");
            chkIsShareHolder.Checked = employee.shareHolder;
            radSalaryType.SelectedIndex = employee.salaryKind == SalaryKind.Monthly ? 0 : 1;
            costCenterPlaceHolder.SetByID(employee.costCenterID);
            checkAccountAsCost.Checked = employee.accountAsCost;
            _parentOrgUnit = PayrollClient.GetOrgUnit(employee.orgUnitID);
            txtUnit.Text = _parentOrgUnit.Name;
            SetEmployeeTaxCenter(employee);
            SetEmployeeImage(employee);

            DisableAllControls(employee.status == EmployeeStatus.Fired);
            comboLoginName.Text = employee.loginName;
            textTitle.Text = employee.title;
            accountExpense.SetByID(employee.expenseParentAccountID);
        }

        private void DisableAllControls(bool readOnly)
        {
            if (readOnly)
                layoutConfirm.Visibility = LayoutVisibility.Never;
            else
                layoutConfirm.Visibility = LayoutVisibility.Always;
            txtUnit.Properties.ReadOnly = readOnly;
            txtEmployeeID.Properties.ReadOnly = readOnly;
            txtEmployeeName.Properties.ReadOnly = readOnly;
            txtTIN.Properties.ReadOnly = readOnly;
            txtTelephone.Properties.ReadOnly = readOnly;
            dateBirth.Enabled = !readOnly;
            txtAddress.Properties.ReadOnly = readOnly;
            cmbSex.Properties.ReadOnly = readOnly;
            dateEnrolled.Enabled = !readOnly;
            cmbEmpType.Properties.ReadOnly = readOnly;
            txtGrossSalary.Properties.ReadOnly = readOnly;
            cmbCostSharingStatus.Properties.ReadOnly = readOnly;
            txtCostSharing.Properties.ReadOnly = readOnly;
            chkIsShareHolder.Enabled = !readOnly;
            radSalaryType.Enabled = !readOnly;
            costCenterPlaceHolder.Enabled = !readOnly;
            checkAccountAsCost.Enabled = !readOnly;
            cmbTaxCenter.Properties.ReadOnly = readOnly;
            btnBrowse.Enabled = !readOnly;
            btnClear.Enabled = !readOnly;
        }
        bool _ignoreDataChangeEvent = false;
        bool _employeeProfileChanged = false;
        private void SetEmployeeTaxCenter(Employee employee)
        {
            int i = 0;
            foreach (TaxCenter center in cmbTaxCenter.Properties.Items)
            {
                if (center.ID == employee.TaxCenter)
                {
                    cmbTaxCenter.SelectedIndex = i;
                    break;
                }
                i++;
            }
        }
        
        CostCenter getCostCenter(Dictionary<int, CostCenter> costCenters, int costCenterID)
        {
            if (costCenters.ContainsKey(costCenterID))
                return costCenters[costCenterID];
            CostCenter cs = AccountingClient.GetAccount<CostCenter>(costCenterID);
            costCenters.Add(costCenterID, cs);
            return cs;
        }
        class CostCenterComparer : IComparer<CostCenter>
        {
            public int Compare(CostCenter x, CostCenter y)
            {
                return x.Code.CompareTo(y.Code);
            }
        }

        
        private void SetEmployeeImage(Employee employee)
        {
            INTAPS.ClientServer.ImageItem[] image = INTAPS.Accounting.Client.AccountingClient.GetImageItems(employee.picture);
            if (image != null)
            {
                if (image.Length > 0)
                {
                    byte[] bytes = INTAPS.Accounting.Client.AccountingClient.LoadImage(image[0].imageID, image[0].itemIndex, true, -1, -1).imageData;

                    using (MemoryStream stream = new MemoryStream(bytes))
                    {
                        picEmployeePic.Image = Image.FromStream(stream);
                    }

                }
            }
        }

        public double getGrossSalary()
        {
            double gs;
            if (double.TryParse(txtGrossSalary.Text, out gs))
            {
                return gs;
            }
            return gs;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            validationTransportAllowance.Validate();

            if (dxValidationProvider1.Validate())
            {
                if (dateBirth.DateTime.Date.CompareTo(DateTime.Now.Date) == 0 || dateBirth.DateTime.Date.CompareTo(dateEnrolled.DateTime.Date) == 0)
                {
                    MessageBox.ShowErrorMessage("Birth date can not be equal to the current or enrollment date");
                    return;
                }
                if (dateBirth.DateTime.Date.CompareTo(dateEnrolled.DateTime.Date) > 0)
                {
                    MessageBox.ShowErrorMessage("Birth date can not be greater than the enrollment date");
                    return;
                }
                if (dateEnrolled.DateTime.Date.CompareTo(DateTime.Now.Date) > 0)
                {
                    MessageBox.ShowErrorMessage("Enrollment date cannot be futured date");
                    return;
                }
                CostCenter cs = costCenterPlaceHolder.account;
                if (cs == null)
                {
                    MessageBox.ShowErrorMessage("Select cost center for the employee");
                    return;
                }
                if (cs.childCount > 1)
                {
                    MessageBox.ShowErrorMessage("Pick a cost center with no child");
                    return;
                }
                Employee employee = new Employee();
                employee.id = _employeeID;
                employee.orgUnitID = _parentOrgUnit.id;
                employee.employeeID = txtEmployeeID.Text;
                employee.employeeName = txtEmployeeName.Text;
                employee.TIN = txtTIN.Text;
                employee.enrollmentDate = dateEnrolled.DateTime;
                employee.telephone = txtTelephone.Text;
                employee.birthDate = dateBirth.DateTime;
                employee.sex = (Sex)cmbSex.SelectedIndex;
                employee.address = txtAddress.Text;
                employee.employmentType = (EmploymentType)cmbEmpType.SelectedIndex;
                employee.grossSalary = double.Parse(txtGrossSalary.Text);
                employee.transportAllowance = txtTransportAllowance.Text == "" ? 0d : double.Parse(txtTransportAllowance.Text);
                employee.totalCostSharingDebt = txtCostSharing.Text == "" ? 0d : double.Parse(txtCostSharing.Text);
                employee.costSharingPayableAmount = txtCostsharingPayable.Text == "" ? 0d : double.Parse(txtCostsharingPayable.Text);
                employee.costSharingStatus = (CostSharingStatus)cmbCostSharingStatus.SelectedIndex;
                employee.shareHolder = chkIsShareHolder.Checked;
                employee.status =  _employeeStatus;
                employee.salaryKind = (SalaryKind)radSalaryType.SelectedIndex;
                employee.TaxCenter = ((TaxCenter)cmbTaxCenter.SelectedItem).ID;
                employee.costCenterID = cs.id;
                employee.accountAsCost = checkAccountAsCost.Checked;
                employee.loginName = comboLoginName.Text;
                employee.BankAccountNo = txtBankAccountNo.Text;
                employee.title = textTitle.Text;
                employee.expenseParentAccountID = accountExpense.GetAccountID();
                byte[] picture;
                picture = GetEmployeeImageInBytes();
                try
                {
                    //version
                    if (_initVersion == -1)
                        employee.ticksFrom = dateTimeVersion.DateTime.Ticks;
                    else
                        employee.ticksFrom = _initVersion;
                    employee.id = iERPTransactionClient.RegisterEmployee(employee, picture, false, null, -1, null, null, null, null);
                    ClearControls();
                    MessageBox.ShowSuccessMessage("Employee successfully saved!");
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.ShowException(ex);
                }
            }
            else
            {
                MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
            }
        }

        private byte[] GetEmployeeImageInBytes()
        {
            byte[] picture = null;
            if (picEmployeePic.Image != null)
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    int newWidth = 128, newHeight = 128;
                    if (picEmployeePic.Image.Width > picEmployeePic.Image.Height)
                    {
                        newWidth = 128;
                        newHeight = 128 * picEmployeePic.Image.Height / picEmployeePic.Image.Width;
                    }
                    else if (picEmployeePic.Image.Width < picEmployeePic.Image.Height)
                    {
                        newHeight = 128;
                        newWidth = 128 * picEmployeePic.Image.Width / picEmployeePic.Image.Height;
                    }
                    using (Bitmap newPic = new Bitmap(picEmployeePic.Image, new Size(newWidth, newHeight)))
                    {
                        newPic.Save(stream, ImageFormat.Jpeg);
                    }
                    picture = stream.ToArray();
                }
            }
            return picture;
        }

        private void ClearControls()
        {
            txtEmployeeID.Text = "";
            txtEmployeeName.Text = "";
            txtTIN.Text = "";
            dateEnrolled.DateTime = DateTime.Now;
            txtTelephone.Text = "";
            txtAddress.Text = "";
            txtGrossSalary.Text = "";
            txtTransportAllowance.Text = "";
            txtCostSharing.Text = "";
            txtCostsharingPayable.Text = "";
            cmbEmpType.SelectedIndex = 0;
            cmbCostSharingStatus.SelectedIndex = 0;
            txtTransportAllowance.Properties.ReadOnly = true;
            chkIsShareHolder.Checked = false;
            picEmployeePic.Image = null;
            txtEmployeeID.Focus();
        }
        public void ChangeButtonTextToUpdate()
        {
            btnAdd.Text = "&Update";
        }
      
        private void txtTransportAllowance_EditValueChanged(object sender, EventArgs e)
        {
            if (txtGrossSalary.Text != "" && txtTransportAllowance.Text != "")
            {
                double transportAllowance = double.Parse(txtTransportAllowance.Text);
                double taxableAmount;

                if (double.Parse(txtGrossSalary.Text) != 0) //If Gross salary is entered
                {
                    InitializeTransportAllowanceValidation();
                    validationTransportAllowance.Validate(txtTransportAllowance);
                    string msg;
                    taxableAmount = TransportAllowanceValidationRule.ValidateTransportAllowance(double.Parse(txtGrossSalary.Text), transportAllowance, out msg);
                }
            }
            else
            {
                if (txtTransportAllowance.Text == "")
                    validationTransportAllowance.RemoveControlError(txtTransportAllowance);
            }

        }

        private void txtGrossSalary_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtGrossSalary.Text != "")
                {
                    if (double.Parse(txtGrossSalary.Text) != 0)
                    {
                        txtTransportAllowance.Properties.ReadOnly = false;
                        txtCostSharing.Properties.ReadOnly = false;
                        if (cmbCostSharingStatus.SelectedIndex == 1) //Evidence not provided
                        {
                            double costSharingPayable = CostSharingPayableValidationRule.CalculateMaximumCostSharingPayable(double.Parse(txtGrossSalary.Text));
                            txtCostsharingPayable.Text = costSharingPayable.ToString("N");
                        }
                        if (cmbCostSharingStatus.SelectedIndex == 0) //Evidence provided
                        {
                            if (txtCostSharing.Text != "")
                            {
                                if (double.Parse(txtCostSharing.Text) != 0d)
                                {
                                    double costSharingPayable = CostSharingPayableValidationRule.CalculateLeastCostSharingPayable(double.Parse(txtGrossSalary.Text));
                                    txtCostsharingPayable.Text = costSharingPayable.ToString("N");
                                }
                            }
                        }
                    }
                    else
                    {
                        RearrangeControlForEmptyGrossSalary();
                    }
                }
                else
                {
                    RearrangeControlForEmptyGrossSalary();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.ToString());
            }

        }

        private void RearrangeControlForEmptyGrossSalary()
        {
            //txtTransportAllowance.Text = "";
            txtTransportAllowance.Properties.ReadOnly = true;
            txtCostSharing.Text = "";
            txtCostSharing.Properties.ReadOnly = true;
            txtCostsharingPayable.Text = "";
            txtCostsharingPayable.Properties.ReadOnly = true;
            layoutCostSharingPayable.HideToCustomization();
            cmbCostSharingStatus.SelectedIndex = 2;
        }

        private void txtCostSharing_EditValueChanged(object sender, EventArgs e)
        {
            if (txtCostSharing.Text != "")
            {
                if (double.Parse(txtCostSharing.Text) > 0d)
                {
                    double payableAmount = CostSharingPayableValidationRule.CalculateLeastCostSharingPayable(double.Parse(txtGrossSalary.Text));
                    layoutCostSharingPayable.RestoreFromCustomization(layoutCostSharingDebt, InsertType.Bottom);
                    txtCostsharingPayable.Text = payableAmount.ToString("N");
                    txtCostsharingPayable.Properties.ReadOnly = false;
                }
                else
                {
                    txtCostsharingPayable.Text = "";
                    txtCostsharingPayable.Properties.ReadOnly = true;
                    layoutCostSharingPayable.HideToCustomization();
                }
            }
        }



        private void cmbCostSharingStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCostSharingStatus.SelectedIndex == 0) //Evidence provided
            {
                if (layoutCostSharingDebt.IsHidden)
                {
                    if (!layoutCostSharingPayable.IsHidden)
                    {
                        layoutCostSharingPayable.HideToCustomization();
                    }
                    layoutCostSharingDebt.RestoreFromCustomization(layoutCostSharingStatus, InsertType.Bottom);
                    txtCostSharing.Text = "";
                    txtCostsharingPayable.Text = "";
                    txtCostsharingPayable.Properties.ReadOnly = false;
                }
            }
            else if (cmbCostSharingStatus.SelectedIndex == 1) //Evidence not provided
            {
                layoutCostSharingDebt.HideToCustomization();
                txtCostsharingPayable.Properties.ReadOnly = true;
                if (layoutCostSharingPayable.IsHidden)
                {
                    layoutCostSharingPayable.RestoreFromCustomization(layoutCostSharingStatus, InsertType.Bottom);
                }
                txtCostSharing.Text = "";

                if (txtGrossSalary.Text != "")
                {
                    if (double.Parse(txtGrossSalary.Text) != 0d)
                    {
                        double payableAmount = CostSharingPayableValidationRule.CalculateMaximumCostSharingPayable(double.Parse(txtGrossSalary.Text));
                        txtCostsharingPayable.Text = payableAmount.ToString("N");
                    }
                    else
                    {
                        txtCostsharingPayable.Text = "";
                    }
                }
                else
                {
                    txtCostsharingPayable.Text = "";
                }

            }
            else //Not applicable
            {
                layoutCostSharingDebt.HideToCustomization();
                layoutCostSharingPayable.HideToCustomization();
                txtCostSharing.Text = "";
                txtCostsharingPayable.Text = "";
            }
        }

        private void EmployeeEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void txtCostsharingPayable_EditValueChanged(object sender, EventArgs e)
        {
            if (txtCostsharingPayable.Text != "")
            {
                InitializeCostSharingPayableValidation();
                dxValidationProvider1.Validate(txtCostsharingPayable);
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            picEmployeePic.LoadImage();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            picEmployeePic.Image = null;
        }

        private void EmployeeEditor_Shown(object sender, EventArgs e)
        {
            validationTransportAllowance.Validate();
        }

        private void radSalaryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtGrossSalary.Text = "";
            if (radSalaryType.SelectedIndex == 0)
            {
                txtGrossSalary.Properties.NullValuePrompt = "Enter Monthly Salary";
            }
            else
            {
                txtGrossSalary.Properties.NullValuePrompt = "Enter Daily Salary";
            }
        }

        private void txtBankAccountNo_EditValueChanged(object sender, EventArgs e)
        {
            employeeProfileChanged();
        }

        private void employeeProfileChanged()
        {
            if (_ignoreDataChangeEvent)
                return;
            _employeeProfileChanged = true;
            layoutEmployeeProfile.Text = "Employee Profile*";
        }
    }
}