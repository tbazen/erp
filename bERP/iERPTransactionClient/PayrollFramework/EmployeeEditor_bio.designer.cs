﻿namespace BIZNET.iERP.Client
{
    partial class EmployeeEditor_bio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.btnBrowse = new DevExpress.XtraEditors.SimpleButton();
            this.picEmployeePic = new DevExpress.XtraEditors.PictureEdit();
            this.dateTimeVersion = new BIZNET.iERP.Client.BNDualCalendar();
            this.accountExpense = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.txtBankAccountNo = new DevExpress.XtraEditors.TextEdit();
            this.textTitle = new DevExpress.XtraEditors.TextEdit();
            this.comboLoginName = new DevExpress.XtraEditors.ComboBoxEdit();
            this.checkAccountAsCost = new DevExpress.XtraEditors.CheckEdit();
            this.costCenterPlaceHolder = new INTAPS.Accounting.Client.CostCenterPlaceHolder();
            this.cmbTaxCenter = new DevExpress.XtraEditors.ComboBoxEdit();
            this.radSalaryType = new DevExpress.XtraEditors.RadioGroup();
            this.dateEnrolled = new BIZNET.iERP.Client.BNDualCalendar();
            this.dateBirth = new BIZNET.iERP.Client.BNDualCalendar();
            this.cmbCostSharingStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtEmployeeID = new DevExpress.XtraEditors.TextEdit();
            this.txtCostsharingPayable = new DevExpress.XtraEditors.TextEdit();
            this.cmbSex = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtUnit = new DevExpress.XtraEditors.TextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.txtCostSharing = new DevExpress.XtraEditors.TextEdit();
            this.txtTransportAllowance = new DevExpress.XtraEditors.TextEdit();
            this.txtGrossSalary = new DevExpress.XtraEditors.TextEdit();
            this.cmbEmpType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtAddress = new DevExpress.XtraEditors.TextEdit();
            this.txtTelephone = new DevExpress.XtraEditors.TextEdit();
            this.txtTIN = new DevExpress.XtraEditors.TextEdit();
            this.txtEmployeeName = new DevExpress.XtraEditors.TextEdit();
            this.chkIsShareHolder = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutConfirm = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutEmployeeProfile = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.basicInfoTab = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.empInfoTab = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCostSharingPayable = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCostSharingStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCostSharingDebt = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dxValidationProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.validationTransportAllowance = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEmployeePic.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccountNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textTitle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboLoginName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAccountAsCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTaxCenter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSalaryType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCostSharingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCostsharingPayable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSex.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCostSharing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransportAllowance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrossSalary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEmpType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelephone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsShareHolder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutConfirm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutEmployeeProfile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.basicInfoTab)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.empInfoTab)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCostSharingPayable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCostSharingStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCostSharingDebt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationTransportAllowance)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Controls.Add(this.panel1);
            this.layoutControl1.Controls.Add(this.dateTimeVersion);
            this.layoutControl1.Controls.Add(this.accountExpense);
            this.layoutControl1.Controls.Add(this.txtBankAccountNo);
            this.layoutControl1.Controls.Add(this.textTitle);
            this.layoutControl1.Controls.Add(this.comboLoginName);
            this.layoutControl1.Controls.Add(this.checkAccountAsCost);
            this.layoutControl1.Controls.Add(this.costCenterPlaceHolder);
            this.layoutControl1.Controls.Add(this.cmbTaxCenter);
            this.layoutControl1.Controls.Add(this.radSalaryType);
            this.layoutControl1.Controls.Add(this.dateEnrolled);
            this.layoutControl1.Controls.Add(this.dateBirth);
            this.layoutControl1.Controls.Add(this.cmbCostSharingStatus);
            this.layoutControl1.Controls.Add(this.txtEmployeeID);
            this.layoutControl1.Controls.Add(this.txtCostsharingPayable);
            this.layoutControl1.Controls.Add(this.cmbSex);
            this.layoutControl1.Controls.Add(this.txtUnit);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.txtCostSharing);
            this.layoutControl1.Controls.Add(this.txtTransportAllowance);
            this.layoutControl1.Controls.Add(this.txtGrossSalary);
            this.layoutControl1.Controls.Add(this.cmbEmpType);
            this.layoutControl1.Controls.Add(this.txtAddress);
            this.layoutControl1.Controls.Add(this.txtTelephone);
            this.layoutControl1.Controls.Add(this.txtTIN);
            this.layoutControl1.Controls.Add(this.txtEmployeeName);
            this.layoutControl1.Controls.Add(this.chkIsShareHolder);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(536, 88, 347, 350);
            this.layoutControl1.OptionsView.HighlightFocusedItem = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(774, 485);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnClear);
            this.panel1.Controls.Add(this.btnBrowse);
            this.panel1.Controls.Add(this.picEmployeePic);
            this.panel1.Location = new System.Drawing.Point(495, 60);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(257, 344);
            this.panel1.TabIndex = 53;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Location = new System.Drawing.Point(186, 241);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(68, 22);
            this.btnClear.StyleController = this.layoutControl1;
            this.btnClear.TabIndex = 30;
            this.btnClear.Text = "&Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowse.Location = new System.Drawing.Point(108, 241);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(72, 22);
            this.btnBrowse.StyleController = this.layoutControl1;
            this.btnBrowse.TabIndex = 29;
            this.btnBrowse.Text = "&Browse";
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // picEmployeePic
            // 
            this.picEmployeePic.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picEmployeePic.Location = new System.Drawing.Point(3, 3);
            this.picEmployeePic.Name = "picEmployeePic";
            this.picEmployeePic.Properties.ShowMenu = false;
            this.picEmployeePic.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picEmployeePic.Size = new System.Drawing.Size(251, 227);
            this.picEmployeePic.StyleController = this.layoutControl1;
            this.picEmployeePic.TabIndex = 28;
            // 
            // dateTimeVersion
            // 
            this.dateTimeVersion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateTimeVersion.Location = new System.Drawing.Point(24, 88);
            this.dateTimeVersion.Name = "dateTimeVersion";
            this.dateTimeVersion.ShowEthiopian = true;
            this.dateTimeVersion.ShowGregorian = true;
            this.dateTimeVersion.ShowTime = true;
            this.dateTimeVersion.Size = new System.Drawing.Size(465, 63);
            this.dateTimeVersion.TabIndex = 50;
            this.dateTimeVersion.VerticalLayout = true;
            // 
            // accountExpense
            // 
            this.accountExpense.account = null;
            this.accountExpense.AllowAdd = false;
            this.accountExpense.Location = new System.Drawing.Point(140, 218);
            this.accountExpense.Name = "accountExpense";
            this.accountExpense.OnlyLeafAccount = false;
            this.accountExpense.Size = new System.Drawing.Size(610, 20);
            this.accountExpense.TabIndex = 52;
            // 
            // txtBankAccountNo
            // 
            this.txtBankAccountNo.Location = new System.Drawing.Point(117, 225);
            this.txtBankAccountNo.Name = "txtBankAccountNo";
            this.txtBankAccountNo.Size = new System.Drawing.Size(134, 20);
            this.txtBankAccountNo.StyleController = this.layoutControl1;
            this.txtBankAccountNo.TabIndex = 51;
            this.txtBankAccountNo.EditValueChanged += new System.EventHandler(this.txtBankAccountNo_EditValueChanged);
            // 
            // textTitle
            // 
            this.textTitle.Location = new System.Drawing.Point(50, 197);
            this.textTitle.Name = "textTitle";
            this.textTitle.Size = new System.Drawing.Size(233, 20);
            this.textTitle.StyleController = this.layoutControl1;
            this.textTitle.TabIndex = 44;
            // 
            // comboLoginName
            // 
            this.comboLoginName.Location = new System.Drawing.Point(323, 326);
            this.comboLoginName.Name = "comboLoginName";
            this.comboLoginName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboLoginName.Size = new System.Drawing.Size(165, 20);
            this.comboLoginName.StyleController = this.layoutControl1;
            this.comboLoginName.TabIndex = 43;
            // 
            // checkAccountAsCost
            // 
            this.checkAccountAsCost.Location = new System.Drawing.Point(395, 191);
            this.checkAccountAsCost.Name = "checkAccountAsCost";
            this.checkAccountAsCost.Properties.Caption = "Account as Cost";
            this.checkAccountAsCost.Size = new System.Drawing.Size(356, 19);
            this.checkAccountAsCost.StyleController = this.layoutControl1;
            this.checkAccountAsCost.TabIndex = 42;
            // 
            // costCenterPlaceHolder
            // 
            this.costCenterPlaceHolder.account = null;
            this.costCenterPlaceHolder.AllowAdd = false;
            this.costCenterPlaceHolder.Location = new System.Drawing.Point(128, 191);
            this.costCenterPlaceHolder.Name = "costCenterPlaceHolder";
            this.costCenterPlaceHolder.OnlyLeafAccount = false;
            this.costCenterPlaceHolder.Size = new System.Drawing.Size(261, 20);
            this.costCenterPlaceHolder.TabIndex = 41;
            // 
            // cmbTaxCenter
            // 
            this.cmbTaxCenter.Location = new System.Drawing.Point(84, 245);
            this.cmbTaxCenter.Name = "cmbTaxCenter";
            this.cmbTaxCenter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTaxCenter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbTaxCenter.Size = new System.Drawing.Size(667, 20);
            this.cmbTaxCenter.StyleController = this.layoutControl1;
            this.cmbTaxCenter.TabIndex = 38;
            // 
            // radSalaryType
            // 
            this.radSalaryType.EditValue = true;
            this.radSalaryType.Location = new System.Drawing.Point(87, 354);
            this.radSalaryType.Name = "radSalaryType";
            this.radSalaryType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Monthly"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Daily")});
            this.radSalaryType.Size = new System.Drawing.Size(166, 25);
            this.radSalaryType.StyleController = this.layoutControl1;
            this.radSalaryType.TabIndex = 37;
            this.radSalaryType.SelectedIndexChanged += new System.EventHandler(this.radSalaryType_SelectedIndexChanged);
            // 
            // dateEnrolled
            // 
            this.dateEnrolled.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateEnrolled.Location = new System.Drawing.Point(23, 61);
            this.dateEnrolled.Name = "dateEnrolled";
            this.dateEnrolled.ShowEthiopian = true;
            this.dateEnrolled.ShowGregorian = true;
            this.dateEnrolled.ShowTime = false;
            this.dateEnrolled.Size = new System.Drawing.Size(411, 21);
            this.dateEnrolled.TabIndex = 33;
            this.dateEnrolled.VerticalLayout = false;
            // 
            // dateBirth
            // 
            this.dateBirth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateBirth.Location = new System.Drawing.Point(23, 269);
            this.dateBirth.Name = "dateBirth";
            this.dateBirth.ShowEthiopian = true;
            this.dateBirth.ShowGregorian = true;
            this.dateBirth.ShowTime = false;
            this.dateBirth.Size = new System.Drawing.Size(411, 21);
            this.dateBirth.TabIndex = 32;
            this.dateBirth.VerticalLayout = false;
            // 
            // cmbCostSharingStatus
            // 
            this.cmbCostSharingStatus.EditValue = "Not applicable";
            this.cmbCostSharingStatus.Location = new System.Drawing.Point(126, 139);
            this.cmbCostSharingStatus.Name = "cmbCostSharingStatus";
            this.cmbCostSharingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCostSharingStatus.Properties.Items.AddRange(new object[] {
            "Cost-Sharing debt evidence provided",
            "Cost-Sharing debt evidence not provided",
            "Not applicable"});
            this.cmbCostSharingStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbCostSharingStatus.Size = new System.Drawing.Size(625, 20);
            this.cmbCostSharingStatus.StyleController = this.layoutControl1;
            this.cmbCostSharingStatus.TabIndex = 24;
            this.cmbCostSharingStatus.SelectedIndexChanged += new System.EventHandler(this.cmbCostSharingStatus_SelectedIndexChanged);
            // 
            // txtEmployeeID
            // 
            this.txtEmployeeID.Location = new System.Drawing.Point(327, 197);
            this.txtEmployeeID.Name = "txtEmployeeID";
            this.txtEmployeeID.Properties.Mask.EditMask = "\\w.+";
            this.txtEmployeeID.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtEmployeeID.Size = new System.Drawing.Size(163, 20);
            this.txtEmployeeID.StyleController = this.layoutControl1;
            this.txtEmployeeID.TabIndex = 23;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "Employee ID cannot be blank";
            this.dxValidationProvider1.SetValidationRule(this.txtEmployeeID, conditionValidationRule1);
            // 
            // txtCostsharingPayable
            // 
            this.txtCostsharingPayable.Location = new System.Drawing.Point(172, 165);
            this.txtCostsharingPayable.Name = "txtCostsharingPayable";
            this.txtCostsharingPayable.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtCostsharingPayable.Properties.Mask.EditMask = "#,########0.00;";
            this.txtCostsharingPayable.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCostsharingPayable.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtCostsharingPayable.Properties.ReadOnly = true;
            this.txtCostsharingPayable.Size = new System.Drawing.Size(217, 20);
            this.txtCostsharingPayable.StyleController = this.layoutControl1;
            this.txtCostsharingPayable.TabIndex = 22;
            this.txtCostsharingPayable.EditValueChanged += new System.EventHandler(this.txtCostsharingPayable_EditValueChanged);
            // 
            // cmbSex
            // 
            this.cmbSex.EditValue = "Male";
            this.cmbSex.Location = new System.Drawing.Point(48, 298);
            this.cmbSex.Name = "cmbSex";
            this.cmbSex.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSex.Properties.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.cmbSex.Size = new System.Drawing.Size(205, 20);
            this.cmbSex.StyleController = this.layoutControl1;
            this.cmbSex.TabIndex = 21;
            // 
            // txtUnit
            // 
            this.txtUnit.Location = new System.Drawing.Point(49, 45);
            this.txtUnit.Name = "txtUnit";
            this.txtUnit.Properties.ReadOnly = true;
            this.txtUnit.Size = new System.Drawing.Size(441, 20);
            this.txtUnit.StyleController = this.layoutControl1;
            this.txtUnit.TabIndex = 19;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnClose);
            this.panelControl1.Controls.Add(this.btnAdd);
            this.panelControl1.Location = new System.Drawing.Point(5, 425);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(764, 55);
            this.panelControl1.TabIndex = 18;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(674, 17);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(81, 26);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Location = new System.Drawing.Point(570, 17);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(85, 26);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "&Save";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtCostSharing
            // 
            this.txtCostSharing.Location = new System.Drawing.Point(513, 165);
            this.txtCostSharing.Name = "txtCostSharing";
            this.txtCostSharing.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtCostSharing.Properties.Mask.EditMask = "#,########0.00;";
            this.txtCostSharing.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCostSharing.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtCostSharing.Properties.ReadOnly = true;
            this.txtCostSharing.Size = new System.Drawing.Size(238, 20);
            this.txtCostSharing.StyleController = this.layoutControl1;
            this.txtCostSharing.TabIndex = 16;
            this.txtCostSharing.EditValueChanged += new System.EventHandler(this.txtCostSharing_EditValueChanged);
            // 
            // txtTransportAllowance
            // 
            this.txtTransportAllowance.Location = new System.Drawing.Point(128, 113);
            this.txtTransportAllowance.Name = "txtTransportAllowance";
            this.txtTransportAllowance.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtTransportAllowance.Properties.Mask.EditMask = "#,########0.00;";
            this.txtTransportAllowance.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTransportAllowance.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTransportAllowance.Properties.ReadOnly = true;
            this.txtTransportAllowance.Size = new System.Drawing.Size(623, 20);
            this.txtTransportAllowance.StyleController = this.layoutControl1;
            this.txtTransportAllowance.TabIndex = 13;
            this.txtTransportAllowance.EditValueChanged += new System.EventHandler(this.txtTransportAllowance_EditValueChanged);
            // 
            // txtGrossSalary
            // 
            this.txtGrossSalary.Location = new System.Drawing.Point(323, 354);
            this.txtGrossSalary.Name = "txtGrossSalary";
            this.txtGrossSalary.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtGrossSalary.Properties.Mask.EditMask = "#,########0.00;";
            this.txtGrossSalary.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGrossSalary.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtGrossSalary.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtGrossSalary.Size = new System.Drawing.Size(167, 20);
            this.txtGrossSalary.StyleController = this.layoutControl1;
            this.txtGrossSalary.TabIndex = 12;
            this.txtGrossSalary.EditValueChanged += new System.EventHandler(this.txtGrossSalary_EditValueChanged);
            // 
            // cmbEmpType
            // 
            this.cmbEmpType.EditValue = "Permanent";
            this.cmbEmpType.Location = new System.Drawing.Point(122, 87);
            this.cmbEmpType.Name = "cmbEmpType";
            this.cmbEmpType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbEmpType.Properties.Items.AddRange(new object[] {
            "Permanent",
            "Temporary"});
            this.cmbEmpType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbEmpType.Size = new System.Drawing.Size(386, 20);
            this.cmbEmpType.StyleController = this.layoutControl1;
            this.cmbEmpType.TabIndex = 11;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(69, 324);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Properties.Mask.EditMask = "\\w.+";
            this.txtAddress.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtAddress.Size = new System.Drawing.Size(184, 20);
            this.txtAddress.StyleController = this.layoutControl1;
            this.txtAddress.TabIndex = 9;
            // 
            // txtTelephone
            // 
            this.txtTelephone.Location = new System.Drawing.Point(318, 298);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Properties.Mask.EditMask = "(\\+251)\\d\\d\\d-\\d\\d-\\d\\d-\\d\\d";
            this.txtTelephone.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.txtTelephone.Size = new System.Drawing.Size(172, 20);
            this.txtTelephone.StyleController = this.layoutControl1;
            this.txtTelephone.TabIndex = 8;
            // 
            // txtTIN
            // 
            this.txtTIN.Location = new System.Drawing.Point(283, 223);
            this.txtTIN.Name = "txtTIN";
            this.txtTIN.Properties.Mask.EditMask = "\\d+";
            this.txtTIN.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTIN.Size = new System.Drawing.Size(207, 20);
            this.txtTIN.StyleController = this.layoutControl1;
            this.txtTIN.TabIndex = 5;
            // 
            // txtEmployeeName
            // 
            this.txtEmployeeName.Location = new System.Drawing.Point(57, 171);
            this.txtEmployeeName.Name = "txtEmployeeName";
            this.txtEmployeeName.Properties.Mask.EditMask = "[a-zA-Z].+";
            this.txtEmployeeName.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtEmployeeName.Size = new System.Drawing.Size(433, 20);
            this.txtEmployeeName.StyleController = this.layoutControl1;
            this.txtEmployeeName.TabIndex = 4;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "Employee name cannot be blank";
            this.dxValidationProvider1.SetValidationRule(this.txtEmployeeName, conditionValidationRule2);
            // 
            // chkIsShareHolder
            // 
            this.chkIsShareHolder.Location = new System.Drawing.Point(514, 87);
            this.chkIsShareHolder.Name = "chkIsShareHolder";
            this.chkIsShareHolder.Properties.Caption = "Shareholder";
            this.chkIsShareHolder.Size = new System.Drawing.Size(237, 19);
            this.chkIsShareHolder.StyleController = this.layoutControl1;
            this.chkIsShareHolder.TabIndex = 26;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutConfirm,
            this.layoutEmployeeProfile});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(774, 485);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutConfirm
            // 
            this.layoutConfirm.Control = this.panelControl1;
            this.layoutConfirm.CustomizationFormText = "layoutControlItem13";
            this.layoutConfirm.Location = new System.Drawing.Point(0, 420);
            this.layoutConfirm.Name = "layoutConfirm";
            this.layoutConfirm.Size = new System.Drawing.Size(768, 59);
            this.layoutConfirm.Text = "layoutConfirm";
            this.layoutConfirm.TextSize = new System.Drawing.Size(0, 0);
            this.layoutConfirm.TextToControlDistance = 0;
            this.layoutConfirm.TextVisible = false;
            // 
            // layoutEmployeeProfile
            // 
            this.layoutEmployeeProfile.CustomizationFormText = "Employee Profile";
            this.layoutEmployeeProfile.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup2});
            this.layoutEmployeeProfile.Location = new System.Drawing.Point(0, 0);
            this.layoutEmployeeProfile.Name = "layoutEmployeeProfile";
            this.layoutEmployeeProfile.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AutoSize;
            this.layoutEmployeeProfile.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutEmployeeProfile.Size = new System.Drawing.Size(768, 420);
            this.layoutEmployeeProfile.Text = "Employee Profile";
            this.layoutEmployeeProfile.TextVisible = false;
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.CustomizationFormText = "tabbedControlGroup2";
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.SelectedTabPage = this.basicInfoTab;
            this.tabbedControlGroup2.SelectedTabPageIndex = 0;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(758, 410);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.basicInfoTab,
            this.empInfoTab});
            this.tabbedControlGroup2.Text = "tabbedControlGroup2";
            // 
            // basicInfoTab
            // 
            this.basicInfoTab.CustomizationFormText = "Basic Information";
            this.basicInfoTab.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.lblAddress,
            this.layoutControlItem4,
            this.layoutControlItem15,
            this.layoutControlItem13,
            this.layoutControlItem32,
            this.layoutControlItem3,
            this.layoutControlItem9,
            this.emptySpaceItem1,
            this.layoutControlItem14,
            this.layoutControlItem1,
            this.layoutControlItem26,
            this.layoutControlItem2,
            this.layoutControlItem16,
            this.layoutControlItem7,
            this.layoutControlItem23});
            this.basicInfoTab.Location = new System.Drawing.Point(0, 0);
            this.basicInfoTab.Name = "basicInfoTab";
            this.basicInfoTab.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.basicInfoTab.Size = new System.Drawing.Size(734, 364);
            this.basicInfoTab.Text = "Basic Information";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtTelephone;
            this.layoutControlItem5.CustomizationFormText = "Telephone:";
            this.layoutControlItem5.Location = new System.Drawing.Point(236, 253);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(237, 26);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem5.Text = "Telephone:";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(54, 13);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // lblAddress
            // 
            this.lblAddress.Control = this.txtAddress;
            this.lblAddress.CustomizationFormText = "Address:";
            this.lblAddress.Location = new System.Drawing.Point(0, 279);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(236, 30);
            this.lblAddress.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.lblAddress.Text = "Address:";
            this.lblAddress.TextSize = new System.Drawing.Size(43, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.dateBirth;
            this.layoutControlItem4.CustomizationFormText = "Birth Date:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 208);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(473, 45);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(473, 45);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(473, 45);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem4.Text = "Birth Date:";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(52, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.cmbSex;
            this.layoutControlItem15.CustomizationFormText = "Sex:";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 253);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(236, 26);
            this.layoutControlItem15.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem15.Text = "Sex:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(22, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.comboLoginName;
            this.layoutControlItem13.CustomizationFormText = "Login Name:";
            this.layoutControlItem13.Location = new System.Drawing.Point(236, 279);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(237, 30);
            this.layoutControlItem13.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem13.Text = "Login Name:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(59, 13);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.txtBankAccountNo;
            this.layoutControlItem32.CustomizationFormText = "Bank Account No.:";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 178);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(236, 30);
            this.layoutControlItem32.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem32.Text = "Bank Account No.:";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(89, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.panel1;
            this.layoutControlItem3.CustomizationFormText = "Photo:";
            this.layoutControlItem3.Location = new System.Drawing.Point(473, 0);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(104, 40);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(261, 364);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Photo:";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(32, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.dateTimeVersion;
            this.layoutControlItem9.CustomizationFormText = "Version ";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(104, 100);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(473, 100);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem9.Text = "Version ";
            this.layoutControlItem9.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(38, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 340);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(473, 24);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txtUnit;
            this.layoutControlItem14.CustomizationFormText = "Unit:";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(473, 26);
            this.layoutControlItem14.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem14.Text = "Unit:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(23, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtEmployeeName;
            this.layoutControlItem1.CustomizationFormText = "Employee Name:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 126);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(473, 26);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem1.Text = "Name:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(31, 13);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.textTitle;
            this.layoutControlItem26.CustomizationFormText = "layoutControlItem26";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 152);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(266, 26);
            this.layoutControlItem26.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem26.Text = "Title:";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(24, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtTIN;
            this.layoutControlItem2.CustomizationFormText = "Tax Identification Number (TIN):";
            this.layoutControlItem2.Location = new System.Drawing.Point(236, 178);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(237, 30);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem2.Text = "TIN:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(21, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.txtEmployeeID;
            this.layoutControlItem16.CustomizationFormText = "Employee Identification No.:";
            this.layoutControlItem16.Location = new System.Drawing.Point(266, 152);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(207, 26);
            this.layoutControlItem16.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem16.Text = "ID No.:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(35, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtGrossSalary;
            this.layoutControlItem7.CustomizationFormText = "Salary:";
            this.layoutControlItem7.Location = new System.Drawing.Point(236, 309);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(237, 31);
            this.layoutControlItem7.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem7.Text = "Basic Salary:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.radSalaryType;
            this.layoutControlItem23.CustomizationFormText = "Salary Type";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 309);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(236, 31);
            this.layoutControlItem23.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem23.Text = "Salary Type:";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(61, 13);
            // 
            // empInfoTab
            // 
            this.empInfoTab.CustomizationFormText = "Employment Information";
            this.empInfoTab.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem19,
            this.layoutControlItem6,
            this.layoutControlItem33,
            this.layoutControlItem22,
            this.layoutCostSharingPayable,
            this.layoutCostSharingStatus,
            this.layoutCostSharingDebt,
            this.layoutControlItem25,
            this.layoutControlItem11,
            this.layoutControlItem24});
            this.empInfoTab.Location = new System.Drawing.Point(0, 0);
            this.empInfoTab.Name = "empInfoTab";
            this.empInfoTab.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.empInfoTab.Size = new System.Drawing.Size(734, 364);
            this.empInfoTab.Text = "Employment Information";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtTransportAllowance;
            this.layoutControlItem8.CustomizationFormText = "Transport Allowance:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 68);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(734, 26);
            this.layoutControlItem8.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem8.Text = "Transport Allowance:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.dateEnrolled;
            this.layoutControlItem19.CustomizationFormText = "Enrolled Date:";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(443, 42);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(443, 42);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(734, 42);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem19.Text = "Enrollement Date:";
            this.layoutControlItem19.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.cmbEmpType;
            this.layoutControlItem6.CustomizationFormText = "Employment Status";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 42);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(151, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(491, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem6.Text = "Employment Status:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.accountExpense;
            this.layoutControlItem33.CustomizationFormText = "Expense/Cost Account:";
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 172);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(734, 28);
            this.layoutControlItem33.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem33.Text = "Expense/Cost Account:";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(113, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.costCenterPlaceHolder;
            this.layoutControlItem22.CustomizationFormText = "Current Cost Center:";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 146);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(372, 26);
            this.layoutControlItem22.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem22.Text = "Current Cost Center:";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutCostSharingPayable
            // 
            this.layoutCostSharingPayable.Control = this.txtCostsharingPayable;
            this.layoutCostSharingPayable.CustomizationFormText = "Cost-Sharing payable amount:";
            this.layoutCostSharingPayable.Location = new System.Drawing.Point(0, 120);
            this.layoutCostSharingPayable.Name = "layoutCostSharingPayable";
            this.layoutCostSharingPayable.Size = new System.Drawing.Size(372, 26);
            this.layoutCostSharingPayable.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutCostSharingPayable.Text = "Cost-Sharing payable amount:";
            this.layoutCostSharingPayable.TextSize = new System.Drawing.Size(146, 13);
            // 
            // layoutCostSharingStatus
            // 
            this.layoutCostSharingStatus.Control = this.cmbCostSharingStatus;
            this.layoutCostSharingStatus.CustomizationFormText = "Cost-Sharing status:";
            this.layoutCostSharingStatus.Location = new System.Drawing.Point(0, 94);
            this.layoutCostSharingStatus.Name = "layoutCostSharingStatus";
            this.layoutCostSharingStatus.Size = new System.Drawing.Size(734, 26);
            this.layoutCostSharingStatus.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutCostSharingStatus.Text = "Cost-Sharing Status:";
            this.layoutCostSharingStatus.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutCostSharingDebt
            // 
            this.layoutCostSharingDebt.Control = this.txtCostSharing;
            this.layoutCostSharingDebt.CustomizationFormText = "Toal cost sharing debt:";
            this.layoutCostSharingDebt.Location = new System.Drawing.Point(372, 120);
            this.layoutCostSharingDebt.Name = "layoutCostSharingDebt";
            this.layoutCostSharingDebt.Size = new System.Drawing.Size(362, 26);
            this.layoutCostSharingDebt.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutCostSharingDebt.Text = "Toal Cost-Sharing Debt:";
            this.layoutCostSharingDebt.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.checkAccountAsCost;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(372, 146);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(362, 26);
            this.layoutControlItem25.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem25.Text = "layoutControlItem25";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextToControlDistance = 0;
            this.layoutControlItem25.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.chkIsShareHolder;
            this.layoutControlItem11.CustomizationFormText = " ";
            this.layoutControlItem11.Location = new System.Drawing.Point(491, 42);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(243, 25);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(243, 25);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(243, 26);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem11.Text = " ";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.cmbTaxCenter;
            this.layoutControlItem24.CustomizationFormText = "Tax Center";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 200);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(734, 164);
            this.layoutControlItem24.Spacing = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem24.Text = "Tax Center:";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(58, 13);
            // 
            // dxValidationProvider1
            // 
            this.dxValidationProvider1.ValidateHiddenControls = false;
            this.dxValidationProvider1.ValidationMode = DevExpress.XtraEditors.DXErrorProvider.ValidationMode.Manual;
            // 
            // validationTransportAllowance
            // 
            this.validationTransportAllowance.ValidateHiddenControls = false;
            // 
            // EmployeeEditor_bio
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(774, 485);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "EmployeeEditor_bio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Employee Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EmployeeEditor_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picEmployeePic.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccountNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textTitle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboLoginName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkAccountAsCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTaxCenter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSalaryType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCostSharingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCostsharingPayable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSex.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCostSharing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransportAllowance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrossSalary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEmpType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelephone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmployeeName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsShareHolder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutConfirm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutEmployeeProfile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.basicInfoTab)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.empInfoTab)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCostSharingPayable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCostSharingStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCostSharingDebt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationTransportAllowance)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit txtEmployeeName;
        private DevExpress.XtraEditors.TextEdit txtTIN;
        private DevExpress.XtraEditors.TextEdit txtAddress;
        private DevExpress.XtraEditors.TextEdit txtTelephone;
        private DevExpress.XtraEditors.ComboBoxEdit cmbEmpType;
        private DevExpress.XtraEditors.TextEdit txtGrossSalary;
        private DevExpress.XtraEditors.TextEdit txtCostSharing;
        private DevExpress.XtraEditors.TextEdit txtTransportAllowance;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutConfirm;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.TextEdit txtUnit;
        private DevExpress.XtraEditors.ComboBoxEdit cmbSex;
        private DevExpress.XtraEditors.TextEdit txtCostsharingPayable;
        private DevExpress.XtraEditors.TextEdit txtEmployeeID;
        private DevExpress.XtraEditors.ComboBoxEdit cmbCostSharingStatus;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidationProvider1;
        private DevExpress.XtraEditors.CheckEdit chkIsShareHolder;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validationTransportAllowance;
        private BIZNET.iERP.Client.BNDualCalendar dateBirth;
        private BIZNET.iERP.Client.BNDualCalendar dateEnrolled;
        private DevExpress.XtraEditors.RadioGroup radSalaryType;
        private DevExpress.XtraEditors.ComboBoxEdit cmbTaxCenter;
        private INTAPS.Accounting.Client.CostCenterPlaceHolder costCenterPlaceHolder;
        private DevExpress.XtraEditors.CheckEdit checkAccountAsCost;
        private DevExpress.XtraEditors.ComboBoxEdit comboLoginName;
        private DevExpress.XtraEditors.TextEdit textTitle;
        private BNDualCalendar dateTimeVersion;
        private DevExpress.XtraEditors.TextEdit txtBankAccountNo;
        private INTAPS.Accounting.Client.AccountPlaceHolder accountExpense;
        private DevExpress.XtraLayout.LayoutControlGroup layoutEmployeeProfile;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup basicInfoTab;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem lblAddress;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlGroup empInfoTab;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutCostSharingDebt;
        private DevExpress.XtraLayout.LayoutControlItem layoutCostSharingStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutCostSharingPayable;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.PictureEdit picEmployeePic;
        private DevExpress.XtraEditors.SimpleButton btnBrowse;
        private DevExpress.XtraEditors.SimpleButton btnClear;
    }
}