﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;
using System.Collections;
using DevExpress.XtraEditors.DXErrorProvider;
using INTAPS.Payroll;
using INTAPS.Payroll.Client;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using System.IO;
using System.Drawing.Imaging;
using DevExpress.Utils;
using DevExpress.XtraGrid;

namespace BIZNET.iERP.Client
{
    public partial class EmployeeEditor_wd : DevExpress.XtraEditors.XtraForm
    {
        int _employeeID = -1;
        const string REGULAR_TIME = "Regular Time";
        const string OVER_TIME = "Over Time";
        const string OTHERS = "Special Benefit/Deduction";
        const string PERMANENT_BENEFIT= "Permanent Benefit/Deduction";

        public bool updated = false;
        int _workDataPeriodID = -1;
        DataTable tableEmployees=new DataTable("data");
        public EmployeeEditor_wd(int employeeID, int periodID)
        {
            InitializeComponent();
            PrepareWorkDataTable();
            _workDataPeriodID = periodID;
            _employeeID = employeeID;
            try
            {
                _ignoreDataChangeEvent = true;
                DisplayEmployeeWorkData(_employeeID);
            }
            finally
            {
                _ignoreDataChangeEvent = false;
            }
        }
        private void DisableAllControls(bool readOnly)
        {
            gridViewWorkData.OptionsBehavior.Editable = !readOnly;
        }
        bool _ignoreDataChangeEvent = false;
        bool _workingDataChanged = false;
        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
                Data_RegularTime regularData;
                OverTimeData overtimeData;
                List<SingleValueData> singleData;
                List<int> singleFormulaID;
                if (!_workingDataChanged)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Nothing Changed.");
                    return;
                }
                try
                {
                    getWorkingData(out regularData, out overtimeData, out singleData, out singleFormulaID);
                    iERPTransactionClient.SetWorkingData(_employeeID, _workDataPeriodID, regularData, overtimeData, singleData.ToArray(), singleFormulaID.ToArray());
                    _workingDataChanged = false;
                    updated = true;
                    MessageBox.ShowSuccessMessage("Work data successfully registered!");
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.ShowException(ex);
                }
        }
        void tableEmployees_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            if (_ignoreDataChangeEvent)
                return;
            _workingDataChanged = true;

        }
        private void PrepareWorkDataTable()
        {
            tableEmployees = new DataTable();
            tableEmployees.RowChanged += new DataRowChangeEventHandler(tableEmployees_RowChanged);
            tableEmployees.Columns.Add("Component", typeof(string));
            tableEmployees.Columns.Add("Data", typeof(string));
            tableEmployees.Columns.Add("Amount", typeof(double));
            tableEmployees.Columns.Add("FormulaID", typeof(int));
            tableEmployees.Columns.Add("Remark", typeof(string));

            gridControlWorkData.DataSource = tableEmployees;
            gridViewWorkData.Columns["Component"].OptionsColumn.AllowEdit = false;
            gridViewWorkData.Columns["Component"].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
            gridViewWorkData.Columns["Data"].OptionsColumn.AllowEdit = false;
            gridViewWorkData.Columns["FormulaID"].Visible = false;
            gridViewWorkData.Columns["Amount"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gridViewWorkData.Columns["Amount"].DisplayFormat.FormatString = "#####0.00;";
            gridViewWorkData.Columns["Component"].SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
            gridViewWorkData.Columns["Data"].SortOrder = DevExpress.Data.ColumnSortOrder.None;
            gridViewWorkData.Columns["Remark"].OptionsColumn.AllowEdit = false;

            gridViewWorkData.GroupFormat = String.Format("{0}{1}", "", "{1}");


        }
        private void DisplayEmployeeWorkData(int empID)
        {
            _ignoreDataChangeEvent = true;
            try
            {
                tableEmployees.Clear();

                #region Regular work data
                int regularFormulaID;
                Data_RegularTime regularTimeData = GetRegularTimeData(empID, out regularFormulaID);
                if (regularTimeData != null)
                {
                    #region Working Days
                    DataRow wd_row = tableEmployees.NewRow();
                    wd_row[0] = REGULAR_TIME;
                    wd_row[1] = "Working Days";
                    wd_row[2] = (double)regularTimeData.WorkingDays;
                    wd_row[3] = regularFormulaID;
                    tableEmployees.Rows.Add(wd_row);
                    #endregion
                    #region Working Hours
                    DataRow wh_row = tableEmployees.NewRow();
                    wh_row[0] = REGULAR_TIME;
                    wh_row[1] = "Working Hours";
                    wh_row[2] = (double)regularTimeData.WorkingHours;
                    wh_row[3] = regularFormulaID;
                    tableEmployees.Rows.Add(wh_row);
                    #endregion
                    #region Partial Working Hours
                    DataRow pwh_row = tableEmployees.NewRow();
                    pwh_row[0] = REGULAR_TIME;
                    pwh_row[1] = "Partial Working Hours";
                    pwh_row[2] = (double)regularTimeData.PartialWorkingHour;
                    pwh_row[3] = regularFormulaID;
                    tableEmployees.Rows.Add(pwh_row);
                    #endregion
                }
                #endregion

                #region Over Time data
                int overTimeFormulaID;
                OverTimeData overTimeData = GetOverTimeData(empID, out overTimeFormulaID);
                if (overTimeData != null)
                {
                    #region Off Hours
                    DataRow offhrs_row = tableEmployees.NewRow();
                    offhrs_row[0] = OVER_TIME;
                    offhrs_row[1] = "Off Hours";
                    offhrs_row[2] = overTimeData.offHours;
                    offhrs_row[3] = overTimeFormulaID;
                    tableEmployees.Rows.Add(offhrs_row);
                    #endregion
                    #region Late Hours
                    DataRow latehrs_row = tableEmployees.NewRow();
                    latehrs_row[0] = OVER_TIME;
                    latehrs_row[1] = "Late Hours";
                    latehrs_row[2] = overTimeData.lateHours;
                    latehrs_row[3] = overTimeFormulaID;
                    tableEmployees.Rows.Add(latehrs_row);
                    #endregion
                    #region Weekend Hours
                    DataRow weekendhrs_row = tableEmployees.NewRow();
                    weekendhrs_row[0] = OVER_TIME;
                    weekendhrs_row[1] = "Weekend Hours";
                    weekendhrs_row[2] = overTimeData.weekendHours;
                    weekendhrs_row[3] = overTimeFormulaID;
                    tableEmployees.Rows.Add(weekendhrs_row);
                    #endregion
                    #region Holiday Hours
                    DataRow holidayhrs_row = tableEmployees.NewRow();
                    holidayhrs_row[0] = OVER_TIME;
                    holidayhrs_row[1] = "Holiday Hours";
                    holidayhrs_row[2] = overTimeData.holidayHours;
                    holidayhrs_row[3] = overTimeFormulaID;
                    tableEmployees.Rows.Add(holidayhrs_row);
                    #endregion
                }
                #endregion

                #region General data
                string[] formulas;
                int[] formulaID;
                double[] values = GetGeneralData(empID, out formulas, out formulaID);
                if (values != null)
                {
                    int j = 0;
                    foreach (string name in formulas)
                    {
                        DataRow row = tableEmployees.NewRow();
                        row[0] = OTHERS;
                        row[1] = name;
                        row[2] = values[j];
                        row[3] = formulaID[j];
                        tableEmployees.Rows.Add(row);
                        j++;
                    }
                }
                string[] remark;
                values = GetPermanentBenefitData(empID, out formulas, out formulaID,out remark);
                if (values != null)
                {
                    int j = 0;
                    foreach (string name in formulas)
                    {
                        DataRow row = tableEmployees.NewRow();
                        row[0] = PERMANENT_BENEFIT;
                        row[1] = name;
                        row[2] = values[j];
                        row[3] = formulaID[j];
                        row[4] = remark[j];
                        tableEmployees.Rows.Add(row);
                        j++;
                    }
                }
                #endregion

                gridControlWorkData.DataSource = tableEmployees;
                gridViewWorkData.RefreshData();
                if (tableEmployees.Rows.Count > 0)
                {
                    
                    gridViewWorkData.Columns["Component"].Group();
                    gridViewWorkData.ExpandAllGroups();
                }
                _workingDataChanged = false;
            }
            finally
            {
                _ignoreDataChangeEvent = false;
            }
        }
        public Data_RegularTime GetRegularTimeData(int empID, out int formulaID)
        {
            Data_RegularTime data = null;
            formulaID = -1;
            int pcdID = (int)iERPTransactionClient.GetSystemParamter("RegularTimePayrollFormulaID");
            PayrollComponentFormula regularFormula = PayrollClient.GetPCFormula(pcdID);
            if (regularFormula == null)
                return null;
            PayrollComponentData[] ret = PayrollClient.GetPCData(pcdID, regularFormula.ID, AppliesToObjectType.Employee, empID, _workDataPeriodID, false);
            formulaID = regularFormula.ID;
            if (empID != -1)
            {
                if (ret.Length > 0)
                {
                    object regularTimeData = PayrollClient.GetPCAdditionalData(ret[0].ID);
                    if (regularTimeData != null)
                    {
                        data = (Data_RegularTime)regularTimeData;
                    }
                }
                else
                    data = new Data_RegularTime();
            }
            else
            {
                data = new Data_RegularTime();
            }

            return data;
        }
        private OverTimeData GetOverTimeData(int empID, out int formulaID)
        {
            OverTimeData data = null;
            formulaID = -1;
            int pcdID = (int)iERPTransactionClient.GetSystemParamter("OverTimePayrollFormulaID");

            PayrollComponentFormula overTimeFormula = PayrollClient.GetPCFormula(pcdID);
            if (overTimeFormula==null)
                return null;

            PayrollComponentData[] ret = PayrollClient.GetPCData(pcdID, overTimeFormula.ID, AppliesToObjectType.Employee, empID, _workDataPeriodID, false);
            formulaID = overTimeFormula.ID;
            if (empID != -1)
            {
                if (ret.Length > 0)
                {
                    object overTimeData = PayrollClient.GetPCAdditionalData(ret[0].ID);
                    if (overTimeData != null)
                    {
                        data = (OverTimeData)overTimeData;
                    }

                }
                else
                    data = new OverTimeData();
            }
            else
            {
                data = new OverTimeData();
            }
            return data;
        }
        private double[] GetGeneralData(int empID, out string[] formulas,out int[]formulaID)
        {
            List<double> amounts = new List<double>();
            List<string> names = new List<string>();
            List<int> ids = new List<int>();
            int pcdID = (int)iERPTransactionClient.GetSystemParamter("SingleValuePayrollComponentID");
            if (pcdID != 0)
            {
                PayrollComponentFormula[] singleValueFormula = PayrollClient.GetPCFormulae(pcdID);
                if (singleValueFormula.Length == 0)
                {
                    formulas = null;
                    formulaID = null;
                    return null;
                }

                int i = 0;
                foreach (PayrollComponentFormula formula in singleValueFormula)
                {
                    if (empID != -1)
                    {
                        PayrollComponentData[] ret = PayrollClient.GetPCData(pcdID, singleValueFormula[i].ID, AppliesToObjectType.Employee, empID, _workDataPeriodID, false);
                        if (ret.Length > 0)
                        {
                            object singleValueData = PayrollClient.GetPCAdditionalData(ret[0].ID);
                            if (singleValueData != null)
                            {
                                SingleValueData data = (SingleValueData)singleValueData;
                                amounts.Add(data.value);
                                names.Add(formula.Name);
                                ids.Add(formula.ID);
                            }
                        }
                        else
                        {
                            amounts.Add(0d);
                            names.Add(formula.Name);
                            ids.Add(formula.ID);
                        }
                    }
                    else
                    {
                        amounts.Add(0d);
                        names.Add(formula.Name);
                        ids.Add(formula.ID);
                    }
                    i++;
                }

            }
            formulas = names.ToArray();
            formulaID = ids.ToArray();
            return amounts.ToArray();
        }
        private double[] GetPermanentBenefitData(int empID, out string[] formulas, out int[] formulaID,out string[] remark)
        {
            List<double> amounts = new List<double>();
            List<string> names = new List<string>();
            List<int> ids = new List<int>();
            List<string> _remark = new List<string>();
            int pcdID = (int)iERPTransactionClient.GetSystemParamter("permanentBenefitComponentID");
            if (pcdID != 0)
            {
                PayrollComponentFormula[] permanentBenefitFormula = PayrollClient.GetPCFormulae(pcdID);
                if (permanentBenefitFormula.Length == 0)
                {
                    formulas = null;
                    formulaID = null;
                    remark = null;
                    return null;
                }

                int i = 0;
                foreach (PayrollComponentFormula formula in permanentBenefitFormula)
                {
                    if (empID != -1)
                    {
                        PayrollComponentData[] ret = PayrollClient.GetPCData(pcdID, permanentBenefitFormula[i].ID, AppliesToObjectType.Employee, empID, _workDataPeriodID, false);
                        if (ret.Length > 0)
                        {
                            object singleValueData = PayrollClient.GetPCAdditionalData(ret[0].ID);
                            if (singleValueData != null)
                            {
                                SingleValueData data = (SingleValueData)singleValueData;
                                amounts.Add(data.value);
                                names.Add(formula.Name);
                                ids.Add(formula.ID);
                                PayPeriodRange range = ret[0].periodRange;
                                if (range.FiniteRange)
                                    _remark.Add(PayrollClient.GetPayPeriod(ret[0].periodRange.PeriodFrom).name + " to " + PayrollClient.GetPayPeriod(ret[0].periodRange.PeriodTo).name);
                                else
                                    _remark.Add("Since " + PayrollClient.GetPayPeriod(ret[0].periodRange.PeriodFrom).name);
                            }
                        }
                        else
                        {
                            amounts.Add(0d);
                            names.Add(formula.Name);
                            ids.Add(formula.ID);
                            _remark.Add("");
                        }
                    }
                    else
                    {
                        amounts.Add(0d);
                        names.Add(formula.Name);
                        ids.Add(formula.ID);
                        _remark.Add("");
                    }
                    i++;
                }

            }
            formulas = names.ToArray();
            formulaID = ids.ToArray();
            remark = _remark.ToArray();
            return amounts.ToArray();
        }
        private double[] DeletePermanentBenefitData(int empID)
        {
            //List<double> amounts = new List<double>();
            //List<string> names = new List<string>();
            //List<int> ids = new List<int>();
            //List<string> _remark = new List<string>();
            //int pcdID = (int)iERPTransactionClient.GetSystemParamter("permanentBenefitComponentID");
            //if (pcdID != 0)
            //{
            //    PayrollComponentFormula[] permanentBenefitFormula = PayrollClient.GetPCFormulae(pcdID);

            //    int i = 0;
            //    foreach (PayrollComponentFormula formula in permanentBenefitFormula)
            //    {
            //        if (empID != -1)
            //        {
            //            PayrollComponentData[] ret = PayrollClient.GetPCData(pcdID, permanentBenefitFormula[i].ID, AppliesToObjectType.Employee, empID, comboPeriod.selectedPeriodID, false);
            //            if (ret.Length > 0)
            //            {
            //                object singleValueData = PayrollClient.GetPCAdditionalData(ret[0].ID);
            //                if (singleValueData != null)
            //                {
            //                    SingleValueData data = (SingleValueData)singleValueData;
            //                    amounts.Add(data.value);
            //                    names.Add(formula.Name);
            //                    ids.Add(formula.ID);
            //                    PayPeriodRange range = ret[0].periodRange;
            //                    if (range.FiniteRange)
            //                        _remark.Add(PayrollClient.GetPayPeriod(ret[0].periodRange.PeriodFrom).name + " to " + PayrollClient.GetPayPeriod(ret[0].periodRange.PeriodTo).name);
            //                    else
            //                        _remark.Add("Since " + PayrollClient.GetPayPeriod(ret[0].periodRange.PeriodFrom).name);
            //                }
            //            }
            //            else
            //            {
            //                amounts.Add(0d);
            //                names.Add(formula.Name);
            //                ids.Add(formula.ID);
            //                _remark.Add("");
            //            }
            //        }
            //        else
            //        {
            //            amounts.Add(0d);
            //            names.Add(formula.Name);
            //            ids.Add(formula.ID);
            //            _remark.Add("");
            //        }
            //        i++;
            //    }

            //}
            //formulas = names.ToArray();
            //formulaID = ids.ToArray();
            //remark = _remark.ToArray();
            //return amounts.ToArray();
            return new double[] { };
        }
        
        private void getWorkingData(out Data_RegularTime regularData, out OverTimeData overtimeData, out List<SingleValueData> singleData, out List<int> singleFormulaID)
        {
            regularData = new Data_RegularTime();
            overtimeData = new OverTimeData();
            singleData = new List<SingleValueData>();
            singleFormulaID = new List<int>();

            for (int i = 0; i < gridViewWorkData.DataRowCount; i++)
            {
                string component = (string)gridViewWorkData.GetRowCellValue(i, "Component");
                string data = (string)gridViewWorkData.GetRowCellValue(i, "Data");
                double amount = (double)gridViewWorkData.GetRowCellValue(i, "Amount");
                int formulaID = (int)gridViewWorkData.GetRowCellValue(i, "FormulaID");
                switch (component)
                {
                    case REGULAR_TIME:
                        if (data.Equals("Working Days"))
                            regularData.WorkingDays = amount;
                        else if (data.Equals("Working Hours"))
                            regularData.WorkingHours = amount;
                        else if (data.Equals("Partial Working Hours"))
                            regularData.PartialWorkingHour = amount;
                        break;

                    case OVER_TIME:
                        if (data.Equals("Off Hours"))
                            overtimeData.offHours = amount;
                        else if (data.Equals("Late Hours"))
                            overtimeData.lateHours = amount;
                        else if (data.Equals("Weekend Hours"))
                            overtimeData.weekendHours = amount;
                        else if (data.Equals("Holiday Hours"))
                            overtimeData.holidayHours = amount;
                        break;

                    case OTHERS:case PERMANENT_BENEFIT:
                        SingleValueData sdata = new SingleValueData();
                        sdata.value = amount;
                        singleData.Add(sdata);
                        singleFormulaID.Add(formulaID);
                        break;
                    default:
                        break;
                }
            }
        }
       
    }
}