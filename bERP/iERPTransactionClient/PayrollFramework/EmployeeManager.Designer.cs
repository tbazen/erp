﻿namespace BIZNET.iERP.Client
{
    partial class EmployeeManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmployeeManager));
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.datePicker = new BIZNET.iERP.Client.BNDualCalendar();
            this.checkCurrentDate = new DevExpress.XtraEditors.CheckEdit();
            this.checkShowDismissed = new DevExpress.XtraEditors.CheckEdit();
            this.buttoImport = new DevExpress.XtraEditors.SimpleButton();
            this.buttonSelectAll = new DevExpress.XtraEditors.SimpleButton();
            this.buttonChangeOrg = new DevExpress.XtraEditors.SimpleButton();
            this.buttonNew = new DevExpress.XtraEditors.SimpleButton();
            this.buttonEdit = new DevExpress.XtraEditors.SimpleButton();
            this.buttonDismiss = new DevExpress.XtraEditors.SimpleButton();
            this.buttonEnroll = new DevExpress.XtraEditors.SimpleButton();
            this.buttonDelete = new DevExpress.XtraEditors.SimpleButton();
            this.pageNavigator = new INTAPS.UI.PageNavigator();
            this.textQuery = new DevExpress.XtraEditors.TextEdit();
            this.lableSearchInfo = new DevExpress.XtraEditors.LabelControl();
            this.labelDataDate = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnSearch = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.ctxtOrgUnit = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miRootOrg = new System.Windows.Forms.ToolStripMenuItem();
            this.miChildOrg = new System.Windows.Forms.ToolStripMenuItem();
            this.miDelOrg = new System.Windows.Forms.ToolStripMenuItem();
            this.miEditOrg = new System.Windows.Forms.ToolStripMenuItem();
            this.miGroupUnder = new System.Windows.Forms.ToolStripMenuItem();
            this.miMakeRootOrganization = new System.Windows.Forms.ToolStripMenuItem();
            this.miRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.listEmployee = new System.Windows.Forms.ListView();
            this.colNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colGender = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTIN = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colGrossSalary = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.orgTree = new INTAPS.Payroll.Client.OrgTree();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkCurrentDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkShowDismissed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textQuery.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.ctxtOrgUnit.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.datePicker);
            this.panelControl2.Controls.Add(this.checkCurrentDate);
            this.panelControl2.Controls.Add(this.checkShowDismissed);
            this.panelControl2.Controls.Add(this.buttoImport);
            this.panelControl2.Controls.Add(this.buttonSelectAll);
            this.panelControl2.Controls.Add(this.buttonChangeOrg);
            this.panelControl2.Controls.Add(this.buttonNew);
            this.panelControl2.Controls.Add(this.buttonEdit);
            this.panelControl2.Controls.Add(this.buttonDismiss);
            this.panelControl2.Controls.Add(this.buttonEnroll);
            this.panelControl2.Controls.Add(this.buttonDelete);
            this.panelControl2.Controls.Add(this.pageNavigator);
            this.panelControl2.Controls.Add(this.textQuery);
            this.panelControl2.Controls.Add(this.lableSearchInfo);
            this.panelControl2.Controls.Add(this.labelDataDate);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Controls.Add(this.btnSearch);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1120, 110);
            this.panelControl2.TabIndex = 10;
            // 
            // datePicker
            // 
            this.datePicker.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.datePicker.Location = new System.Drawing.Point(94, 83);
            this.datePicker.Name = "datePicker";
            this.datePicker.ShowEthiopian = false;
            this.datePicker.ShowGregorian = true;
            this.datePicker.ShowTime = false;
            this.datePicker.Size = new System.Drawing.Size(202, 21);
            this.datePicker.TabIndex = 6;
            this.datePicker.VerticalLayout = true;
            this.datePicker.DateTimeChanged += new System.EventHandler(this.datePicker_DateTimeChanged);
            // 
            // checkCurrentDate
            // 
            this.checkCurrentDate.EditValue = true;
            this.checkCurrentDate.Location = new System.Drawing.Point(12, 56);
            this.checkCurrentDate.Name = "checkCurrentDate";
            this.checkCurrentDate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkCurrentDate.Properties.Appearance.Options.UseFont = true;
            this.checkCurrentDate.Properties.Caption = "Current Data";
            this.checkCurrentDate.Size = new System.Drawing.Size(185, 19);
            this.checkCurrentDate.TabIndex = 5;
            this.checkCurrentDate.CheckedChanged += new System.EventHandler(this.checkCurrentDate_CheckedChanged);
            // 
            // checkShowDismissed
            // 
            this.checkShowDismissed.Location = new System.Drawing.Point(10, 28);
            this.checkShowDismissed.Name = "checkShowDismissed";
            this.checkShowDismissed.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkShowDismissed.Properties.Appearance.Options.UseFont = true;
            this.checkShowDismissed.Properties.Caption = "Show Dismissed Employees";
            this.checkShowDismissed.Size = new System.Drawing.Size(185, 19);
            this.checkShowDismissed.TabIndex = 5;
            this.checkShowDismissed.CheckedChanged += new System.EventHandler(this.checkShowDismissed_CheckedChanged);
            // 
            // buttoImport
            // 
            this.buttoImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttoImport.Location = new System.Drawing.Point(454, 81);
            this.buttoImport.Name = "buttoImport";
            this.buttoImport.Size = new System.Drawing.Size(71, 23);
            this.buttoImport.TabIndex = 4;
            this.buttoImport.Text = "Import";
            this.buttoImport.Click += new System.EventHandler(this.buttoImport_Click);
            // 
            // buttonSelectAll
            // 
            this.buttonSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSelectAll.Location = new System.Drawing.Point(540, 81);
            this.buttonSelectAll.Name = "buttonSelectAll";
            this.buttonSelectAll.Size = new System.Drawing.Size(75, 23);
            this.buttonSelectAll.TabIndex = 4;
            this.buttonSelectAll.Text = "Select All";
            this.buttonSelectAll.Click += new System.EventHandler(this.buttonSelectAll_Click);
            // 
            // buttonChangeOrg
            // 
            this.buttonChangeOrg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonChangeOrg.Location = new System.Drawing.Point(621, 81);
            this.buttonChangeOrg.Name = "buttonChangeOrg";
            this.buttonChangeOrg.Size = new System.Drawing.Size(82, 23);
            this.buttonChangeOrg.TabIndex = 4;
            this.buttonChangeOrg.Text = "Transfer";
            this.buttonChangeOrg.Click += new System.EventHandler(this.buttonChangeOrg_Click);
            // 
            // buttonNew
            // 
            this.buttonNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNew.Location = new System.Drawing.Point(709, 81);
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(75, 23);
            this.buttonNew.TabIndex = 4;
            this.buttonNew.Text = "New";
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // buttonEdit
            // 
            this.buttonEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEdit.Location = new System.Drawing.Point(790, 81);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(75, 23);
            this.buttonEdit.TabIndex = 4;
            this.buttonEdit.Text = "Edit";
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // buttonDismiss
            // 
            this.buttonDismiss.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDismiss.Location = new System.Drawing.Point(871, 81);
            this.buttonDismiss.Name = "buttonDismiss";
            this.buttonDismiss.Size = new System.Drawing.Size(75, 23);
            this.buttonDismiss.TabIndex = 4;
            this.buttonDismiss.Text = "Dismiss";
            this.buttonDismiss.Click += new System.EventHandler(this.buttonDismiss_Click);
            // 
            // buttonEnroll
            // 
            this.buttonEnroll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEnroll.Location = new System.Drawing.Point(952, 81);
            this.buttonEnroll.Name = "buttonEnroll";
            this.buttonEnroll.Size = new System.Drawing.Size(75, 23);
            this.buttonEnroll.TabIndex = 4;
            this.buttonEnroll.Text = "Enroll";
            this.buttonEnroll.Click += new System.EventHandler(this.buttonEnroll_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDelete.Location = new System.Drawing.Point(1033, 81);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete.TabIndex = 4;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // pageNavigator
            // 
            this.pageNavigator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pageNavigator.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pageNavigator.Location = new System.Drawing.Point(694, 1);
            this.pageNavigator.Name = "pageNavigator";
            this.pageNavigator.NRec = 0;
            this.pageNavigator.PageSize = 30;
            this.pageNavigator.RecordIndex = 0;
            this.pageNavigator.Size = new System.Drawing.Size(414, 46);
            this.pageNavigator.TabIndex = 3;
            this.pageNavigator.PageChanged += new System.EventHandler(this.pageNavigator_PageChanged);
            this.pageNavigator.PaddingChanged += new System.EventHandler(this.pageNavigator_PaddingChanged);
            // 
            // textQuery
            // 
            this.textQuery.Location = new System.Drawing.Point(94, 6);
            this.textQuery.Name = "textQuery";
            this.textQuery.Size = new System.Drawing.Size(270, 20);
            this.textQuery.TabIndex = 2;
            this.textQuery.EditValueChanged += new System.EventHandler(this.textQuery_EditValueChanged);
            this.textQuery.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textQuery_KeyDown);
            // 
            // lableSearchInfo
            // 
            this.lableSearchInfo.Location = new System.Drawing.Point(215, 66);
            this.lableSearchInfo.Name = "lableSearchInfo";
            this.lableSearchInfo.Size = new System.Drawing.Size(0, 13);
            this.lableSearchInfo.TabIndex = 1;
            // 
            // labelDataDate
            // 
            this.labelDataDate.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDataDate.Location = new System.Drawing.Point(14, 85);
            this.labelDataDate.Name = "labelDataDate";
            this.labelDataDate.Size = new System.Drawing.Size(60, 13);
            this.labelDataDate.TabIndex = 1;
            this.labelDataDate.Text = "Data Date:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(12, 9);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(54, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Name/ID:";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(370, 6);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(74, 20);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.Text = "&Search";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.CustomizationFormText = "Name:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(377, 28);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem4.Text = "Name:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(124, 13);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.CustomizationFormText = "Code:";
            this.layoutControlItem5.Location = new System.Drawing.Point(377, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(330, 28);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem5.Text = "Code:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(124, 13);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageSize = new System.Drawing.Size(24, 24);
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Org.bmp");
            this.imageCollection1.Images.SetKeyName(1, "User.bmp");
            // 
            // ctxtOrgUnit
            // 
            this.ctxtOrgUnit.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miRootOrg,
            this.miChildOrg,
            this.miDelOrg,
            this.miEditOrg,
            this.miGroupUnder,
            this.miMakeRootOrganization,
            this.miRefresh});
            this.ctxtOrgUnit.Name = "ctxtTree";
            this.ctxtOrgUnit.Size = new System.Drawing.Size(290, 158);
            this.ctxtOrgUnit.Opening += new System.ComponentModel.CancelEventHandler(this.ctxtOrgUnit_Opening);
            // 
            // miRootOrg
            // 
            this.miRootOrg.Name = "miRootOrg";
            this.miRootOrg.Size = new System.Drawing.Size(289, 22);
            this.miRootOrg.Text = "New Root Organizational Unit";
            this.miRootOrg.Click += new System.EventHandler(this.miRootOrg_Click);
            // 
            // miChildOrg
            // 
            this.miChildOrg.Name = "miChildOrg";
            this.miChildOrg.Size = new System.Drawing.Size(289, 22);
            this.miChildOrg.Text = "Create Child Organizational Unit";
            this.miChildOrg.Click += new System.EventHandler(this.miChildOrg_Click);
            // 
            // miDelOrg
            // 
            this.miDelOrg.Name = "miDelOrg";
            this.miDelOrg.Size = new System.Drawing.Size(289, 22);
            this.miDelOrg.Text = "Delete Organizational Unit";
            this.miDelOrg.Click += new System.EventHandler(this.miDelOrg_Click);
            // 
            // miEditOrg
            // 
            this.miEditOrg.Name = "miEditOrg";
            this.miEditOrg.Size = new System.Drawing.Size(289, 22);
            this.miEditOrg.Text = "Edit Organiational Unit Information";
            this.miEditOrg.Click += new System.EventHandler(this.miEditOrg_Click);
            // 
            // miGroupUnder
            // 
            this.miGroupUnder.Name = "miGroupUnder";
            this.miGroupUnder.Size = new System.Drawing.Size(289, 22);
            this.miGroupUnder.Text = "Group Under Different Organizatinal Unit";
            this.miGroupUnder.Click += new System.EventHandler(this.miGroupUnder_Click);
            // 
            // miMakeRootOrganization
            // 
            this.miMakeRootOrganization.Name = "miMakeRootOrganization";
            this.miMakeRootOrganization.Size = new System.Drawing.Size(289, 22);
            this.miMakeRootOrganization.Text = "Make Root Organization";
            this.miMakeRootOrganization.Click += new System.EventHandler(this.miMakeRootOrganization_Click);
            // 
            // miRefresh
            // 
            this.miRefresh.Name = "miRefresh";
            this.miRefresh.Size = new System.Drawing.Size(289, 22);
            this.miRefresh.Text = "Refresh";
            this.miRefresh.Click += new System.EventHandler(this.miRefresh_Click);
            // 
            // listEmployee
            // 
            this.listEmployee.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNo,
            this.colID,
            this.colName,
            this.colGender,
            this.colTIN,
            this.colGrossSalary,
            this.colStatus});
            this.listEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listEmployee.FullRowSelect = true;
            this.listEmployee.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listEmployee.HideSelection = false;
            this.listEmployee.Location = new System.Drawing.Point(279, 110);
            this.listEmployee.Name = "listEmployee";
            this.listEmployee.Size = new System.Drawing.Size(841, 564);
            this.listEmployee.TabIndex = 15;
            this.listEmployee.UseCompatibleStateImageBehavior = false;
            this.listEmployee.View = System.Windows.Forms.View.Details;
            this.listEmployee.SelectedIndexChanged += new System.EventHandler(this.listEmployee_SelectedIndexChanged);
            this.listEmployee.DoubleClick += new System.EventHandler(this.listEmployee_DoubleClick);
            // 
            // colNo
            // 
            this.colNo.Text = "No.";
            // 
            // colID
            // 
            this.colID.Text = "ID";
            this.colID.Width = 85;
            // 
            // colName
            // 
            this.colName.Text = "Name";
            this.colName.Width = 163;
            // 
            // colGender
            // 
            this.colGender.Text = "Gender";
            this.colGender.Width = 82;
            // 
            // colTIN
            // 
            this.colTIN.Text = "TIN";
            this.colTIN.Width = 92;
            // 
            // colGrossSalary
            // 
            this.colGrossSalary.Text = "Salary";
            this.colGrossSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colGrossSalary.Width = 116;
            // 
            // colStatus
            // 
            this.colStatus.Text = "Status";
            this.colStatus.Width = 81;
            // 
            // orgTree
            // 
            this.orgTree.ContextMenuStrip = this.ctxtOrgUnit;
            this.orgTree.Dock = System.Windows.Forms.DockStyle.Left;
            this.orgTree.HideSelection = false;
            this.orgTree.ImageIndex = 0;
            this.orgTree.includeDismissed = false;
            this.orgTree.IncludeEmployees = false;
            this.orgTree.Location = new System.Drawing.Point(0, 110);
            this.orgTree.Name = "orgTree";
            this.orgTree.SelectedImageIndex = 0;
            this.orgTree.Size = new System.Drawing.Size(279, 564);
            this.orgTree.TabIndex = 14;
            this.orgTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.orgTree_AfterSelect);
            // 
            // EmployeeManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1120, 674);
            this.Controls.Add(this.listEmployee);
            this.Controls.Add(this.orgTree);
            this.Controls.Add(this.panelControl2);
            this.Name = "EmployeeManager";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Employee Manager";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkCurrentDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkShowDismissed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textQuery.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ctxtOrgUnit.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnSearch;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private System.Windows.Forms.ListView listEmployee;
        private System.Windows.Forms.ColumnHeader colID;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader colGender;
        private System.Windows.Forms.ColumnHeader colTIN;
        private System.Windows.Forms.ColumnHeader colGrossSalary;
        private System.Windows.Forms.ColumnHeader colStatus;
        private INTAPS.Payroll.Client.OrgTree orgTree;
        private DevExpress.XtraEditors.TextEdit textQuery;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.ColumnHeader colNo;
        private INTAPS.UI.PageNavigator pageNavigator;
        private System.Windows.Forms.ContextMenuStrip ctxtOrgUnit;
        private System.Windows.Forms.ToolStripMenuItem miRootOrg;
        private System.Windows.Forms.ToolStripMenuItem miChildOrg;
        private System.Windows.Forms.ToolStripMenuItem miDelOrg;
        private System.Windows.Forms.ToolStripMenuItem miEditOrg;
        private System.Windows.Forms.ToolStripMenuItem miGroupUnder;
        private System.Windows.Forms.ToolStripMenuItem miRefresh;
        private DevExpress.XtraEditors.SimpleButton buttonDismiss;
        private DevExpress.XtraEditors.SimpleButton buttonEnroll;
        private DevExpress.XtraEditors.SimpleButton buttonDelete;
        private DevExpress.XtraEditors.SimpleButton buttoImport;
        private DevExpress.XtraEditors.LabelControl lableSearchInfo;
        private DevExpress.XtraEditors.SimpleButton buttonEdit;
        private DevExpress.XtraEditors.SimpleButton buttonNew;
        private DevExpress.XtraEditors.CheckEdit checkShowDismissed;
        private DevExpress.XtraEditors.SimpleButton buttonSelectAll;
        private System.Windows.Forms.ToolStripMenuItem miMakeRootOrganization;
        private DevExpress.XtraEditors.SimpleButton buttonChangeOrg;
        private BNDualCalendar datePicker;
        private DevExpress.XtraEditors.CheckEdit checkCurrentDate;
        private DevExpress.XtraEditors.LabelControl labelDataDate;
    }
}