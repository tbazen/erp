﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using INTAPS.Payroll.Client;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll;
using DevExpress.XtraLayout.Utils;
using DevExpress.Utils;
using DevExpress.XtraTreeList.StyleFormatConditions;
using DevExpress.XtraGrid;

namespace BIZNET.iERP.Client
{
    public partial class EmployeeManager : DevExpress.XtraEditors.XtraForm
    {
        public EmployeeManager()
        {
            InitializeComponent();
            updateButtonState();
            updateDataDateControls();
            reloadData();
        }

        private void reloadData()
        {
            orgTree.LoadData(this.dataDate);
            listEmployee.Items.Clear();
        }

        private void miImport_Click(object sender, EventArgs e)
        {

        }

        private void orgTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            textQuery.Text = "";
            startSeach();
        }

        private void ctxtOrgUnit_Opening(object sender, CancelEventArgs e)
        {
            bool ItemSelected = orgTree.SelectedNode != null;
            miChildOrg.Enabled = ItemSelected;
            miDelOrg.Enabled = ItemSelected;
            miEditOrg.Enabled = ItemSelected;
            miGroupUnder.Enabled = ItemSelected;
            OrgUnit selectedOrgUnit = orgTree.SelectedOrg;
            miMakeRootOrganization.Enabled = selectedOrgUnit != null && selectedOrgUnit.PID != -1;
        }

        private void miRootOrg_Click(object sender, EventArgs e)
        {
            OrgUnitProfile o = new OrgUnitProfile(-1);
            if (o.ShowDialog() == DialogResult.OK)
            {
                orgTree.AddOrgUnit(o.FormData);
            }
        }

        private void miChildOrg_Click(object sender, EventArgs e)
        {
            OrgUnitProfile o = new OrgUnitProfile(orgTree.SelectedOrg.ObjectIDVal);
            if (o.ShowDialog() == DialogResult.OK)
            {
                orgTree.AddOrgUnit(o.FormData);
            }
        }

        private void miDelOrg_Click(object sender, EventArgs e)
        {
            if (INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete this organizational unit from database?"))
            {
                try
                {
                    PayrollClient.DeleteOrgUnit(orgTree.SelectedOrg.ObjectIDVal);
                    orgTree.DeleteOrgUnit(orgTree.SelectedOrg.ObjectIDVal);
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't delete organizational unit.", ex);
                }
            }
        }

        private void miEditOrg_Click(object sender, EventArgs e)
        {
            OrgUnitProfile o = new OrgUnitProfile((OrgUnit)orgTree.SelectedOrg.Clone());
            if (o.ShowDialog() == DialogResult.OK)
            {
                orgTree.UpdateOrgUnit(o.FormData);
            }
        }


        private void btnSearch_Click(object sender, EventArgs e)
        {
            startSeach();
        }
        void updateButtonState()
        {
            string q = textQuery.Text.Trim();
            int orgUnit = -1;
            if (string.IsNullOrEmpty(q))
            {
                orgUnit = orgTree.SelectedOrg == null ? -1 : orgTree.SelectedOrg.id;
            }
            else
            {
                orgUnit = -1;
            }
            btnSearch.Enabled = !string.IsNullOrEmpty(q);
            buttonNew.Enabled = buttoImport.Enabled = orgUnit != -1;
            buttonChangeOrg.Enabled = buttonDelete.Enabled = listEmployee.SelectedItems.Count > 0;
            buttonEdit.Enabled = listEmployee.SelectedItems.Count == 1;
            if (listEmployee.SelectedItems.Count == 0)
            {
                buttonEnroll.Enabled = buttonDismiss.Enabled = false;
            }
            else
            {
                bool enroll = false;
                bool dismiss = false;
                foreach (ListViewItem li in listEmployee.SelectedItems)
                {
                    Employee e = li.Tag as Employee;
                    if (e.status == EmployeeStatus.Fired || e.status == EmployeeStatus.Pending)
                    {
                        enroll = true;
                    }
                    else if (e.status == EmployeeStatus.Enrolled)
                    {
                        dismiss = true;
                    }
                    buttonEnroll.Enabled = enroll;
                    buttonDismiss.Enabled = dismiss;
                }
            }
        }
        private void startSeach()
        {
            pageNavigator.RecordIndex = 0;

            search();
        }

        private void search()
        {
            string q = textQuery.Text.Trim();
            int orgUnit = -1;
            if (string.IsNullOrEmpty(q))
            {

                orgUnit = orgTree.SelectedOrg == null ? -1 : orgTree.SelectedOrg.id;
            }
            else
            {
                orgUnit = -1;
            }
            if (orgUnit == -1)
                lableSearchInfo.Text = "Search Result";
            else
                lableSearchInfo.Text = "Employees under " + orgTree.SelectedOrg.Name;
            listEmployee.Items.Clear();


            try
            {
                int N;
                Employee[] emp = PayrollClient.SearchEmployee(dataDate.Ticks, orgUnit, q, checkShowDismissed.Checked, EmployeeSearchType.All, pageNavigator.RecordIndex, pageNavigator.PageSize, out N);
                pageNavigator.NRec = N;
                for (int i = 0; i < emp.Length; i++)
                {
                    ListViewItem li = new ListViewItem((pageNavigator.RecordIndex + i + 1).ToString());
                    li.SubItems.Add(emp[i].employeeID);
                    li.SubItems.Add(emp[i].employeeName);
                    li.SubItems.Add(emp[i].sex.ToString());
                    li.SubItems.Add(emp[i].TIN);
                    li.SubItems.Add(AccountBase.FormatAmount(emp[i].grossSalary));

                    li.SubItems.Add(Employee.getStatusString(emp[i].status));
                    li.Tag = emp[i];
                    listEmployee.Items.Add(li);
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
            updateButtonState();
        }

        private void pageNavigator_PaddingChanged(object sender, EventArgs e)
        {

        }

        private void pageNavigator_PageChanged(object sender, EventArgs e)
        {
            search();
        }

        private void listEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateButtonState();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            int c = listEmployee.SelectedItems.Count;
            if (c == 0)
                return;
            if (MessageBox.ShowWarningMessage("Are you sure you want to delete the selected emplyees?") != DialogResult.Yes)
                return;
            int nError = 0;
            List<ListViewItem> items = new List<ListViewItem>();
            Exception error = null;
            for (int i = c - 1; i >= 0; i--)
            {
                Employee emp = listEmployee.SelectedItems[i].Tag as Employee;
                try
                {
                    PayrollClient.DeleteEmployee(emp.id);
                    items.Add(listEmployee.SelectedItems[i]);
                }
                catch (Exception ex)
                {
                    nError++;
                    error = ex;
                }
            }
            if (nError > 0)
            {
                if (nError > 1)
                    MessageBox.ShowErrorMessage("Error trying to delete one or more failed to delete");
                else
                    MessageBox.ShowException(error);
            }
            search();
        }

        private void buttoImport_Click(object sender, EventArgs e)
        {
            OrgUnit ou = orgTree.SelectedOrg;
            if (ou == null)
                return;
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Excel Files|*.xlsx|Macro Enabled Excel Files|*.xlsm";
            ofd.Multiselect = false;
            if (ofd.ShowDialog(this) != DialogResult.OK)
                return;
            try
            {
                ImportEmployeeForm imp = new ImportEmployeeForm(ofd.FileName, ou.id);
                imp.ShowDialog(this);
                if (imp.imported)
                    search();
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void buttonEnroll_Click(object sender, EventArgs e)
        {
            int c = listEmployee.SelectedItems.Count;
            if (c == 0)
                return;
            int nError = 0;
            Exception error = null;
            for (int i = c - 1; i >= 0; i--)
            {
                Employee emp = listEmployee.SelectedItems[i].Tag as Employee;
                try
                {
                    if (emp.status != EmployeeStatus.Enrolled)
                        PayrollClient.EnrollEmployee(dataDate, emp.id);
                }
                catch (Exception ex)
                {
                    nError++;
                    error = ex;
                }
            }
            if (nError > 0)
            {
                if (nError > 1)
                    MessageBox.ShowErrorMessage("Error trying to enroll one or more failed to delete");
                else
                    MessageBox.ShowException(error);
            }
            search();
        }

        private void buttonDismiss_Click(object sender, EventArgs e)
        {
            int c = listEmployee.SelectedItems.Count;
            if (c == 0)
                return;
            int nError = 0;
            Exception error = null;
            for (int i = c - 1; i >= 0; i--)
            {
                Employee emp = listEmployee.SelectedItems[i].Tag as Employee;
                try
                {
                    if (emp.status == EmployeeStatus.Enrolled)
                        PayrollClient.FireEmployee(dataDate, emp.id);
                }
                catch (Exception ex)
                {
                    nError++;
                    error = ex;
                }
            }
            if (nError > 0)
            {
                if (nError > 1)
                    MessageBox.ShowErrorMessage("Error trying to dismiss one or more failed to delete");
                else
                    MessageBox.ShowException(error);
            }
            search();
        }

        private void textQuery_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!string.IsNullOrEmpty(textQuery.Text.Trim()))
                    startSeach();
            }
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            int c = listEmployee.SelectedItems.Count;
            if (c == 0)
                return;
            try
            {
                Employee emp = listEmployee.SelectedItems[0].Tag as Employee;
                if ("true".Equals(System.Configuration.ConfigurationManager.AppSettings["html_ui"]))
                {
                    EmployeeProfileForm prof = new EmployeeProfileForm(emp.id);
                    prof.Show(this);
                }
                else
                {
                    EmployeeEditor ee = new EmployeeEditor(emp.id, this.dataDate.Ticks, true);
                    ee.ShowDialog(this);
                    if (ee.updated)
                        search();
                }
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
        private void buttonNew_Click(object sender, EventArgs e)
        {
            OrgUnit ou = orgTree.SelectedOrg;
            if (ou == null)
                return;
            if ("true".Equals(System.Configuration.ConfigurationManager.AppSettings["html_ui"]))
                {
                EmployeeEditor_bio bio = new EmployeeEditor_bio(ou.id);
                if (bio.ShowDialog(this) == DialogResult.OK)
                {
                    EmployeeProfileForm prof = new EmployeeProfileForm(bio.employeeID);
                    prof.Show(this);
                }
            }
            else
            {
                var editor= new EmployeeEditor(ou.id);
                if (editor.ShowDialog(this) == DialogResult.OK)
                {
                    this.reloadData();
                }
            }
        }

        private void miRefresh_Click(object sender, EventArgs e)
        {
            orgTree.LoadData(this.dataDate);
        }

        private void miGroupUnder_Click(object sender, EventArgs e)
        {
            INTAPS.Payroll.Client.EmployeePicker picker = new INTAPS.Payroll.Client.EmployeePicker();

            if (picker.PickOrgUnitItem(false, true) == DialogResult.OK)
            {
                try
                {
                    OrgUnit org = orgTree.SelectedOrg;
                    org.PID = picker.PickedOrgUnit.ObjectIDVal;
                    PayrollClient.UpdateOrgUnit(org);
                    orgTree.DeleteOrgUnit(org.ObjectIDVal);
                    orgTree.AddOrgUnit(org);
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Operation not succesfull.", ex);
                }
            }
        }

        private void checkShowDismissed_CheckedChanged(object sender, EventArgs e)
        {
            if (orgTree.SelectedOrg != null || textQuery.Text.Trim() != "")
                search();
        }

        private void listEmployee_DoubleClick(object sender, EventArgs e)
        {
            buttonEdit_Click(null, null);
        }

        private void buttonSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem li in listEmployee.Items)
                li.Selected = true;
            listEmployee.Focus();
        }

        private void miMakeRootOrganization_Click(object sender, EventArgs e)
        {

            try
            {
                OrgUnit org = orgTree.SelectedOrg;
                org.PID = -1;
                PayrollClient.UpdateOrgUnit(org);
                orgTree.DeleteOrgUnit(org.ObjectIDVal);
                orgTree.AddOrgUnit(org);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Operation not succesfull.", ex);
            }

        }

        private void textQuery_EditValueChanged(object sender, EventArgs e)
        {
            btnSearch.Enabled = !string.IsNullOrEmpty(textQuery.Text.Trim());
        }

        private void buttonChangeOrg_Click(object sender, EventArgs e)
        {

            int c = listEmployee.SelectedItems.Count;
            if (c == 0)
                return;
            INTAPS.Payroll.Client.EmployeePicker picker = new INTAPS.Payroll.Client.EmployeePicker();
            if (picker.PickOrgUnitItem(false, true) != DialogResult.OK)
                return;

            int nError = 0;
            Exception error = null;
            for (int i = c - 1; i >= 0; i--)
            {
                Employee emp = listEmployee.SelectedItems[i].Tag as Employee;
                try
                {
                    PayrollClient.ChangeEmployeeOrgUnit(emp.id, picker.PickedOrgUnit.id, dataDate);
                }
                catch (Exception ex)
                {
                    nError++;
                    error = ex;
                }
            }
            if (nError > 0)
            {
                if (nError > 1)
                    MessageBox.ShowErrorMessage("Error trying to enroll one or more failed to delete");
                else
                    MessageBox.ShowException(error);
            }
            search();
        }

        private void checkCurrentDate_CheckedChanged(object sender, EventArgs e)
        {
            updateDataDateControls();
            reloadData();

        }

        private void updateDataDateControls()
        {
            labelDataDate.Enabled = datePicker.Enabled = !checkCurrentDate.Checked;
        }
        DateTime dataDate
        {
            get
            {
                if (checkCurrentDate.Checked)
                    return DateTime.Now;
                return datePicker.DateTime;
            }
        }
        private void datePicker_DateTimeChanged(object sender, EventArgs e)
        {
            reloadData();
        }
    }
}