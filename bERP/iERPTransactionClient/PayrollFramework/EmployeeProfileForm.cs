﻿using System;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using INTAPS;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.ClientServer.Client;
using INTAPS.Payroll.Client;
using INTAPS.UI.HTML;

namespace BIZNET.iERP.Client
{
    public partial class EmployeeProfileForm : Form
    {
        private class CefEmployeeProfile
        {
            private readonly EmployeeProfileForm _parent;
            private AccountPicker<Account> _mPicker;
            private EmployeeProfileForm employeeProfileForm;
            private ChromiumWebBrowser _chromeBrowser;

            public CefEmployeeProfile(EmployeeProfileForm parent, ChromiumWebBrowser chromeBrowser)
            {
                this._parent = parent;
                this._chromeBrowser = chromeBrowser;
            }

            public void newversion(int employeeId)
            {
                _parent.Invoke(new ProcessParameterless(delegate
                {
                    EmployeeEditor_bio bio = new EmployeeEditor_bio(employeeId, -1, false);
                    if (bio.ShowDialog(_parent) == DialogResult.OK)
                    {
                        _chromeBrowser.ExecuteScriptAsync("BERP.refreshDocument();");
                    }
                }));
                
            }

            public void editProfile(int employeeId, string v)
            {
                _parent.Invoke(new ProcessTowParameter<int, long>(delegate(int empId, long ver)
                {
                    EmployeeEditor_bio bio = new EmployeeEditor_bio(empId, ver, false);
                    if (bio.ShowDialog(_parent) == DialogResult.OK)
                    {
                        loadBio(long.Parse(v));
                    }
                }), employeeId, long.Parse(v));
                
            }

            public void addcontrolaccount(int employeeId, string v)
            {
                _parent.Invoke(new ProcessTowParameter<int, long>(delegate(int empId, long ver)
                {
                    if (_mPicker == null)
                        _mPicker = CreatePicker();
                    if (_mPicker.ShowDialog() == DialogResult.OK)
                    {
                        _chromeBrowser.ExecuteScriptAsync(String.Format("BERP.addAccount({0},{1});", new object[] { employeeId, _mPicker.selectedAccount.id }));
                    }
                }), employeeId, long.Parse(v));
            }

            public void editAccountInfo(int employeeId, string v)
            {
                _parent.Invoke(new ProcessTowParameter<int, long>(delegate(int empId, long ver)
                {
                    EmployeeEditor_bio bio = new EmployeeEditor_bio(empId, ver, true);

                    if (bio.ShowDialog(_parent) == DialogResult.OK)
                    {
                        loadBio(long.Parse(v));
                    }
                }), employeeId, long.Parse(v));
                
            }

            public void viewledger(int employeeId, int accountid)
            {
                _parent.Invoke(new ProcessTowParameter<int, int>(delegate(int id, int accId)
                {
                    AccountBase[] accounts = PayrollClient.GetParentAcccountsByEmployee(id);

                    AccountBase account = accounts.FirstOrDefault(a => a.id == accId);
                    if (account != null)
                        new LedgerViewer(CostCenter.ROOT_COST_CENTER, new int[] { account.id }, "Ledger of property: {0}".format(account.NameCode)).Show(_parent);
                }), employeeId, accountid);

            }

            public void editWorkdata(int employeeId, int periodId)
            {
                _parent.Invoke(new ProcessTowParameter<int, int>(delegate(int empID, int perID)
                {
                    EmployeeEditor_wd bio = new EmployeeEditor_wd(empID, perID);
                    if (bio.ShowDialog(_parent) == DialogResult.OK)
                    {
                        _chromeBrowser.ExecuteScriptAsync("BERP.loadPeriod();");
                    }
                }), employeeId, periodId); 
            }

            private void loadBio(long ver)
            {
                _chromeBrowser.ExecuteScriptAsync(String.Format("BERP.loadBio({0});", new object[] { ver }));
            }
            
            private AccountPicker<Account> CreatePicker()
            {
                AccountTree acTree = new AccountTree { CostCenterAccountMode = false };
                AccountPicker<Account> ret = new AccountPicker<Account>(acTree) { OnlyLeafAccount = false };
                return ret;
            }


        }

        readonly int _employeeId;
        public ChromiumWebBrowser chromeBrowser;


        public EmployeeProfileForm(int employeeId)
        {
            _employeeId = employeeId;
            InitializeComponent();
            InitializeChromium();
            chromeBrowser.RegisterJsObject("EmployeeProfile", new CefEmployeeProfile(this, chromeBrowser));
            chromeBrowser.KeyDown += chromeBrowser_KeyDown;
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            MessageBox.ShowWarningMessage(keyData.ToString());
            return base.ProcessCmdKey(ref msg, keyData);
        }
        void chromeBrowser_KeyDown(object sender, KeyEventArgs e)
        {
            MessageBox.ShowWarningMessage(e.KeyCode.ToString());
        }

        public void InitializeChromium()
        {
            string url = "{0}{1}?sid={2}&id={3}".format(
                 ConfigurationManager.AppSettings["webserver"]
                    , "employee_profile.html", ApplicationClient.SessionID, _employeeId);
            chromeBrowser = new ChromiumWebBrowser(url);
            this.Controls.Add(chromeBrowser);
            chromeBrowser.Dock = DockStyle.Fill;

            // Allow the use of local resources in the browser
            BrowserSettings browserSettings = new BrowserSettings();
            browserSettings.FileAccessFromFileUrls = CefState.Enabled;
            browserSettings.UniversalAccessFromFileUrls = CefState.Enabled;
            chromeBrowser.BrowserSettings = browserSettings;
            
        }

    }
}
