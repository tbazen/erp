﻿namespace BIZNET.iERP.Client
{
    partial class ImportEmployeeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.listView1 = new System.Windows.Forms.ListView();
            this.colNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colGender = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colAge = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colGrossSalary = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.checkAccountAsCost = new DevExpress.XtraEditors.CheckEdit();
            this.dualCalendar = new BIZNET.iERP.Client.BNDualCalendar();
            this.costCenterPlaceHolder = new INTAPS.Accounting.Client.CostCenterPlaceHolder();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkAccountAsCost.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.textBox1);
            this.panelControl1.Controls.Add(this.progressBar1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 302);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(792, 99);
            this.panelControl1.TabIndex = 0;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 18);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(775, 76);
            this.textBox1.TabIndex = 2;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(0, 0);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(792, 11);
            this.progressBar1.TabIndex = 1;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(681, 5);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(106, 30);
            this.buttonOk.TabIndex = 0;
            this.buttonOk.Text = "&Import";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNo,
            this.colStatus,
            this.colID,
            this.colName,
            this.colGender,
            this.colAge,
            this.colGrossSalary});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.FullRowSelect = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(0, 120);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(792, 182);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // colNo
            // 
            this.colNo.Text = "No.";
            this.colNo.Width = 33;
            // 
            // colStatus
            // 
            this.colStatus.Text = "Status";
            // 
            // colID
            // 
            this.colID.Text = "ID#";
            this.colID.Width = 69;
            // 
            // colName
            // 
            this.colName.Text = "Name";
            this.colName.Width = 242;
            // 
            // colGender
            // 
            this.colGender.Text = "Gender";
            this.colGender.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colGender.Width = 98;
            // 
            // colAge
            // 
            this.colAge.Text = "Age";
            // 
            // colGrossSalary
            // 
            this.colGrossSalary.Text = "Gross Salary";
            this.colGrossSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colGrossSalary.Width = 188;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.checkAccountAsCost);
            this.panelControl2.Controls.Add(this.dualCalendar);
            this.panelControl2.Controls.Add(this.buttonOk);
            this.panelControl2.Controls.Add(this.costCenterPlaceHolder);
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(792, 120);
            this.panelControl2.TabIndex = 2;
            // 
            // checkAccountAsCost
            // 
            this.checkAccountAsCost.EditValue = true;
            this.checkAccountAsCost.Location = new System.Drawing.Point(481, 7);
            this.checkAccountAsCost.Name = "checkAccountAsCost";
            this.checkAccountAsCost.Properties.Caption = "Account As Cost";
            this.checkAccountAsCost.Size = new System.Drawing.Size(123, 19);
            this.checkAccountAsCost.TabIndex = 3;
            // 
            // dualCalendar
            // 
            this.dualCalendar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dualCalendar.Location = new System.Drawing.Point(12, 48);
            this.dualCalendar.Name = "dualCalendar";
            this.dualCalendar.ShowEthiopian = true;
            this.dualCalendar.ShowGregorian = true;
            this.dualCalendar.ShowTime = true;
            this.dualCalendar.Size = new System.Drawing.Size(414, 63);
            this.dualCalendar.TabIndex = 2;
            this.dualCalendar.VerticalLayout = true;
            // 
            // costCenterPlaceHolder
            // 
            this.costCenterPlaceHolder.account = null;
            this.costCenterPlaceHolder.AllowAdd = false;
            this.costCenterPlaceHolder.Location = new System.Drawing.Point(94, 5);
            this.costCenterPlaceHolder.Name = "costCenterPlaceHolder";
            this.costCenterPlaceHolder.OnlyLeafAccount = false;
            this.costCenterPlaceHolder.Size = new System.Drawing.Size(332, 21);
            this.costCenterPlaceHolder.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 29);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(80, 13);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Enrollment Date:";
            this.labelControl2.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(58, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Cost Center";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // ImportEmployeeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 401);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "ImportEmployeeForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Import Employee Form";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkAccountAsCost.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader colNo;
        private System.Windows.Forms.ColumnHeader colID;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader colGender;
        private System.Windows.Forms.ColumnHeader colGrossSalary;
        private System.Windows.Forms.ProgressBar progressBar1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private INTAPS.Accounting.Client.CostCenterPlaceHolder costCenterPlaceHolder;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private BNDualCalendar dualCalendar;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.ColumnHeader colAge;
        private DevExpress.XtraEditors.CheckEdit checkAccountAsCost;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ColumnHeader colStatus;
    }
}