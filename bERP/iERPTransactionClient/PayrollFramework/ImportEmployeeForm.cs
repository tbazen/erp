﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using INTAPS.Payroll;
using INTAPS.Payroll.Client;
using INTAPS.ClientServer;
using System.Reflection;

namespace BIZNET.iERP.Client
{
    public partial class ImportEmployeeForm : DevExpress.XtraEditors.XtraForm
    {
        public bool imported = false;
        public List<Employee> employees;
        public List<Dictionary<int, SingleValueData>> additionalData;
        int _orgUnit;
        FieldInfo[] additionalImportFields;
        PayrollComponentFormula[] additionalImportFormulae;
        string _fileName;
        public ImportEmployeeForm(string fileName,int ordUnit)
        {
            InitializeComponent();
            object []s=PayrollClient.GetSystemParameters(new string[]{"additionalImportFields","additionalImportFormulae"});
            if(string.IsNullOrEmpty(s[0] as string))
                additionalImportFields=new FieldInfo[0];
            else
            {
                int i=0;
                string []fn =((string)s[0]).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                additionalImportFields = new FieldInfo[fn.Length];
                foreach (string f in fn)
                {
                    FieldInfo fi = typeof(Employee).GetField(f);
                    if (fi == null)
                    {
                        throw new ServerUserMessage("Import settings are invalid. Infalid field name " + fi);
                    }
                    additionalImportFields[i] = fi;
                    i++;
                    ColumnHeader h = listView1.Columns.Add(fi.Name);
                    h.Width = 50;
                    if (fi.FieldType == typeof(double))
                        h.TextAlign = HorizontalAlignment.Right;
                }
                

            }
            if (string.IsNullOrEmpty(s[1] as string))
                additionalImportFormulae = new PayrollComponentFormula[0];
            else
            {
                string [] fs= ((string)s[1]).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                additionalImportFormulae = new PayrollComponentFormula[fs.Length];
                for (int i = 0; i < fs.Length; i++)
                {
                    int fid;
                    if (!int.TryParse((string)fs[i], out fid))
                        throw new ServerUserMessage("Import settings are invalid. Invalid formula ID:" + fs[i]);
                    additionalImportFormulae[i] = PayrollClient.GetPCFormula(fid);
                    if (additionalImportFormulae[i] == null)
                    {
                        throw new ServerUserMessage("Import settings are invalid. Invalid formula ID:" + fs[i]);
                    }
                    ColumnHeader h = listView1.Columns.Add(additionalImportFormulae[i].Name);
                    h.Width = 50;
                    h.TextAlign = HorizontalAlignment.Right;
                }
            }
            costCenterPlaceHolder.SetByID(Globals.currentCostCenterID);
            _orgUnit = ordUnit;
            _fileName = fileName;
            
        }
        protected override void OnLoad(EventArgs e)
        {
            importFile(_fileName);
        }
        static string getCellText(Cell cell, List<string> stringTable)
        {
            if (cell == null)
                return null;
            if (cell.DataType != null && cell.DataType.InnerText == "s")
                return stringTable[int.Parse(cell.CellValue.Text)];
            double d;
            if (double.TryParse(cell.CellValue.Text, out d))
                return Math.Round(d, 6).ToString();
            return cell.CellValue.Text;
        }
        private Cell getCell(Dictionary<int, Cell> cells, int row)
        {
            if (cells.ContainsKey(row))
                return cells[row];
            return null;
        }
        void importFile(string fileName)
        {
            SpreadsheetDocument doc = null;
            try
            {
                doc = SpreadsheetDocument.Open(fileName, true);
                if (doc == null)
                {
                    MessageBox.ShowErrorMessage("Couldn't open " + fileName);
                    return;
                }


                SharedStringTable ss = doc.WorkbookPart.SharedStringTablePart.SharedStringTable;
                List<string> stringTable = new List<string>();
                foreach (SharedStringItem item in ss)
                {
                    stringTable.Add(item.Text.Text);
                }
                int recordCount = 0;
                int skippedRecord = 0;
                employees = new List<Employee>();
                additionalData = new List<Dictionary<int, SingleValueData>>();
                foreach (WorksheetPart ws in doc.WorkbookPart.WorksheetParts)
                {
                    foreach (OpenXmlElement data in ws.Worksheet)
                    {
                        if (data is SheetData)
                        {
                            int row = -1;
                            Console.WriteLine("Transfering " + (data.ChildElements.Count - 1) + " Rows");
                            Console.WriteLine();
                            foreach (Row el in data)
                            {
                                row++;
                                if (row < 1)
                                    continue;
                                ListViewItem li = new ListViewItem((listView1.Items.Count + 1).ToString());
                                try
                                {
                                    
                                    Console.WriteLine(row + "                       ");
                                    
                                    
                                        Dictionary<int, Cell> cells = new Dictionary<int, Cell>();
                                        foreach (Cell c in el)
                                        {
                                            string r = c.CellReference.Value;
                                            for (int i = 0; i < r.Length; i++)
                                                if (Char.IsNumber(r[i]))
                                                {
                                                    r = r.Substring(0, i);
                                                    break;
                                                }

                                            cells.Add((int)(r[0] - 'A') + 1, c);
                                        }
                                        int cellColIndex = 1;

                                        string strID = getCellText(getCell(cells, cellColIndex++), stringTable);
                                        string strName = getCellText(getCell(cells, cellColIndex++), stringTable);
                                        string strGender = getCellText(getCell(cells, cellColIndex++), stringTable);
                                        string strAge = getCellText(getCell(cells, cellColIndex++), stringTable);
                                        string strGrossSalary = getCellText(getCell(cells, cellColIndex++), stringTable);
                                        if (strID==null || (strID=strID.Trim())=="")
                                        {
                                            throw new Exception(string.Format("Record {0}: Invalid ID: '{1}'", row, strID));
                                        }
                                        if (strName == null || (strName = strName.Trim()) == "")
                                        {
                                            throw new Exception(string.Format("Record {0}: Invalid Name: '{1}'", row, strName));
                                        }
                                        Employee e = new Employee();
                                        
                                        int age;
                                        if (strAge == null || !int.TryParse(strAge, out age))
                                        {
                                            age = 30;
                                        }
                                        double grossSalary;
                                        
                                        if (!double.TryParse(strGrossSalary, out grossSalary))
                                        {
                                            throw new Exception(string.Format("Record {0}: Invalid gross salary: '{1}'", row, strGrossSalary));
                                        }                                        
                                        
                                        Sex s;
                                        if (strGender!=null &&  strGender.Length > 0 && char.ToUpper(strGender[0]) == 'F')
                                            s = Sex.Female;
                                        else
                                            s = Sex.Male;
                                        
                                        
                                        e.employeeID = strID;
                                        e.employeeName = strName;
                                        e.grossSalary = grossSalary;
                                        e.sex = s;
                                        li.SubItems.Add("Ready");
                                        li.SubItems.Add(strID);
                                        li.SubItems.Add(strName);
                                        li.SubItems.Add(s.ToString());
                                        li.SubItems.Add(age.ToString());
                                        li.SubItems.Add(grossSalary.ToString("#,#0.00"));
                                        for (int i = 0; i < additionalImportFields.Length; i++)
                                        {
                                            string val= getCellText(getCell(cells, cellColIndex++), stringTable);
                                            if (string.IsNullOrEmpty(val))
                                            {
                                                li.SubItems.Add("");
                                                continue;
                                            }
                                            additionalImportFields[i].SetValue(e, Convert.ChangeType(val, additionalImportFields[i].FieldType));
                                            li.SubItems.Add(val);
                                        }
                                        Dictionary<int, SingleValueData> thisData = new Dictionary<int, SingleValueData>();
                                        for (int i = 0; i < additionalImportFormulae.Length; i++)
                                        {
                                            string val = getCellText(getCell(cells, cellColIndex++), stringTable);
                                            
                                            if (string.IsNullOrEmpty(val))
                                            {
                                                li.SubItems.Add("");

                                            }
                                            else
                                            {
                                                double dval;
                                                if (!double.TryParse(val, out dval))
                                                    throw new ServerUserMessage("Invalid value for " + additionalImportFormulae[i].Name + " :" + val);
                                                SingleValueData sv = new SingleValueData();
                                                sv.value = dval;
                                                thisData.Add(additionalImportFormulae[i].ID, sv);
                                            }
                                            
                                            li.SubItems.Add(val);
                                        }

                                        DateTime now=DateTime.Today;
                                        e.birthDate=new DateTime(now.Year-age,1,1);
                                        e.orgUnitID = _orgUnit;
                                        employees.Add(e);
                                        additionalData.Add(thisData);
                                        li.Tag = e;
                                        recordCount++;
                                    
                                }
                                catch (Exception ex)
                                {
                                    if (li.SubItems.Count == 1)
                                        li.SubItems.Add("Error");
                                    else
                                        li.SubItems[1].Text = "Error";
                                    li.Tag = ex;
                                    li.BackColor = System.Drawing.Color.Red;
                                    skippedRecord++;
                                    
                                }
                                listView1.Items.Add(li);
                            }
                        }
                    }
                }
                foreach (ColumnHeader ch in listView1.Columns)
                    ch.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
                if(skippedRecord>0)
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage(string.Format("{0} records transfered\n{1} records skipped\n", recordCount, skippedRecord));
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error transfering records", ex);
            }
            finally
            {
                if (doc != null)
                    doc.Close();
            }
        }        
        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (costCenterPlaceHolder.GetAccountID() == -1)
            {
                MessageBox.ShowErrorMessage("Please select a cost center");
                return;
            }
            if (employees.Count == 0)
            {
                MessageBox.ShowErrorMessage("No emplyees selected");
                return;
            }
            progressBar1.Maximum = employees.Count;
            progressBar1.Step = 1;
            progressBar1.Value = 0;
            this.Enabled = false;
            PayrollPeriod period=PayrollClient.GetPayPeriod(dualCalendar.DateTime);
            try
            {
                
                int j=0;
                foreach (Employee emp in employees)
                {
                    try
                    {
                        emp.accountAsCost = true;
                        emp.costCenterID = costCenterPlaceHolder.GetAccountID();
                        emp.enrollmentDate = dualCalendar.DateTime;
                        emp.accountAsCost = checkAccountAsCost.Checked;
                        emp.status = EmployeeStatus.Enrolled;
                        Dictionary<int,SingleValueData> iddata= additionalData[j];
                        int[] fid = new int[iddata.Count];
                        SingleValueData[] data = new SingleValueData[iddata.Count];
                        int i = 0;
                        foreach (KeyValuePair<int, SingleValueData> kv in iddata)
                        {
                            fid[i] = kv.Key;
                            data[i] = kv.Value;
                            i++;
                        }
                        emp.id = iERPTransactionClient.RegisterEmployee(emp, null, true,null,period.id, null, null,data, fid);

                    }
                    catch (Exception ex)
                    {
                        foreach (ListViewItem li in listView1.Items)
                        {
                            if (li.Tag == emp)
                            {
                                li.Tag = ex;
                                li.BackColor = System.Drawing.Color.Red;
                                li.SubItems[1].Text = "Error";
                                break;
                            }
                        }
                    }
                    progressBar1.Value = progressBar1.Value + 1;
                    Application.DoEvents();
                    j++;
                }
                imported = true;
                buttonOk.Enabled = false;
            }
            finally
            {
                this.Enabled = true;
            }
        }
        private void buttonCencel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void labelControl1_Click(object sender, EventArgs e)
        {

        }
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Exception ex;;
            if (listView1.SelectedItems.Count == 0 || (ex = listView1.SelectedItems[0].Tag as Exception) == null)
                textBox1.Text = "";
            else
            {
                textBox1.Text = ex.Message;
            }
        }

       
    }
}