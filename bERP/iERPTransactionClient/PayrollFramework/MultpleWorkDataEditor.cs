﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using INTAPS.Payroll;
using INTAPS.Payroll.Client;

namespace BIZNET.iERP.Client
{
    public abstract partial class MultpleWorkDataEditor : Form
    {
        protected int _periodID;
        INTAPS.ObjectDiffCache<int, double[]> cache;
        bool _ignoreCellChanged = false;
        bool _ignoreNodeSelectionChange = false;
        bool _firstNodeSelectEvent = true;
        public MultpleWorkDataEditor(int periodID)
        {
            _ignoreNodeSelectionChange = true;
            InitializeComponent();
            this.Text = getEditorName();
            cache = new INTAPS.ObjectDiffCache<int, double[]>
            (
                empID => getData(empID)
                    , x => x == -1
                    , x => x == null
            );
            _periodID=periodID;
            OrgUnit u = new OrgUnit() { id = -1, Name = iERPTransactionClient.GetCompanyProfile().Name };
            
            loadOrgUnit(treeView.Nodes,u);
            treeView.SelectedNode = null;
            initDataTable();
            _ignoreNodeSelectionChange = false;
            htmlTool.setDelegate(generateHTML);
        }

        private string generateHTML(out string headers)
        {
            try
            {
                List<List<object>> data = new List<List<object>>();

                getTreeData(data, treeList.Nodes);

                string[] dc;
                getDataColumns(out dc);
                return INTAPS.Accounting.Client.AccountingClient.EvaluateEHTML("Payroll.workData",
                    
                    new object[] { "data",  new INTAPS.Evaluator.EData(data)
                    ,"headers",new INTAPS.Evaluator.EData(dc)
                    ,"subtitle",new INTAPS.Evaluator.EData(getEditorName())
                    }, out headers
                    );
            }
            catch (Exception ex)
            {
                headers = null;
                return string.Format("Error trying to generate html<br/>{0}", System.Web.HttpUtility.HtmlEncode(ex.Message));
            }
        }

        private void getTreeData(List<List<object>> data,TreeListNodes nodes)
        {
            foreach (TreeListNode n in nodes)
            {
                List<object> row = new List<object>();
                int colCount = treeList.Columns.Count;
                for (int i = 0; i < colCount; i++)
                    row.Add(n.GetValue(i));
                data.Add(row);
                getTreeData(data, n.Nodes);
     
            }
        }
        
        protected override void OnLoad(EventArgs e)
        {
        }
        void loadOrgUnit(TreeNodeCollection nodes, OrgUnit u)
        {
            TreeNode n = new TreeNode(u.Name);
            n.Tag = u;
            nodes.Add(n);
            foreach (OrgUnit ch in PayrollClient.GetOrgUnits(u.id))
            {
                loadOrgUnit(n.Nodes, ch);
                n.Expand();
            }
            
        }
        void loadEmployees(TreeListNodes nodes, int oid)
        {
            foreach (OrgUnit ch in PayrollClient.GetOrgUnits(oid))
            {
                object[] row = new object[treeList.Columns.Count];
                row[0] = ch.Name;
                TreeListNode n = nodes.Add(row);
                n.Tag = ch;
                loadEmployees(n.Nodes, ch.id);
                n.Expanded = true;
            }
            foreach (Employee e in PayrollClient.GetEmployees(oid, false))
            {

                object[] row;
                int i = 0;
                row = new object[treeList.Columns.Count];
                row[i++] = e.employeeID;
                row[i++] = e.employeeName;

                double[] vals;
                vals = getData(e.id);
                if (vals != null)
                    foreach (double d in vals)
                        row[i++] = d;
                TreeListNode n = nodes.Add(row);
                n.Tag = e;
                if (nodes.ParentNode != null && !nodes.ParentNode.Expanded)
                    nodes.ParentNode.Expanded = true;
                Application.DoEvents();
            }
        }
        void initDataTable()
        {

            List<TreeListColumn> cols = new List<TreeListColumn>();
            int i = 0;
            TreeListColumn col = new TreeListColumn();
            col.Caption = "ID";
            col.Visible = true;
            col.VisibleIndex = i++;
            col.SortIndex = 0;
            col.SortOrder = SortOrder.Descending;
            cols.Add(col);

            col = new TreeListColumn();
            col.Caption = "Name";
            col.Visible = true;
            col.VisibleIndex = i++;
            cols.Add(col);
            string[] desc;
            int coli = 0;
            foreach (string field in getDataColumns(out desc))
            {
                col = new TreeListColumn();
                col.Caption = desc[coli];
                col.FieldName = field;
                col.Visible = true;
                col.VisibleIndex = i++;
                col.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.Decimal;
                cols.Add(col);
                coli++;                
            }
            
            treeList.Columns.AddRange(cols.ToArray());
            treeList.CellValueChanged += treeList_CellValueChanged;
            treeList.Leave += treeList_Leave;
            treeList.FocusedNodeChanged += treeList_FocusedNodeChanged;
            
               
        }

       

        void treeList_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            if (e.Node!= editNode)
            {
                saveCurrentNode();
                editNode = null;
            }
        }

      
        void treeList_Leave(object sender, EventArgs e)
        {
            saveCurrentNode();
            editNode = null;
        }

        TreeListNode editNode = null;
        List<System.Threading.Thread> saveThreads = new List<System.Threading.Thread>();
        void saveCurrentNode()
        {
            if (editNode == null)
                return;
            Employee emp=editNode.Tag as Employee;
            if(emp==null)
                return;
            double[] vals=new double[treeList.Columns.Count-2];
            for(int i=0;i<vals.Length;i++)
            {
                object val=editNode.GetValue(i+2);
                vals[i] = val == null ? 0d : Convert.ToDouble(val);
            }
            System.Threading.Thread t=null;
            t=new System.Threading.Thread(delegate(object _emp)
                {
                    try
                    {
                        setData(emp.id, vals);
                    }
                    catch (Exception ex)
                    {
                        this.Invoke(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                            {
                                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
                                double[] oval = getData(((Employee)_emp).id);
                                for (int i = 0; i < vals.Length; i++)
                                {
                                    editNode.SetValue(i + 2, oval == null ? 0d : oval[i]);
                                }
                            }
                        ));
                    }
                    finally
                    {
                        //if (t != null)
                        //{
                        //    lock (saveThreads)
                        //    {
                        //        //saveThreads.Remove(t);
                        //        if (saveThreads.Count == 0)
                        //        {
                        //            this.Invoke(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                        //            {
                        //                //this.Text = getEditorName();
                        //            }
                        //            ));
                        //        }
                        //    }
                        //}
                    }
                });
            t.Start(emp);
            lock (saveThreads)
            {
                saveThreads.Add(t);
                //if (saveThreads.Count == 1)
                //    this.Text = getEditorName() + " (Saving..)";
            }
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            finishSaving();
            base.OnClosing(e);
        }
        void treeList_CellValueChanged(object sender, DevExpress.XtraTreeList.CellValueChangedEventArgs e)
        {
            if (e.Node.Tag is OrgUnit)
            {
                if(_ignoreCellChanged)
                    return;
                _ignoreCellChanged = true;
                e.Node.SetValue(e.Column, 0);
                _ignoreCellChanged = false;
                return;
            }
            if (editNode != e.Node)
            {
                saveCurrentNode();
                editNode = e.Node;
            }
        }

        public abstract string[] getDataColumns(out string[] desc);
        public abstract double[] getData(int empID);
        public abstract void setData(int empID, double[] vals);
        public virtual string getEditorName()
        {
            return "Payroll Data Editor";
        }
        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if(_firstNodeSelectEvent)
            {
                _firstNodeSelectEvent = false;
                return;
            }
            if (_ignoreNodeSelectionChange)
                return;
            treeList.Nodes.Clear();
            this.Text = getEditorName() + " Loading data";
            treeList.Nodes.Clear();
            Application.DoEvents();
            this.Enabled = false;
            loadEmployees(treeList.Nodes, ((OrgUnit)e.Node.Tag).id);
            this.Enabled = true;
            this.Text = getEditorName();
        }

        void finishSaving()
        {
            lock (saveThreads)
            {
                bool isAlive=false;
                 foreach (System.Threading.Thread t in saveThreads)
                 {
                     if(t.IsAlive)
                     {
                         isAlive=true;
                         break;
                     }
                 }
                if(!isAlive)
                    return;
                WaitSplashScreen.showWaitSplash(this, "Please wait until all the changes are saved.",
                    new INTAPS.UI.HTML.ProcessParameterless(
                delegate()
                {
                    foreach (System.Threading.Thread t in saveThreads)
                    {
                        if (t.IsAlive)
                            t.Join();
                    }
                }
               ));
            }
            saveThreads.Clear();
        }
        private void treeView_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            finishSaving();
        }

    }
    
    public class OverTimeDataEditor : MultpleWorkDataEditor
    {
        PayrollComponentFormula overTimeFormula = null;
        public OverTimeDataEditor(int pid)
            : base(pid)
        {
            int fid = (int)iERPTransactionClient.GetSystemParamter("OverTimePayrollFormulaID");
            overTimeFormula = PayrollClient.GetPCFormula(fid);

        }
        public override string[] getDataColumns(out string[] desc)
        {
            desc=new string[] { "Off Hours","Late Hours","Weekend Hours","Holiday Hours"};
            return new string[] { "offHours", "lateHours", "weekendHours", "holidayHours" };
        }

        public override double[] getData(int empID)
        {
            if (overTimeFormula == null)
                return null;
            PayrollComponentData[] ret = PayrollClient.GetPCData(overTimeFormula.PCDID, overTimeFormula.ID, AppliesToObjectType.Employee, empID, base._periodID, false);
            int formulaID = overTimeFormula.ID;
            if (empID != -1)
            {
                if (ret.Length > 0)
                {
                    object overTimeData = PayrollClient.GetPCAdditionalData(ret[0].ID);
                    if (overTimeData != null)
                    {
                        OverTimeData data = (OverTimeData)overTimeData;
                        return new double[] { data.offHours, data.lateHours, data.weekendHours, data.holidayHours };
                    }
                }
            }
            return null;
        }
        public override string getEditorName()
        {
            return "Overtime Data";
        }
        public override void setData(int empID, double[] vals)
        {
            PayrollComponentData[] ret = PayrollClient.GetPCData(overTimeFormula.PCDID, overTimeFormula.ID, AppliesToObjectType.Employee, empID, base._periodID, false);
            PayrollClient.CreatePCData(
                new PayrollComponentData()
                {
                    ID=ret!=null && ret.Length>0?ret[0].ID:-1,
                    at=new AppliesToEmployees(empID),
                    DataDate=DateTime.Now,
                    FormulaID=overTimeFormula.ID,
                    PCDID=overTimeFormula.PCDID,
                    periodRange=new INTAPS.Accounting.PayPeriodRange(_periodID,_periodID)
                },
                new OverTimeData()
                {
                    offHours=vals[0],
                    lateHours=vals[1],
                    weekendHours=vals[2],
                    holidayHours=vals[3]
                }
                );
        }
    }
    public class RegularTimeDataEditor : MultpleWorkDataEditor
    {
        PayrollComponentFormula regularTimeFormula = null;
        public RegularTimeDataEditor(int pid)
            : base(pid)
        {
            int fid = (int)iERPTransactionClient.GetSystemParamter("RegularTimePayrollFormulaID");
            regularTimeFormula = PayrollClient.GetPCFormula(fid);

        }
        public override string[] getDataColumns(out string[] desc)
        {
            desc = new string[] { "Working Days", "Working Hours", "Partial Working Hours" };
            return new string[] { "workingDays", "workingHours", "partialWorkingHours" };
        }

        public override double[] getData(int empID)
        {
            if (regularTimeFormula == null)
                return null;
            PayrollComponentData[] ret = PayrollClient.GetPCData(regularTimeFormula.PCDID, regularTimeFormula.ID, AppliesToObjectType.Employee, empID, base._periodID, false);
            int formulaID = regularTimeFormula.ID;
            if (empID != -1)
            {
                if (ret.Length > 0)
                {
                    object overTimeData = PayrollClient.GetPCAdditionalData(ret[0].ID);
                    if (overTimeData != null)
                    {
                        Data_RegularTime data = (Data_RegularTime)overTimeData;
                        return new double[] { data.WorkingDays, data.WorkingHours, data.PartialWorkingHour};
                    }
                }
            }
            return null;
        }
        public override string getEditorName()
        {
            return "Regular Time Data";
        }
        public override void setData(int empID, double[] vals)
        {
            PayrollComponentData[] ret = PayrollClient.GetPCData(regularTimeFormula.PCDID, regularTimeFormula.ID, AppliesToObjectType.Employee, empID, base._periodID, false);
            PayrollClient.CreatePCData(
                new PayrollComponentData()
                {
                    ID = ret != null && ret.Length > 0 ? ret[0].ID : -1,
                    at = new AppliesToEmployees(empID),
                    DataDate = DateTime.Now,
                    FormulaID = regularTimeFormula.ID,
                    PCDID = regularTimeFormula.PCDID,
                    periodRange = new INTAPS.Accounting.PayPeriodRange(_periodID, _periodID)
                },
                new Data_RegularTime()
                {
                    WorkingDays=vals[0],
                    WorkingHours=vals[1],
                    PartialWorkingHour=vals[2],
                }
                );
        }
    }

    public class SpecialBenefitDeductionDataEditor : MultpleWorkDataEditor
    {
        List<string> names = null;
        List<int> ids = null;
        int pcdID = -1;
        public SpecialBenefitDeductionDataEditor(int pid)
            : base(pid)
        {
            
        }
        public override string[] getDataColumns(out string[] desc)
        {
            if(names==null)
            {
                names = new List<string>();
                ids = new List<int>();

                pcdID = (int)iERPTransactionClient.GetSystemParamter("SingleValuePayrollComponentID");
                if (pcdID != 0)
                {
                    PayrollComponentFormula[] singleValueFormula = PayrollClient.GetPCFormulae(pcdID);
                    if (singleValueFormula.Length == 0)
                    {
                        desc = null;
                        return null;
                    }

                    foreach (PayrollComponentFormula formula in singleValueFormula)
                    {
                        names.Add(formula.Name);
                        ids.Add(formula.ID);
                    }

                }
            }
            desc = names.ToArray();
            return names.ToArray();
        }

        public override double[] getData(int empID)
        {
            if (names.Count==0)
                return null;
            double[] ret = new double[names.Count];
            for (int i = 0; i < names.Count; i++)
            {
                PayrollComponentData[] retPCD = PayrollClient.GetPCData(pcdID, ids[i], AppliesToObjectType.Employee, empID, base._periodID, false);
                if (empID != -1)
                {
                    if (retPCD.Length > 0)
                    {
                        object overTimeData = PayrollClient.GetPCAdditionalData(retPCD[0].ID);
                        if (overTimeData != null)
                        {
                            ret[i] = ((SingleValueData)overTimeData).value;
                            continue;
                        }
                    }
                }
                ret[i] = 0;
            }
            return ret;
        }
        public override string getEditorName()
        {
            return "Special Benefit/Deductions";
        }
        public override void setData(int empID, double[] vals)
        {
            for (int i = 0; i < names.Count; i++)
            {
                PayrollComponentData[] ret = PayrollClient.GetPCData(pcdID, ids[i], AppliesToObjectType.Employee, empID, base._periodID, false);
                PayrollClient.CreatePCData(
                new PayrollComponentData()
                {
                    ID = ret != null && ret.Length > 0 ? ret[0].ID : -1,
                    at = new AppliesToEmployees(empID),
                    DataDate = DateTime.Now,
                    FormulaID=ids[i],
                    PCDID = pcdID,
                    periodRange = new INTAPS.Accounting.PayPeriodRange(_periodID, _periodID)
                },
                new SingleValueData()
                {
                    value=vals[i]
                }
                );
            }
        }
    }
    public class PrermanentBenefitDeductionDataEditor : MultpleWorkDataEditor
    {
        List<string> names = null;
        List<int> ids = null;
        int pcdID = -1;
        
        public PrermanentBenefitDeductionDataEditor(int pid)
            : base(pid)
        {

        }
        public override string[] getDataColumns(out string[] desc)
        {
            if (names == null)
            {
                names = new List<string>();
                ids = new List<int>();

                pcdID = (int)iERPTransactionClient.GetSystemParamter("permanentBenefitComponentID");
                if (pcdID != 0)
                {
                    PayrollComponentFormula[] singleValueFormula = PayrollClient.GetPCFormulae(pcdID);
                    if (singleValueFormula.Length == 0)
                    {
                        desc = null;
                        return null;
                    }

                    foreach (PayrollComponentFormula formula in singleValueFormula)
                    {
                        names.Add(formula.Name);
                        ids.Add(formula.ID);
                    }

                }
            }
            desc = names.ToArray();
            return names.ToArray();
        }

        public override double[] getData(int empID)
        {
            if (names.Count == 0)
                return null;
            double[] ret = new double[names.Count];
            for (int i = 0; i < names.Count; i++)
            {
                PayrollComponentData[] retPCD = PayrollClient.GetPCData(pcdID, ids[i], AppliesToObjectType.Employee, empID, base._periodID, false);
                if (empID != -1)
                {
                    if (retPCD.Length > 0)
                    {
                        object overTimeData = PayrollClient.GetPCAdditionalData(retPCD[0].ID);
                        if (overTimeData != null)
                        {
                            ret[i] = ((SingleValueData)overTimeData).value;
                            continue;
                        }
                    }
                }
                ret[i] = 0;
            }
            return ret;
        }
        public override string getEditorName()
        {
            return "Permanent Benefit/Deductions";
        }
        public override void setData(int empID, double[] vals)
        {
            for (int i = 0; i < names.Count; i++)
            {
                PayrollComponentData[] ret = PayrollClient.GetPCData(pcdID, ids[i], AppliesToObjectType.Employee, empID, base._periodID, false);
                iERPTransactionClient.setPermanentWorkingData(empID, _periodID, ids[i],
                new PayrollComponentData()
                {
                    ID = ret != null && ret.Length > 0 ? ret[0].ID : -1,
                    at = new AppliesToEmployees(empID),
                    DataDate = DateTime.Now,
                    FormulaID = ids[i],
                    PCDID = pcdID,
                    periodRange = new INTAPS.Accounting.PayPeriodRange(_periodID, _periodID)
                }
                , new SingleValueData() { value=vals[i]}
                );
            }
        }
    }

}
