﻿namespace BIZNET.iERP.Client
{
    partial class PayPayroll2 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.voucher = new BIZNET.iERP.Client.DocumentSerialPlaceHolder();
            this.cmbBankAccount = new BIZNET.iERP.Client.BankAccountPlaceholder();
            this.cmbCasher = new BIZNET.iERP.Client.CashAccountPlaceholder();
            this.paymentTypeSelector = new BIZNET.iERP.Client.PaymentTypeSelector();
            this.datePayment = new BIZNET.iERP.Client.BNDualCalendar();
            this.txtServiceCharge = new DevExpress.XtraEditors.TextEdit();
            this.labelBankBalance = new DevExpress.XtraEditors.LabelControl();
            this.labelCashBalance = new DevExpress.XtraEditors.LabelControl();
            this.txtNote = new DevExpress.XtraEditors.MemoEdit();
            this.txtReference = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlNote = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlBankBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlCashBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlSlipRef = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlServiceCharge = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlCashAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlBankAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.validationPenality = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.tabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.labelPeriod = new DevExpress.XtraEditors.LabelControl();
            this.comboPeriod = new BIZNET.iERP.Client.PeriodSelector();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.tabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelTotal = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.buttonPrev = new DevExpress.XtraEditors.SimpleButton();
            this.buttonNext = new DevExpress.XtraEditors.SimpleButton();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.chkSelectAll = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBankAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCasher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentTypeSelector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceCharge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSlipRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlServiceCharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationPenality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectAll.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.voucher);
            this.layoutControl1.Controls.Add(this.cmbBankAccount);
            this.layoutControl1.Controls.Add(this.cmbCasher);
            this.layoutControl1.Controls.Add(this.paymentTypeSelector);
            this.layoutControl1.Controls.Add(this.datePayment);
            this.layoutControl1.Controls.Add(this.txtServiceCharge);
            this.layoutControl1.Controls.Add(this.labelBankBalance);
            this.layoutControl1.Controls.Add(this.labelCashBalance);
            this.layoutControl1.Controls.Add(this.txtNote);
            this.layoutControl1.Controls.Add(this.txtReference);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(136, 199, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(731, 418);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // voucher
            // 
            this.voucher.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.voucher.Appearance.Options.UseBackColor = true;
            this.voucher.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.voucher.Location = new System.Drawing.Point(7, 23);
            this.voucher.Name = "voucher";
            this.voucher.Size = new System.Drawing.Size(356, 90);
            this.voucher.TabIndex = 24;
            // 
            // cmbBankAccount
            // 
            this.cmbBankAccount.Location = new System.Drawing.Point(153, 180);
            this.cmbBankAccount.Name = "cmbBankAccount";
            this.cmbBankAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbBankAccount.Properties.ImmediatePopup = true;
            this.cmbBankAccount.Size = new System.Drawing.Size(378, 20);
            this.cmbBankAccount.StyleController = this.layoutControl1;
            this.cmbBankAccount.TabIndex = 23;
            // 
            // cmbCasher
            // 
            this.cmbCasher.Location = new System.Drawing.Point(153, 150);
            this.cmbCasher.Name = "cmbCasher";
            this.cmbCasher.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCasher.Properties.ImmediatePopup = true;
            this.cmbCasher.Size = new System.Drawing.Size(378, 20);
            this.cmbCasher.StyleController = this.layoutControl1;
            this.cmbCasher.TabIndex = 22;
            // 
            // paymentTypeSelector
            // 
            this.paymentTypeSelector.Include_BankTransferFromAccount = true;
            this.paymentTypeSelector.Include_BankTransferFromCash = true;
            this.paymentTypeSelector.Include_Cash = true;
            this.paymentTypeSelector.Include_Check = true;
            this.paymentTypeSelector.Include_CpoFromBank = true;
            this.paymentTypeSelector.Include_CpoFromCash = true;
            this.paymentTypeSelector.Include_Credit = false;
            this.paymentTypeSelector.Location = new System.Drawing.Point(153, 120);
            this.paymentTypeSelector.Name = "paymentTypeSelector";
            this.paymentTypeSelector.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.paymentTypeSelector.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.paymentTypeSelector.Size = new System.Drawing.Size(568, 20);
            this.paymentTypeSelector.StyleController = this.layoutControl1;
            this.paymentTypeSelector.TabIndex = 21;
            // 
            // datePayment
            // 
            this.datePayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.datePayment.Location = new System.Drawing.Point(370, 26);
            this.datePayment.Name = "datePayment";
            this.datePayment.ShowEthiopian = true;
            this.datePayment.ShowGregorian = true;
            this.datePayment.ShowTime = true;
            this.datePayment.Size = new System.Drawing.Size(351, 63);
            this.datePayment.TabIndex = 19;
            this.datePayment.VerticalLayout = true;
            // 
            // txtServiceCharge
            // 
            this.txtServiceCharge.Location = new System.Drawing.Point(153, 240);
            this.txtServiceCharge.Name = "txtServiceCharge";
            this.txtServiceCharge.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtServiceCharge.Properties.Mask.EditMask = "#,########0.00;";
            this.txtServiceCharge.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtServiceCharge.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtServiceCharge.Size = new System.Drawing.Size(568, 20);
            this.txtServiceCharge.StyleController = this.layoutControl1;
            this.txtServiceCharge.TabIndex = 12;
            // 
            // labelBankBalance
            // 
            this.labelBankBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBankBalance.Location = new System.Drawing.Point(538, 177);
            this.labelBankBalance.Name = "labelBankBalance";
            this.labelBankBalance.Size = new System.Drawing.Size(186, 26);
            this.labelBankBalance.StyleController = this.layoutControl1;
            this.labelBankBalance.TabIndex = 8;
            this.labelBankBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // labelCashBalance
            // 
            this.labelCashBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCashBalance.Location = new System.Drawing.Point(538, 147);
            this.labelCashBalance.Name = "labelCashBalance";
            this.labelCashBalance.Size = new System.Drawing.Size(186, 26);
            this.labelCashBalance.StyleController = this.layoutControl1;
            this.labelCashBalance.TabIndex = 7;
            this.labelCashBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(153, 270);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(568, 138);
            this.txtNote.StyleController = this.layoutControl1;
            this.txtNote.TabIndex = 6;
            // 
            // txtReference
            // 
            this.txtReference.Location = new System.Drawing.Point(153, 210);
            this.txtReference.Name = "txtReference";
            this.txtReference.Properties.Mask.EditMask = "\\w.*";
            this.txtReference.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtReference.Size = new System.Drawing.Size(568, 20);
            this.txtReference.StyleController = this.layoutControl1;
            this.txtReference.TabIndex = 2;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "Slip Reference cannot be empty";
            this.validationPenality.SetValidationRule(this.txtReference, conditionValidationRule2);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlNote,
            this.layoutControlBankBalance,
            this.layoutControlCashBalance,
            this.layoutControlSlipRef,
            this.layoutControlServiceCharge,
            this.layoutControlItem1,
            this.layoutControlItem4,
            this.layoutControlCashAccount,
            this.layoutControlBankAccount,
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(731, 418);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlNote
            // 
            this.layoutControlNote.Control = this.txtNote;
            this.layoutControlNote.CustomizationFormText = "Note:";
            this.layoutControlNote.Location = new System.Drawing.Point(0, 260);
            this.layoutControlNote.MinSize = new System.Drawing.Size(193, 26);
            this.layoutControlNote.Name = "layoutControlNote";
            this.layoutControlNote.Size = new System.Drawing.Size(721, 148);
            this.layoutControlNote.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlNote.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlNote.Text = "Note:";
            this.layoutControlNote.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlBankBalance
            // 
            this.layoutControlBankBalance.Control = this.labelBankBalance;
            this.layoutControlBankBalance.CustomizationFormText = "layoutControlItem8";
            this.layoutControlBankBalance.Location = new System.Drawing.Point(531, 170);
            this.layoutControlBankBalance.MaxSize = new System.Drawing.Size(190, 30);
            this.layoutControlBankBalance.MinSize = new System.Drawing.Size(190, 30);
            this.layoutControlBankBalance.Name = "layoutControlBankBalance";
            this.layoutControlBankBalance.Size = new System.Drawing.Size(190, 30);
            this.layoutControlBankBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlBankBalance.Text = "layoutControlItem8";
            this.layoutControlBankBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlBankBalance.TextToControlDistance = 0;
            this.layoutControlBankBalance.TextVisible = false;
            // 
            // layoutControlCashBalance
            // 
            this.layoutControlCashBalance.Control = this.labelCashBalance;
            this.layoutControlCashBalance.CustomizationFormText = "layoutControlCashBalance";
            this.layoutControlCashBalance.Location = new System.Drawing.Point(531, 140);
            this.layoutControlCashBalance.MaxSize = new System.Drawing.Size(190, 30);
            this.layoutControlCashBalance.MinSize = new System.Drawing.Size(190, 30);
            this.layoutControlCashBalance.Name = "layoutControlCashBalance";
            this.layoutControlCashBalance.Size = new System.Drawing.Size(190, 30);
            this.layoutControlCashBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlCashBalance.Text = "layoutControlCashBalance";
            this.layoutControlCashBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlCashBalance.TextToControlDistance = 0;
            this.layoutControlCashBalance.TextVisible = false;
            // 
            // layoutControlSlipRef
            // 
            this.layoutControlSlipRef.Control = this.txtReference;
            this.layoutControlSlipRef.CustomizationFormText = "Slip Reference:";
            this.layoutControlSlipRef.Location = new System.Drawing.Point(0, 200);
            this.layoutControlSlipRef.Name = "layoutControlSlipRef";
            this.layoutControlSlipRef.Size = new System.Drawing.Size(721, 30);
            this.layoutControlSlipRef.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlSlipRef.Text = "Slip Reference:";
            this.layoutControlSlipRef.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlServiceCharge
            // 
            this.layoutControlServiceCharge.Control = this.txtServiceCharge;
            this.layoutControlServiceCharge.CustomizationFormText = "Service Charge:";
            this.layoutControlServiceCharge.Location = new System.Drawing.Point(0, 230);
            this.layoutControlServiceCharge.Name = "layoutControlServiceCharge";
            this.layoutControlServiceCharge.Size = new System.Drawing.Size(721, 30);
            this.layoutControlServiceCharge.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlServiceCharge.Text = "Service Charge:";
            this.layoutControlServiceCharge.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.datePayment;
            this.layoutControlItem1.CustomizationFormText = "Date:";
            this.layoutControlItem1.Location = new System.Drawing.Point(360, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(150, 46);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(361, 110);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Text = "Date:";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.paymentTypeSelector;
            this.layoutControlItem4.CustomizationFormText = "Payment Method:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 110);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(721, 30);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem4.Text = "Payment Method:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlCashAccount
            // 
            this.layoutControlCashAccount.Control = this.cmbCasher;
            this.layoutControlCashAccount.CustomizationFormText = "Payment from Cash Account:";
            this.layoutControlCashAccount.Location = new System.Drawing.Point(0, 140);
            this.layoutControlCashAccount.Name = "layoutControlCashAccount";
            this.layoutControlCashAccount.Size = new System.Drawing.Size(531, 30);
            this.layoutControlCashAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlCashAccount.Text = "Payment from Cash Account:";
            this.layoutControlCashAccount.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlBankAccount
            // 
            this.layoutControlBankAccount.Control = this.cmbBankAccount;
            this.layoutControlBankAccount.CustomizationFormText = "Payment from Bank Account:";
            this.layoutControlBankAccount.Location = new System.Drawing.Point(0, 170);
            this.layoutControlBankAccount.Name = "layoutControlBankAccount";
            this.layoutControlBankAccount.Size = new System.Drawing.Size(531, 30);
            this.layoutControlBankAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlBankAccount.Text = "Payment from Bank Account:";
            this.layoutControlBankAccount.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.voucher;
            this.layoutControlItem2.CustomizationFormText = "Reference";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(360, 110);
            this.layoutControlItem2.Text = "Reference";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(140, 13);
            // 
            // validationPenality
            // 
            this.validationPenality.ValidateHiddenControls = false;
            // 
            // tabControl
            // 
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedTabPage = this.tabPage1;
            this.tabControl.Size = new System.Drawing.Size(737, 446);
            this.tabControl.TabIndex = 7;
            this.tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPage1,
            this.tabPage2});
            this.tabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.tabControl_SelectedPageChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.labelPeriod);
            this.tabPage1.Controls.Add(this.comboPeriod);
            this.tabPage1.Controls.Add(this.gridControl);
            this.tabPage1.Controls.Add(this.labelControl1);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(6);
            this.tabPage1.Size = new System.Drawing.Size(731, 418);
            this.tabPage1.Text = "Employees";
            // 
            // labelPeriod
            // 
            this.labelPeriod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPeriod.Appearance.BackColor = System.Drawing.Color.LightSteelBlue;
            this.labelPeriod.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPeriod.Location = new System.Drawing.Point(521, 21);
            this.labelPeriod.Name = "labelPeriod";
            this.labelPeriod.Size = new System.Drawing.Size(36, 13);
            this.labelPeriod.TabIndex = 19;
            this.labelPeriod.Text = "Period";
            // 
            // comboPeriod
            // 
            this.comboPeriod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboPeriod.Location = new System.Drawing.Point(570, 17);
            this.comboPeriod.Name = "comboPeriod";
            this.comboPeriod.PeriodType = "AP_Payroll";
            this.comboPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboPeriod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboPeriod.Size = new System.Drawing.Size(141, 20);
            this.comboPeriod.TabIndex = 18;
            this.comboPeriod.SelectedIndexChanged += new System.EventHandler(this.comboPeriod_SelectedIndexChanged);
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(6, 63);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(719, 349);
            this.gridControl.TabIndex = 15;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView_CellValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.LightSteelBlue;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(112)))));
            this.labelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl1.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.labelControl1.Location = new System.Drawing.Point(6, 6);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Padding = new System.Windows.Forms.Padding(3);
            this.labelControl1.Size = new System.Drawing.Size(719, 57);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 14;
            this.labelControl1.Text = "Employees";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.layoutControl1);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(731, 418);
            this.tabPage2.Text = "Payment";
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.chkSelectAll);
            this.panelControl1.Controls.Add(this.labelTotal);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.buttonPrev);
            this.panelControl1.Controls.Add(this.buttonNext);
            this.panelControl1.Controls.Add(this.buttonCancel);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 446);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(737, 63);
            this.panelControl1.TabIndex = 8;
            // 
            // labelTotal
            // 
            this.labelTotal.Location = new System.Drawing.Point(64, 7);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(20, 13);
            this.labelTotal.TabIndex = 27;
            this.labelTotal.Text = "#,#";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(7, 7);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(32, 13);
            this.labelControl2.TabIndex = 26;
            this.labelControl2.Text = "Total:";
            // 
            // buttonPrev
            // 
            this.buttonPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrev.Location = new System.Drawing.Point(488, 29);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(80, 23);
            this.buttonPrev.TabIndex = 25;
            this.buttonPrev.Text = "<<Previous";
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNext.Location = new System.Drawing.Point(574, 29);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(80, 23);
            this.buttonNext.TabIndex = 24;
            this.buttonNext.Text = "Next>>";
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(666, 29);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(59, 23);
            this.buttonCancel.TabIndex = 23;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click_1);
            // 
            // chkSelectAll
            // 
            this.chkSelectAll.EditValue = true;
            this.chkSelectAll.Location = new System.Drawing.Point(5, 33);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Properties.Caption = "Select All";
            this.chkSelectAll.Size = new System.Drawing.Size(97, 19);
            this.chkSelectAll.TabIndex = 28;
            this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            // 
            // PayPayroll2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(737, 509);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "PayPayroll2";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Payroll Payment Form";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbBankAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCasher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paymentTypeSelector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceCharge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSlipRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlServiceCharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationPenality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectAll.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.MemoEdit txtNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlNote;
        private DevExpress.XtraEditors.LabelControl labelCashBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCashBalance;
        private DevExpress.XtraEditors.LabelControl labelBankBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlBankBalance;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validationPenality;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraEditors.TextEdit txtServiceCharge;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlServiceCharge;
        private BIZNET.iERP.Client.BNDualCalendar datePayment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private PaymentTypeSelector paymentTypeSelector;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private CashAccountPlaceholder cmbCasher;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCashAccount;
        private BankAccountPlaceholder cmbBankAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlBankAccount;
        private DevExpress.XtraEditors.TextEdit txtReference;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlSlipRef;
        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraTab.XtraTabPage tabPage2;
        private DevExpress.XtraTab.XtraTabPage tabPage1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraEditors.SimpleButton buttonPrev;
        private DevExpress.XtraEditors.SimpleButton buttonNext;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DocumentSerialPlaceHolder voucher;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.LabelControl labelTotal;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelPeriod;
        protected PeriodSelector comboPeriod;
        private DevExpress.XtraEditors.CheckEdit chkSelectAll;
    }
}