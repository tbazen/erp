﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraReports.UI;

namespace BIZNET.iERP.Client
{
    public partial class PayPayroll2 : XtraForm
    {
        PayrollPaymentSetDocument m_set = null;
        private bool newDocument=true;
        private IAccountingClient _client;
        private ActivationParameter _activation;
        private PaymentMethodAndBalanceController _paymentController;
        DataTable _employees=null;
        int _periodID;
        int _payrollSetDocumentID = -1;
        DevExpress.XtraSplashScreen.SplashScreenManager waitScreen;

        public PayPayroll2(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            waitScreen = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::BIZNET.iERP.Client.WaitForm1), true, true);
            voucher.setKeys(typeof(PayrollPaymentSetDocument), "voucher");
            intializeGrid();
            _client = client;
            _activation = activation;
            _paymentController = new PaymentMethodAndBalanceController(paymentTypeSelector, layoutControlBankAccount, cmbBankAccount, layoutControlBankBalance, labelBankBalance, layoutControlCashAccount, cmbCasher, layoutControlCashBalance, labelCashBalance, layoutControlServiceCharge, layoutControlSlipRef, datePayment, _activation);

            txtReference.LostFocus += Control_LostFocus;
            
            populateList();
            if (activation.periodID < 1)
            {
                labelPeriod.Visible = comboPeriod.Visible = true;
            }
            else
            {
                labelPeriod.Visible = comboPeriod.Visible = false;
                setPeriod(PayrollClient.GetPayPeriod(activation.periodID));
            }
            if (_periodID < 1)
            {
                comboPeriod.setByDate(DateTime.Now);
                comboPeriod_SelectedIndexChanged(null, null);
            }
            else
                populateList();

            updatePrevNextButton();
        }

        private void populateList()
        {
            _employees.Rows.Clear();
            if (_periodID > 0)
            {
                if (_activation.parentAccountDocument is PayrollSetDocument)
                {
                    PayrollSetDocument pset = (PayrollSetDocument)_activation.parentAccountDocument;
                    int[] employeIDs = new int[pset.payrolls.Length];
                    for (int j = 0; j < employeIDs.Length; j++) employeIDs[j] = pset.payrolls[j].employee.id;
                    double[] payroll;
                    double[] paid = PayrollClient.getPaymetAmounts(_periodID, employeIDs, out payroll);
                    int i = 0;
                    foreach (PayrollDocument pdoc in pset.payrolls)
                    {
                        double unpaid = payroll[i] - paid[i];
                        if (AccountBase.AmountGreater(unpaid, 0))
                        {
                            //Employee e = PayrollClient.GetEmployee(pdoc.employee.id);
                            Employee e = pdoc.employee;
                            addToTable(e, unpaid, unpaid);
                        }
                        i++;
                    }
                    _payrollSetDocumentID = pset.AccountDocumentID;
                }
                else if (_activation.employeeID > 0)
                {
                    double[] payroll;
                    double[] paid = PayrollClient.getPaymetAmounts(_periodID, new int[] { _activation.employeeID }, out payroll);
                    double unpaid = payroll[0] - paid[0];
                    if (AccountBase.AmountGreater(unpaid, 0))
                    {
                        Employee e = PayrollClient.GetEmployee(_activation.employeeID);
                        addToTable(e, unpaid, unpaid);
                    }
                    _payrollSetDocumentID = PayrollClient.getEmployeePayrollDocumentID(_activation.employeeID, _periodID);
                }
            }
            updateTotal();
        }

        void updateTotal()
        {
            double total = 0;
            foreach (DataRow row in _employees.Rows)
            {
                if((bool)row["Select"])
                    total+= (double)row["Payment"];
            }
            labelTotal.Text = total.ToString("#,#0.00");
        }
       
        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validationPenality.Validate(control);
        }

        internal void LoadData(PayrollPaymentSetDocument payrollPayment)
        {
            m_set = payrollPayment;
            if (payrollPayment == null)
            {
                ResetControls();
            }
            else
            {
                _paymentController.IgnoreEvents();
                newDocument = false;
                try
                {
                    _paymentController.PaymentTypeSelector.PaymentMethod = m_set.paymentMethod;
                    PopulateControlsForUpdate(payrollPayment);
                }
                finally
                {
                    _paymentController.AcceptEvents();
                }
            }
        }
        private void ResetControls()
        {
            datePayment.DateTime = DateTime.Now;
            txtReference.Text = "";
            txtNote.Text = "";
        }
        private void PopulateControlsForUpdate(PayrollPaymentSetDocument payrollPayment)
        {
            labelPeriod.Visible = comboPeriod.Visible = false;
            voucher.setReference(payrollPayment.AccountDocumentID, payrollPayment.voucher);
            setPeriod(payrollPayment.payPeriod);
            datePayment.DateTime = payrollPayment.DocumentDate;
            _paymentController.assetAccountID = payrollPayment.assetAccountID;
            _payrollSetDocumentID = payrollPayment.payrollDocumentID;
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(payrollPayment.paymentMethod))
                txtServiceCharge.Text = payrollPayment.serviceChargeAmount.ToString("N");
            if (PaymentMethodHelper.IsPaymentMethodWithReference(payrollPayment.paymentMethod))
            {
                if (PaymentMethodHelper.IsPaymentMethodCPO(payrollPayment.paymentMethod))
                    txtReference.Text = payrollPayment.cpoNumber;
                else if (PaymentMethodHelper.IsPaymentMethodTransfer(payrollPayment.paymentMethod))
                    txtReference.Text = payrollPayment.transferReference;
                else 
                    txtReference.Text = payrollPayment.checkNumber;
            }
            
            txtNote.Text = payrollPayment.ShortDescription;
            int[] es = new int[payrollPayment.employee.Length];
            for (int i = 0; i < payrollPayment.employee.Length; i++)
                es[i] = payrollPayment.employee[i].id;
            double [] payroll;
            double[] paid = PayrollClient.getPaymetAmounts(payrollPayment.payPeriod.id, es, out payroll);
            for (int i = 0; i < payrollPayment.employee.Length; i++)
                addToTable(payrollPayment.employee[i], payroll[i] - paid[i] + payrollPayment.amount[i], payrollPayment.amount[i]);
            updateTotal();
        }

        private void setPeriod(PayrollPeriod payrollPeriod)
        {
            this.Text = "Payroll Payment for Period " + payrollPeriod.name;
            _periodID = payrollPeriod.id;
        }

        private void save()
        {
            try
            {
                if (validationPenality.Validate())
                {
                    int docID = m_set == null ? -1 : m_set.AccountDocumentID;

                    if (_payrollSetDocumentID < 1)
                    {
                        MessageBox.ShowErrorMessage("Payroll not selected");
                        return;
                    }
                    PayrollPaymentSetDocument set = new PayrollPaymentSetDocument();
                    set.payrollDocumentID = _payrollSetDocumentID;
                    set.voucher = voucher.getReference();
                    if (set.voucher == null)
                    {
                        MessageBox.ShowErrorMessage("Please enter reference");
                        return;
                    }
                    set.AccountDocumentID = docID;
                    set.DocumentDate = datePayment.DateTime;
                    set.paymentMethod = _paymentController.PaymentTypeSelector.PaymentMethod;
                    set.assetAccountID = _paymentController.assetAccountID;
                    if (PaymentMethodHelper.IsPaymentMethodWithReference(_paymentController.PaymentTypeSelector.PaymentMethod))
                    {
                        if (PaymentMethodHelper.IsPaymentMethodCPO(_paymentController.PaymentTypeSelector.PaymentMethod))
                            set.cpoNumber = txtReference.Text;
                        else if (PaymentMethodHelper.IsPaymentMethodTransfer(_paymentController.PaymentTypeSelector.PaymentMethod))
                            set.transferReference = txtReference.Text;
                        else
                            set.checkNumber = txtReference.Text;

                    }
                    set.ShortDescription = txtNote.Text;
                    if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(_paymentController.PaymentTypeSelector.PaymentMethod))
                    {
                        set.serviceChargePayer = ServiceChargePayer.Company;
                        double serviceChargeAmount = txtServiceCharge.Text == "" ? 0 : double.Parse(txtServiceCharge.Text);
                        set.serviceChargeAmount = serviceChargeAmount;
                    }

                    List<int> employeeID=new List<int>();
                    List<double> payment=new List<double>();
                    waitScreen.ShowWaitForm();
                    foreach (DataRow row in _employees.Rows)
                    {
                        waitScreen.SetWaitFormDescription("Processing Payment: " + (string)row["Name"]);

                        int ID = (int)row["ID"];
                        bool select = (bool)row["Select"];
                        if (!select)
                            continue;
                        double unpaid = (double)row["Unpaid"];
                        double paid = (double)row["Payment"];
                        bool fullPayment = (bool)row["Pay Full"];
                        double pay = fullPayment ? unpaid : paid;
                        if (AccountBase.AmountEqual(pay, 0))
                            continue;
                        employeeID.Add(ID);
                        payment.Add(pay);
                        //System.Threading.Thread.Sleep(25);
                    }
                  
                    set.employeeID = employeeID.ToArray();
                    set.amount = payment.ToArray();
                    set.periodID = _periodID;
                    waitScreen.SetWaitFormDescription("Posting Payment...");
                    _client.PostGenericDocument(set);
                    waitScreen.SetWaitFormDescription("Posting Payment Done");
                    waitScreen.CloseWaitForm();

                    string message = docID == -1 ? "Payroll Payment successfully saved!" : "Payroll Payment successfully updated!";
                    MessageBox.ShowSuccessMessage(message);
                    m_set = set;
                    this.DialogResult = DialogResult.OK;
                    Close();
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }
            
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
            finally
            {
                if (waitScreen.IsSplashFormVisible)
                    waitScreen.CloseWaitForm();
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private double CalculateTotalPayment()
        {
            return 0;
        }
        private void addToTable(Employee e, double unpaid,double paid)
        {
            _employees.Rows.Add(e.id, true,e.employeeID, e.employeeName, unpaid, paid, AccountBase.AmountEqual(unpaid,paid));
        }
       

        void intializeGrid()
        {
            _employees = new DataTable("Employee");
            _employees.Columns.Add("ID", typeof(int));
            _employees.Columns.Add("Select", typeof(bool));
            _employees.Columns.Add("EmpID", typeof(string));
            _employees.Columns.Add("Name", typeof(string));
            _employees.Columns.Add("Unpaid", typeof(double));
            _employees.Columns.Add("Payment", typeof(double));
            _employees.Columns.Add("Pay Full", typeof(bool));
            
            gridControl.DataSource = _employees;
            gridView.Columns["ID"].Visible = false;

            gridView.Columns["EmpID"].OptionsColumn.ReadOnly = true;
            gridView.Columns["Name"].OptionsColumn.ReadOnly = true;
            gridView.Columns["Unpaid"].OptionsColumn.ReadOnly = true;

        }
        

        private void InitializeExemptionValidation(double totalDeclaredAmount)
        {
            TaxExemptionValidationRule exemptionValidation = new TaxExemptionValidationRule(totalDeclaredAmount);
            exemptionValidation.ErrorType = ErrorType.Default;
            exemptionValidation.ErrorText = "The exemption amount you entered will make total payment negative. Please correct it and try again!";
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {

            if (tabControl.SelectedTabPageIndex == tabControl.TabPages.Count - 1)
                save();
            else
            {
                tabControl.SelectedTabPageIndex++;
            }
        }

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            tabControl.SelectedTabPageIndex--;
        }

        private void buttonCancel_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column == gridView.Columns["Pay Full"])
            {
                bool full = (bool)e.Value;
                if (full)
                {
                    DataRow row = gridView.GetDataRow(e.RowHandle);
                    row["Payment"] = row["Unpaid"];
                }
            }
            else if (e.Column == gridView.Columns["Payment"])
            {
                DataRow row = gridView.GetDataRow(e.RowHandle);
                row["Pay Full"] = false;
            }
            updateTotal();
        }

        private void comboPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            setPeriod((PayrollPeriod)comboPeriod.selectedPeriod);
            populateList();
        }
        void updatePrevNextButton()
        {

            buttonNext.Text = tabControl.SelectedTabPageIndex == tabControl.TabPages.Count - 1 ? "Finish" : "Next>>";
            buttonPrev.Enabled = tabControl.SelectedTabPageIndex > 0;
        }
        private void tabControl_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            updatePrevNextButton();
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < gridView.DataRowCount; i++)
            {
                DataRow row = gridView.GetDataRow(i);
                row["Select"] = chkSelectAll.Checked;
            }
        }
    }
}
