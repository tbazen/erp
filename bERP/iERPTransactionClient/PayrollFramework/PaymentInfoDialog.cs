﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;
using DevExpress.XtraEditors.Controls;

namespace BIZNET.iERP.Client
{
    public partial class PaymentInfoDialog : XtraForm
    {
        CashAccount[] m_cashOnHandAccounts;
        private BankAccountInfo[] m_BankAccounts;
        private bool m_ValidateCash;
        private bool newDocument=true;
        private string paymentSource;
        private double m_TotalPayment = 0;

        public struct PaymentInfo
        {
            public string payVoucherNumber;
            public BizNetPaymentMethod payMethod;
            public int assetAccountID;
            public string slipReference;
            public string note;
        }
        public  delegate void PaymentInfoSelectionHandler(PaymentInfo info);
        public static event PaymentInfoSelectionHandler PaymentInfoSelected;
        private ActivationParameter _activation;
        private PaymentMethodAndBalanceController _paymentController;
        private IAccountingClient _client;
        public PaymentInfoDialog(double paymentAmount,DateTime date)
        {
            InitializeComponent();
            this.date.DateTime = date;
            _activation = new ActivationParameter();
            _client = AccountingClient.AccountingClientDefaultInstance;
            _activation.paymentMethod = BizNetPaymentMethod.Check;
            m_TotalPayment = paymentAmount;
            _paymentController = new PaymentMethodAndBalanceController(paymentTypeSelector, layoutControlBankAccount, cmbBankAccount, layoutControlBankBalance, labelBankBalance, layoutControlCashAccount, cmbCasher, layoutControlCashBalance, labelCashBalance, new DevExpress.XtraLayout.LayoutControlItem(), layoutControlSlipRef, this.date, _activation);
            _paymentController.PaymentInformationChanged += new EventHandler(_paymentMethodController_PaymentInformationChanged);
            SetFormTitle();
            txtPaymentVoucherNo.LostFocus += Control_LostFocus;
            txtReference.LostFocus += Control_LostFocus;
        }

        private void SetFormTitle()
        {

            string title = "Salary Payment ";


            lblTitle.Text = "<size=14><color=#360087><b>" + title + "</b></color></size>" +
                "<size=14><color=#360087><b>" + _paymentController.PaymentSource + "</b></color></size>";
        }
        void _paymentMethodController_PaymentInformationChanged(object sender, EventArgs e)
        {
            SetFormTitle();
        }
       
        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validateControls.Validate(control);
        }

        
       
        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (validateControls.Validate())
            {
                PaymentInfo payInfo = new PaymentInfo();
                payInfo.payVoucherNumber = txtPaymentVoucherNo.Text;
                payInfo.note = txtNote.Text;
                payInfo.payMethod = _paymentController.PaymentTypeSelector.PaymentMethod;
                payInfo.assetAccountID = _paymentController.assetAccountID;
                if (PaymentMethodHelper.IsPaymentMethodWithReference(_paymentController.PaymentTypeSelector.PaymentMethod))
                {
                    payInfo.slipReference = txtReference.Text;
                }
                if (PaymentInfoSelected != null)
                    PaymentInfoSelected(payInfo);
                Close();
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}
