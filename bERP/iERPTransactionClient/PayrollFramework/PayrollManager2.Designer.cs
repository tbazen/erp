﻿namespace BIZNET.iERP.Client
{
    partial class PayrollManager2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PayrollManager2));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.listPayrolls = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.browser = new INTAPS.UI.HTML.ControlBrowser();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.htmlTool = new INTAPS.UI.HTML.HTMLExportTool();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.buttonEditPayroll = new DevExpress.XtraEditors.SimpleButton();
            this.buttonPay = new DevExpress.XtraEditors.SimpleButton();
            this.buttonDelete = new DevExpress.XtraEditors.SimpleButton();
            this.buttonGenerate = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.buttonWorkData = new DevExpress.XtraEditors.SimpleButton();
            this.menuWorkData = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.overtimeDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPrintSlip = new DevExpress.XtraEditors.SimpleButton();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.regularTimeDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.specialBenefitDeductionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.permanentBenefitDeductionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comboFormat = new BIZNET.iERP.Client.PayrollSheetFormatSelector();
            this.comboPeriod = new BIZNET.iERP.Client.PeriodSelector();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.menuWorkData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboFormat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboPeriod.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 43);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.listPayrolls);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.browser);
            this.splitContainerControl1.Panel2.Controls.Add(this.panelControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(940, 367);
            this.splitContainerControl1.SplitterPosition = 103;
            this.splitContainerControl1.TabIndex = 17;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // listPayrolls
            // 
            this.listPayrolls.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.listPayrolls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listPayrolls.FullRowSelect = true;
            this.listPayrolls.HideSelection = false;
            this.listPayrolls.Location = new System.Drawing.Point(0, 0);
            this.listPayrolls.Name = "listPayrolls";
            this.listPayrolls.Size = new System.Drawing.Size(940, 103);
            this.listPayrolls.TabIndex = 0;
            this.listPayrolls.UseCompatibleStateImageBehavior = false;
            this.listPayrolls.View = System.Windows.Forms.View.Details;
            this.listPayrolls.SelectedIndexChanged += new System.EventHandler(this.listPayrolls_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Date";
            this.columnHeader1.Width = 118;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Title";
            this.columnHeader2.Width = 250;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Gross";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader3.Width = 124;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Net";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader4.Width = 127;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Paid";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader5.Width = 96;
            // 
            // browser
            // 
            this.browser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browser.Location = new System.Drawing.Point(0, 38);
            this.browser.MinimumSize = new System.Drawing.Size(20, 20);
            this.browser.Name = "browser";
            this.browser.Size = new System.Drawing.Size(940, 221);
            this.browser.StyleSheetFile = "payroll.css";
            this.browser.TabIndex = 0;
            this.browser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.browser_DocumentCompleted);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.comboFormat);
            this.panelControl2.Controls.Add(this.htmlTool);
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(940, 38);
            this.panelControl2.TabIndex = 1;
            // 
            // htmlTool
            // 
            this.htmlTool.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.htmlTool.Location = new System.Drawing.Point(5, 6);
            this.htmlTool.Name = "htmlTool";
            this.htmlTool.Size = new System.Drawing.Size(84, 27);
            this.htmlTool.TabIndex = 20;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl2.Location = new System.Drawing.Point(705, 12);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(73, 13);
            this.labelControl2.TabIndex = 17;
            this.labelControl2.Text = "Payroll Format:";
            this.labelControl2.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnPrintSlip);
            this.panelControl1.Controls.Add(this.btnClear);
            this.panelControl1.Controls.Add(this.buttonWorkData);
            this.panelControl1.Controls.Add(this.buttonEditPayroll);
            this.panelControl1.Controls.Add(this.buttonPay);
            this.panelControl1.Controls.Add(this.buttonDelete);
            this.panelControl1.Controls.Add(this.buttonGenerate);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.comboPeriod);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(940, 43);
            this.panelControl1.TabIndex = 18;
            // 
            // buttonEditPayroll
            // 
            this.buttonEditPayroll.Location = new System.Drawing.Point(312, 9);
            this.buttonEditPayroll.Name = "buttonEditPayroll";
            this.buttonEditPayroll.Size = new System.Drawing.Size(67, 29);
            this.buttonEditPayroll.TabIndex = 18;
            this.buttonEditPayroll.Text = "Edit";
            this.buttonEditPayroll.Click += new System.EventHandler(this.buttonEditPayroll_Click);
            // 
            // buttonPay
            // 
            this.buttonPay.Location = new System.Drawing.Point(228, 9);
            this.buttonPay.Name = "buttonPay";
            this.buttonPay.Size = new System.Drawing.Size(78, 29);
            this.buttonPay.TabIndex = 18;
            this.buttonPay.Text = "Pay";
            this.buttonPay.Click += new System.EventHandler(this.buttonPay_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(120, 9);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(102, 29);
            this.buttonDelete.TabIndex = 18;
            this.buttonDelete.Text = "Delete Payroll";
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.Location = new System.Drawing.Point(12, 9);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Size = new System.Drawing.Size(102, 29);
            this.buttonGenerate.TabIndex = 18;
            this.buttonGenerate.Text = "Generate Payroll";
            this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Location = new System.Drawing.Point(738, 16);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(30, 13);
            this.labelControl1.TabIndex = 17;
            this.labelControl1.Text = "Period";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // buttonWorkData
            // 
            this.buttonWorkData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonWorkData.Location = new System.Drawing.Point(635, 9);
            this.buttonWorkData.Name = "buttonWorkData";
            this.buttonWorkData.Size = new System.Drawing.Size(82, 29);
            this.buttonWorkData.TabIndex = 18;
            this.buttonWorkData.Text = "Edit Data";
            this.buttonWorkData.Click += new System.EventHandler(this.buttonWorkData_Click);
            // 
            // menuWorkData
            // 
            this.menuWorkData.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.overtimeDataToolStripMenuItem,
            this.regularTimeDataToolStripMenuItem,
            this.specialBenefitDeductionToolStripMenuItem,
            this.permanentBenefitDeductionToolStripMenuItem});
            this.menuWorkData.Name = "menuWorkData";
            this.menuWorkData.Size = new System.Drawing.Size(233, 114);
            // 
            // overtimeDataToolStripMenuItem
            // 
            this.overtimeDataToolStripMenuItem.Name = "overtimeDataToolStripMenuItem";
            this.overtimeDataToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.overtimeDataToolStripMenuItem.Text = "Overtime Data";
            this.overtimeDataToolStripMenuItem.Click += new System.EventHandler(this.overtimeDataToolStripMenuItem_Click);
            // 
            // btnPrintSlip
            // 
            this.btnPrintSlip.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintSlip.Appearance.Options.UseFont = true;
            this.btnPrintSlip.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintSlip.Image")));
            this.btnPrintSlip.Location = new System.Drawing.Point(385, 9);
            this.btnPrintSlip.Name = "btnPrintSlip";
            this.btnPrintSlip.Size = new System.Drawing.Size(96, 29);
            this.btnPrintSlip.TabIndex = 19;
            this.btnPrintSlip.Text = "&Print Slip";
            this.btnPrintSlip.Click += new System.EventHandler(this.btnPrintSlip_Click);
            // 
            // btnClear
            // 
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.Location = new System.Drawing.Point(502, 9);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(102, 29);
            this.btnClear.TabIndex = 18;
            this.btnClear.Text = "C&lear Selection";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // regularTimeDataToolStripMenuItem
            // 
            this.regularTimeDataToolStripMenuItem.Name = "regularTimeDataToolStripMenuItem";
            this.regularTimeDataToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.regularTimeDataToolStripMenuItem.Text = "Regular Time Data";
            this.regularTimeDataToolStripMenuItem.Click += new System.EventHandler(this.regularTimeDataToolStripMenuItem_Click);
            // 
            // specialBenefitDeductionToolStripMenuItem
            // 
            this.specialBenefitDeductionToolStripMenuItem.Name = "specialBenefitDeductionToolStripMenuItem";
            this.specialBenefitDeductionToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.specialBenefitDeductionToolStripMenuItem.Text = "Special Benefit/Deduction";
            this.specialBenefitDeductionToolStripMenuItem.Click += new System.EventHandler(this.specialBenefitDeductionToolStripMenuItem_Click);
            // 
            // permanentBenefitDeductionToolStripMenuItem
            // 
            this.permanentBenefitDeductionToolStripMenuItem.Name = "permanentBenefitDeductionToolStripMenuItem";
            this.permanentBenefitDeductionToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.permanentBenefitDeductionToolStripMenuItem.Text = "Permanent Benefit/Deduction";
            this.permanentBenefitDeductionToolStripMenuItem.Click += new System.EventHandler(this.permanentBenefitDeductionToolStripMenuItem_Click);
            // 
            // comboFormat
            // 
            this.comboFormat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comboFormat.Location = new System.Drawing.Point(787, 9);
            this.comboFormat.Name = "comboFormat";
            this.comboFormat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboFormat.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboFormat.Size = new System.Drawing.Size(118, 20);
            this.comboFormat.TabIndex = 25;
            this.comboFormat.SelectedIndexChanged += new System.EventHandler(this.comboFormat_SelectedIndexChanged);
            // 
            // comboPeriod
            // 
            this.comboPeriod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboPeriod.Location = new System.Drawing.Point(787, 12);
            this.comboPeriod.Name = "comboPeriod";
            this.comboPeriod.PeriodType = "AP_Payroll";
            this.comboPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboPeriod.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboPeriod.Size = new System.Drawing.Size(141, 20);
            this.comboPeriod.TabIndex = 16;
            this.comboPeriod.SelectedIndexChanged += new System.EventHandler(this.comboPeriod_SelectedIndexChanged);
            // 
            // PayrollManager2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(940, 410);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "PayrollManager2";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Payroll Manager";
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.menuWorkData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboFormat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboPeriod.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected PeriodSelector comboPeriod;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton buttonPay;
        private DevExpress.XtraEditors.SimpleButton buttonDelete;
        private DevExpress.XtraEditors.SimpleButton buttonGenerate;
        private System.Windows.Forms.ListView listPayrolls;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private INTAPS.UI.HTML.ControlBrowser browser;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private INTAPS.UI.HTML.HTMLExportTool htmlTool;
        private PayrollSheetFormatSelector comboFormat;
        private DevExpress.XtraEditors.SimpleButton buttonEditPayroll;
        private DevExpress.XtraEditors.SimpleButton btnPrintSlip;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private DevExpress.XtraEditors.SimpleButton buttonWorkData;
        private System.Windows.Forms.ContextMenuStrip menuWorkData;
        private System.Windows.Forms.ToolStripMenuItem overtimeDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regularTimeDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem specialBenefitDeductionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem permanentBenefitDeductionToolStripMenuItem;
    }
}