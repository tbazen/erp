﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Payroll;
using INTAPS.Payroll.Client;
using INTAPS.Accounting.Client;
using DevExpress.XtraReports.UI;

namespace BIZNET.iERP.Client
{
    public partial class PayrollManager2 : DevExpress.XtraEditors.XtraForm
    {
        DevExpress.XtraSplashScreen.SplashScreenManager waitScreen;

        public PayrollManager2()
        {
            InitializeComponent();
            waitScreen = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::BIZNET.iERP.Client.WaitForm1), true, true);
            htmlTool.setBrowser(browser);
            comboPeriod.setByDate(DateTime.Now);
            loadList();
            listPayrolls_SelectedIndexChanged(null, null);
        }

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            PreviewPayroll2 preview = new PreviewPayroll2(comboPeriod.selectedPeriod.id);
            if (preview.ShowDialog(this) == DialogResult.OK)
            {
                loadList();
                foreach (ListViewItem li in listPayrolls.Items)
                {
                    if (((PayrollSetDocument) li.Tag).AccountDocumentID == preview.payrollSet.AccountDocumentID)
                    {
                        li.Selected = true;
                        break;
                    }
                }
            }
        }

        private void loadList()
        {
            listPayrolls.Items.Clear();
            double [] payroll;
            double[] paid;
            foreach (PayrollSetDocument set in PayrollClient.getPayrollSetsOfAPeriod(comboPeriod.selectedPeriodID))
            {
                int[] e = new int[set.payrolls.Length];
                for (int i = 0; i < e.Length; i++)
                    e[i] = set.payrolls[i].employee.id;
                paid=PayrollClient.getPaymetAmounts(comboPeriod.selectedPeriodID,e, out payroll);
                double paidTotal = 0;
                foreach (double pd in paid)
                    paidTotal += pd;
                ListViewItem li = new ListViewItem(set.DocumentDate.ToString("MMM dd,yyyy"));
                li.SubItems.Add(set.LongDescription);
                li.SubItems.Add(set.grossEarning().ToString("#,#0.00"));
                li.SubItems.Add(set.netPayment().ToString("#,#0.00"));
                li.SubItems.Add(paidTotal.ToString("#,#0.00"));
                li.Tag = set;
                listPayrolls.Items.Add(li);
            }
        }

        private void browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
        }

        private void listPayrolls_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonDelete.Enabled = listPayrolls.SelectedItems.Count > 0;
            buttonPay.Enabled = listPayrolls.SelectedItems.Count == 1;
            buttonEditPayroll.Enabled = listPayrolls.SelectedItems.Count == 1;
            if (listPayrolls.SelectedItems.Count == 0)
            {
                browser.LoadTextPage("", "");
                return;
            }
            generateSheet();
        }
        void generateSheet()
        {
            PayrollSetDocument payrollSet = new PayrollSetDocument();
            List<PayrollDocument> payList = new List<PayrollDocument>();
            if (listPayrolls.SelectedItems.Count == 0)
                browser.LoadTextPage("Payroll", "");
            else
            {
                waitScreen.ShowWaitForm();
                for (int i = 0; i < listPayrolls.SelectedItems.Count; i++)
                {
                    waitScreen.SetWaitFormDescription("Generating Payroll Preview");
                    PayrollSetDocument setdoc = listPayrolls.SelectedItems[i].Tag as PayrollSetDocument;
                    if (i == 0)
                    {
                        payrollSet.LongDescription = setdoc.LongDescription;
                    }
                    foreach (PayrollDocument doc in setdoc.payrolls)
                    {
                        payList.Add(doc);
                    }
                }
                payrollSet.payrolls = payList.ToArray();
                string html=PayrollClient.GetPayrollTableHTML(payrollSet, (comboPeriod.selectedPeriod as PayrollPeriod).id, comboFormat.SelectedIndex);
                browser.LoadTextPage("Payroll",html);
                waitScreen.CloseWaitForm();
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (listPayrolls.SelectedItems.Count > 0)
                if (MessageBox.ShowWarningMessage("Are you sure you want to delete the selected payrolls?") != DialogResult.Yes)
                    return;
            try
            {
                foreach (ListViewItem li in listPayrolls.SelectedItems)
                {
                    PayrollSetDocument doc = (PayrollSetDocument)li.Tag;
                    AccountingClient.DeleteGenericDocument(doc.AccountDocumentID);
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
            loadList();
            listPayrolls_SelectedIndexChanged(null, null);
        }

        private void buttonShowPayments_Click(object sender, EventArgs e)
        {

        }

        private void buttonPay_Click(object sender, EventArgs e)
        {
            if (listPayrolls.SelectedItems.Count == 0)
                return;
            PayrollSetDocument set=(PayrollSetDocument)listPayrolls.SelectedItems[0].Tag;
            int [] ed=new int[set.payrolls.Length];
            for(int i=0;i<ed.Length;i++)
                ed[i]=set.payrolls[i].employee.id;
            ActivationParameter par = new ActivationParameter();
            par.periodID = comboPeriod.selectedPeriodID;
            //par.employeeIDs = ed;
            par.parentAccountDocument = set;
            PayPayroll2 pp = new PayPayroll2(AccountingClient.AccountingClientDefaultInstance, par);
            if (pp.ShowDialog(this) == DialogResult.OK)
            {
                loadList();
            }
        }

        private void comboPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                loadList();
            }
            catch(Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void comboFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                generateSheet();
            }
            catch(Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void buttonEditPayroll_Click(object sender, EventArgs e)
        {
            
            if (listPayrolls.SelectedItems.Count != 1)
                return;
            try
            {
                PayrollSetDocument set = listPayrolls.SelectedItems[0].Tag as PayrollSetDocument;
                PreviewPayroll2 preview = new PreviewPayroll2();
                preview.loadData(set);
                if (preview.ShowDialog(this) == DialogResult.OK)
                {
                    loadList();
                    foreach (ListViewItem li in listPayrolls.Items)
                    {
                        if (((PayrollSetDocument)li.Tag).AccountDocumentID == preview.payrollSet.AccountDocumentID)
                        {
                            li.Selected = true;
                            break;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void btnPrintSlip_Click(object sender, EventArgs e)
        {
            //Print Slip
            PayrollSlip report = null;
            if (listPayrolls.SelectedItems.Count != 1)
                return;
            PayrollSetDocument set = listPayrolls.SelectedItems[0].Tag as PayrollSetDocument;
            Employee[] employeeID = set.Employees;
            waitScreen.ShowWaitForm();
            for (int i = 0; i < set.Employees.Length; i++)
            {
                waitScreen.SetWaitFormDescription("Generating Payslip for Employee ID:" + employeeID[i].employeeID);

                if (i == 0)
                {
                    report = new PayrollSlip(employeeID[i].id, comboPeriod.selectedPeriodID);
                    report.CreateDocument();
                }
                else
                {
                    PayrollSlip slip = new PayrollSlip(employeeID[i].id, comboPeriod.selectedPeriodID);
                    slip.CreateDocument();
                    if (report != null)
                        report.Pages.AddRange(slip.Pages);
                }
                //System.Threading.Thread.Sleep(25);Never block the main UI thread.
            }
            waitScreen.CloseWaitForm();
            //      report.PrintingSystem.ContinuousPageNumbering = true;
            ReportPrintTool printTool = new ReportPrintTool(report);
            printTool.ShowPreviewDialog();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            listPayrolls.SelectedIndices.Clear();
        }

        private void overtimeDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            INTAPS.Accounting.AccountingPeriod prd = comboPeriod.selectedPeriod;
            if (prd == null)
                return;
            new OverTimeDataEditor(prd.id).Show(this);
        }

        private void buttonWorkData_Click(object sender, EventArgs e)
        {
            menuWorkData.Show(buttonWorkData.PointToScreen(new Point(0, buttonWorkData.Height)));
        }

        private void regularTimeDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            INTAPS.Accounting.AccountingPeriod prd = comboPeriod.selectedPeriod;
            if (prd == null)
                return;
            new RegularTimeDataEditor(prd.id).Show(this);

        }

        private void specialBenefitDeductionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            INTAPS.Accounting.AccountingPeriod prd = comboPeriod.selectedPeriod;
            if (prd == null)
                return;
            new SpecialBenefitDeductionDataEditor(prd.id).Show(this);
        }

        private void permanentBenefitDeductionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            INTAPS.Accounting.AccountingPeriod prd = comboPeriod.selectedPeriod;
            if (prd == null)
                return;
            new PrermanentBenefitDeductionDataEditor(prd.id).Show(this);
        }
    }
}