﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using INTAPS.Payroll;
using INTAPS.Payroll.Client;
using INTAPS.Evaluator;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class PayrollSlip : DevExpress.XtraReports.UI.XtraReport
    {
       
        private bool _IsPayrollGenerated;
        private Employee m_Employee;
        private PayrollPeriod m_PayPeriod;
        public PayrollSlip(int employeeID,int periodID)
        {
            try
            {
                InitializeComponent();
                m_Employee = PayrollClient.GetEmployee(employeeID);
                m_PayPeriod = PayrollClient.GetPayPeriod(periodID);
                const string REGULAR_TIME="Regular Time";
                List<string> additionFormulaNames = new List<string>();
                List<double> additionValues = new List<double>();

                List<string> deductionFormulaNames = new List<string>();
                List<double> deductionValues = new List<double>();
                PayrollDocument payDoc = null;
                PayrollSetDocument payrollSet = PayrollClient.getPayrollSetByEmployee(periodID,employeeID);
                if (payrollSet != null)
                {
                    if (payrollSet.payrolls.Length > 0)
                    {
                        foreach (PayrollDocument doc in payrollSet.payrolls)
                        {
                            if (doc.employee.id == employeeID)
                            {
                                payDoc = doc;
                                break;
                            }
                        }
                        Array.Sort<PayrollComponent>(payDoc.components, delegate(PayrollComponent c1, PayrollComponent c2)
                        {
                            if (c1.Deduct == c2.Deduct)
                                return c2.Amount.CompareTo(c1.Amount);
                            return c1.Deduct.CompareTo(c2.Deduct);
                        }
                          );

                        foreach (PayrollComponent comp in payDoc.components)
                        {
                            if (comp.Deduct)
                            {
                                deductionFormulaNames.Add(comp.Name);
                                deductionValues.Add(comp.Amount);
                            }
                            else
                            {
                                if (comp.Name.ToUpper().Equals(REGULAR_TIME.ToUpper()))
                                    additionFormulaNames.Add("Gross Salary");
                                else
                                    additionFormulaNames.Add(comp.Name);
                                additionValues.Add(comp.Amount);
                            }
                        }
                        if (payDoc == null)
                            _IsPayrollGenerated = false;
                        else
                        {
                            _IsPayrollGenerated = true;
                            PopulateEmployeeProfile(payDoc.DocumentDate, periodID, payDoc.NetPay);
                            PopulateAdditionsOrDeductions(tblPayments, additionFormulaNames.ToArray(), additionValues.ToArray());
                            PopulateAdditionsOrDeductions(tblDeductions, deductionFormulaNames.ToArray(), deductionValues.ToArray());
                            PopulateRemainingLoanBalance(periodID);
                        }
                
                    }
                    else
                        _IsPayrollGenerated = false;
                }
                else
                    _IsPayrollGenerated = false;

               
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }

        }

        public bool IsPayrollGenerated
        {
            get { return _IsPayrollGenerated; }
        }
        private void PopulateEmployeeProfile(DateTime datePayroll,int periodID,double netPay)
        {
         
            lblTitle.Text = m_Employee.employeeName;
            lblPayrollDate.Text = datePayroll.ToShortDateString();
            lblID.Text = m_Employee.employeeID;
            lblPeriod.Text = m_PayPeriod.name;
            lblNetPay.Text = netPay.ToString("N");
        }

        private  void PopulateAdditionsOrDeductions(XRTable table,string[] formulaName, double[] formula)
        {
            double total = 0;
            table.Rows.Clear();
            for (int i = 0; i < formulaName.Length+1; i++)
            {
                string txt = "";
               
                XRTableRow row = new XRTableRow();
                if (i <= formulaName.Length - 1)
                {
                    double val = formula[i];
                    total += val;
                    if (val != 0)
                    {
                        txt = val.ToString("0,0.00");
                        XRTableCell cellName = XRUtility.CreateXRTableCell(formulaName[i], "cell" + formulaName[i], 357.19f, DevExpress.XtraPrinting.TextAlignment.BottomLeft);
                        XRTableCell cellValue = XRUtility.CreateXRTableCell(txt, "cell" + txt, 267.23f, DevExpress.XtraPrinting.TextAlignment.BottomRight);
                        row.Cells.AddRange(new XRTableCell[] { cellName, cellValue });
                    }
                }
                else if (i == formulaName.Length)
                {
                    XRTableCell totalCell = XRUtility.CreateXRTableCell("TOTAL", "cellTotal", 357.19f, new Font(table.Font, FontStyle.Bold), DevExpress.XtraPrinting.TextAlignment.BottomLeft);
                    totalCell.BorderWidth = 2;
                    totalCell.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    totalCell.BorderColor = Color.Black;

                    XRTableCell totalValue = XRUtility.CreateXRTableCell(total.ToString("N"), "cellTotalValue", 267.23f, new Font(table.Font, FontStyle.Bold), DevExpress.XtraPrinting.TextAlignment.BottomRight);
                    totalValue.BorderWidth = 2;
                    totalValue.Borders = DevExpress.XtraPrinting.BorderSide.All;
                    totalValue.BorderColor=Color.Black;
                    row.Cells.AddRange(new XRTableCell[] { totalCell, totalValue });
                }
                if (row.Cells.Count > 0)
                    table.Rows.Add(row);
            }
        }


        private void PopulateRemainingLoanBalance(int periodID)
        {
            //Employee employee = PayrollClient.GetEmployee(employeeID);
            int longTermAcc = (int)PayrollClient.GetSystemParameter("staffLongTermLoanAccountID");
            int shortTermAc = (int)PayrollClient.GetSystemParameter("staffSalaryAdvanceAccountID");
            int maincostCenterAc = (int)iERPTransactionClient.GetSystemParamter("mainCostCenterID");

            Account longTerm = iERPTransactionClient.GetEmployeeAccount(m_Employee, longTermAcc);
            Account shortTerm = iERPTransactionClient.GetEmployeeAccount(m_Employee, shortTermAc);

            double staffLongTermCostCenterBalance = maincostCenterAc == m_Employee.costCenterID ? 0 : AccountingClient.GetNetBalanceAfter(AccountingClient.GetCostCenterAccount(m_Employee.costCenterID, longTerm.id).id, 0, DateTime.Now);
            double staffShortTermCostCenterBalance = maincostCenterAc == m_Employee.costCenterID ? 0 : AccountingClient.GetNetBalanceAfter(AccountingClient.GetCostCenterAccount(m_Employee.costCenterID, shortTerm.id).id, 0, DateTime.Now);

            double longTermBalance = AccountingClient.GetNetBalanceAfter(AccountingClient.GetCostCenterAccount(maincostCenterAc, longTerm.id).id,0, DateTime.Now) + staffLongTermCostCenterBalance;
            double shortTermBalance = AccountingClient.GetNetBalanceAfter(AccountingClient.GetCostCenterAccount(maincostCenterAc, shortTerm.id).id, 0, DateTime.Now) + staffShortTermCostCenterBalance;
            if (Account.AmountEqual(longTermBalance, 0))
                tblLoanBalance.Rows.RemoveAt(0);
            else
                cellLongTerm.Text = longTermBalance.ToString("N");
            if (Account.AmountEqual(shortTermBalance, 0))
                tblLoanBalance.Rows.RemoveAt(tblLoanBalance.Rows.Count == 2 ? 0 : 1);
            else
                cellShortTerm.Text = shortTermBalance.ToString("N");
            cellLoanValueTotal.Text = (longTermBalance + shortTermBalance).ToString("N");

           
          

        }

        private void cellPageNo_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            if (e.PageCount == 1)
                e.Cancel = true;
        }

        private void xrPageInfo1_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            if (e.PageCount == 1)
                e.Cancel = true;
        }

       

    }

}
