﻿namespace BIZNET.iERP.Client
{
    partial class PreviewPayroll2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.employeeDomain = new INTAPS.Payroll.Client.EmployeeDomainControl();
            this.pnlWaitPanel = new System.Windows.Forms.Panel();
            this.lblDesc = new System.Windows.Forms.Label();
            this.pbWaitAnimation = new System.Windows.Forms.PictureBox();
            this.browser = new INTAPS.UI.HTML.ControlBrowser();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.format = new BIZNET.iERP.Client.PayrollSheetFormatSelector();
            this.textTitle = new DevExpress.XtraEditors.MemoEdit();
            this.calendar = new BIZNET.iERP.Client.BNDualCalendar();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.buttonGenerate = new DevExpress.XtraEditors.SimpleButton();
            this.buttonSave = new DevExpress.XtraEditors.SimpleButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.pnlWaitPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWaitAnimation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.format.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textTitle.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 104);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.employeeDomain);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.pnlWaitPanel);
            this.splitContainerControl1.Panel2.Controls.Add(this.browser);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1037, 373);
            this.splitContainerControl1.SplitterPosition = 264;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // employeeDomain
            // 
            this.employeeDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.employeeDomain.Location = new System.Drawing.Point(0, 0);
            this.employeeDomain.Name = "employeeDomain";
            this.employeeDomain.Size = new System.Drawing.Size(264, 373);
            this.employeeDomain.TabIndex = 4;
            this.employeeDomain.DomainChanged += new System.EventHandler(this.employeeDomain_DomainChanged);
            // 
            // pnlWaitPanel
            // 
            this.pnlWaitPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlWaitPanel.BackColor = System.Drawing.Color.White;
            this.pnlWaitPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlWaitPanel.Controls.Add(this.lblDesc);
            this.pnlWaitPanel.Controls.Add(this.pbWaitAnimation);
            this.pnlWaitPanel.Location = new System.Drawing.Point(226, 128);
            this.pnlWaitPanel.Name = "pnlWaitPanel";
            this.pnlWaitPanel.Size = new System.Drawing.Size(310, 45);
            this.pnlWaitPanel.TabIndex = 20;
            this.pnlWaitPanel.Visible = false;
            // 
            // lblDesc
            // 
            this.lblDesc.BackColor = System.Drawing.Color.White;
            this.lblDesc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDesc.Location = new System.Drawing.Point(39, 0);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(269, 43);
            this.lblDesc.TabIndex = 1;
            this.lblDesc.Text = "Generating Payroll";
            this.lblDesc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pbWaitAnimation
            // 
            this.pbWaitAnimation.Dock = System.Windows.Forms.DockStyle.Left;
            this.pbWaitAnimation.ErrorImage = null;
            this.pbWaitAnimation.Image = global::BIZNET.iERP.Client.Properties.Resources.wait30trans;
            this.pbWaitAnimation.Location = new System.Drawing.Point(0, 0);
            this.pbWaitAnimation.Name = "pbWaitAnimation";
            this.pbWaitAnimation.Size = new System.Drawing.Size(39, 43);
            this.pbWaitAnimation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWaitAnimation.TabIndex = 0;
            this.pbWaitAnimation.TabStop = false;
            // 
            // browser
            // 
            this.browser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browser.Location = new System.Drawing.Point(0, 0);
            this.browser.MinimumSize = new System.Drawing.Size(20, 20);
            this.browser.Name = "browser";
            this.browser.Size = new System.Drawing.Size(768, 373);
            this.browser.StyleSheetFile = "payroll.css";
            this.browser.TabIndex = 1;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.format);
            this.panelControl1.Controls.Add(this.textTitle);
            this.panelControl1.Controls.Add(this.calendar);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.buttonGenerate);
            this.panelControl1.Controls.Add(this.buttonSave);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1037, 104);
            this.panelControl1.TabIndex = 19;
            // 
            // format
            // 
            this.format.Location = new System.Drawing.Point(775, 28);
            this.format.Name = "format";
            this.format.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.format.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.format.Size = new System.Drawing.Size(118, 20);
            this.format.TabIndex = 24;
            this.format.SelectedIndexChanged += new System.EventHandler(this.format_SelectedIndexChanged);
            // 
            // textTitle
            // 
            this.textTitle.Location = new System.Drawing.Point(314, 29);
            this.textTitle.Name = "textTitle";
            this.textTitle.Size = new System.Drawing.Size(379, 65);
            this.textTitle.TabIndex = 23;
            this.textTitle.EditValueChanged += new System.EventHandler(this.textTitle_EditValueChanged);
            // 
            // calendar
            // 
            this.calendar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.calendar.Location = new System.Drawing.Point(12, 31);
            this.calendar.Name = "calendar";
            this.calendar.ShowEthiopian = true;
            this.calendar.ShowGregorian = true;
            this.calendar.ShowTime = true;
            this.calendar.Size = new System.Drawing.Size(264, 63);
            this.calendar.TabIndex = 21;
            this.calendar.VerticalLayout = true;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Location = new System.Drawing.Point(764, 12);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(44, 13);
            this.labelControl3.TabIndex = 22;
            this.labelControl3.Text = "Format:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(303, 12);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(28, 13);
            this.labelControl2.TabIndex = 22;
            this.labelControl2.Text = "Title:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(5, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(30, 13);
            this.labelControl1.TabIndex = 22;
            this.labelControl1.Text = "Date:";
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGenerate.Location = new System.Drawing.Point(930, 42);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Size = new System.Drawing.Size(102, 23);
            this.buttonGenerate.TabIndex = 18;
            this.buttonGenerate.Text = "Generate";
            this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.Enabled = false;
            this.buttonSave.Location = new System.Drawing.Point(930, 71);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(102, 23);
            this.buttonSave.TabIndex = 18;
            this.buttonSave.Text = "Save";
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // PreviewPayroll2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1037, 477);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "PreviewPayroll2";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Preview Payroll";
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.pnlWaitPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbWaitAnimation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.format.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textTitle.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private INTAPS.Payroll.Client.EmployeeDomainControl employeeDomain;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton buttonSave;
        private INTAPS.UI.HTML.ControlBrowser browser;
        private System.Windows.Forms.Timer timer1;
        private BNDualCalendar calendar;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.MemoEdit textTitle;
        private PayrollSheetFormatSelector format;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.Panel pnlWaitPanel;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.PictureBox pbWaitAnimation;
        private DevExpress.XtraEditors.SimpleButton buttonGenerate;
    }
}