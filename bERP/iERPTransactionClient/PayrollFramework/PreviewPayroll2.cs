﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Payroll;
using INTAPS.Payroll.Client;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class PreviewPayroll2 : DevExpress.XtraEditors.XtraForm
    {
        System.Threading.Thread generateThread=null;
        AppliesToEmployees _at;
        DateTime _date = DateTime.MinValue;
        int _periodID = -1;
        string _html="";
        string _title = null;
        Exception ex=null;
        int _format = -1;
        public PayrollSetDocument payrollSet = null;
        bool _ignoreEvent=false;
        bool _payrollValid = true;
        public PreviewPayroll2()
        {
            InitializeComponent();
            employeeDomain.LoadData();
            setPayrollValid(false);
        }
        void setPayrollValid(bool valid)
        {
            _payrollValid = valid;
            buttonSave.Enabled = valid;
        }
        public PreviewPayroll2(int periodID):this()
        {
            _periodID = periodID;
            
        }
        public PreviewPayroll2(IAccountingClient client, ActivationParameter activation)
            :this()
        {
        }
        public void loadData(PayrollSetDocument doc)
        {
            _ignoreEvent = true;
            try
            {
                payrollSet = doc;
                if (doc == null)
                {
                    payrollSet = null;
                    return;
                }
                payrollSet = doc.clone();

                foreach (PayrollDocument p in payrollSet.payrolls)
                {
                    foreach (PayrollComponent comp in p.components)
                    {
                        foreach (TransactionOfBatch t in comp.Transactions)
                        {
                            //t.AccountID = AccountingClient.GetCostCenterAccount(t.AccountID).accountID;
                        }
                    }
                }
                textTitle.Text = doc.title;
                employeeDomain.SetData(doc.at);
                _periodID = doc.payPeriod.id;
                updatePreview();
                setPayrollValid(true);
            }
            finally
            {
                _ignoreEvent = false;
            }

        }
        void startGeneration()
        {
            buttonSave.Enabled = false;
            _at = employeeDomain.GetData();
            _date = calendar.DateTime;
            _title = textTitle.Text;
            _format = format.SelectedIndex;
            pnlWaitPanel.Show();
            startThread();
            timer1.Start();

        }
        
        private void startThread()
        {
            generateThread = new System.Threading.Thread(new System.Threading.ThreadStart(generatePayroll));
            generateThread.Start();
        }
        
        void generatePayroll()
        {
            try
            {
                BatchError errors;
                PayrollSetDocument doc = PayrollClient.GeneratePayrollPreview2(_date, _title, _at, _periodID, out errors);
                _html = "";
                if (errors != null && errors.ErrorDesc!=null && errors.ErrorDesc.Length > 0)
                {
                    foreach (string d in errors.ErrorDesc)
                    {
                        string errmsg = string.Format("<span class='generateError'>{0}</span>", System.Web.HttpUtility.HtmlEncode(d));
                        if ("".Equals(_html))
                            _html = errmsg;
                        else
                            _html += "<br/>" + errmsg;
                    }
                }
                _html += PayrollClient.GetPayrollTableHTML(doc, _periodID, _format);
                if (payrollSet != null)
                    doc.AccountDocumentID = payrollSet.AccountDocumentID;
                payrollSet = doc; 
            }
            catch (Exception ex)
            {
                this.ex = ex;
                _html = "Error " + ex.Message;
            }
        }

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (generateThread == null || !generateThread.IsAlive)
            {
                timer1.Stop();
                browser.LoadTextPage("Payroll", _html);
                pnlWaitPanel.Hide();
                setPayrollValid(payrollSet != null);
            }
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            
            startGeneration();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (payrollSet == null)
                return;
            try
            {
                payrollSet.title = textTitle.Text;
                payrollSet.DocumentDate = _date;
                payrollSet.AccountDocumentID=AccountingClient.PostGenericDocument(payrollSet);
                MessageBox.ShowSuccessMessage("Payroll Saved Succesfully");
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }
        bool _titleEdited = false;
        private void employeeDomain_DomainChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            if (textTitle.Text == "" || !_titleEdited)
            {
                textTitle.Text = PayrollClient.getAppliesToString(employeeDomain.GetData());
                _titleEdited = false;
            }
            setPayrollValid(false);
        }

        private void textTitle_EditValueChanged(object sender, EventArgs e)
        {
            _titleEdited = true;
            if (payrollSet != null)
                payrollSet.title = textTitle.Text;
        }

        private void format_SelectedIndexChanged(object sender, EventArgs e)
        {

            updatePreview();
        }

        private void updatePreview()
        {
            if (payrollSet == null)
            {
                browser.LoadTextPage("Payroll", "");
            }
            else
            {
                _html = PayrollClient.GetPayrollTableHTML(payrollSet, _periodID, format.SelectedIndex);
                browser.LoadTextPage("Payroll", _html);
            }
        }
    }
}