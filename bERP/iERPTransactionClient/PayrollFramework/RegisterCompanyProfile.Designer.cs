﻿namespace BIZNET.iERP.Client
{
    partial class RegisterCompanyProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule4 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule5 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegisterCompanyProfile));
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtHouseNo = new DevExpress.XtraEditors.TextEdit();
            this.txtKebele = new DevExpress.XtraEditors.TextEdit();
            this.txtWoreda = new DevExpress.XtraEditors.TextEdit();
            this.txtZone = new DevExpress.XtraEditors.TextEdit();
            this.txtRegion = new DevExpress.XtraEditors.TextEdit();
            this.txtCompanyNameAmh = new DevExpress.XtraEditors.TextEdit();
            this.txtMobilePhone = new DevExpress.XtraEditors.TextEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.grpTaxDetails = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.cmbPensionTaxCenter = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbVATTaxCenter = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbWithholdTaxCenter = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dateReg = new BIZNET.iERP.Client.BNDualCalendar();
            this.txtOccupation = new DevExpress.XtraEditors.TextEdit();
            this.cmbBusinessCategory = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbTaxRegType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtMachineNo = new DevExpress.XtraEditors.TextEdit();
            this.txtVatNumber = new DevExpress.XtraEditors.TextEdit();
            this.txtTinNumber = new DevExpress.XtraEditors.TextEdit();
            this.cmbBusinessEntity = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutBusinessEntity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTinNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutVATNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMachineNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTaxRegType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTaxRegistrationDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.txtWebAddress = new DevExpress.XtraEditors.TextEdit();
            this.txtEmail = new DevExpress.XtraEditors.TextEdit();
            this.txtFaxNumber = new DevExpress.XtraEditors.TextEdit();
            this.txtOfficeTele = new DevExpress.XtraEditors.TextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.btnBrowse = new DevExpress.XtraEditors.SimpleButton();
            this.txtCompanyName = new DevExpress.XtraEditors.TextEdit();
            this.picLogo = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutLogoPanel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutLogo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCompanyName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutOfficeTelephone = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutFaxNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutWebsite = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.validateCompanyProf = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.chkWithheldVAT = new DevExpress.XtraEditors.CheckEdit();
            this.layoutWithheldVAT = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHouseNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKebele.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWoreda.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyNameAmh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilePhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpTaxDetails)).BeginInit();
            this.grpTaxDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPensionTaxCenter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVATTaxCenter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbWithholdTaxCenter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOccupation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBusinessCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTaxRegType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMachineNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVatNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBusinessEntity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBusinessEntity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTinNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVATNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMachineNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTaxRegType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTaxRegistrationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFaxNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOfficeTele.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLogoPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOfficeTelephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFaxNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWebsite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validateCompanyProf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWithheldVAT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWithheldVAT)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Controls.Add(this.txtHouseNo);
            this.layoutControl1.Controls.Add(this.txtKebele);
            this.layoutControl1.Controls.Add(this.txtWoreda);
            this.layoutControl1.Controls.Add(this.txtZone);
            this.layoutControl1.Controls.Add(this.txtRegion);
            this.layoutControl1.Controls.Add(this.txtCompanyNameAmh);
            this.layoutControl1.Controls.Add(this.txtMobilePhone);
            this.layoutControl1.Controls.Add(this.panelControl2);
            this.layoutControl1.Controls.Add(this.grpTaxDetails);
            this.layoutControl1.Controls.Add(this.txtWebAddress);
            this.layoutControl1.Controls.Add(this.txtEmail);
            this.layoutControl1.Controls.Add(this.txtFaxNumber);
            this.layoutControl1.Controls.Add(this.txtOfficeTele);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.txtCompanyName);
            this.layoutControl1.Controls.Add(this.picLogo);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(279, 34, 250, 350);
            this.layoutControl1.OptionsView.HighlightFocusedItem = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(675, 598);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtHouseNo
            // 
            this.txtHouseNo.Location = new System.Drawing.Point(142, 199);
            this.txtHouseNo.Name = "txtHouseNo";
            this.txtHouseNo.Properties.Mask.EditMask = "[a-zA-Z0-9].*";
            this.txtHouseNo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtHouseNo.Size = new System.Drawing.Size(332, 20);
            this.txtHouseNo.StyleController = this.layoutControl1;
            this.txtHouseNo.TabIndex = 20;
            // 
            // txtKebele
            // 
            this.txtKebele.Location = new System.Drawing.Point(142, 167);
            this.txtKebele.Name = "txtKebele";
            this.txtKebele.Properties.Mask.EditMask = "[a-zA-Z0-9].*";
            this.txtKebele.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtKebele.Size = new System.Drawing.Size(332, 20);
            this.txtKebele.StyleController = this.layoutControl1;
            this.txtKebele.TabIndex = 19;
            // 
            // txtWoreda
            // 
            this.txtWoreda.Location = new System.Drawing.Point(142, 135);
            this.txtWoreda.Name = "txtWoreda";
            this.txtWoreda.Properties.Mask.EditMask = "[a-zA-Z0-9].*";
            this.txtWoreda.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtWoreda.Size = new System.Drawing.Size(332, 20);
            this.txtWoreda.StyleController = this.layoutControl1;
            this.txtWoreda.TabIndex = 18;
            // 
            // txtZone
            // 
            this.txtZone.Location = new System.Drawing.Point(142, 103);
            this.txtZone.Name = "txtZone";
            this.txtZone.Properties.Mask.EditMask = "[a-zA-Z0-9].*";
            this.txtZone.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtZone.Size = new System.Drawing.Size(332, 20);
            this.txtZone.StyleController = this.layoutControl1;
            this.txtZone.TabIndex = 17;
            // 
            // txtRegion
            // 
            this.txtRegion.Location = new System.Drawing.Point(142, 71);
            this.txtRegion.Name = "txtRegion";
            this.txtRegion.Properties.Mask.EditMask = "[a-zA-Z0-9].*";
            this.txtRegion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtRegion.Size = new System.Drawing.Size(332, 20);
            this.txtRegion.StyleController = this.layoutControl1;
            this.txtRegion.TabIndex = 16;
            // 
            // txtCompanyNameAmh
            // 
            this.txtCompanyNameAmh.Location = new System.Drawing.Point(141, 40);
            this.txtCompanyNameAmh.Name = "txtCompanyNameAmh";
            this.txtCompanyNameAmh.Properties.Appearance.Font = new System.Drawing.Font("Nyala", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCompanyNameAmh.Properties.Appearance.Options.UseFont = true;
            this.txtCompanyNameAmh.Size = new System.Drawing.Size(334, 20);
            this.txtCompanyNameAmh.StyleController = this.layoutControl1;
            this.txtCompanyNameAmh.TabIndex = 15;
            // 
            // txtMobilePhone
            // 
            this.txtMobilePhone.Location = new System.Drawing.Point(141, 260);
            this.txtMobilePhone.Name = "txtMobilePhone";
            this.txtMobilePhone.Properties.Mask.EditMask = "(\\+251)\\d\\d\\d-\\d\\d-\\d\\d-\\d\\d";
            this.txtMobilePhone.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.txtMobilePhone.Size = new System.Drawing.Size(334, 20);
            this.txtMobilePhone.StyleController = this.layoutControl1;
            this.txtMobilePhone.TabIndex = 14;
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.btnCancel);
            this.panelControl2.Controls.Add(this.btnOk);
            this.panelControl2.Location = new System.Drawing.Point(7, 559);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(661, 32);
            this.panelControl2.TabIndex = 13;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(579, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(79, 26);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "&Close";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(492, 3);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(76, 26);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "&Ok";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // grpTaxDetails
            // 
            this.grpTaxDetails.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTaxDetails.Appearance.Options.UseFont = true;
            this.grpTaxDetails.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTaxDetails.AppearanceCaption.ForeColor = System.Drawing.Color.Black;
            this.grpTaxDetails.AppearanceCaption.Options.UseFont = true;
            this.grpTaxDetails.AppearanceCaption.Options.UseForeColor = true;
            this.grpTaxDetails.Controls.Add(this.layoutControl2);
            this.grpTaxDetails.Location = new System.Drawing.Point(7, 377);
            this.grpTaxDetails.Name = "grpTaxDetails";
            this.grpTaxDetails.Size = new System.Drawing.Size(661, 178);
            this.grpTaxDetails.TabIndex = 12;
            this.grpTaxDetails.Text = "Tax Registration Details";
            // 
            // layoutControl2
            // 
            this.layoutControl2.AllowCustomizationMenu = false;
            this.layoutControl2.Controls.Add(this.chkWithheldVAT);
            this.layoutControl2.Controls.Add(this.cmbPensionTaxCenter);
            this.layoutControl2.Controls.Add(this.cmbVATTaxCenter);
            this.layoutControl2.Controls.Add(this.cmbWithholdTaxCenter);
            this.layoutControl2.Controls.Add(this.dateReg);
            this.layoutControl2.Controls.Add(this.txtOccupation);
            this.layoutControl2.Controls.Add(this.cmbBusinessCategory);
            this.layoutControl2.Controls.Add(this.cmbTaxRegType);
            this.layoutControl2.Controls.Add(this.txtMachineNo);
            this.layoutControl2.Controls.Add(this.txtVatNumber);
            this.layoutControl2.Controls.Add(this.txtTinNumber);
            this.layoutControl2.Controls.Add(this.cmbBusinessEntity);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(2, 22);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsView.HighlightFocusedItem = true;
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(657, 154);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // cmbPensionTaxCenter
            // 
            this.cmbPensionTaxCenter.Location = new System.Drawing.Point(127, 187);
            this.cmbPensionTaxCenter.Name = "cmbPensionTaxCenter";
            this.cmbPensionTaxCenter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPensionTaxCenter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbPensionTaxCenter.Size = new System.Drawing.Size(506, 20);
            this.cmbPensionTaxCenter.StyleController = this.layoutControl2;
            this.cmbPensionTaxCenter.TabIndex = 16;
            // 
            // cmbVATTaxCenter
            // 
            this.cmbVATTaxCenter.Location = new System.Drawing.Point(127, 157);
            this.cmbVATTaxCenter.Name = "cmbVATTaxCenter";
            this.cmbVATTaxCenter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbVATTaxCenter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbVATTaxCenter.Size = new System.Drawing.Size(506, 20);
            this.cmbVATTaxCenter.StyleController = this.layoutControl2;
            this.cmbVATTaxCenter.TabIndex = 15;
            // 
            // cmbWithholdTaxCenter
            // 
            this.cmbWithholdTaxCenter.Location = new System.Drawing.Point(127, 127);
            this.cmbWithholdTaxCenter.Name = "cmbWithholdTaxCenter";
            this.cmbWithholdTaxCenter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbWithholdTaxCenter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbWithholdTaxCenter.Size = new System.Drawing.Size(506, 20);
            this.cmbWithholdTaxCenter.StyleController = this.layoutControl2;
            this.cmbWithholdTaxCenter.TabIndex = 14;
            // 
            // dateReg
            // 
            this.dateReg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateReg.Location = new System.Drawing.Point(127, 247);
            this.dateReg.Name = "dateReg";
            this.dateReg.ShowEthiopian = true;
            this.dateReg.ShowGregorian = true;
            this.dateReg.ShowTime = false;
            this.dateReg.Size = new System.Drawing.Size(411, 21);
            this.dateReg.TabIndex = 13;
            this.dateReg.VerticalLayout = false;
            // 
            // txtOccupation
            // 
            this.txtOccupation.Location = new System.Drawing.Point(127, 37);
            this.txtOccupation.Name = "txtOccupation";
            this.txtOccupation.Properties.Mask.EditMask = "[a-zA-Z].*";
            this.txtOccupation.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtOccupation.Size = new System.Drawing.Size(506, 20);
            this.txtOccupation.StyleController = this.layoutControl2;
            this.txtOccupation.TabIndex = 11;
            // 
            // cmbBusinessCategory
            // 
            this.cmbBusinessCategory.EditValue = "Not Listed";
            this.cmbBusinessCategory.Location = new System.Drawing.Point(127, 67);
            this.cmbBusinessCategory.Name = "cmbBusinessCategory";
            this.cmbBusinessCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbBusinessCategory.Properties.Items.AddRange(new object[] {
            "Not Listed",
            "Importer",
            "Exporter"});
            this.cmbBusinessCategory.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbBusinessCategory.Size = new System.Drawing.Size(506, 20);
            this.cmbBusinessCategory.StyleController = this.layoutControl2;
            this.cmbBusinessCategory.TabIndex = 10;
            // 
            // cmbTaxRegType
            // 
            this.cmbTaxRegType.EditValue = "VAT (Value Added Tax)";
            this.cmbTaxRegType.Location = new System.Drawing.Point(127, 217);
            this.cmbTaxRegType.Name = "cmbTaxRegType";
            this.cmbTaxRegType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTaxRegType.Properties.Items.AddRange(new object[] {
            "VAT (Value Added Tax)",
            "TOT (Turn Over Tax)",
            "Non-Tax Payer"});
            this.cmbTaxRegType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbTaxRegType.Size = new System.Drawing.Size(506, 20);
            this.cmbTaxRegType.StyleController = this.layoutControl2;
            this.cmbTaxRegType.TabIndex = 9;
            this.cmbTaxRegType.SelectedIndexChanged += new System.EventHandler(this.cmbTaxRegType_SelectedIndexChanged);
            // 
            // txtMachineNo
            // 
            this.txtMachineNo.Location = new System.Drawing.Point(127, 307);
            this.txtMachineNo.Name = "txtMachineNo";
            this.txtMachineNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMachineNo.Properties.Appearance.Options.UseFont = true;
            this.txtMachineNo.Properties.Mask.EditMask = "(\\w)*";
            this.txtMachineNo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtMachineNo.Size = new System.Drawing.Size(506, 20);
            this.txtMachineNo.StyleController = this.layoutControl2;
            this.txtMachineNo.TabIndex = 8;
            // 
            // txtVatNumber
            // 
            this.txtVatNumber.Location = new System.Drawing.Point(127, 277);
            this.txtVatNumber.Name = "txtVatNumber";
            this.txtVatNumber.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVatNumber.Properties.Appearance.Options.UseFont = true;
            this.txtVatNumber.Properties.Mask.EditMask = "\\d+";
            this.txtVatNumber.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtVatNumber.Size = new System.Drawing.Size(506, 20);
            this.txtVatNumber.StyleController = this.layoutControl2;
            this.txtVatNumber.TabIndex = 7;
            conditionValidationRule4.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule4.ErrorText = "VAT Number can not be blank";
            this.validateCompanyProf.SetValidationRule(this.txtVatNumber, conditionValidationRule4);
            // 
            // txtTinNumber
            // 
            this.txtTinNumber.Location = new System.Drawing.Point(127, 97);
            this.txtTinNumber.Name = "txtTinNumber";
            this.txtTinNumber.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTinNumber.Properties.Appearance.Options.UseFont = true;
            this.txtTinNumber.Properties.Mask.EditMask = "\\d*";
            this.txtTinNumber.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTinNumber.Properties.Mask.ShowPlaceHolders = false;
            this.txtTinNumber.Size = new System.Drawing.Size(506, 20);
            this.txtTinNumber.StyleController = this.layoutControl2;
            this.txtTinNumber.TabIndex = 5;
            conditionValidationRule5.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule5.ErrorText = "TIN can not be blank";
            this.validateCompanyProf.SetValidationRule(this.txtTinNumber, conditionValidationRule5);
            // 
            // cmbBusinessEntity
            // 
            this.cmbBusinessEntity.EditValue = "Limited Partnership";
            this.cmbBusinessEntity.Location = new System.Drawing.Point(127, 7);
            this.cmbBusinessEntity.Name = "cmbBusinessEntity";
            this.cmbBusinessEntity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBusinessEntity.Properties.Appearance.Options.UseFont = true;
            this.cmbBusinessEntity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbBusinessEntity.Properties.Items.AddRange(new object[] {
            "Sole proprietorship",
            "Partnership",
            "General Partnership",
            "Limited Partnership",
            "Share Company",
            "Private Limited Company",
            "Joint Venture",
            "Government Organization"});
            this.cmbBusinessEntity.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbBusinessEntity.Size = new System.Drawing.Size(188, 20);
            this.cmbBusinessEntity.StyleController = this.layoutControl2;
            this.cmbBusinessEntity.TabIndex = 4;
            this.cmbBusinessEntity.SelectedIndexChanged += new System.EventHandler(this.cmbBusinessEntity_SelectedIndexChanged);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutBusinessEntity,
            this.layoutTinNumber,
            this.layoutVATNumber,
            this.layoutMachineNumber,
            this.layoutTaxRegType,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutTaxRegistrationDate,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutWithheldVAT});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(640, 334);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutBusinessEntity
            // 
            this.layoutBusinessEntity.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutBusinessEntity.AppearanceItemCaption.Options.UseFont = true;
            this.layoutBusinessEntity.Control = this.cmbBusinessEntity;
            this.layoutBusinessEntity.CustomizationFormText = "Business Entity:";
            this.layoutBusinessEntity.FillControlToClientArea = false;
            this.layoutBusinessEntity.Location = new System.Drawing.Point(0, 0);
            this.layoutBusinessEntity.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutBusinessEntity.MinSize = new System.Drawing.Size(148, 30);
            this.layoutBusinessEntity.Name = "layoutBusinessEntity";
            this.layoutBusinessEntity.Size = new System.Drawing.Size(318, 30);
            this.layoutBusinessEntity.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutBusinessEntity.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutBusinessEntity.Text = "Business Entity:";
            this.layoutBusinessEntity.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutTinNumber
            // 
            this.layoutTinNumber.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutTinNumber.AppearanceItemCaption.Options.UseFont = true;
            this.layoutTinNumber.Control = this.txtTinNumber;
            this.layoutTinNumber.CustomizationFormText = "Tin Number:";
            this.layoutTinNumber.FillControlToClientArea = false;
            this.layoutTinNumber.Location = new System.Drawing.Point(0, 90);
            this.layoutTinNumber.Name = "layoutTinNumber";
            this.layoutTinNumber.Size = new System.Drawing.Size(636, 30);
            this.layoutTinNumber.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutTinNumber.Text = "TIN:";
            this.layoutTinNumber.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutVATNumber
            // 
            this.layoutVATNumber.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutVATNumber.AppearanceItemCaption.Options.UseFont = true;
            this.layoutVATNumber.Control = this.txtVatNumber;
            this.layoutVATNumber.CustomizationFormText = "Vat Number:";
            this.layoutVATNumber.FillControlToClientArea = false;
            this.layoutVATNumber.Location = new System.Drawing.Point(0, 270);
            this.layoutVATNumber.Name = "layoutVATNumber";
            this.layoutVATNumber.Size = new System.Drawing.Size(636, 30);
            this.layoutVATNumber.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutVATNumber.Text = "VAT Number:";
            this.layoutVATNumber.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutMachineNumber
            // 
            this.layoutMachineNumber.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutMachineNumber.AppearanceItemCaption.Options.UseFont = true;
            this.layoutMachineNumber.Control = this.txtMachineNo;
            this.layoutMachineNumber.CustomizationFormText = "Cash Register Machine Number:";
            this.layoutMachineNumber.FillControlToClientArea = false;
            this.layoutMachineNumber.Location = new System.Drawing.Point(0, 300);
            this.layoutMachineNumber.Name = "layoutMachineNumber";
            this.layoutMachineNumber.Size = new System.Drawing.Size(636, 30);
            this.layoutMachineNumber.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutMachineNumber.Text = "Machine Number:";
            this.layoutMachineNumber.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutTaxRegType
            // 
            this.layoutTaxRegType.Control = this.cmbTaxRegType;
            this.layoutTaxRegType.CustomizationFormText = "Tax Registration Type:";
            this.layoutTaxRegType.Location = new System.Drawing.Point(0, 210);
            this.layoutTaxRegType.Name = "layoutTaxRegType";
            this.layoutTaxRegType.Size = new System.Drawing.Size(636, 30);
            this.layoutTaxRegType.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutTaxRegType.Text = "Tax Registration Type:";
            this.layoutTaxRegType.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.cmbBusinessCategory;
            this.layoutControlItem4.CustomizationFormText = "Business Category:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(636, 30);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem4.Text = "Business Category:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtOccupation;
            this.layoutControlItem5.CustomizationFormText = "Occupation:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(636, 30);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem5.Text = "Occupation:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutTaxRegistrationDate
            // 
            this.layoutTaxRegistrationDate.Control = this.dateReg;
            this.layoutTaxRegistrationDate.CustomizationFormText = "Tax Registration Date:";
            this.layoutTaxRegistrationDate.Location = new System.Drawing.Point(0, 240);
            this.layoutTaxRegistrationDate.MaxSize = new System.Drawing.Size(588, 30);
            this.layoutTaxRegistrationDate.MinSize = new System.Drawing.Size(588, 30);
            this.layoutTaxRegistrationDate.Name = "layoutTaxRegistrationDate";
            this.layoutTaxRegistrationDate.Size = new System.Drawing.Size(636, 30);
            this.layoutTaxRegistrationDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutTaxRegistrationDate.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutTaxRegistrationDate.Text = "Tax Registration Date:";
            this.layoutTaxRegistrationDate.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.cmbWithholdTaxCenter;
            this.layoutControlItem12.CustomizationFormText = "Withholding Tax Center:";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(636, 30);
            this.layoutControlItem12.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem12.Text = "Withholding Tax Center:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.cmbVATTaxCenter;
            this.layoutControlItem13.CustomizationFormText = "VAT Tax Center:";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(636, 30);
            this.layoutControlItem13.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem13.Text = "VAT Tax Center:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.cmbPensionTaxCenter;
            this.layoutControlItem14.CustomizationFormText = "Pension Tax Center:";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 180);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(636, 30);
            this.layoutControlItem14.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem14.Text = "Pension Tax Center:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(117, 13);
            // 
            // txtWebAddress
            // 
            this.txtWebAddress.Location = new System.Drawing.Point(141, 350);
            this.txtWebAddress.Name = "txtWebAddress";
            this.txtWebAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWebAddress.Properties.Appearance.Options.UseFont = true;
            this.txtWebAddress.Properties.Mask.EditMask = "www\\.[a-zA-Z]+\\.[a-zA-Z]{2,3}[\\.]?[a-zA-Z]{0,3}";
            this.txtWebAddress.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.txtWebAddress.Size = new System.Drawing.Size(334, 20);
            this.txtWebAddress.StyleController = this.layoutControl1;
            this.txtWebAddress.TabIndex = 11;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(141, 320);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Properties.Appearance.Options.UseFont = true;
            this.txtEmail.Properties.Mask.EditMask = "[\\w-\\.]+@[a-zA-Z]+\\.[a-zA-Z]{2,3}[\\.]?[a-zA-Z]{0,3}";
            this.txtEmail.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.txtEmail.Size = new System.Drawing.Size(334, 20);
            this.txtEmail.StyleController = this.layoutControl1;
            this.txtEmail.TabIndex = 10;
            // 
            // txtFaxNumber
            // 
            this.txtFaxNumber.Location = new System.Drawing.Point(141, 290);
            this.txtFaxNumber.Name = "txtFaxNumber";
            this.txtFaxNumber.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFaxNumber.Properties.Appearance.Options.UseFont = true;
            this.txtFaxNumber.Properties.Mask.EditMask = "(\\+251)\\d\\d\\d-\\d\\d-\\d\\d-\\d\\d";
            this.txtFaxNumber.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.txtFaxNumber.Size = new System.Drawing.Size(334, 20);
            this.txtFaxNumber.StyleController = this.layoutControl1;
            this.txtFaxNumber.TabIndex = 9;
            // 
            // txtOfficeTele
            // 
            this.txtOfficeTele.Location = new System.Drawing.Point(141, 230);
            this.txtOfficeTele.Name = "txtOfficeTele";
            this.txtOfficeTele.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOfficeTele.Properties.Appearance.Options.UseFont = true;
            this.txtOfficeTele.Properties.Mask.EditMask = "(\\+251)\\d\\d\\d-\\d\\d-\\d\\d-\\d\\d";
            this.txtOfficeTele.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
            this.txtOfficeTele.Size = new System.Drawing.Size(334, 20);
            this.txtOfficeTele.StyleController = this.layoutControl1;
            this.txtOfficeTele.TabIndex = 8;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "Company Telephone can not be blank";
            this.validateCompanyProf.SetValidationRule(this.txtOfficeTele, conditionValidationRule1);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnClear);
            this.panelControl1.Controls.Add(this.btnBrowse);
            this.panelControl1.Location = new System.Drawing.Point(482, 161);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(186, 30);
            this.panelControl1.TabIndex = 7;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClear.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Appearance.Options.UseFont = true;
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.Location = new System.Drawing.Point(100, 8);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(64, 20);
            this.btnClear.TabIndex = 0;
            this.btnClear.Text = "Cl&ear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBrowse.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowse.Appearance.Options.UseFont = true;
            this.btnBrowse.Image = ((System.Drawing.Image)(resources.GetObject("btnBrowse.Image")));
            this.btnBrowse.Location = new System.Drawing.Point(5, 8);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(73, 20);
            this.btnBrowse.TabIndex = 0;
            this.btnBrowse.Text = "&Browse";
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Location = new System.Drawing.Point(141, 10);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCompanyName.Properties.Appearance.Options.UseFont = true;
            this.txtCompanyName.Properties.Mask.EditMask = "[a-zA-Z].*";
            this.txtCompanyName.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtCompanyName.Size = new System.Drawing.Size(334, 20);
            this.txtCompanyName.StyleController = this.layoutControl1;
            this.txtCompanyName.TabIndex = 4;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "Company Name can not be blank";
            this.validateCompanyProf.SetValidationRule(this.txtCompanyName, conditionValidationRule2);
            // 
            // picLogo
            // 
            this.picLogo.Location = new System.Drawing.Point(482, 22);
            this.picLogo.Name = "picLogo";
            this.picLogo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picLogo.Size = new System.Drawing.Size(186, 135);
            this.picLogo.StyleController = this.layoutControl1;
            this.picLogo.TabIndex = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem1,
            this.layoutLogoPanel,
            this.layoutLogo,
            this.layoutCompanyName,
            this.layoutOfficeTelephone,
            this.layoutFaxNumber,
            this.layoutEmail,
            this.layoutWebsite,
            this.layoutControlItem2,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(675, 598);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.panelControl2;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 552);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(104, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(665, 36);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.grpTaxDetails;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 370);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(665, 182);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutLogoPanel
            // 
            this.layoutLogoPanel.Control = this.panelControl1;
            this.layoutLogoPanel.CustomizationFormText = "Logo Panel";
            this.layoutLogoPanel.FillControlToClientArea = false;
            this.layoutLogoPanel.Location = new System.Drawing.Point(475, 154);
            this.layoutLogoPanel.MaxSize = new System.Drawing.Size(0, 34);
            this.layoutLogoPanel.MinSize = new System.Drawing.Size(104, 24);
            this.layoutLogoPanel.Name = "layoutLogoPanel";
            this.layoutLogoPanel.Size = new System.Drawing.Size(190, 216);
            this.layoutLogoPanel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutLogoPanel.Text = "Logo Panel";
            this.layoutLogoPanel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutLogoPanel.TextToControlDistance = 0;
            this.layoutLogoPanel.TextVisible = false;
            // 
            // layoutLogo
            // 
            this.layoutLogo.Control = this.picLogo;
            this.layoutLogo.CustomizationFormText = "Logo:";
            this.layoutLogo.FillControlToClientArea = false;
            this.layoutLogo.Location = new System.Drawing.Point(475, 0);
            this.layoutLogo.MaxSize = new System.Drawing.Size(0, 154);
            this.layoutLogo.MinSize = new System.Drawing.Size(34, 154);
            this.layoutLogo.Name = "layoutLogo";
            this.layoutLogo.Size = new System.Drawing.Size(190, 154);
            this.layoutLogo.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutLogo.Text = "Logo:";
            this.layoutLogo.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutLogo.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutLogo.TextSize = new System.Drawing.Size(30, 13);
            this.layoutLogo.TextToControlDistance = 2;
            // 
            // layoutCompanyName
            // 
            this.layoutCompanyName.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutCompanyName.AppearanceItemCaption.Options.UseFont = true;
            this.layoutCompanyName.Control = this.txtCompanyName;
            this.layoutCompanyName.CustomizationFormText = "Company Name:";
            this.layoutCompanyName.FillControlToClientArea = false;
            this.layoutCompanyName.Location = new System.Drawing.Point(0, 0);
            this.layoutCompanyName.Name = "layoutCompanyName";
            this.layoutCompanyName.Size = new System.Drawing.Size(475, 30);
            this.layoutCompanyName.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutCompanyName.Text = "Company Name (English):";
            this.layoutCompanyName.TextSize = new System.Drawing.Size(128, 13);
            // 
            // layoutOfficeTelephone
            // 
            this.layoutOfficeTelephone.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutOfficeTelephone.AppearanceItemCaption.Options.UseFont = true;
            this.layoutOfficeTelephone.Control = this.txtOfficeTele;
            this.layoutOfficeTelephone.CustomizationFormText = "Office Telephone:";
            this.layoutOfficeTelephone.FillControlToClientArea = false;
            this.layoutOfficeTelephone.Location = new System.Drawing.Point(0, 220);
            this.layoutOfficeTelephone.Name = "layoutOfficeTelephone";
            this.layoutOfficeTelephone.Size = new System.Drawing.Size(475, 30);
            this.layoutOfficeTelephone.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutOfficeTelephone.Text = "Office Telephone:";
            this.layoutOfficeTelephone.TextSize = new System.Drawing.Size(128, 13);
            // 
            // layoutFaxNumber
            // 
            this.layoutFaxNumber.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutFaxNumber.AppearanceItemCaption.Options.UseFont = true;
            this.layoutFaxNumber.Control = this.txtFaxNumber;
            this.layoutFaxNumber.CustomizationFormText = "Fax Number:";
            this.layoutFaxNumber.FillControlToClientArea = false;
            this.layoutFaxNumber.Location = new System.Drawing.Point(0, 280);
            this.layoutFaxNumber.Name = "layoutFaxNumber";
            this.layoutFaxNumber.Size = new System.Drawing.Size(475, 30);
            this.layoutFaxNumber.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutFaxNumber.Text = "Fax Number:";
            this.layoutFaxNumber.TextSize = new System.Drawing.Size(128, 13);
            // 
            // layoutEmail
            // 
            this.layoutEmail.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutEmail.AppearanceItemCaption.Options.UseFont = true;
            this.layoutEmail.Control = this.txtEmail;
            this.layoutEmail.CustomizationFormText = "E-mail Address:";
            this.layoutEmail.FillControlToClientArea = false;
            this.layoutEmail.Location = new System.Drawing.Point(0, 310);
            this.layoutEmail.Name = "layoutEmail";
            this.layoutEmail.Size = new System.Drawing.Size(475, 30);
            this.layoutEmail.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutEmail.Text = "E-mail Address:";
            this.layoutEmail.TextSize = new System.Drawing.Size(128, 13);
            // 
            // layoutWebsite
            // 
            this.layoutWebsite.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutWebsite.AppearanceItemCaption.Options.UseFont = true;
            this.layoutWebsite.Control = this.txtWebAddress;
            this.layoutWebsite.CustomizationFormText = "Website Address:";
            this.layoutWebsite.FillControlToClientArea = false;
            this.layoutWebsite.Location = new System.Drawing.Point(0, 340);
            this.layoutWebsite.Name = "layoutWebsite";
            this.layoutWebsite.Size = new System.Drawing.Size(475, 30);
            this.layoutWebsite.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutWebsite.Text = "Website Address:";
            this.layoutWebsite.TextSize = new System.Drawing.Size(128, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtMobilePhone;
            this.layoutControlItem2.CustomizationFormText = "Mobile Phone:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 250);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(475, 30);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem2.Text = "Mobile Phone:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(128, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtCompanyNameAmh;
            this.layoutControlItem6.CustomizationFormText = "Company Name (Amharic):";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(475, 30);
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem6.Text = "Company Name (Amharic):";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(128, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtRegion;
            this.layoutControlItem7.CustomizationFormText = "Region:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem7.Size = new System.Drawing.Size(475, 32);
            this.layoutControlItem7.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem7.Text = "Region:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(128, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtZone;
            this.layoutControlItem8.CustomizationFormText = "Zone:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 92);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem8.Size = new System.Drawing.Size(475, 32);
            this.layoutControlItem8.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem8.Text = "Zone:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(128, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtWoreda;
            this.layoutControlItem9.CustomizationFormText = "Woreda:";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 124);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem9.Size = new System.Drawing.Size(475, 32);
            this.layoutControlItem9.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem9.Text = "Woreda:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(128, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txtKebele;
            this.layoutControlItem10.CustomizationFormText = "Kebele:";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 156);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem10.Size = new System.Drawing.Size(475, 32);
            this.layoutControlItem10.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem10.Text = "Kebele:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(128, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txtHouseNo;
            this.layoutControlItem11.CustomizationFormText = "House Number:";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 188);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem11.Size = new System.Drawing.Size(475, 32);
            this.layoutControlItem11.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem11.Text = "House Number:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(128, 13);
            // 
            // validateCompanyProf
            // 
            this.validateCompanyProf.ValidateHiddenControls = false;
            // 
            // chkWithheldVAT
            // 
            this.chkWithheldVAT.Location = new System.Drawing.Point(325, 7);
            this.chkWithheldVAT.Name = "chkWithheldVAT";
            this.chkWithheldVAT.Properties.Caption = "Is VAT Agent";
            this.chkWithheldVAT.Size = new System.Drawing.Size(308, 19);
            this.chkWithheldVAT.StyleController = this.layoutControl2;
            this.chkWithheldVAT.TabIndex = 17;
            // 
            // layoutWithheldVAT
            // 
            this.layoutWithheldVAT.Control = this.chkWithheldVAT;
            this.layoutWithheldVAT.CustomizationFormText = "Withheld VAT";
            this.layoutWithheldVAT.Location = new System.Drawing.Point(318, 0);
            this.layoutWithheldVAT.Name = "layoutWithheldVAT";
            this.layoutWithheldVAT.Size = new System.Drawing.Size(318, 30);
            this.layoutWithheldVAT.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutWithheldVAT.Text = "Withheld VAT";
            this.layoutWithheldVAT.TextSize = new System.Drawing.Size(0, 0);
            this.layoutWithheldVAT.TextToControlDistance = 0;
            this.layoutWithheldVAT.TextVisible = false;
            // 
            // RegisterCompanyProfile
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(675, 598);
            this.Controls.Add(this.layoutControl1);
            this.MaximizeBox = false;
            this.Name = "RegisterCompanyProfile";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Register Company Profile";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtHouseNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKebele.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWoreda.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyNameAmh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobilePhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpTaxDetails)).EndInit();
            this.grpTaxDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbPensionTaxCenter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVATTaxCenter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbWithholdTaxCenter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOccupation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBusinessCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTaxRegType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMachineNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVatNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBusinessEntity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBusinessEntity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTinNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVATNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMachineNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTaxRegType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTaxRegistrationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFaxNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOfficeTele.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLogoPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOfficeTelephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutFaxNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWebsite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validateCompanyProf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWithheldVAT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWithheldVAT)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.GroupControl grpTaxDetails;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.TextEdit txtMachineNo;
        private DevExpress.XtraEditors.TextEdit txtVatNumber;
        private DevExpress.XtraEditors.TextEdit txtTinNumber;
        private DevExpress.XtraEditors.ComboBoxEdit cmbBusinessEntity;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutBusinessEntity;
        private DevExpress.XtraLayout.LayoutControlItem layoutTinNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutVATNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutMachineNumber;
        private DevExpress.XtraEditors.TextEdit txtWebAddress;
        private DevExpress.XtraEditors.TextEdit txtEmail;
        private DevExpress.XtraEditors.TextEdit txtFaxNumber;
        private DevExpress.XtraEditors.TextEdit txtOfficeTele;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private DevExpress.XtraEditors.SimpleButton btnBrowse;
        private DevExpress.XtraEditors.TextEdit txtCompanyName;
        private DevExpress.XtraEditors.PictureEdit picLogo;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbTaxRegType;
        private DevExpress.XtraLayout.LayoutControlItem layoutTaxRegType;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.ComboBoxEdit cmbBusinessCategory;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutLogoPanel;
        private DevExpress.XtraLayout.LayoutControlItem layoutLogo;
        private DevExpress.XtraLayout.LayoutControlItem layoutCompanyName;
        private DevExpress.XtraLayout.LayoutControlItem layoutOfficeTelephone;
        private DevExpress.XtraLayout.LayoutControlItem layoutFaxNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutEmail;
        private DevExpress.XtraLayout.LayoutControlItem layoutWebsite;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validateCompanyProf;
        private DevExpress.XtraEditors.TextEdit txtMobilePhone;
        private DevExpress.XtraEditors.TextEdit txtOccupation;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit txtCompanyNameAmh;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.TextEdit txtWoreda;
        private DevExpress.XtraEditors.TextEdit txtZone;
        private DevExpress.XtraEditors.TextEdit txtRegion;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.TextEdit txtHouseNo;
        private DevExpress.XtraEditors.TextEdit txtKebele;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private BIZNET.iERP.Client.BNDualCalendar dateReg;
        private DevExpress.XtraLayout.LayoutControlItem layoutTaxRegistrationDate;
        private DevExpress.XtraEditors.ComboBoxEdit cmbWithholdTaxCenter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.ComboBoxEdit cmbVATTaxCenter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPensionTaxCenter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.CheckEdit chkWithheldVAT;
        private DevExpress.XtraLayout.LayoutControlItem layoutWithheldVAT;
    }
}