﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;
using System.IO;
using System.Drawing.Imaging;
using DevExpress.XtraEditors.DXErrorProvider;
using INTAPS.UI.ButtonGrid;

namespace BIZNET.iERP.Client
{
    public partial class RegisterCompanyProfile : XtraForm
    {
        public RegisterCompanyProfile()
        {
            InitializeComponent();
            txtCompanyName.LostFocus += Control_LostFocus;
            txtOfficeTele.LostFocus += Control_LostFocus;
            txtTinNumber.LostFocus += Control_LostFocus;
            txtVatNumber.LostFocus += Control_LostFocus;
            INTAPS.Ethiopic.PowerGeezMapper mapper = new INTAPS.Ethiopic.PowerGeezMapper();
            mapper.AddControl(txtCompanyNameAmh.Controls[0]);
            InitializeTINValidation();
            LoadTaxCenters();
            PopulateCompanyProfile();
        }
        private void InitializeTINValidation()
        {
            TINValidationRule tinValidation = new TINValidationRule();
            tinValidation.ErrorText = "TIN cannot be blank and must be 10 digits";
            tinValidation.ErrorType = ErrorType.Default;
            validateCompanyProf.SetValidationRule(txtTinNumber, tinValidation);
        }
        private void LoadTaxCenters()
        {
            TaxCenter[] taxCenter = iERPTransactionClient.GetTaxCenters();
            foreach (TaxCenter center in taxCenter)
            {
                cmbWithholdTaxCenter.Properties.Items.Add(center);
                cmbVATTaxCenter.Properties.Items.Add(center);
                cmbPensionTaxCenter.Properties.Items.Add(center);
            }
            cmbWithholdTaxCenter.SelectedIndex = 0;
            cmbVATTaxCenter.SelectedIndex = 0;
            cmbPensionTaxCenter.SelectedIndex = 0;
        }
        private void PopulateCompanyProfile()
        {
            object[] vals = iERPTransactionClient.GetSystemParameters(new string[] { "companyProfile" });
            if (vals != null)
            {
                if (vals.Length > 0 && vals[0]!=null)
                {
                    CompanyProfile loadedProfile = (CompanyProfile)vals[0];
                    if (loadedProfile.Logo != null)
                    {
                        using (MemoryStream stream = new MemoryStream(loadedProfile.Logo))
                        {
                            picLogo.Image = Image.FromStream(stream);
                        }
                    }
                    txtCompanyName.Text = loadedProfile.Name;
                    txtCompanyNameAmh.Text = loadedProfile.AmharicName;
                    txtOfficeTele.Text = loadedProfile.Telephone;
                    txtMobilePhone.Text = loadedProfile.MobilePhone;
                    txtRegion.Text = loadedProfile.Region;
                    txtZone.Text = loadedProfile.Zone;
                    txtWoreda.Text = loadedProfile.Woreda;
                    txtKebele.Text = loadedProfile.Kebele;
                    txtHouseNo.Text = loadedProfile.HouseNumber;
                    if (loadedProfile.WithholdingTaxCenter != 0)
                        SetWithholdingTaxCenter(loadedProfile.WithholdingTaxCenter);
                    if (loadedProfile.VATTaxCenter != 0)
                        SetVATTaxCenter(loadedProfile.VATTaxCenter);
                    if (loadedProfile.PensionTaxCenter != 0)
                        SetPensionTaxCenter(loadedProfile.PensionTaxCenter);
                    txtEmail.Text = loadedProfile.Email;
                    txtWebAddress.Text = loadedProfile.WebSite;
                    txtFaxNumber.Text = loadedProfile.Fax;
                    cmbBusinessEntity.SelectedIndex = (int)loadedProfile.BusinessEntity;
                    cmbBusinessCategory.SelectedIndex = (int)loadedProfile.BusinessCategory;
                    txtOccupation.Text = loadedProfile.Occupation;
                    txtTinNumber.Text = loadedProfile.TIN;
                    cmbTaxRegType.SelectedIndex = (int)loadedProfile.TaxRegistrationType;
                    dateReg.DateTime = loadedProfile.TaxRegistrationDate;
                    txtVatNumber.Text = loadedProfile.VATNumber;
                    txtMachineNo.Text = loadedProfile.MachineNumber;
                    chkWithheldVAT.Checked = loadedProfile.isVATAgent;
                }
            }

        }
        private void SetWithholdingTaxCenter(int taxCenter)
        {
            int i = 0;
            foreach (TaxCenter center in cmbWithholdTaxCenter.Properties.Items)
            {
                if (center.ID == taxCenter)
                {
                    cmbWithholdTaxCenter.SelectedIndex = i;
                    break;
                }
                i++;
            }
        }
        private void SetVATTaxCenter(int taxCenter)
        {
            int i = 0;
            foreach (TaxCenter center in cmbVATTaxCenter.Properties.Items)
            {
                if (center.ID == taxCenter)
                {
                    cmbVATTaxCenter.SelectedIndex = i;
                    break;
                }
                i++;
            }
        }
        private void SetPensionTaxCenter(int taxCenter)
        {
            int i = 0;
            foreach (TaxCenter center in cmbPensionTaxCenter.Properties.Items)
            {
                if (center.ID == taxCenter)
                {
                    cmbPensionTaxCenter.SelectedIndex = i;
                    break;
                }
                i++;
            }
        }
        void Control_LostFocus(object sender, EventArgs e)
        {
            validateCompanyProf.Validate((Control)sender);
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                picLogo.LoadImage();
            }
            catch (Exception ex)
            {
              MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            picLogo.Image = null;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (validateCompanyProf.Validate())
                {
                    if (DateTime.Compare(dateReg.DateTime, DateTime.Now) > 0)
                    {
                        MessageBox.ShowErrorMessage("You cannot make Tax Registration Date greater than current date");
                        return;
                    }
                    CompanyProfile companyProfile = new CompanyProfile();
                    if (picLogo.Image != null)
                    {
                        MemoryStream stream = ProcessLogoImageToStream();
                        companyProfile.Logo = stream.ToArray();

                    }
                    companyProfile.Name = txtCompanyName.Text;
                    companyProfile.AmharicName = txtCompanyNameAmh.Text;
                    companyProfile.Region = txtRegion.Text;
                    companyProfile.Zone = txtZone.Text;
                    companyProfile.Woreda = txtWoreda.Text;
                    companyProfile.Kebele = txtKebele.Text;
                    companyProfile.HouseNumber = txtHouseNo.Text;
                    companyProfile.WithholdingTaxCenter = ((TaxCenter)cmbWithholdTaxCenter.SelectedItem).ID;
                    companyProfile.VATTaxCenter = ((TaxCenter)cmbVATTaxCenter.SelectedItem).ID;
                    companyProfile.PensionTaxCenter = ((TaxCenter)cmbPensionTaxCenter.SelectedItem).ID;
                    companyProfile.Telephone = txtOfficeTele.Text;
                    companyProfile.MobilePhone = txtMobilePhone.Text;
                    companyProfile.Fax = txtFaxNumber.Text;
                    companyProfile.Email = txtEmail.Text;
                    companyProfile.WebSite = txtWebAddress.Text;
                    companyProfile.BusinessEntity = (BusinessEntity)cmbBusinessEntity.SelectedIndex;
                    companyProfile.BusinessCategory = (BusinessCategory)cmbBusinessCategory.SelectedIndex;
                    companyProfile.Occupation = txtOccupation.Text;
                    companyProfile.TIN = txtTinNumber.Text;
                    companyProfile.TaxRegistrationType = (TaxRegisrationType)cmbTaxRegType.SelectedIndex;
                    companyProfile.TaxRegistrationDate = dateReg.DateTime.Date;
                    companyProfile.VATNumber = txtVatNumber.Text;
                    companyProfile.MachineNumber = txtMachineNo.Text;
                    companyProfile.isVATAgent = chkWithheldVAT.Checked;
                    iERPTransactionClient.SetSystemParameters(new string[] { "companyProfile" }, new object[] { companyProfile });
                    MessageBox.ShowSuccessMessage("Company Profile successfully saved!");
                    RefreshMainButtonGrid();
                    Close();
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please check the errors marked in red and try again!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error occurred trying to save company profile.\n" + ex.Message);
            }
        }

        private MemoryStream ProcessLogoImageToStream()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                int newWidth = 128, newHeight = 128;
                if (picLogo.Image.Width > picLogo.Image.Height)
                {
                    newWidth = 128;
                    newHeight = 128 * picLogo.Image.Height / picLogo.Image.Width;
                }
                else if (picLogo.Image.Width < picLogo.Image.Height)
                {
                    newHeight = 128;
                    newWidth = 128 * picLogo.Image.Width / picLogo.Image.Height;
                }
                using (Bitmap newPic = new Bitmap(picLogo.Image, new Size(newWidth, newHeight)))
                {
                    newPic.Save(stream, ImageFormat.Png);
                }
                return stream;
            }
           
        }

        private void cmbTaxRegType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTaxRegType.SelectedIndex != 0)
            {
                layoutVATNumber.HideToCustomization();
                txtVatNumber.Text = "";
            }
            else
            {
                if (layoutVATNumber.IsHidden)
                {
                    layoutVATNumber.RestoreFromCustomization(layoutTaxRegType, InsertType.Bottom);
                }
            }
            if (cmbTaxRegType.SelectedIndex == 2)
            {
                layoutMachineNumber.HideToCustomization();
                layoutTaxRegistrationDate.HideToCustomization();
            }
            else
            {
                if(layoutTaxRegistrationDate.IsHidden)
                    layoutTaxRegistrationDate.RestoreFromCustomization(layoutTaxRegType, InsertType.Bottom);
                if (cmbTaxRegType.SelectedIndex == 0)
                {
                    if (layoutMachineNumber.IsHidden)
                        layoutMachineNumber.RestoreFromCustomization(layoutVATNumber, InsertType.Bottom);
                }
                else
                {
                    if (layoutMachineNumber.IsHidden)
                        layoutMachineNumber.RestoreFromCustomization(layoutTaxRegType, InsertType.Bottom);
                }
            }
        }

        private void cmbBusinessEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbBusinessEntity.SelectedIndex == 7) //Government organization
            {
                if (layoutWithheldVAT.IsHidden)
                    layoutWithheldVAT.RestoreFromCustomization(layoutBusinessEntity, InsertType.Right);
            }
            else
            {
                chkWithheldVAT.Checked = false;
                layoutWithheldVAT.HideToCustomization();
            }
        }

        private  void RefreshMainButtonGrid()
        {
            foreach (Control control in INTAPS.UI.UIFormApplicationBase.MainForm.Controls)
            {

                if (control is SplitContainerControl)
                {
                    Control buttonGrid = (ButtonGrid)((SplitContainerControl)control).Panel1.Controls[0];
                    ((ButtonGrid)buttonGrid).UpdateLayout();
                }
            }
        }
    }
}