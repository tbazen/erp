using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.UI.ButtonGrid;
using INTAPS.Payroll.Client;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Drawing;

namespace BIZNET.iERP.Client
{
    public class StaffMembersList : ObjectBGList<Employee>
    {
        ButtonGrid buttonGrid;
        public StaffMembersList(ButtonGrid grid, ButtonGridItem parent)
            : base(grid, parent)
        {
            buttonGrid = grid;
        }
        public override bool Searchable
        {
            get { return true; }
        }

        protected virtual bool CanIncludeEmployee(Employee e)
        {
            return true;
        }
        public override List<Employee> LoadData(string query)
        {
            List<Employee> ret = new List<Employee>();
            int N;
            Employee[] allEmployees = null;
            if (String.IsNullOrEmpty(query))
                allEmployees = PayrollClient.SearchEmployee(DateTime.Now.Ticks, -1, "", false, INTAPS.Payroll.EmployeeSearchType.All, 0, 5, out N);
            else
            {
                allEmployees = PayrollClient.SearchEmployee(DateTime.Now.Ticks, -1, query, false, INTAPS.Payroll.EmployeeSearchType.All, 0, 5, out N);

            }
            foreach (Employee employee in allEmployees)
            {
                if (employee.status != EmployeeStatus.Fired)
                {
                    if (CanIncludeEmployee(employee))
                    {
                        ret.Add(employee);
                    }
                }
            }
            return ret;

        }

        public override ButtonGridItem[] CreateButton(Employee employee)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("emp"+employee.employeeID, employee.EmployeeNameID, Properties.Resources.Employee_2);
            //ButtonGridHelper.SetButtonBackColor(button, Color.LightBlue);
            return new ButtonGridItem[] { button };
        }

        public override string searchLabel
        {
            get { return "Employee:"; }
        }
    }
    public class StaffWithExpenseAdvanceList : StaffMembersList
    {
        BizNetPaymentMethod payMethod;
        public StaffWithExpenseAdvanceList(ButtonGrid grid, ButtonGridItem parent,BizNetPaymentMethod paymentMethod)
            : base(grid, parent)
        {
            payMethod = paymentMethod;
        }
        public StaffWithExpenseAdvanceList(ButtonGrid grid, ButtonGridItem parent)
            : base(grid, parent)
        {
            
        }
         
        private int employeeID;
        protected override bool CanIncludeEmployee(Employee e)
        {
            int staffExpenseAdvanceParentAccount = (int)PayrollClient.GetSystemParameters(new string[] { "staffExpenseAdvanceAccountID" })[0];
            if (staffExpenseAdvanceParentAccount == -1)
                return false;
            foreach (int ac in e.accounts)
            {
                Account acnt = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(ac);
                if (acnt != null)
                {
                    if (acnt.PID == staffExpenseAdvanceParentAccount)
                    {
                        employeeID = e.id;
                        return true;
                    }
                }
            }
            return false;
        }

        public override ButtonGridItem[] CreateButton(Employee employee)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("emp" + employee.employeeID, employee.EmployeeNameID, Properties.Resources.Employee_2);
            button.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(105), payMethod, employee.id);
            return new ButtonGridItem[] { button };

          //  ButtonGridHelper.SetHeaderModeText(item, employee.EmployeeNameID + " - Expense Advance");
        }
        
    }
    public class StaffWithSalaryAdvanceList : StaffMembersList
    {
        BizNetPaymentMethod payMethod;
        public StaffWithSalaryAdvanceList(ButtonGrid grid, ButtonGridItem parent,BizNetPaymentMethod paymentMethod)
            : base(grid, parent)
        {
            payMethod = paymentMethod;
        }
        public StaffWithSalaryAdvanceList(ButtonGrid grid, ButtonGridItem parent)
            : base(grid, parent)
        {
            
        }
         
        private int employeeID;
        protected override bool CanIncludeEmployee(Employee e)
       {
            int staffShortTermLoanParentAccount = (int)PayrollClient.GetSystemParameters(new string[] { "staffSalaryAdvanceAccountID" })[0];
            if (staffShortTermLoanParentAccount == -1)
                return false;
            foreach (int ac in e.accounts)
            {
                Account acnt = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(ac);
                if (acnt != null)
                {
                    if (acnt.PID == staffShortTermLoanParentAccount)
                    {
                        employeeID = e.id;
                        return true;
                    }
                }
            }
            return false;
        }

        public override ButtonGridItem[] CreateButton(Employee employee)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("emp" + employee.employeeID, employee.EmployeeNameID, Properties.Resources.Employee_2);
            button.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(106), payMethod, employee.id);
            return new ButtonGridItem[] { button };
        }

    }

    public class StaffWithLongTermLoanList : StaffMembersList
    {
        BizNetPaymentMethod payMethod;
        public StaffWithLongTermLoanList(ButtonGrid grid, ButtonGridItem parent,BizNetPaymentMethod paymentMethod)
            : base(grid, parent)
        {
            payMethod = paymentMethod;
        }
        public StaffWithLongTermLoanList(ButtonGrid grid, ButtonGridItem parent)
            : base(grid, parent)
        {
            
        }
         
        private int employeeID;
        protected override bool CanIncludeEmployee(Employee e)
        {
            int staffLongTermLoanParentAccount = (int)PayrollClient.GetSystemParameters(new string[] { "staffLongTermLoanAccountID" })[0];
            if (staffLongTermLoanParentAccount == -1)
                return false;
            foreach (int ac in e.accounts)
            {
                Account acnt = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(ac);
                if (acnt != null)
                {
                    if (acnt.PID == staffLongTermLoanParentAccount)
                    {
                        employeeID = e.id;
                        return true;
                    }
                }
            }
            return false;
        }

        public override ButtonGridItem[] CreateButton(Employee employee)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("emp" + employee.employeeID, employee.EmployeeNameID, Properties.Resources.Employee_2);
            button.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(107), payMethod, employee.id);
            return new ButtonGridItem[] { button };
        }

    }
    public class StaffWithLongTermLoanWithServiceChargeList : StaffMembersList
    {
        BizNetPaymentMethod payMethod;
        public StaffWithLongTermLoanWithServiceChargeList(ButtonGrid grid, ButtonGridItem parent, BizNetPaymentMethod paymentMethod)
            : base(grid, parent)
        {
            payMethod = paymentMethod;
        }
        public StaffWithLongTermLoanWithServiceChargeList(ButtonGrid grid, ButtonGridItem parent)
            : base(grid, parent)
        {

        }

        private int employeeID;
        protected override bool CanIncludeEmployee(Employee e)
        {
            int staffLongTermLoanParentAccount = (int)PayrollClient.GetSystemParameters(new string[] { "staffLongTermLoanAccountID" })[0];
            if (staffLongTermLoanParentAccount == -1)
                return false;
            foreach (int ac in e.accounts)
            {
                Account acnt = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(ac);
                if (acnt != null)
                {
                    if (acnt.PID == staffLongTermLoanParentAccount)
                    {
                        employeeID = e.id;
                        return true;
                    }
                }
            }
            return false;
        }

        public override ButtonGridItem[] CreateButton(Employee employee)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("emp" + employee.employeeID, employee.EmployeeNameID, Properties.Resources.Employee_2);
            button.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(Constants.LONGTERMLOAN_WITHSERVICECHARGE_DOCUMENTTYPEID), payMethod, employee.id);
            return new ButtonGridItem[] { button };
        }

    }

    public class StaffWithUnclaimedSalaryList : StaffMembersList
    {
        BizNetPaymentMethod payMethod;
        public StaffWithUnclaimedSalaryList(ButtonGrid grid, ButtonGridItem parent,BizNetPaymentMethod paymentMethod)
            : base(grid, parent)
        {
            payMethod = paymentMethod;
        }
        public StaffWithUnclaimedSalaryList(ButtonGrid grid, ButtonGridItem parent)
            : base(grid, parent)
        {
            
        }
         
        private int employeeID;
        protected override bool CanIncludeEmployee(Employee e)
        {
            int staffWithUnclaimedSalaryParentAccount = (int)PayrollClient.GetSystemParameters(new string[] { "UnclaimedSalaryAccount" })[0];
            if (staffWithUnclaimedSalaryParentAccount == -1)
                return false;
            foreach (int ac in e.accounts)
            {
                Account acnt = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(ac);
                if (acnt != null)
                {
                    if (acnt.PID == staffWithUnclaimedSalaryParentAccount)
                    {
                        employeeID = e.id;
                        return true;
                    }
                }
            }
            return false;
        }

        public override ButtonGridItem[] CreateButton(Employee employee)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("emp" + employee.employeeID, employee.EmployeeNameID, Properties.Resources.Employee_2);
            int typeID = AccountingClient.GetDocumentTypeByType(typeof(PayrollPaymentSetDocument)).id;
            button.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(typeID), payMethod, employee.id);
            return new ButtonGridItem[] { button };
        }

    }
    public class StaffLoanPaymentList : StaffMembersList
    {
        BizNetPaymentMethod payMethod;
        public StaffLoanPaymentList(ButtonGrid grid, ButtonGridItem parent, BizNetPaymentMethod paymentMethod)
            : base(grid, parent)
        {
            payMethod = paymentMethod;
        }
        public StaffLoanPaymentList(ButtonGrid grid, ButtonGridItem parent)
            : base(grid, parent)
        {

        }

        private int employeeID;
        protected override bool CanIncludeEmployee(Employee e)
        {
            int loanFromStaffAccount = (int)PayrollClient.GetSystemParameters(new string[] { "LoanFromStaffAccountID" })[0];
            if (loanFromStaffAccount == -1)
                return false;
            foreach (int ac in e.accounts)
            {
                Account acnt = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(ac);
                if (acnt != null)
                {
                    if (acnt.PID == loanFromStaffAccount)
                    {
                        employeeID = e.id;
                        return true;
                    }
                }
            }
            return false;
        }

        public override ButtonGridItem[] CreateButton(Employee employee)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("emp" + employee.employeeID, employee.EmployeeNameID, Properties.Resources.Employee_2);
            button.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(121), payMethod, employee.id);
            return new ButtonGridItem[] { button };

            //  ButtonGridHelper.SetHeaderModeText(item, employee.EmployeeNameID + " - Expense Advance");
        }

    }

    public class StaffPerDiemPaymentList : StaffMembersList
    {
        BizNetPaymentMethod payMethod;
        public StaffPerDiemPaymentList(ButtonGrid grid, ButtonGridItem parent, BizNetPaymentMethod paymentMethod)
            : base(grid, parent)
        {
            payMethod = paymentMethod;
        }
        public StaffPerDiemPaymentList(ButtonGrid grid, ButtonGridItem parent)
            : base(grid, parent)
        {

        }

        private int employeeID;
        protected override bool CanIncludeEmployee(Employee e)
        {
            object[] vals=PayrollClient.GetSystemParameters(new string[] { "staffPerDiemAccountID", "staffPerDiemCostAccountID" });
            foreach (int staffPerdiemParentAccountID in vals)
            {
                if (staffPerdiemParentAccountID == -1)
                    continue;
                foreach (int ac in e.accounts)
                {
                    Account acnt = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(ac);
                    if (acnt != null)
                    {
                        if (acnt.PID == staffPerdiemParentAccountID)
                        {
                            Employee employee = PayrollClient.GetEmployee(e.id);
                            if (employee.grossSalary > 0)
                            {
                                employeeID = e.id;
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public override ButtonGridItem[] CreateButton(Employee employee)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("emp" + employee.employeeID, employee.EmployeeNameID, Properties.Resources.Employee_2);
            button.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(129), payMethod, employee.id);
            return new ButtonGridItem[] { button };

            //  ButtonGridHelper.SetHeaderModeText(item, employee.EmployeeNameID + " - Expense Advance");
        }

    }
    public class ShareHoldersLoanList : StaffMembersList
    {
        BizNetPaymentMethod payMethod;
        public ShareHoldersLoanList(ButtonGrid grid, ButtonGridItem parent, BizNetPaymentMethod paymentMethod)
            : base(grid, parent)
        {
            payMethod = paymentMethod;
        }
        public ShareHoldersLoanList(ButtonGrid grid, ButtonGridItem parent)
            : base(grid, parent)
        {

        }

        private int employeeID;
        protected override bool CanIncludeEmployee(Employee e)
        {
            int shareHoldersLoanParentAccount = (int)PayrollClient.GetSystemParameters(new string[] { "ShareHoldersLoanAccountID" })[0];
            if (shareHoldersLoanParentAccount == -1)
                return false;
            foreach (int ac in e.accounts)
            {
                Account acnt = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(ac);
                if (acnt != null)
                {
                    if (acnt.PID == shareHoldersLoanParentAccount)
                    {
                        employeeID = e.id;
                        return true;
                    }
                }
            }
            return false;
        }

        public override ButtonGridItem[] CreateButton(Employee employee)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("emp" + employee.employeeID, employee.EmployeeNameID, Properties.Resources.Employee_2);
            button.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(136), payMethod, employee.id);
            return new ButtonGridItem[] { button };
        }

    }

}
