using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.UI.ButtonGrid;
using INTAPS.Payroll.Client;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Drawing;

namespace BIZNET.iERP.Client
{
    public class StaffMembersReturnList : ObjectBGList<Employee>
    {
        ButtonGrid buttonGrid;
        public StaffMembersReturnList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        public override bool Searchable
        {
            get { return true; }
        }
        protected virtual bool CanIncludeEmployee(Employee e)
        {
            return true;
        }
        protected virtual bool employeeAccountHasBalance(Employee emp, Account acnt, DateTime date)
        {
            return Account.AmountGreater(INTAPS.Accounting.Client.AccountingClient.GetNetCostCenterAccountBalanceAsOf((int)iERPTransactionClient.GetSystemParamter("mainCostCenterID"), acnt.id, date), 0);
        }
        public override ButtonGridItem[] CreateButton(Employee employee)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("empReceipt"+employee.employeeID, employee.EmployeeNameID, Properties.Resources.Employee_2);
            //ButtonGridHelper.SetButtonBackColor(button, Color.LightBlue);
            return new ButtonGridItem[] { button };
        }
        public override List<Employee> LoadData(string query)
        {
            List<Employee> ret = new List<Employee>();
            int N;

            Employee[] allEmployees = null;
            if (String.IsNullOrEmpty(query))
                allEmployees = PayrollClient.SearchEmployee(DateTime.Now.Ticks, -1, "", false, INTAPS.Payroll.EmployeeSearchType.All, 0, 5, out N);
            else
            {
                allEmployees = PayrollClient.SearchEmployee(DateTime.Now.Ticks, -1, query, false, INTAPS.Payroll.EmployeeSearchType.All, 0, 5, out N);

            }
            foreach (Employee employee in allEmployees)
            {
                if (employee.status != EmployeeStatus.Fired)
                {
                    if (CanIncludeEmployee(employee))
                    {
                        ret.Add(employee);
                    }
                }
            }
            return ret;

        }


        public override string searchLabel
        {
            get { return "Employee:"; }
        }
    }
    public class StaffWithExpenseAdvanceReturnList : StaffMembersReturnList
    {
        BizNetPaymentMethod payMethod;
        public StaffWithExpenseAdvanceReturnList(ButtonGrid grid, ButtonGridItem parentItem,BizNetPaymentMethod paymentMethod)
            : base(grid, parentItem)
        {
            payMethod = paymentMethod;
        }
        public StaffWithExpenseAdvanceReturnList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            
        }
         
        private int employeeID;
        protected override bool CanIncludeEmployee(Employee e)
        {
            int staffExpenseAdvanceParentAccount = (int)PayrollClient.GetSystemParameters(new string[] { "staffExpenseAdvanceAccountID" })[0];
            if (staffExpenseAdvanceParentAccount == -1)
                return false;
            foreach (int ac in e.accounts)
            {
                Account acnt = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(ac);
                if (acnt.PID == staffExpenseAdvanceParentAccount)
                {
                    if (employeeAccountHasBalance(e,acnt,DateTime.Now))
                    {
                        employeeID = e.id;
                        return true;
                    }
                }
            }
            return false;
        }

        public override ButtonGridItem[] CreateButton(Employee employee)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("empReceipt" + employee.employeeID, employee.EmployeeNameID, Properties.Resources.Employee_2);
            button.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(111), payMethod, employee.id);
            return new ButtonGridItem[] { button };
        }
    }

    public class StaffWithShortTermLoanReturnList : StaffMembersReturnList
    {
        BizNetPaymentMethod payMethod;
        public StaffWithShortTermLoanReturnList(ButtonGrid grid, ButtonGridItem parentItem,BizNetPaymentMethod paymentMethod)
            : base(grid, parentItem)
        {
            payMethod = paymentMethod;
        }
        public StaffWithShortTermLoanReturnList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            
        }
         
        private int employeeID;
        protected override bool CanIncludeEmployee(Employee e)
        {
            int staffShortTermLoanParentAccount = (int)PayrollClient.GetSystemParameters(new string[] { "staffSalaryAdvanceAccountID" })[0];
            if (staffShortTermLoanParentAccount == -1)
                return false;
            foreach (int ac in e.accounts)
            {
                Account acnt = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(ac);
                if (acnt.PID == staffShortTermLoanParentAccount)
                {
                    if (employeeAccountHasBalance(e,acnt,DateTime.Now))
                    {
                        employeeID = e.id;
                        return true;
                    }
                }
            }
            return false;
        }
        public override ButtonGridItem[] CreateButton(Employee employee)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("empReceipt" + employee.employeeID, employee.EmployeeNameID, Properties.Resources.Employee_2);
            button.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(112), payMethod, employee.id);
            return new ButtonGridItem[] { button };
        }
    }

    public class StaffWithLongTermLoanReturnList : StaffMembersReturnList
    {
        BizNetPaymentMethod payMethod;
        public StaffWithLongTermLoanReturnList(ButtonGrid grid, ButtonGridItem parentItem,BizNetPaymentMethod paymentMethod)
            : base(grid, parentItem)
        {
            payMethod = paymentMethod;
        }
        public StaffWithLongTermLoanReturnList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            
        }
         
        private int employeeID;
        protected override bool CanIncludeEmployee(Employee e)
        {
            int staffLongTermLoanParentAccount = (int)PayrollClient.GetSystemParameters(new string[] { "staffLongTermLoanAccountID" })[0];
            if (staffLongTermLoanParentAccount == -1)
                return false;
            foreach (int ac in e.accounts)
            {
                Account acnt = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(ac);
                if (acnt.PID == staffLongTermLoanParentAccount)
                {
                    if (employeeAccountHasBalance(e,acnt,DateTime.Now))
                    {
                        employeeID = e.id;
                        return true;
                    }
                }
            }
            return false;
        }

        public override ButtonGridItem[] CreateButton(Employee employee)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("empReceipt" + employee.employeeID, employee.EmployeeNameID, Properties.Resources.Employee_2);
            button.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(113), payMethod, employee.id);
            return new ButtonGridItem[] { button };
        }
    }

    public class LoanFromStaffList:StaffMembersReturnList
    {
        BizNetPaymentMethod payMethod;
        public LoanFromStaffList(ButtonGrid grid, ButtonGridItem parentItem,BizNetPaymentMethod paymentMethod)
            : base(grid, parentItem)
        {
            payMethod = paymentMethod;
        }
        public LoanFromStaffList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            
        }

        private int employeeID;
        protected override bool CanIncludeEmployee(Employee e)
        {
            int loanFromStaffAccount = (int)PayrollClient.GetSystemParameters(new string[] { "LoanFromStaffAccountID" })[0];
            if (loanFromStaffAccount == -1)
                return false;
            foreach (int ac in e.accounts)
            {
                Account acnt = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(ac);
                if (acnt.PID == loanFromStaffAccount)
                {
                    employeeID = e.id;
                    return true;
                }
            }
            return false;
        }
        public override ButtonGridItem[] CreateButton(Employee employee)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("empReceipt" + employee.employeeID, employee.EmployeeNameID, Properties.Resources.Employee_2);
            button.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(120), payMethod, employee.id);
            return new ButtonGridItem[] { button };
        }

    }
    public class ShareHoldersLoanReturnList : StaffMembersReturnList
    {
        BizNetPaymentMethod payMethod;
        public ShareHoldersLoanReturnList(ButtonGrid grid, ButtonGridItem parentItem, BizNetPaymentMethod paymentMethod)
            : base(grid, parentItem)
        {
            payMethod = paymentMethod;
        }
        public ShareHoldersLoanReturnList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {

        }

        private int employeeID;
        protected override bool CanIncludeEmployee(Employee e)
        {
            int shareHoldersLoanParentAccount = (int)PayrollClient.GetSystemParameters(new string[] { "ShareHoldersLoanAccountID" })[0];
            if (shareHoldersLoanParentAccount == -1)
                return false;
            foreach (int ac in e.accounts)
            {
                Account acnt = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(ac);
                if (acnt.PID == shareHoldersLoanParentAccount)
                {
                    if (employeeAccountHasBalance(e,acnt,DateTime.Now))
                    {
                        employeeID = e.id;
                        return true;
                    }
                }
            }
            return false;
        }
        
        public override ButtonGridItem[] CreateButton(Employee employee)
        {
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("empReceipt" + employee.employeeID, employee.EmployeeNameID, Properties.Resources.Employee_2);
            button.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(137), payMethod, employee.id);
            return new ButtonGridItem[] { button };
        }
    }
}
