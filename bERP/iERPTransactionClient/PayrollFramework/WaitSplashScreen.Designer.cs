﻿namespace BIZNET.iERP.Client
{
    partial class WaitSplashScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressPanel = new DevExpress.XtraWaitForm.ProgressPanel();
            this.SuspendLayout();
            // 
            // progressPanel
            // 
            this.progressPanel.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progressPanel.Appearance.Options.UseBackColor = true;
            this.progressPanel.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.progressPanel.AppearanceCaption.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.progressPanel.AppearanceCaption.Options.UseFont = true;
            this.progressPanel.AppearanceCaption.Options.UseForeColor = true;
            this.progressPanel.AppearanceDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progressPanel.AppearanceDescription.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.progressPanel.AppearanceDescription.Options.UseFont = true;
            this.progressPanel.AppearanceDescription.Options.UseForeColor = true;
            this.progressPanel.Description = "";
            this.progressPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.progressPanel.ImageHorzOffset = 20;
            this.progressPanel.Location = new System.Drawing.Point(0, 0);
            this.progressPanel.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.progressPanel.Name = "progressPanel";
            this.progressPanel.Size = new System.Drawing.Size(508, 45);
            this.progressPanel.TabIndex = 1;
            this.progressPanel.Text = "progressPanel1";
            // 
            // WaitSplashScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 122);
            this.ControlBox = false;
            this.Controls.Add(this.progressPanel);
            this.MaximizeBox = false;
            this.Name = "WaitSplashScreen";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraWaitForm.ProgressPanel progressPanel;

    }
}