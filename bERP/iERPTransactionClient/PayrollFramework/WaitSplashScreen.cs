﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERP.Client
{
    public partial class WaitSplashScreen : Form
    {
        public WaitSplashScreen()
        {
            InitializeComponent();
        }
        public static void showWaitSplash(IWin32Window window, string message,Delegate task)
        {
            WaitSplashScreen sp = new WaitSplashScreen();
            sp.progressPanel.Caption = message;
            new System.Threading.Thread(delegate()
                {
                    task.DynamicInvoke();
                    sp.Invoke(new INTAPS.UI.HTML.ProcessParameterless(delegate() { sp.Close(); }));
                }).Start();
            sp.ShowDialog(window);
        }
    }
}
