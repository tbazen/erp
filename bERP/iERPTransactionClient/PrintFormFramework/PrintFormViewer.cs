﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BIZNET.iERP.Client
{
    public partial class PrintFormViewer : DevExpress.XtraEditors.XtraForm
    {
        public PrintFormViewer()
        {
            InitializeComponent();
        }
        public void LoadReport(DevExpress.XtraReports.UI.XtraReport report)
        {
            report.CreateDocument();
            viewer.PrintingSystem = report.PrintingSystem;
            report.PrintingSystem.ShowMarginsWarning = false;            
        }

    }
}