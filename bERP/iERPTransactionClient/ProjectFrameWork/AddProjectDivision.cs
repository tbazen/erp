﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    public partial class AddProjectDivision : DevExpress.XtraEditors.XtraForm
    {
        private string m_DivisionName;
        private bool _machinaryAndVehicle;
        private int _costCenterID;
        public AddProjectDivision(bool machinaryAndVehicle)
        {
            InitializeComponent();
            _machinaryAndVehicle = machinaryAndVehicle;
            this.Text = machinaryAndVehicle ? "Machinary/Vehicle" : "Project Division";
        }
        public AddProjectDivision(int costCenterID, bool machinaryAndVehicle)
            :this(machinaryAndVehicle)
        {
            _costCenterID = costCenterID;
            string name = AccountingClient.GetAccount<CostCenter>(_costCenterID).Name;
            txtName.Text = name;
        }
        public string DivisionName
        {
            get
            {
                return m_DivisionName;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtName.Text == "")
            {
                string name = _machinaryAndVehicle ? "Machinary/Vehicle" : "Division";
                MessageBox.ShowErrorMessage("Please enter " + name + " name and try again");
                return;
            }
            try
            {
                m_DivisionName = txtName.Text;
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }
    }
}