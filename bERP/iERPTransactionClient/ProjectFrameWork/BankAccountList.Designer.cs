﻿namespace BIZNET.iERP.Client
{
    partial class BankAccountList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BankAccountList));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnAddBank = new DevExpress.XtraEditors.SimpleButton();
            this.btnRemove = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlBankAccounts = new DevExpress.XtraGrid.GridControl();
            this.gridViewBankAccounts = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBankAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBankAccounts)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnAddBank);
            this.panelControl1.Controls.Add(this.btnRemove);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(685, 36);
            this.panelControl1.TabIndex = 1;
            // 
            // btnAddBank
            // 
            this.btnAddBank.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddBank.Appearance.Options.UseFont = true;
            this.btnAddBank.Image = global::BIZNET.iERP.Client.Properties.Resources.bank_add;
            this.btnAddBank.Location = new System.Drawing.Point(3, 3);
            this.btnAddBank.Name = "btnAddBank";
            this.btnAddBank.Size = new System.Drawing.Size(163, 31);
            this.btnAddBank.TabIndex = 1;
            this.btnAddBank.Text = "Add &Bank Account";
            this.btnAddBank.Click += new System.EventHandler(this.btnAddBank_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.Appearance.Options.UseFont = true;
            this.btnRemove.Image = ((System.Drawing.Image)(resources.GetObject("btnRemove.Image")));
            this.btnRemove.Location = new System.Drawing.Point(172, 3);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(89, 31);
            this.btnRemove.TabIndex = 0;
            this.btnRemove.Text = "&Remove";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // gridControlBankAccounts
            // 
            this.gridControlBankAccounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlBankAccounts.Location = new System.Drawing.Point(0, 36);
            this.gridControlBankAccounts.MainView = this.gridViewBankAccounts;
            this.gridControlBankAccounts.Name = "gridControlBankAccounts";
            this.gridControlBankAccounts.Size = new System.Drawing.Size(685, 472);
            this.gridControlBankAccounts.TabIndex = 2;
            this.gridControlBankAccounts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBankAccounts});
            // 
            // gridViewBankAccounts
            // 
            this.gridViewBankAccounts.GridControl = this.gridControlBankAccounts;
            this.gridViewBankAccounts.Name = "gridViewBankAccounts";
            this.gridViewBankAccounts.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewBankAccounts.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewBankAccounts.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewBankAccounts.OptionsBehavior.Editable = false;
            this.gridViewBankAccounts.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewBankAccounts.OptionsCustomization.AllowFilter = false;
            this.gridViewBankAccounts.OptionsCustomization.AllowGroup = false;
            this.gridViewBankAccounts.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewBankAccounts.OptionsCustomization.AllowSort = false;
            this.gridViewBankAccounts.OptionsView.ShowGroupPanel = false;
            this.gridViewBankAccounts.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewBankAccounts_FocusedRowChanged);
            this.gridViewBankAccounts.DoubleClick += new System.EventHandler(this.gridViewBankAccounts_DoubleClick);
            // 
            // BankAccountList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlBankAccounts);
            this.Controls.Add(this.panelControl1);
            this.Name = "BankAccountList";
            this.Size = new System.Drawing.Size(685, 508);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBankAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBankAccounts)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnRemove;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControlBankAccounts;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewBankAccounts;
        private DevExpress.XtraEditors.SimpleButton btnAddBank;
    }
}
