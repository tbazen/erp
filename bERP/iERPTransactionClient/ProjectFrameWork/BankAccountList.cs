﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class BankAccountList : DevExpress.XtraEditors.XtraUserControl
    {
        private DataTable bankAccountTable;
        DevExpress.XtraSplashScreen.SplashScreenManager waitScreen;
        private string m_ProjectCode;
        public BankAccountList(string projectCode)
        {
            InitializeComponent();
            waitScreen = new DevExpress.XtraSplashScreen.SplashScreenManager(ProjectManager.MainProjectManager, typeof(global::BIZNET.iERP.Client.WaitForm1), true, true);
            m_ProjectCode = projectCode;
            bankAccountTable = new DataTable();
            PrepareDataTable();
            btnRemove.Enabled = false;
           // LoadProjectBankAccounts(m_ProjectCode);

        }

        private void PrepareDataTable()
        {
            bankAccountTable.Columns.Add("MainCsAccountID", typeof(int));
            bankAccountTable.Columns.Add("Bank Name", typeof(string));
            bankAccountTable.Columns.Add("Branch Name", typeof(string));
            bankAccountTable.Columns.Add("Account Name", typeof(string));
            bankAccountTable.Columns.Add("Account Number", typeof(string));
            bankAccountTable.Columns.Add("Project/Division", typeof(string));
            gridControlBankAccounts.DataSource = bankAccountTable;
            gridViewBankAccounts.Columns["MainCsAccountID"].Visible = false;
        }
        public void LoadProjectBankAccounts(string projectCode)
        {
            try
            {
                bankAccountTable.Clear();
                Project project = iERPTransactionClient.GetProjectInfo(projectCode);
                waitScreen.ShowWaitForm();
                foreach (int mainCsID in project.projectData.bankAccounts)
                {
                    DataRow row = bankAccountTable.NewRow();
                    BankAccountInfo bankAccount = iERPTransactionClient.GetBankAccount(mainCsID);
                    string divisionName = AccountingClient.GetAccount<CostCenter>(AccountingClient.GetCostCenterAccount(mainCsID).costCenterID).Name;
                    waitScreen.SetWaitFormDescription("Loading: " + bankAccount.bankName);
                    row[0] = mainCsID;
                    row[1] = bankAccount.bankName;
                    row[2] = bankAccount.branchName;
                    row[3] = bankAccount.bankAccountName;
                    row[4] = bankAccount.bankAccountNumber;
                    row[5] = divisionName;
                    bankAccountTable.Rows.Add(row);
                    Application.DoEvents();
                }
                gridControlBankAccounts.DataSource = bankAccountTable;
                gridControlBankAccounts.RefreshDataSource();
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to retrieve projects\n" + ex.Message);
            }
            finally
            {
                waitScreen.CloseWaitForm();
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.ShowWarningMessage("Are you sure you want to remove the selected bank account from the project?") == DialogResult.Yes)
                {
                    int mainCsAccountID = (int)gridViewBankAccounts.GetRowCellValue(gridViewBankAccounts.FocusedRowHandle, "MainCsAccountID");
                    iERPTransactionClient.RemoveBankAccountFromProject(mainCsAccountID, m_ProjectCode);
                    gridViewBankAccounts.DeleteSelectedRows();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void gridViewBankAccounts_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewBankAccounts.SelectedRowsCount > 0)
                btnRemove.Enabled = true;
            else
                btnRemove.Enabled = false;
        }

        private void btnAddBank_Click(object sender, EventArgs e)
        {
            try
            {
                Project project = iERPTransactionClient.GetProjectInfo(m_ProjectCode);
                CostCenter costCenter = AccountingClient.GetAccount<CostCenter>(project.costCenterID);
                if (costCenter.childCount>=1)
                {
                    MessageBox.ShowErrorMessage("You cannot add bank account to this project/branch since a division or store is created under it!");
                    return;
                }
                RegisterBank bank = new RegisterBank(project.costCenterID, false);
                if (bank.ShowDialog() == DialogResult.OK)
                {
                    LoadProjectBankAccounts(m_ProjectCode);
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void gridViewBankAccounts_DoubleClick(object sender, EventArgs e)
        {
            int mainCsAccountID = (int)gridViewBankAccounts.GetRowCellValue(gridViewBankAccounts.FocusedRowHandle, "MainCsAccountID");
            RegisterBank bank = new RegisterBank(mainCsAccountID);
            if (bank.ShowDialog() == DialogResult.OK)
            {
                LoadProjectBankAccounts(m_ProjectCode);
            }
        }

    }
}
