﻿namespace BIZNET.iERP.Client
{
    partial class CashAccountList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CashAccountList));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnAddCash = new DevExpress.XtraEditors.SimpleButton();
            this.btnRemove = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlCashAccounts = new DevExpress.XtraGrid.GridControl();
            this.gridViewCashAccounts = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.cashAccountTree = new BIZNET.iERP.Client.CashAccountCategoryTree();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCashAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCashAccounts)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnAddCash);
            this.panelControl1.Controls.Add(this.btnRemove);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(685, 36);
            this.panelControl1.TabIndex = 1;
            // 
            // btnAddCash
            // 
            this.btnAddCash.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCash.Appearance.Options.UseFont = true;
            this.btnAddCash.Image = global::BIZNET.iERP.Client.Properties.Resources.cash_add;
            this.btnAddCash.Location = new System.Drawing.Point(3, 3);
            this.btnAddCash.Name = "btnAddCash";
            this.btnAddCash.Size = new System.Drawing.Size(161, 31);
            this.btnAddCash.TabIndex = 1;
            this.btnAddCash.Text = "Add &Cash Account";
            this.btnAddCash.Click += new System.EventHandler(this.btnAddCash_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.Appearance.Options.UseFont = true;
            this.btnRemove.Image = ((System.Drawing.Image)(resources.GetObject("btnRemove.Image")));
            this.btnRemove.Location = new System.Drawing.Point(170, 3);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(89, 31);
            this.btnRemove.TabIndex = 0;
            this.btnRemove.Text = "&Remove";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // gridControlCashAccounts
            // 
            this.gridControlCashAccounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlCashAccounts.Location = new System.Drawing.Point(270, 36);
            this.gridControlCashAccounts.MainView = this.gridViewCashAccounts;
            this.gridControlCashAccounts.Name = "gridControlCashAccounts";
            this.gridControlCashAccounts.Size = new System.Drawing.Size(415, 472);
            this.gridControlCashAccounts.TabIndex = 2;
            this.gridControlCashAccounts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCashAccounts});
            // 
            // gridViewCashAccounts
            // 
            this.gridViewCashAccounts.GridControl = this.gridControlCashAccounts;
            this.gridViewCashAccounts.Name = "gridViewCashAccounts";
            this.gridViewCashAccounts.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewCashAccounts.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewCashAccounts.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewCashAccounts.OptionsBehavior.Editable = false;
            this.gridViewCashAccounts.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewCashAccounts.OptionsCustomization.AllowFilter = false;
            this.gridViewCashAccounts.OptionsCustomization.AllowGroup = false;
            this.gridViewCashAccounts.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewCashAccounts.OptionsCustomization.AllowSort = false;
            this.gridViewCashAccounts.OptionsView.ShowGroupPanel = false;
            this.gridViewCashAccounts.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewCashAccounts_FocusedRowChanged);
            this.gridViewCashAccounts.DoubleClick += new System.EventHandler(this.gridViewCashAccounts_DoubleClick);
            // 
            // cashAccountTree
            // 
            this.cashAccountTree.Dock = System.Windows.Forms.DockStyle.Left;
            this.cashAccountTree.Location = new System.Drawing.Point(0, 36);
            this.cashAccountTree.Name = "cashAccountTree";
            this.cashAccountTree.Size = new System.Drawing.Size(270, 472);
            this.cashAccountTree.TabIndex = 3;
            // 
            // CashAccountList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlCashAccounts);
            this.Controls.Add(this.cashAccountTree);
            this.Controls.Add(this.panelControl1);
            this.Name = "CashAccountList";
            this.Size = new System.Drawing.Size(685, 508);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCashAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCashAccounts)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnRemove;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControlCashAccounts;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCashAccounts;
        private DevExpress.XtraEditors.SimpleButton btnAddCash;
        private CashAccountCategoryTree cashAccountTree;
    }
}
