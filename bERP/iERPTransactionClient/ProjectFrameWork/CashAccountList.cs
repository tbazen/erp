﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class CashAccountList : DevExpress.XtraEditors.XtraUserControl
    {
        private DataTable cashAccountTable;
        DevExpress.XtraSplashScreen.SplashScreenManager waitScreen;
        private string m_ProjectCode;
        public CashAccountList(string projectCode)
        {
            InitializeComponent();
            waitScreen = new DevExpress.XtraSplashScreen.SplashScreenManager(ProjectManager.MainProjectManager, typeof(global::BIZNET.iERP.Client.WaitForm1), true, true);
            m_ProjectCode = projectCode;
            cashAccountTable = new DataTable();
            PrepareDataTable();
            btnRemove.Enabled = false;
           // LoadProjectCashAccounts(m_ProjectCode);

        }

        private void PrepareDataTable()
        {
            cashAccountTable.Columns.Add("Code", typeof(string));
            cashAccountTable.Columns.Add("Name", typeof(string));
            cashAccountTable.Columns.Add("Project/Division", typeof(string));
            cashAccountTable.Columns.Add("CsAccountID", typeof(int));
            gridControlCashAccounts.DataSource = cashAccountTable;
            gridViewCashAccounts.Columns["CsAccountID"].Visible = false;
        }
        public void LoadProjectCashAccounts(string projectCode)
        {
            try
            {
                cashAccountTable.Clear();
                Project project = iERPTransactionClient.GetProjectInfo(projectCode);
                waitScreen.ShowWaitForm();
                foreach (string code in project.projectData.cashAccounts)
                {
                    DataRow row = cashAccountTable.NewRow();
                    CashAccount cashAccount = iERPTransactionClient.GetCashAccount(code);
                    string divisionName = AccountingClient.GetAccount<CostCenter>(AccountingClient.GetCostCenterAccount(cashAccount.csAccountID).costCenterID).Name;
                    waitScreen.SetWaitFormDescription("Loading: " + cashAccount.name);
                    row[0] = code;
                    row[1] = cashAccount.name;
                    row[2] = divisionName;
                    row[3] = cashAccount.csAccountID;
                    cashAccountTable.Rows.Add(row);
                    Application.DoEvents();
                }
                gridControlCashAccounts.DataSource = cashAccountTable;
                gridControlCashAccounts.RefreshDataSource();
                waitScreen.CloseWaitForm();
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to retrieve projects\n" + ex.Message);
            }
        }

        private void gridViewStores_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.ShowWarningMessage("Are you sure you want to remove the selected cash account from the project?") == DialogResult.Yes)
                {
                    int csAccountID = (int)gridViewCashAccounts.GetRowCellValue(gridViewCashAccounts.FocusedRowHandle, "CsAccountID");
                    string code = (string)gridViewCashAccounts.GetRowCellValue(gridViewCashAccounts.FocusedRowHandle, "Code");
                    iERPTransactionClient.RemoveCashAccountFromProject(csAccountID,code,m_ProjectCode);
                    gridViewCashAccounts.DeleteSelectedRows();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void gridViewCashAccounts_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewCashAccounts.SelectedRowsCount > 0)
                btnRemove.Enabled = true;
            else
                btnRemove.Enabled = false;
        }

        private void btnAddCash_Click(object sender, EventArgs e)
        {
            try
            {
                Project project = iERPTransactionClient.GetProjectInfo(m_ProjectCode);
                CostCenter costCenter = AccountingClient.GetAccount<CostCenter>(project.costCenterID);
                if (costCenter.childCount >= 1)
                {

                    MessageBox.ShowErrorMessage("You cannot add cash account to this project/branch since a division or store is created under it!");
                    return;
                }
                if (cashAccountTree.selectedCashAccountID == -1)
                {
                    MessageBox.ShowErrorMessage("Select category");
                    return;
                }
                RegisterCashAccount cashAccount = new RegisterCashAccount(cashAccountTree.selectedCashAccountID,project.costCenterID, false);
                if (cashAccount.ShowDialog() == DialogResult.OK)
                {
                    LoadProjectCashAccounts(m_ProjectCode);
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void gridViewCashAccounts_DoubleClick(object sender, EventArgs e)
        {
            string code = (string)gridViewCashAccounts.GetRowCellValue(gridViewCashAccounts.FocusedRowHandle, "Code");
            RegisterCashAccount cash = new RegisterCashAccount(iERPTransactionClient.GetCashAccount(code));
            if (cash.ShowDialog() == DialogResult.OK)
            {
                LoadProjectCashAccounts(m_ProjectCode);
            }
        }

    }
}
