﻿namespace BIZNET.iERP.Client
{
    partial class DivisionPicker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControlDivisions = new DevExpress.XtraGrid.GridControl();
            this.gridViewDivisions = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnNewDivision = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDivisions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDivisions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControlDivisions
            // 
            this.gridControlDivisions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlDivisions.Location = new System.Drawing.Point(0, 30);
            this.gridControlDivisions.MainView = this.gridViewDivisions;
            this.gridControlDivisions.Name = "gridControlDivisions";
            this.gridControlDivisions.Size = new System.Drawing.Size(292, 250);
            this.gridControlDivisions.TabIndex = 0;
            this.gridControlDivisions.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDivisions});
            this.gridControlDivisions.DoubleClick += new System.EventHandler(this.gridControlDivisions_DoubleClick);
            // 
            // gridViewDivisions
            // 
            this.gridViewDivisions.GridControl = this.gridControlDivisions;
            this.gridViewDivisions.Name = "gridViewDivisions";
            this.gridViewDivisions.OptionsBehavior.Editable = false;
            this.gridViewDivisions.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewDivisions.OptionsCustomization.AllowFilter = false;
            this.gridViewDivisions.OptionsCustomization.AllowGroup = false;
            this.gridViewDivisions.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewDivisions.OptionsCustomization.AllowSort = false;
            this.gridViewDivisions.OptionsView.ShowGroupPanel = false;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnClose);
            this.panelControl1.Controls.Add(this.btnOK);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 280);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(292, 35);
            this.panelControl1.TabIndex = 1;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(212, 7);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(131, 7);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "&Ok";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.btnNewDivision);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(292, 30);
            this.panelControl2.TabIndex = 2;
            // 
            // btnNewDivision
            // 
            this.btnNewDivision.Location = new System.Drawing.Point(0, 0);
            this.btnNewDivision.Name = "btnNewDivision";
            this.btnNewDivision.Size = new System.Drawing.Size(75, 28);
            this.btnNewDivision.TabIndex = 0;
            this.btnNewDivision.Text = "&New Division";
            this.btnNewDivision.Click += new System.EventHandler(this.btnNewDivision_Click);
            // 
            // DivisionPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 315);
            this.Controls.Add(this.gridControlDivisions);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "DivisionPicker";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Division Picker";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DivisionPicker_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDivisions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDivisions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlDivisions;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDivisions;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnNewDivision;
    }
}