﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class DivisionPicker : DevExpress.XtraEditors.XtraForm
    {
        private DataTable tableDivisions;
        private Project _Project;
        public DivisionPicker(Project project)
        {
            InitializeComponent();
            tableDivisions = new DataTable();
            PrepareDataTable();
            _Project = project;
            LoadDivisions(project.code);
        }
       
        private void PrepareDataTable()
        {
            tableDivisions.Columns.Add("Division Name", typeof(string));
            tableDivisions.Columns.Add("Project",typeof(string));
            tableDivisions.Columns.Add("CostCenterID", typeof(int));
            gridControlDivisions.DataSource = tableDivisions;
            gridViewDivisions.Columns["CostCenterID"].Visible = false;
            gridViewDivisions.GroupFormat = String.Format("{0}{1}", "", "{1}");
        }
        private void LoadDivisions(string projectCode)
        {
            tableDivisions.Rows.Clear();
            Project project = iERPTransactionClient.GetProjectInfo(projectCode);
            foreach (int divisionCostCenterID in project.projectData.projectDivisions)
            {
                CostCenter division = AccountingClient.GetAccount<CostCenter>(divisionCostCenterID);
                DataRow row = tableDivisions.NewRow();
                row[0] = division.Name;
                row[1] = project.Name;
                row[2] = divisionCostCenterID;
                tableDivisions.Rows.Add(row);
            }
            gridControlDivisions.DataSource = tableDivisions;
            gridViewDivisions.Columns["Project"].Group();
            gridViewDivisions.ExpandAllGroups();
            gridControlDivisions.RefreshDataSource();

        }

        private void gridControlDivisions_DoubleClick(object sender, EventArgs e)
        {
            SetDivision();
        }

        private void SetDivision()
        {
            try
            {
                if (gridViewDivisions.SelectedRowsCount > 0)
                {
                    if (!gridViewDivisions.IsGroupRow(gridViewDivisions.FocusedRowHandle))
                    {
                        int divisionCsID = (int)gridViewDivisions.GetRowCellValue(gridViewDivisions.FocusedRowHandle, "CostCenterID");
                        //iERPTransactionClient.RegisterProject(_Project, divisionCsID, null);
                        this.DialogResult = DialogResult.OK;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            SetDivision();
        }

        private void btnNewDivision_Click(object sender, EventArgs e)
        {
            try
            {
                AddProjectDivision division = new AddProjectDivision(false);
                if (division.ShowDialog() == DialogResult.OK)
                {
                   // iERPTransactionClient.RegisterProject(_Project, -1, division.DivisionName);
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void DivisionPicker_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult != DialogResult.OK)
                this.DialogResult = DialogResult.Cancel;
        }

    }
}