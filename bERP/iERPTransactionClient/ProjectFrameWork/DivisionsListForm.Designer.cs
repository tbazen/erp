﻿namespace BIZNET.iERP.Client
{
    partial class DivisionsListForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DivisionsListForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnAddCash = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddBank = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddEmployee = new DevExpress.XtraEditors.SimpleButton();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddDivision = new DevExpress.XtraEditors.SimpleButton();
            this.btnRemoveDivision = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlDivisons = new DevExpress.XtraGrid.GridControl();
            this.gridViewDivisions = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDivisons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDivisions)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnAddCash);
            this.panelControl1.Controls.Add(this.btnAddBank);
            this.panelControl1.Controls.Add(this.btnAddEmployee);
            this.panelControl1.Controls.Add(this.btnEdit);
            this.panelControl1.Controls.Add(this.btnAddDivision);
            this.panelControl1.Controls.Add(this.btnRemoveDivision);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(787, 36);
            this.panelControl1.TabIndex = 1;
            // 
            // btnAddCash
            // 
            this.btnAddCash.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCash.Appearance.Options.UseFont = true;
            this.btnAddCash.Image = global::BIZNET.iERP.Client.Properties.Resources.cash_add;
            this.btnAddCash.Location = new System.Drawing.Point(389, 2);
            this.btnAddCash.Name = "btnAddCash";
            this.btnAddCash.Size = new System.Drawing.Size(161, 31);
            this.btnAddCash.TabIndex = 0;
            this.btnAddCash.Text = "Add &Cash Account";
            this.btnAddCash.Click += new System.EventHandler(this.btnAddCash_Click);
            // 
            // btnAddBank
            // 
            this.btnAddBank.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddBank.Appearance.Options.UseFont = true;
            this.btnAddBank.Image = global::BIZNET.iERP.Client.Properties.Resources.bank_add;
            this.btnAddBank.Location = new System.Drawing.Point(220, 2);
            this.btnAddBank.Name = "btnAddBank";
            this.btnAddBank.Size = new System.Drawing.Size(163, 31);
            this.btnAddBank.TabIndex = 0;
            this.btnAddBank.Text = "Add &Bank Account";
            this.btnAddBank.Click += new System.EventHandler(this.btnAddBank_Click);
            // 
            // btnAddEmployee
            // 
            this.btnAddEmployee.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddEmployee.Appearance.Options.UseFont = true;
            this.btnAddEmployee.Location = new System.Drawing.Point(107, 2);
            this.btnAddEmployee.Name = "btnAddEmployee";
            this.btnAddEmployee.Size = new System.Drawing.Size(107, 31);
            this.btnAddEmployee.TabIndex = 0;
            this.btnAddEmployee.Text = "Add &Employee";
            this.btnAddEmployee.Visible = false;
            this.btnAddEmployee.Click += new System.EventHandler(this.btnAddEmployee_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Appearance.Options.UseFont = true;
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.Location = new System.Drawing.Point(556, 2);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(86, 31);
            this.btnEdit.TabIndex = 0;
            this.btnEdit.Text = "Ed&it";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAddDivision
            // 
            this.btnAddDivision.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddDivision.Appearance.Options.UseFont = true;
            this.btnAddDivision.Location = new System.Drawing.Point(3, 2);
            this.btnAddDivision.Name = "btnAddDivision";
            this.btnAddDivision.Size = new System.Drawing.Size(98, 31);
            this.btnAddDivision.TabIndex = 0;
            this.btnAddDivision.Text = "Add &Division";
            this.btnAddDivision.Click += new System.EventHandler(this.btnAddDivision_Click);
            // 
            // btnRemoveDivision
            // 
            this.btnRemoveDivision.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemoveDivision.Appearance.Options.UseFont = true;
            this.btnRemoveDivision.Image = ((System.Drawing.Image)(resources.GetObject("btnRemoveDivision.Image")));
            this.btnRemoveDivision.Location = new System.Drawing.Point(648, 2);
            this.btnRemoveDivision.Name = "btnRemoveDivision";
            this.btnRemoveDivision.Size = new System.Drawing.Size(136, 31);
            this.btnRemoveDivision.TabIndex = 0;
            this.btnRemoveDivision.Text = "&Remove Division";
            this.btnRemoveDivision.Click += new System.EventHandler(this.btnRemoveDivision_Click);
            // 
            // gridControlDivisons
            // 
            this.gridControlDivisons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlDivisons.Location = new System.Drawing.Point(0, 36);
            this.gridControlDivisons.MainView = this.gridViewDivisions;
            this.gridControlDivisons.Name = "gridControlDivisons";
            this.gridControlDivisons.Size = new System.Drawing.Size(787, 472);
            this.gridControlDivisons.TabIndex = 2;
            this.gridControlDivisons.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDivisions});
            // 
            // gridViewDivisions
            // 
            this.gridViewDivisions.GridControl = this.gridControlDivisons;
            this.gridViewDivisions.Name = "gridViewDivisions";
            this.gridViewDivisions.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewDivisions.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewDivisions.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewDivisions.OptionsBehavior.Editable = false;
            this.gridViewDivisions.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewDivisions.OptionsCustomization.AllowFilter = false;
            this.gridViewDivisions.OptionsCustomization.AllowGroup = false;
            this.gridViewDivisions.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewDivisions.OptionsCustomization.AllowSort = false;
            this.gridViewDivisions.OptionsView.ShowGroupPanel = false;
            this.gridViewDivisions.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewDivisions_FocusedRowChanged);
            this.gridViewDivisions.DoubleClick += new System.EventHandler(this.gridViewDivisions_DoubleClick);
            // 
            // DivisionsListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlDivisons);
            this.Controls.Add(this.panelControl1);
            this.Name = "DivisionsListForm";
            this.Size = new System.Drawing.Size(787, 508);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDivisons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDivisions)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnRemoveDivision;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControlDivisons;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDivisions;
        private DevExpress.XtraEditors.SimpleButton btnAddDivision;
        private DevExpress.XtraEditors.SimpleButton btnAddCash;
        private DevExpress.XtraEditors.SimpleButton btnAddBank;
        private DevExpress.XtraEditors.SimpleButton btnAddEmployee;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
    }
}
