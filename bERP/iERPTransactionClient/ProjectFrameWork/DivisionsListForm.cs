﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Payroll;
using INTAPS.Payroll.Client;

namespace BIZNET.iERP.Client
{
    public partial class DivisionsListForm : DevExpress.XtraEditors.XtraUserControl
    {
        private DataTable divisionsTable;
        DevExpress.XtraSplashScreen.SplashScreenManager waitScreen;
        private string m_ProjectCode;
        private Project[] m_Projects;
        public DivisionsListForm(string projectCode)
        {
            InitializeComponent();
            waitScreen = new DevExpress.XtraSplashScreen.SplashScreenManager(ProjectManager.MainProjectManager, typeof(global::BIZNET.iERP.Client.WaitForm1), true, true);
            m_ProjectCode = projectCode;
            m_Projects = iERPTransactionClient.GetAllProjects();
            divisionsTable = new DataTable();
            PrepareDataTable();
            btnRemoveDivision.Enabled = false;
            btnAddBank.Enabled = btnAddCash.Enabled = btnAddEmployee.Enabled = false;
            LoadProjectDivisons(m_ProjectCode);

        }

        private void PrepareDataTable()
        {
            divisionsTable.Columns.Add("Code", typeof(string));
            divisionsTable.Columns.Add("Name", typeof(string));
            divisionsTable.Columns.Add("CostCenterID", typeof(int));
            gridControlDivisons.DataSource = divisionsTable;
            gridViewDivisions.Columns["CostCenterID"].Visible = false;
        }
        public void LoadProjectDivisons(string projectCode)
        {
            try
            {
                divisionsTable.Clear();
                Project project = iERPTransactionClient.GetProjectInfo(projectCode);
                waitScreen.ShowWaitForm();
                foreach (int costCenterID in project.projectData.projectDivisions)
                {
                    DataRow row = divisionsTable.NewRow();
                    CostCenter costCenter = AccountingClient.GetAccount<CostCenter>(costCenterID);
                    waitScreen.SetWaitFormDescription("Loading: " + costCenter.Name);
                    row[0] = costCenter.Code;
                    row[1] = costCenter.Name;
                    row[2] = costCenter.id;
                    divisionsTable.Rows.Add(row);
                    Application.DoEvents();
                }
                gridControlDivisons.DataSource = divisionsTable;
                gridControlDivisons.RefreshDataSource();
                waitScreen.CloseWaitForm();
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to retrieve projects\n" + ex.Message);
            }
        }

        private void gridViewDivisions_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewDivisions.SelectedRowsCount > 0)
            {
                btnRemoveDivision.Enabled = true;
                btnAddBank.Enabled = true;
                btnAddCash.Enabled = true;
            }
            else
            {
                btnRemoveDivision.Enabled = false;
                btnAddBank.Enabled = false;
                btnAddCash.Enabled = false;
            }
            
        }

        private void btnRemoveDivision_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.ShowWarningMessage("Are you sure you want to remove the selected division from the project?") == DialogResult.Yes)
                {
                    int costCenterID = (int)gridViewDivisions.GetRowCellValue(gridViewDivisions.FocusedRowHandle, "CostCenterID");
                    iERPTransactionClient.RemoveDivisionFromProject(costCenterID, m_ProjectCode);
                    gridViewDivisions.DeleteSelectedRows();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void btnAddDivision_Click(object sender, EventArgs e)
        {
            try
            {
                Project project = iERPTransactionClient.GetProjectInfo(m_ProjectCode);
                int costCenterID = project.costCenterID;
                string code = m_ProjectCode;
                AddProjectDivision division = new AddProjectDivision(false);
                division.ShowInTaskbar = false;
                division.StartPosition = FormStartPosition.CenterScreen;
                if (iERPTransactionClient.IsBankAccountCreatedUnderProject(project.projectData.bankAccounts.ToArray(), project.costCenterID))
                {
                    MessageBox.ShowErrorMessage("You cannot create division since the project/branch has bank account created under it");
                    return;
                }
                if (iERPTransactionClient.IsCashAccountCreatedUnderProject(project.projectData.cashAccounts.ToArray(), project.costCenterID))
                {
                    MessageBox.ShowErrorMessage("You cannot create division since the project/branch has cash account created under it");
                    return;
                }
                if (iERPTransactionClient.IsEmployeeCreatedUnderProject(project.projectData.employees.ToArray(), project.costCenterID))
                {
                    MessageBox.ShowErrorMessage("You cannot create division since the project/branch has employees created under it");
                    return;
                }
                if (division.ShowDialog() == DialogResult.OK)
                {
                    iERPTransactionClient.AddDivisionToProject(division.DivisionName,m_ProjectCode);
                    MessageBox.ShowSuccessMessage("Project divison successfully created!");
                    LoadProjectDivisons(m_ProjectCode);
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void btnAddEmployee_Click(object sender, EventArgs e)
        {
            try
            {
                int divisionCostCenterID = (int)gridViewDivisions.GetRowCellValue(gridViewDivisions.FocusedRowHandle, "CostCenterID");
                EmployeePicker picker = new EmployeePicker(PickerMode.Multiple);
                List<Employee> empList = new List<Employee>();
                if (picker.ShowDialog() == DialogResult.OK)
                {
                    foreach (Employee emp in picker.GetSelectedEmployees)
                    {
                        if (!IsEmployeeInvolvedInProjectDivisions(emp))
                        {
                            empList.Add(emp);
                        }
                    }
                    if (empList.Count > 0)
                    {
                        iERPTransactionClient.AddEmployeeAccountsToProject(empList.ToArray(), m_ProjectCode, divisionCostCenterID);
                        MessageBox.ShowSuccessMessage("Employee(s) successfully added to the project");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }
        private bool IsEmployeeInvolvedInProjectDivisions(Employee emp)
        {
            foreach (Project project in m_Projects)
            {
                if (project.projectType == ProjectType.Project && (emp.costCenterID == project.costCenterID && project.projectData.employees.Contains(emp.id)))
                    return true;
                foreach (int divisionCostCenterID in project.projectData.projectDivisions)
                {
                    if (emp.costCenterID == divisionCostCenterID && project.projectData.employees.Contains(emp.id))
                        return true;
                }
            }
            return false;
        }

        private void btnAddBank_Click(object sender, EventArgs e)
        {
            try
            {
                int divisionCostCenterID = (int)gridViewDivisions.GetRowCellValue(gridViewDivisions.FocusedRowHandle, "CostCenterID");
                RegisterBank bank = new RegisterBank(divisionCostCenterID, true);
                if (bank.ShowDialog() == DialogResult.OK)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void btnAddCash_Click(object sender, EventArgs e)
        {
            try
            {
                
                int divisionCostCenterID = (int)gridViewDivisions.GetRowCellValue(gridViewDivisions.FocusedRowHandle, "CostCenterID");
                CashCategoryPicker p = new CashCategoryPicker();
                if (p.ShowDialog(this) != DialogResult.OK)
                    return;
                RegisterCashAccount cashAccount = new RegisterCashAccount(p.category.id,divisionCostCenterID, true);
                if (cashAccount.ShowDialog() == DialogResult.OK)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int costCenterID = (int)gridViewDivisions.GetRowCellValue(gridViewDivisions.FocusedRowHandle, "CostCenterID");
                AddProjectDivision division = new AddProjectDivision(costCenterID, false);
                if (division.ShowDialog() == DialogResult.OK)
                {
                    CostCenter costCenter = AccountingClient.GetAccount<CostCenter>(costCenterID);
                    costCenter.Name = division.DivisionName;
                    AccountingClient.UpdateAccount<CostCenter>(costCenter);
                    MessageBox.ShowSuccessMessage("Division successfully updated!");
                    gridViewDivisions.SetRowCellValue(gridViewDivisions.FocusedRowHandle, "Name", division.DivisionName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void gridViewDivisions_DoubleClick(object sender, EventArgs e)
        {
            btnEdit_Click(null, null);
        }
    }
}
