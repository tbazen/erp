﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using INTAPS.Payroll;
using INTAPS.Payroll.Client;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    public partial class EmployeePicker : DevExpress.XtraEditors.XtraForm
    {
        TreeListColumn[] columns;
        private const string ORGUNIT_NODETYPE = "Unit";
        private const string EMPLOYEE_NODETYPE = "Employee";
        private int m_OrgImageIndex = 0;
        private TreeListNode m_SelectedNode;
        private List<Employee> m_SelectedEmployees;
        private PickerMode m_PickerMode;
        List<int> LoadedNodes;
        public EmployeePicker(PickerMode pickerMode)
        {
            InitializeComponent();
            m_PickerMode = pickerMode;
            if (pickerMode == PickerMode.Single)
                treeEmployees.OptionsView.ShowCheckBoxes = false;
            m_SelectedEmployees = new List<Employee>();
            columns = new TreeListColumn[4];
            LoadedNodes = new List<int>();
            CreateColumns();
            LoadData();
        }
        public Employee[] GetSelectedEmployees
        {
            get
            {
                return m_SelectedEmployees.ToArray();
            }
        }
        private void CreateColumns()
        {
            #region ID Column
            columns[0] = new TreeListColumn();
            columns[0].Caption = "ID";
            columns[0].FieldName = "ID";
            columns[0].MinWidth = 51;
            columns[0].Name = "colID";
            columns[0].OptionsColumn.AllowEdit = false;
            columns[0].OptionsColumn.AllowMove = false;
            columns[0].OptionsColumn.AllowMoveToCustomizationForm = false;
            columns[0].OptionsColumn.AllowSort = false;
            columns[0].UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            columns[0].Visible = true;
            columns[0].VisibleIndex = 0;
            columns[0].Width = 230;
            #endregion

            #region Name Column
            columns[1] = new TreeListColumn();
            columns[1].Caption = "Name";
            columns[1].FieldName = "Name";
            columns[1].MinWidth = 51;
            columns[1].Name = "colName";
            columns[1].OptionsColumn.AllowEdit = false;
            columns[1].OptionsColumn.AllowMove = false;
            columns[1].OptionsColumn.AllowMoveToCustomizationForm = false;
            columns[1].OptionsColumn.AllowSort = false;
            columns[1].UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            columns[1].Visible = true;
            columns[1].VisibleIndex = 1;
            columns[1].Width = 300;
            #endregion

            #region Cost Center Column
            columns[2] = new TreeListColumn();
            columns[2].Caption = "Cost Center";
            columns[2].FieldName = "CostCenter";
            columns[2].MinWidth = 51;
            columns[2].Name = "colCostCenter";
            columns[2].OptionsColumn.AllowEdit = false;
            columns[2].OptionsColumn.AllowMove = false;
            columns[2].OptionsColumn.AllowMoveToCustomizationForm = false;
            columns[2].OptionsColumn.AllowSort = false;
            columns[2].UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            columns[2].Visible = true;
            columns[2].VisibleIndex = 2;
            columns[2].Width = 170;
            #endregion

            #region Node Type Column
            columns[3] = new TreeListColumn();
            columns[3].Caption = "NodeType";
            columns[3].FieldName = "NodeType";
            columns[3].MinWidth = 51;
            columns[3].Name = "colNodeType";
            columns[3].UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            #endregion

            treeEmployees.Columns.AddRange(columns);
        }
        private void LoadData()
        {
            treeEmployees.Nodes.Clear();
            LoadChilds(treeEmployees.Nodes, -1);
            LoadChildOfChilds(treeEmployees.Nodes);
        }
        void LoadChilds(TreeListNodes nodes, int PID)
        {
            if (LoadedNodes.Contains(PID))
                return;
            LoadedNodes.Add(PID);

            OrgUnit[] orgUnits = PayrollClient.GetOrgUnits(PID);
            if (orgUnits != null)
            {
                foreach (OrgUnit unit in orgUnits)
                {
                    AddOrgUnitNode(nodes, unit);
                }
                AddEmployeeNodes(nodes, PID);
            }
        }
        void LoadChildOfChilds(TreeListNodes nodes)
        {
            foreach (TreeListNode n in nodes)
            {
                if (n.Tag is OrgUnit)
                    LoadChilds(n.Nodes, ((OrgUnit)n.Tag).ObjectIDVal);
            }
        }
        TreeListNode AddOrgUnitNode(TreeListNodes parent, OrgUnit orgUnit)
        {
            TreeListNode n = treeEmployees.AppendNode(new object[] {
                               String.Empty,
                        orgUnit.Name,
                        String.Empty,
                        ORGUNIT_NODETYPE
                           }, parent.ParentNode, orgUnit);
          //  n.StateImageIndex = m_CategoryImageIndex;
            return n;
        }
        private void AddEmployeeNodes(TreeListNodes nodes, int orgID)
        {
            Employee[] es = PayrollClient.GetEmployees(orgID, false);
            foreach (Employee e in es)
            {
                if (e.status == EmployeeStatus.Enrolled)
                    AddEmployeeNode(nodes, e);
            }
        }
        void AddEmployeeNode(TreeListNodes nodes, Employee employee)
        {
            TreeListNode n = treeEmployees.AppendNode(new object[] {
                        employee.employeeID,
                        employee.employeeName,
                        AccountingClient.GetAccount<CostCenter>(employee.costCenterID).Name ,
                        EMPLOYEE_NODETYPE
            }, nodes.ParentNode, employee);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void treeItems_AfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            if ((string)e.Node[3] == ORGUNIT_NODETYPE)
            {
                e.Node.ExpandAll();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddSelectedEmployeeNode(treeEmployees.Nodes);
            if(m_SelectedEmployees.Count>0)
            this.DialogResult = DialogResult.OK;
        }
        private void AddSelectedEmployeeNode(TreeListNodes nodes)
        {
            foreach (TreeListNode n in nodes)
            {
                bool validate = m_PickerMode == PickerMode.Multiple ? (string)n[3] == EMPLOYEE_NODETYPE && n.Checked : (string)n[3] == EMPLOYEE_NODETYPE && n.Selected;
                if (validate)
                {
                    m_SelectedEmployees.Add(PayrollClient.GetEmployee(((Employee)n.Tag).id)); //Am calling 'GetEmployee' method because the the current employee structure has no EmployeeAccounts and we need the employee account
                }
                AddSelectedEmployeeNode(n.Nodes);
            }
        }

        private void treeEmployees_DoubleClick(object sender, EventArgs e)
        {
            AddSelectedEmployeeNode(treeEmployees.Nodes);
            if (m_SelectedEmployees.Count > 0)
            this.DialogResult = DialogResult.OK;
        }

        private void treeEmployees_BeforeExpand(object sender, DevExpress.XtraTreeList.BeforeExpandEventArgs e)
        {
            LoadChildOfChilds(e.Node.Nodes);
        }
    }
    public enum PickerMode
    {
        Single,
        Multiple
    }
}