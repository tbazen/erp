﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Payroll;
using DevExpress.Utils;
using INTAPS.Payroll.Client;

namespace BIZNET.iERP.Client
{
    public partial class EmployeesListForm : DevExpress.XtraEditors.XtraUserControl
    {
        private DataTable employeeTable;
        DevExpress.XtraSplashScreen.SplashScreenManager waitScreen;
        private string m_ProjectCode;
        private Project[] m_Projects;
        public EmployeesListForm(string projectCode)
        {
            InitializeComponent();
            waitScreen = new DevExpress.XtraSplashScreen.SplashScreenManager(ProjectManager.MainProjectManager, typeof(global::BIZNET.iERP.Client.WaitForm1), true, true);
            m_ProjectCode = projectCode;
            employeeTable = new DataTable();
            PrepareDataTable();
            btnRemove.Enabled = false;
           // LoadProjectEmployees(m_ProjectCode);
            m_Projects = iERPTransactionClient.GetAllProjects();

        }

        private void PrepareDataTable()
        {
            employeeTable.Columns.Add("ID", typeof(string));
            employeeTable.Columns.Add("Name", typeof(string));
            employeeTable.Columns.Add("Sex", typeof(string));
            employeeTable.Columns.Add("Gross Salary", typeof(double));
            employeeTable.Columns.Add("Project/Division", typeof(string));
            employeeTable.Columns.Add("EmployeeID", typeof(int));
            gridControlEmployees.DataSource = employeeTable;
            gridViewEmployees.Columns["EmployeeID"].Visible = false;
            gridViewEmployees.Columns["Gross Salary"].DisplayFormat.FormatType = FormatType.Numeric;
            gridViewEmployees.Columns["Gross Salary"].DisplayFormat.FormatString = "N";
            gridViewEmployees.GroupFormat = String.Format("{0}{1}", "", "{1}");
            
        }
        public void LoadProjectEmployees(string projectCode)
        {
            try
            {
                employeeTable.Clear();
                Project project = iERPTransactionClient.GetProjectInfo(projectCode);
                waitScreen.ShowWaitForm();
                Dictionary<int, CostCenter> costCenters = new Dictionary<int, CostCenter>();
                foreach (int employeeID in project.projectData.employees)
                {
                    DataRow row = employeeTable.NewRow();
                    Employee employee = PayrollClient.GetEmployee(employeeID);
                    if (employee != null)
                    {
                        waitScreen.SetWaitFormDescription("Loading: " + employee.employeeName);
                        row[0] = employee.EmployeeNameID;
                        row[1] = employee.employeeName;
                        row[2] = employee.sex.ToString();
                        row[3] = employee.grossSalary;
                        row[4] = getCostCenter(costCenters, employee.costCenterID).Name;
                        row[5] = employee.id;
                        employeeTable.Rows.Add(row);
                        Application.DoEvents();
                    }
                }
                gridControlEmployees.DataSource = employeeTable;
                gridViewEmployees.Columns["Project/Division"].Group();
                gridViewEmployees.ExpandAllGroups();
                gridControlEmployees.RefreshDataSource();
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to retrieve employees\n" + ex.Message);
            }
            finally
            {
                waitScreen.CloseWaitForm();
            }
        }
        private CostCenter getCostCenter(Dictionary<int, CostCenter> costCenters, int costCenterID)
        {
            if (costCenters.ContainsKey(costCenterID))
                return costCenters[costCenterID];
            CostCenter cs = AccountingClient.GetAccount<CostCenter>(costCenterID);
            costCenters.Add(costCenterID, cs);
            return cs;
        }
        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                int empID = (int)gridViewEmployees.GetRowCellValue(gridViewEmployees.FocusedRowHandle, "EmployeeID");
                Project project = iERPTransactionClient.GetProjectInfo(m_ProjectCode);
                if (MessageBox.ShowWarningMessage("Are you sure you want to remove the employee?") == DialogResult.Yes)
                {
                    iERPTransactionClient.RemoveEmployeeFromProject(empID, m_ProjectCode);
                    gridViewEmployees.DeleteSelectedRows();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void gridViewEmployees_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            //if (gridViewEmployees.SelectedRowsCount > 0)
            //    btnRemove.Enabled = true;
            //else
            //    btnRemove.Enabled = false;
        }

        private void btnAddEmployee_Click(object sender, EventArgs e)
        {
            try
            {
                Project project = iERPTransactionClient.GetProjectInfo(m_ProjectCode);
                CostCenter costCenter = AccountingClient.GetAccount<CostCenter>(project.costCenterID);
                if (costCenter.childCount >= 1)
                {
                    MessageBox.ShowErrorMessage("You cannot add employees to this project/branch since a division or store is created under it!");
                    return;
                }
                EmployeePicker picker = new EmployeePicker(PickerMode.Multiple);
                List<Employee> empList = new List<Employee>();
                if (picker.ShowDialog() == DialogResult.OK)
                {
                    foreach (Employee emp in picker.GetSelectedEmployees)
                    {
                        if (!IsEmployeeInvolvedInProjects(emp))
                        {
                            empList.Add(emp);
                        }
                    }
                    if (empList.Count > 0)
                    {
                        iERPTransactionClient.AddEmployeeAccountsToProject(empList.ToArray(), m_ProjectCode, project.costCenterID);
                        MessageBox.ShowSuccessMessage("Employee(s) successfully added to the project");
                        LoadProjectEmployees(m_ProjectCode);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }
        private bool IsEmployeeInvolvedInProjects(Employee emp)
        {
            foreach (Project project in m_Projects)
            {
                if (project.projectType == ProjectType.Project && (emp.costCenterID == project.costCenterID && project.projectData.employees.Contains(emp.id)))
                    return true;
                foreach (int divisionCostCenterID in project.projectData.projectDivisions)
                {
                    if (emp.costCenterID == divisionCostCenterID && project.projectData.employees.Contains(emp.id))
                        return true;
                }
            }
            return false;
        }


    }
}
