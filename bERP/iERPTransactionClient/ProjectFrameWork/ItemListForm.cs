﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

namespace BIZNET.iERP.Client
{
    public partial class ItemListForm : DevExpress.XtraEditors.XtraUserControl
    {
        private DataTable itemsTable;
        DevExpress.XtraSplashScreen.SplashScreenManager waitScreen;
        private string m_ProjectCode;
        public ItemListForm(string projectCode)
        {
            InitializeComponent();
            waitScreen = new DevExpress.XtraSplashScreen.SplashScreenManager(ProjectManager.MainProjectManager, typeof(global::BIZNET.iERP.Client.WaitForm1), true, true);
            m_ProjectCode = projectCode;
            itemsTable = new DataTable();
            PrepareDataTable();
            InitializeInpaceTextEditor();
            btnRemove.Enabled = false;
          //  LoadProjectItems(m_ProjectCode);

        }

        private void PrepareDataTable()
        {
            itemsTable.Columns.Add("Code", typeof(string));
            itemsTable.Columns.Add("Name", typeof(string));
            itemsTable.Columns.Add("Type", typeof(string));
            itemsTable.Columns.Add("Unit Price", typeof(double));
            gridControlItems.DataSource = itemsTable;
            gridViewItems.Columns["Unit Price"].DisplayFormat.FormatType = FormatType.Numeric;
            gridViewItems.Columns["Unit Price"].DisplayFormat.FormatString = "N";

            gridViewItems.Columns["Code"].OptionsColumn.AllowEdit = false;
            gridViewItems.Columns["Name"].OptionsColumn.AllowEdit = false;
            gridViewItems.Columns["Type"].OptionsColumn.AllowEdit = false;
            gridViewItems.GroupFormat = String.Format("{0}{1}", "", "{1}");
        }
        private void InitializeInpaceTextEditor()
        {
            RepositoryItemTextEdit txtUnitPrice = new RepositoryItemTextEdit();
            txtUnitPrice.Mask.EditMask = "#,########0.00;";
            txtUnitPrice.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            txtUnitPrice.Mask.UseMaskAsDisplayFormat = true;
            gridViewItems.GridControl.RepositoryItems.Add(txtUnitPrice);
            gridViewItems.Columns["Unit Price"].ColumnEdit = txtUnitPrice;

        }
        public void LoadProjectItems(string projectCode)
        {
            waitScreen.ShowWaitForm();
            try
            {
                itemsTable.Clear();
                Project project = iERPTransactionClient.GetProjectInfo(projectCode);
                int i = 0;
                if (project.projectData.items != null)
                {
                    foreach (string itemCode in project.projectData.items)
                    {
                        DataRow row = itemsTable.NewRow();
                        TransactionItems tranItem = iERPTransactionClient.GetTransactionItems(itemCode);
                        if (tranItem != null)
                        {
                            waitScreen.SetWaitFormDescription("Loading: " + tranItem.Name);
                            row[0] = tranItem.Code;
                            row[1] = tranItem.Name;
                            row[2] = GetItemTypeDescription(tranItem);
                            row[3] = project.projectData.unitPrices[i];
                            itemsTable.Rows.Add(row);
                            i++;
                            Application.DoEvents();
                        }
                    }
                    gridControlItems.DataSource = itemsTable;
                    gridControlItems.RefreshDataSource();
                    gridViewItems.Columns["Type"].Group();
                    gridViewItems.ExpandAllGroups();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to retrieve items\n" + ex.Message);
            }
            finally
            {
                waitScreen.CloseWaitForm();
            }
        }
        private string GetItemTypeDescription(TransactionItems item)
        {
            List<string> list = new List<string>();
            if (item.IsExpenseItem)
                list.Add(item.IsDirectCost ? "Direct Cost Item" : "Expense Item");
            if (item.IsSalesItem)
                list.Add("Sales Item");
            if (item.IsFixedAssetItem)
                list.Add("Fixed Asset Item");
            return string.Join(",", list.ToArray());
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string, double> projectItems = new Dictionary<string, double>();
                if (MessageBox.ShowWarningMessage("Are you sure you want to remove the selected item from the project?") == DialogResult.Yes)
                {
                    string itemCode = (string)gridViewItems.GetRowCellValue(gridViewItems.FocusedRowHandle, "Code");
                    iERPTransactionClient.RemoveItemFromProject(itemCode, m_ProjectCode);
                    gridViewItems.DeleteSelectedRows();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string, double> projectItems = new Dictionary<string, double>();
                for (int i = 0; i < gridViewItems.DataRowCount; i++)
                {
                    if (!gridViewItems.IsGroupRow(i))
                    {
                        string itemCode = (string)gridViewItems.GetRowCellValue(i, "Code");
                        double unitPrice = gridViewItems.GetRowCellValue(i, "Unit Price") is DBNull ? 0 : (double)gridViewItems.GetRowCellValue(i, "Unit Price");
                        projectItems.Add(itemCode, unitPrice);
                    }
                }
                if (gridViewItems.DataRowCount > 0)
                {
                    Project project = iERPTransactionClient.GetProjectInfo(m_ProjectCode);
                    string[] items = new string[projectItems.Keys.Count];
                    double[] unitPrices = new double[projectItems.Values.Count];
                    projectItems.Keys.CopyTo(items, 0);
                    projectItems.Values.CopyTo(unitPrices, 0);
                    project.projectData.items = items;
                    project.projectData.unitPrices = unitPrices;
                    iERPTransactionClient.RegisterProject(project, true);
                    MessageBox.ShowSuccessMessage("Data sccessfully saved!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void gridViewItems_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewItems.SelectedRowsCount > 0)
                btnRemove.Enabled = true;
            else
                btnRemove.Enabled = false;
        }

        private void btnAddItems_Click(object sender, EventArgs e)
        {
            Dictionary<string, double> projectItems = new Dictionary<string, double>();

            try
            {
                ItemPicker picker = new ItemPicker();
                if (picker.ShowDialog() == DialogResult.OK)
                {
                    if (picker.GetSelectedItems.Length > 0)
                    {
                        iERPTransactionClient.AddItemAccountsToProject(picker.GetSelectedItems, m_ProjectCode);
                        LoadProjectItems(m_ProjectCode);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }
        private bool ItemIsAlreadyInProject(Project project, string itemCode)
        {
            if (project.projectData.items != null)
            {
                foreach (string code in project.projectData.items)
                {
                    if (itemCode == code)
                        return true;
                }
            }
            return false;
        }
    }
}
