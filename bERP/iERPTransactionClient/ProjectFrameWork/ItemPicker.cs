﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;

namespace BIZNET.iERP.Client
{
    public partial class ItemPicker : DevExpress.XtraEditors.XtraForm
    {
        TreeListColumn[] columns;
        private const string CATEGORY_NODETYPE = "Category";
        private const string ITEM_NODETYPE = "Item";
        private int m_CategoryImageIndex = 0;
        private TreeListNode m_SelectedNode;
        private bool m_CategoryClicked = false;
        private List<TransactionItems> m_SelectedItems;
        List<int> LoadedNodes;
        public ItemPicker()
        {
            InitializeComponent();
            m_SelectedItems = new List<TransactionItems>();
            columns = new TreeListColumn[4];
            LoadedNodes = new List<int>();
            CreateColumns();
            LoadData();
        }

        public TransactionItems[] GetSelectedItems
        {
            get
            {
                return m_SelectedItems.ToArray();
            }
        }
        private void CreateColumns()
        {
            #region Code Column
            columns[0] = new TreeListColumn();
            columns[0].Caption = "Code";
            columns[0].FieldName = "Code";
            columns[0].MinWidth = 51;
            columns[0].Name = "colCode";
            columns[0].OptionsColumn.AllowEdit = false;
            columns[0].OptionsColumn.AllowMove = false;
            columns[0].OptionsColumn.AllowMoveToCustomizationForm = false;
            columns[0].OptionsColumn.AllowSort = false;
            columns[0].UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            columns[0].Visible = true;
            columns[0].VisibleIndex = 0;
            columns[0].Width = 220;
            #endregion

            #region Name Column
            columns[1] = new TreeListColumn();
            columns[1].Caption = "Name";
            columns[1].FieldName = "Name";
            columns[1].MinWidth = 51;
            columns[1].Name = "colName";
            columns[1].OptionsColumn.AllowEdit = false;
            columns[1].OptionsColumn.AllowMove = false;
            columns[1].OptionsColumn.AllowMoveToCustomizationForm = false;
            columns[1].OptionsColumn.AllowSort = false;
            columns[1].UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            columns[1].Visible = true;
            columns[1].VisibleIndex = 1;
            columns[1].Width = 300;
            #endregion

            #region Type Column
            columns[2] = new TreeListColumn();
            columns[2].AllowIncrementalSearch = false;
            columns[2].Caption = "Type";
            columns[2].FieldName = "Type";
            columns[2].Name = "colType";
            columns[2].OptionsColumn.AllowEdit = false;
            columns[2].OptionsColumn.AllowMove = false;
            columns[2].OptionsColumn.AllowMoveToCustomizationForm = false;
            columns[2].OptionsColumn.AllowSort = false;
            columns[2].UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            columns[2].Visible = true;
            columns[2].VisibleIndex = 2;
            columns[2].Width = 200;
            #endregion

            #region Node Type Column
            columns[3] = new TreeListColumn();
            columns[3].Caption = "NodeType";
            columns[3].FieldName = "NodeType";
            columns[3].MinWidth = 51;
            columns[3].Name = "colNodeType";
            columns[3].UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            #endregion

            treeItems.Columns.AddRange(columns);
        }
        private void LoadData()
        {
            treeItems.Nodes.Clear();
            LoadChilds(treeItems.Nodes, -1);
            LoadChildOfChilds(treeItems.Nodes);
        }
        void LoadChilds(TreeListNodes nodes, int PID)
        {
            if (LoadedNodes.Contains(PID))
                return;
            LoadedNodes.Add(PID);

            ItemCategory[] categories = iERPTransactionClient.GetItemCategories(PID);
            if (categories != null)
            {
                foreach (ItemCategory categ in categories)
                {
                    AddItemCategoryNode(nodes, categ);
                }
                AddItemNodes(nodes, PID);
            }

        }
        void LoadChildOfChilds(TreeListNodes nodes)
        {
            foreach (TreeListNode n in nodes)
            {
                if (n.Tag is ItemCategory)
                    LoadChilds(n.Nodes, ((ItemCategory)n.Tag).ID);
            }
        }
        TreeListNode AddItemCategoryNode(TreeListNodes parent, ItemCategory category)
        {
            TreeListNode n = treeItems.AppendNode(new object[] {
                               category.Code,
                        category.description,
                        String.Empty,
                        CATEGORY_NODETYPE
                           }, parent.ParentNode, category);
          //  n.StateImageIndex = m_CategoryImageIndex;
            return n;
        }
        private void AddItemNodes(TreeListNodes nodes, int categId)
        {
            TransactionItems[] items = iERPTransactionClient.GetItemsInCategory(categId);
            if (items != null)
            {
                foreach (TransactionItems item in items)
                {
                    AddItemNode(nodes, item);
                }
            }
        }
        void AddItemNode(TreeListNodes nodes, TransactionItems item)
        {
            TreeListNode n = treeItems.AppendNode(new object[] {
                        item.Code,
                        item.Name,
                        GetItemTypeDescription(item),
                        ITEM_NODETYPE
            }, nodes.ParentNode, item);
        }
        private string GetItemTypeDescription(TransactionItems item)
        {
            List<string> list = new List<string>();
            if (item.IsExpenseItem)
                list.Add(item.IsDirectCost ? "Direct Cost Item" : "Expense Item");
            if (item.IsSalesItem)
                list.Add("Sales Item");
            if (item.IsFixedAssetItem)
                list.Add("Fixed Asset Item");
            return string.Join(",", list.ToArray());
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void treeItems_AfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            if ((string)e.Node[3] == CATEGORY_NODETYPE)
            {
                e.Node.ExpandAll();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddSelectedItemNode(treeItems.Nodes);
            this.DialogResult = DialogResult.OK;
        }
        private void AddSelectedItemNode(TreeListNodes nodes)
        {
            foreach (TreeListNode n in nodes)
            {
                if ((string)n[3] == ITEM_NODETYPE && n.Checked)
                {
                    m_SelectedItems.Add((TransactionItems)n.Tag);
                }
                AddSelectedItemNode(n.Nodes);
            }
        }

        private void treeItems_DoubleClick(object sender, EventArgs e)
        {
            AddSelectedItemNode(treeItems.Nodes);
            this.DialogResult = DialogResult.OK;
        }
    }
}