﻿namespace BIZNET.iERP.Client
{
    partial class MachinaryAndVehiclesListForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MachinaryAndVehiclesListForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.btnRemoveMV = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlMachinaryAndVehicles = new DevExpress.XtraGrid.GridControl();
            this.gridViewMachinaryAndVehicles = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMachinaryAndVehicles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMachinaryAndVehicles)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnEdit);
            this.panelControl1.Controls.Add(this.btnAdd);
            this.panelControl1.Controls.Add(this.btnRemoveMV);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(702, 36);
            this.panelControl1.TabIndex = 1;
            // 
            // btnEdit
            // 
            this.btnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Appearance.Options.UseFont = true;
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.Location = new System.Drawing.Point(176, 2);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(73, 31);
            this.btnEdit.TabIndex = 0;
            this.btnEdit.Text = "&Edit";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Appearance.Options.UseFont = true;
            this.btnAdd.Location = new System.Drawing.Point(3, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(167, 31);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "&Add Machinary/Vehicle";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRemoveMV
            // 
            this.btnRemoveMV.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemoveMV.Appearance.Options.UseFont = true;
            this.btnRemoveMV.Image = ((System.Drawing.Image)(resources.GetObject("btnRemoveMV.Image")));
            this.btnRemoveMV.Location = new System.Drawing.Point(255, 2);
            this.btnRemoveMV.Name = "btnRemoveMV";
            this.btnRemoveMV.Size = new System.Drawing.Size(208, 31);
            this.btnRemoveMV.TabIndex = 0;
            this.btnRemoveMV.Text = "&Remove Machinary/Vehicle";
            this.btnRemoveMV.Click += new System.EventHandler(this.btnRemoveDivision_Click);
            // 
            // gridControlMachinaryAndVehicles
            // 
            this.gridControlMachinaryAndVehicles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlMachinaryAndVehicles.Location = new System.Drawing.Point(0, 36);
            this.gridControlMachinaryAndVehicles.MainView = this.gridViewMachinaryAndVehicles;
            this.gridControlMachinaryAndVehicles.Name = "gridControlMachinaryAndVehicles";
            this.gridControlMachinaryAndVehicles.Size = new System.Drawing.Size(702, 472);
            this.gridControlMachinaryAndVehicles.TabIndex = 2;
            this.gridControlMachinaryAndVehicles.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMachinaryAndVehicles});
            // 
            // gridViewMachinaryAndVehicles
            // 
            this.gridViewMachinaryAndVehicles.GridControl = this.gridControlMachinaryAndVehicles;
            this.gridViewMachinaryAndVehicles.Name = "gridViewMachinaryAndVehicles";
            this.gridViewMachinaryAndVehicles.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewMachinaryAndVehicles.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewMachinaryAndVehicles.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewMachinaryAndVehicles.OptionsBehavior.Editable = false;
            this.gridViewMachinaryAndVehicles.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewMachinaryAndVehicles.OptionsCustomization.AllowFilter = false;
            this.gridViewMachinaryAndVehicles.OptionsCustomization.AllowGroup = false;
            this.gridViewMachinaryAndVehicles.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewMachinaryAndVehicles.OptionsCustomization.AllowSort = false;
            this.gridViewMachinaryAndVehicles.OptionsView.ShowGroupPanel = false;
            this.gridViewMachinaryAndVehicles.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewDivisions_FocusedRowChanged);
            this.gridViewMachinaryAndVehicles.DoubleClick += new System.EventHandler(this.gridViewMachinaryAndVehicles_DoubleClick);
            // 
            // MachinaryAndVehiclesListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlMachinaryAndVehicles);
            this.Controls.Add(this.panelControl1);
            this.Name = "MachinaryAndVehiclesListForm";
            this.Size = new System.Drawing.Size(702, 508);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMachinaryAndVehicles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMachinaryAndVehicles)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnRemoveMV;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControlMachinaryAndVehicles;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewMachinaryAndVehicles;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
    }
}
