﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Payroll;
using INTAPS.Payroll.Client;

namespace BIZNET.iERP.Client
{
    public partial class MachinaryAndVehiclesListForm : DevExpress.XtraEditors.XtraUserControl
    {
        private DataTable mvTable;
        DevExpress.XtraSplashScreen.SplashScreenManager waitScreen;
        private string m_ProjectCode;
        public MachinaryAndVehiclesListForm(string projectCode)
        {
            InitializeComponent();
            waitScreen = new DevExpress.XtraSplashScreen.SplashScreenManager(ProjectManager.MainProjectManager, typeof(global::BIZNET.iERP.Client.WaitForm1), true, true);
            m_ProjectCode = projectCode;
            mvTable = new DataTable();
            PrepareDataTable();
            btnRemoveMV.Enabled = false;
            btnEdit.Enabled = false;
            LoadMachinaryAndVehicles(m_ProjectCode);

        }

        private void PrepareDataTable()
        {
            mvTable.Columns.Add("Code", typeof(string));
            mvTable.Columns.Add("Name", typeof(string));
            mvTable.Columns.Add("CostCenterID", typeof(int));
            gridControlMachinaryAndVehicles.DataSource = mvTable;
            gridViewMachinaryAndVehicles.Columns["CostCenterID"].Visible = false;
        }
        public void LoadMachinaryAndVehicles(string projectCode)
        {
            try
            {
                mvTable.Clear();
                Project project = iERPTransactionClient.GetProjectInfo(projectCode);
                waitScreen.ShowWaitForm();
                foreach (int costCenterID in project.projectData.machinaryAndVehicles)
                {
                    DataRow row = mvTable.NewRow();
                    CostCenter costCenter = AccountingClient.GetAccount<CostCenter>(costCenterID);
                    waitScreen.SetWaitFormDescription("Loading: " + costCenter.Name);
                    row[0] = costCenter.Code;
                    row[1] = costCenter.Name;
                    row[2] = costCenter.id;
                    mvTable.Rows.Add(row);
                    Application.DoEvents();
                }
                gridControlMachinaryAndVehicles.DataSource = mvTable;
                gridControlMachinaryAndVehicles.RefreshDataSource();
                waitScreen.CloseWaitForm();
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to retrieve projects\n" + ex.Message);
            }
        }

        private void gridViewDivisions_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewMachinaryAndVehicles.SelectedRowsCount > 0)
            {
                btnRemoveMV.Enabled = true;
                btnEdit.Enabled = true;
            }
            else
            {
                btnRemoveMV.Enabled = false;
                btnEdit.Enabled = false;
            }
            
        }

        private void btnRemoveDivision_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.ShowWarningMessage("Are you sure you want to remove the selected Machinary/Vehicle from the project?") == DialogResult.Yes)
                {
                    int costCenterID = (int)gridViewMachinaryAndVehicles.GetRowCellValue(gridViewMachinaryAndVehicles.FocusedRowHandle, "CostCenterID");
                    iERPTransactionClient.RemoveMachinaryAndVehicleFromProject(costCenterID, m_ProjectCode);
                    gridViewMachinaryAndVehicles.DeleteSelectedRows();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

       
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Project project = iERPTransactionClient.GetProjectInfo(m_ProjectCode);
                int costCenterID = project.costCenterID;
                string code = m_ProjectCode;
                AddProjectDivision division = new AddProjectDivision(true);
                if (iERPTransactionClient.IsBankAccountCreatedUnderProject(project.projectData.bankAccounts.ToArray(), project.costCenterID))
                {
                    MessageBox.ShowErrorMessage("You cannot create Machinary/Vehicle since the project/branch has bank account created under it");
                    return;
                }
                if (iERPTransactionClient.IsCashAccountCreatedUnderProject(project.projectData.cashAccounts.ToArray(), project.costCenterID))
                {
                    MessageBox.ShowErrorMessage("You cannot create Machinary/Vehicle since the project/branch has cash account created under it");
                    return;
                }
                if (iERPTransactionClient.IsEmployeeCreatedUnderProject(project.projectData.employees.ToArray(), project.costCenterID))
                {
                    MessageBox.ShowErrorMessage("You cannot create Machinary/Vehicle since the project/branch has employees created under it");
                    return;
                }
                if (division.ShowDialog() == DialogResult.OK)
                {
                    iERPTransactionClient.AddMachinaryAndVehicleToProject(division.DivisionName, m_ProjectCode);
                    MessageBox.ShowSuccessMessage("Machinary/Vehicle successfully created!");
                    LoadMachinaryAndVehicles(m_ProjectCode);
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int costCenterID = (int)gridViewMachinaryAndVehicles.GetRowCellValue(gridViewMachinaryAndVehicles.FocusedRowHandle, "CostCenterID");
                AddProjectDivision mvCostCenter = new AddProjectDivision(costCenterID, true);
                if (mvCostCenter.ShowDialog() == DialogResult.OK)
                {
                    CostCenter costCenter = AccountingClient.GetAccount<CostCenter>(costCenterID);
                    costCenter.Name = mvCostCenter.DivisionName;
                    AccountingClient.UpdateAccount<CostCenter>(costCenter);
                    MessageBox.ShowSuccessMessage("Machinary/Vehicle successfully updated!");
                    gridViewMachinaryAndVehicles.SetRowCellValue(gridViewMachinaryAndVehicles.FocusedRowHandle, "Name", mvCostCenter.DivisionName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void gridViewMachinaryAndVehicles_DoubleClick(object sender, EventArgs e)
        {
            btnEdit_Click(null, null);
        }
    }
}
