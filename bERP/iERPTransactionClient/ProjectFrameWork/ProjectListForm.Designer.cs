﻿namespace BIZNET.iERP.Client
{
    partial class ProjectListForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectListForm));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnNewProject = new DevExpress.XtraBars.BarButtonItem();
            this.btnEditProject = new DevExpress.XtraBars.BarButtonItem();
            this.btnDeleteProject = new DevExpress.XtraBars.BarButtonItem();
            this.btnActivateProject = new DevExpress.XtraBars.BarButtonItem();
            this.btnShowDetails = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.gridControlProjects = new DevExpress.XtraGrid.GridControl();
            this.gridViewProjects = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlProjects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProjects)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnNewProject,
            this.btnEditProject,
            this.btnDeleteProject,
            this.btnActivateProject,
            this.btnShowDetails});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 11;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnNewProject, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnEditProject, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnDeleteProject, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnActivateProject, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnShowDetails, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DisableClose = true;
            this.bar2.OptionsBar.DisableCustomization = true;
            this.bar2.OptionsBar.DrawBorder = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnNewProject
            // 
            this.btnNewProject.Caption = "&New";
            this.btnNewProject.Glyph = ((System.Drawing.Image)(resources.GetObject("btnNewProject.Glyph")));
            this.btnNewProject.Id = 0;
            this.btnNewProject.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewProject.ItemAppearance.Normal.Options.UseFont = true;
            this.btnNewProject.Name = "btnNewProject";
            this.btnNewProject.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNewProject_ItemClick);
            // 
            // btnEditProject
            // 
            this.btnEditProject.Caption = "&Edit";
            this.btnEditProject.Glyph = ((System.Drawing.Image)(resources.GetObject("btnEditProject.Glyph")));
            this.btnEditProject.Id = 1;
            this.btnEditProject.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditProject.ItemAppearance.Normal.Options.UseFont = true;
            this.btnEditProject.Name = "btnEditProject";
            this.btnEditProject.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEditProject_ItemClick);
            // 
            // btnDeleteProject
            // 
            this.btnDeleteProject.Caption = "&Delete";
            this.btnDeleteProject.Glyph = ((System.Drawing.Image)(resources.GetObject("btnDeleteProject.Glyph")));
            this.btnDeleteProject.Id = 2;
            this.btnDeleteProject.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteProject.ItemAppearance.Normal.Options.UseFont = true;
            this.btnDeleteProject.Name = "btnDeleteProject";
            this.btnDeleteProject.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDeleteProject_ItemClick);
            // 
            // btnActivateProject
            // 
            this.btnActivateProject.Caption = "Activate/Deac&tivate";
            this.btnActivateProject.Glyph = ((System.Drawing.Image)(resources.GetObject("btnActivateProject.Glyph")));
            this.btnActivateProject.Id = 3;
            this.btnActivateProject.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActivateProject.ItemAppearance.Normal.Options.UseFont = true;
            this.btnActivateProject.Name = "btnActivateProject";
            this.btnActivateProject.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnActivateProject_ItemClick);
            // 
            // btnShowDetails
            // 
            this.btnShowDetails.Caption = "S&how Details";
            this.btnShowDetails.Glyph = ((System.Drawing.Image)(resources.GetObject("btnShowDetails.Glyph")));
            this.btnShowDetails.Id = 10;
            this.btnShowDetails.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShowDetails.ItemAppearance.Normal.Options.UseFont = true;
            this.btnShowDetails.Name = "btnShowDetails";
            this.btnShowDetails.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnShowDetails_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(794, 40);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 492);
            this.barDockControlBottom.Size = new System.Drawing.Size(794, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 40);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 452);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(794, 40);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 452);
            // 
            // gridControlProjects
            // 
            this.gridControlProjects.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlProjects.Location = new System.Drawing.Point(0, 40);
            this.gridControlProjects.MainView = this.gridViewProjects;
            this.gridControlProjects.MenuManager = this.barManager1;
            this.gridControlProjects.Name = "gridControlProjects";
            this.gridControlProjects.Size = new System.Drawing.Size(794, 452);
            this.gridControlProjects.TabIndex = 4;
            this.gridControlProjects.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewProjects});
            // 
            // gridViewProjects
            // 
            this.gridViewProjects.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewProjects.Appearance.Row.Options.UseFont = true;
            this.gridViewProjects.GridControl = this.gridControlProjects;
            this.gridViewProjects.Name = "gridViewProjects";
            this.gridViewProjects.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewProjects.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewProjects.OptionsBehavior.Editable = false;
            this.gridViewProjects.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewProjects.OptionsCustomization.AllowFilter = false;
            this.gridViewProjects.OptionsCustomization.AllowGroup = false;
            this.gridViewProjects.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewProjects.OptionsCustomization.AllowSort = false;
            this.gridViewProjects.OptionsView.ShowGroupPanel = false;
            this.gridViewProjects.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewProjects_FocusedRowChanged);
            this.gridViewProjects.DoubleClick += new System.EventHandler(this.gridViewProjects_DoubleClick);
            // 
            // ProjectListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 492);
            this.Controls.Add(this.gridControlProjects);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ProjectListForm";
            this.ShowIcon = false;
            this.Text = "Project List Manager";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlProjects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProjects)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnNewProject;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnEditProject;
        private DevExpress.XtraBars.BarButtonItem btnDeleteProject;
        private DevExpress.XtraBars.BarButtonItem btnActivateProject;
        private DevExpress.XtraGrid.GridControl gridControlProjects;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewProjects;
        private DevExpress.XtraBars.BarButtonItem btnShowDetails;
    }
}
