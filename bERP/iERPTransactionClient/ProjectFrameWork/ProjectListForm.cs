﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Payroll.Client;
using DevExpress.Utils;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using DevExpress.XtraGrid;
using INTAPS.Payroll;

namespace BIZNET.iERP.Client
{
    public partial class ProjectListForm : DevExpress.XtraEditors.XtraForm
    {
        private DataTable projectTable;
        DevExpress.XtraSplashScreen.SplashScreenManager waitScreen;
        private Project[] m_Projects;
        public static ProjectListForm ProjectList;
        public ProjectListForm()
        {
            InitializeComponent();
            waitScreen = new DevExpress.XtraSplashScreen.SplashScreenManager(ProjectManager.MainProjectManager, typeof(global::BIZNET.iERP.Client.WaitForm1), true, true);
            projectTable = new DataTable();
            PrepareDataTable();
            DisableMenuButtons();
            LoadProjects();
        }

        private void PrepareDataTable()
        {
            projectTable.Columns.Add("Code", typeof(string));
            projectTable.Columns.Add("Name", typeof(string));
            projectTable.Columns.Add("Manager", typeof(string));
            projectTable.Columns.Add("Type", typeof(string));
            projectTable.Columns.Add("Contract Price", typeof(double));
            projectTable.Columns.Add("CostCenterID", typeof(int));
            projectTable.Columns.Add("Activated", typeof(bool));
            gridControlProjects.DataSource = projectTable;
            gridViewProjects.Columns["Activated"].Visible = false;
            gridViewProjects.Columns["CostCenterID"].Visible = false;
            gridViewProjects.Columns["Contract Price"].DisplayFormat.FormatType = FormatType.Numeric;
            gridViewProjects.Columns["Contract Price"].DisplayFormat.FormatString = "N";
        }
        public void LoadProjects()
        {
            try
            {
                projectTable.Clear();
                m_Projects = iERPTransactionClient.GetAllProjects();
                waitScreen.ShowWaitForm();
                foreach (Project project in m_Projects)
                {
                    DataRow row = projectTable.NewRow();
                    waitScreen.SetWaitFormDescription("Loading: " + project.Name);
                    CostCenter costCenter = AccountingClient.GetAccount<CostCenter>(project.costCenterID);
                    Employee emp = null;
                    if (project.employeeID != -1)
                        emp = PayrollClient.GetEmployee(project.employeeID);
                    row[0] = project.code;
                    row[1] = project.Name;
                    if (emp != null)
                        row[2] = emp.employeeName;
                    row[3] = project.projectType.ToString();
                    row[4] = project.contractPrice;
                    row[5] = project.costCenterID;
                    row[6] = costCenter.Status == AccountStatus.Activated ? true : false;
                    projectTable.Rows.Add(row);
                    Application.DoEvents();
                }
                gridControlProjects.DataSource = projectTable;
                gridControlProjects.RefreshDataSource();
                ApplyFormatConditioning();

            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to retrieve projects\n" + ex.Message);
            }
            finally
            {
                waitScreen.CloseWaitForm();
            }
        }
        private void DisableMenuButtons()
        {
            btnEditProject.Enabled = btnDeleteProject.Enabled = btnActivateProject.Enabled = btnShowDetails.Enabled = false;
        }

        private void gridViewProjects_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewProjects.SelectedRowsCount > 0)
            {
                EnableMenuButtons();
                bool activated = (bool)gridViewProjects.GetRowCellValue(gridViewProjects.FocusedRowHandle, "Activated");
                btnActivateProject.Caption = activated ? "Deactivate Project" : "Activate Project";
            }
            else
                DisableMenuButtons();
        }

        private void EnableMenuButtons()
        {
            btnEditProject.Enabled = btnDeleteProject.Enabled = btnActivateProject.Enabled = btnShowDetails.Enabled = true;
        }

        private void btnNewProject_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RegisterProject regProject = new RegisterProject();
            regProject.ProjectAdded += regProject_ProjectAdded;
            regProject.ShowInTaskbar = false;
            regProject.StartPosition = FormStartPosition.CenterScreen;
            regProject.ShowDialog();
        }

        void regProject_ProjectAdded(string code)
        {
            LoadProjects();
        }

        private void btnEditProject_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string code = (string)gridViewProjects.GetRowCellValue(gridViewProjects.FocusedRowHandle, "Code");
            RegisterProject project = new RegisterProject(code);
            project.ShowInTaskbar = false;
            project.StartPosition = FormStartPosition.CenterScreen;
            if (project.ShowDialog() == DialogResult.OK)
            {
                LoadProjects();
            }
        }

        private void btnDeleteProject_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                string code = (string)gridViewProjects.GetRowCellValue(gridViewProjects.FocusedRowHandle, "Code");
                if (MessageBox.ShowWarningMessage("Are you sure you want to delete the project?") == DialogResult.Yes)
                {
                    iERPTransactionClient.DeleteProject(code);
                    MessageBox.ShowSuccessMessage("Project successfully deleted");
                    gridViewProjects.DeleteSelectedRows();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void btnActivateProject_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string code = (string)gridViewProjects.GetRowCellValue(gridViewProjects.FocusedRowHandle, "Code");
            bool activated = (bool)gridViewProjects.GetRowCellValue(gridViewProjects.FocusedRowHandle, "Activated");
            ActivateOrDeactivateProject(code, activated);
        }

        private void ActivateOrDeactivateProject(string code, bool activated)
        {
            try
            {
                bool confirmed = false;
                if (activated)
                {
                    if (MessageBox.ShowWarningMessage("Are you sure you want to deactivate the project?") == DialogResult.Yes)
                    {
                        iERPTransactionClient.DeactivateProject(code);
                        confirmed = true;
                    }
                }
                else
                {
                    if (MessageBox.ShowWarningMessage("Are you sure you want to activate the project?") == DialogResult.Yes)
                    {
                        iERPTransactionClient.ActivateProject(code);
                        confirmed = true;
                    }
                }
                if (confirmed)
                {
                    string message = activated ? "Project successfully deactivated" : "Project successfully activated";
                    MessageBox.ShowSuccessMessage(message);
                    LoadProjects();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to activate/deactivate project!\n" + ex.Message);
            }
        }

        private void ApplyFormatConditioning()
        {
            DevExpress.XtraGrid.StyleFormatCondition condition = new DevExpress.XtraGrid.StyleFormatCondition();
            condition.Appearance.Options.UseBackColor = true;
            condition.Appearance.BackColor = Color.FromArgb(0x8F, 0xF2, 0x14, 0x43);
            condition.Condition = FormatConditionEnum.Expression;
            condition.Expression = "[Activated] != True";
            condition.ApplyToRow = true;
            gridViewProjects.FormatConditions.Add(condition);
        }


        private bool IsEmployeeInvolvedInProject(int employeeID)
        {
            foreach (Project project in m_Projects)
            {
                foreach (int empID in project.projectData.employees)
                {
                    if (employeeID == empID)
                        return true;
                }
            }
            return false;
        }
        private void btnShowDetails_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string projectCode = (string)gridViewProjects.GetRowCellValue(gridViewProjects.FocusedRowHandle, "Code");
            ProjectManager manager = new ProjectManager(projectCode);
            ProjectManager.MainProjectManager = manager;
            manager.Show(this);
        }

        private void gridViewProjects_DoubleClick(object sender, EventArgs e)
        {
            btnShowDetails_ItemClick(null, null);
        }

       
    }
}
