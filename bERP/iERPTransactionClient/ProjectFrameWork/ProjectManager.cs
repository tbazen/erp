﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using DevExpress.XtraEditors;
using INTAPS.Payroll.Client;
using DevExpress.XtraLayout;

namespace BIZNET.iERP.Client
{
    public partial class ProjectManager : XtraForm
    {
        public ProjectListForm m_ProjectListForm;
        private DivisionsListForm m_DivisionListForm;
        private MachinaryAndVehiclesListForm m_MachinaryAndVehicleListForm;
        private EmployeesListForm m_EmployeeListForm;
        private ItemListForm m_ItemListForm;
        private StoreList m_StoreListForm;
        private CashAccountList m_CashAccountList;
        private BankAccountList m_BankAccountList;
        public static ProjectManager MainProjectManager;
        private string m_ProjectCode;
        public ProjectManager(string projectCode)
        {
            InitializeComponent();
            m_ProjectCode = projectCode;
            this.Text = iERPTransactionClient.GetProjectInfo(projectCode).Name + " details";
            m_DivisionListForm = new DivisionsListForm(projectCode);
            m_MachinaryAndVehicleListForm = new MachinaryAndVehiclesListForm(projectCode);
            m_EmployeeListForm = new EmployeesListForm(projectCode);
            m_ItemListForm = new ItemListForm(projectCode);
            m_StoreListForm = new StoreList(projectCode);
            m_CashAccountList = new CashAccountList(projectCode);
            m_BankAccountList = new BankAccountList(projectCode);
            navDivisions_LinkClicked(null, null);
        }


       

        private void navDivisions_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            AddDivisionListPage();
            m_DivisionListForm.LoadProjectDivisons(m_ProjectCode);
        }

        private void AddDivisionListPage()
        {
            panForms.Controls.Clear();
            panForms.Controls.Add(m_DivisionListForm);
            m_DivisionListForm.Dock = DockStyle.Fill;
        }

        private void navEmployees_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            AddEmployeeListPage();
            m_EmployeeListForm.LoadProjectEmployees(m_ProjectCode);
        }

        private void AddEmployeeListPage()
        {
            panForms.Controls.Clear();
            panForms.Controls.Add(m_EmployeeListForm);
            m_EmployeeListForm.Dock = DockStyle.Fill;
        }

        private void navItems_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            AddItemsListPage();
            m_ItemListForm.LoadProjectItems(m_ProjectCode);
        }

        private void AddItemsListPage()
        {
            panForms.Controls.Clear();
            panForms.Controls.Add(m_ItemListForm);
            m_ItemListForm.Dock = DockStyle.Fill;
        }

        private void navStores_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            AddStoreListPage();
            m_StoreListForm.LoadProjectStores(m_ProjectCode);
        }

        private void AddStoreListPage()
        {
            panForms.Controls.Clear();
            panForms.Controls.Add(m_StoreListForm);
            m_StoreListForm.Dock = DockStyle.Fill;
        }

        private void navCashAccounts_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            AddCashAccountsPage();
            m_CashAccountList.LoadProjectCashAccounts(m_ProjectCode);
        }

        private void AddCashAccountsPage()
        {
            panForms.Controls.Clear();
            panForms.Controls.Add(m_CashAccountList);
            m_CashAccountList.Dock = DockStyle.Fill;
        }

        private void navBankAccounts_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            AddBankAccountsPage();
            m_BankAccountList.LoadProjectBankAccounts(m_ProjectCode);
        }

        private void AddBankAccountsPage()
        {
            panForms.Controls.Clear();
            panForms.Controls.Add(m_BankAccountList);
            m_BankAccountList.Dock = DockStyle.Fill;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void navMachinaryAndVehicles_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            AddMachinaryAndVehicleListPage();
            m_MachinaryAndVehicleListForm.LoadMachinaryAndVehicles(m_ProjectCode);
        }
        private void AddMachinaryAndVehicleListPage()
        {
            panForms.Controls.Clear();
            panForms.Controls.Add(m_MachinaryAndVehicleListForm);
            m_MachinaryAndVehicleListForm.Dock = DockStyle.Fill;
        }
    }
        
}
