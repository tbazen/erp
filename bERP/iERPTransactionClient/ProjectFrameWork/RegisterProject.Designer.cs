﻿namespace BIZNET.iERP.Client
{
    partial class RegisterProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule3 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.cmbProjectType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtDescription = new DevExpress.XtraEditors.MemoEdit();
            this.txtContractPrice = new DevExpress.XtraEditors.TextEdit();
            this.btnPickManager = new DevExpress.XtraEditors.ButtonEdit();
            this.dtStartDate = new BIZNET.iERP.Client.BNDualCalendar();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutProjManager = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutContractPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutProjectType = new DevExpress.XtraLayout.LayoutControlItem();
            this.validationProject = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbProjectType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContractPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPickManager.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutProjManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutContractPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutProjectType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationProject)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.cmbProjectType);
            this.layoutControl1.Controls.Add(this.txtDescription);
            this.layoutControl1.Controls.Add(this.txtContractPrice);
            this.layoutControl1.Controls.Add(this.btnPickManager);
            this.layoutControl1.Controls.Add(this.dtStartDate);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.txtName);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(602, 142, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(412, 253);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // cmbProjectType
            // 
            this.cmbProjectType.EditValue = "Project";
            this.cmbProjectType.Location = new System.Drawing.Point(95, 86);
            this.cmbProjectType.Name = "cmbProjectType";
            this.cmbProjectType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbProjectType.Properties.Items.AddRange(new object[] {
            "Project",
            "Branch"});
            this.cmbProjectType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbProjectType.Size = new System.Drawing.Size(309, 20);
            this.cmbProjectType.StyleController = this.layoutControl1;
            this.cmbProjectType.TabIndex = 16;
            this.cmbProjectType.SelectedIndexChanged += new System.EventHandler(this.cmbProjectType_SelectedIndexChanged);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(95, 176);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(309, 36);
            this.txtDescription.StyleController = this.layoutControl1;
            this.txtDescription.TabIndex = 15;
            // 
            // txtContractPrice
            // 
            this.txtContractPrice.Location = new System.Drawing.Point(95, 146);
            this.txtContractPrice.Name = "txtContractPrice";
            this.txtContractPrice.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtContractPrice.Properties.Mask.EditMask = "#,########0.00;";
            this.txtContractPrice.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtContractPrice.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtContractPrice.Size = new System.Drawing.Size(309, 20);
            this.txtContractPrice.StyleController = this.layoutControl1;
            this.txtContractPrice.TabIndex = 14;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "Contract Price cannot be blank";
            this.validationProject.SetValidationRule(this.txtContractPrice, conditionValidationRule1);
            // 
            // btnPickManager
            // 
            this.btnPickManager.Location = new System.Drawing.Point(95, 116);
            this.btnPickManager.Name = "btnPickManager";
            this.btnPickManager.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnPickManager.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.btnPickManager.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEdit1_Properties_ButtonClick);
            this.btnPickManager.Size = new System.Drawing.Size(309, 20);
            this.btnPickManager.StyleController = this.layoutControl1;
            this.btnPickManager.TabIndex = 13;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "Project Manager cannot be blank";
            this.validationProject.SetValidationRule(this.btnPickManager, conditionValidationRule2);
            // 
            // dtStartDate
            // 
            this.dtStartDate.Location = new System.Drawing.Point(95, 38);
            this.dtStartDate.Name = "dtStartDate";
            this.dtStartDate.ShowEthiopian = true;
            this.dtStartDate.ShowGregorian = true;
            this.dtStartDate.ShowTime = false;
            this.dtStartDate.Size = new System.Drawing.Size(309, 42);
            this.dtStartDate.TabIndex = 12;
            this.dtStartDate.VerticalLayout = true;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnCancel);
            this.panelControl1.Controls.Add(this.btnOk);
            this.panelControl1.Location = new System.Drawing.Point(5, 219);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(402, 29);
            this.panelControl1.TabIndex = 11;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(322, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Close";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(241, 2);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "&Ok";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(95, 8);
            this.txtName.Name = "txtName";
            this.txtName.Properties.Mask.EditMask = "[0-9a-zA-Z].*";
            this.txtName.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtName.Size = new System.Drawing.Size(309, 20);
            this.txtName.StyleController = this.layoutControl1;
            this.txtName.TabIndex = 6;
            conditionValidationRule3.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule3.ErrorText = "Project name cannot be empty";
            this.validationProject.SetValidationRule(this.txtName, conditionValidationRule3);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem3,
            this.layoutControlItem1,
            this.layoutProjManager,
            this.layoutContractPrice,
            this.layoutControlItem5,
            this.layoutProjectType});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(412, 253);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.panelControl1;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 214);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(406, 33);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtName;
            this.layoutControlItem3.CustomizationFormText = "TradeRelation Name:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(406, 30);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "Name:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dtStartDate;
            this.layoutControlItem1.CustomizationFormText = "Start Date:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 48);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(191, 48);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(406, 48);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Text = "Start Date:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutProjManager
            // 
            this.layoutProjManager.Control = this.btnPickManager;
            this.layoutProjManager.CustomizationFormText = "Project Manager:";
            this.layoutProjManager.Location = new System.Drawing.Point(0, 108);
            this.layoutProjManager.Name = "layoutProjManager";
            this.layoutProjManager.Size = new System.Drawing.Size(406, 30);
            this.layoutProjManager.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutProjManager.Text = "Project Manager:";
            this.layoutProjManager.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutContractPrice
            // 
            this.layoutContractPrice.Control = this.txtContractPrice;
            this.layoutContractPrice.CustomizationFormText = "Contract Price:";
            this.layoutContractPrice.Location = new System.Drawing.Point(0, 138);
            this.layoutContractPrice.Name = "layoutContractPrice";
            this.layoutContractPrice.Size = new System.Drawing.Size(406, 30);
            this.layoutContractPrice.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutContractPrice.Text = "Contract Price:";
            this.layoutContractPrice.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtDescription;
            this.layoutControlItem5.CustomizationFormText = "Description:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(406, 46);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem5.Text = "Description:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutProjectType
            // 
            this.layoutProjectType.Control = this.cmbProjectType;
            this.layoutProjectType.CustomizationFormText = "Type:";
            this.layoutProjectType.Location = new System.Drawing.Point(0, 78);
            this.layoutProjectType.Name = "layoutProjectType";
            this.layoutProjectType.Size = new System.Drawing.Size(406, 30);
            this.layoutProjectType.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutProjectType.Text = "Type:";
            this.layoutProjectType.TextSize = new System.Drawing.Size(83, 13);
            // 
            // validationProject
            // 
            this.validationProject.ValidateHiddenControls = false;
            // 
            // RegisterProject
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(412, 253);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "RegisterProject";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Create Project";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RegisterProject_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbProjectType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContractPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPickManager.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutProjManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutContractPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutProjectType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationProject)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validationProject;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private BNDualCalendar dtStartDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.ButtonEdit btnPickManager;
        private DevExpress.XtraLayout.LayoutControlItem layoutProjManager;
        private DevExpress.XtraEditors.TextEdit txtContractPrice;
        private DevExpress.XtraLayout.LayoutControlItem layoutContractPrice;
        private DevExpress.XtraEditors.MemoEdit txtDescription;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.ComboBoxEdit cmbProjectType;
        private DevExpress.XtraLayout.LayoutControlItem layoutProjectType;
    }
}