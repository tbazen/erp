﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.IO;
using System.Drawing.Imaging;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraGrid;
using DevExpress.Utils;
using INTAPS.UI.ButtonGrid;
using INTAPS.Payroll.Client;
using INTAPS.Payroll;

namespace BIZNET.iERP.Client
{
    public partial class RegisterProject : XtraForm
    {
        private string m_ProjectCode;
        private Project m_Project;
        private int m_CostCenterID;
        public delegate void ProjectAddedHandler(string code);
        public event ProjectAddedHandler ProjectAdded;
        private Project[] m_Projects;
        public RegisterProject()
        {
            InitializeComponent();
            txtName.LostFocus += Control_LostFocus;
            txtContractPrice.LostFocus += Control_LostFocus;
            btnPickManager.LostFocus += Control_LostFocus;
            m_Projects = iERPTransactionClient.GetAllProjects();
        }
        public RegisterProject(string code)
            : this()
        {
            m_ProjectCode = code;
            Project project = iERPTransactionClient.GetProjectInfo(m_ProjectCode);
            if (project != null)
            {
                m_Project = project;
                m_CostCenterID = m_Project.costCenterID;
                LoadProjectForUpdate(m_Project);
            }
        }

        private void LoadProjectForUpdate(Project project)
        {
            txtName.Text = project.Name;
            dtStartDate.DateTime = project.startDate;
            cmbProjectType.SelectedIndex = ((int)project.projectType) - 1;
            if (cmbProjectType.SelectedIndex == 0)
                btnPickManager.Text = PayrollClient.GetEmployee(project.employeeID).employeeName;
            btnPickManager.Tag = project.employeeID;
            txtContractPrice.Text = project.contractPrice.ToString("N");
            txtDescription.Text = project.description;
        }


        void Control_LostFocus(object sender, EventArgs e)
        {
            validationProject.Validate((Control)sender);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validationProject.Validate())
                {
                    MessageBox.ShowErrorMessage("Please check the errors marked in red and try again!");
                    return;
                }
                CreateProject();
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error occurred while trying to create project\n" + ex.Message);
            }

            
        }

        private void CreateProject()
        {
            Project project = SetProject();
            string code = iERPTransactionClient.RegisterProject(project, true);
            string successMessage = "Project successfully saved!";
            string message = m_ProjectCode == null ? successMessage : "Project successfully updated!";
            MessageBox.ShowSuccessMessage(message);
            if (project.code == null)
            {
                if (ProjectAdded != null)
                    ProjectAdded(code);
                ResetControls();
            }
            else
                Close();
            this.DialogResult = DialogResult.OK;
        }

        private Project SetProject()
        {
            Project project = new Project();
            if (m_Project != null)
                project = m_Project;
            project.code = m_ProjectCode;
            project.Name = txtName.Text;
            project.startDate = dtStartDate.DateTime;
            project.employeeID = cmbProjectType.SelectedIndex == 0 ? (int)btnPickManager.Tag : -1;
            project.contractPrice = !layoutContractPrice.IsHidden ? double.Parse(txtContractPrice.Text) : 0;
            project.description = txtDescription.Text;
            project.costCenterID = m_CostCenterID;
            project.projectType = (ProjectType)cmbProjectType.SelectedIndex + 1;
            return project;
        }

        
        private void ResetControls()
        {
            txtName.Text = "";
            txtContractPrice.Text = "";
            txtDescription.Text = "";
            btnPickManager.Text = "";
        }

        private void buttonEdit1_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            //show employee picker
            EmployeePicker picker = new EmployeePicker(PickerMode.Single);
            picker.StartPosition = FormStartPosition.CenterScreen;
            if (picker.ShowDialog() == DialogResult.OK)
            {
                if (picker.GetSelectedEmployees.Length > 0)
                {
                    Employee emp = picker.GetSelectedEmployees[0];
                    //if (IsEmployeeInvolvedInProjectDivisions(emp.costCenterID))
                    //{
                    //    MessageBox.ShowErrorMessage("The selected employee is involved in another project division. Please pick different employee and try again!");
                    //    btnPickManager.Text = "";
                    //    return;
                    //}
                    btnPickManager.Text = emp.employeeName;
                    btnPickManager.Tag = emp.id;
                }
            }

        }

        private bool IsEmployeeInvolvedInProjectDivisions(int costCenterID)
        {
            foreach (Project project in m_Projects)
            {
                foreach (int divisionCostCenterID in project.projectData.projectDivisions)
                {
                    if (m_Project == null || (m_Project != null && PayrollClient.GetEmployee((int)btnPickManager.Tag).costCenterID != costCenterID))
                    {
                        if (costCenterID == divisionCostCenterID)
                            return true;
                    }
                }
            }
            return false;
        }

        private void RegisterProject_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult != DialogResult.OK)
                this.DialogResult = DialogResult.Cancel;
        }

        private void cmbProjectType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbProjectType.SelectedIndex == 1)
            {
                layoutProjManager.HideToCustomization();
                layoutContractPrice.HideToCustomization();
            }
            else
            {
                if (layoutProjManager.IsHidden)
                    layoutProjManager.RestoreFromCustomization(layoutProjectType, InsertType.Bottom);
                if (layoutContractPrice.IsHidden)
                    layoutContractPrice.RestoreFromCustomization(layoutProjManager, InsertType.Bottom);
            }
        }

    }
}