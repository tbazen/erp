﻿namespace BIZNET.iERP.Client
{
    partial class StoreList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StoreList));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnRemove = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlStores = new DevExpress.XtraGrid.GridControl();
            this.gridViewStores = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnAddStores = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStores)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnAddStores);
            this.panelControl1.Controls.Add(this.btnRemove);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(685, 36);
            this.panelControl1.TabIndex = 1;
            // 
            // btnRemove
            // 
            this.btnRemove.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.Appearance.Options.UseFont = true;
            this.btnRemove.Image = ((System.Drawing.Image)(resources.GetObject("btnRemove.Image")));
            this.btnRemove.Location = new System.Drawing.Point(125, 2);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(89, 31);
            this.btnRemove.TabIndex = 0;
            this.btnRemove.Text = "&Remove";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // gridControlStores
            // 
            this.gridControlStores.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlStores.Location = new System.Drawing.Point(0, 36);
            this.gridControlStores.MainView = this.gridViewStores;
            this.gridControlStores.Name = "gridControlStores";
            this.gridControlStores.Size = new System.Drawing.Size(685, 472);
            this.gridControlStores.TabIndex = 2;
            this.gridControlStores.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewStores});
            // 
            // gridViewStores
            // 
            this.gridViewStores.GridControl = this.gridControlStores;
            this.gridViewStores.Name = "gridViewStores";
            this.gridViewStores.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewStores.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewStores.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewStores.OptionsBehavior.Editable = false;
            this.gridViewStores.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewStores.OptionsCustomization.AllowFilter = false;
            this.gridViewStores.OptionsCustomization.AllowGroup = false;
            this.gridViewStores.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewStores.OptionsCustomization.AllowSort = false;
            this.gridViewStores.OptionsView.ShowGroupPanel = false;
            this.gridViewStores.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewStores_FocusedRowChanged);
            // 
            // btnAddStores
            // 
            this.btnAddStores.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddStores.Appearance.Options.UseFont = true;
            this.btnAddStores.Image = ((System.Drawing.Image)(resources.GetObject("btnAddStores.Image")));
            this.btnAddStores.Location = new System.Drawing.Point(3, 2);
            this.btnAddStores.Name = "btnAddStores";
            this.btnAddStores.Size = new System.Drawing.Size(116, 31);
            this.btnAddStores.TabIndex = 0;
            this.btnAddStores.Text = "Add S&tore";
            this.btnAddStores.Click += new System.EventHandler(this.btnAddStores_Click);
            // 
            // StoreList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlStores);
            this.Controls.Add(this.panelControl1);
            this.Name = "StoreList";
            this.Size = new System.Drawing.Size(685, 508);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStores)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnRemove;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControlStores;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewStores;
        private DevExpress.XtraEditors.SimpleButton btnAddStores;
    }
}
