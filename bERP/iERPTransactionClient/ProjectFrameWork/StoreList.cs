﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class StoreList : DevExpress.XtraEditors.XtraUserControl
    {
        private DataTable storeTable;
        DevExpress.XtraSplashScreen.SplashScreenManager waitScreen;
        private string m_ProjectCode;
        public StoreList(string projectCode)
        {
            InitializeComponent();
            waitScreen = new DevExpress.XtraSplashScreen.SplashScreenManager(ProjectManager.MainProjectManager, typeof(global::BIZNET.iERP.Client.WaitForm1), true, true);
            m_ProjectCode = projectCode;
            storeTable = new DataTable();
            PrepareDataTable();
            btnRemove.Enabled = false;
          //  LoadProjectStores(m_ProjectCode);

        }

        private void PrepareDataTable()
        {
            storeTable.Columns.Add("Code", typeof(string));
            storeTable.Columns.Add("Name", typeof(string));
            storeTable.Columns.Add("CostCenterID", typeof(int));
            gridControlStores.DataSource = storeTable;
            gridViewStores.Columns["CostCenterID"].Visible = false;
        }
        public void LoadProjectStores(string projectCode)
        {
            try
            {
                storeTable.Clear();
                Project project = iERPTransactionClient.GetProjectInfo(projectCode);
                waitScreen.ShowWaitForm();
                foreach (string code in project.projectData.stores)
                {
                    DataRow row = storeTable.NewRow();
                    StoreInfo store = iERPTransactionClient.GetStoreInfo(code);
                    if (store != null)
                    {
                        waitScreen.SetWaitFormDescription("Loading: " + store.description);
                        row[0] = code;
                        row[1] = store.description;
                        row[2] = store.costCenterID;
                        storeTable.Rows.Add(row);
                        Application.DoEvents();
                    }
                }
                gridControlStores.DataSource = storeTable;
                gridControlStores.RefreshDataSource();
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to retrieve projects\n" + ex.Message);
            }
            finally
            {
                waitScreen.CloseWaitForm();
            }
        }

        private void gridViewStores_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewStores.SelectedRowsCount > 0)
                btnRemove.Enabled = true;
            else
                btnRemove.Enabled = false;
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.ShowWarningMessage("Are you sure you want to remove the selected store from the project?") == DialogResult.Yes)
                {
                    int costCenterID = (int)gridViewStores.GetRowCellValue(gridViewStores.FocusedRowHandle, "CostCenterID");
                    string code = (string)gridViewStores.GetRowCellValue(gridViewStores.FocusedRowHandle, "Code");
                    iERPTransactionClient.RemoveStoreFromProject(costCenterID,code,m_ProjectCode);
                    gridViewStores.DeleteSelectedRows();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void btnAddStores_Click(object sender, EventArgs e)
        {
            try
            {
                int costCenterID = iERPTransactionClient.GetProjectInfo(m_ProjectCode).costCenterID;
                Project project = iERPTransactionClient.GetProjectInfo(m_ProjectCode);
                if (iERPTransactionClient.IsBankAccountCreatedUnderProject(project.projectData.bankAccounts.ToArray(), project.costCenterID))
                {
                    MessageBox.ShowErrorMessage("You cannot create store since the project/branch has bank account created under it");
                    return;
                }
                if (iERPTransactionClient.IsCashAccountCreatedUnderProject(project.projectData.cashAccounts.ToArray(), project.costCenterID))
                {
                    MessageBox.ShowErrorMessage("You cannot create store since the project/branch has cash account created under it");
                    return;
                }
                if (iERPTransactionClient.IsEmployeeCreatedUnderProject(project.projectData.employees.ToArray(), project.costCenterID))
                {
                    MessageBox.ShowErrorMessage("You cannot create store since the project/branch has employees created under it");
                    return;
                }
                RegisterStore regStore = new RegisterStore(costCenterID);
                if (regStore.ShowDialog() == DialogResult.OK)
                {
                    LoadProjectStores(m_ProjectCode);
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }
    }
}
