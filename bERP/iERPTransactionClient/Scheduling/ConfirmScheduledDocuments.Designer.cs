﻿namespace BIZNET.iERP.Client
{
    partial class ConfirmScheduledDocuments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonReschedule = new System.Windows.Forms.Button();
            this.buttonConfirm = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonReload = new System.Windows.Forms.Button();
            this.listScheduledDocuments = new System.Windows.Forms.ListView();
            this.colDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colReference = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colOverdueDays = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colNote = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dualCalendar = new BIZNET.iERP.Client.BNDualCalendar();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dualCalendar);
            this.panel1.Controls.Add(this.buttonReschedule);
            this.panel1.Controls.Add(this.buttonConfirm);
            this.panel1.Controls.Add(this.buttonDelete);
            this.panel1.Controls.Add(this.buttonReload);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(723, 87);
            this.panel1.TabIndex = 0;
            // 
            // buttonReschedule
            // 
            this.buttonReschedule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReschedule.Location = new System.Drawing.Point(539, 52);
            this.buttonReschedule.Name = "buttonReschedule";
            this.buttonReschedule.Size = new System.Drawing.Size(83, 26);
            this.buttonReschedule.TabIndex = 0;
            this.buttonReschedule.Text = "Reschedule";
            this.buttonReschedule.UseVisualStyleBackColor = true;
            this.buttonReschedule.Click += new System.EventHandler(this.buttonReschedule_Click);
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonConfirm.Location = new System.Drawing.Point(628, 52);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(83, 26);
            this.buttonConfirm.TabIndex = 0;
            this.buttonConfirm.Text = "Confirm";
            this.buttonConfirm.UseVisualStyleBackColor = true;
            this.buttonConfirm.Click += new System.EventHandler(this.buttonConfirm_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDelete.Location = new System.Drawing.Point(450, 52);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(83, 26);
            this.buttonDelete.TabIndex = 0;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonReload
            // 
            this.buttonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReload.Location = new System.Drawing.Point(628, 3);
            this.buttonReload.Name = "buttonReload";
            this.buttonReload.Size = new System.Drawing.Size(83, 26);
            this.buttonReload.TabIndex = 0;
            this.buttonReload.Text = "Reload";
            this.buttonReload.UseVisualStyleBackColor = true;
            this.buttonReload.Click += new System.EventHandler(this.buttonReload_Click);
            // 
            // listScheduledDocuments
            // 
            this.listScheduledDocuments.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colDate,
            this.colReference,
            this.colType,
            this.colOverdueDays,
            this.colNote});
            this.listScheduledDocuments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listScheduledDocuments.FullRowSelect = true;
            this.listScheduledDocuments.Location = new System.Drawing.Point(0, 87);
            this.listScheduledDocuments.Name = "listScheduledDocuments";
            this.listScheduledDocuments.Size = new System.Drawing.Size(723, 328);
            this.listScheduledDocuments.TabIndex = 1;
            this.listScheduledDocuments.UseCompatibleStateImageBehavior = false;
            this.listScheduledDocuments.View = System.Windows.Forms.View.Details;
            this.listScheduledDocuments.SelectedIndexChanged += new System.EventHandler(this.listScheduledDocuments_SelectedIndexChanged);
            // 
            // colDate
            // 
            this.colDate.Text = "Schedule Date";
            this.colDate.Width = 117;
            // 
            // colReference
            // 
            this.colReference.Text = "Reference";
            this.colReference.Width = 130;
            // 
            // colType
            // 
            this.colType.Text = "Type";
            this.colType.Width = 95;
            // 
            // colOverdueDays
            // 
            this.colOverdueDays.Text = "Overdue Duration";
            this.colOverdueDays.Width = 160;
            // 
            // colNote
            // 
            this.colNote.Text = "Note";
            this.colNote.Width = 302;
            // 
            // dualCalendar
            // 
            this.dualCalendar.Location = new System.Drawing.Point(3, 3);
            this.dualCalendar.Name = "dualCalendar";
            this.dualCalendar.ShowEthiopian = true;
            this.dualCalendar.ShowGregorian = true;
            this.dualCalendar.ShowTime = true;
            this.dualCalendar.Size = new System.Drawing.Size(596, 21);
            this.dualCalendar.TabIndex = 2;
            this.dualCalendar.VerticalLayout = false;
            // 
            // ConfirmScheduledDocuments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 415);
            this.Controls.Add(this.listScheduledDocuments);
            this.Controls.Add(this.panel1);
            this.Name = "ConfirmScheduledDocuments";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Scheduled Document Alert";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonReschedule;
        private System.Windows.Forms.ListView listScheduledDocuments;
        private System.Windows.Forms.ColumnHeader colDate;
        private System.Windows.Forms.ColumnHeader colReference;
        private System.Windows.Forms.ColumnHeader colType;
        private System.Windows.Forms.ColumnHeader colOverdueDays;
        private System.Windows.Forms.ColumnHeader colNote;
        private BIZNET.iERP.Client.BNDualCalendar dualCalendar;
        private System.Windows.Forms.Button buttonReload;
        private System.Windows.Forms.Button buttonConfirm;
    }
}