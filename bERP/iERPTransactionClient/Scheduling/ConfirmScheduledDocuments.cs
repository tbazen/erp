﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    public partial class ConfirmScheduledDocuments : Form
    {
        public ConfirmScheduledDocuments()
        {
            InitializeComponent();
            loadData();
            dualCalendar.DateTime = DateTime.Now;
            listScheduledDocuments_SelectedIndexChanged(null, null);
            AccountingClient.DocumentPosted += new DocumentPostedHandler(AccountingClient_DocumentPosted);
        }
        protected override void OnClosed(EventArgs e)
        {
            AccountingClient.DocumentPosted -= new DocumentPostedHandler(AccountingClient_DocumentPosted);
            base.OnClosed(e);
        }
        void AccountingClient_DocumentPosted(AccountDocument doc)
        {
            foreach (ListViewItem li in listScheduledDocuments.Items)
            {
                AccountDocument itemDoc= li.Tag as AccountDocument;
                if (itemDoc == null)
                    continue;
                if (itemDoc.AccountDocumentID == doc.AccountDocumentID)
                {
                    loadData();
                    return;
                }
            }
        }
        void loadData()
        {
            int[] docs = AccountingClient.getMaterializePendingDocumentIDs(dualCalendar.DateTime);

            listScheduledDocuments.Items.Clear();
            foreach (int docID in docs)
            {
                ListViewItem li = new ListViewItem();
                try
                {
                    AccountDocument doc = AccountingClient.GetAccountDocument(docID, false);
                    li.Text = doc.DocumentDate.ToString("dd/MM/yyyy hh:ss");
                    li.SubItems.Add(doc.PaperRef);
                    li.SubItems.Add(AccountingClient.GetDocumentTypeByID(doc.DocumentTypeID).name);
                    TimeSpan ts=  dualCalendar.DateTime.Subtract(doc.DocumentDate);
                    if (ts.TotalDays > 1)
                        li.SubItems.Add(string.Format("{0} days {1} hours", Math.Floor(ts.TotalDays), Math.Round((ts.TotalDays - Math.Floor(ts.TotalDays)) * 24)));
                    else if (ts.TotalHours > 1)
                        li.SubItems.Add(string.Format("{0} hours {1} minutes", Math.Floor(ts.TotalHours), Math.Round((ts.TotalHours - Math.Floor(ts.TotalHours)) * 60)));
                    else if (ts.TotalMinutes > 1)
                        li.SubItems.Add(string.Format("{0} minutes {1} seconds", Math.Floor(ts.TotalMinutes), Math.Round((ts.TotalMinutes - Math.Floor(ts.TotalMinutes)) * 60)));
                    else
                        li.SubItems.Add("Just now");
                    li.Tag = doc;
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error loading document ID:" + docID,ex);
                    li.Text = "error";
                    li.Checked = false;
                    li.Tag = null;

                }
                listScheduledDocuments.Items.Add(li);
            }
        }

        private void buttonReload_Click(object sender, EventArgs e)
        {
            loadData();
        }

        private void listScheduledDocuments_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonDelete.Enabled=buttonConfirm.Enabled = buttonReschedule.Enabled = listScheduledDocuments.SelectedItems.Count == 1
                && listScheduledDocuments.SelectedItems[0].Tag is AccountDocument;
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete this document?"))
                return;
            try
            {
                AccountDocument doc = (AccountDocument)listScheduledDocuments.SelectedItems[0].Tag;
                AccountingClient.DeleteAccountDocument(doc.AccountDocumentID);
                listScheduledDocuments.SelectedItems[0].Remove();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to delete document", ex);
            }
        }

        private void buttonReschedule_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime date;
                if (GetDateBox.Show("Core Accounting","Enter new schedule date",out date)!= DialogResult.OK)
                    return;
                AccountDocument doc = (AccountDocument)listScheduledDocuments.SelectedItems[0].Tag;
                AccountingClient.RescheduleAccountDocument(doc.AccountDocumentID,date);
                listScheduledDocuments.SelectedItems[0].Remove();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to reschedule document", ex);
            }
        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                AccountDocument doc = AccountingClient.GetAccountDocument(( (AccountDocument)listScheduledDocuments.SelectedItems[0].Tag).AccountDocumentID,true);
                IGenericDocumentClientHandler handler=AccountingClient.GetDocumentHandler(doc.DocumentTypeID);
                Form f= handler.CreateEditor(false) as Form;
                handler.SetEditorDocument(f, doc);
                f.Show(this);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to load document editor", ex);
            }
        }
    }
}
