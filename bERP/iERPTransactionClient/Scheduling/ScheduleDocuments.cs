﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public partial class DocumentScheduler : Form,IAccountingClient
    {
        int[] _typeIDS;
        int _maxDocs;
        bool _directPost;
        bool _amountConstraint;
        double _minAmount;
        double _maxAmount;
        public List<AccountDocument> scheduledDocs=null;
        ActivationParameter _pars;
        public DocumentScheduler(AccountDocument[] schedules, int[] docTypeIDS, int maxNumberofDocus, bool directPost, bool amountConstraint, double minAmount, double maxAmout,ActivationParameter pars)
        {
            InitializeComponent();
            _amountConstraint = amountConstraint;
            _minAmount = minAmount;
            _maxAmount = maxAmout;
            _pars = pars;
            if (!_amountConstraint)
            {
                listDocuments.Columns.Remove(colAmount);
                labelTotal.Hide();
            }
            else
            {
                labelTotal.Tag = 0d;
                labelTotal.Text = "0.00";
            }

            _directPost = directPost;
            _typeIDS = docTypeIDS;
            _maxDocs = maxNumberofDocus;
            if (_typeIDS.Length > 1)
            {
                foreach (int typeID in _typeIDS)
                {
                    ToolStripMenuItem mi = new ToolStripMenuItem(AccountingClient.GetDocumentTypeByID(typeID).name);
                    contextMenuStrip.Items.Add(mi);
                    mi.Tag = typeID;
                    mi.Click += new EventHandler(mi_Click);
                }
            }
            else
                listDocuments.Columns.Remove(colType);

            buttonPost.Enabled = false;
            listDocuments_SelectedIndexChanged(null, null);
            
            if (schedules != null)
                foreach (AccountDocument s in schedules)
                {
                    AddDocument(s);
                }
        }

        void mi_Click(object sender, EventArgs e)
        {
            ShowDocumentEditor((int)((ToolStripMenuItem)sender).Tag);
        }

        public int PostGenericDocument(AccountDocument doc)
        {
            AddDocument(doc);
            return -1;
        }
        ListViewItem updateItem= null;
        private void AddDocument(AccountDocument doc)
        {
            if (doc == null)
                return;
            ListViewItem li=null;
            if (updateItem != null)
            {
                li = updateItem;
                li.SubItems.Clear();
            }

            bool add=false;
            if (li == null)
            {
                li = new ListViewItem();
                add = true;
            }
            
            li.Text=doc.DocumentDate.ToString("dd/MM/yyyy hh:mm");
            li.Tag = doc;
            li.SubItems.Add(doc.PaperRef);
            if (_amountConstraint)
            {
                if (!(doc is IStandardAttributeDocument))
                    throw new Exception("The document doesn't support amount");
                double amount = ((IStandardAttributeDocument)doc).documentAmount;
                li.SubItems.Add(amount.ToString("#,#0.00"));
                if (AccountBase.AmountGreater((double)labelTotal.Tag + amount, _maxAmount))
                    throw new Exception("Total amount of scheduled document can be greater than " + _maxAmount.ToString("#,#0.00"));
                labelTotal.Tag = (double)labelTotal.Tag + amount;
                buttonPost.Enabled = AccountBase.AmountLess((double)labelTotal.Tag + amount, _minAmount);
            }
            else
                buttonPost.Enabled = true;
            if (_maxDocs > 0)
                buttonPost.Enabled = buttonPost.Enabled && (listDocuments.Items.Count + 1 < _maxDocs);
            if (_typeIDS.Length > 1)
                li.SubItems.Add(AccountingClient.GetDocumentTypeByType(doc.GetType()).name);
            li.SubItems.Add(doc.ShortDescription);
            li.SubItems.Add("");
            if(add)
                listDocuments.Items.Add(li);
            buttonPost.Enabled = true;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (_typeIDS.Length == 1)
            {
                ShowDocumentEditor(_typeIDS[0]);
            }
            else
                contextMenuStrip.Show(Cursor.Position);
        }

        private void ShowDocumentEditor(int docTypeID)
        {
            bERPClientDocumentHandler handler = iERPTransactionClient.GetDocumentHandler(docTypeID);
            try
            {
                handler.setAccountingClient(this);
                Form f = handler.CreateEditor(_pars) as Form;
                updateItem = null;
                f.Show(this);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error loading document editor", ex);
            }
        }

        private void listDocuments_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonEdit.Enabled = buttonRemove.Enabled = listDocuments.SelectedItems.Count == 1;

        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            listDocuments.SelectedItems[0].Remove();
            //buttonPost.Enabled = listDocuments.Items.Count > 0;
            buttonEdit.Enabled = buttonRemove.Enabled = listDocuments.SelectedItems.Count == 1;
        }

        private void buttonPost_Click(object sender, EventArgs e)
        {
            if (_directPost)
            {
                foreach (ListViewItem li in listDocuments.Items)
                {
                    AccountDocument doc = li.Tag as AccountDocument;
                    if (doc == null)
                        continue;
                    try
                    {
                        AccountingClient.PostGenericDocument(doc);
                    }
                    catch (Exception ex)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error posting document:" + doc.PaperRef, ex);
                        li.SubItems[colStatus.Index].Text = "error";
                    }
                }
            }
            else
            {
                scheduledDocs = new List<AccountDocument>();
                foreach (ListViewItem li in listDocuments.Items)
                {
                    AccountDocument doc = li.Tag as AccountDocument;
                    if (doc == null)
                        continue;
                    doc.scheduled = true;
                    doc.materialized = false;
                    scheduledDocs.Add(doc);
                }
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
        public List<DocType> GetListedSchedules<DocType>()
            where DocType:AccountDocument
        {
            List<DocType>  scheduledDocs = new List<DocType>();
            foreach (ListViewItem li in listDocuments.Items)
            {
                DocType doc = li.Tag as DocType;
                if (doc == null)
                    continue;
                doc.scheduled = true;
                doc.materialized = false;
                scheduledDocs.Add(doc);
            }
            return scheduledDocs;
        }
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            AccountDocument doc = (AccountDocument)listDocuments.SelectedItems[0].Tag;
            try
            {
                bERPClientDocumentHandler handler= iERPTransactionClient.GetDocumentHandler(AccountingClient.GetDocumentTypeByType(doc.GetType()).id);
                handler.setAccountingClient(this);
                Form f= handler.CreateEditor(_pars) as Form;
                handler.SetEditorDocument(f, doc);
                updateItem = listDocuments.SelectedItems[0];
                f.Show(this);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to load document editor", ex);
            }
        }
    }
}
