﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraPrinting;

namespace BIZNET.iERP.Client
{
    public partial class ChartOfAccountTemplate : DevExpress.XtraEditors.XtraForm
    {
        List<int> LoadedNodes;
        DevExpress.XtraSplashScreen.SplashScreenManager waitScreen;
        private int MAXIMUM_CHILD;
        public ChartOfAccountTemplate()
        {
            InitializeComponent();
            MAXIMUM_CHILD = iERP.Client.Properties.Settings.Default.COATemplate_Max_Child;
            btnChildAccounts.EditValue = MAXIMUM_CHILD;
            LoadedNodes = new List<int>();
            waitScreen = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::BIZNET.iERP.Client.WaitForm1), true, true);
            RepositoryItemButtonEdit editButton = (RepositoryItemButtonEdit)btnChildAccounts.Edit;
            editButton.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(editButton_ButtonClick);
           LoadChartOfAccount();
        }

        void editButton_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            ButtonEdit editButton = (ButtonEdit)sender;
            MAXIMUM_CHILD = int.Parse(editButton.Text);
            treeChartOfAccount.Focus();
            LoadedNodes.Clear();
            waitScreen.ShowWaitForm();
            waitScreen.SetWaitFormDescription("Loading Chart Of Account");
            LoadChartOfAccount();
            treeChartOfAccount.ExpandAll();
            waitScreen.CloseWaitForm();
        }

        private void LoadChartOfAccount()
        {
            treeChartOfAccount.Nodes.Clear();
            LoadChilds(treeChartOfAccount.Nodes, -1);
            LoadChildOfChilds(treeChartOfAccount.Nodes);
        }


        void LoadChilds(TreeListNodes nodes, int PID)
        {
            if (LoadedNodes.Contains(PID))
                return;
            LoadedNodes.Add(PID);
            Account[] acs;
            acs = GetChilds(PID);
            //check number of child accounts limit
           // int noOfChild = int.Parse(btnChildAccounts.EditValue.ToString());
            if (acs.Length > MAXIMUM_CHILD)
                return;
            foreach (Account a in acs)
            {
                AddAccountNode(nodes, a);
            }
            //if (acs.Length < N)
            //{
            //    TreeNode n = new TreeNode(N - acs.Length + " accounts not shown!");
            //    nodes.Add(n);
            //}
        }
        void LoadChildOfChilds(TreeListNodes nodes)
        {
            foreach (TreeListNode n in nodes)
            {
                if (n.Tag is Account)
                    LoadChilds(n.Nodes, ((Account)n.Tag).ObjectIDVal);
            }
        }
        Account[] GetChilds(int PID)
        {
            return INTAPS.Accounting.Client.AccountingClient.GetChildAllAcccount<Account>(PID);
        }
        void AddAccountNode(TreeListNodes parent, Account ac)
        {
            TreeListNode n = treeChartOfAccount.AppendNode(new object[]{ac.Code + " - " + ac.Name}, parent.ParentNode, ac);
           // n.ImageIndex = -1;
           // parent.Add(n);
           // SetNode(n, ac);
          //  _allNodes.Add(ac.id, n);
            //if (ac.childCount > 0)
            //{
            //    TreeListNode ph = new TreeListNode();
            //    ph.Tag = ac.id;
            //    n.Nodes.Add(ph);
            //}
        }

        private void treeChartOfAccount_BeforeExpand(object sender, DevExpress.XtraTreeList.BeforeExpandEventArgs e)
        {
            LoadChildOfChilds(e.Node.Nodes);
        }

        private void btnPreview_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
          //  treeChartOfAccount.ShowPrintPreview();
            PrintableComponentLink componentLink = new PrintableComponentLink(new PrintingSystem());
            componentLink.CreateMarginalHeaderArea += new CreateAreaEventHandler(componentLink_CreateMarginalHeaderArea);
            componentLink.Component = treeChartOfAccount;
            componentLink.CreateDocument();
            PrintTool pt = new PrintTool(componentLink.PrintingSystemBase);
            pt.PrintingSystem.ShowMarginsWarning = false;
            pt.PrintingSystem.PageSettings.BottomMargin = 24;
            pt.PrintingSystem.PageSettings.TopMargin = 45;
            pt.PrintingSystem.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            
            pt.ShowPreviewDialog();
        }

        void componentLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, "Page {0} of {1}", Color.Black,
        new RectangleF(0, 0, 200, 50), BorderSide.None);
        }

        private void ChartOfAccountTemplate_FormClosing(object sender, FormClosingEventArgs e)
        {
            iERP.Client.Properties.Settings.Default.COATemplate_Max_Child = MAXIMUM_CHILD;
            iERP.Client.Properties.Settings.Default.Save();
            e.Cancel = false;
        }
    }
}