﻿namespace BIZNET.iERP.Client
{
    partial class ReportItemEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridAccountItems = new System.Windows.Forms.DataGridView();
            this.colCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFactor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDelete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panelHeader = new DevExpress.XtraEditors.PanelControl();
            this.buttonAddFormula = new System.Windows.Forms.Button();
            this.checkShowValue = new DevExpress.XtraEditors.CheckEdit();
            this.checkRemoveZero = new DevExpress.XtraEditors.CheckEdit();
            this.comboCss = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboCredit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textTag = new DevExpress.XtraEditors.TextEdit();
            this.textLabel = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelFooter = new DevExpress.XtraEditors.PanelControl();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.formulaEditor = new INTAPS.Evaluator.FormulaEditor();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAccountItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelHeader)).BeginInit();
            this.panelHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkShowValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkRemoveZero.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboCss.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboCredit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textTag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textLabel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelFooter)).BeginInit();
            this.panelFooter.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gridAccountItems);
            this.splitContainerControl1.Panel2.Controls.Add(this.formulaEditor);
            this.splitContainerControl1.Panel2.Controls.Add(this.panelHeader);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(975, 508);
            this.splitContainerControl1.SplitterPosition = 339;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridAccountItems
            // 
            this.gridAccountItems.AllowUserToAddRows = false;
            this.gridAccountItems.AllowUserToDeleteRows = false;
            this.gridAccountItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridAccountItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCode,
            this.colName,
            this.colFactor,
            this.colDelete});
            this.gridAccountItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridAccountItems.Location = new System.Drawing.Point(0, 105);
            this.gridAccountItems.Name = "gridAccountItems";
            this.gridAccountItems.Size = new System.Drawing.Size(631, 251);
            this.gridAccountItems.TabIndex = 1;
            this.gridAccountItems.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridAccountItems_CellClick);
            this.gridAccountItems.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridAccountItems_CellContentClick);
            this.gridAccountItems.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridAccountItems_CellDoubleClick);
            this.gridAccountItems.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.gridAccountItems_DataError);
            this.gridAccountItems.SelectionChanged += new System.EventHandler(this.gridAccountItems_SelectionChanged);
            // 
            // colCode
            // 
            this.colCode.HeaderText = "Code";
            this.colCode.Name = "colCode";
            // 
            // colName
            // 
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            // 
            // colFactor
            // 
            this.colFactor.HeaderText = "Factor";
            this.colFactor.Name = "colFactor";
            // 
            // colDelete
            // 
            this.colDelete.HeaderText = "";
            this.colDelete.Name = "colDelete";
            this.colDelete.Width = 30;
            // 
            // panelHeader
            // 
            this.panelHeader.Controls.Add(this.buttonAddFormula);
            this.panelHeader.Controls.Add(this.checkShowValue);
            this.panelHeader.Controls.Add(this.checkRemoveZero);
            this.panelHeader.Controls.Add(this.comboCss);
            this.panelHeader.Controls.Add(this.comboCredit);
            this.panelHeader.Controls.Add(this.textTag);
            this.panelHeader.Controls.Add(this.textLabel);
            this.panelHeader.Controls.Add(this.labelControl3);
            this.panelHeader.Controls.Add(this.labelControl2);
            this.panelHeader.Controls.Add(this.labelControl1);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(631, 105);
            this.panelHeader.TabIndex = 0;
            // 
            // buttonAddFormula
            // 
            this.buttonAddFormula.Location = new System.Drawing.Point(531, 77);
            this.buttonAddFormula.Name = "buttonAddFormula";
            this.buttonAddFormula.Size = new System.Drawing.Size(95, 23);
            this.buttonAddFormula.TabIndex = 4;
            this.buttonAddFormula.Text = "Add Formula";
            this.buttonAddFormula.UseVisualStyleBackColor = true;
            this.buttonAddFormula.Click += new System.EventHandler(this.buttonAddFormula_Click);
            // 
            // checkShowValue
            // 
            this.checkShowValue.EditValue = true;
            this.checkShowValue.Location = new System.Drawing.Point(323, 45);
            this.checkShowValue.Name = "checkShowValue";
            this.checkShowValue.Properties.Caption = "Show Value";
            this.checkShowValue.Size = new System.Drawing.Size(97, 19);
            this.checkShowValue.TabIndex = 3;
            // 
            // checkRemoveZero
            // 
            this.checkRemoveZero.EditValue = true;
            this.checkRemoveZero.Location = new System.Drawing.Point(216, 45);
            this.checkRemoveZero.Name = "checkRemoveZero";
            this.checkRemoveZero.Properties.Caption = "Exclude if zero";
            this.checkRemoveZero.Size = new System.Drawing.Size(123, 19);
            this.checkRemoveZero.TabIndex = 3;
            // 
            // comboCss
            // 
            this.comboCss.Location = new System.Drawing.Point(54, 44);
            this.comboCss.Name = "comboCss";
            this.comboCss.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboCss.Properties.Items.AddRange(new object[] {
            "Section_Header_1",
            "Odd_Row",
            "Even_Row"});
            this.comboCss.Size = new System.Drawing.Size(150, 20);
            this.comboCss.TabIndex = 2;
            // 
            // comboCredit
            // 
            this.comboCredit.Location = new System.Drawing.Point(481, 44);
            this.comboCredit.Name = "comboCredit";
            this.comboCredit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboCredit.Properties.Items.AddRange(new object[] {
            "Debit",
            "Credit"});
            this.comboCredit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboCredit.Size = new System.Drawing.Size(150, 20);
            this.comboCredit.TabIndex = 2;
            // 
            // textTag
            // 
            this.textTag.Location = new System.Drawing.Point(338, 5);
            this.textTag.Name = "textTag";
            this.textTag.Size = new System.Drawing.Size(202, 20);
            this.textTag.TabIndex = 1;
            this.textTag.EditValueChanged += new System.EventHandler(this.textLabel_EditValueChanged);
            // 
            // textLabel
            // 
            this.textLabel.Location = new System.Drawing.Point(54, 5);
            this.textLabel.Name = "textLabel";
            this.textLabel.Size = new System.Drawing.Size(202, 20);
            this.textLabel.TabIndex = 1;
            this.textLabel.EditValueChanged += new System.EventHandler(this.textLabel_EditValueChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(289, 8);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(18, 13);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "Tag";
            this.labelControl3.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(5, 47);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(17, 13);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Css";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(5, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(25, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Label";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // panelFooter
            // 
            this.panelFooter.Controls.Add(this.buttonOk);
            this.panelFooter.Controls.Add(this.buttonCancel);
            this.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFooter.Location = new System.Drawing.Point(0, 508);
            this.panelFooter.Name = "panelFooter";
            this.panelFooter.Size = new System.Drawing.Size(975, 36);
            this.panelFooter.TabIndex = 1;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(796, 6);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 0;
            this.buttonOk.Text = "Ok";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(888, 6);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 0;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // formulaEditor
            // 
            this.formulaEditor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.formulaEditor.Font = new System.Drawing.Font("Batang", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formulaEditor.Formula = "";
            this.formulaEditor.Location = new System.Drawing.Point(0, 356);
            this.formulaEditor.MultiLine = true;
            this.formulaEditor.Name = "formulaEditor";
            this.formulaEditor.Size = new System.Drawing.Size(631, 152);
            this.formulaEditor.TabIndex = 1;
            this.formulaEditor.Text = "";
            this.formulaEditor.Validated += new System.EventHandler(this.formulaEditor_Validated);
            // 
            // ReportItemEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 544);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.panelFooter);
            this.Name = "ReportItemEditor";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Report Item";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridAccountItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelHeader)).EndInit();
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkShowValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkRemoveZero.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboCss.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboCredit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textTag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textLabel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelFooter)).EndInit();
            this.panelFooter.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.PanelControl panelFooter;
        private DevExpress.XtraEditors.PanelControl panelHeader;
        private System.Windows.Forms.DataGridView gridAccountItems;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFactor;
        private System.Windows.Forms.DataGridViewButtonColumn colDelete;
        private DevExpress.XtraEditors.TextEdit textLabel;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit comboCredit;
        private DevExpress.XtraEditors.ComboBoxEdit comboCss;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.CheckEdit checkRemoveZero;
        private DevExpress.XtraEditors.CheckEdit checkShowValue;
        private DevExpress.XtraEditors.TextEdit textTag;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.Button buttonAddFormula;
        private INTAPS.Evaluator.FormulaEditor formulaEditor;
    }
}