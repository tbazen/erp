﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Evaluator;

namespace BIZNET.iERP.Client
{
    public partial class ReportItemEditor : DevExpress.XtraEditors.XtraForm
    {
        public SummaryItemBase itemInfo = null;
        bool _sectionEdit;
        public event EventHandler AccountViewStructureChanged;
        public event EventHandler SelectedAccountChanged;
        AccountListTree accountTree;
        Symbolic m_fePar = new Symbolic();
        public ReportItemEditor(bool sectionEdit)
        {
            InitializeComponent();
            if (!AccountingClient.IsConnected)
                return;

            accountTree = new AccountListTree();
            accountTree.AccountExplorerTypeChanged += accountTree_AccountExplorerTypeChanged;
            splitContainerControl1.Panel1.Controls.Add(accountTree);
            accountTree.Dock = DockStyle.Fill;

            colFactor.ValueType = typeof(double);
            accountTree.AccountDoubleClicked += new EventHandler(accountTree_DoubleClick);
            accountTree.getTree().ViewStructureChanged += new EventHandler(accountTree_ViewStructureChanged);
            accountTree.SelectedAccountChanged += new EventHandler(accountTree_SelectedAccountChanged);

            _sectionEdit = sectionEdit;
            if (_sectionEdit)
            {
                splitContainerControl1.Collapsed = true;
                accountTree.Hide();
                gridAccountItems.Visible = false;
                this.ClientSize = new Size(panelHeader.Width, panelHeader.Height + panelFooter.Height);
            }
            else
            {
                checkShowValue.Visible = false;
                AccountTree actree = accountTree.getTree();
                actree.ExplorerType = accountTree.explorerType;
                actree.LoadData(-1);
            }
            formulaEditor.m_Parent = m_fePar;
            m_fePar.SetSymbolProvider(new ClientSymbolProvider(new FunctionDocumentation[0]));
        }

        void accountTree_AccountExplorerTypeChanged(object sender, EventArgs e)
        {
            if (accountTree != null)
            {
                AccountTree actree = accountTree.getTree();
                actree.ExplorerType = accountTree.explorerType;
                actree.LoadData(-1);
            }
        }
        public ReportItemEditor(SummaryItemBase itemInfo)
            : this(itemInfo is SummarySection)
        {
            textLabel.Text = itemInfo.label;
            textTag.Text = itemInfo.tag;
            comboCredit.SelectedIndex = itemInfo.credit ? 1 : 0;
            comboCss.Text = itemInfo.format;
            checkRemoveZero.Checked = itemInfo.removeIfZero;
            if (_sectionEdit)
            {
                checkShowValue.Checked = ((SummarySection)itemInfo).showTotal;
            }
            else
            { 
                SummaryItemInfo thisItemInfo=(SummaryItemInfo)itemInfo;
                for (int i = 0; i < thisItemInfo.accounts.Count; i++)
            {
                if (thisItemInfo.accounts[i] == -1)
                {
                    int index = gridAccountItems.Rows.Add(new object[] {thisItemInfo.formulae[i] , "formula", thisItemInfo.factors[i], "X" });
                    gridAccountItems.Rows[index].Tag = null;
                }
                else
                {
                    Account ac = AccountingClient.GetAccount<Account>(thisItemInfo.accounts[i]);
                    int index;
                    if (ac == null)
                        index = gridAccountItems.Rows.Add(new object[] { "error", "error:", thisItemInfo.factors[i], "X" });
                    else
                        index = gridAccountItems.Rows.Add(new object[] { ac.Code, ac.Name, thisItemInfo.factors[i], "X" });
                    if (ac == null)
                        gridAccountItems.Rows[index].Tag = -1;
                    else
                        gridAccountItems.Rows[index].Tag = ac.id;
                }
            }
        }
        }

        private void accountTree_ViewStructureChanged(object sender, EventArgs e)
        {
            if (AccountViewStructureChanged != null)
                AccountViewStructureChanged(this, null);
        }
        private void accountTree_SelectedAccountChanged(object sender, EventArgs e)
        {
            if (SelectedAccountChanged != null)
                SelectedAccountChanged(this, null);
        }
        private void accountTree_DoubleClick(object sender, EventArgs e)
        {
            Account ac = accountTree.SelectedAccount;
            if (ac == null)
                return;
            if (gridAccountItems.Rows.Count == 0)
            {
                comboCredit.SelectedIndex = ac.CreditAccount ? 1 : 0;
                textLabel.Text = ac.Name;
                textTag.Text = ac.Code;
            }
            int index=gridAccountItems.Rows.Add(new object[] { ac.Code, ac.Name,1d, "X" });
            gridAccountItems.Rows[index].Tag = ac.id;
        }

        private void gridAccountItems_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
                gridAccountItems.Rows.RemoveAt(e.RowIndex);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if(textLabel.Text.Trim()=="")
            {
                MessageBox.ShowErrorMessage("Enter label");
                return;
            }
            if (textTag.Text.Trim() == "")
            {
                MessageBox.ShowErrorMessage("Please enter tag");
                return;
            }
            if(!_sectionEdit && gridAccountItems.Rows.Count==0 )
            {
                MessageBox.ShowErrorMessage("Please enter at least one item");
                return;
            }
            if (_sectionEdit)
            {
                itemInfo = new SummarySection();
                itemInfo.label = textLabel.Text.Trim();
                itemInfo.tag = textTag.Text;
                itemInfo.credit = comboCredit.SelectedIndex == 1;
                itemInfo.format = comboCss.Text;
                itemInfo.removeIfZero = checkRemoveZero.Checked;
                ((SummarySection)itemInfo).showTotal = checkShowValue.Checked;
            }
            else
            {
                itemInfo = new SummaryItemInfo();
                itemInfo.label = textLabel.Text.Trim();
                itemInfo.tag = textTag.Text;
                itemInfo.credit = comboCredit.SelectedIndex == 1;
                itemInfo.format = comboCss.Text;
                itemInfo.removeIfZero = checkRemoveZero.Checked;
                
                foreach(DataGridViewRow row in gridAccountItems.Rows)
                {
                    if (row.Tag is int)
                    {
                        ((SummaryItemInfo)itemInfo).formulae.Add(null);
                        ((SummaryItemInfo)itemInfo).accounts.Add((int)row.Tag);
                        ((SummaryItemInfo)itemInfo).factors.Add((double)row.Cells[2].Value);
                    }
                    else
                    {
                        ((SummaryItemInfo)itemInfo).formulae.Add((string)row.Cells[0].Value);
                        ((SummaryItemInfo)itemInfo).accounts.Add(-1);
                        ((SummaryItemInfo)itemInfo).factors.Add((double)row.Cells[2].Value);

                    }

                }
            }
            this.DialogResult = DialogResult.OK;
        }

        private void gridAccountItems_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.ShowErrorMessage("Enter numeric value for factor");
        }

        private void accountTree_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void textLabel_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void buttonAddFormula_Click(object sender, EventArgs e)
        {
            int index = gridAccountItems.Rows.Add(new object[] { "", "Formula", 1d, "X" });
            gridAccountItems.Rows[index].Tag = null;

        }

        private void gridAccountItems_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void gridAccountItems_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void formulaEditor_Validated(object sender, EventArgs e)
        {
            if (gridAccountItems.SelectedRows.Count == 0)
                return;
            if (gridAccountItems.SelectedRows[0].Tag is int)
                return;
            gridAccountItems.SelectedRows[0].Cells[0].Value = formulaEditor.Formula;
        }

        private void gridAccountItems_SelectionChanged(object sender, EventArgs e)
        {
            formulaEditor.ReadOnly = true;
            formulaEditor.Text = "";
            if (gridAccountItems.SelectedRows.Count == 0)
                return;
            if (gridAccountItems.SelectedRows[0].Tag is int)
                return;
            formulaEditor.ReadOnly = false;
            formulaEditor.Formula = gridAccountItems.SelectedRows[0].Cells[0].Value as string;
        }

    }
}