﻿namespace BIZNET.iERP.Client
{
    partial class StatementDesigner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.accountTree = new INTAPS.Accounting.Client.AccountTree();
            this.browser = new INTAPS.UI.HTML.ControlBrowser();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.textDebitLabel = new DevExpress.XtraEditors.TextEdit();
            this.labelDebit = new DevExpress.XtraEditors.LabelControl();
            this.textCreditLabel = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.textPerticularsHeader = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.checkTotalRow = new DevExpress.XtraEditors.CheckEdit();
            this.checkTwoColumns = new DevExpress.XtraEditors.CheckEdit();
            this.checkHeaderRow = new DevExpress.XtraEditors.CheckEdit();
            this.checkFlow = new DevExpress.XtraEditors.CheckEdit();
            this.buttonRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.buttonAddParentAndChilds = new DevExpress.XtraEditors.SimpleButton();
            this.buttonAddAllChilds = new DevExpress.XtraEditors.SimpleButton();
            this.buttonAddOne = new DevExpress.XtraEditors.SimpleButton();
            this.buttonExport = new DevExpress.XtraEditors.SimpleButton();
            this.buttonImport = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textDebitLabel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCreditLabel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textPerticularsHeader.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkTotalRow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkTwoColumns.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkHeaderRow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkFlow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.accountTree);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.browser);
            this.splitContainerControl1.Panel2.Controls.Add(this.panelControl1);
            this.splitContainerControl1.Panel2.Controls.Add(this.panelControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(847, 545);
            this.splitContainerControl1.SplitterPosition = 10;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // accountTree
            // 
            this.accountTree.CostCenterAccountMode = true;
            this.accountTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.accountTree.ExplorerType = INTAPS.Accounting.AccountExplorerType.ALL;
            this.accountTree.FullRowSelect = true;
            this.accountTree.HideSelection = false;
            this.accountTree.ImageIndex = 0;
            this.accountTree.Location = new System.Drawing.Point(0, 0);
            this.accountTree.Name = "accountTree";
            this.accountTree.SelectedImageIndex = 0;
            this.accountTree.Size = new System.Drawing.Size(10, 545);
            this.accountTree.TabIndex = 0;
            this.accountTree.Visible = false;
            // 
            // browser
            // 
            this.browser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browser.Location = new System.Drawing.Point(39, 105);
            this.browser.MinimumSize = new System.Drawing.Size(20, 20);
            this.browser.Name = "browser";
            this.browser.Size = new System.Drawing.Size(793, 440);
            this.browser.StyleSheetFile = "berp.css";
            this.browser.TabIndex = 1;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.textDebitLabel);
            this.panelControl1.Controls.Add(this.labelDebit);
            this.panelControl1.Controls.Add(this.textCreditLabel);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.textPerticularsHeader);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.checkTotalRow);
            this.panelControl1.Controls.Add(this.checkTwoColumns);
            this.panelControl1.Controls.Add(this.checkHeaderRow);
            this.panelControl1.Controls.Add(this.checkFlow);
            this.panelControl1.Controls.Add(this.buttonRefresh);
            this.panelControl1.Controls.Add(this.buttonImport);
            this.panelControl1.Controls.Add(this.buttonExport);
            this.panelControl1.Controls.Add(this.buttonOk);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(39, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(793, 105);
            this.panelControl1.TabIndex = 2;
            // 
            // textDebitLabel
            // 
            this.textDebitLabel.Location = new System.Drawing.Point(70, 43);
            this.textDebitLabel.Name = "textDebitLabel";
            this.textDebitLabel.Size = new System.Drawing.Size(192, 20);
            this.textDebitLabel.TabIndex = 7;
            // 
            // labelDebit
            // 
            this.labelDebit.Location = new System.Drawing.Point(11, 47);
            this.labelDebit.Name = "labelDebit";
            this.labelDebit.Size = new System.Drawing.Size(53, 13);
            this.labelDebit.TabIndex = 6;
            this.labelDebit.Text = "Debit Label";
            // 
            // textCreditLabel
            // 
            this.textCreditLabel.Location = new System.Drawing.Point(70, 74);
            this.textCreditLabel.Name = "textCreditLabel";
            this.textCreditLabel.Size = new System.Drawing.Size(192, 20);
            this.textCreditLabel.TabIndex = 7;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(11, 78);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(57, 13);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Credit Label";
            // 
            // textPerticularsHeader
            // 
            this.textPerticularsHeader.Location = new System.Drawing.Point(70, 12);
            this.textPerticularsHeader.Name = "textPerticularsHeader";
            this.textPerticularsHeader.Size = new System.Drawing.Size(192, 20);
            this.textPerticularsHeader.TabIndex = 7;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(11, 16);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(50, 13);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Item Label";
            // 
            // checkTotalRow
            // 
            this.checkTotalRow.Location = new System.Drawing.Point(361, 76);
            this.checkTotalRow.Name = "checkTotalRow";
            this.checkTotalRow.Properties.Caption = "Grand Total Row";
            this.checkTotalRow.Size = new System.Drawing.Size(123, 19);
            this.checkTotalRow.TabIndex = 5;
            // 
            // checkTwoColumns
            // 
            this.checkTwoColumns.Location = new System.Drawing.Point(402, 10);
            this.checkTwoColumns.Name = "checkTwoColumns";
            this.checkTwoColumns.Properties.Caption = "Two Columns";
            this.checkTwoColumns.Size = new System.Drawing.Size(105, 19);
            this.checkTwoColumns.TabIndex = 4;
            this.checkTwoColumns.CheckedChanged += new System.EventHandler(this.chkHeadeRow_CheckedChanged);
            // 
            // checkHeaderRow
            // 
            this.checkHeaderRow.Location = new System.Drawing.Point(298, 10);
            this.checkHeaderRow.Name = "checkHeaderRow";
            this.checkHeaderRow.Properties.Caption = "Header Row";
            this.checkHeaderRow.Size = new System.Drawing.Size(88, 19);
            this.checkHeaderRow.TabIndex = 4;
            this.checkHeaderRow.CheckedChanged += new System.EventHandler(this.chkHeadeRow_CheckedChanged);
            // 
            // checkFlow
            // 
            this.checkFlow.Location = new System.Drawing.Point(298, 75);
            this.checkFlow.Name = "checkFlow";
            this.checkFlow.Properties.Caption = "Flow";
            this.checkFlow.Size = new System.Drawing.Size(57, 19);
            this.checkFlow.TabIndex = 4;
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRefresh.Location = new System.Drawing.Point(625, 74);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(75, 23);
            this.buttonRefresh.TabIndex = 2;
            this.buttonRefresh.Text = "Two Columns";
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(706, 74);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 2;
            this.buttonOk.Text = "Update";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.buttonAddParentAndChilds);
            this.panelControl2.Controls.Add(this.buttonAddAllChilds);
            this.panelControl2.Controls.Add(this.buttonAddOne);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(39, 545);
            this.panelControl2.TabIndex = 0;
            this.panelControl2.Visible = false;
            // 
            // buttonAddParentAndChilds
            // 
            this.buttonAddParentAndChilds.Location = new System.Drawing.Point(5, 175);
            this.buttonAddParentAndChilds.Name = "buttonAddParentAndChilds";
            this.buttonAddParentAndChilds.Size = new System.Drawing.Size(28, 23);
            this.buttonAddParentAndChilds.TabIndex = 0;
            this.buttonAddParentAndChilds.Text = ">>>";
            // 
            // buttonAddAllChilds
            // 
            this.buttonAddAllChilds.Location = new System.Drawing.Point(5, 146);
            this.buttonAddAllChilds.Name = "buttonAddAllChilds";
            this.buttonAddAllChilds.Size = new System.Drawing.Size(28, 23);
            this.buttonAddAllChilds.TabIndex = 0;
            this.buttonAddAllChilds.Text = ">>";
            // 
            // buttonAddOne
            // 
            this.buttonAddOne.Location = new System.Drawing.Point(5, 117);
            this.buttonAddOne.Name = "buttonAddOne";
            this.buttonAddOne.Size = new System.Drawing.Size(28, 23);
            this.buttonAddOne.TabIndex = 0;
            this.buttonAddOne.Text = ">";
            // 
            // buttonExport
            // 
            this.buttonExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExport.Location = new System.Drawing.Point(706, 47);
            this.buttonExport.Name = "buttonExport";
            this.buttonExport.Size = new System.Drawing.Size(75, 23);
            this.buttonExport.TabIndex = 2;
            this.buttonExport.Text = "Export";
            this.buttonExport.Click += new System.EventHandler(this.buttonExport_Click);
            // 
            // buttonImport
            // 
            this.buttonImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImport.Location = new System.Drawing.Point(625, 47);
            this.buttonImport.Name = "buttonImport";
            this.buttonImport.Size = new System.Drawing.Size(75, 23);
            this.buttonImport.TabIndex = 2;
            this.buttonImport.Text = "Import";
            this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
            // 
            // StatementDesigner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 545);
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "StatementDesigner";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Statement Designer";
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textDebitLabel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textCreditLabel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textPerticularsHeader.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkTotalRow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkTwoColumns.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkHeaderRow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkFlow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private INTAPS.UI.HTML.ControlBrowser browser;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private DevExpress.XtraEditors.SimpleButton buttonRefresh;
        private INTAPS.Accounting.Client.AccountTree accountTree;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton buttonAddParentAndChilds;
        private DevExpress.XtraEditors.SimpleButton buttonAddAllChilds;
        private DevExpress.XtraEditors.SimpleButton buttonAddOne;
        private DevExpress.XtraEditors.CheckEdit checkFlow;
        private DevExpress.XtraEditors.CheckEdit checkTotalRow;
        private DevExpress.XtraEditors.TextEdit textDebitLabel;
        private DevExpress.XtraEditors.LabelControl labelDebit;
        private DevExpress.XtraEditors.TextEdit textCreditLabel;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit textPerticularsHeader;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckEdit checkHeaderRow;
        private DevExpress.XtraEditors.CheckEdit checkTwoColumns;
        private DevExpress.XtraEditors.SimpleButton buttonImport;
        private DevExpress.XtraEditors.SimpleButton buttonExport;
    }
}