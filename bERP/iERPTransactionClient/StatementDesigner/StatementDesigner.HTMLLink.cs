﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;

namespace BIZNET.iERP.Client
{
    public partial class StatementDesigner : DevExpress.XtraEditors.XtraForm
    {
        class HTMLLinkHandler : INTAPS.UI.HTML.IHTMLBuilder
        {
            StatementDesigner _parent;
            public HTMLLinkHandler(StatementDesigner e)
            {
                _parent = e;
            }

            public void Build()
            {

            }

            public string GetOuterHtml()
            {
                return "<span id='report'>"+ _parent._genertedHTML+"</span>";
            }

            public bool ProcessUrl(string path, Hashtable query)
            {
                return _parent.ProcessUrl(path, query);
            }

            public void SetHost(INTAPS.UI.HTML.IHTMLDocumentHost host)
            {

            }

            public string WindowCaption
            {
                get { return "Report"; }
            }
        }
    }
}