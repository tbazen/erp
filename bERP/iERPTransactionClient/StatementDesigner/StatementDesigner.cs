﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;

namespace BIZNET.iERP.Client
{
    public partial class StatementDesigner : DevExpress.XtraEditors.XtraForm
    {
        const string LINK_DELETE_ITEM = "delItem";
        const string LINK_ADD_SECTION = "addSection";
        const string LINK_ADD_ITEM = "addItem";
        const string LINK_EDIT_ITEM = "editItem";
        const string LINK_ITEM_UP= "itemUp";
        const string LINK_ITEM_DOWN = "itemDown";
        const string LINK_ITEM_LINK = "itemLink";
        string _genertedHTML = "";
        string _settingKey=null;
        public SummaryInformation summaryInformation = null;
        public event EventHandler DesignUpdated;
        
        public StatementDesigner(string settingKey)
            : this(iERPTransactionClient.GetSystemParamter(settingKey) as SummaryInformation)
        {
            _settingKey = settingKey;
        }

        public StatementDesigner()
            : this(new SummaryInformation())
        {
        }
        public StatementDesigner(SummaryInformation ss)
        {
            InitializeComponent();
            if (ss == null)
                ss = new SummaryInformation();
            startEdit(ss);
        }

        void startEdit(SummaryInformation info)
        {
            summaryInformation = info;
            checkHeaderRow.Checked = summaryInformation.headerRow;
            if (summaryInformation.headerRow)
            {
                textPerticularsHeader.Text = summaryInformation.itemHeader;
                textDebitLabel.Text = summaryInformation.debitHeader;
                textCreditLabel.Text = summaryInformation.creditHeader;
            }
            checkTwoColumns.Checked = summaryInformation.twoColumns;
            checkFlow.Checked=summaryInformation.flow;
            checkTotalRow.Checked=summaryInformation.grandTotal;
            renderDesign();
        }
        HTMLLinkHandler _handler = null;
        private void renderDesign()
        {
            bERPHtmlTable table = new bERPHtmlTable();
            bERPHtmlTableRowGroup body = new bERPHtmlTableRowGroup(TableRowGroupType.body);
            table.groups.Add(body);

            body.rows.Add(createItemRow(null, summaryInformation.rootSection, "0,"));
            List<int> levels = new List<int>();
            levels.Add(0);
            addChildRows(body, summaryInformation.rootSection, "0,",summaryInformation.twoColumns, 0,levels);
            int maxLevel = 0;
            foreach (int l in levels)
                if (l > maxLevel)
                    maxLevel = l;
            if (maxLevel > 0)
            {
                int i = 0;
                
                foreach (bERPHtmlTableRow row in body.rows)
                {
                    row.cells[0].colSpan = maxLevel - levels[i] + 1;
                    for (int k = 0; k < levels[i]; k++)
                    {
                        row.cells.Insert(0, new bERPHtmlTableCell("", "IndentCell"));
                    }
                    i++;
                }
            }
            
            StringBuilder builder = new StringBuilder();
            table.build(builder);
            _genertedHTML = builder.ToString();
            if (_handler == null)
            {
                _handler = new HTMLLinkHandler(this);
                browser.LoadControl(_handler);
            }
            else
                browser.UpdateElementInnerHtml("report", _genertedHTML);
        }

        private void addChildRows(bERPHtmlTableRowGroup body,SummarySection section,string rootIndex
                        , bool twoColumns, int level, List<int> levels
            )
        {
            int i=0;
            foreach (SummaryItemBase si in section.summaryItem)
            {
                levels.Add(level);
                if (si is SummaryItemInfo)
                {
                    bERPHtmlTableRow row = createItemRow(section, si, rootIndex + i.ToString());
                    body.rows.Add(row);
                }
                else
                {
                    bERPHtmlTableRow row = createItemRow(section, si, rootIndex + i.ToString());
                    body.rows.Add(row);
                    addChildRows(body, (SummarySection)si, rootIndex + i.ToString() + ","
                        ,twoColumns,level+1,levels
                        );
                }
                i++;
            }
        }
        bERPHtmlTableRow createItemRow(SummarySection parent, SummaryItemBase si, string index)
        {
            string path = INTAPS.UI.HTML.HTMLBuilder.GetAppHTMLRoot();
            bERPHtmlTableRow ret = new bERPHtmlTableRow();
            bERPHtmlTableCell cellDelete = new bERPHtmlTableCell("");
            cellDelete.css = "Item_cmd";
            if (!index.Equals( "0,"))
                cellDelete.innerHtml = "<a href='" + LINK_DELETE_ITEM + "?id=" + index + "'><img alt='Delete Item' src='" + path + "\\Images\\summary_delete_item.png'></a>";

            bERPHtmlTableCell cellEditItem = new bERPHtmlTableCell("");
            cellEditItem.css = "Item_cmd";
            if (!index.Equals("0,"))
                cellEditItem.innerHtml = "<a href='" + LINK_EDIT_ITEM + "?id=" + index + "'><img alt='Edit Item' src='" + path + "\\Images\\summary_edit_section.png'></a>";

            bERPHtmlTableCell cellAddItem = new bERPHtmlTableCell("");
            cellAddItem.css = "Item_cmd";
            if (si is SummarySection)
                cellAddItem.innerHtml = "<a href='" + LINK_ADD_ITEM + "?id=" + index + "'><img alt='Add Value' src='" + path + "\\Images\\summary_add_item.png'></a>";

            bERPHtmlTableCell cellAddSection = new bERPHtmlTableCell("");
            cellAddSection.css = "Item_cmd";
            if(si is SummarySection)
                cellAddSection.innerHtml = "<a href='" + LINK_ADD_SECTION + "?id=" + index + "'><img alt='Add Section' src='" + path + "\\Images\\summary_add_section.png'></a>";

            bERPHtmlTableCell cellUp= new bERPHtmlTableCell("");
            cellUp.css = "Item_cmd";
            if (parent!=null && parent.summaryItem.IndexOf(si)>0)
                cellUp.innerHtml = "<a href='" + LINK_ITEM_UP + "?id=" + index + "'><img alt='Move Item Up' src='" + path + "\\Images\\summary_item_up.png'></a>";
            
            bERPHtmlTableCell cellDown = new bERPHtmlTableCell("");
            cellDown.css = "Item_cmd";
            if (parent != null && parent.summaryItem.IndexOf(si) <parent.summaryItem.Count-1)
                cellDown.innerHtml = "<a href='" + LINK_ITEM_DOWN + "?id=" + index + "'><img alt='Move Item Down' src='" + path + "\\Images\\summary_item_down.png'></a>";

            bERPHtmlTableCell cellLink = new bERPHtmlTableCell("");
            cellLink.css = "Item_cmd";
            cellLink.innerHtml = "<a href='" + LINK_ITEM_LINK + "?id=" + index + "'><img alt='Group Item Under' src='" + path + "\\Images\\summary_group_under.png'></a>";


            bERPHtmlTableCell cell;
            ret.cells.AddRange(new bERPHtmlTableCell[]{
                cell=new bERPHtmlTableCell(si.label)
                ,cellDelete,cellEditItem,cellAddItem,cellAddSection,cellUp,cellDown,cellLink
            });
            ret.css = si.format;
            return ret;
        }
        SummaryItemBase getByIndex(string sindex, out SummarySection sec)
        {
            if (sindex.Equals("0,"))
            {
                sec = null;
                return summaryInformation.rootSection;
            }
            string[] index = sindex.Split(',');
            sec = summaryInformation.rootSection;
            SummaryItemBase sitem = null;
            for (int i = 1; i < index.Length; i++)
            {
                sitem = sec.summaryItem[int.Parse(index[i])];
                if (i < index.Length - 1)
                {
                    if (sitem is SummarySection)
                        sec = (SummarySection)sitem;
                    else
                        return null;
                }
            }
            return sitem;
        }
        internal bool ProcessUrl(string path, Hashtable query)
        {
            ReportItemEditor item;
            SummarySection sec ;
            SummaryItemBase sib;
            int itemIndex;
            switch (path)
            {
                case LINK_ADD_ITEM:
                    item = new ReportItemEditor(false);
                    if (item.ShowDialog(this) == DialogResult.OK)
                    {
                        ((SummarySection) getByIndex((string)query["id"],out sec)).summaryItem.Add(item.itemInfo);
                        renderDesign();
                    }
                    return true;
                case LINK_ADD_SECTION:
                    item = new ReportItemEditor(true);
                    if (item.ShowDialog(this) == DialogResult.OK)
                    {
                        ((SummarySection)getByIndex((string)query["id"], out sec)).summaryItem.Add(item.itemInfo);
                        renderDesign();
                    }
                    return true;
                case LINK_EDIT_ITEM:
                    sib=getByIndex((string)query["id"],out sec);
                    item = new ReportItemEditor(sib);
                    if(item.ShowDialog(this)==DialogResult.OK)
                    {                        
                        sec.summaryItem[sec.summaryItem.IndexOf(sib)] = item.itemInfo;
                        if (sib is SummarySection)
                            ((SummarySection)item.itemInfo).summaryItem = ((SummarySection)sib).summaryItem;
                        renderDesign();
                    }
                    return true;
                case LINK_DELETE_ITEM:
                    if (MessageBox.ShowWarningMessage("Are you sure you want to delete this item?") == DialogResult.Yes)
                    {
                        SummaryItemBase sitem = getByIndex((string)query["id"], out sec);
                        if (sitem == null)
                        {
                            MessageBox.ShowErrorMessage("Invalid command");
                            return true;
                        }
                        sec.summaryItem.Remove(sitem);
                        renderDesign();
                    }
                    return true;
                case LINK_ITEM_UP:
                    sib=getByIndex((string)query["id"],out sec);
                    itemIndex=sec.summaryItem.IndexOf(sib);
                    sec.summaryItem.RemoveAt(itemIndex);
                    sec.summaryItem.Insert(itemIndex - 1, sib);
                    renderDesign();
                    return true;
                case LINK_ITEM_DOWN:
                    sib=getByIndex((string)query["id"],out sec);
                    itemIndex=sec.summaryItem.IndexOf(sib);
                    sec.summaryItem.RemoveAt(itemIndex);
                    sec.summaryItem.Insert(itemIndex + 1, sib);
                    renderDesign();
                    return true;
                case LINK_ITEM_LINK:
                    INTAPS.UI.UIFormApplicationBase.runInUIThread(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                    {
                        StatementItemPicker itemPicker = new StatementItemPicker(summaryInformation);
                        if(itemPicker.ShowDialog(this)==DialogResult.OK)
                        {
                                sib=getByIndex((string)query["id"],out sec);
                                itemIndex=sec.summaryItem.IndexOf(sib);
                            if(sib is SummarySection)
                            {
                                if(summaryInformation.isHierarchicalParent((SummarySection)sib,itemPicker.selectedSection))
                                {
                                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("It is not allowed to make a section a child of its child");
                                    return;
                                }
                            }
                            summaryInformation.getItemParent(sib).summaryItem.Remove(sib);
                            itemPicker.selectedSection.summaryItem.Add(sib);
                            renderDesign();
                        }
                    })
                    );
                    return true;
            }
            return false;
        }
        SummaryInformation extractDesign(out string error)
        {
            SummaryInformation ret = summaryInformation;
            ret.headerRow = checkHeaderRow.Checked;
            if (ret.headerRow)
            {
                ret.itemHeader = textPerticularsHeader.Text;
                ret.debitHeader = textDebitLabel.Text;
                ret.creditHeader = textCreditLabel.Text;
            }
            ret.flow = checkFlow.Checked;
            ret.twoColumns = checkTwoColumns.Checked;
            ret.grandTotal = checkTotalRow.Checked;
            error = null;
            return ret;

        }
        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (_settingKey != null)
            {
                try
                {
                    string err;
                    SummaryInformation sum = extractDesign(out err);
                    if (sum == null)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError(err);
                        return;
                    }
                    summaryInformation = sum;
                    iERPTransactionClient.SetSystemParameter(_settingKey, summaryInformation);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Design saved successfully");
                }
                catch (Exception ex)
                {
                    MessageBox.ShowException(ex);
                    return;
                }
            }
            if (DesignUpdated != null)
                DesignUpdated(this, null);
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            renderDesign();
        }

        private void chkHeadeRow_CheckedChanged(object sender, EventArgs e)
        {
            textPerticularsHeader.Enabled = textDebitLabel.Enabled = checkHeaderRow.Checked;
            textCreditLabel.Enabled = checkHeaderRow.Checked && checkTwoColumns.Enabled;
            if (checkTwoColumns.Enabled)
                labelDebit.Text = "Debit Label";
            else
                labelDebit.Text = "Balance Label";
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "*.statementdesign|*.statementdesign";
            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(SummaryInformation));
                    using (System.IO.FileStream fs = System.IO.File.OpenRead(dialog.FileName))
                    {
                        startEdit(s.Deserialize(fs) as SummaryInformation);
                        fs.Close();
                    }
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
                }
            }

        }

        private void buttonExport_Click(object sender, EventArgs e)
        {
            string err;
            SummaryInformation sum = extractDesign(out err);
            if (err != null)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError(err);
                return;
            }
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "*.statementdesign|*.statementdesign";
            if(dialog.ShowDialog(this)==DialogResult.OK)
            {
                try
                {
                    System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(SummaryInformation));
                    using(System.IO.FileStream fs=System.IO.File.Create(dialog.FileName))
                    {
                        s.Serialize(fs, sum);
                        fs.Close();
                    }
                }
                catch(Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
                }
            }
        }
    }
}