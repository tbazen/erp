﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERP.Client
{
    public partial class StatementItemPicker : Form
    {
        SummaryInformation sinfo;
        class SummaryItemTree:INTAPS.UI.Windows.ObjectTreeObjectID
        {
            public StatementItemPicker parent;



            protected override object[] GetChildItems(object item)
            {
                if (item == null)
                {
                    return new object[] { parent.sinfo.rootSection };
                }
                if(item is SummarySection)
                {
                    return ((SummarySection)item).summaryItem.ToArray();
                }
                return new object[0];
            }

            protected override object GetItemID(object item)
            {
                return item;
            }

            protected override object GetItemPID(object item)
            {
                return parent.sinfo.getItemParent((SummaryItemBase)item);
            }

            protected override string GetItemText(object item)
            {
                return ((SummaryItemBase)item).label;
            }
        }
        SummaryItemTree tree;
        public StatementItemPicker(SummaryInformation sinfo)
        {
            InitializeComponent();
            this.sinfo = sinfo;

            tree = new SummaryItemTree() { parent = this };
            tree.Dock = DockStyle.Fill;
            this.Controls.Add(tree);
            tree.BringToFront();
            tree.LoadData();
            tree.DoubleClick += tree_DoubleClick;
        }

        void tree_DoubleClick(object sender, EventArgs e)
        {
            buttonOk_Click(null, null);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        public SummarySection selectedSection;
        private void buttonOk_Click(object sender, EventArgs e)
        {
            SummarySection b = tree.SelectedObject as SummarySection;
            if (b == null)
                return;
            selectedSection = b;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
