﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using INTAPS.ClientServer.Client;

namespace BIZNET.iERP.Client
{
    public class CashAccountCategoryTree:TreeView
    {
        public CashAccountCategory SelectedCategory
        {
            get
            {
                if (this.SelectedNode == null)
                    return null;
                return (CashAccountCategory)this.SelectedNode.Tag;
            }
        }
        public int selectedCashAccountID
        {
            get
            {
                if (this.SelectedNode == null)
                    return -1;
                return ((CashAccountCategory)this.SelectedNode.Tag).id;
            }
        }
        public CashAccountCategoryTree()
        {
            if (!ApplicationClient.isConnected())
                return;
            loadChilds(this.Nodes, -1);
            this.ExpandAll();
        }

        private void loadChilds(TreeNodeCollection nodes, int categoryID)
        {
            nodes.Clear();
            foreach (CashAccountCategory cat in iERPTransactionClient.getChildCashCategories(categoryID))
            {
                TreeNode n = new TreeNode(cat.name);
                n.Tag = cat;
                nodes.Add(n);
                loadChilds(n.Nodes, cat.id);
            }
        }

        internal void reload()
        {
            loadChilds(this.Nodes, -1);
        }
        TreeNode getNode(TreeNodeCollection nodes, int catID)
        {
            foreach (TreeNode n in nodes)
            {
                if (((CashAccountCategory)n.Tag).id == catID)
                    return n;
                TreeNode childNode = getNode(n.Nodes, catID);
                if (childNode != null)
                    return childNode;
            }
            return null;
        }
        internal void selectNode(int catID)
        {
            this.SelectedNode= getNode(this.Nodes, catID);
        }
    }
}
