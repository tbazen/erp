﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BIZNET.iERP.Client
{
    public partial class CashCategoryForm : DevExpress.XtraEditors.XtraForm
    {
        CashAccountCategory _cat = null;
        int _parentCategoryID = -1;
        public CashAccountCategory newCat = null;
        public CashCategoryForm(int parentCategoryID)
        {
            InitializeComponent();
            _parentCategoryID = parentCategoryID;
            if (_parentCategoryID == -1)
            {
                labelParentCategory.Text = "";
            }
            else
                labelParentCategory.Text = "Child of " + iERPTransactionClient.getCashCategory(_parentCategoryID).name;

        }
        public CashCategoryForm(CashAccountCategory cat)
            : this(cat.categoryID)
        {
            _cat = cat;
            textName.Text = _cat.name;
            acAccount.SetByID(_cat.accountID);
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (textName.Text.Trim() == "")
                {
                    MessageBox.ShowErrorMessage("Enter name");
                    return;
                }
                if (acAccount.GetAccountID() == -1)
                {
                    MessageBox.ShowErrorMessage("Select account");
                    return;
                }
                CashAccountCategory newCat = new CashAccountCategory();
                newCat.name = textName.Text.Trim();
                newCat.categoryID = _parentCategoryID;
                newCat.accountID = acAccount.GetAccountID();
                if (_cat == null)
                    iERPTransactionClient.createCashCategory(newCat);
                else
                {
                    newCat.id = _cat.id;
                    iERPTransactionClient.updateCashCategory(newCat);
                }
                this.newCat = newCat;
                this.DialogResult = DialogResult.OK;
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}