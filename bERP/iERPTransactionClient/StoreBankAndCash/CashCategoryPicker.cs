﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BIZNET.iERP.Client
{
    public partial class CashCategoryPicker : DevExpress.XtraEditors.XtraForm
    {
        public CashAccountCategory category=null;
        public CashCategoryPicker()
        {
            InitializeComponent();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (catTree.selectedCashAccountID == -1)
            {
                MessageBox.ShowErrorMessage("Select a category");
                return;
            }
            category = catTree.SelectedCategory;
            this.DialogResult = DialogResult.OK;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}