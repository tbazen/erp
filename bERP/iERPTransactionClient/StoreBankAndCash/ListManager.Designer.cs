﻿namespace BIZNET.iERP.Client
{
    partial class ListManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListManager));
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridManager = new DevExpress.XtraGrid.GridControl();
            this.gridViewManager = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutCategory = new DevExpress.XtraLayout.LayoutControlItem();
            this.cashCatTree = new BIZNET.iERP.Client.CashAccountCategoryTree();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.standaloneBarDockControl2 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barManager = new DevExpress.XtraBars.Bar();
            this.btnAdd = new DevExpress.XtraBars.BarButtonItem();
            this.btnEdit = new DevExpress.XtraBars.BarButtonItem();
            this.btnDelete = new DevExpress.XtraBars.BarButtonItem();
            this.btnActivate = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barCheckItem1 = new DevExpress.XtraBars.BarCheckItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutConverter1 = new DevExpress.XtraLayout.Converter.LayoutConverter(this.components);
            this.contextCategory = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menNewRootCat = new System.Windows.Forms.ToolStripMenuItem();
            this.menNewCat = new System.Windows.Forms.ToolStripMenuItem();
            this.menEditCat = new System.Windows.Forms.ToolStripMenuItem();
            this.menDelCat = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            this.contextCategory.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(723, 592);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.AppearanceTabPage.Header.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup2.AppearanceTabPage.Header.Options.UseFont = true;
            this.layoutControlGroup2.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup2.CaptionImage")));
            this.layoutControlGroup2.CustomizationFormText = "TradeRelation General Information";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutCategory});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(717, 586);
            this.layoutControlGroup2.Text = "List General Information";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridManager;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(230, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(487, 586);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // gridManager
            // 
            this.gridManager.Location = new System.Drawing.Point(235, 5);
            this.gridManager.MainView = this.gridViewManager;
            this.gridManager.Name = "gridManager";
            this.gridManager.Size = new System.Drawing.Size(483, 582);
            this.gridManager.TabIndex = 6;
            this.gridManager.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewManager});
            this.gridManager.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridManager_ProcessGridKey);
            // 
            // gridViewManager
            // 
            this.gridViewManager.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewManager.Appearance.GroupPanel.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.gridViewManager.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewManager.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewManager.GridControl = this.gridManager;
            this.gridViewManager.GroupPanelText = "Store List";
            this.gridViewManager.Name = "gridViewManager";
            this.gridViewManager.OptionsBehavior.Editable = false;
            this.gridViewManager.OptionsFind.FindDelay = 100;
            this.gridViewManager.OptionsMenu.EnableColumnMenu = false;
            this.gridViewManager.OptionsMenu.EnableFooterMenu = false;
            this.gridViewManager.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewManager.OptionsMenu.ShowAutoFilterRowItem = false;
            this.gridViewManager.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewManager_FocusedRowChanged);
            this.gridViewManager.DoubleClick += new System.EventHandler(this.gridViewManager_DoubleClick);
            // 
            // layoutCategory
            // 
            this.layoutCategory.Control = this.cashCatTree;
            this.layoutCategory.CustomizationFormText = "Categories";
            this.layoutCategory.Location = new System.Drawing.Point(0, 0);
            this.layoutCategory.Name = "layoutCategory";
            this.layoutCategory.Size = new System.Drawing.Size(230, 586);
            this.layoutCategory.Text = "Categories";
            this.layoutCategory.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutCategory.TextSize = new System.Drawing.Size(52, 13);
            // 
            // cashCatTree
            // 
            this.cashCatTree.ContextMenuStrip = this.contextCategory;
            this.cashCatTree.Location = new System.Drawing.Point(5, 21);
            this.cashCatTree.Name = "cashCatTree";
            this.cashCatTree.Size = new System.Drawing.Size(226, 566);
            this.cashCatTree.TabIndex = 8;
            this.cashCatTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.cashCatTree_AfterSelect);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.standaloneBarDockControl2;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 50);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(5, 40);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(717, 50);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // standaloneBarDockControl2
            // 
            this.standaloneBarDockControl2.CausesValidation = false;
            this.standaloneBarDockControl2.Location = new System.Drawing.Point(5, 5);
            this.standaloneBarDockControl2.Name = "standaloneBarDockControl2";
            this.standaloneBarDockControl2.Size = new System.Drawing.Size(713, 46);
            this.standaloneBarDockControl2.Text = "standaloneBarDockControl2";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barManager});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl2);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnAdd,
            this.btnEdit,
            this.btnDelete,
            this.barCheckItem1,
            this.btnActivate,
            this.barStaticItem1});
            this.barManager1.MainMenu = this.barManager;
            this.barManager1.MaxItemId = 17;
            // 
            // barManager
            // 
            this.barManager.BarName = "Main menu";
            this.barManager.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this.barManager.DockCol = 0;
            this.barManager.DockRow = 0;
            this.barManager.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barManager.FloatLocation = new System.Drawing.Point(197, 188);
            this.barManager.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnAdd, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnActivate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barManager.OptionsBar.MultiLine = true;
            this.barManager.OptionsBar.UseWholeRow = true;
            this.barManager.Text = "Main menu";
            // 
            // btnAdd
            // 
            this.btnAdd.Caption = "&Add";
            this.btnAdd.Glyph = ((System.Drawing.Image)(resources.GetObject("btnAdd.Glyph")));
            this.btnAdd.Id = 0;
            this.btnAdd.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ItemAppearance.Normal.Options.UseFont = true;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAdd_ItemClick);
            // 
            // btnEdit
            // 
            this.btnEdit.Caption = "&Edit";
            this.btnEdit.Enabled = false;
            this.btnEdit.Glyph = ((System.Drawing.Image)(resources.GetObject("btnEdit.Glyph")));
            this.btnEdit.Id = 1;
            this.btnEdit.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.ItemAppearance.Normal.Options.UseFont = true;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEdit_ItemClick);
            // 
            // btnDelete
            // 
            this.btnDelete.Caption = "&Delete";
            this.btnDelete.Enabled = false;
            this.btnDelete.Glyph = ((System.Drawing.Image)(resources.GetObject("btnDelete.Glyph")));
            this.btnDelete.Id = 2;
            this.btnDelete.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ItemAppearance.Normal.Options.UseFont = true;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDelete_ItemClick);
            // 
            // btnActivate
            // 
            this.btnActivate.Caption = "Activate/Deactivate";
            this.btnActivate.Enabled = false;
            this.btnActivate.Glyph = ((System.Drawing.Image)(resources.GetObject("btnActivate.Glyph")));
            this.btnActivate.Id = 4;
            this.btnActivate.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActivate.ItemAppearance.Normal.Options.UseFont = true;
            this.btnActivate.Name = "btnActivate";
            this.btnActivate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnActivate_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(723, 40);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 632);
            this.barDockControlBottom.Size = new System.Drawing.Size(723, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 40);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 592);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(723, 40);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 592);
            // 
            // barCheckItem1
            // 
            this.barCheckItem1.Caption = "barCheckItem1";
            this.barCheckItem1.Id = 3;
            this.barCheckItem1.Name = "barCheckItem1";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "First";
            this.barStaticItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem1.Glyph")));
            this.barStaticItem1.Id = 5;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.cashCatTree);
            this.layoutControl1.Controls.Add(this.gridManager);
            this.layoutControl1.Controls.Add(this.standaloneBarDockControl2);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
            this.layoutControl1.Location = new System.Drawing.Point(0, 40);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(333, 252, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(723, 592);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // contextCategory
            // 
            this.contextCategory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menNewRootCat,
            this.menNewCat,
            this.menEditCat,
            this.menDelCat});
            this.contextCategory.Name = "contextCategory";
            this.contextCategory.Size = new System.Drawing.Size(178, 92);
            this.contextCategory.Opening += new System.ComponentModel.CancelEventHandler(this.contextCategory_Opening);
            // 
            // menNewRootCat
            // 
            this.menNewRootCat.Name = "menNewRootCat";
            this.menNewRootCat.Size = new System.Drawing.Size(177, 22);
            this.menNewRootCat.Text = "New Root Category";
            this.menNewRootCat.Click += new System.EventHandler(this.menNewRootCat_Click);
            // 
            // menNewCat
            // 
            this.menNewCat.Name = "menNewCat";
            this.menNewCat.Size = new System.Drawing.Size(177, 22);
            this.menNewCat.Text = "New Category";
            this.menNewCat.Click += new System.EventHandler(this.menNewCat_Click);
            // 
            // menEditCat
            // 
            this.menEditCat.Name = "menEditCat";
            this.menEditCat.Size = new System.Drawing.Size(177, 22);
            this.menEditCat.Text = "Edit Category";
            this.menEditCat.Click += new System.EventHandler(this.menEditCat_Click);
            // 
            // menDelCat
            // 
            this.menDelCat.Name = "menDelCat";
            this.menDelCat.Size = new System.Drawing.Size(177, 22);
            this.menDelCat.Text = "Delete Category";
            this.menDelCat.Click += new System.EventHandler(this.menDelCat_Click);
            // 
            // ListManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(723, 632);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ListManager";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "List Manager";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            this.contextCategory.ResumeLayout(false);
            this.ResumeLayout(false);

}

        #endregion


        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl gridManager;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewManager;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnAdd;
        private DevExpress.XtraBars.BarButtonItem btnEdit;
        private DevExpress.XtraBars.BarButtonItem btnDelete;
        private DevExpress.XtraBars.BarButtonItem btnActivate;
        private DevExpress.XtraBars.BarCheckItem barCheckItem1;
        private DevExpress.XtraLayout.Converter.LayoutConverter layoutConverter1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutCategory;
        private CashAccountCategoryTree cashCatTree;
        private System.Windows.Forms.ContextMenuStrip contextCategory;
        private System.Windows.Forms.ToolStripMenuItem menNewCat;
        private System.Windows.Forms.ToolStripMenuItem menEditCat;
        private System.Windows.Forms.ToolStripMenuItem menDelCat;
        private System.Windows.Forms.ToolStripMenuItem menNewRootCat;
    }
}