﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using DevExpress.XtraLayout.Utils;
using System.Collections;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using DevExpress.XtraGrid;
using DevExpress.Utils;
using DevExpress.XtraLayout;

namespace BIZNET.iERP.Client
{
    public partial class ListManager : XtraForm
    {
        DataTable storeTable;
        DataTable cashAccountTable;
        DataTable bankAccountTable;
        private ListManagerType m_ListManagerType;
        DevExpress.XtraSplashScreen.SplashScreenManager waitScreen;
        public ListManager(ListManagerType managerType)
        {
            try
            {
                InitializeComponent();
                waitScreen = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::BIZNET.iERP.Client.WaitForm1), true, true);

                m_ListManagerType = managerType;
                if (managerType == ListManagerType.Store)
                {
                    storeTable = new DataTable();
                    Text = "Store Manager";
                    layoutCategory.Visibility = LayoutVisibility.Never;
                    PrepareStoreTable();
                    LoadStores();
                }

                else if (managerType == ListManagerType.CashAccount)
                {
                    InitializeCashAccountSettings();
                    cashAccountTable = new DataTable();
                    Text = "Cash Account Manager";
                    layoutCategory.Visibility = LayoutVisibility.Always; 
                    PrepareCashAccountTable();
                    LoadCashAccounts();
                }
                else
                {
                    InitializeBankAccountSettings();
                    bankAccountTable = new DataTable();
                    Text = "Bank Account Manager";
                    layoutCategory.Visibility = LayoutVisibility.Never; 
                    PrepareBankAccountTable();
                    LoadBankAccounts();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

         private void InitializeCashAccountSettings()
         {
             InitializeCashAccountIcons();
             InitializeCashAccountTitles();
         }

         private void InitializeBankAccountSettings()
         {
             InitializeBankAccountIcons();
             InitializeBankAccountTitles();
         }
         
        private void PrepareStoreTable()
        {
            storeTable.Columns.Add("Code", typeof(string));
            storeTable.Columns.Add("Name", typeof(string));
            storeTable.Columns.Add("Cost Center", typeof(string));
            storeTable.Columns.Add("Activated", typeof(bool));
            gridManager.DataSource = storeTable;
            gridViewManager.Columns["Activated"].Visible = false;

        }

        private void InitializeCashAccountIcons()
        {
            btnAdd.Glyph = Properties.Resources.cash_add;
            btnEdit.Glyph = Properties.Resources.cash_edit;
            btnDelete.Glyph = Properties.Resources.cash_delete;
            btnActivate.Glyph = Properties.Resources.cash_activate;
        }
        private void InitializeCashAccountTitles()
        {
            gridViewManager.GroupPanelText = "Cash Accounts List";
        }
        private void PrepareCashAccountTable()
        {
            cashAccountTable.Columns.Add("Code", typeof(string));
            cashAccountTable.Columns.Add("Name", typeof(string));
            cashAccountTable.Columns.Add("Cost Center", typeof(string));
            cashAccountTable.Columns.Add("Activated", typeof(bool));
            gridManager.DataSource = cashAccountTable;
            gridViewManager.Columns["Activated"].Visible = false;
        }

        private void InitializeBankAccountIcons()
        {
            btnAdd.Glyph = Properties.Resources.bank_add;
            btnEdit.Glyph = Properties.Resources.bank_edit;
            btnDelete.Glyph = Properties.Resources.bank_delete;
            btnActivate.Glyph = Properties.Resources.bank_activate;
        }
        private void InitializeBankAccountTitles()
        {
            gridViewManager.GroupPanelText = "Bank Accounts List";
        }
        private void PrepareBankAccountTable()
        {
            bankAccountTable.Columns.Add("MainCsAccount", typeof(int));
            bankAccountTable.Columns.Add("Bank Name", typeof(string));
            bankAccountTable.Columns.Add("Branch Name", typeof(string));
            bankAccountTable.Columns.Add("Account Number", typeof(string));
            bankAccountTable.Columns.Add("Account Name", typeof(string));
            bankAccountTable.Columns.Add("Cost Center", typeof(string));
            bankAccountTable.Columns.Add("Activated", typeof(bool));
            gridManager.DataSource = bankAccountTable;
            gridViewManager.Columns["Activated"].Visible = false;
            gridViewManager.Columns["MainCsAccount"].Visible = false;

        }

        private void LoadStores()
        {
            try
            {
                storeTable.Clear();
                StoreInfo[] stores = iERPTransactionClient.GetAllStores();
                waitScreen.ShowWaitForm();
                foreach (StoreInfo store in stores)
                {
                    DataRow row = storeTable.NewRow();
                    CostCenter costCenter = AccountingClient.GetAccount<CostCenter>(store.costCenterID);
                    CostCenter parentCostCenter = AccountingClient.GetAccount<CostCenter>(costCenter.PID);
                    waitScreen.SetWaitFormDescription("Loading: " + store.description);
                    row[0] = store.code;
                    row[1] = store.description;
                    row[2] = parentCostCenter.Name;
                    row[3] = costCenter.Status == AccountStatus.Activated ? true : false;
                    storeTable.Rows.Add(row);
                    Application.DoEvents();
                }
                gridManager.DataSource = storeTable;
                gridManager.RefreshDataSource();
                ApplyFormatConditioning();


            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to retrieve stores\n" + ex.Message);
            }
            finally
            {
                waitScreen.CloseWaitForm();
            }
        }

        private void LoadCashAccounts()
        {
            try
            {
                cashAccountTable.Clear();
                if (cashCatTree.SelectedNode == null)
                    return;
                int catID = (cashCatTree.SelectedNode.Tag as CashAccountCategory).id;
                CashAccount[] cashAccounts = iERPTransactionClient.getCashAccountsByCategory(catID);
                waitScreen.ShowWaitForm();
                foreach (CashAccount cashAccount in cashAccounts)
                {
                    DataRow row = cashAccountTable.NewRow();
                    CostCenterAccount costCenterAcc = AccountingClient.GetCostCenterAccount(cashAccount.csAccountID);
                    if (costCenterAcc == null)
                        continue;
                    CostCenter costCenter = AccountingClient.GetAccount<CostCenter>(costCenterAcc.costCenterID);
                    waitScreen.SetWaitFormDescription("Searching: " + cashAccount.name);
                    row[0] = cashAccount.code;
                    row[1] = cashAccount.name;
                    row[2] = costCenter.Name;
                    row[3] = AccountingClient.GetAccount<Account>(costCenterAcc.accountID).Status == AccountStatus.Activated ? true : false;
                    cashAccountTable.Rows.Add(row);
                }
                gridManager.DataSource = cashAccountTable;
                gridManager.RefreshDataSource();
                ApplyFormatConditioning();
                waitScreen.CloseWaitForm();

            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to retrieve cash accounts\n" + ex.Message);
            }
        }
        private void LoadBankAccounts()
        {
            try
            {
                bankAccountTable.Clear();
                BankAccountInfo[] bankAccounts = iERPTransactionClient.GetAllBankAccounts();
                waitScreen.ShowWaitForm();
                foreach (BankAccountInfo bankAccount in bankAccounts)
                {
                    DataRow row = bankAccountTable.NewRow();
                    CostCenterAccount csAccount = AccountingClient.GetCostCenterAccount(bankAccount.mainCsAccount);
                    CostCenter costCenter = AccountingClient.GetAccount<CostCenter>(csAccount.costCenterID);
                    waitScreen.SetWaitFormDescription("Searching: " + bankAccount.bankName);
                    row[0] = bankAccount.mainCsAccount;
                    row[1] = bankAccount.bankName;
                    row[2] = bankAccount.branchName;
                    row[3] = bankAccount.bankAccountNumber;
                    row[4] = bankAccount.bankAccountName;
                    row[5] = costCenter.Name;
                    row[6] = AccountingClient.GetAccount<Account>(csAccount.accountID).Status == AccountStatus.Activated ? true : false;
                    bankAccountTable.Rows.Add(row);
                    Application.DoEvents();
                }
                gridManager.DataSource = bankAccountTable;
                gridManager.RefreshDataSource();
                ApplyFormatConditioning();
                waitScreen.CloseWaitForm();
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to retrieve bank accounts\n" + ex.Message);
                if (waitScreen.IsSplashFormVisible)
                    waitScreen.CloseWaitForm();
            }
        }


       
       
        private void btnAdd_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (m_ListManagerType == ListManagerType.Store)
            {
                using (RegisterStore store = new RegisterStore())
                {
                    store.StoreAdded += StoreAdded;
                    store.ShowInTaskbar = false;
                    store.StartPosition = FormStartPosition.CenterScreen;
                    store.ShowDialog();
                }
            }
             
            else if(m_ListManagerType==ListManagerType.CashAccount)
            {
                if (cashCatTree.SelectedNode == null)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Select a category");
                    return;
                }
                using (RegisterCashAccount cashAccount = new RegisterCashAccount(((CashAccountCategory)cashCatTree.SelectedNode.Tag).id))
                {
                    cashAccount.CashAccountAdded += CashAccountAdded;
                    cashAccount.ShowInTaskbar = false;
                    cashAccount.StartPosition = FormStartPosition.CenterScreen;
                    cashAccount.ShowDialog();
                }
            }
            else
            {
                using (RegisterBank bankAccount = new RegisterBank())
                {
                    bankAccount.BankAccountAdded += BankAccountAdded;
                    bankAccount.ShowInTaskbar = false;
                    bankAccount.StartPosition = FormStartPosition.CenterScreen;
                    bankAccount.ShowDialog();
                }
            }
            
        }

       void BankAccountAdded(int mainCsAccount)
        {
           LoadBankAccounts();
        }

        void CashAccountAdded(string code)
        {
            LoadCashAccounts();
        }
       
        void StoreAdded(string code)
        {
            LoadStores();
        }

        private void btnEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            ShowRegistrationFormForEditing();
        }

        
        private void ShowCashAccountInformationForEditing(string code)
        {
            using (RegisterCashAccount cashAccount = new RegisterCashAccount(iERPTransactionClient.GetCashAccount(code)))
            {
                cashAccount.ShowInTaskbar = false;
                cashAccount.StartPosition = FormStartPosition.CenterScreen;
                if (cashAccount.ShowDialog() == DialogResult.OK)
                {
                   LoadCashAccounts();
                }
            }
        }

        private void ShowBankAccountInformationForEditing(int mainCsAccount)
        {
            using (RegisterBank bankAccount = new RegisterBank(mainCsAccount))
            {
                bankAccount.ShowInTaskbar = false;
                bankAccount.StartPosition = FormStartPosition.CenterScreen;
                if (bankAccount.ShowDialog() == DialogResult.OK)
                {
                   LoadBankAccounts();
                }
            }
        }
         
        private void ShowStoreInfoForEditing(string code)
        {
            using (RegisterStore store = new RegisterStore(code))
            {
                store.ShowInTaskbar = false;
                store.StartPosition = FormStartPosition.CenterScreen;
                if (store.ShowDialog() == DialogResult.OK)
                {
                    LoadStores();
                }
            }
        }

        private void btnDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                int[] selectedRowHandle = gridViewManager.GetSelectedRows();
                object code = m_ListManagerType == ListManagerType.BankAccount ? gridViewManager.GetRowCellValue(selectedRowHandle[0], "MainCsAccount") : (string)gridViewManager.GetRowCellValue(selectedRowHandle[0], "Code");
                if (m_ListManagerType == ListManagerType.Store)
                {
                    if (MessageBox.ShowWarningMessage("Are you sure you want to delete the store?") == DialogResult.Yes)
                    {
                        iERPTransactionClient.DeleteStore((string)code);
                        MessageBox.ShowSuccessMessage("Store successfully deleted");
                        gridViewManager.DeleteSelectedRows();
                    }
                }

                else if (m_ListManagerType == ListManagerType.CashAccount)
                {
                    if (MessageBox.ShowWarningMessage("Are you sure you want to delete the cash account?") == DialogResult.Yes)
                    {
                        iERPTransactionClient.DeleteCashAccount((string)code);
                        MessageBox.ShowSuccessMessage("Cash account successfully deleted");
                        gridViewManager.DeleteSelectedRows();
                    }
                }
                else
                {
                    if (MessageBox.ShowWarningMessage("Are you sure you want to delete the bank account?") == DialogResult.Yes)
                    {
                        iERPTransactionClient.DeleteBankAccount((int)code);
                        MessageBox.ShowSuccessMessage("Bank account successfully deleted");
                        gridViewManager.DeleteSelectedRows();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        
         
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnActivate_ItemClick(object sender, ItemClickEventArgs e)
        {

            int[] selectedRowHandle = gridViewManager.GetSelectedRows();
            object code = m_ListManagerType == ListManagerType.BankAccount ? gridViewManager.GetRowCellValue(selectedRowHandle[0], "MainCsAccount") : (string)gridViewManager.GetRowCellValue(selectedRowHandle[0], "Code");
            bool activated = (bool)gridViewManager.GetRowCellValue(selectedRowHandle[0], "Activated");
            if (m_ListManagerType == ListManagerType.Store)
                ActivateOrDeactivateStore((string)code, activated);

            else if (m_ListManagerType == ListManagerType.CashAccount)
                ActivateOrDeactivateCashAccount((string)code, activated);
            else
                ActivateOrDeactivateBankAccount((int)code, activated);

        }

        private void ActivateOrDeactivateStore(string code, bool activated)
        {
            try
            {

                if (activated)
                {
                    if (MessageBox.ShowWarningMessage("Are you sure you want to deactivate the store?") == DialogResult.Yes)
                        iERPTransactionClient.DeactivateStore(code);
                }
                else
                {
                    if (MessageBox.ShowWarningMessage("Are you sure you want to activate the store?") == DialogResult.Yes)
                        iERPTransactionClient.ActivateStore(code);
                }
                string message = activated ? "Store successfully deactivated" : "Store successfully activated";
                MessageBox.ShowSuccessMessage(message);
                LoadStores();
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to activate/deactivate store!\n" + ex.Message);
            }
        }

       
        private void ActivateOrDeactivateCashAccount(string code, bool activated)
        {
            try
            {
                if (activated)
                {
                   // if (MessageBox.ShowWarningMessage("Are you sure you want to deactivate the selected expense item?") == DialogResult.Yes)
                       // iERPTransactionClient.DeactivateExpenseItem(code);
                    throw new NotImplementedException("Method not implemented");
                }
                else
                {
                    //if (MessageBox.ShowWarningMessage("Are you sure you want to activate the selected expense item?") == DialogResult.Yes)
                        //iERPTransactionClient.ActivateExpenseItem(code);
                    throw new NotImplementedException("Method not implemented");
                }
                string message = activated ? "Cash account successfully deactivated" : "Cash account successfully activated";
                MessageBox.ShowSuccessMessage(message);
                LoadCashAccounts();
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to activate/deactivate cash account!\n" + ex.Message);

            }
        }
        private void ActivateOrDeactivateBankAccount(int mainCsAccount, bool activated)
        {
            try
            {
                if (activated)
                {
                  //  if (MessageBox.ShowWarningMessage("Are you sure you want to deactivate the selected sales item?") == DialogResult.Yes)
                       // iERPTransactionClient.DeactivateSalesItem(code);
                    throw new NotImplementedException("Method not implemented");
                }
                else
                {
                  //  if (MessageBox.ShowWarningMessage("Are you sure you want to activate the selected sales item?") == DialogResult.Yes)
                        //iERPTransactionClient.ActivateSalesItem(code);
                    throw new NotImplementedException("Method not implemented");
                }
                string message = activated ? "Bank account successfully deactivated" : "Bank account successfully activated";
                MessageBox.ShowSuccessMessage(message);
                LoadBankAccounts();
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to activate/deactivate sales item!\n" + ex.Message);

            }
        }
         
        private void gridViewManager_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewManager.SelectedRowsCount == 1)
            {
                btnEdit.Enabled = true;
                btnDelete.Enabled = true;
                btnActivate.Enabled = true;
                int[] rowHandle = gridViewManager.GetSelectedRows();
                bool activated = (bool)gridViewManager.GetRowCellValue(rowHandle[0], "Activated");
                btnActivate.Caption = activated ? "Deactivate" : "Activate";
            }
            else
            {
                btnEdit.Enabled = false;
                btnDelete.Enabled = false;
                btnActivate.Enabled = false;
            }
        }

        private void gridViewManager_DoubleClick(object sender, EventArgs e)
        {
            ShowRegistrationFormForEditing();

        }

        private void ShowRegistrationFormForEditing()
        {
            int[] selectedRowHandle = gridViewManager.GetSelectedRows();
            object code = m_ListManagerType == ListManagerType.BankAccount ? gridViewManager.GetRowCellValue(selectedRowHandle[0], "MainCsAccount") : (string)gridViewManager.GetRowCellValue(selectedRowHandle[0], "Code");
            if (m_ListManagerType == ListManagerType.Store)
            {
                ShowStoreInfoForEditing((string)code);
            }
             
            else if(m_ListManagerType==ListManagerType.CashAccount)
            {
                ShowCashAccountInformationForEditing((string)code);
            }
            else
            {
                ShowBankAccountInformationForEditing((int)code);
            }
        }

        private void gridManager_ProcessGridKey(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ShowRegistrationFormForEditing();
            }
        }

        private void ApplyFormatConditioning()
        {
            DevExpress.XtraGrid.StyleFormatCondition condition = new DevExpress.XtraGrid.StyleFormatCondition();
            condition.Appearance.Options.UseBackColor = true;
            condition.Appearance.BackColor = Color.FromArgb(0x8F, 0xF2, 0x14, 0x43);
            condition.Condition = FormatConditionEnum.Expression;
            condition.Expression = "[Activated] != True";
            condition.ApplyToRow = true;
            gridViewManager.FormatConditions.Add(condition);
        }

        private void menNewCat_Click(object sender, EventArgs e)
        {
            CashAccountCategory cat = cashCatTree.SelectedNode.Tag as CashAccountCategory;
            CashCategoryForm f=new CashCategoryForm(cat.id);
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                cashCatTree.reload();
                cashCatTree.selectNode(f.newCat.id);
            }
        }

        private void menEditCat_Click(object sender, EventArgs e)
        {
            CashAccountCategory cat = cashCatTree.SelectedNode.Tag as CashAccountCategory;
            CashCategoryForm f = new CashCategoryForm(cat);
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                cashCatTree.reload();
                cashCatTree.selectNode(f.newCat.id);
            }
        }

        private void menDelCat_Click(object sender, EventArgs e)
        {
            CashAccountCategory cat = cashCatTree.SelectedNode.Tag as CashAccountCategory;
            try
            {
                iERPTransactionClient.deleteCashCategory(cat.id);
                cashCatTree.selectNode(cat.id);
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void contextCategory_Opening(object sender, CancelEventArgs e)
        {
            if (cashCatTree.SelectedNode == null)
            {
                menNewCat.Enabled = false;
                menDelCat.Enabled = false;
                menEditCat.Enabled = false;
            }
            else
            {
                menNewCat.Enabled = true;
                menDelCat.Enabled = true;
                menEditCat.Enabled = true;
            }
            
        }

        private void menNewRootCat_Click(object sender, EventArgs e)
        {
            CashCategoryForm f = new CashCategoryForm(-1);
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                cashCatTree.reload();
                cashCatTree.selectNode(f.newCat.id);
            }
        }

        private void cashCatTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            LoadCashAccounts();
        }

    }
    public enum ListManagerType
    {
        Store,
        CashAccount,
        BankAccount
    }
}