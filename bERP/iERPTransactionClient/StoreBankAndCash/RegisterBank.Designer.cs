﻿namespace BIZNET.iERP.Client
{
    partial class RegisterBank
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule3 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegisterBank));
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule4 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.pickCostCenter = new INTAPS.Accounting.Client.CostCenterPlaceHolder();
            this.txtBankAccName = new DevExpress.XtraEditors.TextEdit();
            this.txtBankAccNumber = new DevExpress.XtraEditors.TextEdit();
            this.txtBranchName = new DevExpress.XtraEditors.TextEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridAccountInfo = new DevExpress.XtraGrid.GridControl();
            this.gridViewAccountInfo = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.txtBankName = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAccountInfo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBankAccName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.validationBankAccount = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBranchName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAccountInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAccountInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAccountInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBankAccName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationBankAccount)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.pickCostCenter);
            this.layoutControl1.Controls.Add(this.txtBankAccName);
            this.layoutControl1.Controls.Add(this.txtBankAccNumber);
            this.layoutControl1.Controls.Add(this.txtBranchName);
            this.layoutControl1.Controls.Add(this.groupControl1);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.txtBankName);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(602, 142, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(412, 346);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // pickCostCenter
            // 
            this.pickCostCenter.account = null;
            this.pickCostCenter.AllowAdd = false;
            this.pickCostCenter.Location = new System.Drawing.Point(121, 8);
            this.pickCostCenter.Name = "pickCostCenter";
            this.pickCostCenter.OnlyLeafAccount = false;
            this.pickCostCenter.Size = new System.Drawing.Size(283, 20);
            this.pickCostCenter.TabIndex = 17;
            // 
            // txtBankAccName
            // 
            this.txtBankAccName.Location = new System.Drawing.Point(121, 128);
            this.txtBankAccName.Name = "txtBankAccName";
            this.txtBankAccName.Size = new System.Drawing.Size(283, 20);
            this.txtBankAccName.StyleController = this.layoutControl1;
            this.txtBankAccName.TabIndex = 16;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "Bank account name cannot be empty";
            this.validationBankAccount.SetValidationRule(this.txtBankAccName, conditionValidationRule1);
            // 
            // txtBankAccNumber
            // 
            this.txtBankAccNumber.Location = new System.Drawing.Point(121, 98);
            this.txtBankAccNumber.Name = "txtBankAccNumber";
            this.txtBankAccNumber.Size = new System.Drawing.Size(283, 20);
            this.txtBankAccNumber.StyleController = this.layoutControl1;
            this.txtBankAccNumber.TabIndex = 15;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "Bank account number cannot be empty";
            this.validationBankAccount.SetValidationRule(this.txtBankAccNumber, conditionValidationRule2);
            // 
            // txtBranchName
            // 
            this.txtBranchName.Location = new System.Drawing.Point(121, 68);
            this.txtBranchName.Name = "txtBranchName";
            this.txtBranchName.Size = new System.Drawing.Size(283, 20);
            this.txtBranchName.StyleController = this.layoutControl1;
            this.txtBranchName.TabIndex = 14;
            conditionValidationRule3.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule3.ErrorText = "Branch Name cannot be empty";
            this.validationBankAccount.SetValidationRule(this.txtBranchName, conditionValidationRule3);
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.CaptionImage = ((System.Drawing.Image)(resources.GetObject("groupControl1.CaptionImage")));
            this.groupControl1.Controls.Add(this.gridAccountInfo);
            this.groupControl1.Location = new System.Drawing.Point(5, 155);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(402, 149);
            this.groupControl1.TabIndex = 13;
            this.groupControl1.Text = "Account Information";
            // 
            // gridAccountInfo
            // 
            this.gridAccountInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridAccountInfo.Location = new System.Drawing.Point(2, 32);
            this.gridAccountInfo.MainView = this.gridViewAccountInfo;
            this.gridAccountInfo.Name = "gridAccountInfo";
            this.gridAccountInfo.Size = new System.Drawing.Size(398, 115);
            this.gridAccountInfo.TabIndex = 13;
            this.gridAccountInfo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAccountInfo});
            // 
            // gridViewAccountInfo
            // 
            this.gridViewAccountInfo.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewAccountInfo.Appearance.GroupPanel.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.gridViewAccountInfo.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewAccountInfo.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewAccountInfo.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewAccountInfo.Appearance.GroupRow.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.gridViewAccountInfo.Appearance.GroupRow.Options.UseFont = true;
            this.gridViewAccountInfo.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridViewAccountInfo.GridControl = this.gridAccountInfo;
            this.gridViewAccountInfo.GroupPanelText = "Account Information";
            this.gridViewAccountInfo.Name = "gridViewAccountInfo";
            this.gridViewAccountInfo.OptionsBehavior.Editable = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowFilter = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowGroup = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowSort = false;
            this.gridViewAccountInfo.OptionsFind.FindDelay = 100;
            this.gridViewAccountInfo.OptionsFind.FindMode = DevExpress.XtraEditors.FindMode.Always;
            this.gridViewAccountInfo.OptionsMenu.EnableColumnMenu = false;
            this.gridViewAccountInfo.OptionsMenu.EnableFooterMenu = false;
            this.gridViewAccountInfo.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewAccountInfo.OptionsView.ShowGroupPanel = false;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnCancel);
            this.panelControl1.Controls.Add(this.btnOk);
            this.panelControl1.Location = new System.Drawing.Point(5, 308);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(402, 33);
            this.panelControl1.TabIndex = 11;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(320, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Close";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(239, 4);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "&Ok";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // txtBankName
            // 
            this.txtBankName.Location = new System.Drawing.Point(121, 38);
            this.txtBankName.Name = "txtBankName";
            this.txtBankName.Properties.Mask.EditMask = "[0-9a-zA-Z].*";
            this.txtBankName.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtBankName.Size = new System.Drawing.Size(283, 20);
            this.txtBankName.StyleController = this.layoutControl1;
            this.txtBankName.TabIndex = 6;
            conditionValidationRule4.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule4.ErrorText = "Bank name cannot be empty";
            this.validationBankAccount.SetValidationRule(this.txtBankName, conditionValidationRule4);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutAccountInfo,
            this.layoutControlItem3,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutBankAccName,
            this.layoutControlItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(412, 346);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.panelControl1;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 303);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(406, 37);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutAccountInfo
            // 
            this.layoutAccountInfo.Control = this.groupControl1;
            this.layoutAccountInfo.CustomizationFormText = "layoutAccountInfo";
            this.layoutAccountInfo.Location = new System.Drawing.Point(0, 150);
            this.layoutAccountInfo.MaxSize = new System.Drawing.Size(0, 153);
            this.layoutAccountInfo.MinSize = new System.Drawing.Size(104, 153);
            this.layoutAccountInfo.Name = "layoutAccountInfo";
            this.layoutAccountInfo.Size = new System.Drawing.Size(406, 153);
            this.layoutAccountInfo.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutAccountInfo.Text = "layoutAccountInfo";
            this.layoutAccountInfo.TextSize = new System.Drawing.Size(0, 0);
            this.layoutAccountInfo.TextToControlDistance = 0;
            this.layoutAccountInfo.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtBankName;
            this.layoutControlItem3.CustomizationFormText = "Bank Name:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(406, 30);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "Bank Name:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(109, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtBranchName;
            this.layoutControlItem1.CustomizationFormText = "Branch Name:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(406, 30);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Text = "Branch Name:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(109, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtBankAccNumber;
            this.layoutControlItem2.CustomizationFormText = "Bank Account Number:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(406, 30);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem2.Text = "Bank Account Number:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(109, 13);
            // 
            // layoutBankAccName
            // 
            this.layoutBankAccName.Control = this.txtBankAccName;
            this.layoutBankAccName.CustomizationFormText = "Bank Account Name:";
            this.layoutBankAccName.Location = new System.Drawing.Point(0, 120);
            this.layoutBankAccName.Name = "layoutBankAccName";
            this.layoutBankAccName.Size = new System.Drawing.Size(406, 30);
            this.layoutBankAccName.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutBankAccName.Text = "Bank Account Name:";
            this.layoutBankAccName.TextSize = new System.Drawing.Size(109, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.pickCostCenter;
            this.layoutControlItem5.CustomizationFormText = "Cost Center:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(406, 30);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem5.Text = "Cost Center:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(109, 13);
            // 
            // validationBankAccount
            // 
            this.validationBankAccount.ValidateHiddenControls = false;
            // 
            // RegisterBank
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(412, 346);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "RegisterBank";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Register Bank";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RegisterBank_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBankAccNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBranchName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridAccountInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAccountInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtBankName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAccountInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBankAccName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationBankAccount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit txtBankName;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validationBankAccount;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutAccountInfo;
        private DevExpress.XtraGrid.GridControl gridAccountInfo;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewAccountInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit txtBankAccName;
        private DevExpress.XtraEditors.TextEdit txtBankAccNumber;
        private DevExpress.XtraEditors.TextEdit txtBranchName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutBankAccName;
        private INTAPS.Accounting.Client.CostCenterPlaceHolder pickCostCenter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
    }
}