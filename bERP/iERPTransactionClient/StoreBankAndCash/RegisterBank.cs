﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.IO;
using System.Drawing.Imaging;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraGrid;
using DevExpress.Utils;
using INTAPS.UI.ButtonGrid;

namespace BIZNET.iERP.Client
{
    public partial class RegisterBank : XtraForm
    {
        private int m_MainCsAccountID = -1;
        private DataTable accountInfoTable;
        private BankAccountInfo m_BankAccount;
        public delegate void BankAccountAddedHandler(int mainCsAccount);
        public event BankAccountAddedHandler BankAccountAdded;
        public int m_CostCenter;
        private bool m_CreateUnderDivsion;
        private bool m_Flag = false;
        public RegisterBank()
        {
            InitializeComponent();
            m_Flag = true;
            Size = new Size(392, 221);
            layoutAccountInfo.HideToCustomization();
            txtBankName.LostFocus += Control_LostFocus;
            txtBankAccName.LostFocus += Control_LostFocus;
            txtBankAccNumber.LostFocus += Control_LostFocus;
        }
        public RegisterBank(int projectCostCenterID, bool createUnderDivision)
            : this()
        {
            m_Flag = false;
            pickCostCenter.Enabled = false;
            m_CreateUnderDivsion = createUnderDivision;
            pickCostCenter.SetByID(projectCostCenterID);
        }
        public RegisterBank(int mainCsAccount)
            : this()
        {
            m_Flag = true;
            m_MainCsAccountID = mainCsAccount;
            pickCostCenter.Enabled = false;
            Size = new Size(392, 371);
            accountInfoTable = new DataTable();
            PrepareAccountInfoTable();
            BankAccountInfo bankAccount = iERPTransactionClient.GetBankAccount(m_MainCsAccountID);
            if (bankAccount != null)
            {
                m_BankAccount = bankAccount;
                m_CostCenter = AccountingClient.GetCostCenterAccount(bankAccount.mainCsAccount).costCenterID;
                LoadBankAccountForUpdate(m_BankAccount);
                layoutAccountInfo.RestoreFromCustomization(layoutBankAccName, InsertType.Bottom);
                DisplayBankAccountInformation(m_BankAccount);
            }
        }

        private void PrepareAccountInfoTable()
        {
            accountInfoTable.Columns.Add("Account Code", typeof(string));
            accountInfoTable.Columns.Add("Name", typeof(string));
            accountInfoTable.Columns.Add("Account Type", typeof(string));
            accountInfoTable.Columns.Add("Balance", typeof(double));
            accountInfoTable.Columns.Add("Activated", typeof(bool));
            gridAccountInfo.DataSource = accountInfoTable;
            gridViewAccountInfo.Columns["Activated"].Visible = false;
            gridViewAccountInfo.Columns["Name"].Visible = false;
            gridViewAccountInfo.Columns["Balance"].DisplayFormat.FormatType = FormatType.Numeric;
            gridViewAccountInfo.Columns["Balance"].DisplayFormat.FormatString = "N";
        }



        private void LoadBankAccountForUpdate(BankAccountInfo bankAccount)
        {
            txtBankName.Text = bankAccount.bankName;
            txtBranchName.Text = bankAccount.branchName;
            txtBankAccName.Text = bankAccount.bankAccountName;
            txtBankAccNumber.Text = bankAccount.bankAccountNumber;
            pickCostCenter.SetByID(m_CostCenter);
        }
        private void DisplayBankAccountInformation(BankAccountInfo bankAccount)
        {
            accountInfoTable.Clear();

            DataRow mainBankAccountRow = accountInfoTable.NewRow();
            DataRow bankServiceChargeAccountRow = accountInfoTable.NewRow();
            CostCenterAccount mainBankaCsAccount = INTAPS.Accounting.Client.AccountingClient.GetCostCenterAccount(bankAccount.mainCsAccount);
            CostCenterAccount bankServiceChargeCsAccount = INTAPS.Accounting.Client.AccountingClient.GetCostCenterAccount(bankAccount.bankServiceChargeCsAccountID);
            #region Main Bank Account
            Account mainAccount=AccountingClient.GetAccount<Account>(mainBankaCsAccount.accountID);
            mainBankAccountRow[0] = mainAccount.Code;
            mainBankAccountRow[1] = mainAccount.Name;
            mainBankAccountRow[2] = "Main Bank Account";
            mainBankAccountRow[3] = INTAPS.Accounting.Client.AccountingClient.GetNetCostCenterAccountBalanceAsOf(m_CostCenter, mainBankaCsAccount.accountID, DateTime.Now);
            mainBankAccountRow[4] = mainAccount.Status == AccountStatus.Activated ? true : false;
            #endregion

            #region Bank Service Charge Account
            Account bankServiceCharge = AccountingClient.GetAccount<Account>(bankServiceChargeCsAccount.accountID);
            bankServiceChargeAccountRow[0] = bankServiceCharge.Code;
            bankServiceChargeAccountRow[1] = bankServiceCharge.Name;
            bankServiceChargeAccountRow[2] = "Bank Service Charge Account";
            bankServiceChargeAccountRow[3] = INTAPS.Accounting.Client.AccountingClient.GetNetCostCenterAccountBalanceAsOf(m_CostCenter, bankServiceChargeCsAccount.accountID, DateTime.Now);
            bankServiceChargeAccountRow[4] = bankServiceCharge.Status == AccountStatus.Activated ? true : false;
            #endregion


            accountInfoTable.Rows.Add(mainBankAccountRow);
            accountInfoTable.Rows.Add(bankServiceChargeAccountRow);

            gridAccountInfo.DataSource = accountInfoTable;
            gridAccountInfo.RefreshDataSource();
            gridViewAccountInfo.ExpandAllGroups();
            gridViewAccountInfo.BestFitColumns();

            ApplyAccountInfoFormatConditioning();
        }

        private void ApplyAccountInfoFormatConditioning()
        {
            StyleFormatCondition condition = new StyleFormatCondition();
            condition.Appearance.Options.UseBackColor = true;
            condition.Appearance.BackColor = Color.FromArgb(0x8F, 0xF2, 0x14, 0x43);
            condition.Condition = FormatConditionEnum.Expression;
            condition.Expression = "[Activated] != True";
            condition.ApplyToRow = true;
            gridViewAccountInfo.FormatConditions.Add(condition);
        }

        void Control_LostFocus(object sender, EventArgs e)
        {
            validationBankAccount.Validate((Control)sender);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {

                if (pickCostCenter.account == null)
                {
                    MessageBox.ShowErrorMessage("Please pick cost center and try again!");
                    return;
                }
                if (!validationBankAccount.Validate())
                {
                    MessageBox.ShowErrorMessage("Please check the errors marked in red and try again!");
                    return;
                }
                if (pickCostCenter.account.childCount > 1)
                {
                    MessageBox.ShowErrorMessage("Pick a cost center with no child");
                    return;
                }
                BankAccountInfo bankAccount = new BankAccountInfo();
                if (m_BankAccount != null)
                    bankAccount = m_BankAccount;
                bankAccount.mainCsAccount = m_MainCsAccountID;
                bankAccount.bankName = txtBankName.Text;
                bankAccount.branchName = txtBranchName.Text;
                bankAccount.bankAccountName = txtBankAccName.Text;
                bankAccount.bankAccountNumber = txtBankAccNumber.Text;

                CostCenter costCenter = AccountingClient.GetAccount<CostCenter>(pickCostCenter.GetAccountID());
                int projectCostCenterID = -1;
                if (!m_Flag)
                    projectCostCenterID = m_CreateUnderDivsion ? costCenter.PID : costCenter.id;
                else
                    projectCostCenterID = iERPTransactionClient.GetProjectInfo(costCenter.id) != null ? costCenter.id : costCenter.PID;
                Project project = iERPTransactionClient.GetProjectInfo(projectCostCenterID);
                int mainCsAccount = -1;
                if (project != null)
                {
                    mainCsAccount = iERPTransactionClient.AddBankAccountToProject(bankAccount, project.code, pickCostCenter.GetAccountID());
                }
                else
                {
                    mainCsAccount = iERPTransactionClient.CreateBankAccount(bankAccount,pickCostCenter.GetAccountID());
                }
                if (mainCsAccount != -1)
                {
                    string message = m_MainCsAccountID == -1 ? "Bank successfully saved!" : "Bank successfully updated";
                    MessageBox.ShowSuccessMessage(message);
                    if (bankAccount.mainCsAccount == -1)
                    {
                        if (BankAccountAdded != null)
                            BankAccountAdded(mainCsAccount);
                        ResetControls();
                    }
                    else
                        Close();
                }
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error occurred while trying to create bank\n" + ex.Message);
            }
        }
        private void ResetControls()
        {
            txtBankName.Text = "";
            txtBankAccNumber.Text = "";
            txtBankAccName.Text = "";
            txtBranchName.Text = "";
        }

        private void RegisterBank_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult != DialogResult.OK)
                this.DialogResult = DialogResult.Cancel;
        }
    }
}