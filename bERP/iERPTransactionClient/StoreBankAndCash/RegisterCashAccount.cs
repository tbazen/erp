﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.IO;
using System.Drawing.Imaging;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraGrid;
using DevExpress.Utils;
using INTAPS.UI.ButtonGrid;

namespace BIZNET.iERP.Client
{
    public partial class RegisterCashAccount : XtraForm
    {
        private string m_CashAccountCode = null;
        private DataTable accountInfoTable;
        private CashAccount m_CashAccount;
        public delegate void CashAccountAddedHandler(string code);
        public event CashAccountAddedHandler CashAccountAdded;
        public int m_CostCenter;
        private bool m_CreateUnderDivision;
        private bool m_Flag = false;
        int catID;
        void setCatID(int catID)
        {
            this.catID = catID;
            labelCategory.Text = "Category: " + iERPTransactionClient.getCashCategory(catID).name;
        }
        public RegisterCashAccount(int catID)
        {
            InitializeComponent();
            m_Flag = true;
            layoutAccountInfo.HideToCustomization();
            txtAccName.LostFocus += Control_LostFocus;
            setCatID(catID);
        }
        public RegisterCashAccount(int catID, int projectCostCenterID, bool createUnderDivision)
            :this(catID)
        {
            pickCostCenter.Enabled = false;
            m_Flag = false;
            m_CreateUnderDivision = createUnderDivision;
            pickCostCenter.SetByID(projectCostCenterID);
        }
        public RegisterCashAccount(CashAccount cashAccount)
            : this(cashAccount.categoryID)
        {
            m_CashAccountCode = cashAccount.code;
            m_Flag = true;
            pickCostCenter.Enabled = false;
            accountInfoTable = new DataTable();
            PrepareAccountInfoTable();
            m_CashAccount = cashAccount;
            m_CostCenter = AccountingClient.GetCostCenterAccount(cashAccount.csAccountID).costCenterID;
            LoadCashAccountForUpdate(m_CashAccount);
            layoutAccountInfo.RestoreFromCustomization(layoutcashName, InsertType.Bottom);
            DisplayCashAccountInformation(m_CashAccount);
        }

        private void PrepareAccountInfoTable()
        {
            accountInfoTable.Columns.Add("Account Code", typeof(string));
            accountInfoTable.Columns.Add("Name", typeof(string));
            accountInfoTable.Columns.Add("Account Type", typeof(string));
            accountInfoTable.Columns.Add("Balance", typeof(double));
            accountInfoTable.Columns.Add("Activated", typeof(bool));
            gridAccountInfo.DataSource = accountInfoTable;
            gridViewAccountInfo.Columns["Activated"].Visible = false;
            gridViewAccountInfo.Columns["Name"].Visible = false;
            gridViewAccountInfo.Columns["Balance"].DisplayFormat.FormatType = FormatType.Numeric;
            gridViewAccountInfo.Columns["Balance"].DisplayFormat.FormatString = "N";
        }



        private void LoadCashAccountForUpdate(CashAccount cashAccount)
        {
            txtAccName.Text = cashAccount.name;
            pickCostCenter.SetByID(m_CostCenter);
            pickerEmployee.SetByID(cashAccount.employeeID);
            chkIsPettyCash.Checked = cashAccount.pettyCash;
        }
        private void DisplayCashAccountInformation(CashAccount cashAccount)
        {
            accountInfoTable.Clear();

            DataRow cashAccountRow = accountInfoTable.NewRow();
            CostCenterAccount CashCsAccount = INTAPS.Accounting.Client.AccountingClient.GetCostCenterAccount(cashAccount.csAccountID);
            #region Cash Account
            Account mainCashAccount=AccountingClient.GetAccount<Account>(CashCsAccount.accountID);
            cashAccountRow[0] = mainCashAccount.Code;
            cashAccountRow[1] = mainCashAccount.Name;
            cashAccountRow[2] = "Cash Account";
            cashAccountRow[3] = INTAPS.Accounting.Client.AccountingClient.GetNetCostCenterAccountBalanceAsOf(m_CostCenter, CashCsAccount.accountID, DateTime.Now);
            cashAccountRow[4] = mainCashAccount.Status == AccountStatus.Activated ? true : false;
            #endregion
            accountInfoTable.Rows.Add(cashAccountRow);

            gridAccountInfo.DataSource = accountInfoTable;
            gridAccountInfo.RefreshDataSource();
            gridViewAccountInfo.ExpandAllGroups();
            gridViewAccountInfo.BestFitColumns();

            ApplyAccountInfoFormatConditioning();
        }

        private void ApplyAccountInfoFormatConditioning()
        {
            StyleFormatCondition condition = new StyleFormatCondition();
            condition.Appearance.Options.UseBackColor = true;
            condition.Appearance.BackColor = Color.FromArgb(0x8F, 0xF2, 0x14, 0x43);
            condition.Condition = FormatConditionEnum.Expression;
            condition.Expression = "[Activated] != True";
            condition.ApplyToRow = true;
            gridViewAccountInfo.FormatConditions.Add(condition);
        }

        void Control_LostFocus(object sender, EventArgs e)
        {
            validationCashAccount.Validate((Control)sender);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {

                if (pickCostCenter.account == null)
                {
                    MessageBox.ShowErrorMessage("Please pick cost center and try again!");
                    return;
                }
                if (!validationCashAccount.Validate())
                {
                    MessageBox.ShowErrorMessage("Please check the errors marked in red and try again!");
                    return;
                }
                if (pickCostCenter.account.childCount > 1)
                {
                    MessageBox.ShowErrorMessage("Pick a cost center with no child");
                    return;
                }
                if (pickerEmployee.GetEmployeeID() == -1)
                {
                    MessageBox.ShowErrorMessage("Please pick employee to associate with cash account and try again");
                    return;
                }
                CashAccount cashAccount = new CashAccount();
                if (m_CashAccount != null)
                    cashAccount = m_CashAccount;
                cashAccount.categoryID = catID;
                cashAccount.code = m_CashAccountCode;
                cashAccount.name = txtAccName.Text;
                cashAccount.employeeID = pickerEmployee.GetEmployeeID();
                cashAccount.pettyCash = chkIsPettyCash.Checked;
                CostCenter costCenter = AccountingClient.GetAccount<CostCenter>(pickCostCenter.GetAccountID());
                int projectCostCenterID = -1;
                if (!m_Flag)
                    projectCostCenterID = m_CreateUnderDivision ? costCenter.PID : costCenter.id;
                else
                    projectCostCenterID = iERPTransactionClient.GetProjectInfo(costCenter.id) != null ? costCenter.id : costCenter.PID;
                Project project = iERPTransactionClient.GetProjectInfo(projectCostCenterID);
                string cashCode=null;
                if (project != null)
                {
                    cashCode = iERPTransactionClient.AddCashAccountToProject(cashAccount, project.code, pickCostCenter.GetAccountID());
                }
                else
                {
                    cashCode = iERPTransactionClient.CreateCashAccount(cashAccount, costCenter.id);
                }
                if(cashCode!=null)
                {
                    string message = m_CashAccountCode == null ? "Cash successfully saved!" : "Cash successfully updated";
                    MessageBox.ShowSuccessMessage(message);
                    if (cashAccount.code == null)
                    {
                        if (CashAccountAdded != null)
                            CashAccountAdded(cashCode);
                        ResetControls();
                    }
                    else
                        Close();
                }
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error occurred while trying to create cash\n" + ex.Message);
            }

            
        }
        private void ResetControls()
        {
            txtAccName.Text = "";
        }

        private void RegisterCashAccount_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult != DialogResult.OK)
                this.DialogResult = DialogResult.Cancel;
        }

        private void layoutControl1_LayoutUpdate(object sender, EventArgs e)
        {
            try
            {
                if (layoutControl1.Root.MinSize != null)
                    this.Size = new Size(Size.Width, layoutControl1.Root.MinSize.Height + 50);
            }
            catch { }
        }
    }
}