﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.IO;
using System.Drawing.Imaging;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraGrid;
using DevExpress.Utils;
using INTAPS.UI.ButtonGrid;

namespace BIZNET.iERP.Client
{
    public partial class RegisterStore : XtraForm
    {
        private string m_StoreCode;
        private int m_CostCenterID;
        private DataTable accountInfoTable;
        private StoreInfo m_StoreInfo;
        public delegate void StoreAddedHandler(string code);
        public event StoreAddedHandler StoreAdded;
        public RegisterStore()
        {
            InitializeComponent();
            txtName.LostFocus += Control_LostFocus;
        }
        public RegisterStore(int projectCostCenterID)
            : this()
        {
            costCenterPicker.Enabled = false;
            costCenterPicker.SetByID(projectCostCenterID);
        }
        public RegisterStore(string code)
            : this()
        {
            m_StoreCode = code;
            costCenterPicker.Enabled = false;
            StoreInfo store = iERPTransactionClient.GetStoreInfo(m_StoreCode);
            if (store != null)
            {
                m_StoreInfo = store;
                m_CostCenterID = m_StoreInfo.costCenterID;
                LoadStoreForUpdate(m_StoreInfo);
            }
        }

        private void LoadStoreForUpdate(StoreInfo storeInfo)
        {
            txtName.Text = storeInfo.description;
            int costCenterParentID = AccountingClient.GetAccount<CostCenter>(storeInfo.costCenterID).PID;
            costCenterPicker.SetByID(costCenterParentID);
        }


        void Control_LostFocus(object sender, EventArgs e)
        {
            validationStore.Validate((Control)sender);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (costCenterPicker.account == null)
                {
                    MessageBox.ShowErrorMessage("Please pick cost center for the store and try again!");
                    return;
                }
                if (!validationStore.Validate())
                {
                    MessageBox.ShowErrorMessage("Please check the errors marked in red and try again!");
                    return;
                }
                StoreInfo store = new StoreInfo();
                if (m_StoreInfo != null)
                    store = m_StoreInfo;
                store.code = m_StoreCode;
                store.description = txtName.Text;
                store.costCenterID = m_CostCenterID;
                Project project = iERPTransactionClient.GetProjectInfo(costCenterPicker.GetAccountID());
                string code = null;
                if (project != null)
                {
                    code = iERPTransactionClient.AddStoreToProject(store, project.code);
                }
                else
                    code = iERPTransactionClient.RegisterStore(store, costCenterPicker.GetAccountID());
                if (code != null)
                {
                    string message = m_StoreCode == null ? "Store successfully saved!" : "Store successfully updated";
                    MessageBox.ShowSuccessMessage(message);
                    if (store.code == null)
                    {
                        if (StoreAdded != null)
                            StoreAdded(code);
                        ResetControls();
                    }
                    else
                        Close();
                }
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error occurred while trying to create store\n" + ex.Message);
            }

            
        }
        private void ResetControls()
        {
            txtName.Text = "";
            costCenterPicker.account = null;
        }

        private void RegisterStore_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult != DialogResult.OK)
                this.DialogResult = DialogResult.Cancel;
        }

    }
}