using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;

namespace BIZNET.iERP.Client
{

    class DCHTaxDeclarationBase : bERPClientDocumentHandler
    {
        public DCHTaxDeclarationBase(Type formType)
            : base(formType)
        {

        }
        public override void SetEditorDocument(object editor, INTAPS.Accounting.AccountDocument doc)
        {
            TaxDeclarationFormBase f = (TaxDeclarationFormBase)editor;
            f.LoadData((TaxDeclarationDocumentBase)doc);
        }
    }

    class DCHIncomeTaxDeclaration : DCHTaxDeclarationBase
    {
        public DCHIncomeTaxDeclaration()
            : base(typeof(IncomeTaxDeclarationForm))
        {

        }
    }
    class DCHPensionTaxDeclaration : DCHTaxDeclarationBase
    {
        public DCHPensionTaxDeclaration()
            : base(typeof(PensionTaxDeclarationForm))
        {

        }
    }
    class DCHWithholdingTaxDeclaration : DCHTaxDeclarationBase
    {
        public DCHWithholdingTaxDeclaration()
            : base(typeof(WithholdingTaxDeclarationForm))
        {

        }
    }
    class DCHVATWithholdingDeclaration : DCHTaxDeclarationBase
    {
        public DCHVATWithholdingDeclaration()
            : base(typeof(VATWithholdingDeclarationForm))
        {

        }
    }
    class DCHVATDeclaration : DCHTaxDeclarationBase
    {
        public DCHVATDeclaration()
            : base(typeof(VATDeclarationForm))
        {

        }
    }
}
