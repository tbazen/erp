﻿namespace BIZNET.iERP.Client
{
    partial class TaxDeclarationFormBase 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.voucher = new BIZNET.iERP.Client.DocumentSerialPlaceHolder();
            this.labelCarryForwardCredit = new DevExpress.XtraEditors.LabelControl();
            this.labelAccumulatedCredit = new DevExpress.XtraEditors.LabelControl();
            this.labelCurrentCredit = new DevExpress.XtraEditors.LabelControl();
            this.cmbBankAccount = new BIZNET.iERP.Client.BankAccountPlaceholder();
            this.cmbCashAccount = new BIZNET.iERP.Client.CashAccountPlaceholder();
            this.comboPaymentType = new BIZNET.iERP.Client.PaymentTypeSelector();
            this.datePayment = new BIZNET.iERP.Client.BNDualCalendar();
            this.txtReceiptNumber = new DevExpress.XtraEditors.TextEdit();
            this.labelTotal = new DevExpress.XtraEditors.LabelControl();
            this.txtExemption = new DevExpress.XtraEditors.TextEdit();
            this.txtOtherPenality = new DevExpress.XtraEditors.TextEdit();
            this.txtInterest = new DevExpress.XtraEditors.TextEdit();
            this.txtDocumentNumber = new DevExpress.XtraEditors.TextEdit();
            this.txtServiceCharge = new DevExpress.XtraEditors.TextEdit();
            this.labelDeclaredAmount = new DevExpress.XtraEditors.LabelControl();
            this.labelBankBalance = new DevExpress.XtraEditors.LabelControl();
            this.labelCashBalance = new DevExpress.XtraEditors.LabelControl();
            this.txtNote = new DevExpress.XtraEditors.MemoEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.txtReference = new DevExpress.XtraEditors.TextEdit();
            this.txtPenality = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutPenality = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlNote = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlBankBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlCashBalance = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutDeclaredAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutInterest = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutOtherPenality = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutExemptions = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlSlipRef = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlServiceCharge = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlPaymentMethod = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlCashAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlBankAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutTotalCredit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutAccumulatedCredit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutCreditForward = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlVoucher = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.validationTaxDeclaration = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBankAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCashAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboPaymentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiptNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExemption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOtherPenality.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInterest.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDocumentNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceCharge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPenality.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPenality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDeclaredAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOtherPenality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExemptions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSlipRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlServiceCharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPaymentMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTotalCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAccumulatedCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreditForward)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlVoucher)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationTaxDeclaration)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.voucher);
            this.layoutControl1.Controls.Add(this.labelCarryForwardCredit);
            this.layoutControl1.Controls.Add(this.labelAccumulatedCredit);
            this.layoutControl1.Controls.Add(this.labelCurrentCredit);
            this.layoutControl1.Controls.Add(this.cmbBankAccount);
            this.layoutControl1.Controls.Add(this.cmbCashAccount);
            this.layoutControl1.Controls.Add(this.comboPaymentType);
            this.layoutControl1.Controls.Add(this.datePayment);
            this.layoutControl1.Controls.Add(this.txtReceiptNumber);
            this.layoutControl1.Controls.Add(this.labelTotal);
            this.layoutControl1.Controls.Add(this.txtExemption);
            this.layoutControl1.Controls.Add(this.txtOtherPenality);
            this.layoutControl1.Controls.Add(this.txtInterest);
            this.layoutControl1.Controls.Add(this.txtDocumentNumber);
            this.layoutControl1.Controls.Add(this.txtServiceCharge);
            this.layoutControl1.Controls.Add(this.labelDeclaredAmount);
            this.layoutControl1.Controls.Add(this.labelBankBalance);
            this.layoutControl1.Controls.Add(this.labelCashBalance);
            this.layoutControl1.Controls.Add(this.txtNote);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.lblTitle);
            this.layoutControl1.Controls.Add(this.txtReference);
            this.layoutControl1.Controls.Add(this.txtPenality);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(136, 199, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(721, 696);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // voucher
            // 
            this.voucher.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.voucher.Appearance.Options.UseBackColor = true;
            this.voucher.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.voucher.Location = new System.Drawing.Point(7, 82);
            this.voucher.Name = "voucher";
            this.voucher.Size = new System.Drawing.Size(351, 68);
            this.voucher.TabIndex = 27;
            this.voucher.Load += new System.EventHandler(this.voucher_Load);
            // 
            // labelCarryForwardCredit
            // 
            this.labelCarryForwardCredit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCarryForwardCredit.Location = new System.Drawing.Point(196, 450);
            this.labelCarryForwardCredit.Name = "labelCarryForwardCredit";
            this.labelCarryForwardCredit.Size = new System.Drawing.Size(47, 13);
            this.labelCarryForwardCredit.StyleController = this.layoutControl1;
            this.labelCarryForwardCredit.TabIndex = 26;
            this.labelCarryForwardCredit.Text = "0.00 Birr";
            // 
            // labelAccumulatedCredit
            // 
            this.labelAccumulatedCredit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAccumulatedCredit.Location = new System.Drawing.Point(196, 423);
            this.labelAccumulatedCredit.Name = "labelAccumulatedCredit";
            this.labelAccumulatedCredit.Size = new System.Drawing.Size(47, 13);
            this.labelAccumulatedCredit.StyleController = this.layoutControl1;
            this.labelAccumulatedCredit.TabIndex = 25;
            this.labelAccumulatedCredit.Text = "0.00 Birr";
            // 
            // labelCurrentCredit
            // 
            this.labelCurrentCredit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCurrentCredit.Location = new System.Drawing.Point(196, 396);
            this.labelCurrentCredit.Name = "labelCurrentCredit";
            this.labelCurrentCredit.Size = new System.Drawing.Size(47, 13);
            this.labelCurrentCredit.StyleController = this.layoutControl1;
            this.labelCurrentCredit.TabIndex = 24;
            this.labelCurrentCredit.Text = "0.00 Birr";
            // 
            // cmbBankAccount
            // 
            this.cmbBankAccount.Location = new System.Drawing.Point(194, 217);
            this.cmbBankAccount.Name = "cmbBankAccount";
            this.cmbBankAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbBankAccount.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbBankAccount.Size = new System.Drawing.Size(225, 20);
            this.cmbBankAccount.StyleController = this.layoutControl1;
            this.cmbBankAccount.TabIndex = 23;
            // 
            // cmbCashAccount
            // 
            this.cmbCashAccount.IncludeExpenseAdvance = true;
            this.cmbCashAccount.Location = new System.Drawing.Point(194, 187);
            this.cmbCashAccount.Name = "cmbCashAccount";
            this.cmbCashAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCashAccount.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbCashAccount.Size = new System.Drawing.Size(225, 20);
            this.cmbCashAccount.StyleController = this.layoutControl1;
            this.cmbCashAccount.TabIndex = 22;
            // 
            // comboPaymentType
            // 
            this.comboPaymentType.Include_BankTransferFromAccount = true;
            this.comboPaymentType.Include_BankTransferFromCash = true;
            this.comboPaymentType.Include_Cash = true;
            this.comboPaymentType.Include_Check = true;
            this.comboPaymentType.Include_CpoFromBank = true;
            this.comboPaymentType.Include_CpoFromCash = true;
            this.comboPaymentType.Include_Credit = false;
            this.comboPaymentType.Location = new System.Drawing.Point(194, 157);
            this.comboPaymentType.Name = "comboPaymentType";
            this.comboPaymentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboPaymentType.Properties.Items.AddRange(new object[] {
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash",
            "Cash",
            "Check",
            "Bank transfer from bank account",
            "Bank transfer from cash",
            "CPO from bank account",
            "CPO from cash"});
            this.comboPaymentType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboPaymentType.Size = new System.Drawing.Size(517, 20);
            this.comboPaymentType.StyleController = this.layoutControl1;
            this.comboPaymentType.TabIndex = 21;
            // 
            // datePayment
            // 
            this.datePayment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.datePayment.Location = new System.Drawing.Point(365, 85);
            this.datePayment.Name = "datePayment";
            this.datePayment.ShowEthiopian = true;
            this.datePayment.ShowGregorian = true;
            this.datePayment.ShowTime = true;
            this.datePayment.Size = new System.Drawing.Size(346, 63);
            this.datePayment.TabIndex = 20;
            this.datePayment.VerticalLayout = true;
            // 
            // txtReceiptNumber
            // 
            this.txtReceiptNumber.Location = new System.Drawing.Point(194, 277);
            this.txtReceiptNumber.Name = "txtReceiptNumber";
            this.txtReceiptNumber.Size = new System.Drawing.Size(517, 20);
            this.txtReceiptNumber.StyleController = this.layoutControl1;
            this.txtReceiptNumber.TabIndex = 19;
            // 
            // labelTotal
            // 
            this.labelTotal.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelTotal.Location = new System.Drawing.Point(194, 595);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(47, 13);
            this.labelTotal.StyleController = this.layoutControl1;
            this.labelTotal.TabIndex = 18;
            this.labelTotal.Text = "0.00 Birr";
            // 
            // txtExemption
            // 
            this.txtExemption.Location = new System.Drawing.Point(194, 565);
            this.txtExemption.Name = "txtExemption";
            this.txtExemption.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtExemption.Properties.DisplayFormat.FormatString = "#,########0.00;";
            this.txtExemption.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtExemption.Properties.EditFormat.FormatString = "#,########0.00;";
            this.txtExemption.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtExemption.Properties.Mask.EditMask = "#,########0.00;";
            this.txtExemption.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtExemption.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtExemption.Size = new System.Drawing.Size(517, 20);
            this.txtExemption.StyleController = this.layoutControl1;
            this.txtExemption.TabIndex = 17;
            this.txtExemption.EditValueChanged += new System.EventHandler(this.txtExemption_EditValueChanged);
            // 
            // txtOtherPenality
            // 
            this.txtOtherPenality.Location = new System.Drawing.Point(194, 535);
            this.txtOtherPenality.Name = "txtOtherPenality";
            this.txtOtherPenality.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtOtherPenality.Properties.DisplayFormat.FormatString = "#,########0.00;";
            this.txtOtherPenality.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtOtherPenality.Properties.EditFormat.FormatString = "#,########0.00;";
            this.txtOtherPenality.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtOtherPenality.Properties.Mask.EditMask = "#,########0.00;";
            this.txtOtherPenality.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtOtherPenality.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtOtherPenality.Size = new System.Drawing.Size(517, 20);
            this.txtOtherPenality.StyleController = this.layoutControl1;
            this.txtOtherPenality.TabIndex = 16;
            this.txtOtherPenality.EditValueChanged += new System.EventHandler(this.txtOtherPenality_EditValueChanged);
            // 
            // txtInterest
            // 
            this.txtInterest.Location = new System.Drawing.Point(194, 505);
            this.txtInterest.Name = "txtInterest";
            this.txtInterest.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtInterest.Properties.DisplayFormat.FormatString = "#,########0.00;";
            this.txtInterest.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtInterest.Properties.EditFormat.FormatString = "#,########0.00;";
            this.txtInterest.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtInterest.Properties.Mask.EditMask = "#,########0.00;";
            this.txtInterest.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtInterest.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtInterest.Size = new System.Drawing.Size(517, 20);
            this.txtInterest.StyleController = this.layoutControl1;
            this.txtInterest.TabIndex = 15;
            this.txtInterest.EditValueChanged += new System.EventHandler(this.txtInterest_EditValueChanged);
            // 
            // txtDocumentNumber
            // 
            this.txtDocumentNumber.Location = new System.Drawing.Point(194, 247);
            this.txtDocumentNumber.Name = "txtDocumentNumber";
            this.txtDocumentNumber.Properties.Mask.EditMask = "\\w.*";
            this.txtDocumentNumber.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtDocumentNumber.Size = new System.Drawing.Size(517, 20);
            this.txtDocumentNumber.StyleController = this.layoutControl1;
            this.txtDocumentNumber.TabIndex = 14;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "Payment voucher number cannot be blank";
            this.validationTaxDeclaration.SetValidationRule(this.txtDocumentNumber, conditionValidationRule1);
            // 
            // txtServiceCharge
            // 
            this.txtServiceCharge.Location = new System.Drawing.Point(194, 337);
            this.txtServiceCharge.Name = "txtServiceCharge";
            this.txtServiceCharge.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtServiceCharge.Properties.Mask.EditMask = "#,########0.00;";
            this.txtServiceCharge.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtServiceCharge.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtServiceCharge.Size = new System.Drawing.Size(517, 20);
            this.txtServiceCharge.StyleController = this.layoutControl1;
            this.txtServiceCharge.TabIndex = 12;
            // 
            // labelDeclaredAmount
            // 
            this.labelDeclaredAmount.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDeclaredAmount.Location = new System.Drawing.Point(196, 369);
            this.labelDeclaredAmount.Name = "labelDeclaredAmount";
            this.labelDeclaredAmount.Size = new System.Drawing.Size(47, 13);
            this.labelDeclaredAmount.StyleController = this.layoutControl1;
            this.labelDeclaredAmount.TabIndex = 11;
            this.labelDeclaredAmount.Text = "0.00 Birr";
            // 
            // labelBankBalance
            // 
            this.labelBankBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBankBalance.Location = new System.Drawing.Point(426, 214);
            this.labelBankBalance.Name = "labelBankBalance";
            this.labelBankBalance.Size = new System.Drawing.Size(186, 26);
            this.labelBankBalance.StyleController = this.layoutControl1;
            this.labelBankBalance.TabIndex = 8;
            this.labelBankBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // labelCashBalance
            // 
            this.labelCashBalance.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCashBalance.Location = new System.Drawing.Point(426, 184);
            this.labelCashBalance.Name = "labelCashBalance";
            this.labelCashBalance.Size = new System.Drawing.Size(186, 26);
            this.labelCashBalance.StyleController = this.layoutControl1;
            this.labelCashBalance.TabIndex = 7;
            this.labelCashBalance.Text = "Current Balance: 0.00 Birr";
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(194, 618);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(517, 37);
            this.txtNote.StyleController = this.layoutControl1;
            this.txtNote.TabIndex = 6;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.buttonCancel);
            this.panelControl1.Controls.Add(this.buttonOk);
            this.panelControl1.Location = new System.Drawing.Point(7, 662);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(707, 27);
            this.panelControl1.TabIndex = 5;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(643, 3);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(59, 23);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(565, 3);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(62, 23);
            this.buttonOk.TabIndex = 4;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AllowHtmlString = true;
            this.lblTitle.Appearance.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lblTitle.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(112)))));
            this.lblTitle.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblTitle.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lblTitle.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.lblTitle.Location = new System.Drawing.Point(5, 5);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(711, 59);
            this.lblTitle.StyleController = this.layoutControl1;
            this.lblTitle.TabIndex = 13;
            this.lblTitle.Text = "Return of Collected Tax for Period";
            // 
            // txtReference
            // 
            this.txtReference.Location = new System.Drawing.Point(194, 307);
            this.txtReference.Name = "txtReference";
            this.txtReference.Properties.Mask.EditMask = "\\w.*";
            this.txtReference.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtReference.Size = new System.Drawing.Size(517, 20);
            this.txtReference.StyleController = this.layoutControl1;
            this.txtReference.TabIndex = 2;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "Slip Reference cannot be empty";
            this.validationTaxDeclaration.SetValidationRule(this.txtReference, conditionValidationRule2);
            // 
            // txtPenality
            // 
            this.txtPenality.Location = new System.Drawing.Point(194, 475);
            this.txtPenality.Name = "txtPenality";
            this.txtPenality.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtPenality.Properties.DisplayFormat.FormatString = "#,########0.00;";
            this.txtPenality.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtPenality.Properties.EditFormat.FormatString = "#,########0.00;";
            this.txtPenality.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtPenality.Properties.Mask.EditMask = "#,########0.00;";
            this.txtPenality.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPenality.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtPenality.Size = new System.Drawing.Size(517, 20);
            this.txtPenality.StyleController = this.layoutControl1;
            this.txtPenality.TabIndex = 2;
            this.txtPenality.EditValueChanged += new System.EventHandler(this.txtPenality_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutPenality,
            this.layoutControlItem7,
            this.layoutControlNote,
            this.layoutControlBankBalance,
            this.layoutControlCashBalance,
            this.layoutDeclaredAmount,
            this.layoutControlGroup2,
            this.layoutInterest,
            this.layoutOtherPenality,
            this.layoutExemptions,
            this.layoutControlItem5,
            this.layoutControlSlipRef,
            this.layoutControlServiceCharge,
            this.layoutControlItem8,
            this.layoutControlPaymentMethod,
            this.layoutControlCashAccount,
            this.layoutControlBankAccount,
            this.layoutTotalCredit,
            this.layoutAccumulatedCredit,
            this.layoutCreditForward,
            this.layoutControlVoucher,
            this.layoutControlItem6,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(721, 696);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutPenality
            // 
            this.layoutPenality.Control = this.txtPenality;
            this.layoutPenality.CustomizationFormText = "Amount:";
            this.layoutPenality.Location = new System.Drawing.Point(0, 465);
            this.layoutPenality.Name = "layoutPenality";
            this.layoutPenality.Size = new System.Drawing.Size(711, 30);
            this.layoutPenality.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutPenality.Text = "Penailty:";
            this.layoutPenality.TextSize = new System.Drawing.Size(181, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.panelControl1;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 655);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(711, 31);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlNote
            // 
            this.layoutControlNote.Control = this.txtNote;
            this.layoutControlNote.CustomizationFormText = "Note:";
            this.layoutControlNote.Location = new System.Drawing.Point(0, 608);
            this.layoutControlNote.MinSize = new System.Drawing.Size(193, 26);
            this.layoutControlNote.Name = "layoutControlNote";
            this.layoutControlNote.Size = new System.Drawing.Size(711, 47);
            this.layoutControlNote.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlNote.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlNote.Text = "Note:";
            this.layoutControlNote.TextSize = new System.Drawing.Size(181, 13);
            // 
            // layoutControlBankBalance
            // 
            this.layoutControlBankBalance.Control = this.labelBankBalance;
            this.layoutControlBankBalance.CustomizationFormText = "layoutControlItem8";
            this.layoutControlBankBalance.Location = new System.Drawing.Point(419, 207);
            this.layoutControlBankBalance.MaxSize = new System.Drawing.Size(190, 30);
            this.layoutControlBankBalance.MinSize = new System.Drawing.Size(190, 30);
            this.layoutControlBankBalance.Name = "layoutControlBankBalance";
            this.layoutControlBankBalance.Size = new System.Drawing.Size(292, 30);
            this.layoutControlBankBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlBankBalance.Text = "layoutControlItem8";
            this.layoutControlBankBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlBankBalance.TextToControlDistance = 0;
            this.layoutControlBankBalance.TextVisible = false;
            // 
            // layoutControlCashBalance
            // 
            this.layoutControlCashBalance.Control = this.labelCashBalance;
            this.layoutControlCashBalance.CustomizationFormText = "layoutControlCashBalance";
            this.layoutControlCashBalance.Location = new System.Drawing.Point(419, 177);
            this.layoutControlCashBalance.MaxSize = new System.Drawing.Size(190, 30);
            this.layoutControlCashBalance.MinSize = new System.Drawing.Size(190, 30);
            this.layoutControlCashBalance.Name = "layoutControlCashBalance";
            this.layoutControlCashBalance.Size = new System.Drawing.Size(292, 30);
            this.layoutControlCashBalance.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlCashBalance.Text = "layoutControlCashBalance";
            this.layoutControlCashBalance.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlCashBalance.TextToControlDistance = 0;
            this.layoutControlCashBalance.TextVisible = false;
            // 
            // layoutDeclaredAmount
            // 
            this.layoutDeclaredAmount.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutDeclaredAmount.AppearanceItemCaption.Options.UseFont = true;
            this.layoutDeclaredAmount.Control = this.labelDeclaredAmount;
            this.layoutDeclaredAmount.CustomizationFormText = "layoutControlExpenseAdvance";
            this.layoutDeclaredAmount.Location = new System.Drawing.Point(0, 357);
            this.layoutDeclaredAmount.Name = "layoutDeclaredAmount";
            this.layoutDeclaredAmount.Size = new System.Drawing.Size(711, 27);
            this.layoutDeclaredAmount.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutDeclaredAmount.Text = "Declared Amount:";
            this.layoutDeclaredAmount.TextSize = new System.Drawing.Size(181, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(711, 59);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.Color.MidnightBlue;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.Control = this.lblTitle;
            this.layoutControlItem2.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(900, 59);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(596, 59);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem2.Size = new System.Drawing.Size(711, 59);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutInterest
            // 
            this.layoutInterest.Control = this.txtInterest;
            this.layoutInterest.CustomizationFormText = "Interest";
            this.layoutInterest.Location = new System.Drawing.Point(0, 495);
            this.layoutInterest.Name = "layoutInterest";
            this.layoutInterest.Size = new System.Drawing.Size(711, 30);
            this.layoutInterest.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutInterest.Text = "Interest:";
            this.layoutInterest.TextSize = new System.Drawing.Size(181, 13);
            // 
            // layoutOtherPenality
            // 
            this.layoutOtherPenality.Control = this.txtOtherPenality;
            this.layoutOtherPenality.CustomizationFormText = "Other Penality";
            this.layoutOtherPenality.Location = new System.Drawing.Point(0, 525);
            this.layoutOtherPenality.Name = "layoutOtherPenality";
            this.layoutOtherPenality.Size = new System.Drawing.Size(711, 30);
            this.layoutOtherPenality.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutOtherPenality.Text = "Other Penality:";
            this.layoutOtherPenality.TextSize = new System.Drawing.Size(181, 13);
            // 
            // layoutExemptions
            // 
            this.layoutExemptions.Control = this.txtExemption;
            this.layoutExemptions.CustomizationFormText = "Exemptions";
            this.layoutExemptions.Location = new System.Drawing.Point(0, 555);
            this.layoutExemptions.Name = "layoutExemptions";
            this.layoutExemptions.Size = new System.Drawing.Size(711, 30);
            this.layoutExemptions.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutExemptions.Text = "Exemptions:";
            this.layoutExemptions.TextSize = new System.Drawing.Size(181, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.labelTotal;
            this.layoutControlItem5.CustomizationFormText = "Total Payment:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 585);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(711, 23);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem5.Text = "Total Payment:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(181, 13);
            // 
            // layoutControlSlipRef
            // 
            this.layoutControlSlipRef.Control = this.txtReference;
            this.layoutControlSlipRef.CustomizationFormText = "Slip Reference:";
            this.layoutControlSlipRef.Location = new System.Drawing.Point(0, 297);
            this.layoutControlSlipRef.Name = "layoutControlSlipRef";
            this.layoutControlSlipRef.Size = new System.Drawing.Size(711, 30);
            this.layoutControlSlipRef.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlSlipRef.Text = "Slip Reference:";
            this.layoutControlSlipRef.TextSize = new System.Drawing.Size(181, 13);
            // 
            // layoutControlServiceCharge
            // 
            this.layoutControlServiceCharge.Control = this.txtServiceCharge;
            this.layoutControlServiceCharge.CustomizationFormText = "Service Charge:";
            this.layoutControlServiceCharge.Location = new System.Drawing.Point(0, 327);
            this.layoutControlServiceCharge.Name = "layoutControlServiceCharge";
            this.layoutControlServiceCharge.Size = new System.Drawing.Size(711, 30);
            this.layoutControlServiceCharge.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlServiceCharge.Text = "Service Charge:";
            this.layoutControlServiceCharge.TextSize = new System.Drawing.Size(181, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.datePayment;
            this.layoutControlItem8.CustomizationFormText = "Date:";
            this.layoutControlItem8.Location = new System.Drawing.Point(355, 59);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 88);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(191, 88);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(356, 88);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem8.Text = "Date:";
            this.layoutControlItem8.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(181, 13);
            // 
            // layoutControlPaymentMethod
            // 
            this.layoutControlPaymentMethod.Control = this.comboPaymentType;
            this.layoutControlPaymentMethod.CustomizationFormText = "Payment Method";
            this.layoutControlPaymentMethod.Location = new System.Drawing.Point(0, 147);
            this.layoutControlPaymentMethod.Name = "layoutControlPaymentMethod";
            this.layoutControlPaymentMethod.Size = new System.Drawing.Size(711, 30);
            this.layoutControlPaymentMethod.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlPaymentMethod.Text = "Payment Method";
            this.layoutControlPaymentMethod.TextSize = new System.Drawing.Size(181, 13);
            // 
            // layoutControlCashAccount
            // 
            this.layoutControlCashAccount.Control = this.cmbCashAccount;
            this.layoutControlCashAccount.CustomizationFormText = "Payment from Cash Account:";
            this.layoutControlCashAccount.Location = new System.Drawing.Point(0, 177);
            this.layoutControlCashAccount.MaxSize = new System.Drawing.Size(419, 30);
            this.layoutControlCashAccount.MinSize = new System.Drawing.Size(203, 30);
            this.layoutControlCashAccount.Name = "layoutControlCashAccount";
            this.layoutControlCashAccount.Size = new System.Drawing.Size(419, 30);
            this.layoutControlCashAccount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlCashAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlCashAccount.Text = "Payment from Cash Account:";
            this.layoutControlCashAccount.TextSize = new System.Drawing.Size(181, 13);
            // 
            // layoutControlBankAccount
            // 
            this.layoutControlBankAccount.Control = this.cmbBankAccount;
            this.layoutControlBankAccount.CustomizationFormText = "Payment from Bank Account:";
            this.layoutControlBankAccount.Location = new System.Drawing.Point(0, 207);
            this.layoutControlBankAccount.MaxSize = new System.Drawing.Size(419, 30);
            this.layoutControlBankAccount.MinSize = new System.Drawing.Size(204, 30);
            this.layoutControlBankAccount.Name = "layoutControlBankAccount";
            this.layoutControlBankAccount.Size = new System.Drawing.Size(419, 30);
            this.layoutControlBankAccount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlBankAccount.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlBankAccount.Text = "Payment from Bank Account:";
            this.layoutControlBankAccount.TextSize = new System.Drawing.Size(181, 13);
            // 
            // layoutTotalCredit
            // 
            this.layoutTotalCredit.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutTotalCredit.AppearanceItemCaption.Options.UseFont = true;
            this.layoutTotalCredit.Control = this.labelCurrentCredit;
            this.layoutTotalCredit.CustomizationFormText = "Current Total Credit:";
            this.layoutTotalCredit.Location = new System.Drawing.Point(0, 384);
            this.layoutTotalCredit.Name = "layoutTotalCredit";
            this.layoutTotalCredit.Size = new System.Drawing.Size(711, 27);
            this.layoutTotalCredit.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutTotalCredit.Text = "Current Total Credit:";
            this.layoutTotalCredit.TextSize = new System.Drawing.Size(181, 13);
            // 
            // layoutAccumulatedCredit
            // 
            this.layoutAccumulatedCredit.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutAccumulatedCredit.AppearanceItemCaption.Options.UseFont = true;
            this.layoutAccumulatedCredit.Control = this.labelAccumulatedCredit;
            this.layoutAccumulatedCredit.CustomizationFormText = "Accumulated Credit:";
            this.layoutAccumulatedCredit.Location = new System.Drawing.Point(0, 411);
            this.layoutAccumulatedCredit.Name = "layoutAccumulatedCredit";
            this.layoutAccumulatedCredit.Size = new System.Drawing.Size(711, 27);
            this.layoutAccumulatedCredit.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutAccumulatedCredit.Text = "Accumulated Credit:";
            this.layoutAccumulatedCredit.TextSize = new System.Drawing.Size(181, 13);
            // 
            // layoutCreditForward
            // 
            this.layoutCreditForward.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutCreditForward.AppearanceItemCaption.Options.UseFont = true;
            this.layoutCreditForward.Control = this.labelCarryForwardCredit;
            this.layoutCreditForward.CustomizationFormText = "Credit Available For Next Period:";
            this.layoutCreditForward.Location = new System.Drawing.Point(0, 438);
            this.layoutCreditForward.Name = "layoutCreditForward";
            this.layoutCreditForward.Size = new System.Drawing.Size(711, 27);
            this.layoutCreditForward.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutCreditForward.Text = "Credit Available For Next Period:";
            this.layoutCreditForward.TextSize = new System.Drawing.Size(181, 13);
            // 
            // layoutControlVoucher
            // 
            this.layoutControlVoucher.Control = this.txtDocumentNumber;
            this.layoutControlVoucher.CustomizationFormText = "Payment Voucher Number:";
            this.layoutControlVoucher.Location = new System.Drawing.Point(0, 237);
            this.layoutControlVoucher.Name = "layoutControlVoucher";
            this.layoutControlVoucher.Size = new System.Drawing.Size(711, 30);
            this.layoutControlVoucher.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlVoucher.Text = "Document Number:";
            this.layoutControlVoucher.TextSize = new System.Drawing.Size(181, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtReceiptNumber;
            this.layoutControlItem6.CustomizationFormText = "Receipt Number:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 267);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(711, 30);
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem6.Text = "Receipt Number:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(181, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.voucher;
            this.layoutControlItem1.CustomizationFormText = "Document Reference";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 59);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(355, 88);
            this.layoutControlItem1.Text = "Document Reference";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(181, 13);
            // 
            // validationTaxDeclaration
            // 
            this.validationTaxDeclaration.ValidateHiddenControls = false;
            // 
            // TaxDeclarationFormBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 696);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "TaxDeclarationFormBase";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Collect Tax Return";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbBankAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCashAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboPaymentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceiptNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExemption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOtherPenality.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInterest.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDocumentNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceCharge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPenality.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPenality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDeclaredAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutOtherPenality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutExemptions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSlipRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlServiceCharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPaymentMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCashAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlBankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTotalCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutAccumulatedCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutCreditForward)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlVoucher)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationTaxDeclaration)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtPenality;
        private DevExpress.XtraEditors.TextEdit txtReference;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutPenality;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlSlipRef;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.MemoEdit txtNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlNote;
        private DevExpress.XtraEditors.LabelControl labelCashBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCashBalance;
        private DevExpress.XtraEditors.LabelControl labelBankBalance;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlBankBalance;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validationTaxDeclaration;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraEditors.TextEdit txtServiceCharge;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlServiceCharge;
        private DevExpress.XtraEditors.LabelControl lblTitle;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit txtDocumentNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlVoucher;
        private DevExpress.XtraEditors.LabelControl labelDeclaredAmount;
        private DevExpress.XtraLayout.LayoutControlItem layoutDeclaredAmount;
        private DevExpress.XtraEditors.TextEdit txtInterest;
        private DevExpress.XtraLayout.LayoutControlItem layoutInterest;
        private DevExpress.XtraEditors.LabelControl labelTotal;
        private DevExpress.XtraEditors.TextEdit txtExemption;
        private DevExpress.XtraEditors.TextEdit txtOtherPenality;
        private DevExpress.XtraLayout.LayoutControlItem layoutOtherPenality;
        private DevExpress.XtraLayout.LayoutControlItem layoutExemptions;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.TextEdit txtReceiptNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private BIZNET.iERP.Client.BNDualCalendar datePayment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private PaymentTypeSelector comboPaymentType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlPaymentMethod;
        private CashAccountPlaceholder cmbCashAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCashAccount;
        private BankAccountPlaceholder cmbBankAccount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlBankAccount;
        private DevExpress.XtraEditors.LabelControl labelCarryForwardCredit;
        private DevExpress.XtraEditors.LabelControl labelAccumulatedCredit;
        private DevExpress.XtraEditors.LabelControl labelCurrentCredit;
        private DevExpress.XtraLayout.LayoutControlItem layoutTotalCredit;
        private DevExpress.XtraLayout.LayoutControlItem layoutAccumulatedCredit;
        private DevExpress.XtraLayout.LayoutControlItem layoutCreditForward;
        private DocumentSerialPlaceHolder voucher;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
    }
}