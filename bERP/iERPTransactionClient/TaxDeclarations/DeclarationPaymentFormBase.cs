﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;
using DevExpress.XtraEditors.Controls;

namespace BIZNET.iERP.Client
{
    abstract partial class TaxDeclarationFormBase : XtraForm
    {
        //data
        protected bool _newDocument = true;
        protected double _DeclaredAmount;
        protected TaxDeclarationDocumentBase _doc = null;
        protected double _TotalPayment;
        protected int _CurrentTaxDeclarationID;
        protected AccountingPeriod _CurrentTaxDeclarationPeriod;

        //UI logic
        protected IAccountingClient _client;
        protected ActivationParameter _activation;
        protected PaymentMethodAndBalanceController _paymentController;

        public TaxDeclarationFormBase(IAccountingClient client, ActivationParameter activation)
        {
            InitializeComponent();
            _client = client;
            _activation = activation;

            voucher.setKeys(typeof(TaxDeclarationDocumentBase), "voucher");

            _CurrentTaxDeclarationPeriod = _activation.CurrentTaxDeclarationPeriod;
            _CurrentTaxDeclarationID = _activation.CurrentTaxDeclarationID;
            _paymentController = new PaymentMethodAndBalanceController(comboPaymentType, layoutControlBankAccount, cmbBankAccount, layoutControlBankBalance, labelBankBalance, layoutControlCashAccount,cmbCashAccount, layoutControlCashBalance, labelCashBalance, layoutControlServiceCharge, layoutControlSlipRef, datePayment, _activation);
            _paymentController.PaymentInformationChanged += new EventHandler(_paymentMethodController_PaymentInformationChanged);
            SetDeclaredAmount();
            PrepareControlsLayoutBasedOnDocumentType();
            SetFormTitle();
            txtDocumentNumber.LostFocus += Control_LostFocus;
            txtReference.LostFocus += Control_LostFocus;
        }

        protected abstract int relevantAccountID { get; }
        protected abstract void PrepareControlsLayoutBasedOnDocumentType();
        protected abstract TaxDeclarationDocumentBase CreateDocumentInstance();
        protected abstract string getTitle(bool transitive);

        protected virtual void setAdditionalFormData(SupplierAccountDocument customerAccountDocument)
        {

        }
        protected virtual void getAdditionalFormData(SupplierAccountDocument customerAccountDocument)
        {

        }
        protected virtual bool validateData(int assetAccountID)
        {
            return true;
        }
        private void onPaymentInformationChanged()
        {
            SetFormTitle();
        }
        private void SetFormTitle()
        {
            string period = _activation.CurrentTaxDeclarationPeriod != null ? _activation.CurrentTaxDeclarationPeriod.name : "";

            lblTitle.Text = "<size=14><color=#360087><b>" + getTitle(true) + "</b></color></size>" +
                "<size=14><color=#C30013><b>" + period + "</b></color></size>" +
                "<size=14><color=#360087><b>" + GetPaymentSourceDescription() + "</b></color></size>";
        }
        private string GetPaymentSourceDescription()
        {
            if (Account.AmountGreater(_activation.CurrentDeclaredAmount, 0))
                return _paymentController.PaymentSource;
            else
                return string.Empty;
        }
        void _paymentMethodController_PaymentInformationChanged(object sender, EventArgs e)
        {
            onPaymentInformationChanged();
        }
        private void SetDeclaredAmount()
        {
            labelDeclaredAmount.Text = TSConstants.FormatBirr(_activation.CurrentDeclaredAmount) + " Birr";
            _DeclaredAmount = _activation.CurrentDeclaredAmount;
            labelTotal.Text = TSConstants.FormatBirr(_DeclaredAmount) + " Birr"; ;
        }
        private void SetDeclaredAmount(double amount)
        {
            labelDeclaredAmount.Text = TSConstants.FormatBirr(amount) + " Birr";
            _DeclaredAmount = amount;
        }
        void Control_LostFocus(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            validationTaxDeclaration.Validate(control);
        }

        
       
        internal void LoadData(TaxDeclarationDocumentBase taxDocument)
        {
            _doc = taxDocument;
            if (taxDocument == null)
            {
                ResetControls();
            }
            else
            {
                _paymentController.IgnoreEvents();
                _newDocument = false;
                try
                {
                    SetDeclaredAmount(_doc.declaredAmount);
                    if (!_newDocument)
                        SetActivationParameters(_doc);
                    if (Account.AmountGreater(_activation.CurrentDeclaredAmount, 0))
                        _paymentController.PaymentTypeSelector.PaymentMethod = _doc.paymentMethod;
                    _CurrentTaxDeclarationID = _activation.CurrentTaxDeclarationID;
                    _CurrentTaxDeclarationPeriod = _activation.CurrentTaxDeclarationPeriod;
                    PopulateControlsForUpdate(taxDocument);
                }
                finally
                {
                    _paymentController.AcceptEvents();
                }
                SetFormTitle();
            }
        }

        private void SetActivationParameters(TaxDeclarationDocumentBase doc)
        {
            _activation.CurrentTaxDeclarationID = doc.declarationID;
            _activation.CurrentTaxDeclarationPeriod = iERPTransactionClient.GetAccountingPeriod((string)iERPTransactionClient.GetSystemParamter("taxDeclarationPeriods"), doc.declarationPeriodID);
            _activation.CurrentDeclaredAmount = doc.declaredAmount;
        }
        private void ResetControls()
        {
            datePayment.DateTime = DateTime.Now;
           
            if (_doc != null)
                SetDeclaredAmount(_doc.declaredAmount);
            txtReference.Text = "";
            txtNote.Text = "";
            txtDocumentNumber.Text = txtServiceCharge.Text = "";
            txtPenality.Text = txtInterest.Text = txtOtherPenality.Text = txtExemption.Text = "";
            voucher.setReference(-1, null);
        }
        private void PopulateControlsForUpdate(TaxDeclarationDocumentBase taxDocument)
        {
            _paymentController.assetAccountID = taxDocument.assetAccountID;
            datePayment.DateTime = taxDocument.DocumentDate;
            txtDocumentNumber.Text = taxDocument.PaperRef;
            voucher.setReference(taxDocument.AccountDocumentID, taxDocument.voucher);
            txtReceiptNumber.Text = taxDocument.receiptNumber;
            txtPenality.Text = taxDocument.penality.ToString("N");
            txtInterest.Text = taxDocument.interest.ToString("N");
            txtOtherPenality.Text = taxDocument.unaccountedTax.ToString("N");
            txtExemption.Text = taxDocument.deduction.ToString("N");
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(_paymentController.PaymentTypeSelector.PaymentMethod))
                txtServiceCharge.Text = taxDocument.serviceChargeAmount.ToString("N");
            SetReferenceNumber(false);
            txtNote.Text = taxDocument.ShortDescription;
            _CurrentTaxDeclarationID = taxDocument.declarationID;
            _CurrentTaxDeclarationPeriod = iERPTransactionClient.GetAccountingPeriod((string)iERPTransactionClient.GetSystemParamter("taxDeclarationPeriods"), taxDocument.declarationPeriodID);
        }
        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (validationTaxDeclaration.Validate())
                {
                    validateData(_paymentController.assetAccountID);

                    int docID = _doc == null ? -1 : _doc.AccountDocumentID;
                    _doc = CreateDocumentInstance();
                    _doc.AccountDocumentID = docID;
                    _doc.DocumentDate = datePayment.DateTime;
                    _doc.PaperRef = txtDocumentNumber.Text;
                    _doc.voucher = voucher.getReference();
                    _doc.receiptNumber = txtReceiptNumber.Text;
                    _doc.paymentMethod = layoutControlPaymentMethod.IsHidden ? BizNetPaymentMethod.None : _paymentController.PaymentTypeSelector.PaymentMethod;
                    _doc.declaredAmount = _DeclaredAmount;
                    _doc.declarationPeriodID = _CurrentTaxDeclarationPeriod.id;
                    _doc.declarationID = _CurrentTaxDeclarationID;
                    _doc.penality = txtPenality.Text == "" ? 0 : double.Parse(txtPenality.Text);
                    _doc.interest = txtInterest.Text == "" ? 0 : double.Parse(txtInterest.Text);
                    _doc.unaccountedTax = txtOtherPenality.Text == "" ? 0 : double.Parse(txtOtherPenality.Text);
                    _doc.deduction = txtExemption.Text == "" ? 0 : double.Parse(txtExemption.Text);
                    _doc.assetAccountID = _paymentController.assetAccountID;
                    if (PaymentMethodHelper.IsPaymentMethodWithReference(_paymentController.PaymentTypeSelector.PaymentMethod))
                    {
                        bool updateDoc = _doc == null ? false : true;
                        SetReferenceNumber(updateDoc);
                    }
                    _doc.ShortDescription = txtNote.Text;
                    if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(_paymentController.PaymentTypeSelector.PaymentMethod))
                    {
                        _doc.serviceChargePayer = ServiceChargePayer.Company;
                        double serviceChargeAmount = txtServiceCharge.Text == "" ? 0 : double.Parse(txtServiceCharge.Text);
                        _doc.serviceChargeAmount = serviceChargeAmount;
                    }
                    _client.PostGenericDocument(_doc);
                    if (!(_client is DocumentScheduler))
                    {
                        if (TaxDeclarationManager.TaxDeclarationManagerForm != null)
                            TaxDeclarationManager.TaxDeclarationManagerForm.ReloadSavedAndPaid();
                        string message = docID == -1 ? "Payment successfully saved!" : "Payment successfully updated!";
                        MessageBox.ShowSuccessMessage(message);
                    }
                    Close();
                }
                else
                {
                    MessageBox.ShowErrorMessage("Please clear all errors marked in red and try again!");
                }
            }
            
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void SetReferenceNumber(bool updateDoc)
        {
            switch (comboPaymentType.PaymentMethod)
            {
                case BizNetPaymentMethod.Check:
                    if (_newDocument)
                        _doc.checkNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = _doc.checkNumber;
                        else
                            _doc.checkNumber = txtReference.Text;
                    break;
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.CPOFromBankAccount:
                    if (_newDocument)
                        _doc.cpoNumber = txtReference.Text;
                    else
                        if (!updateDoc)
                            txtReference.Text = _doc.cpoNumber;
                        else
                            _doc.cpoNumber = txtReference.Text;
                    break;
                
                default:
                    break;
            }
        }
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private double CalculateTotalPayment()
        {
            double penality = txtPenality.Text == "" ? 0 : double.Parse(txtPenality.Text);
            double interest = txtInterest.Text == "" ? 0 : double.Parse(txtInterest.Text);
            double otherPenality = txtOtherPenality.Text == "" ? 0 : double.Parse(txtOtherPenality.Text);
            double exemption = txtExemption.Text == "" ? 0 : double.Parse(txtExemption.Text);

            _TotalPayment = (penality + interest + otherPenality + _DeclaredAmount) - exemption;
            labelTotal.Text = TSConstants.FormatBirr(Math.Round(_TotalPayment,2)) + " Birr";
            return _TotalPayment;
        }

        protected void HideControlsForNonPayableVAT()
        {
            layoutControlPaymentMethod.HideToCustomization();
            layoutPenality.HideToCustomization();
            layoutOtherPenality.HideToCustomization();
            layoutInterest.HideToCustomization();
            layoutExemptions.HideToCustomization();
            layoutControlCashAccount.HideToCustomization();
            layoutControlCashBalance.HideToCustomization();
            layoutControlBankAccount.HideToCustomization();
            layoutControlBankBalance.HideToCustomization();
            layoutControlSlipRef.HideToCustomization();
            layoutControlServiceCharge.HideToCustomization();
        }
        protected void HideVATRelatedControls()
        {
            layoutTotalCredit.HideToCustomization();
            layoutAccumulatedCredit.HideToCustomization();
            layoutCreditForward.HideToCustomization();
        }
        protected void SetCurrentTotalCreditAmount()
        {
            labelCurrentCredit.Text = Account.AmountLess(_activation.CurrentPeriodCredit, 0) ? "0.00 Birr" : TSConstants.FormatBirr(_activation.CurrentPeriodCredit) + " Birr";
        }
        protected void SetAccumulatedCredit()
        {
            labelAccumulatedCredit.Text = TSConstants.FormatBirr(_activation.accumulatedCredit) + " Birr";
        }
        protected void SetCarryForwardCredit()
        {
            labelCarryForwardCredit.Text = TSConstants.FormatBirr(_activation.CarryCreditForward) + " Birr";
        }
        
        private void txtInterest_EditValueChanged(object sender, EventArgs e)
        {
            CalculateTotalPayment();
        }

        private void txtOtherPenality_EditValueChanged(object sender, EventArgs e)
        {
            CalculateTotalPayment();
        }

        private void txtExemption_EditValueChanged(object sender, EventArgs e)
        {
            double penality = txtPenality.Text == "" ? 0 : double.Parse(txtPenality.Text);
            double interest = txtInterest.Text == "" ? 0 : double.Parse(txtInterest.Text);
            double otherPenality = txtOtherPenality.Text == "" ? 0 : double.Parse(txtOtherPenality.Text);
            double totalDeclaredAmount = penality + interest + otherPenality + _DeclaredAmount;
            InitializeExemptionValidation(totalDeclaredAmount);
            if (validationTaxDeclaration.Validate(txtExemption))
                CalculateTotalPayment();
        }

        private void InitializeExemptionValidation(double totalDeclaredAmount)
        {
            TaxExemptionValidationRule exemptionValidation = new TaxExemptionValidationRule(totalDeclaredAmount);
            exemptionValidation.ErrorType = ErrorType.Default;
            exemptionValidation.ErrorText = "The exemption amount you entered will make total payment negative. Please correct it and try again!";
            validationTaxDeclaration.SetValidationRule(txtExemption, exemptionValidation);
        }

        private void txtPenality_EditValueChanged(object sender, EventArgs e)
        {
            CalculateTotalPayment();
        }

        private void voucher_Load(object sender, EventArgs e)
        {

        }

    }
}
