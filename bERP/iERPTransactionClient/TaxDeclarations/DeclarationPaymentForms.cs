﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout.Utils;
using INTAPS.Payroll;
using System.Collections;
using DevExpress.XtraEditors.Controls;

namespace BIZNET.iERP.Client
{

    class IncomeTaxDeclarationForm : TaxDeclarationFormBase
    {
        public IncomeTaxDeclarationForm(IAccountingClient client, ActivationParameter activation)
            : base(client, activation)
        {
        }
        protected override bool validateData(int assetAccountID)
        {
            return true;
        }
        protected override int relevantAccountID
        {
            get
            {
                return -1; //no relevant account id since we retrieve the amount to be declared from another source not an account balance
            }
        }

        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "Collected Income Tax Return Form";
            base.HideVATRelatedControls();
        }

        protected override TaxDeclarationDocumentBase CreateDocumentInstance()
        {
            return new IncomeTaxDeclarationDocument();
        }

        protected override string getTitle(bool transitive)
        {
            return "Return of Collected Income Tax for Period ";
        }
    }

    class PensionTaxDeclarationForm : TaxDeclarationFormBase
    {
        public PensionTaxDeclarationForm(IAccountingClient client, ActivationParameter activation)
            : base(client, activation)
        {
        }
        protected override bool validateData(int assetAccountID)
        {
            return true;
        }
        protected override int relevantAccountID
        {
            get
            {
                return -1; //no relevant account id since we retrieve the amount to be declared from another source not an account balance
            }
        }

        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "Collected Pension Tax Return Form";
            base.HideVATRelatedControls();
        }

        protected override TaxDeclarationDocumentBase CreateDocumentInstance()
        {
            return new PensionTaxDeclarationDocument();
        }

        protected override string getTitle(bool transitive)
        {
            return "Return of Collected Pension Tax for Period ";
        }
    }

    class WithholdingTaxDeclarationForm : TaxDeclarationFormBase
    {
        public WithholdingTaxDeclarationForm(IAccountingClient client, ActivationParameter activation)
            : base(client, activation)
        {
        }
        protected override bool validateData(int assetAccountID)
        {
            return true;
        }
        protected override int relevantAccountID
        {
            get
            {
                return -1; //no relevant account id since we retrieve the amount to be declared from another source not an account balance
            }
        }

        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "Collected Withholding Tax Return Form";
            base.HideVATRelatedControls();
        }

        protected override TaxDeclarationDocumentBase CreateDocumentInstance()
        {
            return new WithholdingTaxDeclarationDocument();
        }

        protected override string getTitle(bool transitive)
        {
            return "Return of Collected Withholding Tax for Period ";
        }
    }

    class VATWithholdingDeclarationForm : TaxDeclarationFormBase
    {
        public VATWithholdingDeclarationForm(IAccountingClient client, ActivationParameter activation)
            : base(client, activation)
        {
        }
        protected override bool validateData(int assetAccountID)
        {
            return true;
        }
        protected override int relevantAccountID
        {
            get
            {
                return -1; //no relevant account id since we retrieve the amount to be declared from another source not an account balance
            }
        }

        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "Collected VAT Withholding Return Form";
            base.HideVATRelatedControls();
        }

        protected override TaxDeclarationDocumentBase CreateDocumentInstance()
        {
            return new VATWithholdingDeclarationDocument();
        }

        protected override string getTitle(bool transitive)
        {
            return "Return of Collected VAT Withholding for Period ";
        }
    }


    class VATDeclarationForm : TaxDeclarationFormBase
    {
        public VATDeclarationForm(IAccountingClient client, ActivationParameter activation)
            : base(client, activation)
        {
        }
        protected override bool validateData(int assetAccountID)
        {
            return true;
        }
        protected override int relevantAccountID
        {
            get
            {
                return -1; //no relevant account id since we retrieve the amount to be declared from another source not an account balance
            }
        }

        protected override void PrepareControlsLayoutBasedOnDocumentType()
        {
            Text = "Collected Value Added Tax Return Form";
            if (Account.AmountEqual(base._DeclaredAmount, 0))
                base.HideControlsForNonPayableVAT();
            base.SetCurrentTotalCreditAmount();
            base.SetAccumulatedCredit();
            base.SetCarryForwardCredit();
        }

        protected override TaxDeclarationDocumentBase CreateDocumentInstance()
        {
            return new VATDeclarationDocument();
        }

        protected override string getTitle(bool transitive)
        {
            return "Return of Value Added Tax for Period ";
        }
    }

}
