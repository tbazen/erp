using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.UI.ButtonGrid;
using System.Drawing;
using System.IO;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{

    public class CustomersBondReturnList : ObjectBGList<BondPaymentDocument>
    {
        ButtonGrid buttonGrid;
        BizNetPaymentMethod payMethod;
        public CustomersBondReturnList(ButtonGrid grid, ButtonGridItem parentItem,BizNetPaymentMethod paymentMethod)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
            payMethod = paymentMethod;
        }
        public CustomersBondReturnList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
         
        public override bool Searchable
        {
            get { return true; }
        }
        public override ButtonGridItem[] CreateButton(BondPaymentDocument bondPayment)
        {
            string refNumber = bondPayment.cpoNumber ?? bondPayment.PaperRef;
            ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButton("Bond" + bondPayment.DocumentTypeID, "Bond " + refNumber);
            TradeRelation customer = iERPTransactionClient.GetCustomer(bondPayment.customerCode);
            ButtonGridHelper.AddMiddleText(button, customer.Name);
            ButtonGridHelper.AddCornerText(button, "Birr:" + TSConstants.FormatBirr(bondPayment.amount));
            button.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(125), payMethod,bondPayment);

            return new ButtonGridItem[] { button };

        }

        public override List<BondPaymentDocument> LoadData(string query)
        {
            List<BondPaymentDocument> ret = new List<BondPaymentDocument>();
            BondPaymentDocument[] bondPayments = iERPTransactionClient.GetAllUnreturnedBonds(query);

            foreach (BondPaymentDocument bondPayment in bondPayments)
            {
                ret.Add(bondPayment);
            }

            return ret;
        }

        public override string searchLabel
        {
            get { return "Bank Account:"; }
        }
    }

}
