using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.UI.ButtonGrid;
using System.Drawing;
using System.IO;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{
    public class TradeRelationDocumentTypeActivatorList<TransactionType> : ObjectBGList<TradeRelation> where TransactionType: TradeTransaction
    {
        ButtonGrid buttonGrid;
        TradeRelationType _relationType;
        public TradeRelationDocumentTypeActivatorList(ButtonGrid grid, ButtonGridItem parentItem,TradeRelationType relationType)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
            _relationType = relationType;
        }
        public override bool Searchable
        {
            get { return true; }
        }
        public override ButtonGridItem[] CreateButton(TradeRelation relation)
        {
            if (relation == null)
            {
                ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("newCustomer", 
                    
                    _relationType==TradeRelationType.Customer?"Add New Customer":"Add New Supplier",
                    Properties.Resources.customers_add_256);
                new EventActivator<object>(button, null).Clicked += AddNewCustomer_Clicked;
                return new ButtonGridItem[] { button };
            }
            else
            {
                Image customerImage = Properties.Resources.customers;
                ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("customer" + relation.Code, relation.Name, customerImage);
                string customerTIN = relation.TIN == "" ? "" : (long.Parse(relation.TIN) == 0 ? "" : "TIN: " + relation.TIN);
                ButtonGridHelper.AddCornerText(button, customerTIN);
                int typeID = AccountingClient.GetDocumentTypeByType(typeof(TransactionType)).id;
                button.Tag = new DocumentWithRelationActivator(iERPTransactionClient.GetDocumentHandler(typeID),relation);
                return new ButtonGridItem[] { button };
            }
        }

        void AddNewCustomer_Clicked(ButtonGridBodyButtonItem t, object z)
        {
            TradeRelationCategoryPicker picker = new TradeRelationCategoryPicker();
            if (picker.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm) != System.Windows.Forms.DialogResult.OK)
                return;
            RegisterRelation registerCustomer = new RegisterRelation(picker.category.code, _relationType);
            registerCustomer.ShowDialog();
        }

        protected virtual bool CanIncludeCustomer(TradeRelation relation)
        {
            return true;
        }

        public override List<TradeRelation> LoadData(string query)
        {
            List<TradeRelation> ret = new List<TradeRelation>();
            int N;
            TradeRelation[] allRelation = null;
            if (string.IsNullOrEmpty(query))
                allRelation = iERPTransactionClient.searchTradeRelation(null,new TradeRelationType[]{_relationType},0,100,new object[0],new string[0],out N);
            else
            {
                string[] column = new string[1];
                object[] criteria = new object[1];
                int customerTIN;
                criteria[0] = query;
                if (int.TryParse(query, out customerTIN))
                    column[0] = "TIN";
                else
                    column[0] = "Name";
                allRelation = iERPTransactionClient.searchTradeRelation(null, new TradeRelationType[] { _relationType }, 0, 100, criteria, column, out N);
            }
            ret.Add(null);
            foreach (TradeRelation relation in allRelation)
            {
                if (CanIncludeCustomer(relation))
                {
                    ret.Add(relation);
                }
            }
            return ret;
        }


        public override string searchLabel
        {
            get { return "Trade Relation:"; }
        }
    }

    public class CustomersList : ObjectBGList<TradeRelation>
    {
        ButtonGrid buttonGrid;
        public CustomersList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        public override bool Searchable
        {
            get { return true; }
        }
        public override ButtonGridItem[] CreateButton(TradeRelation customer)
        {
            if (customer == null)
            {
                ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("newCustomer", "Add New Customer", Properties.Resources.customers_add_256);
                new EventActivator<object>(button, null).Clicked += AddNewCustomer_Clicked;
                return new ButtonGridItem[] { button };

            }
            else
            {
                Image customerImage = Properties.Resources.customers;
                ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("customer" + customer.Code, customer.Name, customerImage);
                //ButtonGridHelper.SetButtonBackColor(button, Color.LightBlue);
                string customerTIN = customer.TIN == "" ? "" : (int.Parse(customer.TIN) == 0 ? "" : "TIN: " + customer.TIN);
                ButtonGridHelper.AddCornerText(button, customerTIN);
                return new ButtonGridItem[] { button };
            }
        }

        void AddNewCustomer_Clicked(ButtonGridBodyButtonItem t, object z)
        {
            TradeRelationCategoryPicker picker=new TradeRelationCategoryPicker();
            if(picker.ShowDialog( INTAPS.UI.UIFormApplicationBase.MainForm)!=System.Windows.Forms.DialogResult.OK)
                return;
            RegisterRelation registerCustomer = new RegisterRelation(picker.category.code, TradeRelationType.Customer);
            registerCustomer.ShowDialog();
        }

        protected virtual bool CanIncludeCustomer(TradeRelation customer)
        {
            return true;
        }

        public override List<TradeRelation> LoadData(string query)
        {
            List<TradeRelation> ret = new List<TradeRelation>();
            int N;
            TradeRelation[] allCustomers = null;
            if (string.IsNullOrEmpty(query))
                allCustomers = iERPTransactionClient.SearchCustomers(0, 100, null, null, out N);
            else
            {
                string[] column = new string[1];
                object[] criteria = new object[1];
                int customerTIN;
                criteria[0] = query;
                if (int.TryParse(query, out customerTIN))
                    column[0] = "TIN";
                else
                    column[0] = "Name";
                allCustomers = iERPTransactionClient.SearchCustomers(0, 100, criteria, column, out N);
            }
            ret.Add(null);
            foreach (TradeRelation customer in allCustomers)
            {
                if (CanIncludeCustomer(customer))
                {
                    ret.Add(customer);
                }
            }
            return ret;
        }

        public override string searchLabel
        {
            get { return "Customer:"; }
        }
    }

    public class CustomersPayableList : CustomersList
    {
        BizNetPaymentMethod payMethod;
        public CustomersPayableList(ButtonGrid grid, ButtonGridItem parentItem,BizNetPaymentMethod paymentMethod)
            : base(grid, parentItem)
        {
            payMethod = paymentMethod;
        }
        public CustomersPayableList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            
        }
        string customerCode;
        protected override bool CanIncludeCustomer(TradeRelation customer)
        {
            if (customer.Activated)
            {
                customerCode = customer.Code;
                return true;
            }
            return false;
        }
        public override ButtonGridItem[] CreateButton(TradeRelation customer)
        {
            ButtonGridBodyButtonItem item = (ButtonGridBodyButtonItem)base.CreateButton(customer)[0];
            if (customer != null)
            {
                int typeID = AccountingClient.GetDocumentTypeByType(typeof(CustomerCreditSettlement2Document)).id;
                item.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(typeID), payMethod, customer.Code);
            }
            return new ButtonGridItem[] { item };

        }

    }

    public class CustomersCreditList : CustomersList
    {
        BizNetPaymentMethod payMethod;
        public CustomersCreditList(ButtonGrid grid, ButtonGridItem parentItem,BizNetPaymentMethod paymentMethod)
            : base(grid, parentItem)
        {
            payMethod = paymentMethod;
        }
        public CustomersCreditList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            
        }
         
        string customerCode;
        protected override bool CanIncludeCustomer(TradeRelation customer)
        {
            return true;
        }
        public override ButtonGridItem[] CreateButton(TradeRelation customer)
        {
            ButtonGridBodyButtonItem item = (ButtonGridBodyButtonItem)base.CreateButton(customer)[0];
            if (customer != null)
            {
                int typeID = AccountingClient.GetDocumentTypeByType(typeof(CustomerCreditSettlement2Document)).id;
                item.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(117), payMethod, customer.Code);
            }
            return new ButtonGridItem[] { item };
        }
    }

    public class CustomersAdvanceReturnList : CustomersList
    {
        ButtonGrid buttonGrid;
        BizNetPaymentMethod payMethod;
        public CustomersAdvanceReturnList(ButtonGrid grid, ButtonGridItem parentItem,BizNetPaymentMethod paymentMethod)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
            payMethod = paymentMethod;
        }
        public CustomersAdvanceReturnList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            
        }
         
        string customerCode;
        protected override bool CanIncludeCustomer(TradeRelation customer)
        {
            return true;
        }
        public override ButtonGridItem[] CreateButton(TradeRelation customer)
        {
            ButtonGridBodyButtonItem item = (ButtonGridBodyButtonItem)base.CreateButton(customer)[0];
            if (customer != null)
            {
                int typeID = AccountingClient.GetDocumentTypeByType(typeof(CustomerAdvanceReturn2Document)).id;
                item.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(typeID), payMethod, customer.Code);
            }
            return new ButtonGridItem[] { item };

        }
    }

    public class CustomersSalesList : CustomersList
    {
        string customerCode;
        PurchaseAndSalesData purchaseAndSalesData;
        ButtonGrid buttonGrid;

        public CustomersSalesList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        public CustomersSalesList(ButtonGrid grid,ButtonGridItem parentItem,PurchaseAndSalesData salesData)
            :base(grid,parentItem)
        {
            buttonGrid = grid;
            purchaseAndSalesData = salesData;
        }
        protected override bool CanIncludeCustomer(TradeRelation customer)
        {
            if (customer.Activated)
            {
                customerCode = customer.Code;
                return true;
            }
            return false;
        }
        public override ButtonGridItem[] CreateButton(TradeRelation customer)
        {
            ButtonGridBodyButtonItem item = (ButtonGridBodyButtonItem)base.CreateButton(customer)[0];
            if (customer != null)
            {
                item.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(146), purchaseAndSalesData.paymentMethod, customer.Code);
            }
            return new ButtonGridItem[] { item };
        }

    }

    public class CustomersBondPaymentList : CustomersList
    {
        ButtonGrid buttonGrid;
        BizNetPaymentMethod payMethod;
        public CustomersBondPaymentList(ButtonGrid grid, ButtonGridItem parentItem, BizNetPaymentMethod paymentMethod)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
            payMethod = paymentMethod;
        }
        public CustomersBondPaymentList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {

        }

        string customerCode;
        protected override bool CanIncludeCustomer(TradeRelation customer)
        {
            if (customer.Activated)
            {
                customerCode = customer.Code;
                return true;
            }
            return false;
        }
        public override ButtonGridItem[] CreateButton(TradeRelation customer)
        {
            ButtonGridBodyButtonItem item = (ButtonGridBodyButtonItem)base.CreateButton(customer)[0];
            if (customer != null)
                item.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(124), payMethod, customer.Code);
            return new ButtonGridItem[] { item };

        }
    }


}
