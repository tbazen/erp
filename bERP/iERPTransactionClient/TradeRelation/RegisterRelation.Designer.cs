﻿namespace BIZNET.iERP.Client
{
    partial class RegisterRelation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegisterRelation));
            this.memoAddress = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.labelCategory = new DevExpress.XtraEditors.LabelControl();
            this.chkDontWithhold = new DevExpress.XtraEditors.CheckEdit();
            this.checkSupplier = new DevExpress.XtraEditors.CheckEdit();
            this.txtDescription = new DevExpress.XtraEditors.MemoEdit();
            this.checkFinancer = new DevExpress.XtraEditors.CheckEdit();
            this.checkFinancee = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.txtHouseNo = new DevExpress.XtraEditors.TextEdit();
            this.checkCustomer = new DevExpress.XtraEditors.CheckEdit();
            this.txtKebele = new DevExpress.XtraEditors.TextEdit();
            this.txtTIN = new DevExpress.XtraEditors.TextEdit();
            this.txtWoreda = new DevExpress.XtraEditors.TextEdit();
            this.txtZone = new DevExpress.XtraEditors.TextEdit();
            this.cmbTaxRegType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtVATNumber = new DevExpress.XtraEditors.TextEdit();
            this.txtRegion = new DevExpress.XtraEditors.TextEdit();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.txtWebsite = new DevExpress.XtraEditors.TextEdit();
            this.txtTele = new DevExpress.XtraEditors.TextEdit();
            this.txtEmail = new DevExpress.XtraEditors.TextEdit();
            this.chkIsVatAgent = new DevExpress.XtraEditors.CheckEdit();
            this.btnBrowse = new DevExpress.XtraEditors.SimpleButton();
            this.picLogo = new DevExpress.XtraEditors.PictureEdit();
            this.chkIsWithholdingAgent = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutVATNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutVATAgent = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutVATReg = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutWithholding = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutNonWithholding = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridAccountInfo = new DevExpress.XtraGrid.GridControl();
            this.gridViewAccountInfo = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.validationSupplier = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.tabRelation = new DevExpress.XtraTab.XtraTabControl();
            this.tabDetail = new DevExpress.XtraTab.XtraTabPage();
            this.tabAccountInfo = new DevExpress.XtraTab.XtraTabPage();
            this.tabLedger = new DevExpress.XtraTab.XtraTabPage();
            this.browser = new INTAPS.UI.HTML.ControlBrowser();
            this.panelLedgerAccounts = new System.Windows.Forms.Panel();
            this.exportButton = new INTAPS.UI.HTML.BowserController();
            this.radAsPayable = new System.Windows.Forms.RadioButton();
            this.radReceivable = new System.Windows.Forms.RadioButton();
            this.ledgerAccounts = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonShowLedger = new DevExpress.XtraEditors.SimpleButton();
            this.tabDocuments = new DevExpress.XtraTab.XtraTabPage();
            this.browserTran = new INTAPS.UI.HTML.ControlBrowser();
            this.panel1 = new System.Windows.Forms.Panel();
            this.exportButtonTran = new INTAPS.UI.HTML.BowserController();
            this.buttonShowTran = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.memoAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDontWithhold.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkFinancer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkFinancee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHouseNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKebele.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWoreda.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTaxRegType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTele.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsVatAgent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsWithholdingAgent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVATNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVATAgent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVATReg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWithholding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutNonWithholding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAccountInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAccountInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationSupplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabRelation)).BeginInit();
            this.tabRelation.SuspendLayout();
            this.tabDetail.SuspendLayout();
            this.tabAccountInfo.SuspendLayout();
            this.tabLedger.SuspendLayout();
            this.panelLedgerAccounts.SuspendLayout();
            this.tabDocuments.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // memoAddress
            // 
            this.memoAddress.EditValue = "";
            this.memoAddress.Location = new System.Drawing.Point(12, 336);
            this.memoAddress.Name = "memoAddress";
            this.memoAddress.Size = new System.Drawing.Size(760, 109);
            this.memoAddress.StyleController = this.layoutControl2;
            this.memoAddress.TabIndex = 30;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.labelCategory);
            this.layoutControl2.Controls.Add(this.chkDontWithhold);
            this.layoutControl2.Controls.Add(this.checkSupplier);
            this.layoutControl2.Controls.Add(this.memoAddress);
            this.layoutControl2.Controls.Add(this.txtDescription);
            this.layoutControl2.Controls.Add(this.checkFinancer);
            this.layoutControl2.Controls.Add(this.checkFinancee);
            this.layoutControl2.Controls.Add(this.panelControl1);
            this.layoutControl2.Controls.Add(this.txtName);
            this.layoutControl2.Controls.Add(this.txtHouseNo);
            this.layoutControl2.Controls.Add(this.checkCustomer);
            this.layoutControl2.Controls.Add(this.txtKebele);
            this.layoutControl2.Controls.Add(this.txtTIN);
            this.layoutControl2.Controls.Add(this.txtWoreda);
            this.layoutControl2.Controls.Add(this.txtZone);
            this.layoutControl2.Controls.Add(this.cmbTaxRegType);
            this.layoutControl2.Controls.Add(this.txtVATNumber);
            this.layoutControl2.Controls.Add(this.txtRegion);
            this.layoutControl2.Controls.Add(this.btnClear);
            this.layoutControl2.Controls.Add(this.txtWebsite);
            this.layoutControl2.Controls.Add(this.txtTele);
            this.layoutControl2.Controls.Add(this.txtEmail);
            this.layoutControl2.Controls.Add(this.chkIsVatAgent);
            this.layoutControl2.Controls.Add(this.btnBrowse);
            this.layoutControl2.Controls.Add(this.picLogo);
            this.layoutControl2.Controls.Add(this.chkIsWithholdingAgent);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(784, 604);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // labelCategory
            // 
            this.labelCategory.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelCategory.Location = new System.Drawing.Point(12, 12);
            this.labelCategory.Name = "labelCategory";
            this.labelCategory.Size = new System.Drawing.Size(45, 13);
            this.labelCategory.StyleController = this.layoutControl2;
            this.labelCategory.TabIndex = 32;
            this.labelCategory.Text = "#####";
            // 
            // chkDontWithhold
            // 
            this.chkDontWithhold.Location = new System.Drawing.Point(304, 125);
            this.chkDontWithhold.Name = "chkDontWithhold";
            this.chkDontWithhold.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.chkDontWithhold.Properties.Caption = "Do not Withhold";
            this.chkDontWithhold.Size = new System.Drawing.Size(468, 21);
            this.chkDontWithhold.StyleController = this.layoutControl2;
            this.chkDontWithhold.TabIndex = 31;
            // 
            // checkSupplier
            // 
            this.checkSupplier.Location = new System.Drawing.Point(195, 29);
            this.checkSupplier.Name = "checkSupplier";
            this.checkSupplier.Properties.Caption = "Supplier";
            this.checkSupplier.Size = new System.Drawing.Size(105, 19);
            this.checkSupplier.StyleController = this.layoutControl2;
            this.checkSupplier.TabIndex = 27;
            this.checkSupplier.CheckedChanged += new System.EventHandler(this.checkSupplier_CheckedChanged);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(12, 465);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(760, 62);
            this.txtDescription.StyleController = this.layoutControl2;
            this.txtDescription.TabIndex = 10;
            // 
            // checkFinancer
            // 
            this.checkFinancer.Location = new System.Drawing.Point(195, 75);
            this.checkFinancer.Name = "checkFinancer";
            this.checkFinancer.Properties.Caption = "Creditor";
            this.checkFinancer.Size = new System.Drawing.Size(105, 19);
            this.checkFinancer.StyleController = this.layoutControl2;
            this.checkFinancer.TabIndex = 28;
            // 
            // checkFinancee
            // 
            this.checkFinancee.Location = new System.Drawing.Point(195, 98);
            this.checkFinancee.Name = "checkFinancee";
            this.checkFinancee.Properties.Caption = "Debitor";
            this.checkFinancee.Size = new System.Drawing.Size(105, 19);
            this.checkFinancee.StyleController = this.layoutControl2;
            this.checkFinancee.TabIndex = 29;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnCancel);
            this.panelControl1.Controls.Add(this.btnOk);
            this.panelControl1.Location = new System.Drawing.Point(12, 531);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(760, 61);
            this.panelControl1.TabIndex = 11;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(678, 32);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Close";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(597, 32);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "&Ok";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(390, 29);
            this.txtName.Name = "txtName";
            this.txtName.Properties.Mask.EditMask = "[a-zA-Z].*";
            this.txtName.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtName.Size = new System.Drawing.Size(382, 20);
            this.txtName.StyleController = this.layoutControl2;
            this.txtName.TabIndex = 6;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "TradeRelation name cannot be empty";
            this.validationSupplier.SetValidationRule(this.txtName, conditionValidationRule2);
            // 
            // txtHouseNo
            // 
            this.txtHouseNo.Location = new System.Drawing.Point(98, 296);
            this.txtHouseNo.Name = "txtHouseNo";
            this.txtHouseNo.Size = new System.Drawing.Size(674, 20);
            this.txtHouseNo.StyleController = this.layoutControl2;
            this.txtHouseNo.TabIndex = 23;
            // 
            // checkCustomer
            // 
            this.checkCustomer.Location = new System.Drawing.Point(195, 52);
            this.checkCustomer.Name = "checkCustomer";
            this.checkCustomer.Properties.Caption = "Customer";
            this.checkCustomer.Size = new System.Drawing.Size(105, 19);
            this.checkCustomer.StyleController = this.layoutControl2;
            this.checkCustomer.TabIndex = 26;
            this.checkCustomer.CheckedChanged += new System.EventHandler(this.checkCustomer_CheckedChanged);
            // 
            // txtKebele
            // 
            this.txtKebele.Location = new System.Drawing.Point(435, 272);
            this.txtKebele.Name = "txtKebele";
            this.txtKebele.Size = new System.Drawing.Size(337, 20);
            this.txtKebele.StyleController = this.layoutControl2;
            this.txtKebele.TabIndex = 22;
            // 
            // txtTIN
            // 
            this.txtTIN.Location = new System.Drawing.Point(390, 53);
            this.txtTIN.Name = "txtTIN";
            this.txtTIN.Properties.Mask.EditMask = "\\d*";
            this.txtTIN.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTIN.Size = new System.Drawing.Size(382, 20);
            this.txtTIN.StyleController = this.layoutControl2;
            this.txtTIN.TabIndex = 12;
            // 
            // txtWoreda
            // 
            this.txtWoreda.Location = new System.Drawing.Point(98, 272);
            this.txtWoreda.Name = "txtWoreda";
            this.txtWoreda.Size = new System.Drawing.Size(292, 20);
            this.txtWoreda.StyleController = this.layoutControl2;
            this.txtWoreda.TabIndex = 21;
            // 
            // txtZone
            // 
            this.txtZone.Location = new System.Drawing.Point(401, 248);
            this.txtZone.Name = "txtZone";
            this.txtZone.Size = new System.Drawing.Size(371, 20);
            this.txtZone.StyleController = this.layoutControl2;
            this.txtZone.TabIndex = 20;
            // 
            // cmbTaxRegType
            // 
            this.cmbTaxRegType.EditValue = "VAT (Value Added Tax)";
            this.cmbTaxRegType.Location = new System.Drawing.Point(390, 101);
            this.cmbTaxRegType.Name = "cmbTaxRegType";
            this.cmbTaxRegType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTaxRegType.Properties.Items.AddRange(new object[] {
            "VAT (Value Added Tax)",
            "TOT (Turn Over Tax)",
            "Non-VAT & TOT Tax Payer"});
            this.cmbTaxRegType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbTaxRegType.Size = new System.Drawing.Size(382, 20);
            this.cmbTaxRegType.StyleController = this.layoutControl2;
            this.cmbTaxRegType.TabIndex = 18;
            this.cmbTaxRegType.SelectedIndexChanged += new System.EventHandler(this.cmbTaxRegType_SelectedIndexChanged);
            // 
            // txtVATNumber
            // 
            this.txtVATNumber.Location = new System.Drawing.Point(390, 77);
            this.txtVATNumber.Name = "txtVATNumber";
            this.txtVATNumber.Properties.Mask.EditMask = "\\d*";
            this.txtVATNumber.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtVATNumber.Size = new System.Drawing.Size(382, 20);
            this.txtVATNumber.StyleController = this.layoutControl2;
            this.txtVATNumber.TabIndex = 13;
            // 
            // txtRegion
            // 
            this.txtRegion.Location = new System.Drawing.Point(98, 248);
            this.txtRegion.Name = "txtRegion";
            this.txtRegion.Properties.Mask.EditMask = "\\w.*";
            this.txtRegion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtRegion.Size = new System.Drawing.Size(218, 20);
            this.txtRegion.StyleController = this.layoutControl2;
            this.txtRegion.TabIndex = 7;
            // 
            // btnClear
            // 
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.Location = new System.Drawing.Point(110, 174);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(81, 22);
            this.btnClear.StyleController = this.layoutControl2;
            this.btnClear.TabIndex = 16;
            this.btnClear.Text = "Cl&ear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // txtWebsite
            // 
            this.txtWebsite.Location = new System.Drawing.Point(98, 224);
            this.txtWebsite.Name = "txtWebsite";
            this.txtWebsite.Properties.Mask.EditMask = "\\w.*";
            this.txtWebsite.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtWebsite.Size = new System.Drawing.Size(674, 20);
            this.txtWebsite.StyleController = this.layoutControl2;
            this.txtWebsite.TabIndex = 4;
            // 
            // txtTele
            // 
            this.txtTele.Location = new System.Drawing.Point(98, 200);
            this.txtTele.Name = "txtTele";
            this.txtTele.Properties.Mask.EditMask = "\\d.*";
            this.txtTele.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTele.Size = new System.Drawing.Size(292, 20);
            this.txtTele.StyleController = this.layoutControl2;
            this.txtTele.TabIndex = 8;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(480, 200);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Properties.Mask.EditMask = "\\w.*";
            this.txtEmail.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtEmail.Size = new System.Drawing.Size(292, 20);
            this.txtEmail.StyleController = this.layoutControl2;
            this.txtEmail.TabIndex = 9;
            // 
            // chkIsVatAgent
            // 
            this.chkIsVatAgent.Location = new System.Drawing.Point(304, 175);
            this.chkIsVatAgent.Name = "chkIsVatAgent";
            this.chkIsVatAgent.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.chkIsVatAgent.Properties.Caption = "VAT Agent";
            this.chkIsVatAgent.Size = new System.Drawing.Size(468, 21);
            this.chkIsVatAgent.StyleController = this.layoutControl2;
            this.chkIsVatAgent.TabIndex = 25;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Image = ((System.Drawing.Image)(resources.GetObject("btnBrowse.Image")));
            this.btnBrowse.Location = new System.Drawing.Point(12, 174);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(94, 22);
            this.btnBrowse.StyleController = this.layoutControl2;
            this.btnBrowse.TabIndex = 15;
            this.btnBrowse.Text = "&Browse";
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // picLogo
            // 
            this.picLogo.Location = new System.Drawing.Point(12, 29);
            this.picLogo.Name = "picLogo";
            this.picLogo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.picLogo.Size = new System.Drawing.Size(179, 141);
            this.picLogo.StyleController = this.layoutControl2;
            this.picLogo.TabIndex = 14;
            // 
            // chkIsWithholdingAgent
            // 
            this.chkIsWithholdingAgent.Location = new System.Drawing.Point(304, 150);
            this.chkIsWithholdingAgent.Name = "chkIsWithholdingAgent";
            this.chkIsWithholdingAgent.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.chkIsWithholdingAgent.Properties.Caption = "Withholding Agent";
            this.chkIsWithholdingAgent.Size = new System.Drawing.Size(468, 21);
            this.chkIsWithholdingAgent.StyleController = this.layoutControl2;
            this.chkIsWithholdingAgent.TabIndex = 24;
            this.chkIsWithholdingAgent.CheckedChanged += new System.EventHandler(this.chkIsWithholdingAgent_CheckedChanged);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem17,
            this.layoutControlItem16,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem1,
            this.layoutControlItem4,
            this.layoutControlItem9,
            this.layoutControlItem15,
            this.layoutControlItem20,
            this.layoutControlItem8,
            this.layoutVATNumber,
            this.layoutControlItem5,
            this.layoutControlItem2,
            this.layoutControlItem14,
            this.layoutControlItem21,
            this.layoutVATAgent,
            this.layoutVATReg,
            this.layoutControlItem3,
            this.layoutWithholding,
            this.layoutControlItem6,
            this.layoutControlItem24,
            this.layoutNonWithholding,
            this.layoutControlItem7});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(784, 604);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.checkSupplier;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(183, 17);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(109, 23);
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.checkCustomer;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(183, 40);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(109, 23);
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.checkFinancer;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(183, 63);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(109, 23);
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.checkFinancee;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(183, 86);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(109, 102);
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.picLogo;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(183, 145);
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.btnBrowse;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 162);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(98, 26);
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.btnClear;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(98, 162);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(85, 26);
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtWebsite;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 212);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(764, 24);
            this.layoutControlItem1.Text = "Website:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtRegion;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 236);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(308, 24);
            this.layoutControlItem4.Text = "Region:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtWoreda;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 260);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(382, 24);
            this.layoutControlItem9.Text = "Woreda:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.txtHouseNo;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 284);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(764, 24);
            this.layoutControlItem15.Text = "House No:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.memoAddress;
            this.layoutControlItem20.CustomizationFormText = "layoutControlItem20";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 308);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(764, 129);
            this.layoutControlItem20.Text = "Address:";
            this.layoutControlItem20.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.panelControl1;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 519);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(764, 65);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutVATNumber
            // 
            this.layoutVATNumber.Control = this.txtVATNumber;
            this.layoutVATNumber.CustomizationFormText = "layoutTaxReg";
            this.layoutVATNumber.Location = new System.Drawing.Point(292, 65);
            this.layoutVATNumber.Name = "layoutVATNumber";
            this.layoutVATNumber.Size = new System.Drawing.Size(472, 24);
            this.layoutVATNumber.Text = "VAT Number:";
            this.layoutVATNumber.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtTele;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 188);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(382, 24);
            this.layoutControlItem5.Text = "Phone Number:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtZone;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(308, 236);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(456, 24);
            this.layoutControlItem2.Text = "Zone / Subcity :";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(76, 13);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txtKebele;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(382, 260);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(382, 24);
            this.layoutControlItem14.Text = "Kebele:";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(36, 13);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.txtTIN;
            this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
            this.layoutControlItem21.Location = new System.Drawing.Point(292, 41);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(472, 24);
            this.layoutControlItem21.Text = "TIN:";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutVATAgent
            // 
            this.layoutVATAgent.Control = this.chkIsVatAgent;
            this.layoutVATAgent.CustomizationFormText = "layoutControlItem13";
            this.layoutVATAgent.Location = new System.Drawing.Point(292, 163);
            this.layoutVATAgent.Name = "layoutVATAgent";
            this.layoutVATAgent.Size = new System.Drawing.Size(472, 25);
            this.layoutVATAgent.Text = "layoutVATAgent";
            this.layoutVATAgent.TextSize = new System.Drawing.Size(0, 0);
            this.layoutVATAgent.TextToControlDistance = 0;
            this.layoutVATAgent.TextVisible = false;
            // 
            // layoutVATReg
            // 
            this.layoutVATReg.Control = this.cmbTaxRegType;
            this.layoutVATReg.CustomizationFormText = "layoutVATReg";
            this.layoutVATReg.Location = new System.Drawing.Point(292, 89);
            this.layoutVATReg.Name = "layoutVATReg";
            this.layoutVATReg.Size = new System.Drawing.Size(472, 24);
            this.layoutVATReg.Text = "Tax Registration:";
            this.layoutVATReg.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtName;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(292, 17);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(472, 24);
            this.layoutControlItem3.Text = "Name:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutWithholding
            // 
            this.layoutWithholding.Control = this.chkIsWithholdingAgent;
            this.layoutWithholding.CustomizationFormText = "layoutControlItem7";
            this.layoutWithholding.Location = new System.Drawing.Point(292, 138);
            this.layoutWithholding.Name = "layoutWithholding";
            this.layoutWithholding.Size = new System.Drawing.Size(472, 25);
            this.layoutWithholding.Text = "layoutWithholding";
            this.layoutWithholding.TextSize = new System.Drawing.Size(0, 0);
            this.layoutWithholding.TextToControlDistance = 0;
            this.layoutWithholding.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtEmail;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(382, 188);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(382, 24);
            this.layoutControlItem6.Text = "Email:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.txtDescription;
            this.layoutControlItem24.CustomizationFormText = "layoutControlItem24";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 437);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(764, 82);
            this.layoutControlItem24.Text = "Remarks:";
            this.layoutControlItem24.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutNonWithholding
            // 
            this.layoutNonWithholding.Control = this.chkDontWithhold;
            this.layoutNonWithholding.CustomizationFormText = "layoutNonWithholding";
            this.layoutNonWithholding.Location = new System.Drawing.Point(292, 113);
            this.layoutNonWithholding.Name = "layoutNonWithholding";
            this.layoutNonWithholding.Size = new System.Drawing.Size(472, 25);
            this.layoutNonWithholding.Text = "layoutNonWithholding";
            this.layoutNonWithholding.TextSize = new System.Drawing.Size(0, 0);
            this.layoutNonWithholding.TextToControlDistance = 0;
            this.layoutNonWithholding.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.Control = this.labelCategory;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(764, 17);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.CaptionImage = ((System.Drawing.Image)(resources.GetObject("groupControl1.CaptionImage")));
            this.groupControl1.Controls.Add(this.gridAccountInfo);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(784, 604);
            this.groupControl1.TabIndex = 17;
            this.groupControl1.Text = "Account Information";
            // 
            // gridAccountInfo
            // 
            this.gridAccountInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridAccountInfo.Location = new System.Drawing.Point(2, 32);
            this.gridAccountInfo.MainView = this.gridViewAccountInfo;
            this.gridAccountInfo.Name = "gridAccountInfo";
            this.gridAccountInfo.Size = new System.Drawing.Size(780, 570);
            this.gridAccountInfo.TabIndex = 12;
            this.gridAccountInfo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAccountInfo});
            // 
            // gridViewAccountInfo
            // 
            this.gridViewAccountInfo.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewAccountInfo.Appearance.GroupPanel.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.gridViewAccountInfo.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewAccountInfo.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewAccountInfo.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewAccountInfo.Appearance.GroupRow.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.gridViewAccountInfo.Appearance.GroupRow.Options.UseFont = true;
            this.gridViewAccountInfo.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridViewAccountInfo.GridControl = this.gridAccountInfo;
            this.gridViewAccountInfo.GroupPanelText = "Account Information";
            this.gridViewAccountInfo.Name = "gridViewAccountInfo";
            this.gridViewAccountInfo.OptionsBehavior.Editable = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowFilter = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowGroup = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridViewAccountInfo.OptionsCustomization.AllowSort = false;
            this.gridViewAccountInfo.OptionsFind.FindDelay = 100;
            this.gridViewAccountInfo.OptionsFind.FindMode = DevExpress.XtraEditors.FindMode.Always;
            this.gridViewAccountInfo.OptionsMenu.EnableColumnMenu = false;
            this.gridViewAccountInfo.OptionsMenu.EnableFooterMenu = false;
            this.gridViewAccountInfo.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewAccountInfo.OptionsView.ShowGroupPanel = false;
            // 
            // validationSupplier
            // 
            this.validationSupplier.ValidateHiddenControls = false;
            // 
            // tabRelation
            // 
            this.tabRelation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabRelation.Location = new System.Drawing.Point(0, 0);
            this.tabRelation.Name = "tabRelation";
            this.tabRelation.SelectedTabPage = this.tabDetail;
            this.tabRelation.Size = new System.Drawing.Size(790, 632);
            this.tabRelation.TabIndex = 1;
            this.tabRelation.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabDetail,
            this.tabAccountInfo,
            this.tabLedger,
            this.tabDocuments});
            // 
            // tabDetail
            // 
            this.tabDetail.Controls.Add(this.layoutControl2);
            this.tabDetail.Name = "tabDetail";
            this.tabDetail.Size = new System.Drawing.Size(784, 604);
            this.tabDetail.Text = "Detail";
            // 
            // tabAccountInfo
            // 
            this.tabAccountInfo.Controls.Add(this.groupControl1);
            this.tabAccountInfo.Name = "tabAccountInfo";
            this.tabAccountInfo.Size = new System.Drawing.Size(784, 604);
            this.tabAccountInfo.Text = "Accounting Information";
            // 
            // tabLedger
            // 
            this.tabLedger.Controls.Add(this.browser);
            this.tabLedger.Controls.Add(this.panelLedgerAccounts);
            this.tabLedger.Name = "tabLedger";
            this.tabLedger.Size = new System.Drawing.Size(784, 604);
            this.tabLedger.Text = "Ledger";
            // 
            // browser
            // 
            this.browser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browser.Location = new System.Drawing.Point(0, 122);
            this.browser.MinimumSize = new System.Drawing.Size(20, 20);
            this.browser.Name = "browser";
            this.browser.Size = new System.Drawing.Size(784, 482);
            this.browser.StyleSheetFile = "berp.css";
            this.browser.TabIndex = 2;
            // 
            // panelLedgerAccounts
            // 
            this.panelLedgerAccounts.BackColor = System.Drawing.Color.Silver;
            this.panelLedgerAccounts.Controls.Add(this.exportButton);
            this.panelLedgerAccounts.Controls.Add(this.radAsPayable);
            this.panelLedgerAccounts.Controls.Add(this.radReceivable);
            this.panelLedgerAccounts.Controls.Add(this.ledgerAccounts);
            this.panelLedgerAccounts.Controls.Add(this.buttonShowLedger);
            this.panelLedgerAccounts.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLedgerAccounts.Location = new System.Drawing.Point(0, 0);
            this.panelLedgerAccounts.Name = "panelLedgerAccounts";
            this.panelLedgerAccounts.Size = new System.Drawing.Size(784, 122);
            this.panelLedgerAccounts.TabIndex = 0;
            // 
            // exportButton
            // 
            this.exportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exportButton.Dock = System.Windows.Forms.DockStyle.None;
            this.exportButton.Location = new System.Drawing.Point(684, 3);
            this.exportButton.Name = "exportButton";
            this.exportButton.ShowBackForward = false;
            this.exportButton.ShowRefresh = false;
            this.exportButton.Size = new System.Drawing.Size(65, 25);
            this.exportButton.TabIndex = 3;
            this.exportButton.Text = "bowserController1";
            // 
            // radAsPayable
            // 
            this.radAsPayable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radAsPayable.AutoSize = true;
            this.radAsPayable.Location = new System.Drawing.Point(594, 84);
            this.radAsPayable.Name = "radAsPayable";
            this.radAsPayable.Size = new System.Drawing.Size(119, 17);
            this.radAsPayable.TabIndex = 5;
            this.radAsPayable.TabStop = true;
            this.radAsPayable.Text = "Account as Payable";
            this.radAsPayable.UseVisualStyleBackColor = true;
            // 
            // radReceivable
            // 
            this.radReceivable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radReceivable.AutoSize = true;
            this.radReceivable.Checked = true;
            this.radReceivable.Location = new System.Drawing.Point(594, 61);
            this.radReceivable.Name = "radReceivable";
            this.radReceivable.Size = new System.Drawing.Size(133, 17);
            this.radReceivable.TabIndex = 5;
            this.radReceivable.TabStop = true;
            this.radReceivable.Text = "Account as Receivable";
            this.radReceivable.UseVisualStyleBackColor = true;
            // 
            // ledgerAccounts
            // 
            this.ledgerAccounts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ledgerAccounts.Location = new System.Drawing.Point(11, 3);
            this.ledgerAccounts.Name = "ledgerAccounts";
            this.ledgerAccounts.Size = new System.Drawing.Size(577, 113);
            this.ledgerAccounts.TabIndex = 4;
            // 
            // buttonShowLedger
            // 
            this.buttonShowLedger.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonShowLedger.Location = new System.Drawing.Point(594, 32);
            this.buttonShowLedger.Name = "buttonShowLedger";
            this.buttonShowLedger.Size = new System.Drawing.Size(75, 23);
            this.buttonShowLedger.TabIndex = 2;
            this.buttonShowLedger.Text = "&Show Ledger";
            this.buttonShowLedger.Click += new System.EventHandler(this.buttonShowLedger_Click);
            // 
            // tabDocuments
            // 
            this.tabDocuments.Controls.Add(this.browserTran);
            this.tabDocuments.Controls.Add(this.panel1);
            this.tabDocuments.Name = "tabDocuments";
            this.tabDocuments.Size = new System.Drawing.Size(784, 604);
            this.tabDocuments.Text = "Transaction Documents";
            // 
            // browserTran
            // 
            this.browserTran.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browserTran.Location = new System.Drawing.Point(0, 94);
            this.browserTran.MinimumSize = new System.Drawing.Size(20, 20);
            this.browserTran.Name = "browserTran";
            this.browserTran.Size = new System.Drawing.Size(784, 510);
            this.browserTran.StyleSheetFile = "berp.css";
            this.browserTran.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.exportButtonTran);
            this.panel1.Controls.Add(this.buttonShowTran);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(784, 94);
            this.panel1.TabIndex = 1;
            // 
            // exportButtonTran
            // 
            this.exportButtonTran.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exportButtonTran.Dock = System.Windows.Forms.DockStyle.None;
            this.exportButtonTran.Location = new System.Drawing.Point(688, 0);
            this.exportButtonTran.Name = "exportButtonTran";
            this.exportButtonTran.ShowBackForward = false;
            this.exportButtonTran.ShowRefresh = false;
            this.exportButtonTran.Size = new System.Drawing.Size(65, 25);
            this.exportButtonTran.TabIndex = 3;
            this.exportButtonTran.Text = "bowserController1";
            // 
            // buttonShowTran
            // 
            this.buttonShowTran.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonShowTran.Location = new System.Drawing.Point(641, 41);
            this.buttonShowTran.Name = "buttonShowTran";
            this.buttonShowTran.Size = new System.Drawing.Size(122, 23);
            this.buttonShowTran.TabIndex = 2;
            this.buttonShowTran.Text = "Show &Transactions";
            this.buttonShowTran.Click += new System.EventHandler(this.buttonShowTran_Click);
            // 
            // RegisterRelation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(790, 632);
            this.Controls.Add(this.tabRelation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RegisterRelation";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Register Relation";
            this.Load += new System.EventHandler(this.RegisterRelation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.memoAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkDontWithhold.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkFinancer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkFinancee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHouseNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKebele.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWoreda.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTaxRegType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTele.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsVatAgent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsWithholdingAgent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVATNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVATAgent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutVATReg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutWithholding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutNonWithholding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridAccountInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAccountInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationSupplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabRelation)).EndInit();
            this.tabRelation.ResumeLayout(false);
            this.tabDetail.ResumeLayout(false);
            this.tabAccountInfo.ResumeLayout(false);
            this.tabLedger.ResumeLayout(false);
            this.panelLedgerAccounts.ResumeLayout(false);
            this.panelLedgerAccounts.PerformLayout();
            this.tabDocuments.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtWebsite;
        private DevExpress.XtraEditors.TextEdit txtRegion;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.TextEdit txtTele;
        private DevExpress.XtraEditors.TextEdit txtEmail;
        private DevExpress.XtraEditors.MemoEdit txtDescription;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider validationSupplier;
        private DevExpress.XtraEditors.TextEdit txtVATNumber;
        private DevExpress.XtraEditors.TextEdit txtTIN;
        private DevExpress.XtraEditors.PictureEdit picLogo;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private DevExpress.XtraEditors.SimpleButton btnBrowse;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gridAccountInfo;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewAccountInfo;
        private DevExpress.XtraEditors.ComboBoxEdit cmbTaxRegType;
        private DevExpress.XtraEditors.TextEdit txtHouseNo;
        private DevExpress.XtraEditors.TextEdit txtKebele;
        private DevExpress.XtraEditors.TextEdit txtWoreda;
        private DevExpress.XtraEditors.TextEdit txtZone;
        private DevExpress.XtraEditors.CheckEdit chkIsVatAgent;
        private DevExpress.XtraEditors.CheckEdit chkIsWithholdingAgent;
        private DevExpress.XtraEditors.CheckEdit checkFinancee;
        private DevExpress.XtraEditors.CheckEdit checkFinancer;
        private DevExpress.XtraEditors.CheckEdit checkSupplier;
        private DevExpress.XtraEditors.CheckEdit checkCustomer;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraEditors.MemoEdit memoAddress;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutVATReg;
        private DevExpress.XtraLayout.LayoutControlItem layoutVATNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutWithholding;
        private DevExpress.XtraLayout.LayoutControlItem layoutVATAgent;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraTab.XtraTabControl tabRelation;
        private DevExpress.XtraTab.XtraTabPage tabDetail;
        private DevExpress.XtraTab.XtraTabPage tabAccountInfo;
        private DevExpress.XtraEditors.CheckEdit chkDontWithhold;
        private DevExpress.XtraLayout.LayoutControlItem layoutNonWithholding;
        private DevExpress.XtraEditors.LabelControl labelCategory;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraTab.XtraTabPage tabLedger;
        private System.Windows.Forms.Panel panelLedgerAccounts;
        private DevExpress.XtraEditors.SimpleButton buttonShowLedger;
        private INTAPS.UI.HTML.ControlBrowser browser;
        private INTAPS.UI.HTML.BowserController exportButton;
        private System.Windows.Forms.FlowLayoutPanel ledgerAccounts;
        private System.Windows.Forms.RadioButton radAsPayable;
        private System.Windows.Forms.RadioButton radReceivable;
        private DevExpress.XtraTab.XtraTabPage tabDocuments;
        private System.Windows.Forms.Panel panel1;
        private INTAPS.UI.HTML.BowserController exportButtonTran;
        private DevExpress.XtraEditors.SimpleButton buttonShowTran;
        private INTAPS.UI.HTML.ControlBrowser browserTran;
    }
}