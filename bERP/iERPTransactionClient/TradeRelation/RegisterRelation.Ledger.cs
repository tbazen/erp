﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.UI.HTML;

namespace BIZNET.iERP.Client
{
    public partial class RegisterRelation:INTAPS.UI.HTML.IHTMLBuilder
    {
        string _ledgerHTML="";
        string _tranHTML= "";
        DebitCreditType _debitCreditType = DebitCreditType.Unknown;
        void updateDebitCreditType()
        {
            DebitCreditType type = DebitCreditType.Unknown;
            foreach (CheckBox chk in ledgerAccounts.Controls)
            {
                if (!chk.Checked)
                    continue;
                Account ac = chk.Tag as Account;
                CostCenterAccount csa = AccountingClient.GetCostCenterAccount(CostCenter.ROOT_COST_CENTER, ac.id);
                if (csa == null)
                    continue;
                DebitCreditType thisDebitCreditType = ac.CreditAccount ? DebitCreditType.Credit : DebitCreditType.Debit;
                if (type == DebitCreditType.Unknown)
                    type = thisDebitCreditType;
                else
                    if (type != thisDebitCreditType)
                        type = DebitCreditType.Mixed;
            }
            _debitCreditType = type;
            switch (type)
            {
                case DebitCreditType.Debit:
                    radAsPayable.Checked = false;
                    radReceivable.Checked = true;
                    radReceivable.Enabled = radAsPayable.Enabled = false;
                    break;
                case DebitCreditType.Credit:
                    radAsPayable.Checked = true;
                    radReceivable.Checked = false;
                    radReceivable.Enabled = radAsPayable.Enabled = false;
                    break;
                default:
                    radReceivable.Enabled = radAsPayable.Enabled = true;
                    radAsPayable.Checked =_relation.IsSupplier;
                    radReceivable.Checked = !_relation.IsSupplier;
                    break;
            }
        }
        void initLedger()
        {
            if (_relation == null)
            {
                tabLedger.Hide();
                tabDocuments.Hide();
            }
            else
            {
                exportButton.SetBrowser(browser);
                setGeneratingMode(false);
                invalidateLedger();
                updateDebitCreditType();

                exportButtonTran.SetBrowser(browserTran);
            }
        }
            void setGeneratingMode(bool generating)
            {
                buttonShowTran.Enabled=exportButtonTran.Enabled= buttonShowLedger.Enabled = exportButton.Enabled = !generating;
            
            }
        enum DebitCreditType
        {
            Unknown,
            Debit,
            Credit,
            Mixed
        }
        private void buttonShowLedger_Click(object sender, EventArgs e)
        {
            browser.LoadTextPage("Ledger", "<h1>Generating ledger please wait...</h1>");
            setGeneratingMode(true);
            List<int> accounts=new List<int>();
            foreach(CheckBox chk in ledgerAccounts.Controls)
            {
                if(!chk.Checked)
                    continue;
                Account ac=chk.Tag as Account;
                accounts.Add(ac.id);
            }
            
            
            bool asCreditAccount=radAsPayable.Checked;
            string accountTitle = (asCreditAccount ? "Payable to " : "Receivable from ") + _relation.NameCode;
            new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(
                delegate(object par)
                {
                    try
                    {
                        _ledgerHTML = AccountExplorer.buildLedger(DateTime.Parse("1/1/1900"), DateTime.Now,CostCenter.ROOT_COST_CENTER, accounts.ToArray(), accountTitle, "Main Cost Center", asCreditAccount, AccountExplorer.ReportType.Ledger,true);
                    }
                    catch(Exception ex)
                    {
                        _ledgerHTML = string.Format("<h1>Error generating ledger</h1><h2>{0}</h2><h3>{1}</h3>", System.Web.HttpUtility.HtmlEncode(ex.Message), System.Web.HttpUtility.HtmlEncode(ex.StackTrace));
                    }
                    this.Invoke(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                        {
                            browser.LoadControl(this);
                            setGeneratingMode(false);
                        }
                    ));
                })).Start(null);
        }

        private void buttonShowTran_Click(object sender, EventArgs e)
        {
            browserTran.LoadTextPage("Ledger", "<h1>Loading trasaction please wait, it might take time...</h1>");
            setGeneratingMode(true);
            

            string accountTitle = "Transactions of "+_relation.NameCode;
            new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(
                delegate(object par)
                {
                    HTMLPlaceHodler p;
                    try
                    {
                        p=new HTMLPlaceHodler(){parent=this,html=iERPTransactionClient.getTradeRelationTransactions(_relation.Code, DateTime.Parse("1/1/1900").Ticks, DateTime.Now.Ticks, null)};
                    }
                    catch (Exception ex)
                    {
                        p = new HTMLPlaceHodler() { parent = this, html = string.Format("<h1>Error generating transactions</h1><h2>{0}</h2><h3>{1}</h3>", System.Web.HttpUtility.HtmlEncode(ex.Message), System.Web.HttpUtility.HtmlEncode(ex.StackTrace)) };
                    }
                    this.Invoke(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                    {
                        browserTran.LoadControl(p);
                        setGeneratingMode(false);
                    }
                    ));
                })).Start(null);
        }
        void invalidateLedger()
        {
            browser.LoadTextPage("Ledger", "<h1>Click show ledger to see the leger..</h1>");
        }
        void legerAccountChecked(object sender, EventArgs e)
        {
            invalidateLedger();
            updateDebitCreditType();
        }

        public void Build()
        {
            
        }

        public string GetOuterHtml()
        {
            return _ledgerHTML;
        }
        void realoadLedger()
        {
            invalidateLedger();
        }
        public bool ProcessUrl(string path, System.Collections.Hashtable query)
        {
            return AccountExplorer.ProcessUrl(this, path, query,new INTAPS.UI.HTML.ProcessParameterless(realoadLedger));
        }

        public void SetHost(INTAPS.UI.HTML.IHTMLDocumentHost host)
        {
            
        }

        public string WindowCaption
        {
            get { return "Ledger"; }
        }
        class HTMLPlaceHodler : IHTMLBuilder
        {
            public string html;
            public RegisterRelation parent;
            public void Build()
            {
                parent.Build();
            }

            public string GetOuterHtml()
            {
                return html;
            }

            public bool ProcessUrl(string path, System.Collections.Hashtable query)
            {
                return parent.ProcessUrl(path, query);
            }

            public void SetHost(IHTMLDocumentHost host)
            {
                parent.SetHost(host);
            }

            public string WindowCaption
            {
                get { return "Report"; }
            }
        }
    }
}
