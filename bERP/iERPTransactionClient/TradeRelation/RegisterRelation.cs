﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.IO;
using System.Drawing.Imaging;
using DevExpress.XtraGrid;
using DevExpress.XtraLayout.Utils;
using DevExpress.Utils;
using INTAPS.UI.ButtonGrid;

namespace BIZNET.iERP.Client
{
    public partial class RegisterRelation : DevExpress.XtraEditors.XtraForm
    {
        private string _relationCode;

        public string RelationCode
        {
            get { return _relationCode; }

        }
        private DataTable accountInfoTable;
        private TradeRelation _relation;
        public delegate void RelationAddedHandler(string supplierCode);
        public event RelationAddedHandler RelationAdded;
        string _pcode;
        public RegisterRelation(string code)
            : this(null, code)
        {
            
        }
        public RegisterRelation(string pcode,string trCode)
        {
            InitializeComponent();
            
            picLogo.Image = Properties.Resources.Supplier;
            txtName.LostFocus += Control_LostFocus;
            HideWithholdControlLayout();
            if (trCode == null)
            {
                _pcode = pcode;
            }
            else
            {
                _relationCode = trCode;
                accountInfoTable = new DataTable();
                PrepareAccountInfoTable();
                TradeRelation relation = iERPTransactionClient.GetRelation(_relationCode);
                _pcode = relation.pcode;
                if (relation != null)
                {
                    _relation = relation;
                    LoadForUpdate(_relation);
                    DisplayAccountInformation(_relation);
                }
            }
            if (string.IsNullOrEmpty(_pcode))
                labelCategory.Text = "";
            else
                labelCategory.Text = "Category: " + iERPTransactionClient.getTradeRelationCategory(_pcode).nameCode;
            initLedger();

        }

        private void HideWithholdControlLayout()
        {
            CompanyProfile profile = iERPTransactionClient.GetSystemParamter("companyProfile") as CompanyProfile;
            if (profile != null)
            {
                if (!profile.isWitholdingAgent)
                    layoutWithholding.HideToCustomization();
            }
            else
            {
                MessageBox.ShowErrorMessage("Please configure Company Profile frist!");
            }
        }
        public RegisterRelation(string pcode,TradeRelationType t)
            : this(pcode,null)
        {
            checkSupplier.Checked = t == TradeRelationType.Supplier;
            checkCustomer.Checked = t==TradeRelationType.Customer;
        }
        

        private void PrepareAccountInfoTable()
        {
            accountInfoTable.Columns.Add("Account Code", typeof(string));
            accountInfoTable.Columns.Add("Name", typeof(string));
            accountInfoTable.Columns.Add("Account Type", typeof(string));
            accountInfoTable.Columns.Add("Balance", typeof(double));
            accountInfoTable.Columns.Add("Activated", typeof(bool));
            gridAccountInfo.DataSource = accountInfoTable;
            gridViewAccountInfo.Columns["Activated"].Visible = false;
            gridViewAccountInfo.Columns["Name"].Visible = false;
            gridViewAccountInfo.Columns["Balance"].DisplayFormat.FormatType = FormatType.Numeric;
            gridViewAccountInfo.Columns["Balance"].DisplayFormat.FormatString = "N";
        }


        private void LoadForUpdate(TradeRelation relation)
        {
            picLogo.Image = relation.IsSupplier ? Properties.Resources.Supplier: Properties.Resources.customers;
            if (relation.Logo != null)
            {
                using (MemoryStream stream = new MemoryStream(relation.Logo))
                {
                    picLogo.Image = Image.FromStream(stream);
                }
            }
            txtRegion.Text = relation.Region;
            txtZone.Text = relation.Zone;
            txtWoreda.Text = relation.Woreda;
            txtKebele.Text = relation.Kebele;
            txtHouseNo.Text = relation.HouseNumber;
            txtDescription.Text = relation.Description;
            txtEmail.Text = relation.Email;
            txtName.Text = relation.Name;
            txtTIN.Text = relation.TIN;
            txtVATNumber.Text = relation.VATRegNumber;
            txtTele.Text = relation.Telephone;
            txtWebsite.Text = relation.Website;
            cmbTaxRegType.SelectedIndex = (int)relation.TaxRegistrationType;
            checkCustomer.Checked = relation.IsCustomer;
            checkSupplier.Checked = relation.IsSupplier;
            checkFinancer.Checked = relation.IsFinancer;
            checkFinancee.Checked = relation.IsFinancee;
            chkIsVatAgent.Checked = relation.IsVatAgent;
            chkIsWithholdingAgent.Checked = relation.IsWithholdingAgent;
            chkDontWithhold.Checked = !relation.Withhold;
            memoAddress.Text = relation.Address;
        }
        private void DisplayAccountInformation(TradeRelation relation)
        {
            try
            {
                accountInfoTable.Clear();
                ledgerAccounts.Controls.Clear();

                foreach (System.Reflection.FieldInfo fi in typeof(TradeRelation).GetFields())
                {
                    object[] atr = fi.GetCustomAttributes(typeof(AIChildOfAttribute),true);
                    if (atr==null || atr.Length == 0)
                        continue;
                    AIChildOfAttribute a = (AIChildOfAttribute)atr[0];
                    AccountBase account = a.isCostCenter? (AccountBase)INTAPS.Accounting.Client.AccountingClient.GetAccount<CostCenter>((int)fi.GetValue(relation)):INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>((int)fi.GetValue(relation));
                    if (account == null)
                        continue;
                    DataRow accountRow = accountInfoTable.NewRow();
                    accountRow[0] = account.Code;
                    accountRow[1] = account.Name;
                    accountRow[2] = a.displayName;
                    if (a.isCostCenter)
                        accountRow[3] = 0;
                    else
                        accountRow[3] = INTAPS.Accounting.Client.AccountingClient.GetNetCostCenterAccountBalanceAsOf(iERPTransactionClient.mainCostCenterID, account.id, DateTime.Now);
                    accountRow[4] = account.Status == AccountStatus.Activated ? true : false;
                    accountInfoTable.Rows.Add(accountRow);
                    if (!a.isCostCenter)
                    {
                        //add to the ledger selector
                        CheckBox chk = new CheckBox();
                        chk.Text = account.NameCode;
                        chk.Tag = account;
                        chk.Checked = true;
                        chk.AutoSize = true;
                        chk.CheckedChanged += new EventHandler(legerAccountChecked);
                        ledgerAccounts.Controls.Add(chk);
                    }
                }
                gridAccountInfo.DataSource = accountInfoTable;
                gridAccountInfo.RefreshDataSource();
                gridViewAccountInfo.ExpandAllGroups();
                ApplyAccountInfoFormatConditioning();
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

       

        private void ApplyAccountInfoFormatConditioning()
        {
            StyleFormatCondition condition = new StyleFormatCondition();
            condition.Appearance.Options.UseBackColor = true;
            condition.Appearance.BackColor = Color.FromArgb(0x8F, 0xF2, 0x14, 0x43);
            condition.Condition = FormatConditionEnum.Expression;
            condition.Expression = "[Activated] != True";
            condition.ApplyToRow = true;
            gridViewAccountInfo.FormatConditions.Add(condition);
        }

        void Control_LostFocus(object sender, EventArgs e)
        {
            validationSupplier.Validate((Control)sender);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validationSupplier.Validate())
                {
                    MessageBox.ShowErrorMessage("Please check the errors marked in red and try again!");
                    return;
                }
                TradeRelation relation = new TradeRelation();
                if (_relation != null)
                    relation = _relation;
                if (picLogo.Image != null)
                {
                    using (MemoryStream stream = new MemoryStream())
                    {
                        int newWidth=128, newHeight=128;
                        if (picLogo.Image.Width > picLogo.Image.Height)
                        {
                            newWidth = 128;
                            newHeight = 128 * picLogo.Image.Height / picLogo.Image.Width;
                        }
                        else if (picLogo.Image.Width < picLogo.Image.Height)
                        {
                            newHeight = 128;
                            newWidth = 128 * picLogo.Image.Width / picLogo.Image.Height;
                        }
                        using (Bitmap newPic = new Bitmap(picLogo.Image,new Size(newWidth,newHeight)))
                        {
                            newPic.Save(stream, ImageFormat.Jpeg);
                        }
                        relation.Logo = stream.ToArray();
                    }
                }
                relation.pcode = _pcode;
                relation.Code = _relationCode;
                relation.Name = txtName.Text;
                relation.TIN = txtTIN.Text;
                relation.VATRegNumber = txtVATNumber.Text;
                relation.Region = txtRegion.Text;
                relation.Zone = txtZone.Text;
                relation.Woreda = txtWoreda.Text;
                relation.Kebele = txtKebele.Text;
                relation.HouseNumber = txtHouseNo.Text;
                relation.Description = txtDescription.Text;
                relation.Email = txtEmail.Text;
                relation.Telephone = txtTele.Text;
                relation.Website = txtWebsite.Text;
                relation.TaxRegistrationType = (TaxRegisrationType)cmbTaxRegType.SelectedIndex;

                relation.IsSupplier = checkSupplier.Checked;
                relation.IsCustomer = checkCustomer.Checked;
                relation.IsFinancer = checkFinancer.Checked;
                relation.IsFinancee = checkFinancee.Checked;
                relation.IsVatAgent = chkIsVatAgent.Checked;
                relation.IsWithholdingAgent = chkIsWithholdingAgent.Checked;
                relation.Withhold = !chkDontWithhold.Checked;
                relation.Address = memoAddress.Text;
                string relationCode = iERPTransactionClient.RegisterRelation(relation);
                string message = _relationCode == null ? "Relation successfully saved!" : "Relation successfully updated";
                MessageBox.ShowSuccessMessage(message);
                if (relation.Code == null)
                {
                    if (RelationAdded != null)
                        RelationAdded(relationCode);
                    ResetControls();
                    RefreshMainButtonGrid();

                }
                else
                {
                    this.DialogResult = DialogResult.OK;
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error occurred while trying to create relation\n" + ex.Message);
            }

            
        }

        private static void RefreshMainButtonGrid()
        {
            foreach (Control control in INTAPS.UI.UIFormApplicationBase.MainForm.Controls)
            {
                if (control is SplitContainerControl)
                {
                    Control buttonGridControl = ((SplitContainerControl)control).Panel1.Controls[0];
                    ((ButtonGrid)buttonGridControl).UpdateLayoutInPlace();
                }
            }
        }
        private void ResetControls()
        {
            txtName.Text = "";
            txtTIN.Text = "";
            txtVATNumber.Text = "";
            txtRegion.Text = "";
            txtDescription.Text = "";
            txtEmail.Text = "";
            txtWebsite.Text = "";
            txtTele.Text = "";
            txtZone.Text = txtWoreda.Text = "";
            txtKebele.Text = txtHouseNo.Text = "";
            cmbTaxRegType.SelectedIndex = 0;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            picLogo.LoadImage();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            picLogo.Image = Properties.Resources.Supplier;
        }

        private void cmbTaxRegType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTaxRegType.SelectedIndex == 1 || cmbTaxRegType.SelectedIndex == 2)
            {
                layoutVATNumber.HideToCustomization();
            }
            else
            {
                layoutVATNumber.RestoreFromCustomization(layoutVATReg, InsertType.Bottom);
            }
        }

        private void chkIsWithholdingAgent_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkCustomer_CheckedChanged(object sender, EventArgs e)
        {
            if(checkCustomer.Checked)
            {
                if (!checkSupplier.Checked)
                {
                    layoutNonWithholding.HideToCustomization();
                    chkDontWithhold.Checked = false;
                }
                if(layoutWithholding.IsHidden)
                    layoutWithholding.RestoreFromCustomization(layoutVATReg, InsertType.Bottom);
                if(layoutVATAgent.IsHidden)
                    layoutVATAgent.RestoreFromCustomization(layoutWithholding, InsertType.Bottom);
            }
            else
            {
                layoutWithholding.HideToCustomization();
                layoutVATAgent.HideToCustomization();
            }
        }

        private void checkSupplier_CheckedChanged(object sender, EventArgs e)
        {
            if (checkSupplier.Checked)
            {
                if (!checkCustomer.Checked)
                {
                    layoutWithholding.HideToCustomization();
                    layoutVATAgent.HideToCustomization();
                    chkIsWithholdingAgent.Checked = chkIsVatAgent.Checked = false;
                }
                if (layoutNonWithholding.IsHidden)
                    layoutNonWithholding.RestoreFromCustomization(layoutVATReg, InsertType.Bottom);
            }
            else
            {
                layoutNonWithholding.HideToCustomization();
            }
        }

        private void RegisterRelation_Load(object sender, EventArgs e)
        {

        }
       
    }
}