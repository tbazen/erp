﻿namespace BIZNET.iERP.Client
{
    partial class RegisterRelation2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelCategory = new System.Windows.Forms.Label();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.checkSupplier = new System.Windows.Forms.CheckBox();
            this.checkCustomer = new System.Windows.Forms.CheckBox();
            this.checkFinancer = new System.Windows.Forms.CheckBox();
            this.checkFinancee = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.labelTin = new System.Windows.Forms.Label();
            this.txtTIN = new System.Windows.Forms.TextBox();
            this.labelVatNo = new System.Windows.Forms.Label();
            this.txtVATNumber = new System.Windows.Forms.TextBox();
            this.labelTaxReg = new System.Windows.Forms.Label();
            this.cmbTaxRegType = new System.Windows.Forms.ComboBox();
            this.chkDontWithhold = new System.Windows.Forms.CheckBox();
            this.chkIsWithholdingAgent = new System.Windows.Forms.CheckBox();
            this.chkIsVatAgent = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTele = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtWebsite = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtRegion = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtZone = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtWoreda = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtKebele = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtHouseNo = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.memoAddress = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.validationSupplier = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBoxSetting = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbCounty = new System.Windows.Forms.ComboBox();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationSupplier)).BeginInit();
            this.groupBoxSetting.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelCategory
            // 
            this.labelCategory.AutoSize = true;
            this.labelCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCategory.Location = new System.Drawing.Point(13, 13);
            this.labelCategory.Name = "labelCategory";
            this.labelCategory.Size = new System.Drawing.Size(47, 13);
            this.labelCategory.TabIndex = 0;
            this.labelCategory.Text = "#####";
            // 
            // picLogo
            // 
            this.picLogo.Location = new System.Drawing.Point(16, 29);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(179, 141);
            this.picLogo.TabIndex = 1;
            this.picLogo.TabStop = false;
            // 
            // checkSupplier
            // 
            this.checkSupplier.AutoSize = true;
            this.checkSupplier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkSupplier.Location = new System.Drawing.Point(223, 29);
            this.checkSupplier.Name = "checkSupplier";
            this.checkSupplier.Size = new System.Drawing.Size(61, 17);
            this.checkSupplier.TabIndex = 2;
            this.checkSupplier.Text = "Supplier";
            this.checkSupplier.UseVisualStyleBackColor = true;
            this.checkSupplier.CheckedChanged += new System.EventHandler(this.checkSupplier_CheckedChanged);
            // 
            // checkCustomer
            // 
            this.checkCustomer.AutoSize = true;
            this.checkCustomer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkCustomer.Location = new System.Drawing.Point(223, 52);
            this.checkCustomer.Name = "checkCustomer";
            this.checkCustomer.Size = new System.Drawing.Size(67, 17);
            this.checkCustomer.TabIndex = 2;
            this.checkCustomer.Text = "Customer";
            this.checkCustomer.UseVisualStyleBackColor = true;
            this.checkCustomer.CheckedChanged += new System.EventHandler(this.checkCustomer_CheckedChanged);
            // 
            // checkFinancer
            // 
            this.checkFinancer.AutoSize = true;
            this.checkFinancer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkFinancer.Location = new System.Drawing.Point(223, 75);
            this.checkFinancer.Name = "checkFinancer";
            this.checkFinancer.Size = new System.Drawing.Size(59, 17);
            this.checkFinancer.TabIndex = 2;
            this.checkFinancer.Text = "Creditor";
            this.checkFinancer.UseVisualStyleBackColor = true;
            // 
            // checkFinancee
            // 
            this.checkFinancee.AutoSize = true;
            this.checkFinancee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkFinancee.Location = new System.Drawing.Point(223, 98);
            this.checkFinancee.Name = "checkFinancee";
            this.checkFinancee.Size = new System.Drawing.Size(57, 17);
            this.checkFinancee.TabIndex = 2;
            this.checkFinancee.Text = "Debitor";
            this.checkFinancee.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(370, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Name:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(414, 26);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(334, 20);
            this.txtName.TabIndex = 4;
            // 
            // labelTin
            // 
            this.labelTin.AutoSize = true;
            this.labelTin.Location = new System.Drawing.Point(380, 57);
            this.labelTin.Name = "labelTin";
            this.labelTin.Size = new System.Drawing.Size(28, 13);
            this.labelTin.TabIndex = 3;
            this.labelTin.Text = "TIN:";
            // 
            // txtTIN
            // 
            this.txtTIN.Location = new System.Drawing.Point(414, 53);
            this.txtTIN.Name = "txtTIN";
            this.txtTIN.Size = new System.Drawing.Size(334, 20);
            this.txtTIN.TabIndex = 4;
            // 
            // labelVatNo
            // 
            this.labelVatNo.AutoSize = true;
            this.labelVatNo.Location = new System.Drawing.Point(337, 112);
            this.labelVatNo.Name = "labelVatNo";
            this.labelVatNo.Size = new System.Drawing.Size(71, 13);
            this.labelVatNo.TabIndex = 3;
            this.labelVatNo.Text = "VAT Number:";
            // 
            // txtVATNumber
            // 
            this.txtVATNumber.Location = new System.Drawing.Point(414, 108);
            this.txtVATNumber.Name = "txtVATNumber";
            this.txtVATNumber.Size = new System.Drawing.Size(256, 20);
            this.txtVATNumber.TabIndex = 4;
            // 
            // labelTaxReg
            // 
            this.labelTaxReg.AutoSize = true;
            this.labelTaxReg.Location = new System.Drawing.Point(321, 84);
            this.labelTaxReg.Name = "labelTaxReg";
            this.labelTaxReg.Size = new System.Drawing.Size(87, 13);
            this.labelTaxReg.TabIndex = 3;
            this.labelTaxReg.Text = "Tax Registration:";
            // 
            // cmbTaxRegType
            // 
            this.cmbTaxRegType.FormattingEnabled = true;
            this.cmbTaxRegType.Items.AddRange(new object[] {
            "VAT (Value Added Tax)",
            "TOT (Turn Over Tax)",
            "Non-VAT & TOT Tax Payer"});
            this.cmbTaxRegType.Location = new System.Drawing.Point(414, 80);
            this.cmbTaxRegType.Name = "cmbTaxRegType";
            this.cmbTaxRegType.Size = new System.Drawing.Size(256, 21);
            this.cmbTaxRegType.TabIndex = 5;
            this.cmbTaxRegType.Text = "VAT (Value Added Tax)";
            this.cmbTaxRegType.SelectedIndexChanged += new System.EventHandler(this.cmbTaxRegType_SelectedIndexChanged);
            // 
            // chkDontWithhold
            // 
            this.chkDontWithhold.AutoSize = true;
            this.chkDontWithhold.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkDontWithhold.Location = new System.Drawing.Point(12, 18);
            this.chkDontWithhold.Name = "chkDontWithhold";
            this.chkDontWithhold.Size = new System.Drawing.Size(100, 17);
            this.chkDontWithhold.TabIndex = 2;
            this.chkDontWithhold.Text = "Do not Withhold";
            this.chkDontWithhold.UseVisualStyleBackColor = true;
            this.chkDontWithhold.Visible = false;
            // 
            // chkIsWithholdingAgent
            // 
            this.chkIsWithholdingAgent.AutoSize = true;
            this.chkIsWithholdingAgent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkIsWithholdingAgent.Location = new System.Drawing.Point(121, 18);
            this.chkIsWithholdingAgent.Name = "chkIsWithholdingAgent";
            this.chkIsWithholdingAgent.Size = new System.Drawing.Size(110, 17);
            this.chkIsWithholdingAgent.TabIndex = 2;
            this.chkIsWithholdingAgent.Text = "Withholding Agent";
            this.chkIsWithholdingAgent.UseVisualStyleBackColor = true;
            this.chkIsWithholdingAgent.Visible = false;
            // 
            // chkIsVatAgent
            // 
            this.chkIsVatAgent.AutoSize = true;
            this.chkIsVatAgent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkIsVatAgent.Location = new System.Drawing.Point(240, 18);
            this.chkIsVatAgent.Name = "chkIsVatAgent";
            this.chkIsVatAgent.Size = new System.Drawing.Size(75, 17);
            this.chkIsVatAgent.TabIndex = 2;
            this.chkIsVatAgent.Text = "VAT Agent";
            this.chkIsVatAgent.UseVisualStyleBackColor = true;
            this.chkIsVatAgent.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Phone Number:";
            // 
            // txtTele
            // 
            this.txtTele.Location = new System.Drawing.Point(90, 23);
            this.txtTele.Name = "txtTele";
            this.txtTele.Size = new System.Drawing.Size(273, 20);
            this.txtTele.TabIndex = 4;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(90, 49);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(273, 20);
            this.txtEmail.TabIndex = 4;
            // 
            // txtWebsite
            // 
            this.txtWebsite.Location = new System.Drawing.Point(91, 75);
            this.txtWebsite.Name = "txtWebsite";
            this.txtWebsite.Size = new System.Drawing.Size(273, 20);
            this.txtWebsite.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(41, 132);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Region:";
            // 
            // txtRegion
            // 
            this.txtRegion.Location = new System.Drawing.Point(90, 128);
            this.txtRegion.Name = "txtRegion";
            this.txtRegion.Size = new System.Drawing.Size(273, 20);
            this.txtRegion.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(367, 132);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Zone/Subcity:";
            // 
            // txtZone
            // 
            this.txtZone.Location = new System.Drawing.Point(447, 128);
            this.txtZone.Name = "txtZone";
            this.txtZone.Size = new System.Drawing.Size(273, 20);
            this.txtZone.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(37, 158);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Woreda:";
            // 
            // txtWoreda
            // 
            this.txtWoreda.Location = new System.Drawing.Point(90, 154);
            this.txtWoreda.Name = "txtWoreda";
            this.txtWoreda.Size = new System.Drawing.Size(273, 20);
            this.txtWoreda.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(399, 158);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Kebele:";
            // 
            // txtKebele
            // 
            this.txtKebele.Location = new System.Drawing.Point(447, 154);
            this.txtKebele.Name = "txtKebele";
            this.txtKebele.Size = new System.Drawing.Size(273, 20);
            this.txtKebele.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(27, 184);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "House No:";
            // 
            // txtHouseNo
            // 
            this.txtHouseNo.Location = new System.Drawing.Point(90, 180);
            this.txtHouseNo.Name = "txtHouseNo";
            this.txtHouseNo.Size = new System.Drawing.Size(273, 20);
            this.txtHouseNo.TabIndex = 4;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(37, 206);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "Address:";
            // 
            // memoAddress
            // 
            this.memoAddress.Location = new System.Drawing.Point(90, 206);
            this.memoAddress.Multiline = true;
            this.memoAddress.Name = "memoAddress";
            this.memoAddress.Size = new System.Drawing.Size(625, 54);
            this.memoAddress.TabIndex = 4;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(53, 468);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(47, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Remark:";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(107, 465);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(641, 54);
            this.txtDescription.TabIndex = 4;
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.Color.LightBlue;
            this.btnOk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Image = global::BIZNET.iERP.Client.Properties.Resources.save;
            this.btnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOk.Location = new System.Drawing.Point(531, 534);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(97, 32);
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "Save";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.LightBlue;
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Image = global::BIZNET.iERP.Client.Properties.Resources.cancel;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(652, 534);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(97, 32);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // validationSupplier
            // 
            this.validationSupplier.ContainerControl = this;
            // 
            // groupBoxSetting
            // 
            this.groupBoxSetting.Controls.Add(this.chkDontWithhold);
            this.groupBoxSetting.Controls.Add(this.chkIsWithholdingAgent);
            this.groupBoxSetting.Controls.Add(this.chkIsVatAgent);
            this.groupBoxSetting.Location = new System.Drawing.Point(414, 135);
            this.groupBoxSetting.Name = "groupBoxSetting";
            this.groupBoxSetting.Size = new System.Drawing.Size(334, 50);
            this.groupBoxSetting.TabIndex = 8;
            this.groupBoxSetting.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbCounty);
            this.groupBox2.Controls.Add(this.txtCity);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtTele);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtWebsite);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtEmail);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtRegion);
            this.groupBox2.Controls.Add(this.txtKebele);
            this.groupBox2.Controls.Add(this.memoAddress);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtHouseNo);
            this.groupBox2.Controls.Add(this.txtZone);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.txtWoreda);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(16, 191);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(732, 270);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Contact Information";
            // 
            // cmbCounty
            // 
            this.cmbCounty.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCounty.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCounty.FormattingEnabled = true;
            this.cmbCounty.Location = new System.Drawing.Point(91, 101);
            this.cmbCounty.Name = "cmbCounty";
            this.cmbCounty.Size = new System.Drawing.Size(272, 21);
            this.cmbCounty.TabIndex = 6;
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(447, 101);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(273, 20);
            this.txtCity.TabIndex = 4;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(415, 105);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(27, 13);
            this.label15.TabIndex = 3;
            this.label15.Text = "City:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Country:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(36, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Website:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(50, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Email:";
            // 
            // btnBrowse
            // 
            this.btnBrowse.AutoSize = true;
            this.btnBrowse.Location = new System.Drawing.Point(16, 155);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(80, 13);
            this.btnBrowse.TabIndex = 10;
            this.btnBrowse.TabStop = true;
            this.btnBrowse.Text = "Change Picture";
            this.btnBrowse.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnBrowse_LinkClicked);
            // 
            // RegisterRelation2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(773, 577);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBoxSetting);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.cmbTaxRegType);
            this.Controls.Add(this.labelTaxReg);
            this.Controls.Add(this.txtVATNumber);
            this.Controls.Add(this.labelVatNo);
            this.Controls.Add(this.txtTIN);
            this.Controls.Add(this.labelTin);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkFinancee);
            this.Controls.Add(this.checkFinancer);
            this.Controls.Add(this.checkCustomer);
            this.Controls.Add(this.checkSupplier);
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.labelCategory);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "RegisterRelation2";
            this.Text = "Register Relation";
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validationSupplier)).EndInit();
            this.groupBoxSetting.ResumeLayout(false);
            this.groupBoxSetting.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelCategory;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.CheckBox checkSupplier;
        private System.Windows.Forms.CheckBox checkCustomer;
        private System.Windows.Forms.CheckBox checkFinancer;
        private System.Windows.Forms.CheckBox checkFinancee;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label labelTin;
        private System.Windows.Forms.TextBox txtTIN;
        private System.Windows.Forms.Label labelVatNo;
        private System.Windows.Forms.TextBox txtVATNumber;
        private System.Windows.Forms.Label labelTaxReg;
        private System.Windows.Forms.ComboBox cmbTaxRegType;
        private System.Windows.Forms.CheckBox chkDontWithhold;
        private System.Windows.Forms.CheckBox chkIsWithholdingAgent;
        private System.Windows.Forms.CheckBox chkIsVatAgent;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTele;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtWebsite;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtRegion;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtZone;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtWoreda;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtKebele;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtHouseNo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox memoAddress;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ErrorProvider validationSupplier;
        private System.Windows.Forms.GroupBox groupBoxSetting;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.LinkLabel btnBrowse;
        private System.Windows.Forms.ComboBox cmbCounty;
    }
}