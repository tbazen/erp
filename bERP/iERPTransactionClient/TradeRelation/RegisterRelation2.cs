﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
using INTAPS.UI.ButtonGrid;

namespace BIZNET.iERP.Client
{
    public partial class RegisterRelation2 : Form
    {
        private string _relationCode;
        public string RelationCode
        {
            get { return _relationCode; }

        }
        private TradeRelation _relation;
        public delegate void RelationAddedHandler(string supplierCode);
        public event RelationAddedHandler RelationAdded;
        string _pcode;
        private bool _isInternational;

        public RegisterRelation2(string pcode, string trCode, bool isInternational)
        {
            InitializeComponent();
            this._isInternational = isInternational;
            
            PopulateCountries();
            picLogo.Image = Properties.Resources.Supplier;
            HideWithholdControlLayout();
            if (isInternational)
                ShowHideControls(true);
            if (trCode == null)
            {
                _pcode = pcode;
                Text = (this._isInternational) ? "Register Relation (INTERNATIONAL)" : "Register Relation (LOCAL)";
            }
            else
            {
                Text = (this._isInternational) ? "Edit Relation (INTERNATIONAL)" : "Edit Relation (LOCAL)";
                _relationCode = trCode;
                TradeRelation relation = iERPTransactionClient.GetRelation(_relationCode);
                _pcode = relation.pcode;
                _relation = relation;
                LoadForUpdate(_relation);
            }
            if (string.IsNullOrEmpty(_pcode))
                labelCategory.Text = "";
            else
                labelCategory.Text = "Category: " + iERPTransactionClient.getTradeRelationCategory(_pcode).nameCode;
            
        }

        private void ShowHideControls(bool isInternational)
        {
            labelTin.Visible = !isInternational;
            txtTIN.Visible = !isInternational;
            labelTaxReg.Visible = !isInternational;
            cmbTaxRegType.Visible = !isInternational;
            labelVatNo.Visible = !isInternational;
            txtVATNumber.Visible = !isInternational;
            groupBoxSetting.Visible = !isInternational;
            checkCustomer.Enabled = checkFinancee.Enabled = checkFinancer.Enabled = !isInternational;
            checkSupplier.Checked = isInternational;
            groupBoxSetting.Visible = isInternational;
        }
        
        private void HideWithholdControlLayout()
        {
            CompanyProfile profile = iERPTransactionClient.GetSystemParamter("companyProfile") as CompanyProfile;
            if (profile != null)
            {
                if (!profile.isWitholdingAgent)
                    chkIsWithholdingAgent.Visible = false;
            }
            else
            {
                MessageBox.ShowErrorMessage("Please configure Company Profile frist!");
            }
        }

        private void PopulateCountries()
        {
            cmbCounty.Items.Clear();
            Country[] countries = iERPTransactionClient.GetCountries();
            foreach (Country t in countries)
            {
                cmbCounty.Items.Add(t);
            }
           
        }
        
        private void LoadForUpdate(TradeRelation relation)
        {
            picLogo.Image = relation.IsSupplier ? Properties.Resources.Supplier : Properties.Resources.customers;
            if (relation.Logo != null)
            {
                using (MemoryStream stream = new MemoryStream(relation.Logo))
                {
                    picLogo.Image = Image.FromStream(stream);
                }
            }
            if (_isInternational)
            {
                Country country = iERPTransactionClient.GetCountryById(relation.Country);
                cmbCounty.SelectedIndex = cmbCounty.FindString(country.Name);
            }
            else
            {
                Country country = iERPTransactionClient.GetCountryById(67); //Ethiopia
                cmbCounty.SelectedIndex = cmbCounty.FindString(country.Name);
                cmbCounty.Enabled = false;
            }
            
            txtCity.Text = relation.City;
            txtRegion.Text = relation.Region;
            txtZone.Text = relation.Zone;
            txtWoreda.Text = relation.Woreda;
            txtKebele.Text = relation.Kebele;
            txtHouseNo.Text = relation.HouseNumber;
            txtDescription.Text = relation.Description;
            txtEmail.Text = relation.Email;
            txtName.Text = relation.Name;
            txtTIN.Text = relation.TIN;
            txtVATNumber.Text = relation.VATRegNumber;
            txtTele.Text = relation.Telephone;
            txtWebsite.Text = relation.Website;
            cmbTaxRegType.SelectedIndex = (int)relation.TaxRegistrationType;
            checkCustomer.Checked = relation.IsCustomer;
            checkSupplier.Checked = relation.IsSupplier;
            checkFinancer.Checked = relation.IsFinancer;
            checkFinancee.Checked = relation.IsFinancee;
            chkIsVatAgent.Checked = relation.IsVatAgent;
            chkIsWithholdingAgent.Checked = relation.IsWithholdingAgent;
            chkDontWithhold.Checked = !relation.Withhold;
            memoAddress.Text = relation.Address;
        }

        private bool ValidateSupplierForm()
        {
            if (string.IsNullOrEmpty(txtName.Text))
            {
                validationSupplier.SetError(txtName, "Please specify name");
                return true;
            }
            validationSupplier.SetError(txtName, "");
            if (!this._isInternational)
            {
                if (string.IsNullOrEmpty(txtTIN.Text))
                {
                    validationSupplier.SetError(txtTIN, "Please specify TIN");
                    return true;
                }
                validationSupplier.SetError(txtTIN, "");

                if (cmbTaxRegType.SelectedItem == null)
                {
                    validationSupplier.SetError(cmbTaxRegType, "Please specify Tax Reg. type");
                    return true;
                }
                validationSupplier.SetError(cmbTaxRegType, "");
            }
            
            return false;
        }
        
        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateSupplierForm())
                {
                    MessageBox.ShowErrorMessage("Please check the errors marked in red and try again!");
                    return;
                }
                TradeRelation relation = new TradeRelation();
                if (_relation != null)
                    relation = _relation;
                if (picLogo.Image != null)
                {
                    using (MemoryStream stream = new MemoryStream())
                    {
//                        int newWidth = 128, newHeight = 128;
//                        if (picLogo.Image.Width > picLogo.Image.Height)
//                        {
//                            newWidth = 128;
//                            newHeight = 128 * picLogo.Image.Height / picLogo.Image.Width;
//                        }
//                        else if (picLogo.Image.Width < picLogo.Image.Height)
//                        {
//                            newHeight = 128;
//                            newWidth = 128 * picLogo.Image.Width / picLogo.Image.Height;
//                        }
                        using (Bitmap newPic = new Bitmap(picLogo.Image))
                        {
                            newPic.Save(stream, ImageFormat.Jpeg);
                        }
                        relation.Logo = stream.ToArray();
                    }
                }
                relation.pcode = _pcode;
                relation.Code = _relationCode;
                relation.Name = txtName.Text;
                relation.TIN = txtTIN.Text;
                relation.VATRegNumber = txtVATNumber.Text;
                relation.IsInternational = _isInternational;
                relation.Country = _isInternational ? ((Country)cmbCounty.SelectedItem).Id : (short)67;
                relation.City = txtCity.Text;
                relation.Region = txtRegion.Text;
                relation.Zone = txtZone.Text;
                relation.Woreda = txtWoreda.Text;
                relation.Kebele = txtKebele.Text;
                relation.HouseNumber = txtHouseNo.Text;
                relation.Description = txtDescription.Text;
                relation.Email = txtEmail.Text;
                relation.Telephone = txtTele.Text;
                relation.Website = txtWebsite.Text;
                relation.TaxRegistrationType = (TaxRegisrationType)cmbTaxRegType.SelectedIndex;

                relation.IsSupplier = checkSupplier.Checked;
                relation.IsCustomer = checkCustomer.Checked;
                relation.IsFinancer = checkFinancer.Checked;
                relation.IsFinancee = checkFinancee.Checked;
                relation.IsVatAgent = chkIsVatAgent.Checked;
                relation.IsWithholdingAgent = chkIsWithholdingAgent.Checked;
                relation.Withhold = !chkDontWithhold.Checked;
                relation.Address = memoAddress.Text;
                string relationCode = iERPTransactionClient.RegisterRelation(relation);
                string message = _relationCode == null ? "Relation successfully saved!" : "Relation successfully updated";
                MessageBox.ShowSuccessMessage(message);
                if (relation.Code == null)
                {
                    if (RelationAdded != null)
                        RelationAdded(relationCode);
                    ResetControls();
                    
                }
                else
                {
                    this.DialogResult = DialogResult.OK;
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error occurred while trying to create relation\n" + ex.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void ResetControls()
        {
            txtName.Text = "";
            txtTIN.Text = "";
            txtVATNumber.Text = "";
            txtRegion.Text = "";
            txtDescription.Text = "";
            txtEmail.Text = "";
            txtWebsite.Text = "";
            txtTele.Text = "";
            txtZone.Text = txtWoreda.Text = "";
            txtKebele.Text = txtHouseNo.Text = "";
            cmbTaxRegType.SelectedIndex = 0;
        }

        private void cmbTaxRegType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTaxRegType.SelectedIndex == 1 || cmbTaxRegType.SelectedIndex == 2)
            {
                txtVATNumber.Visible = false;
                labelVatNo.Visible = false;
            }
            else
            {
                txtVATNumber.Visible = true;
                labelVatNo.Visible = true;
            }
        }

        private void checkCustomer_CheckedChanged(object sender, EventArgs e)
        {
            if (checkCustomer.Checked)
            {
                if (!checkSupplier.Checked)
                {
                    chkDontWithhold.Visible = false;
                    chkDontWithhold.Checked = false;
                }
                if (!chkIsWithholdingAgent.Visible)
                    chkIsWithholdingAgent.Visible = true;
                if (!chkIsVatAgent.Visible)
                    chkIsVatAgent.Visible = true;
            }
            else
            {
                chkIsWithholdingAgent.Visible = false;
                chkIsVatAgent.Visible = false;
            }
        }

        private void checkSupplier_CheckedChanged(object sender, EventArgs e)
        {
            if (checkSupplier.Checked)
            {
                if (!checkCustomer.Checked)
                {
                    chkIsWithholdingAgent.Visible = false;
                    chkIsVatAgent.Visible = false;
                    chkIsWithholdingAgent.Checked = chkIsVatAgent.Checked = false;
                }
                if (!chkDontWithhold.Visible)
                    chkDontWithhold.Visible = true;
            }
            else
            {
                chkDontWithhold.Visible = false;
            }
        }

        private void btnBrowse_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Title = "Select Photo";
            openFileDialog1.DefaultExt = "JPG";
            openFileDialog1.Filter = "Images (*.BMP;*.JPG;*.GIF,*.PNG,*.TIFF)|*.BMP;*.JPG;*.GIF;*.PNG;*.TIFF";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK) 
            {
                string file = openFileDialog1.FileName;
                try
                {
                    Image origImg = Image.FromFile(file);
                    Image myThumbnail = ResizeImage(file,128,128,origImg.Width,origImg.Height);
                    
                    picLogo.Image = myThumbnail;

                }
                catch (IOException)
                {
                }
            }
        }

        private Image ResizeImage(string originalFilename, int canvasWidth, int canvasHeight, int originalWidth, int originalHeight)
        {
            Image image = Image.FromFile(originalFilename);

            System.Drawing.Image thumbnail = new Bitmap(canvasWidth, canvasHeight); 
            System.Drawing.Graphics graphic = System.Drawing.Graphics.FromImage(thumbnail);
            graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphic.SmoothingMode = SmoothingMode.HighQuality;
            graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
            graphic.CompositingQuality = CompositingQuality.HighQuality;
            // Figure out the ratio
            double ratioX = (double)canvasWidth / (double)originalWidth;
            double ratioY = (double)canvasHeight / (double)originalHeight;
            // use whichever multiplier is smaller
            double ratio = ratioX < ratioY ? ratioX : ratioY;
            // now we can get the new height and width
            int newHeight = Convert.ToInt32(originalHeight * ratio);
            int newWidth = Convert.ToInt32(originalWidth * ratio);
            // Now calculate the X,Y position of the upper-left corner 
            // (one of these will always be zero)
            int posX = Convert.ToInt32((canvasWidth - (originalWidth * ratio)) / 2);
            int posY = Convert.ToInt32((canvasHeight - (originalHeight * ratio)) / 2);

            graphic.Clear(Color.White); // white padding
            graphic.DrawImage(image, posX, posY, newWidth, newHeight);

            System.Drawing.Imaging.ImageCodecInfo[] info = ImageCodecInfo.GetImageEncoders();
            EncoderParameters encoderParameters;
            encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality,100L);
            return thumbnail;
        }

    }
}
