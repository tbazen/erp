﻿namespace BIZNET.iERP.Client
{
    partial class RelationManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RelationManager));
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barManager = new DevExpress.XtraBars.Bar();
            this.btnAdd = new DevExpress.XtraBars.BarButtonItem();
            this.btnAddInternational = new DevExpress.XtraBars.BarButtonItem();
            this.btnEdit = new DevExpress.XtraBars.BarButtonItem();
            this.btnDelete = new DevExpress.XtraBars.BarButtonItem();
            this.btnActivate = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barCheckItem1 = new DevExpress.XtraBars.BarCheckItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.btnFirst = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrevious = new DevExpress.XtraBars.BarButtonItem();
            this.txtTotalRecords = new DevExpress.XtraBars.BarStaticItem();
            this.btnNext = new DevExpress.XtraBars.BarButtonItem();
            this.btnLast = new DevExpress.XtraBars.BarButtonItem();
            this.txtCurrentPage = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.pageNavigator = new INTAPS.UI.PageNavigator();
            this.panelPickerButtons = new DevExpress.XtraEditors.PanelControl();
            this.buttonSelect = new DevExpress.XtraEditors.SimpleButton();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.treeCategories = new BIZNET.iERP.Client.TradeRelationCategoryTree();
            this.contextRelationCategory = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miEditCategory = new System.Windows.Forms.ToolStripMenuItem();
            this.miAddSubcategory = new System.Windows.Forms.ToolStripMenuItem();
            this.miAddCategory = new System.Windows.Forms.ToolStripMenuItem();
            this.miDeleteCategory = new System.Windows.Forms.ToolStripMenuItem();
            this.txtVATNumber = new DevExpress.XtraEditors.TextEdit();
            this.txtTIN = new DevExpress.XtraEditors.TextEdit();
            this.txtCode = new DevExpress.XtraEditors.TextEdit();
            this.gridManager = new DevExpress.XtraGrid.GridControl();
            this.gridViewManager = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPickerButtons = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutConverter1 = new DevExpress.XtraLayout.Converter.LayoutConverter(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelPickerButtons)).BeginInit();
            this.panelPickerButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeCategories)).BeginInit();
            this.contextRelationCategory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPickerButtons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItem10,
            this.layoutControlItem3,
            this.layoutControlItem1,
            this.layoutPickerButtons,
            this.layoutControlItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(908, 592);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtName;
            this.layoutControlItem4.CustomizationFormText = "Name";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(365, 30);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem4.Text = "Name";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(52, 13);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(63, 8);
            this.txtName.MenuManager = this.barManager1;
            this.txtName.Name = "txtName";
            this.txtName.Properties.Mask.EditMask = "[a-zA-Z].*";
            this.txtName.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtName.Size = new System.Drawing.Size(300, 20);
            this.txtName.StyleController = this.layoutControl1;
            this.txtName.TabIndex = 4;
            this.txtName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchFieldKeyDown);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barManager});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnAdd,
            this.btnEdit,
            this.btnDelete,
            this.barCheckItem1,
            this.btnActivate,
            this.barStaticItem1,
            this.btnFirst,
            this.btnPrevious,
            this.txtTotalRecords,
            this.btnNext,
            this.btnLast,
            this.txtCurrentPage,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.btnAddInternational});
            this.barManager1.MainMenu = this.barManager;
            this.barManager1.MaxItemId = 22;
            // 
            // barManager
            // 
            this.barManager.BarName = "Main menu";
            this.barManager.DockCol = 0;
            this.barManager.DockRow = 0;
            this.barManager.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barManager.FloatLocation = new System.Drawing.Point(485, 153);
            this.barManager.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnAdd, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnAddInternational, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnActivate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barManager.OptionsBar.MultiLine = true;
            this.barManager.OptionsBar.UseWholeRow = true;
            this.barManager.Text = "Main menu";
            // 
            // btnAdd
            // 
            this.btnAdd.Caption = "New Local Customer/Supplier";
            this.btnAdd.Glyph = ((System.Drawing.Image)(resources.GetObject("btnAdd.Glyph")));
            this.btnAdd.Id = 0;
            this.btnAdd.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ItemAppearance.Normal.Options.UseFont = true;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAdd_ItemClick);
            // 
            // btnAddInternational
            // 
            this.btnAddInternational.Caption = "New International Supplier";
            this.btnAddInternational.Glyph = global::BIZNET.iERP.Client.Properties.Resources.company_add;
            this.btnAddInternational.Id = 21;
            this.btnAddInternational.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.btnAddInternational.ItemAppearance.Normal.Options.UseFont = true;
            this.btnAddInternational.Name = "btnAddInternational";
            this.btnAddInternational.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAddInternational_ItemClick);
            // 
            // btnEdit
            // 
            this.btnEdit.Caption = "&Edit";
            this.btnEdit.Enabled = false;
            this.btnEdit.Glyph = ((System.Drawing.Image)(resources.GetObject("btnEdit.Glyph")));
            this.btnEdit.Id = 1;
            this.btnEdit.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.ItemAppearance.Normal.Options.UseFont = true;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEdit_ItemClick);
            // 
            // btnDelete
            // 
            this.btnDelete.Caption = "&Delete";
            this.btnDelete.Enabled = false;
            this.btnDelete.Glyph = ((System.Drawing.Image)(resources.GetObject("btnDelete.Glyph")));
            this.btnDelete.Id = 2;
            this.btnDelete.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ItemAppearance.Normal.Options.UseFont = true;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDelete_ItemClick);
            // 
            // btnActivate
            // 
            this.btnActivate.Caption = "Activate/Deactivate";
            this.btnActivate.Enabled = false;
            this.btnActivate.Glyph = ((System.Drawing.Image)(resources.GetObject("btnActivate.Glyph")));
            this.btnActivate.Id = 4;
            this.btnActivate.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActivate.ItemAppearance.Normal.Options.UseFont = true;
            this.btnActivate.Name = "btnActivate";
            this.btnActivate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnActivate_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(908, 40);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 632);
            this.barDockControlBottom.Size = new System.Drawing.Size(908, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 40);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 592);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(908, 40);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 592);
            // 
            // barCheckItem1
            // 
            this.barCheckItem1.Caption = "barCheckItem1";
            this.barCheckItem1.Id = 3;
            this.barCheckItem1.Name = "barCheckItem1";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "First";
            this.barStaticItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem1.Glyph")));
            this.barStaticItem1.Id = 5;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // btnFirst
            // 
            this.btnFirst.Caption = "&First";
            this.btnFirst.Enabled = false;
            this.btnFirst.Glyph = ((System.Drawing.Image)(resources.GetObject("btnFirst.Glyph")));
            this.btnFirst.Id = 10;
            this.btnFirst.Name = "btnFirst";
            // 
            // btnPrevious
            // 
            this.btnPrevious.Caption = "&Previous";
            this.btnPrevious.Enabled = false;
            this.btnPrevious.Glyph = ((System.Drawing.Image)(resources.GetObject("btnPrevious.Glyph")));
            this.btnPrevious.Id = 11;
            this.btnPrevious.Name = "btnPrevious";
            // 
            // txtTotalRecords
            // 
            this.txtTotalRecords.Caption = "{No results found }";
            this.txtTotalRecords.Id = 12;
            this.txtTotalRecords.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalRecords.ItemAppearance.Normal.Options.UseFont = true;
            this.txtTotalRecords.Name = "txtTotalRecords";
            this.txtTotalRecords.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // btnNext
            // 
            this.btnNext.Caption = "&Next";
            this.btnNext.Enabled = false;
            this.btnNext.Glyph = ((System.Drawing.Image)(resources.GetObject("btnNext.Glyph")));
            this.btnNext.Id = 13;
            this.btnNext.Name = "btnNext";
            // 
            // btnLast
            // 
            this.btnLast.Caption = "&Last";
            this.btnLast.Enabled = false;
            this.btnLast.Glyph = ((System.Drawing.Image)(resources.GetObject("btnLast.Glyph")));
            this.btnLast.Id = 14;
            this.btnLast.Name = "btnLast";
            // 
            // txtCurrentPage
            // 
            this.txtCurrentPage.Caption = "Page 1";
            this.txtCurrentPage.Id = 15;
            this.txtCurrentPage.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrentPage.ItemAppearance.Normal.Options.UseFont = true;
            this.txtCurrentPage.Name = "txtCurrentPage";
            this.txtCurrentPage.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Add Category";
            this.barButtonItem1.Id = 17;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Add Subcategory";
            this.barButtonItem2.Id = 18;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Delete Category";
            this.barButtonItem3.Id = 19;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Edit Category";
            this.barButtonItem4.Id = 20;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.pageNavigator);
            this.layoutControl1.Controls.Add(this.panelPickerButtons);
            this.layoutControl1.Controls.Add(this.treeCategories);
            this.layoutControl1.Controls.Add(this.txtVATNumber);
            this.layoutControl1.Controls.Add(this.txtTIN);
            this.layoutControl1.Controls.Add(this.txtName);
            this.layoutControl1.Controls.Add(this.txtCode);
            this.layoutControl1.Controls.Add(this.gridManager);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 40);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(333, 252, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(908, 592);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // pageNavigator
            // 
            this.pageNavigator.Location = new System.Drawing.Point(370, 65);
            this.pageNavigator.Name = "pageNavigator";
            this.pageNavigator.NRec = 0;
            this.pageNavigator.PageSize = 50;
            this.pageNavigator.RecordIndex = 0;
            this.pageNavigator.Size = new System.Drawing.Size(533, 32);
            this.pageNavigator.TabIndex = 16;
            this.pageNavigator.PageChanged += new System.EventHandler(this.pageNavigator_PageChanged);
            // 
            // panelPickerButtons
            // 
            this.panelPickerButtons.Controls.Add(this.buttonSelect);
            this.panelPickerButtons.Controls.Add(this.buttonCancel);
            this.panelPickerButtons.Location = new System.Drawing.Point(5, 543);
            this.panelPickerButtons.Name = "panelPickerButtons";
            this.panelPickerButtons.Size = new System.Drawing.Size(898, 44);
            this.panelPickerButtons.TabIndex = 15;
            // 
            // buttonSelect
            // 
            this.buttonSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSelect.Location = new System.Drawing.Point(726, 15);
            this.buttonSelect.Name = "buttonSelect";
            this.buttonSelect.Size = new System.Drawing.Size(75, 24);
            this.buttonSelect.TabIndex = 3;
            this.buttonSelect.Text = "Select";
            this.buttonSelect.Click += new System.EventHandler(this.buttonSelect_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(816, 15);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 22);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click_1);
            // 
            // treeCategories
            // 
            this.treeCategories.ContextMenuStrip = this.contextRelationCategory;
            this.treeCategories.Location = new System.Drawing.Point(5, 81);
            this.treeCategories.Name = "treeCategories";
            this.treeCategories.OptionsBehavior.Editable = false;
            this.treeCategories.OptionsSelection.UseIndicatorForSelection = true;
            this.treeCategories.Size = new System.Drawing.Size(361, 458);
            this.treeCategories.TabIndex = 14;
            this.treeCategories.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeCategories_FocusedNodeChanged);
            this.treeCategories.Click += new System.EventHandler(this.treeCategories_Click);
            // 
            // contextRelationCategory
            // 
            this.contextRelationCategory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miEditCategory,
            this.miAddSubcategory,
            this.miAddCategory,
            this.miDeleteCategory});
            this.contextRelationCategory.Name = "contextRelationCategory";
            this.contextRelationCategory.Size = new System.Drawing.Size(166, 92);
            this.contextRelationCategory.Opening += new System.ComponentModel.CancelEventHandler(this.contextRelationCategory_Opening);
            // 
            // miEditCategory
            // 
            this.miEditCategory.Name = "miEditCategory";
            this.miEditCategory.Size = new System.Drawing.Size(165, 22);
            this.miEditCategory.Text = "Edit";
            this.miEditCategory.Click += new System.EventHandler(this.miEditCategory_Click);
            // 
            // miAddSubcategory
            // 
            this.miAddSubcategory.Name = "miAddSubcategory";
            this.miAddSubcategory.Size = new System.Drawing.Size(165, 22);
            this.miAddSubcategory.Text = "Add Subcategory";
            this.miAddSubcategory.Click += new System.EventHandler(this.miAddSubcategory_Click);
            // 
            // miAddCategory
            // 
            this.miAddCategory.Name = "miAddCategory";
            this.miAddCategory.Size = new System.Drawing.Size(165, 22);
            this.miAddCategory.Text = "Add Category";
            this.miAddCategory.Click += new System.EventHandler(this.miAddCategory_Click);
            // 
            // miDeleteCategory
            // 
            this.miDeleteCategory.Name = "miDeleteCategory";
            this.miDeleteCategory.Size = new System.Drawing.Size(165, 22);
            this.miDeleteCategory.Text = "Delete";
            this.miDeleteCategory.Click += new System.EventHandler(this.miDeleteCategory_Click);
            // 
            // txtVATNumber
            // 
            this.txtVATNumber.Location = new System.Drawing.Point(428, 38);
            this.txtVATNumber.MenuManager = this.barManager1;
            this.txtVATNumber.Name = "txtVATNumber";
            this.txtVATNumber.Properties.Mask.EditMask = "\\d*";
            this.txtVATNumber.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtVATNumber.Size = new System.Drawing.Size(472, 20);
            this.txtVATNumber.StyleController = this.layoutControl1;
            this.txtVATNumber.TabIndex = 7;
            this.txtVATNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchFieldKeyDown);
            // 
            // txtTIN
            // 
            this.txtTIN.Location = new System.Drawing.Point(63, 38);
            this.txtTIN.MenuManager = this.barManager1;
            this.txtTIN.Name = "txtTIN";
            this.txtTIN.Properties.Mask.EditMask = "\\d*";
            this.txtTIN.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTIN.Size = new System.Drawing.Size(300, 20);
            this.txtTIN.StyleController = this.layoutControl1;
            this.txtTIN.TabIndex = 6;
            this.txtTIN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchFieldKeyDown);
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(428, 8);
            this.txtCode.MenuManager = this.barManager1;
            this.txtCode.Name = "txtCode";
            this.txtCode.Properties.Mask.EditMask = "\\d*";
            this.txtCode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtCode.Size = new System.Drawing.Size(472, 20);
            this.txtCode.StyleController = this.layoutControl1;
            this.txtCode.TabIndex = 5;
            this.txtCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchFieldKeyDown);
            // 
            // gridManager
            // 
            this.gridManager.Location = new System.Drawing.Point(370, 101);
            this.gridManager.MainView = this.gridViewManager;
            this.gridManager.Name = "gridManager";
            this.gridManager.Size = new System.Drawing.Size(533, 438);
            this.gridManager.TabIndex = 6;
            this.gridManager.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewManager});
            this.gridManager.ProcessGridKey += new System.Windows.Forms.KeyEventHandler(this.gridManager_ProcessGridKey);
            this.gridManager.DoubleClick += new System.EventHandler(this.gridManager_DoubleClick);
            // 
            // gridViewManager
            // 
            this.gridViewManager.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewManager.Appearance.GroupPanel.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.gridViewManager.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewManager.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridViewManager.GridControl = this.gridManager;
            this.gridViewManager.GroupPanelText = "Trade Relations";
            this.gridViewManager.Name = "gridViewManager";
            this.gridViewManager.OptionsBehavior.Editable = false;
            this.gridViewManager.OptionsFind.FindDelay = 100;
            this.gridViewManager.OptionsMenu.EnableColumnMenu = false;
            this.gridViewManager.OptionsMenu.EnableFooterMenu = false;
            this.gridViewManager.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewManager.OptionsMenu.ShowAutoFilterRowItem = false;
            this.gridViewManager.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewManager_FocusedRowChanged);
            this.gridViewManager.DoubleClick += new System.EventHandler(this.gridViewManager_DoubleClick);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtCode;
            this.layoutControlItem5.CustomizationFormText = "Code";
            this.layoutControlItem5.Location = new System.Drawing.Point(365, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(537, 30);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem5.Text = "Code";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(52, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtTIN;
            this.layoutControlItem7.CustomizationFormText = "TIN";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(365, 30);
            this.layoutControlItem7.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem7.Text = "TIN:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(52, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txtVATNumber;
            this.layoutControlItem10.CustomizationFormText = "VAT No.";
            this.layoutControlItem10.Location = new System.Drawing.Point(365, 30);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(537, 30);
            this.layoutControlItem10.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem10.Text = "VAT No.";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(52, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.treeCategories;
            this.layoutControlItem3.CustomizationFormText = "Categories";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(365, 0);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(365, 40);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(365, 478);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Categories";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(52, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridManager;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(365, 96);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(537, 442);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutPickerButtons
            // 
            this.layoutPickerButtons.Control = this.panelPickerButtons;
            this.layoutPickerButtons.CustomizationFormText = "layoutControlItem2";
            this.layoutPickerButtons.Location = new System.Drawing.Point(0, 538);
            this.layoutPickerButtons.MinSize = new System.Drawing.Size(201, 24);
            this.layoutPickerButtons.Name = "layoutPickerButtons";
            this.layoutPickerButtons.Size = new System.Drawing.Size(902, 48);
            this.layoutPickerButtons.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPickerButtons.Text = "layoutPickerButtons";
            this.layoutPickerButtons.TextSize = new System.Drawing.Size(0, 0);
            this.layoutPickerButtons.TextToControlDistance = 0;
            this.layoutPickerButtons.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.pageNavigator;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(365, 60);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(201, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(537, 36);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // RelationManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(908, 632);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "RelationManager";
            this.ShowIcon = false;
            this.Text = "Manager";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelPickerButtons)).EndInit();
            this.panelPickerButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeCategories)).EndInit();
            this.contextRelationCategory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtVATNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPickerButtons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.ResumeLayout(false);

}

        #endregion


        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl gridManager;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewManager;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnAdd;
        private DevExpress.XtraBars.BarButtonItem btnEdit;
        private DevExpress.XtraBars.BarButtonItem btnDelete;
        private DevExpress.XtraBars.BarButtonItem btnActivate;
        private DevExpress.XtraBars.BarCheckItem barCheckItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.Converter.LayoutConverter layoutConverter1;
        private DevExpress.XtraEditors.TextEdit txtVATNumber;
        private DevExpress.XtraEditors.TextEdit txtTIN;
        private DevExpress.XtraEditors.TextEdit txtCode;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarButtonItem btnFirst;
        private DevExpress.XtraBars.BarButtonItem btnPrevious;
        private DevExpress.XtraBars.BarStaticItem txtTotalRecords;
        private DevExpress.XtraBars.BarButtonItem btnNext;
        private DevExpress.XtraBars.BarButtonItem btnLast;
        private DevExpress.XtraBars.BarStaticItem txtCurrentPage;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private TradeRelationCategoryTree treeCategories;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.PanelControl panelPickerButtons;
        private DevExpress.XtraEditors.SimpleButton buttonSelect;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraLayout.LayoutControlItem layoutPickerButtons;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private INTAPS.UI.PageNavigator pageNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private System.Windows.Forms.ContextMenuStrip contextRelationCategory;
        private System.Windows.Forms.ToolStripMenuItem miEditCategory;
        private System.Windows.Forms.ToolStripMenuItem miAddSubcategory;
        private System.Windows.Forms.ToolStripMenuItem miAddCategory;
        private System.Windows.Forms.ToolStripMenuItem miDeleteCategory;
        private DevExpress.XtraBars.BarButtonItem btnAddInternational;
    }
}