﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using DevExpress.XtraLayout.Utils;
using System.Collections;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using DevExpress.XtraGrid;
using DevExpress.Utils;
using DevExpress.XtraLayout;
using DevExpress.XtraSplashScreen;

namespace BIZNET.iERP.Client
{
    public partial class RelationManager : XtraForm
    {
        string _selectedCode=null;

        public string SelectedCode
        {
            get { return _selectedCode; }
        }
        DataTable relationTable;
        TradeRelationType m_ManagerType;
        ManagerPurpose m_purpose;
        SplashScreenManager waitScreen;
        bool _treeMode = false;

        public RelationManager(TradeRelationType managerType, ManagerPurpose purpose)
        {
            _treeMode = false;
            InitializeComponent();
            waitScreen = new SplashScreenManager(this, typeof(global::BIZNET.iERP.Client.WaitForm1), true, true);
            m_ManagerType = managerType;
            m_purpose = purpose;

            relationTable = new DataTable();
            PrepareTable();
            if (m_purpose != ManagerPurpose.Picker)
            {
                panelPickerButtons.Visible=false;
            }
            treeCategories.initializeControl();
            treeCategories.reloadCategories();

        }
        void setTreeMode(bool treeMode)
        {
            if (_treeMode == treeMode)
                return;
            _treeMode = treeMode;
            if (treeMode)
                clearSearchFields();
        }
               
        private void PrepareTable()
        {
            relationTable.Columns.Add("Category", typeof(string));
            relationTable.Columns.Add("Code", typeof(string));
            relationTable.Columns.Add("Name", typeof(string));
            relationTable.Columns.Add("Type", typeof(string));
            relationTable.Columns.Add("TIN", typeof(string));
            relationTable.Columns.Add("Telephone", typeof(string));
            relationTable.Columns.Add("Activated", typeof(bool));
            relationTable.Columns.Add("International", typeof(bool));

            gridManager.DataSource = relationTable;
            gridViewManager.Columns["Activated"].Visible = false;
        }
        
        private void SearchTradeRelations()
        {
            waitScreen.ShowWaitForm();
            //this.Enabled = false;
            try
            {
                relationTable.Clear();
                int N;
                List<string> col = new List<string>();
                List<object> cr = new List<object>();
                CollectSearchCriteria(col, cr);
                TradeRelation[] relations = iERPTransactionClient.searchTradeRelation(
                    _treeMode ? (treeCategories.Selection[0].Tag as TradeRelationCategory).code : null
                    , m_ManagerType == TradeRelationType.All ? null : new TradeRelationType[] { m_ManagerType }
                    , pageNavigator.RecordIndex
                    , pageNavigator.PageSize
                    , cr.ToArray()
                    , col.ToArray()
                    , out N);
                pageNavigator.NRec = N;

                foreach (TradeRelation relation in relations)
                {
                    DataRow row = relationTable.NewRow();
                    waitScreen.SetWaitFormDescription("Searching: " + relation.Name);
                    TradeRelationCategory cat= iERPTransactionClient.getTradeRelationCategory( relation.pcode);
                    row[0] = cat == null ? "Unknown" : cat.name;
                    row[1] = relation.Code;
                    row[2] = relation.Name;
                    row[3] = relation.TradeType;
                    row[4] = relation.TIN;
                    row[5] = relation.Telephone;
                    row[6] = relation.Activated;
                    row[7] = relation.IsInternational;

                    relationTable.Rows.Add(row);
                    //Application.DoEvents();
                }
                gridManager.DataSource = relationTable;
                gridManager.RefreshDataSource();
                ApplyManagerFormatConditioning();

            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to retrieve trade relations\n" + ex.Message);
            }
            finally
            {
                waitScreen.CloseWaitForm();
                //this.Enabled = true;
            }
        }
        private void btnAdd_ItemClick(object sender, ItemClickEventArgs e)
        {
            TradeRelationCategory selectedCat = null;
            if (treeCategories.Selection.Count == 0)
                return;
            if ((selectedCat = (treeCategories.Selection[0].Tag as TradeRelationCategory)) == null)
                return;

            using (RegisterRelation2 relation = new RegisterRelation2(selectedCat.code, null,false))
            {
                relation.RelationAdded += relation_SupplierAdded;
                relation.ShowInTaskbar = false;
                relation.StartPosition = FormStartPosition.CenterScreen;
                relation.ShowDialog();
                
            }

            
        }
        void reloadSearchResult()
        {
            pageNavigator.RecordIndex = 0;
            SearchTradeRelations();
        }
        void relation_SupplierAdded(string supplierCode)
        {
            reloadSearchResult();
        }

        private void btnEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            int[] selectedRowHandle = gridViewManager.GetSelectedRows();
            string code = (string)gridViewManager.GetRowCellValue(gridViewManager.FocusedRowHandle, "Code");
            bool isInternational = (bool)gridViewManager.GetRowCellValue(gridViewManager.FocusedRowHandle, "International");


            using (TradeRelationProfileForm relationForm = new TradeRelationProfileForm(code,isInternational))
            {
                relationForm.ShowInTaskbar = false;
                relationForm.StartPosition = FormStartPosition.CenterScreen;
                relationForm.ShowDialog();
                txtCode.Text = code;
            }
        }       

        private void btnDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            int[] selectedRowHandle = gridViewManager.GetSelectedRows();
            string code = (string)gridViewManager.GetRowCellValue(selectedRowHandle[0], "Code");
            if (MessageBox.ShowWarningMessage("Are you sure you want to delete the selected trade relation?") == DialogResult.Yes)
            {
                try
                {
                    iERPTransactionClient.DeleteRelation(code);
                    MessageBox.ShowSuccessMessage("TradeRelation successfully deleted");
                    reloadSearchResult();
                }
                catch (Exception ex)
                {
                    MessageBox.ShowException(ex);
                }
            }
        }
        
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CollectSearchCriteria(List<string> columns, List<object> criteria)
        {
            if (txtCode.Text != "")
            {
                columns.Add("Code");
                criteria.Add(TSConstants.FormatSerialCode(int.Parse(txtCode.Text)));
            }
            if (txtName.Text != "")
            {
                columns.Add("Name");
                criteria.Add(txtName.Text);
            }
            if (txtTIN.Text != "")
            {
                columns.Add("TIN");
                criteria.Add(int.Parse(txtTIN.Text));
            }
            if (txtVATNumber.Text != "")
            {
                columns.Add("VATRegNumber");
                criteria.Add(int.Parse(txtVATNumber.Text));
            }
        }


        private void ApplyManagerFormatConditioning()
        {
            StyleFormatCondition condition = new StyleFormatCondition();
            condition.Appearance.Options.UseBackColor = true;
            condition.Appearance.BackColor = Color.FromArgb(0x8F, 0xF2, 0x14, 0x43);
            condition.Condition = FormatConditionEnum.Expression;
            condition.Expression = "[Activated] != True";
            condition.ApplyToRow = true;
            gridViewManager.FormatConditions.Add(condition);
        }

        private void btnActivate_ItemClick(object sender, ItemClickEventArgs e)
        {

            int[] rowHandle = gridViewManager.GetSelectedRows();
            string code = (string)gridViewManager.GetRowCellValue(rowHandle[0], "Code");
            bool activated = (bool)gridViewManager.GetRowCellValue(rowHandle[0], "Activated");
            try
            {

                if (activated)
                {
                    if (MessageBox.ShowWarningMessage("Are you sure you want to deactivate the selected trade relation?") == DialogResult.Yes)
                        iERPTransactionClient.DeactivateRelation(code);
                }
                else
                {
                    if (MessageBox.ShowWarningMessage("Are you sure you want to activate the selected trade relation?") == DialogResult.Yes)
                        iERPTransactionClient.ActivateRelation(code);
                }
                string message = activated ? "Trade relation successfully deactivated" : "Trade relation successfully activated";
                MessageBox.ShowSuccessMessage(message);
                SearchTradeRelations();
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage("Error trying to activate/deactivate trade relation!\n" + ex.Message);
            }

        }

        
        private void gridViewManager_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewManager.SelectedRowsCount == 1)
            {
                btnEdit.Enabled = true;
                btnDelete.Enabled = true;
                btnActivate.Enabled = true;
                int[] rowHandle = gridViewManager.GetSelectedRows();
                bool activated = (bool)gridViewManager.GetRowCellValue(rowHandle[0], "Activated");
                btnActivate.Caption = activated ? "Deactivate" : "Activate";
            }
            else
            {
                btnEdit.Enabled = false;
                btnDelete.Enabled = false;
                btnActivate.Enabled = false;
            }
        }

        private void clearSearchFields()
        {
            txtName.Text = "";
            txtTIN.Text = "";
            txtVATNumber.Text = "";
            txtCode.Text = "";
        }

        

        private void gridViewManager_DoubleClick(object sender, EventArgs e)
        {
            //gridManager_DoubleClick(null, null);
        }

        

        private void gridManager_ProcessGridKey(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonSelect_Click(null, null);
            }
        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            if (this.m_purpose == ManagerPurpose.Browser)
            {
                btnEdit_ItemClick(null, null);
            }
            else
            {
                int[] selectedRowHandle = gridViewManager.GetSelectedRows();
                if (selectedRowHandle.Length > 0)
                {
                    string code = (string)gridViewManager.GetRowCellValue(selectedRowHandle[0], "Code");
                    _selectedCode = code;
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void treeCategories_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            
        }

        

        private void searchFieldKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                setTreeMode(false);
                reloadSearchResult();
            }

        }

        private void pageNavigator_PageChanged(object sender, EventArgs e)
        {
            SearchTradeRelations();
        }

        private void miEditCategory_Click(object sender, EventArgs e)
        {
            TradeRelationCategory cat = contextRelationCategory.Tag as TradeRelationCategory;
            if (cat == null)
                return;
            TradeRelationCategoryForm f = new TradeRelationCategoryForm(cat);
            if (f.ShowDialog(this) == DialogResult.OK)
                treeCategories.reloadCategories();
        }

        private void miAddSubcategory_Click(object sender, EventArgs e)
        {
            TradeRelationCategory cat = contextRelationCategory.Tag as TradeRelationCategory;
            TradeRelationCategoryForm f = new TradeRelationCategoryForm(cat == null ? "" : cat.code);
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                treeCategories.reloadCategories();
            }
        }
      

        private void miAddCategory_Click(object sender, EventArgs e)
        {
            TradeRelationCategory cat = contextRelationCategory.Tag as TradeRelationCategory;
            TradeRelationCategoryForm f = new TradeRelationCategoryForm(cat == null ? "" : cat.pcode);
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                treeCategories.reloadCategories();
            }
        }

        private void miDeleteCategory_Click(object sender, EventArgs e)
        {
            TradeRelationCategory cat = contextRelationCategory.Tag as TradeRelationCategory;
            if(cat==null)
                return;
            try
            {
                if (MessageBox.ShowWarningMessage("Are you sure you want to delete the selected category?") != DialogResult.Yes)
                    return;
                iERPTransactionClient.deleteTradeRelationCategory(cat.code);
                treeCategories.reloadCategories();
            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }

        private void contextRelationCategory_Opening(object sender, CancelEventArgs e)
        {
            TradeRelationCategory selectedCat = null;
            bool selected = treeCategories.Selection.Count > 0 && 
                (selectedCat=(treeCategories.Selection[0].Tag as TradeRelationCategory))!=null
                ;
            miAddSubcategory.Enabled = selected;
            miDeleteCategory.Enabled = selected;
            miEditCategory.Enabled = selected;
            contextRelationCategory.Tag = selectedCat;
        }

        private void treeCategories_Click(object sender, EventArgs e)
        {
            DevExpress.XtraTreeList.Nodes.TreeListNode node=treeCategories.CalcHitInfo(treeCategories.PointToClient(Cursor.Position)).Node;
            if (node == null)
                return;
            TradeRelationCategory cat = node.Tag as TradeRelationCategory;
            if (cat == null)
                return;
            clearSearchFields();
            setTreeMode(true);
            reloadSearchResult();
        }

        private void gridManager_DoubleClick(object sender, EventArgs e)
        {
            buttonSelect_Click(null, null);
        }

        private void buttonCancel_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddInternational_ItemClick(object sender, ItemClickEventArgs e)
        {
            TradeRelationCategory selectedCat = null;
            if (treeCategories.Selection.Count == 0)
                return;
            if ((selectedCat = (treeCategories.Selection[0].Tag as TradeRelationCategory)) == null)
                return;

            using (RegisterRelation2 relation = new RegisterRelation2(selectedCat.code, null,true))
            {
                relation.RelationAdded += relation_SupplierAdded;
                relation.ShowInTaskbar = false;
                relation.StartPosition = FormStartPosition.CenterScreen;
                relation.ShowDialog();

            }
        }
    }
    
    public enum ManagerPurpose
    {
        Browser,
        Picker
    }
}