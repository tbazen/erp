using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.UI.ButtonGrid;
using System.Drawing;
using System.IO;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
     public class SuppliersList :ObjectBGList<TradeRelation>
    {
        ButtonGrid buttonGrid;
        public SuppliersList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        public override bool Searchable
        {
            get { return true; }
        }
        public override ButtonGridItem[] CreateButton(TradeRelation supplier)
        {
            if (supplier == null)
            {
                ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("newSupplier", "Add New Supplier", Properties.Resources.company_add_256);
                new EventActivator<object>(button, null).Clicked += AddNewSupplier_Click;
                return new ButtonGridItem[] { button };

            }
            else
            {
                Image supplierLogo = supplier.Logo == null ? Properties.Resources.Supplier : Image.FromStream(new MemoryStream(supplier.Logo));
                ButtonGridBodyButtonItem button = ButtonGridHelper.CreateButtonWithImage("supplier" + supplier.Code, supplier.Name, supplierLogo);
                string supplierTIN = supplier.TIN == "" ? "" : (long.Parse(supplier.TIN) == 0 ? "" : "TIN: " + supplier.TIN);
                ButtonGridHelper.AddCornerText(button, supplierTIN);
                //ButtonGridHelper.SetButtonBackColor(button, Color.LightBlue);
                return new ButtonGridItem[] { button };
            }
        }
         void AddNewSupplier_Click(ButtonGridBodyButtonItem t, object z)
         {
             TradeRelationCategoryPicker picker = new TradeRelationCategoryPicker();
             if (picker.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm) != System.Windows.Forms.DialogResult.OK)
                 return;
             RegisterRelation registerSupplier = new RegisterRelation(picker.category.code, TradeRelationType.Supplier);
             registerSupplier.Show();
         }
        protected virtual bool CanIncludeSupplier(TradeRelation supplier)
        {
            return true;
        }

        public override List<TradeRelation> LoadData(string query)
        {
            List<TradeRelation> ret = new List<TradeRelation>();
            //if (string.IsNullOrEmpty(query))
            //    return ret;
            int N;
            TradeRelation[] suppliers = null;
            if (string.IsNullOrEmpty(query))
                suppliers = iERPTransactionClient.SearchSuppliers(0, 100, null, null, out N);
            else
            {
                string[] column = new string[1];
                object[] criteria = new object[1];
                int supplierTIN;
                criteria[0] = query;
                if (int.TryParse(query, out supplierTIN))
                    column[0] = "TIN";
                else
                    column[0] = "Name";
                suppliers = iERPTransactionClient.SearchSuppliers(0, 100, criteria, column, out N);
            }
            ret.Add(null);
            foreach (TradeRelation supplier in suppliers)
            {
                if (CanIncludeSupplier(supplier))
                {
                    ret.Add(supplier);
                }
            }
            return ret;
        }


        public override string searchLabel
        {
            get { return "Supplier:"; }
        }
    }
     public class SuppliersAdvanceList :SuppliersList
    {
        string supplierCode;
         BizNetPaymentMethod payMethod;
        public SuppliersAdvanceList(ButtonGrid grid, ButtonGridItem parentItem,BizNetPaymentMethod paymentMethod)
            : base(grid, parentItem)
        {
            payMethod = paymentMethod;
        }
         public SuppliersAdvanceList(ButtonGrid grid, ButtonGridItem parentItem)
             : base(grid, parentItem)
         {
             
         }
         
                            
        protected override bool CanIncludeSupplier(TradeRelation supplier)
        {
            if (supplier.Activated)
            {
                supplierCode = supplier.Code;
                return true;
            }
            return false;
        }

         public override ButtonGridItem[] CreateButton(TradeRelation supplier)
         {
             ButtonGridBodyButtonItem item = (ButtonGridBodyButtonItem)base.CreateButton(supplier)[0];
             if (supplier != null)
                 item.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(AccountingClient.GetDocumentTypeByType(typeof(SupplierAdvancePayment2Document)).id), payMethod, supplier.Code);
             return new ButtonGridItem[] { item };
         }

         
    }

    public class SuppliersPayableList : SuppliersList
    {
        string supplierCode;
        BizNetPaymentMethod payMethod;
        public SuppliersPayableList(ButtonGrid grid, ButtonGridItem parentItem,BizNetPaymentMethod paymentMethod)
            : base(grid, parentItem)
        {
            payMethod = paymentMethod;
        }
        public SuppliersPayableList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            
        }
         

        protected override bool CanIncludeSupplier(TradeRelation supplier)
        {
            return true;
            //UNIMPLEMENTED
            //Account payableAccount = GenericAccountingClient<IAccountingService>.GetAccount(supplier.PayableAccountID);
            //double balance = 0;
            //if (payableAccount != null)
            //{
            //    balance = payableAccount.Balances.Length == 0 ? 0 : Math.Abs(payableAccount.Balances[0].Balance);
            //}

            //if (supplier.Activated && Account.AmountGreater(balance, 0))
            //{
            //    supplierCode = supplier.Code;
            //    return true;
            //}
            //return false;
        }

        public override ButtonGridItem[] CreateButton(TradeRelation supplier)
        {
            ButtonGridBodyButtonItem item = (ButtonGridBodyButtonItem)base.CreateButton(supplier)[0];
            if (supplier != null)
            {
                int typeID = AccountingClient.GetDocumentTypeByType(typeof(SupplierCreditSettlement2Document)).id;
                item.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(typeID), payMethod, supplier.Code);
            }
            return new ButtonGridItem[] { item };
        }
    }
    public class SuppliersAdvanceReturnList : SuppliersList
    {
        string supplierCode;
        ButtonGrid buttonGrid;
        BizNetPaymentMethod payMethod;
        public SuppliersAdvanceReturnList(ButtonGrid grid, ButtonGridItem parentItem,BizNetPaymentMethod paymentMethod)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
            payMethod = paymentMethod;
        }
        public SuppliersAdvanceReturnList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            
        }
         

        protected override bool CanIncludeSupplier(TradeRelation supplier)
        {
            return true;
            //UNIMPLEMENTED
            //Account receivableAccount = GenericAccountingClient<IAccountingService>.GetAccount(supplier.ReceivableAccountID);
            //double balance = 0;
            //if (receivableAccount != null)
            //{
            //    balance = receivableAccount.Balances.Length == 0 ? 0 : Math.Abs(receivableAccount.Balances[0].Balance);
            //}

            //if (supplier.Activated && Account.AmountGreater(balance, 0))
            //{
            //    supplierCode = supplier.Code;
            //    return true;
            //}
            //return false;
        }

        public override ButtonGridItem[] CreateButton(TradeRelation supplier)
        {
            ButtonGridBodyButtonItem item = (ButtonGridBodyButtonItem)base.CreateButton(supplier)[0];
            if (supplier != null)
            {
                int typeID = AccountingClient.GetDocumentTypeByType(typeof(SupplierAdvanceReturn2Document)).id;
                item.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(typeID), payMethod, supplier.Code);
            }
            return new ButtonGridItem[] { item };
        }

    }

    public class SuppliersPurchaseList : SuppliersList
    {
        string supplierCode;
        PurchaseAndSalesData purchaseAndSalesData;
        ButtonGrid buttonGrid;
        public SuppliersPurchaseList(ButtonGrid grid, ButtonGridItem parentItem)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
        }
        public SuppliersPurchaseList(ButtonGrid grid, ButtonGridItem parentItem, PurchaseAndSalesData purchaseData)
            : base(grid, parentItem)
        {
            buttonGrid = grid;
            purchaseAndSalesData = purchaseData;
        }

        protected override bool CanIncludeSupplier(TradeRelation supplier)
        {
            if (supplier.Activated)
            {
                supplierCode = supplier.Code;
                return true;
            }
            return false;
        }

        public override ButtonGridItem[] CreateButton(TradeRelation supplier)
        {
            ButtonGridBodyButtonItem item = (ButtonGridBodyButtonItem)base.CreateButton(supplier)[0];
            if (supplier != null)
            {
                item.Tag = new DocumentWithPaymentTypeActivator(iERPTransactionClient.GetDocumentHandler(142), purchaseAndSalesData.paymentMethod, supplier.Code);
            }
            return new ButtonGridItem[] { item };
        }
    }
}
