﻿namespace BIZNET.iERP.Client
{
    partial class TradeRelationCategoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.csOnCustomerOrder = new INTAPS.Accounting.Client.CostCenterPlaceHolder();
            this.csOnSupplierOrder = new INTAPS.Accounting.Client.CostCenterPlaceHolder();
            this.acReceivedBond = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.acPaidBond = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.acRetentionPayable = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.acRetentionReceivable = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.acPayable = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.acReceivable = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.textCode = new DevExpress.XtraEditors.TextEdit();
            this.textName = new DevExpress.XtraEditors.TextEdit();
            this.labelParentCategory = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.buttonSave = new DevExpress.XtraEditors.SimpleButton();
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.Re = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Re)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.csOnCustomerOrder);
            this.layoutControl1.Controls.Add(this.csOnSupplierOrder);
            this.layoutControl1.Controls.Add(this.acReceivedBond);
            this.layoutControl1.Controls.Add(this.acPaidBond);
            this.layoutControl1.Controls.Add(this.acRetentionPayable);
            this.layoutControl1.Controls.Add(this.acRetentionReceivable);
            this.layoutControl1.Controls.Add(this.acPayable);
            this.layoutControl1.Controls.Add(this.acReceivable);
            this.layoutControl1.Controls.Add(this.textCode);
            this.layoutControl1.Controls.Add(this.textName);
            this.layoutControl1.Controls.Add(this.labelParentCategory);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(577, 376);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // csOnCustomerOrder
            // 
            this.csOnCustomerOrder.account = null;
            this.csOnCustomerOrder.AllowAdd = false;
            this.csOnCustomerOrder.Location = new System.Drawing.Point(223, 302);
            this.csOnCustomerOrder.Name = "csOnCustomerOrder";
            this.csOnCustomerOrder.OnlyLeafAccount = false;
            this.csOnCustomerOrder.Size = new System.Drawing.Size(339, 20);
            this.csOnCustomerOrder.TabIndex = 15;
            // 
            // csOnSupplierOrder
            // 
            this.csOnSupplierOrder.account = null;
            this.csOnSupplierOrder.AllowAdd = false;
            this.csOnSupplierOrder.Location = new System.Drawing.Point(223, 272);
            this.csOnSupplierOrder.Name = "csOnSupplierOrder";
            this.csOnSupplierOrder.OnlyLeafAccount = false;
            this.csOnSupplierOrder.Size = new System.Drawing.Size(339, 20);
            this.csOnSupplierOrder.TabIndex = 14;
            // 
            // acReceivedBond
            // 
            this.acReceivedBond.account = null;
            this.acReceivedBond.AllowAdd = false;
            this.acReceivedBond.Location = new System.Drawing.Point(223, 242);
            this.acReceivedBond.Name = "acReceivedBond";
            this.acReceivedBond.OnlyLeafAccount = false;
            this.acReceivedBond.Size = new System.Drawing.Size(339, 20);
            this.acReceivedBond.TabIndex = 13;
            // 
            // acPaidBond
            // 
            this.acPaidBond.account = null;
            this.acPaidBond.AllowAdd = false;
            this.acPaidBond.Location = new System.Drawing.Point(223, 212);
            this.acPaidBond.Name = "acPaidBond";
            this.acPaidBond.OnlyLeafAccount = false;
            this.acPaidBond.Size = new System.Drawing.Size(339, 20);
            this.acPaidBond.TabIndex = 12;
            // 
            // acRetentionPayable
            // 
            this.acRetentionPayable.account = null;
            this.acRetentionPayable.AllowAdd = false;
            this.acRetentionPayable.Location = new System.Drawing.Point(223, 182);
            this.acRetentionPayable.Name = "acRetentionPayable";
            this.acRetentionPayable.OnlyLeafAccount = false;
            this.acRetentionPayable.Size = new System.Drawing.Size(339, 20);
            this.acRetentionPayable.TabIndex = 11;
            // 
            // acRetentionReceivable
            // 
            this.acRetentionReceivable.account = null;
            this.acRetentionReceivable.AllowAdd = false;
            this.acRetentionReceivable.Location = new System.Drawing.Point(223, 152);
            this.acRetentionReceivable.Name = "acRetentionReceivable";
            this.acRetentionReceivable.OnlyLeafAccount = false;
            this.acRetentionReceivable.Size = new System.Drawing.Size(339, 20);
            this.acRetentionReceivable.TabIndex = 10;
            // 
            // acPayable
            // 
            this.acPayable.account = null;
            this.acPayable.AllowAdd = false;
            this.acPayable.Location = new System.Drawing.Point(223, 122);
            this.acPayable.Name = "acPayable";
            this.acPayable.OnlyLeafAccount = false;
            this.acPayable.Size = new System.Drawing.Size(339, 20);
            this.acPayable.TabIndex = 9;
            // 
            // acReceivable
            // 
            this.acReceivable.account = null;
            this.acReceivable.AllowAdd = false;
            this.acReceivable.Location = new System.Drawing.Point(223, 92);
            this.acReceivable.Name = "acReceivable";
            this.acReceivable.OnlyLeafAccount = false;
            this.acReceivable.Size = new System.Drawing.Size(339, 20);
            this.acReceivable.TabIndex = 8;
            // 
            // textCode
            // 
            this.textCode.Location = new System.Drawing.Point(223, 62);
            this.textCode.Name = "textCode";
            this.textCode.Size = new System.Drawing.Size(339, 20);
            this.textCode.StyleController = this.layoutControl1;
            this.textCode.TabIndex = 7;
            // 
            // textName
            // 
            this.textName.Location = new System.Drawing.Point(223, 32);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(339, 20);
            this.textName.StyleController = this.layoutControl1;
            this.textName.TabIndex = 6;
            // 
            // labelParentCategory
            // 
            this.labelParentCategory.Location = new System.Drawing.Point(12, 12);
            this.labelParentCategory.Name = "labelParentCategory";
            this.labelParentCategory.Size = new System.Drawing.Size(48, 13);
            this.labelParentCategory.StyleController = this.layoutControl1;
            this.labelParentCategory.TabIndex = 5;
            this.labelParentCategory.Text = "######";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.buttonSave);
            this.panelControl1.Controls.Add(this.buttonCancel);
            this.panelControl1.Location = new System.Drawing.Point(12, 329);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(553, 35);
            this.panelControl1.TabIndex = 4;
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.Location = new System.Drawing.Point(383, 7);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 0;
            this.buttonSave.Text = "Save";
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(464, 7);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 0;
            this.buttonCancel.Text = "Cancel";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.Re,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(577, 376);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.panelControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 317);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(557, 39);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.labelParentCategory;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(557, 17);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textName;
            this.layoutControlItem3.CustomizationFormText = "Name";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(557, 30);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem3.Text = "Name";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(204, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textCode;
            this.layoutControlItem4.CustomizationFormText = "Code";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 47);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(557, 30);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem4.Text = "Code";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(204, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.acReceivable;
            this.layoutControlItem5.CustomizationFormText = "Receivable Account";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 77);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(557, 30);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem5.Text = "Receivable Account";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(204, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.acPayable;
            this.layoutControlItem6.CustomizationFormText = "Payable Account";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 107);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(557, 30);
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem6.Text = "Payable Account";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(204, 13);
            // 
            // Re
            // 
            this.Re.Control = this.acRetentionReceivable;
            this.Re.CustomizationFormText = "Retention Receivable Account";
            this.Re.Location = new System.Drawing.Point(0, 137);
            this.Re.Name = "Re";
            this.Re.Size = new System.Drawing.Size(557, 30);
            this.Re.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.Re.Text = "Retention Receivable Account";
            this.Re.TextSize = new System.Drawing.Size(204, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.acRetentionPayable;
            this.layoutControlItem7.CustomizationFormText = "Retention Payable Account";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 167);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(557, 30);
            this.layoutControlItem7.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem7.Text = "Retention Payable Account";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(204, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.acPaidBond;
            this.layoutControlItem8.CustomizationFormText = "Paid Bond Account";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 197);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(557, 30);
            this.layoutControlItem8.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem8.Text = "Paid Bond Account";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(204, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.acReceivedBond;
            this.layoutControlItem9.CustomizationFormText = "Received Bond Account";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 227);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(557, 30);
            this.layoutControlItem9.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem9.Text = "Received Bond Account";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(204, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.csOnSupplierOrder;
            this.layoutControlItem10.CustomizationFormText = "Items Ordered From Suppliers Cost Center";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 257);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(557, 30);
            this.layoutControlItem10.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem10.Text = "Items Ordered From Suppliers Cost Center";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(204, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.csOnCustomerOrder;
            this.layoutControlItem11.CustomizationFormText = "Items Ordered by Customers Cost Center";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 287);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(557, 30);
            this.layoutControlItem11.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem11.Text = "Items Ordered by Customers Cost Center";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(204, 13);
            // 
            // TradeRelationCategoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 376);
            this.Controls.Add(this.layoutControl1);
            this.Name = "TradeRelationCategoryForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Trade Relation Category";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Re)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textCode;
        private DevExpress.XtraEditors.TextEdit textName;
        private DevExpress.XtraEditors.LabelControl labelParentCategory;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private INTAPS.Accounting.Client.AccountPlaceHolder acRetentionPayable;
        private INTAPS.Accounting.Client.AccountPlaceHolder acRetentionReceivable;
        private INTAPS.Accounting.Client.AccountPlaceHolder acPayable;
        private INTAPS.Accounting.Client.AccountPlaceHolder acReceivable;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem Re;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.SimpleButton buttonSave;
        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private INTAPS.Accounting.Client.AccountPlaceHolder acReceivedBond;
        private INTAPS.Accounting.Client.AccountPlaceHolder acPaidBond;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private INTAPS.Accounting.Client.CostCenterPlaceHolder csOnCustomerOrder;
        private INTAPS.Accounting.Client.CostCenterPlaceHolder csOnSupplierOrder;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
    }
}