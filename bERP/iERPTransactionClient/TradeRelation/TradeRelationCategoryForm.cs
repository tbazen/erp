﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BIZNET.iERP.Client
{
    public partial class TradeRelationCategoryForm : DevExpress.XtraEditors.XtraForm
    {
        TradeRelationCategory _cat=null;
        string _pcode;
        public TradeRelationCategoryForm(string pcode)
        {
            
            InitializeComponent();
            _pcode = pcode;
            if (string.IsNullOrEmpty(_pcode))
                labelParentCategory.Text = "";
            else
                labelParentCategory.Text = "Child of " + iERPTransactionClient.getTradeRelationCategory(pcode).nameCode;
        }
        public TradeRelationCategoryForm(TradeRelationCategory cat):this(cat.pcode)
        {
            _cat = cat;
            textName.Text = _cat.name;
            textCode.Text = _cat.code;
            csOnCustomerOrder.SetByID(_cat.onCustomerOrderParentCostCenterID);
            csOnSupplierOrder.SetByID(_cat.onSupplierOrderParentCostCenterID);
            acPaidBond.SetByID(_cat.PaidBondParentAccountID);
            acPayable.SetByID(_cat.PayableParentAccountID);
            acReceivable.SetByID(_cat.ReceivableParentAccountID);
            acReceivedBond.SetByID(_cat.ReceivedBondParentAccountID);
            acRetentionPayable.SetByID(_cat.RetentionPayableParentAccountID);
            acRetentionReceivable.SetByID(_cat.RetentionReceivableParentAccountID);
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(textName.Text.Trim()=="")
                {
                    MessageBox.ShowErrorMessage("Enter name");
                    return;
                }
                if(textCode.Text.Trim()=="")
                {
                    MessageBox.ShowErrorMessage("Enter code");
                    return;
                }
                TradeRelationCategory newCat = new TradeRelationCategory();
                newCat.name = textName.Text.Trim();
                newCat.pcode = _pcode;
                newCat.code = textCode.Text.Trim();
                newCat.onCustomerOrderParentCostCenterID = csOnCustomerOrder.GetAccountID();
                newCat.onSupplierOrderParentCostCenterID = csOnSupplierOrder.GetAccountID();
                newCat.PaidBondParentAccountID = acPaidBond.GetAccountID();
                newCat.PayableParentAccountID = acPayable.GetAccountID();
                newCat.ReceivableParentAccountID= acReceivable.GetAccountID();
                newCat.ReceivedBondParentAccountID =acReceivedBond.GetAccountID();
                newCat.RetentionPayableParentAccountID =acRetentionPayable.GetAccountID();
                newCat.RetentionReceivableParentAccountID = acRetentionReceivable.GetAccountID();
                if (_cat == null)
                    iERPTransactionClient.createTradeRelationCategory(newCat);
                else
                {
                    newCat.code = _cat.code;
                    newCat.pcode = _cat.pcode;
                    iERPTransactionClient.updateTradeRelationCategory(newCat);
                }
                this.DialogResult = DialogResult.OK;
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.ShowException(ex);
            }
        }
    }
}