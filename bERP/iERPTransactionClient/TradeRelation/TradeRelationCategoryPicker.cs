﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BIZNET.iERP.Client
{
    public partial class TradeRelationCategoryPicker : DevExpress.XtraEditors.XtraForm
    {
        public TradeRelationCategory category;
        public TradeRelationCategoryPicker()
        {
            InitializeComponent();
            tree.initializeControl();
            tree.reloadCategories();        
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            TradeRelationCategory selectedCat = null;
            if (tree.Selection.Count == 0)
            {
                MessageBox.ShowErrorMessage("Select a category");
                return;
            }
            if ((selectedCat = (tree.Selection[0].Tag as TradeRelationCategory)) == null)
            {
                MessageBox.ShowErrorMessage("Select a category");
                return;
            }
            category = selectedCat;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}