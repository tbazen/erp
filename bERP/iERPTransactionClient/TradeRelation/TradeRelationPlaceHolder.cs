﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace BIZNET.iERP.Client
{
    public partial class TradeRelationPlaceHolder : INTAPS.UI.ObjectPlaceHolderGenID<TradeRelation,string>
    {
        public TradeRelationPlaceHolder()
            : base("")
        {
        }
        protected override string GetObjectString(TradeRelation obj)
        {
            return obj.Name +" TIN:"+(obj.hasTINNumber?obj.TIN:"No TIN");
        }

        protected override TradeRelation PickObject()
        {
            RelationManager m = new RelationManager(TradeRelationType.All, ManagerPurpose.Picker);
            if (m.ShowDialog() == DialogResult.OK)
            {
                return iERPTransactionClient.GetRelation(m.SelectedCode);
            }
            return null;
        }

        protected override TradeRelation[] SearchObject(string query)
        {
            int N;
            TradeRelation[] res = iERPTransactionClient.searchTradeRelation(null, null, 0, 10, new object[] { query }, new string[] { "Code" }, out N);
            return res;
        }

        protected override TradeRelation GetObjectBYID(string id)
        {
            return iERPTransactionClient.GetRelation(id);
        }

        protected override string GetObjectID(TradeRelation obj)
        {
            return obj.Code;
        }
    }
}
