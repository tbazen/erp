﻿using System;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using INTAPS;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.ClientServer.Client;
using INTAPS.Payroll.Client;
using INTAPS.UI;
using INTAPS.UI.HTML;

namespace BIZNET.iERP.Client
{
    public partial class TradeRelationProfileForm : Form
    {
        private class CefTradeRelationProfile
        {
            private TransactionViewer _mtransactionView;
            private readonly TradeRelationProfileForm _parent;
            private ChromiumWebBrowser _chromeBrowser;

            public CefTradeRelationProfile(TradeRelationProfileForm parent, ChromiumWebBrowser chromeBrowser)
            {
                this._parent = parent;
                this._chromeBrowser = chromeBrowser;
            }

            public void editRelation(string code, string isInternational)
            {
                _parent.Invoke(new ProcessTowParameter<string, byte>(delegate(string relcode, byte inter)
                {
                    bool isInter = Convert.ToBoolean(inter);
                    RegisterRelation2 frm = new RegisterRelation2(null, relcode, isInter);
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        frm.ShowInTaskbar = false;
                        frm.StartPosition = FormStartPosition.CenterScreen;
                        _chromeBrowser.ExecuteScriptAsync("BERP.loadTradeRelation();");
                    }
                }), code, Convert.ToByte(isInternational));
               
            }

            public void viewledger(string code, int accountid)
            {
                _parent.Invoke(new ProcessTowParameter<string, int>(delegate(string tradRelCode, int accId)
                {
                    Account account = AccountingClient.GetAccount<Account>(accId);
                    if (account != null)
                        new LedgerViewer(CostCenter.ROOT_COST_CENTER, new int[] { account.id }, "Ledger of property: {0}".format(account.NameCode)).Show(_parent);
                }), code, accountid);

            }

            public void viewtransaction(string code)
            {
                _parent.Invoke(new ProcessSingleParameter<string>(delegate(string tradeRelCode)
                {
                    TradeRelation tradeRelation = iERPTransactionClient.GetRelation(tradeRelCode);
                    if (_mtransactionView == null)
                        _mtransactionView = CreateTransactionViewer(tradeRelation);
                    if (_mtransactionView.ShowDialog() == DialogResult.OK)
                    {

                    }
                }), code);
            }

            private TransactionViewer CreateTransactionViewer(TradeRelation relation)
            {
                TransactionViewer frm = new TransactionViewer(relation);
                return frm;
            }
        }

        private ChromiumWebBrowser _chromeBrowser;
        private readonly string _code;
        private readonly bool _isInternational;

        public TradeRelationProfileForm(string code, bool isInternational)
        {
            _code = code;
            _isInternational = isInternational;
            InitializeComponent();
            InitializeChromium();
            _chromeBrowser.RegisterJsObject("TradeRelationProfile", new CefTradeRelationProfile(this, _chromeBrowser));
            
        }

        public void InitializeChromium()
        {
            string url = "{0}{1}?sid={2}&code={3}&isInternational={4}".format(
                 ConfigurationManager.AppSettings["webserver"]
                    , "trade_profile.html", ApplicationClient.SessionID, _code, _isInternational);
            _chromeBrowser = new ChromiumWebBrowser(url);
            this.Controls.Add(_chromeBrowser);
            _chromeBrowser.Dock = DockStyle.Fill;

            // Allow the use of local resources in the browser
            BrowserSettings browserSettings = new BrowserSettings();
            browserSettings.FileAccessFromFileUrls = CefState.Enabled;
            browserSettings.UniversalAccessFromFileUrls = CefState.Enabled;
            _chromeBrowser.BrowserSettings = browserSettings;

            //_chromeBrowser.IsBrowserInitializedChanged += chromeBrowser_IsBrowserInitializedChanged;

        }

        void chromeBrowser_IsBrowserInitializedChanged(object sender, IsBrowserInitializedChangedEventArgs e)
        {
            if (_chromeBrowser.IsBrowserInitialized)
                _chromeBrowser.ShowDevTools();
        }

        
    }
}
