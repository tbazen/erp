﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Threading;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP.Client
{


    public partial class TradeRelationSearchPopup : DevExpress.XtraEditors.XtraForm, IItemPickerPopup
    {
        public event EventHandler ItemSelected;
        public TradeRelation selectedRelation = null;
        TradeRelationType _searchType = TradeRelationType.All;
        string _query;
        bool _upateSeach=false;
        Thread _searchThread = null;
        TradeRelation[] _searchResult = null;
        TradeRelationCategory [] _cats=null;
        Exception _ex = null;
        
        public TradeRelationSearchPopup()
        {
            InitializeComponent();
            base.SetStyle(ControlStyles.Selectable, false);
            labelStatus.Text = "Ready";
        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams ret = base.CreateParams;
                ret.Style = (int)Flags.WindowStyles.WS_THICKFRAME |
                   (int)Flags.WindowStyles.WS_CHILD;
                ret.ExStyle |= (int)Flags.WindowStyles.WS_EX_NOACTIVATE |
                   (int)Flags.WindowStyles.WS_EX_TOOLWINDOW;
                ret.X = this.Location.X;
                ret.Y = this.Location.Y;

                return ret;
            }
        } 
        public TradeRelation getTopItem()
        {
            if (_searchResult == null || _searchResult.Length == 0)
                return null;
            return _searchResult[0];
        }
        
        public void setSearch(string q, TradeRelationType byCode)
        {
            _query = q;
            _searchType=byCode;
            _upateSeach = true;
            buttonNewItem.Text = "New " + (byCode == TradeRelationType.Supplier ? "Supplier" : "Customer");
            if (_searchThread == null || !_searchThread.IsAlive)
            {
                startSeach();
            }
        }
        public TradeRelationType searchType
        {
            get { return _searchType; }
        }
        void showSearchResultError(Exception ex)
        {
            MessageBox.ShowException(ex);
        }
        void listSearchResult()
        {

            listView.Items.Clear();
            int i = 0;
            foreach (TradeRelation item in _searchResult)
            {
                ListViewItem li = new ListViewItem(item.Code);
                li.Tag = item;
                li.SubItems.Add(item.Name);
                li.SubItems.Add(item.TIN);
                li.SubItems.Add(_cats[i] == null ? "" : _cats[i].name);
                
                listView.Items.Add(li);
                i++;
            }

            if (_upateSeach)
                startSeach();
            else
                labelStatus.Text = "Ready";
        }
        private void startSeach()
        {
            _upateSeach = false;
            labelStatus.Text = "Searching..";

            _searchThread = new Thread(delegate()
                {

                    try
                    {
                        int N;
                        string[] fields = new string[] { "Code", "Name", "TIN" };
                        object[] vals = new string[] { _query, _query, _query };
                        _searchResult = iERPTransactionClient.searchTradeRelation(null, new TradeRelationType[] { _searchType }, 0, 30, vals, fields, out N);
                        _cats = new TradeRelationCategory[_searchResult.Length];
                        for (int i = 0; i < _cats.Length; i++)
                            _cats[i] = iERPTransactionClient.getTradeRelationCategory(_searchResult[i].pcode);
                        this.BeginInvoke(new INTAPS.UI.HTML.ProcessParameterless(listSearchResult));
                    }
                    catch (Exception ex)
                    {
                        this.BeginInvoke(new INTAPS.UI.HTML.ProcessSingleParameter<Exception>(showSearchResultError), ex);
                    }
                });
            _searchThread.Start();
            timer.Enabled = true;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        
        bool _ignoreEvent = false;
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent)
                return;
            _ignoreEvent = true;
            try
            {
                if (listView.SelectedItems.Count > 0)
                {
                    ListViewItem selected = listView.SelectedItems[0];
                    selectedRelation = selected.Tag as TradeRelation;
                    if (ItemSelected != null)
                        ItemSelected(this, null);
                }
            }
            finally
            {
                _ignoreEvent = false;
            }
        }
        RegisterRelation relationEditor;
        private void buttonNewItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            TradeRelationCategoryPicker catPicker = new TradeRelationCategoryPicker();
            if (catPicker.ShowDialog(this) != DialogResult.OK)
                return;
            relationEditor = new RegisterRelation(catPicker.category.code,null);
            if (relationEditor.ShowDialog(this) == DialogResult.OK)
            {
                selectedRelation = iERPTransactionClient.GetRelation(relationEditor.RelationCode);
                raiseItemSelectedEvent();
            }
        }

        private void raiseItemSelectedEvent()
        {
            if (ItemSelected != null)
            {
                ItemSelected(this, null);
            }
        }



        public void selectTopItem()
        {
            if (_searchResult != null && _searchResult.Length > 0)
            {
                selectedRelation = _searchResult[0];
                raiseItemSelectedEvent();
            }
        }
    }
 
}