﻿namespace BIZNET.iERP.Client
{
    partial class TransactionViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.exportButtonTran = new INTAPS.UI.HTML.BowserController();
            this.buttonShowTran = new DevExpress.XtraEditors.SimpleButton();
            this.browserTran = new INTAPS.UI.HTML.ControlBrowser();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.exportButtonTran);
            this.panel1.Controls.Add(this.buttonShowTran);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(790, 94);
            this.panel1.TabIndex = 2;
            // 
            // exportButtonTran
            // 
            this.exportButtonTran.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exportButtonTran.Dock = System.Windows.Forms.DockStyle.None;
            this.exportButtonTran.Location = new System.Drawing.Point(694, 0);
            this.exportButtonTran.Name = "exportButtonTran";
            this.exportButtonTran.ShowBackForward = false;
            this.exportButtonTran.ShowRefresh = false;
            this.exportButtonTran.Size = new System.Drawing.Size(65, 25);
            this.exportButtonTran.TabIndex = 3;
            this.exportButtonTran.Text = "bowserController1";
            // 
            // buttonShowTran
            // 
            this.buttonShowTran.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonShowTran.Location = new System.Drawing.Point(647, 41);
            this.buttonShowTran.Name = "buttonShowTran";
            this.buttonShowTran.Size = new System.Drawing.Size(122, 23);
            this.buttonShowTran.TabIndex = 2;
            this.buttonShowTran.Text = "Show &Transactions";
            this.buttonShowTran.Click += new System.EventHandler(this.buttonShowTran_Click);
            // 
            // browserTran
            // 
            this.browserTran.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browserTran.Location = new System.Drawing.Point(0, 94);
            this.browserTran.MinimumSize = new System.Drawing.Size(20, 20);
            this.browserTran.Name = "browserTran";
            this.browserTran.Size = new System.Drawing.Size(790, 538);
            this.browserTran.StyleSheetFile = "berp.css";
            this.browserTran.TabIndex = 4;
            // 
            // TransactionViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(790, 632);
            this.Controls.Add(this.browserTran);
            this.Controls.Add(this.panel1);
            this.Name = "TransactionViewer";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Transaction Documents";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private INTAPS.UI.HTML.BowserController exportButtonTran;
        private DevExpress.XtraEditors.SimpleButton buttonShowTran;
        private INTAPS.UI.HTML.ControlBrowser browserTran;
    }
}