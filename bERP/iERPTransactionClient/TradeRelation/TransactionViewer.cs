﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using INTAPS.UI.HTML;

namespace BIZNET.iERP.Client
{
    public partial class TransactionViewer : Form
    {
        private TradeRelation relation;
        class HTMLPlaceHodler : IHTMLBuilder
        {
            public string html;
            public TransactionViewer parent;
            public void Build()
            {
                parent.Build();
            }

            public string GetOuterHtml()
            {
                return html;
            }

            public bool ProcessUrl(string path, System.Collections.Hashtable query)
            {
                return parent.ProcessUrl(path, query);
            }

            public void SetHost(IHTMLDocumentHost host)
            {
                parent.SetHost(host);
            }

            public string WindowCaption
            {
                get { return "Report"; }
            }
        }
        public TransactionViewer()
        {
            InitializeComponent();
        }

        public TransactionViewer(TradeRelation relation):this()
        {
            this.relation = relation;
        }

        private void buttonShowTran_Click(object sender, EventArgs e)
        {
            exportButtonTran.Enabled = true;
            exportButtonTran.SetBrowser(browserTran);
            browserTran.LoadTextPage("Ledger", "<h1>Loading trasaction please wait, it might take time...</h1>");

            new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(
                delegate(object par)
                {
                    TransactionViewer.HTMLPlaceHodler p;
                    try
                    {
                        p = new TransactionViewer.HTMLPlaceHodler() { parent = this, html = iERPTransactionClient.getTradeRelationTransactions(this.relation.Code, DateTime.Parse("1/1/1900").Ticks, DateTime.Now.Ticks, null) };
                    }
                    catch (Exception ex)
                    {
                        p = new TransactionViewer.HTMLPlaceHodler() { parent = this, html = string.Format("<h1>Error generating transactions</h1><h2>{0}</h2><h3>{1}</h3>", System.Web.HttpUtility.HtmlEncode(ex.Message), System.Web.HttpUtility.HtmlEncode(ex.StackTrace)) };
                    }
                    this.Invoke(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                    {
                        browserTran.LoadControl(p);
                    }
                    ));
                })).Start(null);

        }

        public void Build()
        {

        }
        public bool ProcessUrl(string path, System.Collections.Hashtable query)
        {
            return AccountExplorer.ProcessUrl(this, path, query, new INTAPS.UI.HTML.ProcessParameterless(realoadTransaction));
        }

        private void realoadTransaction()
        {
            browserTran.LoadTextPage("Transaction", "<h1>Click show transaction documents...</h1>");
        }

        public void SetHost(INTAPS.UI.HTML.IHTMLDocumentHost host)
        {

        }
        
    }
}
