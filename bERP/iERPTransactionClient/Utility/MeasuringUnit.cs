﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.IO;
using System.Drawing.Imaging;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraGrid;
using DevExpress.Utils;
using INTAPS.UI.ButtonGrid;

namespace BIZNET.iERP.Client
{
    public delegate bool ValidateInputBoxInput(string input,out string errorMessage);
    public partial class bERPInputBox : XtraForm
    {
        public string inputValue = null;

        ValidateInputBoxInput validator;
        public bERPInputBox(string title, string propmpt, string defaultValue, ValidateInputBoxInput validator)
        {
            this.InitializeComponent();
            this.Text = title;
            this.labelInput.Text = propmpt;
            this.textInput.Text = defaultValue;
            this.validator = validator;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (validator != null)
            {
                string err;
                if (!validator(textInput.Text, out err))
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError(err);
                    return;
                }
            }
            this.inputValue = textInput.Text;
            this.DialogResult = DialogResult.OK;
        }

        private void bERPInputBox_Load(object sender, EventArgs e)
        {
        
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public static bool showInputBox(string title, string prompt, string defaultValue, ValidateInputBoxInput validator, out string input)
        {
            bERPInputBox ib=new bERPInputBox(title,prompt,defaultValue,validator);
            if(ib.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm)==DialogResult.OK)
            {
                input=ib.inputValue;
                return true;
            }
            else
            {
                input=null;
                return false;
            }
        }
    }
}