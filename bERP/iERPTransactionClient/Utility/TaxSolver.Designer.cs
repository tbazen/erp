﻿namespace BIZNET.iERP.Client
{
    partial class TaxSolver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textPension = new DevExpress.XtraEditors.TextEdit();
            this.textIncomeTax = new DevExpress.XtraEditors.TextEdit();
            this.labelError = new DevExpress.XtraEditors.LabelControl();
            this.textPensionRate = new DevExpress.XtraEditors.TextEdit();
            this.textNet = new DevExpress.XtraEditors.TextEdit();
            this.textBase = new DevExpress.XtraEditors.TextEdit();
            this.textAdditionalTaxable = new DevExpress.XtraEditors.TextEdit();
            this.checkPension = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPensionRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutBase = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutNet = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.textTaxFormula = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textPension.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textIncomeTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textPensionRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textNet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBase.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textAdditionalTaxable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPension.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPensionRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutNet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textTaxFormula.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(481, 359);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.layoutControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(475, 331);
            this.xtraTabPage1.Text = "Solver";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textPension);
            this.layoutControl1.Controls.Add(this.textIncomeTax);
            this.layoutControl1.Controls.Add(this.labelError);
            this.layoutControl1.Controls.Add(this.textPensionRate);
            this.layoutControl1.Controls.Add(this.textNet);
            this.layoutControl1.Controls.Add(this.textBase);
            this.layoutControl1.Controls.Add(this.textAdditionalTaxable);
            this.layoutControl1.Controls.Add(this.checkPension);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(762, 135, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(475, 331);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textPension
            // 
            this.textPension.Location = new System.Drawing.Point(141, 105);
            this.textPension.Name = "textPension";
            this.textPension.Properties.Appearance.Options.UseTextOptions = true;
            this.textPension.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textPension.Properties.Mask.EditMask = "#,########0.00;";
            this.textPension.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textPension.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textPension.Properties.ReadOnly = true;
            this.textPension.Size = new System.Drawing.Size(319, 20);
            this.textPension.StyleController = this.layoutControl1;
            this.textPension.TabIndex = 11;
            // 
            // textIncomeTax
            // 
            this.textIncomeTax.Location = new System.Drawing.Point(141, 75);
            this.textIncomeTax.Name = "textIncomeTax";
            this.textIncomeTax.Properties.Appearance.Options.UseTextOptions = true;
            this.textIncomeTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textIncomeTax.Properties.Mask.EditMask = "#,########0.00;";
            this.textIncomeTax.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textIncomeTax.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textIncomeTax.Properties.ReadOnly = true;
            this.textIncomeTax.Size = new System.Drawing.Size(319, 20);
            this.textIncomeTax.StyleController = this.layoutControl1;
            this.textIncomeTax.TabIndex = 10;
            // 
            // labelError
            // 
            this.labelError.Location = new System.Drawing.Point(12, 215);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(13, 13);
            this.labelError.StyleController = this.layoutControl1;
            this.labelError.TabIndex = 9;
            this.labelError.Text = "Ok";
            // 
            // textPensionRate
            // 
            this.textPensionRate.Location = new System.Drawing.Point(141, 188);
            this.textPensionRate.Name = "textPensionRate";
            this.textPensionRate.Properties.Appearance.Options.UseTextOptions = true;
            this.textPensionRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textPensionRate.Properties.Mask.EditMask = "#,########0.00;";
            this.textPensionRate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textPensionRate.Size = new System.Drawing.Size(319, 20);
            this.textPensionRate.StyleController = this.layoutControl1;
            this.textPensionRate.TabIndex = 8;
            this.textPensionRate.EditValueChanged += new System.EventHandler(this.textPensionRate_EditValueChanged);
            // 
            // textNet
            // 
            this.textNet.Location = new System.Drawing.Point(141, 135);
            this.textNet.Name = "textNet";
            this.textNet.Properties.Appearance.Options.UseTextOptions = true;
            this.textNet.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textNet.Properties.Mask.EditMask = "#,########0.00;";
            this.textNet.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textNet.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textNet.Size = new System.Drawing.Size(319, 20);
            this.textNet.StyleController = this.layoutControl1;
            this.textNet.TabIndex = 7;
            this.textNet.EditValueChanged += new System.EventHandler(this.textNet_EditValueChanged);
            // 
            // textBase
            // 
            this.textBase.Location = new System.Drawing.Point(141, 15);
            this.textBase.Name = "textBase";
            this.textBase.Properties.Appearance.Options.UseTextOptions = true;
            this.textBase.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textBase.Properties.Mask.EditMask = "#,########0.00;";
            this.textBase.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textBase.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textBase.Size = new System.Drawing.Size(319, 20);
            this.textBase.StyleController = this.layoutControl1;
            this.textBase.TabIndex = 5;
            this.textBase.EditValueChanged += new System.EventHandler(this.textPensionBase_EditValueChanged);
            // 
            // textAdditionalTaxable
            // 
            this.textAdditionalTaxable.Location = new System.Drawing.Point(141, 45);
            this.textAdditionalTaxable.Name = "textAdditionalTaxable";
            this.textAdditionalTaxable.Properties.Appearance.Options.UseTextOptions = true;
            this.textAdditionalTaxable.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textAdditionalTaxable.Properties.Mask.EditMask = "#,########0.00;";
            this.textAdditionalTaxable.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textAdditionalTaxable.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textAdditionalTaxable.Size = new System.Drawing.Size(319, 20);
            this.textAdditionalTaxable.StyleController = this.layoutControl1;
            this.textAdditionalTaxable.TabIndex = 4;
            this.textAdditionalTaxable.EditValueChanged += new System.EventHandler(this.textTaxable_EditValueChanged);
            // 
            // checkPension
            // 
            this.checkPension.EditValue = true;
            this.checkPension.Location = new System.Drawing.Point(12, 162);
            this.checkPension.Name = "checkPension";
            this.checkPension.Properties.Caption = "Calculate Pension";
            this.checkPension.Size = new System.Drawing.Size(451, 19);
            this.checkPension.StyleController = this.layoutControl1;
            this.checkPension.TabIndex = 6;
            this.checkPension.CheckedChanged += new System.EventHandler(this.checkPension_CheckedChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutPensionRate,
            this.layoutBase,
            this.layoutControlItem8,
            this.layoutNet,
            this.layoutControlItem2,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(475, 293);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textAdditionalTaxable;
            this.layoutControlItem1.CustomizationFormText = "Taxable";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(455, 30);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem1.Text = "Additional Taxable";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.checkPension;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(455, 23);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutPensionRate
            // 
            this.layoutPensionRate.Control = this.textPensionRate;
            this.layoutPensionRate.CustomizationFormText = "Pension Contribution(%):";
            this.layoutPensionRate.Location = new System.Drawing.Point(0, 173);
            this.layoutPensionRate.Name = "layoutPensionRate";
            this.layoutPensionRate.Size = new System.Drawing.Size(455, 30);
            this.layoutPensionRate.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutPensionRate.Text = "Pension Contribution(%):";
            this.layoutPensionRate.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutBase
            // 
            this.layoutBase.Control = this.textBase;
            this.layoutBase.CustomizationFormText = "Pension Base";
            this.layoutBase.Location = new System.Drawing.Point(0, 0);
            this.layoutBase.Name = "layoutBase";
            this.layoutBase.Size = new System.Drawing.Size(455, 30);
            this.layoutBase.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutBase.Text = "Gross Salary";
            this.layoutBase.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.labelError;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 203);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(455, 108);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutNet
            // 
            this.layoutNet.Control = this.textNet;
            this.layoutNet.CustomizationFormText = "Net";
            this.layoutNet.Location = new System.Drawing.Point(0, 120);
            this.layoutNet.Name = "layoutNet";
            this.layoutNet.Size = new System.Drawing.Size(455, 30);
            this.layoutNet.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutNet.Text = "Net";
            this.layoutNet.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textIncomeTax;
            this.layoutControlItem2.CustomizationFormText = "Income Tax";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(455, 30);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem2.Text = "Income Tax";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(122, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textPension;
            this.layoutControlItem4.CustomizationFormText = "Pension";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(455, 30);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem4.Text = "Pension";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(122, 13);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.layoutControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(475, 331);
            this.xtraTabPage2.Text = "Advanced";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.textTaxFormula);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(475, 331);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // textTaxFormula
            // 
            this.textTaxFormula.Location = new System.Drawing.Point(116, 15);
            this.textTaxFormula.Name = "textTaxFormula";
            this.textTaxFormula.Size = new System.Drawing.Size(344, 20);
            this.textTaxFormula.StyleController = this.layoutControl2;
            this.textTaxFormula.TabIndex = 4;
            this.textTaxFormula.EditValueChanged += new System.EventHandler(this.textTaxFormula_EditValueChanged);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(475, 331);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textTaxFormula;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(455, 311);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItem5.Text = "Income Tax Formula";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(97, 13);
            // 
            // TaxSolver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 359);
            this.Controls.Add(this.xtraTabControl1);
            this.Name = "TaxSolver";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tax Solver";
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textPension.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textIncomeTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textPensionRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textNet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBase.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textAdditionalTaxable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPension.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPensionRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutNet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textTaxFormula.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit textNet;
        private DevExpress.XtraEditors.CheckEdit checkPension;
        private DevExpress.XtraEditors.TextEdit textBase;
        private DevExpress.XtraEditors.TextEdit textAdditionalTaxable;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutBase;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutNet;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.TextEdit textTaxFormula;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.TextEdit textPensionRate;
        private DevExpress.XtraLayout.LayoutControlItem layoutPensionRate;
        private DevExpress.XtraEditors.LabelControl labelError;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.TextEdit textPension;
        private DevExpress.XtraEditors.TextEdit textIncomeTax;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
    }
}