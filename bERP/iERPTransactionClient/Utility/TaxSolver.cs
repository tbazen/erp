﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.Payroll.Client;
using INTAPS.Payroll;
using INTAPS.Evaluator;

namespace BIZNET.iERP.Client
{
    public partial class TaxSolver : DevExpress.XtraEditors.XtraForm
    {
        const double PRECISION = 1e-6;
        const int MAX_ITER= 1000;

        double salaryBase;
        double variableTaxable;
        double pensionRate;
        double net;
        Symbolic _taxformula;
        bool _ignoreEvent = false;
        bool calculateBase = true;
        void updateFormula()
        {
            _taxformula = new Symbolic(new Stack());
            _taxformula.SetSymbolProvider(new INTAPS.Evaluator.DefaultProvider());
            _taxformula.Expression = textTaxFormula.Text;
        }
        void updateValues()
        {
            _ignoreEvent=true;
            try
            {

                pensionRate = 0;
                if (checkPension.Checked)
                {
                    if (double.TryParse(textPensionRate.Text, out pensionRate))
                    {
                        pensionRate = pensionRate / 100;
                    }
                    else
                    {
                        textPensionRate.Text = "";
                        pensionRate = 0;
                    }
                }
                if (!double.TryParse(textNet.Text, out net))
                {
                    textNet.Text = "";
                    net = 0;
                }
                if (!double.TryParse(textAdditionalTaxable.Text, out variableTaxable))
                {
                    textAdditionalTaxable.Text = "";
                    variableTaxable = 0;
                }
                if (!double.TryParse(textBase.Text, out salaryBase))
                {
                    textBase.Text = "";
                    salaryBase = 0;
                }
                updateTaxes();
            }
            finally
            {
                _ignoreEvent = false;
            }

        }

        private void updateTaxes()
        {
            try
            {
                textIncomeTax.Text = (salaryBase + variableTaxable - netFunction(salaryBase, variableTaxable, 0)).ToString();
            }
            catch (Exception ex)
            {
                textIncomeTax.Text = "Error:" + ex.Message;
            }
            try
            {
                textPension.Text = (salaryBase * pensionRate).ToString();
            }
            catch (Exception ex)
            {
                textPension.Text = "Error:" + ex.Message;
            }
        }
        public TaxSolver()
        {
            InitializeComponent();
            int fid = Convert.ToInt32(iERPTransactionClient.GetSystemParamter("IncomeTaxPayrollFormulaID"));
            PayrollComponentFormula taxf = null;
            foreach (PayrollComponentFormula f in PayrollClient.GetPCFormulae((int)iERPTransactionClient.GetSystemParamter("IncomeTaxPayrollComponentID")))
            {
                if (f.ID == fid)
                {
                    taxf = f;
                    break;
                }
            }
            if (taxf == null)
                throw new INTAPS.ClientServer.ServerUserMessage("Income tax formula not found");
            textTaxFormula.Text= taxf.GetVarFormula("tax");
            textPensionRate.Text=((double)PayrollClient.GetSystemParameters(new string[]{"EmployeePensionRate"})[0]*100).ToString();
        }

        private void textTaxable_EditValueChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return; 
            updateValues();
            if (calculateBase)
                updateBaseSalary();
            else
                updateNet();
        }

        private void updateNet()
        {
            _ignoreEvent = true;
            try
            {
                setCalcuationMode(false);
                net = netFunction(salaryBase, variableTaxable, pensionRate);
                textNet.Text = net.ToString();
            }
            catch (Exception ex)
            {
                labelError.Text ="Error:"+ ex.Message;
            }
            finally
            {
                _ignoreEvent = false;
                updateTaxes();

            }
        }
        public double netFunction(double gross, double additional, double pensionRate)
        {
            _taxformula[0] = new EData(DataType.Float, gross + additional);
            EData tax = _taxformula.Evaluate();
            if (tax.Type != DataType.Float)
                throw new Exception(tax.ToString());
            return gross + additional - (double)tax.Value - pensionRate * gross;
        }
        private void textNet_EditValueChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            updateValues();
            updateBaseSalary();
        }
        void setCalcuationMode(bool calcBase)
        {
            calculateBase=calcBase;
            if (calculateBase)
            {
                layoutNet.Text = "Net:";
                layoutBase.Text = "Gross Salary (Calculated):";
            }
            else
            {
                layoutNet.Text = "Net (Calculated):";
                layoutBase.Text = "Gross Salary:";

            }
        }
        private void updateBaseSalary()
        {
            _ignoreEvent = true;
            try
            {
                setCalcuationMode(true);
                double g1 = net;
                double g2 = net * 2;
                double g = 0;
                int iterCount = 0;
                do
                {
                    double n1 = netFunction(g1, variableTaxable, pensionRate);
                    double n2 = netFunction(g2, variableTaxable, pensionRate);
                    if (Math.Abs(n1 - n2) < PRECISION)
                    {
                        g = (g1 + g2) / 2;
                        break;
                    }
                    g = g1 + (net - n1) * (g2 - g1) / (n2 - n1);
                    double d1, d2;
                    if ((d1 = Math.Abs(g - g1)) < PRECISION)
                    {
                        g = g1;
                        break;
                    }
                    if ((d2 = Math.Abs(g - g2)) < PRECISION)
                    {
                        g = g2;
                        break;
                    }
                    if (d1 < d2)
                        g2 = g;
                    else
                        g1 = g;
                    iterCount++;
                    if (iterCount >= MAX_ITER)
                        throw new Exception("Solution not found");
                }
                while (true);
                salaryBase = g;
                textBase.Text = g.ToString();
                labelError.Text = "Ok " + iterCount + " tries";
            }
            catch (Exception ex)
            {
                labelError.Text = "Error:"+ex.Message;
            }
            finally
            {
                _ignoreEvent = false;
                updateTaxes();

            }
        }
        private void textPensionBase_EditValueChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            updateValues();
            updateNet();
        }

        private void checkPension_CheckedChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            layoutPensionRate.Visibility = checkPension.Checked ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            updateValues();
            if (calculateBase)
                updateBaseSalary();
            else
                updateNet();
        }

        private void textPensionRate_EditValueChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            updateValues();
            if (calculateBase)
                updateBaseSalary();
            else
                updateNet();
        }

        private void textTaxFormula_EditValueChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            updateFormula();
            if (calculateBase)
                updateBaseSalary();
            else
                updateNet();
        }
    }
}