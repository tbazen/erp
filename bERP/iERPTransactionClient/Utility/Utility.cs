using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Xml;
using DevExpress.XtraEditors.DXErrorProvider;
using INTAPS.Accounting;
using BIZNET.iERP.Client;
using DevExpress.XtraReports.UI;
using System.Drawing;
using DevExpress.XtraPrinting;
using INTAPS.ClientServer;
using INTAPS.Accounting.Client;

namespace BIZNET.iERP
{
    public class MessageBox
    {
        public static DialogResult ShowErrorMessage(string error)
        {
            return XtraMessageBox.Show(error, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
        }

        public static DialogResult ShowWarningMessage(string warning)
        {
            return XtraMessageBox.Show(warning, "Warning", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning);
        }
        public static DialogResult ShowNormalWarningMessage(string warning)
        {
            return XtraMessageBox.Show(warning, "Warning", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
        public static DialogResult ShowSuccessMessage(string success)
        {
            return XtraMessageBox.Show(success, "Success", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
        }

        internal static void ShowException(Exception ex)
        {
            string msg = null;
            while (ex != null)
            {
                if (ex is AccessDeniedException)
                {
                    ShowErrorMessage(ex.Message);
                    return;
                }
                if (ex is ServerUserMessage)
                {
                    ShowErrorMessage(ex.Message);
                    return;
                }
                
                if (ex is ServerValidationErrorException)
                {
                    ShowErrorMessage(ex.Message);
                    return;
                }
                if (ex is SystemConfigurationErrorException)
                {
                    MessageBox.ShowErrorMessage("System not properly configured. Additional info :" + ex.Message);
                    return;
                }
                if (msg == null)
                    msg = ex.Message;
                else
                    msg = ex.Message + "\n" + msg;
                ex = ex.InnerException;
            }
            MessageBox.ShowErrorMessage("Unknown system error, please report to system adminisrator.\n" + msg);
        }
    }

    public class XMLUtility
    {
        public static object XmlToObject(string xml, Type type)
        {
            if (string.IsNullOrEmpty(xml))
                return null;
            XmlSerializer x = new XmlSerializer(type);
            return x.Deserialize(new System.IO.StringReader(xml));
        }
        public static string ObjectToXml(object o)
        {
            XmlSerializer x = new XmlSerializer(o.GetType());
            using (System.IO.StringWriter sw = new System.IO.StringWriter())
            {
                x.Serialize(sw, o);
                sw.Close();
                return sw.ToString();
            }
        }
    }

    public class NonEmptyNumericValidationRule : ValidationRule
    {
        public override bool Validate(Control control, object value)
        {
            double val;
            if (value != null && value.ToString() != "")
                val = double.Parse(value.ToString());
            else
                val = 0d;
            //If input is not blank validate further
            if (ValidationHelper.Validate(val, ConditionOperator.IsNotBlank, null, null, null))
            {
                if (val == 0d) //0 input value is not allowed
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else //If input is blank or empty
            {
                return false;
            }
        }
    }
    public class UnclaimedPeriodValidationRule : ValidationRule
    {
        double unclaimed;
        public UnclaimedPeriodValidationRule(double unclaimedAmount)
        {
            unclaimed = unclaimedAmount;
        }
        public override bool Validate(Control control, object value)
        {
            double val;
            if (value != null && value.ToString() != "")
                val = double.Parse(value.ToString());
            else
                val = 0d;
            //If input is not blank validate further
            if (ValidationHelper.Validate(val, ConditionOperator.IsNotBlank, null, null, null))
            {
                if (val == 0d) //0 input value is not allowed
                {
                    return false;
                }
                else
                {
                    if (Account.AmountGreater(val, unclaimed))
                    {
                        ErrorText = "Payment amount cannot be greater than salary amount of the selected period";
                        return false;
                    }
                    else
                        return true;
                }
            }
            else //If input is blank or empty
            {
                return false;
            }
        }
    }
    public class EmployeeWorkKindValidationRule : ValidationRule
    {
        EmployeeWorkValidationKind empWorkKind;
        bool ValidateBlank;
        public EmployeeWorkKindValidationRule(EmployeeWorkValidationKind workKind,bool validateEmpty)
        {
            empWorkKind = workKind;
            ValidateBlank = validateEmpty;
        }
        public override bool Validate(Control control, object value)
        {
            double val;
            if (value != null && value.ToString() != "")
                val = double.Parse(value.ToString());
            else
                val = 0d;
            if (ValidateBlank)
            {
                //If input is not blank validate further
                if (ValidationHelper.Validate(val, ConditionOperator.IsNotBlank, null, null, null))
                {
                    return ValidateInput(val);
                }
                else //If input is blank or empty
                {
                    return false;
                }
            }
            else
            {
                return ValidateInput(val);
            }
        }

        private bool ValidateInput(double val)
        {

            if (empWorkKind == EmployeeWorkValidationKind.DayHour)
            {
                if (val > 24)
                {
                    ErrorText = "Working hour(s) cannot be greater than 24.";
                    return false;
                }
                else
                    return true;
            }
            else if (empWorkKind == EmployeeWorkValidationKind.Day)
            {
                if (val > 30)
                {
                    ErrorText = "Working day(s) cannot be greater than 30.";
                    return false;
                }
                else
                    return true;
            }
            else //Month hour
            {
                if (val > 720)
                {
                    ErrorText = "Working month(s) cannot be greater than 720.";
                    return false;
                }
                else
                    return true;
            }

        }

    }
    public class TINValidationRule : ValidationRule
    {
        public override bool Validate(Control control, object value)
        {

            //If input is not blank validate further
            string val = value==null?null:value.ToString();
            if (ValidationHelper.Validate(val, ConditionOperator.IsNotBlank, null, null, null))
            {
                int length = val.Length;
                if (length != 10)
                {
                    return false;
                }
            }
            else //If input is blank or empty
            {
                return false;
            }
            return true;
        }
    }
    public class CodeFormatValidationRule : ValidationRule
    {
        public override bool Validate(Control control, object value)
        {
            string val = value.ToString();
            if (ValidationHelper.Validate(val, ConditionOperator.IsNotBlank, null, null, null))
            {
                int num;
                if (int.TryParse(val, out num))
                {
                    if (num != 0)
                    {
                        return false;
                    }
                }
                else
                    return false;
            }
            else //If input is blank or empty
            {
                return false;
            }
            return true;
        }
    }
    public class PerDiemDayAdjustmentValidationRule : ValidationRule
    {
        double numberOfDays;
        private double m_Minimum;
        private double m_Maximum;
        public PerDiemDayAdjustmentValidationRule(double noOfDays)
        {
            numberOfDays = Math.Round(noOfDays, 2);
        }

        public override bool Validate(Control control, object value)
        {
            double adjustment;
            if (value != null && value.ToString() != "")
                adjustment = double.Parse(value.ToString());
            else
            {
                ErrorText = "Please enter number of days and try again!";
                return false;
            }
             //   adjustment = 0d;
            m_Minimum = 0;
            m_Maximum = 0;
            if (adjustment != 0)
            {
                m_Minimum = numberOfDays - 1;
                m_Maximum = numberOfDays + 1;

                if (adjustment >= m_Minimum && adjustment <= m_Maximum)
                    return true;
                else
                {
                    ErrorText = String.Format("You entered invalid day adjustment. Please enter a day between {0} and {1}", m_Minimum, m_Maximum);
                    return false;
                }
            }
            else
                return true;

        }
    }
    public class PerDiemAmountValidationRule : ValidationRule
    {
        double empGrossSalary;
        public PerDiemAmountValidationRule(double grossSalary)
        {
            empGrossSalary = grossSalary;
        }
        public override bool Validate(Control control, object value)
        {
            double val;
            double maximumPerDiem = 150;
            if (value != null && value.ToString() != "")
                val = double.Parse(value.ToString());
            else
                val = 0d;
            //If input is not blank validate further
            if (ValidationHelper.Validate(val, ConditionOperator.IsNotBlank, null, null, null))
            {
                if (val == 0d) //0 input value is not allowed
                {
                    return false;
                }
                else
                {
                    double perDiem = empGrossSalary * 0.04;
                    if (val <= 150)
                    {
                        if (val <= perDiem)
                            return true;
                        else
                            return false;
                    }
                    else
                        return false;

                }
            }
            else //If input is blank or empty
            {
                return false;
            }
        }
    }
    public class TransportAllowanceValidationRule : ValidationRule
    {
        double grossSalary;
        private string message;
        public TransportAllowanceValidationRule(double salary)
        {
            grossSalary = salary;
        }
        public override bool Validate(Control control, object value)
        {
            double transportAllowance;
            if (value != null && value.ToString() != "")
                transportAllowance = double.Parse(value.ToString());
            else
                transportAllowance = 0d;
            if (transportAllowance != 0d) //If input value is not zero, process validation
            {
                double taxableAmount = ValidateTransportAllowance(grossSalary, transportAllowance, out message);
                if (taxableAmount > 0)
                {
                    this.ErrorText = message;
                    this.ErrorType = ErrorType.Information;
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        public static double ValidateTransportAllowance(double grossSalary, double transportAllowance, out string message)
        {
            const double rate = 0.25; //rate used to calculate taxable allowance
            double taxFreeAmount1 = rate * grossSalary;
            const double taxFreeAmount2 = 1000;
            double taxableAmount = 0d;
            message = String.Empty;
            if (transportAllowance <= taxFreeAmount1)
            {
                if (transportAllowance > taxFreeAmount2)
                {
                    taxableAmount = transportAllowance - taxFreeAmount2;
                    message = String.Format("{0} Birr will be added to the gross salarly and become taxable.\nThe tax free transport allowance amount can not be greater than\n1/4th of the gross salary and can not exceed {1} Birr.\n(Refer Directive No.21 2001)"
                                       , taxableAmount
                                       , taxFreeAmount2);
                }
            }
            else
            {
                if (taxFreeAmount1 <= taxFreeAmount2)
                    taxableAmount = transportAllowance - taxFreeAmount1;
                else
                    taxableAmount = transportAllowance - taxFreeAmount2;
                message = String.Format("{0} Birr will be added to the gross salarly and become taxable.\nThe tax free transport allowance amount can not be greater than\n1/4th of the gross salary and can not exceed {1} Birr.\n(Refer Directive No.21 2001)"
                                   , taxableAmount
                                   , taxFreeAmount2);
            }
            return taxableAmount;
        }

    }
    public class CostSharingPayableValidationRule : ValidationRule
    {
        double grossSalary;
        public CostSharingPayableValidationRule(double salary)
        {
            grossSalary = salary;
        }
        public override bool Validate(Control control, object value)
        {
            string message;
            double costSharingPayable;
            if (value != null && value.ToString() != "")
                costSharingPayable = double.Parse(value.ToString());
            else
                costSharingPayable = 0d;
            if (costSharingPayable == 0d)
            {
                this.ErrorText = "Cost sharing payable amount cannot be empty or zero";
                return false;
            }
            else
            {
                bool validated = ValidateCostSharingPayable(grossSalary, costSharingPayable, out message);
                if (!validated)
                    this.ErrorText = message;
                return validated;
            }
        }
        public static bool ValidateCostSharingPayable(double grossSalary, double payableAmount, out string message)
        {
            const double leastRate = 0.1;
            const double maxRate = (1 / 3);
            message = "";
            if (Account.AmountLess(payableAmount, grossSalary * leastRate)) //If amount is less than 10% of the gross salary
            {
                message = "The cost sharing payable amount cannot be less than 10% of the gross salary";
                return false;
            }
            else if (Account.AmountGreater(payableAmount, grossSalary * 1 / 3))// If amount is greater than 1/3rd of the gross salary
            {
                message = "The cost sharing payable amount cannot exceed 1/3 of the gross salary";
                return false;
            }
            else
            {
                return true;
            }
        }
        public static double CalculateLeastCostSharingPayable(double grossSalary)
        {
            const double leastRate = 0.1; //10%
            return Math.Round((grossSalary * leastRate), 2);
        }
        public static double CalculateMaximumCostSharingPayable(double grossSalary)
        {
            return (Math.Round(grossSalary * 1 / 3, 2));
        }
    }
    public class TaxExemptionValidationRule : ValidationRule
    {
        double totalDeclarationAmount;
        public TaxExemptionValidationRule(double totalAmount)
        {
            totalDeclarationAmount = totalAmount;
        }
        public override bool Validate(Control control, object value)
        {
           
            double exemptionAmount;
            if (value != null && value.ToString() != "")
                exemptionAmount = double.Parse(value.ToString());
            else
                exemptionAmount = 0d;
            if (exemptionAmount != 0)
            {
                if (totalDeclarationAmount - exemptionAmount < 0)
                    return false;
            }
            return true;
        }
    }

    public static class CurrencyTranslator
    {
        // Array of sting to hold the words from one to nineteen 
        static private string[] _arrayOfOnes = { string.Empty, "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", 
        "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", 
        "Nineteen" };
        // Array of string to hold the words of tens - 10, 20,.., 90
        static private string[] _arrayOfTens = { string.Empty , "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", 
        "Seventy", "Eighty", "Ninety"};

        // Converts the decimal to currency
        public static string TranslateCurrency(decimal currencyValue)
        {
            decimal sign = currencyValue < 0 ? -1 : 1;
            currencyValue = sign*currencyValue;

            string numericCurrency = currencyValue.ToString().Trim();
            //Check for the max capacity limit of the input 
            if (numericCurrency.Length > 18)
                return "Invalid input format";

            string leftValue, decimalWord;
            //Right align the charecters with padding of "0" to length the of 18 charecters
            if (numericCurrency.IndexOf(".") >= 0)
            {
                leftValue = numericCurrency.Substring(0, numericCurrency.IndexOf(".")).PadLeft(18, Convert.ToChar("0"));
                decimalWord = numericCurrency.Substring(numericCurrency.IndexOf(".") + 1).PadRight(2, Convert.ToChar("0"));
                decimalWord = (decimalWord.Length > 2 ? decimalWord.Substring(0, 2) : decimalWord);
            }
            else
            {
                leftValue = numericCurrency.PadLeft(18, Convert.ToChar("0"));
                decimalWord = "0";
            }

            string quadrillionWord, trillionWord, billionWord, millionWord, thousandWord, hundredWord;
            quadrillionWord = TranslateHundreds(Convert.ToInt32(leftValue.Substring(0, 3))); // One Quadrillion - Number of zeros 15
            trillionWord = TranslateHundreds(Convert.ToInt32(leftValue.Substring(3, 3)));    // One Trillion - Number of zeros 12
            billionWord = TranslateHundreds(Convert.ToInt32(leftValue.Substring(6, 3)));     // One Billion - Number of zeros 9
            millionWord = TranslateHundreds(Convert.ToInt32(leftValue.Substring(9, 3)));    // One Million - Number of zeros 6
            thousandWord = TranslateHundreds(Convert.ToInt32(leftValue.Substring(12, 3)));
            hundredWord = TranslateHundreds(Convert.ToInt32(leftValue.Substring(15, 3)));
            decimalWord = TranslateDecimal(decimalWord);

            // Start building the currency
            StringBuilder currencyInEnglish = new StringBuilder();
            currencyInEnglish.Append((quadrillionWord.Trim() == string.Empty ? string.Empty : quadrillionWord + " Quadrillion "));
            currencyInEnglish.Append((trillionWord.Trim() == string.Empty ? string.Empty : trillionWord + " Trillion "));
            currencyInEnglish.Append((billionWord.Trim() == string.Empty ? string.Empty : billionWord + " Billion "));
            currencyInEnglish.Append((millionWord.Trim() == string.Empty ? string.Empty : millionWord + " Million "));
            currencyInEnglish.Append((thousandWord.Trim() == string.Empty ? string.Empty : thousandWord + " Thousand "));
            currencyInEnglish.Append((hundredWord.Trim() == string.Empty ? string.Empty : hundredWord));

            currencyInEnglish.Append(currencyInEnglish.ToString() == string.Empty ? "Zero Birr " : " Birr");
            if (currencyInEnglish.ToString().StartsWith("One Birrs"))
            {
                currencyInEnglish.Replace("One Birrs", "One Birr");
            }
            currencyInEnglish.Append((decimalWord == string.Empty ? " and Zero Cents" : " and " + decimalWord + " Cents"));
            return (sign==-1?"Negative ":"") + currencyInEnglish.ToString();
        }


        #region Private Functions


        static private string TranslateHundreds(int value)
        {
            string result;
            // If the value is less than hundred then convert it as tens 
            if (value <= 99)
            {
                result = (TranslateTens(value));
            }
            else
            {
                result = _arrayOfOnes[Convert.ToInt32(Math.Floor(Convert.ToDecimal(value / 100)))];
                // Math.Floor method is used to get the largest integer from the decial value
                result += " Hundred ";
                // The rest of the decimal is converted into tens
                if (value - Math.Floor(Convert.ToDecimal((value / 100) * 100)) == 0)
                {
                    result += string.Empty;
                }
                else
                {
                    result += string.Empty + TranslateTens(Convert.ToInt32(value - Math.Floor(Convert.ToDecimal((value / 100) * 100))));
                }
            }
            return result;
        }


        static private string TranslateTens(int value)
        {
            string result;
            // If the value is less than 20 then get the word directly from the array
            if (value < 20)
            {
                result = _arrayOfOnes[value];
                value = 0;
            }
            else
            {
                result = _arrayOfTens[(int)Math.Floor(Convert.ToDecimal(value / 10))];
                value -= Convert.ToInt32(Math.Floor(Convert.ToDecimal((value / 10) * 10)));
            }
            result = result + (_arrayOfOnes[value].Trim() == string.Empty ? string.Empty : "-" + _arrayOfOnes[value]);
            return result;
        }

        // Translates the decimal part of the currency to words
        static private string TranslateDecimal(string value)
        {
            string result = string.Empty;
            // Check whether the decimal part starts with a zero. Example : 12.05
            if (value != "0")
            {
                if (value.StartsWith("0"))
                {
                    result = "Zero ";
                    result += _arrayOfOnes[Convert.ToInt32(value.Substring(1, 1))];
                }
                else
                {
                    result = TranslateTens(Convert.ToInt32(value));
                }

            }
            return result;
        }
        #endregion
    }

    public class CodeFormatter
    {
         public static string FormatAttachmentNumber(int attachmentNumber)
        {
            object format = iERPTransactionClient.GetSystemParamter("AttachmentNumberFormat");
            if (format == null)
            {
                throw new Exception ("Cannot get formatting value to format attachment number. Please configure Attachement Number Format in settings and try again!");
            }
            return attachmentNumber.ToString((string)format);
        }
    }

    public class XRUtility
    {
        public static XRTableCell CreateXRTableCell(string caption, string name, float width)
        {
            XRTableCell cell = new XRTableCell();

            cell.Text = caption;
            cell.Name = name;
            cell.WidthF = width;
            return cell;

        }
        public static XRTableCell CreateXRTableCell(string caption, string name, float width, TextAlignment alignment)
        {
            XRTableCell cell = new XRTableCell();

            cell.Text = caption;
            cell.Name = name;
            cell.WidthF = width;
            cell.TextAlignment = alignment;
            return cell;

        }
        public static XRTableCell CreateXRTableCell(string caption, string name, float width, Font font)
        {
            XRTableCell cell = new XRTableCell();

            cell.Text = caption;
            cell.Name = name;
            cell.WidthF = width;
            cell.Font = font;
            return cell;

        }
        public static XRTableCell CreateXRTableCell(string caption, string name, float width, Font font, TextAlignment alignment)
        {
            XRTableCell cell = new XRTableCell();

            cell.Text = caption;
            cell.Name = name;
            cell.WidthF = width;
            cell.Font = font;
            cell.TextAlignment = alignment;
            return cell;

        }
    }

    public class TransactionValidator
    {
        public static bool ValidateNegativeTransactionPost(int assetAccountID, double amount,PaymentTypeSelector paymentTypeSelector
            , TextEdit textServiceCharge, ServiceChargePayer serviceChargePayer, DateTime docDate)
        {
            double payAmount = amount;
            bool post = true;
            double assetBalance = AccountingClient.GetNetBalanceAsOf(assetAccountID, docDate);
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(paymentTypeSelector.PaymentMethod) && textServiceCharge.Text != "")
            {
                if (serviceChargePayer == ServiceChargePayer.Company)
                    payAmount = amount + double.Parse(textServiceCharge.Text);
            }
            if (Account.AmountGreater(payAmount, assetBalance))
            {
                if (MessageBox.ShowWarningMessage("There is no sufficient money in the selected payment account. As a result, posting will cause negative balance. Are you sure you want to continue?") == DialogResult.Yes)
                    post = true;
                else
                    post = false;
            }
            else
                post = true;
            return post;
        }
    }
    public class Constants
    {
        public const int LONGTERMLOAN_WITHSERVICECHARGE_DOCUMENTTYPEID = 178;
    }
}
