using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.ClientServer;
namespace BIZNET.iERP.Client
{
    public class ActivationParameter
    {
        public BizNetPaymentMethod paymentMethod = BizNetPaymentMethod.None;
        public int employeeID = -1;
        public string objectCode = null;
        public BondPaymentDocument bondPaymentDocument;
        public int assetAccountID = -1;
        public int storeID = -1;
        public Purchase2Document purchaseDocument;
        public bool fixPaymentMethod;
        public bool fixAssetAccountID;
        public Sell2Document sellDocument;
        public AccountingPeriod CurrentTaxDeclarationPeriod;
        public int CurrentTaxDeclarationID;
        public double CurrentDeclaredAmount;
        public double CurrentPeriodCredit;
        public double CarryCreditForward;
        public double accumulatedCredit;
        public int CurrentMainAccountID;
        public int periodID;
        public AccountDocument parentAccountDocument;
        public int referedDocumentID=-1;
        public bool disableReference = false;
        public bool disableDate = false;
        public double fixedAmount = -1;
        public bool fixAmount = false;
        public bool saveWithNoPayment=false;
    }
}
