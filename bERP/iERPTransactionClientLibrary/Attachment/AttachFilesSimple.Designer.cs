﻿namespace BIZNET.iERP.Client
{
    partial class AttachFilesSimple
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.comboDocumentType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridImages = new System.Windows.Forms.DataGridView();
            this.colImageFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFace = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.buttonAddFiles = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textLocationCode = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textReference = new System.Windows.Forms.TextBox();
            this.textDescription = new System.Windows.Forms.TextBox();
            this.buttonClearFiles = new System.Windows.Forms.Button();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.documentDate = new INTAPS.Ethiopic.DualCalendar();
            this.panelDetail = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridImages)).BeginInit();
            this.panelHeader.SuspendLayout();
            this.panelDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(651, 246);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 24);
            this.buttonCancel.TabIndex = 0;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(570, 247);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 1;
            this.buttonOk.Text = "Ok";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(323, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Document Type";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // comboDocumentType
            // 
            this.comboDocumentType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboDocumentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDocumentType.FormattingEnabled = true;
            this.comboDocumentType.Location = new System.Drawing.Point(427, 26);
            this.comboDocumentType.Name = "comboDocumentType";
            this.comboDocumentType.Size = new System.Drawing.Size(240, 21);
            this.comboDocumentType.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Images";
            // 
            // dataGridImages
            // 
            this.dataGridImages.AllowUserToAddRows = false;
            this.dataGridImages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridImages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridImages.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colImageFile,
            this.colFace});
            this.dataGridImages.Location = new System.Drawing.Point(12, 42);
            this.dataGridImages.Name = "dataGridImages";
            this.dataGridImages.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridImages.Size = new System.Drawing.Size(714, 198);
            this.dataGridImages.TabIndex = 5;
            // 
            // colImageFile
            // 
            this.colImageFile.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.colImageFile.HeaderText = "File Name";
            this.colImageFile.Name = "colImageFile";
            this.colImageFile.ReadOnly = true;
            this.colImageFile.Width = 73;
            // 
            // colFace
            // 
            this.colFace.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.colFace.FillWeight = 30F;
            this.colFace.HeaderText = "Document Face";
            this.colFace.Items.AddRange(new object[] {
            "Front Side",
            "Back Side of Previous Image",
            "Back Side with No Front Side"});
            this.colFace.Name = "colFace";
            this.colFace.Width = 80;
            // 
            // buttonAddFiles
            // 
            this.buttonAddFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddFiles.Location = new System.Drawing.Point(651, 10);
            this.buttonAddFiles.Name = "buttonAddFiles";
            this.buttonAddFiles.Size = new System.Drawing.Size(75, 23);
            this.buttonAddFiles.TabIndex = 6;
            this.buttonAddFiles.Text = "Add Files";
            this.buttonAddFiles.UseVisualStyleBackColor = true;
            this.buttonAddFiles.Click += new System.EventHandler(this.buttonAddFiles_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Location:";
            this.label3.Click += new System.EventHandler(this.label1_Click);
            // 
            // textLocationCode
            // 
            this.textLocationCode.Location = new System.Drawing.Point(109, 76);
            this.textLocationCode.Name = "textLocationCode";
            this.textLocationCode.Size = new System.Drawing.Size(558, 20);
            this.textLocationCode.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(323, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Reference:";
            this.label5.Click += new System.EventHandler(this.label1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Description:";
            this.label6.Click += new System.EventHandler(this.label1_Click);
            // 
            // textReference
            // 
            this.textReference.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textReference.Location = new System.Drawing.Point(427, 4);
            this.textReference.Name = "textReference";
            this.textReference.Size = new System.Drawing.Size(241, 20);
            this.textReference.TabIndex = 7;
            // 
            // textDescription
            // 
            this.textDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textDescription.Location = new System.Drawing.Point(109, 102);
            this.textDescription.Multiline = true;
            this.textDescription.Name = "textDescription";
            this.textDescription.Size = new System.Drawing.Size(617, 153);
            this.textDescription.TabIndex = 7;
            // 
            // buttonClearFiles
            // 
            this.buttonClearFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClearFiles.Location = new System.Drawing.Point(570, 10);
            this.buttonClearFiles.Name = "buttonClearFiles";
            this.buttonClearFiles.Size = new System.Drawing.Size(75, 23);
            this.buttonClearFiles.TabIndex = 6;
            this.buttonClearFiles.Text = "Clear Files";
            this.buttonClearFiles.UseVisualStyleBackColor = true;
            this.buttonClearFiles.Click += new System.EventHandler(this.buttonClearFiles_Click);
            // 
            // panelHeader
            // 
            this.panelHeader.Controls.Add(this.documentDate);
            this.panelHeader.Controls.Add(this.label1);
            this.panelHeader.Controls.Add(this.textDescription);
            this.panelHeader.Controls.Add(this.label3);
            this.panelHeader.Controls.Add(this.textReference);
            this.panelHeader.Controls.Add(this.comboDocumentType);
            this.panelHeader.Controls.Add(this.label5);
            this.panelHeader.Controls.Add(this.textLocationCode);
            this.panelHeader.Controls.Add(this.label6);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(738, 270);
            this.panelHeader.TabIndex = 8;
            this.panelHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.panelHeader_Paint);
            // 
            // documentDate
            // 
            this.documentDate.Location = new System.Drawing.Point(12, 7);
            this.documentDate.Name = "documentDate";
            this.documentDate.ShowEthiopian = true;
            this.documentDate.ShowGregorian = true;
            this.documentDate.ShowTime = false;
            this.documentDate.Size = new System.Drawing.Size(293, 40);
            this.documentDate.TabIndex = 8;
            this.documentDate.VerticalLayout = true;
            // 
            // panelDetail
            // 
            this.panelDetail.Controls.Add(this.label2);
            this.panelDetail.Controls.Add(this.buttonCancel);
            this.panelDetail.Controls.Add(this.buttonClearFiles);
            this.panelDetail.Controls.Add(this.buttonOk);
            this.panelDetail.Controls.Add(this.buttonAddFiles);
            this.panelDetail.Controls.Add(this.dataGridImages);
            this.panelDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDetail.Location = new System.Drawing.Point(0, 270);
            this.panelDetail.Name = "panelDetail";
            this.panelDetail.Size = new System.Drawing.Size(738, 281);
            this.panelDetail.TabIndex = 9;
            // 
            // AttachFilesSimple
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(738, 551);
            this.Controls.Add(this.panelDetail);
            this.Controls.Add(this.panelHeader);
            this.Name = "AttachFilesSimple";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Attach Files";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridImages)).EndInit();
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            this.panelDetail.ResumeLayout(false);
            this.panelDetail.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboDocumentType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridImages;
        private System.Windows.Forms.Button buttonAddFiles;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textLocationCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textReference;
        private System.Windows.Forms.TextBox textDescription;
        private System.Windows.Forms.Button buttonClearFiles;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Panel panelDetail;
        private INTAPS.Ethiopic.DualCalendar documentDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colImageFile;
        private System.Windows.Forms.DataGridViewComboBoxColumn colFace;
    }
}