﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using INTAPS;
namespace BIZNET.iERP.Client
{
    public partial class AttachFilesSimple : Form
    {
        public class ImageInfo
        {
            public string fileName;
            public Image img;
        }
        public AttachedFileGroup group = null;
        public List<AttachedFileItem> items=null;
        public Dictionary<AttachedFileItemImage, ImageInfo> imageInfos = new Dictionary<AttachedFileItemImage, ImageInfo>();

        
        bool includeHeader;
        bool includeDetail;
        public AttachFilesSimple(bool includeHeader,bool includeDetail, DateTime defaultDate)
        {
            InitializeComponent();
            this.includeHeader = includeHeader;
            this.includeDetail = includeDetail;
            if (!includeHeader)
                panelHeader.Height = 0;
            if (!includeDetail)
                panelDetail.Height = 0;
            documentDate.DateTime = defaultDate;
            comboDocumentType.Items.AddRange(iERPTransactionClient.getAllFileAttachmentTypes());
            if (comboDocumentType.Items.Count > 0)
                comboDocumentType.SelectedIndex = 0;
        }
        protected override void OnLoad(EventArgs e)
        {
            addFiles();
        }
        void addFiles()
        {
            OpenFileDialog file = new OpenFileDialog();
            file.Multiselect = true;
            if (file.ShowDialog(this) == DialogResult.OK)
            {
                Image[] theseImages = new Image[file.FileNames.Length];
                int i = 0;
                foreach (string f in file.FileNames)
                {
                    try
                    {
                        theseImages[i] = System.Drawing.Image.FromFile(f);
                    }
                    catch (Exception ex)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError(
                            "Error loading image from file {0}\n{1}".format(f, ex.Message));
                        return;
                    }
                    i++;
                }

                i = 0;
                foreach (string f in file.FileNames)
                {
                    int index = dataGridImages.Rows.Add(f, "Front Side");
                    dataGridImages.Rows[index].Tag = theseImages[i];
                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(f);
                    if (includeHeader)
                    {
                        if (textLocationCode.Text.Trim().Equals(""))
                        {
                            textLocationCode.Text = fileInfo.Name;
                        }
                       
                        if (textReference.Text.Trim().Equals(""))
                        {
                            textReference.Text = fileInfo.Name;
                        }
                        if (textDescription.Text.Trim().Equals(""))
                        {
                            textDescription.Text = fileInfo.Name;
                        }
                    }
                    i++;
                }
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if(dataGridImages.Rows.Count==0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select at least one image");
                return;
            }
            if (includeDetail)
            {
                List<AttachedFileItem> items = new List<AttachedFileItem>();
                imageInfos = new Dictionary<AttachedFileItemImage, ImageInfo>();
                AttachedFileItem prev = null;
                foreach (DataGridViewRow row in dataGridImages.Rows)
                {
                    String fileName = row.Cells[0].Value as string;
                    System.IO.FileInfo fi = new System.IO.FileInfo(fileName);
                    AttachedFileItemImage img = new AttachedFileItemImage();
                    img.fileName = fi.Name;
                    img.fileImage = System.IO.File.ReadAllBytes(fileName);
                    imageInfos.Add(img, new ImageInfo() { fileName=fileName,img=(Image)row.Tag});
                    bool addToPrev = false;
                    switch (row.Cells[1].Value.ToString())
                    {
                        case "Front Side":
                            img.face = AttachmentPaperFace.Paper_Front;
                            break;
                        case "Back Side of Previous Image":
                            img.face = AttachmentPaperFace.Paper_Back;
                            addToPrev = true;
                            break;
                        case "Back Side with No Front Side":
                            img.face = AttachmentPaperFace.Paper_Back;
                            break;
                    }
                    if (addToPrev)
                    {
                        if (prev == null)
                        {
                            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("There is no front page for " + fileName);
                            return;
                        }
                        prev.images = INTAPS.ArrayExtension.appendToArray(prev.images, img);
                        prev = null;
                    }
                    else
                    {
                        AttachedFileItem fileItem = new AttachedFileItem();
                        fileItem.images = new AttachedFileItemImage[] { img };
                        items.Add(fileItem);
                        if (img.face == AttachmentPaperFace.Paper_Front)
                            prev = fileItem;
                        else
                            prev = null;
                    }
                }
                this.items = items;
            }
            if (includeHeader)
            {
                AttachedFileGroup group = new AttachedFileGroup();
                group.attachmentTypeID = ((AttachmentType)comboDocumentType.SelectedItem).id;
                group.locationID = textLocationCode.Text.Trim();
                group.locationText = group.locationID;
                group.paperRef = textReference.Text.Trim();
                group.title = textDescription.Text.Trim();
                group.documentDate = documentDate.DateTime;
                this.group = group;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void buttonAddFiles_Click(object sender, EventArgs e)
        {
            addFiles();
        }

        private void buttonClearFiles_Click(object sender, EventArgs e)
        {
            dataGridImages.Rows.Clear();
        }

        private void panelHeader_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
