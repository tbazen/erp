﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERP.Client
{
    public partial class PromptAddAttachment : Form
    {
        int documentID;
        IWin32Window owner;
        public PromptAddAttachment(int documentID)
        {
            InitializeComponent();
            this.documentID=documentID;
        }
        public static DialogResult  promptAddAttachment(int documentID,IWin32Window owner)
        {
            PromptAddAttachment p=new PromptAddAttachment(documentID);
            p.owner=owner;
            return p.ShowDialog(owner);
        }

        private void buttonYes_Click(object sender, EventArgs e)
        {
            VisualBrowser vb = new VisualBrowser(documentID,-1, VisualBrowser.VisualBrowserMode.Create);
            this.DialogResult=vb.ShowDialog(owner);
            this.Close();
        }
        private void buttonNo_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        } 
    }
}
