namespace BIZNET.iERP.Client
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Windows.Forms;
    internal enum LabelDrawMode
    {
        None,
        Body,
        Bottom
    }
    internal delegate void InvalidateDelegate();
    internal class ThumbPanel : Panel
    {
        AttachmentPaperFace face;
        int fileItemID;
        
        private static Font infoTextFont = new Font("Arial", 10f);
        private Brush m_HighlightBrush;
        private bool m_ImageLoaded = false;
        private System.Drawing.Image m_Img;
        private Thread m_LoadImageThread;
        private LabelDrawMode m_Mode;
        private bool m_Selected = false;
        private Pen m_SelectionPen;
        private const int THUMB_HEIGHT = 100;
        public void setByImage(Image img)
        {
            this.m_Img = img;
            this.m_ImageLoaded = true;
            this.Invalidate();
        }
        public void setFace(int fileItemID,AttachmentPaperFace face)
        {
            this.face = face;
            this.fileItemID = fileItemID;
            this.m_Img = null;
            this.m_ImageLoaded = false;
            StartLoadImage();
        }
        public ThumbPanel()
        {
            base.Height = 100;
            this.m_SelectionPen = new Pen(Color.Blue, 4f);
            this.m_HighlightBrush = new SolidBrush(Color.FromArgb(0x80, 0, 0, 0xff));
        }

        private void EditorEnter(object sender, EventArgs e)
        {
            this.OnClick(e);
        }

        private void EditorKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Message msg = new Message();
                this.ProcessCmdKey(ref msg, Keys.Down);
            }
        }

        private void LoadImage()
        {
            try
            {
                MemoryStream stream = new MemoryStream(iERPTransactionClient.getAttachedFileItemImageResized(this.fileItemID,this.face, base.Width, base.Height));
                System.Drawing.Image image = System.Drawing.Image.FromStream(stream);
                this.m_Img = image;
                stream.Dispose();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                this.m_Img = null;
            }
            this.m_ImageLoaded = true;
            base.Invoke(new InvalidateDelegate(this.Invalidate));
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (this.m_Img == null)
            {
                e.Graphics.FillRectangle(Brushes.White, 0, 0, base.Width, base.Height);
                if (!this.m_ImageLoaded)
                {
                    e.Graphics.DrawString("Loading..", infoTextFont, Brushes.Green, new PointF(0f, (float) ((base.Height / 2) - 10)));
                }
                else
                {
                    e.Graphics.DrawLine(Pens.Red, 0, 0, base.Width, base.Height);
                    e.Graphics.DrawLine(Pens.Red, base.Width, 0, 0, base.Height);
                }
            }
            else
            {
                int w, h;
                int x, y;
                if (this.m_Img.Width * base.Height > base.Width * this.m_Img.Height)
                {
                    h = m_Img.Height * base.Width / m_Img.Width;
                    y = (base.Height - h) / 2;
                    x = 0;
                    w = base.Width;
                }
                else
                {
                    w = m_Img.Width * base.Height / m_Img.Height;
                    x = (base.Width - w) / 2;
                    y = 0;
                    h = base.Height;
                }
                e.Graphics.DrawImage(this.m_Img, x, y, w, h);
            }
            if (this.m_Selected)
            {
                e.Graphics.DrawRectangle(this.m_SelectionPen, 0, 0, base.Width, base.Height);
                e.Graphics.FillRectangle(this.m_HighlightBrush, 0, 0, base.Width, base.Height);
            }
            else
            {
                switch (this.m_Mode)
                {
                    case LabelDrawMode.Body:
                        e.Graphics.DrawLine(this.m_SelectionPen, 0, 0, 0, base.Height);
                        e.Graphics.DrawLine(this.m_SelectionPen, base.Width, 0, base.Width, base.Height);
                        break;

                    case LabelDrawMode.Bottom:
                        e.Graphics.DrawLine(this.m_SelectionPen, 0, 0, 0, base.Height);
                        e.Graphics.DrawLine(this.m_SelectionPen, base.Width, 0, base.Width, base.Height);
                        e.Graphics.DrawLine(this.m_SelectionPen, 0, base.Height, base.Width, base.Height);
                        break;
                }
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            e.Graphics.FillRectangle(Brushes.Black, new Rectangle(0, 0, base.Width, base.Height));
        }
        private void StartLoadImage()
        {
            this.m_LoadImageThread = new Thread(new ThreadStart(this.LoadImage));
            this.m_LoadImageThread.Start();
        }

        public System.Drawing.Image Image
        {
            get
            {
                return this.m_Img;
            }
            set
            {
                this.m_Img = value;
            }
        }

        public bool IsSelected
        {
            get
            {
                return this.m_Selected;
            }
            set
            {
                this.m_Selected = value;
                base.Invalidate();
            }
        }

        internal LabelDrawMode Mode
        {
            get
            {
                return this.m_Mode;
            }
            set
            {
                this.m_Mode = value;
                base.Invalidate();
            }
        }

        
    }
}

