namespace BIZNET.iERP.Client
{
    using INTAPS.Archive.UI;
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;
    using INTAPS.Accounting;
    using INTAPS.Accounting.Client;
    using System.Collections.Generic;
    public partial class VisualBrowser : Form,IImagePanelHost,INTAPS.Archive.UI.IImageBrowser
    {
        const int THUMB_LIST_PADDING = 2;
        const int GROUP_PADDING = 5;
        const int GROUP_SPACING = 6;
        const int ITEM_PADDING = 5;
        const int ITEM_SPACING = 4;
        const int FACE_SPACING = 2;
        public enum VisualBrowserMode
        {
            View,
            Edit,
            Create
        }
        VisualBrowserMode mode;
        class ImageData
        {
            public AttachedFileGroup group;
            public AttachedFileItem item;
            public AttachedFileItemImage imgInfo;
            public Image image;
            public string file;
            public ThumbPanel panel;
        }
        private IContainer components = null;
        private const int LABEL_HEIGHT = 30;
        private AccountDocument m_document;
        int m_groupID;
        private Label m_SelectedLabel = null;
        private Color m_SelectedLabelColor;
        private ThumbPanel m_SelectedPanel = null;
        private Color m_UnselectedLabelColor;
        List<ImageData> m_images = new List<ImageData>();
        public VisualBrowser(int documentID,int groupID,VisualBrowserMode mode)
        {
            this.m_groupID = groupID;
            this.InitializeComponent();
            this.mode = mode;
            this.m_document = AccountingClient.GetAccountDocument(documentID,false);
            this.m_SelectedLabelColor = Color.Blue;
            this.m_UnselectedLabelColor = Color.LightGray;
            btnFirst.Visible = btnDown.Visible = btnUp.Visible = btnLast.Visible = mode != VisualBrowserMode.View;
            buttonAdd.Visible = buttonSave.Visible = mode == VisualBrowserMode.Create;
        }
        void addDocumentGroup()
        {
            lock (m_images)
            {
                AttachFilesSimple attach = new AttachFilesSimple(true, true, m_document.DocumentDate);
                if (attach.ShowDialog(this) == DialogResult.OK)
                {

                    Panel groupPanel = createGroupPanel(attach.group);

                    foreach (AttachedFileItem item in attach.items)
                    {
                        Panel itemPanel = createItemPanel(groupPanel, item);
                        foreach (AttachedFileItemImage img in item.images)
                        {
                            ImageData data = new ImageData();
                            data.group = attach.group;
                            data.item = item;
                            data.imgInfo = img;
                            data.file = attach.imageInfos[img].fileName;
                            data.image = attach.imageInfos[img].img;
                            (data.panel=addTumbPanel(item, itemPanel, data)).setByImage(data.image);
                            
                            m_images.Add(data);
                        }
                    }
                    layoutThumbnailPanel();
                }
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            if(mode==VisualBrowserMode.Create)
            {
                addDocumentGroup();
            }
            else
                this.PopulateImages();
        }
        private void ArrangeAdvanced_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (mode==VisualBrowserMode.Create && this.pnlImages.Controls.Count>0)
            {
                e.Cancel = !INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to close without saving changes?");
            }
        }


        private void btnDown_Click(object sender, EventArgs e)
        {
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                
            }
            catch(Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
        private void btnUp_Click(object sender, EventArgs e)
        {
            
        }

        private void CreateImagePanel()
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void l_Click(object sender, EventArgs e)
        {
        }

        private void MoveDocumentDown()
        {
        }

        private void MoveDocumentFirst()
        {
        }

        private void MoveDocumentLast()
        {
        }

        private void MoveDocumentUp(Label MoveLabel)
        {
        }

        private void MoveImageDown()
        {
        }

        private void MoveImageFirst()
        {
        }

        private void MoveImageLast()
        {
        }

        private void MoveImageUp()
        {
        }

        private void MoveStepDown()
        {
        }

        private void MoveStepUp()
        {
        }

        private void MoveToFirst()
        {
        }

        private void MoveToLast()
        {
        }


        ThumbPanel selectedPanel = null;
        
        private void pan_Click(object sender, EventArgs e)
        {
            ThumbPanel panel = (ThumbPanel)sender;
            ImageData data = (ImageData)panel.Tag;
            viewerIndex = m_images.IndexOf(data);
            imageViewer1.ShowImage();
            selectThumbnail(panel);
        }

        private void selectThumbnail(ThumbPanel panel)
        {
            
            if (selectedPanel != panel)
            {
                if (selectedPanel != null)
                    selectedPanel.Parent.BackColor = m_UnselectedLabelColor;
                selectedPanel = panel;
                if (selectedPanel != null) 
                    selectedPanel.Parent.BackColor = m_SelectedLabelColor;
            }
            if (selectedPanel != null)
            {
                pnlImages.ScrollControlIntoView(selectedPanel);
            }
        }
        
        void layoutThumbnailPanel()
        {
            int grp_y=THUMB_LIST_PADDING;
            foreach(Panel grp in pnlImages.Controls)
            {
                grp.Width = pnlImages.Width - 2*THUMB_LIST_PADDING;
                grp.Left = THUMB_LIST_PADDING;
                grp.Top = grp_y;
                
                Panel lbl = (Panel)grp.Controls[0];
                lbl.Width = grp.Width - 2 * GROUP_PADDING;
                lbl.Top = GROUP_PADDING;
                lbl.Left=GROUP_PADDING;
                lbl.Controls[0].Visible = mode != VisualBrowserMode.View;
                int item_y = lbl.Bottom;
                for (int i = 1; i < grp.Controls.Count;i++)
                {
                    Panel item = (Panel)grp.Controls[i];
                    item.Width = lbl.Width;
                    item.Left = lbl.Left;
                    item.Top = item_y;
                    int img_y = ITEM_PADDING;
                    foreach (ThumbPanel pnl in item.Controls)
                    {
                        pnl.Width = item.Width - 2 * ITEM_PADDING;
                        pnl.Left = ITEM_PADDING;
                        pnl.Top = img_y;
                        pnl.Controls[0].Visible = mode != VisualBrowserMode.View;
                        img_y += pnl.Height + ITEM_PADDING;
                    }
                    item.Height = img_y;
                    item_y += item.Height + ITEM_SPACING;
                }
                grp.Height = item_y;
                grp_y += grp.Height + GROUP_SPACING;
            }
        }
        private void PopulateImages()
        {
            lock (m_images)
            {
                this.m_images.Clear();
                this.pnlImages.Controls.Clear();

                AttachedFileGroup[] groups;
                if (m_groupID == -1)
                    groups = iERPTransactionClient.getAttachedFileGroupForDocument(m_document.AccountDocumentID);
                else
                    groups = new AttachedFileGroup[] { iERPTransactionClient.getAttachedFileGroup(m_groupID) };
                foreach (AttachedFileGroup group in groups)
                {
                    Panel groupPanel = createGroupPanel(group);
                    AttachedFileItem[] items = iERPTransactionClient.getAttachedFileItems(group.id);
                    foreach (AttachedFileItem item in items)
                    {
                        Panel itemPanel = createItemPanel(groupPanel, item);
                        foreach (AttachedFileItemImage img in item.images)
                        {
                            ImageData data = new ImageData();
                            data.group = group;
                            data.item = item;
                            data.imgInfo = img;
                            data.file = null;
                            (data.panel=addTumbPanel(item, itemPanel, data)).setFace(item.id, img.face);
                            m_images.Add(data);
                        }
                    }
                }
                layoutThumbnailPanel();
                viewerIndex = m_images.Count > 0 ? 0 : -1;
                imageViewer1.SetSource(this);
            }
        }

        private ThumbPanel addTumbPanel(AttachedFileItem item, Panel itemPanel, ImageData img)
        {
            ThumbPanel panel = new ThumbPanel();
            panel.Height = 100;
            itemPanel.Controls.Add(panel);
            panel.Click += new EventHandler(this.pan_Click);

            Button deleteButton = new Button();
            deleteButton.Text = "X";
            deleteButton.ForeColor = Color.Red;
            deleteButton.Width = 20;
            deleteButton.Height = 20;
            deleteButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            deleteButton.Top = 0;
            deleteButton.Left = panel.Width - deleteButton.Width;
            panel.Controls.Add(deleteButton);
            panel.Tag = img;
            return panel;
        }

        private static Panel createItemPanel(Panel groupPanel, AttachedFileItem item)
        {
            Panel itemPanel = new Panel();
            itemPanel.BorderStyle = BorderStyle.FixedSingle;
            groupPanel.Controls.Add(itemPanel);
            itemPanel.Tag = item;
            return itemPanel;
        }

        private Panel createGroupPanel(AttachedFileGroup group)
        {
            Panel groupPanel = new Panel();
            this.pnlImages.Controls.Add(groupPanel);
            
            Label label = new Label();
            
            
            
            label.Text = group.title;
            label.AutoSize = false;
            label.Font = new Font("Ethiopia Jiret", 10f);
            label.BackColor = this.m_UnselectedLabelColor;
            label.Click += new EventHandler(this.l_Click);
            groupPanel.Tag = group;

            Button deleteButton = new Button();
            deleteButton.Text = "X";
            deleteButton.ForeColor = Color.Red;
            deleteButton.Width = 20;
            deleteButton.Height = 20;

            Panel titlePanel = new Panel();
            titlePanel.Height = 30;
            label.Dock = DockStyle.Fill;
            deleteButton.Dock = DockStyle.Right;
            titlePanel.Controls.Add(deleteButton);
            titlePanel.Controls.Add(label);

            groupPanel.Controls.Add(titlePanel);

            return groupPanel;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            int index;
            switch (keyData)
            {
                case Keys.Left:
                case Keys.Up:
                    if (this.m_SelectedPanel == null)
                    {
                        if (this.m_SelectedLabel != null)
                        {
                            index = this.pnlImages.Controls.IndexOf(this.m_SelectedLabel);
                        }
                        else
                        {
                            index = -1;
                        }
                    }
                    else
                    {
                        index = this.pnlImages.Controls.IndexOf(this.m_SelectedPanel);
                    }
                    if (index > 1)
                    {
                        index--;
                        while ((index >= 0) && !(this.pnlImages.Controls[index] is ThumbPanel))
                        {
                            index--;
                        }
                        if (index >= 0)
                        {
                            this.pan_Click(this.pnlImages.Controls[index], null);
                        }
                    }
                    return true;

                case Keys.Right:
                case Keys.Down:
                    if (this.m_SelectedPanel == null)
                    {
                        if (this.m_SelectedLabel != null)
                        {
                            index = this.pnlImages.Controls.IndexOf(this.m_SelectedLabel);
                        }
                        else
                        {
                            index = -1;
                        }
                        break;
                    }
                    index = this.pnlImages.Controls.IndexOf(this.m_SelectedPanel);
                    break;

                default:
                    return base.ProcessCmdKey(ref msg, keyData);
            }
            if (index < (this.pnlImages.Controls.Count - 1))
            {
                index++;
                while ((index < this.pnlImages.Controls.Count) && !(this.pnlImages.Controls[index] is ThumbPanel))
                {
                    index++;
                }
                if (index < this.pnlImages.Controls.Count)
                {
                    this.pan_Click(this.pnlImages.Controls[index], null);
                }
            }
            return true;
        }

        protected override bool ProcessKeyMessage(ref Message m)
        {
            return base.ProcessKeyMessage(ref m);
        }

        private void RemoveAt(int index)
        {
            
        }

        private void SelectLabel(Label l)
        {
            
        }

        private void UnselectLable(Label l)
        {
                    }

        public void DragToolChanged(DragTool tool)
        {
            
        }

        public void PanChanged(Point pan)
        {
            
        }

        public void ZoomLevelChanged(float Level)
        {
            
        }

        int viewerIndex = -1;
        public Image CurrentImage
        {
            get
            {
                if (viewerIndex < 0 || viewerIndex >= m_images.Count)
                    return null;
                System.Threading.Thread loadThread=null;
                ImageData ret;
                lock (m_images)
                {
                    if (m_images[viewerIndex].image == null)
                    {
                        loadThread = new System.Threading.Thread(delegate(object par)
                            {
                                int callIndex = (int)par;
                                ImageData data = null;
                                lock (m_images)
                                {
                                    if (m_images == null || callIndex >= m_images.Count)
                                        return;
                                    data = m_images[callIndex];
                                }
                                Image img = iERPTransactionClient.getAttachedFileItemImage(data.imgInfo.fileItemID, data.imgInfo.face);
                                lock (m_images)
                                {
                                    data.image = img;
                                }
                                if (viewerIndex == callIndex)
                                {
                                    this.Invoke(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                                        {
                                            imageViewer1.ShowImage();
                                        }));
                                }

                            }
                            );
                    }
                    ret= m_images[viewerIndex];
                }
                if (loadThread != null)
                    loadThread.Start(viewerIndex);
                selectThumbnail(ret.panel);
                
                return ret.image;
            }
        }

        public int CurrentPageNumber
        {
            get 
            {
                return viewerIndex+1;
            }
        }

        public string CurrentTitle
        {
            get {
                if (viewerIndex < 0 || viewerIndex >= m_images.Count)
                    return null;
                return m_images[viewerIndex].group.title;
            }
        }

        public bool IsCurrnetImageBack
        {
            get 
            {
                if (viewerIndex < 0 || viewerIndex >= m_images.Count)
                    return false;
                return m_images[viewerIndex].imgInfo.face==AttachmentPaperFace.Paper_Front;
            }
        }

        public void MoveNext()
        {
            if(viewerIndex<m_images.Count-1)
                viewerIndex++;
        }

        public void MovePrev()
        {
            if (viewerIndex > 0)
                viewerIndex--;
        }

        public int NPages
        {
            get 
            {
                return m_images.Count;
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            addDocumentGroup();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<AttachedFileGroup> groups = new List<AttachedFileGroup>();
                List<List<AttachedFileItem>> group_items = new List<List<AttachedFileItem>>();
                foreach (Panel grp in pnlImages.Controls)
                {
                    groups.Add((AttachedFileGroup)grp.Tag);
                    List<AttachedFileItem> items = new List<AttachedFileItem>();
                    for (int i = 1; i < grp.Controls.Count; i++)
                    {
                        items.Add((AttachedFileItem)grp.Controls[i].Tag);
                    }
                    group_items.Add(items);
                }
                iERPTransactionClient.attachMultipeFile(m_document.AccountDocumentID, groups, group_items);
                mode = VisualBrowserMode.Edit;
                this.DialogResult = DialogResult.OK;
            }
            catch(Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
    }
}

