﻿namespace BIZNET.iERP.Client
{
    partial class AttachmentBrowser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabStructure = new System.Windows.Forms.TabPage();
            this.tabSearch = new System.Windows.Forms.TabPage();
            this.tree = new System.Windows.Forms.TreeView();
            this.dateFrom = new INTAPS.Ethiopic.DualCalendar();
            this.checkFrom = new System.Windows.Forms.CheckBox();
            this.dateTo = new INTAPS.Ethiopic.DualCalendar();
            this.checkTo = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textReference = new System.Windows.Forms.TextBox();
            this.comboDocumentType = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonSearch = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabStructure.SuspendLayout();
            this.tabSearch.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.tabControl);
            this.splitContainer.Size = new System.Drawing.Size(717, 452);
            this.splitContainer.SplitterDistance = 358;
            this.splitContainer.TabIndex = 0;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabStructure);
            this.tabControl.Controls.Add(this.tabSearch);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(358, 452);
            this.tabControl.TabIndex = 0;
            // 
            // tabStructure
            // 
            this.tabStructure.Controls.Add(this.tree);
            this.tabStructure.Location = new System.Drawing.Point(4, 22);
            this.tabStructure.Name = "tabStructure";
            this.tabStructure.Padding = new System.Windows.Forms.Padding(3);
            this.tabStructure.Size = new System.Drawing.Size(350, 426);
            this.tabStructure.TabIndex = 0;
            this.tabStructure.Text = "Structure";
            this.tabStructure.UseVisualStyleBackColor = true;
            // 
            // tabSearch
            // 
            this.tabSearch.Controls.Add(this.listView1);
            this.tabSearch.Controls.Add(this.panel1);
            this.tabSearch.Location = new System.Drawing.Point(4, 22);
            this.tabSearch.Name = "tabSearch";
            this.tabSearch.Padding = new System.Windows.Forms.Padding(3);
            this.tabSearch.Size = new System.Drawing.Size(350, 426);
            this.tabSearch.TabIndex = 1;
            this.tabSearch.Text = "Search";
            this.tabSearch.UseVisualStyleBackColor = true;
            this.tabSearch.Click += new System.EventHandler(this.tabSearch_Click);
            // 
            // tree
            // 
            this.tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tree.Location = new System.Drawing.Point(3, 3);
            this.tree.Name = "tree";
            this.tree.Size = new System.Drawing.Size(344, 420);
            this.tree.TabIndex = 0;
            // 
            // dateFrom
            // 
            this.dateFrom.Location = new System.Drawing.Point(3, 21);
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.ShowEthiopian = true;
            this.dateFrom.ShowGregorian = true;
            this.dateFrom.ShowTime = false;
            this.dateFrom.Size = new System.Drawing.Size(302, 40);
            this.dateFrom.TabIndex = 9;
            this.dateFrom.VerticalLayout = true;
            // 
            // checkFrom
            // 
            this.checkFrom.AutoSize = true;
            this.checkFrom.Location = new System.Drawing.Point(3, 3);
            this.checkFrom.Name = "checkFrom";
            this.checkFrom.Size = new System.Drawing.Size(80, 17);
            this.checkFrom.TabIndex = 10;
            this.checkFrom.Text = "Dates From";
            this.checkFrom.UseVisualStyleBackColor = true;
            // 
            // dateTo
            // 
            this.dateTo.Location = new System.Drawing.Point(3, 85);
            this.dateTo.Name = "dateTo";
            this.dateTo.ShowEthiopian = true;
            this.dateTo.ShowGregorian = true;
            this.dateTo.ShowTime = false;
            this.dateTo.Size = new System.Drawing.Size(302, 40);
            this.dateTo.TabIndex = 9;
            this.dateTo.VerticalLayout = true;
            this.dateTo.Load += new System.EventHandler(this.dateTo_Load);
            // 
            // checkTo
            // 
            this.checkTo.AutoSize = true;
            this.checkTo.Location = new System.Drawing.Point(3, 67);
            this.checkTo.Name = "checkTo";
            this.checkTo.Size = new System.Drawing.Size(70, 17);
            this.checkTo.TabIndex = 10;
            this.checkTo.Text = "Dates To";
            this.checkTo.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 158);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Document Type";
            // 
            // textReference
            // 
            this.textReference.Location = new System.Drawing.Point(99, 131);
            this.textReference.Name = "textReference";
            this.textReference.Size = new System.Drawing.Size(206, 20);
            this.textReference.TabIndex = 14;
            // 
            // comboDocumentType
            // 
            this.comboDocumentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDocumentType.FormattingEnabled = true;
            this.comboDocumentType.Location = new System.Drawing.Point(99, 157);
            this.comboDocumentType.Name = "comboDocumentType";
            this.comboDocumentType.Size = new System.Drawing.Size(205, 21);
            this.comboDocumentType.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Query:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.buttonSearch);
            this.panel1.Controls.Add(this.checkFrom);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.dateFrom);
            this.panel1.Controls.Add(this.textReference);
            this.panel1.Controls.Add(this.dateTo);
            this.panel1.Controls.Add(this.comboDocumentType);
            this.panel1.Controls.Add(this.checkTo);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(344, 230);
            this.panel1.TabIndex = 15;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader4,
            this.columnHeader3});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.Location = new System.Drawing.Point(3, 233);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(344, 190);
            this.listView1.TabIndex = 16;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Reference";
            this.columnHeader1.Width = 67;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Date";
            // 
            // columnHeader3
            // 
            this.columnHeader3.DisplayIndex = 2;
            this.columnHeader3.Text = "Document Type";
            this.columnHeader3.Width = 104;
            // 
            // columnHeader4
            // 
            this.columnHeader4.DisplayIndex = 3;
            this.columnHeader4.Text = "Description";
            this.columnHeader4.Width = 107;
            // 
            // buttonSearch
            // 
            this.buttonSearch.Location = new System.Drawing.Point(229, 184);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(75, 23);
            this.buttonSearch.TabIndex = 15;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(148, 184);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "Clear";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // AttachmentBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 452);
            this.Controls.Add(this.splitContainer);
            this.Name = "AttachmentBrowser";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Attachment Browser";
            this.Load += new System.EventHandler(this.AttachmentBrowser_Load);
            this.splitContainer.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabStructure.ResumeLayout(false);
            this.tabSearch.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabStructure;
        private System.Windows.Forms.TabPage tabSearch;
        private System.Windows.Forms.TreeView tree;
        private System.Windows.Forms.CheckBox checkTo;
        private INTAPS.Ethiopic.DualCalendar dateTo;
        private System.Windows.Forms.CheckBox checkFrom;
        private INTAPS.Ethiopic.DualCalendar dateFrom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textReference;
        private System.Windows.Forms.ComboBox comboDocumentType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonSearch;
    }
}