﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;

namespace BIZNET.iERP.Client
{
    public partial class AttachmentBrowser : Form
    {
        ChromiumWebBrowser chromeBrowser;
        public AttachmentBrowser()
        {
            InitializeComponent();
        }
        public void InitializeChromium()
        {
            chromeBrowser = new ChromiumWebBrowser("about:blank");
            
            chromeBrowser.Dock = DockStyle.Fill;
            splitContainer.Panel2.Controls.Add(chromeBrowser);
            // Allow the use of local resources in the browser
            BrowserSettings browserSettings = new BrowserSettings();
            browserSettings.FileAccessFromFileUrls = CefState.Enabled;
            browserSettings.UniversalAccessFromFileUrls = CefState.Enabled;
            chromeBrowser.BrowserSettings = browserSettings;
        }
        private void AttachmentBrowser_Load(object sender, EventArgs e)
        {

        }

        private void dateTo_Load(object sender, EventArgs e)
        {

        }

        private void tabSearch_Click(object sender, EventArgs e)
        {

        }
    }
}
