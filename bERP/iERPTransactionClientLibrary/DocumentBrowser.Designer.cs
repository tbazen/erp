using INTAPS.UI;
namespace BIZNET.iERP.Client
{
    partial class DocumentBrowser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DocumentBrowser));
            this.listView = new System.Windows.Forms.ListView();
            this.colDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colRef = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colNote = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelStandardSearch = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.checkUnbudgeted = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textQuery = new System.Windows.Forms.TextBox();
            this.chkTo = new System.Windows.Forms.CheckBox();
            this.comboDocumentType = new System.Windows.Forms.ComboBox();
            this.chkFrom = new System.Windows.Forms.CheckBox();
            this.dtpFrom = new INTAPS.Ethiopic.ETDateTimeDropDown();
            this.dtpTo = new INTAPS.Ethiopic.ETDateTimeDropDown();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.pageNavigator = new INTAPS.UI.PageNavigator();
            this.controlBrowser = new INTAPS.UI.HTML.ControlBrowser();
            this.bowserController1 = new INTAPS.UI.HTML.BowserController();
            this.buttonEdit = new System.Windows.Forms.ToolStripButton();
            this.buttonDelete = new System.Windows.Forms.ToolStripButton();
            this.buttonBudget = new System.Windows.Forms.ToolStripButton();
            this.panelButtonBar = new System.Windows.Forms.Panel();
            this.buttonSelect = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelStandardSearch.SuspendLayout();
            this.bowserController1.SuspendLayout();
            this.panelButtonBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView
            // 
            this.listView.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colDate,
            this.colRef,
            this.colType,
            this.colNote});
            this.listView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView.FullRowSelect = true;
            this.listView.HideSelection = false;
            this.listView.Location = new System.Drawing.Point(0, 232);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(433, 190);
            this.listView.TabIndex = 0;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.Details;
            this.listView.SelectedIndexChanged += new System.EventHandler(this.listView_SelectedIndexChanged);
            // 
            // colDate
            // 
            this.colDate.Text = "Date";
            this.colDate.Width = 110;
            // 
            // colRef
            // 
            this.colRef.Text = "Reference";
            this.colRef.Width = 122;
            // 
            // colType
            // 
            this.colType.Text = "Type";
            this.colType.Width = 86;
            // 
            // colNote
            // 
            this.colNote.Text = "Note";
            this.colNote.Width = 77;
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.listView);
            this.splitContainer.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.controlBrowser);
            this.splitContainer.Panel2.Controls.Add(this.bowserController1);
            this.splitContainer.Size = new System.Drawing.Size(980, 422);
            this.splitContainer.SplitterDistance = 433;
            this.splitContainer.SplitterWidth = 5;
            this.splitContainer.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panelStandardSearch);
            this.panel1.Controls.Add(this.buttonSearch);
            this.panel1.Controls.Add(this.pageNavigator);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(433, 232);
            this.panel1.TabIndex = 1;
            // 
            // panelStandardSearch
            // 
            this.panelStandardSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelStandardSearch.Controls.Add(this.label1);
            this.panelStandardSearch.Controls.Add(this.checkUnbudgeted);
            this.panelStandardSearch.Controls.Add(this.label2);
            this.panelStandardSearch.Controls.Add(this.textQuery);
            this.panelStandardSearch.Controls.Add(this.chkTo);
            this.panelStandardSearch.Controls.Add(this.comboDocumentType);
            this.panelStandardSearch.Controls.Add(this.chkFrom);
            this.panelStandardSearch.Controls.Add(this.dtpFrom);
            this.panelStandardSearch.Controls.Add(this.dtpTo);
            this.panelStandardSearch.Location = new System.Drawing.Point(3, 3);
            this.panelStandardSearch.Name = "panelStandardSearch";
            this.panelStandardSearch.Size = new System.Drawing.Size(423, 170);
            this.panelStandardSearch.TabIndex = 58;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Query:";
            // 
            // checkUnbudgeted
            // 
            this.checkUnbudgeted.AutoSize = true;
            this.checkUnbudgeted.Location = new System.Drawing.Point(15, 131);
            this.checkUnbudgeted.Name = "checkUnbudgeted";
            this.checkUnbudgeted.Size = new System.Drawing.Size(149, 19);
            this.checkUnbudgeted.TabIndex = 57;
            this.checkUnbudgeted.Text = "List Only Unbudgeted";
            this.checkUnbudgeted.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.Location = new System.Drawing.Point(12, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Type:";
            // 
            // textQuery
            // 
            this.textQuery.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textQuery.Location = new System.Drawing.Point(120, 7);
            this.textQuery.Name = "textQuery";
            this.textQuery.Size = new System.Drawing.Size(279, 23);
            this.textQuery.TabIndex = 1;
            this.textQuery.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textQuery_KeyDown);
            // 
            // chkTo
            // 
            this.chkTo.AutoSize = true;
            this.chkTo.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.chkTo.Location = new System.Drawing.Point(73, 96);
            this.chkTo.Name = "chkTo";
            this.chkTo.Size = new System.Drawing.Size(41, 21);
            this.chkTo.TabIndex = 55;
            this.chkTo.Text = "To";
            this.chkTo.UseVisualStyleBackColor = true;
            this.chkTo.CheckedChanged += new System.EventHandler(this.chkTo_CheckedChanged);
            // 
            // comboDocumentType
            // 
            this.comboDocumentType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboDocumentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDocumentType.FormattingEnabled = true;
            this.comboDocumentType.Location = new System.Drawing.Point(120, 37);
            this.comboDocumentType.Name = "comboDocumentType";
            this.comboDocumentType.Size = new System.Drawing.Size(279, 23);
            this.comboDocumentType.TabIndex = 2;
            // 
            // chkFrom
            // 
            this.chkFrom.AutoSize = true;
            this.chkFrom.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.chkFrom.Location = new System.Drawing.Point(15, 70);
            this.chkFrom.Name = "chkFrom";
            this.chkFrom.Size = new System.Drawing.Size(59, 21);
            this.chkFrom.TabIndex = 55;
            this.chkFrom.Text = "From";
            this.chkFrom.UseVisualStyleBackColor = true;
            this.chkFrom.CheckedChanged += new System.EventHandler(this.chkFrom_CheckedChanged);
            // 
            // dtpFrom
            // 
            this.dtpFrom.Location = new System.Drawing.Point(120, 68);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(171, 20);
            this.dtpFrom.Style = INTAPS.Ethiopic.ETDateStyle.Gregorian;
            this.dtpFrom.TabIndex = 54;
            // 
            // dtpTo
            // 
            this.dtpTo.Location = new System.Drawing.Point(120, 94);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(171, 20);
            this.dtpTo.Style = INTAPS.Ethiopic.ETDateStyle.Gregorian;
            this.dtpTo.TabIndex = 53;
            // 
            // buttonSearch
            // 
            this.buttonSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSearch.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSearch.ForeColor = System.Drawing.Color.White;
            this.buttonSearch.Location = new System.Drawing.Point(348, 183);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(78, 37);
            this.buttonSearch.TabIndex = 56;
            this.buttonSearch.Text = "&Search";
            this.buttonSearch.UseVisualStyleBackColor = false;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // pageNavigator
            // 
            this.pageNavigator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pageNavigator.Location = new System.Drawing.Point(0, 179);
            this.pageNavigator.Name = "pageNavigator";
            this.pageNavigator.NRec = 0;
            this.pageNavigator.PageSize = 0;
            this.pageNavigator.RecordIndex = 0;
            this.pageNavigator.Size = new System.Drawing.Size(294, 50);
            this.pageNavigator.TabIndex = 3;
            this.pageNavigator.PageChanged += new System.EventHandler(this.pageNavigator_PageChanged);
            // 
            // controlBrowser
            // 
            this.controlBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlBrowser.Location = new System.Drawing.Point(0, 25);
            this.controlBrowser.MinimumSize = new System.Drawing.Size(23, 23);
            this.controlBrowser.Name = "controlBrowser";
            this.controlBrowser.Size = new System.Drawing.Size(542, 397);
            this.controlBrowser.StyleSheetFile = "berp.css";
            this.controlBrowser.TabIndex = 0;
            // 
            // bowserController1
            // 
            this.bowserController1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonEdit,
            this.buttonDelete,
            this.buttonBudget});
            this.bowserController1.Location = new System.Drawing.Point(0, 0);
            this.bowserController1.Name = "bowserController1";
            this.bowserController1.ShowBackForward = false;
            this.bowserController1.ShowRefresh = false;
            this.bowserController1.Size = new System.Drawing.Size(542, 25);
            this.bowserController1.TabIndex = 1;
            this.bowserController1.Text = "bowserController1";
            this.bowserController1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.bowserController1_ItemClicked);
            // 
            // buttonEdit
            // 
            this.buttonEdit.AutoSize = false;
            this.buttonEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.buttonEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonEdit.Image = ((System.Drawing.Image)(resources.GetObject("buttonEdit.Image")));
            this.buttonEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(80, 22);
            this.buttonEdit.Text = "Edit";
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.AutoSize = false;
            this.buttonDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonDelete.Image = ((System.Drawing.Image)(resources.GetObject("buttonDelete.Image")));
            this.buttonDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(80, 22);
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.Click += new System.EventHandler(this.miDeleteDocument_Click);
            // 
            // buttonBudget
            // 
            this.buttonBudget.AutoSize = false;
            this.buttonBudget.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.buttonBudget.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonBudget.ImageTransparentColor = System.Drawing.Color.Lime;
            this.buttonBudget.Name = "buttonBudget";
            this.buttonBudget.Size = new System.Drawing.Size(80, 22);
            this.buttonBudget.Text = "Budget";
            this.buttonBudget.Click += new System.EventHandler(this.buttonBudget_Click);
            // 
            // panelButtonBar
            // 
            this.panelButtonBar.BackColor = System.Drawing.Color.DarkOrange;
            this.panelButtonBar.Controls.Add(this.buttonSelect);
            this.panelButtonBar.Controls.Add(this.btnCancel);
            this.panelButtonBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelButtonBar.Location = new System.Drawing.Point(0, 422);
            this.panelButtonBar.Name = "panelButtonBar";
            this.panelButtonBar.Size = new System.Drawing.Size(980, 42);
            this.panelButtonBar.TabIndex = 11;
            // 
            // buttonSelect
            // 
            this.buttonSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSelect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonSelect.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSelect.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSelect.ForeColor = System.Drawing.Color.White;
            this.buttonSelect.Location = new System.Drawing.Point(806, 7);
            this.buttonSelect.Name = "buttonSelect";
            this.buttonSelect.Size = new System.Drawing.Size(76, 32);
            this.buttonSelect.TabIndex = 8;
            this.buttonSelect.Text = "&Select";
            this.buttonSelect.UseVisualStyleBackColor = false;
            this.buttonSelect.Click += new System.EventHandler(this.buttonSelect_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(901, 6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(76, 32);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // DocumentBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(980, 464);
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.panelButtonBar);
            this.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "DocumentBrowser";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Document Browser";
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panelStandardSearch.ResumeLayout(false);
            this.panelStandardSearch.PerformLayout();
            this.bowserController1.ResumeLayout(false);
            this.bowserController1.PerformLayout();
            this.panelButtonBar.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView;
        private System.Windows.Forms.ColumnHeader colDate;
        private System.Windows.Forms.ColumnHeader colRef;
        private System.Windows.Forms.ColumnHeader colType;
        private System.Windows.Forms.ColumnHeader colNote;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Panel panel1;
        private INTAPS.UI.HTML.ControlBrowser controlBrowser;
        private System.Windows.Forms.TextBox textQuery;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboDocumentType;
        private System.Windows.Forms.Label label2;
        private PageNavigator pageNavigator;
        private INTAPS.Ethiopic.ETDateTimeDropDown dtpTo;
        private INTAPS.Ethiopic.ETDateTimeDropDown dtpFrom;
        private System.Windows.Forms.CheckBox chkTo;
        private System.Windows.Forms.CheckBox chkFrom;
        private System.Windows.Forms.Button buttonSearch;
        private INTAPS.UI.HTML.BowserController bowserController1;
        private System.Windows.Forms.ToolStripButton buttonEdit;
        private System.Windows.Forms.ToolStripButton buttonBudget;
        private System.Windows.Forms.Panel panelButtonBar;
        private System.Windows.Forms.Button buttonSelect;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ToolStripButton buttonDelete;
        private System.Windows.Forms.CheckBox checkUnbudgeted;
        protected System.Windows.Forms.Panel panelStandardSearch;
    }
}