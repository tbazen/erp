using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS;
namespace BIZNET.iERP.Client
{
    public partial class DocumentBrowser: Form,INTAPS.UI.HTML.IHTMLBuilder
    {
        public AccountDocument pickedDocument = null;
        int budgetDocumentTypeID = -1;
        const int PAGE_SIZE = 30;
        AccountDocument _selectedDocument = null;
        String _html = "";
        static int CompareDoc(DocumentType x, DocumentType y)
        {
            return x.name.CompareTo(y.name);
        }
        public DocumentBrowser()
            : this(false, -1)
        {
        }
        public DocumentBrowser(bool picker,int limitToDocumentTypeID)
        {
            InitializeComponent();
            DocumentType budgetDocumentType = AccountingClient.GetDocumentTypeByTypeNoException(typeof(BudgetTransactionDocument));
            budgetDocumentTypeID = budgetDocumentType == null ? -1 : budgetDocumentType.id;

            panelButtonBar.Visible = picker;
            UpdateDateRangeControls();
            comboDocumentType.Items.Add("All");
            DocumentType[] items = (DocumentType[])INTAPS.Accounting.Client.AccountingClient.GetAllDocumentTypes().Clone();
            Array.Sort<DocumentType>(items, new Comparison<DocumentType>(CompareDoc));
            comboDocumentType.Items.AddRange(items);
            if (limitToDocumentTypeID != -1)
            {
                foreach (object docType in comboDocumentType.Items)
                {
                    if (docType is DocumentType)
                    {
                        if (((DocumentType)docType).id == limitToDocumentTypeID)
                        {
                            comboDocumentType.SelectedItem = docType;
                            break;
                        }
                    }
                }
            }
            else
                comboDocumentType.SelectedIndex = 0;

            pageNavigator.PageSize = PAGE_SIZE;
            bowserController1.SetBrowser(controlBrowser);
            listView_SelectedIndexChanged(null, null);
        }

        private void chkTo_CheckedChanged(object sender, EventArgs e)
        {
            UpdateDateRangeControls();
        }

        private void chkFrom_CheckedChanged(object sender, EventArgs e)
        {
            UpdateDateRangeControls();
        }

        private void UpdateDateRangeControls()
        {
            dtpFrom.Enabled = chkFrom.Checked;
            chkTo.Enabled = chkFrom.Checked;
            dtpTo.Enabled = chkFrom.Checked && chkTo.Checked;
            if (chkTo.Checked && chkFrom.Checked)
                chkFrom.Text = "Date From";
            else
                chkFrom.Text = "Date";
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            pageNavigator.RecordIndex = 0;
            LoadData();
        }
        protected virtual AccountDocument[] searchData()
        {
            int typeID;
            if (comboDocumentType.SelectedIndex == 0)
                typeID = -1;
            else
                typeID = ((DocumentType)comboDocumentType.SelectedItem).id;
            int N;

            Cursor.Current = Cursors.WaitCursor;
            AccountDocument[] docs;
            if (checkUnbudgeted.Checked)
            {

                if (typeID == -1 && !chkFrom.Checked && string.IsNullOrEmpty(textQuery.Text.Trim()))
                {
                    docs = iERPTransactionClient.getUnbudgetedDocuments(null, pageNavigator.RecordIndex, PAGE_SIZE, out N);
                }
                else
                {
                    DocumentSearchPar par = new DocumentSearchPar()
                    {
                        query = textQuery.Text.Trim(),
                        type = typeID,
                        date = chkFrom.Checked,
                        from = dtpFrom.Value,
                        to = chkTo.Checked ? dtpTo.Value.AddDays(1) : dtpFrom.Value.AddDays(1)
                    };
                    docs = iERPTransactionClient.getUnbudgetedDocuments(par, pageNavigator.RecordIndex, PAGE_SIZE, out N);
                }
            }
            else
            {
                docs = INTAPS.Accounting.Client.AccountingClient.GetAccountDocuments(textQuery.Text
                    , typeID, chkFrom.Checked, dtpFrom.Value, chkTo.Checked ? dtpTo.Value.AddDays(1) : dtpFrom.Value.AddDays(1), pageNavigator.RecordIndex, PAGE_SIZE, out N);
            }
            pageNavigator.NRec = N;
            return docs;
        }
        private void LoadData()
        {
            try
            {
                listView.Items.Clear();
                Font strikeoutFont = new Font(listView.Font, FontStyle.Strikeout);

                foreach (AccountDocument doc in searchData())
                {
                    ListViewItem li = new ListViewItem();
                    SetDocument(strikeoutFont, doc, li);
                    listView.Items.Add(li);
                }
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error loading documents.", ex);
            }   
        }

        private void SetDocument(Font strikeoutFont, AccountDocument doc, ListViewItem li)
        {
            li.Text = doc.DocumentDate.ToString();
            li.Tag = doc;
            li.SubItems.Add(doc.PaperRef);
            li.SubItems.Add(INTAPS.Accounting.Client.AccountingClient.GetDocumentTypeByID(doc.DocumentTypeID).name);
            li.SubItems.Add(doc.ShortDescription);
            if (doc.reversed)
                li.Font = strikeoutFont;
        }

        private void pageNavigator_PageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        string GetDocHTML(AccountDocument doc)
        {
            INTAPS.Accounting.IGenericDocumentClientHandler h = INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(doc.DocumentTypeID);
            if (h != null)
                return INTAPS.Accounting.Client.AccountingClient.GetDocumentHTML(doc.AccountDocumentID);
            else
                return doc.BuildHTML();
        }
        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (listView.SelectedItems.Count == 0)
            {
                buttonEdit.Visible = false;
                buttonDelete.Visible= false;
                buttonBudget.Visible = false;
                buttonSelect.Enabled = false;
                controlBrowser.LoadTextPage("Document", "");
                return;
            }
            AccountDocument doc = (AccountDocument)listView.SelectedItems[0].Tag;
            INTAPS.Accounting.IGenericDocumentClientHandler h = INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(doc.DocumentTypeID);
            buttonEdit.Visible = h!=null;
            buttonBudget.Visible = budgetDocumentTypeID != -1 &&
                budgetDocumentTypeID != doc.DocumentTypeID &&
                !iERPTransactionClient.isDocumentBudgetSet(doc.AccountDocumentID);
            buttonSelect.Enabled = buttonDelete.Visible = true;
            
            try
            {
                string attachment = "";
                if (iERPTransactionClient.getAttachedFileGroupForDocument(doc.AccountDocumentID).Length > 0)
                {
                    attachment = "<br/><a class='attachment_link' href=viewAttachment?docID={0}>Paper Documents</a>".format(doc.AccountDocumentID);
                }
                _html=INTAPS.Accounting.Client.AccountingClient.getDocumentEntriesHTML(doc.AccountDocumentID)
                     +attachment;
                _selectedDocument = doc;
                controlBrowser.LoadControl(this);
            }
            catch (Exception ex)
            {
                controlBrowser.LoadTextPage("Error", ex.Message);
            }
        }

        private void miDeleteDocument_Click(object sender, EventArgs e)
        {
            if (listView.SelectedItems.Count ==0)
                return;
            List<AccountDocument> docs = new List<AccountDocument>();
            foreach(ListViewItem lvi in listView.SelectedItems)
            {
                AccountDocument selectedDoc = (AccountDocument)lvi.Tag;
                docs.Add(selectedDoc);
            }
            DeleteProgressForm dp = new DeleteProgressForm(docs, false);
            if (dp.ShowDialog(this) == DialogResult.OK)
            {
                int c = listView.SelectedItems.Count;
                while (c > 0)
                {
                    ListViewItem lvi = listView.SelectedItems[0];
                    lvi.Remove();
                    c--;
                }
            }
        }

        private void contextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            e.Cancel = listView.SelectedItems.Count == 0;
        }

        private void textQuery_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonSearch_Click(buttonSearch, null);
            }
        }

        private void miReverseDocument_Click(object sender, EventArgs e)
        {
            if (listView.SelectedItems.Count ==0)
                return;

            List<AccountDocument> docs = new List<AccountDocument>();
            foreach (ListViewItem lvi in listView.SelectedItems)
            {
                AccountDocument selectedDoc = (AccountDocument)lvi.Tag;
                docs.Add(selectedDoc);
            }
            DeleteProgressForm dp = new DeleteProgressForm(docs, true);
            if (dp.ShowDialog(this) == DialogResult.OK)
            {
                int c = listView.SelectedItems.Count;
                while (c > 0)
                {
                    ListViewItem lvi = listView.SelectedItems[0];
                    Font f = new Font(lvi.Font, FontStyle.Strikeout);
                    lvi.Font = f;
                    c--;
                }
            }

        }
        
        protected virtual bool CanEditDocument(AccountDocument doc)
        {
            INTAPS.Accounting.IGenericDocumentClientHandler h = INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(doc.DocumentTypeID);
            if (h == null)
                return false;
            return h.CanEdit;
        }
        
        protected virtual DialogResult EditDocument(AccountDocument doc)
        {
            INTAPS.Accounting.IGenericDocumentClientHandler h = INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(doc.DocumentTypeID);
            if (h == null)
                return DialogResult.Cancel;
            h.setAccountingClient(AccountingClient.AccountingClientDefaultInstance);
            Form f= h.CreateEditor(false) as Form;
            if (f == null)
                return DialogResult.Cancel;
            h.SetEditorDocument(f, AccountingClient.GetAccountDocument(doc.AccountDocumentID,true));
            return f.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm);
        }

        

        private void bowserController1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = listView.SelectedItems[0];
            AccountDocument selectedDoc = (AccountDocument)lvi.Tag;
            try
            {
                try
                {

                    Cursor.Current = Cursors.WaitCursor;
                    if (EditDocument(selectedDoc) == DialogResult.OK)
                    {
                        Font strikeoutFont = new Font(listView.Font, FontStyle.Strikeout);
                        LoadData();
                    }
                    Application.DoEvents();
                }
                finally
                {
                    Cursor.Current = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Unable to edit document.", ex);
            }
        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            if (listView.SelectedItems.Count == 0)
                return;
            pickedDocument = (AccountDocument)listView.SelectedItems[0].Tag;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonBudget_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = listView.SelectedItems[0];
            AccountDocument selectedDoc = (AccountDocument)lvi.Tag;
            Form f = ((bERPClientDocumentHandler)AccountingClient.GetDocumentHandler(budgetDocumentTypeID)).CreateEditor(new ActivationParameter() { referedDocumentID = selectedDoc.AccountDocumentID }) as Form;
            f.Show(this);
        }

        public void Build()
        {
            throw new NotImplementedException();
        }

        public string GetOuterHtml()
        {
            return _html;
        }

        public bool ProcessUrl(string path, System.Collections.Hashtable query)
        {
            switch(path)
            {
                case "viewAttachment":
                    this.Invoke(new INTAPS.UI.HTML.ProcessSingleParameter<int>(delegate(int docID)
                        {
                            VisualBrowser vb = new VisualBrowser(_selectedDocument.AccountDocumentID, -1, VisualBrowser.VisualBrowserMode.View);
                            vb.Show(this);
                        }
                        ),int.Parse((string)query["docID"]));
                    return true;
            }
            return false;
        }

        public void SetHost(INTAPS.UI.HTML.IHTMLDocumentHost host)
        {
            
        }

        public string WindowCaption
        {
            get { return "document"; }
        }
    }
}