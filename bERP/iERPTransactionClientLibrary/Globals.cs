using System;
using System.Collections.Generic;
using System.Text;
//using INTAPS.UI.ButtonGrid;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Client
{
    public class Globals
    {
        //public static ButtonGrid MainButtonGrid;
        public static BizNetPaymentMethod CurrentPaymentMethod = BizNetPaymentMethod.None;
        public static int CurrentEmployeeID;
        public static string CurrentCode;
        public static bool DocumentWithPaymentTypeActivated;
        public static PurchaseAndSalesData CurrentPurchaseAndSalesData;
        public static BondPaymentDocument CurrentBondPaymentDocument;
        public static int CurrentMainAccountID;
        public static AccountingPeriod CurrentTaxDeclarationPeriod;
        public static AccountingPeriod CurrentAccountingPeriod;
        public static int CurrentFiscalYear;
        public static double CurrentDeclaredAmount;
        public static int CurrentTaxDeclarationID;
        //public static TaxDeclarationManager CurrentTaxDeclarationManager;
        public static double CurrentPeriodCredit = 0;
        public static double CarryCreditForward = 0;
        public static double accumulatedCredit = 0;
        public static int currentCostCenterID;
        public static int referedDocumentID;
        public static ActivationParameter GetActivationParameter()
        {
            ActivationParameter ret = new ActivationParameter();
            ret.paymentMethod=CurrentPaymentMethod;
            ret.employeeID=CurrentEmployeeID;
            ret.objectCode = Globals.CurrentCode;
            ret.bondPaymentDocument=CurrentBondPaymentDocument;
            ret.CurrentTaxDeclarationPeriod = CurrentTaxDeclarationPeriod;
            ret.CurrentTaxDeclarationID = CurrentTaxDeclarationID;
            ret.CurrentDeclaredAmount = CurrentDeclaredAmount;
            ret.CurrentMainAccountID = CurrentMainAccountID;
            ret.CurrentPeriodCredit = CurrentPeriodCredit;
            ret.CurrentPeriodCredit = CurrentPeriodCredit;
            ret.accumulatedCredit = accumulatedCredit;
            return ret;
        }

        public static int buttonGridDocumentTypeID { get; set; }

        public static CostCenterAccountWithDescription getCostCenterAccountByCode(string code)
        {
            string[] parts = code.Split(':');
            if (parts.Length == 0 || parts.Length > 2)
                throw new ServerUserMessage("Invalid code format");
            if (parts.Length == 2)
            {
                return AccountingClient.GetCostCenterAccountWithDescription(code);
            }
            Account ac=AccountingClient.GetAccount<Account>(code);
            if (ac == null)
                return null;
            return AccountingClient.GetCostCenterAccountWithDescription(currentCostCenterID, 
                ac.id
                );
        }
        static Globals()
        {
            Initialize();
        }
        public static void Initialize()
        {
            string text = (string)iERPTransactionClient.GetSystemParamter("taxDeclarationPeriods");
            if (string.IsNullOrEmpty(text))
                Globals.CurrentTaxDeclarationPeriod = null;
            else
                Globals.CurrentTaxDeclarationPeriod = iERPTransactionClient.GetAccountingPeriod(text, DateTime.Now);
            Globals.CurrentAccountingPeriod = iERPTransactionClient.GetAccountingPeriod("AP_Accounting_Month", DateTime.Now);
            INTAPS.Ethiopic.EtGrDate etToday = INTAPS.Ethiopic.EtGrDate.ToEth(DateTime.Now);
            if (etToday.Month < 11)
                Globals.CurrentFiscalYear = etToday.Year;
            else
                Globals.CurrentFiscalYear = etToday.Year + 1;
        }
    }
    
    
}
