﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERP.Client
{
    public class FixedAssetRuleClientAttribute:Attribute
    {
        public int id;
        public FixedAssetRuleClientAttribute(int id)
        {
            this.id = id;
        }
    }
    public interface IFixedAssetRuleClient
    {
        Form getConfigurationForm(object configurationData);
    }
}
