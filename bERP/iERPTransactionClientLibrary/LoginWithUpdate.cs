﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace BIZNET.iERP.Client
{
    public class LoginWithUpdate: System.Windows.Forms.Form
    {
        protected virtual void setDownloadProgress(double progress)
        {
        }
        private static void writeUpdateFile(string updateFolder, string s)
        {
            string pathPart = "";
            string[] path = s.Split('\\');
            for (int j = 0; j < path.Length - 1; j++)
            {
                pathPart += path[j] + "\\";
                if (!System.IO.Directory.Exists(updateFolder + pathPart))
                    System.IO.Directory.CreateDirectory(updateFolder + pathPart);
            }
            System.IO.File.WriteAllBytes(updateFolder + pathPart + path[path.Length - 1], iERPTransactionClient.getERPClientLatestFile(s));
        }
        protected void checkClientUpdate()
        {
            if (System.Configuration.ConfigurationManager.AppSettings["autoUpdateClient"] != "true")
                return;
            this.Enabled = false;
            Application.DoEvents();
            string updateFolder = Application.StartupPath + "\\" + System.Configuration.ConfigurationManager.AppSettings["updateFolder"];
            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();

            bool updated = false;
            try
            {
                string[] serverFileList = iERPTransactionClient.getAllClientFiles();
                int p = 0;
                foreach (string s in serverFileList)
                {
                    setDownloadProgress((double)p / (double)serverFileList.Length);
                    Application.DoEvents();
                    p++;

                    string fn = Application.StartupPath + "\\" + s;
                    if (System.IO.File.Exists(fn))
                    {
                        if (iERPTransactionClient.isBERPClientFileUpdated(s, md5.ComputeHash(System.IO.File.ReadAllBytes(Application.StartupPath + "\\" + s))))
                        {
                            writeUpdateFile(updateFolder, s);
                            updated = true;
                        }
                    }
                    else
                    {
                        updated = true;
                        writeUpdateFile(updateFolder, s);
                    }
                }
                if (!updated)
                    return;
                MessageBox.Show("The bERP Client will close and update with a new copy from the server.", "Application Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                System.Diagnostics.Process.Start(updateFolder + "\\iERPUpdaterClient.exe", Application.ExecutablePath);
            }
            catch (Exception ex)
            {
                string msg = "";
                while (ex != null)
                {
                    if (msg != "")
                        msg += "\n";
                    msg += ex.Message;
                    msg += ex.StackTrace;
                    ex = ex.InnerException;
                }
                MessageBox.Show("Error trying to update application.\nYou can be running an wrong application version.\n" + msg);
            }
        }
        
    }
}
