﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP.Client
{
    public abstract class bERPClientDocumentHandler:IGenericDocumentClientHandler
    {
        protected IAccountingClient _client;
        protected ActivationContext _pars;
        protected Type _formType;
        public bERPClientDocumentHandler(Type t)
        {
            _formType = t;
        }

        //methods that bERP don't use
        public AccountDocument GetEditorDocument(object editor)
        {
            throw new NotImplementedException();
        }

        public bool VerifyUserEntry(object editor, out string error)
        {
            throw new NotImplementedException();
        }
        
        //default implementations
        public virtual bool CanEdit
        {
            get { return true; }
        }
        public virtual void setAccountingClient(IAccountingClient client)
        {
            _client = client;
        }
        public virtual object CreateEditor(bool embeded)
        {
            return Activator.CreateInstance(_formType, new object[] { _client, new ActivationParameter() });
        }
        public virtual object CreateEditor(ActivationParameter pars)
        {
            return Activator.CreateInstance(_formType, new object[] { _client, pars });
        }

        //methods that need to be implemented
        public abstract void SetEditorDocument(object editor, AccountDocument doc);
        


    }
}
