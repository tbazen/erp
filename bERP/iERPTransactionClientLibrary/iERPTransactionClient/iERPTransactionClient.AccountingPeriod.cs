using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Payroll;
using INTAPS.ClientServer.Client;

namespace BIZNET.iERP.Client
{
    public partial class iERPTransactionClient
    {
        static public void GenerateAccountingPeriodSeries(Type generator, string type, DateTime from, DateTime to)
        {
           // server.GenerateAccountingPeriodSeries(ApplicationClient.SessionID, generator, type, from, to);
           RESTAPIClient.Call<VoidRet>(path + "GenerateAccountingPeriodSeries", new { sessionID=ApplicationClient.SessionID, generator= generator , type = type, from= from, to= to });
        }

        static public AccountingPeriod GetAccountingPeriod(string periodType, int id)
        {
            // return server.GetAccountingPeriod(ApplicationClient.SessionID, periodType, id);
            return RESTAPIClient.Call<AccountingPeriod>(path + "GetAccountingPeriod", new { sessionID=ApplicationClient.SessionID, periodType= periodType, id= id });
        }
        static public AccountingPeriod GetAccountingPeriod(string periodType, DateTime date)
        {
            //return server.GetAccountingPeriod(ApplicationClient.SessionID, periodType, date);
            return RESTAPIClient.Call<AccountingPeriod>(path + "GetAccountingPeriod2", new { sessionID=ApplicationClient.SessionID, periodType=periodType, date= date });
        }
        static public AccountingPeriod[] GetAccountingPeriodsInFiscalYear(string periodType, int year)
        {
            //return server.GetAccountingPeriodsInFiscalYear(ApplicationClient.SessionID, periodType, year);
            return RESTAPIClient.Call<AccountingPeriod[]>(path + "GetAccountingPeriodsInFiscalYear", new { sessionID=ApplicationClient.SessionID, periodType= periodType, year = year });
        }
        static public AccountingPeriod[] GetTouchedPeriods(string periodType, DateTime fromDate, DateTime toDate)
        {
            //return server.GetTouchedPeriods(ApplicationClient.SessionID, periodType, fromDate, toDate);
            return RESTAPIClient.Call<AccountingPeriod[]>(path + "GetTouchedPeriods", new { sessionID=ApplicationClient.SessionID, periodType= periodType, fromDate= fromDate, toDate= toDate });
        }
        static public AccountingPeriod GetNextAccountingPeriod(string periodType, int id)
        {
            //return server.GetNextAccountingPeriod(ApplicationClient.SessionID, periodType, id);
            return RESTAPIClient.Call<AccountingPeriod>(path + "GetNextAccountingPeriod", new { sessionID=ApplicationClient.SessionID , periodType = periodType , id = id });
        }

        static public AccountingPeriod GetPreviousAccountingPeriod(string periodType, int id)
        {
            //return server.GetPreviousAccountingPeriod(ApplicationClient.SessionID, periodType, id);
            return RESTAPIClient.Call<AccountingPeriod>(path + "GetPreviousAccountingPeriod", new { sessionID=ApplicationClient.SessionID, periodType= periodType, id = id });
        }

        public static void setPermanentWorkingData(int employeeID, int periodID, int formulaID, PayrollComponentData singleValueData, object additionalData)
        {
            //server.setPermanentWorkingData(ApplicationClient.SessionID, employeeID, periodID, formulaID, singleValueData, additionalData);
            RESTAPIClient.Call<VoidRet>(path + "setPermanentWorkingData", new { sessionID=ApplicationClient.SessionID, employeeID= employeeID, periodID= periodID, formulaID= formulaID, singleValueData= singleValueData, additionalData= new INTAPS.ClientServer.BinObject(additionalData) });
        }
        public static void ChageEmployeeOrgnizationalUnit(int p)
        {
            throw new NotImplementedException();
        }

        public static CompanyProfile GetCompanyProfile()
        {
            CompanyProfile profie=GetSystemParamter("companyProfile") as CompanyProfile;
            //if (profie == null)
            //    throw new Exception("Company profile not setup");
            return profie;
        }

        internal static AttachedFileGroup getAttachedFileGroup(int fileGroupID)
        {
            //return server.getAttachedFileGroup(ApplicationClient.SessionID,fileGroupID);
            return RESTAPIClient.Call<AttachedFileGroup>(path + "getAttachedFileGroup", new { sessionID=ApplicationClient.SessionID, fileGroupID= fileGroupID });
        }
    }
}
