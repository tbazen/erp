using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Payroll;
using INTAPS.ClientServer.Client;

namespace BIZNET.iERP.Client
{
    public partial class iERPTransactionClient
    {
        static public BankAccountInfo GetBankAccount(int mainAccountID)
        {
            //return server.GetBankAccount(ApplicationClient.SessionID, mainAccountID);
            return RESTAPIClient.Call<BankAccountInfo>(path + "GetBankAccount", new { sessionID=ApplicationClient.SessionID, mainAccountID= mainAccountID });
        }

        static public BankAccountInfo[] GetAllBankAccounts()
        {
            //return server.GetAllBankAccounts(ApplicationClient.SessionID);
            return RESTAPIClient.Call<BankAccountInfo[]>(path + "GetAllBankAccounts", new { sessionID= ApplicationClient.SessionID });
        }

        static public void DeleteBankAccount(int accountID)
        {
            //server.DeleteBankAccount(ApplicationClient.SessionID, accountID);
            RESTAPIClient.Call<VoidRet>(path + "DeleteBankAccount", new { sessionID=ApplicationClient.SessionID, accountID= accountID });
        }

        static public int CreateBankAccount(BankAccountInfo account, int costCenterID)
        {
            //  return server.CreateBankAccount(ApplicationClient.SessionID, account,costCenterID);
            return RESTAPIClient.Call<int>(path + "CreateBankAccount", new { sessionID=ApplicationClient.SessionID, account= account, costCenterID= costCenterID });
        }
        static public void ActivateBankAccount(int mainCsAccount)
        {
            //server.ActivateBankAccount(ApplicationClient.SessionID, mainCsAccount);
            RESTAPIClient.Call<VoidRet>(path + "ActivateBankAccount", new { sessionID=ApplicationClient.SessionID, mainCsAccount= mainCsAccount });
        }
        static public void DeactivateBankAccount(int mainCsAccount)
        {
           // server.DeactivateBankAccount(ApplicationClient.SessionID, mainCsAccount);
           RESTAPIClient.Call<VoidRet>(path + "DeactivateBankAccount", new { sessionID=ApplicationClient.SessionID, mainCsAccount= mainCsAccount });
        }
    }
}
