using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Payroll;
using INTAPS.ClientServer.Client;

namespace BIZNET.iERP.Client
{
    public partial class iERPTransactionClient
    {
        static public CashAccount GetCashAccount(string code)
        {
            //return server.GetCashAccount(ApplicationClient.SessionID, code);
            return RESTAPIClient.Call<CashAccount>(path + "GetCashAccount", new { sessionID=ApplicationClient.SessionID, code= code });
        }
        static public CashAccount GetCashAccount(int csAccountID)
        {
            // return server.GetCashAccount(ApplicationClient.SessionID, csAccountID);
            return RESTAPIClient.Call<CashAccount>(path + "GetCashAccount2", new { sessionID=ApplicationClient.SessionID, csAccountID= csAccountID });
        }

        static public CashAccount[] GetAllCashAccounts(bool includeExpenseAdvance)
        {
            //return server.GetAllCashAccounts(ApplicationClient.SessionID,includeExpenseAdvance);
            return RESTAPIClient.Call<CashAccount[]>(path + "GetAllCashAccounts", new { sessionID=ApplicationClient.SessionID, includeExpenseAccount= includeExpenseAdvance });
        }

        static public void DeleteCashAccount(string code)
        {
            //server.DeleteCashAccount(ApplicationClient.SessionID, code);
            RESTAPIClient.Call<VoidRet>(path + "DeleteCashAccount", new { sessionID=ApplicationClient.SessionID, code= code });
        }

        static public string CreateCashAccount(CashAccount account,int costCenterID)
        {
            // return server.CreateCashAccount(ApplicationClient.SessionID, account, costCenterID);
            return RESTAPIClient.Call<string>(path + "CreateCashAccount", new { sessionID=ApplicationClient.SessionID, account= account, costCenterID= costCenterID });
        }
        static public void ActivateCashAccount(string code)
        {
            //server.ActivateCashAccount(ApplicationClient.SessionID, code);
            RESTAPIClient.Call<VoidRet>(path + "ActivateCashAccount", new { sessionID=ApplicationClient.SessionID, code= code });
        }
        static public void DeactivateCashAccount(string code)
        {
            //server.DeactivateCashAccount(ApplicationClient.SessionID, code);
            RESTAPIClient.Call<VoidRet>(path + "DeactivateCashAccount", new { sessionID=ApplicationClient.SessionID, code= code });
        }
    }
}
