using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Payroll;
using INTAPS.ClientServer.Client;

namespace BIZNET.iERP.Client
{
    public partial class iERPTransactionClient
    {
        #region TradeRelation Methods
        public static string RegisterCustomer(TradeRelation customer)
        {
            //return server.RegisterCustomer(ApplicationClient.SessionID, customer);
            return RESTAPIClient.Call<string>(path + "RegisterCustomer", new { sessionID=ApplicationClient.SessionID, customer= customer });
        }
        public static TradeRelation GetCustomer(string code)
        {
            //  return server.GetCustomer(ApplicationClient.SessionID, code);
            return RESTAPIClient.Call<TradeRelation>(path + "GetCustomer", new { sessionID=ApplicationClient.SessionID, code= code });
        }
        public static TradeRelation[] GetAllCustomers()
        {
            //return server.GetAllCustomers(ApplicationClient.SessionID);
            return RESTAPIClient.Call<TradeRelation[]>(path + "GetAllCustomers", new { sessionID=ApplicationClient.SessionID });
        }
        public class SearchCustomersOut
        {
            public int NoOfRecords;
            public TradeRelation[] _ret;
        }
        public static TradeRelation[] SearchCustomers(int index, int NoOfPages, object[] criteria, string[] column, out int NoOfRecords)
        {
            //return server.SearchCustomers(ApplicationClient.SessionID, index, NoOfPages, criteria, column, out NoOfRecords);
            var _ret=RESTAPIClient.Call<SearchCustomersOut>(path + "SearchCustomers", new { sessionID=ApplicationClient.SessionID, index= index, NoOfPages= NoOfPages, criteria= criteria, column = column });
            NoOfRecords = _ret.NoOfRecords;
            return _ret._ret;
        }
        public static bool IsCustomerInvolvedInTransactions(string customerCode)
        {
            // return server.IsCustomerInvolvedInTransactions(ApplicationClient.SessionID, customerCode);
            return RESTAPIClient.Call<bool>(path + "IsCustomerInvolvedInTransactions", new { sessionID=ApplicationClient.SessionID, customerCode= customerCode });
        }
        public static BondPaymentDocument[] GetAllUnreturnedBonds(string query)
        {
            //return server.GetAllUnreturnedBonds(ApplicationClient.SessionID,query);
            return RESTAPIClient.Call<BondPaymentDocument[]>(path + "GetAllUnreturnedBonds", new { sessionID=ApplicationClient.SessionID, query= query });
        }
        #endregion

    }
}
