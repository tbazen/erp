using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Payroll;
using System.Data;
using INTAPS.ClientServer.Client;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Client
{
    public partial class iERPTransactionClient
    {
        public class GetCandidatesForDeclarationOut
        {
            public int[] taxCenterID; public double[] taxAmount;
            public int[] _ret;
        }
        static public int[] GetCandidatesForDeclaration(int declarationType, DateTime declarationDate, int periodID, out int [] taxCenterID, out double[] taxAmount)
        {
            // return server.GetCandidatesForDeclaration(ApplicationClient.SessionID, declarationType, declarationDate, periodID,out taxCenterID,out taxAmount);
            var _ret=RESTAPIClient.Call<GetCandidatesForDeclarationOut>(path + "GetCandidatesForDeclaration", new { sessionID=ApplicationClient.SessionID, declarationType= declarationType, declarationDate= declarationDate, periodID= periodID });
            taxCenterID = _ret.taxCenterID;
            taxAmount = _ret.taxAmount;
            return _ret._ret;
        }
        public class PreviewDeclarationOut
        {
            public double total;
            public BinObject _ret;
        }
        static public DataSet PreviewDeclaration(int declarationType, DateTime declarationDate, int periodID, int taxCenterID, int[] documents, int[] rejectedDocuments, out double total, bool newDeclaration)
        {
            //return server.PreviewDeclaration(ApplicationClient.SessionID, declarationType, declarationDate, periodID, taxCenterID,documents,rejectedDocuments, out total,newDeclaration);
            var _ret=RESTAPIClient.Call<PreviewDeclarationOut>(path + "PreviewDeclaration", new { sessionID=ApplicationClient.SessionID, declarationType= declarationType, declarationDate= declarationDate, periodID= periodID, taxCenterID= taxCenterID, documents= documents, rejectedDocuments= rejectedDocuments , newDeclaration = newDeclaration });
            total = _ret.total;
            return _ret._ret.Deserialized() as DataSet;
        }

        static public int SaveTaxDeclaration(int declarationType, DateTime declarationDate, int periodID, int[] documents, int taxCenterID)
        {
            // return server.SaveTaxDeclaration(ApplicationClient.SessionID, declarationType, declarationDate, periodID, documents,taxCenterID);
            return RESTAPIClient.Call<int>(path + "SaveTaxDeclaration", new { sessionID=ApplicationClient.SessionID, declarationType= declarationType, declarationDate= declarationDate, periodID= periodID, documents= documents, taxCenterID = taxCenterID });
        }

        static public TaxDeclaration[] GetTaxDeclarations(int declarationType, int period1, int period2)
        {
            // return server.GetTaxDeclarations(ApplicationClient.SessionID, declarationType, period1, period2);
            return RESTAPIClient.Call<BinObject>(path + "GetTaxDeclarations", new { sessionID = ApplicationClient.SessionID, declarationType = declarationType, period1 = period1, period2 = period2 }).Deserialized() as TaxDeclaration[];
        }

        static public TaxDeclaration GetTaxDeclaration(int declatationID)
        {
            // return server.GetTaxDeclaration(ApplicationClient.SessionID, declatationID);
            return RESTAPIClient.Call<BinObject>(path + "GetTaxDeclaration", new { sessionID = ApplicationClient.SessionID, declatationID = declatationID }).Deserialized() as TaxDeclaration;
        }
        static public TaxDeclaration GetTaxDeclaration(int declarationType, int periodID)
        {
            // return server.GetTaxDeclaration(ApplicationClient.SessionID,declarationType,periodID);
            return RESTAPIClient.Call<TaxDeclaration>(path + "GetTaxDeclaration2", new { sessionID =ApplicationClient.SessionID, declarationType = declarationType , periodID = periodID });
        }
        static public bool HasTaxDeclarationBegan(int declarationType)
        {
            //return server.HasTaxDeclarationBegan(ApplicationClient.SessionID, declarationType);
            return RESTAPIClient.Call<bool>(path + "HasTaxDeclarationBegan", new { sessionID=ApplicationClient.SessionID, declarationType= declarationType });
        }
        static public void DeleteDeclaration(int declarationID)
        {
         //   server.DeleteDeclaration(ApplicationClient.SessionID, declarationID);
         RESTAPIClient.Call<VoidRet>(path + "DeleteDeclaration", new { sessionID= ApplicationClient.SessionID, declarationID= declarationID });
        }

        static public void UpdateDeclaration(TaxDeclaration decl)
        {
           // server.UpdateDeclaration(ApplicationClient.SessionID, decl);
           RESTAPIClient.Call<VoidRet>(path + "UpdateDeclaration", new { sessionID=ApplicationClient.SessionID, decl= decl });
        }

        static public int CountPaidTaxDeclarations(int declarationType)
        {
            //return  server.CountPaidTaxDeclarations(ApplicationClient.SessionID, declarationType);
            return RESTAPIClient.Call<int>(path + "CountPaidTaxDeclarations", new { sessionID=ApplicationClient.SessionID, declarationType= declarationType });
        }
        static public bool IsPreviousDeclarationPeriodConfirmed(int declarationType, int currPeriodID)
        {
            //return server.IsPreviousDeclarationPeriodConfirmed(ApplicationClient.SessionID, declarationType, currPeriodID);
            return RESTAPIClient.Call<bool>(path + "IsPreviousDeclarationPeriodConfirmed", new { sessionID=ApplicationClient.SessionID, declarationType= declarationType, currPeriodID= currPeriodID });
        }
        static public TaxCenter[] GetTaxCenters()
        {
            //return server.GetTaxCenters(ApplicationClient.SessionID);
            return RESTAPIClient.Call<TaxCenter[]>(path + "GetTaxCenters", new { sessionID=ApplicationClient.SessionID });
        }
        static public TaxCenter GetTaxCenter(int id)
        {
            // return server.GetTaxCenter(ApplicationClient.SessionID, id);
            return RESTAPIClient.Call<TaxCenter>(path + "GetTaxCenter", new { sessionID=ApplicationClient.SessionID, id= id });
        }
    }
}