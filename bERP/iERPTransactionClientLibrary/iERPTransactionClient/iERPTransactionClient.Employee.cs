using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Payroll;
using INTAPS.ClientServer.Client;

namespace BIZNET.iERP.Client
{
    public partial class iERPTransactionClient
    {
        static public int RegisterEmployee(Employee employee, byte[] picture, bool updateWorkingData, int[] otherAccounts, int periodID, Data_RegularTime regular, OverTimeData overtime, SingleValueData[] othersData, int[] singleValueFormulaID)
        {
            //return server.RegisterEmployee(ApplicationClient.SessionID, employee, picture, updateWorkingData,otherAccounts, periodID, regular, overtime, othersData, singleValueFormulaID);
            return RESTAPIClient.Call<int>(path + "RegisterEmployee", new { sessionID=ApplicationClient.SessionID, employee= employee, picture= picture, updateWorkingData= updateWorkingData, otherAccounts= otherAccounts, periodID= periodID, regular= regular, overtime= overtime, othersData = othersData , singleValueFormulaID = singleValueFormulaID });
        }
        static  public void DeleteEmployee(int ID)
        {
           // server.DeleteEmployee(ApplicationClient.SessionID, ID);
           RESTAPIClient.Call<VoidRet>(path + "DeleteEmployee", new { sessionID=ApplicationClient.SessionID, ID= ID });
        }
        static public void DeleteEmployeeAccount(int empID, int accountID)
        {
           // server.DeleteEmployeeAccount(ApplicationClient.SessionID, empID, accountID);
           RESTAPIClient.Call<VoidRet>(path + "DeleteEmployeeAccount", new { sessionID=ApplicationClient.SessionID, empID = empID , accountID = accountID });
        }
        static public void FireEmployee(int ID,DateTime dismissalDate)
        {
           // server.FireEmployee(ApplicationClient.SessionID, ID, dismissalDate);
           RESTAPIClient.Call<VoidRet>(path + "FireEmployee", new { sessionID=ApplicationClient.SessionID, ID = ID , dismissalDate = dismissalDate });
        }
        static  public void EnrollEmployee(int ID,DateTime enrollDate)
        {
            //server.EnrollEmployee(ApplicationClient.SessionID, ID, enrollDate);
            RESTAPIClient.Call<VoidRet>(path + "EnrollEmployee", new { sessionID=ApplicationClient.SessionID, ID = ID , enrollDate = enrollDate });
        }
        static public Employee[] GetAllEmployeesExcept(int[] employeeID)
        {
            //return server.GetAllEmployeesExcept(ApplicationClient.SessionID, employeeID);
            return RESTAPIClient.Call<Employee[]>(path + "GetAllEmployeesExcept", new { sessionID=ApplicationClient.SessionID, employeeID = employeeID });
        }
        static public int[] GetPaidSalaryDocs(int empID, PayrollPeriod period)
        {
            //   return server.GetPaidSalaryDocs(ApplicationClient.SessionID, empID, period);
            return RESTAPIClient.Call<int[]>(path + "GetPaidSalaryDocs", new { sessionID=ApplicationClient.SessionID, empID= empID, period = period });
        }
        static public double GetUnclaimedSalaryBalance(int empID, PayrollPeriod period)
        {
            //  return server.GetUnclaimedSalaryBalance(ApplicationClient.SessionID, empID, period);
            return RESTAPIClient.Call<double>(path + "GetUnclaimedSalaryBalance", new {  sessionID=ApplicationClient.SessionID, empID= empID, period= period });
        }
        static public double GetEmployeePaidSalaryTotal(int empID, int periodID)
        {
            // return server.GetEmployeePaidSalaryTotal(ApplicationClient.SessionID, empID, periodID);
            return RESTAPIClient.Call<double>(path + "GetEmployeePaidSalaryTotal", new {   sessionID=ApplicationClient.SessionID, empID = empID , periodID = periodID });
        }
        static public UnclaimedSalaryDocument[] GetEmployeePaidSalaries(int empID, int periodID)
        {
            // return server.GetEmployeePaidSalaries(ApplicationClient.SessionID, empID, periodID);
            return RESTAPIClient.Call<UnclaimedSalaryDocument[]>(path + "GetEmployeePaidSalaries", new {  sessionID=ApplicationClient.SessionID, empID= empID, periodID= periodID });
        }
        static public BatchError GenerateAndPostPayroll(int[] employees, int periodID, DateTime date)
        {
            //return server.GenerateAndPostPayroll(ApplicationClient.SessionID, employees, periodID, date);
            return RESTAPIClient.Call<BatchError>(path + "GenerateAndPostPayroll", new {  sessionID=ApplicationClient.SessionID, employees= employees, periodID = periodID , date = date });
        }
        static public bool EmployeeExists(int id)
        {
            //return server.EmployeeExists(ApplicationClient.SessionID, id);
            return RESTAPIClient.Call<bool>(path + "EmployeeExists", new {  sessionID=ApplicationClient.SessionID, id= id });
        }
        static public void SetWorkingData(int employeeID, int periodID, Data_RegularTime regular, OverTimeData overtime, SingleValueData[] othersData, int[] singleValueFormulaID)
        {
             //server.SetWorkingData(ApplicationClient.SessionID, employeeID, periodID, regular, overtime, othersData, singleValueFormulaID);
             RESTAPIClient.Call<VoidRet>(path + "SetWorkingData", new {  sessionID=ApplicationClient.SessionID, employeeID= employeeID, periodID= periodID, regular= regular, overtime = overtime , othersData = othersData , singleValueFormulaID = singleValueFormulaID });
        }
        static public double GetGroupPayrollUnclaimed(int periodID, int taxCenter)
        {
            // return server.GetGroupPayrollUnclaimed(ApplicationClient.SessionID, periodID, taxCenter);
            return RESTAPIClient.Call<double>(path + "GetGroupPayrollUnclaimed", new { sessionID=ApplicationClient.SessionID, periodID= periodID, taxCenter = taxCenter });
        }
        static public double GetGroupPayrollTotalSalaryPaid(int periodID, int taxCenter)
        {
            // return server.GetGroupPayrollTotalSalaryPaid(ApplicationClient.SessionID, periodID, taxCenter);
            return RESTAPIClient.Call<double>(path + "GetGroupPayrollTotalSalaryPaid", new {  sessionID=ApplicationClient.SessionID, periodID= periodID, taxCenter= taxCenter });
        }
        static public void closeLoan(int loanDocumentID)
        {
            //server.closeLoan(ApplicationClient.SessionID, loanDocumentID);
            RESTAPIClient.Call<VoidRet>(path + "closeLoan", new {  sessionID=ApplicationClient.SessionID, loanDocumentID= loanDocumentID });
        }
    }
}
