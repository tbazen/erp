using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Payroll;
using INTAPS.ClientServer.Client;
using INTAPS.Payroll.Client;
using System.Reflection;

namespace BIZNET.iERP.Client
{

    public partial class iERPTransactionClient
    {
        static Dictionary<int, FixedAssetRuleAttribute> _fixedAssetRuleAttributes;
        static Dictionary<int, IFixedAssetRuleClient> _fixedAssetRuleClients;

        public static void loadFixedAssetRules()
        {

            _fixedAssetRuleAttributes = new Dictionary<int, FixedAssetRuleAttribute>();

            _fixedAssetRuleClients = new Dictionary<int, IFixedAssetRuleClient>();
            string assemblies = System.Configuration.ConfigurationManager.AppSettings["fixedAssetRuleAssemblies"];
            if (assemblies == null)
                return;
            foreach (string an in assemblies.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                try
                {
                    Assembly a = System.Reflection.Assembly.Load(an);
                    int count = 0;
                    foreach (Type t in a.GetTypes())
                    {
                        object[] atr = t.GetCustomAttributes(typeof(FixedAssetRuleClientAttribute), true);
                        if (atr == null || atr.Length == 0)
                            continue;
                        if (atr.Length > 1)
                        {
                            Console.WriteLine("Multiple job FixedAssetRuleClientAttribute found for type "+t);
                            continue;
                        }

                        FixedAssetRuleClientAttribute fatr = atr[0] as FixedAssetRuleClientAttribute;
                        ConstructorInfo ci = t.GetConstructor(new Type[] { });
                        if (ci == null)
                        {
                            Console.WriteLine("Constructor not found for type ");
                            continue;
                        }
                        try
                        {
                            IFixedAssetRuleClient o = ci.Invoke(new object[] { }) as IFixedAssetRuleClient;
                            if (o == null)
                                if (ci == null)
                                {
                                    Console.WriteLine("Fixed asset rule client " + t + " doesn't implement IFixedAssetRuleClient");
                                    continue;
                                }
                            if (_fixedAssetRuleClients.ContainsKey(fatr.id))
                            {
                                Console.WriteLine("Fixed asset rule client " + t + " is using an ID that is already used " + fatr.id);
                                continue;
                            }

                            //FixedAssetRuleAttribute serveratr = server.getFixedAssetRuleAttribute(ApplicationClient.SessionID, fatr.id);
                            FixedAssetRuleAttribute serveratr =RESTAPIClient.Call<BIZNET.iERP.FixedAssetRuleAttribute>(path + "getFixedAssetRuleAttribute", new {  sessionID=ApplicationClient.SessionID, typeID= fatr.id });
                            if (serveratr == null)
                            {
                                Console.WriteLine("Fixed asset rule client " + fatr.id+" is not defined on the server side");
                                continue;

                            }
                            _fixedAssetRuleAttributes.Add(fatr.id, serveratr);
                            _fixedAssetRuleClients.Add(fatr.id, o);
                            count++;
                        }
                        catch (Exception tex)
                        {
                            Console.WriteLine("Error trying to instantiate client rule handler " + t + "\n" + tex.Message);
                        }
                    }
                    if (count > 0)
                    {
                        Console.WriteLine(string.Format("{0} job rule client handler(s) loaded from {1}", count, an));
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error trying to load client rule handler assembly " + an + "\n" + ex.Message);
                }

            }
        }

        public static IFixedAssetRuleClient getFixedAssetRuleClient(int typeID)
        {
            if (_fixedAssetRuleClients.ContainsKey(typeID))
                return _fixedAssetRuleClients[typeID];
            return null;
        }
        public static Dictionary<int, FixedAssetRuleAttribute> getFixedAssetRuleAttributes
        {
            get
            {
                return _fixedAssetRuleAttributes;
            }
        }

    }
}