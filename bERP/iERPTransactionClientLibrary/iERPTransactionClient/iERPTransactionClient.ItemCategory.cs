using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Payroll;
using INTAPS.ClientServer.Client;

namespace BIZNET.iERP.Client
{
    public partial class iERPTransactionClient
    {
        static public ItemCategory GetItemCategory(int ID)
        {
            //return server.GetItemCategory(ApplicationClient.SessionID, ID);
            return RESTAPIClient.Call<ItemCategory>(path + "GetItemCategory2", new {  sessionID=ApplicationClient.SessionID, ID = ID });
        }
        static public ItemCategory[] GetItemCategories(int PID, ItemType itemType)
        {
            //return server.GetItemCategories(ApplicationClient.SessionID, PID,itemType);
            return RESTAPIClient.Call<ItemCategory[]>(path + "GetItemCategories", new {  sessionID=ApplicationClient.SessionID, PID = PID , itemType = itemType });
        }
        static public ItemCategory[] GetItemCategories(int PID)
        {
            //return server.GetItemCategories(ApplicationClient.SessionID, PID);
            return RESTAPIClient.Call<ItemCategory[]>(path + "GetItemCategories2", new {  sessionID=ApplicationClient.SessionID, PID= PID });
        }
        static public TransactionItems[] GetItemsInCategory(int categID)
        {
            //return server.GetItemsInCategory(ApplicationClient.SessionID, categID);
            return RESTAPIClient.Call<TransactionItems[]>(path + "GetItemsInCategory", new {  sessionID=ApplicationClient.SessionID, categID= categID });
        }
        static public ItemCategory[] GetAllItemCategories()
        {
            //return server.GetAllItemCategories(ApplicationClient.SessionID);
            return RESTAPIClient.Call<ItemCategory[]>(path + "GetAllItemCategories", new {  sessionID=ApplicationClient.SessionID, });
        }

        static public void DeleteItemCategory(int ID)
        {
            //server.DeleteItemCategory(ApplicationClient.SessionID, ID);
            RESTAPIClient.Call<VoidRet>(path + "DeleteItemCategory", new {  sessionID=ApplicationClient.SessionID, ID = ID });
        }

        static public int RegisterItemCategory(ItemCategory category)
        {
            // return server.RegisterItemCategory(ApplicationClient.SessionID,category);
            return RESTAPIClient.Call<int>(path + "RegisterItemCategory", new { sessionID=ApplicationClient.SessionID, category= category });
        }
    }
}
