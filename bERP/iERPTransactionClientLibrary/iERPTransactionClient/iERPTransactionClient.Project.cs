using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Payroll;
using INTAPS.ClientServer.Client;

namespace BIZNET.iERP.Client
{
    public partial class iERPTransactionClient
    {
        #region Project Methods
        public static string RegisterProject(Project project, bool logData)
        {
            //return server.RegisterProject(ApplicationClient.SessionID, project, logData);
            return RESTAPIClient.Call<string>(path + "RegisterProject", new {  sessionID=ApplicationClient.SessionID, project = project , logData = logData });
        }
        public static Project GetProjectInfo(string code)
        {
            // return server.GetProjectInfo(ApplicationClient.SessionID, code);
            return RESTAPIClient.Call<Project>(path + "GetProjectInfo", new {  sessionID=ApplicationClient.SessionID, code= code });
        }
        public static Project GetProjectInfo(int projectCostCenterID)
        {
            // return server.GetProjectInfo(ApplicationClient.SessionID, projectCostCenterID);
            return RESTAPIClient.Call<Project>(path + "GetProjectInfo2", new {  sessionID=ApplicationClient.SessionID, projectCostCenterID= projectCostCenterID });
        }
        public static Project[] GetAllProjects()
        {
            //return server.GetAllProjects(ApplicationClient.SessionID);
            return RESTAPIClient.Call<Project[]>(path + "GetAllProjects", new {   sessionID=ApplicationClient.SessionID});
        }
        public static void DeleteProject(string code)
        {
            //server.DeleteProject(ApplicationClient.SessionID, code);
            RESTAPIClient.Call<VoidRet>(path + "DeleteProject", new {  sessionID=ApplicationClient.SessionID });
        }
        public static void ActivateProject(string code)
        {
            //server.ActivateProject(ApplicationClient.SessionID, code);
            RESTAPIClient.Call<VoidRet>(path + "ActivateProject", new {  sessionID=ApplicationClient.SessionID, code= code });
        }
        public static void DeactivateProject(string code)
        {
           // server.DeactivateProject(ApplicationClient.SessionID, code);
           RESTAPIClient.Call<VoidRet>(path + "DeactivateProject", new {  sessionID=ApplicationClient.SessionID, code= code });
        }
        public static int CreateProjectDivision(int projectCostCenterID, string divisionName)
        {
            // return server.CreateProjectDivision(ApplicationClient.SessionID, projectCostCenterID, divisionName);
            return RESTAPIClient.Call<int>(path + "CreateProjectDivision", new {  sessionID=ApplicationClient.SessionID, projectCostCenterID= projectCostCenterID, divisionName= divisionName });
        }
        public static int CreateProjectMachinaryAndVehicle(int projectCostCenterID, string name)
        {
            // return server.CreateProjectMachinaryAndVehicle(ApplicationClient.SessionID, projectCostCenterID, name);
            return RESTAPIClient.Call<int>(path + "CreateProjectMachinaryAndVehicle", new {  sessionID=ApplicationClient.SessionID, projectCostCenterID= projectCostCenterID, name= name });
        }
        public static void AddItemAccountsToProject(TransactionItems[] items, string projectCode)
        {
            //server.AddItemAccountsToProject(ApplicationClient.SessionID, items, projectCode);
            RESTAPIClient.Call<VoidRet>(path + "AddItemAccountsToProject", new {  sessionID=ApplicationClient.SessionID, items= items, projectCode= projectCode });
        }
        public static void AddEmployeeAccountsToProject(Employee[] employees, string projectCode, int divisionCostCenterID)
        {
            //server.AddEmployeeAccountsToProject(ApplicationClient.SessionID, employees, projectCode, divisionCostCenterID);
            RESTAPIClient.Call<VoidRet>(path + "AddEmployeeAccountsToProject", new {  sessionID=ApplicationClient.SessionID, employees= employees, projectCode= projectCode, divisionCostCenterID= divisionCostCenterID });
        }
        public static void AddDivisionToProject(string divisionName, string projectCode)
        {
            //server.AddDivisionToProject(ApplicationClient.SessionID, divisionName, projectCode);
            RESTAPIClient.Call<VoidRet>(path + "AddDivisionToProject", new {   sessionID=ApplicationClient.SessionID, divisionName = divisionName, projectCode = projectCode });
        }
        public static void AddMachinaryAndVehicleToProject(string name, string projectCode)
        {
           // server.AddMachinaryAndVehicleToProject(ApplicationClient.SessionID, name, projectCode);
           RESTAPIClient.Call<VoidRet>(path + "AddMachinaryAndVehicleToProject", new {  sessionID=ApplicationClient.SessionID, name= name, projectCode = projectCode });
        }
        public static string AddStoreToProject(StoreInfo store, string projectCode)
        {
            // return server.AddStoreToProject(ApplicationClient.SessionID, store, projectCode);
            return RESTAPIClient.Call<string>(path + "AddStoreToProject", new {  sessionID=ApplicationClient.SessionID , store = store , projectCode = projectCode });
        }
        public static string AddCashAccountToProject(CashAccount cashAccount, string projectCode, int divisionCostCenterID)
        {
            //  return server.AddCashAccountToProject(ApplicationClient.SessionID, cashAccount, projectCode, divisionCostCenterID);
            return RESTAPIClient.Call<string>(path + "AddCashAccountToProject", new {   sessionID=ApplicationClient.SessionID, cashAccount = cashAccount, projectCode= projectCode, divisionCostCenterID= divisionCostCenterID });
        }
        public static int AddBankAccountToProject(BankAccountInfo bankAccount, string projectCode, int divisionCostCenterID)
        {
            //return server.AddBankAccountToProject(ApplicationClient.SessionID, bankAccount, projectCode,divisionCostCenterID);
            return RESTAPIClient.Call<int>(path + "AddBankAccountToProject", new {  sessionID=ApplicationClient.SessionID, bankAccount= bankAccount , projectCode = projectCode , divisionCostCenterID = divisionCostCenterID });
        }
        public static void RemoveEmployeeFromProject(int employeeID, string projectCode)
        {
          //  server.RemoveEmployeeFromProject(ApplicationClient.SessionID, employeeID, projectCode);
          RESTAPIClient.Call<VoidRet>(path + "RemoveEmployeeFromProject", new { sessionID=ApplicationClient.SessionID, employeeID= employeeID, projectCode= projectCode });
        }
        public static void RemoveDivisionFromProject(int divisionCostCenterID, string projectCode)
        {
          //  server.RemoveDivisionFromProject(ApplicationClient.SessionID, divisionCostCenterID, projectCode);
          RESTAPIClient.Call<VoidRet>(path + "RemoveDivisionFromProject", new { sessionID=ApplicationClient.SessionID, divisionCostCenterID= divisionCostCenterID, projectCode= projectCode });
        }
        public static void RemoveMachinaryAndVehicleFromProject(int mvCostCenterID, string projectCode)
        {
            //server.RemoveMachinaryAndVehicleFromProject(ApplicationClient.SessionID, mvCostCenterID, projectCode);
            RESTAPIClient.Call<VoidRet>(path + "RemoveMachinaryAndVehicleFromProject", new {  sessionID=ApplicationClient.SessionID, mvCostCenterID= mvCostCenterID, projectCode= projectCode });
        }
        public static void RemoveStoreFromProject(int storeCostCenterID, string storeCode, string projectCode)
        {
           // server.RemoveStoreFromProject(ApplicationClient.SessionID, storeCostCenterID, storeCode,projectCode);
           RESTAPIClient.Call<VoidRet>(path + "RemoveStoreFromProject", new {  sessionID=ApplicationClient.SessionID, storeCostCenterID= storeCostCenterID, storeCode= storeCode, projectCode= projectCode });
        }
        public static void RemoveItemFromProject(string itemCode, string projectCode)
        {
           // server.RemoveItemFromProject(ApplicationClient.SessionID, itemCode, projectCode);
           RESTAPIClient.Call<VoidRet>(path + "RemoveItemFromProject", new { sessionID=ApplicationClient.SessionID, itemCode= itemCode, projectCode= projectCode });
        }
        public static void RemoveCashAccountFromProject(int csAccountID, string code, string projectCode)
        {
             //server.RemoveCashAccountFromProject(ApplicationClient.SessionID, csAccountID, code,projectCode);
             RESTAPIClient.Call<VoidRet>(path + "RemoveCashAccountFromProject", new {  sessionID=ApplicationClient.SessionID, csAccountID= csAccountID, code= code, projectCode= projectCode });
        }
        public static void RemoveBankAccountFromProject(int csAccountID, string projectCode)
        {
            //server.RemoveBankAccountFromProject(ApplicationClient.SessionID, csAccountID, projectCode);
            RESTAPIClient.Call<VoidRet>(path + "RemoveBankAccountFromProject", new {  sessionID=ApplicationClient.SessionID, csAccountID= csAccountID, projectCode= projectCode });
        }
        #endregion

    }
}
