using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Payroll;
using INTAPS.ClientServer.Client;

namespace BIZNET.iERP.Client
{
    public partial class iERPTransactionClient
    {
        #region Store Methods
        public static string RegisterStore(StoreInfo store, int costCenterParentID)
        {
            // return server.RegisterStore(ApplicationClient.SessionID, store,costCenterParentID);
            return RESTAPIClient.Call<string>(path + "RegisterStore", new {  sessionID=ApplicationClient.SessionID, store= store, costCenterParentID= costCenterParentID });
        }
        public static StoreInfo GetStoreInfo(string code)
        {
            //return server.GetStoreInfo(ApplicationClient.SessionID, code);
            return RESTAPIClient.Call<StoreInfo>(path + "GetStoreInfo", new {   sessionID=ApplicationClient.SessionID, code = code });
        }
        public static StoreInfo[] GetAllStores()
        {
            // return server.GetAllStores(ApplicationClient.SessionID);
            return RESTAPIClient.Call<StoreInfo[]>(path + "GetAllStores", new {  sessionID=ApplicationClient.SessionID });
        }
        public static void DeleteStore(string code)
        {
           // server.DeleteStore(ApplicationClient.SessionID, code);
           RESTAPIClient.Call<VoidRet>(path + "DeleteStore", new {  sessionID=ApplicationClient.SessionID, code= code });
        }
        public static void ActivateStore(string code)
        {
            //server.ActivateStore(ApplicationClient.SessionID, code);
            RESTAPIClient.Call<VoidRet>(path + "ActivateStore", new {  sessionID=ApplicationClient.SessionID, code= code });
        }
        public static void DeactivateStore(string code)
        {
           // server.DeactivateStore(ApplicationClient.SessionID, code);
           RESTAPIClient.Call<VoidRet>(path + "DeactivateStore", new {  sessionID=ApplicationClient.SessionID, code = code });
        }
        #endregion

    }
}
