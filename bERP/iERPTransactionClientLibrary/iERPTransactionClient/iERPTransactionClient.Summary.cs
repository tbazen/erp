using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Payroll;
using System.Data;
using INTAPS.ClientServer.Client;

namespace BIZNET.iERP.Client
{
    public partial class iERPTransactionClient
    {
        public class renderSummaryTableOut
        {
            public int maxLevel;
            public string _ret;
        }
        static public string renderSummaryTable(string summarySettingField, int costCenterID, DateTime time1, DateTime time2, out int maxLevel)
        {
           // return server.renderSummaryTable(ApplicationClient.SessionID, summarySettingField, costCenterID, time1, time2, out maxLevel);
           var _ret=RESTAPIClient.Call<renderSummaryTableOut>(path + "renderSummaryTable", new {   sessionID=ApplicationClient.SessionID, summarySettingField = summarySettingField , costCenterID = costCenterID , time1 = time1 , time2 = time2 });
            maxLevel = _ret.maxLevel;
            return _ret._ret;
        }

        static public string renderLedgerTable(LedgerParameters pars)
        {
            //   return server.renderLedgerTable(ApplicationClient.SessionID, pars);
            return RESTAPIClient.Call<string>(path + "renderLedgerTable", new {  sessionID=ApplicationClient.SessionID, pars = pars });
        }

        static public TradeRelation GetRelation(string code)
        {
            // return server.GetRelation(ApplicationClient.SessionID, code);
            return RESTAPIClient.Call<TradeRelation>(path + "GetRelation", new { sessionID=ApplicationClient.SessionID, code= code });
        }

        static public Country[] GetCountries()
        {
            //return server.GetCountries(ApplicationClient.SessionID);
            return RESTAPIClient.Call<Country[]>(path + "GetCountries", new { sessionID=ApplicationClient.SessionID });
        }
        static public Country GetCountryById(short id)
        {
            //return server.GetCountryById(ApplicationClient.SessionID,id);
            return RESTAPIClient.Call<Country>(path + "GetCountryById", new {  sessionID=ApplicationClient.SessionID, id= id });
        }
        static public string RegisterRelation(TradeRelation relation)
        {
            //  return server.RegisterRelation(ApplicationClient.SessionID, relation);
            return RESTAPIClient.Call<string>(path + "RegisterRelation", new {  sessionID=ApplicationClient.SessionID, relation= relation });
        }

        static public double GetDepreciation(int costCenteriD, string itemCode, DateTime dateTime)
        {
            //return server.GetDepreciation(ApplicationClient.SessionID, costCenteriD, itemCode, dateTime);
            return RESTAPIClient.Call<double>(path + "GetDepreciation", new {   sessionID=ApplicationClient.SessionID, costCenteriD = costCenteriD , itemCode = itemCode , dateTime = dateTime });
        }
        public class GetDepreciation2Out
        {
            public double rate; public double year;
            public double _ret;
        }
        static public double GetDepreciation(int costCenteriD, string itemCode, DateTime dateTime, out double rate, out double year)
        {
            //  return server.GetDepreciation(ApplicationClient.SessionID, costCenteriD, itemCode, dateTime, out rate, out year);
                var _ret=RESTAPIClient.Call<GetDepreciation2Out>(path + "GetDepreciation2", new {  sessionID=ApplicationClient.SessionID, costCenteriD= costCenteriD, itemCode= itemCode, dateTime= dateTime });
            rate = _ret.rate;
            year = _ret.year;
            return _ret._ret;
        }

        static public FixedAssetDepreciationDocument previewDepreciation(DateTime time)
        {
            //  return server.previewDepreciation(ApplicationClient.SessionID, time);
            return RESTAPIClient.Call<FixedAssetDepreciationDocument>(path + "previewDepreciation", new {  sessionID=ApplicationClient.SessionID, time= time });
        }

        static public int getDepreciationDocument(DateTime time)
        {
            //return server.getDepreciationDocument(ApplicationClient.SessionID, time);
            return RESTAPIClient.Call<int>(path + "getDepreciationDocument", new {  sessionID=ApplicationClient.SessionID, time= time });
        }

        static public TaxRates getTaxRates()
        {
            //return server.getTaxRates(ApplicationClient.SessionID);
            return RESTAPIClient.Call<TaxRates>(path + "getTaxRates", new {  sessionID=ApplicationClient.SessionID });
        }

        static public TransactionItems GetTransactionItems(int accountID)
        {
            //return server.GetTransactionItems(ApplicationClient.SessionID, accountID);
            return RESTAPIClient.Call<TransactionItems>(path + "GetTransactionItems2", new {  sessionID=ApplicationClient.SessionID, accountID= accountID });
        }

        static public BankReconciliationDocument getLastBankReconciliation(int bankID, DateTime date)
        {
            // return server.getLastBankReconciliation(ApplicationClient.SessionID, bankID, date);
            return RESTAPIClient.Call<BankReconciliationDocument>(path + "getLastBankReconciliation", new { sessionID=ApplicationClient.SessionID, bankID= bankID, date= date });
        }

        static public void ActivateRelation(string code)
        {
            //server.ActivateRelation(ApplicationClient.SessionID, code);
            RESTAPIClient.Call<VoidRet>(path + "ActivateRelation", new {  sessionID=ApplicationClient.SessionID, code= code });
        }

        static public void DeactivateRelation(string code)
        {
            //server.DeactivateRelation(ApplicationClient.SessionID, code);
            RESTAPIClient.Call<VoidRet>(path + "DeactivateRelation", new { sessionID=ApplicationClient.SessionID, code= code });
        }

        static public void DeleteRelation(string code)
        {
          //  server.DeleteRelation(ApplicationClient.SessionID, code);
          RESTAPIClient.Call<VoidRet>(path + "DeleteRelation", new {  sessionID=ApplicationClient.SessionID, code= code });
        }

        static public string renderInventoryTable(InventoryGeneratorParameters pars)
        {
            //return server.renderInventoryTable(ApplicationClient.SessionID, pars);
            return RESTAPIClient.Call<string>(path + "renderInventoryTable", new {  sessionID=ApplicationClient.SessionID, pars = pars });
        }

        static public string renderCostCenterProfile(int costCenterID, DateTime time1, DateTime time2)
        {
            //return server.renderCostCenterProfile(ApplicationClient.SessionID, costCenterID, time1, time2);
            return RESTAPIClient.Call<string>(path + "renderCostCenterProfile", new { sessionID=ApplicationClient.SessionID, costCenterID= costCenterID, time1= time1, time2= time2 });
        }

    }
}