using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Payroll;
using INTAPS.ClientServer.Client;

namespace BIZNET.iERP.Client
{
    public partial class iERPTransactionClient
    {
        #region TradeRelation Methods
        public static string RegisterSupplier(TradeRelation supplier)
        {
            //return server.RegisterSupplier(ApplicationClient.SessionID, supplier);
            return RESTAPIClient.Call<string>(path + "RegisterSupplier", new {  sessionID=ApplicationClient.SessionID, supplier= supplier });
        }
        public static TradeRelation GetSupplier(string code)
        {
            //   return server.GetSupplier(ApplicationClient.SessionID, code);
            return RESTAPIClient.Call<TradeRelation>(path + "GetSupplier", new {  sessionID=ApplicationClient.SessionID, code= code });
        }
        public static TradeRelation[] GetAllSuppliers()
        {
            //return server.GetAllSuppliers(ApplicationClient.SessionID);
            return RESTAPIClient.Call<TradeRelation[]>(path + "GetAllSuppliers", new {  sessionID=ApplicationClient.SessionID});
        }
        public class SearchSuppliersOut
        {
            public int NoOfRecords;
            public TradeRelation[] _ret;
        }
        public static TradeRelation[] SearchSuppliers(int index, int NoOfPages, object[] criteria, string[] column, out int NoOfRecords)
        {
           // return server.SearchSuppliers(ApplicationClient.SessionID, index, NoOfPages, criteria, column, out NoOfRecords);
           var _ret=RESTAPIClient.Call<SearchSuppliersOut>(path + "SearchSuppliers", new {  sessionID=ApplicationClient.SessionID, index= index, NoOfPages= NoOfPages, criteria= criteria, column= column });
            NoOfRecords = _ret.NoOfRecords;
            return _ret._ret;
        }
        public static bool IsSupplierInvolvedInTransactions(string supplierCode)
        {
            //return server.IsSupplierInvolvedInTransactions(ApplicationClient.SessionID, supplierCode);
            return RESTAPIClient.Call<bool>(path + "IsSupplierInvolvedInTransactions", new {  sessionID=ApplicationClient.SessionID, supplierCode = supplierCode });
        }
        #endregion

    }
}
