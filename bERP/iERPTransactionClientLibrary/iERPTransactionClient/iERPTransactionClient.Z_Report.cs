using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Payroll;
using INTAPS.ClientServer.Client;

namespace BIZNET.iERP.Client
{
    public partial class iERPTransactionClient
    {
        public static ZReportDocument[] GetZReport(AccountingPeriod period)
        {
            // return server.GetZReport(ApplicationClient.SessionID, period);
            return RESTAPIClient.Call<ZReportDocument[]>(path + "GetZReport", new { sessionID=ApplicationClient.SessionID, period= period });
        }
        public class GetSalesTaxableAndNonTaxableOut
        {
            public double nonTaxableAmount; public double tax;
            public double _ret;
        }
        public static double GetSalesTaxableAndNonTaxable(DateTime date,out double nonTaxableAmount,out double tax)
        {
            //return server.GetSalesTaxableAndNonTaxable(ApplicationClient.SessionID, date, out nonTaxableAmount,out tax);
            var _ret=RESTAPIClient.Call<GetSalesTaxableAndNonTaxableOut>(path + "GetSalesTaxableAndNonTaxable", new {  sessionID=ApplicationClient.SessionID, date= date });
            nonTaxableAmount = _ret.nonTaxableAmount;
            tax = _ret.tax;
            return _ret._ret;
        }
    }
}