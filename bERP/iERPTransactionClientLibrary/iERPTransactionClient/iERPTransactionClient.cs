using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Payroll;
using INTAPS.ClientServer.Client;
using INTAPS.Payroll.Client;
using System.Drawing;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Client
{

    public partial class iERPTransactionClient
    {
        //public static IiERPTransactionService server;
        static String path;

        //static INTAPS.ClientServer.ISecurityService securityServer;
        public static bool promptAttachments;
        
        public static void Connect(string url)
        {
            path = url + "/api/erp/bnfinance/";
            //server = (IiERPTransactionService)System.Runtime.Remoting.RemotingServices.Connect(typeof(IiERPTransactionService), url + "/iERPServer");
            //securityServer = (INTAPS.ClientServer.ISecurityService)System.Runtime.Remoting.RemotingServices.Connect(typeof(IiERPTransactionService), url + "/SecurityServer");
            loadFixedAssetRules();
            promptAttachments ="true".Equals( System.Configuration.ConfigurationManager.AppSettings["promptAttachments"]);
        }
        public static void changePassword(string newPassword)
        {
            //securityServer.ChangePassword(ApplicationClient.SessionID, newPassword);
            SecurityClient.ChangePassword(newPassword);

        }
        public static int mainCostCenterID
        {
            get
            {
                return Convert.ToInt32(GetSystemParamter("mainCostCenterID"));
            }
        }
        public static object[] GetSystemParameters(string[] names)
        {
            return ApplicationClient.GetSystemParameters(path,names);
            
        }
        public static object GetSystemParamter(string name)
        {
            return ApplicationClient.GetSystemParameters(path, new string[] { name })[0];
            
        }
        public static void SetSystemParameters(string[] fields, object[] vals)
        {
            ApplicationClient.SetSystemParameters(path, fields, vals); 
            
        }
        public static void SetSystemParameter(string field, object val)
        {
            ApplicationClient.SetSystemParameters(path, new String[] { field }, new object[] { val });
        }
        public static int GetEmployeeAccountID(Employee employee, int parentAccount)
        {
            foreach (int ac in employee.accounts)
            {
                Account acnt = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(ac);
                if (acnt.PID == parentAccount)
                {
                    return ac;
                }
            }
            return -1;
        }
        public static Account GetEmployeeAccount(Employee employee, int parentAccount)
        {
            foreach (int ac in employee.accounts)
            {
                Account acnt = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(ac);
                if (acnt.PID == parentAccount)
                {
                    return acnt;
                }
            }
            return null;
        }
        public static bool IsBankAccountCreatedUnderProject(int[] bankAccounts, int projectCostCenterID)
        {
            foreach (int bank in bankAccounts)
            {
                int costCenterID = AccountingClient.GetCostCenterAccount(bank).costCenterID;
                if (costCenterID == projectCostCenterID)
                    return true;
            }
            return false;
        }
        public static bool IsCashAccountCreatedUnderProject(string[] cashAccounts, int projectCostCenterID)
        {
            foreach (string cash in cashAccounts)
            {
                int cashAccount = iERPTransactionClient.GetCashAccount(cash).csAccountID;
                int costCenterID = AccountingClient.GetCostCenterAccount(cashAccount).costCenterID;
                if (costCenterID == projectCostCenterID)
                    return true;
            }
            return false;
        }
        public static bool IsEmployeeCreatedUnderProject(int[] employees, int projectCostCenterID)
        {
            try
            {
                foreach (int emp in employees)
                {
                    Employee employee = PayrollClient.GetEmployee(emp);
                    int costCenterID = -1;
                    if (employee != null)
                        costCenterID = employee.costCenterID;
                    else
                    {
                        Project project = iERPTransactionClient.GetProjectInfo(projectCostCenterID);
                        if (project.projectData.employees.Contains(emp))
                        {
                            project.projectData.employees.Remove(emp);
                            iERPTransactionClient.RegisterProject(project, false);
                        }
                    }

                    if (costCenterID == projectCostCenterID)
                        return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static MeasureUnit[] GetMeasureUnits()
        {
            //return server.GetMeasureUnits(ApplicationClient.SessionID);
            return RESTAPIClient.Call<MeasureUnit[]>(path + "GetMeasureUnits", new { sessionID= ApplicationClient.SessionID });
        }
        public static MeasureUnit GetMeasureUnit(int ID)
        {
            //return server.GetMeasureUnit(ApplicationClient.SessionID, ID);
            return RESTAPIClient.Call<MeasureUnit>(path + "GetMeasureUnit", new { sessionID= ApplicationClient.SessionID, ID= ID });
        }
        public static int RegisterMeasureUnit(MeasureUnit measureUnit)
        {
            // return server.RegisterMeasureUnit(ApplicationClient.SessionID, measureUnit);
            return RESTAPIClient.Call<int>(path + "RegisterMeasureUnit", new { sessionID= ApplicationClient.SessionID, measureUnit= measureUnit });
        }
        static public int CountReferedDocuments(DocumentReferenceType type, string refNumber)
        {
            //return server.CountReferedDocuments(ApplicationClient.SessionID, type, refNumber);
            return RESTAPIClient.Call<int>(path + "CountReferedDocuments", new { sessionID= ApplicationClient.SessionID, type= type, refNumber= refNumber });
        }

        static public bERPDocumentReference[] GetReferedDocuments(DocumentReferenceType type, string refNumber)
        {
            // return server.GetReferedDocuments(ApplicationClient.SessionID, type, refNumber);
            return RESTAPIClient.Call<bERPDocumentReference[]>(path + "GetReferedDocuments", new { sessionID= ApplicationClient.SessionID, type= type, refNumber= refNumber });
        }
        static public BIZNET.iERP.TypedDataSets.VATDeclarationData GetVATDeclaration(int periodID)
        {
            // return server.GetVATDeclaration(ApplicationClient.SessionID, periodID);
            return RESTAPIClient.Call<BIZNET.iERP.TypedDataSets.VATDeclarationData>(path + "GetVATDeclaration", new { sessionID=ApplicationClient.SessionID, periodID= periodID });
        }
        static public void GenerateYearAccountingPeriod(int year)
        {
           // server.GenerateYearAccountingPeriod(ApplicationClient.SessionID, year);
           RESTAPIClient.Call<VoidRet>(path + "GenerateYearAccountingPeriod", new { sessionID=ApplicationClient.SessionID, year= year }); 
        }

        public static bERPClientDocumentHandler GetDocumentHandler(int documentTypeID)
        {
            return AccountingClient.GetDocumentHandler(documentTypeID) as bERPClientDocumentHandler;
        }
        public static TransactionItems GetTransactionItems(string code)
        {
            // return server.GetTransactionItems(ApplicationClient.SessionID, code);
            return RESTAPIClient.Call<TransactionItems>(path + "GetTransactionItems", new { sessionID=ApplicationClient.SessionID, code= code });
        }
        public class SearchTransactionItemsOut
        {
            public int NoOfRecords;
            public TransactionItems[] _ret;
        }
        public static TransactionItems[] SearchTransactionItems(int index, int pageSize, object[] criteria, string[] column, out int NoOfRecords)
        {
            // return server.SearchTransactionItems(ApplicationClient.SessionID, index, pageSize, criteria, column, out NoOfRecords);
            var _ret = RESTAPIClient.Call<SearchTransactionItemsOut>(path + "SearchTransactionItems", new { sessionID=ApplicationClient.SessionID, index= index, pageSize= pageSize, criteria= criteria, column= column });
            NoOfRecords = _ret.NoOfRecords;
            return _ret._ret;
        }
        public static string RegisterTransactionItem(TransactionItems item, int[] costCenterID)
        {
            // return server.RegisterTransactionItem(ApplicationClient.SessionID, item, costCenterID);
            return RESTAPIClient.Call<string>(path + "RegisterTransactionItem", new { sessionID=ApplicationClient.SessionID, item= item, costCenterID= costCenterID });
        }
        public static void DeleteTransactionItem(string code)
        {
            //server.DeleteTransactionItem(ApplicationClient.SessionID, code);
            RESTAPIClient.Call<VoidRet>(path + "DeleteTransactionItem", new { sessionID=ApplicationClient.SessionID, code= code });
        }
        public static void ActivateTransactionItem(string itemCode)
        {
           // server.ActivateTransactionItem(ApplicationClient.SessionID, itemCode);
           RESTAPIClient.Call<VoidRet>(path + "ActivateTransactionItem", new { sessionID=ApplicationClient.SessionID, itemCode= itemCode });
        }
        public static void DeactivateTransactionItem(string itemCode)
        {
            //server.DeactivateTransactionItem(ApplicationClient.SessionID, itemCode);
            RESTAPIClient.Call<VoidRet>(path + "DeactivateTransactionItem", new { sessionID=ApplicationClient.SessionID, itemCode =itemCode });
        }

        public static bool isBERPClientFileUpdated(string fileName, byte[] clientMD5)
        {
            //return server.isBERPClientFileUpdated(ApplicationClient.SessionID, fileName, clientMD5);
            return RESTAPIClient.Call<bool>(path + "isBERPClientFileUpdated", new { sessionID= ApplicationClient.SessionID, fileName= fileName, clientMD5= clientMD5 });
        }

        public static byte[] getERPClientLatestFile(string fileName)
        {
            //return server.getERPClientLatestFile(ApplicationClient.SessionID, fileName);
            return RESTAPIClient.Call<byte[]>(path + "getERPClientLatestFile", new { sessionID=ApplicationClient.SessionID, fileName= fileName });
        }

        public static string[] getAllClientFiles()
        {
            // return server.getAllClientFiles(ApplicationClient.SessionID);
            return RESTAPIClient.Call<string[]>(path + "getAllClientFiles", new { sessionID=ApplicationClient.SessionID });
        }

        public static INTAPS.Accounting.Account GetEmployeeAccount(int parentAccountID, int employeeID)
        {
            //return server.GetEmployeeAccount(ApplicationClient.SessionID, parentAccountID, employeeID);
            return RESTAPIClient.Call<INTAPS.Accounting.Account>(path + "GetEmployeeAccount", new { sessionID=ApplicationClient.SessionID, parentAccountID= parentAccountID, employeeID= employeeID });
        }
        public class GetPrimaryJournalOut
        {
            public int NRecord;
            public string _ret;
        }
        public static string getPrimaryJournal(JournalFilter filter, int pageIndex, int pageSize, out int NRecord)
        {
            //string ret = server.getPrimaryJournal(ApplicationClient.SessionID, filter, pageIndex, pageSize, out NRecord);
            var _ret =RESTAPIClient.Call<GetPrimaryJournalOut>(path + "getPrimaryJournal", new { sessionID=ApplicationClient.SessionID, filter= filter, pageIndex= pageIndex, pageSize= pageSize });
            string ret = _ret._ret;
            ret = ret.Replace("src='", "src='" + INTAPS.UI.HTML.HTMLBuilder.GetAppHTMLRoot());
            NRecord = _ret.NRecord;
            return ret;
        }

        public static string getDocumentListHtmlByTypedReference(int typeID, string reference)
        {
            //  return server.getDocumentListHtmlByTypedReference(ApplicationClient.SessionID, typeID, reference);
            return RESTAPIClient.Call<string>(path + "getDocumentListHtmlByTypedReference", new { sessionID=ApplicationClient.SessionID, typeID= typeID, reference= reference });
        }

        public static string getDocumentEntriesHTML(int docID)
        {
            //return server.getDocumentEntriesHTML(ApplicationClient.SessionID, docID);
            return RESTAPIClient.Call<string>(path + "getDocumentEntriesHTML", new { sessionID=ApplicationClient.SessionID, docID= docID });
        }

        public static string getTypedReferenceEntriesHTML(INTAPS.Accounting.DocumentTypedReference tref)
        {
            //return server.getTypedReferenceEntriesHTML(ApplicationClient.SessionID, tref);
            return RESTAPIClient.Call<string>(path + "getTypedReferenceEntriesHTML", new { sessionID=ApplicationClient.SessionID, tref= tref });
        }

        public static BIZNET.iERP.Budget getBudget(int budgetID)
        {
            //return server.getBudget(ApplicationClient.SessionID, budgetID);
            return RESTAPIClient.Call<BIZNET.iERP.Budget>(path + "getBudget", new { sessionID=ApplicationClient.SessionID, budgetID= budgetID });
        }

        public static BIZNET.iERP.Budget getBudget(int costCenterID, System.DateTime target, bool exact)
        {
            //return server.getBudget(ApplicationClient.SessionID, costCenterID, target, exact);
            return RESTAPIClient.Call<BIZNET.iERP.Budget>(path + "getBudget2", new { sessionID=ApplicationClient.SessionID, costCenterID= costCenterID, target= target, exact= exact });
        }

        public static BIZNET.iERP.Budget[] getAllBudgets(int costCenterID)
        {
            //return server.getAllBudgets(ApplicationClient.SessionID, costCenterID);
            return RESTAPIClient.Call<BIZNET.iERP.Budget[]>(path + "getAllBudgets", new { sessionID=ApplicationClient.SessionID, costCenterID= costCenterID });
        }

        public static void updateBudget(BIZNET.iERP.Budget budget, BIZNET.iERP.BudgetEntry[] entries)
        {
            //server.updateBudget(ApplicationClient.SessionID, budget, entries);
            RESTAPIClient.Call<VoidRet>(path + "updateBudget", new { sessionID=ApplicationClient.SessionID, budget= budget, entries= entries });
        }

        public static void createBudget(BIZNET.iERP.Budget budget, BIZNET.iERP.BudgetEntry[] entries)
        {
            //server.createBudget(ApplicationClient.SessionID, budget, entries);
            RESTAPIClient.Call<VoidRet>(path + "createBudget", new { sessionID=ApplicationClient.SessionID, budget= budget, entries= entries });
        }

        public static void deleteBudget(int budgetID)
        {
            //server.deleteBudget(ApplicationClient.SessionID, budgetID);
            RESTAPIClient.Call<VoidRet>(path + "deleteBudget", new { sessionID=ApplicationClient.SessionID, budgetID = budgetID });
        }

        public static BIZNET.iERP.BudgetEntry[] getBudgetEntries(int budgetID)
        {
            //return server.getBudgetEntries(ApplicationClient.SessionID, budgetID);
            return RESTAPIClient.Call<BIZNET.iERP.BudgetEntry[]>(path + "getBudgetEntries", new { sessionID=ApplicationClient.SessionID, budgetID= budgetID });
        }

        public static string getBudgetReport(BIZNET.iERP.BudgetFilter filter)
        {
            //return server.getBudgetReport(ApplicationClient.SessionID, filter);
            return RESTAPIClient.Call<string>(path + "getBudgetReport", new { sessionID=ApplicationClient.SessionID, filter=filter });
        }

        public static BIZNET.iERP.ClosedRange[] getBookClosings()
        {
            //return server.getBookClosings(ApplicationClient.SessionID);
            return RESTAPIClient.Call<BIZNET.iERP.ClosedRange[]>(path + "getBookClosings", new { sessionID= ApplicationClient.SessionID });
        }

        public static void closeBookForTransaction(BIZNET.iERP.ClosedRange range)
        {
            //server.closeBookForTransaction(ApplicationClient.SessionID, range);
            RESTAPIClient.Call<VoidRet>(path + "closeBookForTransaction", new { sessionID= ApplicationClient.SessionID, range= range });
        }

        public static void updateBookClosingForTransaction(BIZNET.iERP.ClosedRange range)
        {
           // server.updateBookClosingForTransaction(ApplicationClient.SessionID, range);
           RESTAPIClient.Call<VoidRet>(path + "updateBookClosingForTransaction", new { sessionID= ApplicationClient.SessionID, range= range });
        }

        public static void openBookForTransaction()
        {
           // server.openBookForTransaction(ApplicationClient.SessionID);
           RESTAPIClient.Call<VoidRet>(path + "openBookForTransaction", new { sessionID= ApplicationClient.SessionID });
        }

        public static System.DateTime getTransactionsClosedUpto()
        {
            // return server.getTransactionsClosedUpto(ApplicationClient.SessionID);
            return RESTAPIClient.Call<System.DateTime>(path + "getTransactionsClosedUpto", new { sessionID= ApplicationClient.SessionID });
        }

        public static bool AllowNegativeTransactionPost()
        {
            //  return server.AllowNegativeTransactionPost(ApplicationClient.SessionID);
            return RESTAPIClient.Call<bool>(path + "AllowNegativeTransactionPost", new { sessionID= ApplicationClient.SessionID });
        }


        public static void createTradeRelationCategory(BIZNET.iERP.TradeRelationCategory cat)
        {
            //server.createTradeRelationCategory(ApplicationClient.SessionID, cat);
            RESTAPIClient.Call<VoidRet>(path + "createTradeRelationCategory", new { sessionID= ApplicationClient.SessionID, cat= cat });
        }

        public static void updateTradeRelationCategory(BIZNET.iERP.TradeRelationCategory cat)
        {
            //server.updateTradeRelationCategory(ApplicationClient.SessionID, cat);
            RESTAPIClient.Call<VoidRet>(path + "updateTradeRelationCategory", new { sessionID= ApplicationClient.SessionID, cat= cat });
        }

        public static void deleteTradeRelationCategory(string tradeRelationCategoryCode)
        {
            //server.deleteTradeRelationCategory(ApplicationClient.SessionID, tradeRelationCategoryCode);
            RESTAPIClient.Call<VoidRet>(path + "deleteTradeRelationCategory", new { sessionID= ApplicationClient.SessionID, tradeRelationCategoryCode= tradeRelationCategoryCode });
        }

        public static BIZNET.iERP.TradeRelation[] getTradeRelationsByCategory(string parentCategoryCode)
        {
            //return server.getTradeRelationsByCategory(ApplicationClient.SessionID, parentCategoryCode);
            return RESTAPIClient.Call<BIZNET.iERP.TradeRelation[]>(path + "getTradeRelationsByCategory", new { sessionID= ApplicationClient.SessionID, parentCategoryCode= parentCategoryCode });
        }

        public static BIZNET.iERP.TradeRelationCategory[] getCategories(string parentCategoryCode)
        {
            //return server.getCategories(ApplicationClient.SessionID, parentCategoryCode);
            return RESTAPIClient.Call<BIZNET.iERP.TradeRelationCategory[]>(path + "getCategories", new { sessionID= ApplicationClient.SessionID, parentCategoryCode = parentCategoryCode });
        }
        public class SearchTradeRelationOut
        {
            public int NoOfRecords;
            public BIZNET.iERP.TradeRelation[] _ret;
        }
        public static BIZNET.iERP.TradeRelation[] searchTradeRelation(string categoryCode, BIZNET.iERP.TradeRelationType[] types, int index, int pageSize, object[] criteria, string[] column, out int NoOfRecords)
        {
            //return server.searchTradeRelation(ApplicationClient.SessionID, categoryCode, types, index, pageSize, criteria, column, out NoOfRecords);
            var _ret = RESTAPIClient.Call<SearchTradeRelationOut>(path + "searchTradeRelation", new { sessionID= ApplicationClient.SessionID, categoryCode= categoryCode, types= types, index= index, pageSize= pageSize, criteria= criteria, column= column });
            NoOfRecords = _ret.NoOfRecords;
            return _ret._ret;
        }

        public static BIZNET.iERP.TradeRelationCategory getTradeRelationCategory(string code)
        {
            //return server.getTradeRelationCategory(ApplicationClient.SessionID, code);
            return RESTAPIClient.Call<BIZNET.iERP.TradeRelationCategory>(path + "getTradeRelationCategory", new { sessionID= ApplicationClient.SessionID, code= code });
        }

        public static BIZNET.iERP.ItemCategory GetItemCategory(string Code)
        {
            // return server.GetItemCategory(ApplicationClient.SessionID, Code);
            return RESTAPIClient.Call<BIZNET.iERP.ItemCategory>(path + "GetItemCategory", new { sessionID= ApplicationClient.SessionID, Code= Code });
        }
        public static string generateDetaileCashFlowReport(DateTime date1, DateTime date2, DetailedCashFlowReportOption options)
        {
            //return server.generateDetaileCashFlowReport(ApplicationClient.SessionID, date1, date2, options);
            return RESTAPIClient.Call<string>(path + "generateDetaileCashFlowReport", new { sessionID= ApplicationClient.SessionID, date1= date1 , date2 = date2,options = options });
        }

        public static string getTrialBalance<AgregateType, DetailType>(System.Collections.Generic.List<int> expandedAccount, List<int> rootAccounts, AgregateType agregateTo, System.DateTime date, bool twoColumns)
            where AgregateType : AccountBase, new()
            where DetailType : AccountBase, new()
        {
            // return server.getTrialBalance<AgregateType, DetailType>(ApplicationClient.SessionID, expandedAccount, rootAccounts, agregateTo, date, twoColumns);
            return RESTAPIClient.Call<string>(path + "getTrialBalance", new { sessionID= ApplicationClient.SessionID, expandedAccount= expandedAccount, rootAccounts= rootAccounts, agregateTo= new BinObject(agregateTo), date= date, twoColumns= twoColumns, AgregateType=typeof(AgregateType).Name, DetailType=typeof(DetailType).Name });
        }

        public static BIZNET.iERP.PaymentInstrumentType[] getAllPaymentInstrumentTypes()
        {
            //return server.getAllPaymentInstrumentTypes(ApplicationClient.SessionID);
            return RESTAPIClient.Call<BIZNET.iERP.PaymentInstrumentType[]>(path + "getAllPaymentInstrumentTypes", new { sessionID=ApplicationClient.SessionID });
        }

        public static BIZNET.iERP.PaymentInstrumentType getPaymentInstrumentType(string itemCode)
        {
            //return server.getPaymentInstrumentType(ApplicationClient.SessionID, itemCode);
            return RESTAPIClient.Call<BIZNET.iERP.PaymentInstrumentType>(path + "getPaymentInstrumentType", new { sessionID= ApplicationClient.SessionID, itemCode= itemCode });
        }

        public static BIZNET.iERP.BankInfo getBankInfo(int bankID)
        {
            //return server.getBankInfo(ApplicationClient.SessionID, bankID);
            return RESTAPIClient.Call<BIZNET.iERP.BankInfo>(path + "getBankInfo", new { sessionID= ApplicationClient.SessionID, bankID= bankID });
        }

        public static BIZNET.iERP.BankBranchInfo getBankBranchInfo(int branchID)
        {
            //return server.getBankBranchInfo(ApplicationClient.SessionID, branchID);
            return RESTAPIClient.Call<BIZNET.iERP.BankBranchInfo>(path + "getBankBranchInfo", new { sessionID=ApplicationClient.SessionID, branchID= branchID });
        }

        public static BIZNET.iERP.BankInfo[] getAllBanks()
        {
            //return server.getAllBanks(ApplicationClient.SessionID);
            return RESTAPIClient.Call<BIZNET.iERP.BankInfo[]>(path + "getAllBanks", new { sessionID= ApplicationClient.SessionID });
        }

        public static BIZNET.iERP.BankBranchInfo[] getAllBranchsOfBank(int bankID)
        {
            // return server.getAllBranchsOfBank(ApplicationClient.SessionID, bankID);
            return RESTAPIClient.Call<BIZNET.iERP.BankBranchInfo[]>(path + "getAllBranchsOfBank", new { sessionID=ApplicationClient.SessionID, bankID= bankID });

        }

        public static void savePaymentInstrumentType(BIZNET.iERP.PaymentInstrumentType type)
        {
          //  server.savePaymentInstrumentType(ApplicationClient.SessionID, type);
          RESTAPIClient.Call<VoidRet>(path + "savePaymentInstrumentType", new { sessionID= ApplicationClient.SessionID, type= type });
        }

        public static void deletePaymentInstrumentType(string typeID)
        {
            //server.deletePaymentInstrumentType(ApplicationClient.SessionID, typeID);
            RESTAPIClient.Call<VoidRet>(path + "deletePaymentInstrumentType", new { sessionID=ApplicationClient.SessionID, typeID= typeID });
        }

        public static int saveBankInfo(BIZNET.iERP.BankInfo bank)
        {
            //  return server.saveBankInfo(ApplicationClient.SessionID, bank);
            return RESTAPIClient.Call<int>(path + "saveBankInfo", new { sessionID=ApplicationClient.SessionID, bank= bank });
        }

        public static int saveBankBranchInfo(BIZNET.iERP.BankBranchInfo branch)
        {
            // return server.saveBankBranchInfo(ApplicationClient.SessionID, branch);
            return RESTAPIClient.Call<int>(path + "saveBankBranchInfo", new { sessionID=ApplicationClient.SessionID, branch= branch });
        }

        public static void deleteBankInfo(int bankID)
        {
            //server.deleteBankInfo(ApplicationClient.SessionID, bankID);
            RESTAPIClient.Call<VoidRet>(path + "deleteBankInfo", new { sessionID=ApplicationClient.SessionID, bankID= bankID });
        }

        public static void deleteBankBranchInfo(int branchID)
        {
            //server.deleteBankBranchInfo(ApplicationClient.SessionID, branchID);
            RESTAPIClient.Call<VoidRet>(path + "deleteBankBranchInfo", new { sessionID= ApplicationClient.SessionID, branchID= branchID });
        }

        public static BIZNET.iERP.FixedAssetRuleAttribute getFixedAssetRuleAttribute(int typeID)
        {
            //  return server.getFixedAssetRuleAttribute(ApplicationClient.SessionID, typeID);
            return RESTAPIClient.Call<BIZNET.iERP.FixedAssetRuleAttribute>(path + "getFixedAssetRuleAttribute", new { sessionID=ApplicationClient.SessionID, typeID= typeID });
        }

        public static BIZNET.iERP.FixedAssetRuleAttribute[] getAllFixedAssetRuleAttributes()
        {
            // return server.getAllFixedAssetRuleAttributes(ApplicationClient.SessionID);
            return RESTAPIClient.Call<BIZNET.iERP.FixedAssetRuleAttribute[]>(path + "getAllFixedAssetRuleAttributes", new { sessionID=ApplicationClient.SessionID });
        }

        public static void saveFixedAssetRuleSetting(int typeID, object data)
        {
            //server.saveFixedAssetRuleSetting(ApplicationClient.SessionID, typeID, data);
            RESTAPIClient.Call<VoidRet>(path + "saveFixedAssetRuleSetting", new { sessionID=ApplicationClient.SessionID, typeID= typeID, data= data });
        }

        public static BIZNET.iERP.CashAccountCategory[] getChildCashCategories(int categoryID)
        {
            // return server.getChildCashCategories(ApplicationClient.SessionID, categoryID);
            return RESTAPIClient.Call<BIZNET.iERP.CashAccountCategory[]>(path + "getChildCashCategories", new { sessionID=ApplicationClient.SessionID, categoryID= categoryID });
        }

        public static BIZNET.iERP.CashAccountCategory getCashCategory(int categoryID)
        {
            //return server.getCashCategory(ApplicationClient.SessionID, categoryID);
            return RESTAPIClient.Call<BIZNET.iERP.CashAccountCategory>(path + "getCashCategory", new { sessionID= ApplicationClient.SessionID, categoryID= categoryID });
        }

        public static int createCashCategory(BIZNET.iERP.CashAccountCategory cat)
        {
            //return server.createCashCategory(ApplicationClient.SessionID, cat);
            return RESTAPIClient.Call<int>(path + "createCashCategory", new { sessionID= ApplicationClient.SessionID, cat= cat });
        }

        public static BIZNET.iERP.CashAccount[] getCashAccountsByCategory(int categoryID)
        {
            // return server.getCashAccountsByCategory(ApplicationClient.SessionID, categoryID);
            return RESTAPIClient.Call<BIZNET.iERP.CashAccount[]>(path + "getCashAccountsByCategory", new { sessionID=ApplicationClient.SessionID, categoryID= categoryID });
        }

        public static void updateCashCategory(BIZNET.iERP.CashAccountCategory cat)
        {
            //server.updateCashCategory(ApplicationClient.SessionID, cat);
            RESTAPIClient.Call<VoidRet>(path + "updateCashCategory", new { sessionID=ApplicationClient.SessionID,cat=cat });
        }

        public static void deleteCashCategory(int catID)
        {
            //server.deleteCashCategory(ApplicationClient.SessionID, catID);
            RESTAPIClient.Call<VoidRet>(path + "deleteCashCategory", new { sessionID= ApplicationClient.SessionID, catID= catID });
        }

        public static void recalculateInventoryWeightedAverage()
        {
            //server.recalculateInventoryWeightedAverage(ApplicationClient.SessionID);
            RESTAPIClient.Call<VoidRet>(path + "recalculateInventoryWeightedAverage", new { sessionID=ApplicationClient.SessionID });
        }
        public static void SaveStockLevelConfiguration(StockLevelConfiguration[] config)
        {
           // server.SaveStockLevelConfiguration(ApplicationClient.SessionID, config);
           RESTAPIClient.Call<VoidRet>(path + "SaveStockLevelConfiguration", new { sessionID=ApplicationClient.SessionID, config= config });
        }
        public static StockLevelConfiguration[] getStockLevelConfiguration()
        {
            //return server.getStockLevelConfiguration(ApplicationClient.SessionID);
            return RESTAPIClient.Call<StockLevelConfiguration[]>(path + "getStockLevelConfiguration", new { sessionID=ApplicationClient.SessionID });
        }

        public static void setConvesionFactors(string itemScope, int categoryScope, BIZNET.iERP.UnitCoversionFactor[] factors)
        {
          //  server.setConvesionFactors(ApplicationClient.SessionID, itemScope, categoryScope, factors);
          RESTAPIClient.Call<VoidRet>(path + "setConvesionFactors", new { sessionID=ApplicationClient.SessionID, itemScope= itemScope , categoryScope = categoryScope , factors = factors });
        }


        public static BIZNET.iERP.UnitCoversionFactor[] getConversionFactors(string itemScope, int categoryScope)
        {
            //return server.getConversionFactors(ApplicationClient.SessionID, itemScope, categoryScope);
            return RESTAPIClient.Call<BIZNET.iERP.UnitCoversionFactor[]>(path + "getConversionFactors", new { sessionID=ApplicationClient.SessionID, itemScope= itemScope, categoryScope= categoryScope });
        }

        public static BIZNET.iERP.UnitConvertor getItemUnitConvertor(string itemCode)
        {
            // return server.getItemUnitConvertor(ApplicationClient.SessionID, itemCode);
            return RESTAPIClient.Call<BIZNET.iERP.UnitConvertor>(path + "getItemUnitConvertor", new { sessionID=ApplicationClient.SessionID, itemCode = itemCode });
        }

        public static bool isDocumentBudgetSet(int documentID)
        {
            //return server.isDocumentBudgetSet(ApplicationClient.SessionID, documentID);
            return RESTAPIClient.Call<bool>(path + "isDocumentBudgetSet", new { sessionID=ApplicationClient.SessionID, documentID= documentID });
        }
        public class GetUnbudgetedDocumentsOut
        {
            public int N;
            public INTAPS.Accounting.AccountDocument[] _ret;
        }
        public static INTAPS.Accounting.AccountDocument[] getUnbudgetedDocuments(INTAPS.Accounting.DocumentSearchPar pars, int index, int pageSize, out int N)
        {
            //return server.getUnbudgetedDocuments(ApplicationClient.SessionID, pars, index, pageSize, out N);
            var _ret = RESTAPIClient.Call<GetUnbudgetedDocumentsOut>(path + "getUnbudgetedDocuments", new { sessionID=ApplicationClient.SessionID, pars= pars, index= index, pageSize= pageSize });
            N = _ret.N;
            return _ret._ret;
        }
        public static PropertyType[] getAllPropertTypes()
        {
            // return server.getAllPropertTypes(ApplicationClient.SessionID);
            return RESTAPIClient.Call<BIZNET.iERP.PropertyType[]>(path + "getAllPropertTypes", new { sessionID=ApplicationClient.SessionID });
        }
        public class GetPropertyByCodeOut
        {
            public object extraData;
            public BIZNET.iERP.Property _ret;
        }
        public static BIZNET.iERP.Property getPropertyByCode(string propertyCode, out object extraData)
        {
            // return server.getPropertyByCode(ApplicationClient.SessionID, propertyCode, out extraData);
            var _ret = RESTAPIClient.Call<GetPropertyByCodeOut>(path + "getPropertyByCode", new { sessionID=ApplicationClient.SessionID, propertyCode= propertyCode });
            extraData = _ret.extraData;
            return _ret._ret;
        }
        public class GetPropertyOut
        {
            public object extraData;
            public BIZNET.iERP.Property _ret;
        }
        public static BIZNET.iERP.Property getProperty(int propertyID, out object extraData)
        {
            //return server.getProperty(ApplicationClient.SessionID, propertyID, out extraData);
            var _ret = RESTAPIClient.Call<GetPropertyOut>(path + "getProperty", new { sessionID=ApplicationClient.SessionID, propertyID= propertyID });
            extraData = _ret.extraData;
            return _ret._ret;
        }

        public static int createProperty(BIZNET.iERP.Property prop, object extraData)
        {
            //return server.createProperty(ApplicationClient.SessionID, prop, extraData);
            return RESTAPIClient.Call<int>(path + "createProperty", new { sessionID=ApplicationClient.SessionID, prop= prop, extraData= extraData });
        }

        public static void updateProperty(BIZNET.iERP.Property prop, object extraData)
        {
            //server.updateProperty(ApplicationClient.SessionID, prop, extraData);
            RESTAPIClient.Call<VoidRet>(path + "updateProperty", new { sessionID=ApplicationClient.SessionID, prop= prop, extraData= extraData });
        }
        public class GetPropertyItemCategory2Out
        {
            public int N;
            public BIZNET.iERP.PropertyItemCategory[] _ret;
        }
        public static BIZNET.iERP.PropertyItemCategory[] getPropertyItemCategory(int parentCategoryID, int pageIndex, int pageSize, out int N)
        {
            //return server.getPropertyItemCategory(ApplicationClient.SessionID, parentCategoryID, pageIndex, pageSize, out N);
            var _ret =RESTAPIClient.Call<GetPropertyItemCategory2Out>(path + "getPropertyItemCategory2", new { sessionID=ApplicationClient.SessionID, parentCategoryID= parentCategoryID, pageIndex=pageIndex, pageSize= pageSize });
            N = _ret.N;
            return _ret._ret;
        }
        public class GetPropertyByItemCodeOut
        {
            public object extraData;
            public BIZNET.iERP.Property _ret;
        }
        public static BIZNET.iERP.Property getPropertyByItemCode(string propertyCode, out object extraData)
        {
            //return server.getPropertyByItemCode(ApplicationClient.SessionID, propertyCode, out extraData);
            var _ret = RESTAPIClient.Call<GetPropertyByItemCodeOut>(path + "getPropertyByItemCode", new { sessionID=ApplicationClient.SessionID, propertyCode= propertyCode });
            extraData = _ret.extraData;
            return _ret._ret;
        }

        public static void deleteProperty(int propertyID)
        {
            //server.deleteProperty(ApplicationClient.SessionID, propertyID);
            RESTAPIClient.Call<VoidRet>(path + "deleteProperty", new { sessionID=ApplicationClient.SessionID, propertyID= propertyID });
        }

        public static BIZNET.iERP.PropertyItemCategory getPropertyItemCategory(string itemCode)
        {
            //return server.getPropertyItemCategory(ApplicationClient.SessionID, itemCode);
            return RESTAPIClient.Call<BIZNET.iERP.PropertyItemCategory>(path + "getPropertyItemCategory", new { sessionID=ApplicationClient.SessionID, itemCode= itemCode });
        }
        public class GetPropertiesByParentItemCodeOut
        {
            public int N;
            public BIZNET.iERP.Property[] _ret;
        }
        public static BIZNET.iERP.Property[] getPropertiesByParentItemCode(string parentItemCode, int pageIndex, int pageSize, out int N)
        {
            // return server.getPropertiesByParentItemCode(ApplicationClient.SessionID, parentItemCode, pageIndex, pageSize, out N);
            var _ret =RESTAPIClient.Call<GetPropertiesByParentItemCodeOut>(path + "getPropertiesByParentItemCode", new { sessionID=ApplicationClient.SessionID, parentItemCode= parentItemCode, pageIndex= pageIndex, pageSize= pageSize });
            N = _ret.N;
            return _ret._ret;
        }
        public static string getTradeRelationTransactions(string relationCode, long ticksFrom, long ticksTo, int[] docTypes)
        {
            // return server.getTradeRelationTransactions(ApplicationClient.SessionID, relationCode, ticksFrom, ticksTo, docTypes);
            return RESTAPIClient.Call<string>(path + "getTradeRelationTransactions", new { sessionID=ApplicationClient.SessionID, relationCode= relationCode, ticksFrom=ticksFrom, ticksTo=ticksTo, docTypes= docTypes });
        }

        public static BIZNET.iERP.MeasureUnit GetMeasureUnit(string name)
        {
            // return server.GetMeasureUnit(ApplicationClient.SessionID, name);
            return RESTAPIClient.Call<BIZNET.iERP.MeasureUnit>(path + "GetMeasureUnit2", new { sessionID=ApplicationClient.SessionID, name= name });
        }

        public static double convertUnit(string itemCode, double quantity, int fromUnit, int toUnit)
        {
            //return server.convertUnit(ApplicationClient.SessionID, itemCode, quantity, fromUnit, toUnit);
            return RESTAPIClient.Call<double>(path + "convertUnit", new { sessionID=ApplicationClient.SessionID, itemCode= itemCode, quantity= quantity, fromUnit= fromUnit, toUnit= toUnit });
        }

        public static BIZNET.iERP.PropertyType getPropertyType(int id)
        {
            //return server.getPropertyType(ApplicationClient.SessionID, id);
            return RESTAPIClient.Call<BIZNET.iERP.PropertyType>(path + "getPropertyType", new { sessionID=ApplicationClient.SessionID, id= id });
        }

        public static INTAPS.Accounting.AccountBalance[] getMaterialBalance(System.DateTime time, string itemCode, int[] itemIDs)
        {
            //   return server.getMaterialBalance(ApplicationClient.SessionID, time, itemCode, itemIDs);
            return RESTAPIClient.Call<INTAPS.Accounting.AccountBalance[]>(path + "getMaterialBalance", new { sessionID=ApplicationClient.SessionID, time= time, itemCode = itemCode , itemIDs = itemIDs });
        }
        public static INTAPS.Accounting.AccountBalance verifyAndGetPropertyQuantityBalanceBalance(System.DateTime time, string propertyCode)
        {
            // AccountBalance[] bal = server.getMaterialBalance(ApplicationClient.SessionID, time, propertyCode, new int[] { TransactionItem.MATERIAL_QUANTITY });
            AccountBalance[] bal =RESTAPIClient.Call<INTAPS.Accounting.AccountBalance[]>(path + "getMaterialBalance", new { sessionID=ApplicationClient.SessionID, time= time, itemCode= propertyCode, itemIDs = new int[] { TransactionItem.MATERIAL_QUANTITY } });
            if (bal.Length != 1 || !AccountBase.AmountEqual(bal[0].DebitBalance, 1))
            {
                throw new INTAPS.ClientServer.ServerUserMessage("Property {0}  has invalid balance. Balance found in {1} cost centers", propertyCode, bal.Length);
            }
            return bal[0];
        }
        public static void rearrangeAttachedFileItems(int fileGroupID, int[] oldOrdern, int[] newOrdern)
        {
          //  server.rearrangeAttachedFileItems(ApplicationClient.SessionID, fileGroupID, oldOrdern, newOrdern);
          RESTAPIClient.Call<VoidRet>(path + "rearrangeAttachedFileItems", new { sessionID=ApplicationClient.SessionID, fileGroupID= fileGroupID, oldOrdern= oldOrdern, newOrdern= newOrdern });
        }
        public static int replaceAttachmentFileItem(AttachedFileItem item)
        {
            //return server.replaceAttachmentFileItem(ApplicationClient.SessionID, item);
            return RESTAPIClient.Call<int>(path + "replaceAttachmentFileItem", new { sessionID=ApplicationClient.SessionID, item= item });
        }
        public static int attachFile(int documentID, AttachedFileGroup fileGroup, IEnumerable<AttachedFileItem> items)
        {
            // return server.attachFile(ApplicationClient.SessionID, documentID, fileGroup, items);
            return RESTAPIClient.Call<int>(path + "attachFile", new { sessionID=ApplicationClient.SessionID, documentID= documentID, fileGroup= fileGroup, items= items });
        }
        public static void deleteAttachmentFileItem(int attachmentFileID)
        {
            //server.deleteAttachmentFileItem(ApplicationClient.SessionID, attachmentFileID);
            RESTAPIClient.Call<VoidRet>(path + "deleteAttachmentFileItem", new { sessionID=ApplicationClient.SessionID, attachmentFileID= attachmentFileID });
        }
        public static void detachAttachmentFileGroup(int attachmentFileGroupID, int documentID)
        {
            //server.detachAttachmentFileGroup(ApplicationClient.SessionID, attachmentFileGroupID, documentID);
            RESTAPIClient.Call<VoidRet>(path + "detachAttachmentFileGroup", new { sessionID=ApplicationClient.SessionID, attachmentFileGroupID= attachmentFileGroupID , documentID = documentID });
        }
        public static void deleteAttachmentFileGroup(int attachmentFileGroupID)
        {
           // server.deleteAttachmentFileGroup(ApplicationClient.SessionID, attachmentFileGroupID);
           RESTAPIClient.Call<VoidRet>(path + "deleteAttachmentFileGroup", new { sessionID=ApplicationClient.SessionID, attachmentFileGroupID= attachmentFileGroupID });
        }
        public static AttachedFileGroup[] getAttachedFileGroupForDocument(int documentID)
        {
            //return server.getAttachedFileGroupForDocument(ApplicationClient.SessionID, documentID);
            return RESTAPIClient.Call<AttachedFileGroup[]>(path + "getAttachedFileGroupForDocument", new { sessionID=ApplicationClient.SessionID, documentID= documentID });
        }
        public static AttachedFileItem[] getAttachedFileItems(int fileGroupID)
        {
            // return server.getAttachedFileItems(ApplicationClient.SessionID, fileGroupID);
            return RESTAPIClient.Call<AttachedFileItem[]>(path + "getAttachedFileItems", new { sessionID =ApplicationClient.SessionID, fileGroupID = fileGroupID });
        }
        public static AttachedFileItem getAttachedFileItem(int attachedFileItemID)
        {
            // return server.getAttachedFileItem(ApplicationClient.SessionID, attachedFileItemID);
            return RESTAPIClient.Call<AttachedFileItem>(path + "getAttachedFileItem", new { sessionID=ApplicationClient.SessionID, attachedFileItemID= attachedFileItemID });
        }
        public static AttachedFileItemImage getAttachedFileItemImageInfo(int attachedFileItemID, AttachmentPaperFace face)
        {
            // return server.getAttachedFileItemImageInfo(ApplicationClient.SessionID, attachedFileItemID, face);
            return RESTAPIClient.Call<AttachedFileItemImage>(path + "getAttachedFileItemImageInfo", new { sessionID=ApplicationClient.SessionID, attachedFileItemID= attachedFileItemID, face= face });
        }
        public static byte[] getAttachedFileItemImageData(int attachmentFileID, AttachmentPaperFace face)
        {
            //return server.getAttachedFileItemImageData(ApplicationClient.SessionID, attachmentFileID, face);
            return RESTAPIClient.Call<byte[]>(path + "getAttachedFileItemImageData", new { sessionID=ApplicationClient.SessionID, attachmentFileID= attachmentFileID, face= face });
        }
        public static Image getAttachedFileItemImage(int attachmentFileID, AttachmentPaperFace face)
        {
            //byte[] data=server.getAttachedFileItemImageData(ApplicationClient.SessionID, attachmentFileID, face);
            byte[] data =RESTAPIClient.Call<byte[]>(path + "getAttachedFileItemImageData", new { sessionID=ApplicationClient.SessionID, attachmentFileID= attachmentFileID, face= face });
            using (System.IO.MemoryStream ms=new System.IO.MemoryStream(data))
            {
                return Image.FromStream(ms);
            }
        }
        public static byte[] getAttachedFileItemImageResized(int attachmentFileID, AttachmentPaperFace face, int width, int height)
        {
            //return server.getAttachedFileItemImageResized(ApplicationClient.SessionID, attachmentFileID, face,width,height);
            return RESTAPIClient.Call<byte[]>(path + "getAttachedFileItemImageResized", new { sessionID =ApplicationClient.SessionID, attachmentFileID = attachmentFileID , face = face , width = width , height = height });
        }
        public static AttachmentType[] getAllFileAttachmentTypes()
        {
            // return server.getAllFileAttachmentTypes(ApplicationClient.SessionID);
            return RESTAPIClient.Call<AttachmentType[]>(path + "getAllFileAttachmentTypes", new { sessionID=ApplicationClient.SessionID });
        }
        public static List<int> attachMultipeFile(int documentID, IEnumerable<AttachedFileGroup> fileGroup, IEnumerable<IEnumerable<AttachedFileItem>> items)
        {
            // return server.attachMultipeFile(ApplicationClient.SessionID,documentID, fileGroup, items);
            return RESTAPIClient.Call<List<int>>(path + "attachMultipeFile", new { sessionID=ApplicationClient.SessionID, documentID= documentID, fileGroup= fileGroup, items= items });
        }
        public static CashAccount GetStaffExpenseAdvanceCashAccount(int employeeID)
        {
            // return server.GetStaffExpenseAdvanceCashAccount(ApplicationClient.SessionID, employeeID);
            return RESTAPIClient.Call<CashAccount>(path + "GetStaffExpenseAdvanceCashAccount", new { sessionID=ApplicationClient.SessionID, employeeID= employeeID });
        }
    }
}

