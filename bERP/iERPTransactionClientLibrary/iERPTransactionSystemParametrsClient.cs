﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Client
{
    public static  class iERPTransactionSystemParametrsClient
    {
        public static int assetAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("assetAccountID");
            }
        }

        public static int assetLiabilityAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("assetLiabilityAccountID");
            }
        }
        public static int assetCapitalAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("assetCapitalAccountID");
            }
        }
        public static int assetIncomeAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("assetIncomeAccountID");
            }
        }
        public static int costOfSalesAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("costOfSalesAccountID");
            }
        }
        public static int expenseAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("expenseAccountID");
            }
        }
        public static int vehicleFuelExpenseAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("vehicleFuelExpenseAccountID");
            }
        }
        public static int vehicleSparePartExpenseAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("vehicleSparePartExpenseAccountID");
            }
        }
        public static int vehicleMaintainanceExpenseAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("vehicleMaintainanceExpenseAccountID");
            }
        }
        public static int LoanFromStaffAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("LoanFromStaffAccountID");
            }
        }
        public static int laborExpenseAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("laborExpenseAccountID");
            }
        }
        public static int laborIncomeTaxAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("laborIncomeTaxAccountID");
            }
        }
        public static int tradeReceivablesAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("tradeReceivablesAccountID");
            }
        }
        public static int tradePayablesAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("tradePayablesAccountID");
            }
        }
        public static int financeReceivablesAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("financeReceivablesAccountID");
            }
        }
        public static int financePayablesAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("financePayablesAccountID");
            }
        }
        public static int onSupplierOrderCostCenterID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("onSupplierOrderCostCenterID");
            }
        }
        public static int onCustomerOrderCostCenterID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("onCustomerOrderCostCenterID");
            }
        }
        public static int OriginalFixedAssetAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("OriginalFixedAssetAccountID");
            }
        }
        public static int FixedAssetDepreciationAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("FixedAssetDepreciationAccountID");
            }
        }
        public static int FixedAssetAccumulatedDepreciationAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("FixedAssetAccumulatedDepreciationAccountID");
            }
        }
        public static bool depreciateBookValue 
        {
            get
            {
                return (bool)iERPTransactionClient.GetSystemParamter("depreciateBookValue");
            }
        }
        public static int AttachmentNumber
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("AttachmentNumber");
            }
        }
        public static string AttachmentNumberFormat
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("AttachmentNumberFormat") as string;
            }
        }
        public static string BankAccountSuffixCodeFormat
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("BankAccountSuffixCodeFormat") as string;
            }
        }
        public static string CashAccountSuffixCodeFormat
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("CashAccountSuffixCodeFormat") as string;
            }
        }

        public static string AllowNegativeTransaction
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("AllowNegativeTransaction") as string;
            }
        }

        public static CompanyProfile companyProfile
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("companyProfile") as CompanyProfile;
            }
        }
        public static double CommonVATRate
        {
            get
            {
                return (double)iERPTransactionClient.GetSystemParamter("CommonVATRate");
            }
        }
        public static double ExportVATRate
        {
            get
            {
                return (double)iERPTransactionClient.GetSystemParamter("ExportVATRate");
            }
        }
        public static double RawHideExportVATRate
        {
            get
            {
                return (double)iERPTransactionClient.GetSystemParamter("RawHideExportVATRate");
            }
        }
        public static double GoodsTaxableRate
        {
            get
            {
                return (double)iERPTransactionClient.GetSystemParamter("GoodsTaxableRate");
            }
        }
        public static ServicesTaxable ServicesTaxable
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("ServicesTaxable") as ServicesTaxable;
            }
        }

        public static double WHGoodsNonTaxablePriceLimit
        {
            get
            {
                return (double)iERPTransactionClient.GetSystemParamter("WHGoodsNonTaxablePriceLimit");
            }
        }
        
        public static double WHGoodsTaxableRate
        {
            get
            {
                return (double)iERPTransactionClient.GetSystemParamter("WHGoodsTaxableRate");
            }
        }

        public static double WHServicesNonTaxablePriceLimit
        {
            get
            {
                return (double)iERPTransactionClient.GetSystemParamter("WHServicesNonTaxablePriceLimit");
            }
        }
        public static double WHServicesTaxableRate
        {
            get
            {
                return (double)iERPTransactionClient.GetSystemParamter("WHServicesTaxableRate");
            }
        }

        public static double UnknownTINWithHoldingRate
        {
            get
            {
                return (double)iERPTransactionClient.GetSystemParamter("UnknownTINWithHoldingRate");
            }
        }

        public static int inputVATAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("inputVATAccountID");
            }
        }
        public static int accumulatedReceivableVATAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("accumulatedReceivableVATAccountID");
            }
        }
        public static int outPutVATAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("outPutVATAccountID");
            }
        }
        public static int outputVATWithHeldAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("outputVATWithHeldAccountID");
            }
        }
        public static int paidWithHoldingTaxAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("paidWithHoldingTaxAccountID");
            }
        }
        public static int withholdingExpenseAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("withholdingExpenseAccountID");
            }
        }
        public static int collectedWithHoldingTaxAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("collectedWithHoldingTaxAccountID");
            }
        }
        public static int TOTExpenseAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("TOTExpenseAccountID");
            }
        }
        public static int collectedTOTAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("collectedTOTAccountID");
            }
        }
        public static int customDutyTaxExpenseAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("customDutyTaxExpenseAccountID");
            }
        }        
        public static int exciseTaxExpenseAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("exciseTaxExpenseAccountID");
            }
        }        
        public static int surTaxExpenseAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("surTaxExpenseAccountID");
            }
        }
        public static int taxPenalityAcountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("taxPenalityAcountID");
            }
        }
        public static int taxInterestAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("taxInterestAccountID");
            }
        }
        public static int taxExemptionsAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("taxExemptionsAccountID");
            }
        }
        public static string taxDeclarationPeriods
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("taxDeclarationPeriods") as string;
            }
        }
        public static int zReportTaxableAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("zReportTaxableAccountID");
            }
        }
        public static int zReportNonTaxableAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("zReportNonTaxableAccountID");
            }
        }
        public static int WithdrawalAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("WithdrawalAccountID");
            }
        }
        public static int GrossSalaryPayrollComponentID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("GrossSalaryPayrollComponentID");
            }
        }
        public static int GrossSalaryPayrollFormulaID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("GrossSalaryPayrollFormulaID");
            }
        }
        public static int TransportAllowancePayrollComponentID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("TransportAllowancePayrollComponentID");
            }
        }
        public static int TransportAllowancePayrollFormulaID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("TransportAllowancePayrollFormulaID");
            }
        }
        public static int TaxableTransportAllowancePayrollComponentID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("TaxableTransportAllowancePayrollComponentID");
            }
        }
        public static int TaxableTransportAllowancePayrollFormulaID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("TaxableTransportAllowancePayrollFormulaID");
            }
        }

        public static string otherBenfitsFormulaIDs
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("otherBenfitsFormulaIDs") as string;
            }
        }
        public static int IncomeTaxPayrollComponentID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("IncomeTaxPayrollComponentID");
            }
        }
        public static int IncomeTaxPayrollFormulaID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("IncomeTaxPayrollFormulaID");
            }
        }

        public static int EmployeePensionPayrollComponentID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("EmployeePensionPayrollComponentID");
            }
        }        
        public static int EmployeePensionPayrollFormulaID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("EmployeePensionPayrollFormulaID");
            }
        }

        public static int EmployerPensionPayrollComponentID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("EmployerPensionPayrollComponentID");
            }
        }
        public static int EmployerPensionPayrollFormulaID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("EmployerPensionPayrollFormulaID");
            }
        }

        public static int CostSharingPayrollComponentID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("CostSharingPayrollComponentID");
            }
        }
        public static int CostSharingPayrollFormulaID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("CostSharingPayrollFormulaID");
            }
        }

        public static int OverTimePayrollComponentID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("OverTimePayrollComponentID");
            }
        }
        public static int OverTimePayrollFormulaID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("OverTimePayrollFormulaID");
            }
        }

        public static int SingleValuePayrollComponentID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("SingleValuePayrollComponentID");
            }
        }
        public static int SingleValuePayrollFormulaID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("SingleValuePayrollFormulaID");
            }
        }

        public static int OtherBenefitsPayrollComponentID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("OtherBenefitsPayrollComponentID");
            }
        }
        public static int OtherBenefitsPayrollFormulaID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("OtherBenefitsPayrollFormulaID");
            }
        }

        public static int RegularTimePayrollComponentID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("RegularTimePayrollComponentID");
            }
        }
        public static int RegularTimePayrollFormulaID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("RegularTimePayrollFormulaID");
            }
        }
        public static int permanentBenefitComponentID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("permanentBenefitComponentID");
            }
        }



        public static SummaryInformation reportBalanceSheet1
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("assetCapitalAccountID") as SummaryInformation;
            }
        }
        public static SummaryInformation reportBalanceSheet2
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("reportBalanceSheet2") as SummaryInformation;
            }
        }
        public static SummaryInformation reportBalanceSheet3
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("reportBalanceSheet3") as SummaryInformation;
            }
        }
        public static SummaryInformation reportBalanceSheet4
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("SummaryInformation") as SummaryInformation;
            }
        }
        public static SummaryInformation reportIncomeStatement1
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("reportIncomeStatement1") as SummaryInformation;
            }
        }
        public static SummaryInformation reportIncomeStatement2
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("reportIncomeStatement2") as SummaryInformation;
            }
        }
        public static SummaryInformation reportIncomeStatement3
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("reportIncomeStatement3") as SummaryInformation;
            }
        }
        public static SummaryInformation reportIncomeStatement4
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("reportIncomeStatement4") as SummaryInformation;
            }
        }
        public static SummaryInformation reportCashFlow1
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("reportCashFlow1") as SummaryInformation;
            }
        }
        public static SummaryInformation reportCashFlow2
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("reportCashFlow2") as SummaryInformation;
            }
        }
        public static SummaryInformation reportCashFlow3
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("reportCashFlow3") as SummaryInformation;
            }
        }
        public static SummaryInformation reportCashFlow4
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("reportCashFlow4") as SummaryInformation;
            }
        }
        public static SummaryInformation reportDashBoard
        {
            get
            {
                return iERPTransactionClient.GetSystemParamter("reportDashBoard") as SummaryInformation;
            }
        }
        public static int mainBankAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("mainBankAccountID");
            }
        }
        public static int bankServiceChargeAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("bankServiceChargeAccountID");
            }
        }
        public static int bankServiceChargeByCash
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("bankServiceChargeByCash");
            }
        }
        public static int cashAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("cashAccountID");
            }
        }

        public static AccountPeriodStyle accountingPeriodStyle
        {
            get
            {
                return (AccountPeriodStyle)iERPTransactionClient.GetSystemParamter("accountingPeriodStyle");
            }
        }
        public static int inventoryIncomeAccount 
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("inventoryIncomeAccount");
            }
        }
        public static int inventoryExpenseAccount 
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("inventoryExpenseAccount");
            }
        }
        public static int materialInFlowAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("materialInFlowAccountID");
            }
        }
        public static int internalServiceReceivableAccountID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("internalServiceReceivableAccountID");
            }
        }
        public static int internalServicePayableAccountID 
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("internalServicePayableAccountID");
            }
        }
        public static int mainCostCenterID
        {
            get
            {
                return (int)iERPTransactionClient.GetSystemParamter("mainCostCenterID");
            }
        }

    }
}
