﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.Accounting;

namespace BIZNET.iERP.Server
{
    class CashTransactionUtility
    {
        public static void addCashTransactionCostItem(List<CashFlowItem> items,
            BizNetPaymentMethod paymentMethod
            , int assetAccountID
            , double serviceChargeAmount
            , ServiceChargePayer serviceChargePayer)
        {
            if (!PaymentMethodHelper.IsPaymentMethodWithServiceCharge(paymentMethod))
                return;
            if (serviceChargePayer != ServiceChargePayer.Company)
                return;
            items.Add(CashFlowItem.createByText("Bank Service Charge", serviceChargeAmount));
        }
        public static void addCashTransactionMainItem(List<CashFlowItem> items,
            BizNetPaymentMethod paymentMethod
            , int assetAccountID
            , double amount
            , double serviceChargeAmount
            , ServiceChargePayer serviceChargePayer
            , string checkNo
            ,string cpoNo
            ,string slipNo
            )
        {
            if (PaymentMethodHelper.IsPaymentMethodCredit(paymentMethod))
                return;
            double sc = 0;
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(paymentMethod)
                && serviceChargePayer == ServiceChargePayer.Company)
                sc = serviceChargeAmount;
            CashFlowItem item = CashFlowItem.createByText(assetAccountID, -1, "", -amount-sc);
            item.instrumentReference = PaymentMethodHelper.selectReference(paymentMethod, checkNo, cpoNo, slipNo);
            item.paymentMethod = paymentMethod;
            items.Add(item);
                
        }
        public static bool isTransactionInCostCenter(AccountingBDE accounting, int accountDocumentID, List<int> costCenters,bool debit)
        {
            if (costCenters == null)
                return true;
            AccountTransaction[] tran = accounting.GetTransactionsOfDocument(accountDocumentID);
            foreach (AccountTransaction t in tran)
            {
                if (debit ^ AccountBase.AmountLess(t.Amount, 0))
                {
                    if(costCenters.Contains( accounting.GetCostCenterAccountAccountID<CostCenter>(t.AccountID)))
                        return true;
                }
            }
            return false;
        }
        public static bool isCostCenterAccountInCostCenter(AccountingBDE accounting, int csaID, List<int> costCenters)
        {
            if (costCenters == null)
                return true;
            return costCenters.Contains(accounting.GetCostCenterAccountAccountID<CostCenter>(csaID));
        }
        public static bool isCostCenterAccountInCostCenter(AccountingBDE accounting, List<int> costCenters,params int [] csaIDs)
        {
            if (costCenters == null)
                return true;
            foreach (int a in csaIDs)
            {
                if (costCenters.Contains(accounting.GetCostCenterAccountAccountID<CostCenter>(a)))
                    return true;
            }
            return false;
        }
        public static bool isCostCenterAccountInCostCenter(AccountingBDE accounting, List<int> costCenters, AccountTransaction[] trans)
        {
            if (costCenters == null)
                return true;
            foreach (AccountTransaction t in trans)
            {
                int csid = accounting.GetCostCenterAccountAccountID<CostCenter>(t.AccountID);
                if (costCenters.Contains(csid))
                    return true;
            }
            return false;
        }
        public static AccountTransaction findTransactionByAccountID(AccountingBDE accountingBDE, AccountTransaction[] docTrans, int accountID)
        {
            foreach (AccountTransaction tran in docTrans)
            {
                if (accountingBDE.GetCostCenterAccountAccountID<Account>(tran.AccountID) == accountID)
                    return tran;
            }
            return null;
        }
        public static AccountTransaction findTransactionByAccountIDExtended(AccountingBDE accountingBDE, AccountTransaction[] docTrans, params int [] accountIDs)
        {
            foreach (AccountTransaction tran in docTrans)
            {
                foreach (int ac in accountIDs)
                {
                    int[] acs = INTAPS.ArrayExtension.appendToArray<int>(accountingBDE.getAllChildAccounts<Account>(ac), ac);
                    if (Array.IndexOf<int>(acs, accountingBDE.GetCostCenterAccountAccountID<Account>(tran.AccountID)) != -1)
                        return tran;
                }
            }
            return null;
        }
    }
}
