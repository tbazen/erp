﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;

namespace BIZNET.iERP.Server
{
    [BDEChangeFilter(typeof(AccountDocument))]
    class ClosingFilter : iERPChangeFilterBase, IBDEChangeFilter
    {

        bool IsBankClosed(DateTime time)
        {
            return bde.reconciliationDone(time);
        }
        bool IsFixedAssetClosed(DateTime time)
        {
            return bde.getDepreciationDocument(time) != -1;
        }
        public override bool CanCreate(int AID, object objectData, out string userMessage)
        {
            AccountDocument doc=(AccountDocument)objectData;
            DateTime closedUpto=bde.getTransactionsClosedUpto();
            if (doc.DocumentDate <= closedUpto)
            {
                userMessage = "The book is closed upto " + closedUpto;
                return false;
            }
            userMessage = null;
            return true;
        }
        public bool CanDelete(int AID, Type objectType, out string userMessage, params object[] keyValues)
        {
            AccountDocument doc = bde.Accounting.GetAccountDocument((int)keyValues[0], false);
            return CanCreate(AID, doc, out userMessage);
        }

        public bool CanUpdate(int AID, object objectData, out string userMessage)
        {
            return CanCreate(AID, objectData, out userMessage);
        }
    }
}
