﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server.FixedAssetDespreciationRule
{
    [FixedAssetRule(101,"Building Pooling",false)]
    public class FDAPoolingBuilding : FADPooling
    {
        public FDAPoolingBuilding(iERPTransactionBDE bde)
            : base(bde, 0.05)
        {

        }
    }
    [FixedAssetRule(102, "Intangible Assets Pooling", false)]
    public class FDAPoolingIntangibleAssets : FADPooling
    {
        public FDAPoolingIntangibleAssets(iERPTransactionBDE bde)
            : base(bde, 0.10)
        {

        }
    }
    [FixedAssetRule(103, "Computer Related Pooling", false)]
    public class FDAPoolingComputerRelated : FADPooling
    {
        public FDAPoolingComputerRelated(iERPTransactionBDE bde)
            : base(bde, 0.25)
        {

        }
    }
    [FixedAssetRule(104, "General Pooling", false)]
    public class FDAPoolingGeneral : FADPooling
    {
        public FDAPoolingGeneral(iERPTransactionBDE bde)
            : base(bde, 0.20)
        {

        }
    }
    public class FADPooling:IFixedAssetServerRule
    {
        iERPTransactionBDE bde;
        double rate;
        
        public FADPooling(iERPTransactionBDE bde, double rate)
        {
            this.bde = bde;
            this.rate=rate;
        }
        public double calculateDepreciation(int costCenterID, TransactionItems titem, DateTime upto, out double rate, out double year)
        {
            INTAPS.RDBMS.SQLHelper helper = bde.GetReaderHelper();
            try
            {
                AccountingPeriod yearPeriod = bde.GetAccountingPeriod("AP_Accounting_Year", upto.AddDays(-1));
                if (yearPeriod == null)
                    throw new INTAPS.ClientServer.ServerUserMessage("Accounting year not found for {1}", upto);
                
                double nYearDays = yearPeriod.toDate.Subtract(yearPeriod.fromDate).TotalDays;
                
                rate = this.rate;
                year = 0;
                
                int accumulatedDepreciationCSAID = -1;
                
                int orginalValueCSAID = -1;
                
                CostCenterAccount csa = bde.Accounting.GetCostCenterAccount(costCenterID, titem.accumulatedDepreciationAccountID);
                if (csa == null)
                    accumulatedDepreciationCSAID = -1;
                else
                    accumulatedDepreciationCSAID = csa.id;
                csa = bde.Accounting.GetCostCenterAccount(costCenterID, titem.originalFixedAssetAccountID);
                if (csa == null)
                    return 0;
                orginalValueCSAID = csa.id;

                int N;
                AccountBalance bal;
                double firstBalance = bde.Accounting.GetBalanceAsOf(orginalValueCSAID, 0,yearPeriod.fromDate).DebitBalance;
                if(bde.SysPars.depreciateBookValue)
                    firstBalance += bde.Accounting.GetBalanceAsOf(accumulatedDepreciationCSAID, 0, yearPeriod.fromDate).DebitBalance;
                
                AccountTransaction[] orginalValueLedger = 
                    INTAPS.ArrayExtension.mergeArray(new AccountTransaction[]{new AccountTransaction(){AccountID=orginalValueCSAID,ItemID=0,Amount=firstBalance,tranTicks=yearPeriod.fromDate.Ticks}},
                    bde.Accounting.GetTransactionByFilter(helper, string.Format("accountID in ({0}) and tranTicks>={1} and  tranTicks<{2} and itemID={2}", orginalValueCSAID,yearPeriod.fromDate.Ticks, upto.Ticks, TransactionItem.DEFAULT_CURRENCY), 0, -1, out N, out bal));
                DateTime time = yearPeriod.fromDate;
                double dep = 0;
                double totalAmount = 0;
                foreach (AccountTransaction t in orginalValueLedger)
                {
                    totalAmount += t.Amount;
                    double totalDays = yearPeriod.toDate.Subtract(new DateTime(t.tranTicks)).TotalDays;
                    double daysToNow = upto.Subtract(new DateTime(t.tranTicks)).TotalDays;
                    dep+= this.rate * t.Amount * daysToNow/totalDays;
                }
                dep+= bde.Accounting.GetFlow(accumulatedDepreciationCSAID, 0, yearPeriod.fromDate, upto).DebitBalance;
                rate = this.rate;
                if (AccountBase.AmountEqual(rate, 0) || AccountBase.AmountEqual(totalAmount, 0))
                    year = 1;
                else
                    year = dep / totalAmount / rate;
                
                return dep;
            }
            finally
            {
                bde.ReleaseHelper(helper);
            }
        }
        //public double calculateDepreciation(int costCenterID, TransactionItems titem, DateTime baseTime, DateTime upto,out double rate,out double year)
        //{
        //    INTAPS.RDBMS.SQLHelper helper = bde.GetReaderHelper();
        //    try
        //    {
        //        int depreciationDocumentTypeID = bde.Accounting.GetDocumentTypeByType(typeof(FixedAssetDepreciationDocument)).id;
        //        INTAPS.Ethiopic.EtGrDate etDate = INTAPS.Ethiopic.EtGrDate.ToEth(upto);
                
        //        if(upto.Date!=upto
        //            ||
        //        etDate.Day!=1&&etDate.Month!=11
        //            )
        //        {
        //            rate = this.rate;
        //            year = 0;
        //            return 0;
        //        }
                
        //        FixedAssetDepreciationDocument prev = bde.Accounting.getPreviousDocument(depreciationDocumentTypeID, upto.Ticks, true) as FixedAssetDepreciationDocument;
        //        if(prev!=null)
        //        {
        //            INTAPS.Ethiopic.EtGrDate prevDate = INTAPS.Ethiopic.EtGrDate.ToEth(prev.DocumentDate);
        //            if (prevDate.Year == etDate.Year)
        //            {
        //                rate = this.rate;
        //                year = 0;
        //                return 0;
        //            }
        //        }
        //        rate = this.rate;
        //        year = 1;
        //        int accumulatedDepreciationID = -1;
        //        int orginalValueID = -1;
        //        CostCenterAccount csa = bde.Accounting.GetCostCenterAccount(costCenterID, titem.accumulatedDepreciationAccountID);
        //        if (csa == null)
        //            accumulatedDepreciationID = -1;
        //        else
        //            accumulatedDepreciationID = csa.id;
        //        csa = bde.Accounting.GetCostCenterAccount(costCenterID, titem.originalFixedAssetAccountID);
        //        if (csa == null)
        //            return 0;
        //        orginalValueID = csa.id;

        //        double balance;
        //        //if (bde.SysPars.depreciateBookValue)
        //            balance = bde.Accounting.GetNetBalanceAsOf(orginalValueID, 0, upto)
        //            + bde.Accounting.GetNetBalanceAsOf(accumulatedDepreciationID, 0, upto);
        //        //else
        //        //    balance = bde.Accounting.GetNetBalanceAsOf(orginalValueID, 0, upto);
                
        //        return rate * balance;
        //    }
        //    finally
        //    {
        //        bde.ReleaseHelper(helper);
        //    }
        //}

        public void setConfiguration(object configuration)
        {
            throw new NotImplementedException();
        }

        public object getConfigurationData()
        {
            throw new NotImplementedException();
        }
    }
}
