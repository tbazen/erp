﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP.Server.FixedAssetDespreciationRule
{
    [FixedAssetRule(201,"Water Supply Assets 5 Years",false)]
    public class FDAStraightLine5Percent : FADStraightLine
    {
        public FDAStraightLine5Percent(iERPTransactionBDE bde)
            : base(bde, 0.20)
        {

        }
    }

    [FixedAssetRule(202, "Water Supply Assets 7 Years", false)]
    public class FDAStraightLine7Percent:FADStraightLine
    {
        public FDAStraightLine7Percent(iERPTransactionBDE bde)
            : base(bde, 0.1428571428571429)
        {

        }
    }

    [FixedAssetRule(203, "Water Supply Assets 8 Years", false)]
    public class FDAStraightLine8Percent : FADStraightLine
    {
        public FDAStraightLine8Percent(iERPTransactionBDE bde)
            : base(bde, 0.125)
        {

        }
    }

    [FixedAssetRule(204, "Water Supply Assets 10 Years", false)]
    public class FDAStraightLine10Percent : FADStraightLine
    {
        public FDAStraightLine10Percent(iERPTransactionBDE bde)
            : base(bde, 0.10)
        {

        }
    }

    [FixedAssetRule(205, "Water Supply Assets 15 Years", false)]
    public class FDAStraightLine15Percent : FADStraightLine
    {
        public FDAStraightLine15Percent(iERPTransactionBDE bde)
            : base(bde, 0.066666667)
        {

        }
    }

    [FixedAssetRule(206, "Water Supply Assets 20 Years", false)]
    public class FDAStraightLine20Percent : FADStraightLine
    {
        public FDAStraightLine20Percent(iERPTransactionBDE bde)
            : base(bde, 0.05)
        {

        }
    }
    [FixedAssetRule(207, "Water Supply Assets 25 Years", false)]
    public class FDAStraightLine25Percent : FADStraightLine
    {
        public FDAStraightLine25Percent(iERPTransactionBDE bde)
            : base(bde, 0.04)
        {

        }
    }
    [FixedAssetRule(208, "Water Supply Assets 30 Years", false)]
    public class FDAStraightLine30Percent : FADStraightLine
    {
        public FDAStraightLine30Percent(iERPTransactionBDE bde)
            : base(bde, 0.0333333333)
        {

        }
    }
    [FixedAssetRule(209, "Water Supply Assets 50 Years", false)]
    public class FDAStraightLine50Percent : FADStraightLine
    {
        public FDAStraightLine50Percent(iERPTransactionBDE bde)
            : base(bde, 0.02)
        {

        }
    }
    [FixedAssetRule(210, "Water Supply Assets 40 Years", false)]
    public class FDAStraightLine40Percent : FADStraightLine
    {
        public FDAStraightLine40Percent(iERPTransactionBDE bde)
            : base(bde, 0.025)
        {

        }
    }
}
