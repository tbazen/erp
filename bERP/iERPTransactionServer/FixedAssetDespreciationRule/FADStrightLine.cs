﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP.Server.FixedAssetDespreciationRule
{
    [FixedAssetRule(1,"Building Straight Line",false)]
    public class FDAStraightLineBuilding:FADStraightLine
    {
        public FDAStraightLineBuilding(iERPTransactionBDE bde):base(bde,0.05)
        {

        }
    }
    [FixedAssetRule(2, "Intangable Assets Straight Line", false)]
    public class FDAStraightLineIntangibleAssets : FADStraightLine
    {
        public FDAStraightLineIntangibleAssets(iERPTransactionBDE bde)
            : base(bde, 0.10)
        {

        }
    }
    [FixedAssetRule(3, "Computer Related Straight Line", false)]
    public class FDAStraightLineComputerRelated : FADStraightLine
    {
        public FDAStraightLineComputerRelated(iERPTransactionBDE bde)
            : base(bde, 0.25)
        {

        }
    }
    [FixedAssetRule(9, "General Straight Line ", false)]
    public class FDAStraightLineGeneral: FADStraightLine
    {
        public FDAStraightLineGeneral(iERPTransactionBDE bde)
            : base(bde, 0.20)
        {

        }
    }
    public class FADStraightLine:IFixedAssetServerRule
    {
        iERPTransactionBDE bde;
        double rate;
        bool ethiopianYearAligned
        {
            get
            {
                return bde.SysPars.accountingPeriodStyle != AccountPeriodStyle.GregJul1FiscalYearGregorianMonth;
            }
        }
        public FADStraightLine(iERPTransactionBDE bde, double rate)
        {
            this.bde = bde;
            this.rate=rate;
        }
        public double calculateDepreciation(int costCenterID, TransactionItems titem, DateTime upto,out double rate,out double year)
        {
            INTAPS.RDBMS.SQLHelper helper = bde.GetReaderHelper();
            try
            {
                AccountingPeriod yearPeriod=bde.GetAccountingPeriod("AP_Accounting_Year",upto);
                if (yearPeriod == null)
                    throw new INTAPS.ClientServer.ServerUserMessage("Accounting year not found for {1}", upto);
                double nDays=yearPeriod.toDate.Subtract(yearPeriod.fromDate).TotalDays;
                rate = this.rate;
                year = 0;
                int accumulatedDepreciationID = -1;
                int orginalValueID = -1;
                CostCenterAccount csa = bde.Accounting.GetCostCenterAccount(costCenterID, titem.accumulatedDepreciationAccountID);
                if (csa == null)
                    accumulatedDepreciationID = -1;
                else
                    accumulatedDepreciationID = csa.id;
                csa = bde.Accounting.GetCostCenterAccount(costCenterID, titem.originalFixedAssetAccountID);
                if (csa == null)
                    return 0;
                orginalValueID = csa.id;

                int N;
                AccountBalance bal;
                AccountTransaction[] orginalValueLedger = bde.Accounting.GetTransactionByFilter(helper, string.Format("accountID in ({0}) and tranTicks<{1} and itemID={2}", orginalValueID, upto.Ticks, TransactionItem.DEFAULT_CURRENCY), 0, -1, out N, out bal);
                AccountTransaction[] accDepValueLedger = bde.Accounting.GetTransactionByFilter(helper, string.Format("accountID in ({0}) and tranTicks<{1} and itemID={2}", accumulatedDepreciationID, upto.Ticks, TransactionItem.DEFAULT_CURRENCY), 0, -1, out N, out bal);

                if (orginalValueLedger.Length == 0)
                    return 0;
                DateTime depStart;
                if (accDepValueLedger.Length > 0)
                    depStart = accDepValueLedger[accDepValueLedger.Length - 1].execTime.AddSeconds(1);
                else
                {
                    depStart = orginalValueLedger[0].execTime.AddSeconds(1);
                }

                double total = 0;

                double balance;
                if (bde.SysPars.depreciateBookValue)
                    balance = bde.Accounting.GetNetBalanceAsOf(orginalValueID, 0, depStart)
                    + bde.Accounting.GetNetBalanceAsOf(accumulatedDepreciationID, 0, depStart);
                else
                    balance = bde.Accounting.GetNetBalanceAsOf(orginalValueID, 0, depStart);

                year = 0;
                for (int i = 0; i <= orginalValueLedger.Length; i++)
                {

                    DateTime time;
                    AccountTransaction ledger;
                    if (i < orginalValueLedger.Length)
                    {
                        ledger = orginalValueLedger[i];
                        time = ledger.execTime;
                    }
                    else
                    {
                        ledger = null;
                        time = upto;
                    }
                    if (time > depStart)
                    {
                        double days = time.Subtract(depStart).TotalDays;
                        double years = days / nDays;
                        if (AccountBase.AmountGreater(balance, 0))
                        {
                            total += rate * balance * years;
                            year += years;
                        }
                    }
                    if (i < orginalValueLedger.Length)
                    {
                        if (time > depStart)
                        {
                            depStart = time;
                            balance += ledger.Amount;
                        }
                    }
                }


                return total;
            }
            finally
            {
                bde.ReleaseHelper(helper);
            }
        }

        public void setConfiguration(object configuration)
        {
            throw new NotImplementedException();
        }

        public object getConfigurationData()
        {
            throw new NotImplementedException();
        }
    }
}
