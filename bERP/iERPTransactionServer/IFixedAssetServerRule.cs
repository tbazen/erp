﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Server
{
    public interface IFixedAssetServerRule
    {
        double calculateDepreciation(int costCenterID, TransactionItems titem, DateTime upto, out double rate, out double year);
        void setConfiguration(object configuration);
        object getConfigurationData();
    }

}
