﻿using System.Linq;
using System.Text;
using INTAPS.Evaluator;

namespace BIZNET.iERP.Server
{
    public class PCCompanyName : IFunction
    {
        iERPTransactionBDE bde;
        public PCCompanyName(iERPTransactionBDE bde)
        {
            this.bde = bde;
        }

        public EData Evaluate(EData[] Pars)
        {
            return new EData(DataType.Text, bde.SysPars.companyProfile.Name);
        }

        public string Name
        {
            get { return "bERP Company Name"; }
        }

        public int ParCount
        {
            get { return 0; }
        }

        public string Symbol
        {
            get { return "berpCompanyName"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
    }
}
