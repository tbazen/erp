﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using INTAPS.ClientServer;
using INTAPS;
namespace BIZNET.iERP.Server
{
    public interface IPropertyExtraDataHandler
    {
        Type extraDataType { get; }
        int[] propertyTypeIDs { get; }
        void saveExtraData(int AID,int propertyID,object data);
        void deleteExtraData(int AID, int propertyID);

        object loadExtraData(int propertyID);
    }
    public class SingleTablePropertyHandler<ExtraType>
        where ExtraType:class,new()
    {

        iERPTransactionBDE _bde = null;
        iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                return _bde;
            }
        }
        public Type extraDataType
        {
            get { return typeof(ExtraType); }
        }

        public void saveExtraData(int AID,int propertyID, object data)
        {
            FieldInfo fi = data.GetType().GetField("propertyID");
            if(fi!=null)
                fi.SetValue(data, propertyID);
            if (loadExtraData(propertyID) != null)
            {
                bde.WriterHelper.logDeletedData(AID, "{0}.dbo.{1}".format(bde.DBName, typeof(ExtraType).Name), "propertyID=" + propertyID);
                bde.WriterHelper.UpdateSingleTableRecord<ExtraType>(bde.DBName, data as ExtraType, new string[] { "__AID" }, new object[] { AID },"propertyID="+propertyID);
            }
            else
                bde.WriterHelper.InsertSingleTableRecord<ExtraType>(bde.DBName, data as ExtraType,new string[]{"__AID"},new object[]{AID});
        }

        public void deleteExtraData(int AID, int propertyID)
        {
            bde.WriterHelper.logDeletedData(AID, "{0}.dbo.{1}".format(bde.DBName, typeof(ExtraType).Name), "propertyID=" + propertyID);
            bde.WriterHelper.DeleteSingleTableRecrod<ExtraType>(bde.DBName, propertyID);
        }

        public object loadExtraData(int propertyID)
        {
            INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
            try
            {
                ExtraType[] ret= reader.GetSTRArrayByFilter<ExtraType>("propertyID=" + propertyID);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                _bde.ReleaseHelper(reader);
            }
        }
        
    }
    
    class PropertyExtraDataHandlers
    {
        Dictionary<Type, IPropertyExtraDataHandler> handlers = new Dictionary<Type, IPropertyExtraDataHandler>();
        public void loadPropertyHandler(Assembly a)
        {
            foreach(Type t in a.GetTypes())
            {
                IPropertyExtraDataHandler h;
                if (typeof(IPropertyExtraDataHandler).IsAssignableFrom(t) && !t.IsInterface && !t.IsAbstract)
                {
                    h = Activator.CreateInstance(t) as IPropertyExtraDataHandler;
                    handlers.Add(h.extraDataType, h);
                }
            }
        }
       
        public bool hasHandler(Type t)
        {
            if (t== null)
                return false;
            return handlers.ContainsKey(t);
        }

        internal IPropertyExtraDataHandler getHandler(Type type)
        {
            if (!hasHandler(type))
                return null;
            return handlers[type];
        }

    }
    
    
    
}
