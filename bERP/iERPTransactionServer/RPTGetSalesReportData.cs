﻿using System;
using System.Collections.Generic;
using System.Linq;
using INTAPS.Accounting;
using INTAPS.Evaluator;

namespace BIZNET.iERP.Server
{
    public class RPTGetSalesReportData : IFunction
    {
        iERPTransactionBDE bde;
        public RPTGetSalesReportData(iERPTransactionBDE bde)
        {
            this.bde = bde;
        }

        public EData Evaluate(EData[] Pars)
        {
            //where time>=date1 and time<date2
            DateTime date1 = (DateTime)Pars[0].Value;
            DateTime date2 = (DateTime)Pars[1].Value;
            
            List<List<string>> ret=new List<List<string>>();
            int documenttypeid = bde.Accounting.GetDocumentTypeByType(typeof(Sell2Document)).id;
            int N;
            AccountDocument[] salesTransactions = bde.Accounting.GetAccountDocuments(
                new DocumentSearchPar()
                {
                   date = true,
                   from = date1,
                   to =date2,
                   type = documenttypeid,
                   includeType = new int[] { documenttypeid }
                }, null, 0, -1, out N
                );
            IOrderedEnumerable<AccountDocument> salesDesending = salesTransactions.OrderBy(x=> x.DocumentDate);

            foreach (Sell2Document doc in salesDesending)
            {
                
                TradeRelation cust = bde.GetRelation(doc.relationCode);
                string mrc = bde.Accounting.getMachineryNumber(doc.assetAccountID); 
                string refNo = "";
                DocumentSerialType documentSerialType = null;
                DocumentSerial[] documentSerials = bde.Accounting.getDocumentSerials(doc.AccountDocumentID);
                
                if (documentSerials.Length > 0)
                {
                    documentSerialType = bde.Accounting.getDocumentSerialType(documentSerials[0].typeID);
                    if (documentSerialType != null)
                        refNo += documentSerialType.prefix;
                    refNo += doc.PaperRef;
                }
                else
                {
                    refNo = doc.PaperRef;
                }

                for ( int i=0;  i < doc.items.Length ; i++)
                {
                    TransactionDocumentItem item = doc.items[i];
                    if (i == 0)
                    {
                        ret.Add(new List<string>
                        {
                            cust.Name,
                            cust.Region,
                            cust.City,
                            cust.Zone,
                            "'"+cust.TIN,
                            item.name,
                            item.quantity.ToString(),
                            (item.quantity*item.unitPrice).ToString("N"),
                            mrc,
                            refNo,
                            doc.DocumentDate.ToShortDateString(),
                            ""
                        });
                    }
                    else
                    {
                        ret.Add(new List<string>
                        {
                            "",
                            "",
                            "",
                            "",
                            "",
                            item.name,
                            item.quantity.ToString(),
                            (item.quantity*item.unitPrice).ToString("N"),
                            mrc,
                            refNo,
                            doc.DocumentDate.ToShortDateString(),
                            ""
                        });
                    }
                }
            }
            return new EData(ret);
        }

        public string Name
        {
            get { return "bERP Sales Report"; }
        }

        public int ParCount
        {
            get { return 2; }
        }

        public string Symbol
        {
            get { return "getSalesReportData"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
    }
}