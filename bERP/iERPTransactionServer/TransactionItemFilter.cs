﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;

namespace BIZNET.iERP.Server
{
//    [BDEChangeFilter(typeof(Account))]
//    public class TransactionItemChangeFilter : iERPChangeFilterBase, IBDEChangeFilter
//    {
//        public override bool CanCreate(int AID, object objectData, out string userMessage)
//        {
//            userMessage = null;
//            if (base._allowAID == AID)
//                return true;
//            INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
//            try
//            {
//                int csAcountID = ((CostCenterAccount)objectData).accountID;
//                int cnt = (int)reader.ExecuteScalar(string.Format(
//                                @"Select COUNT(*) from {0}.dbo.BankAccountInfo c inner join {1}.dbo.CostCenterAccount csa
//                                    on c.mainCsAccount=csa.id or c.bankServiceChargeCsAccountID=csa.id
//                                    where csa.accountID={2}", bde.DBName, bdeAccounting.DBName, csAcountID));
//                if (cnt > 0)
//                {
//                    userMessage = "Bank account can be attached only to a single cost center.";
//                    return false;
//                }
//                return true;

//            }
//            finally
//            {
//                bde.ReleaseHelper(reader);
//            }
//        }
//        public bool CanDelete(int AID, Type objectType, out string userMessage, params object[] keyValues)
//        {
//            userMessage = null;
//            if (base._allowAID == AID)
//                return true;

//            userMessage = null;
//            if (objectType == typeof(Account))
//            {
//                INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
//                try
//                {
//                    int accountID = (int)keyValues[0];
//                    int cnt = (int)reader.ExecuteScalar(string.Format(
//                                    @"Select COUNT(*) from {0}.dbo.BankAccountInfo c inner join {1}.dbo.CostCenterAccount csa
//                                    on c.mainCsAccount=csa.id or c.bankServiceChargeCsAccountID=csa.id
//                                    where csa.accountID={2}", bde.DBName, bdeAccounting.DBName, accountID));
//                    if (cnt > 0)
//                    {
//                        userMessage = "The account can't be deleted because it is used for a bank account.";
//                        return false;
//                    }
//                    return true;

//                }
//                finally
//                {
//                    bde.ReleaseHelper(reader);
//                }
//            }
//            else if (objectType == typeof(CostCenterAccount))
//            {
//                INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
//                try
//                {
//                    int csAcountID = (int)keyValues[0];
//                    int cnt = (int)reader.ExecuteScalar(string.Format(
//                                    @"Select COUNT(*) from {0}.dbo.BankAccountInfo c inner join {1}.dbo.CostCenterAccount csa
//                                    on c.mainCsAccount=csa.id or c.bankServiceChargeCsAccountID=csa.id
//                                    where csa.id={2}", bde.DBName, bdeAccounting.DBName, csAcountID));
//                    if (cnt > 0)
//                    {
//                        userMessage = "The account can't be deleted because it is used for a bank account.";
//                        return false;
//                    }
//                    return true;

//                }
//                finally
//                {
//                    bde.ReleaseHelper(reader);
//                }
//            }
//            return true;
//        }

//        public bool CanUpdate(int AID, object objectData, out string userMessage)
//        {
//            userMessage = null;
//            if (base._allowAID == AID)
//                return true;

//            userMessage = null;
//            Type objectType = objectData.GetType();
//            if (objectType == typeof(Account))
//            {
//                INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
//                try
//                {
//                    int accountID = ((Account)objectData).id;
//                    int cnt = (int)reader.ExecuteScalar(string.Format(
//                                    @"Select COUNT(*) from {0}.dbo.BankAccountInfo c inner join {1}.dbo.CostCenterAccount csa
//                                    on c.mainCsAccount=csa.id or c.bankServiceChargeCsAccountID=csa.id
//                                    where csa.accountID={2}", bde.DBName, bdeAccounting.DBName, accountID));
//                    if (cnt > 0)
//                    {
//                        userMessage = "The account can't be updatd because it is used for a bank account.";
//                        return false;
//                    }
//                    return true;

//                }
//                finally
//                {
//                    bde.ReleaseHelper(reader);
//                }
//            }
//            else if (objectType == typeof(CostCenterAccount))
//            {
//                INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
//                try
//                {
//                    int csAcountID = ((CostCenterAccount)objectData).id;
//                    int cnt = (int)reader.ExecuteScalar(string.Format(
//                                    @"Select COUNT(*) from {0}.dbo.BankAccountInfo c inner join {1}.dbo.CostCenterAccount csa
//                                    on c.mainCsAccount=csa.id or c.bankServiceChargeCsAccountID=csa.id
//                                    where csa.id={2}", bde.DBName, bdeAccounting.DBName, csAcountID));
//                    if (cnt > 0)
//                    {
//                        userMessage = "The account can't be updated because it is used for a bank account.";
//                        return false;
//                    }
//                    return true;

//                }
//                finally
//                {
//                    bde.ReleaseHelper(reader);
//                }
//            }
//            return true;

//        }
//    }
}
