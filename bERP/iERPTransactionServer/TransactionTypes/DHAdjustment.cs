using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Collections;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public class DHAdjustment : DeleteReverseDocumentHandler,  IDocumentServerHandler
    {
        iERPTransactionBDE _bde = null;
        iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHAdjustment(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, INTAPS.Accounting.AccountDocument doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return bde.ConfigurationPermissionGranted(userSession);
        }

        public override string GetHTML(INTAPS.Accounting.AccountDocument doc)
        {
            return null;
        }

        public int Post(int AID, AccountDocument doc)
        {
            AdjustmentDocument adjustmentDoc = (AdjustmentDocument)doc;
            adjustmentDoc.PaperRef = adjustmentDoc.voucher.reference;
            
            List<AdjustmentAccount> adjAccounts = new List<AdjustmentAccount>(adjustmentDoc.adjustmentAccounts);
            List<TransactionOfBatch> transactionBatches = new List<TransactionOfBatch>();
            


            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                

                for (int i = 0; i < adjAccounts.Count; i++)
                {
                    int accountID = adjAccounts[i].accountID;
                    bool purchaseTax=accountID == bde.SysPars.inputVATAccountID
                        || accountID == bde.SysPars.collectedWithHoldingTaxAccountID;
                    bool sellsTax=accountID == bde.SysPars.outPutVATAccountID
                            || accountID == bde.SysPars.paidWithHoldingTaxAccountID;
                    bool incomeTax = bdeAccounting.IsControlOf<Account>(bde.SysPars.laborIncomeTaxAccountID, accountID)
                        || bdeAccounting.IsControlOf<Account>(bde.Payroll.SysPars.IncomeTaxLiabilityAccount, accountID);
                    bool pension = bdeAccounting.IsControlOf<Account>(bde.Payroll.SysPars.staffPensionPayableAccountID, accountID);

                    int csID = bdeAccounting.GetOrCreateCostCenterAccount(AID, adjAccounts[i].costCenterID, adjAccounts[i].accountID).id;
                    if (!AccountBase.AmountEqual(adjAccounts[i].debitAmount, 0))
                    {
                        TransactionOfBatch debitSide = new TransactionOfBatch(csID, adjAccounts[i].debitAmount, adjAccounts[i].description);
                        transactionBatches.Add(debitSide);
                    }
                    if (!AccountBase.AmountEqual(adjAccounts[i].creditAmount, 0))
                    {
                        TransactionOfBatch creditSide = new TransactionOfBatch(csID, -adjAccounts[i].creditAmount, adjAccounts[i].description);
                        transactionBatches.Add(creditSide);
                    }
                }
                
                
                if (bde.SysPars.summerizeItemTransactions)
                {
                    TransactionSummerizer sum = new TransactionSummerizer(_bde.Accounting, bde.SysPars.summaryRoots);
                    sum.addTransaction(AID, transactionBatches);
                    List<TransactionOfBatch> sumTran=new List<TransactionOfBatch>(sum.getSummary());
                    foreach (string r in bde.SysPars.summaryRoots)
                    {
                        Account sumAccount = bdeAccounting.GetAccount<Account>(r + "-7000");
                        if (sumAccount != null)
                        {
                            int account7000 = sumAccount.id;
                            //foreach (AdjustmentAccount adj in adjAccounts)
                            //    if (adj.accountID == account7000)
                            //        throw new ServerUserMessage("7000 detail account is reserved for system use.");

                            double net = 0;
                            foreach (TransactionOfBatch t in transactionBatches)
                            {
                                string accountCode = bdeAccounting.GetAccount<Account>(bdeAccounting.GetCostCenterAccount(t.AccountID).accountID).Code;
                                if (accountCode.IndexOf(r) == 0)
                                {
                                    net -= t.Amount;
                                }
                            }
                            if (!AccountBase.AmountEqual(net, 0))
                            {
                                sumTran.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID,
                                    account7000).id, net, "Direct entry summary"));
                            }
                        }
                    }
                    foreach (TransactionOfBatch sumEntry in sumTran)
                    {
                        foreach (TransactionOfBatch entry in transactionBatches)
                        {
                            if (sumEntry.AccountID == entry.AccountID)
                                throw new ServerUserMessage("The summary account {0} can't be included because a cooresponding detail account is also included, please remove the summary account",
                                    bdeAccounting.GetCostCenterAccountWithDescription(sumEntry.AccountID).account.Code);
                        }
                        transactionBatches.Add(sumEntry);
                    }
                }
                TransactionOfBatch[] transactions = new TransactionOfBatch[transactionBatches.Count];
                transactionBatches.CopyTo(transactions);
                adjustmentDoc.PaperRef = adjustmentDoc.voucher.reference;
                if(adjustmentDoc.AccountDocumentID!=-1)
                    bdeAccounting.DeleteAccountDocument(AID, adjustmentDoc.AccountDocumentID,true);
                ret = bdeAccounting.RecordTransaction(AID, adjustmentDoc,
                transactions
                , true, adjustmentDoc.voucher);
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch (Exception ex)
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw ex;
            }
        }
        public void ValidateCashAccount(BankDepositDocument bankDocument)
        {
            if (bankDocument.casherAccountID!= -1)
            {
                double balance = GetCashAccountBalance(bankDocument);
                if(balance<0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make cash account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No cash accounts found.\nPlease configure cash account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetCashAccountBalance(BankDepositDocument bankDocument)
        {
            return bdeAccounting.GetNetBalanceAsOf(bankDocument.casherAccountID,0,bankDocument.DocumentDate);
        }
        #endregion
    }
}
