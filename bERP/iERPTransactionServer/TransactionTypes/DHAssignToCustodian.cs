using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    public class DHAssignToCustodian : DeleteReverseDocumentHandler, IDocumentServerHandler
    {
        public DHAssignToCustodian(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
          iERPTransactionBDE _bde = null;
        iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, AccountDocument doc, UserSessionData userSession)
        {
            return bde.CashierPermissionGranted(userSession);
        }
        public override void DeleteDocument(int AID, int docID)
        {
            AssignToCustodianDocument convertDoc = bdeAccounting.GetAccountDocument<AssignToCustodianDocument>(docID);
            if (convertDoc == null)
                return;
            base.DeleteDocument(AID, docID);
            foreach (AssignToCustodianDocument.AssignmentItem item in convertDoc.items)
            {
                if(item.newProperty!=null)
                    bde.deleteProperty(AID,item.newProperty.id);
            }
            
        }
        public int Post(int AID,AccountDocument doc)
        {
            AssignToCustodianDocument custDoc = (AssignToCustodianDocument)doc;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();

                if (custDoc.AccountDocumentID == -1)
                {
                    foreach (AssignToCustodianDocument.AssignmentItem item in custDoc.items)
                    {
                        if (item.existingPropertyID == -1)
                        {
                            item.newProperty.id = -1;
                            item.newProperty.id = bde.createProperty(AID, item.newProperty, item.newPropertyData);
                        }
                    }
                }
                else
                {
                    AssignToCustodianDocument oldDocument = bdeAccounting.GetAccountDocument<AssignToCustodianDocument>(custDoc.AccountDocumentID);
                    bdeAccounting.DeleteAccountDocument(AID, custDoc.AccountDocumentID, true);
                    Dictionary<int, Property> oldProps = new Dictionary<int, Property>();
                    foreach (AssignToCustodianDocument.AssignmentItem item in oldDocument.items)
                    {
                        if (item.existingPropertyID == -1)
                            oldProps.Add(item.newProperty.id, item.newProperty);
                    }
                    foreach (AssignToCustodianDocument.AssignmentItem item in custDoc.items)
                    {
                        if (item.existingPropertyID == -1)
                        {
                            if (oldProps.ContainsKey(item.newProperty.id))
                            {
                                Property oldProp = oldProps[item.newProperty.id];
                                item.newProperty.itemCode = oldProp.itemCode;
                                item.newProperty.parentItemCode = oldProp.parentItemCode;
                                item.newProperty.propertyCode = oldProp.propertyCode;
                                bde.updateProperty(AID, item.newProperty, item.newPropertyData);
                                oldProps.Remove(item.newProperty.id);
                            }
                            else
                            {
                                item.newProperty.id = -1;
                                item.newProperty.id = bde.createProperty(AID, item.newProperty, item.newPropertyData);
                            }
                        }
                    }
                    foreach (KeyValuePair<int, Property> kv in oldProps)
                    {
                        bde.deleteProperty(AID, kv.Key);
                    }
                }
                List<TransactionOfBatch> trans = new List<TransactionOfBatch>();
                foreach (AssignToCustodianDocument.AssignmentItem item in custDoc.items)
                {
                    object extraData;
                    if (item.existingPropertyID == -1)
                    {
                                            TransactionItems titem;
                    TransactionItems propItem;

                        titem = bde.GetTransactionItems(item.newProperty.parentItemCode);
                        if (!titem.IsFixedAssetItem)
                            throw new ServerUserMessage("Item {0} is not fixed asset item", item.newProperty.parentItemCode);
                        propItem = bde.GetTransactionItems(bde.getProperty(item.newProperty.id, out extraData).itemCode);
                        StoreInfo store = bde.GetStoreInfo(item.newPropertyCostCenterID);
                        double q = bde.Accounting.GetBalanceAsOf(item.newPropertyCostCenterID, titem.materialAssetAccountID(store != null), TransactionItem.MATERIAL_QUANTITY, doc.DocumentDate).DebitBalance;

                        if (AccountBase.AmountLess(q, 1))
                            throw new ServerUserMessage("At least one item of {0} is need to assign this fixed asset item to a custodian", titem.Code);

                        double oval = bde.Accounting.GetBalanceAsOf(item.newPropertyCostCenterID, titem.materialAssetAccountID(store != null), TransactionItem.DEFAULT_CURRENCY, doc.DocumentDate).DebitBalance;
                        double acdep = bde.Accounting.GetBalanceAsOf(item.newPropertyCostCenterID, titem.accumulatedDepreciationAccountID, TransactionItem.DEFAULT_CURRENCY, doc.DocumentDate).DebitBalance;


                        trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, item.newPropertyCostCenterID, titem.materialAssetAccountID(store != null)).id
                            , TransactionItem.MATERIAL_QUANTITY, -1, "Asign property to custodian"));
                        trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, custDoc.custodianID, propItem.materialAssetAccountID(store != null)).id
                            , TransactionItem.MATERIAL_QUANTITY, 1, "Asign property to custodian")
                        );
                        if (!AccountBase.AmountEqual(oval, 0))
                        {
                            trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, item.newPropertyCostCenterID, titem.materialAssetAccountID(store != null)).id
                                , TransactionItem.DEFAULT_CURRENCY, -oval / q, "Asign property to custodian"));
                            trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, custDoc.custodianID, propItem.materialAssetAccountID(store != null)).id
                                , TransactionItem.DEFAULT_CURRENCY, oval / q, "Asign property to custodian"));
                        }
                        if (!AccountBase.AmountEqual(acdep, 0))
                        {
                            trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, item.newPropertyCostCenterID, titem.accumulatedDepreciationAccountID).id
                                , TransactionItem.DEFAULT_CURRENCY, -acdep / q, "Asign property to custodian"));
                            trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, custDoc.custodianID, propItem.accumulatedDepreciationAccountID).id
                                , TransactionItem.DEFAULT_CURRENCY, acdep / q, "Asign property to custodian"));
                        }
                    }
                    else
                    {

                        Property existingProperty=bde.getProperty(item.existingPropertyID, out extraData);
                        TransactionItems existingPropertyItem = bde.GetTransactionItems(existingProperty.itemCode);
                        AccountBalance[] matBal=bde.getMaterialBalance(custDoc.DocumentDate,existingPropertyItem.Code,new int[]{TransactionItem.MATERIAL_QUANTITY});
                        Property.verifyPropertyQuantityBalance(existingProperty,matBal,true);
                        
                        double q = bde.Accounting.GetBalanceAsOf(matBal[0].AccountID , TransactionItem.MATERIAL_QUANTITY, doc.DocumentDate).DebitBalance;
                        if (AccountBase.AmountLess(q, 1))
                            throw new ServerUserMessage("At lease on item of {0} is need to convert it to property", existingPropertyItem.Code);

                        int sourceAcDepAccountID = bdeAccounting.GetOrCreateCostCenterAccount(AID, bdeAccounting.GetCostCenterAccount(matBal[0].AccountID).costCenterID, existingPropertyItem.accumulatedDepreciationAccountID).id;
                        double oval = bde.Accounting.GetBalanceAsOf(matBal[0].AccountID, TransactionItem.DEFAULT_CURRENCY, doc.DocumentDate).DebitBalance;

                        double acdep = bde.Accounting.GetBalanceAsOf(sourceAcDepAccountID, TransactionItem.DEFAULT_CURRENCY, doc.DocumentDate).DebitBalance;


                        trans.Add(new TransactionOfBatch(matBal[0].AccountID
                            , TransactionItem.MATERIAL_QUANTITY, -1, "Asign property to custodian"));
                        trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, custDoc.custodianID, existingPropertyItem.originalFixedAssetAccountID).id
                            , TransactionItem.MATERIAL_QUANTITY, 1, "Asign property to custodian")
                        );
                        if (!AccountBase.AmountEqual(oval, 0))
                        {
                            trans.Add(new TransactionOfBatch(matBal[0].AccountID
                                , TransactionItem.DEFAULT_CURRENCY, -oval / q, "Asign property to custodian"));
                            trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, custDoc.custodianID, existingPropertyItem.originalFixedAssetAccountID).id
                                , TransactionItem.DEFAULT_CURRENCY, oval / q, "Asign property to custodian"));
                        }
                        if (!AccountBase.AmountEqual(acdep, 0))
                        {
                            trans.Add(new TransactionOfBatch(sourceAcDepAccountID
                                , TransactionItem.DEFAULT_CURRENCY, -acdep / q, "Asign property to custodian"));
                            trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, custDoc.custodianID, existingPropertyItem.accumulatedDepreciationAccountID).id
                                , TransactionItem.DEFAULT_CURRENCY, acdep / q, "Asign property to custodian"));
                        }
                        
                    }
                    
                    
                }
                custDoc.PaperRef = Guid.NewGuid().ToString();
                if(string.IsNullOrEmpty(custDoc.ShortDescription))
                    custDoc.ShortDescription = "Assignment of Property to a custodian";
                int ret = bdeAccounting.RecordTransaction(AID, custDoc,trans.ToArray());
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        private TransactionOfBatch[] CollectCashPaymentTransactionsOfBatch(int AID, WithdrawalDocument withDrawalDocument)
        {
            CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.bankServiceChargeByCash);
            if (csa == null)
                throw new ServerConfigurationError("The bank service charge by cash account not configured properly. The account does not belong to 'Head Quarter' cost center");

            int serviceChargeAccountID = csa.id;
            TransactionOfBatch[] transactions = null;

            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            int withdrawalAccountID = bde.SysPars.WithdrawalAccountID;
            CostCenterAccount csWithdrawal = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, withdrawalAccountID);
            if (csWithdrawal == null)
            {
                throw new ServerUserMessage("'Withdrawal Account' does not belong to 'Head Quarter' cost center. Please join the account to the cost center and try again!");
            }
            creditSide = new TransactionOfBatch(withDrawalDocument.assetAccountID, -withDrawalDocument.amount);
            debitSide = new TransactionOfBatch(csWithdrawal.id, withDrawalDocument.amount);

            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(withDrawalDocument.paymentMethod) && withDrawalDocument.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(withDrawalDocument.serviceChargeAmount, 0))
            {
                string note = withDrawalDocument.paymentMethod == BizNetPaymentMethod.CPOFromCash ? "CPO service charge" : "Bank transfer service charge";

                TransactionOfBatch debitSide2, creditSide2;
                debitSide2 = new TransactionOfBatch(serviceChargeAccountID, withDrawalDocument.serviceChargeAmount, note);
                creditSide2 = new TransactionOfBatch(withDrawalDocument.assetAccountID, -withDrawalDocument.serviceChargeAmount, note);
                transactions = new TransactionOfBatch[] { debitSide, creditSide, debitSide2, creditSide2 };
            }
            else
            {
                transactions = new TransactionOfBatch[] { debitSide, creditSide };
            }
            return transactions;
        }
        private TransactionOfBatch[] CollectBankPaymentTransactionsOfBatch(int AID, WithdrawalDocument withdrawalDocument)
        {
            TransactionOfBatch[] transactions = null;
            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            int withdrawalAccountID = bde.SysPars.WithdrawalAccountID;
            CostCenterAccount csWithdrawal = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, withdrawalAccountID);
            if (csWithdrawal == null)
            {
                throw new ServerUserMessage("'Withdrawal Account' does not belong to 'Head Quarter' cost center. Please join the account to the cost center and try again!");
            }

            BankAccountInfo ba = bde.GetBankAccount(withdrawalDocument.assetAccountID);
            int serviceChargeAccountID = ba.bankServiceChargeCsAccountID;
            string note = withdrawalDocument.paymentMethod == BizNetPaymentMethod.CPOFromBankAccount ? "CPO service charge" : "Bank transfer service charge";

            creditSide = new TransactionOfBatch(ba.mainCsAccount, -withdrawalDocument.amount);
            debitSide = new TransactionOfBatch(csWithdrawal.id, withdrawalDocument.amount);
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(withdrawalDocument.paymentMethod) && withdrawalDocument.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(withdrawalDocument.serviceChargeAmount, 0))
            {
                TransactionOfBatch debitSide2, creditSide2;
                debitSide2 = new TransactionOfBatch(serviceChargeAccountID, withdrawalDocument.serviceChargeAmount, note);
                creditSide2 = new TransactionOfBatch(ba.mainCsAccount, -withdrawalDocument.serviceChargeAmount, note);
                transactions = new TransactionOfBatch[] { debitSide, creditSide, debitSide2, creditSide2 };
            }
            else
            {
                transactions = new TransactionOfBatch[] { debitSide, creditSide };
            }
            return transactions;
        }
        public void ValidateAccounts(WithdrawalDocument withdrawalDocument, bool validateCash)
        {
            if (!validateCash) //validate bank
            {
                ValidateBankAccount(withdrawalDocument);
            }
            else
            {
                ValidateCashAccount(withdrawalDocument);
            }
        }

        private void ValidateBankAccount(WithdrawalDocument withdrawalDocument)
        {
            if (withdrawalDocument.assetAccountID != -1)
            {
                double balance = GetBankAccountBalance(withdrawalDocument);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make bank account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No bank accounts found.\nPlease configure bank account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetBankAccountBalance(WithdrawalDocument withdrawalDocument)
        {
            return bdeAccounting.GetNetBalanceAsOf(withdrawalDocument.assetAccountID, 0, withdrawalDocument.DocumentDate);
        }

        private void ValidateCashAccount(WithdrawalDocument withdrawalDocument)
        {
            if (withdrawalDocument.assetAccountID != -1)
            {
                double balance = GetCashAccountBalance(withdrawalDocument);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make cash account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No cash accounts found.\nPlease configure cash account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetCashAccountBalance(WithdrawalDocument withdrawalDocument)
        {
            return bdeAccounting.GetNetBalanceAsOf(withdrawalDocument.assetAccountID, 0, withdrawalDocument.DocumentDate);
        }
       
        #endregion

        
    }

}
