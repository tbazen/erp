using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    public class DHBankDeposit : DeleteReverseDocumentHandler,  IDocumentServerHandler,ICashTransaction
    {
         iERPTransactionBDE _bde =null;
         iERPTransactionBDE bde
         {
             get
             {
                 if (_bde == null)
                     _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                 return _bde;
             }
         }
         public DHBankDeposit(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
             : base(bdeAccounting)
        {
            
        }
        #region IDocumentServerHandler Members

         public bool CheckPostPermission(int docID, INTAPS.Accounting.AccountDocument doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return bde.CashierPermissionGranted(userSession);
        }

        public override string GetHTML(INTAPS.Accounting.AccountDocument doc)
        {
            BankDepositDocument bdoc = (BankDepositDocument)doc;
            return "<b>Amount:<b/>" + TSConstants.FormatBirr(bdoc.amount);
        }

        public int Post(int AID,AccountDocument doc)
        {
            BankDepositDocument bankDocument = (BankDepositDocument)doc;
            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            this.bde.assertNoSummaryCsAccount(bankDocument.casherAccountID);
            creditSide = new TransactionOfBatch(bankDocument.casherAccountID, -bankDocument.amount);
            BankAccountInfo ba = bde.GetBankAccount(bankDocument.bankAccountID);
            debitSide = new TransactionOfBatch(ba.mainCsAccount, bankDocument.amount);
            TransactionOfBatch[] transactions = new TransactionOfBatch[] { debitSide, creditSide };
            
            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                
                if (doc.AccountDocumentID != -1)
                    bdeAccounting.DeleteAccountDocument(AID, doc.AccountDocumentID, true);

                if (bankDocument.voucher != null && string.IsNullOrEmpty(bankDocument.PaperRef))
                    bankDocument.PaperRef = bankDocument.voucher.reference;
                
                DocumentTypedReference[] typedRefs = bankDocument.voucher == null ? new DocumentTypedReference[0] :
                  new DocumentTypedReference[] { bankDocument.voucher };

                ret = bdeAccounting.RecordTransaction(AID, bankDocument,transactions, true, typedRefs);

                if (!doc.scheduled && !bde.AllowNegativeTransactionPost())
                    bdeAccounting.validateNegativeBalance(bankDocument.casherAccountID, 0, false, bankDocument.DocumentDate, "Transaction is blocked because there is no sufficient cash");
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch (Exception ex)
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw ex;
            }
        }
        public void ValidateCashAccount(BankDepositDocument bankDocument)
        {
            if (bankDocument.casherAccountID!= -1)
            {
                double balance = GetCashAccountBalance(bankDocument);
                if(balance<0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make cash account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No cash accounts found.\nPlease configure cash account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetCashAccountBalance(BankDepositDocument bankDocument)
        {
            return bdeAccounting.GetNetBalanceAsOf(bankDocument.casherAccountID,0,bankDocument.DocumentDate);
        }
        #endregion


        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            BankDepositDocument paymentDocument = doc as BankDepositDocument;
            if (paymentDocument == null)
                return null;
            if (!CashTransactionUtility.isCostCenterAccountInCostCenter(bde.Accounting,paymentDocument.casherAccountID, option.costCenters))
                return null;
            CashFlowItem item1 = CashFlowItem.createByText(paymentDocument.bankAccountID, -1, "", paymentDocument.amount);
            CashFlowItem item2 = CashFlowItem.createByText(paymentDocument.casherAccountID, -1, "", -paymentDocument.amount);
            return new List<CashFlowItem>(new CashFlowItem[] { item1, item2 });

        }
        public string getInstrumentNo(AccountDocument _doc, out string typeName)
        {
            typeName = null;
            return null;
        }
    }
}
