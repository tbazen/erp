﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.Accounting;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    class DHBankReconciliation : DeleteReverseDocumentHandler, IDocumentServerHandler
    {
        iERPTransactionBDE _bde = null;
        iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHBankReconciliation(AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        public bool CheckPostPermission(int docID, AccountDocument _doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return true;
        }

        public int Post(int AID, AccountDocument _doc)
        {
            BankReconciliationDocument doc = (BankReconciliationDocument)_doc;
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    //doc.DocumentDate = AccountBase.getClosingTime(doc.DocumentDate);
                    BankAccountInfo ba = bde.GetBankAccount(doc.bankID);
                    if (ba == null)
                        throw new ServerUserMessage("Invalid bank account ID:" + doc.bankID);
                    if (doc.AccountDocumentID != -1)
                        bdeAccounting.DeleteAccountDocument(AID,doc.AccountDocumentID,true);
                    BankReconciliationDocument prev = null;
                    int[] allDocs = bdeAccounting.WriterHelper.GetColumnArray<int>(string.Format("Select ID from {0}.dbo.Document where tranTicks<={1} and DocumentTypeID={2} order by tranTicks desc", bdeAccounting.DBName, doc.tranTicks, bdeAccounting.GetDocumentTypeByType(typeof(BankReconciliationDocument)).id), 0);
                    foreach (int docId in allDocs)
                    {
                        BankReconciliationDocument bdoc = bdeAccounting.GetAccountDocument(docId, true) as BankReconciliationDocument;
                        if (bdoc.bankID != doc.bankID)
                            continue;
                        prev = bdoc;
                        break;
                    }
                    long prevDate;
                    int[] prevTransactions;
                    if (prev!=null)
                    {
                        if (doc.DocumentDate.Subtract(prev.DocumentDate).TotalDays <= 1)
                            throw new ServerUserMessage("Reconciliation already done for this date");
                        prevDate = prev.tranTicks;
                        prevTransactions = prev.outstandingTransactions;
                        doc.previosReconciationDocument = prev.AccountDocumentID;
                    }
                    else
                    {
                        doc.previosReconciationDocument = -1;
                        object minDate = bdeAccounting.WriterHelper.ExecuteScalar(string.Format("Select min(tranTicks) from {0}.dbo.AccountTransaction where tranTicks<{1}", bdeAccounting.DBName, doc.DocumentDate.Ticks));
                        if (minDate is long)
                            prevDate = (long)minDate;
                        else
                            throw new ServerUserMessage("No transaction to reconsile for this bank account");
                        prevTransactions = new int[0];
                    }
                    //validate transactions
                    
                    foreach (int tid in doc.outstandingTransactions)
                    {
                        AccountTransaction tran = bdeAccounting.GetTransaction(tid);
                        if (tran == null)
                            throw new ServerUserMessage("Invalid transaction ID:" + tid);
                        if (tran.tranTicks < prevDate || tran.tranTicks>= doc.tranTicks)
                        {
                            bool foundInPrev=false;
                            foreach(int prevTran in prevTransactions)
                                if (prevTran == tid)
                                {
                                    foundInPrev = true;
                                    break;
                                }
                            if(!foundInPrev)
                                throw new ServerUserMessage("New unreconciled transaction not the reconcilation range");
                        }
                    }
                    int ret = bdeAccounting.RecordTransaction(AID, doc, new TransactionOfBatch[0]);
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        public override void DeleteDocument(int AID, int docID)
        {
            BankReconciliationDocument doc = bdeAccounting.GetAccountDocument(docID, true) as BankReconciliationDocument;
            if (doc == null)
                throw new ServerUserMessage("Bank reconciliation document not found in database.");

            //if (bde.reconciliationDone(doc.DocumentDate))
                //throw new ServerUserMessage("You can't delete this bank reconciliation document because one or more bank reconciliations are made after the one you are trying to delete.");
            int [] reconAfter;
            INTAPS.RDBMS.SQLHelper helper = bdeAccounting.GetReaderHelper();
            try
            {
                reconAfter = helper.GetColumnArray<int>(string.Format("Select ID from {0}.dbo.Document where tranTicks>{1} and DocumentTypeID={2} order by tranTicks"
                   , bdeAccounting.DBName, doc.DocumentDate.Ticks, bdeAccounting.GetDocumentTypeByType(typeof(BankReconciliationDocument)).id));
            }
            finally
            {
                bdeAccounting.ReleaseHelper(helper);
            }
            lock (bdeAccounting.WriterHelper)
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                try
                {
                    foreach (int a in reconAfter)
                    {
                        BankReconciliationDocument aDoc = bdeAccounting.GetAccountDocument(a, true) as BankReconciliationDocument;
                        if (aDoc.bankID == doc.bankID)
                            base.DeleteDocument(AID, a);
                    }
                    base.DeleteDocument(AID, docID);
                    bdeAccounting.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        public override string GetHTML(AccountDocument _doc)
        {
            return "";
        }
    }
}
