using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    public class DHBankServiceChargePayment : DeleteReverseDocumentHandler,IDocumentServerHandler,ICashTransaction
    {
        iERPTransactionBDE _bde = null;
        iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }

        public DHBankServiceChargePayment(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, AccountDocument doc, UserSessionData userSession)
        {
            return bde.CashierPermissionGranted(userSession);
        }

        public override string GetHTML(AccountDocument doc)
        {
            BankServiceChargePaymentDocument bdoc = (BankServiceChargePaymentDocument)doc;
            return "<b>Amount:<b/>" + TSConstants.FormatBirr(bdoc.amount);
        }

        public int Post(int AID, AccountDocument doc)
        {
            BankServiceChargePaymentDocument serviceChargePaymentDoc = (BankServiceChargePaymentDocument)doc;
            TransactionOfBatch[] transactions = null;
            int receivableAccount = bde.GetBankAccount(serviceChargePaymentDoc.assetAccountID).bankServiceChargeCsAccountID;
             if (receivableAccount == -1)
             {
                 throw new INTAPS.ClientServer.ServerUserMessage("Bank Service Charge Account not found!");
             }
            switch (serviceChargePaymentDoc.paymentMethod)
            {
                case BizNetPaymentMethod.Cash:
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.BankTransferFromCash:
                    transactions = CollectCashPaymentTransactionsOfBatch(serviceChargePaymentDoc,receivableAccount);
                    break;

                case BizNetPaymentMethod.Check:
                case BizNetPaymentMethod.CPOFromBankAccount:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    transactions = CollectBankPaymentTransactionsOfBatch(serviceChargePaymentDoc,receivableAccount);
                    break;
                case BizNetPaymentMethod.None:
                    break;
                default:
                    break;
            }

            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                if (serviceChargePaymentDoc.voucher != null)
                    serviceChargePaymentDoc.PaperRef = serviceChargePaymentDoc.voucher.reference;
                DocumentTypedReference[] typedRefs = serviceChargePaymentDoc.voucher == null ? new DocumentTypedReference[0] :
                  new DocumentTypedReference[] { serviceChargePaymentDoc.voucher };
                if (doc.AccountDocumentID != -1)
                {
                    bdeAccounting.DeleteAccountDocument(AID, doc.AccountDocumentID, true);
                }

                ret = bdeAccounting.RecordTransaction(AID, serviceChargePaymentDoc,
                transactions
                , true, typedRefs);
                
                if (!doc.scheduled && !bde.AllowNegativeTransactionPost())
                {
                    bdeAccounting.validateNegativeBalance
                       (serviceChargePaymentDoc.assetAccountID, 0, false, serviceChargePaymentDoc.DocumentDate
                       , "Transaction is blocked because there is no sufficient balance in the bank account");
                }
                // ValidateAccounts(serviceChargePaymentDoc,validateCash);
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        private TransactionOfBatch[] CollectCashPaymentTransactionsOfBatch(BankServiceChargePaymentDocument serviceChargePaymentDoc, int receivableAccount)
        {
            TransactionOfBatch[] transactions = null;

            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;

            creditSide = new TransactionOfBatch(serviceChargePaymentDoc.assetAccountID, -serviceChargePaymentDoc.amount);
            debitSide = new TransactionOfBatch(receivableAccount, serviceChargePaymentDoc.amount);

            transactions = new TransactionOfBatch[] { debitSide, creditSide };

            return transactions;
        }
        private TransactionOfBatch[] CollectBankPaymentTransactionsOfBatch(BankServiceChargePaymentDocument serviceChargePaymentDoc, int receivableAccount)
        {
            TransactionOfBatch[] transactions = null;

            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            BankAccountInfo ba = bde.GetBankAccount(serviceChargePaymentDoc.assetAccountID);
            string note = serviceChargePaymentDoc.paymentMethod == BizNetPaymentMethod.CPOFromBankAccount ? "CPO service charge" : "Bank transfer service charge";

            creditSide = new TransactionOfBatch(ba.mainCsAccount, -serviceChargePaymentDoc.amount);
            debitSide = new TransactionOfBatch(receivableAccount, serviceChargePaymentDoc.amount);

            transactions = new TransactionOfBatch[] { debitSide, creditSide };

            return transactions;
        }
        public void ValidateAccounts(BankServiceChargePaymentDocument serviceChargePaymentDoc, bool validateCash)
        {
            if (!validateCash) //validate bank
            {
                ValidateBankAccount(serviceChargePaymentDoc);
            }
            else
            {
                ValidateCashAccount(serviceChargePaymentDoc);
            }
        }

        private void ValidateBankAccount(BankServiceChargePaymentDocument serviceChargePaymentDoc)
        {
            if (serviceChargePaymentDoc.assetAccountID != -1)
            {
                double balance = GetBankAccountBalance(serviceChargePaymentDoc);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make bank account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No bank accounts found.\nPlease configure bank account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetBankAccountBalance(BankServiceChargePaymentDocument serviceChargePaymentDoc)
        {
            return bdeAccounting.GetNetBalanceAsOf(serviceChargePaymentDoc.assetAccountID, 0, serviceChargePaymentDoc.DocumentDate);
        }

        private void ValidateCashAccount(BankServiceChargePaymentDocument serviceChargePaymentDoc)
        {
            if (serviceChargePaymentDoc.assetAccountID != -1)
            {
                double balance = GetCashAccountBalance(serviceChargePaymentDoc);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make cash account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No cash accounts found.\nPlease configure cash account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetCashAccountBalance(BankServiceChargePaymentDocument serviceChargePaymentDoc)
        {
            return bdeAccounting.GetNetBalanceAsOf(serviceChargePaymentDoc.assetAccountID, 0, serviceChargePaymentDoc.DocumentDate);
        }
        
        #endregion

        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option,AccountTransaction [] docTrans)
        {
            BankServiceChargePaymentDocument paymentDocument = doc as BankServiceChargePaymentDocument;
            if (paymentDocument == null)
                return null;
            if (!CashTransactionUtility.isCostCenterAccountInCostCenter(bde.Accounting, option.costCenters, docTrans))
                return null;

            BankAccountInfo ba= bde.GetBankAccount(paymentDocument.assetAccountID);
            CashFlowItem item1 = CashFlowItem.createByText(paymentDocument.assetAccountID, -1, "Banks Service Charege from Bank Account " + ba.BankBranchAccount,-paymentDocument.amount);
            CashFlowItem item2 = CashFlowItem.createByText(-1, ba.bankServiceChargeCsAccountID, "Banks Service Charege from Bank Account " + ba.BankBranchAccount,paymentDocument.amount);
            return new List<CashFlowItem>(new CashFlowItem[] { item1,item2 });
        }
        public string getInstrumentNo(AccountDocument _doc, out string typeName)
        {
            typeName = null;
            return null;
        }

    }

}
