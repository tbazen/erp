using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    public class DHBankToBank: DeleteReverseDocumentHandler,  IDocumentServerHandler,ICashTransaction
    {
        iERPTransactionBDE _bde = null;
        iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHBankToBank(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, INTAPS.Accounting.AccountDocument doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return bde.CashierPermissionGranted(userSession);
        }

        public override string GetHTML(INTAPS.Accounting.AccountDocument doc)
        {
            BankToBankDocument bdoc = (BankToBankDocument)doc;
            return "<b>Amount:<b/>" + TSConstants.FormatBirr(bdoc.amount);
        }

        public int Post(int AID,AccountDocument doc)
        {
            BankToBankDocument bankDocument = (BankToBankDocument)doc;
          
            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            BankAccountInfo fromBankAccount = bde.GetBankAccount(bankDocument.fromBankAccountID);
            BankAccountInfo toBankAccount = bde.GetBankAccount(bankDocument.toBankAccountID);
            creditSide = new TransactionOfBatch(fromBankAccount.mainCsAccount, -bankDocument.amount);
            debitSide = new TransactionOfBatch(toBankAccount.mainCsAccount, bankDocument.amount);
            TransactionOfBatch[] transactions = new TransactionOfBatch[] { debitSide, creditSide };
            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                if (bankDocument.voucher != null)
                    bankDocument.PaperRef = bankDocument.voucher.reference;

                DocumentTypedReference[] typedRefs = bankDocument.voucher == null ? new DocumentTypedReference[0] :
                   new DocumentTypedReference[] { bankDocument.voucher };
                if (doc.AccountDocumentID == -1)
                    ret = bdeAccounting.RecordTransaction(AID, bankDocument,
                    transactions
                    , true, typedRefs);
                else
                    ret = bdeAccounting.UpdateTransaction(AID, bankDocument, transactions, true, typedRefs);
                if (!doc.scheduled && !bde.AllowNegativeTransactionPost())
                    bdeAccounting.validateNegativeBalance(bankDocument.fromBankAccountID, 0, false, bankDocument.DocumentDate
                        , "The transaction is blocked because the source bank doesn't have sufficient balance.");
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch (Exception ex)
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw ex;
            }
        }
        public void ValidateBankAccount(BankToBankDocument bankDocument)
        {
            if (bankDocument.fromBankAccountID!= -1)
            {
                double balance = GetBankAccountBalance(bankDocument);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make bank account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No bank accounts found.\nPlease configure bank account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetBankAccountBalance(BankToBankDocument bankDocument)
        {
            return bdeAccounting.GetNetBalanceAsOf(bankDocument.fromBankAccountID, 0, bankDocument.DocumentDate);
        }

        #endregion

        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            BankToBankDocument paymentDocument = doc as BankToBankDocument;
            if (paymentDocument == null)
                return null;
            //Arbitirarily the destination back account is used for filtering
            if (!CashTransactionUtility.isCostCenterAccountInCostCenter(bde.Accounting, paymentDocument.toBankAccountID, option.costCenters))
                return null;
            BankAccountInfo baFrom = bde.GetBankAccount(paymentDocument.fromBankAccountID);
            BankAccountInfo baTo = bde.GetBankAccount(paymentDocument.toBankAccountID);
            CashFlowItem item1 = CashFlowItem.createByText(paymentDocument.toBankAccountID, -1, "Withdrawn from " + baFrom.BankBranchAccount, -paymentDocument.amount);
            CashFlowItem item2 = CashFlowItem.createByText(paymentDocument.fromBankAccountID, -1, "Deposited to " + baTo.BankBranchAccount, paymentDocument.amount);
            return new List<CashFlowItem>(new CashFlowItem[] { item1, item2 });
        }
        public string getInstrumentNo(AccountDocument _doc, out string typeName)
        {
            typeName = null;
            return null;
        }

    }
}
