using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    public class DHBankWithdrawal: DeleteReverseDocumentHandler,  IDocumentServerHandler,ICashTransaction
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }

        public DHBankWithdrawal(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, INTAPS.Accounting.AccountDocument doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            
            return bde.CashierPermissionGranted(userSession);
        }

        public override string GetHTML(INTAPS.Accounting.AccountDocument doc)
        {
            BankWithdrawalDocument bdoc = (BankWithdrawalDocument)doc;
            return "<b>Amount:<b/>" + TSConstants.FormatBirr(bdoc.amount);
        }

        public int Post(int AID,AccountDocument doc)
        {
            BankWithdrawalDocument bankDocument = (BankWithdrawalDocument)doc;
           // ValidateBankAccount(bankDocument);
            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            BankAccountInfo ba = bde.GetBankAccount(bankDocument.bankAccountID);
            creditSide = new TransactionOfBatch(ba.mainCsAccount, -bankDocument.amount);
            bde.assertNoSummaryCsAccount(bankDocument.casherAccountID);
            debitSide = new TransactionOfBatch(bankDocument.casherAccountID, bankDocument.amount);
            TransactionOfBatch[] transactions = new TransactionOfBatch[] { debitSide, creditSide };
            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                if (bankDocument.voucher != null && string.IsNullOrEmpty(bankDocument.PaperRef))
                    bankDocument.PaperRef = bankDocument.voucher.reference;
                DocumentTypedReference[] typedRefs = bankDocument.voucher == null ? new DocumentTypedReference[0] :
                 new DocumentTypedReference[] { bankDocument.voucher };

                if (doc.AccountDocumentID == -1)
                    ret = bdeAccounting.RecordTransaction(AID, bankDocument,
                    transactions
                    , true, typedRefs);
                else
                    ret = bdeAccounting.UpdateTransaction(AID, bankDocument, transactions, true, typedRefs);
                if (!doc.scheduled)
                {
                    if (!bde.AllowNegativeTransactionPost())
                    {
                        bdeAccounting.validateNegativeBalance
                            (bankDocument.bankAccountID, 0, false, bankDocument.DocumentDate
                            , "Transaction is blocked because there is no sufficient balance in the bank account");
                    }
                }

                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch (Exception ex)
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw ex;
            }
        }
        public void ValidateBankAccount(BankWithdrawalDocument bankDocument)
        {
            if (bankDocument.bankAccountID!= -1)
            {
                double balance = GetBankAccountBalance(bankDocument);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make bank account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No bank accounts found.\nPlease configure bank account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetBankAccountBalance(BankWithdrawalDocument bankDocument)
        {
            return bdeAccounting.GetNetBalanceAsOf(bankDocument.bankAccountID, 0, bankDocument.DocumentDate);
        }

        #endregion


        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            BankWithdrawalDocument paymentDocument = doc as BankWithdrawalDocument;
            if (paymentDocument == null)
                return null;
            if (!CashTransactionUtility.isCostCenterAccountInCostCenter(bde.Accounting, paymentDocument.casherAccountID, option.costCenters))
                return null;

            CashFlowItem item1 = CashFlowItem.createByText(paymentDocument.bankAccountID, -1, "", -paymentDocument.amount);
            CashFlowItem item2 = CashFlowItem.createByText(paymentDocument.casherAccountID, -1, "", paymentDocument.amount);
            return new List<CashFlowItem>(new CashFlowItem[] { item1,item2});
        }
        public string getInstrumentNo(AccountDocument _doc, out string typeName)
        {
            BankWithdrawalDocument doc = _doc as BankWithdrawalDocument;
            typeName = PaymentMethodHelper.GetPaymentTypeReferenceName(BizNetPaymentMethod.Check);
            return doc.chekNumber;
        }

    }
}
