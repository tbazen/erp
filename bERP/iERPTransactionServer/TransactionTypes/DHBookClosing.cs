using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Collections;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
namespace BIZNET.iERP.Server
{
    public class DHBookClosing : DeleteReverseDocumentHandler, IDocumentServerHandler
    {
        iERPTransactionBDE _bde = null;
        iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHBookClosing(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, INTAPS.Accounting.AccountDocument doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return bde.ConfigurationPermissionGranted(userSession);
        }

        public override string GetHTML(INTAPS.Accounting.AccountDocument doc)
        {
            return null;
        }
        public int Post(int AID, AccountDocument doc)
        {
            BookClosingDocument bookClosingDocument = (BookClosingDocument)doc;
            bookClosingDocument.PaperRef = bookClosingDocument.voucher.reference;
            bookClosingDocument.closingAccount = bde.SysPars.closingAccountID;
            Account capitalAccount = null;
            Console.WriteLine("Capital accounts");
            foreach (Account ac in bde.Accounting.GetLeafAccounts<Account>(bde.SysPars.assetCapitalAccountID))
            {
                if (ac.id == bookClosingDocument.closingAccount)
                {
                    capitalAccount = ac;
                    break;
                }
            }
            if (capitalAccount == null)
                throw new ServerUserMessage("Invalid closing account id {0}. Closing account should be a capital account.", bookClosingDocument.closingAccount);
            bookClosingDocument.DocumentDate = new DateTime(bookClosingDocument.DocumentDate.Year, bookClosingDocument.DocumentDate.Month, bookClosingDocument.DocumentDate.Day, 23, 58, 59);

            AccountDocument prev = bde.Accounting.getPreviousDocument(bde.Accounting.GetDocumentTypeByType(typeof(BookClosingDocument)).id, bookClosingDocument.tranTicks, false);

            if (prev != null)
            {
                if (prev.DocumentDate.Date == bookClosingDocument.DocumentDate.Date)
                    throw new ServerUserMessage("More than one closing transactions is not allowed in a sigle day.");
            }
            
            double netIncome = 0;
            Console.WriteLine("Collecting balances..");
            List<Account> inactiveAccounts = new List<Account>();
            double totalCr = 0, totalDb = 0;
            List<TransactionOfBatch> trans = new List<TransactionOfBatch>();
            foreach (int parent in new int[] { 
                bde.SysPars.assetIncomeAccountID,
                bde.SysPars.costOfSalesAccountID,
                bde.SysPars.expenseAccountID })
            {
                Account parentAccount = bde.Accounting.GetAccount<Account>(parent);
                foreach (Account la in bde.Accounting.GetLeafAccounts<Account>(parentAccount.id))
                {
                    bool hasTransaction = false;
                    foreach (CostCenterAccount csa in bde.Accounting.GetCostCenterAccountsOf<Account>(la.id))
                    {
                        if (csa.isControlAccount)
                            continue;
                        double balance = bde.Accounting.GetBalanceAsOf(csa.id, 0, bookClosingDocument.DocumentDate).DebitBalance;
                        if (AccountBase.AmountEqual(balance, 0))
                            continue;
                        TransactionOfBatch tran = new TransactionOfBatch();
                        tran.AccountID = csa.id;
                        tran.Amount = -balance;
                        
                        tran.Note= "Closing transaction";
                        trans.Add(tran);
                        hasTransaction = true;
                        
                        netIncome += balance;
                    }
                    if (hasTransaction && la.Status != AccountStatus.Activated)
                    {
                        inactiveAccounts.Add(la);
                    }
                }
            }

            if (!AccountBase.AmountEqual(netIncome, 0))
            {
                TransactionOfBatch netTran = new TransactionOfBatch();
                netTran.AccountID = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, capitalAccount.id).id;
                netTran.Amount = netIncome;
                netTran.Note = "Net Income from Closing Transaction";
                trans.Add(netTran);
            }
            Console.WriteLine("{0} accounts found. Net Income:{1} TotalDb:{2} TotalCr:{3}", trans.Count - 1, netIncome.ToString("#,#0.00"), totalDb, totalCr);
            Console.WriteLine("Posting transaction. Will take time.");
            bde.WriterHelper.BeginTransaction();
            try
            {
                foreach (Account a in inactiveAccounts)
                    bde.Accounting.ActivateAcount<Account>(AID, a.id, DateTime.Now);
                bookClosingDocument.AccountDocumentID = bde.Accounting.RecordTransaction(AID, bookClosingDocument,trans.ToArray());
                foreach (Account a in inactiveAccounts)
                    bde.Accounting.DeactivateAccount<Account>(AID, a.id, DateTime.Now);
                bde.closeBookForTransaction(AID, new ClosedRange() {actionDate=DateTime.Now,agentName="System",AID=AID,comment=bookClosingDocument.ShortDescription,toDate=bookClosingDocument.DocumentDate});
                bde.WriterHelper.CommitTransaction();
                return bookClosingDocument.AccountDocumentID;
            }
            catch
            {
                bde.WriterHelper.RollBackTransaction();
                throw;
            }
        }
        public void ValidateCashAccount(BankDepositDocument bankDocument)
        {
            if (bankDocument.casherAccountID != -1)
            {
                double balance = GetCashAccountBalance(bankDocument);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make cash account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No cash accounts found.\nPlease configure cash account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetCashAccountBalance(BankDepositDocument bankDocument)
        {
            return bdeAccounting.GetNetBalanceAsOf(bankDocument.casherAccountID, 0, bankDocument.DocumentDate);
        }
        #endregion
    }
}
