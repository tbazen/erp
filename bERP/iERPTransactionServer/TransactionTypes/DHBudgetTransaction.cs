using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Collections;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS;
namespace BIZNET.iERP.Server
{
    public class DHBudgetTransaction : DeleteReverseDocumentHandler,  IDocumentServerHandler
    {
        iERPTransactionBDE _bde = null;
        iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHBudgetTransaction(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, INTAPS.Accounting.AccountDocument doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return true;
        }

        public override string GetHTML(INTAPS.Accounting.AccountDocument doc)
        {
            return "";
        }

        public int Post(int AID, AccountDocument doc)
        {
            BudgetTransactionDocument budgetTranDoc = (BudgetTransactionDocument)doc;
            foreach(var docID in budgetTranDoc.referedDocs)
                if (bde.isDocumentBudgetSet(docID))
                    throw new ServerUserMessage("Budget transactions are already recorded for the referenced transaction");

            foreach (var docID in budgetTranDoc.referedDocs)
            {
                AccountDocument referedDoc = this.bdeAccounting.GetAccountDocument(docID, false);
                if (referedDoc == null)
                    throw new ServerUserMessage("Refered document not found. Document ID:{0}", docID);
                if (budgetTranDoc.transferProperties)
                {
                    if (String.IsNullOrEmpty(budgetTranDoc.PaperRef))
                        budgetTranDoc.PaperRef = referedDoc.PaperRef;
                    if (String.IsNullOrEmpty(budgetTranDoc.ShortDescription))
                        budgetTranDoc.ShortDescription = "Budget: " + referedDoc.ShortDescription;
                    budgetTranDoc.DocumentDate = referedDoc.DocumentDate;
                }
            }

            List<BudgetTransaction> adjAccounts = new List<BudgetTransaction>(budgetTranDoc.transactions);
            List<TransactionOfBatch> transactions = new List<TransactionOfBatch>();

            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                

                for (int i = 0; i < adjAccounts.Count; i++)
                {
                    int accountID = adjAccounts[i].accountID;

                    int csID = bdeAccounting.GetOrCreateCostCenterAccount(AID, adjAccounts[i].costCenterID, adjAccounts[i].accountID).id;
                    if (!AccountBase.AmountEqual(adjAccounts[i].debitAmount, 0))
                    {
                        TransactionOfBatch debitSide = new TransactionOfBatch(csID,TransactionItem.BUDGET, adjAccounts[i].debitAmount, adjAccounts[i].description);
                        transactions.Add(debitSide);
                    }
                    if (!AccountBase.AmountEqual(adjAccounts[i].creditAmount, 0))
                    {
                        TransactionOfBatch creditSide = new TransactionOfBatch(csID,TransactionItem.BUDGET, -adjAccounts[i].creditAmount, adjAccounts[i].description);
                        transactions.Add(creditSide);
                    }
                }
                
                if(budgetTranDoc.AccountDocumentID!=-1)
                    deleteDoc(AID, budgetTranDoc.AccountDocumentID, true);
                ret = bdeAccounting.RecordTransaction(AID, budgetTranDoc,transactions.ToArray(), true);
                if (budgetTranDoc.transferProperties)
                {
                    foreach (var docID in budgetTranDoc.referedDocs)
                        foreach (DocumentSerial r in bdeAccounting.getDocumentSerials(docID))
                        {
                            bdeAccounting.addDocumentSerials(AID, budgetTranDoc, r.typedReference);
                        }
                }
                foreach(var docID in budgetTranDoc.referedDocs)
                    bde.setBudgetDocument(AID, docID, ret);
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch (Exception ex)
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw ex;
            }
        }
        void deleteDoc(int AID,int docID,bool forUpdate)
        {
            //bdeAccounting.WriterHelper.ExecuteNonQuery("delete from {0}.dbo.{1} where bugetTransaction={2}".format(bde.DBName, typeof(BudgetTransactionReference).Name, docID));
            this.bdeAccounting.DeleteAccountDocument(AID, docID, forUpdate);
        }
        public override void DeleteDocument(int AID, int docID)
        {
            deleteDoc(AID, docID, false);
        }
        #endregion
    }
}
