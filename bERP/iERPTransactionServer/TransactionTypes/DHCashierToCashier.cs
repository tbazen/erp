using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    public class DHCashierToCashier : DeleteReverseDocumentHandler,  IDocumentServerHandler,ICashTransaction
    {
        public DHCashierToCashier(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
            
        }
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, INTAPS.Accounting.AccountDocument doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            iERPTransactionBDE bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
            return bde.CashierPermissionGranted(userSession);
        }

        public override string GetHTML(INTAPS.Accounting.AccountDocument doc)
        {
            CashierToCashierDocument bdoc = (CashierToCashierDocument)doc;
            return "<b>Amount:<b/>" + TSConstants.FormatBirr(bdoc.amount);
        }

        public int Post(int AID, AccountDocument doc)
        {
            CashierToCashierDocument cashierToCashierDoc = (CashierToCashierDocument)doc;
            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            bde.assertNoSummaryCsAccount(cashierToCashierDoc.fromCashierAccountID);
            bde.assertNoSummaryCsAccount(cashierToCashierDoc.toCashierAccountID);
            creditSide = new TransactionOfBatch(cashierToCashierDoc.fromCashierAccountID, -cashierToCashierDoc.amount);
            debitSide = new TransactionOfBatch(cashierToCashierDoc.toCashierAccountID, cashierToCashierDoc.amount);
            TransactionOfBatch[] transactions = new TransactionOfBatch[] { debitSide, creditSide };
            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                if (cashierToCashierDoc.voucher != null)
                    cashierToCashierDoc.PaperRef = cashierToCashierDoc.voucher.reference;

                DocumentTypedReference[] typedRefs = cashierToCashierDoc.voucher == null ? new DocumentTypedReference[0] :
                   new DocumentTypedReference[] { cashierToCashierDoc.voucher };
                if (doc.AccountDocumentID == -1)
                    ret = bdeAccounting.RecordTransaction(AID, cashierToCashierDoc,
                    transactions
                    , true, typedRefs);
                else
                    ret = bdeAccounting.UpdateTransaction(AID, cashierToCashierDoc, transactions, true, typedRefs);
                if (!doc.scheduled)
                    if (!bde.AllowNegativeTransactionPost())
                    bdeAccounting.validateNegativeBalance(cashierToCashierDoc.fromCashierAccountID, 0, false, cashierToCashierDoc.DocumentDate
                        , "The transaction is blocked because the source cash doesn't have sufficient balance.");

                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch (Exception ex)
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw ex;
            }
        }
        public void ValidateCashAccount(CashierToCashierDocument cashierToCashierDoc)
        {
            if (cashierToCashierDoc.fromCashierAccountID!= -1)
            {
                double balance = GetCashAccountBalance(cashierToCashierDoc);
                if(balance<0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make cash account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No cash accounts found.\nPlease configure cash account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetCashAccountBalance(CashierToCashierDocument cashierToCashierdoc)
        {
            return bdeAccounting.GetNetBalanceAsOf(cashierToCashierdoc.fromCashierAccountID,0,cashierToCashierdoc.DocumentDate);
        }
        #endregion

        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            CashierToCashierDocument paymentDocument = doc as CashierToCashierDocument;
            if (paymentDocument == null)
                return null;

            if (!CashTransactionUtility.isCostCenterAccountInCostCenter(bde.Accounting, paymentDocument.toCashierAccountID, option.costCenters))
                return null;

            CashFlowItem item1 = CashFlowItem.createByText(paymentDocument.fromCashierAccountID, -1, "", -paymentDocument.amount);
            CashFlowItem item2 = CashFlowItem.createByText(paymentDocument.toCashierAccountID, -1, "", paymentDocument.amount);
            return new List<CashFlowItem>(new CashFlowItem[] { item1, item2 });
        }
        public string getInstrumentNo(AccountDocument _doc, out string typeName)
        {
            typeName = null;
            return null;
        }
    }
}
