using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    public class DHConvertFixedAssetToProperty : DeleteReverseDocumentHandler, IDocumentServerHandler
    {
        public DHConvertFixedAssetToProperty(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
          iERPTransactionBDE _bde = null;
        iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, AccountDocument doc, UserSessionData userSession)
        {
            return bde.CashierPermissionGranted(userSession);
        }
        public override void DeleteDocument(int AID, int docID)
        {
            ConvertFixedAssetToPropertyDocument convertDoc = bdeAccounting.GetAccountDocument<ConvertFixedAssetToPropertyDocument>(docID);
            if (convertDoc == null)
                return;
            base.DeleteDocument(AID, docID);
            foreach (ConvertFixedAssetToPropertyDocument.ConvertFixedAssetToPropertyItem item in convertDoc.items)
            {
                bde.deleteProperty(AID,item.property.id);
            }
            
        }
        public int Post(int AID,AccountDocument doc)
        {
            ConvertFixedAssetToPropertyDocument convertDoc = (ConvertFixedAssetToPropertyDocument)doc;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();

                if (convertDoc.AccountDocumentID == -1)
                {
                    foreach (ConvertFixedAssetToPropertyDocument.ConvertFixedAssetToPropertyItem item in convertDoc.items)
                    {
                        item.property.id = -1;
                        item.property.id = bde.createProperty(AID, item.property, item.propertyData);
                    }
                }
                else
                {
                    ConvertFixedAssetToPropertyDocument oldDocument = bdeAccounting.GetAccountDocument<ConvertFixedAssetToPropertyDocument>(convertDoc.AccountDocumentID);
                    bdeAccounting.DeleteAccountDocument(AID, convertDoc.AccountDocumentID, true);
                    Dictionary<int, Property> oldProps = new Dictionary<int, Property>();
                    foreach (ConvertFixedAssetToPropertyDocument.ConvertFixedAssetToPropertyItem item in oldDocument.items)
                        oldProps.Add(item.property.id, item.property);
                    foreach (ConvertFixedAssetToPropertyDocument.ConvertFixedAssetToPropertyItem item in convertDoc.items)
                    {
                        if (oldProps.ContainsKey(item.property.id))
                        {
                            Property oldProp = oldProps[item.property.id];
                            item.property.itemCode = oldProp.itemCode;
                            item.property.parentItemCode = oldProp.parentItemCode;
                            item.property.propertyCode = oldProp.propertyCode;
                            bde.updateProperty(AID, item.property, item.propertyData);
                            oldProps.Remove(item.property.id);
                        }
                        else
                        {
                            item.property.id = -1;
                            item.property.id = bde.createProperty(AID, item.property, item.propertyData);
                        }
                    }
                    foreach (KeyValuePair<int, Property> kv in oldProps)
                    {
                        bde.deleteProperty(AID, kv.Key);
                    }
                }
                List<TransactionOfBatch> trans = new List<TransactionOfBatch>();
                foreach (ConvertFixedAssetToPropertyDocument.ConvertFixedAssetToPropertyItem item in convertDoc.items)
                {
                    TransactionItems titem = bde.GetTransactionItems(item.property.parentItemCode);
                    if (!titem.IsFixedAssetItem)
                        throw new ServerUserMessage("Item {0} is not fixed asset item", item.property.parentItemCode);


                    object extraData;
                    TransactionItems propItem = bde.GetTransactionItems(bde.getProperty(item.property.id, out extraData).itemCode);
                    StoreInfo store = bde.GetStoreInfo(item.costCenterID);
                    double q = bde.Accounting.GetBalanceAsOf(item.costCenterID, titem.materialAssetAccountID(store != null), TransactionItem.MATERIAL_QUANTITY, doc.DocumentDate).DebitBalance;
                    if (AccountBase.AmountLess(q, 1))
                        throw new ServerUserMessage("At lease on item of {0} is need to convert it to property", titem.Code);
                    foreach (int aid in titem.getPrivateAccounts())
                        if (bde.Accounting.CostCenterAccountHasTransaction(bdeAccounting.GetCostCenterAccountID(item.costCenterID, aid), doc.tranTicks, -1))
                        {
                            throw new ServerUserMessage("It is not possible to convert item {0} to property it has transaction for date after or on {1}", item.property.parentItemCode, doc.DocumentDate);
                        }
                    double oval = bde.Accounting.GetBalanceAsOf(item.costCenterID, titem.materialAssetAccountID(store != null), TransactionItem.DEFAULT_CURRENCY, doc.DocumentDate).DebitBalance;
                    double acdep = bde.Accounting.GetBalanceAsOf(item.costCenterID, titem.accumulatedDepreciationAccountID, TransactionItem.DEFAULT_CURRENCY, doc.DocumentDate).DebitBalance;


                    trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, item.costCenterID, titem.materialAssetAccountID(store != null)).id
                        , TransactionItem.MATERIAL_QUANTITY, -1, "Convesion to property"));
                    trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, item.costCenterID, propItem.materialAssetAccountID(store != null)).id
                        , TransactionItem.MATERIAL_QUANTITY, 1, "Convesion to property")
                    );
                    if (!AccountBase.AmountEqual(oval, 0))
                    {
                        trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, item.costCenterID, titem.materialAssetAccountID(store != null)).id
                            , TransactionItem.DEFAULT_CURRENCY, -oval / q, "Convesion to property"));
                        trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, item.costCenterID, propItem.materialAssetAccountID(store != null)).id
                            , TransactionItem.DEFAULT_CURRENCY, oval / q, "Convesion to property"));
                    }
                    if (!AccountBase.AmountEqual(acdep, 0))
                    {
                        trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, item.costCenterID, titem.accumulatedDepreciationAccountID).id
                            , TransactionItem.DEFAULT_CURRENCY, -acdep / q, "Convesion to property"));
                        trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, item.costCenterID, propItem.accumulatedDepreciationAccountID).id
                            , TransactionItem.DEFAULT_CURRENCY, acdep / q, "Convesion to property"));
                    }
                }
                convertDoc.PaperRef = Guid.NewGuid().ToString();
                convertDoc.ShortDescription = "Conversion of fixed asset to property";
                int ret = bdeAccounting.RecordTransaction(AID, convertDoc,trans.ToArray());
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        private TransactionOfBatch[] CollectCashPaymentTransactionsOfBatch(int AID, WithdrawalDocument withDrawalDocument)
        {
            CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.bankServiceChargeByCash);
            if (csa == null)
                throw new ServerConfigurationError("The bank service charge by cash account not configured properly. The account does not belong to 'Head Quarter' cost center");

            int serviceChargeAccountID = csa.id;
            TransactionOfBatch[] transactions = null;

            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            int withdrawalAccountID = bde.SysPars.WithdrawalAccountID;
            CostCenterAccount csWithdrawal = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, withdrawalAccountID);
            if (csWithdrawal == null)
            {
                throw new ServerUserMessage("'Withdrawal Account' does not belong to 'Head Quarter' cost center. Please join the account to the cost center and try again!");
            }
            creditSide = new TransactionOfBatch(withDrawalDocument.assetAccountID, -withDrawalDocument.amount);
            debitSide = new TransactionOfBatch(csWithdrawal.id, withDrawalDocument.amount);

            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(withDrawalDocument.paymentMethod) && withDrawalDocument.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(withDrawalDocument.serviceChargeAmount, 0))
            {
                string note = withDrawalDocument.paymentMethod == BizNetPaymentMethod.CPOFromCash ? "CPO service charge" : "Bank transfer service charge";

                TransactionOfBatch debitSide2, creditSide2;
                debitSide2 = new TransactionOfBatch(serviceChargeAccountID, withDrawalDocument.serviceChargeAmount, note);
                creditSide2 = new TransactionOfBatch(withDrawalDocument.assetAccountID, -withDrawalDocument.serviceChargeAmount, note);
                transactions = new TransactionOfBatch[] { debitSide, creditSide, debitSide2, creditSide2 };
            }
            else
            {
                transactions = new TransactionOfBatch[] { debitSide, creditSide };
            }
            return transactions;
        }
        private TransactionOfBatch[] CollectBankPaymentTransactionsOfBatch(int AID, WithdrawalDocument withdrawalDocument)
        {
            TransactionOfBatch[] transactions = null;
            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            int withdrawalAccountID = bde.SysPars.WithdrawalAccountID;
            CostCenterAccount csWithdrawal = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, withdrawalAccountID);
            if (csWithdrawal == null)
            {
                throw new ServerUserMessage("'Withdrawal Account' does not belong to 'Head Quarter' cost center. Please join the account to the cost center and try again!");
            }

            BankAccountInfo ba = bde.GetBankAccount(withdrawalDocument.assetAccountID);
            int serviceChargeAccountID = ba.bankServiceChargeCsAccountID;
            string note = withdrawalDocument.paymentMethod == BizNetPaymentMethod.CPOFromBankAccount ? "CPO service charge" : "Bank transfer service charge";

            creditSide = new TransactionOfBatch(ba.mainCsAccount, -withdrawalDocument.amount);
            debitSide = new TransactionOfBatch(csWithdrawal.id, withdrawalDocument.amount);
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(withdrawalDocument.paymentMethod) && withdrawalDocument.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(withdrawalDocument.serviceChargeAmount, 0))
            {
                TransactionOfBatch debitSide2, creditSide2;
                debitSide2 = new TransactionOfBatch(serviceChargeAccountID, withdrawalDocument.serviceChargeAmount, note);
                creditSide2 = new TransactionOfBatch(ba.mainCsAccount, -withdrawalDocument.serviceChargeAmount, note);
                transactions = new TransactionOfBatch[] { debitSide, creditSide, debitSide2, creditSide2 };
            }
            else
            {
                transactions = new TransactionOfBatch[] { debitSide, creditSide };
            }
            return transactions;
        }
        public void ValidateAccounts(WithdrawalDocument withdrawalDocument, bool validateCash)
        {
            if (!validateCash) //validate bank
            {
                ValidateBankAccount(withdrawalDocument);
            }
            else
            {
                ValidateCashAccount(withdrawalDocument);
            }
        }

        private void ValidateBankAccount(WithdrawalDocument withdrawalDocument)
        {
            if (withdrawalDocument.assetAccountID != -1)
            {
                double balance = GetBankAccountBalance(withdrawalDocument);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make bank account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No bank accounts found.\nPlease configure bank account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetBankAccountBalance(WithdrawalDocument withdrawalDocument)
        {
            return bdeAccounting.GetNetBalanceAsOf(withdrawalDocument.assetAccountID, 0, withdrawalDocument.DocumentDate);
        }

        private void ValidateCashAccount(WithdrawalDocument withdrawalDocument)
        {
            if (withdrawalDocument.assetAccountID != -1)
            {
                double balance = GetCashAccountBalance(withdrawalDocument);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make cash account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No cash accounts found.\nPlease configure cash account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetCashAccountBalance(WithdrawalDocument withdrawalDocument)
        {
            return bdeAccounting.GetNetBalanceAsOf(withdrawalDocument.assetAccountID, 0, withdrawalDocument.DocumentDate);
        }
       
        #endregion

        
    }

}
