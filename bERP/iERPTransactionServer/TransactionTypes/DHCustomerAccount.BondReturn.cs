using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{

    public class DHBondReturn:DHCustomerAccount,INTAPS.Accounting.IDocumentServerHandler,ICashTransaction
    {
        public DHBondReturn(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
          
        #region IDocumentServerHandler Members



        public int Post(int AID, AccountDocument doc)
        {
            BondReturnDocument bondReturnDocument = (BondReturnDocument)doc;
            TradeRelation cst=bde.GetCustomer(bondReturnDocument.customerCode);
            

            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                BondPaymentDocument bondPaymentDoc = (BondPaymentDocument)bdeAccounting.GetAccountDocument(bondReturnDocument.bondPaymentDocumentID, true);
                if (bondPaymentDoc == null)
                {
                    doc.LongDescription = "Payment document lost";
                    bondReturnDocument.bondPaymentDocumentID = -1;
                }
                else
                {
                    if (bondReturnDocument.returnBond)
                    {
                        bondPaymentDoc.returned = true;
                    }
                    else
                    {
                        bondReturnDocument.bondPaymentDocumentID = -1;
                        bondPaymentDoc.returned = false;
                    }
                    bdeAccounting.UpdateAccountDocumentData(AID,bondPaymentDoc);
                }
                
                int ret = base.Post(AID, doc, cst.PaidBondAccountID, -1, bondReturnDocument.voucher);
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        #endregion

        public override void DeleteDocument(int AID, int docID)
        {
            BondReturnDocument doc = (BondReturnDocument)bdeAccounting.GetAccountDocument(docID, true);
            int bondPaymentDocID = doc.bondPaymentDocumentID;
            bdeAccounting.DeleteAccountDocument(AID, docID,false);
            BondPaymentDocument payDoc = (BondPaymentDocument)bdeAccounting.GetAccountDocument(bondPaymentDocID, true);
            payDoc.returned = false;
            bdeAccounting.UpdateAccountDocumentData(AID,payDoc);
        }



        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            BondReturnDocument paymentDocument = doc as BondReturnDocument;
            if (paymentDocument == null)
                return null;

            if (!CashTransactionUtility.isCostCenterAccountInCostCenter(bde.Accounting, option.costCenters, docTrans))
                return null;

            List<CashFlowItem> ret = new List<CashFlowItem>();
            ret.Add( CashFlowItem.createByText(paymentDocument.assetAccountID, -1, "", paymentDocument.amount));
            BondPaymentDocument bond = bde.Accounting.GetAccountDocument(paymentDocument.bondPaymentDocumentID, true) as BondPaymentDocument;
            
            if (bond != null)
            {
                TradeRelation customer = bde.GetRelation(bond.customerCode);
                if (customer != null)
                {
                    AccountTransaction tran = CashTransactionUtility.findTransactionByAccountID(bde.Accounting, docTrans, customer.PaidBondAccountID);
                    if (tran != null)
                    {
                        ret.Add(CashFlowItem.createByText(-1,tran.AccountID , "Bond Returned From Customer "+customer.NameCode, paymentDocument.amount));
                    }
                }
            }
            return ret;
        }
        public string getInstrumentNo(AccountDocument _doc, out string typeName)
        {
            BondReturnDocument doc = _doc as BondReturnDocument;
            if (!PaymentMethodHelper.IsPaymentMethodWithReference(doc.paymentMethod))
            {
                typeName = null;
                return null;
            }
            typeName = PaymentMethodHelper.GetPaymentTypeReferenceName(doc.paymentMethod);
            return PaymentMethodHelper.selectReference(doc.paymentMethod,doc.checkNumber,doc.cpoNumber,doc.slipReferenceNo);
        }
    }

}

