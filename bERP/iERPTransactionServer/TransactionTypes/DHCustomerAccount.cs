using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    
    public class DHCustomerAccount : DeleteReverseDocumentHandler
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHCustomerAccount(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        public virtual TransactionOfBatch[] generateTransactions
            (int AID,
             CustomerAccountDocument customerDocument
            , int otherAccountID
            , double transactionSign
            , out CostCenterAccount otherAccount)
        {

            otherAccount = bdeAccounting.GetOrCreateCostCenterAccount(AID,bde.SysPars.mainCostCenterID, otherAccountID);
            if (otherAccount == null)
                throw new ServerUserMessage("Problem with expense account designation, company cost center designation or customer accounts");

            TransactionOfBatch[] transactions = null;
            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            BankAccountInfo ba = null;
            if (PaymentMethodHelper.IsPaymentMethodBank(customerDocument.paymentMethod))
                ba = bde.GetBankAccount(customerDocument.assetAccountID);
            string note = customerDocument.paymentMethod == BizNetPaymentMethod.CPOFromBankAccount ? "CPO service charge" : "Bank transfer service charge";

            creditSide = new TransactionOfBatch(customerDocument.assetAccountID, -transactionSign * customerDocument.amount);
            debitSide = new TransactionOfBatch(otherAccount.id, transactionSign * customerDocument.amount);

            if (customerDocument.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(customerDocument.serviceChargeAmount, 0))
            {
                int serviceChargeAccountID = ba == null ?
                    bdeAccounting.GetOrCreateCostCenterAccount(AID,bde.SysPars.mainCostCenterID, bde.SysPars.bankServiceChargeByCash).id
                    : ba.bankServiceChargeCsAccountID;
                if (serviceChargeAccountID == -1)
                    throw new INTAPS.ClientServer.ServerUserMessage("Bank fee expense account not set");
                CostCenterAccount serviceChargeAccount = bdeAccounting.GetCostCenterAccount(serviceChargeAccountID);
                if (serviceChargeAccount == null)
                    throw new INTAPS.ClientServer.ServerUserMessage("Bank fee expense account not set");

                TransactionOfBatch debitSide2, creditSide2;
                debitSide2 = new TransactionOfBatch(serviceChargeAccount.id, customerDocument.serviceChargeAmount, note);
                creditSide2 = new TransactionOfBatch(customerDocument.assetAccountID, -customerDocument.serviceChargeAmount, note);
                transactions = new TransactionOfBatch[] { debitSide, creditSide, debitSide2, creditSide2 };
            }
            else
            {
                transactions = new TransactionOfBatch[] { debitSide, creditSide };
            }
            return transactions;
        }
        #region IDocumentServerHandler Members
        
        public virtual bool CheckPostPermission(int docID, AccountDocument doc, UserSessionData userSession)
        {
            return bde.CashierPermissionGranted(userSession);
        }

        public override string GetHTML(AccountDocument doc)
        {
            CustomerAccountDocument bdoc = (CustomerAccountDocument)doc;
            return "<b>Amount:<b/>" + TSConstants.FormatBirr(bdoc.amount);
        }
        public int Post(int AID, AccountDocument doc, int accountID, double sign, DocumentTypedReference voucher)
        {
            CustomerAccountDocument customerAccountDocument = (CustomerAccountDocument)doc;
            if (voucher != null)
                customerAccountDocument.PaperRef = voucher.reference;
            TransactionOfBatch[] transactions = null;
            CostCenterAccount outherCSAccount;
            transactions = generateTransactions(AID, customerAccountDocument, accountID, sign, out outherCSAccount);
            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                DocumentTypedReference[] typedRefs = voucher == null ? new DocumentTypedReference[0] :
                 new DocumentTypedReference[] { voucher };
                if (doc.AccountDocumentID == -1)
                    ret = bdeAccounting.RecordTransaction(AID, customerAccountDocument, transactions, true, typedRefs);
                else
                    ret = bdeAccounting.UpdateTransaction(AID, customerAccountDocument, transactions, true, typedRefs);

                if (!doc.scheduled && !bde.AllowNegativeTransactionPost())
                {
                    /*string notEnoughCustomerBalanceMessage;
                    if (outherCSAccount.creditAccount)
                        notEnoughCustomerBalanceMessage = "Insufficient customer payable to register this transaction";
                    else
                        notEnoughCustomerBalanceMessage = "Insufficient customer recievable to register this transaction";
                    string notEnoughAssetMessage;
                    if (PaymentMethodHelper.IsPaymentMethodCash(customerAccountDocument.paymentMethod))
                        notEnoughAssetMessage = "Insufficient cash to register this transcation";
                    else if (PaymentMethodHelper.IsPaymentMethodBank(customerAccountDocument.paymentMethod))
                        notEnoughAssetMessage = "Insuffient balance in bank account to register this transaction";
                    else
                        throw new ServerUserMessage("Unsupported payment method");
                    bdeAccounting.validateNegativeBalance(customerAccountDocument.assetAccountID, 0, false, customerAccountDocument.DocumentDate
                        , notEnoughAssetMessage);
                    bdeAccounting.validateNegativeBalance(outherCSAccount.id, 0, outherCSAccount.creditAccount, customerAccountDocument.DocumentDate
                        , notEnoughCustomerBalanceMessage);*/
                }
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        #endregion
    }
    public class DHCustomerPayable : DHCustomerAccount, IDocumentServerHandler
    {
        public DHCustomerPayable(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }

        #region IDocumentServerHandler Members

        public int Post(int AID, AccountDocument _doc)
        {
            lock (bde.WriterHelper)
            {
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    //delete existing settlment schedule documents
                    if (_doc.AccountDocumentID != -1)
                    {
                        CustomerPayableDocument oldDoc = bdeAccounting.GetAccountDocument(_doc.AccountDocumentID, true) as CustomerPayableDocument;
                        if (oldDoc.scheduledSettlement != null)
                        {
                            foreach (AccountDocument delDoc in oldDoc.scheduledSettlement)
                            {
                                //check document exists
                                if(bdeAccounting.DocumentExists(delDoc.AccountDocumentID))
                                    bdeAccounting.DeleteGenericDocument(AID, delDoc.AccountDocumentID);
                            }
                        }
                    }
                    CustomerPayableDocument doc = (CustomerPayableDocument)_doc;
                    //post new settlement schedule documents
                    if (doc.scheduledSettlement != null)
                    {
                        foreach (AccountDocument inv in doc.scheduledSettlement)
                        {
                            inv.AccountDocumentID = -1;
                            if (!(inv.scheduled && !inv.materialized))
                                throw new INTAPS.ClientServer.ServerUserMessage("One of the invoices are not scheduled");
                            inv.AccountDocumentID = bdeAccounting.PostGenericDocument(AID, inv);
                        }
                    }
                    //post the advance payment
                    TradeRelation customer = bde.GetCustomer(doc.customerCode);
                    DHSell2.autoCreateRelationAccountField(bde, AID, customer, "PayableAccountID");
                    int ret = base.Post(AID, _doc, customer.PayableAccountID,-1,doc.voucher);
                    bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        public override TransactionOfBatch[] generateTransactions(int AID, CustomerAccountDocument customerDocument, int otherAccountID, double transactionSign, out CostCenterAccount otherAccount)
        {
            otherAccount = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, otherAccountID);
            if (otherAccount == null)
                throw new ServerUserMessage("Problem with expense account designation, company cost center designation or customer accounts");
            List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            TransactionOfBatch otherCredit = null;
            TransactionOfBatch otherDebit = null;
            TransactionOfBatch debitWithholding = null;
            BankAccountInfo ba = null;
            if (PaymentMethodHelper.IsPaymentMethodBank(customerDocument.paymentMethod))
                ba = bde.GetBankAccount(customerDocument.assetAccountID);
            string note = customerDocument.paymentMethod == BizNetPaymentMethod.CPOFromBankAccount ? "CPO service charge" : "Bank transfer service charge";
            CustomerPayableDocument customerPayable = (CustomerPayableDocument)customerDocument;
            TradeRelation customer = bde.GetCustomer(customerDocument.customerCode);
            creditSide = new TransactionOfBatch(customerDocument.assetAccountID, -transactionSign * customerPayable.totalCash);
            tran.Add(creditSide);
            double paidWithholding, vat;
            paidWithholding = vat = 0;
            if (customerPayable.calculateVAT)
                vat = customerDocument.amount * customerPayable.taxRates.CommonVATRate;
            if (customerPayable.calculateWithholding)
                paidWithholding = customerDocument.amount * customerPayable.taxRates.WHServicesTaxableRate;
            double paidAmount = customerPayable.totalCash + paidWithholding;
            if (!customer.IsVatAgent)
                paidAmount = customerPayable.totalCash + paidWithholding - vat;
            debitSide = new TransactionOfBatch(otherAccount.id, transactionSign * paidAmount);
            tran.Add(debitSide);
            if (customerPayable.calculateVAT)
            {
                if (customer.IsVatAgent)
                {
                    otherCredit = new TransactionOfBatch(bdeAccounting.GetCostCenterAccount(bde.SysPars.mainCostCenterID, bde.SysPars.outPutVATAccountID).id, -vat);
                    otherDebit = new TransactionOfBatch(bdeAccounting.GetCostCenterAccount(bde.SysPars.mainCostCenterID, bde.SysPars.outputVATWithHeldAccountID).id, vat);
                    tran.AddRange(new TransactionOfBatch[] { otherCredit, otherDebit });
                }
                else
                {
                    otherCredit = new TransactionOfBatch(bdeAccounting.GetCostCenterAccount(bde.SysPars.mainCostCenterID, bde.SysPars.outPutVATAccountID).id, -customerDocument.amount * customerPayable.taxRates.CommonVATRate);
                    tran.Add(otherCredit);
                }
            }
            if (customerPayable.calculateWithholding)
            {
                debitWithholding = new TransactionOfBatch(bdeAccounting.GetCostCenterAccount(bde.SysPars.mainCostCenterID, bde.SysPars.paidWithHoldingTaxAccountID).id, paidWithholding);
                tran.Add(debitWithholding);
            }
            if (customerDocument.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(customerDocument.serviceChargeAmount, 0))
            {
                int serviceChargeAccountID = ba == null ?
                    bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.bankServiceChargeByCash).id
                    : ba.bankServiceChargeCsAccountID;
                if (serviceChargeAccountID == -1)
                    throw new INTAPS.ClientServer.ServerUserMessage("Bank fee expense account not set");
                CostCenterAccount serviceChargeAccount = bdeAccounting.GetCostCenterAccount(serviceChargeAccountID);
                if (serviceChargeAccount == null)
                    throw new INTAPS.ClientServer.ServerUserMessage("Bank fee expense account not set");

                TransactionOfBatch debitSide2, creditSide2;
                debitSide2 = new TransactionOfBatch(serviceChargeAccount.id, customerDocument.serviceChargeAmount, note);
                creditSide2 = new TransactionOfBatch(customerDocument.assetAccountID, -customerDocument.serviceChargeAmount, note);
                tran.AddRange(new TransactionOfBatch[] { debitSide2, creditSide2 });
            }

            return tran.ToArray();
        }

        public override void DeleteDocument(int AID, int docID)
        {
            lock (bdeAccounting.WriterHelper)
            {
                CustomerPayableDocument payable = bdeAccounting.GetAccountDocument(docID, true) as CustomerPayableDocument;

                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    base.DeleteDocument(AID, docID);
                    if (payable.scheduledSettlement != null)
                    {
                        foreach (AccountDocument doc in payable.scheduledSettlement)
                        {
                            if(bdeAccounting.DocumentExists(doc.AccountDocumentID))
                                bdeAccounting.DeleteGenericDocument(AID, doc.AccountDocumentID);
                        }
                    }
                    bdeAccounting.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        public override void ReverseDocument(int AID, int docID)
        {
            lock (bdeAccounting.WriterHelper)
            {
                CustomerPayableDocument payable = bdeAccounting.GetAccountDocument(docID, true) as CustomerPayableDocument;

                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    base.ReverseDocument(AID, docID);
                    if (payable.scheduledSettlement != null)
                    {
                        foreach (AccountDocument doc in payable.scheduledSettlement)
                        {
                            if (bdeAccounting.DocumentExists(doc.AccountDocumentID))
                                bdeAccounting.ReverseGenericDocument(AID, doc.AccountDocumentID);
                        }
                    }
                    bdeAccounting.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        #endregion
    }
    public class DHCustomerReceivable : DHCustomerAccount, IDocumentServerHandler
    {
        public DHCustomerReceivable(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {


        }

        #region IDocumentServerHandler Members

        public int Post(int AID, AccountDocument _doc)
        {
            CustomerReceivableDocument doc = (CustomerReceivableDocument)_doc;
            CustomerReceivableDocument recDoc = doc as CustomerReceivableDocument;
            TradeRelation customer = bde.GetCustomer(doc.customerCode);
            if (doc.retention)
            {
                DHSell2.autoCreateRelationAccountField(bde, AID, customer, "RetentionReceivableAccountID");
                return base.Post(AID, _doc, customer.RetentionReceivableAccountID , -1, recDoc.voucher);
            }
            else
            {
                DHSell2.autoCreateRelationAccountField(bde, AID, customer, "ReceivableAccountID");
                return base.Post(AID, _doc, customer.ReceivableAccountID, -1, recDoc.voucher);
            }
        }
        #endregion
    }
    public class DHCustomerAdvanceReturn : DHCustomerAccount, IDocumentServerHandler
    {
        public DHCustomerAdvanceReturn(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
            
        }
        public int Post(int AID,AccountDocument _doc)
        {
            CustomerAccountDocument doc = (CustomerAccountDocument)_doc;
            CustomerAdvanceReturnDocument cusDoc = doc as CustomerAdvanceReturnDocument;
            iERPTransactionBDE bde = (iERPTransactionBDE)ApplicationServer.GetBDE("iERP");
            TradeRelation customer = bde.GetCustomer(doc.customerCode);
            DHSell2.autoCreateRelationAccountField(bde, AID, customer, "PayableAccountID");
            return base.Post(AID, _doc, customer.PayableAccountID, 1, cusDoc.voucher);
        }
    }
    public class DHBondPayment : DHCustomerAccount, IDocumentServerHandler
    {
        public DHBondPayment(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {


        }

        #region IDocumentServerHandler Members

        public int Post(int AID, AccountDocument _doc)
        {
            CustomerAccountDocument doc = (CustomerAccountDocument)_doc;
            BondPaymentDocument bDoc = doc as BondPaymentDocument;
            TradeRelation customer = bde.GetCustomer(doc.customerCode);
            DHSell2.autoCreateRelationAccountField(bde, AID, customer, "PaidBondAccountID");
            return base.Post(AID, _doc, customer.PaidBondAccountID, 1, bDoc.voucher);
        }
        #endregion
    }

}

