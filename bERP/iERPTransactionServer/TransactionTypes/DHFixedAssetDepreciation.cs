﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;

using INTAPS.Accounting.BDE;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using System.Web;

namespace BIZNET.iERP.Server
{
    public partial class DHFixedAssetDepreciation : DHItemTransactionBase, IDocumentServerHandler
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHFixedAssetDepreciation(AccountingBDE act)
            : base(act)
        {
        }
        public bool CheckPostPermission(int docID, AccountDocument _doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return true;
        }

        public int Post(int AID, AccountDocument _doc)
        {
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    
                    bdeAccounting.WriterHelper.BeginTransaction();
                    if (_doc.AccountDocumentID != -1)
                        bdeAccounting.DeleteAccountDocument(AID, _doc.AccountDocumentID,true);
                    FixedAssetDepreciationDocument clientDoc = (FixedAssetDepreciationDocument)_doc;
                    if (clientDoc.items.Length == 0)
                        throw new ServerUserMessage("Empty depreciation document not allowed");
                    FixedAssetDepreciationDocument serverDoc = bde.previewDepreciation(clientDoc.DocumentDate);
                    if (clientDoc.verifyClientDocument)
                    {
                        if (clientDoc.items.Length != serverDoc.items.Length)
                            throw new ServerUserMessage("The depreciation displayed and the one calculated by the server are different.\nProbably the data has changed since you have dispalyed the current depreciation.");
                        for (int i = 0; i < clientDoc.items.Length; i++)
                            if (!AccountBase.AmountEqual(clientDoc.items[0].price, serverDoc.items[0].price))
                                throw new ServerUserMessage("The depreciation displayed and the one calculated by the server are different.\nProbably the data has changed since you have dispalyed the current depreciation.");
                    }
                    clientDoc.items = serverDoc.items;

                    List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
                    List<TransactionOfBatch> summeryTransactions = new List<TransactionOfBatch>();
                    foreach (TransactionDocumentItem ditem in clientDoc.items)
                    {
                        TransactionItems titem = bde.GetTransactionItems(ditem.code);
                        if (titem == null)
                            throw new ServerUserMessage("Invalid item code");
                        if (AccountBase.AmountGreater(ditem.price, 0))
                        {
                            //credit accumulated depreciation account
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, titem, "Accumulated Depreciation"
                            , ditem.costCenterID, titem.accumulatedDepreciationAccountID, titem.accumulatedDepreciationSummaryAccountID, 0
                             , -ditem.price, "");

                            //debit depreciation account
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, titem, "Depreciation Depreciation"
                            , ditem.costCenterID, titem.depreciationAccountID, titem.depreciationSummaryAccountID, 0
                             , ditem.price, "");

                            //tran.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID,ditem.costCenterID, titem.accumulatedDepreciationAccountID).id, 0, -ditem.price, "Deprecation"));
                            //tran.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, ditem.costCenterID, titem.depreciationAccountID).id, 0, ditem.price, "Deprecation"));
                        }
                        
                    }
                    addNetSummaryTransaction(AID, tran, "Net transaction of order " + clientDoc.ShortDescription, summeryTransactions);
                    clientDoc.DocumentDate = clientDoc.DocumentDate.AddSeconds(-70);
                    clientDoc.verifyClientDocument = false;
                    clientDoc.AccountDocumentID = bdeAccounting.RecordTransaction(AID, clientDoc, tran.ToArray());
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return clientDoc.AccountDocumentID;
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

        public double GetNetBalanceAsOf2(int costCenterID, int accountID, DateTime date)
        {
            CostCenterAccount csa = base.bdeAccounting.GetCostCenterAccount(costCenterID, accountID);
            if (csa == null)
                return 0;
            return base.bdeAccounting.GetNetBalanceAsOf(csa.id, TransactionItem.DEFAULT_CURRENCY, date);
        }

        double getBookValue(TransactionDocumentItem ditem, DateTime date)
        {
            TransactionItems item = bde.GetTransactionItems(ditem.code);
            double val = this.GetNetBalanceAsOf2(ditem.costCenterID, item.originalFixedAssetAccountID, date);
            val += this.GetNetBalanceAsOf2(ditem.costCenterID, item.accumulatedDepreciationAccountID, date);
            return val;

        }
        public override string GetHTML(AccountDocument _doc)
        {
            FixedAssetDepreciationDocument fixedAsset = (FixedAssetDepreciationDocument)_doc;
            StringBuilder sb = new StringBuilder();
            

            bERPHtmlTable table = new bERPHtmlTable();
            bERPHtmlTableRowGroup header = new bERPHtmlTableRowGroup(TableRowGroupType.header);
            table.groups.Add(header);
            bERPHtmlBuilder.htmlAddRow(header
                , new bERPHtmlTableCell(TDType.ColHeader, "No.")
                , new bERPHtmlTableCell(TDType.ColHeader, "Item Code")
                , new bERPHtmlTableCell(TDType.ColHeader, "Item Name")
                , new bERPHtmlTableCell(TDType.ColHeader, "Orginal Value")
                , new bERPHtmlTableCell(TDType.ColHeader, "Book Value")
                , new bERPHtmlTableCell(TDType.ColHeader, "Years")
                , new bERPHtmlTableCell(TDType.ColHeader, "Rate")
                , new bERPHtmlTableCell(TDType.ColHeader, "Depreciation")
                );
            bERPHtmlTableRowGroup body = new bERPHtmlTableRowGroup(TableRowGroupType.body);
            table.groups.Add(body);
            table.css = "itemTable";
            int no = 1;
            Array.Sort<TransactionDocumentItem>(fixedAsset.items,(x,y)=>x.code.CompareTo(y.code));
            
            foreach (TransactionDocumentItem item in fixedAsset.items)
            {
                
                TransactionItems titem = bde.GetTransactionItems(item.code);
                
                if (titem == null)
                {
                    bERPHtmlBuilder.htmlAddRow(body
                    , new bERPHtmlTableCell((no++).ToString())
                        , new bERPHtmlTableCell(item.code)
                        , new bERPHtmlTableCell("Item couldn't be loaded", 6)
                        );
                }
                else
                {
                    double currentBalance = this.bdeAccounting.GetNetBalanceAsOf( item.costCenterID, titem.originalFixedAssetAccountID, 0,fixedAsset.DocumentDate);
                    double bookValue=getBookValue(item,fixedAsset.DocumentDate);
                    bERPHtmlTableCell remarkCell;
                    
                    bERPHtmlBuilder.htmlAddRow(body
                        , new bERPHtmlTableCell((no++).ToString())
                        , new bERPHtmlTableCell(titem.Code)
                        , new bERPHtmlTableCell(titem.Name)
                        , new bERPHtmlTableCell(AccountBase.FormatAmount(currentBalance))
                        , new bERPHtmlTableCell(AccountBase.FormatAmount(bookValue))
                        , new bERPHtmlTableCell(AccountBase.FormatAmount(item.price/(item.rate*bookValue)))
                        , new bERPHtmlTableCell(item.rate.ToString("0.000"))
                        , new bERPHtmlTableCell(AccountBase.FormatAmount(item.price ))
                        );

                }
            }
            table.build(sb);
            
            return sb.ToString();
        }
        public override void DeleteDocument(int AID, int docID)
        {
            FixedAssetDepreciationDocument doc = bdeAccounting.GetAccountDocument(docID, true) as FixedAssetDepreciationDocument;
            if (doc == null)
                throw new ServerUserMessage("Fixed asset depreciation document not found in database.");

            if (bde.getDepreciationDocument(doc.DocumentDate) != -1)
                throw new ServerUserMessage("You can't delete this deprecation document because one or more depreciations are calculated after the one you are trying to delete.");

            base.DeleteDocument(AID, docID);

        }
    }
}
