﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using System.Web;

namespace BIZNET.iERP.Server
{
    public partial class DHFixedAssetDisposal : DHItemTransactionBase, IDocumentServerHandler
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHFixedAssetDisposal(AccountingBDE act)
            : base(act)
        {
        }
        public bool CheckPostPermission(int docID, AccountDocument _doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return true;
        }

        public int Post(int AID, AccountDocument _doc)
        {
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    
                    bdeAccounting.WriterHelper.BeginTransaction();
                    if (_doc.AccountDocumentID != -1)
                        bdeAccounting.DeleteAccountDocument(AID, _doc.AccountDocumentID,true);
                    FixedAssetDisposalDocument clientDoc = (FixedAssetDisposalDocument)_doc;
                    if (clientDoc.items.Length == 0)
                        throw new ServerUserMessage("Empty depreciation document not allowed");
                    List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
                    List<TransactionOfBatch> summeryTransactions = new List<TransactionOfBatch>();
                    HashSet<string> itemCodes=new HashSet<string>();
                    foreach (TransactionDocumentItem ditem in clientDoc.items)
                    {
                        if(itemCodes.Contains(ditem.code.ToUpper()))
                            throw new ServerUserMessage("Item {0} is repeated",ditem.code);
                        itemCodes.Add(ditem.code.ToUpper());
                        TransactionItems titem = bde.GetTransactionItems(ditem.code);
                        if (titem == null)
                            throw new ServerUserMessage("Invalid item code");
                        StoreInfo store=bde.GetStoreInfo(ditem.costCenterID);
                        double val = bdeAccounting.GetNetBalanceAsOf(ditem.costCenterID, titem.materialAssetAccountID(store!=null), 0,clientDoc.DocumentDate);
                        val += bdeAccounting.GetNetBalanceAsOf(ditem.costCenterID, titem.accumulatedDepreciationAccountID, 0, clientDoc.DocumentDate);
                        double q = bdeAccounting.GetNetBalanceAsOf(ditem.costCenterID, titem.materialAssetAccountID(store!=null), 1, clientDoc.DocumentDate);
                        if (!AccountBase.AmountEqual(q, ditem.quantity))
                        {
                            if (AccountBase.AmountEqual(q, 0))
                                throw new ServerUserMessage("Item {0} doesn't have sufficient balance", ditem.code);
                            if (!AccountBase.AmountGreater(ditem.quantity, 0))
                                throw new ServerUserMessage("Item {0} disposal quanity is invalid", ditem.code);
                            val = val * ditem.quantity / q;
                        }
                        if (AccountBase.AmountGreater(val, 0))
                        {
                            //credit accumulated depreciation account
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, titem, "Fixed Asset Disposal"
                            , ditem.costCenterID, titem.accumulatedDepreciationAccountID, titem.accumulatedDepreciationSummaryAccountID, 0
                             , -val, "");

                            //debit depreciation account
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, titem, "Fixed Asset Disposal"
                            , ditem.costCenterID, titem.depreciationAccountID, titem.depreciationSummaryAccountID, 0
                             , val, "");
                        }
                        if (AccountBase.AmountGreater(ditem.quantity, 0))
                        {
                            //credit accumulated depreciation account
                            tran.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, ditem.costCenterID, titem.materialAssetAccountID(store != null)).id, TransactionItem.MATERIAL_QUANTITY,
                                -ditem.quantity, "Fixed Asset Disposal"));

                            //debit depreciation account
                            tran.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, ditem.costCenterID, titem.depreciationAccountID).id, TransactionItem.MATERIAL_QUANTITY,
                                ditem.quantity, "Fixed Asset Disposal"));
                        }
                        
                    }
                    addNetSummaryTransaction(AID, tran, "Net transaction of order " + clientDoc.ShortDescription, summeryTransactions);
                    clientDoc.DocumentDate = clientDoc.DocumentDate.AddSeconds(-70);
                    clientDoc.AccountDocumentID = bdeAccounting.RecordTransaction(AID, clientDoc, tran.ToArray(),clientDoc.voucher);
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return clientDoc.AccountDocumentID;
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

        public double GetNetBalanceAsOf2(int costCenterID, int accountID, DateTime date)
        {
            CostCenterAccount csa = base.bdeAccounting.GetCostCenterAccount(costCenterID, accountID);
            if (csa == null)
                return 0;
            return base.bdeAccounting.GetNetBalanceAsOf(csa.id, TransactionItem.DEFAULT_CURRENCY, date);
        }

        double getBookValue(TransactionDocumentItem ditem, DateTime date)
        {
            TransactionItems item = bde.GetTransactionItems(ditem.code);
            double val = this.GetNetBalanceAsOf2(ditem.costCenterID, item.originalFixedAssetAccountID, date);
            val += this.GetNetBalanceAsOf2(ditem.costCenterID, item.accumulatedDepreciationAccountID, date);
            return val;

        }
        public override string GetHTML(AccountDocument _doc)
        {
            FixedAssetDisposalDocument disposalDoc = (FixedAssetDisposalDocument)_doc;
            StringBuilder sb = new StringBuilder();
            

            bERPHtmlTable table = new bERPHtmlTable();
            bERPHtmlTableRowGroup header = new bERPHtmlTableRowGroup(TableRowGroupType.header);
            table.groups.Add(header);
            bERPHtmlBuilder.htmlAddRow(header
                , new bERPHtmlTableCell(TDType.ColHeader, "No.")
                , new bERPHtmlTableCell(TDType.ColHeader, "Item Code")
                , new bERPHtmlTableCell(TDType.ColHeader, "Item Name")
                , new bERPHtmlTableCell(TDType.ColHeader, "Orginal Value")
                , new bERPHtmlTableCell(TDType.ColHeader, "Book Value")
                );
            bERPHtmlTableRowGroup body = new bERPHtmlTableRowGroup(TableRowGroupType.body);
            table.groups.Add(body);
            table.css = "itemTable";
            int no = 1;
            Array.Sort<TransactionDocumentItem>(disposalDoc.items,(x,y)=>x.code.CompareTo(y.code));
            
            foreach (TransactionDocumentItem item in disposalDoc.items)
            {
                
                TransactionItems titem = bde.GetTransactionItems(item.code);
                
                if (titem == null)
                {
                    bERPHtmlBuilder.htmlAddRow(body
                    , new bERPHtmlTableCell((no++).ToString())
                        , new bERPHtmlTableCell(item.code)
                        , new bERPHtmlTableCell("Item couldn't be loaded", 6)
                        );
                }
                else
                {
                    double currentBalance = this.bdeAccounting.GetNetBalanceAsOf( item.costCenterID, titem.originalFixedAssetAccountID, 0,disposalDoc.DocumentDate);
                    double bookValue=getBookValue(item,disposalDoc.DocumentDate);
                    
                    bERPHtmlBuilder.htmlAddRow(body
                        , new bERPHtmlTableCell((no++).ToString())
                        , new bERPHtmlTableCell(titem.Code)
                        , new bERPHtmlTableCell(titem.Name)
                        , new bERPHtmlTableCell(AccountBase.FormatAmount(currentBalance))
                        , new bERPHtmlTableCell(AccountBase.FormatAmount(bookValue))
                        );

                }
            }
            table.build(sb);
            
            return sb.ToString();
        }
        public override void DeleteDocument(int AID, int docID)
        {
            FixedAssetDepreciationDocument doc = bdeAccounting.GetAccountDocument(docID, true) as FixedAssetDepreciationDocument;
            if (doc == null)
                throw new ServerUserMessage("Fixed asset depreciation document not found in database.");

            if (bde.getDepreciationDocument(doc.DocumentDate) != -1)
                throw new ServerUserMessage("You can't delete this deprecation document because one or more depreciations are calculated after the one you are trying to delete.");

            base.DeleteDocument(AID, docID);

        }
    }
}
