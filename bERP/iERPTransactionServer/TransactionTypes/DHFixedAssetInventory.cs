﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.Accounting;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public partial class DHFixedAssetInventory : DHItemTransactionBase, IDocumentServerHandler
    {
        public DHFixedAssetInventory(AccountingBDE act)
            : base(act)
        {
        }
        public bool CheckPostPermission(int docID, AccountDocument _doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return true;
        }

        public int Post(int AID, AccountDocument _doc)
        {
            FixedAssetInventoryDocument doc = (FixedAssetInventoryDocument)_doc;
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    if (doc.voucher != null)
                        doc.PaperRef = doc.voucher.reference;
                    DocumentTypedReference[] typedRefs = doc.voucher == null ? new DocumentTypedReference[0] :
                new DocumentTypedReference[] { doc.voucher };

                    if (_doc.AccountDocumentID != -1)
                        bdeAccounting.DeleteAccountDocument(AID, _doc.AccountDocumentID, true);
                    CostCenterAccount csaInventoryGain = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.inventoryIncomeAccount);
                    CostCenterAccount csaInventoryLoss = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.inventoryExpenseAccount);
                    if (csaInventoryGain == null || csaInventoryLoss == null)
                        throw new ServerUserMessage("Inventory income account and expense accounts should be setup");
                    List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
                    List<TransactionOfBatch> summeryTransactions = new List<TransactionOfBatch>();
                    double diffQTotal = 0, diffOVTotal = 0, diffADTotal = 0;
                    //validate cost center /store
                    for (int i = 0; i < doc.items.Length; i++)
                    {
                        //validate repeated item
                        for (int j = i + 1; j < doc.items.Length; j++)
                        {
                            if (doc.items[i].itemCode.ToUpper() == doc.items[j].itemCode.ToUpper())
                                throw new ServerUserMessage("Please enter only unique items. Repeated Item code: " + doc.items[i].itemCode);
                        }
                        //validate item code
                        TransactionItems titem = bde.GetTransactionItems(doc.items[i].itemCode);
                        if (titem == null)
                            throw new ServerUserMessage("Invalid item code:" + doc.items[i].itemCode);
                        //validate item type
                        if (!titem.IsFixedAssetItem)
                            throw new ServerUserMessage("Only fixed asset items can be included in fixed asset inventory");
                        CostCenterAccount csaAsset = bdeAccounting.GetOrCreateCostCenterAccount(AID, doc.costCenterID, titem.originalFixedAssetAccountID);

                        double qBal = bdeAccounting.GetNetBalanceAsOf(csaAsset.id, TransactionItem.MATERIAL_QUANTITY, doc.DocumentDate);
                        double diff = doc.items[i].quantity - qBal;
                        diffQTotal += diff;
                        if (AccountBase.AmountGreater(diff, 0))
                        {
                            tran.Add(new TransactionOfBatch(csaAsset.id, TransactionItem.MATERIAL_QUANTITY, diff, "Inventory gain"));
                            //tran.Add(new TransactionOfBatch(csaInventoryGain.id, TransactionItem.MATERIAL_QUANTITY, -diff, "Inventory gain"));
                        }
                        else if(AccountBase.AmountLess(diff, 0))
                        {
                            tran.Add(new TransactionOfBatch(csaAsset.id, TransactionItem.MATERIAL_QUANTITY, diff, "Inventory loss"));
                            //tran.Add(new TransactionOfBatch(csaInventoryLoss.id, TransactionItem.MATERIAL_QUANTITY, -diff, "Inventory loss"));
                        }
                        double ovBal = bdeAccounting.GetNetBalanceAsOf(csaAsset.id, TransactionItem.DEFAULT_CURRENCY, doc.DocumentDate);
                        if (!AccountBase.AmountEqual(ovBal, doc.items[i].orginalValue))
                        {
                            diff = doc.items[i].orginalValue - ovBal;
                            diffOVTotal += diff;
                            if (AccountBase.AmountGreater(diff, 0))
                            {
                                addSummerizedItemTransaction(AID, tran, summeryTransactions, titem, "Fixed Asset Orginial Value Account"
                                        , doc.costCenterID, titem.originalFixedAssetAccountID, titem.originalFixedAssetSummaryAccountID, 0
                                    , diff, "Fixed Asset Inventory Gain");

                                //tran.Add(new TransactionOfBatch(csaInventoryGain.id, TransactionItem.DEFAULT_CURRENCY, -diff, "Fixed Asset Inventory Gain"));
                            }
                            else
                            {
                                addSummerizedItemTransaction(AID, tran, summeryTransactions, titem, "Fixed Asset Orginial Value Account"
                                , doc.costCenterID, titem.originalFixedAssetAccountID, titem.originalFixedAssetSummaryAccountID, 0
                                , diff, "Fixed Asset Inventory Loss");

                                //tran.Add(new TransactionOfBatch(csaInventoryLoss.id, TransactionItem.DEFAULT_CURRENCY, -diff, "Fixed Asset Inventory Loss"));
                            }
                        }

                        CostCenterAccount csaAcDep = bdeAccounting.GetOrCreateCostCenterAccount(AID, doc.costCenterID, titem.accumulatedDepreciationAccountID);

                        double acDepBal = bdeAccounting.GetNetBalanceAsOf(csaAcDep.id, TransactionItem.DEFAULT_CURRENCY, doc.DocumentDate);
                        if (!AccountBase.AmountEqual(acDepBal, -doc.items[i].accumulatedDepreciation))
                        {
                            diff = (-doc.items[i].accumulatedDepreciation) - acDepBal;
                            diffADTotal += diff;
                            if (AccountBase.AmountGreater(diff, 0))
                            {
                                addSummerizedItemTransaction(AID, tran, summeryTransactions, titem, "Fixed Asset Accumulated Depreciation Account"
                                        , doc.costCenterID, titem.accumulatedDepreciationAccountID, titem.accumulatedDepreciationSummaryAccountID, 0
                                    , diff, "Fixed Asset Inventory Gain");

                                //tran.Add(new TransactionOfBatch(csaInventoryGain.id, TransactionItem.DEFAULT_CURRENCY, -diff, "Fixed Asset Inventory Gain"));
                            }
                            else
                            {
                                addSummerizedItemTransaction(AID, tran, summeryTransactions, titem, "Fixed Asset Accumulated Depreciation Account"
                                , doc.costCenterID, titem.accumulatedDepreciationAccountID, titem.accumulatedDepreciationSummaryAccountID, 0
                                , diff, "Fixed Asset Inventory Loss");

                                //tran.Add(new TransactionOfBatch(csaInventoryLoss.id, TransactionItem.DEFAULT_CURRENCY, -diff, "Fixed Asset Inventory Loss"));
                            }
                        }
                    }

                    if (AccountBase.AmountGreater(diffQTotal, 0))
                    {
                        tran.Add(new TransactionOfBatch(csaInventoryGain.id, TransactionItem.MATERIAL_QUANTITY, -diffQTotal, "Inventory gain"));
                    }
                    else if (AccountBase.AmountLess(diffQTotal, 0))
                    {
                        tran.Add(new TransactionOfBatch(csaInventoryLoss.id, TransactionItem.MATERIAL_QUANTITY, -diffQTotal, "Inventory loss"));
                    }

                    if (AccountBase.AmountGreater(diffOVTotal, 0))
                    {
                        tran.Add(new TransactionOfBatch(csaInventoryGain.id, TransactionItem.DEFAULT_CURRENCY, -diffOVTotal, "Fixed Asset Inventory Gain"));
                    }
                    else if (AccountBase.AmountLess(diffOVTotal, 0))
                    {

                        tran.Add(new TransactionOfBatch(csaInventoryLoss.id, TransactionItem.DEFAULT_CURRENCY, -diffOVTotal, "Fixed Asset Inventory Loss"));
                    }

                    if (AccountBase.AmountGreater(diffADTotal, 0))
                    {
                        tran.Add(new TransactionOfBatch(csaInventoryGain.id, TransactionItem.DEFAULT_CURRENCY, -diffADTotal, "Fixed Asset Inventory Gain"));
                    }
                    else if (AccountBase.AmountLess(diffADTotal, 0))
                    {
                        tran.Add(new TransactionOfBatch(csaInventoryLoss.id, TransactionItem.DEFAULT_CURRENCY, -diffADTotal, "Fixed Asset Inventory Loss"));
                    }

                    addNetSummaryTransaction(AID, tran, "Inventory Transaction", summeryTransactions);
                    doc.AccountDocumentID = bdeAccounting.RecordTransaction(AID, doc, tran.ToArray(), typedRefs);
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return doc.AccountDocumentID;
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

        public override string GetHTML(AccountDocument _doc)
        {
            FixedAssetInventoryDocument doc = (FixedAssetInventoryDocument)_doc;
            return string.Format("Inventory as of {0}",System.Web.HttpUtility.HtmlEncode(doc.DocumentDate.ToString()));
        }
    }
}
