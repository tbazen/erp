using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    public class DHGroupPayroll : DeleteReverseDocumentHandler, IDocumentServerHandler,ICashTransaction
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHGroupPayroll(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, AccountDocument doc, UserSessionData userSession)
        {
             
            return bde.CashierPermissionGranted(userSession);
        }

        public override string GetHTML(AccountDocument doc)
        {
            GroupPayrollDocument bdoc = (GroupPayrollDocument)doc;
            string html= String.Format("<b>Income Tax:<b/>{0}<br><b>Employee Pension:<b/>{1}<br><b>Employer Pension:<b/>{2}<br><b>Total Salary:<b/>{3}", TSConstants.FormatBirr(bdoc.incomeTaxTotalAmount), TSConstants.FormatBirr(bdoc.employeesTotalPensionAmount), TSConstants.FormatBirr(bdoc.employerTotalPensionAmount), TSConstants.FormatBirr(bdoc.totalSalary));
            return html;
        }

        public int Post(int AID, AccountDocument doc)
        {
            GroupPayrollDocument groupPayrollDoc = (GroupPayrollDocument)doc;
            TransactionOfBatch[] transactions = CollectTransactionsOfBatch(AID, groupPayrollDoc);
            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                if (groupPayrollDoc.voucher != null)
                    groupPayrollDoc.PaperRef = groupPayrollDoc.voucher.reference;
                DocumentTypedReference[] typedRefs = groupPayrollDoc.voucher == null ? new DocumentTypedReference[0] :
                new DocumentTypedReference[] { groupPayrollDoc.voucher };

                if (doc.AccountDocumentID == -1)
                    ret = bdeAccounting.RecordTransaction(AID, groupPayrollDoc,
                    transactions
                    , true, typedRefs);
                else
                    ret = bdeAccounting.UpdateTransaction(AID, groupPayrollDoc, transactions, true, typedRefs);
                if (!doc.scheduled && !bde.AllowNegativeTransactionPost())
                {
                    bdeAccounting.validateNegativeBalance
                       (groupPayrollDoc.assetAccountID, 0, false, groupPayrollDoc.DocumentDate
                       , "The transaction is blcoked because it will cause negative asset balance.");
                }

                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        private TransactionOfBatch[] CollectTransactionsOfBatch(int AID, GroupPayrollDocument groupPayrollDoc)
        {
            PayrollBDE bdePayroll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
            List<TransactionOfBatch> trans = new List<TransactionOfBatch>();
            string note = "";
            BankAccountInfo ba = null;
            if (!groupPayrollDoc.fullUnclaimed)
            {
                if (PaymentMethodHelper.IsPaymentMethodBank(groupPayrollDoc.paymentMethod))
                    ba = bde.GetBankAccount(groupPayrollDoc.assetAccountID);
                note = groupPayrollDoc.paymentMethod == BizNetPaymentMethod.CPOFromBankAccount ? "CPO service charge" : "Bank transfer service charge";
            }
            if (groupPayrollDoc.declareTax)
                if (!groupPayrollDoc.fullUnclaimed)
                    trans.Add(new TransactionOfBatch(groupPayrollDoc.assetAccountID, -groupPayrollDoc.totalSalary, "Total Net Salary Paid"));
            if (Account.AmountGreater(groupPayrollDoc.incomeTaxTotalAmount, 0))
                trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, _bde.SysPars.mainCostCenterID, bdePayroll.SysPars.groupPayrollIncomeTaxAccount).id, -groupPayrollDoc.incomeTaxTotalAmount, "Total Income Tax Payable"));
            double totalPensionPayable = groupPayrollDoc.employeesTotalPensionAmount + groupPayrollDoc.employerTotalPensionAmount;
            double unclaimedSalary = groupPayrollDoc.unclaimedSalary;
            double totalExpense = 0;
            if (groupPayrollDoc.fullUnclaimed)
                totalExpense = groupPayrollDoc.incomeTaxTotalAmount + totalPensionPayable + unclaimedSalary - groupPayrollDoc.overTimeTotalAmount;
            else
                totalExpense = (groupPayrollDoc.totalSalary - groupPayrollDoc.overTimeTotalAmount) + groupPayrollDoc.incomeTaxTotalAmount + totalPensionPayable + unclaimedSalary;
            double overTimeExpense = groupPayrollDoc.overTimeTotalAmount;
            if (Account.AmountGreater(totalPensionPayable, 0))
                trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, _bde.SysPars.mainCostCenterID, bdePayroll.SysPars.groupPayrollPensionPayableAccount).id, -totalPensionPayable, "Employee and Employer pension contribution Payable"));
            if(Account.AmountGreater(unclaimedSalary,0))
                trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, _bde.SysPars.mainCostCenterID, bdePayroll.SysPars.groupPayrollUnclaimedSalaryAccount).id, -unclaimedSalary, "Employees Unclaimed Salary"));

            trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, groupPayrollDoc.costCenterID, groupPayrollDoc.accountAsCost ? bdePayroll.SysPars.groupPayrollCostAccount : bdePayroll.SysPars.groupPayrollExpenseAccount).id, totalExpense, "Salary Expense"));
            if (Account.AmountGreater(overTimeExpense, 0))
                trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, groupPayrollDoc.costCenterID, groupPayrollDoc.accountAsCost ? bdePayroll.SysPars.groupPayrollOverTimeCostAccount : bdePayroll.SysPars.groupPayrollOverTimeExpenseAccount).id, overTimeExpense, "Over Time Expense"));
            if (!groupPayrollDoc.declareTax) //pay taxes and salary
            {
                double totalPayable = groupPayrollDoc.totalSalary + groupPayrollDoc.incomeTaxTotalAmount + groupPayrollDoc.employeesTotalPensionAmount + groupPayrollDoc.employerTotalPensionAmount;
                trans.Add(new TransactionOfBatch(groupPayrollDoc.assetAccountID, -totalPayable, "Total Net Salary, Income Tax and Pension Contributions Paid"));
                if (Account.AmountGreater(groupPayrollDoc.incomeTaxTotalAmount, 0))
                    trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, _bde.SysPars.mainCostCenterID, bdePayroll.SysPars.groupPayrollIncomeTaxAccount).id, groupPayrollDoc.incomeTaxTotalAmount, "Total Income Tax Paid"));
                if (Account.AmountGreater(totalPensionPayable, 0))
                    trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, _bde.SysPars.mainCostCenterID, bdePayroll.SysPars.groupPayrollPensionPayableAccount).id, totalPensionPayable, "Total Pension Contributions Paid"));
            }

            if (!groupPayrollDoc.fullUnclaimed)
            {
                if (groupPayrollDoc.serviceChargePayer == ServiceChargePayer.Company
                    && AccountBase.AmountGreater(groupPayrollDoc.serviceChargeAmount, 0))
                {
                    int serviceChargeAccountID = ba == null ?
                        bdeAccounting.GetOrCreateCostCenterAccount(AID, groupPayrollDoc.costCenterID, bde.SysPars.bankServiceChargeByCash).id
                        : ba.bankServiceChargeCsAccountID;
                    if (serviceChargeAccountID == -1)
                        throw new INTAPS.ClientServer.ServerUserMessage("Bank fee expense account not set");
                    CostCenterAccount serviceChargeAccount = bdeAccounting.GetCostCenterAccount(serviceChargeAccountID);
                    if (serviceChargeAccount == null)
                        throw new INTAPS.ClientServer.ServerUserMessage("Bank fee expense account not set");

                    trans.Add(new TransactionOfBatch(serviceChargeAccount.id, groupPayrollDoc.serviceChargeAmount, note));
                    trans.Add(new TransactionOfBatch(groupPayrollDoc.assetAccountID, -groupPayrollDoc.serviceChargeAmount, note));
                }
            }
            return trans.ToArray();
        }

        #endregion

        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            GroupPayrollDocument paymentDoc = (GroupPayrollDocument)doc;
            if (PaymentMethodHelper.IsPaymentMethodCredit(paymentDoc.paymentMethod))
                return null;
            if (AccountBase.AmountEqual(paymentDoc.totalSalary,0))
                return null;
            
            AccountTransaction tran=CashTransactionUtility.findTransactionByAccountIDExtended(bde.Accounting,docTrans,bde.SysPars.expenseAccountID,bde.SysPars.costOfSalesAccountID);
            if(tran==null)
                return null;
            if (!CashTransactionUtility.isCostCenterAccountInCostCenter(bde.Accounting,tran.AccountID, option.costCenters))
                return null; 

            List<CashFlowItem> items = new List<CashFlowItem>();
            string html = System.Web.HttpUtility.HtmlEncode("Lump Payroll Payment : " + doc.ShortDescription);
            items.Add(new CashFlowItem(html, paymentDoc.totalSalary));

            CashTransactionUtility.addCashTransactionMainItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.totalSalary, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer, paymentDoc.checkNumber,paymentDoc.cpoNumber,paymentDoc.slipReferenceNo);
            CashTransactionUtility.addCashTransactionCostItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer);
            return items;
        }
        public string getInstrumentNo(AccountDocument _doc, out string typeName)
        {
            GroupPayrollDocument doc = _doc as GroupPayrollDocument;
            if (!PaymentMethodHelper.IsPaymentMethodWithReference(doc.paymentMethod))
            {
                typeName = null;
                return null;
            }
            typeName = PaymentMethodHelper.GetPaymentTypeReferenceName(doc.paymentMethod);
            return PaymentMethodHelper.selectReference(doc.paymentMethod,doc.checkNumber,doc.cpoNumber,doc.slipReferenceNo);
        }
    }
}
