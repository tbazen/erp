using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    public class DHGroupPayrollPayment : DeleteReverseDocumentHandler, IDocumentServerHandler,ICashTransaction
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHGroupPayrollPayment(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, AccountDocument doc, UserSessionData userSession)
        {
             
            return bde.CashierPermissionGranted(userSession);
        }

        public override string GetHTML(AccountDocument doc)
        {
            GroupPayrollPaymentDocument bdoc = (GroupPayrollPaymentDocument)doc;
            string html= String.Format("<b>Amount:<b/>{0}", TSConstants.FormatBirr(bdoc.amount));
            return html;
        }

        public int Post(int AID, AccountDocument doc)
        {
            GroupPayrollPaymentDocument groupPayrollPayDoc = (GroupPayrollPaymentDocument)doc;
            TransactionOfBatch[] transactions = CollectTransactionsOfBatch(AID, groupPayrollPayDoc);
            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                if (groupPayrollPayDoc.voucher != null)
                    groupPayrollPayDoc.PaperRef = groupPayrollPayDoc.voucher.reference;
                DocumentTypedReference[] typedRefs = groupPayrollPayDoc.voucher == null ? new DocumentTypedReference[0] :
                new DocumentTypedReference[] { groupPayrollPayDoc.voucher };

                if (doc.AccountDocumentID == -1)
                    ret = bdeAccounting.RecordTransaction(AID, groupPayrollPayDoc,
                    transactions
                    , true, typedRefs);
                else
                    ret = bdeAccounting.UpdateTransaction(AID, groupPayrollPayDoc, transactions, true, typedRefs);
                if (!doc.scheduled && !bde.AllowNegativeTransactionPost())
                {
                    bdeAccounting.validateNegativeBalance
                       (groupPayrollPayDoc.assetAccountID, 0, false, groupPayrollPayDoc.DocumentDate
                       , "The transaction is blcoked because it will cause negative asset balance.");
                }

                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        private TransactionOfBatch[] CollectTransactionsOfBatch(int AID, GroupPayrollPaymentDocument groupPayrollPayDoc)
        {
            PayrollBDE bdePayroll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
            List<TransactionOfBatch> trans = new List<TransactionOfBatch>();
            BankAccountInfo ba = null;
            if (PaymentMethodHelper.IsPaymentMethodBank(groupPayrollPayDoc.paymentMethod))
                ba = bde.GetBankAccount(groupPayrollPayDoc.assetAccountID);
            string note = groupPayrollPayDoc.paymentMethod == BizNetPaymentMethod.CPOFromBankAccount ? "CPO service charge" : "Bank transfer service charge";
            PayrollPeriod period = bdePayroll.GetPayPeriod(groupPayrollPayDoc.periodID);
            TaxCenter center = _bde.GetTaxCenter(groupPayrollPayDoc.taxCenter);
            if (Account.AmountGreater(groupPayrollPayDoc.amount, 0))
            {
                trans.Add(new TransactionOfBatch(groupPayrollPayDoc.assetAccountID, -groupPayrollPayDoc.amount, String.Format("Total Group Payroll Salary Paid for Period of {0} and Tax Center {1}", period.name, center.Name)));
                trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID,_bde.SysPars.mainCostCenterID, bdePayroll.SysPars.groupPayrollUnclaimedSalaryAccount).id, groupPayrollPayDoc.amount, String.Format("Total Group Payroll Salary Paid for Period of {0} and Tax Center {1}", period.name, center.Name)));
            }
           
            if (groupPayrollPayDoc.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(groupPayrollPayDoc.serviceChargeAmount, 0))
            {
                int serviceChargeAccountID = ba == null ?
                    bdeAccounting.GetOrCreateCostCenterAccount(AID,groupPayrollPayDoc.costCenterID, bde.SysPars.bankServiceChargeByCash).id
                    : ba.bankServiceChargeCsAccountID;
                if (serviceChargeAccountID == -1)
                    throw new INTAPS.ClientServer.ServerUserMessage("Bank fee expense account not set");
                CostCenterAccount serviceChargeAccount = bdeAccounting.GetCostCenterAccount(serviceChargeAccountID);
                if (serviceChargeAccount == null)
                    throw new INTAPS.ClientServer.ServerUserMessage("Bank fee expense account not set");

                trans.Add(new TransactionOfBatch(serviceChargeAccount.id, groupPayrollPayDoc.serviceChargeAmount, note));
                trans.Add(new TransactionOfBatch(groupPayrollPayDoc.assetAccountID, -groupPayrollPayDoc.serviceChargeAmount, note));
            }
            return trans.ToArray();
        }


        #endregion
        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            GroupPayrollPaymentDocument paymentDoc = (GroupPayrollPaymentDocument )doc;
            if (PaymentMethodHelper.IsPaymentMethodCredit(paymentDoc.paymentMethod))
                return null;
            
            AccountTransaction tran = CashTransactionUtility.findTransactionByAccountIDExtended(bde.Accounting, docTrans, bde.SysPars.expenseAccountID, bde.SysPars.costOfSalesAccountID);
            if (tran == null)
                return null;
            if (!CashTransactionUtility.isCostCenterAccountInCostCenter(bde.Accounting, tran.AccountID, option.costCenters))
                return null;

            List<CashFlowItem> items = new List<CashFlowItem>();
            string html = System.Web.HttpUtility.HtmlEncode("Lump Payroll Payment : " + doc.ShortDescription);
            items.Add(new CashFlowItem(html, paymentDoc.amount));
            CashTransactionUtility.addCashTransactionMainItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.amount, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer, paymentDoc.checkNumber, paymentDoc.cpoNumber, paymentDoc.slipReferenceNo);
            CashTransactionUtility.addCashTransactionCostItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer);
            return items;
        }
        public string getInstrumentNo(AccountDocument _doc, out string typeName)
        {
            GroupPayrollDocument doc = _doc as GroupPayrollDocument;
            if (!PaymentMethodHelper.IsPaymentMethodWithReference(doc.paymentMethod))
            {
                typeName = null;
                return null;
            }
            typeName = PaymentMethodHelper.GetPaymentTypeReferenceName(doc.paymentMethod);
            return PaymentMethodHelper.selectReference(doc.paymentMethod,doc.checkNumber,doc.cpoNumber,doc.slipReferenceNo);
        }
    }
}
