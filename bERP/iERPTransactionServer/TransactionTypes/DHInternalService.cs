﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.Accounting;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public partial class DHInternalService : DeleteReverseDocumentHandler, IDocumentServerHandler
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHInternalService(AccountingBDE act)
            : base(act)
        {
        }
        public bool CheckPostPermission(int docID, AccountDocument _doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return true;
        }
        
        public int Post(int AID, AccountDocument _doc)
        {
            InternalServiceDocument doc = (InternalServiceDocument)_doc;
            if (doc.sourceCostCenterID == doc.destinationCostCenterID)
                throw new ServerUserMessage("Source and destination stores must be the different");
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    if (doc.voucher != null)
                        doc.PaperRef = doc.voucher.reference;
                    DocumentTypedReference[] typedRefs = doc.voucher == null ? new DocumentTypedReference[0] :
                new DocumentTypedReference[] { doc.voucher };

                    if (_doc.AccountDocumentID != -1)
                        bdeAccounting.DeleteAccountDocument(AID, _doc.AccountDocumentID,true);
                    List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
                    TransactionDocumentItem[] docItems = TransactionDocumentItem.uniquefy(doc.items);
                    foreach (TransactionDocumentItem ditem in docItems)
                    {
                        if(AccountBase.AmountEqual( ditem.quantity,0))
                            continue;
                        if(AccountBase.AmountLess( ditem.quantity,0))
                            throw new ServerUserMessage("Nevagative quantity not allowed");

                        //validate item code
                        TransactionItems titem = bde.GetTransactionItems(ditem.code);
                        if (titem == null)
                            throw new ServerUserMessage("Invalid item code");
                        if (titem.IsInventoryItem )
                            throw new ServerUserMessage("Inventory items can't be used in internal servics. Use store transfer or store issue instead.");
                        //validate existance cost center accounts
                        CostCenterAccount csaIncome= bdeAccounting.GetOrCreateCostCenterAccount(AID,doc.sourceCostCenterID, titem.salesAccountID);
                        if (csaIncome == null)
                            throw new ServerUserMessage("Invalid sales account for the the item:" + titem.Code);
                        CostCenterAccount csaExpense = bdeAccounting.GetOrCreateCostCenterAccount(AID,doc.destinationCostCenterID, titem.expenseAccountID);
                        if (csaExpense == null)
                            throw new ServerUserMessage("Invalid destiantion account for the the item:" + titem.Code);
                        tran.Add(new TransactionOfBatch(csaIncome.id, -ditem.price, ""));
                        tran.Add(new TransactionOfBatch(csaExpense.id, ditem.price, ""));
                        CostCenterAccount csaInternalServicePayable = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.internalServicePayableAccountID);
                        if (csaInternalServicePayable == null)
                            throw new Exception("Internal service payable account is not configured properly");
                        CostCenterAccount csaInternalServiceReceivable = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.internalServiceReceivableAccountID);
                        if (csaInternalServiceReceivable == null)
                            throw new Exception("Internal service receivable account is not configured properly");

                    }
                    doc.AccountDocumentID = bdeAccounting.RecordTransaction(AID, doc, tran.ToArray(), typedRefs);
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return doc.AccountDocumentID;
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

        public override string GetHTML(AccountDocument _doc)
        {
            InternalServiceDocument doc = (InternalServiceDocument)_doc;
            double amount = 0;
            foreach (TransactionDocumentItem item in doc.items)
                amount += item.price;
            return System.Web.HttpUtility.HtmlEncode("Total internal service amount:"+AccountBase.FormatAmount(amount));
        }
    }
}
