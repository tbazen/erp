﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using System.Web;

namespace BIZNET.iERP.Server
{
    public partial class DHInventory : DHItemTransactionBase, IDocumentServerHandler
    {
        public DHInventory(AccountingBDE act)
            : base(act)
        {
        }
        public bool CheckPostPermission(int docID, AccountDocument _doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return true;
        }

        public int Post(int AID, AccountDocument _doc)
        {
            InventoryDocument doc = (InventoryDocument)_doc;
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    if (doc.voucher != null)
                        doc.PaperRef = doc.voucher.reference;
                    DocumentTypedReference[] typedRefs = doc.voucher == null ? new DocumentTypedReference[0] :
                new DocumentTypedReference[] { doc.voucher };

                    if (_doc.AccountDocumentID != -1)
                        bdeAccounting.DeleteAccountDocument(AID, _doc.AccountDocumentID,true);
                    CostCenterAccount csaInventoryGain = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.inventoryIncomeAccount);
                    CostCenterAccount csaInventoryLoss = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.inventoryExpenseAccount);
                    if (csaInventoryGain == null || csaInventoryLoss == null)
                        throw new ServerUserMessage("Inventory income account and expense accounts should be setup");
                    List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
                    List<TransactionOfBatch> summeryTransactions = new List<TransactionOfBatch>();
                    //validate cost center /store
                    StoreInfo st = bde.GetStoreInfo(doc.storeID);
                    if (doc.fixedAsset)
                    {
                        if (st != null)
                            throw new ServerUserMessage("Please select a none-store cost center");
                    }
                    else
                    {
                        if (st == null)
                            throw new ServerUserMessage("Please select a store cost center");
                    }
                    double totalDiff = 0;
                    double totalDiffQ = 0;
                    for (int i = 0; i < doc.items.Length; i++)
                    {
                        //validate repeated item
                        for (int j = i + 1; j < doc.items.Length; j++)
                        {
                            if (doc.items[i].code.ToUpper() == doc.items[j].code.ToUpper())
                                throw new ServerUserMessage("Please enter only unique items. Repeated Item code: "+doc.items[i].code);
                        }
                        //validate item code
                        TransactionItems titem = bde.GetTransactionItems(doc.items[i].code);
                        if (titem == null)
                            throw new ServerUserMessage("Invalid item code:"+doc.items[i].code);
                        //validate item type
                        if (doc.fixedAsset)
                            if (!titem.IsFixedAssetItem)
                                throw new ServerUserMessage("Only fixed asset items can be included in fixed asset inventory");
                        if (!doc.fixedAsset)
                        {
                            ItemCategory categ = bde.GetItemCategory(titem.categoryID);
                            if (!titem.IsInventoryItem)
                            {
                                if (categ.generalInventoryAccountPID == -1 && categ.finishedGoodsAccountPID == -1)
                                    throw new ServerUserMessage("Only inventory items can be included in store inventory");
                            }
                        }
                        int inventoryAccountID = titem.inventoryType == InventoryType.GeneralInventory ? titem.inventoryAccountID : titem.finishedGoodAccountID;
                        CostCenterAccount csaAsset = bdeAccounting.GetOrCreateCostCenterAccount(AID, doc.storeID, doc.fixedAsset ? titem.originalFixedAssetAccountID : inventoryAccountID);

                        double qBal = bdeAccounting.GetNetBalanceAsOf(csaAsset.id, TransactionItem.MATERIAL_QUANTITY, doc.DocumentDate);
                        //if (AccountBase.AmountEqual(qBal, doc.items[i].quantity))
                        //    continue;
                        double diff = doc.items[i].quantity - qBal;
                        totalDiffQ += diff;
                        if (AccountBase.AmountGreater(diff, 0))
                        {
                            tran.Add(new TransactionOfBatch(csaAsset.id, TransactionItem.MATERIAL_QUANTITY, diff, "Inventory gain"));
                        }
                        else
                        {
                            tran.Add(new TransactionOfBatch(csaAsset.id, TransactionItem.MATERIAL_QUANTITY, diff, "Inventory loss"));
                        }
                        double vBal = bdeAccounting.GetNetBalanceAsOf(csaAsset.id, TransactionItem.DEFAULT_CURRENCY, doc.DocumentDate);
                        if (AccountBase.AmountEqual(vBal, doc.items[i].price))
                            continue;
                        doc.items[i].price=doc.items[i].unitPrice * doc.items[i].quantity;
                        diff = doc.items[i].price - vBal;
                        totalDiff += diff;
                        
                        if (AccountBase.AmountGreater(diff, 0))
                        {
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, titem, "Asset Account"
                                    , doc.storeID, titem.materialAssetAccountID(!doc.fixedAsset), titem.materialAssetSummaryAccountID(!doc.fixedAsset), 0
                                , diff, "Inventory Gain");
                        }
                        else
                        {
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, titem, "Asset Account"
                            , doc.storeID, titem.materialAssetAccountID(!doc.fixedAsset), titem.materialAssetSummaryAccountID(!doc.fixedAsset), 0
                            , diff, "Inventory Loss");
                        }
                    }

                    if (AccountBase.AmountGreater(totalDiffQ, 0))
                    {
                        tran.Add(new TransactionOfBatch(csaInventoryGain.id, TransactionItem.MATERIAL_QUANTITY, -totalDiffQ, "Inventory gain"));
                    }
                    else if(AccountBase.AmountLess(totalDiffQ, 0))
                    {
                        tran.Add(new TransactionOfBatch(csaInventoryLoss.id, TransactionItem.MATERIAL_QUANTITY, -totalDiffQ, "Inventory loss"));
                    }
                    if (AccountBase.AmountGreater(totalDiff, 0))
                    {
                        tran.Add(new TransactionOfBatch(csaInventoryGain.id, TransactionItem.DEFAULT_CURRENCY, -totalDiff, "Inventory gain"));
                    }
                    else if(AccountBase.AmountLess(totalDiff, 0))
                    {
                        tran.Add(new TransactionOfBatch(csaInventoryLoss.id, TransactionItem.DEFAULT_CURRENCY, -totalDiff, "Inventory loss"));
                    }
                    addNetSummaryTransaction(AID, tran, "Inventory Transaction", summeryTransactions);
                    doc.AccountDocumentID = bdeAccounting.RecordTransaction(AID, doc, tran.ToArray(), typedRefs);
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return doc.AccountDocumentID;
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

        public override string GetHTML(AccountDocument _doc)
        {
            InventoryDocument inventory = (InventoryDocument)_doc;
            StringBuilder sb = new StringBuilder();
            sb.Append("<div>");
            sb.Append(string.Format("<div class='accountDocumentDate'>{0}</div>", HttpUtility.HtmlEncode("Date:" + AccountBase.FormatDate(_doc.DocumentDate))));
            if (inventory.voucher!= null)
                sb.Append(string.Format("<div class='accountDocumentVoucher'>{0}</div>", HttpUtility.HtmlEncode("Reference:" + inventory.voucher.reference)));
            
            DocumentSerial[] otherRefs = bdeAccounting.getDocumentSerials(_doc.AccountDocumentID);

            bERPHtmlTable table = new bERPHtmlTable();
            bERPHtmlTableRowGroup header = new bERPHtmlTableRowGroup(TableRowGroupType.header);
            table.groups.Add(header);
            bERPHtmlBuilder.htmlAddRow(header
                , new bERPHtmlTableCell(TDType.ColHeader, "No.")
                , new bERPHtmlTableCell(TDType.ColHeader, "Item Code")
                , new bERPHtmlTableCell(TDType.ColHeader, "Item Name")
                , new bERPHtmlTableCell(TDType.ColHeader, "Quantity")
                , new bERPHtmlTableCell(TDType.ColHeader, "Unit")
                , new bERPHtmlTableCell(TDType.ColHeader, "Unit Cost")
                , new bERPHtmlTableCell(TDType.ColHeader, "Cost")
                , new bERPHtmlTableCell(TDType.ColHeader, "Remark")
                );
            bERPHtmlTableRowGroup body = new bERPHtmlTableRowGroup(TableRowGroupType.body);
            table.groups.Add(body);
            table.css = "itemTable";
            double total = 0;
            int no = 1;
            foreach (TransactionDocumentItem item in inventory.items)
            {
                TransactionItems titem = bde.GetTransactionItems(item.code);
                if (titem == null)
                {
                    bERPHtmlBuilder.htmlAddRow(body
                    , new bERPHtmlTableCell((no++).ToString())
                        , new bERPHtmlTableCell(item.code)
                        , new bERPHtmlTableCell("Item couldn't be loaded", 6)
                        );

                }
                else
                {
                    string remark = null;
                    if (titem.IsInventoryItem)
                    {
                        if (item.directExpense)
                            remark = "Direct Expense";

                    }
                    else if (item.prepaid)
                        remark = (remark == null ? "" : remark + "\n") + "Pre paid";

                    bERPHtmlTableCell remarkCell;
                    bERPHtmlBuilder.htmlAddRow(body
                        , new bERPHtmlTableCell((no++).ToString())
                        , new bERPHtmlTableCell(titem.Code)
                        , new bERPHtmlTableCell(titem.Name)
                        , new bERPHtmlTableCell(AccountBase.FormatAmount(item.quantity))
                        , new bERPHtmlTableCell(titem.MeasureUnitID == -1 ? "" : bde.GetMeasureUnit(titem.MeasureUnitID).Name)
                        , new bERPHtmlTableCell(AccountBase.FormatAmount(item.unitPrice))
                        , new bERPHtmlTableCell(AccountBase.FormatAmount(item.unitPrice * item.quantity))
                        , remarkCell = new bERPHtmlTableCell("")
                        );
                    remarkCell.innerText = remark;
                    total += item.unitPrice * item.quantity;

                }
            }
            table.build(sb);
            sb.Append(string.Format("<div class='invoiceTotalItem'>{0}</div>", HttpUtility.HtmlEncode("Total Cost: " + AccountBase.FormatAmount(total))));
            sb.Append("</div>");
            return sb.ToString();
        }
    }
}
