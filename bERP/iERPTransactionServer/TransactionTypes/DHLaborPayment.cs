using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    public class DHLaborPayment : DeleteReverseDocumentHandler, IDocumentServerHandler,ICashTransaction
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHLaborPayment(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, AccountDocument doc, UserSessionData userSession)
        {
             
            return bde.CashierPermissionGranted(userSession);
        }

        public override string GetHTML(AccountDocument doc)
        {
            LaborPaymentDocument bdoc = (LaborPaymentDocument)doc;
            return "<b>Amount:<b/>" + TSConstants.FormatBirr(bdoc.amount);
        }

        public int Post(int AID, AccountDocument doc)
        {
            LaborPaymentDocument laborPaymentDoc = (LaborPaymentDocument)doc;
            TransactionOfBatch[] transactions = null;
            bool validateCash = false;
            switch (laborPaymentDoc.paymentMethod)
            {
                case BizNetPaymentMethod.Cash:
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.BankTransferFromCash:
                    transactions = CollectCashPaymentTransactionsOfBatch(AID, laborPaymentDoc);
                    validateCash = true;
                    break;

                case BizNetPaymentMethod.Check:
                case BizNetPaymentMethod.CPOFromBankAccount:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    transactions = CollectBankPaymentTransactionsOfBatch(AID, laborPaymentDoc);
                    validateCash = false;
                    break;
                case BizNetPaymentMethod.None:
                    break;
                default:
                    break;
            }

            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                if (laborPaymentDoc.voucher == null)
                    throw new ServerUserMessage("Please provide reference");
                laborPaymentDoc.PaperRef = laborPaymentDoc.voucher.reference;
                if (doc.AccountDocumentID == -1)
                    ret = bdeAccounting.RecordTransaction(AID, laborPaymentDoc,
                    transactions
                    , true, laborPaymentDoc.voucher);
                else
                    ret = bdeAccounting.UpdateTransaction(AID, laborPaymentDoc, transactions, true,laborPaymentDoc.voucher);
                if (!doc.scheduled && !bde.AllowNegativeTransactionPost())
                {
                    string account = validateCash ? "cash account" : "bank account";
                    bdeAccounting.validateNegativeBalance
                       (laborPaymentDoc.assetAccountID, 0, false, laborPaymentDoc.DocumentDate
                       , "Transaction is blocked because there is no sufficient balance in the " + account);
                }

              //  ValidateAccounts(laborPaymentDoc, validateCash);
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        private TransactionOfBatch[] CollectCashPaymentTransactionsOfBatch(int AID,LaborPaymentDocument laborPaymentDoc)
        {
            TransactionOfBatch[] transactions = null;

            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            TransactionOfBatch creditSide2;
            iERPTransactionBDE iERPTransactionBDE = (iERPTransactionBDE)ApplicationServer.GetBDE("iERP");

            int laborExpenseAccountID = bde.SysPars.laborExpenseAccountID;
            CostCenterAccount csExpense = bdeAccounting.GetOrCreateCostCenterAccount(AID, laborPaymentDoc.costCenterID, laborExpenseAccountID);
            if (csExpense == null)
                throw new ServerUserMessage("'Labor Expense' account does not belong to '" + bdeAccounting.GetAccount<CostCenter>(laborPaymentDoc.costCenterID).Name + "' cost center");
            int laborIncomeTaxAccountID = bde.SysPars.laborIncomeTaxAccountID;
            CostCenterAccount csIncome = bdeAccounting.GetOrCreateCostCenterAccount(AID, laborPaymentDoc.costCenterID, laborIncomeTaxAccountID);
            if (csIncome == null)
                throw new ServerUserMessage("'Labor Income tax' account does not belong to '" + bdeAccounting.GetAccount<CostCenter>(laborPaymentDoc.costCenterID).Name + "' cost center");

            double incomeTax = CalculateIncomeTax(laborPaymentDoc.amount);

            debitSide = new TransactionOfBatch(csExpense.id, laborPaymentDoc.amount);
            creditSide = new TransactionOfBatch(laborPaymentDoc.assetAccountID, -(laborPaymentDoc.amount - incomeTax));
            creditSide2 = new TransactionOfBatch(csIncome.id, -incomeTax);
            if (!AccountBase.AmountEqual(incomeTax, 0))
                transactions = new TransactionOfBatch[] { debitSide, creditSide, creditSide2 };
            else
                transactions = new TransactionOfBatch[] { debitSide, creditSide };

            return transactions;
        }
        private TransactionOfBatch[] CollectBankPaymentTransactionsOfBatch(int AID,LaborPaymentDocument laborPaymentDoc)
        {
            TransactionOfBatch[] transactions = null;

            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            TransactionOfBatch creditSide2;
            iERPTransactionBDE iERPTransactionBDE = (iERPTransactionBDE)ApplicationServer.GetBDE("iERP");
            BankAccountInfo ba = bde.GetBankAccount(laborPaymentDoc.assetAccountID);
            string note = laborPaymentDoc.paymentMethod == BizNetPaymentMethod.CPOFromBankAccount ? "CPO service charge" : "Bank transfer service charge";

            int laborExpenseAccountID = bde.SysPars.laborExpenseAccountID;
            CostCenterAccount csExpense = bdeAccounting.GetOrCreateCostCenterAccount(AID, laborPaymentDoc.costCenterID, laborExpenseAccountID);
            if (csExpense == null)
                throw new ServerUserMessage("'Labor Expense' account does not belong to '" + bdeAccounting.GetAccount<CostCenter>(laborPaymentDoc.costCenterID).Name + "' cost center");
            int laborIncomeTaxAccountID = bde.SysPars.laborIncomeTaxAccountID;
            CostCenterAccount csIncome = bdeAccounting.GetOrCreateCostCenterAccount(AID, laborPaymentDoc.costCenterID, laborIncomeTaxAccountID);
            if (csIncome == null)
                throw new ServerUserMessage("'Labor Income tax' account does not belong to '" + bdeAccounting.GetAccount<CostCenter>(laborPaymentDoc.costCenterID).Name + "' cost center");

            double incomeTax = CalculateIncomeTax(laborPaymentDoc.amount);

            debitSide = new TransactionOfBatch(csExpense.id, laborPaymentDoc.amount);
            creditSide = new TransactionOfBatch(ba.mainCsAccount, -(laborPaymentDoc.amount - incomeTax));
            creditSide2 = new TransactionOfBatch(csIncome.id, -incomeTax);
            if (!AccountBase.AmountEqual(incomeTax, 0))
                transactions = new TransactionOfBatch[] { debitSide, creditSide, creditSide2 };
            else
                transactions = new TransactionOfBatch[] { debitSide, creditSide};

            return transactions;
        }
        public static double CalculateIncomeTax(double grossSalary)
        {
            double incomeTax = 0;
            if (grossSalary > 150 && grossSalary <= 650)
                incomeTax = grossSalary * 0.1 - 15;
            else if (grossSalary > 650 && grossSalary <= 1400)
                incomeTax = grossSalary * 0.15 - 47.50;
            else if (grossSalary > 1400 && grossSalary <= 2350)
                incomeTax = grossSalary * 0.2 - 117.50;
            else if (grossSalary > 2350 && grossSalary <= 3550)
                incomeTax = grossSalary * 0.25 - 235;
            else if (grossSalary > 3550 && grossSalary <= 5000)
                incomeTax = grossSalary * 0.3 - 412.50;
            else if (grossSalary > 5000)
                incomeTax = grossSalary * 0.35 - 662;

            return incomeTax;
        }
        public void ValidateAccounts(LaborPaymentDocument laborPaymentDoc, bool validateCash)
        {
            if (!validateCash) //validate bank
            {
                ValidateBankAccount(laborPaymentDoc);
            }
            else
            {
                ValidateCashAccount(laborPaymentDoc);
            }
        }

        private void ValidateBankAccount(LaborPaymentDocument laborPaymentDoc)
        {
            if (laborPaymentDoc.assetAccountID != -1)
            {
                double balance = GetBankAccountBalance(laborPaymentDoc);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make bank account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No bank accounts found.\nPlease configure bank account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetBankAccountBalance(LaborPaymentDocument laborPaymentDoc)
        {
            return bdeAccounting.GetNetBalanceAsOf(laborPaymentDoc.assetAccountID, 0, laborPaymentDoc.DocumentDate);
        }

        private void ValidateCashAccount(LaborPaymentDocument laborPaymentDoc)
        {
            if (laborPaymentDoc.assetAccountID != -1)
            {
                double balance = GetCashAccountBalance(laborPaymentDoc);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make cash account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No cash accounts found.\nPlease configure cash account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetCashAccountBalance(LaborPaymentDocument laborPaymentDoc)
        {
            return bdeAccounting.GetNetBalanceAsOf(laborPaymentDoc.assetAccountID, 0, laborPaymentDoc.DocumentDate);
        }

        #endregion
        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            LaborPaymentDocument paymentDoc = (LaborPaymentDocument)doc;
            if (PaymentMethodHelper.IsPaymentMethodCredit(paymentDoc.paymentMethod))
                return null;
            if (!CashTransactionUtility.isTransactionInCostCenter(bde.Accounting, paymentDoc.AccountDocumentID, option.costCenters, true))
                return null;
            List<CashFlowItem> items = new List<CashFlowItem>();
            string html = System.Web.HttpUtility.HtmlEncode("Labour Payment to "+paymentDoc.paidTo);
            items.Add(new CashFlowItem(html, paymentDoc.amount));
            CashTransactionUtility.addCashTransactionMainItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.amount,0,ServiceChargePayer.Company, paymentDoc.checkNumber, null,null);
            return items;
        }
        public string getInstrumentNo(AccountDocument _doc, out string typeName)
        {
            LaborPaymentDocument doc = _doc as LaborPaymentDocument;
            if (!PaymentMethodHelper.IsPaymentMethodWithReference(doc.paymentMethod))
            {
                typeName = null;
                return null;
            }
            typeName = PaymentMethodHelper.GetPaymentTypeReferenceName(doc.paymentMethod);
            return PaymentMethodHelper.selectReference(doc.paymentMethod, doc.checkNumber, doc.checkNumber, doc.slipReferenceNo);
        }
    }
}
