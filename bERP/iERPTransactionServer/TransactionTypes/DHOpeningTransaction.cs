using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    public class DHOpeningTransaction : DeleteReverseDocumentHandler, IDocumentServerHandler
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }


        public DHOpeningTransaction(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }


        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, AccountDocument doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            OpeningTransactionDocument theDoc = (OpeningTransactionDocument)doc;
            return bde.ConfigurationPermissionGranted(userSession);
        }

        public override string GetHTML(AccountDocument doc)
        {
            OpeningTransactionDocument theDoc = (OpeningTransactionDocument)doc;
            return "Opening Transaction Document";
        }

        public int Post(int AID,AccountDocument doc)
        {
            OpeningTransactionDocument theDoc = (OpeningTransactionDocument)doc;
            List<TransactionOfBatch> financeTransaction = new List<TransactionOfBatch>();
            CollectCashAccountTransactions(theDoc, financeTransaction);
            CollectBankAccountTransactions(theDoc, financeTransaction);
            CollectCapitalAccountTransactions(theDoc, financeTransaction);
            CollectAccumulatedVATAccountTransactions(theDoc, financeTransaction);
            CollectStaffLoanAccountTransactions(theDoc, financeTransaction);
            InitializeTransactionType(financeTransaction);
            return PostTransaction(AID,doc, financeTransaction);
        }

        private static void CollectCashAccountTransactions(OpeningTransactionDocument theDoc
        , List<TransactionOfBatch> financeTransaction)
        {

            for (int i = 0; i < theDoc.cashOnHandsAccountID.Length; i++)
            {
                financeTransaction.Add(new TransactionOfBatch(theDoc.cashOnHandsAccountID[i], theDoc.cashOnHands[i], "Openning"));
            }
        }

        private void CollectBankAccountTransactions(OpeningTransactionDocument theDoc
        , List<TransactionOfBatch> financeTransaction)
        {
            for (int i = 0; i < theDoc.bankAccounts.Length; i++)
            {
                BankAccountInfo ba = bde.GetBankAccount(theDoc.bankAccounts[i]);
                if (theDoc.bankBalances[i] != 0)
                    financeTransaction.Add(new TransactionOfBatch(ba.mainCsAccount, theDoc.bankBalances[i], "Openning"));
            }
        }

        private static void CollectCapitalAccountTransactions(OpeningTransactionDocument theDoc
        , List<TransactionOfBatch> financeTransaction)
        {
            for (int i = 0; i < theDoc.capitalAccountID.Length; i++)
            {
                financeTransaction.Add(new TransactionOfBatch(theDoc.capitalAccountID[i], -theDoc.capitals[i], "Openning"));
            }
        }

        private void CollectAccumulatedVATAccountTransactions(OpeningTransactionDocument theDoc, List<TransactionOfBatch> financeTransaction)
        {
            iERPTransactionBDE bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
            financeTransaction.Add(new TransactionOfBatch(bde.SysPars.accumulatedReceivableVATAccountID, theDoc.accumulatedVATBalance, "Openning"));
        }
        private void CollectStaffLoanAccountTransactions(OpeningTransactionDocument theDoc, List<TransactionOfBatch> financeTransaction)
        {
            for (int i = 0; i < theDoc.staffLoanAccountID.Length; i++)
            {
                financeTransaction.Add(new TransactionOfBatch(theDoc.staffLoanAccountID[i], theDoc.staffLoans[i], "Openning"));
            }

        }
        private static void InitializeTransactionType(
         List<TransactionOfBatch> financeTransaction)
        {
            foreach (TransactionOfBatch tt in financeTransaction)
            {
                tt.TransactionType = TransactionType.Opening;
            }
        }

        private int PostTransaction(int AID, AccountDocument doc
        , List<TransactionOfBatch> financeTransaction)
        {
            INTAPS.RDBMS.SQLHelper writer = bdeAccounting.WriterHelper;
            lock (writer)
            {
                writer.BeginTransaction();
                try
                {
                    int ret;
                    if (doc.AccountDocumentID != -1)
                        this.DeleteDocument(AID, doc.AccountDocumentID);
                    ret = bdeAccounting.RecordTransaction(AID, doc, financeTransaction.ToArray(), true, true);
                    writer.CommitTransaction();
                    return ret;
                }
                catch
                {
                    writer.RollBackTransaction();
                    throw;
                }
            }
        }

        #endregion
    }
}
