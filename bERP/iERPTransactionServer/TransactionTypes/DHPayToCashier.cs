using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    public class DHPayToCashier : DeleteReverseDocumentHandler, IDocumentServerHandler,ICashTransaction
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHPayToCashier(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, AccountDocument doc, UserSessionData userSession)
        {
             
            return bde.CashierPermissionGranted(userSession);
        }

        public override string GetHTML(AccountDocument doc)
        {
            PayToCashierDocument bdoc = (PayToCashierDocument)doc;
            string html= String.Format("<b>Amount:<b/>{0}", TSConstants.FormatBirr(bdoc.amount));
            return html;
        }

        public int Post(int AID, AccountDocument doc)
        {
            PayToCashierDocument payToCashierDocument = (PayToCashierDocument)doc;
            TransactionOfBatch[] transactions = CollectTransactionsOfBatch(AID, payToCashierDocument);
            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                if (payToCashierDocument.voucher != null)
                    payToCashierDocument.PaperRef = payToCashierDocument.voucher.reference;
                DocumentTypedReference[] typedRefs = payToCashierDocument.voucher == null ? new DocumentTypedReference[0] :
                new DocumentTypedReference[] { payToCashierDocument.voucher };

                if (doc.AccountDocumentID == -1)
                    ret = bdeAccounting.RecordTransaction(AID, payToCashierDocument,
                    transactions
                    , true, typedRefs);
                else
                    ret = bdeAccounting.UpdateTransaction(AID, payToCashierDocument, transactions, true, typedRefs);
                if (!doc.scheduled && !bde.AllowNegativeTransactionPost())
                {
                    bdeAccounting.validateNegativeBalance
                       (payToCashierDocument.assetAccountID, 0, false, payToCashierDocument.DocumentDate
                       , "The transaction is blcoked because it will cause negative asset balance.");
                }

                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        private TransactionOfBatch[] CollectTransactionsOfBatch(int AID, PayToCashierDocument payToCashierDoc)
        {
            PayrollBDE bdePayroll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
            List<TransactionOfBatch> trans = new List<TransactionOfBatch>();
            BankAccountInfo ba = null;
            if (PaymentMethodHelper.IsPaymentMethodBank(payToCashierDoc.paymentMethod))
                ba = bde.GetBankAccount(payToCashierDoc.assetAccountID);
            string note = payToCashierDoc.paymentMethod == BizNetPaymentMethod.CPOFromBankAccount ? "CPO service charge" : "Bank transfer service charge";
            if (Account.AmountGreater(payToCashierDoc.amount, 0))
            {
                trans.Add(new TransactionOfBatch(payToCashierDoc.assetAccountID, -payToCashierDoc.amount, payToCashierDoc.ShortDescription));
                trans.Add(new TransactionOfBatch(payToCashierDoc.destinationCashAccID, payToCashierDoc.amount, payToCashierDoc.ShortDescription));
            }
           
            if (payToCashierDoc.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(payToCashierDoc.serviceChargeAmount, 0))
            {
                int serviceChargeAccountID = ba == null ?
                    bdeAccounting.GetOrCreateCostCenterAccount(AID,bde.SysPars.mainCostCenterID, bde.SysPars.bankServiceChargeByCash).id
                    : ba.bankServiceChargeCsAccountID;
                if (serviceChargeAccountID == -1)
                    throw new INTAPS.ClientServer.ServerUserMessage("Bank fee expense account not set");
                CostCenterAccount serviceChargeAccount = bdeAccounting.GetCostCenterAccount(serviceChargeAccountID);
                if (serviceChargeAccount == null)
                    throw new INTAPS.ClientServer.ServerUserMessage("Bank fee expense account not set");

                trans.Add(new TransactionOfBatch(serviceChargeAccount.id, payToCashierDoc.serviceChargeAmount, note));
                trans.Add(new TransactionOfBatch(payToCashierDoc.assetAccountID, -payToCashierDoc.serviceChargeAmount, note));
            }
            return trans.ToArray();
        }


        #endregion
        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            PayToCashierDocument paymentDoc = (PayToCashierDocument)doc;
            if (PaymentMethodHelper.IsPaymentMethodCredit(paymentDoc.paymentMethod))
                return null;
            if (!CashTransactionUtility.isCostCenterAccountInCostCenter(bde.Accounting, paymentDoc.destinationCashAccID, option.costCenters))
                return null;
            List<CashFlowItem> items = new List<CashFlowItem>();
            items.Add(CashFlowItem.createByText(paymentDoc.destinationCashAccID, -1, "", paymentDoc.amount));

            CashTransactionUtility.addCashTransactionMainItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.amount, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer, paymentDoc.checkNumber, paymentDoc.cpoNumber, paymentDoc.slipReferenceNo);
            CashTransactionUtility.addCashTransactionCostItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer);
            return items;
        }
        public string getInstrumentNo(AccountDocument _doc, out string typeName)
        {
            typeName = null;
            return null;
        }
    }
}
