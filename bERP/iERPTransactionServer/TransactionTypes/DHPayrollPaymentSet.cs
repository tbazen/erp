using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    public class DHPayrollPaymentSet : DeleteReverseDocumentHandler, IDocumentServerHandler,ICashTransaction
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHPayrollPaymentSet(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        public override void DeleteDocument(int AID, int docID)
        {
            bdeAccounting.WriterHelper.BeginTransaction();
            try
            {
                PayrollBDE bdePayroll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
                deleteExisting(AID, bdePayroll, docID);
                bdeAccounting.WriterHelper.CommitTransaction();
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }
        #region IDocumentServerHandler Members
        void collectPaymentTransaction(int AID, List<TransactionOfBatch> tran, PayrollPaymentSetDocument payment)
        {

            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(payment.paymentMethod) && AccountBase.AmountGreater(payment.serviceChargeAmount, 0)
                && payment.serviceChargePayer == ServiceChargePayer.Company)
            {
                //debit service charge expense
                int serviceChargeAccountID;
                if (PaymentMethodHelper.IsPaymentMethodBank(payment.paymentMethod))
                {
                    BankAccountInfo ba = bde.GetBankAccount(payment.assetAccountID);
                    if (ba == null)
                        throw new ServerUserMessage("Invalid bank account");
                    serviceChargeAccountID = ba.bankServiceChargeCsAccountID;
                }
                else
                {
                    CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.bankServiceChargeByCash);
                    if (csa == null)
                        throw new ServerConfigurationError("The bank service charge by cash account not configured properly");
                    serviceChargeAccountID = csa.id;
                }
                tran.Add(new TransactionOfBatch(
                    serviceChargeAccountID
                , payment.serviceChargeAmount, string.Format("Bank service charge for payroll payment {0}", payment.PaperRef)));


                tran.Add(new TransactionOfBatch(
                payment.assetAccountID
                , -payment.totalAmount - payment.serviceChargeAmount, string.Format("Total cash for payroll payment {0}", payment.PaperRef)));

            }
            else
            {
                tran.Add(new TransactionOfBatch(
                    payment.assetAccountID
                    , -payment.totalAmount, string.Format("Total cash for payroll payment {0}", payment.PaperRef)));
            }

        }
        public bool CheckPostPermission(int docID, AccountDocument doc, UserSessionData userSession)
        {
            return true;
        }

        public override string GetHTML(AccountDocument doc)
        {
            return "Payroll payment";
        }



        PayrollSetDocument getSet(PayrollBDE bdePayroll, Dictionary<int, PayrollSetDocument> sets, int employeeId, int periodID)
        {
            if (sets.ContainsKey(employeeId))
                return sets[employeeId];
            PayrollSetDocument ret = bdePayroll.getPayrollSetByEmployee(periodID, employeeId);
            foreach (PayrollDocument pdoc in ret.payrolls)
                sets.Add(pdoc.employee.id, ret);
            return ret;
        }
        public int Post(int AID, AccountDocument doc)
        {
            PayrollPaymentSetDocument set = (PayrollPaymentSetDocument)doc;
            PayrollBDE bdePayroll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
            Dictionary<int, PayrollSetDocument> sets = new Dictionary<int, PayrollSetDocument>();
            foreach (int empID in set.employeeID)
                getSet(bdePayroll, sets, empID, set.periodID);
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                if (set.voucher == null)
                    throw new ServerUserMessage("Invalid refernce");
                set.PaperRef= set.voucher.reference;
                if (set.AccountDocumentID != -1)
                {
                    deleteExisting(AID, bdePayroll, set.AccountDocumentID);
                }
                set.employee = new Employee[set.employeeID.Length];
                for (int i = 0; i < set.employeeID.Length; i++)
                    foreach(PayrollDocument  pdoc in sets[set.employeeID[i]].payrolls)
                    {
                        if (pdoc.employee.id == set.employeeID[i])
                        {
                            set.employee[i] = pdoc.employee;// bdePayroll.GetEmployee(set.employeeID[i]);
                            break;
                        }
                    }
                set.payPeriod = bdePayroll.GetPayPeriod(set.periodID);
                List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
                collectPaymentTransaction(AID, tran, set);
                collectDebitUnclaimedSalary(AID, bdePayroll, tran, set);
                DocumentTypedReference[] refs;
                if (set.payrollDocumentID == -1)
                    refs = new DocumentTypedReference[] { set.voucher };
                else
                {
                    DocumentSerial[] par = bdeAccounting.getDocumentSerials(set.payrollDocumentID);
                    DocumentTypedReference[] tref = new DocumentTypedReference[par.Length];
                    for (int i = 0; i < par.Length; i++)
                        tref[i] = par[i].typedReference;
                    refs = DocumentTypedReference.createArray(tref, set.voucher);
                }

                set.AccountDocumentID = bdeAccounting.RecordTransaction(AID, set, bdeAccounting.summerizeTransactionOfBatch(tran.ToArray()),refs);
                for (int i = 0; i < set.employee.Length; i++)
                {
                    bdePayroll.WriterHelper.Insert(bdePayroll.DBName, "PayrollPayment", new string[] { "employeeID", "periodID", "accountDocumentID","amount", "__AID" }
                        , new object[] { set.employee[i].id,set.payPeriod.id,set.AccountDocumentID,set.amount[i],  AID });
                }
                
                bdeAccounting.WriterHelper.CommitTransaction();
                return set.AccountDocumentID;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        private void deleteExisting(int AID, PayrollBDE bdePayroll, int docID)
        {
            PayrollPaymentSetDocument oldSet = bdeAccounting.GetAccountDocument(docID, true) as PayrollPaymentSetDocument;
            if (oldSet != null)
            {
                bdeAccounting.DeleteAccountDocument(AID, docID, true);
                bdePayroll.WriterHelper.logDeletedData(AID, bdePayroll.DBName + ".dbo.PayrollPayment", "accountDocumentID=" + docID);
                bdePayroll.WriterHelper.Delete(bdePayroll.DBName, "PayrollPayment", "accountDocumentID=" + docID);
            }
        }

        private void collectDebitUnclaimedSalary(int AID,PayrollBDE bdePayroll, List<TransactionOfBatch> trans,PayrollPaymentSetDocument set)
        {
            
            int companyCostCenterID = bdePayroll.SysPars.mainCostCenterID;
            int[] empID=new int[set.employee.Length];
            for(int i=0;i<empID.Length;i++)
                empID[i]=set.employee[i].id;
            double[] payroll;
            double[] currentPayment = bdePayroll.getPaymetAmounts(set.payPeriod.id,empID,out payroll);
            AccountTransaction[] tran = bdeAccounting.GetTransactionsOfDocument(set.payrollDocumentID);
            for (int i = 0; i < empID.Length; i++)
            {
                if (AccountBase.AmountLess(payroll[i]- currentPayment[i], set.amount[i]))
                    throw new ServerUserMessage(string.Format("Payment amount larger than the payroll for employee {0}", set.employee[i].EmployeeNameID));
                if (AccountBase.AmountLess(set.amount[i], 0))
                    throw new ServerUserMessage(string.Format("Payment amount is negative for employee {0}", set.employee[i].EmployeeNameID));
                int unclaimedAccountID=-1;
                int employeeUnclaimedAccountID=bdePayroll.GetEmployeeUncollectedPaymentAccount(set.employee[i].id);
                foreach (AccountTransaction tt in tran)
                {
                    if (bdeAccounting.GetCostCenterAccount(tt.AccountID).accountID == employeeUnclaimedAccountID)
                    {
                        unclaimedAccountID=tt.AccountID;
                        break;
                    }
                }
                if (unclaimedAccountID == -1)
                {
                    foreach (AccountTransaction tt in tran)
                    {
                        Console.WriteLine(bdeAccounting.GetCostCenterAccountWithDescription(tt.AccountID).code);
                    }
                    throw new ServerUserMessage("Payoll don't have transaction on the unclaimed salary account of " + set.employee[i].EmployeeNameID);
                }
                TransactionOfBatch t= new TransactionOfBatch(unclaimedAccountID, set.amount[i], "Payroll payment for period " + set.payPeriod.name);
                trans.Add(t);
            }
            
        }

        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            PayrollPaymentSetDocument paymentDoc = (PayrollPaymentSetDocument)doc;
            if (PaymentMethodHelper.IsPaymentMethodCredit(paymentDoc.paymentMethod))
                return null;
            List<CashFlowItem> items = new List<CashFlowItem>();
            double totalPayment = 0;
            int i = -1;
            List<Employee> paidEmployees = new List<Employee>();
            
            foreach (Employee emp in paymentDoc.employee)
            {
                i++;
                if (option.costCenters != null && !option.costCenters.Contains(emp.costCenterID))
                {
                    paidEmployees.Add(emp);
                    continue;
                }
                totalPayment = paymentDoc.amount[i];
                
            }
            if (AccountBase.AmountEqual(totalPayment, 0))
                return null;
            string html = System.Web.HttpUtility.HtmlEncode("Paryoll Payment: " + (paidEmployees.Count == 1 ? paidEmployees[0].EmployeeNameID : paymentDoc.employee.Length + " employees"));
            items.Add(new CashFlowItem(html, paymentDoc.totalAmount));
            CashTransactionUtility.addCashTransactionMainItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, totalPayment, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer, paymentDoc.checkNumber, paymentDoc.cpoNumber,paymentDoc.transferReference);
            CashTransactionUtility.addCashTransactionCostItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, totalPayment*paymentDoc.serviceChargeAmount/paymentDoc.totalAmount, paymentDoc.serviceChargePayer);
            return items;
        }
        #endregion

        public string getInstrumentNo(AccountDocument _doc, out string typeName)
        {
            PayrollPaymentSetDocument doc = _doc as PayrollPaymentSetDocument;
            if (!PaymentMethodHelper.IsPaymentMethodWithReference(doc.paymentMethod))
            {
                typeName = null;
                return null;
            }
            typeName = PaymentMethodHelper.GetPaymentTypeReferenceName(doc.paymentMethod);
            return PaymentMethodHelper.selectReference(doc.paymentMethod,doc.checkNumber,doc.cpoNumber,doc.transferReference);
        }
    }
    
}
