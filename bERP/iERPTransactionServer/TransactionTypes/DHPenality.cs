using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{

    public class DHPenality : DeleteReverseDocumentHandler, IDocumentServerHandler,ICashTransaction
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }


        public DHPenality(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, AccountDocument doc, UserSessionData userSession)
        {
            return bde.CashierPermissionGranted(userSession);
        }

        public override string GetHTML(AccountDocument doc)
        {
            PenalityDocument bdoc = (PenalityDocument)doc;
            return "<b>Amount:<b/>" + TSConstants.FormatBirr((bdoc.penality + bdoc.interest + bdoc.unaccountedTax) - bdoc.deduction);
        }

        public int Post(int AID, AccountDocument doc)
        {
            PenalityDocument penalityDocument = (PenalityDocument)doc;
            TransactionOfBatch[] transactions = null;
            bool validateCash = false;
            switch (penalityDocument.paymentMethod)
            {
                case BizNetPaymentMethod.Cash:
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.BankTransferFromCash:
                    transactions = CollectCashPaymentTransactionsOfBatch(AID, penalityDocument);
                    validateCash = true;
                    break;

                case BizNetPaymentMethod.Check:
                case BizNetPaymentMethod.CPOFromBankAccount:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    transactions = CollectBankPaymentTransactionsOfBatch(AID, penalityDocument);
                    validateCash = false;
                    break;
                case BizNetPaymentMethod.None:
                    break;
                default:
                    break;
            }

            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                TransactionOfBatch[] filteredTran = FilterTransactions(transactions);
                    
                if (doc.AccountDocumentID == -1)
                {
                    ret = bdeAccounting.RecordTransaction(AID, penalityDocument,
                    filteredTran
                    , true);
                }
                else
                    ret = bdeAccounting.UpdateTransaction(AID, penalityDocument, filteredTran, true);
                if (!doc.scheduled && !bde.AllowNegativeTransactionPost())
                {
                    string account = validateCash ? "cash account" : "bank account";
                    bdeAccounting.validateNegativeBalance
                       (penalityDocument.assetAccountID, 0, false, penalityDocument.DocumentDate
                       , "Transaction is blocked because there is no sufficient balance in the " + account);
                }
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        protected virtual TransactionOfBatch[] CollectCashPaymentTransactionsOfBatch(int AID, PenalityDocument penalityDoc)
        {
            CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.bankServiceChargeByCash);
            if (csa == null)
                throw new ServerConfigurationError("The bank service charge by cash account not configured properly. The account does not belong to 'Head Quarter' cost center");

            int serviceChargeAccountID = csa.id;
            TransactionOfBatch[] transactions = null;

            TransactionOfBatch creditDeduction = null;
            TransactionOfBatch creditAsset;
            TransactionOfBatch debitPenality, debitInterest, debitUnaccountedTax;
            debitPenality = debitInterest = debitUnaccountedTax = null;
            if (AccountBase.AmountGreater(penalityDoc.penality, 0))
            {
                CostCenterAccount csPenality = bdeAccounting.GetOrCreateCostCenterAccount(AID, penalityDoc.costCenterID, bde.SysPars.taxPenalityAcountID);
                if (csPenality == null)
                    throw new ServerUserMessage("'Tax Penality' account does not belong to '" + bdeAccounting.GetAccount<CostCenter>(penalityDoc.costCenterID).Name + "' cost center");
                debitPenality = new TransactionOfBatch(csPenality.id, penalityDoc.penality);
            }
            if (AccountBase.AmountGreater(penalityDoc.interest, 0))
            {
                CostCenterAccount csInterest = bdeAccounting.GetOrCreateCostCenterAccount(AID, penalityDoc.costCenterID, bde.SysPars.taxInterestAccountID);
                if (csInterest == null)
                    throw new ServerUserMessage("'Tax Interest' account does not belong to '" + bdeAccounting.GetAccount<CostCenter>(penalityDoc.costCenterID).Name + "' cost center");
                debitInterest = new TransactionOfBatch(csInterest.id, penalityDoc.interest);
            }
            if (AccountBase.AmountGreater(penalityDoc.unaccountedTax, 0))
            {
                CostCenterAccount csOtherPenality = bdeAccounting.GetOrCreateCostCenterAccount(AID, penalityDoc.costCenterID, bde.SysPars.taxPenalityAcountID);
                if (csOtherPenality == null)
                    throw new ServerUserMessage("'Tax Penality' account does not belong to '" + bdeAccounting.GetAccount<CostCenter>(penalityDoc.costCenterID).Name + "' cost center");
                debitUnaccountedTax = new TransactionOfBatch(csOtherPenality.id, penalityDoc.unaccountedTax);
            }
            if (AccountBase.AmountGreater(penalityDoc.deduction, 0))
            {
                CostCenterAccount csExemption = bdeAccounting.GetOrCreateCostCenterAccount(AID, penalityDoc.costCenterID, bde.SysPars.taxExemptionsAccountID);
                if (csExemption == null)
                    throw new ServerUserMessage("'Tax Exemption' account does not belong to '" + bdeAccounting.GetAccount<CostCenter>(penalityDoc.costCenterID).Name + "' cost center");
                creditDeduction = new TransactionOfBatch(csExemption.id, -penalityDoc.deduction);
            }
            creditAsset = new TransactionOfBatch(penalityDoc.assetAccountID, -penalityDoc.Total);
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(penalityDoc.paymentMethod) && penalityDoc.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(penalityDoc.serviceChargeAmount, 0))
            {
                string note = penalityDoc.paymentMethod == BizNetPaymentMethod.CPOFromCash ? "CPO service charge" : "Bank transfer service charge";

                TransactionOfBatch debitSide2, creditSide2;
                debitSide2 = new TransactionOfBatch(serviceChargeAccountID, penalityDoc.serviceChargeAmount, note);
                creditSide2 = new TransactionOfBatch(penalityDoc.assetAccountID, -penalityDoc.serviceChargeAmount, note);
                transactions = new TransactionOfBatch[] { debitInterest, debitPenality, debitUnaccountedTax, creditAsset, creditDeduction, debitSide2, creditSide2 };
            }
            else
            {
                transactions = new TransactionOfBatch[] { debitInterest, debitPenality, debitUnaccountedTax, creditAsset, creditDeduction };
            }
            return transactions;
        }
        protected virtual TransactionOfBatch[] CollectBankPaymentTransactionsOfBatch(int AID, PenalityDocument penalityDoc)
        {
            TransactionOfBatch[] transactions = null;

            TransactionOfBatch creditDeduction = null;
            TransactionOfBatch creditAsset;
            TransactionOfBatch debitPenality, debitInterest, debitUnaccountedTax;
            debitPenality = debitInterest = debitUnaccountedTax = null;
            BankAccountInfo ba = bde.GetBankAccount(penalityDoc.assetAccountID);
            int serviceChargeAccountID = ba.bankServiceChargeCsAccountID;
            if (AccountBase.AmountGreater(penalityDoc.penality, 0))
            {
                CostCenterAccount csPenality = bdeAccounting.GetOrCreateCostCenterAccount(AID, penalityDoc.costCenterID, bde.SysPars.taxPenalityAcountID);
                if (csPenality == null)
                    throw new ServerUserMessage("'Tax Penality' account does not belong to '" + bdeAccounting.GetAccount<CostCenter>(penalityDoc.costCenterID).Name + "' cost center");
                debitPenality = new TransactionOfBatch(csPenality.id, penalityDoc.penality);
            }
            if (AccountBase.AmountGreater(penalityDoc.interest, 0))
            {
                CostCenterAccount csInterest = bdeAccounting.GetOrCreateCostCenterAccount(AID, penalityDoc.costCenterID, bde.SysPars.taxInterestAccountID);
                if (csInterest == null)
                    throw new ServerUserMessage("'Tax Interest' account does not belong to '" + bdeAccounting.GetAccount<CostCenter>(penalityDoc.costCenterID).Name + "' cost center");
                debitInterest = new TransactionOfBatch(csInterest.id, penalityDoc.interest);
            }
            if (AccountBase.AmountGreater(penalityDoc.unaccountedTax, 0))
            {
                CostCenterAccount csOtherPenality = bdeAccounting.GetOrCreateCostCenterAccount(AID, penalityDoc.costCenterID, bde.SysPars.taxPenalityAcountID);
                if (csOtherPenality == null)
                    throw new ServerUserMessage("'Tax Penality' account does not belong to '" + bdeAccounting.GetAccount<CostCenter>(penalityDoc.costCenterID).Name + "' cost center");
                debitUnaccountedTax = new TransactionOfBatch(csOtherPenality.id, penalityDoc.unaccountedTax);
            }
            if (AccountBase.AmountGreater(penalityDoc.deduction, 0))
            {
                CostCenterAccount csExemption = bdeAccounting.GetOrCreateCostCenterAccount(AID, penalityDoc.costCenterID, bde.SysPars.taxExemptionsAccountID);
                if (csExemption == null)
                    throw new ServerUserMessage("'Tax Exemption' account does not belong to '" + bdeAccounting.GetAccount<CostCenter>(penalityDoc.costCenterID).Name + "' cost center");
                creditDeduction = new TransactionOfBatch(csExemption.id, -penalityDoc.deduction);
            }
            creditAsset = new TransactionOfBatch(ba.mainCsAccount, -penalityDoc.Total);
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(penalityDoc.paymentMethod) && penalityDoc.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(penalityDoc.serviceChargeAmount, 0))
            {
                string noteServiceCharge = penalityDoc.paymentMethod == BizNetPaymentMethod.CPOFromCash ? "CPO service charge" : "Bank transfer service charge";

                TransactionOfBatch debitSide2, creditSide2;
                debitSide2 = new TransactionOfBatch(serviceChargeAccountID, penalityDoc.serviceChargeAmount, noteServiceCharge);
                creditSide2 = new TransactionOfBatch(ba.mainCsAccount, -penalityDoc.serviceChargeAmount, noteServiceCharge);
                transactions = new TransactionOfBatch[] { debitInterest, debitPenality, debitUnaccountedTax, creditAsset, creditDeduction, debitSide2, creditSide2 };
            }
            else
            {
                transactions = new TransactionOfBatch[] { debitInterest, debitPenality, debitUnaccountedTax, creditAsset, creditDeduction };
            }
            return transactions;
        }
        private TransactionOfBatch[] FilterTransactions(TransactionOfBatch[] transactions)
        {
            List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
            foreach (TransactionOfBatch t in transactions)
            {
                if (t != null)
                    tran.Add(t);
            }
            return tran.ToArray();
        }
        public void ValidateAccounts(PenalityDocument penalityDoc, bool validateCash)
        {
            if (!validateCash) //validate bank
            {
                ValidateBankAccount(penalityDoc);
            }
            else
            {
                ValidateCashAccount(penalityDoc);
            }
        }

        private void ValidateBankAccount(PenalityDocument penalityDoc)
        {
            if (penalityDoc.assetAccountID != -1)
            {
                double balance = GetBankAccountBalance(penalityDoc);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make bank account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No bank accounts found.\nPlease configure bank account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetBankAccountBalance(PenalityDocument penalityDoc)
        {
            return bdeAccounting.GetNetBalanceAsOf(penalityDoc.assetAccountID, 0, penalityDoc.DocumentDate);
        }

        private void ValidateCashAccount(PenalityDocument penalityDoc)
        {
            if (penalityDoc.assetAccountID != -1)
            {
                double balance = GetCashAccountBalance(penalityDoc);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make cash account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No cash accounts found.\nPlease configure cash account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetCashAccountBalance(PenalityDocument penalityDoc)
        {
            return bdeAccounting.GetNetBalanceAsOf(penalityDoc.assetAccountID,0,penalityDoc.DocumentDate);
        }

        #endregion
        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            PenalityDocument paymentDoc = (PenalityDocument)doc;
            if (PaymentMethodHelper.IsPaymentMethodCredit(paymentDoc.paymentMethod))
                return null;
            if (!CashTransactionUtility.isTransactionInCostCenter(bde.Accounting, paymentDoc.AccountDocumentID, option.costCenters, true))
                return null;
            List<CashFlowItem> items = new List<CashFlowItem>();
            string html = System.Web.HttpUtility.HtmlEncode("Tax Penality Payment ");
            items.Add(new CashFlowItem(html, paymentDoc.Total));
            CashTransactionUtility.addCashTransactionMainItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.Total, 0, ServiceChargePayer.Company, paymentDoc.checkNumber, paymentDoc.cpoNumber, paymentDoc.slipReferenceNo);
            CashTransactionUtility.addCashTransactionCostItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer);
            return items;
        }
        public string getInstrumentNo(AccountDocument _doc, out string typeName)
        {
            PenalityDocument doc = _doc as PenalityDocument;
            if (!PaymentMethodHelper.IsPaymentMethodWithReference(doc.paymentMethod))
            {
                typeName = null;
                return null;
            }
            typeName = PaymentMethodHelper.GetPaymentTypeReferenceName(doc.paymentMethod);
            return PaymentMethodHelper.selectReference(doc.paymentMethod,doc.checkNumber,doc.cpoNumber,doc.slipReferenceNo);
        }
    }
}

