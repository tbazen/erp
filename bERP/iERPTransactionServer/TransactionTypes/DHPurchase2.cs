﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using System.Web;
using INTAPS;

namespace BIZNET.iERP.Server
{
    public class DHItemTransactionBase : DeleteReverseDocumentHandler
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHItemTransactionBase(AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        protected void fixUnit(IEnumerable<TransactionDocumentItem> docItems,IEnumerable<TransactionItems> items)
        {
            IEnumerator<TransactionDocumentItem> docItemsEnum = docItems.GetEnumerator();
            IEnumerator<TransactionItems> itemsEnum = items.GetEnumerator();
            while(docItemsEnum.MoveNext() && itemsEnum.MoveNext())
            {
                if(docItemsEnum.Current.unitID==-1)
                {
                    docItemsEnum.Current.unitID = itemsEnum.Current.MeasureUnitID;
                }
                else if(docItemsEnum.Current.unitID!=itemsEnum.Current.MeasureUnitID)
                {
                    
                    double factor = bde.getItemUnitConvertor(itemsEnum.Current.Code).convert(docItemsEnum.Current.unitID, itemsEnum.Current.MeasureUnitID, 1);
                    docItemsEnum.Current.quantity *= factor;
                    docItemsEnum.Current.unitPrice /= factor;
                    docItemsEnum.Current.unitID = itemsEnum.Current.MeasureUnitID;
                }
            }
        }
        public void addSummerizedItemTransaction(int AID, List<TransactionOfBatch> tran, List<TransactionOfBatch> summeryTransactions, TransactionItems titem, string name, int costCenterID, int accountID, int summaryAccountID, int itemID, double amount, string note)
        {
            bdeAccounting.assertValidAccount<CostCenter>(costCenterID, "Cost center for item code:{0} no {1} is invalid".format(titem.Code, itemID + 1));
            bdeAccounting.assertValidAccount<Account>(accountID, "{0} account for item code:{1} no {2} is invalid".format(name, titem.Code, itemID + 1));
            CostCenterAccount csAccount = bdeAccounting.GetOrCreateCostCenterAccount(AID, costCenterID, accountID);
            tran.Add(new TransactionOfBatch(csAccount.id, itemID, amount, note));
                
            if (itemID == 0 && bde.SysPars.summerizeItemTransactions)
            {
                if (summaryAccountID == -1)
                    throw new ServerUserMessage("Summary account '" + name + "' not set.");
                bdeAccounting.assertValidAccount<Account>(summaryAccountID, "{0} summary account for item code:{1} no {2} is invalid".format(name,titem.Code,itemID+1));
                csAccount = bdeAccounting.GetOrCreateCostCenterAccount(AID, csAccount.costCenterID, summaryAccountID);
                bool found = false;
                foreach (TransactionOfBatch t in summeryTransactions)
                    if (t.AccountID == csAccount.id && t.ItemID == itemID)
                    {
                        t.Amount += amount;
                        if (!t.Note.Equals(note))
                            t.Note = "";
                        found = true;
                        break;
                    }
                if (!found)
                    summeryTransactions.Add(new TransactionOfBatch(csAccount.id, itemID, amount, note));
            }

        }
        protected void addNetSummaryTransaction(int AID, List<TransactionOfBatch> tran,

            string note, List<TransactionOfBatch> summeryTransactions)
        {
            if (bde.SysPars.summerizeItemTransactions)
            {
                double vnet = 0;
                double qnet = 0;
                foreach (TransactionOfBatch t in summeryTransactions)
                    if (t.ItemID == 0)
                        vnet += t.Amount;
                    else
                        qnet += t.Amount;
                CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.summerizationContraAccountID);
                if (!AccountBase.AmountEqual(vnet, 0))
                    tran.Add(new TransactionOfBatch(csa.id, -vnet, note));
                if (!AccountBase.AmountEqual(qnet, 0))
                    tran.Add(new TransactionOfBatch(csa.id, TransactionItem.MATERIAL_QUANTITY, -qnet, note));
                tran.AddRange(summeryTransactions);
            }
        }
    }
    public class DHPurchase2 : DHItemTransactionBase, IDocumentServerHandler, ICashTransaction
    {
        public DHPurchase2(AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        public bool CheckPostPermission(int docID, AccountDocument _doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return true;
        }
        void collectTaxationTransaction(int AID, List<TransactionOfBatch> tran, Purchase2Document purchase,TradeRelation supplier,  List<TransactionItems> items, out double amount, out double netAmount, out double[] pwt, out bool addTaxToCost)
        {
            double taxable;
            bool vatApplies, whApplies;

            TaxImposed[] taxes = Purchase2Document.getTaxes(supplier, bde.SysPars.companyProfile,
                purchase.items
                , items.ToArray()
                , purchase.taxRates == null ? bde.getTaxRates() : purchase.taxRates
                , !AccountBase.AmountLess(purchase.manualVATBase, 0), purchase.manualVATBase
                , !AccountBase.AmountLess(purchase.manualWithholdBase, 0), purchase.manualWithholdBase
                , out amount
                , out taxable
                , out vatApplies, out whApplies, out pwt
                );

            if (!AccountBase.AmountEqual(purchase.amount, amount))
                throw new ServerUserMessage("The amount the client sent don't match the values calculated on the server.\nThis might indicate an out of date client");

            if (taxes.Length != purchase.taxImposed.Length)
                throw new ServerUserMessage("The tax amounts the client sent don't match the values calculated on the server.\nThis might indicate an out of date client");

            for (int i = 0; i < taxes.Length; i++)
                if (!purchase.taxImposed[i].Equals(taxes[i]))
                    throw new ServerUserMessage("The tax amount client sent don't match the values calculated on the server.\nThis might indicate an out of date client");

            purchase.amount = amount;
            purchase.taxImposed = taxes;
            

            netAmount = purchase.amount;
            foreach (TaxImposed tax in purchase.taxImposed)
                netAmount += tax.TaxValue;
            addTaxToCost = true;
            //taxes
            foreach (TaxImposed tax in purchase.taxImposed)
            {
                switch (tax.TaxType)
                {
                    case TaxType.VAT:
                        if (bde.SysPars.companyProfile.TaxRegistrationType == TaxRegisrationType.VAT)
                        {
                            addTaxToCost = false;
                            //debit input vat
                            this.bdeAccounting.assertValidAccount<Account>(purchase.invoiced ? bde.SysPars.inputVATAccountID : bde.SysPars.inputVATAccountIDNotInvoiced, "Please configure input VAT Account");
                            CostCenterAccount csVATAccount = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, 
                                purchase.invoiced?bde.SysPars.inputVATAccountID:bde.SysPars.inputVATAccountIDNotInvoiced
                                );
                            if (csVATAccount == null)
                                throw new ServerUserMessage("Input VAT account does not belong to the 'Head Quarter' Cost Cener");
                            tran.Add(new TransactionOfBatch(
                                csVATAccount.id
                                , tax.TaxValue, string.Format("VAT paid on purchase {0}", purchase.PaperRef)));
                        }
                        break;
                    case TaxType.VATWitholding:
                        //credit withheld input vat
                        this.bdeAccounting.assertValidAccount<Account>(bde.SysPars.outputVATWithHeldAccountID, "Please configure withheld VAT account");
                        tran.Add(new TransactionOfBatch(
                            bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID
                            //, purchase.invoiced ? bde.SysPars.outputVATWithHeldAccountID : bde.SysPars.outputVATWithHeldNotInvoicedAccountID
                            , bde.SysPars.outputVATWithHeldAccountID 
                            ).id
                                    , tax.TaxValue, ""));
                        break;
                    case TaxType.WithHoldingTax:
                        this.bdeAccounting.assertValidAccount<Account>(bde.SysPars.collectedWithHoldingTaxAccountID, "Please configure withheld tax account");
                        CostCenterAccount csWithHoldingAccount = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID
                            //,purchase.invoiced ? bde.SysPars.collectedWithHoldingTaxAccountID : bde.SysPars.collectedWithHoldingTaxNotInvoicedAccountID
                            , bde.SysPars.collectedWithHoldingTaxAccountID                             );
                        if (csWithHoldingAccount == null)
                            throw new ServerUserMessage("Collected Withholding tax account does not belong to the 'Head Quarter' Cost Cener");
                        tran.Add(new TransactionOfBatch(
                            csWithHoldingAccount.id
                            , tax.TaxValue, string.Format("Withheld from purchase {0}", purchase.PaperRef)));
                        break;
                }
            }
        }
        void collectPaymentTransaction(int AID, List<TransactionOfBatch> tran, Purchase2Document purchase, TradeRelation supplier, double netAmount)
        {
            //credit advance payment
            if (!AccountBase.AmountEqual(purchase.settleAdvancePayment, 0))
            {
                DHSell2.autoCreateRelationAccountField(bde, AID, supplier, "ReceivableAccountID");
                CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, supplier.ReceivableAccountID);

                tran.Add(new TransactionOfBatch(
                    csa.id
                    , -purchase.settleAdvancePayment, "Advance settled by purchase:" + purchase.PaperRef));
                netAmount -= purchase.settleAdvancePayment;
            }

            //credit supplier payable
            if (!AccountBase.AmountEqual(purchase.supplierCredit, 0))
            {

                DHSell2.autoCreateRelationAccountField(bde, AID, supplier, "PayableAccountID");
                CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, supplier.PayableAccountID);

                string note = string.Format("Credit from {0}({1}) on  purchase {2}", supplier.Name, supplier.Code, purchase.PaperRef);
                tran.Add(new TransactionOfBatch(
                    csa.id
                    , -purchase.supplierCredit, note));
                netAmount -= purchase.supplierCredit;
            }

            //credit retention
            if (!AccountBase.AmountEqual(purchase.retention, 0))
            {
                DHSell2.autoCreateRelationAccountField(bde, AID, supplier, "RetentionPayableAccountID");
                CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, supplier.RetentionPayableAccountID);
                string note = string.Format("Retention from {0}({1}) on  purchase {2}", supplier.Name, supplier.Code, purchase.PaperRef);
                tran.Add(new TransactionOfBatch(
                    csa.id
                    , -purchase.retention, note));
                netAmount -= purchase.retention;
            }

            if (AccountBase.AmountLess(netAmount, 0))
                throw new ServerUserMessage("Negative purchase transaction is not allowed");

            if (!AccountBase.AmountEqual(netAmount, purchase.paidAmount))
            {
                if (AccountBase.AmountGreater(purchase.paidAmount, netAmount)) //if paid amount is greater than the net amount, debit receivable
                {
                    DHSell2.autoCreateRelationAccountField(bde, AID, supplier, "ReceivableAccountID");
                    CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, supplier.ReceivableAccountID);

                    string note = string.Format("Overpayment to {0}({1}) on  purchase {2}", supplier.Name, supplier.Code, purchase.PaperRef);
                    tran.Add(new TransactionOfBatch(
                        csa.id
                        , purchase.paidAmount - netAmount, string.Format("Over payment to {0}({1}) on  purchase {2}", supplier.Name, supplier.Code, purchase.PaperRef)));
                }
                else//
                {
                    DHSell2.autoCreateRelationAccountField(bde, AID, supplier, "PayableAccountID");
                    CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, supplier.PayableAccountID);

                    string note = string.Format("Underpayment to {0}({1}) on  purchase {2}", supplier.Name, supplier.Code, purchase.PaperRef);
                    tran.Add(new TransactionOfBatch(
                        csa.id
                        , purchase.paidAmount - netAmount, string.Format("Under payment to {0}({1}) on  purchase {2}", supplier.Name, supplier.Code, purchase.PaperRef)));
                }
            }


            //credit asset
            if (AccountBase.AmountGreater(netAmount, 0))
            {
                this.bdeAccounting.assertValidCostCenterAccount(purchase.assetAccountID, "The asset account selected for payment is not valid");

                if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(purchase.paymentMethod) && AccountBase.AmountGreater(purchase.serviceChargeAmount, 0)
                    && purchase.serviceChargePayer == ServiceChargePayer.Company)
                {
                    //debit service charge expense
                    int serviceChargeAccountID;
                    if (PaymentMethodHelper.IsPaymentMethodBank(purchase.paymentMethod))
                    {
                        BankAccountInfo ba = bde.GetBankAccount(purchase.assetAccountID);
                        if (ba == null)
                            throw new ServerUserMessage("Invalid bank account");
                        serviceChargeAccountID = ba.bankServiceChargeCsAccountID;
                        this.bdeAccounting.assertValidCostCenterAccount(serviceChargeAccountID, "Please configure service charge account for bank:" + ba.ToString());
                    }
                    else
                    {
                        this.bdeAccounting.assertValidAccount<Account>(bde.SysPars.bankServiceChargeByCash, "Please configure bank service charge by cash account");
                        CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.bankServiceChargeByCash);
                        if (csa == null)
                            throw new ServerConfigurationError("The bank service charge by cash account not configured properly");
                        serviceChargeAccountID = csa.id;
                    }

                    tran.Add(new TransactionOfBatch(
                        serviceChargeAccountID
                    , purchase.serviceChargeAmount, string.Format("Bank service charge for payment on purchase {0}", purchase.PaperRef)));


                    tran.Add(new TransactionOfBatch(
                    purchase.assetAccountID
                    , -purchase.paidAmount - purchase.serviceChargeAmount, string.Format("Payment for {0}({1}) on  purchase {2}", supplier.Name, supplier.Code, purchase.PaperRef)));

                }
                else
                {
                    tran.Add(new TransactionOfBatch(
                        purchase.assetAccountID
                        , -purchase.paidAmount, string.Format("Cash payment for {0}({1}) on  purchase {2}", supplier.Name, supplier.Code, purchase.PaperRef)));
                }
            }
        }
        protected virtual void validateItem(TransactionDocumentItem ditem, TransactionItems item)
        {
            if (item.IsInventoryItem || item.IsFixedAssetItem)
            {
                if (item.inventoryType == InventoryType.FinishedGoods)
                    throw new ServerUserMessage("Finished good items can't be purchased");
            }
        }
        protected virtual void getItemSinkTransactionAccount(int AID, Purchase2Document doc, TradeRelation relation, TransactionDocumentItem ditem, TransactionItems item, out int costCenterID, out int accountID, out int summaryAccountID, out bool summerizable)
        {
            //return bdeAccounting.GetOrCreateCostCenterAccount(AID, ditem.costCenterID, item.expenseAccountID);
            costCenterID = ditem.costCenterID;
            accountID = item.expenseAccountID;
            summaryAccountID = item.expenseSummaryAccountID;
            summerizable = true;
        }
        void collectItemsTransaction(int AID, List<TransactionOfBatch> tran, Purchase2Document purchase, TradeRelation supplier, List<TransactionItems> items, double[] priceWithTax, bool addTaxToCost)
        {
            List<TransactionOfBatch> summeryTransactions = new List<TransactionOfBatch>();
            //bool addTaxToCost = (supplier.TaxRegistrationType != TaxRegisrationType.VAT || ValidateCompanyTaxRegisterationAddToCost(purchase));
            //items
            int j = -1;
            foreach (TransactionDocumentItem ditem in purchase.items)
            {
                j++;
                TransactionItems item = items[j];
                validateItem(ditem, item);
                double cost = addTaxToCost ? priceWithTax[j] : ditem.price;
                if (item.IsInventoryItem || item.IsFixedAssetItem)
                {
                    if (!ditem.directExpense)
                    {
                        //debit supplier receivable account money
                        addSummerizedItemTransaction(AID, tran, summeryTransactions,item, "Pending Order"
                            , supplier.onSupplierOrderCostCenterID, item.pendingOrderAccountID, item.pendingOrderSummaryAccountID, 0
                            , cost, string.Format("Order amount on purchase {0}", purchase.PaperRef));

                        //debit supplier receivable account quantity

                        addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Pending Order"
                            , supplier.onSupplierOrderCostCenterID, item.pendingOrderAccountID, item.pendingOrderSummaryAccountID, TransactionItem.MATERIAL_QUANTITY
                            , ditem.quantity, string.Format("Order quantity on purchase {0}", purchase.PaperRef));

                        //credit the main cost center material inflow account quantity
                        tran.Add(new TransactionOfBatch(
                        bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.materialInFlowAccountID).id
                        , TransactionItem.MATERIAL_QUANTITY
                        , -ditem.quantity, string.Format("Order quantity on purchase {0}", purchase.PaperRef)));

                        continue;
                    }
                    if (item.IsFixedAssetItem)
                    {
                        //debit fixed asset
                        addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Original Fixed Asset"
                            , ditem.costCenterID, item.originalFixedAssetAccountID, item.originalFixedAssetSummaryAccountID, 0
                            , cost, "");

                        //debit fixed quantity
                        addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Original Fixed Asset"
                            , ditem.costCenterID, item.originalFixedAssetAccountID, item.originalFixedAssetSummaryAccountID, TransactionItem.MATERIAL_QUANTITY
                            , ditem.quantity, "");

                        //credit the main cost center material inflow account quantity
                        tran.Add(new TransactionOfBatch(
                        bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.materialInFlowAccountID).id
                        , TransactionItem.MATERIAL_QUANTITY
                        , -ditem.quantity, string.Format("Order quantity on purchase {0}", purchase.PaperRef)));
                        continue;
                    }
                }
                if (item.IsExpenseItem)
                {
                    if (ditem.prepaid)
                    {
                        addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Prepaid Expense"
                            , ditem.costCenterID, item.prePaidExpenseAccountID, item.prePaidExpenseSummaryAccountID, 0
                            , cost, "");
                    }
                    else
                    {
                        //debit expense
                        int csID, acID, acSID;
                        bool summerizable;
                        getItemSinkTransactionAccount(AID, purchase, supplier, ditem, item, out csID, out acID, out acSID, out summerizable);

                        if (summerizable)
                        {
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Expense"
                                    , csID, acID, acSID, 0
                                    , cost, "");
                        }
                        else
                            tran.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, csID, acID).id, cost, ""));
                    }
                    continue;
                }
            }

            addNetSummaryTransaction(AID, tran, "Net Summerized Amount on purchase " + purchase.PaperRef, summeryTransactions);
        }

        

        public int Post(int AID, AccountDocument _doc)
        {
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    Purchase2Document purchase = (Purchase2Document)_doc;

                    if (purchase.scheduled)
                    {
                        if (purchase.getAttachmentArray().Count > 0)
                            throw new ServerUserMessage("Attachment is not supported for scheduled transactions");
                    }

                    TradeRelation supplier = bde.GetRelation(purchase.relationCode);
                    purchase.relationData = supplier;
                    if (_doc.AccountDocumentID != -1)
                    {
                        Purchase2Document old = bdeAccounting.GetAccountDocument(_doc.AccountDocumentID, true) as Purchase2Document;
                        purchase.relationCode = old.relationCode;
                        deleteExisting(AID, _doc.AccountDocumentID,true,purchase.replaceAttachments);
                    }
                    if (purchase.paymentVoucher==null)
                    {
                        throw new ServerUserMessage("No reference provided");
                    }
                    if (string.IsNullOrEmpty(purchase.PaperRef))
                        purchase.PaperRef = purchase.paymentVoucher.reference;


                    //Verify document titems types and collect transaction items objects
                    List<TransactionItems> items = new List<TransactionItems>();
                    foreach (TransactionDocumentItem ditem in purchase.items)
                    {
                        TransactionItems item = bde.GetTransactionItems(ditem.code);
                        if (item == null)
                            throw new ServerUserMessage("Invalid item code:" + item.Code);
                        if (item.IsInventoryItem && item.inventoryType == InventoryType.FinishedGoods)
                            throw new ServerUserMessage("Finished goods can't be purchased");
                        items.Add(item);
                        foreach (TransactionDocumentItem otherItem in purchase.items)
                        {
                            if (otherItem == ditem)
                                continue;
                            if (otherItem.code.Equals(ditem.code) && otherItem.costCenterID==ditem.costCenterID)
                                throw new ServerUserMessage("It is not allowed to repeat items in a single purchase document");
                        }

                    }

                    //collect transaction entries
                    double amount;
                    double netAmount;
                    
                    List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
                    double[] costWithTax;
                    bool addTaxToCost;
                    collectTaxationTransaction(AID, tran, purchase,supplier, items, out amount, out netAmount,  out costWithTax, out addTaxToCost);
                    collectPaymentTransaction(AID, tran, purchase, supplier, netAmount);
                    collectItemsTransaction(AID, tran, purchase, supplier,items,costWithTax,addTaxToCost);
                    if (!purchase.postTransactions)
                    {
                        if (purchase.deliverieIDs.Length > 0
                            || purchase.prepaymentsIDs.Length > 0
                            || purchase.creditSettlementIDs.Length > 0
                            || purchase.invoiceIDs.Length > 0
                            )
                            throw new ServerUserMessage("Tranasctions should be posted for transactions with attached transactions");

                        if (purchase.invoiced)
                            throw new ServerUserMessage("Transactions should be posted for incoiced purchases");
                        tran.Clear();
                    }
                    //post transactions
                    purchase.costWithTax = costWithTax;
                    
                    purchase.AccountDocumentID = bdeAccounting.RecordTransaction(AID, purchase, tran.ToArray(), purchase.paymentVoucher);

                    if(purchase.postTransactions)
                        if(purchase.replaceAttachments)
                            postAttachedDocuments(AID, purchase, supplier);

                    //post Post varification
                    if (!bde.AllowNegativeTransactionPost())
                        bdeAccounting.validateNegativeBalance(purchase.assetAccountID, 0, false, purchase.DocumentDate, "This transaction is blocked because there is no suffient asset in the selected account");

                    bdeAccounting.WriterHelper.CommitTransaction();
                    return purchase.AccountDocumentID;
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

        private bool ValidateCompanyTaxRegisterationAddToCost(Purchase2Document purchase)
        {
            bool addToCost = false;
            if (bde.SysPars.companyProfile.BusinessEntity == BusinessEntity.GovernmentalOrganization)
            {
                if (bde.SysPars.companyProfile.isVATAgent)
                {
                    if (Purchase2Document.GetVATWithholdingRate(purchase.totalBeforeTax, purchase.taxRates) == 0)
                        addToCost = true;
                }
                else
                    addToCost = true;
            }
            else
            {
                if (bde.SysPars.companyProfile.TaxRegistrationType != TaxRegisrationType.VAT)
                    addToCost = true;
            }
            return addToCost;
        }
        private void postAttachedDocuments(int AID, Purchase2Document purchase, TradeRelation supplier)
        {
            AccountDocument[][] docs = new AccountDocument[][] { purchase.deliveries, purchase.prepayments, purchase.creditSettlements, purchase.invoices};
            for (int k = 0; k < docs.Length; k++)
            {
                foreach (AccountDocument attachment in docs[k])
                {
                    processAttachment(purchase, supplier, attachment);
                    if (attachment.DocumentDate.Subtract(DateTime.Now).TotalSeconds > AccountingBDE.FUTURE_TOLERANCE_SECONDS)
                    {
                        attachment.scheduled = true;
                        attachment.materialized = false;
                    }
                    else
                    {
                        attachment.scheduled = false;
                        attachment.materialized = true;
                        attachment.materializedOn = attachment.DocumentDate;
                    }
                    attachment.AccountDocumentID = bdeAccounting.PostGenericDocument(AID, attachment);
                }
            }
        }

        protected virtual void processAttachment(Purchase2Document purchase, TradeRelation supplier, AccountDocument attachment)
        {
            if (attachment is PurchasedItemDeliveryDocument)
            {
                ((PurchasedItemDeliveryDocument)attachment).relationCode = supplier.Code;
                ((PurchasedItemDeliveryDocument)attachment).purchaseDocumentID = purchase.AccountDocumentID;
            }
            else if (attachment is PurchasePrePaymentDocument)
            {
                ((PurchasePrePaymentDocument)attachment).relationCode = supplier.Code;
                ((PurchasePrePaymentDocument)attachment).purchaseDocumentID = purchase.AccountDocumentID;
            }
            else if (attachment is SupplierCreditSettlement2Document)
            {
                ((SupplierCreditSettlement2Document)attachment).relationCode = supplier.Code;
            }
            else if (attachment is PurchaseInvoiceDocument)
            {
                ((PurchaseInvoiceDocument)attachment).purchaseTransactionDocumentID = purchase.AccountDocumentID;
            }
        }

        private void replacePrices(TransactionDocumentItem[] transactionDocumentItem, double[] pwt)
        {
            int i = 0;
            foreach (TransactionDocumentItem item in transactionDocumentItem)
            {
                item.price = pwt[i];
                if (item.quantity > 0)
                    item.unitPrice = item.price / item.quantity;
                i++;
            }
        }
        public override void DeleteDocument(int AID, int docID)
        {
            lock (bde.WriterHelper)
            {
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    deleteExisting(AID, docID, false,true);
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }

        }

        private void deleteExisting(int AID, int did, bool forUpdate,bool includeAttachments)
        {
            AccountDocument doc = bdeAccounting.GetAccountDocument(did, true);
            if (doc == null)
                return;
            if (includeAttachments)
            {
                Purchase2Document purchase = doc as Purchase2Document;
                if (purchase != null)
                {
                    deleteAttachments(AID, forUpdate, purchase);
                }
            }
            bdeAccounting.DeleteAccountDocument(AID, did, forUpdate);
        }

        private void deleteAttachments(int AID, bool forUpdate, Purchase2Document purchase)
        {
            int[][] docIDs = new int[][] { purchase.deliverieIDs, purchase.prepaymentsIDs, purchase.creditSettlementIDs, purchase.invoiceIDs };
            for (int k = 0; k < docIDs.Length; k++)
            {
                foreach (int d in docIDs[k])
                    bdeAccounting.DeleteAccountDocument(AID, d, forUpdate);
            }
        }
        public override void ReverseDocument(int AID, int docID)
        {
            throw new ServerUserMessage("Not supported");
        }
        public override string GetHTML(AccountDocument _doc)
        {
            Purchase2Document invoice = (Purchase2Document)_doc;
            StringBuilder sb = new StringBuilder();
            sb.Append("<div>");
            sb.Append(string.Format("<div class='accountDocumentRef'>{0}</div>", HttpUtility.HtmlEncode("Suppier Reference:" + _doc.PaperRef)));
            sb.Append(string.Format("<div class='accountDocumentDate'>{0}</div>", HttpUtility.HtmlEncode("Date:" + AccountBase.FormatDate(_doc.DocumentDate))));
            if (invoice.paymentVoucher != null)
                sb.Append(string.Format("<div class='accountDocumentVoucher'>{0}</div>", HttpUtility.HtmlEncode("Payment Reference:" + invoice.paymentVoucher.reference)));
            TradeRelation sup = bde.GetSupplier(invoice.relationCode);
            if (sup != null)
            {
                sb.Append(string.Format("<div class='accountDocumentSupplierName'>{0}</div>", HttpUtility.HtmlEncode("Suppier Name:" + sup.Name)));
                if (sup.hasTINNumber)
                    sb.Append(string.Format("<div class='accountDocumentSupplierTIN'>{0}</div>", HttpUtility.HtmlEncode("Suppier TIN:" + sup.TIN)));
            }
            DocumentSerial[] otherRefs = bdeAccounting.getDocumentSerials(_doc.AccountDocumentID);

            bERPHtmlTable table = new bERPHtmlTable();
            bERPHtmlTableRowGroup header = new bERPHtmlTableRowGroup(TableRowGroupType.header);
            table.groups.Add(header);
            bERPHtmlBuilder.htmlAddRow(header
                , new bERPHtmlTableCell(TDType.ColHeader, "No.")
                , new bERPHtmlTableCell(TDType.ColHeader, "Item Code")
                , new bERPHtmlTableCell(TDType.ColHeader, "Item Name")
                , new bERPHtmlTableCell(TDType.ColHeader, "Quantity")
                , new bERPHtmlTableCell(TDType.ColHeader, "Unit")
                , new bERPHtmlTableCell(TDType.ColHeader, "Unit Price")
                , new bERPHtmlTableCell(TDType.ColHeader, "Price")
                , new bERPHtmlTableCell(TDType.ColHeader, "Remark")
                );
            bERPHtmlTableRowGroup body = new bERPHtmlTableRowGroup(TableRowGroupType.body);
            table.groups.Add(body);
            table.css = "itemTable";
            double total = 0;
            int no = 1;
            foreach (TransactionDocumentItem item in invoice.items)
            {
                TransactionItems titem = bde.GetTransactionItems(item.code);
                if (titem == null)
                {
                    bERPHtmlBuilder.htmlAddRow(body
                    , new bERPHtmlTableCell((no++).ToString())
                        , new bERPHtmlTableCell(item.code)
                        , new bERPHtmlTableCell("Item couldn't be loaded", 6)
                        );

                }
                else
                {
                    string remark = null;
                    if (titem.IsInventoryItem)
                    {
                        if (item.directExpense)
                            remark = "Direct Expense";

                    }
                    else if (item.prepaid)
                        remark = (remark == null ? "" : remark + "\n") + "Pre paid";

                    bERPHtmlTableCell remarkCell;
                    bERPHtmlBuilder.htmlAddRow(body
                        , new bERPHtmlTableCell((no++).ToString())
                        , new bERPHtmlTableCell(titem.Code)
                        , new bERPHtmlTableCell(titem.Name)
                        , new bERPHtmlTableCell(AccountBase.FormatAmount(item.quantity))
                        , new bERPHtmlTableCell(titem.MeasureUnitID == -1 ? "" : bde.GetMeasureUnit(titem.MeasureUnitID).Name)
                        , new bERPHtmlTableCell(AccountBase.FormatAmount(item.unitPrice))
                        , new bERPHtmlTableCell(AccountBase.FormatAmount(item.unitPrice * item.quantity))
                        , remarkCell = new bERPHtmlTableCell("")
                        );
                    remarkCell.innerText = remark;
                    total += item.unitPrice * item.quantity;

                }
            }
            table.build(sb);
            sb.Append(string.Format("<div class='invoiceTotalItem'>{0}</div>", HttpUtility.HtmlEncode("Total price: " + AccountBase.FormatAmount(total))));
            foreach (TaxImposed tax in invoice.taxImposed)
            {
                sb.Append(string.Format("<div class='invoiceTotalItem'>{0}</div>", HttpUtility.HtmlEncode(tax.TaxType.ToString() + ": " + AccountBase.FormatAmount(tax.TaxValue))));
                total += tax.TaxValue;
            }
            sb.Append(string.Format("<div class='invoiceTotalItem'>{0}</div>", HttpUtility.HtmlEncode("Total after taxes: " + AccountBase.FormatAmount(total))));
            if (!AccountBase.AmountEqual(invoice.supplierCredit, 0))
                sb.Append(string.Format("<div class='invoiceTotalItem'>{0}</div>", HttpUtility.HtmlEncode("Credit from supplier: -" + AccountBase.FormatAmount(invoice.supplierCredit))));
            if (!AccountBase.AmountEqual(invoice.settleAdvancePayment, 0))
                sb.Append(string.Format("<div class='invoiceTotalItem'>{0}</div>", HttpUtility.HtmlEncode("Deduct from advance payment: -" + AccountBase.FormatAmount(invoice.settleAdvancePayment))));
            double netPayment = total - invoice.supplierCredit - invoice.settleAdvancePayment;
            sb.Append(string.Format("<div class='invoiceTotalItem'>{0}</div>", HttpUtility.HtmlEncode("Net payment: " + AccountBase.FormatAmount(netPayment))));
            if (AccountBase.AmountGreater(netPayment, 0))
            {
                sb.Append(string.Format("<div class='invoiceTotalItem'>{0}</div>", HttpUtility.HtmlEncode("Payment method: " + PaymentMethodHelper.GetPaymentTypeText(invoice.paymentMethod))));
                if (PaymentMethodHelper.IsPaymentMethodWithReference(invoice.paymentMethod))
                    sb.Append(string.Format("<div class='invoiceTotalItem'>{0}</div>", HttpUtility.HtmlEncode(PaymentMethodHelper.GetPaymentTypeReferenceName(invoice.paymentMethod) + ": " + invoice.paymentMethodReference)));
                if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(invoice.paymentMethod) && invoice.serviceChargePayer == ServiceChargePayer.Company)
                    sb.Append(string.Format("<div class='invoiceTotalItem'>{0}</div>", HttpUtility.HtmlEncode("Bank service charge: " + AccountBase.FormatAmount(invoice.serviceChargeAmount))));

            }
            sb.Append("</div>");
            return sb.ToString();
        }

        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            Purchase2Document purchase = (Purchase2Document)doc;
            if (PaymentMethodHelper.IsPaymentMethodCredit(purchase.paymentMethod))
                return null;

            TradeRelation supplier = bde.GetRelation(purchase.relationCode);
            string html = HttpUtility.HtmlEncode("Payment to Supplier: " + (supplier == null ? "[Deleted Supplier " + purchase.relationCode + "]" : supplier.Name));
            TransactionItems titem;
            List<TransactionDocumentItem> filteredItems = new List<TransactionDocumentItem>();
            double filteredTotalBeforeTax = 0;
            foreach (TransactionDocumentItem item in purchase.items)
            {
                if (option.costCenters != null)
                {
                    if (option.costCenters.Contains(item.costCenterID))
                    {
                        filteredItems.Add(item);
                        filteredTotalBeforeTax += item.quantity * item.unitPrice;
                    }
                }
                else
                {
                    filteredTotalBeforeTax += item.quantity * item.unitPrice;
                    filteredItems.Add(item);
                }
            }

            if (filteredItems.Count == 1)
            {
                titem = bde.GetTransactionItems(filteredItems[0].code);
                html += string.Format("<br/>{0}{1}<br/>"
                    , HttpUtility.HtmlEncode(AccountBase.AmountEqual(filteredItems[0].quantity, 1) ? "" : (filteredItems[0].quantity + " " + bde.GetMeasureUnit(titem.MeasureUnitID).Name + " ")), HttpUtility.HtmlEncode(titem.Name));
            }
            else if (filteredItems.Count > 1)
            {
                html += "<table class='dcfrPurchaseItemTable'>";
                foreach (TransactionDocumentItem item in filteredItems)
                {
                    titem = bde.GetTransactionItems(item.code);
                    html += string.Format("<tr><td>{1}</td><td>{0}</td><td>{2} birr</td></tr>", HttpUtility.HtmlEncode(item.quantity + " " + bde.GetMeasureUnit(titem.MeasureUnitID).Name), HttpUtility.HtmlEncode(titem.Name), AccountBase.FormatAmount(item.price));
                }
                html += "</table>";
            }

            //all items filtered out return empty
            if (AccountBase.AmountEqual(filteredTotalBeforeTax, 0))
                return null;

            double filterFactor = AccountBase.AmountEqual(purchase.totalBeforeTax, 0) ? 0 : filteredTotalBeforeTax / purchase.totalBeforeTax;
            if (!AccountBase.AmountEqual(purchase.totalTax, 0))
            {
                html += HttpUtility.HtmlEncode("Total Before Tax :" + AccountBase.FormatAmount(filteredTotalBeforeTax));
                html += HttpUtility.HtmlEncode(" Tax :" + AccountBase.FormatAmount(filterFactor * purchase.totalTax));
            }
            html += HttpUtility.HtmlEncode(" Invoice Total :" + AccountBase.FormatAmount(filteredTotalBeforeTax + filterFactor * purchase.totalTax));
            if (!AccountBase.AmountEqual(purchase.settleAdvancePayment, 0))
            {
                html += "<br/>" + HttpUtility.HtmlEncode("Accounted from Advance Payment :  -" + AccountBase.FormatAmount(filterFactor * purchase.settleAdvancePayment));
            }
            if (!AccountBase.AmountEqual(purchase.retention, 0))
            {
                html += "<br/>" + HttpUtility.HtmlEncode("Retained:  -" + AccountBase.FormatAmount(filterFactor * purchase.retention));
            }
            if (!AccountBase.AmountEqual(purchase.supplierCredit, 0))
            {
                html += "<br/>" + HttpUtility.HtmlEncode("Credit Provided By Supplier:  -" + AccountBase.FormatAmount(filterFactor * purchase.supplierCredit));
            }
            if (!AccountBase.AmountEqual(purchase.overPayment, 0))
            {
                html += "<br/>" + HttpUtility.HtmlEncode("Over Payment to Supplier:  " + AccountBase.FormatAmount(filterFactor * purchase.overPayment));
            }
            if (!AccountBase.AmountEqual(purchase.underPayment, 0))
            {
                html += "<br/>" + HttpUtility.HtmlEncode("Under Payment to Supplier:  -" + AccountBase.FormatAmount(filterFactor * purchase.underPayment));
            }
            if (!string.IsNullOrEmpty(purchase.ShortDescription))
                html += string.Format("<br/><b>Note:</b>{0}", purchase.ShortDescription);
            List<CashFlowItem> items = new List<CashFlowItem>();
            items.Add(new CashFlowItem(-1, -1, html, filterFactor * purchase.paidAmount));

            CashTransactionUtility.addCashTransactionMainItem(items, purchase.paymentMethod, purchase.assetAccountID, filterFactor * purchase.paidAmount, filterFactor * purchase.serviceChargeAmount, purchase.serviceChargePayer, purchase.checkNumber, purchase.cpoNumber, purchase.transferReference);
            CashTransactionUtility.addCashTransactionCostItem(items, purchase.paymentMethod, purchase.assetAccountID, filterFactor * purchase.serviceChargeAmount, purchase.serviceChargePayer);
            return items;
        }

        public string getInstrumentNo(AccountDocument _doc, out string typeName)
        {
            Purchase2Document doc = _doc as Purchase2Document;
            if (!PaymentMethodHelper.IsPaymentMethodWithReference(doc.paymentMethod))
            {
                typeName = null;
                return null;
            }
            typeName = PaymentMethodHelper.GetPaymentTypeReferenceName(doc.paymentMethod);
            return PaymentMethodHelper.selectReference(doc.paymentMethod, doc.checkNumber, doc.cpoNumber, doc.transferReference);
        }
    }
}
