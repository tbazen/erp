﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using System.Web;

namespace BIZNET.iERP.Server
{
    public class DHPurchaseInvoice : DeleteReverseDocumentHandler, IDocumentServerHandler
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHPurchaseInvoice(AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        public bool CheckPostPermission(int docID, AccountDocument _doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            if (_doc == null)
                return false;
            return true;
        }
        public int Post(int AID, AccountDocument _doc)
        {
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    PurchaseInvoiceDocument invoice = (PurchaseInvoiceDocument)_doc;
                    Purchase2Document purchase = bdeAccounting.GetAccountDocument(invoice.purchaseTransactionDocumentID, true) as Purchase2Document;
                    if (purchase == null)
                        throw new ServerUserMessage("Invalid purchase document ID:{0}", purchase.AccountDocumentID);
                    if (purchase.invoiced)
                        throw new ServerUserMessage("Purhcase docuemnt is already invoiced");
                    bool update = false;
                    if (_doc.AccountDocumentID != -1)
                    {
                        base.bdeAccounting.DeleteAccountDocument(AID, _doc.AccountDocumentID, true);
                        update = true;
                    }
                    List<TransactionItems> items = new List<TransactionItems>();
                    foreach (TransactionDocumentItem ditem in purchase.items)
                    {
                        TransactionItems item = bde.GetTransactionItems(ditem.code);
                        if (item == null)
                            throw new ServerUserMessage("Invalid item code:" + item.Code);
                        items.Add(item);
                        foreach (TransactionDocumentItem otherItem in purchase.items)
                        {
                            if (otherItem == ditem)
                                continue;
                            if (otherItem.code.Equals(ditem.code) && otherItem.costCenterID==ditem.costCenterID)
                                throw new ServerUserMessage("It is not allowed to repeat items in a single purchase document");
                        }
                    }
                    TradeRelation supplier = bde.GetSupplier(purchase.relationCode);
                    invoice.taxRates = bde.getTaxRates();
                    double taxable;
                    supplier = bde.GetRelation(purchase.relationCode);
                    purchase.taxRates = bde.getTaxRates();
                    bool vatApplies, whApplies;
                    double amount;
                    double[] pwt;
                    TaxImposed[] taxes = Purchase2Document.getTaxes(supplier, bde.SysPars.companyProfile,
                        invoice.items
                        , items.ToArray()
                        , invoice.taxRates
                        , !AccountBase.AmountLess(purchase.manualVATBase, 0), invoice.manualVATBase
                        , true//!AccountBase.AmountLess(purchase.manualWithholdBase, 0)
                        , 0//invoice.manualWithholdBase
                        , out amount
                        , out taxable
                        , out vatApplies, out whApplies, out pwt
                        );
                    if (taxes.Length != invoice.taxImposed.Length)
                        throw new ServerUserMessage("The tax amounts the client sent don't match the values calculated on the server.\nThis might indicate an out of date client");

                    for (int i = 0; i < taxes.Length; i++)
                        if (!invoice.taxImposed[i].Equals(taxes[i]))
                            throw new ServerUserMessage("The tax amount client sent don't match the values calculated on the server.\nThis might indicate an out of date client");


                    
                    List<TransactionDocumentItem> invoicedItem = new List<TransactionDocumentItem>(purchase.items);
                    invoicedItem = TransactionDocumentItem.subTract(invoicedItem, invoice.items);

                    Dictionary<TaxType, TaxImposed> netTax = new Dictionary<TaxType, TaxImposed>();
                    Dictionary<TaxType, TaxImposed> purchaseTax = new Dictionary<TaxType, TaxImposed>();

                    foreach (TaxImposed t in purchase.taxImposed)
                    {
                        Purchase2Document.addTax(netTax, t);
                        Purchase2Document.addTax(purchaseTax, t);
                    }
                    foreach (TaxImposed t in invoice.taxImposed)
                    {
                        TaxImposed ti = new TaxImposed();
                        ti.TaxType = t.TaxType;
                        ti.TaxValue = -t.TaxValue;
                        ti.TaxBaseValue = -t.TaxBaseValue;
                        Purchase2Document.addTax(netTax, ti);
                    }
                    double totalBeforeTax = 0;
                    foreach (int otherInvoiceID in purchase.invoiceIDs)
                    {
                        
                        PurchaseInvoiceDocument otherInvoice = null;
                        if (otherInvoiceID == invoice.AccountDocumentID)
                            otherInvoice = invoice;
                        else
                            otherInvoice = bdeAccounting.GetAccountDocument(otherInvoiceID, true) as PurchaseInvoiceDocument;
                        if (otherInvoice == null)
                            throw new ServerUserMessage("Invoice ID:{0} in purchase document ID:{1} is invalid", otherInvoiceID, invoice.purchaseTransactionDocumentID);
                        invoicedItem = TransactionDocumentItem.subTract(invoicedItem, otherInvoice.items);
                        totalBeforeTax += otherInvoice.totalBeforeTax;
                        foreach (TaxImposed t in otherInvoice.taxImposed)
                        {
                            TaxImposed ti = new TaxImposed();
                            ti.TaxType = t.TaxType;
                            ti.TaxValue = -t.TaxValue;
                            ti.TaxBaseValue = -t.TaxBaseValue;
                            Purchase2Document.addTax(netTax, ti);
                        }
                    }
                    //assert invoice items are also in purchase document
                    foreach(TransactionDocumentItem i in invoicedItem)
                        if(AccountBase.AmountLess(i.quantity,0))
                            throw new ServerUserMessage("Only items in cluded in the purchase document can be included in the invoice");

                    //Assert invoice tax not more than purhcase tax
                    foreach (KeyValuePair<TaxType, TaxImposed> kv in netTax)
                    {
                        if (!purchaseTax.ContainsKey(kv.Key))
                            throw new ServerUserMessage("The purhcase don't contain tax type {0}", kv.Key);
                        TaxImposed purchaseTaxImposed = purchaseTax[kv.Key];
                        if (AccountBase.AmountGreater(purchaseTaxImposed.TaxValue, 0))// positive taxes
                        {
                            if (AccountBase.AmountLess(kv.Value.TaxValue, 0))
                                throw new ServerUserMessage("Taxes in the invoices can't be higher than the tax calculated for the purchase.");
                        }
                        else //negative taxs (withholding)
                        {
                            if (AccountBase.AmountGreater(kv.Value.TaxValue, 0))
                                throw new ServerUserMessage("Taxes in the invoices can't be higher than the tax calculated for the purchase.");

                        }
                    }
                    if (AccountBase.AmountGreater(totalBeforeTax, purchase.totalBeforeTax))
                        throw new ServerUserMessage("The amount before tax in the invoices can't be higher than the purchase.");
                    
                    
                    List<TransactionOfBatch> tran = new List<TransactionOfBatch>();

                    foreach (TaxImposed tax in invoice.taxImposed)
                    {
                        switch (tax.TaxType)
                        {
                            case TaxType.VAT:
                                if (bde.SysPars.companyProfile.TaxRegistrationType == TaxRegisrationType.VAT)
                                {
                                    //debit input vat
                                    CostCenterAccount csVATAccountNotInvoiced = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID,
                                        bde.SysPars.inputVATAccountIDNotInvoiced
                                        );
                                    CostCenterAccount csVATAccountInvoiced = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID,
                                        bde.SysPars.inputVATAccountID
                                        );
                                    tran.Add(new TransactionOfBatch(
                                        csVATAccountNotInvoiced.id
                                        , -tax.TaxValue, string.Format("Invoice {0}", invoice.PaperRef)));
                                    tran.Add(new TransactionOfBatch(
                                        csVATAccountInvoiced.id
                                        , tax.TaxValue, string.Format("Invoice {0}", invoice.PaperRef)));
                                }
                                break;
                            case TaxType.VATWitholding:
                                //credit withheld input vat
                                tran.Add(new TransactionOfBatch(
                                    bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID
                                    , bde.SysPars.outputVATWithHeldNotInvoicedAccountID
                                    ).id
                                            , -tax.TaxValue, ""));
                                tran.Add(new TransactionOfBatch(
                                        bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID
                                        , bde.SysPars.outputVATWithHeldAccountID
                                        ).id
                                                , tax.TaxValue, ""));
                                break;
                            case TaxType.WithHoldingTax:
                                CostCenterAccount csWithHoldingAccountNotIncoiced = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID,
                                    bde.SysPars.collectedWithHoldingTaxNotInvoicedAccountID
                                    );
                                CostCenterAccount csWithHoldingAccountInvoiced = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID,
                                    bde.SysPars.collectedWithHoldingTaxAccountID
                                    );

                                tran.Add(new TransactionOfBatch(csWithHoldingAccountNotIncoiced.id, -tax.TaxValue
                                    , string.Format("Invoice {0}", invoice.PaperRef)));

                                tran.Add(new TransactionOfBatch(csWithHoldingAccountInvoiced.id, tax.TaxValue
                                    , string.Format("Invoice {0}", invoice.PaperRef)));

                                break;
                            default:
                                throw new ServerUserMessage("Unsupported tax type for invoices:{0}", tax.TaxType);
                        }
                    }

                    invoice.AccountDocumentID=bdeAccounting.RecordTransaction(AID, invoice, tran.ToArray(), DocumentTypedReference.cloneArray(false,bdeAccounting.getDocumentSerials(purchase.AccountDocumentID)));
                    if (INTAPS.ArrayExtension.isNullOrEmpty(purchase.invoiceIDs) || !INTAPS.ArrayExtension.contains(purchase.invoiceIDs, invoice.AccountDocumentID))
                    {
                        purchase.invoiceIDs = INTAPS.ArrayExtension.appendToArray(purchase.invoiceIDs, invoice.AccountDocumentID);
                        bdeAccounting.UpdateAccountDocumentData(AID, purchase);
                    }                    
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return invoice.AccountDocumentID;
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

  
    }
}
