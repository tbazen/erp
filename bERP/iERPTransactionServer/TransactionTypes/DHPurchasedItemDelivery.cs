﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.Accounting;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public class DHPurchasedItemDelivery : DHItemTransactionBase, IDocumentServerHandler
    {
        public DHPurchasedItemDelivery(AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {

        }
        public bool CheckPostPermission(int docID, AccountDocument _doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            if (_doc == null)
            {
                PurchasedItemDeliveryDocument deliveryDoc = (PurchasedItemDeliveryDocument)bdeAccounting.GetAccountDocument(docID, true);
                if (bdeAccounting.GetAccountDocument(deliveryDoc.purchaseDocumentID,true) == null)
                    return true;
                else
                    return false;
            }
            return true;
        }
        public int Post(int AID, AccountDocument _doc)
        {
            PurchasedItemDeliveryDocument doc = (PurchasedItemDeliveryDocument)_doc;
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    DocumentTypedReference[] refs;
                    bool newDocument = _doc.AccountDocumentID == -1;
                    if (!newDocument)
                    {
                        PurchasedItemDeliveryDocument old = bdeAccounting.GetAccountDocument(_doc.AccountDocumentID, true) as PurchasedItemDeliveryDocument;
                        if (old != null)
                        {
                            doc.relationCode = old.relationCode;
                            doc.purchaseDocumentID = old.purchaseDocumentID;
                            bdeAccounting.DeleteAccountDocument(AID, _doc.AccountDocumentID, true);
                        }
                    }
                    Purchase2Document purchase = bdeAccounting.GetAccountDocument(doc.purchaseDocumentID, true) as Purchase2Document;
                    if (purchase == null && doc.orderItems==null)
                        throw new ServerUserMessage("Ordered items is not known from the item delivery");
                    refs = DocumentTypedReference.createArray(DocumentTypedReference.cloneArray(false, bdeAccounting.getDocumentSerials(doc.purchaseDocumentID)), doc.voucher);
                    if (refs.Length == 0)
                    {
                        throw new ServerUserMessage("No reference provided");
                    }

                    if (doc.voucher != null)
                        doc.PaperRef = doc.voucher.reference;
                    else
                        doc.PaperRef = refs[0].reference;


                    TradeRelation supplier = bde.GetSupplier(doc.relationCode);

                    List<TransactionItems> items = new List<TransactionItems>();
                    foreach (TransactionDocumentItem ditem in doc.items)
                    {
                        TransactionItems item = bde.GetTransactionItems(ditem.code);
                        if (item == null)
                            throw new ServerUserMessage("Invalid item code:" + item.Code);
                        if (item.inventoryType == InventoryType.FinishedGoods)
                            throw new ServerUserMessage("Finished goods can't be purchased");
                        items.Add(item);
                        ditem.price = ditem.unitPrice * ditem.quantity;
                    }


                    //items
                    int j = -1;
                    List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
                    List<TransactionOfBatch> summeryTransactions = new List<TransactionOfBatch>();
                    bool addSalesTaxes = purchase!=null && purchase.isSalesTaxAddedToCost(bde.SysPars.companyProfile, supplier);

                    var orderedItems = purchase == null ? doc.orderItems : purchase.items;

                    foreach (TransactionDocumentItem ditem in doc.items)
                    {
                        j++;


                        double price = 0;
                        bool found = false;
                        for (int k = 0; k < orderedItems.Length; k++)
                        {
                            if (orderedItems[k].code.Equals(ditem.code))
                            {
                                if (addSalesTaxes)
                                    price = purchase.costWithTax[k] * ditem.quantity / purchase.items[k].quantity;
                                else
                                    price = orderedItems[k].unitPrice * ditem.quantity;
                                found = true;
                                break;
                            }
                        }
                        if (!found) //Redundent check
                            throw new ServerUserMessage("Delivery item not in the purchase");
                        ditem.unitPrice = price / ditem.quantity;

                        TransactionItems item = items[j];
                        if (item.GoodOrService == GoodOrService.Service)
                        {
                            //credit prepaid expense account
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Prepaid Expense"
                            , ditem.costCenterID, item.prePaidExpenseAccountID, item.prePaidExpenseSummaryAccountID, 0
                             , -price, string.Format("Deliverd service on delivery {0}", doc.PaperRef));

                            //debit the expense account
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Expense Account"
                            , ditem.costCenterID, item.expenseAccountID, item.expenseSummaryAccountID, 0
                             , price, string.Format("Deliverd amount on delivery {0}", doc.PaperRef));

                        }
                        else
                        {
                            StoreInfo store = bde.GetStoreByCostCenterID(ditem.costCenterID);
                            if (store == null)
                                throw new ServerUserMessage("Store not selected");
                            //debit the store inventory account quantity
                            tran.Add(new TransactionOfBatch(
                            bdeAccounting.GetOrCreateCostCenterAccount(AID, ditem.costCenterID, item.materialAssetAccountID(true)).id
                            , TransactionItem.MATERIAL_QUANTITY
                            , ditem.quantity, string.Format("Deliverd quantity on delivery {0}", doc.PaperRef)));

                            //debit the store inventory account money
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Material Asset Account"
                            , ditem.costCenterID, item.materialAssetAccountID(true), item.materialAssetSummaryAccountID(true), 0
                             , price, string.Format("Deliverd amount on delivery {0}", doc.PaperRef));


                            //credit supplier receivable account money
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "On Order Supply"
                            , supplier.onSupplierOrderCostCenterID, item.pendingOrderAccountID, item.pendingOrderSummaryAccountID, 0
                             , -price, string.Format("Deliverd amount on delivery {0}", doc.PaperRef));

                            //credit supplier receivable account quantity
                            tran.Add(new TransactionOfBatch(
                            bdeAccounting.GetOrCreateCostCenterAccount(AID, supplier.onSupplierOrderCostCenterID, item.pendingOrderAccountID).id
                            , TransactionItem.MATERIAL_QUANTITY
                            , -ditem.quantity, string.Format("Deliverd quantity on delivery {0}", doc.PaperRef)));
                        }
                    }
                    addNetSummaryTransaction(AID, tran, "Net transaction of order " + doc.PaperRef, summeryTransactions);
                    int ret = bdeAccounting.RecordTransaction(AID, doc, tran.ToArray(), refs);
                    if (purchase != null)
                    {
                        if (INTAPS.ArrayExtension.isNullOrEmpty(purchase.deliverieIDs) || !INTAPS.ArrayExtension.contains(purchase.deliverieIDs, doc.AccountDocumentID))
                        {
                            purchase.deliverieIDs = INTAPS.ArrayExtension.appendToArray(purchase.deliverieIDs, doc.AccountDocumentID);
                            bdeAccounting.UpdateAccountDocumentData(AID, purchase);
                        }
                    }
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

        public override string GetHTML(AccountDocument _doc)
        {
            return "";
        }
    }
}