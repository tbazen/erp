﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.Accounting;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public class DHPurchasedPrePayment:DHItemTransactionBase,IDocumentServerHandler
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHPurchasedPrePayment(AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        public bool CheckPostPermission(int docID, AccountDocument _doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return true;
        }

        public int Post(int AID, AccountDocument _doc)
        {
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    
                    PurchasePrePaymentDocument doc = (PurchasePrePaymentDocument)_doc;
                    
                    DocumentTypedReference[] refs;
                    bool newDocument = _doc.AccountDocumentID == -1;
                    if (!newDocument)
                    {
                        PurchasePrePaymentDocument old = bdeAccounting.GetAccountDocument(_doc.AccountDocumentID, true) as PurchasePrePaymentDocument;
                        if (old != null)
                        {
                            doc.relationCode = old.relationCode;
                            doc.purchaseDocumentID = old.purchaseDocumentID;
                            bdeAccounting.DeleteAccountDocument(AID, _doc.AccountDocumentID, true);
                        }
                        
                    }

                    Purchase2Document purchase = bdeAccounting.GetAccountDocument(doc.purchaseDocumentID, true) as Purchase2Document;
                    if (purchase == null)
                        throw new ServerUserMessage("Invalid purchase docuemnt document ID " + doc.purchaseDocumentID);
                    refs = DocumentTypedReference.createArray(DocumentTypedReference.cloneArray(false, bdeAccounting.getDocumentSerials(doc.purchaseDocumentID)), doc.voucher);
                    if (refs.Length == 0)
                    {
                        throw new ServerUserMessage("No reference provided");
                    }


                    if (doc.voucher != null)
                        doc.PaperRef = doc.voucher.reference;
                    else
                        doc.PaperRef = refs[0].reference;
                    

                    
                    List<TransactionItems> items = new List<TransactionItems>();
                    foreach (TransactionDocumentItem ditem in doc.items)
                    {
                        TransactionItems item = bde.GetTransactionItems(ditem.code);
                        if (item == null)
                            throw new ServerUserMessage("Invalid item code:" + item.Code);
                        if (!item.IsExpenseItem)
                            throw new ServerUserMessage("Only expense items can be prepaid");
                        if (item.IsInventoryItem)
                            throw new ServerUserMessage("Inventory items can't be prepaid");
                        items.Add(item);

                    }
                    /*Purchase2Document purchase = bdeAccounting.GetAccountDocument(doc.paymentDocumentID, true) as Purchase2Document;
                    if (purchase == null)
                        throw new ServerUserMessage("System inconsistency: the associated purchase document is not found in the database");*/
                    TradeRelation supplier = bde.GetSupplier(doc.relationCode);
                    //items
                    int j = -1;
                    List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
                    List<TransactionOfBatch> summeryTransactions = new List<TransactionOfBatch>();
                    foreach (TransactionDocumentItem ditem in doc.items)
                    {
                        j++;
                        TransactionItems item = items[j];

                        //credit prepaid expense asset acount
                        //tran.Add(new TransactionOfBatch(
                        //bdeAccounting.GetOrCreateCostCenterAccount(AID, ditem.costCenterID, item.prePaidExpenseAccountID).id
                        //        , -ditem.price,"Prepayment"));
                        addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Prepaid Expense"
                                    , ditem.costCenterID, item.prePaidExpenseAccountID, item.prePaidExpenseSummaryAccountID, TransactionItem.DEFAULT_CURRENCY
                                , -ditem.price, "Credit Prepayment");
                        //debit prepaid expense acount
                        //tran.Add(new TransactionOfBatch(
                        //bdeAccounting.GetOrCreateCostCenterAccount(AID, ditem.costCenterID, item.expenseAccountID).id
                        //        , ditem.price, "Prepayment"));
                        addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Expense"
                                    , ditem.costCenterID, item.expenseAccountID, item.expenseSummaryAccountID, TransactionItem.DEFAULT_CURRENCY
                                , ditem.price, "Debit Expense");
                    }
                    addNetSummaryTransaction(AID, tran, "Net transaction of purchase pre payment" + doc.PaperRef, summeryTransactions);
                    int ret = bdeAccounting.RecordTransaction(AID, doc, tran.ToArray(),refs);
                    if (newDocument)
                    {
                        purchase.prepaymentsIDs = INTAPS.ArrayExtension.appendToArray(purchase.prepaymentsIDs, doc.AccountDocumentID);
                        bdeAccounting.UpdateAccountDocumentData(AID, purchase);
                    }
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

        public override string GetHTML(AccountDocument _doc)
        {
            return "";
        }
    }
}
