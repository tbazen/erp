using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    public class DHReceiveFromCashier : DeleteReverseDocumentHandler, IDocumentServerHandler, ICashTransaction
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHReceiveFromCashier(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, AccountDocument doc, UserSessionData userSession)
        {

            return bde.CashierPermissionGranted(userSession);
        }

        public override string GetHTML(AccountDocument doc)
        {
            ReceiveFromCashierDocument bdoc = (ReceiveFromCashierDocument)doc;
            string html = String.Format("<b>Amount:<b/>{0}", TSConstants.FormatBirr(bdoc.amount));
            return html;
        }

        public int Post(int AID, AccountDocument doc)
        {
            ReceiveFromCashierDocument receiveFromCashierDoc = (ReceiveFromCashierDocument)doc;
            TransactionOfBatch[] transactions = CollectTransactionsOfBatch(AID, receiveFromCashierDoc);
            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                if (receiveFromCashierDoc.voucher != null)
                    receiveFromCashierDoc.PaperRef = receiveFromCashierDoc.voucher.reference;
                DocumentTypedReference[] typedRefs = receiveFromCashierDoc.voucher == null ? new DocumentTypedReference[0] :
                new DocumentTypedReference[] { receiveFromCashierDoc.voucher };

                if (doc.AccountDocumentID == -1)
                    ret = bdeAccounting.RecordTransaction(AID, receiveFromCashierDoc,
                    transactions
                    , true, typedRefs);
                else
                    ret = bdeAccounting.UpdateTransaction(AID, receiveFromCashierDoc, transactions, true, typedRefs);
                if (!doc.scheduled && !bde.AllowNegativeTransactionPost())
                {
                    bdeAccounting.validateNegativeBalance
                       (receiveFromCashierDoc.assetAccountID, 0, false, receiveFromCashierDoc.DocumentDate
                       , "The transaction is blcoked because it will cause negative asset balance.");
                }

                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        private TransactionOfBatch[] CollectTransactionsOfBatch(int AID, ReceiveFromCashierDocument receiveFromCashierDoc)
        {
            PayrollBDE bdePayroll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
            List<TransactionOfBatch> trans = new List<TransactionOfBatch>();
            BankAccountInfo ba = null;
            if (PaymentMethodHelper.IsPaymentMethodBank(receiveFromCashierDoc.paymentMethod))
                ba = bde.GetBankAccount(receiveFromCashierDoc.assetAccountID);
            string note = receiveFromCashierDoc.paymentMethod == BizNetPaymentMethod.CPOFromBankAccount ? "CPO service charge" : "Bank transfer service charge";
            if (Account.AmountGreater(receiveFromCashierDoc.amount, 0))
            {
                trans.Add(new TransactionOfBatch(receiveFromCashierDoc.assetAccountID, -receiveFromCashierDoc.amount, receiveFromCashierDoc.ShortDescription));
                trans.Add(new TransactionOfBatch(receiveFromCashierDoc.destinationBankAccID, receiveFromCashierDoc.amount, receiveFromCashierDoc.ShortDescription));
            }

            if (receiveFromCashierDoc.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(receiveFromCashierDoc.serviceChargeAmount, 0))
            {
                int serviceChargeAccountID = ba == null ?
                    bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.bankServiceChargeByCash).id
                    : ba.bankServiceChargeCsAccountID;
                if (serviceChargeAccountID == -1)
                    throw new INTAPS.ClientServer.ServerUserMessage("Bank fee expense account not set");
                CostCenterAccount serviceChargeAccount = bdeAccounting.GetCostCenterAccount(serviceChargeAccountID);
                if (serviceChargeAccount == null)
                    throw new INTAPS.ClientServer.ServerUserMessage("Bank fee expense account not set");

                trans.Add(new TransactionOfBatch(serviceChargeAccount.id, receiveFromCashierDoc.serviceChargeAmount, note));
                trans.Add(new TransactionOfBatch(receiveFromCashierDoc.assetAccountID, -receiveFromCashierDoc.serviceChargeAmount, note));
            }
            return trans.ToArray();
        }


        #endregion

        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            ReceiveFromCashierDocument paymentDoc = (ReceiveFromCashierDocument)doc;
            if (PaymentMethodHelper.IsPaymentMethodCredit(paymentDoc.paymentMethod))
                return null;
            if (!CashTransactionUtility.isCostCenterAccountInCostCenter(bde.Accounting, paymentDoc.assetAccountID, option.costCenters))
                return null;
            List<CashFlowItem> items = new List<CashFlowItem>();
            items.Add(CashFlowItem.createByText(paymentDoc.assetAccountID, -1, "", paymentDoc.amount));

            CashTransactionUtility.addCashTransactionMainItem(items, paymentDoc.paymentMethod, paymentDoc.destinationBankAccID, -paymentDoc.amount, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer, paymentDoc.checkNumber, paymentDoc.cpoNumber, paymentDoc.slipReferenceNo);
            CashTransactionUtility.addCashTransactionCostItem(items, paymentDoc.paymentMethod, paymentDoc.destinationBankAccID, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer);
            return items;
        }
        public string getInstrumentNo(AccountDocument _doc, out string typeName)
        {
            ReceiveFromCashierDocument doc = _doc as ReceiveFromCashierDocument;
            if (!PaymentMethodHelper.IsPaymentMethodWithReference(doc.paymentMethod))
            {
                typeName = null;
                return null;
            }
            typeName = PaymentMethodHelper.GetPaymentTypeReferenceName(doc.paymentMethod);
            return PaymentMethodHelper.selectReference(doc.paymentMethod, doc.checkNumber, doc.cpoNumber, doc.slipReferenceNo);
        }
        
    }
}
