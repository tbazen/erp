using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    public class DHReplenishment: DeleteReverseDocumentHandler, IDocumentServerHandler,ICashTransaction
    {
        PayrollBDE _payRoll = null;
        protected PayrollBDE bdePayroll
        {
            get
            {
                if (_payRoll == null)
                    _payRoll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
                return _payRoll;
            }
        }
        iERPTransactionBDE _bde = null;
        iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }

        public DHReplenishment(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
            
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, AccountDocument doc, UserSessionData userSession)
        {
            return bde.CashierPermissionGranted(userSession);
        }

        public override string GetHTML(AccountDocument doc)
        {
            ReplenishmentDocument bdoc = (ReplenishmentDocument)doc;
            return "<b>Amount:<b/>" + TSConstants.FormatBirr(bdoc.amount);
        }

        public int Post(int AID, AccountDocument doc)
        {
            ReplenishmentDocument replenishmentDoc = (ReplenishmentDocument)doc;
            TransactionOfBatch[] transactions = null;
            bool validateCash=false;
            switch (replenishmentDoc.paymentMethod)
            {
                case BizNetPaymentMethod.Cash:
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.BankTransferFromCash:
                    transactions = CollectCashPaymentTransactionsOfBatch(AID, replenishmentDoc);
                    validateCash = true;
                    break;

                case BizNetPaymentMethod.Check:
                case BizNetPaymentMethod.CPOFromBankAccount:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    transactions = CollectBankPaymentTransactionsOfBatch(AID, replenishmentDoc);
                    validateCash = false;
                    break;
                case BizNetPaymentMethod.None:
                    break;
                default:
                    break;
            }

            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                if (replenishmentDoc.voucher != null)
                    replenishmentDoc.PaperRef = replenishmentDoc.voucher.reference;
                DocumentTypedReference[] typedRefs = replenishmentDoc.voucher == null ? new DocumentTypedReference[0] :
                new DocumentTypedReference[] { replenishmentDoc.voucher };
                if (doc.AccountDocumentID == -1)
                    ret = bdeAccounting.RecordTransaction(AID, replenishmentDoc,
                    transactions
                    , true, typedRefs);
                else
                    ret = bdeAccounting.UpdateTransaction(AID, replenishmentDoc, transactions, true, typedRefs);
                /*if (!doc.scheduled)
                {
                    string account = validateCash ? "cash account" : "bank account";
                    bdeAccounting.validateNegativeBalance
                       (replenishmentDoc.assetAccountID, 0, false, replenishmentDoc.DocumentDate
                       , "Transaction is blocked because there is no sufficient balance in the " + account);
                }*/
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        private TransactionOfBatch[] CollectCashPaymentTransactionsOfBatch(int AID, ReplenishmentDocument replenishmentDoc)
        {
            CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.bankServiceChargeByCash);
            if (csa == null)
                throw new ServerConfigurationError("The bank service charge by cash account not configured properly. The account does not belong to 'Head Quarter' cost center");

            int serviceChargeAccountID = csa.id;
            TransactionOfBatch[] transactions = null;

            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            int withdrawalAccountID = bde.SysPars.WithdrawalAccountID;
            CostCenterAccount csWithdrawal = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, withdrawalAccountID);
            if (csWithdrawal == null)
            {
                throw new ServerUserMessage("'Withdrawal Account' does not belong to 'Head Quarter' cost center. Please join the account to the cost center and try again!");
            }

            creditSide = new TransactionOfBatch(csWithdrawal.id, -replenishmentDoc.amount);
            debitSide = new TransactionOfBatch(replenishmentDoc.assetAccountID, replenishmentDoc.amount);
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(replenishmentDoc.paymentMethod) && replenishmentDoc.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(replenishmentDoc.serviceChargeAmount, 0))
            {
                string note = replenishmentDoc.paymentMethod == BizNetPaymentMethod.CPOFromCash ? "CPO service charge" : "Bank transfer service charge";

                TransactionOfBatch debitSide2, creditSide2;
                debitSide2 = new TransactionOfBatch(serviceChargeAccountID, replenishmentDoc.serviceChargeAmount, note);
                creditSide2 = new TransactionOfBatch(replenishmentDoc.assetAccountID, -replenishmentDoc.serviceChargeAmount, note);
                transactions = new TransactionOfBatch[] { debitSide, creditSide, debitSide2, creditSide2 };
            }
            else
            {
                transactions = new TransactionOfBatch[] { debitSide, creditSide };
            }
            return transactions;
        }
        private TransactionOfBatch[] CollectBankPaymentTransactionsOfBatch(int AID, ReplenishmentDocument replenishmentDoc)
        {
            TransactionOfBatch[] transactions = null;

            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            int withdrawalAccountID = bde.SysPars.WithdrawalAccountID;
            CostCenterAccount csWithdrawal = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, withdrawalAccountID);

            BankAccountInfo ba = bde.GetBankAccount(replenishmentDoc.assetAccountID);
            int serviceChargeAccountID = ba.bankServiceChargeCsAccountID;

            string note = replenishmentDoc.paymentMethod == BizNetPaymentMethod.CPOFromBankAccount ? "CPO service charge" : "Bank transfer service charge";

            creditSide = new TransactionOfBatch(csWithdrawal.id, -replenishmentDoc.amount);
            debitSide = new TransactionOfBatch(ba.mainCsAccount, replenishmentDoc.amount);
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(replenishmentDoc.paymentMethod) && replenishmentDoc.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(replenishmentDoc.serviceChargeAmount, 0))
            {
                TransactionOfBatch debitSide2, creditSide2;
                debitSide2 = new TransactionOfBatch(serviceChargeAccountID, replenishmentDoc.serviceChargeAmount, note);
                creditSide2 = new TransactionOfBatch(ba.mainCsAccount, -replenishmentDoc.serviceChargeAmount, note);
                transactions = new TransactionOfBatch[] { debitSide, creditSide, debitSide2, creditSide2 };
            }
            else
            {
                transactions = new TransactionOfBatch[] { debitSide, creditSide };
            }
            return transactions;
        }
        public void ValidateAccounts(ReplenishmentDocument withdrawalReturnDoc, bool validateCash)
        {
            if (!validateCash) //validate bank
            {
                ValidateBankAccount(withdrawalReturnDoc);
            }
            else
            {
                ValidateCashAccount(withdrawalReturnDoc);
            }
        }

        private void ValidateBankAccount(ReplenishmentDocument withdrawalReturnDoc)
        {
            if (withdrawalReturnDoc.assetAccountID != -1)
            {
                double balance = GetBankAccountBalance(withdrawalReturnDoc);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make bank account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No bank accounts found.\nPlease configure bank account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetBankAccountBalance(ReplenishmentDocument withdrawalReturnDoc)
        {
            return bdeAccounting.GetNetBalanceAsOf(withdrawalReturnDoc.assetAccountID, 0, withdrawalReturnDoc.DocumentDate);
        }

        private void ValidateCashAccount(ReplenishmentDocument withdrawalReturnDoc)
        {
            if (withdrawalReturnDoc.assetAccountID != -1)
            {
                double balance = GetCashAccountBalance(withdrawalReturnDoc);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make cash account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No cash accounts found.\nPlease configure cash account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetCashAccountBalance(ReplenishmentDocument withdrawalReturnDoc)
        {
            return bdeAccounting.GetNetBalanceAsOf(withdrawalReturnDoc.assetAccountID, 0, withdrawalReturnDoc.DocumentDate);
        }
        
        #endregion

        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            WithdrawalDocument paymentDoc = (WithdrawalDocument)doc;
            if (PaymentMethodHelper.IsPaymentMethodCredit(paymentDoc.paymentMethod))
                return null;
            if (!CashTransactionUtility.isTransactionInCostCenter(bde.Accounting, doc.AccountDocumentID, option.costCenters, true))
                return null; 
            List<CashFlowItem> items = new List<CashFlowItem>();
            string html = System.Web.HttpUtility.HtmlEncode("Capital Contribution by Owner");
            items.Add(new CashFlowItem(html, -paymentDoc.amount));

            CashTransactionUtility.addCashTransactionCostItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer);
            return items;
        }
        public string getInstrumentNo(AccountDocument _doc, out string typeName)
        {
            ReplenishmentDocument doc = _doc as ReplenishmentDocument;
            if (!PaymentMethodHelper.IsPaymentMethodWithReference(doc.paymentMethod))
            {
                typeName = null;
                return null;
            }
            typeName = PaymentMethodHelper.GetPaymentTypeReferenceName(doc.paymentMethod);
            return PaymentMethodHelper.selectReference(doc.paymentMethod,doc.checkNumber,doc.cpoNumber,doc.slipReferenceNo);
        }
    }

}
