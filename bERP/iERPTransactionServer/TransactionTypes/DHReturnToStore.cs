﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.Accounting;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public partial class DHReturnToStore : DeleteReverseDocumentHandler, IDocumentServerHandler
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHReturnToStore(AccountingBDE act)
            : base(act)
        {
        }
        public bool CheckPostPermission(int docID, AccountDocument _doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return true;
        }
        
        public int Post(int AID, AccountDocument _doc)
        {
            ReturnToStoreDocument doc = (ReturnToStoreDocument)_doc;
            if (doc.sourceCostCenterID == doc.storeID)
                throw new ServerUserMessage("Source and destination stores must be different");
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    if (doc.voucher != null)
                        doc.PaperRef = doc.voucher.reference;
                    DocumentTypedReference[] typedRefs = doc.voucher == null ? new DocumentTypedReference[0] :
                        new DocumentTypedReference[] { doc.voucher };

                    if (_doc.AccountDocumentID != -1)
                        bdeAccounting.DeleteAccountDocument(AID, _doc.AccountDocumentID,true);
                    List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
                    TransactionDocumentItem[] docItems = TransactionDocumentItem.uniquefy(doc.items);
                    foreach (TransactionDocumentItem ditem in docItems)
                    {
                        if(AccountBase.AmountEqual( ditem.quantity,0))
                            continue;
                        if(AccountBase.AmountLess( ditem.quantity,0))
                            throw new ServerUserMessage("Nevagative quantity not allowed");

                        //validate item code
                        TransactionItems titem = bde.GetTransactionItems(ditem.code);
                        if (titem == null)
                            throw new ServerUserMessage("Invalid item code");
                        if (!titem.IsInventoryItem && !titem.IsFixedAssetItem)
                            throw new ServerUserMessage("Only inventory and fixed assset items are allowed");
                        
                        //get cost center accounts for source and store
                        CostCenterAccount csaAssetSource = bdeAccounting.GetOrCreateCostCenterAccount(AID, doc.sourceCostCenterID,  titem.materialAssetReturnAccountID);
                        CostCenterAccount csaAssetStore = bdeAccounting.GetOrCreateCostCenterAccount(AID, doc.storeID, titem.materialAssetAccountID(true));

                        if (titem.IsFixedAssetItem)
                        {
                            double q = bdeAccounting.GetNetBalanceAsOf(csaAssetSource.id, TransactionItem.MATERIAL_QUANTITY, doc.DocumentDate);
                            double p = bdeAccounting.GetNetBalanceAsOf(csaAssetSource.id, TransactionItem.DEFAULT_CURRENCY, doc.DocumentDate);
                            if (AccountBase.AmountLess(q, ditem.quantity))
                                throw new ServerUserMessage("Insufficient balance for item code:" + titem.Code);
                            ditem.unitPrice = p / q;
                        }

                        
                        ditem.price = ditem.quantity * ditem.unitPrice;


                        tran.Add(new TransactionOfBatch(csaAssetSource.id, TransactionItem.MATERIAL_QUANTITY, -ditem.quantity, ""));
                        tran.Add(new TransactionOfBatch(csaAssetStore.id, TransactionItem.MATERIAL_QUANTITY, ditem.quantity, ""));

                        tran.Add(new TransactionOfBatch(csaAssetSource.id, -ditem.price, ""));
                        tran.Add(new TransactionOfBatch(csaAssetStore.id, ditem.price, ""));

                    }
                    doc.AccountDocumentID = bdeAccounting.RecordTransaction(AID, doc, tran.ToArray(), typedRefs);
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return doc.AccountDocumentID;
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

        public override string GetHTML(AccountDocument _doc)
        {
            ReturnToStoreDocument doc = (ReturnToStoreDocument)_doc;
            double amount = 0;
            foreach (TransactionDocumentItem item in doc.items)
                amount += item.price;
            return System.Web.HttpUtility.HtmlEncode("Total amount transfered:"+AccountBase.FormatAmount(amount));
        }
    }
}
