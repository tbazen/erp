using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;
namespace BIZNET.iERP.Server
{
    public class DHSell2 : DHItemTransactionBase, IDocumentServerHandler
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }

        public DHSell2(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }

        public static void autoCreateRelationAccountField(iERPTransactionBDE bde, int AID,TradeRelation relation, string fieldName)
        {
            System.Reflection.FieldInfo fi=typeof(TradeRelation).GetField(fieldName);
            if(fi==null)
                throw new ServerUserMessage("Invalid trade relation field name:"+fieldName);

            if ((int)fi.GetValue(relation) == -1)// check existence of account and try to create it by updating the registration
            {
                bde.RegisterRelation(AID, relation);
                TradeRelation newRel = bde.GetRelation(relation.Code);
                if ((int)fi.GetValue(newRel) == -1)
                {
                    throw new ServerUserMessage("Couldn't create account for account field " + fieldName + " check the configuration of the related relation category");
                }
                fi.SetValue(relation, fi.GetValue(newRel));
            }
        }
        public static void collectTaxationTransaction(iERPTransactionBDE bde, int AID, TradeRelation customer,List<TransactionOfBatch> tran, SellsTaxInfo sell, List<TransactionItems> items
            ,out double amount
            ,out double netAmount
            ,out TaxImposed[] taxes
            ,out TaxRates taxRates
            )
        {
            double taxable;
            taxRates = bde.getTaxRates();
            bool vatApplies, whApplies;
            sell.taxRates = bde.getTaxRates();
            taxes = Sell2Document.getTaxes
                (
                bde.SysPars.companyProfile
                , customer
                , sell.items
                , items.ToArray()
                , sell.taxRates
                , !AccountBase.AmountLess(sell.manualVATBase, 0)
                , sell.manualVATBase
                , !AccountBase.AmountLess(sell.manualWithholdBase, 0)
                , sell.manualWithholdBase
                , out amount
                , out taxable
                , out vatApplies
                , out whApplies);

            if (!AccountBase.AmountEqual(sell.amount, amount))
                throw new ServerUserMessage("The amount the client sent don't match the values calculated on the server.\nThis might indicate an out of date client");

            if (taxes.Length != sell.taxImposed.Length)
                throw new ServerUserMessage("The tax amounts the client sent don't match the values calculated on the server.\nThis might indicate an out of date client");

            for (int i = 0; i < taxes.Length; i++)
                if (!sell.taxImposed[i].Equals(taxes[i]))
                    throw new ServerUserMessage("The tax amount client sent don't match the values calculated on the server.\nThis might indicate an out of date client");

            //sell.amount = amount;
            //sell.taxImposed = taxes;

            netAmount = sell.amount;
            foreach (TaxImposed tax in taxes)
                netAmount += tax.TaxValue;

            //taxes
            foreach (TaxImposed tax in taxes)
            {
                switch (tax.TaxType)
                {
                    case TaxType.VAT:
                        //debit input vat
                        tran.Add(new TransactionOfBatch(
                            bde.Accounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.outPutVATAccountID).id
                                    , -tax.TaxValue, ""));
                        break;
                    case TaxType.VATWitholding:
                        //credit withheld input vat
                        tran.Add(new TransactionOfBatch(
                            bde.Accounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.outputVATWithHeldAccountID).id
                                    , -tax.TaxValue, ""));
                        break;
                    case TaxType.TOT:
                        //debit TOP expense
                        tran.Add(new TransactionOfBatch(
                            bde.Accounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.collectedTOTAccountID).id
                                    , -tax.TaxValue, ""));
                        break;
                    case TaxType.WithHoldingTax:
                        //credit collected witholding tax
                        tran.Add(new TransactionOfBatch(
                            bde.Accounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.paidWithHoldingTaxAccountID).id
                                    , -tax.TaxValue, ""));
                        break;
                    default:
                        throw new ServerUserMessage("Unsupported tax type:" + TaxImposed.TaxTypeName(tax.TaxType));
                }
            }
        }

        void collectPaymentTransaction(int AID, List<TransactionOfBatch> tran, Sell2Document sell, TradeRelation customer, double netAmount)
        {
            //debit advance payment
            if (!AccountBase.AmountEqual(sell.settleAdvancePayment, 0))
            {
                string note = "TradeRelation advance settled on sale " + sell.PaperRef;
                netAmount -= sell.settleAdvancePayment;

                autoCreateRelationAccountField(bde, AID, customer, "PayableAccountID");
                CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, customer.PayableAccountID);
                tran.Add(new TransactionOfBatch(
                    csa.id
                    , sell.settleAdvancePayment
                    , note));
            }

            //debit credit to customer
            if (!AccountBase.AmountEqual(sell.supplierCredit, 0))
            {
                string note = "Credit for customer on sale " + sell.PaperRef;
                netAmount -= sell.supplierCredit;
                autoCreateRelationAccountField(bde, AID, customer, "ReceivableAccountID");
                CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, customer.ReceivableAccountID);
                tran.Add(new TransactionOfBatch(
                    csa.id
                    , sell.supplierCredit
                    , note));

            }

            //debit retention by customer
            if (!AccountBase.AmountEqual(sell.retention, 0))
            {
                string note = "Retenion by customer on sale " + sell.PaperRef;
                netAmount -= sell.retention;
                autoCreateRelationAccountField(bde, AID, customer, "RetentionReceivableAccountID");
                CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, customer.RetentionReceivableAccountID);
                tran.Add(new TransactionOfBatch(
                    csa.id
                    , sell.retention
                    , note));

            }

            //debit asset
            if (AccountBase.AmountLess(netAmount, 0))
                throw new ServerUserMessage("Negative sales not allowed");
            if (!AccountBase.AmountEqual(netAmount, sell.paidAmount))
            {
                double diff = netAmount - sell.paidAmount;

                string note;
                if (AccountBase.AmountGreater(sell.paidAmount, netAmount)) //if paid amount is greater than the net amount, debit receivable
                {
                    note = string.Format("Overpayment by {0}({1}) on sale {2}", customer.Name, customer.Code, sell.PaperRef);
                    autoCreateRelationAccountField(bde, AID, customer, "PayableAccountID");
                    CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, customer.PayableAccountID);
                    tran.Add(new TransactionOfBatch(
                        csa.id
                        , diff
                        , note));
                }
                else//
                {
                    note = string.Format("Underpayment by {0}({1}) on sale{2}", customer.Name, customer.Code, sell.PaperRef);
                    autoCreateRelationAccountField(bde, AID, customer, "ReceivableAccountID");
                    CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, customer.ReceivableAccountID);
                    tran.Add(new TransactionOfBatch(
                        csa.id
                        , diff
                        , note));
                }
            }


            if (AccountBase.AmountGreater(netAmount, 0))
            {
                if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(sell.paymentMethod) && AccountBase.AmountGreater(sell.serviceChargeAmount, 0)
                    && sell.serviceChargePayer == ServiceChargePayer.Company)
                {
                    //debit service charge expnese
                    int serviceChargeAccountID;
                    if (PaymentMethodHelper.IsPaymentMethodBank(sell.paymentMethod))
                    {
                        BankAccountInfo ba = bde.GetBankAccount(sell.assetAccountID);
                        if (ba == null)
                            throw new ServerUserMessage("Invalid bank account");
                        serviceChargeAccountID = ba.bankServiceChargeCsAccountID;
                    }
                    else
                    {
                        CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.bankServiceChargeByCash);
                        if (csa == null)
                            throw new ServerConfigurationError("The bank service charge by cash account not configured properly");
                        serviceChargeAccountID = csa.id;
                    }
                    tran.Add(new TransactionOfBatch(
                        serviceChargeAccountID
                    , sell.serviceChargeAmount, ""));


                    tran.Add(new TransactionOfBatch(
                    sell.assetAccountID
                    , sell.paidAmount - sell.serviceChargeAmount, ""));

                }
                else
                {
                    if(!AccountBase.AmountEqual(sell.paidAmount,0))
                        tran.Add(new TransactionOfBatch(
                        sell.assetAccountID
                        , sell.paidAmount, ""));
                }
            }
        }
        protected virtual void validateItem(TransactionDocumentItem ditem, TransactionItems item)
        {
            if (item.IsExpenseItem && !item.IsSalesItem)
                throw new ServerUserMessage("Sells of expense items not supported");

        }
        protected virtual CostCenterAccount getItemSinkTransactionAccount(int AID,Sell2Document sell,TradeRelation relation, TransactionDocumentItem ditem, TransactionItems item)
        {
            return bdeAccounting.GetOrCreateCostCenterAccount(AID, ditem.costCenterID, item.salesAccountID);
        }
        internal void collectItemsTransaction(int AID,List<TransactionOfBatch> tran,Sell2Document sell,TradeRelation customer,List<TransactionItems> items)
        {
            fixUnit(sell.items, items);
            List<TransactionOfBatch> summeryTransactions = new List<TransactionOfBatch>();
            //items
            int j = -1;
            foreach (TransactionDocumentItem ditem in sell.items)
            {
                j++;
                TransactionItems item = items[j];
                double cost = ditem.price;
                if (item.IsSalesItem)
                {
                    if (ditem.prepaid)
                    {
                        //credit unearned revenue
                        addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Unearned Revenue"
                            , ditem.costCenterID, item.unearnedRevenueAccountID, item.unearnedSummaryAccountID, 0
                            , -ditem.price, "");

                        //tran.Add(new TransactionOfBatch(
                        //bdeAccounting.GetOrCreateCostCenterAccount(AID, ditem.costCenterID, item.unearnedRevenueAccountID).id
                        //, -ditem.price, ""));

                    }
                    else
                    {
                        //if inventory item credit inventory asset account, debit inventory expense acccount both in ammount and quantity
                        if (item.IsInventoryItem || item.IsFixedAssetItem)
                        {
                            //credit customer payable account money
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Pending Customer Order"
                                , customer.onCustomerOrderCostCenterID, item.pendingDeliveryAcountID, item.pendingDeliverySummaryAccountID, 0
                                , -cost, string.Format("Order amount on sell {0}", sell.PaperRef));

                            //credit custom payable acount quantity
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Pending Customer Order"
                                , customer.onCustomerOrderCostCenterID, item.pendingDeliveryAcountID, item.pendingDeliverySummaryAccountID, TransactionItem.MATERIAL_QUANTITY
                                , -ditem.quantity, string.Format("Order quanity on sell {0}", sell.PaperRef));

                            //debit the main cost center material inflow account quantity
                            tran.Add(new TransactionOfBatch(
                            bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.materialInFlowAccountID).id
                            , TransactionItem.MATERIAL_QUANTITY
                            , ditem.quantity, string.Format("Order quantity on sell{0}", sell.PaperRef)));
                        }
                        else
                        {
                            //if there are finished work, credit that

                            if (item.finishedWorkAccountID > 0)
                            {
                                CostCenterAccount finishedWorkAccount = bdeAccounting.GetOrCreateCostCenterAccount(AID, ditem.costCenterID, item.finishedWorkAccountID);
                                double finishedWorkAmount = bdeAccounting.GetNetBalanceAsOf(finishedWorkAccount.id, 0, sell.DocumentDate);
                                double finishedWorkQuantity = bdeAccounting.GetNetBalanceAsOf(finishedWorkAccount.id, TransactionItem.MATERIAL_QUANTITY, sell.DocumentDate);
                                if (!AccountBase.AmountEqual(finishedWorkQuantity, 0))
                                {
                                    if (AccountBase.AmountLess(finishedWorkQuantity, ditem.quantity))
                                        throw new ServerUserMessage("Insufficient finished work for item " + ditem.code);
                                    ditem.unitPrice = finishedWorkAmount / finishedWorkQuantity;
                                    ditem.price = ditem.unitPrice * ditem.quantity;
                                    tran.Add(new TransactionOfBatch(finishedWorkAccount.id, 0, -ditem.price, ""));
                                    tran.Add(new TransactionOfBatch(finishedWorkAccount.id, TransactionItem.MATERIAL_QUANTITY, -ditem.quantity, ""));
                                    CostCenterAccount csaExpense = bdeAccounting.GetOrCreateCostCenterAccount(AID, ditem.costCenterID, item.expenseAccountID);
                                    tran.Add(new TransactionOfBatch(csaExpense.id, TransactionItem.MATERIAL_QUANTITY, ditem.quantity, ""));
                                    continue;
                                }
                            }
                            //credit revenue
                            if (this.GetType() == typeof(DHSell2))
                                addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Sells Income"
                                    , ditem.costCenterID, item.salesAccountID, item.salesSummaryAccountID, TransactionItem.DEFAULT_CURRENCY
                                , -ditem.price, string.Format("Sells Income from {0}", sell.PaperRef));
                            else
                                tran.Add(new TransactionOfBatch(
                                getItemSinkTransactionAccount(AID, sell, customer, ditem, item).id
                                        , -ditem.price, ""));
                        }
                    }
                    continue;
                }
                if (item.IsFixedAssetItem)
                {
                    //credit fixed asset original value
                    tran.Add(new TransactionOfBatch(
                    bdeAccounting.GetOrCreateCostCenterAccount(AID, ditem.costCenterID, item.originalFixedAssetAccountID).id
                            , -ditem.price, string.Format("Purchase {0}", sell.PaperRef)));
                    continue;
                }
            }
            addNetSummaryTransaction(AID, tran, "Net Summerized Amount on sells " + sell.PaperRef, summeryTransactions);      

        }
        public int Post(int AID, AccountDocument _doc)
        {
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    Sell2Document sell = (Sell2Document)_doc;
                    if (sell.scheduled)
                    {
                        if (sell.getAttachmentArray().Count > 0)
                            throw new ServerUserMessage("Attachment is not supported for scheduled transactions");
                    }
                    if (_doc.AccountDocumentID != -1)
                    {
                        if (!sell.postFromParent)
                        {
                            Sell2Document old = bdeAccounting.GetAccountDocument(_doc.AccountDocumentID, true) as Sell2Document;
                            if (old != null)
                            {
                                deleteExisting(AID, old, true,sell.replaceAttachments);
                                sell.relationCode = old.relationCode;
                            }
                            
                        }
                    }

                    sell.PaperRef = sell.salesVoucher.reference;
                    
                    List<TransactionItems> items = new List<TransactionItems>();
                    foreach (TransactionDocumentItem ditem in sell.items)
                    {
                        TransactionItems item = bde.GetTransactionItems(ditem.code);
                        if (item == null)
                            throw new ServerUserMessage("Invalid item code:" + ditem.code);
                        validateItem(ditem, item);
                        items.Add(item);
                    }
                   
                    TradeRelation customer;
                    double amount;
                    double netAmount;
                    List<TransactionOfBatch> tran=new List<TransactionOfBatch>();
                    customer = bde.GetRelation(sell.relationCode);
                    sell.relationData = customer;
                    collectTaxationTransaction(bde, AID, customer, tran, new SellsTaxInfo(sell), items
                        ,out amount
                        ,out netAmount
                        ,out sell.taxImposed
                        ,out sell.taxRates
                        );
                    collectPaymentTransaction(AID, tran, sell, customer, netAmount);
                    collectItemsTransaction(AID, tran, sell, customer, items);
                    sell.AccountDocumentID = bdeAccounting.RecordTransaction(AID, sell, tran.ToArray(),sell.salesVoucher);
                    postAttachments(AID, sell, customer);
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return sell.AccountDocumentID;
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }


            }
        }

        private void postAttachments(int AID, Sell2Document sell, TradeRelation customer)
        {
            int[][] docIDs = new int[][] { sell.prepaymentsIDs, sell.creditSettlementIDs,sell.deliverieIDs};
            AccountDocument[][] docs = new AccountDocument[][] { sell.prepayments, sell.creditSettlements,sell.deliveries };
            for (int k = 0; k < docIDs.Length; k++)
            {
                int j = 0;
                docIDs[k] = new int[docs[k].Length];
                foreach (AccountDocument attachment in docs[k])
                {
                    if (attachment is SellPrePaymentDocument)
                    {
                        ((SellPrePaymentDocument)attachment).relationCode = customer.Code;
                        ((SellPrePaymentDocument)attachment).sellsAccountDocumentID = sell.AccountDocumentID;
                    }
                    else if (attachment is SoldItemDeliveryDocument)
                    {
                        ((SoldItemDeliveryDocument)attachment).relationCode = customer.Code;
                        ((SoldItemDeliveryDocument)attachment).salesDocumentID= sell.AccountDocumentID;
                    }


                    if (attachment.DocumentDate < sell.DocumentDate)
                        throw new ServerUserMessage("Schedule dates can't be before order date");
                    if (attachment.DocumentDate.Subtract(DateTime.Now).TotalSeconds > AccountingBDE.FUTURE_TOLERANCE_SECONDS)
                    {
                        attachment.scheduled = true;
                        attachment.materialized = false;
                    }
                    else
                    {
                        attachment.scheduled = false;
                        attachment.materialized = true;
                        attachment.materializedOn = attachment.DocumentDate;
                    }
                    docIDs[k][j++] = bdeAccounting.PostGenericDocument(AID, attachment);
                }
            }
        }
        public override void DeleteDocument(int AID, int docID)
        {
            Sell2Document doc = bdeAccounting.GetAccountDocument(docID, true) as Sell2Document;
            if (doc == null)
                throw new ServerUserMessage("Document not found ID:" + docID);

            lock (bde.WriterHelper)
            {
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    deleteExisting(AID, doc,false,true);
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }

        }

        private void deleteExisting(int AID, Sell2Document doc, bool forUpdate, bool replaceAttachment)
        {
            List<Tuple<int, long>> deleteDocument = new List<Tuple<int, long>>();
            foreach (int[] attached in new int[][] { doc.deliverieIDs, doc.creditSettlementIDs, doc.prepaymentsIDs })
            {
                if (attached == null)
                    continue;
                if (replaceAttachment)
                {
                    foreach (int docID in attached)
                    {
                        if (bdeAccounting.DocumentExists(docID))
                        {
                            deleteDocument.Add(new Tuple<int, long>(docID, bdeAccounting.GetAccountDocument(docID, false).tranTicks));
                            //bdeAccounting.DeleteAccountDocument(AID, docID, forUpdate);
                        }
                    }
                }
            }
            deleteDocument.Sort(new Comparison<Tuple<int, long>>(
                delegate(Tuple<int, long> A, Tuple<int, long> B)
                {
                    return B.Item2.CompareTo(A.Item2);
                }
                ));
            foreach (Tuple<int, long> t in deleteDocument)
                bdeAccounting.DeleteAccountDocument(AID, t.Item1, forUpdate);
            bdeAccounting.DeleteAccountDocument(AID, doc.AccountDocumentID, forUpdate);

        }
        public override void ReverseDocument(int AID, int docID)
        {
            throw new ServerUserMessage("Not supported");
        }
        public string GetHTML(AccountDocument _doc)
        {
            return "";
        }

        public virtual bool CheckPostPermission(int docID, AccountDocument _doc, UserSessionData userSession)
        {
            if(_doc!=null)
                return userSession.Permissions.IsPermited("root/ierp/sell/post");
            return userSession.Permissions.IsPermited("root/ierp/sell/delete");
        }
    }
}


