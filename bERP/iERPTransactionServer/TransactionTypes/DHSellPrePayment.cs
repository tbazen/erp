﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.Accounting;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public class DHSellPrePayment:DeleteReverseDocumentHandler,IDocumentServerHandler
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHSellPrePayment(AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        public bool CheckPostPermission(int docID, AccountDocument _doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return true;
        }

        public int Post(int AID, AccountDocument _doc)
        {
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    /*if (_doc.AccountDocumentID != -1)
                        bdeAccounting.DeleteAccountDocument(AID, _doc.AccountDocumentID,true);*/
                    SellPrePaymentDocument doc = (SellPrePaymentDocument)_doc;

                    DocumentTypedReference[] refs;
                    if (_doc.AccountDocumentID != -1)
                    {
                        SellPrePaymentDocument old = bdeAccounting.GetAccountDocument(_doc.AccountDocumentID, true) as SellPrePaymentDocument;
                        doc.relationCode = old.relationCode;
                        doc.sellsAccountDocumentID = old.sellsAccountDocumentID;
                        bdeAccounting.DeleteAccountDocument(AID, _doc.AccountDocumentID, true);
                    }
                    Sell2Document sell2Document=bdeAccounting.GetAccountDocument(doc.AccountDocumentID,true) as Sell2Document;
                    if (sell2Document == null)
                        throw new ServerUserMessage("Invalid sales docuemnt document ID " + doc.sellsAccountDocumentID);

                    refs = DocumentTypedReference.createArray(DocumentTypedReference.cloneArray(false, bdeAccounting.getDocumentSerials(doc.sellsAccountDocumentID)), doc.voucher);
                    if (refs.Length == 0)
                    {
                        throw new ServerUserMessage("No reference provided");
                    }

                    if (doc.voucher != null)
                        doc.PaperRef = doc.voucher.reference;
                    else
                        doc.PaperRef = refs[0].reference;


                    List<TransactionItems> items = new List<TransactionItems>();
                    foreach (TransactionDocumentItem ditem in doc.items)
                    {
                        TransactionItems item = bde.GetTransactionItems(ditem.code);
                        if (item == null)
                            throw new ServerUserMessage("Invalid item code:" + item.Code);
                        if (item.IsExpenseItem && !item.IsSalesItem)
                            throw new ServerUserMessage("Sells of expense item not supported");
                        if (item.IsInventoryItem && item.inventoryType!=InventoryType.FinishedGoods)
                            throw new ServerUserMessage("Inventory items can't be prepaid");
                        items.Add(item);
                    }
                    /*Sell2Document purchase = bdeAccounting.GetAccountDocument(doc.paymentDocumentID, true) as Sell2Document;
                    if (purchase == null)
                        throw new ServerUserMessage("System inconsistency: the associated sells document is not found in the database");*/
                    TradeRelation supplier = bde.GetCustomer(doc.relationCode);
                    //items
                    int j = -1;
                    List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
                    foreach (TransactionDocumentItem ditem in doc.items)
                    {
                        j++;
                        TransactionItems item = items[j];

                        //debit unearned revenue account
                        tran.Add(new TransactionOfBatch(
                        bdeAccounting.GetOrCreateCostCenterAccount(AID, ditem.costCenterID, item.unearnedRevenueAccountID).id
                                , ditem.price,""));
                        //if inventory item credit inventory asset account, debit inventory expense acccount both in ammount and quantity

                        if (item.IsInventoryItem)
                        {
                            CostCenterAccount csaInventory = bdeAccounting.GetOrCreateCostCenterAccount(AID, ditem.costCenterID, item.inventoryAccountID);
                            if (csaInventory == null)
                                throw new ServerUserMessage("Inventory account not found for item code:" + item.Code);
                            CostCenterAccount csaExpense = bdeAccounting.GetOrCreateCostCenterAccount(AID, ditem.costCenterID, item.expenseAccountID);
                            if (csaExpense == null)
                                throw new ServerUserMessage("Expense account not found for item code:" + item.Code);
                            
                            double quantityBalance= bdeAccounting.GetNetBalanceAsOf(csaInventory.id, TransactionItem.MATERIAL_QUANTITY, doc.DocumentDate);
                            
                            if (AccountBase.AmountLess(quantityBalance, ditem.quantity))
                                throw new ServerUserMessage("There are no enough quantity of item code:" + item.Code);
                            
                            tran.Add(new TransactionOfBatch(
                                csaInventory.id, TransactionItem.MATERIAL_QUANTITY
                                    , -ditem.quantity, ""));
                            tran.Add(new TransactionOfBatch(
                                csaExpense.id, TransactionItem.MATERIAL_QUANTITY
                                    , ditem.quantity, ""));
                            tran.Add(new TransactionOfBatch(
                                csaInventory.id
                                    , -ditem.price, ""));
                            tran.Add(new TransactionOfBatch(
                                csaExpense.id
                                    , ditem.price, ""));
                        }
                        else
                        {
                            //credit sells account
                            tran.Add(new TransactionOfBatch(
                            bdeAccounting.GetOrCreateCostCenterAccount(AID, ditem.costCenterID, item.salesAccountID).id
                                    , -ditem.price, ""));
                        }
                    }

                    int ret = bdeAccounting.RecordTransaction(AID, doc, tran.ToArray());

                    if (doc.AccountDocumentID == -1)
                    {
                        sell2Document.prepaymentsIDs = INTAPS.ArrayExtension.appendToArray(sell2Document.prepaymentsIDs, doc.AccountDocumentID);
                        bdeAccounting.UpdateAccountDocumentData(AID, sell2Document);
                    }
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

        public override string GetHTML(AccountDocument _doc)
        {
            return "";
        }
    }
}
