﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using System.Web;

namespace BIZNET.iERP.Server
{
    public class DHSellsInvoice : DeleteReverseDocumentHandler, IDocumentServerHandler
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHSellsInvoice(AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        public bool CheckPostPermission(int docID, AccountDocument _doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return true;
        }
        public int Post(int AID, AccountDocument _doc)
        {
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    SellsInvoiceDocument invoice = (SellsInvoiceDocument)_doc;
                    Sell2Document sells = bdeAccounting.GetAccountDocument(invoice.sellsTransactionDocumentID, true) as Sell2Document;
                    if (sells == null)
                        throw new ServerUserMessage("Invalid sells document ID:{0}", sells.AccountDocumentID);
                    if (sells.invoiced)
                        throw new ServerUserMessage("Sells document is already invoiced");
                    if (_doc.AccountDocumentID != -1)
                    {
                        base.bdeAccounting.DeleteAccountDocument(AID, _doc.AccountDocumentID, true);
                    }
                    List<TransactionItems> items = new List<TransactionItems>();
                    foreach (TransactionDocumentItem ditem in invoice.items)
                    {
                        TransactionItems item = bde.GetTransactionItems(ditem.code);
                        if (item == null)
                            throw new ServerUserMessage("Invalid item code:" + item.Code);
                        items.Add(item);
                        foreach (TransactionDocumentItem otherItem in invoice.items)
                        {
                            if (otherItem == ditem)
                                continue;
                            if (otherItem.code.Equals(ditem.code) && otherItem.costCenterID==ditem.costCenterID)
                                throw new ServerUserMessage("It is not allowed to repeat items in a single sells document");
                        }
                    }
                    
                    TradeRelation customer = bde.GetCustomer(sells.relationCode);
                    List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
                    double amount;
                    double netAmount;
                    DHSell2.collectTaxationTransaction(bde, AID, customer, tran, new SellsTaxInfo(invoice), items, out amount, out netAmount, out sells.taxImposed, out sells.taxRates);


                    //Compare invoices with sells
                    List<TransactionDocumentItem> invoicedItem = new List<TransactionDocumentItem>(sells.items);
                    invoicedItem = TransactionDocumentItem.subTract(invoicedItem, invoice.items);

                    Dictionary<TaxType, TaxImposed> netTax = new Dictionary<TaxType, TaxImposed>();
                    Dictionary<TaxType, TaxImposed> sellsTax = new Dictionary<TaxType, TaxImposed>();

                    foreach (TaxImposed t in sells.taxImposed)
                    {
                        Sell2Document.addTax(netTax, t);
                        Sell2Document.addTax(sellsTax, t);
                    }
                    foreach (TaxImposed t in invoice.taxImposed)
                    {
                        TaxImposed ti = new TaxImposed();
                        ti.TaxType = t.TaxType;
                        ti.TaxValue = -t.TaxValue;
                        ti.TaxBaseValue = -t.TaxBaseValue;
                        Sell2Document.addTax(netTax, ti);
                    }
                    double totalBeforeTax = 0;
                    foreach (int otherInvoiceID in sells.invoiceIDs)
                    {
                        
                        SellsInvoiceDocument otherInvoice = null;
                        if (otherInvoiceID == invoice.AccountDocumentID)
                            otherInvoice = invoice;
                        else
                            otherInvoice = bdeAccounting.GetAccountDocument(otherInvoiceID, true) as SellsInvoiceDocument;
                        if (otherInvoice == null)
                            throw new ServerUserMessage("Invoice ID:{0} in purchase document ID:{1} is invalid", otherInvoiceID, invoice.sellsTransactionDocumentID);
                        invoicedItem = TransactionDocumentItem.subTract(invoicedItem, otherInvoice.items);
                        totalBeforeTax += otherInvoice.totalBeforeTax;
                        foreach (TaxImposed t in otherInvoice.taxImposed)
                        {
                            TaxImposed ti = new TaxImposed();
                            ti.TaxType = t.TaxType;
                            ti.TaxValue = -t.TaxValue;
                            ti.TaxBaseValue = -t.TaxBaseValue;
                            Sell2Document.addTax(netTax, ti);
                        }
                    }
                    //assert invoice items are also in sells document
                    foreach(TransactionDocumentItem i in invoicedItem)
                        if(AccountBase.AmountLess(i.quantity,0))
                            throw new ServerUserMessage("Only items included in the sells document can be included in the invoice");

                    //Assert invoice tax not more than sells tax
                    foreach (KeyValuePair<TaxType, TaxImposed> kv in netTax)
                    {
                        if (!sellsTax.ContainsKey(kv.Key))
                            throw new ServerUserMessage("The sells don't contain tax type {0}", kv.Key);
                        TaxImposed purchaseTaxImposed = sellsTax[kv.Key];
                        if (AccountBase.AmountGreater(purchaseTaxImposed.TaxValue, 0))// positive taxes
                        {
                            if (AccountBase.AmountLess(kv.Value.TaxValue, 0))
                                throw new ServerUserMessage("Taxes in the invoices can't be higher than the tax calculated for the sells.");
                        }
                        else //negative taxs (withholding)
                        {
                            if (AccountBase.AmountGreater(kv.Value.TaxValue, 0))
                                throw new ServerUserMessage("Taxes in the invoices can't be higher than the tax calculated for the sells.");
                        }
                    }
                    if (AccountBase.AmountGreater(totalBeforeTax, sells.totalBeforeTax))
                        throw new ServerUserMessage("The amount before tax in the invoices can't be higher than the sells.");
                    
                    invoice.AccountDocumentID=bdeAccounting.RecordTransaction(AID, invoice, tran.ToArray(), DocumentTypedReference.cloneArray(false,bdeAccounting.getDocumentSerials(sells.AccountDocumentID)));
                    if (INTAPS.ArrayExtension.isNullOrEmpty(sells.invoiceIDs) || !INTAPS.ArrayExtension.contains(sells.invoiceIDs, invoice.AccountDocumentID))
                    {
                        sells.invoiceIDs = INTAPS.ArrayExtension.appendToArray(sells.invoiceIDs, invoice.AccountDocumentID);
                        bdeAccounting.UpdateAccountDocumentData(AID, sells);
                    }                    
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return invoice.AccountDocumentID;
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

  
    }
}
