﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.Accounting;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public class DHSoldItemDelivery : DHItemTransactionBase, IDocumentServerHandler
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHSoldItemDelivery(AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
            
        }
        public bool CheckPostPermission(int docID, AccountDocument _doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            if (_doc == null)
                return false;
            return true;

        }
        public int Post(int AID, AccountDocument _doc)
        {
            SoldItemDeliveryDocument doc = (SoldItemDeliveryDocument)_doc;
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    DocumentTypedReference[] refs;
                    bool newDocument = _doc.AccountDocumentID == -1;
                    if (!newDocument)
                    {
                        SoldItemDeliveryDocument old = bdeAccounting.GetAccountDocument(_doc.AccountDocumentID, true) as SoldItemDeliveryDocument;
                        if (old != null)
                        {
                            doc.relationCode = old.relationCode;
                            doc.salesDocumentID = old.salesDocumentID;
                            bdeAccounting.DeleteAccountDocument(AID, _doc.AccountDocumentID, true);
                        }
                    }
                    Sell2Document sell = bdeAccounting.GetAccountDocument(doc.salesDocumentID, true) as Sell2Document;
                    if (sell == null)
                        throw new ServerUserMessage("Invalid sell docuemnt document ID " + doc.salesDocumentID);
                    refs = DocumentTypedReference.createArray(DocumentTypedReference.cloneArray(false,bdeAccounting.getDocumentSerials(doc.salesDocumentID)), doc.voucher);
                    if (refs.Length == 0)
                    {
                        throw new ServerUserMessage("No reference provided");
                    }

                    if (doc.voucher != null)
                        doc.PaperRef = doc.voucher.reference;
                    else
                        doc.PaperRef = refs[0].reference;


                    TradeRelation customer = bde.GetCustomer(doc.relationCode);

                    List<TransactionItems> items = new List<TransactionItems>();
                    foreach (TransactionDocumentItem ditem in doc.items)
                    {
                        TransactionItems item = bde.GetTransactionItems(ditem.code);
                        if (item == null)
                            throw new ServerUserMessage("Invalid item code:" + item.Code);
                        items.Add(item);
                        ditem.price = ditem.unitPrice * ditem.quantity;
                    }
                    base.fixUnit(doc.items, items);
                    
                    //items
                    int j = -1;
                    List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
                    List<TransactionOfBatch> summeryTransactions = new List<TransactionOfBatch>();
                    bool addSalesTaxes = false; //sell.isSalesTaxAddedToCost(bde.SysPars.companyProfile,customer);
                    foreach (TransactionDocumentItem ditem in doc.items)
                    {
                        j++;

                        double price = 0;
                        int sellsItemCostCenter = -1;
                        bool found = false;
                        for (int k = 0; k < sell.items.Length; k++)
                        {
                            if (sell.items[k].code.Equals(ditem.code))
                            {
                                if (addSalesTaxes)
                                    price = sell.costWithTax[k] * ditem.quantity / sell.items[k].quantity;
                                else
                                    price = sell.items[k].unitPrice * ditem.quantity;
                                sellsItemCostCenter = sell.items[k].costCenterID;
                                found = true;
                                break;
                            }
                        }
                        if (!found) //Redundent check
                            throw new ServerUserMessage("Delivery item not in the sell document");
                        ditem.unitPrice = price / ditem.quantity;

                        TransactionItems item = items[j];
                        if (item.GoodOrService == GoodOrService.Service)
                        {
                            //debit unearned revenue
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Unearned Reveue"
                            , ditem.costCenterID, item.unearnedRevenueAccountID, item.unearnedSummaryAccountID, 0
                             , price, string.Format("Delivered service on delivery document {0}", doc.PaperRef));

                            //credit the income account
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Income Account"
                            , ditem.costCenterID, item.salesAccountID, item.salesSummaryAccountID, 0
                             , -price, string.Format("Delivered service on delivery document {0}", doc.PaperRef));
                        }
                        else
                        {
                            StoreInfo store = _bde.GetStoreByCostCenterID(ditem.costCenterID);
                            if (store == null)
                                throw new ServerUserMessage("Store not selected");
                            
                            CostCenterAccount csaAsset = bdeAccounting.GetOrCreateCostCenterAccount(AID, store.costCenterID, item.materialAssetAccountID(true));
                            double q = 0;
                            double p = 0;
                            q = bdeAccounting.GetNetBalanceAsOf(csaAsset.id, TransactionItem.MATERIAL_QUANTITY, doc.DocumentDate);
                            p = bdeAccounting.GetNetBalanceAsOf(csaAsset.id, TransactionItem.DEFAULT_CURRENCY, doc.DocumentDate);
                            double unitPrice;
                            if (AccountBase.AmountEqual(q, 0))
                                unitPrice = 0;
                            else
                                unitPrice = p / q;
                            ditem.unitPrice = unitPrice;
                            ditem.price = ditem.unitPrice * ditem.quantity;
                            //credit the store inventory account quantity
                            tran.Add(new TransactionOfBatch(
                            bdeAccounting.GetOrCreateCostCenterAccount(AID, ditem.costCenterID, item.materialAssetAccountID(true)).id
                            , TransactionItem.MATERIAL_QUANTITY
                            , -ditem.quantity, string.Format("Delivery of goods to customer {0}", doc.PaperRef)));

                            //credit store inventory account money
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Material Asset Account"
                            , ditem.costCenterID, item.materialAssetAccountID(true), item.materialAssetSummaryAccountID(true), 0
                             , -ditem.quantity * unitPrice, string.Format("Delivery of goods to customer {0}", doc.PaperRef));
                            
                            //debit material expense account
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Material Expense Account"
                            , sellsItemCostCenter, item.expenseAccountID, item.expenseSummaryAccountID, 0
                             , ditem.quantity * unitPrice, string.Format("Delivered goods on delivery document {0}", doc.PaperRef));


                            //credit the income account
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Income Account"
                            , sellsItemCostCenter, item.salesAccountID, item.salesSummaryAccountID, 0
                             , -price, string.Format("Delivered goods on delivery document {0}", doc.PaperRef));

                            
                            //debit supplier payable account money
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, item, "Pending Delivery Account"
                            , customer.onCustomerOrderCostCenterID, item.pendingDeliveryAcountID, item.pendingDeliverySummaryAccountID, 0
                             , price, string.Format("Delivery of goods to customer {0}", doc.PaperRef));

                            //debit supplier payable account quantity
                            tran.Add(new TransactionOfBatch(
                            bdeAccounting.GetOrCreateCostCenterAccount(AID, customer.onCustomerOrderCostCenterID, item.pendingDeliveryAcountID).id
                            , TransactionItem.MATERIAL_QUANTITY
                            , ditem.quantity, string.Format("Delivery of goods to customer {0}", doc.PaperRef)));
                        }
                    }
                    addNetSummaryTransaction(AID, tran, "Net transaction of order " + doc.PaperRef, summeryTransactions);
                    int ret = bdeAccounting.RecordTransaction(AID, doc, tran.ToArray(),refs);
                    if (INTAPS.ArrayExtension.isNullOrEmpty(sell.deliverieIDs) || !INTAPS.ArrayExtension.contains(sell.deliverieIDs, doc.AccountDocumentID))
                    {
                        sell.deliverieIDs = INTAPS.ArrayExtension.appendToArray(sell.deliverieIDs, doc.AccountDocumentID);
                        bdeAccounting.UpdateAccountDocumentData(AID, sell);
                    }
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        
        public override string GetHTML(AccountDocument _doc)
        {
            return "";
        }
    }
}
