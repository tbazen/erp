using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    public class StaffTransactionUtility
    {
        internal static int GetEmployeeAccountID(AccountingBDE bdeAccounting, Employee employee, int parentAccount)
        {
            foreach (int ac in employee.accounts)
            {
                Account acnt = bdeAccounting.GetAccount<Account>(ac);
                if (acnt.PID == parentAccount)
                {
                    return ac;
                }
            }
            return -1;
        }

        public static TransactionOfBatch[] GenerateStaffPaymentReceiptTransaction
            (int AID,
            iERPTransactionBDE bde
            , AccountingBDE bdeAccounting
            , StaffTransactionDocument staffTransactionDoc
            , int otherAccountID
            , int employeeCostCenterID
            , double transactionSign,double multiplier=1)
        {

            bool expenseAccount = bdeAccounting.IsControlOf<Account>(bde.SysPars.expenseAccountID, otherAccountID);
            bool costAccount=bdeAccounting.IsControlOf<Account>(bde.SysPars.costOfSalesAccountID, otherAccountID);
            int costCenterID = expenseAccount || costAccount ? employeeCostCenterID : bde.SysPars.mainCostCenterID;
            if (staffTransactionDoc is StaffPerDiemDocument)
            {
                costCenterID = ((StaffPerDiemDocument)staffTransactionDoc).costCenterID;
            }
            CostCenterAccount otherAccount = bdeAccounting.GetOrCreateCostCenterAccount(AID, costCenterID, otherAccountID);
            
            if (otherAccount == null)
                throw new ServerUserMessage("Problem with expense/cost account designation, company cost center designation or employee accounts");

            TransactionOfBatch[] transactions = null;
            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            BankAccountInfo ba = null;
            if (PaymentMethodHelper.IsPaymentMethodBank(staffTransactionDoc.paymentMethod))
                ba = bde.GetBankAccount(staffTransactionDoc.assetAccountID);
            string note = staffTransactionDoc.paymentMethod == BizNetPaymentMethod.CPOFromBankAccount ? "CPO service charge" : "Bank transfer service charge";

            creditSide = new TransactionOfBatch(staffTransactionDoc.assetAccountID, -transactionSign * staffTransactionDoc.amount * multiplier);
            debitSide = new TransactionOfBatch(otherAccount.id, transactionSign * staffTransactionDoc.amount);

            if (staffTransactionDoc.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(staffTransactionDoc.serviceChargeAmount, 0))
            {
                int serviceChargeAccountID = ba == null ?
                    bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.bankServiceChargeByCash).id
                    : ba.bankServiceChargeCsAccountID;
                if (serviceChargeAccountID == -1)
                    throw new INTAPS.ClientServer.ServerUserMessage("Bank fee expense account not set");
                CostCenterAccount serviceChargeAccount = bdeAccounting.GetCostCenterAccount(serviceChargeAccountID);
                if (serviceChargeAccount == null)
                    throw new INTAPS.ClientServer.ServerUserMessage("Bank fee expense account not set");

                TransactionOfBatch debitSide2, creditSide2;
                debitSide2 = new TransactionOfBatch(serviceChargeAccount.id, staffTransactionDoc.serviceChargeAmount, note);
                creditSide2 = new TransactionOfBatch(staffTransactionDoc.assetAccountID, -staffTransactionDoc.serviceChargeAmount, note);
                transactions = new TransactionOfBatch[] { debitSide, creditSide, debitSide2, creditSide2 };
            }
            else
            {
                transactions = new TransactionOfBatch[] { debitSide, creditSide };
            }
            return transactions;
        }
    }
    public class DHStaffPaymentTransactionBase : DeleteReverseDocumentHandler
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        PayrollBDE _payRoll = null;
        protected PayrollBDE bdePayroll
        {
            get
            {
                if (_payRoll == null)
                    _payRoll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
                return _payRoll;
            }
        }
        public DHStaffPaymentTransactionBase(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, AccountDocument doc, UserSessionData userSession)
        {
            return bde.CashierPermissionGranted(userSession);
        }

        public override string GetHTML(AccountDocument doc)
        {
            StaffReceivableAccountDocument bdoc = (StaffReceivableAccountDocument)doc;
            return "<b>Amount:<b/>" + TSConstants.FormatBirr(bdoc.amount);
        }
        public int Post(int AID, AccountDocument doc, int debitAccount)
        {
            StaffReceivableAccountDocument staffReceivableAccountDoc = (StaffReceivableAccountDocument)doc;
            if (staffReceivableAccountDoc.voucher != null)
                staffReceivableAccountDoc.PaperRef = staffReceivableAccountDoc.voucher.reference;

            Employee eme = bdePayroll.GetEmployee(staffReceivableAccountDoc.employeeID);
            TransactionOfBatch[] transactions = null;

            Employee employee = bdePayroll.GetEmployee(staffReceivableAccountDoc.employeeID);
            CostCenterAccount assetAccount = bdeAccounting.GetCostCenterAccount(staffReceivableAccountDoc.assetAccountID);
            int parentReceivableAccount = debitAccount;
            debitAccount = GetEmployeeAccountID(employee, debitAccount);
            if (debitAccount <0 )
            {
                throw new INTAPS.ClientServer.ServerUserMessage(String.Format("Account not set up for employee {0} under account ID {1}", employee.EmployeeNameID, parentReceivableAccount));
            }
            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                int costCenterID = employee.costCenterID;
                if (staffReceivableAccountDoc is StaffPerDiemDocument)
                {
                    StaffPerDiemDocument perdiem = (StaffPerDiemDocument)staffReceivableAccountDoc;
                    CostCenterAccount csAccount = bdeAccounting.GetCostCenterAccount(perdiem.costCenterID, debitAccount);
                    if (csAccount != null)
                        costCenterID = perdiem.costCenterID;
                    else
                    {
                        CreateEmpleeCostCenterAccount(AID, debitAccount, perdiem.costCenterID);
                        costCenterID = perdiem.costCenterID;
                    }
                }
                transactions = StaffTransactionUtility.GenerateStaffPaymentReceiptTransaction(AID, this.bde, bdeAccounting, staffReceivableAccountDoc, debitAccount, costCenterID, 1);

                DocumentTypedReference[] typedRefs = staffReceivableAccountDoc.voucher == null ? new DocumentTypedReference[0] :
                    new DocumentTypedReference[] { staffReceivableAccountDoc.voucher };
                if (doc.AccountDocumentID != -1)
                    bdeAccounting.DeleteAccountDocument(AID, doc.AccountDocumentID, true);
                ret = bdeAccounting.RecordTransaction(AID, staffReceivableAccountDoc,
                transactions
                , true, typedRefs);
                if (!_bde.AllowNegativeTransactionPost())
                    bdeAccounting.validateNegativeBalance(staffReceivableAccountDoc.assetAccountID, 0, false, DateTime.Now, "The transaction is blocked because it will cause negative asset balance.");
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }
        
        public void ValidateAccounts(StaffReceivableAccountDocument staffReceivableAccountDoc, bool validateCash)
        {
            if (!validateCash) //validate bank
            {
                ValidateBankAccount(staffReceivableAccountDoc);
            }
            else
            {
                ValidateCashAccount(staffReceivableAccountDoc);
            }
        }

        private void ValidateBankAccount(StaffReceivableAccountDocument staffReceivableAccountDoc)
        {
            if (staffReceivableAccountDoc.assetAccountID != -1)
            {
                double balance = GetBankAccountBalance(staffReceivableAccountDoc);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make bank account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No bank accounts found.\nPlease configure bank account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetBankAccountBalance(StaffReceivableAccountDocument staffReceivableAccountDoc)
        {
            return bdeAccounting.GetNetBalanceAsOf(staffReceivableAccountDoc.assetAccountID, 0, staffReceivableAccountDoc.DocumentDate);
        }

        private void ValidateCashAccount(StaffReceivableAccountDocument staffReceivableAccountDoc)
        {
            if (staffReceivableAccountDoc.assetAccountID != -1)
            {
                double balance = GetCashAccountBalance(staffReceivableAccountDoc);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make cash account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No cash accounts found.\nPlease configure cash account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetCashAccountBalance(StaffReceivableAccountDocument staffReceivableAccountDoc)
        {
            return bdeAccounting.GetNetBalanceAsOf(staffReceivableAccountDoc.assetAccountID, 0, staffReceivableAccountDoc.DocumentDate);
        }
        public  int GetEmployeeAccountID(Employee employee, int parentAccount)
        {
            foreach (int ac in employee.accounts)
            {
                Account acnt = bdeAccounting.GetAccount<Account>(ac);
                if (acnt.PID == parentAccount)
                {
                    return ac;
                }
            }
            return -1;
        }
        protected void CreateEmpleeCostCenterAccount(int AID, int employeeStaffPerdiemAccount, int costCenterID)
        {
            CostCenterAccount csAccount = bdeAccounting.GetCostCenterAccount(costCenterID, employeeStaffPerdiemAccount);
            if (csAccount == null)
            {
                bdeAccounting.CreateCostCenterAccount(AID, costCenterID, employeeStaffPerdiemAccount);
            }
        }
        #endregion

        
        public string getInstrumentNo(AccountDocument _doc, out string typeName)
        {
            StaffTransactionDocument doc = _doc as StaffTransactionDocument;
            if (!PaymentMethodHelper.IsPaymentMethodWithReference(doc.paymentMethod))
            {
                typeName = null;
                return null;
            }
            typeName = PaymentMethodHelper.GetPaymentTypeReferenceName(doc.paymentMethod);
            return PaymentMethodHelper.selectReference(doc.paymentMethod,doc.checkNumber,doc.cpoNumber,doc.slipReferenceNo);
        }
    }

    public class DHExpenseAdvance : DHStaffPaymentTransactionBase, IDocumentServerHandler,ICashTransaction
    {
        PayrollBDE _payRoll = null;
        PayrollBDE PayRoll
        {
            get
            {
                if (_payRoll == null)
                    _payRoll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
                return _payRoll;
            }
        }

        

        public DHExpenseAdvance(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {

        }
        #region IDocumentServerHandler Members

        public int Post(int AID, AccountDocument _doc)
        {
            try
            {
                bde.WriterHelper.BeginTransaction();
                //delete existing settlment schedule documents
                if (_doc.AccountDocumentID != -1)
                {
                    ExpenseAdvanceDocument oldDoc = bdeAccounting.GetAccountDocument(_doc.AccountDocumentID, true) as ExpenseAdvanceDocument;
                    if (oldDoc.schedules != null)
                    {
                        foreach (AccountDocument delDoc in oldDoc.schedules)
                        {
                            //check document exists
                            if (bdeAccounting.DocumentExists(delDoc.AccountDocumentID))
                                bdeAccounting.DeleteGenericDocument(AID, delDoc.AccountDocumentID);
                        }
                    }
                }
                ExpenseAdvanceDocument doc = (ExpenseAdvanceDocument)_doc;
                //post new settlement schedule documents
                if (doc.schedules != null)
                {
                    foreach (AccountDocument inv in doc.schedules)
                    {
                        inv.AccountDocumentID = -1;
                        if (!(inv.scheduled && !inv.materialized))
                            throw new INTAPS.ClientServer.ServerUserMessage("One of the settelment documents are not scheduled");
                        inv.AccountDocumentID = bdeAccounting.PostGenericDocument(AID, inv);
                    }
                }
                int ret = base.Post(AID, _doc, PayRoll.SysPars.staffExpenseAdvanceAccountID);

                bde.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bde.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        #endregion

        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            ExpenseAdvanceDocument paymentDoc = (ExpenseAdvanceDocument)doc;
            if (PaymentMethodHelper.IsPaymentMethodCredit(paymentDoc.paymentMethod))
                return null;
            AccountTransaction tran=CashTransactionUtility.findTransactionByAccountIDExtended(bde.Accounting,docTrans,bdePayroll.SysPars.staffExpenseAdvanceAccountID);
            if(tran==null)
                return null;
            if (!CashTransactionUtility.isCostCenterAccountInCostCenter(bde.Accounting, tran.AccountID, option.costCenters))
                return null;
            List<CashFlowItem> items = new List<CashFlowItem>();
            items.Add(CashFlowItem.createByText(tran.AccountID, -1, "", tran.Amount));
            CashTransactionUtility.addCashTransactionMainItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.amount, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer, paymentDoc.checkNumber, paymentDoc.cpoNumber, paymentDoc.slipReferenceNo);
            CashTransactionUtility.addCashTransactionCostItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer);
            return items;
        }
    }
    public class DHStaffLoanPayment : DHStaffPaymentTransactionBase, IDocumentServerHandler
    {
        public DHStaffLoanPayment(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {

        }
        #region IDocumentServerHandler Members

        public int Post(int AID, AccountDocument _doc)
        {
            return base.Post(AID, _doc, bdePayroll.SysPars.LoanFromStaffAccountID);
        }

        #endregion
       
    }
    public class DHShortTermLoan : DHStaffPaymentTransactionBase, IDocumentServerHandler,ICashTransaction
    {
        PayrollBDE _payRoll = null;
        PayrollBDE bdePayroll
        {
            get
            {
                if (_payRoll == null)
                    _payRoll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
                return _payRoll;
            }
        }
        public DHShortTermLoan(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {

        }

        public static void SendToPayroll(int AID,PayrollBDE payroll, iERPTransactionBDE bde, Employee emp,StaffLoandDocumentBase oldDocument, StaffLoandDocumentBase document, int formulaID)
        {

            lock (payroll.WriterHelper)
            {
                payroll.WriterHelper.setReadDB(payroll.DBName);
                try
                {
                    payroll.WriterHelper.BeginTransaction();

                    if (oldDocument != null)
                    {
                        foreach (PayrollComponentData data in oldDocument.data)
                        {
                            object ad;
                            if (payroll.GetPCData(data.ID, out ad) != null)
                                payroll.DeleteData(AID, data.ID);

                            //try
                            //{
                            //payroll.DeleteData(AID, data.ID);
                            //}
                            //catch
                            //{
                            //}
                            //finally
                            //{
                            //object ad;
                            //if (payroll.GetPCData(data.ID, out ad) != null)
                            //   throw new INTAPS.ClientServer.ServerUserMessage("Failed to delete existing loan data. Contact system support");
                            //}
                        }
                    }
                    double amount = document.amount;
                    if (!AccountBase.AmountGreater(amount, 0))
                        throw new INTAPS.ClientServer.ServerUserMessage("Invalid loan amount");
                    List<PayrollComponentData> createData = new List<PayrollComponentData>();
                    
                    PayrollPeriod p = payroll.GetPayPeriod(document.periodID);
                    if (p.toDate <= document.DocumentDate)
                    {
                        throw new ServerUserMessage("The loan return periods can't be before the loan date");
                    }
                    
                    double newDirectReturn = 0;
                    foreach (int rdocID in document.directReturnDocumentID)
                        newDirectReturn += ((StaffLoanReturnDocument)bde.Accounting.GetAccountDocument(rdocID, true)).amount;

                    StaffLoandDocumentBase.LoanReturnItem[] schedule = document.getReturnSchedule(delegate(int pid)
                    {
                        PayrollPeriod period = payroll.getNextPeriod(pid);
                        if (period == null)
                            throw new ServerUserMessage("Payroll period overflow. Please generate more periods");
                        return period.id;
                    }
                    ,newDirectReturn);
                    StaffLoandDocumentBase.LoanReturnItem[] oldSchedule;
                    if (oldDocument == null)
                        oldSchedule = null;
                    else
                    {
                        double directReturn = 0;
                        foreach (int rdocID in oldDocument.directReturnDocumentID)
                            directReturn += ((StaffLoanReturnDocument)bde.Accounting.GetAccountDocument(rdocID, true)).amount;
                        oldSchedule = oldDocument.getReturnSchedule(
                            delegate(int pid)
                            {
                                PayrollPeriod period = payroll.getNextPeriod(pid);
                                if (period == null)
                                    throw new ServerUserMessage("Payroll period overflow. Please generate more periods");
                                return period.id;
                            }
                        ,directReturn);
                    }
                    amount -= newDirectReturn;
                    //Build loan return schedule
                    foreach(StaffLoandDocumentBase.LoanReturnItem s in schedule)
                    {
                        PayrollComponentData componentData = new PayrollComponentData();
                        componentData.FormulaID = formulaID;
                        componentData.PCDID = payroll.SysPars.staffLoanPayrollComponentID;
                        componentData.DataDate = document.DocumentDate;
                        componentData.at = new AppliesToEmployees(document.employeeID);
                        componentData.periodRange = new PayPeriodRange(s.periodID, s.periodID);
                        StaffLoanPaymentData paymentData = new StaffLoanPaymentData();
                        paymentData.Amount = s.returnAmount;
                        paymentData.Exception = false;
                        componentData.ID = payroll.CreatePCData(AID, componentData, paymentData);
                        createData.Add(componentData);
                        amount -= s.returnAmount;

                        PayrollDocument pdoc = payroll.GetPayroll(document.employeeID, s.periodID);
                        if (pdoc != null)
                        {
                            bool changed=true;
                            if (oldSchedule != null)
                            {
                                foreach (StaffLoandDocumentBase.LoanReturnItem os in oldSchedule)
                                {
                                    if (os.periodID == s.periodID)
                                    {
                                        if (AccountBase.AmountEqual(os.returnAmount, s.returnAmount))
                                            changed = false;
                                        break;
                                    }
                                }
                            }
                            if(changed)
                                throw new ServerUserMessage("The loan can't be registered because payroll is already generated for period " + p.name + " for employee " + emp.EmployeeNameID);
                        }
                    }

                    if (!AccountBase.AmountEqual(amount, 0))
                        throw new ServerUserMessage("Loan schedule total doen't match loan amount");
                    document.data = createData.ToArray();
                    payroll.WriterHelper.CommitTransaction();
                }
                catch
                {
                    payroll.WriterHelper.RollBackTransaction();
                    throw;
                }
                finally
                {
                    payroll.WriterHelper.restoreReadDB();
                }
            }
        }
        public static void deletePayrollData(int AID, PayrollBDE payroll, StaffLoandDocumentBase loan)
        {
            foreach (PayrollComponentData data in loan.data)
            {
                payroll.DeleteData(AID, data.ID);
            }
        }
        #region IDocumentServerHandler Members
        public override void DeleteDocument(int AID, int docID)
        {
            StaffLoandDocumentBase doc = bdeAccounting.GetAccountDocument(docID, true) as StaffLoandDocumentBase;
            base.DeleteDocument(AID, docID);
            deletePayrollData(AID,bdePayroll, doc);

        }
        public int Post(int AID, AccountDocument _doc)
        {
            iERPTransactionBDE bde = (iERPTransactionBDE)ApplicationServer.GetBDE("iERP");

            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                StaffLoandDocumentBase old=null;
                StaffLoandDocumentBase doc = (StaffLoandDocumentBase)_doc;
                if (_doc.AccountDocumentID != -1)
                {
                    old = bdeAccounting.GetAccountDocument(_doc.AccountDocumentID, true) as StaffLoandDocumentBase;
                    if (doc.directReturnDocumentID == null)
                        doc.directReturnDocumentID = old.directReturnDocumentID;
                }
                else
                {
                    if (doc.directReturnDocumentID == null)
                        doc.directReturnDocumentID = new int[0];
                }
                SendToPayroll(AID, bdePayroll, bde, bdePayroll.GetEmployee(((StaffLoandDocumentBase)_doc).employeeID), old, (StaffLoandDocumentBase)_doc, bdePayroll.SysPars.staffShortTermLoanPayrollFormulaID);
                int ret = base.Post(AID, _doc, bdePayroll.SysPars.staffSalaryAdvanceAccountID);
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        #endregion
        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            StaffReceivableAccountDocument paymentDoc = (StaffReceivableAccountDocument)doc;
            if (PaymentMethodHelper.IsPaymentMethodCredit(paymentDoc.paymentMethod))
                return null;
            if (!CashTransactionUtility.isTransactionInCostCenter(bde.Accounting, doc.AccountDocumentID, option.costCenters, true))
                return null;
            List<CashFlowItem> items = new List<CashFlowItem>();
            string html = System.Web.HttpUtility.HtmlEncode("Salary Avance Paid to " + bdePayroll.GetEmployee(paymentDoc.employeeID).EmployeeNameID);
            items.Add(new CashFlowItem(html, paymentDoc.amount));
            
            CashTransactionUtility.addCashTransactionMainItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.amount, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer, paymentDoc.checkNumber, paymentDoc.cpoNumber, paymentDoc.slipReferenceNo);
            CashTransactionUtility.addCashTransactionCostItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer);
            return items;
        }
    }
    public class DHLongTermLoan : DHStaffPaymentTransactionBase, IDocumentServerHandler,ICashTransaction
    {
        PayrollBDE _Payroll=null;
        PayrollBDE bdePayroll
        {
            get
            {
                if (_Payroll == null)
                    _Payroll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
                return _Payroll;
            }
        }

        public DHLongTermLoan(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members
        public override void DeleteDocument(int AID, int docID)
        {
            LongTermLoanDocument doc = bdeAccounting.GetAccountDocument(docID, true) as LongTermLoanDocument;
            base.DeleteDocument(AID, docID);
            DHShortTermLoan.deletePayrollData(AID, bdePayroll, doc);
        }
        public int Post(int AID, AccountDocument _doc)
        {

            bdeAccounting.WriterHelper.BeginTransaction();
            try
            {
                
                StaffLoandDocumentBase old = null;
                StaffLoandDocumentBase doc = (StaffLoandDocumentBase)_doc;
                if (_doc.AccountDocumentID != -1)
                {
                    old = bdeAccounting.GetAccountDocument(_doc.AccountDocumentID, true) as StaffLoandDocumentBase;
                    if (doc.directReturnDocumentID == null)
                        doc.directReturnDocumentID = old.directReturnDocumentID;
                }
                else
                {
                    if (doc.directReturnDocumentID == null)
                        doc.directReturnDocumentID = new int[0];
                }

                DHShortTermLoan.SendToPayroll(AID, bdePayroll, bde, bdePayroll.GetEmployee(doc.employeeID), old, doc, bdePayroll.SysPars.staffLongTermLoanPayrollFormulaID);
                int ret = base.Post(AID, _doc, bdePayroll.SysPars.staffLongTermLoanAccountID);
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }
        #endregion
        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            StaffReceivableAccountDocument paymentDoc = (StaffReceivableAccountDocument)doc;
            if (PaymentMethodHelper.IsPaymentMethodCredit(paymentDoc.paymentMethod))
                return null;
            if (!CashTransactionUtility.isTransactionInCostCenter(bde.Accounting, paymentDoc.AccountDocumentID, option.costCenters, true))
                return null;
            List<CashFlowItem> items = new List<CashFlowItem>();
            string html = System.Web.HttpUtility.HtmlEncode("Longterm Loan Paid to " + bdePayroll.GetEmployee(paymentDoc.employeeID).EmployeeNameID);
            items.Add(new CashFlowItem(html, paymentDoc.amount));
            
            CashTransactionUtility.addCashTransactionMainItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.amount, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer, paymentDoc.checkNumber, paymentDoc.cpoNumber, paymentDoc.slipReferenceNo);
            CashTransactionUtility.addCashTransactionCostItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer);
            return items;
        }
    }
    public class DHShareHoldersLoan: DHStaffPaymentTransactionBase, IDocumentServerHandler,ICashTransaction
    {
        PayrollBDE _payRoll = null;
        protected PayrollBDE bdePayroll
        {
            get
            {
                if (_payRoll == null)
                    _payRoll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
                return _payRoll;
            }
        }
        public DHShareHoldersLoan(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {

        }
        #region IDocumentServerHandler Members
        public int Post(int AID, AccountDocument _doc)
        {
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    int ret = base.Post(AID, _doc, bdePayroll.SysPars.ShareHoldersLoanAccountID);
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        #endregion

        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            StaffReceivableAccountDocument paymentDoc = (StaffReceivableAccountDocument)doc;
            if (PaymentMethodHelper.IsPaymentMethodCredit(paymentDoc.paymentMethod))
                return null;
            if (!CashTransactionUtility.isTransactionInCostCenter(bde.Accounting, doc.AccountDocumentID, option.costCenters, true))
                return null; 
            List<CashFlowItem> items = new List<CashFlowItem>();
            string html = System.Web.HttpUtility.HtmlEncode("Shareholder's Loan Paid to " + bdePayroll.GetEmployee(paymentDoc.employeeID).EmployeeNameID);
            items.Add(new CashFlowItem(html, paymentDoc.amount));

            CashTransactionUtility.addCashTransactionMainItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.amount, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer, paymentDoc.checkNumber, paymentDoc.cpoNumber, paymentDoc.slipReferenceNo);
            CashTransactionUtility.addCashTransactionCostItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer);
            return items;
        }
    }
    public class DHUnclaimedSalary : DHStaffPaymentTransactionBase, IDocumentServerHandler,ICashTransaction
    {
        public DHUnclaimedSalary(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {

        }

        #region IDocumentServerHandler Members

        public int Post(int AID, AccountDocument _doc)
        {
            StaffReceivableAccountDocument doc=(StaffReceivableAccountDocument)_doc;
            return base.Post(AID, _doc, bdePayroll.SysPars.UnclaimedSalaryAccount);
        }
        void UnPayPayroll()
        {
        }
        public override void DeleteDocument(int AID, int docID)
        {
            base.DeleteDocument(AID, docID);
        }
        public override void ReverseDocument(int AID, int docID)
        {
            base.ReverseDocument(AID, docID);
        }
        #endregion
        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            StaffReceivableAccountDocument paymentDoc = (StaffReceivableAccountDocument)doc;
            if (PaymentMethodHelper.IsPaymentMethodCredit(paymentDoc.paymentMethod))
                return null;
            
            if (!CashTransactionUtility.isTransactionInCostCenter(bde.Accounting, doc.AccountDocumentID, option.costCenters, true))
                return null;
            Employee emp = bdePayroll.GetEmployee(paymentDoc.employeeID);
            List<CashFlowItem> items = new List<CashFlowItem>();
            string html = System.Web.HttpUtility.HtmlEncode("Salary Paid to " + emp.EmployeeNameID);
            items.Add(new CashFlowItem(html, paymentDoc.amount));
            
            CashTransactionUtility.addCashTransactionMainItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.amount, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer, paymentDoc.checkNumber, paymentDoc.cpoNumber, paymentDoc.slipReferenceNo);
            CashTransactionUtility.addCashTransactionCostItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer);
            return items;
        }
    }

    public class DHStaffPerDiem : DHStaffPaymentTransactionBase, IDocumentServerHandler,ICashTransaction
    {
 
        public DHStaffPerDiem(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members

        public int Post(int AID, AccountDocument _doc)
        {
            iERPTransactionBDE bde = (iERPTransactionBDE)ApplicationServer.GetBDE("iERP");
            StaffReceivableAccountDocument doc = (StaffReceivableAccountDocument)_doc;
            return base.Post(AID, _doc, bdePayroll.GetPerdiemExpenseAccountID(bdePayroll.GetEmployee(doc.employeeID )));
        }

        #endregion
        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            StaffReceivableAccountDocument paymentDoc = (StaffReceivableAccountDocument)doc;
            if (PaymentMethodHelper.IsPaymentMethodCredit(paymentDoc.paymentMethod))
                return null;
            if (!CashTransactionUtility.isTransactionInCostCenter(bde.Accounting, doc.AccountDocumentID, option.costCenters, true))
                return null;
            List<CashFlowItem> items = new List<CashFlowItem>();
            string html = System.Web.HttpUtility.HtmlEncode("Staff Per Diem Paid to " + bdePayroll.GetEmployee(paymentDoc.employeeID).EmployeeNameID);
            items.Add(new CashFlowItem(html, paymentDoc.amount));
            CashTransactionUtility.addCashTransactionMainItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.amount, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer, paymentDoc.checkNumber, paymentDoc.cpoNumber, paymentDoc.slipReferenceNo);
            CashTransactionUtility.addCashTransactionCostItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer);
            return items;
        }
    }

}
