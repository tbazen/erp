using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;

namespace BIZNET.iERP.Server
{
    public class DHStaffReceiptTransactionBase: DeleteReverseDocumentHandler
    {
        iERPTransactionBDE _bde = null;
        iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        PayrollBDE _payRoll = null;
        protected PayrollBDE bdePayroll
        {
            get
            {
                if (_payRoll == null)
                    _payRoll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
                return _payRoll;
            }
        }
        public DHStaffReceiptTransactionBase(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, AccountDocument doc, UserSessionData userSession)
        {
            return bde.CashierPermissionGranted(userSession);
        }

        public override string GetHTML(AccountDocument doc)
        {
            StaffReturnDocument bdoc = (StaffReturnDocument)doc;
            return "<b>Amount:<b/>" + TSConstants.FormatBirr(bdoc.amount);
        }

        public int Post(int AID, AccountDocument doc, int creditAccount)
        {
            StaffReturnDocument staffReturnDocument = (StaffReturnDocument)doc;
            TransactionOfBatch[] transactions = null;
            PayrollBDE payroll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
            Employee employee = payroll.GetEmployee(staffReturnDocument.employeeID);
            int creditAccountParent = creditAccount;
            creditAccount = StaffTransactionUtility.GetEmployeeAccountID(bdeAccounting, employee, creditAccount);
            CostCenterAccount assetAccount=bdeAccounting.GetCostCenterAccount(staffReturnDocument.assetAccountID);
            if (creditAccount == -1)
            {
                throw new INTAPS.ClientServer.ServerUserMessage(String.Format("Account not set up for employee {0} under account ID {1}", employee.EmployeeNameID, creditAccountParent));
            }
            transactions = StaffTransactionUtility.GenerateStaffPaymentReceiptTransaction(AID,bde, bdeAccounting, staffReturnDocument, creditAccount,employee.costCenterID, -1);

            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                DocumentTypedReference[] typedRefs = staffReturnDocument.voucher == null ? new DocumentTypedReference[0] :
                  new DocumentTypedReference[] { staffReturnDocument.voucher };
                int prevLoanDocumentID = -1;
                if (doc.AccountDocumentID != -1)
                {
                    prevLoanDocumentID = (bdeAccounting.GetAccountDocument(doc.AccountDocumentID, true) as StaffReturnDocument).loanDocumentID;
                    bdeAccounting.DeleteAccountDocument(AID, doc.AccountDocumentID, true);
                }
                
                ret = bdeAccounting.RecordTransaction(AID, staffReturnDocument,
                    transactions
                    , true, typedRefs);
                if (prevLoanDocumentID != staffReturnDocument.loanDocumentID)
                {
                    if (prevLoanDocumentID != -1)
                    {
                        StaffLoandDocumentBase loan = bdeAccounting.GetAccountDocument(prevLoanDocumentID, true) as StaffLoandDocumentBase;
                        if (loan != null)
                        {
                            loan.directReturnDocumentID = INTAPS.ArrayExtension.removeItem(loan.directReturnDocumentID, ret);
                            bdeAccounting.PostGenericDocument(AID, loan);
                        }
                    }
                    if(staffReturnDocument.loanDocumentID!=-1)
                    {
                        StaffLoandDocumentBase loan = bdeAccounting.GetAccountDocument(staffReturnDocument.loanDocumentID, true) as StaffLoandDocumentBase;
                        loan.directReturnDocumentID = INTAPS.ArrayExtension.appendToArray(loan.directReturnDocumentID, ret);
                        bdeAccounting.PostGenericDocument(AID, loan);

                    }
                }
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }
        public override void DeleteDocument(int AID, int docID)
        {
            int prevLoanDocumentID = (bdeAccounting.GetAccountDocument(docID, true) as StaffReturnDocument).loanDocumentID;
            if (prevLoanDocumentID != -1)
            {
                StaffLoandDocumentBase loan = bdeAccounting.GetAccountDocument(prevLoanDocumentID, true) as StaffLoandDocumentBase;
                loan.directReturnDocumentID = INTAPS.ArrayExtension.removeItem(loan.directReturnDocumentID, docID);
                bdeAccounting.PostGenericDocument(AID, loan);
            }
            base.DeleteDocument(AID, docID);
        }
        #endregion
    }

    public class DHExpenseAdvanceReturn : DHStaffReceiptTransactionBase, IDocumentServerHandler
    {
        public DHExpenseAdvanceReturn(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members

        public int Post(int AID, AccountDocument _doc)
        {
            return base.Post(AID, _doc, bdePayroll.SysPars.staffExpenseAdvanceAccountID);
        }

        #endregion
    }

    public class DHShortTermLoanReturn : DHStaffReceiptTransactionBase, IDocumentServerHandler
    {
        public DHShortTermLoanReturn(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }

        
        #region IDocumentServerHandler Members

        public int Post(int AID, AccountDocument _doc)
        {
            return base.Post(AID, _doc, bdePayroll.SysPars.staffSalaryAdvanceAccountID);
        }

        #endregion
    }
    public class DHLongTermLoanReturn : DHStaffReceiptTransactionBase, IDocumentServerHandler
    {
        public DHLongTermLoanReturn(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members
        public int Post(int AID, AccountDocument _doc)
        {
            return base.Post(AID, _doc, bdePayroll.SysPars.staffLongTermLoanAccountID);
        }
        #endregion
    }
    public class DHLoanFromStaff : DHStaffReceiptTransactionBase, IDocumentServerHandler
    {
        public DHLoanFromStaff(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {

        }
        #region IDocumentServerHandler Members
        public int Post(int AID, AccountDocument _doc)
        {
            return base.Post(AID, _doc, bdePayroll.SysPars.LoanFromStaffAccountID);
        }
        #endregion
    }

    public class DHShareHoldersLoanReturn : DHStaffReceiptTransactionBase, IDocumentServerHandler
    {
        public DHShareHoldersLoanReturn(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members
        public int Post(int AID, AccountDocument _doc)
        {
            return base.Post(AID, _doc, bdePayroll.SysPars.ShareHoldersLoanAccountID);
        }
        #endregion
    }

}
