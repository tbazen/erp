﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.Accounting;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public partial class DHStoreFinishedGoods : DHItemTransactionBase, IDocumentServerHandler
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        public DHStoreFinishedGoods(AccountingBDE act)
            : base(act)
        {
        }
        public bool CheckPostPermission(int docID, AccountDocument _doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return true;
        }

        public int Post(int AID, AccountDocument _doc)
        {
            StoreFinishedGoodsDocument doc = (StoreFinishedGoodsDocument)_doc;
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    if (doc.voucher != null)
                        doc.PaperRef = doc.voucher.reference;
                    DocumentTypedReference[] typedRefs = doc.voucher == null ? new DocumentTypedReference[0] :
                new DocumentTypedReference[] { doc.voucher };

                    if (_doc.AccountDocumentID != -1)
                        bdeAccounting.DeleteAccountDocument(AID, _doc.AccountDocumentID,true);
                    List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
                    List<TransactionOfBatch> summeryTransactions = new List<TransactionOfBatch>();
                    List<TransactionItems> titems = new List<TransactionItems>();
                    for (int i = 0; i < doc.items.Length; i++)
                    {
                        titems.Add(bde.GetTransactionItems(doc.items[i].code));
                    }
                    base.fixUnit(doc.items, titems);
                    for (int i = 0; i < doc.items.Length; i++)
                    {
                        //validate item code
                        TransactionItems titem = titems[i];
                        if (titem == null)
                            throw new ServerUserMessage("Invalid item code");
                        if ((titem.IsInventoryItem && titem.inventoryType != InventoryType.FinishedGoods) && !titem.IsSalesItem)
                            throw new ServerUserMessage("Only finished goods are allowed");
                        //validate existance cost center account
                        int assetAccountID=titem.IsInventoryItem? 
                            (titem.finishedGoodAccountID==-1?titem.inventoryAccountID:titem.finishedGoodAccountID) : 
                            titem.finishedWorkAccountID;
                        int assetSummaryAccountID = titem.IsInventoryItem ?
                            (titem.finishedGoodAccountID == -1 ? titem.generalInventorySummaryAccountID : titem.finishedGoodsSummaryAccountID) :
                            titem.finishedWorkAccountID;
                        CostCenterAccount csaAsset = bdeAccounting.GetOrCreateCostCenterAccount(AID, doc.costCenterID,assetAccountID);
                        if (csaAsset == null)
                            throw new ServerUserMessage("Invalid asset account for the the item:" + titem.Code);

                        CostCenterAccount csaIncome = bdeAccounting.GetOrCreateCostCenterAccount(AID, doc.items[i].costCenterID, titem.salesAccountID);
                        if (csaIncome == null)
                            throw new ServerUserMessage("Invalid sales account for the item:" + titem.Code);


                        tran.Add(new TransactionOfBatch(csaAsset.id, TransactionItem.MATERIAL_QUANTITY, doc.items[i].quantity, ""));
                        tran.Add(new TransactionOfBatch(csaIncome.id, TransactionItem.MATERIAL_QUANTITY, -doc.items[i].quantity, ""));

                        addSummerizedItemTransaction(AID, tran, summeryTransactions, titem, "Finished Good/Work Account"
                            , doc.costCenterID,assetAccountID,assetSummaryAccountID, 0
                            , doc.items[i].price, "Finished Good/Work");

                        addSummerizedItemTransaction(AID, tran, summeryTransactions, titem, "Sales Account"
                            , doc.items[i].costCenterID, titem.salesAccountID, titem.salesSummaryAccountID, 0
                            , -doc.items[i].price, "Finished Good/Work");

                    }
                    addNetSummaryTransaction(AID, tran, "Net Summerized Amount for Finsihed Godo/Work" + doc.PaperRef, summeryTransactions);
                    doc.AccountDocumentID = bdeAccounting.RecordTransaction(AID, doc, tran.ToArray(), typedRefs);
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return doc.AccountDocumentID;
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

        public override string GetHTML(AccountDocument _doc)
        {
            StoreFinishedGoodsDocument doc = (StoreFinishedGoodsDocument)_doc;
            double ammount = 0;
            foreach (TransactionDocumentItem item in doc.items)
                ammount += item.price;
            return System.Web.HttpUtility.HtmlEncode(ammount.ToString("#,#0.00"));
        }
    }
}
