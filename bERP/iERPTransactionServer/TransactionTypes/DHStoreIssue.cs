﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.Accounting;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public partial class DHStoreIssue : DHItemTransactionBase, IDocumentServerHandler
    {
        public DHStoreIssue(AccountingBDE act)
            : base(act)
        {
        }
        public bool CheckPostPermission(int docID, AccountDocument _doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return true;
        }

        public int Post(int AID, AccountDocument _doc)
        {
            StoreIssueDocument doc = (StoreIssueDocument)_doc;
            lock (bdeAccounting.WriterHelper)
            {
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    if (doc.voucher != null)
                        doc.PaperRef = doc.voucher.reference;
                    DocumentTypedReference[] typedRefs = doc.voucher == null ? new DocumentTypedReference[0] :
                                new DocumentTypedReference[] { doc.voucher };

                    if (_doc.AccountDocumentID != -1)
                        bdeAccounting.DeleteAccountDocument(AID, _doc.AccountDocumentID, true);
                    List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
                    TransactionDocumentItem[] docItems = TransactionDocumentItem.uniquefy(doc.items);

                    List<TransactionOfBatch> summeryTransactions = new List<TransactionOfBatch>();

                    foreach (TransactionDocumentItem ditem in docItems)
                    {
                        if (AccountBase.AmountEqual(ditem.quantity, 0))
                            continue;
                        if (AccountBase.AmountLess(ditem.quantity, 0))
                            throw new ServerUserMessage("Nevagative quantity not allowed");

                        //validate item code
                        TransactionItems titem = bde.GetTransactionItems(ditem.code);
                        if (titem == null)
                            throw new ServerUserMessage("Invalid item code");
                        if (!titem.IsInventoryItem && !titem.IsFixedAssetItem)
                            throw new ServerUserMessage("Only inventory items are allowed");
                        //validate existance cost center accounts
                        CostCenterAccount csaAsset = bdeAccounting.GetOrCreateCostCenterAccount(AID, doc.storeID, titem.materialAssetAccountID(true));
                        double q = 0;
                        double p = 0;
                        q = bdeAccounting.GetNetBalanceAsOf(csaAsset.id, TransactionItem.MATERIAL_QUANTITY, doc.DocumentDate);
                        p = bdeAccounting.GetNetBalanceAsOf(csaAsset.id, TransactionItem.DEFAULT_CURRENCY, doc.DocumentDate);
                        if (AccountBase.AmountEqual(q, 0))
                            ditem.unitPrice = 0;
                        else
                            ditem.unitPrice = p / q;

                        ditem.price = ditem.quantity * ditem.unitPrice;

                        CostCenterAccount csaExpense = bdeAccounting.GetOrCreateCostCenterAccount(AID, ditem.costCenterID, titem.materialAssetReturnAccountID);
                        if (csaExpense == null)
                            throw new ServerUserMessage("Invalid expense account for the item:" + titem.Code);

                        tran.Add(new TransactionOfBatch(csaAsset.id, TransactionItem.MATERIAL_QUANTITY, -ditem.quantity, ""));
                        tran.Add(new TransactionOfBatch(csaExpense.id, TransactionItem.MATERIAL_QUANTITY, ditem.quantity, ""));

                        addSummerizedItemTransaction(AID, tran, summeryTransactions, titem, "Material Asset Account"
                            , doc.storeID, titem.materialAssetAccountID(true), titem.materialAssetSummaryAccountID(true), 0
                            , -ditem.price, "");
                        if (ditem.directExpense)
                            addSummerizedItemTransaction(AID, tran, summeryTransactions, titem, "Expense Account"
                                    , ditem.costCenterID, titem.expenseAccountID, titem.expenseSummaryAccountID, 0
                                    , ditem.price, "");
                        else
                        {
                            if (titem.IsFixedAssetItem)
                                addSummerizedItemTransaction(AID, tran, summeryTransactions, titem, "Original Fixed Asset"
                                        , ditem.costCenterID, titem.originalFixedAssetAccountID, titem.originalFixedAssetSummaryAccountID, 0
                                        , ditem.price, "");
                            else
                            {
                                addSummerizedItemTransaction(AID, tran, summeryTransactions, titem, "Expense Account"
                                , ditem.costCenterID, titem.expenseAccountID, titem.expenseSummaryAccountID, 0
                                , ditem.price, "");
                            }
                        }
                    }
                    addNetSummaryTransaction(AID, tran, "Net Store Issue Item Transaction", summeryTransactions);
                    doc.AccountDocumentID = bdeAccounting.RecordTransaction(AID, doc, tran.ToArray(), typedRefs);
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return doc.AccountDocumentID;
                }
                catch
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

        public override string GetHTML(AccountDocument _doc)
        {
            StoreIssueDocument doc = (StoreIssueDocument)_doc;
            double amount = 0;
            foreach (TransactionDocumentItem item in doc.items)
                amount += item.price;
            return System.Web.HttpUtility.HtmlEncode("Total amount issued:"+AccountBase.FormatAmount(amount));
        }
    }
}
