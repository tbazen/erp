using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;

namespace BIZNET.iERP.Server
{
    

    public class DHSupplierAccount : DeleteReverseDocumentHandler, ICashTransaction
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }

        public DHSupplierAccount(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }

        TransactionOfBatch[] generateTransactions
            (int AID,
             SupplierAccountDocument supplierDocument
            , int otherAccountID
            , double transactionSign
            , out CostCenterAccount otherAccount)
        {

            otherAccount = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, otherAccountID);
            if (otherAccount == null)
                throw new ServerUserMessage("Problem with expense account designation, company cost center designation or supplier accounts");

            TransactionOfBatch[] transactions = null;
            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            BankAccountInfo ba = null;
            if (PaymentMethodHelper.IsPaymentMethodBank(supplierDocument.paymentMethod))
                ba = bde.GetBankAccount(supplierDocument.assetAccountID);
            string note = supplierDocument.paymentMethod == BizNetPaymentMethod.CPOFromBankAccount ? "CPO service charge" : "Bank transfer service charge";

            creditSide = new TransactionOfBatch(supplierDocument.assetAccountID, -transactionSign * supplierDocument.amount);
            debitSide = new TransactionOfBatch(otherAccount.id, transactionSign * supplierDocument.amount);

            if (supplierDocument.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(supplierDocument.serviceChargeAmount, 0))
            {
                int serviceChargeAccountID = ba == null ?
                    bdeAccounting.GetCostCenterAccount(bde.SysPars.mainCostCenterID, bde.SysPars.bankServiceChargeByCash).id
                    : ba.bankServiceChargeCsAccountID;
                if (serviceChargeAccountID == -1)
                    throw new INTAPS.ClientServer.ServerUserMessage("Bank fee expense account not set");
                CostCenterAccount serviceChargeAccount = bdeAccounting.GetCostCenterAccount(serviceChargeAccountID);
                if (serviceChargeAccount == null)
                    throw new INTAPS.ClientServer.ServerUserMessage("Bank fee expense account not set");

                TransactionOfBatch debitSide2, creditSide2;
                debitSide2 = new TransactionOfBatch(serviceChargeAccount.id, supplierDocument.serviceChargeAmount, note);
                creditSide2 = new TransactionOfBatch(supplierDocument.assetAccountID, -supplierDocument.serviceChargeAmount, note);
                transactions = new TransactionOfBatch[] { debitSide, creditSide, debitSide2, creditSide2 };
            }
            else
            {
                transactions = new TransactionOfBatch[] { debitSide, creditSide };
            }
            return transactions;
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, AccountDocument doc, UserSessionData userSession)
        {
            return bde.CashierPermissionGranted(userSession);
        }

        public override string GetHTML(AccountDocument doc)
        {
            SupplierAccountDocument bdoc = (SupplierAccountDocument)doc;
            return "<b>Amount:<b/>" + TSConstants.FormatBirr(bdoc.amount);
        }

        public int PostSupplierTransaction(int AID, AccountDocument doc, int accountID,double sign)
        {
            SupplierAccountDocument supplierAccountDocument = (SupplierAccountDocument)doc;
            TransactionOfBatch[] transactions = null;
            CostCenterAccount outherCSAccount;
            transactions = generateTransactions(AID, supplierAccountDocument, accountID, sign, out outherCSAccount);


            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                DocumentTypedReference[] typedRefs = supplierAccountDocument.voucher == null ? new DocumentTypedReference[0] :
    new DocumentTypedReference[] { supplierAccountDocument.voucher };

                if (doc.AccountDocumentID == -1)
                    ret = bdeAccounting.RecordTransaction(AID, supplierAccountDocument,
                    transactions
                    , true, typedRefs);
                else
                    ret = bdeAccounting.UpdateTransaction(AID, supplierAccountDocument, transactions, true, typedRefs);
                if (!doc.scheduled && !bde.AllowNegativeTransactionPost())
                {
                    string notEnoughCustomerBalanceMessage;
                    if (outherCSAccount.creditAccount)
                        notEnoughCustomerBalanceMessage = "Insufficient supplier payable to register this transaction";
                    else
                        notEnoughCustomerBalanceMessage = "Insufficient supplier recievable to register this transaction";
                    string notEnoughAssetMessage;
                    if (PaymentMethodHelper.IsPaymentMethodCash(supplierAccountDocument.paymentMethod))
                        notEnoughAssetMessage = "Insufficient cash to register this transcation";
                    else if (PaymentMethodHelper.IsPaymentMethodBank(supplierAccountDocument.paymentMethod))
                        notEnoughAssetMessage = "Insuffient balance in bank account to register this transaction";
                    else
                        throw new ServerUserMessage("Unsupported payment method");

                    bdeAccounting.validateNegativeBalance(supplierAccountDocument.assetAccountID, 0, false, supplierAccountDocument.DocumentDate
                        , notEnoughAssetMessage);
                    bdeAccounting.validateNegativeBalance(outherCSAccount.id, 0, outherCSAccount.creditAccount, supplierAccountDocument.DocumentDate
                        , notEnoughCustomerBalanceMessage);

                }
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        
        #endregion

        public virtual string getCashTransactionItemFormat(out bool payment)
        {
            payment = true;
            return "";
        }
        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            return null;
            //SupplierAccountDocument paymentDoc = (SupplierAccountDocument)doc;
            //if (PaymentMethodHelper.IsPaymentMethodCredit(paymentDoc.paymentMethod))
            //    return null;
            //if (!CashTransactionUtility.isTransactionInCostCenter(bde.Accounting, doc.AccountDocumentID, option.costCenters, true))
            //    return null;
            //List<CashFlowItem> items = new List<CashFlowItem>();
            //TradeRelation supplier = bde.GetRelation(paymentDoc.supplierCode);
            //bool payment;
            //string fromat=getCashTransactionItemFormat(out payment);
            //string html = System.Web.HttpUtility.HtmlEncode(string.Format(fromat, supplier.Name));
            //items.Add(new CashFlowItem(html, payment ? paymentDoc.amount : -paymentDoc.amount));

            //CashTransactionUtility.addCashTransactionCostItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer);
            //return items;
        }
        public string getInstrumentNo(AccountDocument _doc, out string typeName)
        {
            SupplierAccountDocument doc = _doc as SupplierAccountDocument;
            if (!PaymentMethodHelper.IsPaymentMethodWithReference(doc.paymentMethod))
            {
                typeName = null;
                return null;
            }
            typeName = PaymentMethodHelper.GetPaymentTypeReferenceName(doc.paymentMethod);
            return PaymentMethodHelper.selectReference(doc.paymentMethod,doc.checkNumber,doc.cpoNumber,doc.slipReferenceNo);
        }
    }
    public class DHSupplierReceivable : DHSupplierAccount, IDocumentServerHandler
    {
        public DHSupplierReceivable(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }

        #region IDocumentServerHandler Members

        public int Post(int AID, AccountDocument _doc)
        {
            SupplierAccountDocument doc = (SupplierAccountDocument)_doc;
            iERPTransactionBDE bde = (iERPTransactionBDE)ApplicationServer.GetBDE("iERP");
            TradeRelation supplier = bde.GetSupplier(doc.supplierCode);
            return base.PostSupplierTransaction(AID, _doc, supplier.ReceivableAccountID,1);
        }

        #endregion
        public override string getCashTransactionItemFormat(out bool payment)
        {
            payment = true;
            return "Advance payment for supplier {0}";
        }
        
    }
    public class DHSupplierPayable : DHSupplierAccount, IDocumentServerHandler
    {
        public DHSupplierPayable(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {


        }

        #region IDocumentServerHandler Members

        public int Post(int AID, AccountDocument _doc)
        {
            SupplierPayableDocument doc = (SupplierPayableDocument)_doc;
            iERPTransactionBDE bde = (iERPTransactionBDE)ApplicationServer.GetBDE("iERP");
            TradeRelation supplier = bde.GetSupplier(doc.supplierCode);
            return base.PostSupplierTransaction(AID, _doc, doc.retention?supplier.RetentionPayableAccountID:supplier.PayableAccountID,1);
        }

        #endregion

        public override string getCashTransactionItemFormat(out bool payment)
        {
            payment = true;
            return "Settlement of Credit to Supplier {0}";
        }
    }
    public class DHSupplierAdvanceReturn: DHSupplierAccount, IDocumentServerHandler
    {
        public DHSupplierAdvanceReturn(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {


        }


        public int Post(int AID, AccountDocument _doc)
        {
            SupplierAccountDocument doc = (SupplierAccountDocument)_doc;
            iERPTransactionBDE bde = (iERPTransactionBDE)ApplicationServer.GetBDE("iERP");
            TradeRelation supplier = bde.GetSupplier(doc.supplierCode);
            return base.PostSupplierTransaction(AID, _doc, supplier.ReceivableAccountID,-1);
        }
        public override string getCashTransactionItemFormat(out bool payment)
        {
            payment = false;
            return "Return of Advance Payment by Supplier {0}";
        }
    }
}

