﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    class DHSupplierAdvancePayment2 : DHPurchase2
    {
        public DHSupplierAdvancePayment2(AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        protected override void validateItem(TransactionDocumentItem ditem, TransactionItems item)
        {
            if (!item.IsExpenseItem || item.IsInventoryItem || item.IsSalesItem || item.IsInventoryItem)
                throw new ServerUserMessage("Only pure expense items are supported for supplier advance payment transaction");
            if (ditem.prepaid)
                throw new ServerUserMessage("Prepayment is not supported for supplier advance payment");
            if (ditem.remitted)
                throw new ServerUserMessage("Tex remission is not supported for supplier advance payment. Make the VAT and Withholding bases to zero");

        }
        protected override void getItemSinkTransactionAccount(int AID, Purchase2Document doc, TradeRelation relation, TransactionDocumentItem ditem, TransactionItems item, out int costCenterID, out int accountID, out int summaryAccountID,out bool summerizable)
        {
            //return bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, relation.ReceivableAccountID);
            costCenterID = bde.SysPars.mainCostCenterID;
            accountID = relation.ReceivableAccountID;
            summaryAccountID = -1;
            summerizable = false;

        }
    }
    class DHSupplierRetentionSettlement : DHPurchase2
    {
        public DHSupplierRetentionSettlement(AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        protected override void validateItem(TransactionDocumentItem ditem, TransactionItems item)
        {
            if (!item.IsExpenseItem || item.IsInventoryItem || item.IsSalesItem || item.IsInventoryItem)
                throw new ServerUserMessage("Only pure expense items are supported for supplier retention settlement transaction");
            if (ditem.prepaid)
                throw new ServerUserMessage("Prepayment is not supported for supplier retention settlement");
            if (ditem.remitted)
                throw new ServerUserMessage("Tex remission is not supported for supplier retention settlement. Make the VAT and Withholding bases to zero");

        }
        protected override void getItemSinkTransactionAccount(int AID, Purchase2Document doc, TradeRelation relation, TransactionDocumentItem ditem, TransactionItems item,out int costCenterID,out int accountID,out int summaryAccountID,out bool summerizable)
        {
            //return bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, relation.RetentionPayableAccountID);
            costCenterID = bde.SysPars.mainCostCenterID;
            accountID = relation.RetentionPayableAccountID;
            summaryAccountID = -1;
            summerizable = false;
        }
    }
    class DHSupplierCreditSettlement : DHPurchase2
    {
        public DHSupplierCreditSettlement(AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        protected override void validateItem(TransactionDocumentItem ditem, TransactionItems item)
        {
            if (!item.IsExpenseItem || item.IsInventoryItem || item.IsSalesItem || item.IsInventoryItem)
                throw new ServerUserMessage("Only pure expense items are supported for supplier credit settlement transaction");
            if (ditem.prepaid)
                throw new ServerUserMessage("Prepayment is not supported for supplier credit settlement");
            if (ditem.remitted)
                throw new ServerUserMessage("Tex remission is not supported for supplier credit settlement. Make the VAT and Withholding bases to zero");

        }
        protected override void getItemSinkTransactionAccount(int AID, Purchase2Document doc, TradeRelation relation, TransactionDocumentItem ditem, TransactionItems item, out int costCenterID, out int accountID, out int summaryAccountID,out bool summerizable)
        {
            //return bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, relation.PayableAccountID);
            costCenterID = bde.SysPars.mainCostCenterID;
            accountID = relation.PayableAccountID;
            summaryAccountID = -1;
            summerizable = false;
        }
    }
    class DHSupplierAdvanceReturn2 : DHSell2
    {
        public DHSupplierAdvanceReturn2(AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        protected override void validateItem(TransactionDocumentItem ditem, TransactionItems item)
        {
            if (!item.IsSalesItem || item.IsFixedAssetItem || item.IsExpenseItem || item.IsInventoryItem)
                throw new ServerUserMessage("Only pure sales items are supported for supplier advance return transaction");
            if (ditem.prepaid)
                throw new ServerUserMessage("Prepayment is not supported for supplier advance return ");
            if (ditem.remitted)
                throw new ServerUserMessage("Tex remission is not supported for supplier advance return . Make the VAT and Withholding bases to zero");

        }
        protected override INTAPS.Accounting.CostCenterAccount getItemSinkTransactionAccount(int AID, Sell2Document doc, TradeRelation relation, TransactionDocumentItem ditem, TransactionItems item)
        {
            return bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, relation.ReceivableAccountID);
        }
    }

    class DHCustomerAdvancePayment2 : DHSell2
    {
        public DHCustomerAdvancePayment2(AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        protected override void validateItem(TransactionDocumentItem ditem, TransactionItems item)
        {
            if (!item.IsSalesItem || item.IsFixedAssetItem || item.IsExpenseItem|| item.IsInventoryItem)
                throw new ServerUserMessage("Only pure sales items are supported for customer advance payment transaction");
            if (ditem.prepaid)
                throw new ServerUserMessage("Unearned revenue is not supported in customer advance payment transaction");
            if (ditem.remitted)
                throw new ServerUserMessage("Tex remission is not supported for customer advance payment. Make the VAT and Withholding bases to zero");

        }
        protected override INTAPS.Accounting.CostCenterAccount getItemSinkTransactionAccount(int AID, Sell2Document doc, TradeRelation relation, TransactionDocumentItem ditem, TransactionItems item)
        {
            return bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, relation.PayableAccountID);
        }

    }
    class DHCustomerRetentionSettlement : DHSell2
    {
        public DHCustomerRetentionSettlement(AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        protected override void validateItem(TransactionDocumentItem ditem, TransactionItems item)
        {
            if (!item.IsSalesItem || item.IsFixedAssetItem || item.IsExpenseItem || item.IsInventoryItem)
                throw new ServerUserMessage("Only pure sales items are supported for customer retention settlement transaction");
            if (ditem.prepaid)
                throw new ServerUserMessage("Unearned revenue is not supported in customer retention settlement transaction");
            if (ditem.remitted)
                throw new ServerUserMessage("Tex remission is not supported for customer retention settlement. Make the VAT and Withholding bases to zero");

        }
        protected override INTAPS.Accounting.CostCenterAccount getItemSinkTransactionAccount(int AID, Sell2Document doc, TradeRelation relation, TransactionDocumentItem ditem, TransactionItems item)
        {
            return bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, relation.RetentionReceivableAccountID);
        }
    }
    class DHCustomerCreditSettlement2 : DHSell2
    {
        public DHCustomerCreditSettlement2(AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        protected override void validateItem(TransactionDocumentItem ditem, TransactionItems item)
        {
            if (!item.IsSalesItem || item.IsFixedAssetItem || item.IsExpenseItem || item.IsInventoryItem)
                throw new ServerUserMessage("Only pure sales items are supported for customer credit settlement transaction");
            if (ditem.prepaid)
                throw new ServerUserMessage("Unearned revenue is not supported in customer credit settlement transaction");
            if (ditem.remitted)
                throw new ServerUserMessage("Tex remission is not supported for customer credit settlement. Make the VAT and Withholding bases to zero");

        }
        protected override INTAPS.Accounting.CostCenterAccount getItemSinkTransactionAccount(int AID, Sell2Document doc, TradeRelation relation, TransactionDocumentItem ditem, TransactionItems item)
        {
            return bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, relation.ReceivableAccountID);
        }
    }
    class DHCustomerAdvanceReturn2 : DHPurchase2
    {
        public DHCustomerAdvanceReturn2(AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        protected override void validateItem(TransactionDocumentItem ditem, TransactionItems item)
        {
            if (!item.IsExpenseItem|| item.IsFixedAssetItem || item.IsSalesItem|| item.IsInventoryItem)
                throw new ServerUserMessage("Only pure expense items are supported for customer advance return transaction");
            if (ditem.prepaid)
                throw new ServerUserMessage("Unearned revenue is not supported in customer advance return transaction");
            if (ditem.remitted)
                throw new ServerUserMessage("Tex remission is not supported for customer advance return. Make the VAT and Withholding bases to zero");

        }
        protected override void getItemSinkTransactionAccount(int AID, Purchase2Document doc, TradeRelation relation, TransactionDocumentItem ditem, TransactionItems item, out int costCenterID, out int accountID, out int summaryAccountID,out bool summerizable)
        {
            //return bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, relation.PayableAccountID);
            costCenterID=bde.SysPars.mainCostCenterID;
            accountID=relation.PayableAccountID;
            summaryAccountID=-1;
            summerizable = false;
        }
    }
}
