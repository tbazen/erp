using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    public class DHWithdrawal : DeleteReverseDocumentHandler, IDocumentServerHandler,ICashTransaction
    {
        public DHWithdrawal(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
          iERPTransactionBDE _bde = null;
        iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, AccountDocument doc, UserSessionData userSession)
        {
            iERPTransactionBDE bde = (iERPTransactionBDE)ApplicationServer.GetBDE("iERP");
            return bde.CashierPermissionGranted(userSession);
        }

        public override string GetHTML(AccountDocument doc)
        {
            WithdrawalDocument bdoc = (WithdrawalDocument)doc;
            return "<b>Amount:<b/>" + TSConstants.FormatBirr(bdoc.amount);
        }

        public int Post(int AID,AccountDocument doc)
        {
            WithdrawalDocument withdrawalDocument = (WithdrawalDocument)doc;
            TransactionOfBatch[] transactions = null;
            PayrollBDE payroll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
            bool validateCash=false;
            switch (withdrawalDocument.paymentMethod)
            {
                case BizNetPaymentMethod.Cash:
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.BankTransferFromCash:
                    transactions = CollectCashPaymentTransactionsOfBatch(AID, withdrawalDocument);
                    validateCash = true;
                    break;

                case BizNetPaymentMethod.Check:
                case BizNetPaymentMethod.CPOFromBankAccount:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    transactions = CollectBankPaymentTransactionsOfBatch(AID, withdrawalDocument);
                    validateCash = false;
                    break;
                case BizNetPaymentMethod.None:
                    break;
                default:
                    break;
            }

            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                if (withdrawalDocument.voucher != null)
                    withdrawalDocument.PaperRef = withdrawalDocument.voucher.reference;
                DocumentTypedReference[] typedRefs = withdrawalDocument.voucher == null ? new DocumentTypedReference[0] :
                new DocumentTypedReference[] { withdrawalDocument.voucher };

                if (doc.AccountDocumentID == -1)
                    ret = bdeAccounting.RecordTransaction(AID, withdrawalDocument,
                    transactions
                    , true, typedRefs);
                else
                    ret = bdeAccounting.UpdateTransaction(AID, withdrawalDocument, transactions, true, typedRefs);
                if (!doc.scheduled && !bde.AllowNegativeTransactionPost())
                {
                    string account = validateCash ? "cash account" : "bank account";
                    bdeAccounting.validateNegativeBalance
                       (withdrawalDocument.assetAccountID, 0, false, withdrawalDocument.DocumentDate
                       , "Transaction is blocked because there is no sufficient balance in the " + account);
                }
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        private TransactionOfBatch[] CollectCashPaymentTransactionsOfBatch(int AID, WithdrawalDocument withDrawalDocument)
        {
            CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.bankServiceChargeByCash);
            if (csa == null)
                throw new ServerConfigurationError("The bank service charge by cash account not configured properly. The account does not belong to 'Head Quarter' cost center");

            int serviceChargeAccountID = csa.id;
            TransactionOfBatch[] transactions = null;

            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            int withdrawalAccountID = bde.SysPars.WithdrawalAccountID;
            CostCenterAccount csWithdrawal = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, withdrawalAccountID);
            if (csWithdrawal == null)
            {
                throw new ServerUserMessage("'Withdrawal Account' does not belong to 'Head Quarter' cost center. Please join the account to the cost center and try again!");
            }
            creditSide = new TransactionOfBatch(withDrawalDocument.assetAccountID, -withDrawalDocument.amount);
            debitSide = new TransactionOfBatch(csWithdrawal.id, withDrawalDocument.amount);

            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(withDrawalDocument.paymentMethod) && withDrawalDocument.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(withDrawalDocument.serviceChargeAmount, 0))
            {
                string note = withDrawalDocument.paymentMethod == BizNetPaymentMethod.CPOFromCash ? "CPO service charge" : "Bank transfer service charge";

                TransactionOfBatch debitSide2, creditSide2;
                debitSide2 = new TransactionOfBatch(serviceChargeAccountID, withDrawalDocument.serviceChargeAmount, note);
                creditSide2 = new TransactionOfBatch(withDrawalDocument.assetAccountID, -withDrawalDocument.serviceChargeAmount, note);
                transactions = new TransactionOfBatch[] { debitSide, creditSide, debitSide2, creditSide2 };
            }
            else
            {
                transactions = new TransactionOfBatch[] { debitSide, creditSide };
            }
            return transactions;
        }
        private TransactionOfBatch[] CollectBankPaymentTransactionsOfBatch(int AID, WithdrawalDocument withdrawalDocument)
        {
            TransactionOfBatch[] transactions = null;
            TransactionOfBatch debitSide;
            TransactionOfBatch creditSide;
            int withdrawalAccountID = bde.SysPars.WithdrawalAccountID;
            CostCenterAccount csWithdrawal = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, withdrawalAccountID);
            if (csWithdrawal == null)
            {
                throw new ServerUserMessage("'Withdrawal Account' does not belong to 'Head Quarter' cost center. Please join the account to the cost center and try again!");
            }

            BankAccountInfo ba = bde.GetBankAccount(withdrawalDocument.assetAccountID);
            int serviceChargeAccountID = ba.bankServiceChargeCsAccountID;
            string note = withdrawalDocument.paymentMethod == BizNetPaymentMethod.CPOFromBankAccount ? "CPO service charge" : "Bank transfer service charge";

            creditSide = new TransactionOfBatch(ba.mainCsAccount, -withdrawalDocument.amount);
            debitSide = new TransactionOfBatch(csWithdrawal.id, withdrawalDocument.amount);
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(withdrawalDocument.paymentMethod) && withdrawalDocument.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(withdrawalDocument.serviceChargeAmount, 0))
            {
                TransactionOfBatch debitSide2, creditSide2;
                debitSide2 = new TransactionOfBatch(serviceChargeAccountID, withdrawalDocument.serviceChargeAmount, note);
                creditSide2 = new TransactionOfBatch(ba.mainCsAccount, -withdrawalDocument.serviceChargeAmount, note);
                transactions = new TransactionOfBatch[] { debitSide, creditSide, debitSide2, creditSide2 };
            }
            else
            {
                transactions = new TransactionOfBatch[] { debitSide, creditSide };
            }
            return transactions;
        }
        public void ValidateAccounts(WithdrawalDocument withdrawalDocument, bool validateCash)
        {
            if (!validateCash) //validate bank
            {
                ValidateBankAccount(withdrawalDocument);
            }
            else
            {
                ValidateCashAccount(withdrawalDocument);
            }
        }

        private void ValidateBankAccount(WithdrawalDocument withdrawalDocument)
        {
            if (withdrawalDocument.assetAccountID != -1)
            {
                double balance = GetBankAccountBalance(withdrawalDocument);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make bank account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No bank accounts found.\nPlease configure bank account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetBankAccountBalance(WithdrawalDocument withdrawalDocument)
        {
            return bdeAccounting.GetNetBalanceAsOf(withdrawalDocument.assetAccountID, 0, withdrawalDocument.DocumentDate);
        }

        private void ValidateCashAccount(WithdrawalDocument withdrawalDocument)
        {
            if (withdrawalDocument.assetAccountID != -1)
            {
                double balance = GetCashAccountBalance(withdrawalDocument);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make cash account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No cash accounts found.\nPlease configure cash account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetCashAccountBalance(WithdrawalDocument withdrawalDocument)
        {
            return bdeAccounting.GetNetBalanceAsOf(withdrawalDocument.assetAccountID, 0, withdrawalDocument.DocumentDate);
        }
       
        #endregion

        public List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option, AccountTransaction[] docTrans)
        {
            WithdrawalDocument paymentDoc = (WithdrawalDocument)doc;
            if (PaymentMethodHelper.IsPaymentMethodCredit(paymentDoc.paymentMethod))
                return null;
            if (!CashTransactionUtility.isTransactionInCostCenter(bde.Accounting, doc.AccountDocumentID, option.costCenters, false))
                return null;
            List<CashFlowItem> items = new List<CashFlowItem>();
            string html = System.Web.HttpUtility.HtmlEncode("Capital Returned to Owner");
            items.Add(new CashFlowItem(html, paymentDoc.amount));

            CashTransactionUtility.addCashTransactionCostItem(items, paymentDoc.paymentMethod, paymentDoc.assetAccountID, paymentDoc.serviceChargeAmount, paymentDoc.serviceChargePayer);
            return items;
        }
        public string getInstrumentNo(AccountDocument _doc, out string typeName)
        {
            WithdrawalDocument doc = _doc as WithdrawalDocument;
            if (!PaymentMethodHelper.IsPaymentMethodWithReference(doc.paymentMethod))
            {
                typeName = null;
                return null;
            }
            typeName = PaymentMethodHelper.GetPaymentTypeReferenceName(doc.paymentMethod);
            return PaymentMethodHelper.selectReference(doc.paymentMethod,doc.checkNumber,doc.cpoNumber,doc.slipReferenceNo);
        }
    }

}
