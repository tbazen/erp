using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
namespace BIZNET.iERP.Server
{
    public class DHZReport : DeleteReverseDocumentHandler, IDocumentServerHandler
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }

        public DHZReport(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, AccountDocument doc, UserSessionData userSession)
        {
            return bde.CashierPermissionGranted(userSession);
        }

        public override string GetHTML(AccountDocument doc)
        {
            ZReportDocument bdoc = (ZReportDocument)doc;
            return "<b>Amount:<b/>" + TSConstants.FormatBirr(bdoc.grandTotal);
        }
        int _excludeFromSearch=-1;
        bool FilterDelegate(AccountDocument doc)
        {
            if (doc.AccountDocumentID == _excludeFromSearch)
                return false;
            return true;
        }

        public int Post(int AID,AccountDocument doc)
        {
            ZReportDocument zReportDocument = (ZReportDocument)doc;
            _excludeFromSearch = doc.AccountDocumentID;
            AccountDocument accDoc = bdeAccounting.FindFirstDocument(zReportDocument.voucher.reference, zReportDocument.DocumentTypeID, new INTAPS.Accounting.BDE.AccountDocumentFilterDelegate(FilterDelegate), false);
            if (accDoc != null)
                throw new INTAPS.ClientServer.ServerUserMessage("Another Z-Report document with the same reference number already exists.\n Please try again with different reference number");
            TransactionOfBatch[] transactions = null;
            bool validateCash = false;
            switch (zReportDocument.paymentMethod)
            {
                case BizNetPaymentMethod.Cash:
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.BankTransferFromCash:
                    transactions = CollectCashPaymentTransactionsOfBatch(AID, zReportDocument);
                    validateCash = true;
                    break;

                case BizNetPaymentMethod.Check:
                case BizNetPaymentMethod.CPOFromBankAccount:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    transactions = CollectBankPaymentTransactionsOfBatch(AID,zReportDocument);
                    validateCash = false;
                    break;
                case BizNetPaymentMethod.None:
                    break;
                default:
                    break;
            }

            int ret;
            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                TransactionOfBatch[] filteredTran = FilterTransactions(transactions);
                    
                 if (zReportDocument.voucher != null)
                    zReportDocument.PaperRef = zReportDocument.voucher.reference;
                 DocumentTypedReference[] typedRefs = zReportDocument.voucher == null ? new DocumentTypedReference[0] :
                 new DocumentTypedReference[] { zReportDocument.voucher };

                 if (doc.AccountDocumentID == -1)
                 {
                     ret = bdeAccounting.RecordTransaction(AID, zReportDocument,
                     filteredTran
                     , true, typedRefs);
                 }
                 else
                     ret = bdeAccounting.UpdateTransaction(AID, zReportDocument, filteredTran, true, typedRefs);
                 if (!doc.scheduled && !bde.AllowNegativeTransactionPost())
                 {
                     string account = validateCash ? "cash account" : "bank account";
                     bdeAccounting.validateNegativeBalance
                        (zReportDocument.assetAccountID, 0, false, zReportDocument.DocumentDate
                        , "Transaction is blocked because there is no sufficient balance in the " + account);
                 }
              //  ValidateAccounts(zReportDocument, validateCash);
                bdeAccounting.WriterHelper.CommitTransaction();
                return ret;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        protected virtual TransactionOfBatch[] CollectCashPaymentTransactionsOfBatch(int AID,ZReportDocument zReportDoc)
        {
            CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.bankServiceChargeByCash);
            if (csa == null)
                throw new ServerConfigurationError("The bank service charge by cash account not configured properly. The account does not belong to 'Head Quarter' cost center");

            int serviceChargeAccountID = csa.id;
            TransactionOfBatch[] transactions = null;

            TransactionOfBatch debitSide;
            TransactionOfBatch creditSideTaxable, creditSideNonTaxable, creditSideTaxPayable;
            creditSideTaxable = creditSideNonTaxable = creditSideTaxPayable = null;
            if (AccountBase.AmountGreater(zReportDoc.NetTaxableAmount, 0))
            {
                int zReportTaxableAccountID = bde.SysPars.zReportTaxableAccountID;
                CostCenterAccount csAccount = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, zReportTaxableAccountID);
                if (csAccount == null)
                    throw new ServerUserMessage("'Z-Report taxable' account does not belong to 'Head Quarter' cost center");

                creditSideTaxable = new TransactionOfBatch(csAccount.id, -zReportDoc.NetTaxableAmount);
            }
            if (AccountBase.AmountGreater(zReportDoc.NetNonTaxableAmount, 0))
            {
                int zReportNonTaxableAccountID = bde.SysPars.zReportNonTaxableAccountID;
                CostCenterAccount csAccount = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, zReportNonTaxableAccountID);
                if (csAccount == null)
                    throw new ServerUserMessage("'Z-Report non-taxable' account does not belong to 'Head Quarter' cost center");

                creditSideNonTaxable = new TransactionOfBatch(csAccount.id, -zReportDoc.NetNonTaxableAmount);
            }
            if (AccountBase.AmountGreater(zReportDoc.NetTaxAmount, 0))
            {
                int taxPayableAccountID = -1;
                int taxCsPayableAccountID = -1;
                if (zReportDoc.taxImposed.TaxType == TaxType.VAT)
                {
                    taxPayableAccountID = bde.SysPars.outPutVATAccountID;
                    CostCenterAccount csTaxPayableAccount = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, taxPayableAccountID);
                    if (csTaxPayableAccount == null)
                        throw new ServerUserMessage("The 'Out put VAT' account does not belong to 'Head Quarter' cost center");
                    taxCsPayableAccountID = csTaxPayableAccount.id;
                }
                else if (zReportDoc.taxImposed.TaxType == TaxType.TOT)
                {
                    taxPayableAccountID = bde.SysPars.collectedTOTAccountID;
                    CostCenterAccount csTaxPayableAccount = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, taxPayableAccountID);
                    if (csTaxPayableAccount == null)
                        throw new ServerUserMessage("The 'Collected TOT' account does not belong to 'Head Quarter' cost center");
                    taxCsPayableAccountID = csTaxPayableAccount.id;
                }
                creditSideTaxPayable = new TransactionOfBatch(taxCsPayableAccountID, -zReportDoc.NetTaxAmount);
            }
            debitSide = new TransactionOfBatch(zReportDoc.assetAccountID, zReportDoc.grandTotal);
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(zReportDoc.paymentMethod) && zReportDoc.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(zReportDoc.serviceChargeAmount, 0))
            {
                string note = zReportDoc.paymentMethod == BizNetPaymentMethod.CPOFromCash ? "CPO service charge" : "Bank transfer service charge";

                TransactionOfBatch debitSide2, creditSide2;
                debitSide2 = new TransactionOfBatch(serviceChargeAccountID, zReportDoc.serviceChargeAmount, note);
                creditSide2 = new TransactionOfBatch(zReportDoc.assetAccountID, -zReportDoc.serviceChargeAmount, note);
                transactions = new TransactionOfBatch[] { debitSide, creditSideTaxable, creditSideNonTaxable, creditSideTaxPayable, debitSide2, creditSide2 };
            }
            else
            {
                transactions = new TransactionOfBatch[] { debitSide, creditSideTaxable, creditSideNonTaxable, creditSideTaxPayable };
            }
            return transactions;
        }
        protected virtual TransactionOfBatch[] CollectBankPaymentTransactionsOfBatch(int AID,ZReportDocument zReportDoc)
        {
            TransactionOfBatch[] transactions = null;

            TransactionOfBatch debitSide;
            BankAccountInfo ba = bde.GetBankAccount(zReportDoc.assetAccountID);
            int serviceChargeAccountID = ba.bankServiceChargeCsAccountID;

            string note = zReportDoc.paymentMethod == BizNetPaymentMethod.CPOFromBankAccount ? "CPO service charge" : "Bank transfer service charge";
            TransactionOfBatch creditSideTaxable, creditSideNonTaxable, creditSideTaxPayable;
            creditSideTaxable = creditSideNonTaxable = creditSideTaxPayable = null;
            if (AccountBase.AmountGreater(zReportDoc.NetTaxableAmount, 0))
            {
                int zReportTaxableAccountID = bde.SysPars.zReportTaxableAccountID;
                CostCenterAccount csAccount = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, zReportTaxableAccountID);
                if (csAccount == null)
                    throw new ServerUserMessage("'Z-Report taxable' account does not belong to 'Head Quarter' cost center");

                creditSideTaxable = new TransactionOfBatch(csAccount.id, -zReportDoc.NetTaxableAmount);
            }
            if (AccountBase.AmountGreater(zReportDoc.NetNonTaxableAmount, 0))
            {
                int zReportNonTaxableAccountID = bde.SysPars.zReportNonTaxableAccountID;
                CostCenterAccount csAccount = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, zReportNonTaxableAccountID);
                if (csAccount == null)
                    throw new ServerUserMessage("'Z-Report non-taxable' account does not belong to 'Head Quarter' cost center");

                creditSideNonTaxable = new TransactionOfBatch(csAccount.id, -zReportDoc.NetNonTaxableAmount);
            }
            if (AccountBase.AmountGreater(zReportDoc.NetTaxAmount, 0))
            {
                int taxPayableAccountID=-1;
                int taxCsPayableAccountID = -1;
                if (zReportDoc.taxImposed.TaxType == TaxType.VAT)
                {
                    taxPayableAccountID = bde.SysPars.outPutVATAccountID;
                    CostCenterAccount csTaxPayableAccount = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, taxPayableAccountID);
                    if (csTaxPayableAccount == null)
                        throw new ServerUserMessage("The 'Out put VAT' account does not belong to 'Head Quarter' cost center");
                    taxCsPayableAccountID = csTaxPayableAccount.id;
                }
                else if (zReportDoc.taxImposed.TaxType == TaxType.TOT)
                {
                    taxPayableAccountID = bde.SysPars.collectedTOTAccountID;
                    CostCenterAccount csTaxPayableAccount = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, taxPayableAccountID);
                    if (csTaxPayableAccount == null)
                        throw new ServerUserMessage("The 'Collected TOT' account does not belong to 'Head Quarter' cost center");
                    taxCsPayableAccountID = csTaxPayableAccount.id;
                }
                creditSideTaxPayable = new TransactionOfBatch(taxCsPayableAccountID, -zReportDoc.NetTaxAmount);
            }
            debitSide = new TransactionOfBatch(ba.mainCsAccount, zReportDoc.grandTotal);
            if (PaymentMethodHelper.IsPaymentMethodWithServiceCharge(zReportDoc.paymentMethod) && zReportDoc.serviceChargePayer == ServiceChargePayer.Company
                && AccountBase.AmountGreater(zReportDoc.serviceChargeAmount,0))
            {
                TransactionOfBatch debitSide2, creditSide2;
                debitSide2 = new TransactionOfBatch(serviceChargeAccountID, zReportDoc.serviceChargeAmount, note);
                creditSide2 = new TransactionOfBatch(ba.mainCsAccount, -zReportDoc.serviceChargeAmount, note);
                transactions = new TransactionOfBatch[] { debitSide, creditSideTaxable, creditSideNonTaxable, creditSideTaxPayable, debitSide2, creditSide2 };
            }
            else
            {
                transactions = new TransactionOfBatch[] { debitSide, creditSideTaxable, creditSideNonTaxable, creditSideTaxPayable };
            }
            return transactions;
        }
        private TransactionOfBatch[] FilterTransactions(TransactionOfBatch[] transactions)
        {
            List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
            foreach (TransactionOfBatch t in transactions)
            {
                if (t != null)
                    tran.Add(t);
            }
            return tran.ToArray();
        }
        public void ValidateAccounts(ZReportDocument zReportDoc, bool validateCash)
        {
            if (!validateCash) //validate bank
            {
                ValidateBankAccount(zReportDoc);
            }
            else
            {
                ValidateCashAccount(zReportDoc);
            }
        }

        private void ValidateBankAccount(ZReportDocument zReportDoc)
        {
            if (zReportDoc.assetAccountID != -1)
            {
                double balance = GetBankAccountBalance(zReportDoc);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make bank account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No bank accounts found.\nPlease configure bank account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetBankAccountBalance(ZReportDocument zReportDoc)
        {
            return bdeAccounting.GetNetBalanceAsOf(zReportDoc.assetAccountID, 0, zReportDoc.DocumentDate);
        }

        private void ValidateCashAccount(ZReportDocument zReportDoc)
        {
            if (zReportDoc.assetAccountID != -1)
            {
                double balance = GetCashAccountBalance(zReportDoc);
                if (balance < 0)
                    throw new INTAPS.ClientServer.ServerUserMessage("The transaction will make cash account negative. Transaction is canceled.");
            }
            else
            {
                throw new INTAPS.ClientServer.ServerUserMessage("No cash accounts found.\nPlease configure cash account, set balance to it in opening transactions setup and try again!");
            }
        }
        private double GetCashAccountBalance(ZReportDocument zReportDoc)
        {
            return bdeAccounting.GetNetBalanceAsOf(zReportDoc.assetAccountID, 0, zReportDoc.DocumentDate);
        }

        #endregion
    }
}

