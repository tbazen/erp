﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;

namespace BIZNET.iERP.Server
{
    public class PayrollDocumentHandler:INTAPS.Accounting.IDocumentServerHandler
    {
                // Fields
        AccountingBDE bdeAccounting;
        PayrollBDE _bdePayroll=null;
        PayrollBDE bdePayroll
        {
            get
            {
                if (_bdePayroll == null)
                    _bdePayroll = INTAPS.ClientServer.ApplicationServer.GetBDE("Payroll") as PayrollBDE;
                return _bdePayroll;
            }
        }
        // Methods
        public PayrollDocumentHandler(AccountingBDE bdeFinance)
        {
            this.bdeAccounting = bdeFinance;
        }

        public bool CheckPostPermission(int docID, INTAPS.Accounting.AccountDocument _doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            return true;
        }

        public int Post(int AID, INTAPS.Accounting.AccountDocument _doc)
        {
            throw new INTAPS.ClientServer.ServerUserMessage("Post is not supported throught the document handler");
        }

        public string GetHTML(INTAPS.Accounting.AccountDocument _doc)
        {
            return ((PayrollDocument)_doc).BuildHTML();
        }

        public void DeleteDocument(int AID, int docID)
        {
            PayrollDocument doc=bdeAccounting.GetAccountDocument(docID,true) as PayrollDocument;
            iERPTransactionBDE bde = INTAPS.ClientServer.ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            if (bde.IsDocumentDeclared(docID))
                throw new INTAPS.ClientServer.ServerUserMessage("It is not allowed to delete a payroll included in tax declaration");
            bdePayroll.DeletePayrollPosted(AID, doc);
        }

        public void ReverseDocument(int AID, int docID)
        {
            throw new INTAPS.ClientServer.ServerUserMessage("Reverse not supported for payroll documents");
        }
    }
}
