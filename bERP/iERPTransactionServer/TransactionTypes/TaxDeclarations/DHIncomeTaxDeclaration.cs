using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using BIZNET.iERP.TypedDataSets;
using INTAPS.Accounting;
using INTAPS.Payroll.BDE;
using INTAPS.ClientServer;
using INTAPS.Payroll;

namespace BIZNET.iERP.Server
{
    public class DHIncomeTaxDeclaration : DHTaxDeclarationBase<IncomeTaxDeclarationDocument>, ITaxDeclarationServerHandler
    {
        PayrollBDE _payRoll = null;
        protected PayrollBDE bdePayroll
        {
            get
            {
                if (_payRoll == null)
                    _payRoll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
                return _payRoll;
            }
        }
        public DHIncomeTaxDeclaration(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        protected override int TaxPayableAcount
        {
            get
            {
                return bdePayroll.SysPars.staffIncomeTaxAccountID;
            }
        }

        protected override int TaxPenalityAccount
        {
            get
            {
                return bdeiERP.SysPars.taxPenalityAcountID;
            }
        }
        protected override int TaxInterestAccount
        {
            get
            {
                return bdeiERP.SysPars.taxInterestAccountID;
            }
        }
        protected override int TaxDeductionsAccount
        {
            get
            {

                return bdeiERP.SysPars.taxExemptionsAccountID;
            }
        }
        public DataSet BuildReportDataSet(int AID, iERPTransactionBDE bde, DateTime declarationDate, int periodID, int taxCenterID, int[] documents, int[] rejectedDocuments, out double totalTax, bool newDeclaration)
        {
            INTAPS.RDBMS.SQLHelper helper = bdePayroll.GetReaderHelper();
            try
            {
                ConnectToiERP();
                List<Employee> resignedEmployees = new List<Employee>();
                IncomeTaxDeclarationData ret = new IncomeTaxDeclarationData();
                AccountingPeriod period = bde.GetAccountingPeriod(bde.SysPars.taxDeclarationPeriods, periodID);
                CompanyProfile prof = bde.SysPars.companyProfile;

                INTAPS.Ethiopic.EtGrDate month = INTAPS.Ethiopic.EtGrDate.ToEth(period.fromDate);
                ret.Summary.AddSummaryRow("",
                    prof.Name,
                 prof.Region, prof.Zone, prof.Woreda, prof.Kebele, prof.HouseNumber, prof.TIN, "", "", prof.Telephone, prof.Fax, month.Month, month.Year, 0, 0, 0, 0, 0, 0, 0, 0
                , periodID, "1");

              

                int NEmployee = 0;
                int i = 0;
                totalTax = 0;
                foreach (int doc in documents)
                {
                    
                    AccountDocument adoc = bdeAccounting.GetAccountDocument(doc, true);
                    if (adoc is PayrollSetDocument)
                    {
                        foreach (PayrollDocument pay in ((PayrollSetDocument)adoc).payrolls)
                        {
                            if (pay.employee.TaxCenter != taxCenterID)
                                continue;
                            /*INTAPS.Payroll.PayrollDocument[] payroll = bdePayroll.GetPayrollByFilter(helper, "AccountDocumentID =" + doc + "and (PostDate>='" + period.fromDate + "' and PostDate<'" + period.toDate + "')");
                            if (payroll.Length != 1)
                                throw new INTAPS.ClientServer.ServerUserMessage("Database inconsistency " + payroll.Length + " found for payroll docID:" + doc);
                            INTAPS.Payroll.PayrollDocument pay = payroll[0];*/

                            INTAPS.Payroll.Employee emp = pay.employee;
                            if (emp.status == EmployeeStatus.Fired)
                            {
                                resignedEmployees.Add(emp);
                            }
                            INTAPS.Payroll.PayrollComponent compGrossSalary;
                            INTAPS.Payroll.PayrollComponent compOvertime;
                            INTAPS.Payroll.PayrollComponent compTransportAllowance;
                            INTAPS.Payroll.PayrollComponent compTaxableTransportAllowance;
                            INTAPS.Payroll.PayrollComponent compIncomeTax;
                            INTAPS.Payroll.PayrollComponent compCostSharing;

                            compGrossSalary = pay.GetComponent(bde.SysPars.GrossSalaryPayrollComponentID, bde.SysPars.GrossSalaryPayrollFormulaID);
                            compOvertime = pay.GetComponent(bde.SysPars.OverTimePayrollComponentID, bde.SysPars.OverTimePayrollFormulaID);
                            compTransportAllowance = pay.GetComponent(bde.SysPars.TransportAllowancePayrollComponentID, bde.SysPars.TransportAllowancePayrollFormulaID);
                            compTaxableTransportAllowance = pay.GetComponent(bde.SysPars.TaxableTransportAllowancePayrollComponentID, bde.SysPars.TaxableTransportAllowancePayrollFormulaID);
                            compIncomeTax = pay.GetComponent(bde.SysPars.IncomeTaxPayrollComponentID, bde.SysPars.IncomeTaxPayrollFormulaID);
                            compCostSharing = pay.GetComponent(bde.SysPars.CostSharingPayrollComponentID, bde.SysPars.CostSharingPayrollFormulaID);
                            double otherBenefits = 0;
                            if (!string.IsNullOrEmpty(bde.SysPars.otherBenfitsFormulaIDs))
                            {
                                string[] fids = bde.SysPars.otherBenfitsFormulaIDs.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                if (fids.Length < 2 || fids.Length % 2 == 1)
                                    throw new ServerUserMessage("Please configure other benefits settings correctly");
                                for (int j = 0; j < fids.Length / 2; j++)
                                {
                                    int cid;
                                    int fid;
                                    if (!int.TryParse(fids[2 * j], out cid))
                                    throw new ServerUserMessage("Please configure other benefits settings correctly");
                                    if (!int.TryParse(fids[2 * j + 1], out fid))
                                        throw new ServerUserMessage("Please configure other benefits settings correctly");
                                    INTAPS.Payroll.PayrollComponent c = pay.GetComponent(cid, fid);
                                    if(c==null)
                                        throw new ServerUserMessage("Please configure other benefits settings correctly. The formula IDs are not correct");
                                    otherBenefits += c.Amount;
                                }
                            }

                            
                            double overTime = 0;
                            double incomeTax = 0;
                            double costSharing = 0;
                            
                            double taxableTransportallowance = 0;
                            double transportAllowance = 0;
                            if (compOvertime != null)
                                overTime = compOvertime.Amount;
                            if (compIncomeTax != null)
                                incomeTax = compIncomeTax.Amount;
                            if (compCostSharing != null)
                                costSharing = compCostSharing.Amount;

                            if (compTransportAllowance != null)
                                transportAllowance = compTransportAllowance.Amount;
                            if (compTaxableTransportAllowance != null)
                                taxableTransportallowance = compTaxableTransportAllowance.Amount;
                            
                            ret.Continuation.AddContinuationRow((i + 1).ToString()
                            , emp.TIN, emp.employeeName
                            , INTAPS.Ethiopic.EtGrDate.ToEth(emp.enrollmentDate).ToString()
                            , emp.grossSalary
                            , transportAllowance
                            , taxableTransportallowance
                            , overTime
                            , otherBenefits
                            , emp.grossSalary + emp.TaxableTransportAllowance + overTime + otherBenefits+taxableTransportallowance
                            , incomeTax
                            , costSharing
                            , pay.NetPay
                            , ret.Summary[0], emp.id);
                            totalTax += incomeTax;
                            NEmployee++;
                            i++;

                        }
                    }
                    else if (adoc is LaborPaymentDocument)
                    {
                        LaborPaymentDocument ldoc = (LaborPaymentDocument)adoc;
                        if (ldoc.taxCenter != taxCenterID)
                            continue;
                        double lIncomeTax = DHLaborPayment.CalculateIncomeTax(ldoc.amount);
                        ret.Continuation.AddContinuationRow((i + 1).ToString()
                                   , "", ldoc.paidTo
                                   , INTAPS.Ethiopic.EtGrDate.ToEth(ldoc.DocumentDate).ToString()
                                   , ldoc.amount
                                   , 0
                                   , 0
                                   , 0
                                   , 0
                                   , ldoc.amount
                                   , lIncomeTax
                                   , 0
                                   , ldoc.amount - lIncomeTax
                                   , ret.Summary[0], bdeiERP.SysPars.laborIncomeTaxAccountID);
                        totalTax += lIncomeTax;
                        NEmployee++;
                        i++;


                    }
                    else if (adoc is GroupPayrollDocument)
                    {
                        GroupPayrollDocument gpDoc = (GroupPayrollDocument)adoc;
                        if (gpDoc.taxCenter != taxCenterID)
                            continue;
                        ret.Continuation.AddContinuationRow((i + 1).ToString()
                                   , "", gpDoc.description
                                   , INTAPS.Ethiopic.EtGrDate.ToEth(gpDoc.DocumentDate).ToString()
                                   , gpDoc.grossSalaryTotalAmount
                                   , 0
                                   , 0
                                   , gpDoc.overTimeTotalAmount
                                   , 0
                                   , gpDoc.taxableGrossSalaryTotalAmount
                                   , gpDoc.incomeTaxTotalAmount
                                   , 0
                                   , gpDoc.totalSalary
                                   , ret.Summary[0], bdePayroll.SysPars.groupPayrollIncomeTaxAccount);
                        totalTax += gpDoc.incomeTaxTotalAmount;
                        NEmployee++;
                        i++;

                    }
                }
                ret.Summary[0][ret.Summary.NEmployeeColumn] = NEmployee;
                if (NEmployee == 0)
                    ret = null;
                //Resigned employees
                if (ret != null)
                {
                    foreach (Employee emp in resignedEmployees)
                    {
                        ret.ResignedEmployees.AddResignedEmployeesRow("", emp.TIN, emp.employeeName, ret.Summary[0]);
                    }
                }
                return ret;
            }
            finally
            {
                bdePayroll.ReleaseHelper(helper);
            }
        }
        public int[] GetCandidateDocuments(iERPTransactionBDE bde, DateTime declarationDate, int periodID, out int []taxCenterID, out double[] taxAmount)
        {
            List<int> taxCenters = new List<int>();
            AccountingPeriod period = bde.GetAccountingPeriod(bde.SysPars.taxDeclarationPeriods, periodID);
            int N;
            AccountDocument[] docs = bdeAccounting.GetAccountDocuments(null, bdeAccounting.GetDocumentTypeByType(typeof(PayrollSetDocument)).id,
                false, period.fromDate, period.toDate, 0, -1, out N);
            AccountDocument[] ldocs = bdeAccounting.GetAccountDocuments(null, bdeAccounting.GetDocumentTypeByType(typeof(LaborPaymentDocument)).id,
                true, period.fromDate, period.toDate, 0, -1, out N);
            AccountDocument[] gPayrollDocs = bdeAccounting.GetAccountDocuments(null, bdeAccounting.GetDocumentTypeByType(typeof(GroupPayrollDocument)).id,
                false, period.fromDate, period.toDate, 0, -1, out N);

            
            INTAPS.RDBMS.SQLHelper helper = bde.GetReaderHelper();
            try
            {
                List<int> ret = new List<int>();
                List<double> amount = new List<double>();
                AccountingPeriod pp = bde.GetAccountingPeriod(bde.SysPars.taxDeclarationPeriods, periodID);
                _GetPayrollDocumentCandidates(bde, pp, docs, helper, ret, amount, taxCenters);
                _GetLaborPaymentCandidates(bde, ldocs, helper, ret, amount,taxCenters);
                _GetGroupPayrollCandidates(bde, pp, gPayrollDocs, helper, ret, amount, taxCenters);
                taxAmount = amount.ToArray();
                taxCenterID = taxCenters.ToArray();
                return ret.ToArray();
            }
            finally
            {
                bde.ReleaseHelper(helper);
            }
        }
        private void _GetPayrollDocumentCandidates(iERPTransactionBDE bde, AccountingPeriod period,AccountDocument[] docs, INTAPS.RDBMS.SQLHelper helper, List<int> ret, List<double> amount,List<int> taxCenters)
        {
            foreach (AccountDocument doc in docs)
            {
                if ((int)helper.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.{1} where documentID={2} and declarationType={3}", bde.DBName, typeof(DeclaredDocument).Name, doc.AccountDocumentID, bdeAccounting.GetDocumentTypeByType(typeof(IncomeTaxDeclarationDocument)).id)) > 0)
                    continue;

                PayrollSetDocument set = bdeAccounting.GetAccountDocument(doc.AccountDocumentID, true) as PayrollSetDocument;

                if (!period.InPeriod(set.payPeriod.toDate.AddDays(-1)))
                    continue;
                double total = 0;
                foreach (PayrollDocument pdoc in set.payrolls)
                {
                    foreach (PayrollComponent com in pdoc.components)
                    {
                        if (com.PCDID == bde.SysPars.IncomeTaxPayrollComponentID && com.FormulaID == bde.SysPars.IncomeTaxPayrollFormulaID)
                        {
                            if (AccountBase.AmountGreater(com.Amount, 0))
                            {
                                total += com.Amount;
                                break;
                            }
                        }
                    }
                    if (!taxCenters.Contains(pdoc.employee.TaxCenter))
                        taxCenters.Add(pdoc.employee.TaxCenter);
                }
                if (AccountBase.AmountGreater(total, 0))
                {
                    ret.Add(set.AccountDocumentID);
                    amount.Add(total);
                }

            }
        }

        private void _GetLaborPaymentCandidates(iERPTransactionBDE bde, AccountDocument[] ldocs, INTAPS.RDBMS.SQLHelper helper, List<int> ret, List<double> amount,List<int> taxCenters)
        {
            foreach (AccountDocument doc in ldocs)
            {
                if ((int)helper.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.{1} where documentID={2} and declarationType={3}", bde.DBName, typeof(DeclaredDocument).Name, doc.AccountDocumentID, bdeAccounting.GetDocumentTypeByType(typeof(IncomeTaxDeclarationDocument)).id)) > 0)
                    continue;
                LaborPaymentDocument ldoc = bdeAccounting.GetAccountDocument(doc.AccountDocumentID, true) as LaborPaymentDocument;
                double tAmount = DHLaborPayment.CalculateIncomeTax(ldoc.amount);
                if (AccountBase.AmountGreater(tAmount, 0))
                {
                    ret.Add(ldoc.AccountDocumentID);
                    amount.Add(tAmount);
                }
                if (!taxCenters.Contains(ldoc.taxCenter))
                    taxCenters.Add(ldoc.taxCenter);
            }
        }

        private void _GetGroupPayrollCandidates(iERPTransactionBDE bde,AccountingPeriod period, AccountDocument[] gPayrollDocs, INTAPS.RDBMS.SQLHelper helper, List<int> ret, List<double> amount,List<int> taxCenters)
        {
            foreach (AccountDocument doc in gPayrollDocs)
            {
                if ((int)helper.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.{1} where documentID={2} and declarationType={3}", bde.DBName, typeof(DeclaredDocument).Name, doc.AccountDocumentID, bdeAccounting.GetDocumentTypeByType(typeof(IncomeTaxDeclarationDocument)).id)) > 0)
                    continue;
                GroupPayrollDocument pDoc = bdeAccounting.GetAccountDocument(doc.AccountDocumentID, true) as GroupPayrollDocument;
                if (!period.InPeriod(bde.Payroll.GetPayPeriod(pDoc.periodID).toDate.AddDays(-1)))
                    continue;
                if (pDoc.declareTax)
                {
                    if (AccountBase.AmountGreater(pDoc.incomeTaxTotalAmount, 0))
                    {
                        ret.Add(pDoc.AccountDocumentID);
                        amount.Add(pDoc.incomeTaxTotalAmount);
                    }
                    if (!taxCenters.Contains(pDoc.taxCenter))
                        taxCenters.Add(pDoc.taxCenter);
                }

            }
        }



        #region ITaxDeclarationServerHandler Members


        public double RetrieveTotalTax(DataSet data)
        {
            IncomeTaxDeclarationData taxData = (IncomeTaxDeclarationData)data;
            double total = 0;
            foreach (TypedDataSets.IncomeTaxDeclarationData.ContinuationRow row in taxData.Continuation)
                total += row.Tax;
            return total;
        }

        #endregion
        protected override List<TransactionOfBatch> DebitTaxLiabilityTransactions(int AID,TaxDeclaration decl, TaxDeclarationDocumentBase doc, TaxDeclarationDocumentAttribute atr, AccountingPeriod period)
        {
            List<TransactionOfBatch> trans = new List<TransactionOfBatch>();
            if (AccountBase.AmountGreater(doc.declaredAmount, 0))
            {
                IncomeTaxDeclarationData data = (IncomeTaxDeclarationData)decl.report;
                foreach (IncomeTaxDeclarationData.ContinuationRow row in data.Continuation)
                {
                    ConnectToiERP();
                    int accountID = -1;
                    bool exists = bdeiERP.EmployeeExists(row.employeeID);
                    if (exists)
                        accountID = bdePayroll.GetEmployeeSubAccount(row.employeeID, bdePayroll.SysPars.staffIncomeTaxAccountID);
                    else
                        accountID = row.employeeID; //Used to handle for the case of LaborPaymentDocument and GroupPayrollPaymentDocument incometax account

                    trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, bdeiERP.SysPars.mainCostCenterID, accountID).id, row.Tax, "Settlment of income tax for employee:" + row.Name + " for " + period.name));

                }
            }
            return trans;
        }
    }
}
