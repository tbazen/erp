using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using BIZNET.iERP.TypedDataSets;
using INTAPS.Accounting;
using INTAPS.Payroll.BDE;
using INTAPS.ClientServer;
using INTAPS.Payroll;
using System.Drawing;

namespace BIZNET.iERP.Server
{
    public class DHPensionTaxDeclaration : DHTaxDeclarationBase<PensionTaxDeclarationDocument>, ITaxDeclarationServerHandler
    {
        PayrollBDE _payRoll = null;
        protected PayrollBDE bdePayroll
        {
            get
            {
                if (_payRoll == null)
                    _payRoll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
                return _payRoll;
            }
        }
        public DHPensionTaxDeclaration(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {

        }
        

        protected override int TaxPenalityAccount
        {
            get
            {
                return bdeiERP.SysPars.taxPenalityAcountID;
            }
        }
        protected override int TaxInterestAccount
        {
            get
            {
                return bdeiERP.SysPars.taxInterestAccountID;
            }
        }
        protected override int TaxDeductionsAccount
        {
            get
            {

                return bdeiERP.SysPars.taxExemptionsAccountID;
            }
        }
        public DataSet BuildReportDataSet(int AID, iERPTransactionBDE bde, DateTime declarationDate, int periodID, int taxCenterID, int[] documents, int[] rejectedDocuments, out double totalTax, bool newDeclaration)
        {
            INTAPS.RDBMS.SQLHelper helper = bdePayroll.GetReaderHelper();
            try
            {
                List<Employee> resignedEmployees = new List<Employee>();
                PensionTaxDeclarationData ret = new PensionTaxDeclarationData();
                AccountingPeriod period = bde.GetAccountingPeriod(bde.SysPars.taxDeclarationPeriods, periodID);
                CompanyProfile prof = bde.SysPars.companyProfile;

                INTAPS.Ethiopic.EtGrDate month = INTAPS.Ethiopic.EtGrDate.ToEth(period.fromDate);
                ret.Summary.AddSummaryRow("",
                    prof.Name,
                 prof.Region, prof.Zone, prof.Woreda, prof.Kebele, prof.HouseNumber, prof.TIN, "", "", prof.Telephone, prof.Fax, month.Month, month.Year, 0, documents.Length, 0, 0, 0, 0, 0, 0, 0, 0
                , periodID, "1");

                int NEmployee = 0;
                int i = 0;
                totalTax = 0;
                foreach (int doc in documents)
                {
                    AccountDocument adoc = bdeAccounting.GetAccountDocument(doc, true);
                    if (adoc is PayrollSetDocument)
                    {
                        foreach (PayrollDocument pay in ((PayrollSetDocument)adoc).payrolls)
                        {
                            if (pay.employee.TaxCenter != taxCenterID)
                                continue;
                            INTAPS.Payroll.Employee emp = pay.employee;
                            if (emp.status == EmployeeStatus.Fired)
                            {
                                resignedEmployees.Add(emp);
                            }
                            INTAPS.Payroll.PayrollComponent compGrossSalary;
                            INTAPS.Payroll.PayrollComponent compPension;
                            compGrossSalary = pay.GetComponent(bde.SysPars.GrossSalaryPayrollComponentID, bde.SysPars.GrossSalaryPayrollFormulaID);
                            compPension = pay.GetComponent(bde.SysPars.EmployeePensionPayrollComponentID, bde.SysPars.EmployeePensionPayrollFormulaID);

                            double pensionEmployee = 0;
                            double pensionEmployer = 0;
                            //CAUTION: using ammount comparision to search for employer contribution
                            if (compPension != null)
                            {
                                pensionEmployee = compPension.Amount;
                                if(AccountBase.AmountGreater(pensionEmployee,0))
                                {
                                foreach (TransactionOfBatch tran in compPension.Transactions)
                                {
                                    //if (bdeAccounting.GetCostCenterAccount(tran.AccountID).accountID == bdePayroll.GetEmployeelPensionContributionExpenseAcount(emp))
                                    int accountID=bdeAccounting.GetCostCenterAccount(tran.AccountID).accountID ;
                                    if(bdeAccounting.isInSameHeirarchy<Account>(bde.SysPars.expenseAccountID,accountID)
                                        ||bdeAccounting.isInSameHeirarchy<Account>(bde.SysPars.costOfSalesAccountID,accountID))
                                      {
                                        pensionEmployer = tran.Amount;
                                        break;
                                    }
                                }
                                }
                            }

                            if (Account.AmountGreater(pensionEmployee + pensionEmployer, 0))
                            {
                                ret.Continuation.AddContinuationRow((i + 1).ToString()
                                , emp.TIN, emp.employeeName
                                , INTAPS.Ethiopic.EtGrDate.ToEth(emp.enrollmentDate).ToString()
                                , emp.grossSalary
                                , pensionEmployee
                                , pensionEmployer
                                , pensionEmployer + pensionEmployee
                                , ret.Summary[0], emp.id);
                                totalTax += pensionEmployer + pensionEmployee;
                                NEmployee++;
                                i++;
                            }
                        }
                    }
                    else if (adoc is GroupPayrollDocument)
                    {
                        GroupPayrollDocument gpDoc = (GroupPayrollDocument)adoc;
                        if (gpDoc.taxCenter != taxCenterID)
                            continue;
                        ret.Continuation.AddContinuationRow((i + 1).ToString()
                       , "", gpDoc.description
                       , INTAPS.Ethiopic.EtGrDate.ToEth(gpDoc.DocumentDate).ToString()
                       , gpDoc.grossSalaryTotalAmount
                       , gpDoc.employeesTotalPensionAmount
                       , gpDoc.employerTotalPensionAmount
                       , gpDoc.employeesTotalPensionAmount + gpDoc.employerTotalPensionAmount
                       , ret.Summary[0], bdePayroll.SysPars.groupPayrollPensionPayableAccount);
                        totalTax += gpDoc.employeesTotalPensionAmount + gpDoc.employerTotalPensionAmount;
                        NEmployee++;
                        i++;
                    }
                }

                ret.Summary[0][ret.Summary.NEmployeeColumn] = NEmployee;
                if (NEmployee == 0)
                    ret = null;
                //Resigned employees
                if (ret != null)
                {
                    foreach (Employee emp in resignedEmployees)
                    {
                        ret.ResignedEmployees.AddResignedEmployeesRow("", emp.TIN, emp.employeeName, ret.Summary[0]);
                    }
                }
                return ret;
            }
            finally
            {
                bdePayroll.ReleaseHelper(helper);
            }
        }
        public int[] GetCandidateDocuments(iERPTransactionBDE bde, DateTime declarationDate, int periodID, out int [] taxCenterID, out double[] taxAmount)
        {
            List<int> taxCenters=new List<int>();
            AccountingPeriod period = bde.GetAccountingPeriod(bde.SysPars.taxDeclarationPeriods, periodID);
            int N;
            AccountDocument[] docs = bdeAccounting.GetAccountDocuments(null, bdeAccounting.GetDocumentTypeByType(typeof(PayrollSetDocument)).id,
                false, period.fromDate, period.toDate, 0, -1, out N);
            AccountDocument[] gPayrollDocs = bdeAccounting.GetAccountDocuments(null, bdeAccounting.GetDocumentTypeByType(typeof(GroupPayrollDocument)).id,
                false, period.fromDate, period.toDate, 0, -1, out N);

            INTAPS.RDBMS.SQLHelper helper = bde.GetReaderHelper();
            try
            {
                
                List<int> ret = new List<int>();
                List<double> amount = new List<double>();
                _GetPensionDocumentCandidates(bde, period,docs, helper, ret, amount,taxCenters);
                _GetGroupPayrollDocumentCandidates(bde, period,gPayrollDocs, helper, ret, amount,taxCenters);
                taxAmount = amount.ToArray();
                taxCenterID = taxCenters.ToArray();
                return ret.ToArray();
            }
            finally
            {
                bde.ReleaseHelper(helper);
            }
        }
        private void _GetPensionDocumentCandidates(iERPTransactionBDE bde,AccountingPeriod period, AccountDocument[] docs, INTAPS.RDBMS.SQLHelper helper, List<int> ret, List<double> amount, List<int> taxCenters)
        {
            foreach (AccountDocument doc in docs)
            {
                if ((int)helper.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.{1} where documentID={2} and declarationType={3}", bde.DBName, typeof(DeclaredDocument).Name, doc.AccountDocumentID, bdeAccounting.GetDocumentTypeByType(typeof(PensionTaxDeclarationDocument)).id)) > 0)
                    continue;
                PayrollSetDocument set = bdeAccounting.GetAccountDocument(doc.AccountDocumentID, true) as PayrollSetDocument;
                if (!period.InPeriod(set.payPeriod.toDate.AddDays(-1)))
                    continue;
                double totalPen = 0;
                foreach (PayrollDocument pdoc in set.payrolls)
                {
                    foreach (PayrollComponent com in pdoc.components)
                    {
                        if ((com.PCDID == bde.SysPars.EmployeePensionPayrollComponentID && com.FormulaID == bde.SysPars.EmployeePensionPayrollFormulaID)
                            || (com.PCDID == bde.SysPars.EmployerPensionPayrollComponentID && com.FormulaID == bde.SysPars.EmployerPensionPayrollFormulaID))
                        {

                            foreach (TransactionOfBatch tran in com.Transactions)
                            {
                                CostCenterAccount trancsa = bdeAccounting.GetCostCenterAccount(tran.AccountID);
                                if (trancsa != null)
                                {
                                    if (bdeAccounting.IsDirectControlOf<Account>(bdePayroll.SysPars.staffPensionPayableAccountID, trancsa.accountID) || trancsa.accountID == bdePayroll.SysPars.staffPensionPayableAccountID)
                                        totalPen += -tran.Amount;
                                }
                            }
                        }
                    }
                    if (!taxCenters.Contains(pdoc.employee.TaxCenter))
                        taxCenters.Add(pdoc.employee.TaxCenter);
                }
                if (AccountBase.AmountGreater(totalPen, 0))
                {
                    ret.Add(doc.AccountDocumentID);
                    amount.Add(totalPen);
                }
            }
        }

        private void _GetGroupPayrollDocumentCandidates(iERPTransactionBDE bde,AccountingPeriod period, AccountDocument[] gPayrollDocs, INTAPS.RDBMS.SQLHelper helper, List<int> ret, List<double> amount, List<int> taxCenters)
        {
            foreach (AccountDocument doc in gPayrollDocs)
            {
                if ((int)helper.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.{1} where documentID={2} and declarationType={3}", bde.DBName, typeof(DeclaredDocument).Name, doc.AccountDocumentID, bdeAccounting.GetDocumentTypeByType(typeof(PensionTaxDeclarationDocument)).id)) > 0)
                    continue;
                GroupPayrollDocument pDoc = bdeAccounting.GetAccountDocument(doc.AccountDocumentID, true) as GroupPayrollDocument;
                if (!period.InPeriod(bde.Payroll.GetPayPeriod(pDoc.periodID).toDate.AddDays(-1)))
                    continue;
                if (pDoc.declareTax)
                {
                    if (AccountBase.AmountGreater(pDoc.employeesTotalPensionAmount + pDoc.employerTotalPensionAmount, 0))
                    {
                        ret.Add(pDoc.AccountDocumentID);
                        amount.Add(pDoc.employeesTotalPensionAmount + pDoc.employerTotalPensionAmount);
                    }
                    if (!taxCenters.Contains(pDoc.taxCenter))
                        taxCenters.Add(pDoc.taxCenter);
                }
            }
        }



        #region ITaxDeclarationServerHandler Members


        public double RetrieveTotalTax(DataSet data)
        {
            PensionTaxDeclarationData taxData = (PensionTaxDeclarationData)data;
            double total = 0;
            foreach (TypedDataSets.PensionTaxDeclarationData.ContinuationRow row in taxData.Continuation)
                total += row.TotalContribution;
            return total;
        }

        #endregion

        protected override int TaxPayableAcount
        {
            get
            {
                return bdePayroll.SysPars.staffPensionPayableAccountID;
            }
        }
        protected override List<TransactionOfBatch> DebitTaxLiabilityTransactions(int AID,TaxDeclaration decl, TaxDeclarationDocumentBase doc, TaxDeclarationDocumentAttribute atr, AccountingPeriod period)
        {
            List<TransactionOfBatch> trans = new List<TransactionOfBatch>();
            if (AccountBase.AmountGreater(doc.declaredAmount, 0))
            {
                PensionTaxDeclarationData data = (PensionTaxDeclarationData)decl.report;
                foreach (PensionTaxDeclarationData.ContinuationRow row in data.Continuation)
                { 
                    int accountID = -1;
                    bool exists = bdeiERP.EmployeeExists(row.employeeID);
                    if (exists)
                        accountID = bdePayroll.GetEmployeeSubAccount(row.employeeID, bdePayroll.SysPars.staffPensionPayableAccountID);
                    else
                        accountID = row.employeeID; //Used to handle for the case of GroupPayrollPaymentDocument pension account
                    if (accountID != bdePayroll.SysPars.staffPensionPayableAccountID)
                        trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, bdeiERP.SysPars.mainCostCenterID, accountID).id, row.TotalContribution, "Settlment of pension for employee:" + row.Name + " for " + period.name));
                    else
                        throw new Exception("Payroll account not found for " + bdePayroll.GetEmployee(row.employeeID).employeeName);
                }
            }
            return trans;
        }
    }

}
