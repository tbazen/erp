using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using System.Data;
using BIZNET.iERP.TypedDataSets;

namespace BIZNET.iERP.Server
{
    interface ITaxDeclarationServerHandler
    {
        DataSet BuildReportDataSet(int AID,iERPTransactionBDE bde, DateTime declarationDate, int periodID, int taxCenterID,int[] documents, int[] rejectedDocuments, out double totalTax,bool newDeclaration);
        int[] GetCandidateDocuments(iERPTransactionBDE bde, DateTime declarationDate, int periodID, out int []taxCenterID, out double[] taxAmount);
        double RetrieveTotalTax(DataSet data);
    }
    public abstract class DHTaxDeclarationBase<DeclarationType> : IDocumentServerHandler
        where DeclarationType: TaxDeclarationDocumentBase
    {
        protected INTAPS.Accounting.BDE.AccountingBDE bdeAccounting;
        protected iERP.Server.iERPTransactionBDE bdeiERP;
        public DHTaxDeclarationBase(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
        {
            this.bdeAccounting = bdeAccounting;
        }
        iERPTransactionBDE _bde = null;
        iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, INTAPS.Accounting.AccountDocument _doc, INTAPS.ClientServer.UserSessionData userSession)
        {
            DeclarationType doc = _doc as DeclarationType;
            ConnectToiERP();
            return bdeiERP.CashierPermissionGranted(userSession);
        }
        public string GetHTML(INTAPS.Accounting.AccountDocument _doc)
        {
            ConnectToiERP();
            DeclarationType doc = _doc as DeclarationType;
            return bdeAccounting.GetDocumentTypeByType(typeof(DeclarationType)).name + " for "
            + bdeiERP.GetAccountingPeriod(TaxDeclarationDocumentBase.GetAttribute(typeof(DeclarationType)).periodType, doc.declarationPeriodID);
        }
        public void DeleteDocument(int AID, int docID)
        {
            ConnectToiERP();
            DeclarationType doc = bdeAccounting.GetAccountDocument(docID, true) as DeclarationType;
            if (doc == null)
                throw new INTAPS.ClientServer.ServerUserMessage("Tax payment document not found in the database");
            lock (bdeAccounting.WriterHelper)
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                try
                {
                    bdeAccounting.DeleteAccountDocument(AID, docID,false);
                    bdeiERP.UndoPayTax<DeclarationType>(doc.declarationID);
                    bdeAccounting.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public void ReverseDocument(int AID,int docID)
        {
            ConnectToiERP();
            DeclarationType doc = bdeAccounting.GetAccountDocument(docID, true) as DeclarationType;
            if (doc == null)
                throw new INTAPS.ClientServer.ServerUserMessage("Tax payment document not found in the database");
            lock (bdeAccounting.WriterHelper)
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                try
                {
                    bdeAccounting.ReverseDocument(AID, docID);
                    bdeiERP.UndoPayTax<DeclarationType>(doc.declarationID);
                    bdeAccounting.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    throw ex;
                }
            }
        }
        protected void ConnectToiERP()
        {
            this.bdeiERP = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
        }

        public virtual int Post(int AID, INTAPS.Accounting.AccountDocument _doc)
        {
            ConnectToiERP();
            TaxDeclarationDocumentAttribute atr = TaxDeclarationDocumentBase.GetAttribute(typeof(DeclarationType));
            DocumentType docType = bdeAccounting.GetDocumentTypeByType(typeof(DeclarationType));
            DeclarationType doc = _doc as DeclarationType;
            if (doc == null)
                throw new INTAPS.ClientServer.ServerUserMessage("Invalid " + atr.name + " document");
            if (doc.AccountDocumentID > 0)
            {
                AccountDocument searchDoc = bdeAccounting.FindFirstDocument(null, docType.id, false, DateTime.MinValue, DateTime.MaxValue,
                new INTAPS.Accounting.BDE.AccountDocumentFilterDelegate(FilterValidDocument), true);
                if (searchDoc != null)
                    throw new INTAPS.ClientServer.ServerUserMessage("Tax declaration for the given period is already registered. You can either modify or delete the existing declaration");
            }

            AccountingPeriod period = bdeiERP.GetAccountingPeriod(atr.periodType, doc.declarationPeriodID);
            TaxDeclaration decl = bdeiERP.GetTaxDeclaration(doc.declarationID);
            doc.declaredAmount = ((ITaxDeclarationServerHandler)this).RetrieveTotalTax(decl.report);

            //validate data
            if (AccountBase.AmountLess(doc.declaredAmount, 0)
            || AccountBase.AmountLess(doc.penality, 0)
            || AccountBase.AmountLess(doc.interest, 0)
            || AccountBase.AmountLess(doc.unaccountedTax, 0)
            || AccountBase.AmountLess(doc.deduction, 0)
            )
                throw new INTAPS.ClientServer.ServerUserMessage("Tax amounts can't be negative");

            double totalPayment = doc.declaredAmount + doc.penality + doc.interest + doc.unaccountedTax - doc.deduction;
            if (AccountBase.AmountLess(totalPayment, 0))
                throw new INTAPS.ClientServer.ServerUserMessage("Total tax payment can't be negative");
            //check account configuration

            CostCenterAccount assetAccount = null;
            if (doc.assetAccountID != -1)
                assetAccount = bdeAccounting.GetCostCenterAccount(doc.assetAccountID);
            INTAPS.RDBMS.SQLHelper dspWriter = bdeAccounting.WriterHelper;
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    int docID = doc.AccountDocumentID;
                    if (docID != -1)
                    {
                        this.DeleteDocument(AID, docID);
                    }

                    List<TransactionOfBatch> trans = new List<TransactionOfBatch>();
                    Dictionary<int, CostCenterAccount> csAccounts = new Dictionary<int, CostCenterAccount>();
                    if (!(doc is VATDeclarationDocument))
                    {
                        if (AccountBase.AmountGreater(totalPayment, 0) && assetAccount != null)
                            trans.Add(new TransactionOfBatch(assetAccount.id, -totalPayment, "Settlement of " + atr.name + " for " + period.name));
                    }
                    List<TransactionOfBatch> dbTran = DebitTaxLiabilityTransactions(AID, decl, doc, atr, period);
                    if (dbTran.Count > 0)
                        trans.AddRange(dbTran);
                    if (AccountBase.AmountGreater(doc.penality, 0))
                    {
                        trans.Add(new TransactionOfBatch(GetCostCenterAccount(AID, csAccounts, this.TaxPenalityAccount).id, doc.penality, "Penality:" + atr.name + " for " + period.name));
                    }

                    if (AccountBase.AmountGreater(doc.interest, 0))
                    {
                        trans.Add(new TransactionOfBatch(GetCostCenterAccount(AID, csAccounts, this.TaxInterestAccount).id, doc.interest, "Interset:" + atr.name + " for " + period.name));
                    }

                    if (AccountBase.AmountGreater(doc.unaccountedTax, 0))
                    {
                        trans.Add(new TransactionOfBatch(GetCostCenterAccount(AID, csAccounts, this.UnaccountedTaxAccount).id, doc.unaccountedTax, "Unaccounted tax:" + atr.name + " for " + period.name));
                    }

                    if (AccountBase.AmountGreater(doc.deduction, 0))
                    {
                        trans.Add(new TransactionOfBatch(GetCostCenterAccount(AID, csAccounts, this.TaxDeductionsAccount).id, -doc.deduction, "Deduction :" + atr.name + " for " + period.name));
                    }
                    if (doc.serviceChargePayer == ServiceChargePayer.Company && !AccountBase.AmountEqual(doc.serviceChargeAmount, 0))
                    {
                        trans.Add(new TransactionOfBatch(assetAccount.id, -doc.serviceChargeAmount, "bank service charge"));
                        CostCenterAccount csaServiceCharge;
                        if (PaymentMethodHelper.IsPaymentMethodBank(doc.paymentMethod))
                            csaServiceCharge = bdeAccounting.GetCostCenterAccount(bde.GetBankAccount(doc.assetAccountID).bankServiceChargeCsAccountID);
                        else
                            csaServiceCharge = bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.bankServiceChargeByCash);
                        if (csaServiceCharge == null)
                            throw new ServerUserMessage("Invalid service charege account");
                        trans.Add(new TransactionOfBatch(csaServiceCharge.id, doc.serviceChargeAmount, "bank service charge"));
                    }

                    DocumentTypedReference[] typedRefs = doc.voucher == null ? new DocumentTypedReference[0] :
    new DocumentTypedReference[] { doc.voucher };
                    docID = bdeAccounting.RecordTransaction(AID, doc, trans.ToArray(), typedRefs);
                    _doc.AccountDocumentID = docID;
                    bdeiERP.PayTax<DeclarationType>(doc);
                    //Check for negative balance
                    if (!bde.AllowNegativeTransactionPost())
                        bdeAccounting.validateNegativeBalance
                            (doc.assetAccountID, 0, false, doc.DocumentDate
                            , "Transaction is blocked because there is no sufficient balance in the selected payment account");
                    dspWriter.CommitTransaction();
                    return docID;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        protected virtual List<TransactionOfBatch> DebitTaxLiabilityTransactions(int AID,TaxDeclaration decl, TaxDeclarationDocumentBase doc, TaxDeclarationDocumentAttribute atr, AccountingPeriod period)
        {
            Account taxAccount = bdeAccounting.GetAccount<Account>(this.TaxPayableAcount);
            List<TransactionOfBatch> trans = new List<TransactionOfBatch>();
            if (AccountBase.AmountGreater(doc.declaredAmount, 0))
                trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, bdeiERP.SysPars.mainCostCenterID, taxAccount.id).id, doc.declaredAmount, "Settlment of declared amount:" + atr.name + " for " + period.name));

            return trans;

        }
       
        int searchFor = -1;
        bool FilterValidDocument(AccountDocument _doc)
        {
            DeclarationType doc = _doc as DeclarationType;
            if (doc == null)
                throw new INTAPS.ClientServer.ServerUserMessage("Invalid document found while searching for tax declarations");
            return doc.declarationPeriodID == searchFor;
        }
        protected abstract int TaxPayableAcount { get;}
        protected abstract int TaxPenalityAccount { get;}
        protected abstract int TaxInterestAccount { get; }
        protected virtual int UnaccountedTaxAccount
        {
            get
            {
                return this.TaxPenalityAccount;
            }
        }
        protected abstract int TaxDeductionsAccount { get;}
        #endregion

        private CostCenterAccount GetCostCenterAccount(int AID,Dictionary<int, CostCenterAccount> csAccounts, int accountID)
        {
            if (csAccounts.ContainsKey(accountID))
                return csAccounts[accountID];
            CostCenterAccount cs = bdeAccounting.GetOrCreateCostCenterAccount(AID, bdeiERP.SysPars.mainCostCenterID, accountID);
            csAccounts.Add(accountID, cs);
            return cs;
        }
    }

}
