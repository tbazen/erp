using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using BIZNET.iERP.TypedDataSets;
using INTAPS.Accounting;
using System.Globalization;
using System.Xml.Serialization;

namespace BIZNET.iERP.Server
{
    public class DHVATDeclaration : DHTaxDeclarationBase<VATDeclarationDocument>, ITaxDeclarationServerHandler
    {
        public DHVATDeclaration(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {

        }
        protected override int TaxPayableAcount
        {
            get
            {
                return bdeiERP.SysPars.outPutVATAccountID;
            }
        }

        protected override int TaxInterestAccount
        {
            get
            {
                return bdeiERP.SysPars.taxInterestAccountID;
            }
        }
        protected override int TaxPenalityAccount
        {
            get
            {
                return bdeiERP.SysPars.taxPenalityAcountID;
            }
        }
        protected override int TaxDeductionsAccount
        {
            get
            {

                return bdeiERP.SysPars.taxExemptionsAccountID;
            }
        }
        public override int Post(int AID, AccountDocument _doc)
        {
            //VATDeclarationDocument doc = (VATDeclarationDocument)_doc;
            //TaxDeclaration declaration = bdeiERP.GetTaxDeclaration(doc.declarationID);
            //VATDeclarationData data = (VATDeclarationData)declaration.report;
            int docID = base.Post(AID, _doc);
            return docID;

        }
        protected override List<TransactionOfBatch> DebitTaxLiabilityTransactions(int AID, TaxDeclaration decl, TaxDeclarationDocumentBase doc, TaxDeclarationDocumentAttribute atr, AccountingPeriod period)
        {
            ConnectToiERP();
            Account taxAccount = bdeAccounting.GetAccount<Account>(this.TaxPayableAcount);
            List<TransactionOfBatch> trans = new List<TransactionOfBatch>();
            //if (AccountBase.AmountGreater(doc.declaredAmount, 0))
            //    trans.Add(new TransactionOfBatch(bdeAccounting.GetCostCenterAccount(bdeiERP.SysPars.mainCostCenterID, taxAccount.id).id, doc.declaredAmount, String.Format("Settlment of declared amount:{0} for {1}", atr.name, period.name)));

            VATDeclarationDocument vatDoc = (VATDeclarationDocument)doc;
            BIZNET.iERP.TypedDataSets.VATDeclarationData data = (BIZNET.iERP.TypedDataSets.VATDeclarationData)bdeiERP.GetTaxDeclaration(decl.id).report;
            double outPutVAT, inputVAT, otherCredit, accumulatedCredit, withheldVAT;
            outPutVAT = inputVAT = otherCredit = accumulatedCredit = withheldVAT = 0;
            double netPayment = 0;
            if (!data.OutputTaxComputation[0].IsVATOnLocalSalesNull())
                outPutVAT = data.OutputTaxComputation[0].VATOnLocalSales;
            if (!data.InputTaxComputation[0].IsTotalTaxValueNull())
                inputVAT = data.InputTaxComputation[0].TotalTaxValue;
            if (!data.OutputTaxComputation[0].IsGovVATOnLocalSalesNull())
            {
                withheldVAT = data.OutputTaxComputation[0].GovVATOnLocalSales;
                outPutVAT += data.OutputTaxComputation[0].GovVATOnLocalSales;
            }
            if (!data.InputTaxComputation[0].IsOtherCreditNull())
                otherCredit = data.InputTaxComputation[0].OtherCredit;
            if (!data.InputTaxComputation[0].IsPreviousMonthCreditNull())
                accumulatedCredit = data.InputTaxComputation[0].PreviousMonthCredit;

            //NetPayment=balance(outputVAT) - balance(inputVAT) - balance(accumulatedCredit) - balance(withheldVAT);
            netPayment = outPutVAT - inputVAT - otherCredit - accumulatedCredit - withheldVAT;
            if (AccountBase.AmountGreater(netPayment, 0))
            {
                // debit outputVAT with balance(outputVAT)
                trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, bdeiERP.SysPars.mainCostCenterID, bdeiERP.SysPars.outPutVATAccountID).id
                    , outPutVAT, "Local sales VAT for period" + period.name + " on VAT declaration confirmation with payment"));

                // credit inputVAT with balance(inputVAT)
                trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, bdeiERP.SysPars.mainCostCenterID, bdeiERP.SysPars.inputVATAccountID).id
                  , -inputVAT, "Local purchase VAT for period" + period.name + " on VAT declaration confirmation with payment"));

                // credit accumulatedCrdiet with balance(accumulatedCredit)
                trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, bdeiERP.SysPars.mainCostCenterID, bdeiERP.SysPars.accumulatedReceivableVATAccountID).id
                  , -accumulatedCredit, "VAT accumulated credit for period" + period.name + " on VAT declaration confirmation with payment"));

                //credit withheldVAT with balance(withheldVAT)
                trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, bdeiERP.SysPars.mainCostCenterID, bdeiERP.SysPars.outputVATWithHeldAccountID).id
                 , -withheldVAT, "Withheld VAT for period" + period.name + " on VAT declaration confirmation with payment"));

                //credit asset with NetPayment
                trans.Add(new TransactionOfBatch(vatDoc.assetAccountID
                 , -netPayment, "VAT Net Payment for period" + period.name + " on VAT declaration confirmation with payment"));

            }
            else
            {
                //debit outputVAT with balance(outputVAT)
                trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, bdeiERP.SysPars.mainCostCenterID, bdeiERP.SysPars.outPutVATAccountID).id
                 , outPutVAT, "Local sales VAT for period" + period.name + " on VAT declaration confirmation with no payment"));

                // credit inputVAT with balance(inputVAT)
                trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, bdeiERP.SysPars.mainCostCenterID, bdeiERP.SysPars.inputVATAccountID).id
                  , -inputVAT, "Local purchase VAT for period" + period.name + " on VAT declaration confirmation with no payment"));

                // debit accumulatedCredit with balance(inputVAT)+balance(withheldVAT)-balance(outputVAT)
                trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, bdeiERP.SysPars.mainCostCenterID, bdeiERP.SysPars.accumulatedReceivableVATAccountID).id
                  , inputVAT + otherCredit + withheldVAT - outPutVAT, "VAT accumulated credit for period" + period.name + " on VAT declaration confirmation with no payment"));
                if (!Account.AmountEqual(withheldVAT, 0))
                {
                    //credit withheldVAT with balance(withheldVAT)
                    trans.Add(new TransactionOfBatch(bdeAccounting.GetOrCreateCostCenterAccount(AID, bdeiERP.SysPars.mainCostCenterID, bdeiERP.SysPars.outputVATWithHeldAccountID).id
                     , -withheldVAT, "Withheld VAT for period" + period.name + " on VAT declaration confirmation with no payment"));
                }
            }
            return trans;

            #region commented lines
            //NetPayment=balance(outputVAT) - balance(inputVAT) - balance(accumulatedCredit) - balance(withheldVAT);
            //if NetPayment>0 then
            //    debit outputVAT with balance(outputVAT)
            //    credit inputVAT with balance(inputVAT)
            //    credit accumulatedCrdiet with balance(accumulatedCredit)
            //    credit withheldVAT with balance(withheldVAT)
            //    credit asset with NetPayment
            //else
            //    debit outputVAT with balance(outputVAT)
            //    credit inputVAT with balance(inputVAT)
            //    debit accumulatedCredit with balance(inputVAT)+balance(withheldVAT)-balance(outputVAT)
            //    credit withheldVAT with balance(withheldVAT)
            //end if


            #endregion
        }
        DocumentTypedReference getRef(AccountDocument doc)
        {
            if (doc is Purchase2Document)
                return ((Purchase2Document)doc).paymentVoucher;
            if (doc is BIZNET.iERP.Sell2Document)
                return ((Sell2Document)doc).salesVoucher;
            if (doc is BIZNET.iERP.CustomerAdvancePayment2Document)
                return ((CustomerAdvancePayment2Document)doc).salesVoucher;
            if (doc is BIZNET.iERP.SupplierAdvancePayment2Document)
                return ((SupplierAdvancePayment2Document)doc).paymentVoucher;
            return null;
        }
        int compareDocuments(AccountDocument doc1, AccountDocument doc2)
        {
            DocumentTypedReference r1 = getRef(doc1);
            DocumentTypedReference r2 = getRef(doc2);
            if (r1 == null && r2 == null)
                return doc1.DocumentDate.CompareTo(doc2.DocumentDate);
            if (r1 != null && r2 == null)
                return 1;
            if (r1 == null && r2 != null)
                return -1;
            if (r1.typeID != r2.typeID)
            {
                return r1.typeID.CompareTo(r2.typeID);
            }
            return r1.reference.CompareTo(r2.reference);
        }
        public DataSet BuildReportDataSet(int AID, iERPTransactionBDE bde, DateTime declarationDate, int periodID, int taxCenterID, int[] documents, int[] rejectedDocuments, out double totalTax, bool newDeclaration)
        {
            VATDeclarationData ret = new VATDeclarationData();

            AccountingPeriod period = bde.GetAccountingPeriod(bde.SysPars.taxDeclarationPeriods, periodID);

            CompanyProfile prof = bde.SysPars.companyProfile;
            INTAPS.Ethiopic.EtGrDate month = INTAPS.Ethiopic.EtGrDate.ToEth(period.fromDate);

            string monthEnglish = GetEnglishMonthName(period.fromDate.Month);
            string monthAmharic = GetAmharicMonthName(month.Month);
            bool isNeal = false;
            ret.Summary.AddSummaryRow("", prof.AmharicName, prof.Name, prof.Occupation
            , prof.Telephone, prof.MobilePhone, prof.TIN, prof.VATNumber
            , monthAmharic, month.Year, monthEnglish, isNeal, periodID);

            totalTax = 0;
            double taxableLocalPurchaseBase = 0;
            double taxableGeneralExpenseBase = 0;
            double taxableUnclaimedExpenseBase = 0;
            double taxableImportedPurchaseBase = 0;
            double taxableLocalSalesBase = 0;
            double taxableExportedBase = 0;
            double remittedValue = 0;
            double VATChargeForCurrentPeriod = 0; //sales VAT
            double salesNonTaxable = 0;
            double purchaseNonTaxable = 0;
            double localSalesVAT = 0;
            double VATAgentCustomerVATTotal = 0;
            double totalSalesValue = 0;

            double localPurchaseVAT = 0;
            double importPurchaseVAT = 0;
            double generalExpenseVAT = 0;
            double totalPurchaseValue = 0;
            double totalTaxablePurchaseValue = 0;
            double totalPurchaseTaxValue = 0;
            double totalPayableAmount = 0;
            double otherCredits = 0;
            double accumulatedCredit = 0;
            double creditCarryForward = 0;
            double netVAT = 0;
            double creditVAT = 0;
            if (documents != null)
            {
                List<AccountDocument> fullDocuments = new List<AccountDocument>();
                for (int i = 0; i < documents.Length; i++)
                {
                    AccountDocument doc = bdeAccounting.GetAccountDocument(documents[i], true);
                    fullDocuments.Add(doc);
                }
                fullDocuments.Sort(new Comparison<AccountDocument>(compareDocuments));

                for (int i = 0; i < documents.Length; i++)
                {
                    //AccountDocument doc = bdeAccounting.GetAccountDocument(documents[i], true);
                    AccountDocument doc = fullDocuments[i];

                    DocumentType docType = bdeAccounting.GetDocumentTypeByID(doc.DocumentTypeID);
                    bool rejected = IsDocumentRejected(doc.AccountDocumentID, rejectedDocuments);

                    if (docType.GetTypeObject() == typeof(Sell2Document)
                        || docType.GetTypeObject() == typeof(CustomerAdvancePayment2Document)) //Output Tax Computation
                    {
                        ComputeOutPutTax(rejected, bde, prof, doc, ret, ref taxableLocalSalesBase, ref taxableExportedBase
                            , ref salesNonTaxable, ref remittedValue, ref localSalesVAT
                            , ref VATAgentCustomerVATTotal, i + 1);
                    }
                    else if (docType.GetTypeObject() == typeof(Purchase2Document)
                        || docType.GetTypeObject() == typeof(SupplierAdvancePayment2Document)
                        || docType.GetTypeObject() == typeof(PurchaseInvoiceDocument)
                        ) //Input Tax for purchase
                    {

                        ComputeInputTax(rejected, bde, doc, ref ret, ref taxableLocalPurchaseBase
                            , ref taxableGeneralExpenseBase, ref taxableImportedPurchaseBase
                            , ref localPurchaseVAT, ref importPurchaseVAT
                            , ref generalExpenseVAT
                            , ref purchaseNonTaxable, ref taxableUnclaimedExpenseBase, i + 1);
                    }
                    else if (docType.GetTypeObject() == typeof(ZReportDocument))
                    {
                        ComputeZReportTax(rejected, bde, prof, doc, ret, ref taxableLocalSalesBase
                            , ref salesNonTaxable, ref localSalesVAT);
                    }
                    else if (docType.GetTypeObject() == typeof(AdjustmentDocument))
                    {
                        AdjustmentDocument adjustmentDoc = doc as AdjustmentDocument;
                        if (adjustmentDoc == null)
                            throw new INTAPS.ClientServer.ServerUserMessage("Document is not an adjustment document");
                        if (!rejected)
                        {
                            AccountTransaction[] transactions = bdeAccounting.GetTransactionsOfDocument(doc.AccountDocumentID);
                            foreach (AccountTransaction tran in transactions)
                            {
                                if (tran.AccountID == bde.SysPars.inputVATAccountID)
                                {
                                    if (AccountBase.AmountGreater(tran.Amount, 0))
                                    {
                                        otherCredits += tran.Amount;
                                    }
                                }
                            }
                        }

                    }

                }
            }
            //int nPages;
            // double totalTOTpurchaseAmount = _GetTOTPurchaseTotalAmount(bde, period, out nPages);
            //localSalesVAT = localSalesVAT - VATAgentCustomerVATTotal;

            //  VATChargeForCurrentPeriod = localSalesVAT;
            VATChargeForCurrentPeriod = localSalesVAT + VATAgentCustomerVATTotal; //Note:government vat should not be accounted. This is only for the purpose of displaying
            purchaseNonTaxable = purchaseNonTaxable + taxableUnclaimedExpenseBase;
            totalSalesValue = taxableLocalSalesBase + taxableExportedBase + salesNonTaxable + remittedValue;

            netVAT = localSalesVAT - (localPurchaseVAT + importPurchaseVAT + generalExpenseVAT); //Note:Government VAT is not considered for netVAT(it's zero). It has value only for the purpose of displaying

            double vatCreditForMonth = (localPurchaseVAT + importPurchaseVAT + generalExpenseVAT) - localSalesVAT;
            if (!AccountBase.AmountEqual(VATAgentCustomerVATTotal, 0) && AccountBase.AmountEqual(localSalesVAT, 0))
            {
                vatCreditForMonth = 0;
            }

            creditVAT = vatCreditForMonth;//Note:Government VAT is not considered for netVAT(it's zero). It has value only for the purpose of displaying
            totalPurchaseValue = taxableLocalPurchaseBase + taxableImportedPurchaseBase + taxableGeneralExpenseBase + purchaseNonTaxable;
            totalTaxablePurchaseValue = taxableLocalPurchaseBase + taxableImportedPurchaseBase + taxableGeneralExpenseBase;
            totalPurchaseTaxValue = localPurchaseVAT + importPurchaseVAT + generalExpenseVAT;
            accumulatedCredit = GetAccumulatedCreditAmount(AID, bde, declarationDate, newDeclaration);
            creditCarryForward = (localPurchaseVAT + importPurchaseVAT + generalExpenseVAT) - localSalesVAT + otherCredits + accumulatedCredit;
            totalPayableAmount = netVAT - otherCredits - accumulatedCredit;
            UpdateNegativeValuesToZero(ref netVAT, ref creditVAT, ref otherCredits, ref totalPayableAmount, ref creditCarryForward);
            totalTax = totalPayableAmount;
            if (AccountBase.AmountEqual(netVAT - otherCredits, 0) && AccountBase.AmountEqual((localPurchaseVAT + importPurchaseVAT + generalExpenseVAT) - localSalesVAT + otherCredits, 0))
                isNeal = true;
            ret.Summary[0][ret.Summary.IsNealDeclarationColumn] = isNeal;

            ret.OutputTaxComputation.AddOutputTaxComputationRow(taxableLocalSalesBase, "", ""
                , localSalesVAT, "", "", VATAgentCustomerVATTotal, "", "", taxableExportedBase, "", ""
                , salesNonTaxable, "", "", remittedValue, "", "", totalSalesValue
                , "", "", VATChargeForCurrentPeriod, "", "", ret.Summary[0]);

            ret.InputTaxComputation.AddInputTaxComputationRow(taxableLocalPurchaseBase
           , "", "", localPurchaseVAT, "", "", taxableImportedPurchaseBase, "", "", importPurchaseVAT
           , "", "", taxableGeneralExpenseBase, "", "", generalExpenseVAT, "", "", purchaseNonTaxable, "", ""
           , totalPurchaseValue
           , "", "", totalTaxablePurchaseValue, "", ""
           , totalPurchaseTaxValue, "", "", netVAT
            , "", "", creditVAT, "", "", otherCredits, "", "", accumulatedCredit, "", "", totalPayableAmount, "", "", creditCarryForward, "", "", ret.Summary[0]);

            return ret;
        }


        private void UpdateNegativeValuesToZero(ref double netVAT, ref double creditVAT
            , ref double otherCredits, ref double totalPayableAmount, ref double creditCarryForward)
        {
            if (AccountBase.AmountLess(netVAT, 0))
            {
                netVAT = 0;
            }
            if (AccountBase.AmountLess(creditVAT, 0))
            {
                creditVAT = 0;
            }
            if (AccountBase.AmountLess(otherCredits, 0))
            {
                otherCredits = 0;
            }
            if (AccountBase.AmountLess(totalPayableAmount, 0))
            {
                totalPayableAmount = 0;
            }
            if (AccountBase.AmountLess(creditCarryForward, 0))
            {
                creditCarryForward = 0;
            }
        }

        private void ComputeInputTax(bool rejected, iERPTransactionBDE bde, AccountDocument doc, ref VATDeclarationData data
            , ref double taxableLocalPurchaseBase, ref double taxableGeneralExpenseBase
            , ref double taxableImportedPurchaseBase, ref double localPurchaseVAT
            , ref double importPurchaseVAT
            , ref double generalExpenseVAT
            , ref double purchaseNonTaxable, ref double taxableUnclaimedExpenseBase, int seqNo)
        {
            double documentGoodPurchaseValue = 0;
            double documentGoodVATValue = 0;
            double documentGoodGenExpValue = 0;
            double documentGoodGenExpVATValue = 0;

            double documentServicePurchaseValue = 0;
            double documentServiceVATValue = 0;
            double documentServiceGenExpValue = 0;
            double documentServiceGenExpVATValue = 0;

            GoodOrService goodOrService = GoodOrService.Good;
            bool mixedType = false;
            string relationCode;
            DocumentTypedReference voucher;
            if (doc is Purchase2Document)
            {
                Purchase2Document pd = doc as Purchase2Document;
                relationCode = pd.relationCode;
                voucher = pd.paymentVoucher;
            }
            else if (doc is PurchaseInvoiceDocument)
            {
                PurchaseInvoiceDocument invoice = doc as PurchaseInvoiceDocument;
                Purchase2Document pd = (Purchase2Document)bde.Accounting.GetAccountDocument(invoice.purchaseTransactionDocumentID, true);
                voucher = pd.paymentVoucher;
                relationCode = pd.relationCode;
            }
            else
                throw new INTAPS.ClientServer.ServerUserMessage("Unexpected document type for input VAT " + doc.GetType());
            if (!rejected)
            {
                TradeRelation supplier = bde.GetSupplier(relationCode);

                if (!string.IsNullOrEmpty(supplier.TIN))
                {
                    ComputePurchaseForSuppliersWithTIN(bde, data, ref taxableLocalPurchaseBase
                        , ref taxableGeneralExpenseBase, ref localPurchaseVAT
                        , ref generalExpenseVAT, ref purchaseNonTaxable
                        , ref taxableUnclaimedExpenseBase, seqNo, ref documentGoodPurchaseValue
                        , ref documentGoodVATValue
                        , ref documentServicePurchaseValue
                        , ref documentServiceVATValue
                        , goodOrService, mixedType, new PurchaseTaxInfo(doc), voucher, supplier);
                }
                else // suppliers with no TIN
                {
                    double noTINGoodPurchaseValue = 0;
                    double noTINGoodVATValue = 0;

                    double noTINServicePurchaseValue = 0;
                    double noTINServiceVATValue = 0;
                    ComputePurchaseForSuppliersWithNoTIN(bde, data, ref noTINGoodPurchaseValue, ref noTINGoodVATValue
                        , ref purchaseNonTaxable, seqNo, ref noTINServicePurchaseValue, ref noTINServiceVATValue
                        , goodOrService, mixedType, new PurchaseTaxInfo(doc), voucher, supplier);
                }
            }
        }

        private void ComputePurchaseForSuppliersWithTIN(iERPTransactionBDE bde, VATDeclarationData data
            , ref double taxableLocalPurchaseBase, ref double taxableGeneralExpenseBase
            , ref double localPurchaseVAT, ref double generalExpenseVAT
            , ref double purchaseNonTaxable, ref double taxableUnclaimedExpenseBase
            , int seqNo, ref double documentGoodPurchaseValue
            , ref double documentGoodVATValue
            , ref double documentServicePurchaseValue
            , ref double documentServiceVATValue
            , GoodOrService goodOrService
            , bool mixedType, PurchaseTaxInfo pd, DocumentTypedReference voucher, TradeRelation supplier)
        {
            pd.taxRates = pd.taxRates == null ? bde.getTaxRates() : pd.taxRates;
            double totalTaxableBase = GetTotalTaxableBase(pd.items);
            double withholdingBaseGood = 0;
            double withholdingBaseService = 0;

            int i = 0;
            foreach (TransactionDocumentItem item in pd.items)
            {
                TransactionItems titem = bde.GetTransactionItems(item.code);

                if (titem.IsFixedAssetItem)
                {
                    TransactionItems fixedAsset = titem;
                    if (i == 0)
                        goodOrService = GoodOrService.Good;
                    else
                    {
                        if (goodOrService != GoodOrService.Good)
                            mixedType = true;
                    }
                    if (fixedAsset.TaxStatus == ItemTaxStatus.NonTaxable)
                    {
                        purchaseNonTaxable += Math.Round(item.quantity * item.unitPrice, 2);
                        documentGoodPurchaseValue += Math.Round(item.quantity * item.unitPrice, 2);
                    }
                    else //TOT or VAT
                    {

                        if (supplier.TaxRegistrationType == TaxRegisrationType.VAT)
                        {
                            double vatBase = Account.AmountLess(pd.manualVATBase, 0) ? Math.Round(item.quantity * item.unitPrice, 2) : Math.Round(item.quantity * item.unitPrice, 2) * (pd.manualVATBase / totalTaxableBase);
                            taxableLocalPurchaseBase += vatBase;
                            localPurchaseVAT += vatBase * pd.taxRates.CommonVATRate;
                            documentGoodPurchaseValue += vatBase;
                            withholdingBaseGood += Math.Round(item.quantity * item.unitPrice, 2);
                            documentGoodVATValue += vatBase * pd.taxRates.CommonVATRate;
                        }
                        else if (supplier.TaxRegistrationType == TaxRegisrationType.TOT)
                        {
                            double totAmount = Math.Round(item.quantity * item.unitPrice, 2) * pd.taxRates.GoodsTaxableRate;
                            purchaseNonTaxable += Math.Round(item.quantity * item.unitPrice, 2) + totAmount;
                            documentGoodPurchaseValue += Math.Round(item.quantity * item.unitPrice, 2) + totAmount;
                            withholdingBaseGood += Math.Round(item.quantity * item.unitPrice, 2);
                        }
                    }
                }
                else if (titem.IsExpenseItem)
                {
                    TransactionItems expenseItem = titem;

                    if (i == 0)
                        goodOrService = expenseItem.GoodOrService;
                    else
                    {
                        if (goodOrService != expenseItem.GoodOrService)
                            mixedType = true;
                    }
                    if (expenseItem.TaxStatus == ItemTaxStatus.NonTaxable)
                    {
                        purchaseNonTaxable += Math.Round(item.quantity * item.unitPrice);
                        if (expenseItem.GoodOrService == GoodOrService.Good)
                            documentGoodPurchaseValue += Math.Round(item.quantity * item.unitPrice, 2);
                        else
                            documentServicePurchaseValue += Math.Round(item.quantity * item.unitPrice, 2);
                    }
                    else
                    {
                        if (expenseItem.ExpenseType == ExpenseType.PurchaseExpense)
                        {

                            if (supplier.TaxRegistrationType == TaxRegisrationType.VAT)
                            {
                                double vatBase = Account.AmountLess(pd.manualVATBase, 0) ? Math.Round(item.quantity * item.unitPrice, 2) : Math.Round(item.quantity * item.unitPrice, 2) * (pd.manualVATBase / totalTaxableBase);
                                taxableLocalPurchaseBase += vatBase;
                                localPurchaseVAT += vatBase * pd.taxRates.CommonVATRate;
                            }

                            if (expenseItem.GoodOrService == GoodOrService.Good)
                            {

                                if (supplier.TaxRegistrationType == TaxRegisrationType.VAT)
                                {
                                    double vatBase = Account.AmountLess(pd.manualVATBase, 0) ? Math.Round(item.quantity * item.unitPrice, 2) : Math.Round(item.quantity * item.unitPrice, 2) * (pd.manualVATBase / totalTaxableBase);
                                    documentGoodPurchaseValue += vatBase;
                                    withholdingBaseGood += Math.Round(item.quantity * item.unitPrice, 2);
                                    documentGoodVATValue += vatBase * pd.taxRates.CommonVATRate;
                                }
                                else if (supplier.TaxRegistrationType == TaxRegisrationType.TOT)
                                {
                                    double totAmount = Math.Round(item.quantity * item.unitPrice, 2) * pd.taxRates.GoodsTaxableRate;
                                    purchaseNonTaxable += Math.Round(item.quantity * item.unitPrice, 2) + totAmount;
                                    documentGoodPurchaseValue += Math.Round(item.quantity * item.unitPrice, 2) + totAmount;
                                    withholdingBaseGood += Math.Round(item.quantity * item.unitPrice, 2);
                                }
                            }
                            else
                            {

                                if (supplier.TaxRegistrationType == TaxRegisrationType.VAT)
                                {
                                    double vatBase = Account.AmountLess(pd.manualVATBase, 0) ? Math.Round(item.quantity * item.unitPrice, 2) : Math.Round(item.quantity * item.unitPrice, 2) * (pd.manualVATBase / totalTaxableBase);
                                    documentServicePurchaseValue += vatBase;
                                    withholdingBaseService += Math.Round(item.quantity * item.unitPrice, 2);
                                    documentServiceVATValue += vatBase * pd.taxRates.CommonVATRate;
                                }
                                else if (supplier.TaxRegistrationType == TaxRegisrationType.TOT)
                                {
                                    double serviceTOTRate = Purchase2Document.GetTOTCalculationRate(goodOrService, titem.ServiceType, pd.taxRates);
                                    double totAmount = Math.Round(item.quantity * item.unitPrice, 2) * serviceTOTRate;
                                    purchaseNonTaxable += Math.Round(item.quantity * item.unitPrice, 2) + totAmount;
                                    documentServicePurchaseValue += Math.Round(item.quantity * item.unitPrice, 2) + totAmount;
                                    withholdingBaseService += Math.Round(item.quantity * item.unitPrice, 2);
                                }
                            }

                        }
                        else if (expenseItem.ExpenseType == ExpenseType.GeneralExpense)
                        {

                            if (supplier.TaxRegistrationType == TaxRegisrationType.VAT)
                            {
                                double vatBase = Account.AmountLess(pd.manualVATBase, 0) ? Math.Round(item.quantity * item.unitPrice, 2) : Math.Round(item.quantity * item.unitPrice, 2) * (pd.manualVATBase / totalTaxableBase);
                                taxableGeneralExpenseBase += vatBase;
                                generalExpenseVAT += vatBase * pd.taxRates.CommonVATRate;
                            }
                            if (expenseItem.GoodOrService == GoodOrService.Good)
                            {

                                if (supplier.TaxRegistrationType == TaxRegisrationType.VAT)
                                {
                                    double vatBase = Account.AmountLess(pd.manualVATBase, 0) ? Math.Round(item.quantity * item.unitPrice, 2) : Math.Round(item.quantity * item.unitPrice, 2) * (pd.manualVATBase / totalTaxableBase);
                                    documentGoodPurchaseValue += vatBase;
                                    withholdingBaseGood += Math.Round(item.quantity * item.unitPrice, 2);
                                    documentGoodVATValue += vatBase * pd.taxRates.CommonVATRate;
                                }
                                else if (supplier.TaxRegistrationType == TaxRegisrationType.TOT)
                                {
                                    double totAmount = Math.Round(item.quantity * item.unitPrice, 2) * pd.taxRates.GoodsTaxableRate;
                                    purchaseNonTaxable += Math.Round(item.quantity * item.unitPrice, 2) + totAmount;
                                    documentGoodPurchaseValue += Math.Round(item.quantity * item.unitPrice, 2) + totAmount;
                                    withholdingBaseGood += Math.Round(item.quantity * item.unitPrice, 2);
                                }
                            }
                            else
                            {

                                if (supplier.TaxRegistrationType == TaxRegisrationType.VAT)
                                {
                                    double vatBase = Account.AmountLess(pd.manualVATBase, 0) ? Math.Round(item.quantity * item.unitPrice, 2) : Math.Round(item.quantity * item.unitPrice, 2) * (pd.manualVATBase / totalTaxableBase);
                                    documentServicePurchaseValue += vatBase;
                                    withholdingBaseService += Math.Round(item.quantity * item.unitPrice, 2);
                                    documentServiceVATValue += vatBase * pd.taxRates.CommonVATRate;
                                }
                                else if (supplier.TaxRegistrationType == TaxRegisrationType.TOT)
                                {
                                    double serviceTOTRate = Purchase2Document.GetTOTCalculationRate(goodOrService, titem.ServiceType, pd.taxRates);
                                    double totAmount = Math.Round(item.quantity * item.unitPrice, 2) * serviceTOTRate;
                                    purchaseNonTaxable += Math.Round(item.quantity * item.unitPrice, 2) + totAmount;
                                    documentServicePurchaseValue += Math.Round(item.quantity * item.unitPrice, 2) + totAmount;
                                    withholdingBaseService += Math.Round(item.quantity * item.unitPrice, 2);
                                }
                            }

                        }
                        else //Unclaimed Expense
                        {
                            if (supplier.TaxRegistrationType == TaxRegisrationType.VAT)
                            {
                                double vatBase = Account.AmountLess(pd.manualVATBase, 0) ? Math.Round(item.quantity * item.unitPrice, 2) : Math.Round(item.quantity * item.unitPrice, 2) * (pd.manualVATBase / totalTaxableBase);
                                double vat = vatBase * pd.taxRates.CommonVATRate;
                                taxableUnclaimedExpenseBase += vatBase + vat;
                            }
                            else if (supplier.TaxRegistrationType == TaxRegisrationType.TOT)
                            {
                                double tot = Purchase2Document.GetTOTCalculationRate(goodOrService, titem.ServiceType, pd.taxRates);
                                taxableUnclaimedExpenseBase += Math.Round(item.quantity * item.unitPrice, 2) + tot;
                            }
                        }
                    }
                }
                i++;
            }


            bool tinNumberKnown = supplier.TIN == null || supplier.TIN == "" ? false : true;
            double withholdingTaxGood = Account.AmountLess(pd.manualWithholdBase, 0) ? withholdingBaseGood * TradeTransaction.GetWithHoldingCalculationRate(tinNumberKnown, withholdingBaseGood, goodOrService, mixedType, pd.taxRates)
                : pd.manualWithholdBase * TradeTransaction.GetWithHoldingCalculationRate(tinNumberKnown, pd.manualWithholdBase, goodOrService, mixedType, pd.taxRates);

            double withholdingTaxService = Account.AmountLess(pd.manualWithholdBase, 0) ? withholdingBaseService * TradeTransaction.GetWithHoldingCalculationRate(tinNumberKnown, withholdingBaseService, goodOrService, mixedType, pd.taxRates)
                : pd.manualWithholdBase * TradeTransaction.GetWithHoldingCalculationRate(tinNumberKnown, pd.manualWithholdBase, goodOrService, mixedType, pd.taxRates);

            ConnectToiERP();
            CompanyProfile companyProfile = bdeiERP.SysPars.companyProfile;

            if (!AccountBase.AmountEqual(documentGoodPurchaseValue, 0))
            {
                data.LocalPurchaseReport.AddLocalPurchaseReportRow(
                     supplier.Name
                    , supplier.TIN
                    , documentGoodPurchaseValue
                    , documentGoodVATValue
                    , companyProfile.isWitholdingAgent && supplier.Withhold ? withholdingTaxGood : 0
                    , pd.PaperRef
                    , new DateTime(pd.tranTicks).ToString("dd/MM/yyyy")
                    , voucher == null ? String.Empty : bdeAccounting.getDocumentSerialType(voucher.typeID).prefix + " " + voucher.reference
                    , data.Summary[0]);
            }

            if (!AccountBase.AmountEqual(documentServicePurchaseValue, 0))
            {
                data.LocalPurchaseServiceReport.AddLocalPurchaseServiceReportRow(
                     supplier.Name
                   , supplier.TIN
                   , documentServicePurchaseValue
                   , documentServiceVATValue
                   , companyProfile.isWitholdingAgent && supplier.Withhold ? withholdingTaxService : 0
                   , pd.PaperRef
                   , new DateTime(pd.tranTicks).ToString("dd/MM/yyyy")
                   , voucher == null ? String.Empty : bdeAccounting.getDocumentSerialType(voucher.typeID).prefix + " " + voucher.reference
                   , data.Summary[0]);
            }
        }

        private void ComputePurchaseForSuppliersWithNoTIN(iERPTransactionBDE bde, VATDeclarationData data
    , ref double noTINGoodPurchaseValue
    , ref double noTINGoodVATValue
    , ref double purchaseNonTaxable
    , int seqNo, ref double noTINServicePurchaseValue
    , ref double noTINServiceVATValue
    , GoodOrService goodOrService
    , bool mixedType, PurchaseTaxInfo pd, DocumentTypedReference voucher, TradeRelation supplier)
        {
            ConnectToiERP();
            pd.taxRates = pd.taxRates == null ? bde.getTaxRates() : pd.taxRates;

            CompanyProfile companyProfile = bdeiERP.SysPars.companyProfile;
            int i = 0;
            double withholdGoodBase, withholdServiceBase;
            GetTotalPurchaseBase(pd.items, out withholdGoodBase, out withholdServiceBase);

            foreach (TransactionDocumentItem item in pd.items)
            {
                purchaseNonTaxable += Math.Round(item.quantity * item.unitPrice, 2);

                TransactionItems titem = bde.GetTransactionItems(item.code);

                if (titem.IsFixedAssetItem)
                {
                    TransactionItems fixedAsset = titem;
                    if (i == 0)
                        goodOrService = GoodOrService.Good;
                    else
                    {
                        if (goodOrService != GoodOrService.Good)
                            mixedType = true;
                    }

                    noTINGoodPurchaseValue += Math.Round(item.quantity * item.unitPrice, 2);
                    double withholdingTaxGood = Account.AmountLess(pd.manualWithholdBase, 0) ? withholdGoodBase * TradeTransaction.GetWithHoldingCalculationRate(false, withholdGoodBase, goodOrService, mixedType, pd.taxRates)
                        : pd.manualWithholdBase * TradeTransaction.GetWithHoldingCalculationRate(false, pd.manualWithholdBase, goodOrService, mixedType, pd.taxRates);

                    data.NoTINLocalPurchaseGoodReport.AddNoTINLocalPurchaseGoodReportRow(
                     supplier.Name
                   , supplier.Region
                   , supplier.Zone
                   , string.IsNullOrEmpty(supplier.Woreda) ? supplier.Kebele : supplier.Woreda
                   , supplier.HouseNumber
                   , item.name
                   , noTINGoodPurchaseValue
                   , companyProfile.isWitholdingAgent && supplier.Withhold ? withholdingTaxGood : 0
                   , new DateTime(pd.tranTicks) == null ? pd.PaperRef : bdeAccounting.getDocumentSerialType(voucher.typeID).prefix + " " + voucher.reference
                   , new DateTime(pd.tranTicks).ToString("dd/MM/yyyy")
                   , data.Summary[0]);

                }
                else if (titem.IsExpenseItem)
                {
                    TransactionItems expenseItem = titem;

                    if (i == 0)
                        goodOrService = expenseItem.GoodOrService;
                    else
                    {
                        if (goodOrService != expenseItem.GoodOrService)
                            mixedType = true;
                    }
                    if (expenseItem.ExpenseType != ExpenseType.UnclaimedExpense)
                    {
                        if (expenseItem.GoodOrService == GoodOrService.Good)
                        {
                            noTINGoodPurchaseValue += Math.Round(item.quantity * item.unitPrice, 2);
                            double withholdingTaxGood = Account.AmountLess(pd.manualWithholdBase, 0) ? withholdGoodBase * TradeTransaction.GetWithHoldingCalculationRate(false, withholdGoodBase, goodOrService, mixedType, pd.taxRates)
                        : pd.manualWithholdBase * TradeTransaction.GetWithHoldingCalculationRate(false, pd.manualWithholdBase, goodOrService, mixedType, pd.taxRates);

                            data.NoTINLocalPurchaseGoodReport.AddNoTINLocalPurchaseGoodReportRow(
              supplier.Name
            , supplier.Region
            , supplier.Zone
            , string.IsNullOrEmpty(supplier.Woreda) ? supplier.Kebele : supplier.Woreda
            , supplier.HouseNumber
            , item.name
            , noTINGoodPurchaseValue
            , companyProfile.isWitholdingAgent && supplier.Withhold ? withholdingTaxGood : 0
            , voucher == null ? pd.PaperRef : bdeAccounting.getDocumentSerialType(voucher.typeID).prefix + " " + voucher.reference
            , new DateTime(pd.tranTicks).ToString("dd/MM/yyyy")
            , data.Summary[0]);
                        }
                        else
                        {
                            noTINServicePurchaseValue += Math.Round(item.quantity * item.unitPrice, 2);
                            double withholdingTaxService = Account.AmountLess(pd.manualWithholdBase, 0) ? withholdServiceBase * TradeTransaction.GetWithHoldingCalculationRate(false, withholdServiceBase, goodOrService, mixedType, pd.taxRates)
                        : pd.manualWithholdBase * TradeTransaction.GetWithHoldingCalculationRate(false, pd.manualWithholdBase, goodOrService, mixedType, pd.taxRates);

                            data.NoTINLocalPurchaseServiceReport.AddNoTINLocalPurchaseServiceReportRow(
              supplier.Name
            , supplier.Region
            , supplier.Zone
            , string.IsNullOrEmpty(supplier.Woreda) ? supplier.Kebele : supplier.Woreda
            , supplier.HouseNumber
            , item.name
            , noTINServicePurchaseValue
            , companyProfile.isWitholdingAgent && supplier.Withhold ? withholdingTaxService : 0
            , voucher == null ? pd.PaperRef : bdeAccounting.getDocumentSerialType(voucher.typeID).prefix + " " + voucher.reference
            , new DateTime(pd.tranTicks).ToString("dd/MM/yyyy")
            , data.Summary[0]);
                        }
                    }
                }
                i++;
            }
        }


        private void ComputeOutPutTax(bool rejected, iERPTransactionBDE bde, CompanyProfile prof, AccountDocument doc
            , VATDeclarationData data, ref double taxableLocalSalesBase
            , ref double taxableExportedBase, ref double salesNonTaxable
            , ref double remittedValue, ref double localSalesVAT, ref double VATAgentCustomerVATTotal, int seqNo)
        {
            Sell2Document salesDoc = doc as Sell2Document;
            if (salesDoc == null)
                throw new INTAPS.ClientServer.ServerUserMessage("Document is not a sales document");
            if (!rejected)
            {
                double totalTaxableBase = GetTotalTaxableBase(salesDoc.items);

                TradeRelation customer = bde.GetCustomer(salesDoc.relationCode);
                if (prof.TaxRegistrationType == TaxRegisrationType.VAT)
                {
                    double VATAgentCustomerSalesValue, VATAgentCustomerVAT;
                    VATAgentCustomerSalesValue = VATAgentCustomerVAT = 0;
                    foreach (TransactionDocumentItem item in salesDoc.items)
                    {
                        TransactionItems salesItem = bde.GetTransactionItems(item.code);
                        if (salesItem.TaxStatus == ItemTaxStatus.NonTaxable)
                        {
                            salesNonTaxable += Math.Round(item.quantity * item.unitPrice, 2);
                        }
                        else
                        {
                            if (item.remitted)
                            {
                                remittedValue += Math.Round(item.quantity * item.unitPrice, 2);
                            }
                            else
                            {
                                double vatBase = Account.AmountLess(salesDoc.manualVATBase, 0) ? Math.Round(item.quantity * item.unitPrice, 2) : Math.Round(item.quantity * item.unitPrice, 2) * (salesDoc.manualVATBase / totalTaxableBase);
                                taxableLocalSalesBase += vatBase;
                                if (!customer.IsVatAgent)
                                    localSalesVAT += vatBase * salesDoc.taxRates.CommonVATRate;
                                else
                                {
                                    VATAgentCustomerSalesValue += vatBase;
                                    VATAgentCustomerVAT += vatBase * salesDoc.taxRates.CommonVATRate;
                                }
                            }
                        }

                    }
                    VATAgentCustomerVATTotal += VATAgentCustomerVAT;
                    if (customer.IsVatAgent)
                    {
                        data.VATAgentCustomersSalesReport.AddVATAgentCustomersSalesReportRow(customer.Name, customer.TIN, VATAgentCustomerSalesValue
                            , VATAgentCustomerVAT, salesDoc.withHeldVatReceiptNo, salesDoc.DocumentDate.ToShortDateString(), data.Summary[0]);
                    }

                }
            }
        }


        private void ComputeZReportTax(bool rejected, iERPTransactionBDE bde, CompanyProfile prof, AccountDocument doc
    , VATDeclarationData data, ref double taxableLocalSalesBase, ref double salesNonTaxable
    , ref double localSalesVAT)
        {
            ZReportDocument zDoc = doc as ZReportDocument;
            if (zDoc == null)
                throw new INTAPS.ClientServer.ServerUserMessage("Document is not a z-report document");
            if (!rejected)
            {
                if (prof.TaxRegistrationType == TaxRegisrationType.VAT)
                {
                    salesNonTaxable += zDoc.NetNonTaxableAmount;

                    taxableLocalSalesBase += zDoc.NetTaxableAmount;
                    if (zDoc.taxImposed.TaxType == TaxType.VAT)
                        localSalesVAT += zDoc.NetTaxAmount;
                }
            }
        }

        private bool IsDocumentRejected(int docID, int[] rejectedDocuments)
        {
            if (rejectedDocuments != null)
            {
                foreach (int doc in rejectedDocuments)
                {
                    if (docID == doc)
                        return true;
                }
            }
            return false;
        }

        private double GetAccumulatedCreditAmount(int AID, iERPTransactionBDE bde, DateTime time, bool newDeclaration)
        {
            INTAPS.RDBMS.SQLHelper helper = bde.GetReaderHelper();
            //helper.setReadDB(bde.DBName);
            double accumulatedCredit = 0;
            try
            {
                int declarationType = bdeAccounting.GetDocumentTypeByType(typeof(VATDeclarationDocument)).id;
                string sqlCheck = String.Format("Select count(*) From {0}.dbo.TaxDeclaration where declarationType=" + declarationType + " And payDocumentID=-1", bde.DBName);
                string sql = String.Format("Select Top(1) id From {1}.dbo.TaxDeclaration  where declarationType={0} And payDocumentID=-1 order by id desc", declarationType, bde.DBName);
                //If there are no taxes declared, retrieve accumulated credit from database
                if ((int)helper.ExecuteScalar(sqlCheck) == 0)
                {
                    accumulatedCredit = bdeAccounting.GetNetBalanceAsOf(bdeAccounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.accumulatedReceivableVATAccountID).id, 0, time);
                }
                //If there are are already saved tax declarations, retrieve accumulated balance from last tax declaration accumulated credit
                else
                {
                    int taxDeclarationID = (int)helper.ExecuteScalar(sql);
                    TaxDeclaration declaration = bde.GetTaxDeclaration(taxDeclarationID);
                    VATDeclarationData data = (VATDeclarationData)declaration.report;
                    accumulatedCredit = data.InputTaxComputation[0].CarryForwardCredit;
                }
            }
            finally
            {
                bde.ReleaseHelper(helper);
            }
            return accumulatedCredit;
        }
        public int[] GetCandidateDocuments(iERPTransactionBDE bde, DateTime declarationDate, int periodID, out int[] taxCenterID, out double[] taxAmount)
        {
            try
            {
                taxCenterID = new int[] { bde.SysPars.companyProfile.VATTaxCenter };
                AccountingPeriod period = bde.GetAccountingPeriod(bde.SysPars.taxDeclarationPeriods, periodID);
                int N;
                List<int> docID = new List<int>();
                List<double> amount = new List<double>();

                PrepareInputTaxCandidates(bde, period, out N, ref docID, ref amount);
                PrepareOutputTaxCandidates(bde, period, out N, ref docID, ref amount);
                PrepareZReportTaxCandidates(bde, period, out N, ref docID, ref amount);
                PrepareAdjustmentTaxCandidates(bde, period, out N, ref docID, ref amount);
                taxAmount = amount.ToArray();
                return docID.ToArray();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void PrepareInputTaxCandidates(iERPTransactionBDE bde, AccountingPeriod period, out int N, ref List<int> docID, ref List<double> amount)
        {
            AccountDocument[] docs = bdeAccounting.GetAccountDocuments(null, bdeAccounting.GetDocumentTypeByType(typeof(Purchase2Document)).id,
                true, period.fromDate, period.toDate, 0, -1, out N);
            AccountDocument[] docs2 = bdeAccounting.GetAccountDocuments(null, bdeAccounting.GetDocumentTypeByType(typeof(SupplierAdvancePayment2Document)).id,
                    true, period.fromDate, period.toDate, 0, -1, out N);

            AccountDocument[] docs3 = bdeAccounting.GetAccountDocuments(null, bdeAccounting.GetDocumentTypeByType(typeof(PurchaseInvoiceDocument)).id,
        true, period.fromDate, period.toDate, 0, -1, out N);

            int docs2length = docs2.Length;
            List<AccountDocument> candidates = new List<AccountDocument>();
            candidates.AddRange(docs);
            candidates.AddRange(docs2);
            candidates.AddRange(docs3);

            INTAPS.RDBMS.SQLHelper helper = bde.GetReaderHelper();
            try
            {
                try
                {
                    foreach (AccountDocument doc in candidates)
                    {
                        if ((int)helper.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.{1} where documentID={2} and declarationType={3}", bde.DBName, typeof(DeclaredDocument).Name, doc.AccountDocumentID, bdeAccounting.GetDocumentTypeByType(typeof(VATDeclarationDocument)).id)) > 0)
                            continue;
                        TransactionDocumentItem[] docItems;
                        TaxImposed[] taxImposed;
                        AccountDocument fullDoc = bdeAccounting.GetAccountDocument(doc.AccountDocumentID, true);
                        if (fullDoc is Purchase2Document)
                        {
                            Purchase2Document pdoc = fullDoc as Purchase2Document;
                            if (!pdoc.invoiced)
                                continue;
                            docItems = pdoc.items;
                            taxImposed = pdoc.taxImposed;

                        }
                        else
                        {
                            PurchaseInvoiceDocument invoice = fullDoc as PurchaseInvoiceDocument;
                            docItems = invoice.items;
                            taxImposed = invoice.taxImposed;
                        }

                        bool taxAuthorityUnclaimableExists = false;
                        foreach (TransactionDocumentItem item in docItems)
                        {
                            TransactionItems titem = bde.GetTransactionItems(item.code);
                            if (Purchase2Document.TaxAuthorityUnclaimableItemExists(titem))
                            {
                                taxAuthorityUnclaimableExists = true;
                                break;
                            }
                        }
                        if (!taxAuthorityUnclaimableExists)
                        {
                            foreach (TaxImposed tax in taxImposed)
                            {
                                if (tax.TaxType == TaxType.VAT)
                                {
                                    if (AccountBase.AmountGreater(tax.TaxValue, 0))
                                    {
                                        amount.Add(tax.TaxValue);
                                        docID.Add(doc.AccountDocumentID);
                                        break;
                                    }
                                }
                                else //TOT
                                {
                                    amount.Add(0);
                                    docID.Add(doc.AccountDocumentID);
                                    break;
                                }
                            }
                            if (taxImposed.Length == 0) //Non-taxable items
                            {
                                amount.Add(0);
                                docID.Add(doc.AccountDocumentID);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            finally
            {
                bde.ReleaseHelper(helper);
            }
        }


        private void PrepareOutputTaxCandidates(iERPTransactionBDE bde, AccountingPeriod period, out int N, ref List<int> docID, ref List<double> amount)
        {
            AccountDocument[] docs = bdeAccounting.GetAccountDocuments(null, bdeAccounting.GetDocumentTypeByType(typeof(Sell2Document)).id,
                            true, period.fromDate, period.toDate, 0, -1, out N);

            AccountDocument[] docs2 = bdeAccounting.GetAccountDocuments(null, bdeAccounting.GetDocumentTypeByType(typeof(CustomerAdvancePayment2Document)).id,
true, period.fromDate, period.toDate, 0, -1, out N);

            int docs2length = docs2.Length;
            AccountDocument[] candidates = new AccountDocument[docs.Length + docs2length];
            docs.CopyTo(candidates, 0);
            docs2.CopyTo(candidates, docs.Length);

            INTAPS.RDBMS.SQLHelper helper = bde.GetReaderHelper();
            try
            {
                foreach (AccountDocument doc in candidates)
                {
                    if ((int)helper.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.{1} where documentID={2} and declarationType={3}", bde.DBName, typeof(DeclaredDocument).Name, doc.AccountDocumentID, bdeAccounting.GetDocumentTypeByType(typeof(VATDeclarationDocument)).id)) > 0)
                        continue;

                    Sell2Document salesDoc = bdeAccounting.GetAccountDocument(doc.AccountDocumentID, true) as Sell2Document;

                    foreach (TaxImposed tax in salesDoc.taxImposed)
                    {
                        if (tax.TaxType == TaxType.VAT)
                        {
                            if (AccountBase.AmountGreater(tax.TaxValue, 0))
                            {
                                amount.Add(tax.TaxValue);
                                docID.Add(doc.AccountDocumentID);
                                break;
                            }
                        }
                    }
                }
            }
            finally
            {
                bde.ReleaseHelper(helper);
            }
        }

        private void PrepareZReportTaxCandidates(iERPTransactionBDE bde, AccountingPeriod period, out int N, ref List<int> docID, ref List<double> amount)
        {
            AccountDocument[] docs = bdeAccounting.GetAccountDocuments(null, bdeAccounting.GetDocumentTypeByType(typeof(ZReportDocument)).id,
                            true, period.fromDate, period.toDate, 0, -1, out N);
            INTAPS.RDBMS.SQLHelper helper = bde.GetReaderHelper();
            try
            {
                foreach (AccountDocument doc in docs)
                {
                    if ((int)helper.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.{1} where documentID={2} and declarationType={3}", bde.DBName, typeof(DeclaredDocument).Name, doc.AccountDocumentID, bdeAccounting.GetDocumentTypeByType(typeof(VATDeclarationDocument)).id)) > 0)
                        continue;

                    ZReportDocument zReport = bdeAccounting.GetAccountDocument(doc.AccountDocumentID, true) as ZReportDocument;

                    if (zReport.taxImposed.TaxType == TaxType.VAT)
                    {
                        if (AccountBase.AmountGreater(zReport.NetTaxAmount, 0))
                        {
                            amount.Add(zReport.NetTaxAmount);
                            docID.Add(doc.AccountDocumentID);
                        }
                    }

                }
            }
            finally
            {
                bde.ReleaseHelper(helper);
            }
        }

        private void PrepareAdjustmentTaxCandidates(iERPTransactionBDE bde, AccountingPeriod period, out int n, ref List<int> docID, ref List<double> amount)
        {
            AccountDocument[] docs = bdeAccounting.GetAccountDocuments(null, bdeAccounting.GetDocumentTypeByType(typeof(AdjustmentDocument)).id,
                            true, period.fromDate, period.toDate, 0, -1, out n);
            INTAPS.RDBMS.SQLHelper helper = bde.GetReaderHelper();
            double totalTranAmount;
            try
            {
                foreach (AccountDocument doc in docs)
                {
                    if ((int)helper.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.{1} where documentID={2} and declarationType={3}", bde.DBName, typeof(DeclaredDocument).Name, doc.AccountDocumentID, bdeAccounting.GetDocumentTypeByType(typeof(VATDeclarationDocument)).id)) > 0)
                        continue;

                    totalTranAmount = 0;
                    AccountTransaction[] transactions = bdeAccounting.GetTransactionsOfDocument(doc.AccountDocumentID);
                    foreach (AccountTransaction tran in transactions)
                    {
                        if (tran.AccountID == bde.SysPars.inputVATAccountID)
                        {
                            if (AccountBase.AmountGreater(tran.Amount, 0))
                            {
                                totalTranAmount += tran.Amount;
                            }
                        }
                    }
                    if (AccountBase.AmountGreater(totalTranAmount, 0))
                    {
                        amount.Add(totalTranAmount);
                        docID.Add(doc.AccountDocumentID);
                    }

                }
            }
            finally
            {
                bde.ReleaseHelper(helper);
            }
        }

        private double _GetTOTPurchaseTotalAmount(iERPTransactionBDE bde, AccountingPeriod period, out int N)
        {
            AccountDocument[] docs = bdeAccounting.GetAccountDocuments(null, bdeAccounting.GetDocumentTypeByType(typeof(Purchase2Document)).id,
                true, period.fromDate, period.toDate, 0, -1, out N);
            INTAPS.RDBMS.SQLHelper helper = bde.GetReaderHelper();
            try
            {
                double total = 0;
                foreach (AccountDocument doc in docs)
                {
                    if ((int)helper.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.{1} where documentID={2} and declarationType={3}", bde.DBName, typeof(DeclaredDocument).Name, doc.AccountDocumentID, bdeAccounting.GetDocumentTypeByType(typeof(VATDeclarationDocument)).id)) > 0)
                        continue;

                    Purchase2Document pdoc = bdeAccounting.GetAccountDocument(doc.AccountDocumentID, true) as Purchase2Document;
                    TradeRelation suppliers = bde.GetSupplier(pdoc.relationCode);
                    if (suppliers != null)
                    {

                        TradeRelation supplier = suppliers;
                        if (supplier.TaxRegistrationType == TaxRegisrationType.TOT)
                        {
                            foreach (TransactionDocumentItem item in pdoc.items)
                            {
                                total += Math.Round(item.quantity * item.unitPrice);
                            }
                        }
                    }
                }
                return total;
            }
            finally
            {
                bde.ReleaseHelper(helper);
            }
        }
        private double GetTotalTaxableBase(TransactionDocumentItem[] transactionDocumentItems)
        {
            ConnectToiERP();
            double totalTaxable = 0;
            foreach (TransactionDocumentItem item in transactionDocumentItems)
            {
                TransactionItems titm = bdeiERP.GetTransactionItems(item.code);
                if (titm.TaxStatus == ItemTaxStatus.Taxable && !item.remitted)
                    totalTaxable += item.unitPrice * item.quantity;
            }
            return totalTaxable;
        }
        private void GetTotalPurchaseBase(TransactionDocumentItem[] transactionDocumentItems, out double goodBase, out double serviceBase)
        {
            ConnectToiERP();
            goodBase = serviceBase = 0;
            foreach (TransactionDocumentItem item in transactionDocumentItems)
            {
                TransactionItems titm = bdeiERP.GetTransactionItems(item.code);
                if (titm.GoodOrService == GoodOrService.Good)
                    goodBase += item.unitPrice * item.quantity;
                else
                    serviceBase += item.unitPrice * item.quantity;
            }
        }
        #region ITaxDeclarationServerHandler Members


        public double RetrieveTotalTax(DataSet data)
        {
            VATDeclarationData vatDAta = (VATDeclarationData)data;
            return vatDAta.InputTaxComputation[0].TotalPayableAmount;
        }

        #endregion
        private string GetAmharicMonthName(int month)
        {
            string amharicText = "";
            switch (month)
            {
                case 1:
                    amharicText = "መስከረም";
                    break;
                case 2:
                    amharicText = "ጥቅምት";
                    break;
                case 3:
                    amharicText = "ህዳር";
                    break;
                case 4:
                    amharicText = "ታህሳስ";
                    break;
                case 5:
                    amharicText = "ጥር";
                    break;
                case 6:
                    amharicText = "የካቲት";
                    break;
                case 7:
                    amharicText = "መጋቢት";
                    break;
                case 8:
                    amharicText = "ሚያዝያ";
                    break;
                case 9:
                    amharicText = "ግንቦት";
                    break;
                case 10:
                    amharicText = "ሰኔ";
                    break;
                case 11:
                    amharicText = "ሐምሌ";
                    break;
                case 12:
                    amharicText = "ነሃሴ";
                    break;
                default:
                    break;
            }
            return amharicText;

        }
        private string GetEnglishMonthName(int month)
        {
            DateTimeFormatInfo info = DateTimeFormatInfo.GetInstance(null);
            return info.GetMonthName(month);
        }
    }

}
