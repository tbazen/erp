using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using BIZNET.iERP.TypedDataSets;
using INTAPS.Accounting;

namespace BIZNET.iERP.Server
{
    public class DHVATWithholdingDeclarationDocument : DHTaxDeclarationBase<VATWithholdingDeclarationDocument>, ITaxDeclarationServerHandler
    {
        public DHVATWithholdingDeclarationDocument(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {

        }
        protected override int TaxPayableAcount
        {
            get
            {
                return bdeiERP.SysPars.outputVATWithHeldAccountID;
            }
        }

       

        protected override int TaxPenalityAccount
        {
            get
            {
                return bdeiERP.SysPars.taxPenalityAcountID;
            }
        }
        protected override int TaxInterestAccount
        {
            get
            {
                return bdeiERP.SysPars.taxInterestAccountID;
            }
        }
        protected override int TaxDeductionsAccount
        {
            get
            {

                return bdeiERP.SysPars.taxExemptionsAccountID;
            }
        }
        public DataSet BuildReportDataSet(int AID, iERPTransactionBDE bde, DateTime declarationDate, int periodID, int taxCenterID, int[] documents, int[] rejectedDocuments, out double totalTax, bool newDeclaration)
        {
            VATWithholdingDeclarationData ret = new VATWithholdingDeclarationData();

            AccountingPeriod period = bde.GetAccountingPeriod(bde.SysPars.taxDeclarationPeriods, periodID);

            CompanyProfile prof = bde.SysPars.companyProfile;
            INTAPS.Ethiopic.EtGrDate month = INTAPS.Ethiopic.EtGrDate.ToEth(period.fromDate);
            ret.Summary.AddSummaryRow("", prof.Name,
                prof.Region, prof.Zone, prof.Woreda, prof.Kebele, prof.HouseNumber, prof.TIN, "", "", prof.Telephone, prof.Fax, month.Month, month.Year, 0, documents.Length, 0, 0, 0, 0
                , periodID, "", null, null, null, null, null, null, null, null);
            totalTax = 0;

            for (int i = 0; i < documents.Length; i++)
            {
                AccountDocument doc = bdeAccounting.GetAccountDocument(documents[i], true);
                TradeRelation sup;
                double taxable = 0;
                double tax = 0;
                Purchase2Document pd = doc as Purchase2Document;
                if (pd == null)
                    throw new INTAPS.ClientServer.ServerUserMessage("Document is not a purchase document");

                sup = bde.GetSupplier(pd.relationCode);
                foreach (TaxImposed ti in pd.taxImposed)
                {
                    if (ti.TaxType == TaxType.VATWitholding)
                    {
                        taxable += ti.TaxBaseValue;
                        tax += -1 * (ti.TaxValue);
                    }
                }
                if (AccountBase.AmountEqual(tax, 0))
                {
                    i--;
                    continue;
                }
                
                INTAPS.Ethiopic.EtGrDate date = INTAPS.Ethiopic.EtGrDate.ToEth(pd.DocumentDate);
                taxable = Account.AmountLess(pd.manualWithholdBase, 0) ? taxable : pd.manualVATBase;

                ret.Continuation.AddContinuationRow((i + 1).ToString()
                , sup.TIN, sup.Name, sup.Region, sup.Zone, sup.Woreda, sup.Kebele, sup.HouseNumber, "", taxable, tax, pd.vatWithholdingReceiptNo
                , date.Day.ToString("00") + "/" + date.Month.ToString("00") + "/" + (date.Year % 100).ToString("00")
                , ret.Summary[0], "", "", "", "");
                totalTax += tax;
            }
            ret.Summary[0][ret.Summary.TotalTaxColumn] = DBNull.Value;
            ret.Summary[0][ret.Summary.TotalTaxableColumn] = DBNull.Value;
            ret.Summary[0][ret.Summary.ContTaxColumn] = DBNull.Value;
            ret.Summary[0][ret.Summary.ContTaxableColumn] = DBNull.Value;

            return ret;
        }
        public int[] GetCandidateDocuments(iERPTransactionBDE bde, DateTime declarationDate, int periodID,out  int[] taxCenterID, out double[] taxAmount)
        {
            taxCenterID = new int[] { bde.SysPars.companyProfile.VATTaxCenter };
            AccountingPeriod period = bde.GetAccountingPeriod(bde.SysPars.taxDeclarationPeriods, periodID);
            int N;
            AccountDocument[] docs=bdeAccounting.GetAccountDocuments(null, bdeAccounting.GetDocumentTypeByType(typeof(Purchase2Document)).id,
                true, period.fromDate, period.toDate, 0, -1, out N);

            AccountDocument[] docs2 = bdeAccounting.GetAccountDocuments(null, bdeAccounting.GetDocumentTypeByType(typeof(SupplierAdvancePayment2Document)).id,
true, period.fromDate, period.toDate, 0, -1, out N);

            int docs2length = docs2.Length;
            AccountDocument[] candidates = new AccountDocument[docs.Length + docs2length];
            docs.CopyTo(candidates, 0);
            docs2.CopyTo(candidates, docs.Length);

            INTAPS.RDBMS.SQLHelper helper = bde.GetReaderHelper();
            try
            {
                List<int> ret = new List<int>();
                List<double> amount = new List<double>();
                foreach (AccountDocument doc in candidates)
                {
                    if ((int)helper.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.{1} where documentID={2} and declarationType={3}", bde.DBName, typeof(DeclaredDocument).Name, doc.AccountDocumentID, bdeAccounting.GetDocumentTypeByType(typeof(VATWithholdingDeclarationDocument)).id)) > 0)
                         continue;
                     Purchase2Document pdoc = bdeAccounting.GetAccountDocument(doc.AccountDocumentID, true) as Purchase2Document;
                     
                    foreach (TaxImposed tax in pdoc.taxImposed)
                     {
                         if (tax.TaxType == TaxType.VATWitholding)
                         {
                             if (AccountBase.AmountGreater(-1*(tax.TaxValue), 0))
                             {
                                 ret.Add(doc.AccountDocumentID);
                                 amount.Add(-1*(tax.TaxValue));
                                 break;
                             }
                         }
                     }
                }
                taxAmount = amount.ToArray();
                return ret.ToArray();
            }
            finally
            {
                bde.ReleaseHelper(helper);
            }
        }


        #region ITaxDeclarationServerHandler Members


        public double RetrieveTotalTax(DataSet data)
        {
            VATWithholdingDeclarationData vData = (VATWithholdingDeclarationData)data;
            double total = 0;
            foreach (TypedDataSets.VATWithholdingDeclarationData.ContinuationRow row in vData.Continuation)
                total += row.Tax;
            return total;
        }

        #endregion
    }

}
