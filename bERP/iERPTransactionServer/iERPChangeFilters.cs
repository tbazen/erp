﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS;
namespace BIZNET.iERP.Server
{
    [BDEChangeFilter(typeof(AccountDocument)
        , typeof(INTAPS.Payroll.PayrollSetDocument)
        , typeof(Purchase2Document)
        , typeof(Sell2Document)
    )]
    public class DeclaredDocumentFilter : iERPChangeFilterBase, IBDEChangeFilter
    {
        bool disabledByConfiguration = false;
        public DeclaredDocumentFilter()
        {
            disabledByConfiguration = "true".Equals(System.Configuration.ConfigurationManager.AppSettings["disableDeclarationFilter"]);
        }
        bool _enabled = true;
        public bool enabled
        {
            set
            {
                _enabled = value;
            }
        }


        public bool CanDelete(int AID, Type objectType, out string userMessage, params object[] keyValues)
        {
            userMessage = null;
            if (base._allowAID == AID || !_enabled || disabledByConfiguration)
                return true;

            string sql = "Select count(*) from {0}.dbo.DeclaredDocument where documentID={1}";
            INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
            try
            {

                if (((int)reader.ExecuteScalar(string.Format(sql, bde.DBName, keyValues[0]))) == 0)
                {
                    userMessage = null;
                    return true;
                }
                else
                {
                    userMessage = "This document can't be deleted because it is used in tax declaration";
                    return false;
                }


            }
            finally
            {
                bde.ReleaseHelper(reader);
            }
        }

        public bool CanUpdate(int AID, object objectData, out string userMessage)
        {
            return CanDelete(AID, typeof(AccountDocument), out userMessage, ((AccountDocument)objectData).AccountDocumentID);
        }
    }

    public class iERPChangeFilterBase
    {
        iERPTransactionBDE _bde = null;
        protected iERPTransactionBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = (iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _bde;
            }
        }

        INTAPS.Accounting.BDE.AccountingBDE _bdeAccounting = null;
        protected INTAPS.Accounting.BDE.AccountingBDE bdeAccounting
        {
            get
            {
                if (_bdeAccounting == null)
                    _bdeAccounting = (INTAPS.Accounting.BDE.AccountingBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("Accounting");
                return _bdeAccounting;
            }
        }
        protected int _allowAID = -1;
        public void allowAction(int AID)
        {
            _allowAID = AID;
        }
        public void resetAllowAction()
        {
            _allowAID = -1;
        }

        public virtual bool CanCreate(int AID, object objectData, out string userMessage)
        {
            userMessage = null;
            return true;
        }
    }

    [BDEChangeFilter(typeof(CostCenter))]
    public class ProjectCostCentersFilter : iERPChangeFilterBase, IBDEChangeFilter
    {
        public bool CanDelete(int AID, Type objectType, out string userMessage, params object[] keyValues)
        {
            userMessage = null;
            if (base._allowAID == AID)
                return true;
            int costCenterID = (int)keyValues[0];
            foreach (Project p in base.bde.GetAllProjects())
            {
                if (p.costCenterID == costCenterID)
                {
                    userMessage = "This cost center is project/branch {0} - {1}. Use project manager to manage project".format(p.Name, p.code);
                    return false;
                }
                foreach (int d in p.projectData.projectDivisions)
                {
                    if (d== costCenterID)
                    {
                        userMessage = "This cost center is a division in project/branch {0} - {1}. Use project manager to manage project".format(p.Name, p.code);
                        return false;
                    }
                }
                foreach (int d in p.projectData.machinaryAndVehicles)
                {
                    if (d == costCenterID)
                    {
                        userMessage = "This cost center is a machine or vehicle in project/branch {0} - {1}. Use project manager to manage project".format(p.Name, p.code);
                        return false;
                    }
                }
            }
            return true;
        }

        public bool CanUpdate(int AID, object objectData, out string userMessage)
        {
            userMessage = null;
            return true;
        }
    }
    [BDEChangeFilter(typeof(Account))]
    public class DetailAccountChangeFilter : iERPChangeFilterBase, IBDEChangeFilter
    {
        public bool CanDelete(int AID, Type objectType, out string userMessage, params object[] keyValues)
        {
            userMessage = null;
            return true;
        }

        public bool CanUpdate(int AID, object objectData, out string userMessage)
        {
            userMessage = null;
            if (base._allowAID == AID)
                return true;

            userMessage = null;
            if (objectData.GetType() == typeof(Account))
            {
                var acount = objectData as Account;
                String prefixCode = null;
                foreach (var p in bde.SysPars.summaryRoots)
                {
                    if (acount.Code.ToUpper().IndexOf(p) == 0)
                    {
                        prefixCode = p;
                        break;
                    }
                }
                if (acount.PID == -1)
                {
                    if (prefixCode == null)
                        return true;
                    if (acount.Code.Substring(0, prefixCode.Length).Equals(prefixCode))
                        return true;
                    userMessage = $"This account code is not allowed as root acocunt. Did you mean {prefixCode}?";
                    return false;
                }
                var parentAccount= bdeAccounting.GetAccount<Account>(acount.PID);
                if(parentAccount==null)
                {
                    userMessage = "Invalid parent account id:" + acount.PID;
                    return false;
                }
                String parentPrefix = null;
                foreach (var p in bde.SysPars.summaryRoots)
                {
                    if (parentAccount.Code.ToUpper().IndexOf(p) == 0)
                    {
                        parentPrefix = p;
                        break;
                    }
                }
                if (prefixCode == null && parentPrefix==null)
                    return true;
                if(parentPrefix==null && prefixCode!=null)
                {
                    userMessage = $"The prefix {prefixCode} is not allowed under parent account {parentAccount.Code}";
                    return false;
                }
                if(parentPrefix!=null && !parentPrefix.Equals(prefixCode))
                {
                    userMessage = $"The account must be prefixed with {parentPrefix}";
                    return false;   
                }
                return true;
            
            }
            return true;

        }
        public override bool CanCreate(int AID, object objectData, out string userMessage)
        {
            return CanUpdate(AID, objectData, out userMessage);
        }
    }
    [BDEChangeFilter(typeof(Account))]
    public class RelationsAccountChangeFilter : iERPChangeFilterBase, IBDEChangeFilter
    {
        public bool CanDelete(int AID, Type objectType, out string userMessage, params object[] keyValues)
        {
            userMessage = null;
            if (base._allowAID == AID)
                return true;
            INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
            try
            {
                int accountID = (int)keyValues[0];
                TradeRelation[] relation = reader.GetSTRArrayByFilter<TradeRelation>(string.Format("ReceivableAccountID={0} or PayableAccountID={0} ", accountID));
                if (relation.Length > 0)
                {
                    userMessage = "The account can't be deleted because it is used for " + relation[0].TypedName;
                    return false;
                }
                return true;

            }
            finally
            {
                bde.ReleaseHelper(reader);
            }
        }

        public bool CanUpdate(int AID, object objectData, out string userMessage)
        {
            userMessage = null;
            if (base._allowAID == AID)
                return true;

            userMessage = null;
            if (objectData.GetType() == typeof(Account))
            {
                INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
                try
                {
                    int accountID = ((Account)objectData).id;
                    TradeRelation[] custs = reader.GetSTRArrayByFilter<TradeRelation>(string.Format("ReceivableAccountID={0} or PayableAccountID={0}", accountID));
                    if (custs.Length > 0)
                    {
                        userMessage = "The account can't be updated because it is used for " + custs[0].TypedName;
                        return false;
                    }
                    return true;

                }
                finally
                {
                    bde.ReleaseHelper(reader);
                }
            }
            return true;

        }
    }
    [BDEChangeFilter(typeof(CostCenter))]
    public class RelationsCostCenterChangeFilter : iERPChangeFilterBase, IBDEChangeFilter
    {
        public bool CanDelete(int AID, Type objectType, out string userMessage, params object[] keyValues)
        {
            userMessage = null;
            if (base._allowAID == AID)
                return true;
            INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
            try
            {
                int accountID = (int)keyValues[0];
                TradeRelation[] relation = reader.GetSTRArrayByFilter<TradeRelation>(string.Format("onCustomerOrderCostCenterID={0} or onSupplierOrderCostCenterID={0}", accountID));
                if (relation.Length > 0)
                {
                    userMessage = "The accounting center can't be deleted because it is used for " + relation[0].TypedName;
                    return false;
                }
                return true;

            }
            finally
            {
                bde.ReleaseHelper(reader);
            }
        }
        public bool CanUpdate(int AID, object objectData, out string userMessage)
        {
            userMessage = null;
            if (base._allowAID == AID)
                return true;

            userMessage = null;
            if (objectData.GetType() == typeof(Account))
            {
                INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
                try
                {
                    int accountID = ((CostCenter)objectData).id;
                    TradeRelation[] custs = reader.GetSTRArrayByFilter<TradeRelation>(string.Format("onCustomerOrderCostCenterID={0} or onSupplierOrderCostCenterID={0}", accountID));
                    if (custs.Length > 0)
                    {
                        userMessage = "The accounting center can't be updated because it is used for " + custs[0].TypedName;
                        return false;
                    }
                    return true;

                }
                finally
                {
                    bde.ReleaseHelper(reader);
                }
            }
            return true;

        }
    }

    [BDEChangeFilter(typeof(CostCenterAccount))]
    public class CashAccountChangeFilter : iERPChangeFilterBase, IBDEChangeFilter
    {
        public override bool CanCreate(int AID, object objectData, out string userMessage)
        {
            userMessage = null;
            if (base._allowAID == AID)
                return true;
            INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
            try
            {
                int csAcountID = ((CostCenterAccount)objectData).id;
                int cnt = (int)reader.ExecuteScalar(string.Format(
                                @"Select COUNT(*) from {0}.dbo.CashAccount c inner join {1}.dbo.CostCenterAccount csa
                                    on c.csAccountID=csa.id
                                    where csa.ID={2}", bde.DBName, bdeAccounting.DBName, csAcountID));
                if (cnt > 0)
                {
                    userMessage = "Cash account can be attached only to a single cost center.";
                    return false;
                }
                return true;

            }
            finally
            {
                bde.ReleaseHelper(reader);
            }

        }
        public bool CanDelete(int AID, Type objectType, out string userMessage, params object[] keyValues)
        {
            userMessage = null;
            if (base._allowAID == AID)
                return true;

            userMessage = null;
            if (objectType == typeof(Account))
            {
                INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
                try
                {
                    int accountID = (int)keyValues[0];
                    int cnt = (int)reader.ExecuteScalar(string.Format(
                                    @"Select COUNT(*) from {0}.dbo.CashAccount c inner join {1}.dbo.CostCenterAccount csa
                                    on c.csAccountID=csa.id inner join Accounting_2005.dbo.Account ac on csa.accountID=ac.id
                                    where ac.ID={2}", bde.DBName, bdeAccounting.DBName, accountID));
                    if (cnt > 0)
                    {
                        userMessage = "The account can't be deleted because it is used for a cash account.";
                        return false;
                    }
                    return true;

                }
                finally
                {
                    bde.ReleaseHelper(reader);
                }
            }
            else if (objectType == typeof(CostCenterAccount))
            {
                INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
                try
                {
                    int csAcountID = (int)keyValues[0];
                    int cnt = (int)reader.ExecuteScalar(string.Format(
                                    @"Select COUNT(*) from {0}.dbo.CashAccount c inner join {1}.dbo.CostCenterAccount csa
                                    on c.csAccountID=csa.id
                                    where csa.ID={2}", bde.DBName, bdeAccounting.DBName, csAcountID));
                    if (cnt > 0)
                    {
                        userMessage = "The account can't be deleted because it is used for a cash account.";
                        return false;
                    }
                    return true;

                }
                finally
                {
                    bde.ReleaseHelper(reader);
                }
            }
            return true;
        }

        public bool CanUpdate(int AID, object objectData, out string userMessage)
        {
            userMessage = null;
            if (base._allowAID == AID)
                return true;

            userMessage = null;
            Type objectType = objectData.GetType();
            if (objectType == typeof(Account))
            {
                INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
                try
                {
                    int accountID = ((Account)objectData).id;
                    int cnt = (int)reader.ExecuteScalar(string.Format(
                                    @"Select COUNT(*) from {0}.dbo.CashAccount c inner join {1}.dbo.CostCenterAccount csa
                                    on c.csAccountID=csa.id inner join Accounting_2005.dbo.Account ac on csa.accountID=ac.id
                                    where ac.ID={2}", bde.DBName, bdeAccounting.DBName, accountID));
                    if (cnt > 0)
                    {
                        userMessage = "The account can't be updatd because it is used for a cash account.";
                        return false;
                    }
                    return true;

                }
                finally
                {
                    bde.ReleaseHelper(reader);
                }
            }
            else if (objectType == typeof(CostCenterAccount))
            {
                INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
                try
                {
                    int csAcountID = ((CostCenterAccount)objectData).id;
                    int cnt = (int)reader.ExecuteScalar(string.Format(
                                    @"Select COUNT(*) from {0}.dbo.CashAccount c inner join {1}.dbo.CostCenterAccount csa
                                    on c.csAccountID=csa.id
                                    where csa.ID={2}", bde.DBName, bdeAccounting.DBName, csAcountID));
                    if (cnt > 0)
                    {
                        userMessage = "The account can't be updated because it is used for a cash account.";
                        return false;
                    }
                    return true;

                }
                finally
                {
                    bde.ReleaseHelper(reader);
                }
            }
            return true;

        }
    }
    [BDEChangeFilter(typeof(AccountingBDE.RecordTransactionParameters),typeof(AccountDocument))]
    public class BudgetTransactionFilter:iERPChangeFilterBase,IBDEChangeFilter
    {
        HashSet<int> _ignoreDocumentTypeList=null;
        HashSet<int> ignoreDocumentTypeList
        {
            get
            {
                if (_ignoreDocumentTypeList == null)
                {
                    _ignoreDocumentTypeList = new HashSet<int>();
                    foreach(DocumentType t in base.bdeAccounting.GetAllDocumentTypes())
                    {
                        Type docType=t.GetTypeObject();
                        if (docType == null || docType.GetCustomAttributes(typeof(NoBudgetAttribute), false).Length > 0)
                            _ignoreDocumentTypeList.Add(t.id);
                    }
                }
                return _ignoreDocumentTypeList;
            }
        }

        public bool CanDelete(int AID, Type objectType, out string userMessage, params object[] keyValues)
        {
            if (objectType.Equals(typeof(AccountDocument)))
                base.bde.clearBudget(AID, (int)keyValues[0]);
            userMessage = null;
            return true;
        }
        public override bool CanCreate(int AID, object objectData, out string userMessage)
        {
            userMessage = null;
            if (objectData is AccountingBDE.RecordTransactionParameters)
            {
                AccountingBDE.RecordTransactionParameters pars=((AccountingBDE.RecordTransactionParameters)objectData);
                
                List<int> cashAccounts = new List<int>();
                cashAccounts.AddRange(bde.WriterHelper.GetColumnArray<int>("SELECT [mainCsAccount] FROM {0}.[dbo].[BankAccountInfo]".format(bde.DBName)));
                cashAccounts.AddRange(bde.WriterHelper.GetColumnArray<int>("SELECT [csAccountID] FROM {0}.[dbo].[CashAccount]".format(bde.DBName)));
                double net = 0;
                int documentTypeID = pars.document.DocumentTypeID;
                if (documentTypeID < 1)
                    documentTypeID = bdeAccounting.GetDocumentTypeByType(pars.document.GetType()).id;
                if (!ignoreDocumentTypeList.Contains(documentTypeID))
                {
                    foreach (TransactionOfBatch b in pars.batch)
                    {
                        if (cashAccounts.Contains(b.AccountID))
                            net += b.Amount;
                    }
                }
                if(AccountBase.AmountEqual(net,0))
                {
                    bde.setBudgetDocument(AID,pars.document.AccountDocumentID, -1);
                }
            }

            return true;
        }


        public bool CanUpdate(int AID, object objectData, out string userMessage)
        {
            userMessage = null;
            return true;
        }
    }

    //[BDEChangeFilter(typeof(CostCenterAccount))]
    public class BudgetEntryFilter : iERPChangeFilterBase, IBDEChangeFilter
    {
        public bool CanDelete(int AID, Type objectType, out string userMessage, params object[] keyValues)
        {
            userMessage = null;
            if (base._allowAID == AID)
                return true;

            userMessage = null;
            if (objectType == typeof(CostCenterAccount))
            {
                INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
                try
                {
                    int csAcountID = (int)keyValues[0];
                    int cnt = (int)reader.ExecuteScalar(string.Format(
                                    @"Select count(*) from {0}.dbo.Budget inner join {0}.dbo.BudgetEntry 
on Budget.id=BudgetEntry.accountID inner join {1}.dbo.CostCenterAccount c
on Budget.costCenterID=c.costCenterID and BudgetEntry.accountID=c.accountID where c.id={2}", bde.DBName, bdeAccounting.DBName, csAcountID));
                    if (cnt > 0)
                    {
                        userMessage = "The account can't be deleted because it is has budget.";
                        return false;
                    }
                    return true;

                }
                finally
                {
                    bde.ReleaseHelper(reader);
                }
            }
            return true;
        }

        public bool CanUpdate(int AID, object objectData, out string userMessage)
        {
            userMessage = null;
            if (base._allowAID == AID)
                return true;

            userMessage = null;
            Type objectType = objectData.GetType();
            if (objectType == typeof(Account))
            {
                INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
                try
                {
                    int accountID = ((Account)objectData).id;
                    int cnt = (int)reader.ExecuteScalar(string.Format(
                                    @"Select COUNT(*) from {0}.dbo.CashAccount c inner join {1}.dbo.CostCenterAccount csa
                                    on c.csAccountID=csa.id inner join Accounting_2005.dbo.Account ac on csa.accountID=ac.id
                                    where ac.ID={2}", bde.DBName, bdeAccounting.DBName, accountID));
                    if (cnt > 0)
                    {
                        userMessage = "The account can't be updatd because it is used for a cash account.";
                        return false;
                    }
                    return true;

                }
                finally
                {
                    bde.ReleaseHelper(reader);
                }
            }
            else if (objectType == typeof(CostCenterAccount))
            {
                INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
                try
                {
                    int csAcountID = ((CostCenterAccount)objectData).id;
                    int cnt = (int)reader.ExecuteScalar(string.Format(
                                    @"Select COUNT(*) from {0}.dbo.CashAccount c inner join {1}.dbo.CostCenterAccount csa
                                    on c.csAccountID=csa.id
                                    where csa.ID={2}", bde.DBName, bdeAccounting.DBName, csAcountID));
                    if (cnt > 0)
                    {
                        userMessage = "The account can't be updated because it is used for a cash account.";
                        return false;
                    }
                    return true;

                }
                finally
                {
                    bde.ReleaseHelper(reader);
                }
            }
            return true;

        }
    }
    [BDEChangeFilter(typeof(CostCenterAccount))]
    public class BankAccountChangeFilter : iERPChangeFilterBase, IBDEChangeFilter
    {
        public override bool CanCreate(int AID, object objectData, out string userMessage)
        {
            userMessage = null;
            if (base._allowAID == AID)
                return true;
            INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
            try
            {
                int csAcountID = ((CostCenterAccount)objectData).accountID;
                int cnt = (int)reader.ExecuteScalar(string.Format(
                                @"Select COUNT(*) from {0}.dbo.BankAccountInfo c inner join {1}.dbo.CostCenterAccount csa
                                    on c.mainCsAccount=csa.id or c.bankServiceChargeCsAccountID=csa.id
                                    where csa.accountID={2}", bde.DBName, bdeAccounting.DBName, csAcountID));
                if (cnt > 0)
                {
                    userMessage = "Bank account can be attached only to a single cost center.";
                    return false;
                }
                return true;

            }
            finally
            {
                bde.ReleaseHelper(reader);
            }
        }
        public bool CanDelete(int AID, Type objectType, out string userMessage, params object[] keyValues)
        {
            userMessage = null;
            if (base._allowAID == AID)
                return true;

            userMessage = null;
            if (objectType == typeof(Account))
            {
                INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
                try
                {
                    int accountID = (int)keyValues[0];
                    int cnt = (int)reader.ExecuteScalar(string.Format(
                                    @"Select COUNT(*) from {0}.dbo.BankAccountInfo c inner join {1}.dbo.CostCenterAccount csa
                                    on c.mainCsAccount=csa.id or c.bankServiceChargeCsAccountID=csa.id
                                    where csa.accountID={2}", bde.DBName, bdeAccounting.DBName, accountID));
                    if (cnt > 0)
                    {
                        userMessage = "The account can't be deleted because it is used for a bank account.";
                        return false;
                    }
                    return true;

                }
                finally
                {
                    bde.ReleaseHelper(reader);
                }
            }
            else if (objectType == typeof(CostCenterAccount))
            {
                INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
                try
                {
                    int csAcountID = (int)keyValues[0];
                    int cnt = (int)reader.ExecuteScalar(string.Format(
                                    @"Select COUNT(*) from {0}.dbo.BankAccountInfo c inner join {1}.dbo.CostCenterAccount csa
                                    on c.mainCsAccount=csa.id or c.bankServiceChargeCsAccountID=csa.id
                                    where csa.id={2}", bde.DBName, bdeAccounting.DBName, csAcountID));
                    if (cnt > 0)
                    {
                        userMessage = "The account can't be deleted because it is used for a bank account.";
                        return false;
                    }
                    return true;

                }
                finally
                {
                    bde.ReleaseHelper(reader);
                }
            }
            return true;
        }
        public bool CanUpdate(int AID, object objectData, out string userMessage)
        {
            userMessage = null;
            if (base._allowAID == AID)
                return true;

            userMessage = null;
            Type objectType = objectData.GetType();
            if (objectType == typeof(Account))
            {
                INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
                try
                {
                    int accountID = ((Account)objectData).id;
                    int cnt = (int)reader.ExecuteScalar(string.Format(
                                    @"Select COUNT(*) from {0}.dbo.BankAccountInfo c inner join {1}.dbo.CostCenterAccount csa
                                    on c.mainCsAccount=csa.id or c.bankServiceChargeCsAccountID=csa.id
                                    where csa.accountID={2}", bde.DBName, bdeAccounting.DBName, accountID));
                    if (cnt > 0)
                    {
                        userMessage = "The account can't be updatd because it is used for a bank account.";
                        return false;
                    }
                    return true;

                }
                finally
                {
                    bde.ReleaseHelper(reader);
                }
            }
            else if (objectType == typeof(CostCenterAccount))
            {
                INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
                try
                {
                    int csAcountID = ((CostCenterAccount)objectData).id;
                    int cnt = (int)reader.ExecuteScalar(string.Format(
                                    @"Select COUNT(*) from {0}.dbo.BankAccountInfo c inner join {1}.dbo.CostCenterAccount csa
                                    on c.mainCsAccount=csa.id or c.bankServiceChargeCsAccountID=csa.id
                                    where csa.id={2}", bde.DBName, bdeAccounting.DBName, csAcountID));
                    if (cnt > 0)
                    {
                        userMessage = "The account can't be updated because it is used for a bank account.";
                        return false;
                    }
                    return true;

                }
                finally
                {
                    bde.ReleaseHelper(reader);
                }
            }
            return true;

        }
    }

    public class SystemParameterReferenceFilterBase<AccountType> : iERPChangeFilterBase, IBDEChangeFilter
        where AccountType : AccountBase, new()
    {

        public bool CanDelete(int AID, Type objectType, out string userMessage, params object[] keyValues)
        {
            int accountID = (int)keyValues[0];
            foreach (System.Reflection.FieldInfo fi in typeof(iERPSystemParameters).GetFields())
            {
                object[] atr = fi.GetCustomAttributes(typeof(AccountReferenceAttribute), false);
                if (atr.Length == 0)
                    continue;
                AccountReferenceAttribute aref = (AccountReferenceAttribute)atr[0];
                if (typeof(AccountType) == typeof(Account))
                {
                    if (aref is CostCenterReferenceAttribute)
                        continue;

                }
                else
                {
                    if (!(aref is CostCenterReferenceAttribute))
                        continue;
                }
                if (accountID == (int)fi.GetValue(bde.SysPars))
                {
                    userMessage = "This account can't be deleted because it is used by the system for " + aref.description;
                    return false;
                }
            }
            userMessage = null;
            return true;
        }

        public bool CanUpdate(int AID, object objectData, out string userMessage)
        {
            userMessage = null;
            return true;
        }
    }

    [BDEChangeFilter(typeof(Account))]
    public class SystemParameterAccountReferenceFilter : SystemParameterReferenceFilterBase<Account>
    {
    }

    [BDEChangeFilter(typeof(CostCenter))]
    public class SystemParameterCostCenterReferenceFilter : SystemParameterReferenceFilterBase<CostCenter>
    {
    }

    //[BDEChangeFilter(
    //    typeof(AccountDocument)
    //)]
    public class RecalculateWeightedAverageFilter : iERPChangeFilterBase, IBDEChangeFilter
    {
        List<int> _inventoryTransactionTypes = null;
        List<int> inventoryTransactionTypes
        {
            get
            {
                if(_inventoryTransactionTypes==null)
                {
                    _inventoryTransactionTypes = new List<int>();
                    _inventoryTransactionTypes.Add(bdeAccounting.GetDocumentTypeByType(typeof(StoreIssueDocument)).id);
                    _inventoryTransactionTypes.Add(bdeAccounting.GetDocumentTypeByType(typeof(PurchasedItemDeliveryDocument)).id);
                    _inventoryTransactionTypes.Add(bdeAccounting.GetDocumentTypeByType(typeof(SoldItemDeliveryDocument)).id);
                    _inventoryTransactionTypes.Add(bdeAccounting.GetDocumentTypeByType(typeof(StoreFinishedGoodsDocument)).id);
                }
                return _inventoryTransactionTypes;
            }
        }
        public RecalculateWeightedAverageFilter()
        {

        }
        bool _enabled = true;
        public bool enabled
        {
            set
            {
                _enabled = value;
            }
        }

        public bool hasIssueDocument(long ticks)
        {
            INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
            try
            {
                string sql = string.Format("Select count(*) from {0}.dbo.Document where  DocumentTypeID in ({1},{2}) and tranTicks>{3}", bdeAccounting.DBName, bdeAccounting.GetDocumentTypeByType(typeof(StoreIssueDocument)).id, bdeAccounting.GetDocumentTypeByType(typeof(SoldItemDeliveryDocument)).id, ticks);


                if ((int)reader.ExecuteScalar(sql) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                bde.ReleaseHelper(reader);
            }
        }
        public bool CanDelete(int AID, Type objectType, out string userMessage, params object[] keyValues)
        {
            userMessage = null;
            if (base._allowAID == AID || !_enabled)
                return true;

            string sql = "Select tranTicks,DocumentTypeID from {0}.dbo.Document where ID={1}";
            INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
            try
            {
                System.Data.DataRow row = reader.GetDataTable(string.Format(sql, bdeAccounting.DBName, keyValues[0])).Rows[0];

                long ticks = (long)row[0];
                if (!inventoryTransactionTypes.Contains((int)row[1]))
                    return true;

                if (hasIssueDocument(ticks))
                {
                    userMessage = "This document can't be deleted because items are issued after the date of this transaction";
                    return false;
                }
                else
                {
                    userMessage = null;
                    return true;
                }
            }
            finally
            {
                bde.ReleaseHelper(reader);
            }
        }

        public bool CanUpdate(int AID, object objectData, out string userMessage)
        {
            userMessage = null;
            if (base._allowAID == AID || !_enabled)
                return true;

            AccountDocument doc = (AccountDocument)objectData;
            if (!inventoryTransactionTypes.Contains(doc.DocumentTypeID))
                return true;
            long ticks = doc.tranTicks;
            if (hasIssueDocument(ticks))
            {
                userMessage = "This document can't be posted because items are issued after the date of this transaction";
                return false;
            }
            else
            {
                userMessage = null;
                return true;
            }
        }
        public override bool CanCreate(int AID, object objectData, out string userMessage)
        {
            return this.CanUpdate(AID, objectData, out userMessage);
        }
    }

}