using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using BIZNET.iERP;
using INTAPS.Accounting.BDE;
using System.Data.SqlClient;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        public void assertNoSummaryAccount(int account)
        {
            Account ac = this.Accounting.GetAccount<Account>(account);
            if (ac == null)
                throw new INTAPS.ClientServer.ServerUserMessage("Invalid account id:" + account);
            foreach(string prefix in this.SysPars.summaryRoots)
            {
                if (ac.Code.IndexOf(prefix) == 0)
                    throw new INTAPS.ClientServer.ServerUserMessage(prefix + " account is not allowed");
            }
        }
        public void assertNoSummaryCsAccount(int account)
        {
            assertNoSummaryAccount(this.Accounting.GetCostCenterAccount(account).accountID);
        }

        private void InitializeReporting()
        {
            foreach (KeyValuePair<int, IReportProcessor> pair in this.Accounting.ReportProcessors)
            {
                ((ReportProcessorBase)pair.Value).vars.Add("conBERP", ReportProcessorBase.CreateConnection("iERP"));
                ((ReportProcessorBase)pair.Value).funcs.Add(new PCCompanyName(this));
                ((ReportProcessorBase)pair.Value).funcs.Add(new RPTGetSalesReportData(this));
            }
        }
        public void GenerateAccountingPeriodSeries(Type generator, string type, DateTime from, DateTime to)
        {
            lock (dspWriter)
            {
                PeriodSeries ps = Activator.CreateInstance(generator) as PeriodSeries;
                DateTime date = from;
                while (date < to)
                {
                    AccountingPeriod acp = ps.GetPeriodForDate(date);
                    INTAPS.Accounting.BDE.AccountingPeriodHelper.CreatePayPeriod<AccountingPeriod>(this.DBName, dspWriter, type, acp);
                    date = acp.toDate;
                }
            }
        }
        public AccountingPeriod GetAccountingPeriod(string periodType, int id)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                return INTAPS.Accounting.BDE.AccountingPeriodHelper.GetPayPeriod<AccountingPeriod>(helper, periodType, id);
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public AccountingPeriod GetNextAccountingPeriod(string periodType, int id)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                AccountingPeriod thisPeriod= GetAccountingPeriod(periodType, id);
                return INTAPS.Accounting.BDE.AccountingPeriodHelper.GetNextPeriod<AccountingPeriod>(helper, periodType, thisPeriod);
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public AccountingPeriod GetPreviousAccountingPeriod(string periodType, int id)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                AccountingPeriod thisPeriod = GetAccountingPeriod(periodType, id);
                return INTAPS.Accounting.BDE.AccountingPeriodHelper.GetPreviousPeriod<AccountingPeriod>(helper, periodType, thisPeriod);
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public AccountingPeriod GetAccountingPeriod(string periodType, DateTime date)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                return INTAPS.Accounting.BDE.AccountingPeriodHelper.GetPeriodForDate<AccountingPeriod>(helper, periodType, date);
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public AccountingPeriod[] GetAccountingPeriodsInFiscalYear(string periodType, int year)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                var d = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, 1, year)).GridDate;
                AccountingPeriod ap = INTAPS.Accounting.BDE.AccountingPeriodHelper.GetPeriodForDate<AccountingPeriod>
                    (helper,"AP_Accounting_Year",d
                    );
                return INTAPS.Accounting.BDE.AccountingPeriodHelper.GetTouchedPeriods<AccountingPeriod>(helper, periodType
                , ap.fromDate, ap.toDate);
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public AccountingPeriod[] GetTouchedPeriods(string periodType, DateTime fromDate, DateTime toDate)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                return INTAPS.Accounting.BDE.AccountingPeriodHelper.GetTouchedPeriods<AccountingPeriod>(helper, periodType
                , fromDate, toDate);
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public void GenerateYearAccountingPeriod(int year)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    DateTime etYearStart;
                    DateTime etYearEnd;
                    AccountingPeriod yearPeriod;
                    AccountingPeriod monthPeriod;
                    DateTime monthStart;
                    switch (SysPars.accountingPeriodStyle)
                    {
                        case AccountPeriodStyle.EthiopianFiscalYearEthiopianMonth:
                            etYearStart = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, 11, year - 1)).GridDate;
                            etYearEnd = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, 11, year)).GridDate;
                            yearPeriod = getOrCreateFiscalYearPeriod(year, year.ToString(),etYearStart, etYearEnd);
                            int m = 11;
                            int nextM;
                            for (int i = 1; i <= 12; i++)
                            {
                                nextM = m + 1;
                                if (nextM == 13)
                                    nextM = 1;

                                monthPeriod = new AccountingPeriod();
                                monthPeriod.name = INTAPS.Ethiopic.EtGrDate.GetEtMonthNameEng(m,INTAPS.ClientServer.ApplicationServer.languageID) + " " + (m < 11 ? year : year - 1);
                                monthPeriod.fromDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, m, m < 11 ? year : year - 1)).GridDate;
                                monthPeriod.toDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, nextM, nextM < 12 ? year : year - 1)).GridDate;
                                monthPeriod.id = getOrCreateMonthPeriod(monthPeriod);
                                m = nextM;
                            }
                            break;
                        case AccountPeriodStyle.EthiopianFiscalYearGergorianMonth:
                            etYearStart = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, 11, year - 1)).GridDate;
                            etYearEnd = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, 11, year)).GridDate;

                            yearPeriod = getOrCreateFiscalYearPeriod(year, etYearStart.Year + "/" + etYearEnd.Year, etYearStart, etYearEnd);
                            monthStart = etYearStart;
                            for (int i = 1; i <= 12; i++)
                            {
                                DateTime nextMonthStart;
                                if (i == 12)
                                    nextMonthStart = yearPeriod.toDate;
                                else
                                {
                                    nextMonthStart = monthStart.AddMonths(1);
                                    nextMonthStart = new DateTime(nextMonthStart.Year, nextMonthStart.Month, 1);
                                }
                                monthPeriod = new AccountingPeriod();
                                monthPeriod.name = INTAPS.Ethiopic.EtGrDate.GetGrigMonthName(monthStart.Month) + " " + monthStart.Year;
                                monthPeriod.fromDate = monthStart;
                                monthPeriod.toDate = nextMonthStart;
                                monthPeriod.id = getOrCreateMonthPeriod(monthPeriod);
                                monthStart = nextMonthStart;
                            }
                            break;
                        case AccountPeriodStyle.GregJul1FiscalYearGregorianMonth:
                            int gYear=INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, 11, year-1)).GridDate.Year;
                            etYearStart = new DateTime(gYear, 7, 1);
                            etYearEnd = new DateTime(gYear+1, 7, 1);

                            yearPeriod = getOrCreateFiscalYearPeriod(year, etYearStart.Year + "/" + etYearEnd.Year, etYearStart, etYearEnd);
                            monthStart = etYearStart;
                            for (int i = 1; i <= 12; i++)
                            {
                                DateTime nextMonthStart;
                                if (i == 12)
                                    nextMonthStart = yearPeriod.toDate;
                                else
                                {
                                    nextMonthStart = monthStart.AddMonths(1);
                                }
                                monthPeriod = new AccountingPeriod();
                                monthPeriod.name = INTAPS.Ethiopic.EtGrDate.GetGrigMonthName(monthStart.Month       ) + " " + monthStart.Year;
                                monthPeriod.fromDate = monthStart;
                                monthPeriod.toDate = nextMonthStart;
                                monthPeriod.id = getOrCreateMonthPeriod(monthPeriod);
                                monthStart = nextMonthStart;
                            }
                            break;
                        
                        default:
                            throw new INTAPS.ClientServer.ServerUserMessage("Unsupported accounting period style");
                    }
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public void GenerateTaxDeclarationPeriod(int year)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    DateTime etYearStart = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, 11, year - 1)).GridDate;
                    DateTime etYearEnd = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, 11, year)).GridDate;
                    AccountingPeriod yearPeriod;
                    AccountingPeriod monthPeriod;
                    yearPeriod = getOrCreateFiscalYearPeriod(year, year.ToString(), etYearStart, etYearEnd);
                    int m = 11;
                    int nextM;
                    for (int i = 1; i <= 12; i++)
                    {
                        nextM = m + 1;
                        if (nextM == 13)
                            nextM = 1;

                        monthPeriod = new AccountingPeriod();
                        monthPeriod.fromDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, m, m < 11 ? year : year - 1)).GridDate;
                        monthPeriod.toDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, nextM, nextM < 12 ? year : year - 1)).GridDate;
                        monthPeriod.name = INTAPS.Ethiopic.EtGrDate.GetEtMonthNameEng(m) + " " + (m < 11 ? year.ToString() : (year - 1).ToString());
                        monthPeriod.id = getOrCreateMonthPeriod("AP_Tax_Declaration", monthPeriod);
                        m = nextM;
                    }

                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }

        private int getOrCreateMonthPeriod(AccountingPeriod monthPeriod)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    AccountingPeriod prd = AccountingPeriodHelper.GetPeriodForDate<AccountingPeriod>(dspWriter, "AP_Accounting_Month", monthPeriod.fromDate);
                    if (prd != null)
                        return prd.id;
                    return AccountingPeriodHelper.CreatePayPeriod<AccountingPeriod>(this.DBName, dspWriter, "AP_Accounting_Month", monthPeriod);
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        private int getOrCreateMonthPeriod(string accountingPeriodType, AccountingPeriod monthPeriod)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    AccountingPeriod prd = AccountingPeriodHelper.GetPeriodForDate<AccountingPeriod>(dspWriter, accountingPeriodType, monthPeriod.fromDate);
                    if (prd != null)
                        return prd.id;
                    return AccountingPeriodHelper.CreatePayPeriod<AccountingPeriod>(this.DBName, dspWriter, accountingPeriodType, monthPeriod);
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        private AccountingPeriod getOrCreateFiscalYearPeriod(int year, string yearName,DateTime etYearStart, DateTime etYearEnd)
        {
            AccountingPeriod yearPeriod;
            yearPeriod = INTAPS.Accounting.BDE.AccountingPeriodHelper.GetPeriodForDate<AccountingPeriod>(dspWriter, "AP_Accounting_Year", etYearStart);
            if (yearPeriod == null)
            {
                yearPeriod = new AccountingPeriod();
                yearPeriod.fromDate = etYearStart;
                yearPeriod.toDate = etYearEnd;
                yearPeriod.name = yearName;
                yearPeriod.id = INTAPS.Accounting.BDE.AccountingPeriodHelper.CreatePayPeriod<AccountingPeriod>(this.DBName, dspWriter, "AP_Accounting_Year", yearPeriod);
            }
            return yearPeriod;
        }

        public bool IsSupplierInvolvedInTransactions(string supplierCode)
        {
            TradeRelation rel = GetRelation(supplierCode);
            foreach (int a in rel.allAccountFields)
                if (a == -1)
                    continue;
                else
                    if (m_accounting.AccountHasTransaction<Account>(a))
                        return true;
            return false;
        }

        public bool IsCustomerInvolvedInTransactions(string customerCode)
        {
            return IsSupplierInvolvedInTransactions(customerCode);
        }


        internal void assertTradeRelationConflictingAccount(DateTime date,TradeRelation rel)
        {
            if (rel.PayableAccountID != -1 && rel.ReceivableAccountID != -1)
            {
                CostCenterAccount csa = m_accounting.GetCostCenterAccount(SysPars.mainCostCenterID, rel.PayableAccountID);
                if (csa == null)
                    return;
                double bal = m_accounting.GetNetBalanceAsOf(csa.id, 0, date);
                if (AccountBase.AmountEqual(bal, 0))
                    return;
                csa = m_accounting.GetCostCenterAccount(SysPars.mainCostCenterID, rel.ReceivableAccountID);
                if (csa == null)
                    return;
                double bal2 = m_accounting.GetNetBalanceAsOf(csa.id, 0, date);
                if (AccountBase.AmountEqual(bal2, 0))
                    return;
                throw new INTAPS.ClientServer.ServerUserMessage("It is not allowed to have balance on both payable and receivable accounts of a single entity");
            }
        }

        public bool isCatageoryCategoryOf(int parentCategory, int childCategory)
        {
            bool found;
            INTAPS.RDBMS.SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                found = this.isCatageoryCategoryOf(readerHelper, parentCategory, childCategory);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return found;
        }
        private bool isCatageoryCategoryOf(INTAPS.RDBMS.SQLHelper dsp, int parentCategory, int childCategory)
        {
            int cat = childCategory;
            if (parentCategory == -1)
            {
                return true;
            }
            do
            {
                if (childCategory == parentCategory)
                {
                    return true;
                }
                object parentID = dsp.ExecuteScalar("Select PID from ItemCategory where id=" + childCategory);
                if (!(parentID is int))
                {
                    throw new Exception(string.Concat(new object[] { "Category not in database found while traversing parents of ", cat, "\nThe offending category is ", childCategory }));
                }
                childCategory = (int)parentID;
            }
            while (childCategory != -1);
            return false;
        }        
    }
}