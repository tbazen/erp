using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Payroll;
using System.Data.SqlClient;
using System.Data;
using INTAPS.ClientServer;
using System.Net.NetworkInformation;
using INTAPS.RDBMS;
using INTAPS;
namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        String getAttachmentFileItemPath(AttachedFileItem item, AttachedFileItemImage image)
        {
            String path = System.Configuration.ConfigurationManager.AppSettings["attachmentPath"];
            if (string.IsNullOrEmpty(path))
                throw new ServerUserMessage("Attachment storage path is not configured on the server.");
            if (!System.IO.Directory.Exists(path))
                throw new ServerUserMessage("Attachment storage path {0} doesn't exist.", path);
            String file = path + "\\" + image.fileItemID.ToString("0000000000000") +"_"+image.face+(new System.IO.FileInfo(path + "\\" + image.fileName)).Extension;
            return file;
        }
        void assertAttachmentFileGroupGroup(INTAPS.RDBMS.SQLHelper helper, int fileGroupID)
        {
            int[] ordern1 = helper.GetColumnArray<int>("Select ordern from {0}.dbo.AttachedFileItem where attachmentGroupID=" + fileGroupID);
            int[] ordern2 = helper.GetColumnArray<int>("Select ordern from {0}.dbo.AttachedFileGroup where parentID=" + fileGroupID);
            if (ordern1.Length > 0 && ordern2.Length > 0)
                throw new ServerUserMessage("Attachment files and attachment file groups can be mixed under a single attachment file group");
            foreach(int[] ordern in new int[][]{ordern1,ordern2})
            {
                int prev = 0;
                foreach(int n in ordern)
                {
                    if (n != prev + 1)
                        throw new ServerUserMessage("File group childs have invalid ordern field");
                    prev = n;
                }
            }
        }
        int addFileItem(int AID,AttachedFileItem item)
        {
            assertImagesList(item);
            item.id = AutoIncrement.GetKey("fileItemID");
            dspWriter.InsertSingleTableRecord(this.DBName, item, new string[] { "__AID" }, new object[] { AID });
            foreach (AttachedFileItemImage image in item.images)
            {
                image.fileItemID = item.id;
                String file = getAttachmentFileItemPath(item,image);
                System.IO.File.WriteAllBytes(file, image.fileImage);
                
                dspWriter.InsertSingleTableRecord(this.DBName, image, new string[] { "__AID" }, new object[] { AID });
            }
            return item.id;
        }
        //bde
        public AttachedFileGroup getAttachedFileGroup(int id)
        {
            return base.getSingleObjectIntID<AttachedFileGroup>(id);
        }
        
        public int attachFile(int AID, AttachedFileGroup fileGroup, IEnumerable<AttachedFileItem> items)
        {
            fileGroup.id = AutoIncrement.GetKey("fileGroupID");
            if (fileGroup.parentID == -1)
                fileGroup.ordern = 1;
            else
            {
                if (getAttachedFileGroup(fileGroup.parentID) == null)
                    throw new ServerUserMessage("The parentID {0} is not valid", fileGroup.parentID);
                object max = dspWriter.ExecuteScalar("Select max(ordern) from {0}.dbo.AttachedFileGroup where parentID={1}".format(this.DBName, fileGroup.parentID));
                if (max is int)
                    fileGroup.ordern = (int)max + 1;
                else
                    fileGroup.ordern = 1;
            }
            dspWriter.InsertSingleTableRecord(this.DBName, fileGroup, new string[] { "__AID" }, new object[] { AID });
            
            int ordern = 1;
            foreach (AttachedFileItem item in items)
            {
                item.ordern = ordern++;
                item.attachmentGroupID = fileGroup.id;
                item.id = addFileItem(AID, item);
            }
            return fileGroup.id;
        }
        //bde
        public int attachFile(int AID,int documentID,AttachedFileGroup fileGroup,IEnumerable<AttachedFileItem> items)
        {
            fileGroup.id = attachFile(AID, fileGroup, items);
            AttachedFileGroupDocument fg = new AttachedFileGroupDocument();
            fg.documentID = documentID;
            fg.attachedFileGroupID = fileGroup.id;
            object _ordern=dspWriter.ExecuteScalar("Select max(ordern) from {0}.dbo.AttachedFileGroupDocument where documentID={1}".format(this.DBName, documentID));
            fg.ordern = _ordern is int ? (int)_ordern + 1 : 1;
            dspWriter.InsertSingleTableRecord(this.DBName, fg, new string[] { "__AID" }, new object[] { AID });
            return fileGroup.id;
        }

        //bde
        public List<int> attachMultipeFile(int AID,int documentID,IEnumerable<AttachedFileGroup> fileGroup,IEnumerable<IEnumerable<AttachedFileItem>> items)
        {
            List<int> ret = new List<int>();
            IEnumerator<AttachedFileGroup> fg = fileGroup.GetEnumerator();
            IEnumerator<IEnumerable<AttachedFileItem>> item = items.GetEnumerator();
            while(fg.MoveNext() && item.MoveNext())
            {
                ret.Add(attachFile(AID,documentID,fg.Current, item.Current));
            }
            return ret;
        }
        //bde
        public void attachToExistingDocument(int AID,int documentID, int[] fileGroupID)
        {
            foreach(int fid in fileGroupID)
            {
                AttachedFileGroupDocument fg = new AttachedFileGroupDocument();
                fg.documentID = documentID;
                fg.attachedFileGroupID = fid;
                dspWriter.InsertSingleTableRecord(this.DBName, fg, new string[] { "__AID" }, new object[] { AID });
            }
        }
        //bde
        public void rearrangeAttachedFileItems(int AID,int fileGroupID,int[]oldOrdern,int[]newOrdern)
        {
            if (getAttachedFileGroup(fileGroupID) == null)
                throw new ServerUserMessage("Attachment group id {0} doesn't exist", fileGroupID);
            for(int i=0;i<oldOrdern.Length;i++)
            {
                dspWriter.logDeletedData(AID, this.DBName + ".dbo.AttachedFileItem", "attachmentGroupID=" + fileGroupID + " and ordern=" + oldOrdern[i]);
                dspWriter.Update(this.DBName, "AttachedFileItem", new string[] { "ordern" }, new object[] { newOrdern[i] }, "attachmentGroupID=" + fileGroupID + " and ordern=" + oldOrdern[i]);
            }
            assertAttachmentFileGroupGroup(dspWriter, fileGroupID);
        }
        //bde
        public int replaceAttachmentFileItem(int AID,AttachedFileItem item)
        {
            AttachedFileItem existing = getAttachedFileItem(item.id);
            if (existing == null)
                throw new ServerUserMessage("Attachment file ID {0} doesn't exist", item.id);
            item.attachmentGroupID = existing.attachmentGroupID;
            item.ordern = existing.ordern;
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.AttachedFileItem", "id=" + item.id);
            dspWriter.Delete(this.DBName, "AttachedFileItem", "id=" + item.id);
            return addFileItem(AID, item);
        }
        //bde
        public void deleteAttachmentFileItem(int AID, int attachmentFileID)
        {
            AttachedFileItem existing = getAttachedFileItem(attachmentFileID);
            if (existing == null)
                throw new ServerUserMessage("Attachment file ID {0} doesn't exist", attachmentFileID);
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.AttachedFileItem", "id=" + attachmentFileID);
            dspWriter.ExecuteNonQuery("Update {0}.dbo.AttachedFileItem set ordern=ordern-1 where ordern>{1} and attachmentGroupID={2}".format(this.DBName, existing.ordern,existing.attachmentGroupID));
            assertAttachmentFileGroupGroup(dspWriter,existing.attachmentGroupID);
        }
        //bde
        public void detachAttachmentFileGroup(int AID,int attachmentFileGroupID,int documentID)
        {
            int count=(int)dspWriter.ExecuteScalar("Select count(*) from {0}.dbo.AttachedFileGroupDocument where attachedFileGroupID={1}".format(this.DBName,attachmentFileGroupID));
            if (count == 0)
                throw new ServerUserMessage("Invalid file attachment");
            dspWriter.ExecuteScalar("delete from {0}.dbo.AttachedFileGroupDocument where attachedFileGroupID={1} and documentID={2}".format(this.DBName, attachmentFileGroupID, documentID));
            if (count == 1)
                deleteAttachmentFileGroup(AID, attachmentFileGroupID);
        }
        
        //bde
        public void deleteAttachmentFileGroup(int AID, int attachmentFileGroupID)
        {
            AttachedFileGroup[] childs = dspWriter.GetSTRArrayByFilter<AttachedFileGroup>("parentID=" + attachmentFileGroupID);
            if (childs.Length > 0)
                throw new ServerUserMessage("It is not allowed to delete attachment file groups with child attachment file group");
            AttachedFileItem[] items = dspWriter.GetSTRArrayByFilter<AttachedFileItem>("attachmentGroupID=" + attachmentFileGroupID);
            foreach(AttachedFileItem item in items)
                deleteAttachmentFileItem(AID,item.id);
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.AttachedFileGroup", "id=" + attachmentFileGroupID);
            dspWriter.DeleteSingleTableRecrod<AttachedFileGroup>(this.DBName, attachmentFileGroupID);
        }
        //bde
        public AttachedFileGroup[] getAttachedFileGroupForDocument(int documentID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<AttachedFileGroup>("id in (Select attachedFileGroupID from {0}.dbo.AttachedFileGroupDocument where documentID={1})".format(this.DBName, documentID));
            }
            catch
            {
                base.ReleaseHelper(reader);
                throw;
            }
        }
        //bde
        public AttachedFileItem[] getAttachedFileItems(int fileGroupID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                AttachedFileItem[] ret= reader.GetSTRArrayByFilter<AttachedFileItem>("attachmentGroupID="+fileGroupID);
                foreach(AttachedFileItem item in ret)
                {
                    item.images = reader.GetSTRArrayByFilter<AttachedFileItemImage>("fileItemID=" + item.id);
                }
                return ret;
            }
            catch
            {
                base.ReleaseHelper(reader);
                throw;
            }
        }
        //bde
        public AttachedFileItem getAttachedFileItem(int attachedFileItemID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                AttachedFileItem item= getSingleObjectIntID<AttachedFileItem>(attachedFileItemID);
                item.images=reader.GetSTRArrayByFilter<AttachedFileItemImage>("fileItemID="+attachedFileItemID);
                return item;
            }
            catch
            {
                base.ReleaseHelper(reader);
                throw;
            }            
        }
        //bde
        public AttachedFileItemImage getAttachedFileItemImageInfo(int attachedFileItemID,AttachmentPaperFace face)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                AttachedFileItemImage[] ret= reader.GetSTRArrayByFilter<AttachedFileItemImage>("fileItemID=" + attachedFileItemID+
                    " and face="+(int)face);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            catch
            {
                base.ReleaseHelper(reader);
                throw;
            }
        }
        //bde
        public byte[] getAttachedFileItemImageData(int attachmentFileID,AttachmentPaperFace face)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                AttachedFileItemImage image= getAttachedFileItemImageInfo(attachmentFileID,face);
                if (image== null)
                    return null;
                AttachedFileItem item=getAttachedFileItem(image.fileItemID);
                String fileName = getAttachmentFileItemPath(item,image);
                return System.IO.File.ReadAllBytes(fileName);
            }
            catch
            {
                base.ReleaseHelper(reader);
                throw;
            }
        }
        //bde
        public byte[] getAttachedFileItemImageResized(int attachmentFileID, AttachmentPaperFace face, int width, int height)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                AttachedFileItemImage imageInfo = getAttachedFileItemImageInfo(attachmentFileID, face);
                if (imageInfo == null)
                    return null;
                AttachedFileItem item = getAttachedFileItem(imageInfo.fileItemID);
                String fileName = getAttachmentFileItemPath(item, imageInfo);
                System.Drawing.Image image = System.Drawing.Image.FromFile(fileName);
                
                System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(width, height);
                System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(bitmap);
                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                if ((image.Width * height) > (width * image.Height))
                {
                    int newHeight = (image.Height * width) / image.Width;
                    graphics.DrawImage(image, new System.Drawing.Rectangle(0, (height - newHeight) / 2, width, newHeight));
                }
                else
                {
                    int newWidth = (image.Width * height) / image.Height;
                    graphics.DrawImage(image, new System.Drawing.Rectangle((width - newWidth) / 2, 0, newWidth, height));
                }
                graphics.Dispose();
                System.Drawing.Imaging.ImageCodecInfo[] imageEncoders = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();
                System.Drawing.Imaging.ImageCodecInfo encoderInfo = null;
                foreach (System.Drawing.Imaging.ImageCodecInfo info in imageEncoders)
                {
                    if (info.MimeType.Equals("image/jpeg"))
                    {
                        encoderInfo = info;
                    }
                }
                if (encoderInfo == null)
                    throw new ServerUserMessage("Codec not found for image/jpeg not found.");
            
                System.Drawing.Imaging.EncoderParameters parameters = new System.Drawing.Imaging.EncoderParameters(1);
                parameters.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 80);
                System.IO.MemoryStream stream2 = new System.IO.MemoryStream();
                bitmap.Save(stream2, System.Drawing.Imaging.ImageFormat.Jpeg);
                byte[] destinationArray = new byte[stream2.Length];
                Array.Copy(stream2.GetBuffer(), destinationArray, stream2.Length);
                return destinationArray;
                
            }
            catch
            {
                base.ReleaseHelper(reader);
                throw;
            }
        }

        //bde
        public void updateAttachedFileGroupInfo(int AID,AttachedFileGroup info)
        {
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.AttachedFileGroup", "id=" + info.id);
            dspWriter.UpdateSingleTableRecord(this.DBName, info,new string[]{"__AID"},new object[]{AID});
        }
        void assertImagesList(AttachedFileItem item)
        {
            if (item.images.Length < 2)
                return;
            if (item.images.Length > 2)
                throw new ServerUserMessage("Maximum of two faces are allowed per paper");
            if(item.images[0].face!=AttachmentPaperFace.Paper_Front
                || item.images[1].face!=AttachmentPaperFace.Paper_Back)
                throw new ServerUserMessage("If you are attaching two faces of a paper the first should be front face and the second one should be back face");
        }

        public void updateAttachedFileItem(int AID,AttachedFileItem info)
        {
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.AttachedFileItem", "id=" + info.id);
            dspWriter.UpdateSingleTableRecord(this.DBName, info, new string[] { "__AID" }, new object[] { AID });

            foreach(AttachedFileItemImage img in info.images)
            {
                img.fileItemID = info.id;
                dspWriter.UpdateSingleTableRecord(this.DBName, img, new string[] { "__AID" }, new object[] { AID });
            }
        }
        //bde
        public AttachmentType[] getAllFileAttachmentTypes()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<AttachmentType>(null);
            }
            catch
            {
                base.ReleaseHelper(reader);
                throw;
            }
        }
        //bde
        public int createSupperGroup(int AID,AttachedFileGroup supergroup,AttachedFileGroup [] subgroups)
        {
            supergroup.id = AutoIncrement.GetKey("fileGroupID");
            if (supergroup.parentID == -1)
                supergroup.ordern = 1;
            else
            {
                if (getAttachedFileGroup(supergroup.parentID) == null)
                    throw new ServerUserMessage("The parentID {0} is not valid", supergroup.parentID);
                object max = dspWriter.ExecuteScalar("Select max(ordern) from {0}.dbo.AttachedFileGroup where parentID={1}".format(this.DBName, supergroup.parentID));
                if (max is int)
                    supergroup.ordern = (int)max + 1;
                else
                    supergroup.ordern = 1;
            }
            dspWriter.InsertSingleTableRecord(this.DBName, supergroup, new string[] { "__AID" }, new object[] { AID });

            int ordern = 1;
            foreach (AttachedFileGroup group in subgroups)
            {
                group.ordern = ordern++;
                group.parentID = supergroup.id;
                updateAttachedFileGroupInfo(AID, group);
            }
            assertAttachmentFileGroupGroup(dspWriter, supergroup.id);
            return supergroup.id;
        }
        
    }
}
