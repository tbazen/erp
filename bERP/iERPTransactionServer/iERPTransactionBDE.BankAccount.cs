﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using System.Data.SqlClient;
using System.Data;
using INTAPS.Payroll.BDE;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE : INTAPS.ClientServer.BDEBase
    {
        public BankAccountInfo GetBankAccount(int mainAccountID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                BankAccountInfo[] _ret = dspReader.GetSTRArrayByFilter<BankAccountInfo>("mainCsAccount=" + mainAccountID);
                if (_ret.Length == 0)
                    return null;
                return _ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public BankAccountInfo[] GetBankAccountByCostCenter(int costCenterID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                string cr = "";
                foreach (CostCenter cs in m_accounting.GetLeafAccounts<CostCenter>(costCenterID))
                {
                    if (string.IsNullOrEmpty(cr))
                        cr = cs.id.ToString();
                    else
                        cr += "," + cs.id;
                }
                BankAccountInfo[] _ret = dspReader.GetSTRArrayByFilter<BankAccountInfo>(
                    string.Format("exists(Select * from {0}.dbo.CostCenterAccount where costCenterID in ({1}) and id=mainCsAccount)", m_accounting.DBName, cr)
                    );
                return _ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public BankAccountInfo[] GetAllBankAccounts()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<BankAccountInfo>(null);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        
        public void DeleteBankAccount(int AID, int accountID)
        {
            lock (dspWriter)
            {
                INTAPS.ClientServer.ObjectFilters.GetFilterInstance<BankAccountChangeFilter>().allowAction(AID);

                try
                {
                    dspWriter.BeginTransaction();
                    BankAccountInfo existing = GetBankAccount(accountID);
                    if (existing == null)
                        throw new INTAPS.ClientServer.ServerUserMessage("Bank account ID:" + accountID + " not found");
                    logExistingBankAccount(AID, existing);
                    CostCenterAccount csa = m_accounting.GetCostCenterAccount(accountID);
                    if (csa != null)
                        deleteAccountIFExists<Account>(AID, csa.accountID);
                    csa = m_accounting.GetCostCenterAccount(existing.bankServiceChargeCsAccountID);
                    if (csa != null) 
                        deleteAccountIFExists<Account>(AID, csa.accountID);
                    Project[] projects = GetAllProjects();
                    foreach (Project project in projects)
                    {
                        if (project.projectData.bankAccounts.Contains(accountID))
                        {
                            project.projectData.bankAccounts.Remove(accountID);
                            RegisterProject(AID, project,true);
                            break;
                        }
                    }
                    dspWriter.DeleteSingleTableRecrod<BankAccountInfo>(m_DBName, accountID);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<BankAccountChangeFilter>().resetAllowAction();
                }
            }
        }

        private void deleteAccountIFExists<AccountType>(int AID, int accountID) where AccountType:AccountBase, new()
        {
            AccountType acnt;
            acnt = m_accounting.GetAccount<AccountType>(accountID);
            if(acnt!=null)
                m_accounting.DeleteAccount<AccountType>(AID, accountID, true);
        }
        public int CreateBankAccount(int AID,BankAccountInfo account, int costCenterID)
        {
            lock (dspWriter)
            {
                int bankMainCsAccount;
                dspWriter.setReadDB(this.DBName);
                INTAPS.ClientServer.ObjectFilters.GetFilterInstance<BankAccountChangeFilter>().allowAction(AID);

                try
                {
                    dspWriter.BeginTransaction();
                    BankAccountInfo existing = GetBankAccount(account.mainCsAccount);
                    if (existing == null || account.mainCsAccount==-1)
                    {
                        #region main bank account
                        string nextMainAccountCode = GetNextBankAccountCode(SysPars.mainBankAccountID);
                        string bankName = account.ToString();
                        Account bankAccount = CollectBankAccount(SysPars.mainBankAccountID, nextMainAccountCode, bankName);
                        #endregion

                        #region service charge bank account
                        string nextBankServiceChargeCode = GetNextBankAccountCode(SysPars.bankServiceChargeAccountID);
                        string bankServiceChargeName = "Bank Service Charge-" + account.ToString();
                        Account bankServiceChargeAccount = CollectBankAccount(SysPars.bankServiceChargeAccountID, nextBankServiceChargeCode, bankServiceChargeName);
                        #endregion
                        int mainBankAccountID = m_accounting.CreateAccount<Account>(AID, bankAccount);
                        int serviceChargeBankAccountID = m_accounting.CreateAccount<Account>(AID, bankServiceChargeAccount);
                        #region Add accounts to cost center
                        int mainBankCsAccountID = m_accounting.CreateCostCenterAccount(AID, costCenterID, mainBankAccountID);
                        int bankServiceChargeCsAccountID = m_accounting.CreateCostCenterAccount(AID, costCenterID, serviceChargeBankAccountID);
                        account.mainCsAccount = mainBankCsAccountID;
                        account.bankServiceChargeCsAccountID = bankServiceChargeCsAccountID;
                        #endregion

                        dspWriter.InsertSingleTableRecord(m_DBName, account, new string[] { "__AID" }, new object[] { AID });
                        bankMainCsAccount = mainBankCsAccountID;
                    }
                    else
                    {
                        logExistingBankAccount(AID, existing);
                        #region main bank Account
                        CostCenterAccount csAccount = m_accounting.GetCostCenterAccount(account.mainCsAccount);
                        Account mainBankAccount = m_accounting.GetAccount<Account>(csAccount.accountID);
                        mainBankAccount.Name = account.ToString();
                        if (mainBankAccount.Status==AccountStatus.Activated)
                        {
                            mainBankAccount.DeactivateDate = DateTime.MaxValue.Date;
                        }
                        m_accounting.UpdateAccount(AID, mainBankAccount);
                        #endregion

                        #region bank service charge account
                        CostCenterAccount csAccount2 = m_accounting.GetCostCenterAccount(account.bankServiceChargeCsAccountID);
                        Account serviceChargeBankAccount = m_accounting.GetAccount<Account>(csAccount2.accountID);
                        serviceChargeBankAccount.Name = "Bank Service Charge-" + account;
                        if (serviceChargeBankAccount.Status == AccountStatus.Activated)
                        {
                            serviceChargeBankAccount.DeactivateDate = DateTime.MaxValue.Date;
                        }
                        m_accounting.UpdateAccount(AID, serviceChargeBankAccount);
                        #endregion
                        dspWriter.UpdateSingleTableRecord(m_DBName, account, new string[] { "__AID" }, new object[] { AID });
                        bankMainCsAccount = existing.mainCsAccount;
                    }
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<BankAccountChangeFilter>().resetAllowAction();
                    dspWriter.restoreReadDB();
                }
                return bankMainCsAccount;
            }
        }

        private void logExistingBankAccount(int AID, BankAccountInfo existing)
        {
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.BankAccountInfo", "mainCsAccount=" + existing.mainCsAccount);
        }

        private Account CollectBankAccount(int parentBankAccountID, string nextMainBankCode, string bankName)
        {
            Account bankAccount = new Account();
            bankAccount.ActivateDate = DateTime.Now.Date;
            bankAccount.DeactivateDate = DateTime.MaxValue.Date;
            bankAccount.Code = nextMainBankCode;
            bankAccount.CreationDate = DateTime.Now.Date;
            bankAccount.CreditAccount = false;
            bankAccount.Name = bankName;
            bankAccount.PID = parentBankAccountID;
            bankAccount.Status = AccountStatus.Activated;
            return bankAccount;
        }
        private string GetNextBankAccountCode(int parentBankAccountID)
        {
            string code = GetBankParentAccountCode(parentBankAccountID);
            int codeSuffix = GetBankLastChildAccountSuffix(parentBankAccountID);
            if (SysPars.BankAccountSuffixCodeFormat == null)
                throw new ServerUserMessage("Format for Bank Account Suffix Code is not configured in settings. Please configure and try again");

            return TSConstants.FormatBankCode(code, codeSuffix, SysPars.BankAccountSuffixCodeFormat);

        }
        private string GetBankParentAccountCode(int parentBankAccountID)
        {
            Account account = m_accounting.GetAccount<Account>(parentBankAccountID);
            return account.Code;
        }
        private int GetBankLastChildAccountSuffix(int parentBankAccountID)
        {
            Account[] bankAccounts = m_accounting.GetLeafAccounts<Account>(parentBankAccountID);
            if (bankAccounts != null)
            {
                if (bankAccounts.Length == 0
                    || (bankAccounts.Length == 1 && bankAccounts[0].id == parentBankAccountID))
                {
                    return 1;
                }
                string code = bankAccounts[bankAccounts.Length - 1].Code;
                if (code.Contains("-"))
                {
                    string[] splittedCode = code.Split('-');
                    int codeSuffix = int.Parse(splittedCode[splittedCode.Length - 1]);
                    return codeSuffix + 1;
                }
                throw new INTAPS.ClientServer.ServerUserMessage("Account Code format not recognized");
            }
            else
            {
                return 1; //start from 001
            }
        }
       
        public void ActivateBankAccount(int AID, int mainCsAccount)
        {
            lock (dspWriter)
            {
                INTAPS.ClientServer.ObjectFilters.GetFilterInstance<BankAccountChangeFilter>().allowAction(AID);
                BankAccountInfo bankAccount = GetBankAccount(mainCsAccount);
                try
                {
                    dspWriter.BeginTransaction();
                    CostCenterAccount mainAccount = m_accounting.GetCostCenterAccount(mainCsAccount);
                    CostCenterAccount bankservChargeAccount = m_accounting.GetCostCenterAccount(bankAccount.bankServiceChargeCsAccountID);
                    m_accounting.ActivateAcount<Account>(AID, mainAccount.accountID, DateTime.Now.Date);
                    m_accounting.ActivateAcount<Account>(AID, bankservChargeAccount.accountID, DateTime.Now.Date);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<BankAccountChangeFilter>().resetAllowAction();
                }
            }
        }
        public void DeactivateBankAccount(int AID, int mainCsAccount)
        {
            lock (dspWriter)
            {
                INTAPS.ClientServer.ObjectFilters.GetFilterInstance<BankAccountChangeFilter>().allowAction(AID);
                BankAccountInfo bankAccount = GetBankAccount(mainCsAccount);

                try
                {
                    dspWriter.BeginTransaction();
                    CostCenterAccount mainAccount = m_accounting.GetCostCenterAccount(mainCsAccount);
                    CostCenterAccount bankservChargeAccount = m_accounting.GetCostCenterAccount(bankAccount.bankServiceChargeCsAccountID);
                    m_accounting.DeactivateAccount<Account>(AID, mainAccount.accountID, DateTime.Now.Date);
                    m_accounting.DeactivateAccount<Account>(AID, bankservChargeAccount.accountID, DateTime.Now.Date);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<BankAccountChangeFilter>().resetAllowAction();
                }
            }
        }
        public bool reconciliationDone(DateTime time)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                object _ID = helper.ExecuteScalar(string.Format("Select Top 1 ID from {0}.dbo.Document where tranTicks>{1} and DocumentTypeID={2} order by tranTicks"
                   , m_accounting.DBName, time.Ticks, m_accounting.GetDocumentTypeByType(typeof(BankReconciliationDocument)).id));
                return _ID is int;
            }

            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public BankReconciliationDocument getLastBankReconciliation(int bankID,DateTime date)
        {
            INTAPS.RDBMS.SQLHelper reader = m_accounting.GetReaderHelper();
            try
            {
                BankAccountInfo ba = GetBankAccount(bankID);
                if (ba == null)
                    throw new ServerUserMessage("Invalid bank account ID:" + bankID);
                int[] allDocs = reader.GetColumnArray<int>(string.Format("Select ID from {0}.dbo.Document where tranTicks<={1} and DocumentTypeID={2} order by tranTicks desc", m_accounting.DBName, date.Ticks, m_accounting.GetDocumentTypeByType(typeof(BankReconciliationDocument)).id), 0);
                foreach (int docId in allDocs)
                {
                    BankReconciliationDocument bdoc = m_accounting.GetAccountDocument(docId, true) as BankReconciliationDocument;
                    if (bdoc.bankID != bankID)
                        continue;
                    return bdoc;
                }
                return null;
            }
            finally
            {
                m_accounting.ReleaseHelper(reader);
            }
        }
    }
}
