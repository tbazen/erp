﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using System.Data.SqlClient;
using System.Data;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE : INTAPS.ClientServer.BDEBase
    {
        // BDE exposed
        public CashAccountCategory[] getChildCashCategories(int categoryID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<CashAccountCategory>("categoryID=" + categoryID);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public CashAccountCategory getCashCategory(int categoryID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                CashAccountCategory [] ret=reader.GetSTRArrayByFilter<CashAccountCategory>("id=" + categoryID);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public int createCashCategory(int AID, CashAccountCategory cat)
        {
            lock(dspWriter)
            {
                CashAccountCategory parentCat=null;
                if (cat.categoryID != -1)
                {
                    if ((parentCat=getCashCategory(cat.categoryID)) == null)
                        throw new ServerUserMessage("Invalid parent category ID {0}", cat.categoryID);
                    if(parentCat.accountID!=cat.accountID && this.Accounting.IsControlOf<Account>(parentCat.accountID,cat.accountID))
                        throw new ServerUserMessage("The account of a catagory should be the same as that of its parent or child of the parent account");
                }
                if (cat.id == -1)
                    cat.id = AutoIncrement.GetKey("bERP.CashCategoryID");
                if (Accounting.GetAccount<Account>(cat.accountID) == null)
                    throw new ServerUserMessage("Invalid account id {0}", cat.accountID);
                dspWriter.InsertSingleTableRecord(this.DBName, cat,new string[]{"__AID"},new object[]{AID});
                return cat.id;
            }
        }
        // BDE exposed
        public CashAccount[] getCashAccountsByCategory(int categoryID)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                CashAccount[] ret=helper.GetSTRArrayByFilter<CashAccount>("categoryID="+categoryID);
                return ret;
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        // BDE exposed
        public void updateCashCategory(int AID, CashAccountCategory cat)
        {
            lock (dspWriter)
            {
                CashAccountCategory oldCat = getCashCategory(cat.id);
                if(oldCat==null)
                    throw new ServerUserMessage("Invalid category ID {0}", cat.id);
                oldCat.name = cat.name;
                if (oldCat.accountID != cat.accountID)
                    if (getCashAccountsByCategory(cat.id).Length > 0)
                        throw new ServerUserMessage("You can't change account of a cash category with cash accounts");
                oldCat.accountID = cat.accountID;
                if (Accounting.GetAccount<Account>(cat.accountID) == null)
                    throw new ServerUserMessage("Invalid account id {0}", cat.accountID);
                dspWriter.logDeletedData(AID, this.DBName + ".dbo.CashAccountCategory", "id=" + cat.id);
                dspWriter.UpdateSingleTableRecord(this.DBName, oldCat, new string[] { "__AID" }, new object[] { AID });
            }
        }
        // BDE exposed
        public void deleteCashCategory(int AID, int catID)
        {
            lock (dspWriter)
            {
                CashAccountCategory oldCat = getCashCategory(catID);
                if (oldCat == null)
                    throw new ServerUserMessage("Invalid category ID {0}", catID);
                if (getCashAccountsByCategory(catID).Length > 0)
                    throw new ServerUserMessage("You can't delete cash category with cash accounts");
                dspWriter.logDeletedData(AID, this.DBName + ".dbo.CashAccountCategory", "id=" + catID);
                dspWriter.DeleteSingleTableRecrod<CashAccountCategory>(this.DBName, catID);
            }
        }
        public CashAccount[] GetAllCashAccounts(bool includeExpenseAccounts)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                CashAccount[] _ret=helper.GetSTRArrayByFilter<CashAccount>(null);
                if (!includeExpenseAccounts || m_Payroll.SysPars.staffExpenseAdvanceAccountID==-1)
                    return _ret;
                List<CashAccount> ret = new List<CashAccount>();
                ret.AddRange(_ret);
                int N;
                foreach (Account account in m_accounting.GetChildAcccount<Account>(m_Payroll.SysPars.staffExpenseAdvanceAccountID,0, -1, out N))
                {
                    CostCenterAccount csas = m_accounting.GetCostCenterAccount(m_Payroll.SysPars.mainCostCenterID, account.id);
                    if (csas != null)
                    {
                        CashAccount expCash = new CashAccount();
                        expCash.code = account.Code;
                        expCash.name = account.Name;
                        expCash.csAccountID = csas.id;
                        expCash.isStaffExpenseAdvance = true;
                        ret.Add(expCash);
                    }
                   
                }
                return ret.ToArray();

            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public CashAccount GetStaffExpenseAdvanceCashAccount(int employeeID)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                Account account = m_accounting.GetAccount<Account>(m_Payroll.GetEmployeeSubAccount(employeeID, m_Payroll.SysPars.staffExpenseAdvanceAccountID));
                if (account == null)
                    return null;
                    CostCenterAccount csas = m_accounting.GetCostCenterAccount(m_Payroll.SysPars.mainCostCenterID, account.id);
                    if (csas != null)
                    {
                        CashAccount expCash = new CashAccount();
                        expCash.code = account.Code;
                        expCash.name = account.Name;
                        expCash.csAccountID = csas.id;
                        expCash.isStaffExpenseAdvance = true;
                    return expCash;
                    }
                return null;

              
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public CashAccount GetCashAccount(string code)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                CashAccount[] _ret = dspReader.GetSTRArrayByFilter<CashAccount>("code='" + code + "'");
                if (_ret.Length == 0)
                    return null;
                return _ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public CashAccount GetCashAccount(int csAccountID)
        {
            return GetCashAccount(csAccountID, false);
        }
        public CashAccount GetCashAccount(int csAccountID,bool includeExpenseAdvance)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                CashAccount[] _ret = dspReader.GetSTRArrayByFilter<CashAccount>("csAccountID=" + csAccountID + "");
                if (_ret.Length > 0)
                    return _ret[0];


                if (includeExpenseAdvance)
                {
                    CostCenterAccount csas = m_accounting.GetCostCenterAccount(csAccountID);
                    if (m_accounting.IsControlOf<Account>(this.Payroll.SysPars.staffExpenseAdvanceAccountID, csas.accountID))
                    {
                        Account account = m_accounting.GetAccount<Account>(csas.accountID);
                        CashAccount expCash = new CashAccount();
                        expCash.code = account.Code;
                        expCash.name = account.Name;
                        expCash.csAccountID = csas.id;
                        expCash.isStaffExpenseAdvance = true;
                        return expCash;
                    }
                }
                return null;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public CashAccount[] GetCashAccountsByCostCenter(int costCenterID,bool includeStaffCash)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                string cr="";
                CostCenter[] costCenters= m_accounting.GetLeafAccounts<CostCenter>(costCenterID);
                foreach (CostCenter cs in costCenters)
                {
                    if (string.IsNullOrEmpty(cr))
                        cr = cs.id.ToString();
                    else
                        cr += "," + cs.id;
                }
                CashAccount[] _ret = dspReader.GetSTRArrayByFilter<CashAccount>(
                    string.Format("exists(Select * from {0}.dbo.CostCenterAccount where costCenterID in ({1}) and id=csAccountID)", m_accounting.DBName, cr)
                    );
                if (!includeStaffCash)
                    return _ret;
                List<CashAccount> ret = new List<CashAccount>();
                ret.AddRange(_ret);
                int N;
                foreach (Account account in m_accounting.GetChildAcccount<Account>(m_Payroll.SysPars.staffExpenseAdvanceAccountID, 0, -1, out N))
                {
                    foreach (CostCenter cs in costCenters)
                    {
                        CostCenterAccount csa = m_accounting.GetCostCenterAccount(cs.id, account.id);
                        if (csa != null)
                        {
                            CashAccount expCash = new CashAccount();
                            expCash.code = account.Code;
                            expCash.name = account.Name;
                            expCash.csAccountID = csa.id;
                            expCash.isStaffExpenseAdvance = true;
                            ret.Add(expCash);
                        }
                    }
                }
                return ret.ToArray();
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public void DeleteCashAccount(int AID, string code)
        {
            lock (dspWriter)
            {
                try
                {
                    dspWriter.BeginTransaction();
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<CashAccountChangeFilter>().allowAction(AID);
                    CashAccount existing = GetCashAccount(code);
                    if (existing == null)
                        throw new INTAPS.ClientServer.ServerUserMessage("Cost Center Cash account ID:" + existing.csAccountID + " not found");
                    logExistingCashAccount(AID, existing);
                    CostCenterAccount csa = m_accounting.GetCostCenterAccount(existing.csAccountID);
                    if(csa!=null)
                        deleteAccountIFExists<Account>(AID, csa.accountID);
                    Project[] projects = GetAllProjects();
                    foreach (Project project in projects)
                    {
                        if (project.projectData.cashAccounts.Contains(code))
                        {
                            project.projectData.cashAccounts.Remove(code);
                            RegisterProject(AID, project, true);
                            break;
                        }
                    }
                    dspWriter.DeleteSingleTableRecrod<CashAccount>(m_DBName, code);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<CashAccountChangeFilter>().resetAllowAction();
                }
            }
        }

        
        public string CreateCashAccount(int AID, CashAccount account, int costCenterID)
        {
            lock (dspWriter)
            {
                string code;
                dspWriter.setReadDB(this.DBName);
                INTAPS.ClientServer.ObjectFilters.GetFilterInstance<CashAccountChangeFilter>().allowAction(AID);
                try
                {
                    dspWriter.BeginTransaction();
                    CashAccount existing = GetCashAccount(account.code);
                    CashAccountCategory cat=getCashCategory(account.categoryID);
                    if (existing == null || account.code == null)
                    {
                        #region Cash account
                        Account parentAccount = Accounting.GetAccount<Account>(cat.accountID);
                        if (parentAccount == null)
                            throw new ServerUserMessage("Account is not set for the cash account category");
                        string parentCode = parentAccount.Code;
                        if (SysPars.CashAccountSuffixCodeFormat == null)
                            throw new ServerUserMessage("Format for Cash Account Suffix Code is not configured in settings. Please configure and try again");
                        string nextCashAccountCode = TSConstants.FormatCashCode(parentCode, AutoIncrement.GetKey("iERP.CashAccountID"), SysPars.CashAccountSuffixCodeFormat);

                        string cashAccountName = account.name;
                        Account cashAccount = CollectCashAccount(cat.accountID, nextCashAccountCode, cashAccountName);
                        #endregion

                        int cashAccountID = m_accounting.CreateAccount<Account>(AID, cashAccount);
                        #region Add account to cost center
                        int cashCsAccountID = m_accounting.CreateCostCenterAccount(AID, costCenterID, cashAccountID);
                        account.csAccountID = cashCsAccountID;
                        #endregion
                        account.code = GetNextStoreSerialCode("CashAccount", "code", dspWriter);
                        dspWriter.InsertSingleTableRecord(m_DBName, account, new string[] { "__AID" }, new object[] { AID });
                        code = account.code;
                    }
                    else
                    {
                        logExistingCashAccount(AID, existing);
                        dspWriter.UpdateSingleTableRecord(m_DBName, account, new string[] { "__AID" }, new object[] { AID });
                        #region cash Account
                        CostCenterAccount csAccount = m_accounting.GetCostCenterAccount(account.csAccountID);
                        Account cashAccount = m_accounting.GetAccount<Account>(csAccount.accountID);
                        cashAccount.Name = account.name;
                        if (cashAccount.Status == AccountStatus.Activated)
                        {
                            cashAccount.DeactivateDate = DateTime.MaxValue.Date;
                        }
                        m_accounting.UpdateAccount(AID, cashAccount);
                        #endregion
                        code = account.code;
                    }
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<CashAccountChangeFilter>().resetAllowAction();
                    dspWriter.restoreReadDB();
                }
                return code;
            }
        }
        
        private void logExistingCashAccount(int AID, CashAccount cashAccount)
        {
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.CashAccount", "code=" + cashAccount.code);
        }
        private Account CollectCashAccount(int parentCashAccountID, string nextCashAccountCode, string cashAccountName)
        {
            Account cashAccount = new Account();
            cashAccount.ActivateDate = DateTime.Now.Date;
            cashAccount.DeactivateDate = DateTime.MaxValue.Date;
            cashAccount.Code = nextCashAccountCode;
            cashAccount.CreationDate = DateTime.Now.Date;
            cashAccount.CreditAccount = false;
            cashAccount.Name = cashAccountName;
            cashAccount.PID = parentCashAccountID;
            cashAccount.Status = AccountStatus.Activated;
            return cashAccount;
        }
        private string GetNextCashAccountCode(int parentCashAccountID)
        {
            string code = GetCashParentAccountCode(parentCashAccountID);
            int codeSuffix = GetCashLastChildAccountSuffix(parentCashAccountID);
            if (SysPars.CashAccountSuffixCodeFormat == null)
                throw new ServerUserMessage("Format for Cash Account Suffix Code is not configured in settings. Please configure and try again");
            return TSConstants.FormatCashCode(code, codeSuffix,SysPars.CashAccountSuffixCodeFormat);
        }
        private string GetCashParentAccountCode(int parentCashAccountID)
        {
            Account account = m_accounting.GetAccount<Account>(parentCashAccountID);
            return account.Code;
        }
        private int GetCashLastChildAccountSuffix(int parentCashAccountID)
        {
            Account[] cashAccounts = m_accounting.GetLeafAccounts<Account>(parentCashAccountID);
            if (cashAccounts != null)
            {
                if (cashAccounts.Length == 0
                    || (cashAccounts.Length == 1 && cashAccounts[0].id == parentCashAccountID))
                {
                    return 1;
                }
                string code = cashAccounts[cashAccounts.Length - 1].Code;
                if (code.Contains("-"))
                {
                    string[] splittedCode = code.Split('-');
                    int codeSuffix = int.Parse(splittedCode[splittedCode.Length - 1]);
                    return codeSuffix + 1;
                }
                throw new INTAPS.ClientServer.ServerUserMessage("Account Code format not recognized");
            }
            else
            {
                return 1; //start from 001
            }
        }

        public void ActivateCashAccount(int AID,string code)
        {
            lock (dspWriter)
            {
                try
                {
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<CashAccountChangeFilter>().allowAction(AID);
                    CashAccount cashAccount = GetCashAccount(code);
                    CostCenterAccount csAccount = m_accounting.GetCostCenterAccount(cashAccount.csAccountID);
                    m_accounting.ActivateAcount<Account>(AID, csAccount.accountID, DateTime.Now.Date);
                }
                finally
                {
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<CashAccountChangeFilter>().resetAllowAction();
                }
            }
        }
        public void DeactivateCashAccount(int AID, string code)
        {
            lock (dspWriter)
            {
                try
                {
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<CashAccountChangeFilter>().allowAction(AID);
                    CashAccount cashAccount = GetCashAccount(code);
                    CostCenterAccount csAccount = m_accounting.GetCostCenterAccount(cashAccount.csAccountID);
                    m_accounting.DeactivateAccount<Account>(AID, csAccount.accountID, DateTime.Now.Date);
                }
                finally
                {
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<CashAccountChangeFilter>().resetAllowAction();
                }

            }
        }

        private CashAccount[] getCashAccountsByEmployee(int employeeID)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                CashAccount[] ret = helper.GetSTRArrayByFilter<CashAccount>("employeeID=" + employeeID);
                return ret;
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }

    }
}
