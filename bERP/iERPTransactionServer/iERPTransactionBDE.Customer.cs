using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        #region TradeRelation Methods
        public string RegisterCustomer(int AID, TradeRelation customer)
        {
            throw new Exception("Deprecated");
        }
        public TradeRelation GetCustomer(string code)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                TradeRelation [] ret= dspReader.GetSTRArrayByFilter<TradeRelation>("Code='" + code + "' and IsCustomer<>0");
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public TradeRelation[] GetAllCustomers()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<TradeRelation>("IsCustomer<>0");
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public TradeRelation[] SearchCustomers(int index, int pageSize, object[] criteria, string[] column, out int NoOfRecords)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            string filter = "";
            try
            {
                if (column != null)
                {
                    for (int i = 0; i < column.Length; i++)
                    {
                        if (i > 0 && i <= column.Length - 1)
                            filter += " AND ";
                        filter += String.Format("{0} LIKE '%{1}%'", column[i], criteria[i]);
                    }
                }
                if (string.IsNullOrEmpty(filter))
                    filter = "IsCustomer<>0";
                else
                    filter += " and IsCustomer<>0";

                return dspReader.GetSTRArrayByFilter<TradeRelation>(filter, index, pageSize, out NoOfRecords);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public void DeleteCustomer(int AID, string[] code)
        {
            throw new Exception("Deprecated");
        }

        public void ActivateCustomer(int AID, string customerCode)
        {
            throw new Exception("Deprecated");
        }
        
        public void DeactivateCustomer(int AID, string customerCode)
        {
            throw new Exception("Deprecated");

        }

        class UnreturnedBondFilter : INTAPS.RDBMS.IObjectFilter<AccountDocument>
        {

            INTAPS.Accounting.BDE.AccountingBDE  accounting;
            List<BondPaymentDocument> filtered;
            string query;
            public UnreturnedBondFilter(INTAPS.Accounting.BDE.AccountingBDE accounting, List<BondPaymentDocument> filtered, string query)
            {
                this.accounting = accounting;
                this.filtered = filtered;
                this.query = query;
            }
            #region IObjectFilter<BondPaymentDocument> Members

            public bool Include(AccountDocument obj, object[] additional)
            {
                BondPaymentDocument bond = (BondPaymentDocument)accounting.GetAccountDocument(obj.AccountDocumentID, true);
                if (bond.returned)
                    return false;
                if (!string.IsNullOrEmpty(query))
                {
                    if ((string.IsNullOrEmpty(bond.cpoNumber) || bond.cpoNumber.ToUpper().IndexOf(query.ToUpper()) == -1)
                        && (bond.PaperRef.ToUpper().IndexOf(query.ToUpper()) == -1)
                        )
                        return false;
                }
                filtered.Add(bond);
                return true;
            }

            #endregion
        }
        public BondPaymentDocument[] GetAllUnreturnedBonds(string query)
        {
            INTAPS.RDBMS.SQLHelper dspReader = m_accounting.GetReaderHelper();
            int N;
            try
            {
                List<BondPaymentDocument> filtered = new List<BondPaymentDocument>();
                UnreturnedBondFilter f = new UnreturnedBondFilter(m_accounting, filtered, query);
                dspReader.GetSTRArrayByFilter<AccountDocument>("DocumentTypeID=" + m_accounting.GetDocumentTypeByType(typeof(BondPaymentDocument)).id
                , 0, -1, out N, f);
                return filtered.ToArray();
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        #region Private Methods
        class CustomerSalesDocumentsFilter : INTAPS.RDBMS.IObjectFilter<AccountDocument>
        {

            INTAPS.Accounting.BDE.AccountingBDE finance;
            List<Sell2Document> filtered;
            string customerCode;
            public CustomerSalesDocumentsFilter(INTAPS.Accounting.BDE.AccountingBDE finance, List<Sell2Document> filtered, string code)
            {
                this.finance = finance;
                this.filtered = filtered;
                customerCode = code;
            }
            #region IObjectFilter<BondPaymentDocument> Members

            public bool Include(AccountDocument obj, object[] additional)
            {
                Sell2Document salesDocument = (Sell2Document)finance.GetAccountDocument(obj.AccountDocumentID, true);
                if (!salesDocument.relationCode.Equals(customerCode))
                    return false;
                filtered.Add(salesDocument);
                return true;
            }

            #endregion
        }
        private bool HasCompanySoldAnyItems(string customerCode)
        {
            INTAPS.RDBMS.SQLHelper dspReader = m_accounting.GetReaderHelper();
            try
            {
                List<Purchase2Document> filtered = new List<Purchase2Document>();
                SupplierPurchasedDocumentsFilter documentFilter = new SupplierPurchasedDocumentsFilter(m_accounting, filtered, customerCode);
                TradeRelationTransactionFilter f = new TradeRelationTransactionFilter(customerCode);
                return m_accounting.FindFirstDocument(null, m_accounting.GetDocumentTypeByType(typeof(Sell2Document)).id, new INTAPS.Accounting.BDE.AccountDocumentFilterDelegate(f.FindTradeTransaction<Sell2Document>), true) != null;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        private Account CollectCustomerPayableAccount(int customerPayableParentAccountID, string nextPayableCode, string customerName)
        {
            Account customerPayableAccount = new Account();
            customerPayableAccount.ActivateDate = DateTime.Now.Date;
            customerPayableAccount.DeactivateDate = DateTime.MaxValue.Date;
            customerPayableAccount.Code = nextPayableCode;
            customerPayableAccount.CreationDate = DateTime.Now.Date;
            customerPayableAccount.CreditAccount = true;
            customerPayableAccount.Name = customerName;
            customerPayableAccount.PID = customerPayableParentAccountID;
            customerPayableAccount.Status = AccountStatus.Activated;
            return customerPayableAccount;
        }
        private Account CollectCustomerReceivableAccount(int customerReceivableParentAccountID, string nextReceivableCode, string customerName)
        {
            Account customerReceivableAccount = new Account();
            customerReceivableAccount.ActivateDate = DateTime.Now.Date;
            customerReceivableAccount.DeactivateDate = DateTime.MaxValue.Date;
            customerReceivableAccount.Code = nextReceivableCode;
            customerReceivableAccount.CreationDate = DateTime.Now.Date;
            customerReceivableAccount.CreditAccount = false;
            customerReceivableAccount.Name = customerName;
            customerReceivableAccount.PID = customerReceivableParentAccountID;
            customerReceivableAccount.Status = AccountStatus.Activated;
            return customerReceivableAccount;
        }
        private Account CollectBondReceivableAccount(int bondReceivableParentAccountID, string nextBondCode, string customerName)
        {
            Account bondReceivableAccount = new Account();
            bondReceivableAccount.ActivateDate = DateTime.Now.Date;
            bondReceivableAccount.DeactivateDate = DateTime.MaxValue.Date;
            bondReceivableAccount.Code = nextBondCode;
            bondReceivableAccount.CreationDate = DateTime.Now.Date;
            bondReceivableAccount.CreditAccount = false;
            bondReceivableAccount.Name = customerName;
            bondReceivableAccount.PID = bondReceivableParentAccountID;
            bondReceivableAccount.Status = AccountStatus.Activated;
            return bondReceivableAccount;
        }
        private string GetNextCustomerReceivableAccountCode(out int customerReceivableAccountID)
        {
            string code = GetCustomerReceivableParentAccountCode(out customerReceivableAccountID);
            int codeSuffix = GetCustomerReceivableLastChildAccountSuffix(customerReceivableAccountID);
            return TSConstants.FormatAccountCode(code, codeSuffix);

        }
        private string GetNextBondReceivableAccountCode(out int bondReceivableParentAccountID)
        {
            string code = GetBondReceivableParentAccountCode(out bondReceivableParentAccountID);
            int codeSuffix = GetBondReceivableLastChildAccountSuffix(bondReceivableParentAccountID);
            return TSConstants.FormatAccountCode(code, codeSuffix);
        }
        private string GetCustomerReceivableParentAccountCode(out int customerReceivableAccountID)
        {
            customerReceivableAccountID = (int)GetSystemParameters(new string[] { "customersReceivablesAccountID" })[0];
            Account account = m_accounting.GetAccount<Account>(customerReceivableAccountID);
            if (account == null)
                throw new INTAPS.ClientServer.ServerConfigurationError();
            return account.Code;
        }
        private string GetBondReceivableParentAccountCode(out int bondReceivableParentAccountID)
        {
            bondReceivableParentAccountID = (int)GetSystemParameters(new string[] { "customersBondAccountID" })[0];
            Account account = m_accounting.GetAccount<Account>(bondReceivableParentAccountID);
            if (account == null)
                throw new INTAPS.ClientServer.ServerConfigurationError();
            return account.Code;
        }
        private int GetCustomerReceivableLastChildAccountSuffix(int receivableAccountID)
        {
            Account[] customerReceivableAccounts = m_accounting.GetLeafAccounts<Account>(receivableAccountID);
            if (customerReceivableAccounts != null)
            {
                if (customerReceivableAccounts.Length == 0
                    || (customerReceivableAccounts.Length == 1 && customerReceivableAccounts[0].id == receivableAccountID))
                {
                    return 1;
                }
                string code = customerReceivableAccounts[customerReceivableAccounts.Length - 1].Code;
                if (code.Contains("-"))
                {
                    string[] splittedCode = code.Split('-');
                    int codeSuffix = int.Parse(splittedCode[splittedCode.Length - 1]);
                    return codeSuffix + 1;
                }
                throw new INTAPS.ClientServer.ServerUserMessage("Account Code format not recognized");
            }
            else
            {
                return 1; //start from 001
            }
        }
        private int GetBondReceivableLastChildAccountSuffix(int bondReceivableParentAccountID)
        {
            Account[] bondReceivableAccounts = m_accounting.GetLeafAccounts<Account>(bondReceivableParentAccountID);
            if (bondReceivableAccounts != null)
            {
                if (bondReceivableAccounts.Length == 0
                    || (bondReceivableAccounts.Length == 1 && bondReceivableAccounts[0].id == bondReceivableParentAccountID))
                {
                    return 1;
                }
                string code = bondReceivableAccounts[bondReceivableAccounts.Length - 1].Code;
                if (code.Contains("-"))
                {
                    string[] splittedCode = code.Split('-');
                    int codeSuffix = int.Parse(splittedCode[splittedCode.Length - 1]);
                    return codeSuffix + 1;
                }
                throw new INTAPS.ClientServer.ServerUserMessage("Account Code format not recognized");
            }
            else
            {
                return 1; //start from 001
            }
        }
        private string GetNextCustomerPayableAccountCode(out int customerPayableAccountID)
        {
            string code = GetCustomerPayableParentAccountCode(out customerPayableAccountID);
            int codeSuffix = GetCustomerPayableLastChildAccountSuffix(customerPayableAccountID);
            return TSConstants.FormatAccountCode(code, codeSuffix);
        }
        private string GetCustomerPayableParentAccountCode(out int customerPayableAccountID)
        {
            customerPayableAccountID = (int)GetSystemParameters(new string[] { "customersPayablesAccountID" })[0];
            Account account = m_accounting.GetAccount<Account>(customerPayableAccountID);
            if (account == null)
                throw new INTAPS.ClientServer.ServerConfigurationError();
            return account.Code;

        }
        private int GetCustomerPayableLastChildAccountSuffix(int payableAccountID)
        {
            Account[] customerPayableAccounts = m_accounting.GetLeafAccounts<Account>(payableAccountID);
            if (customerPayableAccounts != null)
            {
                if (customerPayableAccounts.Length == 0
                    || (customerPayableAccounts.Length == 1 && customerPayableAccounts[0].id == payableAccountID))
                {
                    return 1;
                }
                string code = customerPayableAccounts[customerPayableAccounts.Length - 1].Code;
                if (code.Contains("-"))
                {
                    string[] splittedCode = code.Split('-');
                    int codeSuffix = int.Parse(splittedCode[splittedCode.Length - 1]);
                    return codeSuffix + 1;
                }
                throw new INTAPS.ClientServer.ServerUserMessage("Account Code format not recognized");
            }
            else
            {
                return 1; //start from 001
            }
        }
        private bool CustomerTINIsDuplicate(string TIN, string code)
        {
            INTAPS.RDBMS.SQLHelper dspReader = m_accounting.GetReaderHelper();
            try
            {
                if (!string.IsNullOrEmpty(TIN))
                {
                    if (code == null)
                    {
                        if ((int)dspReader.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.{1} where TIN='{2}'", m_DBName, typeof(TradeRelation).Name, TIN)) > 0)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if ((int)dspReader.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.{1} where TIN='{2}' AND Code <> '{3}'", m_DBName, typeof(TradeRelation).Name, TIN, code)) > 0)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        #endregion

        #endregion
    }
}