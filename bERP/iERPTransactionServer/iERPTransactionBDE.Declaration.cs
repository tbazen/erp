using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using System.Data.SqlClient;
using System.Data;
using BIZNET.iERP.TypedDataSets;
using System.Collections;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE : INTAPS.ClientServer.BDEBase
    {
        public int[] GetCandidatesForDeclaration(int declarationType, DateTime declarationDate, int periodID, out int [] taxCenterID, out double[] taxAmount)
        {
            ITaxDeclarationServerHandler taxHandler = m_accounting.GetDocumentHandler(declarationType) as ITaxDeclarationServerHandler;
            return taxHandler.GetCandidateDocuments(this, declarationDate, periodID,out taxCenterID, out taxAmount);
        }
        public DataSet PreviewDeclaration(int AID, int declarationType, DateTime declarationDate, int periodID, int taxCenterID, int[] documents, int[] rejectedDocuments, out double total, bool newDeclaration)
        {
            ITaxDeclarationServerHandler taxHandler = m_accounting.GetDocumentHandler(declarationType) as ITaxDeclarationServerHandler;
            return taxHandler.BuildReportDataSet(AID,this, declarationDate, periodID, taxCenterID, documents, rejectedDocuments, out total,newDeclaration);
        }
        public int SaveTaxDeclaration(int AID,int declarationType, DateTime declarationDate, int periodID, int[] documents,int taxCenterID)
        {
            return SaveTaxDeclaration(AID,-1, declarationType, declarationDate, periodID, documents, null, taxCenterID);
        }
        internal int SaveTaxDeclaration(int AID,int oldID,int declarationType, DateTime declarationDate, int periodID, int[] documents,int[]rejectedDocuments,int taxCenterID)
        {            
            ITaxDeclarationServerHandler taxHandler = m_accounting.GetDocumentHandler(declarationType) as ITaxDeclarationServerHandler;
            //validation
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                if (documents != null)
                {
                    foreach (int doc in documents)
                    {
                        DeclaredDocument[] decl = reader.GetSTRArrayByFilter<DeclaredDocument>("documentID=" + doc + " and declarationType=" + declarationType+ " and taxCenterID="+taxCenterID);
                        if (decl.Length == 0)
                            continue;
                         throw new INTAPS.ClientServer.ServerUserMessage("Document already declared. Declaration ID:" + decl[0].declarationID);
                    }
                }
            }

            finally
            {
                base.ReleaseHelper(reader);
            }
            
            lock (dspWriter)
            {
                TaxDeclaration decl=new TaxDeclaration();
                if (oldID == -1)
                    decl.id = AutoIncrement.GetKey("iERP.TaxDeclarationID");
                else
                    decl.id = oldID;
                decl.periodID = periodID;
                decl.declarationDate = declarationDate;
                decl.declaredDocuments = documents;
                decl.report = taxHandler.BuildReportDataSet(AID,this, declarationDate, periodID, taxCenterID, documents, rejectedDocuments, out decl.paidAmount, true);
                if (decl.report != null)
                {
                    decl.declarationType = declarationType;
                    decl.TaxCenter = taxCenterID;
                    dspWriter.BeginTransaction();
                    try
                    {
                        dspWriter.InsertSingleTableRecord(this.DBName, decl);
                        foreach (int doc in documents)
                        {
                            DeclaredDocument ddoc = new DeclaredDocument();
                            ddoc.declarationID = decl.id;
                            ddoc.declarationType = declarationType;
                            ddoc.documentID = doc;
                            ddoc.postDocumentID = -1;
                            ddoc.taxCenterID = taxCenterID;
                            dspWriter.InsertSingleTableRecord(this.DBName, ddoc);
                        }
                        dspWriter.CommitTransaction();
                       
                    }
                    catch
                    {
                        dspWriter.RollBackTransaction();
                        throw;
                    }
                }
                return decl.id;
            }
        }
        internal int UpdateTaxDeclaration(int AID, int oldID, int declarationType, DateTime declarationDate, int periodID, int[] documents, int[] rejectedDocuments, int taxCenterID)
        {
            ITaxDeclarationServerHandler taxHandler = m_accounting.GetDocumentHandler(declarationType) as ITaxDeclarationServerHandler;
            //validation
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                foreach (int doc in documents)
                {
                    DeclaredDocument[] decl = reader.GetSTRArrayByFilter<DeclaredDocument>("documentID=" + doc + " and declarationType=" + declarationType);
                    if (decl.Length == 0)
                        continue;
                    throw new INTAPS.ClientServer.ServerUserMessage("Document allready declared. Declaration ID:" + decl[0].declarationID);
                }
            }

            finally
            {
                base.ReleaseHelper(reader);
            }

            lock (dspWriter)
            {
                TaxDeclaration decl = new TaxDeclaration();
                if (oldID == -1)
                    decl.id = AutoIncrement.GetKey("iERP.TaxDeclarationID");
                else
                    decl.id = oldID;
                decl.periodID = periodID;
                decl.declarationDate = declarationDate;
                decl.declaredDocuments = documents;
                decl.rejectedDocuments = rejectedDocuments;
                decl.declarationType = declarationType;
                decl.TaxCenter = taxCenterID;
                dspWriter.BeginTransaction();
                try
                {
                    foreach (int doc in documents)
                    {
                        DeclaredDocument ddoc = new DeclaredDocument();
                        ddoc.declarationID = decl.id;
                        ddoc.declarationType = declarationType;
                        ddoc.documentID = doc;
                        ddoc.taxCenterID = taxCenterID;
                        if (rejectedDocuments != null)
                        {
                            foreach (int rejected in rejectedDocuments)
                            {
                                if (doc == rejected)
                                {
                                    ddoc.rejectedDocumentID = doc;
                                    break;
                                }
                            }
                        }
                        ddoc.postDocumentID = -1;
                        dspWriter.InsertSingleTableRecord(this.DBName, ddoc);
                    }
                    decl.report = taxHandler.BuildReportDataSet(AID,this, declarationDate, periodID, taxCenterID,documents, rejectedDocuments, out decl.paidAmount, false);
                    dspWriter.InsertSingleTableRecord(this.DBName, decl);
                    dspWriter.CommitTransaction();
                    return decl.id;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        public TaxDeclaration[] GetTaxDeclarations(int declarationType, int period1, int period2)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                TaxDeclaration[] ret= helper.GetSTRArrayByFilter<TaxDeclaration>("declarationType=" + declarationType);
                Dictionary<int, AccountingPeriod> prd = new Dictionary<int, AccountingPeriod>();
                foreach(TaxDeclaration t in ret)
                    if(!prd.ContainsKey(t.periodID))
                        prd.Add(t.periodID,GetAccountingPeriod("AP_Tax_Declaration",t.periodID));
                Array.Sort<TaxDeclaration>(ret, new Comparison<TaxDeclaration>(delegate(TaxDeclaration x, TaxDeclaration y)
                    {
                        if (x.periodID == y.periodID)
                            return x.TaxCenter.CompareTo(y.TaxCenter);
                        
                        return prd[x.periodID].fromDate.CompareTo(prd[y.periodID].fromDate);
                    }
                    ));
                foreach (TaxDeclaration td in ret)
                {
                    FixDataSetType(td);
                }
                return ret;
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public TaxDeclaration GetTaxDeclaration(int declatationID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                TaxDeclaration[] _ret = reader.GetSTRArrayByFilter<TaxDeclaration>("id=" + declatationID);
                if (_ret.Length == 0)
                    return null;
                TaxDeclaration ret=_ret[0];
                FixDataSetType(ret);
                return ret;
                
            }

            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public TaxDeclaration GetTaxDeclaration(int declarationType,int periodID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                TaxDeclaration[] _ret = reader.GetSTRArrayByFilter<TaxDeclaration>(String.Format("declarationType={0} AND periodID={1}", declarationType, periodID));
                if (_ret.Length == 0)
                    return null;
                TaxDeclaration ret = _ret[0];
                FixDataSetType(ret);
                return ret;
            }

            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public bool HasTaxDeclarationBegan(int declarationType)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                TaxDeclaration[] _ret = reader.GetSTRArrayByFilter<TaxDeclaration>(String.Format("declarationType={0}", declarationType));
                if (_ret.Length == 0)
                    return false;
                else
                    return true;
            }

            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public int CountPaidTaxDeclarations(int declarationType)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                TaxDeclaration[] _ret = reader.GetSTRArrayByFilter<TaxDeclaration>(String.Format("declarationType={0} and payDocumentID!=-1", declarationType));
                return _ret.Length;
            }

            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public bool IsPreviousDeclarationPeriodConfirmed(int declarationType, int currPeriodID)
        {
            bool confirmed = false;
            AccountingPeriod period = GetPreviousAccountingPeriod(m_sysPars.taxDeclarationPeriods, currPeriodID);
               INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
               try
               {
                   string sql = "select count(*) From TaxDeclaration Where periodID=" + period.id + " And declarationType=" + declarationType + " And payDocumentID=-1";
                   if ((int)helper.ExecuteScalar(sql) > 0)
                       confirmed = false;
                   else
                       confirmed = true;
               }
               finally
               {
                   base.ReleaseHelper(helper);
               }
               return confirmed;
        }
        private static void FixDataSetType(TaxDeclaration ret)
        {
            Type dataSetType = Type.GetType("BIZNET.iERP.TypedDataSets." + ret.report.DataSetName+","+System.Reflection.Assembly.GetAssembly(typeof(WithholdingTaxDeclarationData)).GetName().Name);
            if (dataSetType == null)
                throw new INTAPS.ClientServer.ServerUserMessage("Unsupported report dataset type");
            DataSet ds = (DataSet)Activator.CreateInstance(dataSetType);
            ds.Merge(ret.report);
            ret.report = ds;
        }
        internal void PayTax<DeclarationDocumentType>(DeclarationDocumentType doc)
            where DeclarationDocumentType:TaxDeclarationDocumentBase
        {
            TaxDeclaration decl = GetTaxDeclaration(doc.declarationID);
            if (decl == null)
                throw new INTAPS.ClientServer.ServerUserMessage("Tax declaration not found in the database ID:" + doc.declarationID);
            if (decl.payDocumentID != -1)
                throw new INTAPS.ClientServer.ServerUserMessage("This declaration is already paid");
            ITaxDeclarationServerHandler taxHandler = m_accounting.GetDocumentHandler(decl.declarationType) as ITaxDeclarationServerHandler;
            decl.paidAmount = doc.NetPay;
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    decl.payDocumentID=doc.AccountDocumentID;
                    decl.payDate=doc.DocumentDate;
                    DataSet data = decl.report;
                    foreach (DataTable table in data.Tables)
                    {
                        if (table.TableName == "Summary")
                        {
                            data.Tables[0].Rows[0][0] = doc.PaperRef; //Update Document No
                            decl.report = data;
                            break;
                        }
                    }
                   
                    dspWriter.Update(this.DBName, typeof(DeclaredDocument).Name, new string[] { "postDocumentID" }, new object[] { decl.payDocumentID }
                    , "declarationID=" + doc.declarationID);
                    dspWriter.UpdateSingleTableRecord(this.DBName, decl);
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        internal void UndoPayTax<DeclarationDocumentType>(int declarationID)
            where DeclarationDocumentType : TaxDeclarationDocumentBase
        {
            TaxDeclaration decl = GetTaxDeclaration(declarationID);
            if (decl == null)
                throw new INTAPS.ClientServer.ServerUserMessage("Tax declaration not found in the database ID:" + declarationID);
            if (decl.payDocumentID == -1)
                throw new INTAPS.ClientServer.ServerUserMessage("This declaration is not paid");
            ITaxDeclarationServerHandler taxHandler = m_accounting.GetDocumentHandler(decl.declarationType) as ITaxDeclarationServerHandler;
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    decl.payDocumentID = -1;
                    DataSet data = decl.report;
                    foreach (DataTable table in data.Tables)
                    {
                        if (table.TableName == "Summary")
                        {
                            data.Tables[0].Rows[0][0] = DBNull.Value; //Update Document No to null
                            decl.report = data;
                            break;
                        }
                    }
                    //decl.report = taxHandler.BuildReportDataSet(this, decl.declarationDate, decl.periodID, decl.declaredDocuments, decl.rejectedDocuments, out decl.paidAmount);
                    dspWriter.Update(this.DBName, typeof(DeclaredDocument).Name,
                        new string[] { "postDocumentID" }, new object[] { -1 }
                        , "declarationID=" + declarationID);
                    dspWriter.UpdateSingleTableRecord(this.DBName, decl);

                    //dspWriter.Update(this.DBName, typeof(TaxDeclaration).Name, new string[] { "payDocumentID" }, new object[] { -1 }
                    //    , "id=" + declarationID);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }   

        }
        public void DeleteDeclaration(int declarationID)
        {
            TaxDeclaration decl = GetTaxDeclaration(declarationID);
            if (decl == null)
                throw new INTAPS.ClientServer.ServerUserMessage("Tax declaration not found in the database ID:" + declarationID);
            ITaxDeclarationServerHandler taxHandler = m_accounting.GetDocumentHandler(decl.declarationType) as ITaxDeclarationServerHandler;
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    if (decl.payDocumentID != -1)
                        throw new INTAPS.ClientServer.ServerUserMessage("It is not allowed to delete a paid declaration.");
                    dspWriter.Delete(this.DBName, typeof(DeclaredDocument).Name, "declarationID=" + declarationID);
                    dspWriter.DeleteSingleTableRecrod<TaxDeclaration>(this.DBName, declarationID);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        public void UpdateDeclaration(int AID,TaxDeclaration decl)
        {
            TaxDeclaration oldData = GetTaxDeclaration(decl.id);
            if (oldData == null)
                throw new INTAPS.ClientServer.ServerUserMessage("Tax declaration don't exist in database");
            if(oldData.payDocumentID!=-1)
                throw new INTAPS.ClientServer.ServerUserMessage("It is not allowed to edit a paid tax declaration");
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    DeleteDeclaration(decl.id);
                    UpdateTaxDeclaration(AID,decl.id, decl.declarationType, decl.declarationDate, decl.periodID, decl.declaredDocuments, decl.rejectedDocuments,decl.TaxCenter);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        public TaxCenter[] GetTaxCenters()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<TaxCenter>(null);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public TaxCenter GetTaxCenter(int id)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                TaxCenter [] ret=dspReader.GetSTRArrayByFilter<TaxCenter>("ID=" + id);
                if(ret.Length==0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
    }
}