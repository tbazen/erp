using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS;
namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        void AddDocumentReference(bERPDocumentReference docRef)
        {
            lock (dspWriter)
            {
                if (string.IsNullOrEmpty(docRef.refNumber)
                    || string.IsNullOrEmpty(docRef.refType)
                    || docRef.documentID < 1)
                    throw new INTAPS.ClientServer.ServerUserMessage("Invlid document reference");
                dspWriter.InsertSingleTableRecord(this.DBName,docRef);
            }
        }
        
        void DeleteDocumentReference(int docID)
        {
            lock (dspWriter)
            {
                dspWriter.Delete(this.m_BDEName, typeof(bERPDocumentReference).Name, "documentID=" + docID);
            }
        }
        public int CountReferedDocuments(DocumentReferenceType type, string refNumber)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {

                return (int)helper.ExecuteScalar(
                    string.Format("Select count(*) from {0}.dbo.{1} where refNumber='{2}' and refType={3}"
                , this.DBName, typeof(bERPDocumentReference).Name, refNumber, (int)type));

            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public bERPDocumentReference[] GetReferedDocuments(DocumentReferenceType type, string refNumber)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                return helper.GetSTRArrayByFilter<bERPDocumentReference>(string.Format("refNumber='{0}' and refType={1}", refNumber, (int)type));
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        

    }
}