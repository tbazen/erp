using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Payroll;
using System.Data.SqlClient;
using System.Data;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        #region Employee Methods
        public int RegisterEmployee(int AID, Employee employee, byte[] picture, bool updateWorkingData,int[]otherAccounts,int periodID, Data_RegularTime regular, OverTimeData overtime, SingleValueData[] othersData, int[] singleValueFormulaID)
        {
            lock (dspWriter)
            {
                try
                {
                    dspWriter.BeginTransaction();
                    if (employee.costCenterID != -1)
                    {
                        CostCenter cs = m_accounting.GetAccount<CostCenter>(employee.costCenterID);
                        if (cs.isControl)
                        {
                            throw new ServerUserMessage("Cost center of the employee must be cost center with no child");
                        }
                    }
                    else
                        throw new ServerUserMessage("Employee cost center must be chosen");
                    Employee oldEmployee = employee.id == -1 ? null : m_Payroll.GetEmployeeLastVersion(employee.id);
                    int[] parentAccounts = new int[]
                        {
                            m_Payroll.SysPars.staffLongTermLoanAccountID
                            ,m_Payroll.SysPars.staffSalaryAdvanceAccountID
                            ,m_Payroll.SysPars.staffExpenseAdvanceAccountID
                            ,m_Payroll.SysPars.UnclaimedSalaryAccount
                            ,m_Payroll.GetIncomeTaxAccountID(employee)
                            ,m_Payroll.GetPensionLiabilityAccountID(employee)
                            ,m_Payroll.GetCostSharingLiabilityAccountID(employee)
                            ,m_Payroll.SysPars.LoanFromStaffAccountID
                            ,m_Payroll.GetOwnersEquityAccountID(employee)
                            ,m_Payroll.GetPerdiemExpenseAccountID(employee)
                            ,m_Payroll.GetPayrollExpenseAcount(employee)
                            ,m_Payroll.GetShareHolderLoanAccountID(employee)
                        };

                    List<int> allEmpAccounts = new List<int>();
                    foreach (int pacc in parentAccounts)
                    {
                        allEmpAccounts.Add(pacc);
                    }
                    if (otherAccounts != null)
                    {
                        foreach (int otheracc in otherAccounts)
                        {
                            if (!allEmpAccounts.Contains(otheracc))
                                allEmpAccounts.Add(otheracc);
                        }
                    }
                    parentAccounts = allEmpAccounts.ToArray();
                    int[] newChildAccounts = new int[parentAccounts.Length];
                    for (int i = 0; i < parentAccounts.Length; i++)
                    {
                        newChildAccounts[i] = -1;
                        if (parentAccounts[i] == -1)
                            continue;
                        Account parent=m_accounting.GetAccount<Account>(parentAccounts[i]);
                        if (parent == null)
                            continue;
                        bool create=false;
                        bool update=false;
                        Account childAccount=null;
                        if (oldEmployee == null)
                            create = true;
                        else
                        {
                             childAccount= m_Payroll.GetEmployeeSubAccount(oldEmployee, parentAccounts[i]);
                             if (childAccount == null)
                                 create = true;
                             else
                             {
                                 newChildAccounts[i] = childAccount.id;
                                 update = true;
                             }
                        }
                        
                        if (create)
                        {
                           
                            Account account = new Account();
                            account.id = -1;
                            account.Name = parent.Name + "-" + employee.employeeName;
                            account.Code = parent.Code + "-" + employee.employeeID;
                            account.ActivateDate = DateTime.Now.Date;
                            account.CreationDate = DateTime.Now.Date;
                            account.CreditAccount = parent.CreditAccount;
                            account.Description = "Automatically Created Employee Account - don't modify or delete";
                            account.protection = AccountProtection.SystemAccount;
                            account.Status = AccountStatus.Activated;
                            account.PID = parentAccounts[i];
                            Account test = m_accounting.GetAccount<Account>(account.Code);
                            if (test == null)
                                account.id = m_accounting.CreateAccount<Account>(AID, account);
                            else
                            {
                                account.id = test.id;
                                m_accounting.UpdateAccount<Account>(AID,account);
                            }
                            newChildAccounts[i] = account.id;
                        }
                        
                        if(update)
                        {
                            if (!oldEmployee.employeeName.Equals(employee.employeeName)
                                || !oldEmployee.employeeID.Equals(employee.employeeID)
                                )
                            {
                                Account account = new Account();
                                account.id = childAccount.id;
                                account.Name = parent.Name + "-" + employee.employeeName;
                                account.Code = parent.Code + "-" + employee.employeeID;
                                account.ActivateDate = childAccount.ActivateDate;
                                account.CreationDate = childAccount.CreationDate;
                                account.CreditAccount =childAccount.CreditAccount;
                                account.Description = "Automatically Created Employee Account - don't modify or delete";
                                account.protection = AccountProtection.SystemAccount;
                                account.Status = account.Status;
                                account.PID = parentAccounts[i];
                                m_accounting.UpdateAccount<Account>(AID, account);
                                newChildAccounts[i] = account.id;
                            }
                        }
                    }
                    List<int> validAccounts=new List<int>();
                    foreach(int ac in newChildAccounts)
                        if(ac!=-1)
                            validAccounts.Add(ac);
                    

                    if (oldEmployee != null)
                    {
                        int oldPensionParentAccountID = m_Payroll.GetPensionLiabilityAccountID(oldEmployee);
                        int oldExpenseParentAccountID = m_Payroll.GetPayrollExpenseAcount(oldEmployee);
                        if (employee.employmentType == EmploymentType.Temporary && oldEmployee.employmentType == EmploymentType.Permanent)
                        {
                            Account oldPensionAccount = m_Payroll.GetEmployeeSubAccount(oldEmployee, oldPensionParentAccountID);
                           // Account oldExpAccount = m_Payroll.GetEmployeeSubAccount(oldEmployee, oldExpenseParentAccountID);

                            if (oldPensionAccount != null)
                            {
                                if (!Accounting.AccountHasTransaction<Account>(oldPensionAccount.id))
                                    DeleteEmployeeAccount(AID, oldEmployee.id, oldPensionAccount.id);
                            }
                        }
                        /*if (employee.employmentType != oldEmployee.employmentType || employee.)
                        {
                            Account oldExpAccount = m_Payroll.GetEmployeeSubAccount(oldEmployee, oldExpenseParentAccountID);

                            if (oldExpAccount != null)
                            {
                                if (!Accounting.AccountHasTransaction<Account>(oldExpAccount.id))
                                    DeleteEmployeeAccount(AID, oldEmployee.id, oldExpAccount.id);
                                if (validAccounts.Contains(oldExpAccount.id))
                                    validAccounts.Remove(oldExpAccount.id);
                            }
                        }*/
                        int[] oldParentAccounts = new int[]
                        {
                            m_Payroll.SysPars.staffLongTermLoanAccountID
                            ,m_Payroll.SysPars.staffShortTermLoanPayrollFormulaID
                            ,m_Payroll.SysPars.staffExpenseAdvanceAccountID
                            ,m_Payroll.SysPars.UnclaimedSalaryAccount
                            ,m_Payroll.GetIncomeTaxAccountID(oldEmployee)
                            ,m_Payroll.GetPensionLiabilityAccountID(oldEmployee)
                            ,m_Payroll.GetCostSharingLiabilityAccountID(oldEmployee)
                            ,m_Payroll.SysPars.LoanFromStaffAccountID
                            ,m_Payroll.GetOwnersEquityAccountID(oldEmployee)
                            ,m_Payroll.GetPerdiemExpenseAccountID(oldEmployee)
                            ,m_Payroll.GetPayrollExpenseAcount(oldEmployee)
                            ,m_Payroll.GetShareHolderLoanAccountID(oldEmployee)
                        };
                        for (int i = 0; i < oldParentAccounts.Length; i++)
                        {
                            if (oldParentAccounts[i] == -1)
                                continue;
                            Account oldAccount = m_Payroll.GetEmployeeSubAccount(oldEmployee, oldParentAccounts[i]);
                            int oldAccountID =oldAccount==null?-1:oldAccount.id;
                            if (oldAccountID !=newChildAccounts[i] && oldAccountID!=-1)
                            {
                                if (!m_accounting.AccountHasTransaction<Account>(oldAccountID))
                                {
                                    if (m_accounting.GetAccount<Account>(oldAccountID) != null)
                                    {
                                        m_accounting.DeleteAccount<Account>(AID, oldAccountID, true);
                                    }
                                }
                                if (validAccounts.Contains(oldAccountID))
                                    validAccounts.Remove(oldAccountID);
                            }
                        }
                       
                    }
                    employee.accounts = validAccounts.ToArray();

                    if (employee.id == -1) //INSERT
                    {
                        employee.dismissDate = DateTime.MaxValue.Date;
                        int newEmpID = m_Payroll.RegisterEmployee(AID,SysPars.mainCostCenterID, employee);
                        #region Add the employee to the cost center project/branch
                        _AddEmployeeToCostCenter(AID, employee, true);
                        #endregion
                        m_Payroll.SetEmployeeImage(newEmpID,employee.ticksFrom, picture);
                        if(updateWorkingData)
                            this.SetWorkingData(AID, newEmpID, periodID, regular, overtime, othersData, singleValueFormulaID);
                    }
                    else //UPDATE
                    {
                        employee.dismissDate = DateTime.MaxValue.Date;
                        if(updateWorkingData)
                            SetWorkingData(AID, employee.id, periodID, regular, overtime, othersData, singleValueFormulaID);
                        m_Payroll.Update(AID, employee, true);
                        #region Update employee project/branch if cost center is changed

                        if (oldEmployee.costCenterID != employee.costCenterID)
                        {
                            _RemoveEmpIfPreviouslyEnrolledInProject(AID, oldEmployee);
                            _AddEmployeeToCostCenter(AID, employee, false);
                        }
                        #endregion
                        m_Payroll.SetEmployeeImage(employee.id,employee.ticksFrom, picture);
                       
                    }
                    dspWriter.CommitTransaction();
                    return employee.id;
                    #endregion
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        // BDE exposed
        public Account GetEmployeeAccount(int parentAccountID, int employeeID)
        {
            return m_accounting.GetAccount<Account>(m_Payroll.GetEmployeeSubAccount(employeeID, parentAccountID));
        }
        private void _AddEmployeeToCostCenter(int AID, Employee employee,bool logData)
        {
            Project project = GetProjectInfo(employee.costCenterID);
            //If project is null, check if the employee cost center is a division and get its project
            if (project == null)
            {
                CostCenter costCenter = m_accounting.GetAccount<CostCenter>(employee.costCenterID);
                project = GetProjectInfo(costCenter.PID);
            }
            if (project != null)
            {
                if (!project.projectData.employees.Contains(employee.id))
                {
                    project.projectData.employees.Add(employee.id);
                    RegisterProject(AID, project, logData);
                }
            }
        }
        public void DeleteEmployee(int AID, int ID)
        {
            try
            {
                dspWriter.BeginTransaction();
                if (!IsEmployeeInvolvedInTransactions(ID))
                {
                    Employee employee = m_Payroll.GetEmployee(ID);
                    foreach (int account in employee.accounts)
                    {
                        m_accounting.DeleteAccount<Account>(AID, account, true);
                    }
                    Project[] projects = GetAllProjects();
                    foreach (Project project in projects)
                    {
                        if (project.employeeID == ID)
                        {
                            throw new ServerUserMessage("You cannot delete this employee since he/she is a manager of project/branch called '" + project.Name + "'\nChange the manager of the project to another employee and try again!");
                        }
                        if (project.projectData.employees.Contains(employee.id))
                        {
                            project.projectData.employees.Remove(employee.id);
                            RegisterProject(AID, project, true);
                            break;
                        }
                    }
                    m_Payroll.DeleteEmployee(AID,ID);
                }
                else
                {
                    throw new INTAPS.ClientServer.ServerUserMessage("You cannot delete this employee since it has one or more transactions involved in it");
                }
                dspWriter.CommitTransaction();

            }
            catch (Exception ex)
            {
                dspWriter.RollBackTransaction();
                throw ex;
            }
        }
        public void FireEmployee(int AID, int ID, DateTime dismissalDate)
        {
            try
            {
                dspWriter.BeginTransaction();
                m_Payroll.Fire(AID,dismissalDate, ID);
                DeactivateEmployee(AID, ID);
                dspWriter.CommitTransaction();
            }
            catch (Exception ex)
            {
                dspWriter.RollBackTransaction();
                throw ex;
            }
        }
        public void EnrollEmployee(int AID, int ID, DateTime enrollDate)
        {
            try
            {
                dspWriter.BeginTransaction();
                m_Payroll.Enroll(AID,enrollDate, ID);
                ActivateEmployee(AID, ID);
                dspWriter.CommitTransaction();
            }
            catch (Exception ex)
            {
                dspWriter.RollBackTransaction();
                throw ex;
            }
        }
        public Employee[] GetAllEmployeesExcept(int[] employeeID)
        {
            string id = "";
            for (int i = 0; i < employeeID.Length; i++)
            {
                if (i != employeeID.Length - 1)
                    id += employeeID[i] + ",";
                else
                    id += employeeID[i];
            }
            string filter = string.Format("id NOT IN({0})", id);
            INTAPS.RDBMS.SQLHelper healper = base.GetReaderHelper();
            IDataReader reader = null;
            try
            {
                reader = healper.ExecuteReader(String.Format("Select ID from {0}.dbo.Employee Where {1}", m_Payroll.DBName, filter));
                List<Employee> _ret = new List<Employee>();
                while (reader.Read())
                {
                    int ID = (int)reader[0];
                    Employee e = m_Payroll.GetEmployee(ID);
                    _ret.Add(e);
                }
                return _ret.ToArray();
            }
            finally
            {
                if (reader != null)
                {
                    if (!reader.IsClosed)
                        reader.Close();
                }
                base.ReleaseHelper(healper);
            }
        }
        public int[] GetPaidSalaryDocs(int empID, PayrollPeriod period)
        {
            try
            {
                int N;
                AccountDocument[] docs = m_accounting.GetAccountDocuments(null, m_accounting.GetDocumentTypeByType(typeof(UnclaimedSalaryDocument)).id, true
                , period.fromDate, period.toDate, 0, -1, out N);
                List<int> docIDs = new List<int>();
                foreach (AccountDocument doc in docs)
                {
                    UnclaimedSalaryDocument salaryDoc = (UnclaimedSalaryDocument)doc;
                    if (salaryDoc.employeeID == empID)
                    {
                        AccountTransaction[] transactions = m_accounting.GetTransactionsOfDocument(doc.AccountDocumentID);
                        foreach (AccountTransaction tran in transactions)
                        {
                            if (IsUnclaimedSalaryAccount(tran.AccountID))
                            {
                                if (AccountBase.AmountGreater(tran.Amount, 0)) //debit transaction
                                {
                                    docIDs.Add(doc.AccountDocumentID);
                                }
                            }
                        }
                    }
                }
                return docIDs.ToArray();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public double GetUnclaimedSalaryBalance(int empID, PayrollPeriod period)
        {
            try
            {
                int N;
                double creditTotal, debitTotal;
                creditTotal = debitTotal = 0;
                AccountDocument[] docs = m_accounting.GetAccountDocuments(null, m_accounting.GetDocumentTypeByType(typeof(UnclaimedSalaryDocument)).id, true
                , period.fromDate, period.toDate, 0, -1, out N);
                foreach (AccountDocument doc in docs)
                {
                    UnclaimedSalaryDocument salaryDoc = (UnclaimedSalaryDocument)doc;
                    if (salaryDoc.employeeID == empID)
                    {
                        AccountTransaction[] transactions = m_accounting.GetTransactionsOfDocument(doc.AccountDocumentID);
                        foreach (AccountTransaction tran in transactions)
                        {
                            if (IsUnclaimedSalaryAccount(tran.AccountID))
                            {
                                if (AccountBase.AmountGreater(tran.Amount, 0)) //debit transaction
                                {
                                    debitTotal += tran.Amount;
                                }
                                else if (AccountBase.AmountLess(tran.Amount, 0))
                                {
                                    creditTotal += tran.Amount;
                                }
                            }
                        }
                    }
                }
                return creditTotal + debitTotal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public double GetEmployeePaidSalaryTotal(int empID, int periodID)
        {
            UnclaimedSalaryDocument[] docs = GetEmployeePaidSalaries(empID, periodID);
            double total = 0;
            foreach (UnclaimedSalaryDocument salaryDoc in docs)
            {
                AccountTransaction[] transactions = m_accounting.GetTransactionsOfDocument(salaryDoc.AccountDocumentID);
                foreach (AccountTransaction tran in transactions)
                {
                    if (IsUnclaimedSalaryAccount(m_accounting.GetCostCenterAccount( tran.AccountID).accountID))
                    {
                        if (AccountBase.AmountGreater(tran.Amount, 0)) //debit transaction
                        {
                            total += tran.Amount;
                        }
                    }
                }
            }
            return total;
        }

        public UnclaimedSalaryDocument[] GetEmployeePaidSalaries(int empID, int periodID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = m_accounting.GetReaderHelper();
            int N;
            try
            {
                List<UnclaimedSalaryDocument> filtered = new List<UnclaimedSalaryDocument>();
                UnclaimedSalaryFilter f = new UnclaimedSalaryFilter(m_accounting, filtered, empID, periodID);
                dspReader.GetSTRArrayByFilter<AccountDocument>("DocumentTypeID=" + m_accounting.GetDocumentTypeByType(typeof(UnclaimedSalaryDocument)).id
                , 0, -1, out N, f);
                return filtered.ToArray();
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        public BatchError GenerateAndPostPayroll(int AID, int[] employees, int periodID, DateTime date)
        {
            lock (dspWriter)
            {
                List<int> objectIDs = new List<int>();
                List<string> errorDescs = new List<string>();
                List<Exception> exceptions = new List<Exception>();

                try
                {
                    dspWriter.BeginTransaction();

                    foreach (int empID in employees)
                    {
                        AppliesToEmployees apply = new AppliesToEmployees();
                        apply.All = false;
                        apply.Employees = new int[] { empID };
                        apply.OrgUnits = new int[] { };

                        BatchError error = m_Payroll.GeneratePayroll(date, apply, periodID);
                        if (error.ObjectID.Length == 0) //no errors
                            m_Payroll.PostPayroll(AID,SysPars.mainCostCenterID, empID, periodID, date);
                        else
                        {
                            foreach (string str in error.ErrorDesc)
                            {
                                errorDescs.Add(str);
                            }
                            foreach (int id in error.ObjectID)
                            {
                                objectIDs.Add(id);
                            }
                            foreach (Exception ex in error.SystemException)
                            {
                                exceptions.Add(ex);
                            }
                        }
                    }
                    dspWriter.CommitTransaction();

                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
                BatchError batchError = new BatchError();
                batchError.ErrorDesc = errorDescs.ToArray();
                batchError.ObjectID = objectIDs.ToArray();
                batchError.SystemException = exceptions.ToArray();
                return batchError;
            }
        }

        public void DeleteEmployeeAccount(int AID, int empID, int accountID)
        {
            lock (dspWriter)
            {
                try
                {
                    dspWriter.BeginTransaction();
                    Employee emp = m_Payroll.GetEmployee(empID);
                    int[] accounts = emp.accounts;
                    if (m_accounting.AccountHasTransaction<Account>(accountID))
                        throw new ServerUserMessage("Cannot delete employee account: " + m_accounting.GetAccount<Account>(accountID).CodeName + " since it has transactions associated with it");
                    m_accounting.DeleteAccount<Account>(AID, accountID, true);
                    List<int> empAccount = new List<int>();
                    foreach (int acc in accounts)
                    {
                        if (acc != accountID)
                            empAccount.Add(acc);
                    }
                    emp.accounts = empAccount.ToArray();
                    m_Payroll.Update(AID, emp, true);
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public bool EmployeeExists(int id)
        {
            bool exists = false;
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                string sql = "select count(*) From " + m_Payroll.DBName + ".dbo.Employee Where id=" + id;
                if ((int)helper.ExecuteScalar(sql) > 0)
                    exists = true;
                else
                    exists = false;
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
            return exists;
        }
        
        #region Private Methods
        
        private bool IsEmployeeInvolvedInTransactions(int ID)
        {
            bool isInvolved = false;
            Employee employee = m_Payroll.GetEmployee(ID);
            CashAccount[] cashAccount = getCashAccountsByEmployee(employee.id);
            if (cashAccount.Length > 0)
            {
                throw new ServerUserMessage("Employee is attached to cash account " + cashAccount[0].code + ":" + cashAccount[0].name);
            }
            foreach (int account in employee.accounts)
            {
                if (IsAccountInvolvedInTransactions(account))
                {
                    isInvolved = true;
                    break;
                }
            }
            return isInvolved;
        }
        private void DeactivateEmployee(int AID, int ID)
        {
            lock (dspWriter)
            {
                Employee employee = m_Payroll.GetEmployee(ID);
                try
                {
                    dspWriter.BeginTransaction();
                    foreach (int account in employee.accounts)
                    {
                        Account acc = m_accounting.GetAccount<Account>(account);
                        m_accounting.DeactivateAccount<Account>(AID, account, DateTime.Now);
                    }
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }

        }
        private void ActivateEmployee(int AID, int ID)
        {
            lock (dspWriter)
            {
                Employee employee = m_Payroll.GetEmployee(ID);

                try
                {
                    dspWriter.BeginTransaction();
                    foreach (int account in employee.accounts)
                    {
                        m_accounting.ActivateAcount<Account>(AID, account, DateTime.Now);
                    }
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        private bool IsUnclaimedSalaryAccount(int accountID)
        {
            Account acnt = m_accounting.GetAccount<Account>(accountID);
            if (acnt.PID == m_Payroll.SysPars.UnclaimedSalaryAccount)
            {
                return true;
            }
            return false;
        }
        private Account GetEmployeeAccount(int parentAccountID, int[] accounts)
        {
            foreach (int ac in accounts)
            {
                Account acnt = m_accounting.GetAccount<Account>(ac);
                if (acnt.PID == parentAccountID)
                {
                    return acnt;
                }
            }
            return null;

        }
        private bool IsAccountInvolvedInTransactions(int accountID)
        {
            if (accountID < 1)
                return false;
            return m_accounting.AccountHasTransaction<Account>(accountID);
        }
        private class UnclaimedSalaryFilter : INTAPS.RDBMS.IObjectFilter<AccountDocument>
        {

            INTAPS.Accounting.BDE.AccountingBDE finance;
            List<UnclaimedSalaryDocument> filtered;
            int employeeID, periodID;
            public UnclaimedSalaryFilter(INTAPS.Accounting.BDE.AccountingBDE finance, List<UnclaimedSalaryDocument> filtered, int employeeID, int periodID)
            {
                this.finance = finance;
                this.filtered = filtered;
                this.employeeID = employeeID;
                this.periodID = periodID;
            }
            #region IObjectFilter<BondPaymentDocument> Members

            public bool Include(AccountDocument obj, object[] additional)
            {
                UnclaimedSalaryDocument bond = (UnclaimedSalaryDocument)finance.GetAccountDocument(obj.AccountDocumentID, true);
                if (bond.employeeID != this.employeeID || bond.periodID != this.periodID)
                    return false;

                filtered.Add(bond);
                return true;
            }

            #endregion
        }
        #endregion

        void assertUniqWorkData(INTAPS.RDBMS.SQLHelper reader, int employeeID)
        {
            string sql = @"SELECT     COUNT(*) AS C,d.FormulaID FROM         
                            {0}.dbo.PCDData d INNER JOIN {0}.dbo.DataAppliesTo a
                            ON d.ID = a.DataID
                            WHERE     (d.PCDID = {1}) AND (d.PeriodTo IS NULL) and objectID={2}
                            GROUP BY d.FormulaID
                            HAVING      (COUNT(*) > 1)";
            DataTable c = reader.GetDataTable(string.Format(sql, this.m_Payroll.DBName, m_sysPars.permanentBenefitComponentID, employeeID));
            if (c.Rows.Count>1)
                throw new ServerUserMessage("BUG: more than one work data with no upper time bound found for employee ID:{0} formula ID:{1}",employeeID,c.Rows[0][1]);
        }
            
        public void SetWorkingData(int AID,int employeeID, int periodID, Data_RegularTime regular, OverTimeData overtime, SingleValueData[] othersData, int[] singleValueFormulaID)
        {
            if (periodID == -1)
                throw new ServerUserMessage("-1 passed for parameter PeriodID method SetWorkingData");
            lock (dspWriter)
            {
                dspWriter.setReadDB(m_Payroll.DBName);
                try
                {
                    dspWriter.BeginTransaction();
                    PayrollDocument[] docs = m_Payroll.GetPayrolls(new AppliesToEmployees(employeeID), periodID);
                    if (docs != null)
                    {
                        if (docs.Length > 0)
                            throw new INTAPS.ClientServer.ServerUserMessage("You cannot save data since payroll is already generated for the selected period!");
                    }

                    foreach (PayrollComponentData d in m_Payroll.GetPCData(SysPars.RegularTimePayrollComponentID, SysPars.RegularTimePayrollFormulaID, AppliesToObjectType.Employee, employeeID, periodID, false))
                        m_Payroll.DeleteData(AID, d.ID);
                    foreach (PayrollComponentData d in m_Payroll.GetPCData(SysPars.OverTimePayrollComponentID, SysPars.OverTimePayrollFormulaID, AppliesToObjectType.Employee, employeeID, periodID, false))
                        m_Payroll.DeleteData(AID, d.ID);
                    foreach (PayrollComponentData d in m_Payroll.GetPCData(SysPars.SingleValuePayrollComponentID, -1, AppliesToObjectType.Employee, employeeID, periodID, false))
                        m_Payroll.DeleteData(AID, d.ID);

                    PayrollComponentData regularData = new PayrollComponentData();
                    PayrollComponentFormula regularFormula = m_Payroll.GetPCFormula(SysPars.RegularTimePayrollFormulaID);
                    if (regularFormula != null && regular != null)
                    {
                        regularData.FormulaID = regularFormula.ID;
                        regularData.at = new INTAPS.Payroll.AppliesToEmployees(employeeID);
                        regularData.DataDate = DateTime.Now;
                        regularData.PCDID = SysPars.RegularTimePayrollComponentID;
                        regularData.periodRange = new PayPeriodRange(periodID, periodID);

                        m_Payroll.CreatePCData(AID, regularData, regular);
                    }
                    PayrollComponentData overtimeData = new PayrollComponentData();
                    PayrollComponentFormula overtimeFormula = m_Payroll.GetPCFormula(SysPars.OverTimePayrollFormulaID);
                    if (overtimeFormula != null && overtime != null)
                    {
                        overtimeData.FormulaID = overtimeFormula.ID;
                        overtimeData.at = new INTAPS.Payroll.AppliesToEmployees(employeeID);
                        overtimeData.DataDate = DateTime.Now;
                        overtimeData.PCDID = SysPars.OverTimePayrollComponentID;
                        overtimeData.periodRange = new PayPeriodRange(periodID, periodID);
                        m_Payroll.CreatePCData(AID, overtimeData, overtime);
                    }
                    for (int i = 0; i < singleValueFormulaID.Length; i++)
                    {
                        PayrollComponentFormula f = m_Payroll.GetPCFormula(singleValueFormulaID[i]);
                        PayrollComponentData singleValueData = new PayrollComponentData();
                        singleValueData.at = new INTAPS.Payroll.AppliesToEmployees(employeeID);
                        singleValueData.FormulaID = singleValueFormulaID[i];
                        singleValueData.DataDate = DateTime.Now;
                        singleValueData.PCDID = f.PCDID;
                        if (f.PCDID == m_sysPars.SingleValuePayrollComponentID)
                        {
                            singleValueData.periodRange = new PayPeriodRange(periodID, periodID);
                            m_Payroll.CreatePCData(AID, singleValueData, othersData[i]);
                        }
                        else if (f.PCDID == m_sysPars.permanentBenefitComponentID)
                        {
                            setPermanentWorkingData(AID, employeeID, periodID, f, singleValueData, othersData[i]);
                        }
                        
                    }
                    assertUniqWorkData(dspWriter, employeeID);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public void setPermanentWorkingData(int AID, int employeeID, int periodID, int formulaID,PayrollComponentData singleValueData,object additionalData)
        {
            setPermanentWorkingData(AID, employeeID, periodID, m_Payroll.GetPCFormula(formulaID), singleValueData,additionalData);
        }
        private void setPermanentWorkingData(int AID, int employeeID, int periodID, PayrollComponentFormula formula, PayrollComponentData singleValueData, object additionalData)
        {
            dspWriter.setReadDB(m_Payroll.DBName);
            try
            {
                PayrollPeriod thisPeriod = m_Payroll.GetPayPeriod(periodID);
                PayrollComponentData[] data = m_Payroll.GetPCData(formula.PCDID, formula.ID, AppliesToObjectType.Employee, employeeID, periodID, false);
                if (data.Length > 1)
                    throw new ServerUserMessage("Database error multiple permanent benefit found for period " + thisPeriod.name + ". Fix the problem using the payroll client and try again");
                if (data.Length == 1)
                {
                    PayrollPeriod prd = m_Payroll.GetPayPeriod(data[0].periodRange.PeriodFrom);
                    if (prd.fromDate < thisPeriod.fromDate)
                    {
                        PayrollPeriod prev = INTAPS.Accounting.BDE.AccountingPeriodHelper.GetPreviousPeriod<PayrollPeriod>(dspWriter, "PayPeriod", thisPeriod);
                        if (prev == null)
                            throw new ServerUserMessage("Previos payroll pariod not generated for " + thisPeriod.name);
                        singleValueData.periodRange = new PayPeriodRange(periodID, data[0].periodRange.PeriodTo);
                        data[0].periodRange.PeriodTo = prev.id;
                        m_Payroll.UpdatePCData(AID, data[0]);
                    }
                    else if (prd.fromDate.Equals(thisPeriod.fromDate))
                    {
                        m_Payroll.DeleteData(AID, data[0].ID);
                        singleValueData.periodRange = new PayPeriodRange(periodID, data[0].periodRange.PeriodTo);
                    }
                    else
                    {
                        throw new ServerUserMessage("BUG: unexpected case. GetPCData returned data that applies to future date");
                    }
                }
                else
                {
                    PayrollComponentData[] allData = m_Payroll.GetPCData(formula.PCDID, formula.ID, AppliesToObjectType.Employee, employeeID, -1, false);
                    DateTime lowerBound = DateTime.Now;
                    PayrollPeriod lowerBoundPeriod = null;
                    foreach (PayrollComponentData c in allData)
                    {
                        PayrollPeriod pp = m_Payroll.GetPayPeriod(c.periodRange.PeriodFrom);
                        if (lowerBoundPeriod == null || pp.fromDate < lowerBound)
                        {
                            lowerBound = pp.fromDate;
                            lowerBoundPeriod = pp;
                        }
                    }
                    if (lowerBoundPeriod != null && lowerBound > thisPeriod.fromDate)
                        lowerBoundPeriod = INTAPS.Accounting.BDE.AccountingPeriodHelper.GetPreviousPeriod<PayrollPeriod>(dspWriter, "PayPeriod", lowerBoundPeriod);
                    singleValueData.periodRange = new PayPeriodRange(periodID, lowerBoundPeriod == null ? -1 : lowerBoundPeriod.id);
                }
                m_Payroll.CreatePCData(AID, singleValueData, additionalData);
            }
            finally
            {
                dspWriter.restoreReadDB();
            }
        }


        class GroupPayrollFilter : INTAPS.RDBMS.IObjectFilter<AccountDocument>
        {

            INTAPS.Accounting.BDE.AccountingBDE accounting;
            List<GroupPayrollDocument> filtered;
            int periodID;
            int taxCenter;
            public GroupPayrollFilter(INTAPS.Accounting.BDE.AccountingBDE accounting, List<GroupPayrollDocument> filtered, int periodID,int taxCenter)
            {
                this.accounting = accounting;
                this.filtered = filtered;
                this.periodID = periodID;
                this.taxCenter = taxCenter;
            }
            #region IObjectFilter<GroupPayrollDocument> Members

            public bool Include(AccountDocument obj, object[] additional)
            {
                GroupPayrollDocument groupPayroll = (GroupPayrollDocument)accounting.GetAccountDocument(obj.AccountDocumentID, true);
                if (groupPayroll.periodID == this.periodID && groupPayroll.taxCenter == this.taxCenter)
                {
                    filtered.Add(groupPayroll);
                    return true;
                }
                return false;
            }

            #endregion
        }
        class GroupPayrollPaymentFilter : INTAPS.RDBMS.IObjectFilter<AccountDocument>
        {

            INTAPS.Accounting.BDE.AccountingBDE accounting;
            List<GroupPayrollPaymentDocument> filtered;
            int periodID;
            int taxCenter;
            public GroupPayrollPaymentFilter(INTAPS.Accounting.BDE.AccountingBDE accounting, List<GroupPayrollPaymentDocument> filtered, int periodID, int taxCenter)
            {
                this.accounting = accounting;
                this.filtered = filtered;
                this.periodID = periodID;
                this.taxCenter = taxCenter;
            }
            #region IObjectFilter<GroupPayrollDocument> Members

            public bool Include(AccountDocument obj, object[] additional)
            {
                GroupPayrollPaymentDocument groupPayroll = (GroupPayrollPaymentDocument)accounting.GetAccountDocument(obj.AccountDocumentID, true);
                if (groupPayroll.periodID == this.periodID && groupPayroll.taxCenter == this.taxCenter)
                {
                    filtered.Add(groupPayroll);
                    return true;
                }
                return false;
            }

            #endregion
        }
        public double GetGroupPayrollUnclaimed(int periodID,int taxCenter)
        {
            INTAPS.RDBMS.SQLHelper dspReader = m_accounting.GetReaderHelper();
            int N;
            double totalUnclaimed = 0;
            try
            {
                List<GroupPayrollDocument> filtered = new List<GroupPayrollDocument>();
                GroupPayrollFilter f = new GroupPayrollFilter(m_accounting, filtered, periodID, taxCenter);
                dspReader.GetSTRArrayByFilter<AccountDocument>("DocumentTypeID=" + m_accounting.GetDocumentTypeByType(typeof(GroupPayrollDocument)).id
                , 0, -1, out N, f);
                GroupPayrollDocument[] group = filtered.ToArray();
                foreach (GroupPayrollDocument gp in group)
                {
                    totalUnclaimed += gp.unclaimedSalary;
                }
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
            return totalUnclaimed;
        }

        public double GetGroupPayrollTotalSalaryPaid(int periodID, int taxCenter)
        {
            INTAPS.RDBMS.SQLHelper dspReader = m_accounting.GetReaderHelper();
            int N;
            double amount = 0;
            try
            {
                
                List<GroupPayrollPaymentDocument> filtered = new List<GroupPayrollPaymentDocument>();
                GroupPayrollPaymentFilter f = new GroupPayrollPaymentFilter(m_accounting, filtered, periodID, taxCenter);
                dspReader.GetSTRArrayByFilter<AccountDocument>("DocumentTypeID=" + m_accounting.GetDocumentTypeByType(typeof(GroupPayrollPaymentDocument)).id
                , 0, -1, out N, f);
                GroupPayrollPaymentDocument[] group = filtered.ToArray();
                foreach (GroupPayrollPaymentDocument doc in group)
                {
                    amount += doc.amount;
                }
                
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
            return amount;
        }
        public DocType[] getEmployeeReturnDocuments<DocType>(int employeeID) where DocType : StaffReturnDocument
        {
            INTAPS.RDBMS.SQLHelper dspReader = m_accounting.GetReaderHelper();
            try
            {
                int N;
                List<DocType> ret=new List<DocType>();
                foreach (DocType dt in Accounting.GetAccountDocuments(null, Accounting.GetDocumentTypeByType(typeof(DocType)).id, false, DateTime.Now, DateTime.Now, 0, -1, out N))
                {
                    if (dt.employeeID == employeeID)
                        ret.Add(dt);
                }
                return ret.ToArray();
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public void closeLoan(int AID,int loanDocumentID)
        {
            StaffLoandDocumentBase doc=m_accounting.GetAccountDocument(loanDocumentID, true) as StaffLoandDocumentBase;
            foreach(PayrollComponentData data in doc.data)
            {
                m_Payroll.DeleteData(AID, data.ID);
            }
            doc.data = new PayrollComponentData[0];
            doc.closed = true;
            m_accounting.UpdateAccountDocumentData(AID, doc);
        }

    }
}
