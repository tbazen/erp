using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using System.Data.SqlClient;
using System.Reflection;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        Dictionary<int, FixedAssetRuleAttribute> _fixedAssetRuleAttributes=null;
        Dictionary<int, IFixedAssetServerRule> _fixedAssetRules=null;
        void loadFixedAssetRules()
        {
            _fixedAssetRuleAttributes = new Dictionary<int, FixedAssetRuleAttribute>();
            _fixedAssetRules = new Dictionary<int, IFixedAssetServerRule>();
            string assemblies = System.Configuration.ConfigurationManager.AppSettings["fixedAssetRuleAssembiles"];
            if (assemblies == null)
                return;
            foreach (string an in assemblies.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                try
                {
                    Assembly a = System.Reflection.Assembly.Load(an);
                    int count = 0;
                    foreach (Type t in a.GetTypes())
                    {
                        object[] atr = t.GetCustomAttributes(typeof(FixedAssetRuleAttribute), true);
                        if (atr == null || atr.Length == 0)
                            continue;
                        if (atr.Length > 1)
                        {
                            ApplicationServer.EventLoger.Log(EventLogType.Errors, "Multilpe FixedAssetRuleAttribute attribtues can't be set for a single fixed asset rule server");
                            continue;
                        }

                        FixedAssetRuleAttribute fatr = atr[0] as FixedAssetRuleAttribute;
                        ConstructorInfo ci = t.GetConstructor(new Type[] { typeof(iERPTransactionBDE) });
                        if (ci == null)
                        {
                            ApplicationServer.EventLoger.Log(EventLogType.Errors, "Constructor not found for type ");
                            continue;
                        }
                        try
                        {
                            IFixedAssetServerRule o = ci.Invoke(new object[] { this }) as IFixedAssetServerRule;
                            if (o == null)
                            {
                                ApplicationServer.EventLoger.Log(EventLogType.Errors, "Fixedasset rule server rule " + t + " doesn't implement IFixedAssetServerRule");
                                continue;
                            }
                            if (_fixedAssetRules.ContainsKey(fatr.id))
                            {
                                ApplicationServer.EventLoger.Log(EventLogType.Errors, "Fixedasset rule server rule " + t + " is useing an ID that is already used " + fatr.id);
                                continue;
                            }
                            _fixedAssetRuleAttributes.Add(fatr.id, fatr);
                            _fixedAssetRules.Add(fatr.id, o);
                            count++;
                        }
                        catch (Exception tex)
                        {
                            ApplicationServer.EventLoger.LogException("Error trying to instantiate fixedasset rule server " + t, tex);
                        }
                    }
                    if (count > 0)
                    {
                        ApplicationServer.EventLoger.Log(string.Format("{0} fixedasset rule servers(s) loaded from {1}", count, an));
                    }
                }
                catch (Exception ex)
                {
                    ApplicationServer.EventLoger.LogException("Error trying to load fixedasset rule server assembly " + an, ex);
                }
            }
        }
        IFixedAssetServerRule getFixedAssetRuleServer(int typeID, bool throwException)
        {
            if (!_fixedAssetRules.ContainsKey(typeID))
            {
                if (throwException)
                    throw new ServerUserMessage("Invalid server handler :" + typeID);
                return null;
            }
            return _fixedAssetRules[typeID];

        }
        // BDE exposed
        public FixedAssetRuleAttribute getFixedAssetRuleAttribute(int typeID)
        {
            if (_fixedAssetRuleAttributes.ContainsKey(typeID))
                return _fixedAssetRuleAttributes[typeID];
            return null;
        }
        // BDE exposed
        public FixedAssetRuleAttribute[] getAllFixedAssetRuleAttributes()
        {
            FixedAssetRuleAttribute[] ret = new FixedAssetRuleAttribute[_fixedAssetRuleAttributes.Count];
            int i = 0;
            foreach (KeyValuePair<int, FixedAssetRuleAttribute> kv in _fixedAssetRuleAttributes)
            {
                ret[i] = kv.Value;
                i++;
            }
            return ret;
        }
        // BDE exposed
        public void saveFixedAssetRuleSetting(int typeID, object data)
        {
            getFixedAssetRuleServer(typeID, true).setConfiguration(data);
        }
        public int getDepreciationDocument(DateTime time)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                object _ID = helper.ExecuteScalar(string.Format("Select Top 1 ID from {0}.dbo.Document where tranTicks>{1} and DocumentTypeID={2} order by tranTicks"
                   , m_accounting.DBName, time.Ticks, m_accounting.GetDocumentTypeByType(typeof(FixedAssetDepreciationDocument)).id));
                if (_ID is int)
                    return (int)_ID;
                return -1;
            }

            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public double GetDepreciation(int costCenteriD, string itemCode, DateTime dateTime)
        {
            TransactionItems titem = GetTransactionItems(itemCode);
            double rate;
            double year;
            return GetDepreciation(costCenteriD, titem, dateTime, out rate, out year);
        }
        public double GetDepreciation(int costCenteriD, string itemCode, DateTime dateTime
            , out double rate, out double year)
        {
            TransactionItems titem = GetTransactionItems(itemCode);
            return GetDepreciation(costCenteriD, titem, dateTime, out rate, out year);
        }
        public double GetDepreciation(int costCenterID, TransactionItems titem
            , DateTime dateTime
            , out double rate
            , out double year)
        {
            
            IFixedAssetServerRule rule = getFixedAssetRuleServer(titem.DepreciationType,false);
            if(rule==null)
            {
                rate = 0;
                year = 0;
                return 0;
            }
            return rule.calculateDepreciation(costCenterID, titem, dateTime, out rate, out year);
        }
       
        public FixedAssetDepreciationDocument previewDepreciation(DateTime time)
        {
            int depDocTypeID = getDepreciationDocument(time);
            if (depDocTypeID != -1)
                throw new ServerUserMessage("Depreciation already posted");
            
            List<TransactionDocumentItem> items = new List<TransactionDocumentItem>();
            TransactionItems[] fixedAssetItems = getAllFixedAssetItems();
            
                foreach (TransactionItems fixedAsset in fixedAssetItems)
                {
                    CostCenterAccount[] css = m_accounting.GetCostCenterAccountsOf<Account>(fixedAsset.originalFixedAssetAccountID);
                    foreach (CostCenterAccount cs in css)
                    {
                        if (cs.childCostCenterCount > 0)
                            continue;
                        if (GetStoreInfo(cs.costCenterID) != null)
                            continue;

                        double year;
                        double rate;
                        double dep = GetDepreciation(cs.costCenterID, fixedAsset.Code, time, out rate, out year);
                        if (AccountBase.AmountEqual(dep, 0))
                            continue;
                        TransactionDocumentItem item = new TransactionDocumentItem();
                        item.code = fixedAsset.Code;
                        item.costCenterID = cs.costCenterID;
                        item.name = fixedAsset.Name;
                        item.unitPrice = dep / (year * rate);
                        item.quantity = year;
                        item.rate = rate;
                        item.price = dep;
                        items.Add(item);
                    }
                }
            FixedAssetDepreciationDocument ret = new FixedAssetDepreciationDocument();
            ret.DocumentDate = time;
            ret.ShortDescription = "Fixed asset depreciation as of " + AccountBase.FormatDate(time);
            ret.items = items.ToArray();
            return ret;
        }

    }

}