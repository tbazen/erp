using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using System.Data.SqlClient;
using System.Data;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
using System.Reflection;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE : INTAPS.ClientServer.BDEBase
    {
        static List<string> getAllCategoryAccountField<CategoryType>()
        {
            List<string> ret = new List<string>();
            foreach (FieldInfo fi in typeof(CategoryType).GetFields())
            {
                object[] atr = fi.GetCustomAttributes(typeof(AIParentAccountFieldAttribute), true);
                if (atr == null || atr.Length == 0)
                    continue;
                ret.Add(fi.Name);
            }
            return ret;
        }
        void addCategorizedItemToCOA<CategoryType, ItemType>(int AID, CategoryType cat, ItemType item) where ItemType : IAccountingItem
        {
            //Dictionary<string, bool> isParentACostCenter = new Dictionary<string, bool>();

            Dictionary<string, int> parentFields = new Dictionary<string, int>();

            Dictionary<string, AIParentAccountFieldAttribute> parentFieldsAtr = new Dictionary<string, AIParentAccountFieldAttribute>();
            foreach (FieldInfo fi in typeof(CategoryType).GetFields())
            {
                object[] atr = fi.GetCustomAttributes(typeof(AIParentAccountFieldAttribute), true);
                if (atr == null || atr.Length == 0)
                    continue;
                AIParentAccountFieldAttribute a = atr[0] as AIParentAccountFieldAttribute;
                int ac = (int)fi.GetValue(cat);
                parentFields.Add(fi.Name, ac);
                //isParentACostCenter.Add(fi.Name, a.isCostCenter);
                parentFieldsAtr.Add(fi.Name, a);

            }
            foreach (FieldInfo fi in typeof(ItemType).GetFields())
            {
                object[] atr = fi.GetCustomAttributes(typeof(AIChildOfAttribute), true);
                if (atr == null || atr.Length == 0)
                    continue;
                AIChildOfAttribute a = atr[0] as AIChildOfAttribute;
                int parentAccountID = -1;
                string fieldName = null;
                bool isCostCenter = false;
                AIParentAccountFieldAttribute catr = null;
                foreach (string cf in a.categoryField)
                {
                    if (!parentFields.ContainsKey(cf))
                        throw new ServerUserMessage(string.Format("Type {0} field {1} don't have corresponding AIParentAccountField attribute in {2}", typeof(ItemType), fi.Name, typeof(CategoryType)));
                    isCostCenter = parentFieldsAtr[cf].isCostCenter;
                    int pid = parentFields[cf];
                    catr = parentFieldsAtr[cf];
                    if (pid != -1)
                    {
                        if (parentAccountID == -1)
                        {
                            parentAccountID = pid;
                            fieldName = cf;
                        }
                        else
                        {
                            throw new ServerUserMessage(string.Format("Ambiguous parent accounts. {0} and {1}", fieldName, cf));
                        }
                    }
                }


                int ac = (int)fi.GetValue(item);
                if (ac == -1)
                {
                    if (parentAccountID == -1)
                        continue;
                    AccountBase account;
                    if (isCostCenter)
                        account = new CostCenter();
                    else
                        account = new Account();
                    AccountBase parentAccount = isCostCenter ? (AccountBase)m_accounting.GetAccount<CostCenter>(parentAccountID) : (AccountBase)m_accounting.GetAccount<Account>(parentAccountID);
                    if (parentAccount == null)
                        continue;
                    if (catr.createChildAccount)
                    {
                        account.PID = parentAccountID;
                        account.Name = parentAccount.Name + "-" + item.coaName;
                        account.Code = parentAccount.Code + "-" + item.coaCode;
                        account.CreationDate = account.ActivateDate = DateTime.Now;
                        account.Description = "Automatically Created Item - don't modify or delete";
                        account.protection = AccountProtection.SystemAccount;
                        account.Status = AccountStatus.Activated;

                        AccountBase test = isCostCenter ? (AccountBase)m_accounting.GetAccount<CostCenter>(account.Code) : (AccountBase)m_accounting.GetAccount<Account>(account.Code);
                        if (test != null)
                            throw new ServerUserMessage("The code " + account.Code + " is already used");
                        if (isCostCenter)
                            ac = m_accounting.CreateAccount<CostCenter>(AID, (CostCenter)account);
                        else
                            ac = m_accounting.CreateAccount<Account>(AID, (Account)account);
                        fi.SetValue(item, ac);
                    }
                    else
                        fi.SetValue(item, parentAccountID);
                }
                else
                {
                    if (parentAccountID == -1)
                    {
                        if (catr.createChildAccount)
                        {
                            if (isCostCenter)
                                deleteAccountIFExists<CostCenter>(AID, ac);
                            else
                                deleteAccountIFExists<Account>(AID, ac);
                        }
                        fi.SetValue(item, -1);
                    }
                    else
                    {
                        AccountBase account;
                        if (isCostCenter)
                            account = m_accounting.GetAccount<CostCenter>(ac);
                        else
                            account = m_accounting.GetAccount<Account>(ac);
                        if (account != null)
                        {
                            if (catr.createChildAccount)
                            {
                                if (account.PID == parentAccountID)
                                {
                                    AccountBase parentAccount = isCostCenter ? (AccountBase)m_accounting.GetAccount<CostCenter>(parentAccountID) : (AccountBase)m_accounting.GetAccount<Account>(parentAccountID);
                                    account.PID = parentAccountID;
                                    account.Name = parentAccount.Name + "-" + item.coaName;
                                    account.Code = parentAccount.Code + "-" + item.coaCode;
                                    account.Code = (isCostCenter ? m_accounting.GetAccount<CostCenter>(account.PID).Code : m_accounting.GetAccount<Account>(account.PID).Code) + "-" + item.coaCode;
                                    if (isCostCenter)
                                        m_accounting.UpdateAccount<CostCenter>(AID, (CostCenter)account);
                                    else
                                        m_accounting.UpdateAccount<Account>(AID, (Account)account);
                                }
                                else
                                {
                                    if (isCostCenter)
                                        deleteAccountIFExists<CostCenter>(AID, ac);
                                    else
                                        deleteAccountIFExists<Account>(AID, ac);
                                    if (isCostCenter)
                                        account = new CostCenter();
                                    else
                                        account = new Account();
                                    AccountBase parentAccount = isCostCenter ? (AccountBase)m_accounting.GetAccount<CostCenter>(parentAccountID) : (AccountBase)m_accounting.GetAccount<Account>(parentAccountID);

                                    account.PID = parentAccountID;
                                    account.Name = parentAccount.Name + "-" + item.coaName;
                                    account.Code = parentAccount.Code + "-" + item.coaCode;
                                    account.CreationDate = account.ActivateDate = DateTime.Now;
                                    account.Description = "Automatically Created Item - don't modify or delete";
                                    account.protection = AccountProtection.SystemAccount;
                                    account.Status = AccountStatus.Activated;

                                    AccountBase test = isCostCenter ? (AccountBase)m_accounting.GetAccount<CostCenter>(account.Code) : (AccountBase)m_accounting.GetAccount<Account>(account.Code);
                                    if (test != null)
                                        throw new ServerUserMessage("The code " + account.Code + " is already used");
                                    if (isCostCenter)
                                        ac = m_accounting.CreateAccount<CostCenter>(AID, (CostCenter)account);
                                    else
                                        ac = m_accounting.CreateAccount<Account>(AID, (Account)account);
                                    fi.SetValue(item, ac);
                                }
                            }
                            else
                            {
                                if (account.id != parentAccountID)
                                {
                                    fi.SetValue(item, parentAccountID);
                                }
                            }
                        }
                    }
                }
            }

        }
    }
}

