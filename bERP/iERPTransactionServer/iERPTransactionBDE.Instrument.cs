using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        // BDE exposed
        public PaymentInstrumentType[] getAllPaymentInstrumentTypes()
        {
             INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<PaymentInstrumentType>(null);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public PaymentInstrumentType getPaymentInstrumentType(string itemCode)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                PaymentInstrumentType []ret= dspReader.GetSTRArrayByFilter<PaymentInstrumentType>("itemCode='"+itemCode+"'");
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public void savePaymentInstrumentType(int AID, PaymentInstrumentType type)
        {
            TransactionItems tranItem=GetTransactionItems(type.itemCode);
            if(tranItem==null)
                throw new ServerUserMessage("Invalid item code "+type.itemCode);
            PaymentInstrumentType old = getPaymentInstrumentType(type.itemCode);
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    if (old == null)
                    {
                        dspWriter.InsertSingleTableRecord(this.DBName, type, new string[] { "__AID" }, new object[] { AID });
                        ApplicationServer.sendDiffMsg(AID, GenericDiff.getUpdateDiff(type.itemCode, type));
                    }
                    else
                    {
                        dspWriter.logDeletedData(AID, this.DBName + ".dbo.PaymentInstrumentType", "itemCode='" + type.itemCode + "'");
                        dspWriter.UpdateSingleTableRecord(this.DBName, type, new string[] { "__AID" }, new object[] { AID });
                        ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(type.itemCode, type));
                    }
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        // BDE exposed
        public void deletePaymentInstrumentType(int AID, string typeID)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.PaymentInstrumentType", "itemCode='" + typeID + "'");
                    dspWriter.DeleteSingleTableRecrod<PaymentInstrumentType>(this.DBName, "itemCode='" + typeID+"'");
                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getDeleteDiff(typeof(PaymentInstrumentType), typeID));
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        // BDE exposed
        public BankInfo getBankInfo(int bankID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                BankInfo[] ret = dspReader.GetSTRArrayByFilter<BankInfo>("id=" + bankID);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public BankBranchInfo getBankBranchInfo(int branchID)
        {
             INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                BankBranchInfo[] ret = dspReader.GetSTRArrayByFilter<BankBranchInfo>("id=" + branchID);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public BankInfo[] getAllBanks()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<BankInfo>(null);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }

        }
        // BDE exposed
        public BankBranchInfo[] getAllBranchsOfBank(int bankID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<BankBranchInfo>("bankID=" + bankID);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        // BDE exposed
        public int saveBankInfo(int AID, BankInfo bank)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    if (bank.id == -1)
                    {
                        bank.id = AutoIncrement.GetKey("bankID");
                        dspWriter.InsertSingleTableRecord(this.DBName, bank, new string[] { "__AID" }, new object[] { AID });
                        ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(bank.id, bank));
                    }
                    else
                    {
                        dspWriter.logDeletedData(AID, this.DBName + ".dbo.BankInfo", "id=" + bank.id);
                        dspWriter.UpdateSingleTableRecord(this.DBName, bank, new string[] { "__AID" }, new object[] { AID });
                        ApplicationServer.sendDiffMsg(AID, GenericDiff.getUpdateDiff(bank.id, bank));
                    }
                    dspWriter.CommitTransaction();
                    return bank.id;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        // BDE exposed
        public int saveBankBranchInfo(int AID, BankBranchInfo branch)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    if (branch.id == -1)
                    {
                        branch.id = AutoIncrement.GetKey("branchID");
                        dspWriter.InsertSingleTableRecord(this.DBName, branch, new string[] { "__AID" }, new object[] { AID });
                        ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(branch.id, branch));
                    }
                    else
                    {
                        dspWriter.logDeletedData(AID, this.DBName + ".dbo.BankBranchInfo", "id=" + branch.id);
                        dspWriter.UpdateSingleTableRecord(this.DBName, branch, new string[] { "__AID" }, new object[] { AID });
                        ApplicationServer.sendDiffMsg(AID, GenericDiff.getUpdateDiff(branch.id, branch));
                    }
                    dspWriter.CommitTransaction();
                    return branch.id;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        // BDE exposed
        public void deleteBankInfo(int AID, int bankID)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {

                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.BankInfo", "id=" + bankID);
                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getDeleteDiff(typeof(BankInfo),bankID));
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        // BDE exposed
        public void deleteBankBranchInfo(int AID, int branchID)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {

                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.BankBranchInfo", "id=" + branchID);
                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getDeleteDiff(typeof(BankBranchInfo), branchID));
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
    }
}
