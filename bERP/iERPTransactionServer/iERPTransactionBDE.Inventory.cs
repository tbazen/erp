using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using System.Data.SqlClient;
using System.Reflection;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        // BDE exposed
        public void recalculateInventoryWeightedAverage(int AID)
        {
            int N;
            dspWriter.setReadDB(this.Accounting.DBName);
            try
            {

                Accounting.PushProgress("Loading issue transactions..");
                DateTime closedUpdto = this.getTransactionsClosedUpto();
                List<AccountDocument> docs = new List<AccountDocument> (Accounting.GetAccountDocuments(null, Accounting.GetDocumentTypeByType(typeof(StoreIssueDocument)).id,
                                true, closedUpdto, DateTime.Now, 0, -1, out N));
                docs.AddRange(Accounting.GetAccountDocuments(null, Accounting.GetDocumentTypeByType(typeof(SoldItemDeliveryDocument)).id,
                                true, closedUpdto, DateTime.Now, 0, -1, out N));                
                Accounting.SetProgress(docs.Count+ " store issues found");
                Console.WriteLine(docs.Count + " store issues found");
                Console.WriteLine();

                for (int i = docs.Count - 1; i >= 0; i--)
                {
                    //Console.CursorTop--;
                    Console.WriteLine(i+"                    ");
                    AccountDocument acDoc = Accounting.GetAccountDocument(docs[i].AccountDocumentID, true);
                    Accounting.PostGenericDocument(AID, acDoc);
                    Accounting.SetProgress((docs.Count-i)/((double)docs.Count));
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
            finally
            {
                dspWriter.restoreReadDB();
                Accounting.PopProgress();
            }
        }
    }
}