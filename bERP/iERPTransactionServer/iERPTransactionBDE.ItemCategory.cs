using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {

        #region Item Category Methods
        public int RegisterItemCategory(int AID, ItemCategory category)
        {
            lock (dspWriter)
            {
                try
                {
                    dspWriter.BeginTransaction();
                    int catID = -1;
                    if (category.ID == -1) //INSERT
                    {
                        
                        if ((int)dspWriter.ExecuteScalar("Select count(*) From "+this.DBName+".dbo.ItemCategory where Code='" + category.Code + "'") > 0)
                            throw new ServerUserMessage("Category Code already exists! Please provide different category code");
                        int categID = INTAPS.ClientServer.AutoIncrement.GetKey("iERP.ItemCategoryID");
                        category.ID = categID;
                        dspWriter.InsertSingleTableRecord(m_DBName, category, new string[] { "__AID" }, new object[] { AID });
                        catID = category.ID;
                    }
                    else //UPDATE
                    {
                        ItemCategory oldCat = GetItemCategory(category.ID);
                        if (oldCat == null)
                            throw new ServerUserMessage("Invalid category ID:" + category.ID);
                        logExistingData(AID, ".dbo.ItemCategory", "ID=" + category.ID);
                        dspWriter.UpdateSingleTableRecord(base.DBName, category, new string[] { "__AID" }, new object[] { AID });


                        bool notequal=false;
                        foreach (string f in getAllCategoryAccountField<ItemCategory>())
                        {
                            System.Reflection.FieldInfo fi = typeof(ItemCategory).GetField(f);
                            if (!fi.GetValue(oldCat).Equals(fi.GetValue(category)))
                            {
                                notequal = true;
                                break;
                            }
                        }
                        if (notequal)
                        {
                            foreach (TransactionItems item in GetItemsInCategory(category.ID))
                            {
                                try
                                {
                                    RegisterTransactionItem(AID, item, null);
                                }
                                catch (Exception ex)
                                {
                                    throw new ServerUserMessage(string.Format("Error trying to update item {0}.\n{1}", item.Code, ex.Message));
                                }
                            }

                        }
                        catID = category.ID;
                    }
                    dspWriter.CommitTransaction();
                    return catID;
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        // BDE exposed
        public ItemCategory GetItemCategory(string Code)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                ItemCategory[] categs = dspReader.GetSTRArrayByFilter<ItemCategory>("Code='" + Code + "'");
                if (categs.Length == 0)
                    return null;
                return categs[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public ItemCategory GetItemCategory(int ID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                ItemCategory[] categs = dspReader.GetSTRArrayByFilter<ItemCategory>("ID=" + ID);
                if (categs.Length == 0)
                    return null;
                return categs[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public ItemCategory[] GetItemCategories(int PID, ItemType itemType)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                string filter = "";
                if (itemType == ItemType.Expense)
                {
                    filter = "PID=" + PID + " AND (expenseAccountPID!=-1 OR directCostAccountPID!=-1)";
                }
                else if (itemType == ItemType.Sales)
                {
                    filter = "PID=" + PID + " AND (salesAccountPID!=-1)";
                }
                else
                {
                    filter = "PID=" + PID + " AND (originalFixedAssetAccountPID!=-1 AND depreciationAccountPID!=-1 AND accumulatedDepreciationAccountPID!=-1)";
                }
                ItemCategory[] categs = dspReader.GetSTRArrayByFilter<ItemCategory>(filter);
                if (categs.Length == 0)
                    return null;
                return categs;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public ItemCategory[] GetItemCategories(int PID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                string filter = "PID=" + PID;
                ItemCategory[] categs = dspReader.GetSTRArrayByFilter<ItemCategory>(filter);
                return categs;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public TransactionItems[] GetItemsInCategory(int categID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                TransactionItems[] items = dspReader.GetSTRArrayByFilter<TransactionItems>("categoryID=" + categID);
                return items;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        void GetItemsInCategoryRecursive(INTAPS.RDBMS.SQLHelper dspReader, int categID, List<TransactionItems> items)
        {
            items.AddRange(dspReader.GetSTRArrayByFilter<TransactionItems>("categoryID=" + categID));
            foreach (ItemCategory cat in GetItemCategories(categID))
                GetItemsInCategoryRecursive(dspReader, cat.ID, items);
        }
        public TransactionItems[] GetItemsInCategoryRecursive(int categID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                List<TransactionItems> items = new List<TransactionItems>();
                GetItemsInCategoryRecursive(dspReader, categID, items);
                return items.ToArray();
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public ItemCategory[] GetAllItemCategories()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<ItemCategory>(null);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public void DeleteItemCategory(int AID, int ID)
        {
            lock (dspWriter)
            {

                try
                {
                    dspWriter.BeginTransaction();
                    logExistingData(AID, ".dbo.ItemCategory", "ID=" + ID);
                    if ((int)dspWriter.ExecuteScalar(String.Format("select count(*) from {1}.dbo.TransactionItems where categoryID={0}", ID,this.DBName)) > 0)
                    {
                        throw new INTAPS.ClientServer.ServerUserMessage("You cannot delete this category since one ore more items exist int it!");
                    }

                    dspWriter.DeleteSingleTableRecrod<ItemCategory>(base.DBName, ID);
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        #endregion

    }
}