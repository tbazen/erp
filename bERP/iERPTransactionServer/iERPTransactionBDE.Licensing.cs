using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Payroll;
using System.Data.SqlClient;
using System.Data;
using INTAPS.ClientServer;
using System.Net.NetworkInformation;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        public static PhysicalAddress GetMacAddress()
        {
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                // Only consider Ethernet network interfaces
                if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet &&
                    nic.OperationalStatus == OperationalStatus.Up)
                {
                    return nic.GetPhysicalAddress();
                }
            }
            return null;
        }
        List<string> licenses = null;
        void loadLicenses()
        {
            PhysicalAddress nic = GetMacAddress();
            if (nic == null)
            {
                ApplicationServer.EventLoger.Log(EventLogType.Errors, "Licensing system couldn't start");
                licenses = new List<string>();
                return;
            }
            byte[] key=nic.GetAddressBytes();
            
            try
            {
                byte[] data = System.IO.File.ReadAllBytes("license.bin");
                System.Security.Cryptography.TripleDES c=System.Security.Cryptography.TripleDESCryptoServiceProvider.Create();
                System.Security.Cryptography.ICryptoTransform t= c.CreateDecryptor();
               
                

            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException("Couldn't load license file", ex);
                licenses = new List<string>();
            }
        }
        public bool licHasFeature(string featureName)
        {
            return licenses.Contains(featureName);
        }
    }
}
