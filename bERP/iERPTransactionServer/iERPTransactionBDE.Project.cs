using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Payroll;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        #region Project Methods
        public string RegisterProject(int AID, Project projectInfo,bool logData)
        {
            lock (dspWriter)
            {
                try
                {
                    dspWriter.BeginTransaction();
                    string ret;
                    if (projectInfo.code == null) //INSERT
                    {
                        string projectName = projectInfo.Name;
                        projectInfo.code = GetNextSerialCode("Project", "code", dspWriter);
                        CostCenter projectCostCenter = CollectProjectCostCenter(projectInfo.code, projectName);

                        int projectCostCenterID = projectCostCenter == null ? -1 : m_accounting.CreateAccount<CostCenter>(AID, projectCostCenter);
                        projectInfo.costCenterID = projectCostCenterID;
                        dspWriter.InsertSingleTableRecord(base.DBName, projectInfo, new string[] { "__AID" }, new object[] { AID });
                        ret = projectInfo.code;
                    }
                    else //UPDATE
                    {
                        Project oldProject = GetProjectInfo(projectInfo.code);
                        CostCenter projectCostCenter = m_accounting.GetAccount<CostCenter>(projectInfo.costCenterID);
                        if (projectCostCenter != null)
                        {
                            projectCostCenter.Name = projectInfo.Name;
                            if (projectCostCenter.Status == AccountStatus.Activated)
                            {
                                projectCostCenter.DeactivateDate = DateTime.MaxValue.Date;
                            }
                            if (!oldProject.Name.Equals(projectInfo.Name))
                                m_accounting.UpdateAccount<CostCenter>(AID, projectCostCenter);
                        }
                        if (logData)
                            dspWriter.logDeletedData(AID, base.DBName + ".dbo.Project", "code='" + projectInfo.code + "'");
                        dspWriter.UpdateSingleTableRecord(base.DBName, projectInfo, new string[] { "__AID" }, new object[] { AID });

                        ret = projectInfo.code;
                    }
                    dspWriter.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public Project GetProjectInfo(string code)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                Project[] projects = dspReader.GetSTRArrayByFilter<Project>("code='" + code + "'");
                if (projects.Length > 0)
                    return projects[0];
                return null;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public Project GetProjectInfo(int projectCostCenterID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                Project[] projects = dspReader.GetSTRArrayByFilter<Project>("costCenterID=" + projectCostCenterID);
                if (projects.Length > 0)
                    return projects[0];
                return null;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public Project[] GetAllProjects()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<Project>(null);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
       
        public void DeleteProject(int AID, string code)
        {
            lock (dspWriter)
            {

                Project project = GetProjectInfo(code);
                try
                {
                    dspWriter.BeginTransaction();
                    dspWriter.logDeletedData(AID, base.DBName + ".dbo.Project", "code='" + code + "'");
                    deleteAccountIFExists<CostCenter>(AID, project.costCenterID);
                    if ((int)dspWriter.ExecuteScalar(String.Format("select count(*) from {1}.dbo.Project where code='{0}'", code, this.DBName)) == 0)
                        throw new INTAPS.ClientServer.ServerUserMessage(String.Format("Error: Project with Code '{0}' not found!", code));
                    dspWriter.DeleteSingleTableRecrod<Project>(base.DBName, code);
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public void ActivateProject(int AID, string code)
        {
            lock (dspWriter)
            {
                Project project = GetProjectInfo(code);

                try
                {
                    dspWriter.BeginTransaction();
                    m_accounting.ActivateAcount<CostCenter>(AID, project.costCenterID, DateTime.Now.Date);
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public void DeactivateProject(int AID, string code)
        {
            lock (dspWriter)
            {
                Project project = GetProjectInfo(code);

                try
                {
                    dspWriter.BeginTransaction();
                    m_accounting.DeactivateAccount<CostCenter>(AID, project.costCenterID, DateTime.Now.Date);
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }

        }

        public int CreateProjectDivision(int AID, int projectCostCenterID, string divisionName)
        {
            string nextDivisionCode = GetNextDivisionCostCenterCode(projectCostCenterID);
            CostCenter divisionCostCenter = CollectDivisionCostCenter(projectCostCenterID, nextDivisionCode, divisionName);

            lock (dspWriter)
            {
                try
                {
                    dspWriter.BeginTransaction();
                    int divisionCostCenterID = m_accounting.CreateAccount<CostCenter>(AID, divisionCostCenter);
                    dspWriter.CommitTransaction();
                    return divisionCostCenterID;
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }

        }
        public int CreateProjectMachinaryAndVehicle(int AID, int projectCostCenterID, string name)
        {
            string nextMVCode = GetNextMachinaryAndVehicleCostCenterCode(projectCostCenterID);
            CostCenter MVCostCenter = CollectDivisionCostCenter(projectCostCenterID, nextMVCode, name);

            lock (dspWriter)
            {
                try
                {
                    dspWriter.BeginTransaction();
                    int MVCostCenterID = m_accounting.CreateAccount<CostCenter>(AID, MVCostCenter);
                    dspWriter.CommitTransaction();
                    return MVCostCenterID;
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }

        }

        public void AddItemAccountsToProject(int AID, TransactionItems[] items, string projectCode)
        {
            lock (dspWriter)
            {
                try
                {
                    dspWriter.BeginTransaction();
                    int N;
                    Dictionary<string, double> projectItems = new Dictionary<string, double>();
                    Project project = GetProjectInfo(projectCode);
                    if (project.projectData.items != null)
                    {
                        for (int i = 0; i < project.projectData.items.Length; i++)
                        {
                            projectItems.Add(project.projectData.items[i], project.projectData.unitPrices[i]);
                        }
                    }
                    foreach (TransactionItems item in items)
                    {
                        if (project != null)
                        {
                            if (!projectItems.ContainsKey(item.Code))
                            {
                                projectItems.Add(item.Code, 0d);
                                _AddItemAccountsToProject(AID, item, project.costCenterID);
                                #region Add Item accounts to project divisions
                                CostCenter[] divisionCostCenters = m_accounting.GetChildAcccount<CostCenter>(project.costCenterID, 0, -1, out N);
                                foreach (CostCenter costCenter in divisionCostCenters)
                                {
                                    _AddItemAccountsToProject(AID, item, costCenter.id);
                                }
                                #endregion
                            }
                        }
                    }
                    string[] projItems = new string[projectItems.Keys.Count];
                    double[] unitPrices = new double[projectItems.Values.Count];
                    projectItems.Keys.CopyTo(projItems, 0);
                    projectItems.Values.CopyTo(unitPrices, 0);
                    project.projectData.items = projItems;
                    project.projectData.unitPrices = unitPrices;
                    this.RegisterProject(AID, project, true);
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public void AddEmployeeAccountsToProject(int AID, Employee[] employees, string projectCode, int divisionCostCenterID)
        {
            //1. Update employee info to the new project cost center
            //2. Join all employee accounts to the new project cost center
            //3. Update the project which contains the added employee lists
            lock (dspWriter)
            {
                Project project = GetProjectInfo(projectCode);

                try
                {
                    dspWriter.BeginTransaction();
                    foreach (Employee emp in employees)
                    {
                        _RemoveEmpIfPreviouslyEnrolledInProject(AID, emp);
                        emp.costCenterID = divisionCostCenterID;
                        m_Payroll.Update(AID, emp, false);
                        if (!project.projectData.employees.Contains(emp.id))
                            project.projectData.employees.Add(emp.id);
                    }
                    if (employees.Length > 0)
                        RegisterProject(AID, project, true);
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }

        }

        private void _RemoveEmpIfPreviouslyEnrolledInProject(int AID, Employee emp)
        {
            Project project = GetProjectInfo(emp.costCenterID);
            //If project is null, check if the employee cost center is a division and get its project
            if (project == null)
            {
                CostCenter costCenter = m_accounting.GetAccount<CostCenter>(emp.costCenterID);
                project = GetProjectInfo(costCenter.PID);
            }
            if (project != null)
            {
                if (project.projectData.employees.Contains(emp.id))
                {
                    project.projectData.employees.Remove(emp.id);
                    RegisterProject(AID, project, false);
                }
            }
        }
        public void AddDivisionToProject(int AID, string divisionName, string projectCode)
        {
            Project project = GetProjectInfo(projectCode);
            lock (dspWriter)
            {
                try
                {
                    dspWriter.BeginTransaction();
                    int divisionCostCenterID = CreateProjectDivision(AID, project.costCenterID, divisionName);
                    if (project.projectData.items != null)
                    {
                        foreach (string code in project.projectData.items)
                        {
                            TransactionItems item = GetTransactionItems(code);
                            _AddItemAccountsToProject(AID, item, divisionCostCenterID);
                        }
                    }
                    project.projectData.projectDivisions.Add(divisionCostCenterID);
                    this.RegisterProject(AID, project, true);
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public void AddMachinaryAndVehicleToProject(int AID, string name, string projectCode)
        {
            Project project = GetProjectInfo(projectCode);
            lock (dspWriter)
            {
                try
                {
                    dspWriter.BeginTransaction();
                    int mvCostCenterID = CreateProjectMachinaryAndVehicle(AID, project.costCenterID, name);
                    if (project.projectData.items != null)
                    {
                        foreach (string code in project.projectData.items)
                        {
                            TransactionItems item = GetTransactionItems(code);
                            _AddItemAccountsToProject(AID, item, mvCostCenterID);
                        }
                    }
                    project.projectData.machinaryAndVehicles.Add(mvCostCenterID);
                    this.RegisterProject(AID, project, true);
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }

        public string AddStoreToProject(int AID, StoreInfo store, string projectCode)
        {
            lock (dspWriter)
            {
                Project project = GetProjectInfo(projectCode);

                try
                {
                    dspWriter.BeginTransaction();
                    string code = RegisterStore(AID, store, project.costCenterID);
                    if (!project.projectData.stores.Contains(code))
                        project.projectData.stores.Add(code);
                    this.RegisterProject(AID, project, true);
                    dspWriter.CommitTransaction();
                    return code;
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public string AddCashAccountToProject(int AID, CashAccount cashAccount, string projectCode,int divisionCostCenterID)
        {
            lock (dspWriter)
            {
                Project project = GetProjectInfo(projectCode);

                try
                {
                    dspWriter.BeginTransaction();
                    string code = CreateCashAccount(AID, cashAccount, divisionCostCenterID);
                    if (!project.projectData.cashAccounts.Contains(code))
                        project.projectData.cashAccounts.Add(code);
                    this.RegisterProject(AID, project, true);
                    dspWriter.CommitTransaction();
                    return code;
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public int AddBankAccountToProject(int AID, BankAccountInfo bankAccount, string projectCode, int divisionCostCenterID)
        {
            lock (dspWriter)
            {
                Project project = GetProjectInfo(projectCode);

                try
                {
                    dspWriter.BeginTransaction();
                    int mainCsID = CreateBankAccount(AID, bankAccount, divisionCostCenterID);
                    if (!project.projectData.bankAccounts.Contains(mainCsID))
                        project.projectData.bankAccounts.Add(mainCsID);
                    this.RegisterProject(AID, project, true);
                    dspWriter.CommitTransaction();
                    return mainCsID;
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public void RemoveEmployeeFromProject(int AID, int employeeID, string projectCode)
        {
            //1. Update employee info to remove project division cost center and join it to the HQ cost center
            //2. Update the project which has the employee already removed from the list
            lock (dspWriter)
            {
                Project project = GetProjectInfo(projectCode);

                try
                {
                    dspWriter.BeginTransaction();
                    //employee.costCenterID = SysPars.mainCostCenterID;
                   // m_Payroll.Update(AID, SysPars.mainCostCenterID, employee, employee, false);
                    if (project.projectData.employees.Contains(employeeID))
                        project.projectData.employees.Remove(employeeID);
                    RegisterProject(AID, project, true);
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public void RemoveDivisionFromProject(int AID, int divisionCostCenterID, string projectCode)
        {
            lock (dspWriter)
            {
                Project project = GetProjectInfo(projectCode);

                try
                {
                    dspWriter.BeginTransaction();
                    _ValidateDivisionAssociatedAccounts(divisionCostCenterID, project);
                    m_accounting.DeleteAccount<CostCenter>(AID, divisionCostCenterID, true);
                    if (project.projectData.projectDivisions.Contains(divisionCostCenterID))
                    {
                        project.projectData.projectDivisions.Remove(divisionCostCenterID);
                        this.RegisterProject(AID, project, true);
                    }
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public void RemoveMachinaryAndVehicleFromProject(int AID, int mvCostCenterID, string projectCode)
        {
            lock (dspWriter)
            {
                Project project = GetProjectInfo(projectCode);

                try
                {
                    dspWriter.BeginTransaction();
                    if (project.projectData.items != null)
                    {
                        if (project.projectData.items.Length > 0)
                        {
                            throw new ServerUserMessage("You cannot delete this Machinary/Vehicle since there are items associated with it!");
                        }
                    }
                    m_accounting.DeleteAccount<CostCenter>(AID, mvCostCenterID, true);
                    if (project.projectData.machinaryAndVehicles.Contains(mvCostCenterID))
                    {
                        project.projectData.machinaryAndVehicles.Remove(mvCostCenterID);
                        this.RegisterProject(AID, project, true);
                    }
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }

        public void RemoveStoreFromProject(int AID, int storeCostCenterID,string storeCode, string projectCode)
        {
            lock (dspWriter)
            {
                Project project = GetProjectInfo(projectCode);

                try
                {
                    dspWriter.BeginTransaction();
                    DeleteStore(AID, storeCode);
                    if (project.projectData.stores.Contains(storeCode))
                    {
                        project.projectData.stores.Remove(storeCode);
                        this.RegisterProject(AID, project, true);
                    }
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public void RemoveItemFromProject(int AID, string itemCode, string projectCode)
        {
            lock (dspWriter)
            {
                try
                {
                    dspWriter.BeginTransaction();
                    TransactionItems item = GetTransactionItems(itemCode);
                    int N;
                    Dictionary<string, double> projectItems = new Dictionary<string, double>();
                    Project project = GetProjectInfo(projectCode);
                    if (project.projectData.items != null)
                    {
                        for (int i = 0; i < project.projectData.items.Length; i++)
                        {
                            projectItems.Add(project.projectData.items[i], project.projectData.unitPrices[i]);
                        }
                    }
                    if (project != null)
                    {
                        if (projectItems.ContainsKey(item.Code))
                        {
                            projectItems.Remove(item.Code);
                            _RemoveItemFromProject(AID, item, project.costCenterID);
                            #region remove Item accounts from project divisions
                            CostCenter[] divisionCostCenters = m_accounting.GetChildAcccount<CostCenter>(project.costCenterID, 0, -1, out N);
                            foreach (CostCenter costCenter in divisionCostCenters)
                            {
                                _RemoveItemFromProject(AID, item, costCenter.id);
                            }
                            #endregion
                        }
                    }
                    
                    string[] projItems = new string[projectItems.Keys.Count];
                    double[] unitPrices = new double[projectItems.Values.Count];
                    projectItems.Keys.CopyTo(projItems, 0);
                    projectItems.Values.CopyTo(unitPrices, 0);
                    project.projectData.items = projItems;
                    project.projectData.unitPrices = unitPrices;
                    this.RegisterProject(AID, project, true);
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public void RemoveCashAccountFromProject(int AID, int csAccountID,string code, string projectCode)
        {
            lock (dspWriter)
            {
                Project project = GetProjectInfo(projectCode);

                try
                {
                    dspWriter.BeginTransaction();
                    DeleteCashAccount(AID, code);
                    //if (project.projectData.cashAccounts.Contains(code))
                    //{
                    //    project.projectData.cashAccounts.Remove(code);
                    //    this.RegisterProject(AID, project, true);
                    //}
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public void RemoveBankAccountFromProject(int AID, int csAccountID, string projectCode)
        {
            lock (dspWriter)
            {
                Project project = GetProjectInfo(projectCode);

                try
                {
                    dspWriter.BeginTransaction();
                    //BankAccountInfo bank = GetBankAccount(csAccountID);
                    //m_accounting.DeleteCostCenterAccount(AID, csAccountID);
                    //m_accounting.DeleteCostCenterAccount(AID, bank.bankServiceChargeCsAccountID);
                    DeleteBankAccount(AID, csAccountID);
                    //if (project.projectData.bankAccounts.Contains(csAccountID))
                    //{
                    //    project.projectData.bankAccounts.Remove(csAccountID);
                    //    this.RegisterProject(AID, project, true);
                    //}
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        
        private CostCenter CollectProjectCostCenter(string projectCode, string projectName)
        {
            CostCenter projectCostCenter = new CostCenter();
            projectCostCenter.ActivateDate = DateTime.Now.Date;
            projectCostCenter.DeactivateDate = DateTime.MaxValue.Date;
            projectCostCenter.Code = projectCode;
            projectCostCenter.CreationDate = DateTime.Now.Date;
            projectCostCenter.Name = projectName;
            projectCostCenter.PID = CostCenter.ROOT_COST_CENTER;
            projectCostCenter.Status = AccountStatus.Activated;
            return projectCostCenter;
        }
        private string GetNextDivisionCostCenterCode(int projectCostCenterID)
        {
            string code = GetDivisionParentCostCenterCode(projectCostCenterID);
            int codeSuffix = GetDivisionLastChildCostCenterSuffix(projectCostCenterID);
            return TSConstants.FormatProjectDivisionCode(code, codeSuffix);

        }
        private string GetDivisionParentCostCenterCode(int projectCostCenterID)
        {
            CostCenter costCenter = m_accounting.GetAccount<CostCenter>(projectCostCenterID);
            return costCenter.Code;
        }
        private int GetDivisionLastChildCostCenterSuffix(int projectCostCenterID)
        {

            CostCenter[] divisionCostCenters = m_accounting.GetLeafAccounts<CostCenter>(projectCostCenterID);
            CostCenter[] filteredDivisionCostCenters = _FilterDivisionCostCenters(divisionCostCenters);
            if (filteredDivisionCostCenters != null)
            {
                if (filteredDivisionCostCenters.Length == 0
                    || (filteredDivisionCostCenters.Length == 1 && filteredDivisionCostCenters[0].id == projectCostCenterID))
                {
                    return 1;
                }
                string code = filteredDivisionCostCenters[filteredDivisionCostCenters.Length - 1].Code;
                if (code.Contains("-"))
                {
                    string[] splittedCode = code.Split('-');
                    int suffCode;
                    string s = splittedCode[splittedCode.Length - 1].Remove(0, 1); //Remove the 'D' word from the suffix
                    if (int.TryParse(s, out suffCode))
                    {
                        int codeSuffix = int.Parse(s);
                        return codeSuffix + 1;
                    }
                }
                throw new INTAPS.ClientServer.ServerUserMessage("Account Code format not recognized");
            }
            else
            {
                return 1; //start from 001
            }

        }

        private CostCenter[] _FilterDivisionCostCenters(CostCenter[] divisionCostCenters)
        {
            List<CostCenter> costCenters = new List<CostCenter>();
            foreach (CostCenter div in divisionCostCenters)
            {
                if (div.Code.Contains("D"))
                    costCenters.Add(div);
            }
            return costCenters.ToArray();
        }
        private CostCenter CollectDivisionCostCenter(int projectCostCenterID, string divisionCostCenterCode, string name)
        {
            CostCenter divisionCostCenter = new CostCenter();
            divisionCostCenter.ActivateDate = DateTime.Now.Date;
            divisionCostCenter.DeactivateDate = DateTime.MaxValue.Date;
            divisionCostCenter.Code = divisionCostCenterCode;
            divisionCostCenter.CreationDate = DateTime.Now.Date;
            divisionCostCenter.Name = name;
            divisionCostCenter.PID = projectCostCenterID;
            divisionCostCenter.Status = AccountStatus.Activated;
            return divisionCostCenter;
        }

        private string GetNextMachinaryAndVehicleCostCenterCode(int projectCostCenterID)
        {
            string code = GetMachinaryAndVehicleParentCostCenterCode(projectCostCenterID);
            int codeSuffix = GetMachinaryAndVehicleLastChildCostCenterSuffix(projectCostCenterID);
            return TSConstants.FormatMachinaryAndVehicleCode(code, codeSuffix);
        }
        private string GetMachinaryAndVehicleParentCostCenterCode(int projectCostCenterID)
        {
            CostCenter costCenter = m_accounting.GetAccount<CostCenter>(projectCostCenterID);
            return costCenter.Code;
        }
        private int GetMachinaryAndVehicleLastChildCostCenterSuffix(int projectCostCenterID)
        {

            CostCenter[] costCenters = m_accounting.GetLeafAccounts<CostCenter>(projectCostCenterID);
            CostCenter[] filteredMachinaryAndVehicleCostCenters = _FilterMachinaryAndVehicleCostCenters(costCenters);
            if (filteredMachinaryAndVehicleCostCenters != null)
            {
                if (filteredMachinaryAndVehicleCostCenters.Length == 0
                    || (filteredMachinaryAndVehicleCostCenters.Length == 1 && filteredMachinaryAndVehicleCostCenters[0].id == projectCostCenterID))
                {
                    return 1;
                }
                string code = filteredMachinaryAndVehicleCostCenters[filteredMachinaryAndVehicleCostCenters.Length - 1].Code;
                if (code.Contains("-"))
                {
                    string[] splittedCode = code.Split('-');
                    int suffCode;
                    string s = splittedCode[splittedCode.Length - 1].Remove(0, 1); //Remove the 'D' word from the suffix
                    if (int.TryParse(s, out suffCode))
                    {
                        int codeSuffix = int.Parse(s);
                        return codeSuffix + 1;
                    }
                }
                throw new INTAPS.ClientServer.ServerUserMessage("Account Code format not recognized");
            }
            else
            {
                return 1; //start from 001
            }

        }

        private CostCenter[] _FilterMachinaryAndVehicleCostCenters(CostCenter[] childCostCenters)
        {
            List<CostCenter> costCenters = new List<CostCenter>();
            foreach (CostCenter costCenter in childCostCenters)
            {
                if (costCenter.Code.Contains("M"))
                    costCenters.Add(costCenter);
            }
            return costCenters.ToArray();
        }
        private void _AddItemAccountsToProject(int AID, TransactionItems item, int projectCostCenterID)
        {
            if (item.expenseAccountID != -1)
                m_accounting.CreateCostCenterAccount(AID,  projectCostCenterID, item.expenseAccountID);
            if (item.prePaidExpenseAccountID != -1)
                m_accounting.CreateCostCenterAccount(AID,  projectCostCenterID, item.prePaidExpenseAccountID);
            if (item.salesAccountID != -1)
                m_accounting.CreateCostCenterAccount(AID,  projectCostCenterID, item.salesAccountID);
            if (item.unearnedRevenueAccountID != -1)
                m_accounting.CreateCostCenterAccount(AID,  projectCostCenterID, item.unearnedRevenueAccountID);
            if (item.inventoryAccountID != -1)
                m_accounting.CreateCostCenterAccount(AID,  projectCostCenterID, item.inventoryAccountID);
            if (item.finishedWorkAccountID != -1)
                m_accounting.CreateCostCenterAccount(AID,  projectCostCenterID, item.finishedWorkAccountID);
            if (item.originalFixedAssetAccountID != -1)
                m_accounting.CreateCostCenterAccount(AID,  projectCostCenterID, item.originalFixedAssetAccountID);
            if (item.depreciationAccountID != -1)
                m_accounting.CreateCostCenterAccount(AID,  projectCostCenterID, item.depreciationAccountID);
            if (item.accumulatedDepreciationAccountID != -1)
                m_accounting.CreateCostCenterAccount(AID,  projectCostCenterID, item.accumulatedDepreciationAccountID);
            if (item.pendingOrderAccountID != -1)
                m_accounting.CreateCostCenterAccount(AID,  projectCostCenterID, item.pendingOrderAccountID);
            if (item.pendingDeliveryAcountID != -1)
                m_accounting.CreateCostCenterAccount(AID,  projectCostCenterID, item.pendingDeliveryAcountID);
        }
        private void _AddEmployeeAccountsToProject(int AID, Employee emp, int projectCostCenterID)
        {
            foreach (int account in emp.accounts)
            {
                m_accounting.CreateCostCenterAccount(AID,  projectCostCenterID, account);
            }
        }
       
        private void _RemoveItemFromProject(int AID, TransactionItems item, int projectCostCenterID)
        {
            foreach (int accountID in new int[]{
                item.expenseAccountID,item.prePaidExpenseAccountID,item.salesAccountID,item.unearnedRevenueAccountID
                ,item.inventoryAccountID,item.finishedWorkAccountID,item.originalFixedAssetAccountID
                ,item.depreciationAccountID,item.accumulatedDepreciationAccountID,item.pendingOrderAccountID
                ,item.pendingDeliveryAcountID}
                )
            {
                if (accountID != -1)
                {
                    CostCenterAccount csAccount = m_accounting.GetCostCenterAccount(projectCostCenterID, accountID);
                    if (csAccount != null)
                        m_accounting.DeleteCostCenterAccount(AID, csAccount.id);
                }
            }
            #region commented lines
            //if (item.expenseAccountID != -1)
            //{
            //    CostCenterAccount csAccount = m_accounting.GetCostCenterAccount(projectCostCenterID, item.expenseAccountID);
            //    m_accounting.DeleteCostCenterAccount(AID, csAccount.id);
            //}
            //if (item.prePaidExpenseAccountID != -1)
            //{
            //    CostCenterAccount csAccount = m_accounting.GetCostCenterAccount(projectCostCenterID, item.prePaidExpenseAccountID);
            //    m_accounting.DeleteCostCenterAccount(AID, csAccount.id);
            //}
            //if (item.salesAccountID != -1)
            //{
            //    CostCenterAccount csAccount = m_accounting.GetCostCenterAccount(projectCostCenterID, item.salesAccountID);
            //    m_accounting.DeleteCostCenterAccount(AID, csAccount.id);
            //}
            //if (item.unearnedRevenueAccountID != -1)
            //{
            //    CostCenterAccount csAccount = m_accounting.GetCostCenterAccount(projectCostCenterID, item.unearnedRevenueAccountID);
            //    m_accounting.DeleteCostCenterAccount(AID, csAccount.id);
            //}
            //if (item.inventoryAccountID != -1)
            //{
            //    CostCenterAccount csAccount = m_accounting.GetCostCenterAccount(projectCostCenterID, item.inventoryAccountID);
            //    m_accounting.DeleteCostCenterAccount(AID, csAccount.id);
            //}
            //if (item.finishedWorkAccountID != -1)
            //{
            //    CostCenterAccount csAccount = m_accounting.GetCostCenterAccount(projectCostCenterID, item.finishedWorkAccountID);
            //    m_accounting.DeleteCostCenterAccount(AID, csAccount.id);
            //}
            //if (item.originalFixedAssetAccountID != -1)
            //{
            //    CostCenterAccount csAccount = m_accounting.GetCostCenterAccount(projectCostCenterID, item.originalFixedAssetAccountID);
            //    m_accounting.DeleteCostCenterAccount(AID, csAccount.id);
            //}
            //if (item.depreciationAccountID != -1)
            //{
            //    CostCenterAccount csAccount = m_accounting.GetCostCenterAccount(projectCostCenterID, item.depreciationAccountID);
            //    m_accounting.DeleteCostCenterAccount(AID, csAccount.id);
            //}
            //if (item.accumulatedDepreciationAccountID != -1)
            //{
            //    CostCenterAccount csAccount = m_accounting.GetCostCenterAccount(projectCostCenterID, item.accumulatedDepreciationAccountID);
            //    m_accounting.DeleteCostCenterAccount(AID, csAccount.id);
            //}
            //if (item.pendingOrderAccountID != -1)
            //{
            //    CostCenterAccount csAccount = m_accounting.GetCostCenterAccount(projectCostCenterID, item.pendingOrderAccountID);
            //    m_accounting.DeleteCostCenterAccount(AID, csAccount.id);
            //}
            //if (item.pendingDeliveryAcountID != -1)
            //{
            //    CostCenterAccount csAccount = m_accounting.GetCostCenterAccount(projectCostCenterID, item.pendingDeliveryAcountID);
            //    m_accounting.DeleteCostCenterAccount(AID, csAccount.id);
            //}
            #endregion
        }
        private void _ValidateDivisionAssociatedAccounts(int divisionCostCenterID, Project project)
        {
            foreach (int empID in project.projectData.employees)
            {
                Employee emp = m_Payroll.GetEmployee(empID);
                if (emp!=null && emp.costCenterID == divisionCostCenterID)
                    throw new ServerUserMessage("You cannot delete this division since there are employee associated with it!");
            }
            foreach (int bankMainCsID in project.projectData.bankAccounts)
            {
                CostCenterAccount csAccount = m_accounting.GetCostCenterAccount(bankMainCsID);
                if (csAccount.costCenterID == divisionCostCenterID)
                    throw new ServerUserMessage("You cannot delete this division since there are bank accounts associated with it!");
            }
            foreach (string code in project.projectData.cashAccounts)
            {
                CashAccount account = GetCashAccount(code);
                CostCenterAccount csAccount = m_accounting.GetCostCenterAccount(account.csAccountID);
                if (csAccount.costCenterID == divisionCostCenterID)
                    throw new ServerUserMessage("You cannot delete this division since there are cash accounts associated with it!");
            }
            if (project.projectData.items != null)
            {
                if (project.projectData.items.Length > 0)
                {
                    throw new ServerUserMessage("You cannot delete this division since there are items associated with it!");
                }
            }
        }


        #endregion
    }
}