using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS;
namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        PropertyExtraDataHandlers propHandlers = new PropertyExtraDataHandlers();
        void initProperty()
        {
            propHandlers.loadPropertyHandler(System.Reflection.Assembly.GetCallingAssembly());
        }
        PropertyItemCategory createPropertyForItem(int AID,string itemCode)
        {
            TransactionItems tranItem=GetTransactionItems(itemCode);
            if (!tranItem.IsFixedAssetItem)
                throw new ServerUserMessage("Item {0} should be fixed asset item to be used for properties.", itemCode);
            ItemCategory cat = GetItemCategory(tranItem.categoryID);
            if(cat==null)
                throw new ServerUserMessage("Item {0} should have a category to be used for properties.", itemCode);
            cat.PID = cat.ID;
            cat.ID = -1;
            cat.Code = tranItem.Code + "-PR";
            cat.description = tranItem.Name + " (Properties)";
            cat.ID=RegisterItemCategory(AID, cat);
            PropertyItemCategory itemCat = new PropertyItemCategory();
            itemCat.categorID = cat.ID;
            itemCat.itemCode = itemCode;
            dspWriter.InsertSingleTableRecord(this.DBName, itemCat, new string[] { "__AID" }, new object[] { AID });
            
            return itemCat;
        }
        // BDE exposed
        public PropertyItemCategory[] getPropertyItemCategory(int parentCategoryID, int pageIndex, int pageSize, out int N)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<PropertyItemCategory>(
                    "categorID in (Select id from {0}.dbo.ItemCategory where PID={1})".format(this.DBName,parentCategoryID)
                    ,pageIndex,pageSize,out N);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public PropertyType getPropertyType(int id)
        {
            return base.getSingleObjectIntID<PropertyType>(id);
        }
        //BDE Exposed
        public PropertyType[] getAllPropertTypes()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<PropertyType>(null);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public PropertyItemCategory getPropertyItemCategory(string itemCode)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                PropertyItemCategory[] ret = reader.GetSTRArrayByFilter<PropertyItemCategory>("itemCode='{0}'".format(itemCode));
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public Property getPropertyByCode(string propertyCode, out object extraData)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                Property[] ret = reader.GetSTRArrayByFilter<Property>("propertyCode='{0}'".format(propertyCode));
                if (ret.Length == 0)
                {
                    extraData = null;
                    return null;
                }
                extraData = getPropertyExtraData(ret[0]);
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        // BDE exposed
        public Property getPropertyByItemCode(string propertyCode, out object extraData)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                Property[] ret = reader.GetSTRArrayByFilter<Property>("itemCode='{0}'".format(propertyCode));
                if (ret.Length == 0)
                {
                    extraData = null;
                    return null;
                }
                extraData = getPropertyExtraData(ret[0]);
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public int createProperty(int AID, Property prop, object extraData)
        {
            //create id
            prop.id = AutoIncrement.GetKey("iERP.PropertyID");

            //get or create Property Item Categroy
            PropertyItemCategory itemCat=getPropertyItemCategory(prop.parentItemCode);
            if (itemCat==null)
            {
                itemCat=createPropertyForItem(AID,prop.parentItemCode);
            }
            
            
            TransactionItems item = GetTransactionItems(prop.parentItemCode);
            
            //validate or generated property code
            if (SysPars.manualPropertyCode)
            {
                object testObject;
                if (string.IsNullOrEmpty(prop.propertyCode))
                    throw new ServerUserMessage("Please enter property code");
                Property test = getPropertyByCode(prop.propertyCode,out testObject);
                if (test != null)
                    throw new ServerUserMessage("Property code {0} already used", prop.propertyCode);
            }
            else
            {
                object lastCode = dspWriter.ExecuteScalar("Select max(propertyCode) from {0}.dbo.Property where parentItemCode='{1}'".format(this.DBName, prop.parentItemCode));
                int sn;
                if (!(lastCode is string))
                    sn = 1;
                else
                    sn = int.Parse(((string)lastCode).Substring(item.Code.Length+3))+1;
                prop.propertyCode = item.Code + "-PR"+sn.ToString(SysPars.propertyCodeFormat);
            }

            //Create item for the property
            item.categoryID = itemCat.categorID;
            if (SysPars.manualItemCode)
                item.Code = prop.propertyCode;
            else
                item.Code = null;
            item.Name = prop.name;
            item.Description= prop.description;
            item.clearAccounts();
            item.IsDirectCost = prop.directCost;
            item.Code=RegisterTransactionItem(AID, item, new int[0]);
            
            prop.itemCode = item.Code;
            prop.propertyCode = item.Code;

            //save extra data
            if(extraData!=null && propHandlers.hasHandler(extraData.GetType()))
            {
                propHandlers.getHandler(extraData.GetType()).saveExtraData(AID,prop.id, extraData);
            }
            dspWriter.InsertSingleTableRecord(this.DBName, prop, new string[] { "__AID" }, new object[] { AID });
            return prop.id;
        }

        // BDE exposed
        public AccountBalance[] getMaterialBalance(DateTime time, string itemCode, int[] itemIDs)
        {
            TransactionItems titem = GetTransactionItems(itemCode);
            if (titem == null)
                return new AccountBalance[0];
            List<AccountBalance> ret = new List<AccountBalance>();
            List<int> accounts = new List<int>();
            if (titem.inventoryAccountID!=-1)
                accounts.Add(titem.inventoryAccountID);
            if (titem.finishedGoodAccountID != -1)
                accounts.Add(titem.finishedGoodAccountID);
            if (titem.originalFixedAssetAccountID != -1)
                accounts.Add(titem.originalFixedAssetAccountID);
            if (titem.accumulatedDepreciationAccountID != -1)
                accounts.Add(titem.accumulatedDepreciationAccountID);


            foreach (int acid in accounts)
            {
                foreach (CostCenterAccount csa in Accounting.GetCostCenterAccountsOf<Account>(acid))
                {
                    if (csa.isControlAccount)
                        continue;
                    foreach (int itemID in itemIDs)
                    {
                        AccountBalance bal = m_accounting.GetBalanceAsOf(csa.id, itemID, time);
                        if(bal.IsZero)
                            continue;
                        ret.Add(bal);
                    }
                }
            }
            return ret.ToArray();
        }
        // BDE exposed
        public Property getProperty(int propertyID, out object extraData)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                Property[] prop = reader.GetSTRArrayByFilter<Property>("id=" + propertyID);
                if(prop.Length==0)
                {
                    extraData = null;
                    return null;
                }
                Property ret=prop[0];
                extraData = getPropertyExtraData(ret);
                return ret;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        private object getPropertyExtraData(Property ret)
        {
            object extraData;
            IPropertyExtraDataHandler h = propHandlers.getHandler(getPropertyType(ret.propertyTypeID).extraDataType);
            if (h == null)
                extraData = null;
            else
                extraData = h.loadExtraData(ret.id);
            return extraData;
        }
        // BDE exposed
        public void updateProperty(int AID, Property prop, object extraData)
        {
            object oldExtraData;
            Property oldProp = getProperty(prop.id, out oldExtraData);
            if (oldProp == null)
                throw new ServerUserMessage("Property with ID {0} not found.", prop.id);
            prop.itemCode = oldProp.itemCode;

            //get or create Property Item Categroy
            PropertyItemCategory itemCat = getPropertyItemCategory(prop.parentItemCode);
            if (itemCat == null)
            {
                throw new ServerUserMessage("Database inconsitence, property found but associated PropertyItemCategory not found.");
            }


            

            //validate or generated property code
            if (SysPars.manualPropertyCode)
            {
                object textExtra;
                Property test = getPropertyByCode(prop.propertyCode,out textExtra);
                if (test != null&& test.id!=oldProp.id)
                    throw new ServerUserMessage("Property code {0} already used", prop.propertyCode);
            }
            else
            {
                prop.propertyCode = oldProp.propertyCode;
            }

            TransactionItems item = GetTransactionItems(prop.itemCode);
            //Create item for the property
            item.Name = prop.name;
            item.Description = prop.description;
            item.IsDirectCost = prop.directCost;
            RegisterTransactionItem(AID, item, new int[0]);

            //save extra data
            IPropertyExtraDataHandler h;
            if ((h = propHandlers.getHandler(getPropertyType(prop.propertyTypeID).extraDataType)) != null)
            {
                h.saveExtraData(AID,prop.id, extraData);
            }
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.Property", "id=" + prop.id);
            dspWriter.UpdateSingleTableRecord(this.DBName, prop, new string[] { "__AID" }, new object[] { AID });
        }
        int getPropertyCountByItemCode(string itemCode)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return (int)reader.ExecuteScalar("Select count(*) from {0}.dbo.Property where parentItemCode='{1}'".format(this.DBName, itemCode));
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public void deleteProperty(int AID, int propertyID)
        {
            object oldExtraData;
            Property oldProp = getProperty(propertyID, out oldExtraData);
            if (oldProp == null)
                throw new ServerUserMessage("Property with ID {0} not found.", propertyID);
            
            //delete the item for the property
            DeleteTransactionItem(AID, oldProp.itemCode);
            
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.Property", "id=" + propertyID);
            dspWriter.DeleteSingleTableRecrod<Property>(this.DBName, propertyID);
            
            //delete property category item if last propert in for the item
            if (getPropertyCountByItemCode(oldProp.parentItemCode) == 0)
            {
                PropertyItemCategory itemCat=getPropertyItemCategory(oldProp.parentItemCode);
                dspWriter.logDeletedData(AID, this.DBName + ".dbo.PropertyItemCategory", "itemCode='{0}'".format(oldProp.parentItemCode));
                dspWriter.Delete(this.DBName, "PropertyItemCategory", "itemCode='{0}'".format(oldProp.parentItemCode));
                DeleteItemCategory(AID, itemCat.categorID);
            }
            
        }

        // BDE exposed
        public Property[] getPropertiesByParentItemCode(string parentItemCode, int pageIndex, int pageSize, out int N)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<Property>("parentItemCode='{0}'".format(parentItemCode), pageIndex, pageSize, out N);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        
    }
}
