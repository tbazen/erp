using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using System.Data.SqlClient;
using System.Data;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using System.Web;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE : INTAPS.ClientServer.BDEBase
    {
        TransactionItems getTransactionItemWithCache(Dictionary<string, TransactionItems> cache, string code)
        {
            if (cache.ContainsKey(code))
                return cache[code];
            TransactionItems ret = GetTransactionItems(code);
            cache.Add(code, ret);
            return ret;
        }
    }
}
