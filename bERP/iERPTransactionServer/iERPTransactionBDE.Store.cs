using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        #region Store Methods
        public string RegisterStore(int AID, StoreInfo storeInfo,int costCenterParentID)
        {
            lock (dspWriter)
            {
                string ret;
                dspWriter.BeginTransaction();
                try
                {
                    if (storeInfo.code == null) //INSERT
                    {
                        string nextStoreCode = GetNextStoreCostCenterCode(costCenterParentID);
                        string storeName = storeInfo.description;
                        CostCenter storeCostCenter = CollectStoreCostCenter(costCenterParentID, nextStoreCode, storeName);
                        
                        int storeCostCenterID = m_accounting.CreateAccount<CostCenter>(AID, storeCostCenter);
                        storeInfo.code = GetNextStoreSerialCode("StoreInfo","code",dspWriter);
                        storeInfo.costCenterID = storeCostCenterID;
                        dspWriter.InsertSingleTableRecord(base.DBName, storeInfo,new string[]{"__AID"},new object[]{AID});
                        ret=storeInfo.code;
                    }
                    else //UPDATE
                    {
                       
                        CostCenter storeCostCenter = m_accounting.GetAccount<CostCenter>(storeInfo.costCenterID);
                        storeCostCenter.Name = storeInfo.description;
                        if (storeCostCenter.Status == AccountStatus.Activated)
                        {
                            storeCostCenter.DeactivateDate = DateTime.MaxValue.Date;
                        }
                        m_accounting.UpdateAccount<CostCenter>(AID, storeCostCenter);
                        dspWriter.logDeletedData(AID, base.DBName + ".dbo.StoreInfo", "code='" + storeInfo.code + "'");
                        dspWriter.UpdateSingleTableRecord(base.DBName, storeInfo, new string[] { "__AID" }, new object[] { AID });
                        ret=storeInfo.code;
                    }
                    dspWriter.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public StoreInfo GetStoreInfo(string code)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                StoreInfo[] ret=dspReader.GetSTRArrayByFilter<StoreInfo>("code='" + code + "'");
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public StoreInfo GetStoreInfo(int costCenterID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                StoreInfo[] ret = dspReader.GetSTRArrayByFilter<StoreInfo>("costCenterID=" + costCenterID);
                if (ret.Length == 0)
                    return null;
                return ret[0];

            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public StoreInfo[] GetStoresByCostCenterID(int costCenterID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                string cr = "";
                CostCenter[] costCenters = m_accounting.GetLeafAccounts<CostCenter>(costCenterID);
                foreach (CostCenter cs in costCenters)
                {
                    if (string.IsNullOrEmpty(cr))
                        cr = cs.id.ToString();
                    else
                        cr += "," + cs.id;
                }
                StoreInfo[] _ret = dspReader.GetSTRArrayByFilter<StoreInfo>(
                    string.Format("exists(Select * from {0}.dbo.CostCenter where id in ({1}) and id=costCenterID)", m_accounting.DBName, cr)
                    );
                return _ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public StoreInfo GetStoreByCostCenterID(int costCenterID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                
                StoreInfo[] _ret = dspReader.GetSTRArrayByFilter<StoreInfo>(string.Format("costCenterID={0}",costCenterID));
                if (_ret.Length == 0)
                    return null;
                return _ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public StoreInfo[] GetAllStores()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<StoreInfo>(null);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
       
        public void DeleteStore(int AID, string code)
        {
            lock (dspWriter)
            {
                StoreInfo store = GetStoreInfo(code);
                try
                {
                    dspWriter.BeginTransaction();
                    dspWriter.logDeletedData(AID, base.DBName + ".dbo.StoreInfo", "code='" + code + "'");
                    m_accounting.DeleteAccount<CostCenter>(AID, store.costCenterID, true);
                    //Delete store if it's in project
                    Project[] projects = GetAllProjects();
                    foreach (Project project in projects)
                    {
                        if (project.projectData.stores.Contains(store.code))
                        {
                            project.projectData.stores.Remove(store.code);
                            RegisterProject(AID, project, true);
                            break;
                        }
                    }

                    if ((int)dspWriter.ExecuteScalar(String.Format("select count(*) from {1}.dbo.StoreInfo where code='{0}'", code,this.DBName)) > 0)
                    {
                        dspWriter.DeleteSingleTableRecrod<StoreInfo>(base.DBName, code);
                    }
                    else
                        throw new INTAPS.ClientServer.ServerUserMessage(String.Format("Error: Store with Code '{0}' not found!", code));
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public void ActivateStore(int AID, string code)
        {
            lock (dspWriter)
            {
                StoreInfo store = GetStoreInfo(code);

                try
                {
                    dspWriter.BeginTransaction();
                    m_accounting.ActivateAcount<CostCenter>(AID, store.costCenterID, DateTime.Now.Date);
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public void DeactivateStore(int AID, string code)
        {
            lock (dspWriter)
            {
                StoreInfo store = GetStoreInfo(code);

                try
                {
                    dspWriter.BeginTransaction();
                    m_accounting.DeactivateAccount<CostCenter>(AID, store.costCenterID, DateTime.Now.Date);
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }

        }
       
        private CostCenter CollectStoreCostCenter(int costCenterParentID, string nextStoreCode, string storeName)
        {
            CostCenter storeCostCenter = new CostCenter();
            storeCostCenter.ActivateDate = DateTime.Now.Date;
            storeCostCenter.DeactivateDate = DateTime.MaxValue.Date;
            storeCostCenter.Code = nextStoreCode;
            storeCostCenter.CreationDate = DateTime.Now.Date;
            storeCostCenter.Name = storeName;
            storeCostCenter.PID = costCenterParentID;
            storeCostCenter.Status = AccountStatus.Activated;
            return storeCostCenter;
        }
        private string GetNextStoreCostCenterCode(int costCenterParentID)
        {
            string code = GetStoreParentCostCenterCode(costCenterParentID);
            int codeSuffix = GetStoreLastChildAccountSuffix(costCenterParentID);
            return TSConstants.FormatStoreCode(code, codeSuffix);

        }
        private string GetStoreParentCostCenterCode(int costCenterParentID)
        {
            CostCenter costCenter = m_accounting.GetAccount<CostCenter>(costCenterParentID);
            return costCenter.Code;
        }
        private int GetStoreLastChildAccountSuffix(int costCenterParentID)
        {

            StoreInfo[] storeInfo = GetAllStores();
            if (storeInfo != null)
            {
                if (storeInfo.Length == 0)
                {
                    return 1;
                }
                string code = storeInfo[storeInfo.Length - 1].code;

                return int.Parse(code) + 1;
            }
            else
            {
                return 1; //start from 001
            }

        }
        #endregion
    }
}