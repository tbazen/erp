﻿using System;
using System.Text;
using INTAPS.Accounting;
using System.Web;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        class BudgetGenerator
        {
            iERPTransactionBDE _parent;
            BudgetFilter _filter;
            public BudgetGenerator(iERPTransactionBDE parent, BudgetFilter filter)
            {
                _parent = parent;
                _filter = filter;
            }
            bERPHtmlTableRowGroup header;
            bERPHtmlTableRow headerRow;

            public string generate()
            {
                Budget b = _parent.getBudget(_filter.costCenterID, _filter.targetDate, false);

                if (b == null)
                    return string.Format("<h2>Accounting center {0} doesn't have budget that apply for {1}</h1>"
                        , HttpUtility.HtmlEncode(_parent.Accounting.GetAccount<CostCenter>(_filter.costCenterID).CodeName)
                        , HttpUtility.HtmlEncode(_filter.targetDate.ToString("MMM dd,yyyy")));
                
                bERPHtmlTable table = new bERPHtmlTable();
                header = new bERPHtmlTableRowGroup(TableRowGroupType.header);
                headerRow = new bERPHtmlTableRow();
                header.rows.Add(headerRow);

                headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Budget Code"));
                headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Budget Name"));
                headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Budget"));
                headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Expenditure"));
                headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Budget Utilization"));
                bERPHtmlTableRowGroup body = new bERPHtmlTableRowGroup(TableRowGroupType.body);
                
                foreach (BudgetEntry e in _parent.getBudgetEntries(b.id))
                {
                    Account descItem = _parent.Accounting.GetAccount<Account>(e.accountID);
                    AccountBalance valItem = _parent.Accounting.GetFlow(b.costCenterID, e.accountID, TransactionItem.BUDGET, new DateTime(b.ticksFrom), new DateTime(b.ticksTo));
                    double utilization =valItem==null?0:valItem.DebitBalance;
                    bool budgeted = !AccountBase.AmountEqual(e.amount, 0);
                    double utilizationRate=budgeted?utilization/e.amount:-1;
                    bERPHtmlTableCell cell1, cell2, cell3;
                    bERPHtmlBuilder.htmlAddRow(body
                            , new bERPHtmlTableCell(descItem.Code)
                            , new bERPHtmlTableCell(descItem.Name)
                            , cell1=new bERPHtmlTableCell(budgeted? AccountBase.FormatAmount(e.amount):"")
                            , cell2=new bERPHtmlTableCell(budgeted? AccountBase.FormatAmount(utilization):"" )
                            , cell3=new bERPHtmlTableCell(budgeted? (utilizationRate*100).ToString("0.0")+"%":"")
                            ).css = budgeted ? (AccountBase.AmountEqual(utilization,0)?"not_using_budget": (AccountBase.AmountGreater(utilizationRate, 1) ? "over_budget" : "within_budget")) : "not_budgeted";
                    cell1.css = cell2.css = cell3.css = "currencyCell";
                }

                //bERPHtmlTableRowGroup footer = new bERPHtmlTableRowGroup(TableRowGroupType.footer);

                StringBuilder builder = new StringBuilder();
                table.groups.Add(header);
                table.groups.Add(body);
                //table.groups.Add(footer);
                table.build(builder);
                return builder.ToString();
            }
        }
        // BDE exposed
        public string getBudgetReport(BudgetFilter filter)
        {
            BudgetGenerator gg = new BudgetGenerator(this, filter);
            return gg.generate();
        }
    }
}
