﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using System.Web;
using INTAPS.Accounting.BDE;
using INTAPS.Payroll;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        
        class DetailedCashFlowItem
        {
            public List<DocumentSerial> refs;
            public List<AccountTransaction> trans;
        }
        class DetailedCashFlowGenerator
        {
            const string DATE_FORMAT_YEAR = "MMM dd,yyyy";
            const string DATE_FORMAT_MONTH = "MMM dd";
            const string DATE_FORMAT_DAY = "dd";
            const string TIME_FORMAT = "hh:mm tt";
            
            //BDE Objects
            iERPTransactionBDE _parent;
            AccountingBDE _accounting;
            
            //Report generation parameters
            DateTime _date1, _date2;
            DetailedCashFlowReportOption _options;
            //data
            List<DetailedCashFlowItem> items = new List<DetailedCashFlowItem>();
            Dictionary<int, AccountBalance> bal = new Dictionary<int, AccountBalance>();


            public DetailedCashFlowGenerator(iERPTransactionBDE parent, DateTime date1, DateTime date2, DetailedCashFlowReportOption options)
            {
                _parent = parent;
                _date1 = date1;
                _date2 = date2;
                _options = options;
                _accounting = parent.Accounting;
                if (_options.costCenterFilter == -1)
                    _options.costCenters= null;
                else
                {
                    INTAPS.RDBMS.SQLHelper dspReader = _accounting.GetReaderHelper();
                    try
                    {
                        _options.costCenters = new List<int>();
                        _options.costCenters.AddRange(_accounting.getAllChildAccounts<CostCenter>(_options.costCenterFilter));
                        _options.costCenters.Add(_options.costCenterFilter);
                    }
                    finally
                    {
                        _accounting.ReleaseHelper(dspReader);
                    }
                    
                }
            }
            #region utility methods
            Dictionary<int, DocumentSerialType> _serialType = new Dictionary<int, DocumentSerialType>();
            DocumentSerialType getSerialType(int typeID)
            {
                if (_serialType.ContainsKey(typeID))
                    return _serialType[typeID];
                DocumentSerialType st= _parent.m_accounting.getDocumentSerialType(typeID);
                _serialType.Add(st.id, st);
                _accounting = _parent.m_accounting;
                return st;
            }
            Dictionary<int, AccountDocument> _docs = new Dictionary<int, AccountDocument>();
            AccountDocument getDocument(int docID)
            {
                if (_docs.ContainsKey(docID))
                    return _docs[docID];
                AccountDocument doc= _parent.m_accounting.GetAccountDocument(docID, true);
                _docs.Add(doc.AccountDocumentID, doc);
                return doc;
            }
            Dictionary<int, AccountTransaction[]> _docTran = new Dictionary<int, AccountTransaction[]>();
            AccountTransaction[] getDocTransactions(int docID)
            {
                if (_docTran.ContainsKey(docID))
                    return _docTran[docID];
                AccountTransaction[] doc = _parent.m_accounting.GetTransactionsOfDocument(docID);
                _docTran.Add(docID, doc);
                return doc;
            }
            private bool isBankAccount(int accountID, out BankAccountInfo bank)
            {
                return (bank = _parent.GetBankAccount(accountID)) != null;
            }
            public bool isCashAccount(int accountID, out CashAccount cash)
            {
                return (cash = _parent.GetCashAccount(accountID, true)) != null;
            }
            public bool isCashOrBankAccount(int accountID)
            {
                CashAccount cash; BankAccountInfo bank;
                return isCashAccount(accountID, out cash) || isBankAccount(accountID, out bank);
            }
            int compareDocs(int docID1, int docID2)
            {
                AccountDocument doc1 = getDocument(docID1);
                AccountDocument doc2 = getDocument(docID2);
                if (!doc1.DocumentDate.Equals(doc2.DocumentDate))
                    return doc1.DocumentDate.CompareTo(doc2.DocumentDate);
                AccountTransaction[] tran1 = getDocTransactions(docID1);
                AccountTransaction[] tran2 = getDocTransactions(docID2);
                foreach(AccountTransaction t1 in tran1)
                    foreach (AccountTransaction t2 in tran2)
                    {
                        if (t1.AccountID == t2.AccountID)
                        {
                            return t1.OrderN.CompareTo(t2.OrderN);
                        }
                    }
                return docID1.CompareTo(docID2);
            }
            #endregion
            bERPHtmlTable buildBankSummaryTable()
            {
                bERPHtmlTableRowGroup summaryHeader;
                bERPHtmlTableRow summaryHeaderHeaderRow;
                bERPHtmlTableRowGroup summaryBody = null;

                bERPHtmlTableRowGroup summaryFooter;
                bERPHtmlTable bankSummaryTable = null;
                AccountBalance total;

                total = new AccountBalance(-1, 0, 0);
                summaryFooter = null;

                foreach (BankAccountInfo b in _parent.GetAllBankAccounts())
                {
                    if (bal.ContainsKey(b.mainCsAccount))
                    {
                        if (summaryBody == null)
                            summaryBody = new bERPHtmlTableRowGroup(TableRowGroupType.body);
                        AccountBalance change = bal[b.mainCsAccount];
                        double net = change.GetBalance(false);
                        summaryBody.htmlAddRow(
                            new bERPHtmlTableCell(b.BankBranchAccount)
                            , new bERPHtmlTableCell(AccountBase.FormatAmount(change.TotalCredit),"dcfrSummaryValue")
                            , new bERPHtmlTableCell(AccountBase.FormatAmount(change.TotalDebit), "dcfrSummaryValue")
                            );
                        total.AddBalance(change);
                    }
                }
                if (summaryBody != null && summaryBody.rows.Count > 0)
                {
                    bankSummaryTable = new bERPHtmlTable();
                    summaryHeader = new bERPHtmlTableRowGroup(TableRowGroupType.header);
                    summaryHeaderHeaderRow = new bERPHtmlTableRow();
                    summaryHeaderHeaderRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Bank Account"));
                    summaryHeaderHeaderRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Withdrawal"));
                    summaryHeaderHeaderRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Deposit"));
                    summaryHeader.rows.Insert(0, summaryHeaderHeaderRow);
                    if (summaryBody.rows.Count > 1)
                    {
                        summaryFooter = new bERPHtmlTableRowGroup(TableRowGroupType.footer);

                        double net = total.GetBalance(false);
                        summaryFooter.htmlAddRow(
                                new bERPHtmlTableCell("Total")
                                 , new bERPHtmlTableCell(AccountBase.FormatAmount(total.TotalCredit), "dcfrSummaryTotal")
                                 , new bERPHtmlTableCell(AccountBase.FormatAmount(total.TotalDebit), "dcfrSummaryTotal")
                                 );
                    }
                    bankSummaryTable.groups.Add(summaryHeader);
                    bankSummaryTable.groups.Add(summaryBody);
                    if (summaryFooter != null)
                        bankSummaryTable.groups.Add(summaryFooter);
                }
                return bankSummaryTable;
            }
            bERPHtmlTable buildCashSummaryTable()
            {
                bERPHtmlTableRowGroup summaryHeader;
                bERPHtmlTable cashSummaryTable = null;
                bERPHtmlTableRowGroup summaryBody = null;
                AccountBalance total = new AccountBalance(-1, 0, 0);
                bERPHtmlTableRowGroup summaryFooter = null;
                bERPHtmlTableRow summaryHeaderHeaderRow;
                foreach (CashAccount cash in _parent.GetAllCashAccounts(true))
                {
                    if (bal.ContainsKey(cash.csAccountID))
                    {
                        if (summaryBody == null)
                            summaryBody = new bERPHtmlTableRowGroup(TableRowGroupType.body);
                        AccountBalance change = bal[cash.csAccountID];
                        double net = change.GetBalance(false);
                        summaryBody.htmlAddRow(
                            new bERPHtmlTableCell(cash.name)
                            , new bERPHtmlTableCell(AccountBase.FormatAmount(AccountBase.AmountGreater(net, 0) ? net : 0), "dcfrSummaryValue")
                            , new bERPHtmlTableCell(AccountBase.FormatAmount(AccountBase.AmountLess(net, 0) ? -net : 0), "dcfrSummaryValue")
                            );
                        total.AddBalance(change);
                    }
                }
                if (summaryBody != null && summaryBody.rows.Count > 0)
                {
                    cashSummaryTable = new bERPHtmlTable();
                    summaryHeader = new bERPHtmlTableRowGroup(TableRowGroupType.header);
                    summaryHeaderHeaderRow = new bERPHtmlTableRow();
                    summaryHeaderHeaderRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Cash Account"));
                    summaryHeaderHeaderRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Net Addition"));
                    summaryHeaderHeaderRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Net Deduction"));
                    summaryHeader.rows.Insert(0, summaryHeaderHeaderRow);
                    if (summaryBody.rows.Count > 1)
                    {
                        summaryFooter = new bERPHtmlTableRowGroup(TableRowGroupType.footer);

                        double net = total.GetBalance(false);
                        summaryFooter.htmlAddRow(
                                new bERPHtmlTableCell("Total")
                                 , new bERPHtmlTableCell(AccountBase.FormatAmount(AccountBase.AmountGreater(net, 0) ? net : 0), "dcfrSummaryTotal")
                                 , new bERPHtmlTableCell(AccountBase.FormatAmount(AccountBase.AmountLess(net, 0) ? -net : 0), "dcfrSummaryTotal")
                                 );
                    }

                    cashSummaryTable.groups.Add(summaryHeader);
                    cashSummaryTable.groups.Add(summaryBody);
                    if (summaryFooter != null)
                        cashSummaryTable.groups.Add(summaryFooter);

                }
                return cashSummaryTable;
            }
            private static void setDocumentRowBorderStyle(bERPHtmlTableRowGroup body, int rowIndex, int nrows)
            {
                bERPHtmlTableRow firstRow = body.rows[rowIndex];
                bERPHtmlTableRow lastRow = body.rows[rowIndex+nrows- 1];
                firstRow.cells[0].css += " dcfrDocumentBorderTop dcfrDocumentBorderButtom";
                firstRow.cells[1].css += " dcfrDocumentBorderTop dcfrDocumentBorderButtom";
                for (int i = 2; i < firstRow.cells.Count; i++)
                    firstRow.cells[i].css += " dcfrDocumentBorderTop";
                for (int i = 0; i < lastRow.cells.Count; i++)
                {
                    if (firstRow == lastRow && i < 2)
                        continue;
                    else
                        lastRow.cells[i].css += " dcfrDocumentBorderButtom";
                }
            }
            CashFlowItem addMainTransaction(int accountID, double amount)
            {
                CashAccount cash;
                BankAccountInfo bank;
                if (isBankAccount(accountID, out bank))
                {
                    if (AccountBase.AmountGreater(amount, 0))
                    {
                        return CashFlowItem.createByText("Deposit to Bank Account " + bank.BankBranchAccount, amount);
                    }

                    if (AccountBase.AmountLess(amount, 0))
                    {
                        return CashFlowItem.createByText("Withdrawal from " + bank.BankBranchAccount, amount);
                    }
                }
                if (isCashAccount(accountID, out cash))
                {
                    if (AccountBase.AmountGreater(amount, 0))
                    {
                        return CashFlowItem.createByText("Paid to " +(cash.isStaffExpenseAdvance?"":" Casher ") +cash.name, amount);
                    }
                    if (AccountBase.AmountLess(amount, 0))
                    {
                        return CashFlowItem.createByText("Paid by " + (cash.isStaffExpenseAdvance ? "" : " Casher ") + cash.name, amount);
                    }
                }
                return null;
            }
            private void describeAccount(List<CashFlowItem> items, AccountDocument doc,AccountTransaction[] docTrans)
            {
                ICashTransaction handler = _accounting.GetDocumentHandler(doc.DocumentTypeID) as ICashTransaction;
                if (handler == null)
                    return;
                List<CashFlowItem> thisItems = handler.getCashFlowItems(doc, _options,docTrans);
                if (thisItems == null)
                    return;
                items.AddRange(thisItems);

            }
            void addCashFlowItemCell(bERPHtmlTableRowGroup body, string prefix, CashFlowItem item)
            {
                bERPHtmlTableCell detailDescCell;
                bERPHtmlTableCell crCell;
                bERPHtmlTableCell dbCell;
                string css = body.rows.Count % 2 == 0 ? "dcfrMainOdd" : "dcfrMainEven";

                detailDescCell = new bERPHtmlTableCell("");
                detailDescCell.innerHtml ="<span class='dcfrSubItemNo'>"+HttpUtility.HtmlEncode( prefix)+"</span>"+item.html;
                detailDescCell.css = "dcfrDetailDesc "+css;
                bool debitItem=AccountBase.AmountGreater(item.amount,0);
                dbCell = new bERPHtmlTableCell(debitItem ? AccountBase.FormatAmount(item.amount):"");
                dbCell.css = "dcfrDetailAmount " + css;
                crCell = new bERPHtmlTableCell(debitItem ? "":AccountBase.FormatAmount(-item.amount));
                crCell.css = "dcfrDetailAmount " + css;

                body.htmlAddRow(detailDescCell, crCell, dbCell);
            }
            bERPHtmlTable buildDetailTable()
            {
                bERPHtmlTable detailTable = null;
                bERPHtmlTableRowGroup body = new bERPHtmlTableRowGroup(TableRowGroupType.body);
                bERPHtmlTableCell cell;
                int itemNo=1;
                foreach (DetailedCashFlowItem item in items)
                {
                    string refstr = null;
                    foreach (DocumentSerial s in item.refs)
                    {
                        if (refstr == null)
                            refstr = getSerialType(s.typeID).prefix + ": " + s.reference;
                        else
                            refstr += "," + getSerialType(s.typeID).prefix + ": " + s.reference;
                    }
                    
                    //extract min and max time for the transaction
                    DateTime minTime = DateTime.MaxValue, maxTime = DateTime.MinValue;
                    foreach (AccountTransaction t in item.trans)
                    {
                        if (t.execTime < minTime)
                            minTime = t.execTime;
                        if (t.execTime > maxTime)
                            maxTime = t.execTime;
                    }


                    //retrieve all documents that share reference with the cash ledger entries
                    List<int> docs = new List<int>();
                    foreach (DocumentSerial s in item.refs)
                    {
                        foreach (int docID in _parent.m_accounting.getDocumentListByTypedReference(s.typedReference))
                        {
                            if (!docs.Contains(docID))
                            {
                                DateTime date = getDocument(docID).DocumentDate;
                                if (date >= _date1 && date < _date2)
                                    docs.Add(docID);
                            }
                        }
                    }
                    docs.Sort(new Comparison<int>(compareDocs));


                    int rowIndex = body.rows.Count;
                    
                    //merge the trasnactions from all the documents
                    List<CashFlowItem> cfItems = new List<CashFlowItem>();
                    List<AccountTransaction> allTransaction = new List<AccountTransaction>();
                    foreach (int docID in docs)
                    {
                        AccountDocument doc = getDocument(docID);
                        AccountTransaction[] trans = getDocTransactions(docID);
                        describeAccount(cfItems, doc,trans);
                        allTransaction.AddRange(trans);
                    }

                    if (cfItems.Count == 0)
                        continue;
                    //summerize transactions per each cash account
                    //Dictionary<int, double> cashSummary = new Dictionary<int, double>();
                    double netMovment = 0;
                    if (_options.costCenters == null)
                    {
                        foreach (AccountTransaction t in allTransaction)
                        {
                            if (isCashOrBankAccount(t.AccountID))
                            {
                                netMovment += t.Amount;
                                //if (cashSummary.ContainsKey(t.AccountID))
                                //    cashSummary[t.AccountID] += t.Amount;
                                //else
                                //    cashSummary.Add(t.AccountID, t.Amount);
                            }
                        }
                    }
                    Dictionary<int, double> cashSummary = new Dictionary<int, double>();
                    for (int i = cfItems.Count - 1; i >= 0; i--)
                    {
                        CashFlowItem it = cfItems[i];
                        if (it.cashAccountID != -1)
                        {
                            if (cashSummary.ContainsKey(it.cashAccountID))
                                cashSummary[it.cashAccountID] += it.amount;
                            else
                                cashSummary.Add(it.cashAccountID, it.amount);
                            if(_options.costCenters != null)
                                netMovment += cfItems[i].amount;
                            cfItems.RemoveAt(i);                            
                        }
                    }

                    bool cashAdded = netMovment > 0; //is the total cash movement addition or deduction
                    int subItemNo = 1;
                    //Cash transactions that are aligned to the overal all net movment
                    foreach (KeyValuePair<int, double> kv in cashSummary)
                    {
                        if ((cashAdded && AccountBase.AmountGreater(kv.Value, 0, 0.01)) || (!cashAdded && AccountBase.AmountLess(kv.Value, 0, 0.01)))
                        {
                            CashFlowItem mainItem = addMainTransaction(kv.Key, kv.Value);
                            addCashFlowItemCell( body,itemNo+"."+(subItemNo++)+". ",  mainItem);
                        }
                    }
                    
                    //none cash transactions that are alligned to the overal cash movement balance the net cash movement as returened by the ICashTransaction interface
                    double netMovmentBalance = 0;
                    foreach (CashFlowItem cfItem in cfItems)
                    {
                        if ((cashAdded && AccountBase.AmountGreater(cfItem.amount, 0, 0.01)) || (!cashAdded && AccountBase.AmountLess(cfItem.amount, 0, 0.01)))
                        {
                            netMovmentBalance += cfItem.amount;
                            addCashFlowItemCell(body, itemNo + "." + (subItemNo++) + ". ", cfItem);
                        }
                    }

                    //none cash transactions that are opposit to the overal cash movement balance the net cash movement as returened by the ICashTransaction interface
                    foreach (CashFlowItem cfItem in cfItems)
                    {
                        if ((!cashAdded && AccountBase.AmountGreater(cfItem.amount, 0, 0.01)) || (cashAdded && AccountBase.AmountLess(cfItem.amount, 0, 0.01)))
                        {
                            netMovmentBalance += cfItem.amount;
                            addCashFlowItemCell(body, itemNo + "." + (subItemNo++) + ". ", cfItem);
                        }
                    }
                    
                    
                    //Cash transactions that are oposit to the overal all net movment
                    foreach (KeyValuePair<int, double> kv in cashSummary)
                    {
                        if ((!cashAdded && AccountBase.AmountGreater(kv.Value, 0, 0.01)) || (cashAdded && AccountBase.AmountLess(kv.Value, 0, 0.01)))
                        {
                            CashFlowItem mainItem = addMainTransaction(kv.Key, kv.Value);
                            addCashFlowItemCell(body, itemNo + "." + (subItemNo++) + ". ", mainItem);
                        }
                    }
                    
                    //untracable. Net cash movment that are not balanceed by none-cash transactions
                    double difference = netMovmentBalance + netMovment;
                    if (!AccountBase.AmountEqual(difference, 0, 0.01))
                    {
                        CashFlowItem diff = new CashFlowItem("<span class='dcfrUntracableLabel'>Untracable</span>",-difference);
                        addCashFlowItemCell(body, itemNo + "." + (subItemNo++) + ". ", diff);
                    }
                    
                    //insert the reference cell
                    int nrows = body.rows.Count - rowIndex;
                    if (nrows > 0)
                    {
                        bERPHtmlTableCell noCell = new bERPHtmlTableCell((itemNo++).ToString());
                        noCell.rowSpan = nrows;
                        noCell.css = "dcfrNo";
                        body.rows[rowIndex].cells.Insert(0, noCell);
                        
                        bERPHtmlTableCell refCell = new bERPHtmlTableCell("");
                        refCell.innerHtml = string.Format("<span class='dcfrRef'>{0}</span><br/><span class='dcfrDate'>{1}</span><br/><span class='dcfrTime'>{2}</span>"
                            , HttpUtility.HtmlEncode(refstr == null ? "[No Reference]" : refstr)
                            , HttpUtility.HtmlEncode(minTime.ToString(DATE_FORMAT_YEAR))
                            , HttpUtility.HtmlEncode(minTime.ToString(TIME_FORMAT))
                            );
                        refCell.rowSpan = nrows;
                        refCell.css = "dcfrMainOdd"; 
                        body.rows[rowIndex].cells.Insert(1, refCell);
                        
                        setDocumentRowBorderStyle(body, rowIndex, nrows);
                    }
                }
                if (body.rows.Count > 0)
                {
                    bERPHtmlTableRowGroup header = new bERPHtmlTableRowGroup(TableRowGroupType.header);
                    bERPHtmlTableRow headerRow = new bERPHtmlTableRow();
                    header.rows.Add(headerRow);
                    headerRow.cells.Add(cell = new bERPHtmlTableCell(TDType.ColHeader, "No."));
                    headerRow.cells.Add(cell = new bERPHtmlTableCell(TDType.ColHeader, "Reference"));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Detail"));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Cash Source"));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Cash Sink"));

                    detailTable = new bERPHtmlTable();
                    detailTable.groups.Add(header);
                    detailTable.groups.Add(body);

                }
                return detailTable;
            }
            void loadLedger()
            {
                int N;
                string max = System.Configuration.ConfigurationManager.AppSettings["MaxDailyReportItems"];
                int pageSize;
                if (string.IsNullOrEmpty(max) || !int.TryParse(max, out pageSize))
                    pageSize = 500;

                List<int> accounts = new List<int>();
                foreach (BankAccountInfo bi in _parent.GetAllBankAccounts())
                    accounts.Add(bi.mainCsAccount);
                foreach (CashAccount cs in _parent.GetAllCashAccounts(true))
                    accounts.Add(cs.csAccountID);
                AccountTransaction[] ledger = _parent.m_accounting.GetTransactions(accounts.ToArray(), TransactionItem.DEFAULT_CURRENCY, _date1, _date2, 0, pageSize, out N);
                if (N > pageSize)
                    throw new ServerUserMessage("Too many transactions selected. Please try with narrower range of time");
                foreach (AccountTransaction l in ledger)
                {
                    if (!bal.ContainsKey(l.AccountID))
                    {
                        bal.Add(l.AccountID, new AccountBalance(l.AccountID, 0, l.Amount));
                    }
                    else
                        bal[l.AccountID].AddBalance(new AccountBalance(l.AccountID, 0, l.Amount));
                    DocumentSerial[] s = _parent.m_accounting.getDocumentSerials(l.documentID);
                    bool found = false;
                    foreach (DetailedCashFlowItem item in items)
                    {
                        List<DocumentSerial> newRefs = new List<DocumentSerial>();
                        foreach (DocumentSerial ds in item.refs)
                        {
                            foreach (DocumentSerial ss in s)
                            {
                                if (ds.Equals(ss))
                                {
                                    found = true;
                                }
                                else if (found)
                                {
                                    newRefs.Add(ss);
                                }
                            }
                        }
                        if (found)
                        {
                            item.refs.AddRange(newRefs);
                            item.trans.Add(l);
                            break;
                        }
                    }
                    if (!found)
                    {
                        DetailedCashFlowItem newItem = new DetailedCashFlowItem();
                        newItem.refs = new List<DocumentSerial>();
                        newItem.refs.AddRange(s);
                        newItem.trans = new List<AccountTransaction>();
                        newItem.trans.Add(l);
                        items.Add(newItem);
                    }
                }
            }
            public string generate()
            {
                loadLedger();
                bERPHtmlTable detailTable = buildDetailTable();
                bERPHtmlTable bankSummaryTable = null;
                bERPHtmlTable cashSummaryTable = null;
                if (_options.costCenters == null)
                {
                    bankSummaryTable = buildBankSummaryTable();
                    cashSummaryTable = buildCashSummaryTable();
                }

                StringBuilder builder = new StringBuilder();
                if (bankSummaryTable != null)
                {
                    builder.Append("<h3>Bank Account Summary</h3>");
                    bankSummaryTable.build(builder);
                }
                if (cashSummaryTable!= null)
                {
                    builder.Append("<h3>Cash Summary</h3>");
                    cashSummaryTable.build(builder);
                }
                if (detailTable != null)
                {
                    builder.Append("<h3>Detail</h3>");
                    detailTable.build(builder);
                }
                return builder.ToString();
            }
            
        }//end of class DetailedCashFlowGenerator 
        
        public string generateDetaileCashFlowReport( DateTime date1, DateTime date2,DetailedCashFlowReportOption options)
        {
            DetailedCashFlowGenerator g = new DetailedCashFlowGenerator(this, date1, date2,options);
            return g.generate();
        }        
    }
}
