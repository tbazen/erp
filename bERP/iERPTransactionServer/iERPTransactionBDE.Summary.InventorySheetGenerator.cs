﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        class InventorySheetGenerator
        {
            CostCenter _costCenter;
            iERPTransactionBDE _parent;
            string _genertedHTML;
            StringBuilder _builder;
            List<int> _levels;
            List<bool> _leaf;
            bERPHtmlTableRowGroup _bodyRows;
            InventoryGeneratorParameters _pars;
            Dictionary<string, StockLevelConfiguration> _stockLevelConfig=null;
            public InventorySheetGenerator(iERPTransactionBDE parent,
                InventoryGeneratorParameters pars
                )
            {

                _pars = pars;
                _parent = parent;
                _costCenter = _parent.Accounting.GetAccount<CostCenter>(_pars.costCenterID);

                if (_costCenter == null)
                {
                    _genertedHTML = "<h1>Please select a store or accounting center</h1>";
                    return;
                }
                //load stock level configuration if needed
                if(_pars.showBelowMinStock || _pars.showAboveMaxStock || _pars.showInventoryRunTime)
                {
                    _stockLevelConfig = new Dictionary<string, StockLevelConfiguration>();
                    foreach(StockLevelConfiguration config in parent.getStockLevelConfiguration())
                    {
                        _stockLevelConfig.Add(config.itemCode.ToLower(), config);
                    }
                }

                _builder = new StringBuilder();
                if (_pars.includeTitle)
                    generateTitleForInventoryList();
                bERPHtmlTable table = new bERPHtmlTable();
                bERPHtmlTableRowGroup header = new bERPHtmlTableRowGroup(TableRowGroupType.header);
                table.groups.Add(header);
                bERPHtmlTableRow headerRow1;
                if (_pars.seprateOrder)
                {
                    headerRow1 = bERPHtmlBuilder.htmlAddRow(header
                            , new bERPHtmlTableCell(TDType.ColHeader, "Code", "", 2, 1)
                            , new bERPHtmlTableCell(TDType.ColHeader, "Name", "", 2, 1)
                            , new bERPHtmlTableCell(TDType.ColHeader, "Unit", "", 2, 1)
                            , new bERPHtmlTableCell(TDType.ColHeader, "On Hand", 3)
                            , new bERPHtmlTableCell(TDType.ColHeader, "On Order", 3)
                            , new bERPHtmlTableCell(TDType.ColHeader, "Total", _pars.nCols)
                                    );
                    headerRow1.cells[2].styles.Add("text-align:center");
                    headerRow1.cells[3].styles.Add("text-align:center");
                    headerRow1.cells[4].styles.Add("text-align:center");

                    bERPHtmlTableRow row = bERPHtmlBuilder.htmlAddRow(header
                        , new bERPHtmlTableCell(TDType.ColHeader, "Quantity", "CurrencyCell")
                        );
                    
                    if (_pars.showInventoryRunTime)
                        row.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Sufficient For", "CurrencyCell"));
                   
                    row.cells.AddRange(new bERPHtmlTableCell[] {
                         new bERPHtmlTableCell(TDType.ColHeader, "Unit Price", "CurrencyCell")
                        , new bERPHtmlTableCell(TDType.ColHeader, "Price", "CurrencyCell")
                        , new bERPHtmlTableCell(TDType.ColHeader, "Quantity", "CurrencyCell")
                        , new bERPHtmlTableCell(TDType.ColHeader, "Unit Price", "CurrencyCell")
                        , new bERPHtmlTableCell(TDType.ColHeader, "Price", "CurrencyCell")
                    });

                    if (_pars.showNormalLevelColumn)
                        row.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Normal Level", "CurrencyCell"));

                    row.cells.AddRange(new bERPHtmlTableCell[] { 
                         new bERPHtmlTableCell(TDType.ColHeader, "Quantity", "CurrencyCell")
                        , new bERPHtmlTableCell(TDType.ColHeader, "Unit Price", "CurrencyCell")
                        , new bERPHtmlTableCell(TDType.ColHeader, "Price", "CurrencyCell")
                            });

                }
                else
                {
                    bERPHtmlTableRow row = bERPHtmlBuilder.htmlAddRow(header
                    , new bERPHtmlTableCell(TDType.ColHeader, "Code", "")
                    , new bERPHtmlTableCell(TDType.ColHeader, "Name", "")
                    , new bERPHtmlTableCell(TDType.ColHeader, "Unit", "")
                    );
                    
                    if (_pars.showNormalLevelColumn)
                        row.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Normal Level", "CurrencyCell"));


                    row.cells.AddRange(new bERPHtmlTableCell[] { 
                         new bERPHtmlTableCell(TDType.ColHeader, "Quantity", "CurrencyCell")
                    });
                    
                    if (_pars.showInventoryRunTime)
                        row.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Sufficient For", "CurrencyCell"));

                    row.cells.AddRange(new bERPHtmlTableCell[] { 
                         new bERPHtmlTableCell(TDType.ColHeader, "Unit Price", "CurrencyCell")
                        , new bERPHtmlTableCell(TDType.ColHeader, "Price", "CurrencyCell")
                            });
                }
                _bodyRows = new bERPHtmlTableRowGroup(TableRowGroupType.body);
                table.groups.Add(_bodyRows);
                _levels = new List<int>();
                _leaf = new List<bool>();

                double total;
                double totalOrder;
                buildInventorList(-1, 0, 0, 0, out total, out totalOrder);

                int maxLevel = 0;
                foreach (int l in _levels)
                    if (l > maxLevel)
                        maxLevel = l;
                if (maxLevel > 0)
                {
                    header.rows[0].cells[1].colSpan = maxLevel + 1;
                    int i = 0;
                    foreach (bERPHtmlTableRow row in _bodyRows.rows)
                    {
                        row.cells[1].colSpan = maxLevel - _levels[i] + 1;
                        for (int k = 0; k < _levels[i]; k++)
                        {
                            row.cells.Insert(1, new bERPHtmlTableCell("", "IndentCell"));
                        }
                        i++;
                    }
                }
                bERPHtmlTableRowGroup footer = new bERPHtmlTableRowGroup(TableRowGroupType.footer);
                table.groups.Add(footer);
                if (_pars.seprateOrder)
                {
                    bERPHtmlBuilder.htmlAddRow(footer,
                        new bERPHtmlTableCell(TDType.ColHeader, "Total", 3 + maxLevel)
                        , new bERPHtmlTableCell(TDType.ColHeader, AccountBase.FormatBalance(total), "CurrencyCell", 1, 3)
                        , new bERPHtmlTableCell(TDType.ColHeader, AccountBase.FormatBalance(totalOrder), "CurrencyCell", 1, 3)
                        , new bERPHtmlTableCell(TDType.ColHeader, AccountBase.FormatBalance(total + totalOrder), "CurrencyCell", 1, _pars.nCols)
                    );
                }
                else
                {
                    bERPHtmlBuilder.htmlAddRow(footer,
                        new bERPHtmlTableCell(TDType.ColHeader, "Total", 3 + maxLevel)
                        , new bERPHtmlTableCell(TDType.ColHeader, AccountBase.FormatBalance(total + totalOrder), "CurrencyCell", 1, _pars.nCols)
                    );
                }

                table.build(_builder);
                _genertedHTML = _builder.ToString();

            }
            public string GenertedHTML
            {
                get { return _genertedHTML; }
            }

            private void generateTitleForInventoryList()
            {
                TSConstants.addCompanyNameLine(_parent.SysPars.companyProfile, _builder);

                _builder.Append("<h2>Inventory Sheet</h2>");
                _builder.Append(string.Format("<span class='SubTitle'><b>As of</b> <u>{0}</u></span>", SummaryInformation.formatDate2(_pars.to)));
                _builder.Append(string.Format("<span class='SubTitle'><b>Store/Accounting Center: </b> <u>{0}</u></span>", _costCenter.NameCode));

            }
            private int buildInventorList(int parentCategory, int level, double totalStorePriceIn, double totalOrderPriceIn, out double totalStorePriceOut, out double totalOrderPriceOut)
            {

                totalStorePriceOut = totalStorePriceIn;
                totalOrderPriceOut = totalOrderPriceIn;
                int count = 0;
                foreach (ItemCategory cat in _parent.GetItemCategories(parentCategory))
                {
                    int index = _bodyRows.rows.Count;
                    double prevTotalStorePrice = totalStorePriceOut;
                    double prevTotalOrderPrice = totalOrderPriceOut;
                    int c = buildInventorList(cat.ID, level + 1, totalStorePriceOut, totalOrderPriceOut, out totalStorePriceOut, out totalOrderPriceOut);
                    if (c > 0)
                    {
                        _levels.Insert(index, level);
                        _leaf.Insert(index, false);
                        bERPHtmlTableRow catRow = new bERPHtmlTableRow();
                        catRow.cells.Add(new bERPHtmlTableCell(cat.Code));
                        catRow.cells.Add(new bERPHtmlTableCell(cat.description));
                        catRow.cells.Add(new bERPHtmlTableCell(""));
                        if (_pars.seprateOrder)
                        {
                            catRow.cells.Add(new bERPHtmlTableCell(TDType.body, AccountBase.FormatBalance(totalStorePriceOut - prevTotalStorePrice), "CurrencyCell", 1, 3));
                            catRow.cells.Add(new bERPHtmlTableCell(TDType.body, AccountBase.FormatBalance(totalOrderPriceOut - prevTotalOrderPrice), "CurrencyCell", 1, 3));
                        }
                        catRow.cells.Add(new bERPHtmlTableCell(TDType.body, AccountBase.FormatBalance(totalStorePriceOut + totalOrderPriceOut - (prevTotalStorePrice + prevTotalOrderPrice)), "CurrencyCell", 1, _pars.nCols));
                        catRow.css = "Section_Header_1";
                        _bodyRows.rows.Insert(index, catRow);
                        count += 1 + c;
                    }
                }
                int rowIndex = 0;
                foreach (TransactionItems item in _parent.GetItemsInCategory(parentCategory))
                {
                    StockLevelConfiguration config=null;

                    if(_stockLevelConfig!=null)
                    {
                        _stockLevelConfig.TryGetValue(item.Code.ToLower(), out config);
                        if(_pars.filterAbnormalStockLevel)
                        {
                            if (config == null)
                                continue;
                        }
                    }
                    double q = 0;
                    double p = 0;
                    //if (item.inventoryAccountID != -1)
                    //{
                    //    q += _parent.Accounting.GetNetCostCenterAccountBalanceAsOf(_costCenter.id, item.inventoryAccountID, TransactionItem.MATERIAL_QUANTITY, _pars.to);
                    //    p += _parent.Accounting.GetNetCostCenterAccountBalanceAsOf(_costCenter.id, item.inventoryAccountID, TransactionItem.DEFAULT_CURRENCY, _pars.to);
                    //}
                    //if (item.finishedGoodAccountID != -1)
                    //{
                    //    q += _parent.Accounting.GetNetCostCenterAccountBalanceAsOf(_costCenter.id, item.finishedGoodAccountID, TransactionItem.MATERIAL_QUANTITY, _pars.to);
                    //    p += _parent.Accounting.GetNetCostCenterAccountBalanceAsOf(_costCenter.id, item.finishedGoodAccountID, TransactionItem.DEFAULT_CURRENCY, _pars.to);
                    //}
                    //if (item.originalFixedAssetAccountID != -1)
                    //{
                    //    q += _parent.Accounting.GetNetCostCenterAccountBalanceAsOf(_costCenter.id, item.originalFixedAssetAccountID, TransactionItem.MATERIAL_QUANTITY, _pars.to);
                    //    p += _parent.Accounting.GetNetCostCenterAccountBalanceAsOf(_costCenter.id, item.originalFixedAssetAccountID, TransactionItem.DEFAULT_CURRENCY, _pars.to);
                    //}
                    //if (item.accumulatedDepreciationAccountID != -1)
                    //{
                    //    q += _parent.Accounting.GetNetCostCenterAccountBalanceAsOf(_costCenter.id, item.accumulatedDepreciationAccountID, TransactionItem.MATERIAL_QUANTITY, _pars.to);
                    //    p += _parent.Accounting.GetNetCostCenterAccountBalanceAsOf(_costCenter.id, item.accumulatedDepreciationAccountID, TransactionItem.DEFAULT_CURRENCY, _pars.to);
                    //}
                    //double po = _parent.Accounting.GetNetCostCenterAccountBalanceAsOf(_costCenter.id, item.pendingOrderAccountID, TransactionItem.DEFAULT_CURRENCY, _pars.to);
                    //double qo = _parent.Accounting.GetNetCostCenterAccountBalanceAsOf(_costCenter.id, item.pendingOrderAccountID, TransactionItem.MATERIAL_QUANTITY, _pars.to);

                    if (item.inventoryAccountID != -1)
                    {
                        q += _parent.Accounting.GetNetCostCenterAccountBalanceAsOfH(_costCenter.id, item.inventoryAccountID, TransactionItem.MATERIAL_QUANTITY, _pars.to);
                        p += _parent.Accounting.GetNetCostCenterAccountBalanceAsOfH(_costCenter.id, item.inventoryAccountID, TransactionItem.DEFAULT_CURRENCY, _pars.to);
                    }
                    if (item.finishedGoodAccountID != -1)
                    {
                        q += _parent.Accounting.GetNetCostCenterAccountBalanceAsOfH(_costCenter.id, item.finishedGoodAccountID, TransactionItem.MATERIAL_QUANTITY, _pars.to);
                        p += _parent.Accounting.GetNetCostCenterAccountBalanceAsOfH(_costCenter.id, item.finishedGoodAccountID, TransactionItem.DEFAULT_CURRENCY, _pars.to);
                    }
                    if (item.originalFixedAssetAccountID != -1)
                    {
                        q += _parent.Accounting.GetNetCostCenterAccountBalanceAsOfH(_costCenter.id, item.originalFixedAssetAccountID, TransactionItem.MATERIAL_QUANTITY, _pars.to);
                        p += _parent.Accounting.GetNetCostCenterAccountBalanceAsOfH(_costCenter.id, item.originalFixedAssetAccountID, TransactionItem.DEFAULT_CURRENCY, _pars.to);
                    }
                    if (item.accumulatedDepreciationAccountID != -1)
                    {
                        q += _parent.Accounting.GetNetCostCenterAccountBalanceAsOfH(_costCenter.id, item.accumulatedDepreciationAccountID, TransactionItem.MATERIAL_QUANTITY, _pars.to);
                        p += _parent.Accounting.GetNetCostCenterAccountBalanceAsOfH(_costCenter.id, item.accumulatedDepreciationAccountID, TransactionItem.DEFAULT_CURRENCY, _pars.to);
                    }
                    double po = _parent.Accounting.GetNetCostCenterAccountBalanceAsOfH(_costCenter.id, item.pendingOrderAccountID, TransactionItem.DEFAULT_CURRENCY, _pars.to);
                    double qo = _parent.Accounting.GetNetCostCenterAccountBalanceAsOfH(_costCenter.id, item.pendingOrderAccountID, TransactionItem.MATERIAL_QUANTITY, _pars.to);

                    if (AccountBase.AmountEqual(q, 0) && AccountBase.AmountEqual(p, 0)
                        && AccountBase.AmountEqual(qo, 0) && AccountBase.AmountEqual(po, 0))
                        continue;
                    double pt = p + po;
                    double qt = q + qo;
                    string up;
                    if (AccountBase.AmountEqual(p, 0) && AccountBase.AmountEqual(q, 0))
                        up = "-";
                    else
                        up = AccountBase.AmountEqual(q, 0) ? "Data Error" : AccountBase.FormatAmount(p / q);

                    string upo;
                    if (AccountBase.AmountEqual(po, 0) && AccountBase.AmountEqual(qo, 0))
                        upo = "-";
                    else
                        upo = AccountBase.AmountEqual(qo, 0) ? "Data Error" : AccountBase.FormatAmount(po / qo);



                    string upt;
                    if (AccountBase.AmountEqual(pt, 0) && AccountBase.AmountEqual(qt, 0))
                        upt = "-";
                    else
                        upt = AccountBase.AmountEqual(qt, 0) ? "Data Error" : AccountBase.FormatAmount(pt / qt);

                    StockLevelType levelType=StockLevelType.Normal;
                    if (_stockLevelConfig != null)
                    {
                        if(config!=null)
                            levelType = config.getStockLevelType(q, _pars.showBelowMinStock, _pars.showAboveMaxStock);
                        if (_pars.filterAbnormalStockLevel)
                        {
                            if(levelType==StockLevelType.Normal)
                                continue;
                        }
                    }

                    totalStorePriceOut += p;
                    totalOrderPriceOut += po;


                    _levels.Add(level);
                    _leaf.Add(true);
                    MeasureUnit unit = _parent.GetMeasureUnit(item.MeasureUnitID);
                    bERPHtmlTableRow itemRow = new bERPHtmlTableRow();
                    itemRow.cells.Add(new bERPHtmlTableCell(item.Code));
                    itemRow.cells.Add(new bERPHtmlTableCell(item.Name));
                    itemRow.cells.Add(new bERPHtmlTableCell(unit == null ? "N/A" : unit.Name));
                    if (_pars.seprateOrder)
                    {
                        itemRow.cells.Add(new bERPHtmlTableCell(MeasureUnit.FormatQuantity(q, null), "CurrencyCell"));
                        if (_pars.showInventoryRunTime)
                            itemRow.cells.Add(new bERPHtmlTableCell(config == null || config.dailyUse == 0 ? "" : (q / config.dailyUse).ToString("0.0") + " days", "CurrencyCell"));

                        itemRow.cells.Add(new bERPHtmlTableCell(up, "CurrencyCell"));
                        itemRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatBalance(p), "CurrencyCell"));


                        itemRow.cells.Add(new bERPHtmlTableCell(MeasureUnit.FormatQuantity(qo, null), "CurrencyCell"));
                        itemRow.cells.Add(new bERPHtmlTableCell(upo, "CurrencyCell"));
                        itemRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatBalance(po), "CurrencyCell"));
                    }
                    if (_pars.showNormalLevelColumn)
                        itemRow.cells.Add(new bERPHtmlTableCell(config==null?"":config.ToString(), "CurrencyCell"));

                    itemRow.cells.Add(new bERPHtmlTableCell(MeasureUnit.FormatQuantity(qt, null), "CurrencyCell"));
                    if (!_pars.seprateOrder && _pars.showInventoryRunTime)
                        itemRow.cells.Add(new bERPHtmlTableCell(config == null || config.dailyUse == 0 ? "" : (qt / config.dailyUse).ToString("0") + " days", "CurrencyCell"));

                    itemRow.cells.Add(new bERPHtmlTableCell(upt, "CurrencyCell"));
                    itemRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatBalance(pt), "CurrencyCell"));
                    


                    itemRow.css = rowIndex % 2 == 0 ? "Even_row" : "Odd_row";
                    switch(levelType)
                    {
                        case StockLevelType.AboveMaximum:
                            itemRow.css += " aboveMaxStock";
                            break;
                        case StockLevelType.BelowMinimum:
                            itemRow.css += " belowMinStock";
                            break;
                    }
                    _bodyRows.rows.Add(itemRow);
                    rowIndex++;
                    count++;
                }
                return count;
            }

        }
    }
}