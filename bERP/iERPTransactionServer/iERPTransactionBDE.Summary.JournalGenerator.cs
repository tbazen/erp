﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using System.Web;
using INTAPS;
namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        class GournalGenerator
        {
            iERPTransactionBDE _parent;
            JournalFilter _filter;
            bERPHtmlTableRowGroup header;
            bERPHtmlTableRow headerRow;
            int pageIndex;
            int pageSize;
            public int NRecords;
            public GournalGenerator(iERPTransactionBDE parent, JournalFilter filter, int pageIndex, int pageSize)
            {
                this.pageIndex = pageIndex;
                this.pageSize = pageSize;
                _parent = parent;
                _filter = filter;
            }

            public string generate()
            {
                JournalPrimaryEntry[] refs;
                if (_filter.specificReferences == null)
                {
                    if (string.IsNullOrEmpty(_filter.textFilter))
                        refs = _parent.Accounting.GetDocumentJournal(true, _filter.time1, _filter.time2, _filter.docTypes, _filter.refTypes, null, pageIndex, pageSize, _filter.sortBy, out NRecords);
                    else
                        refs = _parent.Accounting.GetDocumentJournal(false, _filter.time1, _filter.time2, _filter.docTypes, _filter.refTypes, _filter.textFilter, pageIndex, pageSize, _filter.sortBy, out NRecords);
                }
                else
                {
                    List<JournalPrimaryEntry> jrefs = new List<JournalPrimaryEntry>();
                    for (int i = 0; i < _filter.specificReferences.Length; i++)
                    {
                        DocumentSerial ser = _parent.Accounting.getPrimaryDocumentSerial(_filter.specificReferences[i].typeID, _filter.specificReferences[i].reference);
                        if (ser == null)
                            continue;
                        JournalPrimaryEntry ajref = new JournalPrimaryEntry(ser);
                        ajref.instanceCount = _parent.Accounting.getDocumentTypedReferenceCount(ajref.typeID, ajref.reference);
                        jrefs.Add(ajref);
                    }
                    refs = jrefs.ToArray();
                }
                bERPHtmlTable table = new bERPHtmlTable();
                header = new bERPHtmlTableRowGroup(TableRowGroupType.header);
                headerRow = new bERPHtmlTableRow();
                header.rows.Add(headerRow);


                bERPHtmlTableCell cell;
                headerRow.cells.Add(cell = new bERPHtmlTableCell(TDType.ColHeader, "No."));
                headerRow.cells.Add(cell = new bERPHtmlTableCell(TDType.ColHeader, "Reference", 2));
                cell.styles.Add("width:20%");
                headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Type"));
                headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Date"));
                headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Cash Debit"));
                headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Cash Credit"));
                headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Remark"));
                headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, ""));
                bERPHtmlTableRowGroup body = new bERPHtmlTableRowGroup(TableRowGroupType.body);
                int index = 0;

                List<int> cashAccounts = new List<int>();
                INTAPS.RDBMS.SQLHelper reader = _parent.GetReaderHelper();
                try
                {
                    cashAccounts.AddRange(reader.GetColumnArray<int>("SELECT [mainCsAccount] FROM {0}.[dbo].[BankAccountInfo]".format(_parent.DBName)));
                    cashAccounts.AddRange(reader.GetColumnArray<int>("SELECT [csAccountID] FROM {0}.[dbo].[CashAccount]".format(_parent.DBName)));
                }
                finally
                {
                    _parent.ReleaseHelper(reader);
                }
                cashAccounts.Sort();

                foreach (JournalPrimaryEntry s in refs)
                {
                    bool expanded = false;
                    if (_filter.expand != null)
                    {
                        foreach (DocumentTypedReference tr in _filter.expand)
                            if (tr.Equals(new DocumentTypedReference(s.typeID, s.reference, false)))
                            {
                                expanded = true;
                                break;
                            }
                    }
                    DocumentSerialType stype = _parent.Accounting.getDocumentSerialType(s.typeID);
                    cell = new bERPHtmlTableCell("");
                    cell.styles.Add("vertical-align: middle");
                    cell.styles.Add("width: 0%");
                    cell.id = cell.name = s.typeID + "_" + s.reference;
                    if (s.instanceCount > 1)
                        cell.innerHtml = string.Format(
                            "<a href='expand?typeID={0}&ref={1}'><img name='type_{0}' src='images\\{2}.png'></a>"
                        , s.typeID
                        , System.Web.HttpUtility.UrlEncode(s.reference)
                        , expanded ? "itemwithchild_expanded" : "itemwithchild"
                        );
                    else
                        cell.innerHtml = "<img src='images\\nochilditem.png'>";

                    bERPHtmlTableCell cell1 = new bERPHtmlTableCell("");
                    cell1.styles.Add("width:20%");
                    cell1.innerHtml = System.Web.HttpUtility.HtmlEncode(s.reference);


                    bERPHtmlTableCell cell2 = new bERPHtmlTableCell("");
                    cell2.styles.Add("vertical-align: middle");
                    cell2.styles.Add("width:20%");
                    if (s.instanceCount > 1)
                        cell2.innerHtml = string.Format("<a href='refentry?typeID={0}&ref={1}'>Details</a>", s.typeID, s.reference);
                    else
                        cell2.innerHtml = string.Format("<a href='refentry?typeID={0}&ref={1}'>Details</a><span style='width:10px'></span><a href='docedit?docID={2}&docTypeID={3}'>Edit</a><span style='width:10px'></span><a href='docdelete?docID={2}'>Delete</a>", s.typeID, s.reference, s.documentID, _parent.Accounting.GetDocumentTypeByDocumentID(s.documentID).id);

                    double cashDebit=0;
                    double cashCredit=0;
                    foreach (int docID in this._parent.Accounting.getDocumentListByTypedReference(new DocumentTypedReference(s.typeID, s.reference, false)))
                    {
                        foreach(AccountTransaction tran in _parent.Accounting.GetTransactionsOfDocument(docID))
                        {
                            if(cashAccounts.BinarySearch(tran.AccountID)>-1)
                            {
                                if (tran.Amount > 0)
                                    cashDebit += tran.Amount;
                                else
                                    cashCredit -= tran.Amount;
                            }
                        }
                    }
                    bERPHtmlTableCell cellDebit = new bERPHtmlTableCell(AccountBase.FormatAmount(cashDebit));
                    cellDebit.css = "currencyCell";

                    bERPHtmlTableCell cellCredit = new bERPHtmlTableCell(AccountBase.FormatAmount(cashCredit));
                    cellCredit.css = "currencyCell";


                    bERPHtmlBuilder.htmlAddRow(body
                            , new bERPHtmlTableCell((pageIndex + index + 1).ToString())
                            , cell
                            , cell1
                            , new bERPHtmlTableCell(stype == null ? "" : stype.name)
                            , new bERPHtmlTableCell(s.primaryDate.ToString("MMM dd,yyyy hh:mm tt"))
                            , cellDebit
                            , cellCredit
                            , new bERPHtmlTableCell(s.primaryShortDescription)
                            , cell2
                    ).css = index % 2 == 0 ? "Even_row" : "Odd_row";
                    if (s.instanceCount > 1)
                    {
                        bERPHtmlTableRow detailRow = bERPHtmlBuilder.htmlAddRow(body, cell = new bERPHtmlTableCell("", 9));
                        cell.id = cell.name = "detail_" + s.typeID + "_" + s.reference;
                        if (expanded)
                            cell.innerHtml = _parent.getDocumentListHtmlByTypedReference(s.typeID, s.reference);
                    }
                    index++;
                }

                //bERPHtmlTableRowGroup footer = new bERPHtmlTableRowGroup(TableRowGroupType.footer);

                StringBuilder builder = new StringBuilder();
                table.groups.Add(header);
                table.groups.Add(body);
                //table.groups.Add(footer);
                table.build(builder);
                return builder.ToString();
            }
        }

        // BDE exposed
        public string getTypedReferenceEntriesHTML(DocumentTypedReference tref)
        {
            DocumentSerialType st = m_accounting.getDocumentSerialType(tref.typeID);
            if (st == null)
                return "<h2>Unknown serial type ID:" + tref.typeID + "</h2>";
            string header = "<h2>" + System.Web.HttpUtility.HtmlEncode(st.name) + "</h2>"
                + "<h2>" + System.Web.HttpUtility.HtmlEncode(tref.reference) + "</h2>";

            DocumentSerial primary = m_accounting.getPrimaryDocumentSerial(tref.typeID, tref.reference);
            if (primary != null)
            {
                header += "<h2>" + System.Web.HttpUtility.HtmlEncode("Date: " + AccountBase.FormatTime(primary.primaryDoc.DocumentDate)) + "</h2>";
            }
            string batchHTML = m_accounting.getSummerizedBatchHTML(m_accounting.GetTransactionsOfTypeReference(tref),SysPars.summerizeItemTransactions);
            string footer = string.Format("<br/><span class='documentNoteLabel'>Note:</span><span class='documentNote'>{0}</span>", HttpUtility.HtmlEncode(primary.primaryDoc.ShortDescription));
            return
                header
                + "<h2>Affected accounts</h2>"
                + batchHTML
                +footer;
        }

        // BDE exposed
        public string getDocumentListHtmlByTypedReference(int typeID, string reference)
        {
            bERPHtmlTable detailTable = new bERPHtmlTable();
            bERPHtmlTableRowGroup detailHeader = new bERPHtmlTableRowGroup(TableRowGroupType.header);
            bERPHtmlTableRowGroup detailBody = new bERPHtmlTableRowGroup(TableRowGroupType.body);
            detailTable.groups.Add(detailHeader);
            detailTable.groups.Add(detailBody);
            bERPHtmlTableCell cell,cell2;
            bERPHtmlBuilder.htmlAddRow(detailHeader
                                    , new bERPHtmlTableCell(TDType.ColHeader, "Date")
                                    , new bERPHtmlTableCell(TDType.ColHeader, "Reference")
                                    , new bERPHtmlTableCell(TDType.ColHeader, "Type")
                                    , new bERPHtmlTableCell(TDType.ColHeader, "Remark")
                                    , new bERPHtmlTableCell(TDType.ColHeader, "")
                                    );

            int index = 0;
            foreach (int docID in m_accounting.getDocumentListByTypedReference(new DocumentTypedReference(typeID, reference, false)))
            {
                AccountDocument doc = m_accounting.GetAccountDocument(docID, false);
                if (doc == null)
                {
                    bERPHtmlBuilder.htmlAddRow(detailBody, new bERPHtmlTableCell(TDType.body, "Document ID:" + docID + " could not be loaded", 5));

                }
                else
                {
                    string references = null;
                    foreach (DocumentTypedReference r in m_accounting.getAllDocumentReferences(doc.AccountDocumentID))
                    {
                        string refhtml = HttpUtility.HtmlEncode(m_accounting.getDocumentSerialType(r.typeID).prefix + ":" + r.reference);
                        if (references == null)
                            references = refhtml;
                        else
                            references += "<br/>" + refhtml;
                    }
                    bERPHtmlBuilder.htmlAddRow(detailBody
                        , new bERPHtmlTableCell(TDType.body, doc.DocumentDate.ToString("MMM dd,yyyy hh:mm tt"))
                        , cell2=new bERPHtmlTableCell(TDType.body, "")
                        , new bERPHtmlTableCell(TDType.body, m_accounting.GetDocumentTypeByID(doc.DocumentTypeID).name)
                        , new bERPHtmlTableCell(TDType.body, doc.ShortDescription)
                        ,cell=new bERPHtmlTableCell(TDType.ColHeader, "")
                        ).css = index % 2 == 0 ? "Even_row" : "Odd_row";
                    cell.innerHtml = string.Format("<a href='docentry?docID={0}'>Details</a><span style='width:10px'></span><a href='docedit?docID={0}&docTypeID={1}'>Edit</a><span style='width:10px'></span><a href='docdelete?docID={0}'>Delete</a>", docID, doc.DocumentTypeID);
                    cell2.innerHtml = references;
                }
                index++;
            }
            StringBuilder sb = new StringBuilder();
            detailTable.build(sb);
            return sb.ToString();
        }
        // BDE exposed
        public string getPrimaryJournal(JournalFilter filter,int pageIndex,int pageSize,out int NRecord)
        {
            GournalGenerator gg = new GournalGenerator(this, filter,pageIndex,pageSize);
            string ret=gg.generate();
            NRecord = gg.NRecords;
            return ret;
        }
    }
}
