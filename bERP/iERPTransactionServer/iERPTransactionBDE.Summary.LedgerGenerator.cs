﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using System.Web;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        class TradeRelationLedgerGenerator
        {
            iERPTransactionBDE bde;
            public string getTradeRelationTransactions(iERPTransactionBDE bde,string code, long ticksFrom, long ticksTo, int[] docTypes)
            {
                int N;
                this.bde = bde;
                bool filterByDate = ticksFrom != -1 && ticksTo != -1;
                AccountDocument[] tranDocs = bde.Accounting.GetAccountDocuments(new DocumentSearchPar()
                {
                    date = filterByDate,
                    from=filterByDate?new DateTime(ticksFrom):DateTime.Now,
                    to=filterByDate?new DateTime(ticksTo):DateTime.Now,
                    type = -1,
                    includeType = new int[]
                {
                    bde.Accounting.GetDocumentTypeByType(typeof(Purchase2Document)).id,
                    bde.Accounting.GetDocumentTypeByType(typeof(SupplierAdvancePayment2Document)).id,
                    bde.Accounting.GetDocumentTypeByType(typeof(SupplierAdvanceReturn2Document)).id,
                    bde.Accounting.GetDocumentTypeByType(typeof(SupplierRetentionSettlementDocument)).id,
                    bde.Accounting.GetDocumentTypeByType(typeof(SupplierCreditSettlement2Document)).id,

                    bde.Accounting.GetDocumentTypeByType(typeof(Sell2Document)).id,
                    bde.Accounting.GetDocumentTypeByType(typeof(CustomerAdvancePayment2Document)).id,
                    bde.Accounting.GetDocumentTypeByType(typeof(CustomerAdvanceReturn2Document)).id,
                    bde.Accounting.GetDocumentTypeByType(typeof(CustomerRetentionSettlementDocument)).id,
                    bde.Accounting.GetDocumentTypeByType(typeof(CustomerCreditSettlement2Document)).id,

                },
                },
                null
                , 0, -1, out N);
                List<TradeTransaction> temp= new List<TradeTransaction>();
                for(int i=tranDocs.Length-1;i>=0;i--)
                    if (((TradeTransaction) tranDocs[i]).relationCode.Equals(code))
                        temp.Add(((TradeTransaction)tranDocs[i]));
                tranDocs = temp.ToArray();

                bERPHtmlTableRowGroup header;
                bERPHtmlTableRow headerRow;
                bERPHtmlTableCell dbCell, crCell = null;


                bERPHtmlTable table = new bERPHtmlTable();
                header = new bERPHtmlTableRowGroup(TableRowGroupType.header);
                headerRow = new bERPHtmlTableRow();
                header.rows.Add(headerRow);

                headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Date"));
                headerRow.cells.Add(dbCell = new bERPHtmlTableCell(TDType.ColHeader, "Receipt", "CurrencyCell"));
                headerRow.cells.Add(crCell = new bERPHtmlTableCell(TDType.ColHeader, "Payment", "CurrencyCell"));

                headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "References"));
                headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Remark"));

                bERPHtmlTableRowGroup body = new bERPHtmlTableRowGroup(TableRowGroupType.body);


                double totalPayment = 0;
                double totalReceipt = 0;
                foreach (TradeTransaction t in tranDocs)
                {
                    body.rows.Add(addLedgerRow(body.rows.Count, t, ref totalReceipt, ref totalPayment));
                }


                bERPHtmlTableRowGroup footer = new bERPHtmlTableRowGroup(TableRowGroupType.footer);
                
                TDType cellType = TDType.ColHeader;
                bERPHtmlTableRow row = bERPHtmlBuilder.htmlAddRow(body,
                    new bERPHtmlTableCell(cellType, "Total")
                    , new bERPHtmlTableCell(cellType, Account.FormatBalance(totalReceipt), "CurrencyCell")
                    , new bERPHtmlTableCell(cellType, Account.FormatBalance(totalPayment), "CurrencyCell")
                    );
                row.css = "Section_Header_3";

                StringBuilder builder = new StringBuilder();
                table.groups.Add(header);
                table.groups.Add(body);
                table.groups.Add(footer);
                table.build(builder);
                return builder.ToString();
            }
            private bERPHtmlTableRow addLedgerRow(int index, TradeTransaction t,ref double totalReceipt,ref double totalPayment)
            {
                bERPHtmlTableRow row = new bERPHtmlTableRow();
                bERPHtmlTableCell cell;
                row.cells.Add(new bERPHtmlTableCell(AccountBase.FormatTime(t.DocumentDate)));

                bERPHtmlTableCell cellAmount = null;

                double amount = t.paidAmount;// t is Purchase2Document ? ((Purchase2Document)t).netPayment : ((Sell2Document)t).netPayment;

                if (AccountBase.AmountEqual(amount, 0))
                {
                    row.cells.Add(cellAmount = new bERPHtmlTableCell(""));
                    row.cells.Add(new bERPHtmlTableCell(""));
                }
                else
                {
                    if (t is Sell2Document)
                    {
                        row.cells.Add(cellAmount = new bERPHtmlTableCell(AccountBase.FormatAmount(amount)));
                        row.cells.Add(new bERPHtmlTableCell(""));
                        totalReceipt += amount;
                    }
                    else
                    {
                        row.cells.Add(new bERPHtmlTableCell(""));
                        row.cells.Add(cellAmount = new bERPHtmlTableCell(AccountBase.FormatAmount(amount)));
                        totalPayment += amount;
                    }
                }
                if (cellAmount != null)
                    cellAmount.css = "currencyCell";
                cellAmount.innerHtml = "<a href='" + SummaryInformation.LINK_OPEN_DOC + "?docID=" + t.AccountDocumentID + "&docTypeID=" + t.DocumentTypeID + "'>" + cellAmount.innerHtml + "</a>";

                row.css = index % 2 == 0 ? "Even_row" : "Odd_row";
                
                string references = null;
                foreach (DocumentTypedReference r in bde.m_accounting.getAllDocumentReferences(t.AccountDocumentID))
                {
                    string refhtml = HttpUtility.HtmlEncode(bde.m_accounting.getDocumentSerialType(r.typeID).prefix + ":" + r.reference);
                    if (references == null)
                        references = refhtml;
                    else
                        references += "<br/>" + refhtml;
                }
                row.cells.Add(cell = new bERPHtmlTableCell(""));
                cell.innerHtml = references;

                DocumentType dt = bde.Accounting.GetDocumentTypeByID(t.DocumentTypeID);
                Type docType = dt.GetTypeObject();
                string remark;
                    remark = dt.name;
                    if (!string.IsNullOrEmpty(t.PaperRef))
                    {
                        if (string.IsNullOrEmpty(remark))
                            remark = "Ref:" + t.PaperRef;
                        else
                            remark += "\nRef:" + t.PaperRef;
                    }
                    if (!string.IsNullOrEmpty(t.ShortDescription))
                    {
                        if (string.IsNullOrEmpty(remark))
                            remark = t.ShortDescription;
                        else
                            remark += "\n" + t.ShortDescription;
                    }
                    if (docType.GetInterface("IStandardAttributeDocument") != null)
                    {
                        IStandardAttributeDocument atr = t as IStandardAttributeDocument;
                        if (atr.hasInstrument)
                        {
                            if (string.IsNullOrEmpty(remark))
                                remark = t.ShortDescription;
                            else
                                remark += "\n" + atr.getInstrument(true);
                        }
                    }
                
                row.cells.Add(cell = new bERPHtmlTableCell(remark));
                cell.innerHtml += string.Format("<br/><a href='{2}?docID={0}&docTypeID={1}'>Edit</a><span style='width:10px'></span><a href='{3}?docID={0}'>Delete</a><span style='width:10px'></span><a href='{4}?docID={0}'>Entries</a>", t.AccountDocumentID, t.DocumentTypeID, SummaryInformation.LINK_OPEN_DOC, SummaryInformation.LINK_DELETE_DOC, SummaryInformation.LINK_VIEW_ENTRIES);
                return row;
            }
        }
        class LedgerGenerator
        {
            const int MAX_LDEGER_TRANS = 5000;
            LedgerParameters pars;

            iERPTransactionBDE _parent;
            

            public LedgerGenerator(iERPTransactionBDE parent, LedgerParameters pars)
            {
                this.pars = pars;
                _parent = parent;
                //validate input
                foreach (int accountID in pars.accountIDs)
                {
                    Account account = parent.Accounting.GetAccount<Account>(accountID);
                }
            }


            bERPHtmlTableRowGroup header;
            bERPHtmlTableRow headerRow;
            bERPHtmlTableCell dbCell, crCell = null, balCell;
            bERPHtmlTableRow begRow;
            //CostCenterAccountWithDescription resultAccount;
            Dictionary<int, CostCenterAccountWithDescription> accounts;
            public string generate(out bool hasBalance,out bool hasLedger)
            {
                int N;
                AccountBalance begBal;
                AccountBalance total;
                int pageSize = string.IsNullOrEmpty(pars.groupByPeriod)? MAX_LDEGER_TRANS : -1;
                List<int> la = new List<int>();
                foreach (int ac in pars.accountIDs)
                {
                    CostCenterAccount csa = _parent.Accounting.GetCostCenterAccount(pars.costCenterID,ac);
                    if (csa == null)
                        continue;
                    if (csa.isControlAccount)
                        la.AddRange(_parent.Accounting.getLeaveCostCenterAccounts( csa.id));
                    else
                        la.Add(csa.id);
                }
                AccountTransaction[] tran = _parent.Accounting.GetLedger(-1, pars.itemID, la.ToArray(), new int[] { pars.itemID }, pars.time1, pars.time2, 0, pageSize, out N, out begBal, out total);
                hasLedger = tran.Length > 0;
                hasBalance = hasLedger || !begBal.IsZero;
                if (string.IsNullOrEmpty(pars.groupByPeriod) && N > 5000)
                    throw new ServerUserMessage("Too many transactions in the ledger");
                //resultAccount = _parent.Accounting.GetCostCenterAccountWithDescription(csAccountID);
                accounts = new Dictionary<int, CostCenterAccountWithDescription>();
                bool accountColumn = true;
                bool itemColumn = true;
                
                if (pars.accountIDs.Length == 1 && pars.hideAccountColumnIfUniformAccount || pars.hideItemColumnIfUniformItem)
                {
                    int parCostCenterAccountID = this._parent.Accounting.GetCostCenterAccountID(pars.costCenterID, pars.accountIDs[0]);
                    accountColumn = false;
                    itemColumn = false;
                    foreach (AccountTransaction t in tran)
                    {
                        if (parCostCenterAccountID != t.AccountID)
                        {
                            accountColumn = true;
                        }
                        if (pars.itemID != t.ItemID)
                        {
                            itemColumn = true;
                        }
                    }
                }
                bERPHtmlTable table = new bERPHtmlTable();
                header = new bERPHtmlTableRowGroup(TableRowGroupType.header);
                headerRow = new bERPHtmlTableRow();
                header.rows.Add(headerRow);

                headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Date"));
                if (pars.twoColumns)
                {
                    headerRow.cells.Add(dbCell = new bERPHtmlTableCell(TDType.ColHeader, "Debit", "CurrencyCell"));
                    headerRow.cells.Add(crCell = new bERPHtmlTableCell(TDType.ColHeader, "Credit", "CurrencyCell"));
                }
                else
                    headerRow.cells.Add(dbCell = new bERPHtmlTableCell(TDType.ColHeader, "Amount", "CurrencyCell"));
                headerRow.cells.Add(balCell = new bERPHtmlTableCell(TDType.ColHeader, "Balance", "CurrencyCell"));
                headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "References"));
                headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Remark"));
                if (accountColumn)
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Account"));
                if (itemColumn)
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Item"));

                bERPHtmlTableRowGroup body = new bERPHtmlTableRowGroup(TableRowGroupType.body);
                begRow = bERPHtmlBuilder.htmlAddRow(body,
                    new bERPHtmlTableCell(TDType.ColHeader, "Beginning Balance", headerRow.cells.IndexOf(balCell))
                    , new bERPHtmlTableCell(TDType.ColHeader, Account.FormatBalance(begBal.GetBalance(pars.asCreditAccount)), "CurrencyCell")
                    );
                begRow.css = "Section_Header_3";

                for (int i = headerRow.cells.IndexOf(balCell) + 1; i < headerRow.cells.Count; i++)
                    begRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, ""));



                double endingBalance = begBal.GetBalance(pars.asCreditAccount);
                if (string.IsNullOrEmpty(pars.groupByPeriod))
                {
                    foreach (AccountTransaction t in tran)
                    {
                        body.rows.Add(addLedgerRow(body.rows.Count, t, accountColumn, itemColumn, pars.twoColumns));
                        endingBalance = t.GetNewBalance(pars.asCreditAccount);
                    }
                }
                else
                {
                    INTAPS.RDBMS.SQLHelper helper = _parent.GetReaderHelper();
                    try
                    {
                        AccountingPeriod[] prds = INTAPS.Accounting.BDE.AccountingPeriodHelper.GetTouchedPeriods<AccountingPeriod>(helper, pars.groupByPeriod, pars.time1, pars.time2);
                        if (prds.Length == 0)
                            throw new ServerUserMessage("Periods in database don't cover the time range selected");
                        AccountingPeriod currentPrd = prds[0];
                        int prdIndex = 0;
                        double periodDBTotal = 0;
                        double periodCRTotal = 0;
                        foreach (AccountTransaction t in tran)
                        {
                            while (currentPrd != null && currentPrd.toDate <= t.execTime) //reached current date boundary, insert current period summary
                            {
                                if (AccountBase.AmountGreater(periodDBTotal, 0) || AccountBase.AmountGreater(periodCRTotal, 0))
                                {
                                    addTotalRow(body, "Section_Header_1", currentPrd.name + " Totals", periodDBTotal, periodCRTotal);
                                    addEndingBalance(body, "Section_Header_3", currentPrd.name + " Ending Balance", endingBalance);
                                    bERPHtmlTableRow separator = new bERPHtmlTableRow();
                                    bERPHtmlTableCell separatorCell = new bERPHtmlTableCell("", headerRow.cells.Count);
                                    separator.cells.Add(separatorCell);
                                    body.rows.Add(separator);
                                    separator.css = "GroupSeparator";
                                }
                                periodDBTotal = 0;
                                periodCRTotal = 0;
                                prdIndex++;
                                if (prdIndex < prds.Length)
                                    currentPrd = prds[prdIndex];
                                else
                                    currentPrd = null;

                            }
                            if (string.IsNullOrEmpty(pars.groupByPeriod))
                                body.rows.Add(addLedgerRow(body.rows.Count, t, accountColumn, itemColumn, pars.twoColumns));
                            periodDBTotal += AccountBase.AmountGreater(t.Amount, 0) ? t.Amount : 0;
                            periodCRTotal += AccountBase.AmountLess(t.Amount, 0) ? -t.Amount : 0;
                            endingBalance = t.GetNewBalance(pars.asCreditAccount);
                        }
                        while (currentPrd != null && pars.time2 < currentPrd.toDate) //reached current date boundary, insert current period summary
                        {
                            if (AccountBase.AmountGreater(periodDBTotal, 0) || AccountBase.AmountGreater(periodCRTotal, 0))
                            {
                                addTotalRow(body, "Section_Header_1", currentPrd.name + " Totals", periodDBTotal, periodCRTotal);
                                addEndingBalance(body, "Section_Header_3", currentPrd.name + " Ending Balance", endingBalance);
                                bERPHtmlTableRow separator = new bERPHtmlTableRow();
                                bERPHtmlTableCell separatorCell = new bERPHtmlTableCell("", headerRow.cells.Count);
                                separator.cells.Add(separatorCell);
                                body.rows.Add(separator);
                                separator.css = "GroupSeparator";
                                periodDBTotal = 0;
                                periodCRTotal = 0;
                            }
                            prdIndex++;
                            if (prdIndex < prds.Length)
                                currentPrd = prds[prdIndex];
                            else
                                currentPrd = null;
                        }
                    }
                    finally
                    {
                        _parent.ReleaseHelper(helper);
                    }
                }

                bERPHtmlTableRowGroup footer = new bERPHtmlTableRowGroup(TableRowGroupType.footer);
                addEndingBalance(body, "Section_Header_3", "Ending Balance", endingBalance);
                addTotalRow(footer, "Section_Header_1", "Total", total.TotalDebit, total.TotalCredit);

                StringBuilder builder = new StringBuilder();
                table.groups.Add(header);
                table.groups.Add(body);
                table.groups.Add(footer);
                table.build(builder);
                return builder.ToString();
            }

            private void addEndingBalance(bERPHtmlTableRowGroup rows, string css, string label, double endingBalance)
            {
                TDType cellType = TDType.ColHeader;// rows.type == TableRowGroupType.body ? TDType.body : TDType.ColHeader;
                bERPHtmlTableRow row = bERPHtmlBuilder.htmlAddRow(rows,
                    new bERPHtmlTableCell(cellType, label, headerRow.cells.IndexOf(balCell))
                    , new bERPHtmlTableCell(cellType, Account.FormatBalance(endingBalance), "CurrencyCell")
                    );
                row.css = css;
                for (int i = headerRow.cells.IndexOf(balCell) + 1; i < headerRow.cells.Count; i++)
                    row.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, ""));
            }

            private void addTotalRow(bERPHtmlTableRowGroup rows, string css, string label, double totalDb, double totalCr)
            {
                bERPHtmlTableRow totalRow = new bERPHtmlTableRow();
                rows.rows.Add(totalRow);
                totalRow.cells.Add(new bERPHtmlTableCell(
                        rows.type == TableRowGroupType.body ?
                        TDType.body : TDType.ColHeader, label));
                if (pars.twoColumns)
                {
                    int i = 0;
                    foreach (bERPHtmlTableCell headerCell in headerRow.cells)
                    {
                        if (headerCell == dbCell)
                            totalRow.cells.Add(new bERPHtmlTableCell(rows.type == TableRowGroupType.body ? TDType.body : TDType.ColHeader, AccountBase.FormatAmount(totalDb), "CurrencyCell"));
                        else if (headerCell == crCell)
                            totalRow.cells.Add(new bERPHtmlTableCell(rows.type == TableRowGroupType.body ? TDType.body : TDType.ColHeader, AccountBase.FormatAmount(totalCr), "CurrencyCell"));
                        else if (i > 0)
                            totalRow.cells.Add(new bERPHtmlTableCell(rows.type == TableRowGroupType.body ? TDType.body : TDType.ColHeader, ""));
                        i++;
                    }
                }
                else
                {
                    int i = 0;
                    foreach (bERPHtmlTableCell headerCell in headerRow.cells)
                    {
                        if (headerCell == dbCell)
                            totalRow.cells.Add(new bERPHtmlTableCell(rows.type == TableRowGroupType.body ? TDType.body : TDType.ColHeader, AccountBase.FormatAmount(pars.asCreditAccount ? totalCr - totalDb : totalDb - totalCr), "CurrencyCell"));
                        else if (i > 0)
                            totalRow.cells.Add(new bERPHtmlTableCell(rows.type == TableRowGroupType.body ? TDType.body : TDType.ColHeader, ""));
                        i++;
                    }
                }
                totalRow.css = css;
            }

            private bERPHtmlTableRow addLedgerRow(int index, AccountTransaction t, bool accountColumn, bool itemColumn, bool twoColumns)
            {
                CostCenterAccountWithDescription desc = null;
                bERPHtmlTableRow row = new bERPHtmlTableRow();
                bERPHtmlTableCell cell;
                row.cells.Add(new bERPHtmlTableCell(AccountBase.FormatTime(t.execTime)));

                bERPHtmlTableCell cellAmount = null, cellBalance = null;
                if (twoColumns)
                {

                    if (Account.AmountGreater(t.Amount, 0))
                    {
                        row.cells.Add(cellAmount = new bERPHtmlTableCell(AccountBase.FormatAmount(t.Amount)));
                        row.cells.Add(new bERPHtmlTableCell(""));
                    }
                    else if (Account.AmountLess(t.Amount, 0))
                    {
                        row.cells.Add(new bERPHtmlTableCell(""));
                        row.cells.Add(cellAmount = new bERPHtmlTableCell(AccountBase.FormatAmount(-t.Amount)));
                    }
                    else
                    {
                        row.cells.Add(cellAmount = new bERPHtmlTableCell(""));
                        row.cells.Add(new bERPHtmlTableCell(""));
                    }
                }
                else
                    row.cells.Add(cellBalance = new bERPHtmlTableCell((pars.asCreditAccount ? -t.Amount : t.Amount).ToString("#,#0.0")));

                if (cellAmount != null)
                    cellAmount.css = "currencyCell";

                row.css = index % 2 == 0 ? "Even_row" : "Odd_row";

                row.cells.Add(cellBalance = new bERPHtmlTableCell(AccountBase.FormatBalance(t.GetNewBalance(pars.asCreditAccount))));
                cellBalance.css = "currencyCell";

                AccountDocument doc = _parent.Accounting.GetAccountDocument(t.documentID, false);
                if(!pars.printMode)
                    cellAmount.innerHtml = "<a href='" + SummaryInformation.LINK_OPEN_DOC + "?docID=" + t.documentID + "&docTypeID=" + doc.DocumentTypeID + "'>" + cellAmount.innerHtml + "</a>";
                string references = null;
                foreach (DocumentTypedReference r in _parent.m_accounting.getAllDocumentReferences(doc.AccountDocumentID))
                {
                    string refhtml = HttpUtility.HtmlEncode(_parent.m_accounting.getDocumentSerialType(r.typeID).prefix + ":" + r.reference);
                    if (references == null)
                        references = refhtml;
                    else
                        references += "<br/>" + refhtml;
                }
                row.cells.Add(cell = new bERPHtmlTableCell(""));
                cell.innerHtml = references;

                DocumentType dt = _parent.Accounting.GetDocumentTypeByID(doc.DocumentTypeID);
                Type docType = dt.GetTypeObject();
                string remark;
                if (pars.remarkDetail == LedgerParameters.LedgerRemarkDetail.Concise)
                    remark = doc.ShortDescription;
                else
                {
                    remark = dt.name;
                    if (!string.IsNullOrEmpty(doc.PaperRef))
                    {
                        if (string.IsNullOrEmpty(remark))
                            remark = "Ref:" + doc.PaperRef;
                        else
                            remark += "\nRef:" + doc.PaperRef;
                    }
                    if (!string.IsNullOrEmpty(doc.ShortDescription))
                    {
                        if (string.IsNullOrEmpty(remark))
                            remark = doc.ShortDescription;
                        else
                            remark += "\n" + doc.ShortDescription;
                    }
                    if (!string.IsNullOrEmpty(t.Note))
                    {
                        if (string.IsNullOrEmpty(remark))
                            remark = t.Note;
                        else
                            remark += "\n" + t.Note;
                    }
                    if (docType.GetInterface("IStandardAttributeDocument") != null)
                    {
                        doc = _parent.Accounting.GetAccountDocument(t.documentID, true);
                        IStandardAttributeDocument atr = doc as IStandardAttributeDocument;
                        if (atr.hasInstrument)
                        {
                            if (string.IsNullOrEmpty(remark))
                                remark = doc.ShortDescription;
                            else
                                remark += "\n" + atr.getInstrument(true);
                        }
                    }
                }
                row.cells.Add(cell = new bERPHtmlTableCell(remark));
                if(!pars.printMode)
                    cell.innerHtml += string.Format("<br/><a href='{2}?docID={0}&docTypeID={1}'>Edit</a><span style='width:10px'></span><a href='{3}?docID={0}'>Delete</a><span style='width:10px'></span><a href='{4}?docID={0}'>Entries</a>", doc.AccountDocumentID, doc.DocumentTypeID, SummaryInformation.LINK_OPEN_DOC, SummaryInformation.LINK_DELETE_DOC, SummaryInformation.LINK_VIEW_ENTRIES);
                if (accountColumn)
                {
                    desc = _parent.Accounting.GetCostCenterAccountWithDescription(t.AccountID);
                    row.cells.Add(new bERPHtmlTableCell(desc.NameCode));
                }

                return row;
            }

        }


        class StockCardGenerator
        {
            const int MAX_LDEGER_TRANS = 5000;
            int csAccountID;
            iERPTransactionBDE _parent;
            LedgerParameters _pars;
            public StockCardGenerator(iERPTransactionBDE parent, LedgerParameters pars)
            {
                _pars = pars;
                CostCenterAccount csa = parent.Accounting.GetCostCenterAccount(pars.costCenterID, pars.accountIDs[0]);
                _parent = parent;
                this.csAccountID = csa.id;

            }



            bERPHtmlTableRowGroup header;
            bERPHtmlTableRow headerRow;
            bERPHtmlTableRow begRow;
            CostCenterAccountWithDescription resultAccount;
            Dictionary<int, CostCenterAccountWithDescription> accounts;
            public string generate()
            {
                int N;

                int pageSize = MAX_LDEGER_TRANS;
                INTAPS.RDBMS.SQLHelper reader = _parent.Accounting.GetReaderHelper();
                AccountTransaction[] tran = null;

                try
                {
                    resultAccount = _parent.Accounting.GetCostCenterAccountWithDescription(csAccountID);
                    if (resultAccount == null)
                        tran = new AccountTransaction[0];
                    else
                    {
                        string cr=null;
                        if (resultAccount.isControlAccount)
                        {
                            int[] leavAccounts = _parent.Accounting.getLeaveCostCenterAccounts(reader, csAccountID);
                            if (leavAccounts.Length > 0)
                            {
                                string list = null;
                                foreach (int a in leavAccounts)
                                    list = INTAPS.StringExtensions.AppendOperand(list, ",", a.ToString());
                                cr = string.Format("accountID in ({0}) and itemID={1} and tranTicks>={2} and tranTicks<{3}", list, TransactionItem.MATERIAL_QUANTITY, _pars.time1.Ticks, _pars.time2.Ticks);
                            }
                            else
                                cr = null;
                        }

                        if(cr==null)
                            cr = string.Format("accountID={0} and itemID={1} and tranTicks>={2} and tranTicks<{3}", csAccountID, TransactionItem.MATERIAL_QUANTITY, _pars.time1.Ticks, _pars.time2.Ticks);

                        AccountBalance bal;
                        tran = _parent.Accounting.GetTransactionByFilter(reader, cr, 0, pageSize, out N, out bal);
                        if (N > tran.Length)
                            throw new ServerUserMessage("Too many transactions for stock card");
                    }
                }
                finally
                {
                    _parent.Accounting.ReleaseHelper(reader);
                }


                accounts = new Dictionary<int, CostCenterAccountWithDescription>();
                bool accountColumn = true;
                accountColumn = false;
                foreach (AccountTransaction t in tran)
                {
                    if (csAccountID != t.AccountID)
                    {
                        accountColumn = true;
                    }
                }

                bERPHtmlTable table = new bERPHtmlTable();
                header = new bERPHtmlTableRowGroup(TableRowGroupType.header);
                headerRow = new bERPHtmlTableRow();
                header.rows.Add(headerRow);


                if (_pars.qauntityOnly)
                {
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Date", "", 1, 1));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Received", "centeredCell", 1, 1));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Issued", "centeredCell", 1, 1));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Balance", "centeredCell", 1, 1));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "References", "", 1, 1));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Remark", "", 1, 1));
                    if (accountColumn)
                        headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Account", "", 1, 1));
                }
                else
                {
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Date", "", 2, 1));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Debit", "centeredCell", 1, 3));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Credit", "centeredCell", 1, 3));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Balance", "centeredCell", 1, 3));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "References", "", 2, 1));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Remark", "", 2, 1));
                    if (accountColumn)
                        headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Account", "", 2, 1));
                }

                headerRow = new bERPHtmlTableRow();
                header.rows.Add(headerRow);
                if (!_pars.qauntityOnly)
                {
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Quantity", "CurrencyCell"));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Unit Cost", "CurrencyCell"));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Cost", "CurrencyCell"));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Quantity", "CurrencyCell"));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Unit Cost", "CurrencyCell"));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Cost", "CurrencyCell"));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Quantity", "CurrencyCell"));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Unit Cost", "CurrencyCell"));
                    headerRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, "Cost", "CurrencyCell"));
                }
                double begCost = _parent.Accounting.GetNetBalanceAsOf(csAccountID, TransactionItem.DEFAULT_CURRENCY, _pars.time1);
                double begQunatity = _parent.Accounting.GetNetBalanceAsOf(csAccountID, TransactionItem.MATERIAL_QUANTITY, _pars.time1);

                bERPHtmlTableRowGroup body = new bERPHtmlTableRowGroup(TableRowGroupType.body);
                if (_pars.qauntityOnly)
                {
                    begRow = bERPHtmlBuilder.htmlAddRow(body,
                        new bERPHtmlTableCell(TDType.ColHeader, "Beginning Balance", 3)
                        , new bERPHtmlTableCell(TDType.ColHeader, AccountBase.FormatQuantity(begQunatity), "CurrencyCell")
                        );
                }
                else
                {
                    begRow = bERPHtmlBuilder.htmlAddRow(body,
                        new bERPHtmlTableCell(TDType.ColHeader, "Beginning Balance", 7)
                        , new bERPHtmlTableCell(TDType.ColHeader, AccountBase.FormatQuantity(begQunatity), "CurrencyCell")
                        , new bERPHtmlTableCell(TDType.ColHeader, AccountBase.FormatUnitPrice(begQunatity, begCost), "CurrencyCell")
                        , new bERPHtmlTableCell(TDType.ColHeader, AccountBase.FormatAmount(begCost), "CurrencyCell")
                        );
                }
                begRow.css = "Section_Header_3";
                for (int i = 0; i < (accountColumn ? 3 : 2); i++)
                    begRow.cells.Add(new bERPHtmlTableCell(TDType.ColHeader, ""));



                double q = begQunatity;
                double c = begCost;
                double totalDbq = 0;
                double totalCrq = 0;
                double totalDbc = 0;
                double totalCrc = 0;
                foreach (AccountTransaction t in tran)
                {
                    AccountTransaction[] other = _parent.Accounting.GetTransactionsOfDocument(t.documentID);
                    double docPrice = 0;
                    double docQuantity = 0;
                    foreach (AccountTransaction o in other)
                    {
                        if (o.AccountID == t.AccountID)
                        {
                            if (o.ItemID == TransactionItem.DEFAULT_CURRENCY)
                                docPrice += o.Amount;
                            else if (o.ItemID == TransactionItem.MATERIAL_QUANTITY)
                                docQuantity += o.Amount;

                        }
                    }
                    double thisPrice = AccountBase.AmountEqual(docQuantity, 0) ? 0 : docPrice / docQuantity;
                    if (t.Amount > 0)
                    {
                        totalDbq += t.Amount;
                        totalDbc += t.Amount * thisPrice;
                    }
                    else
                    {
                        totalCrq -= t.Amount;
                        totalCrc -= t.Amount * thisPrice;
                    }
                    body.rows.Add(addStockCardRow(body.rows.Count, t, resultAccount.creditAccount, t.Amount, thisPrice, q, c, accountColumn, out q, out c));
                }

                bERPHtmlTableRowGroup footer = new bERPHtmlTableRowGroup(TableRowGroupType.footer);
                addEndingBalance(body, "Section_Header_3", "Ending Balance", q, c, accountColumn);
                //addTotalRow(footer, "Section_Header_1", "Total", totalDbq, totalCrq, totalDbc, totalCrc, accountColumn);

                StringBuilder builder = new StringBuilder();
                table.groups.Add(header);
                table.groups.Add(body);
                table.groups.Add(footer);
                table.build(builder);
                return builder.ToString();
            }

            private void addEndingBalance(bERPHtmlTableRowGroup rows, string css, string label, double endingQuantity, double endingCost, bool accountColumn)
            {
                TDType cellType = rows.type == TableRowGroupType.body ? TDType.body : TDType.ColHeader;
                bERPHtmlTableRow row;
                if (_pars.qauntityOnly)
                {
                    row = bERPHtmlBuilder.htmlAddRow(rows,
                    new bERPHtmlTableCell(cellType, label, 3)
                    , new bERPHtmlTableCell(cellType, Account.FormatBalance(endingQuantity), "CurrencyCell")
                    );
                }
                else
                {
                    row = bERPHtmlBuilder.htmlAddRow(rows,
                    new bERPHtmlTableCell(cellType, label,  7)
                    , new bERPHtmlTableCell(cellType, Account.FormatBalance(endingQuantity), "CurrencyCell")
                    , new bERPHtmlTableCell(cellType, Account.FormatUnitPrice(endingQuantity, endingCost), "CurrencyCell")
                    , new bERPHtmlTableCell(cellType, Account.FormatBalance(endingCost), "CurrencyCell")
                    );
                }
                row.cells.Add(new bERPHtmlTableCell(""));//refernce
                row.cells.Add(new bERPHtmlTableCell(""));//Remark
                if (accountColumn)
                    row.cells.Add(new bERPHtmlTableCell(""));//account

                row.css = css;
            }

            private void addTotalRow(bERPHtmlTableRowGroup rows, string css, string label, double totalDbq, double totalCrq, double totalDbc, double totalCrc, bool accountColumn)
            {
                bERPHtmlTableRow totalRow = new bERPHtmlTableRow();
                rows.rows.Add(totalRow);
                totalRow.cells.Add(new bERPHtmlTableCell(
                        rows.type == TableRowGroupType.body ?
                        TDType.body : TDType.ColHeader, label));
                if (_pars.qauntityOnly)
                {
                    totalRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatQuantity(totalDbq)));
                    totalRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatQuantity(totalCrq)));
                    totalRow.cells.Add(new bERPHtmlTableCell(""));//balance
                    totalRow.cells.Add(new bERPHtmlTableCell(""));//refernce
                    totalRow.cells.Add(new bERPHtmlTableCell(""));//Remark
                    if (accountColumn)
                        totalRow.cells.Add(new bERPHtmlTableCell(""));//account

                }
                else
                {
                    totalRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatQuantity(totalDbq)));
                    totalRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatUnitPrice(totalDbq, totalDbc)));
                    totalRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatAmount(totalDbc)));
                    totalRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatQuantity(totalCrq)));
                    totalRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatUnitPrice(totalCrq, totalCrc)));
                    totalRow.cells.Add(new bERPHtmlTableCell(AccountBase.FormatAmount(totalCrc)));

                    totalRow.cells.Add(new bERPHtmlTableCell(""));//balance
                    totalRow.cells.Add(new bERPHtmlTableCell(""));//balance
                    totalRow.cells.Add(new bERPHtmlTableCell(""));//refernce
                    totalRow.cells.Add(new bERPHtmlTableCell(""));//Remark
                    if (accountColumn)
                        totalRow.cells.Add(new bERPHtmlTableCell(""));//account
                }
                totalRow.css = css;
            }

            private bERPHtmlTableRow addStockCardRow(int index, AccountTransaction t, bool creditAcccount, double q, double unitPrice, double prevQBalance, double prevCBalance, bool accountColumn, out double qbalance, out double cbalance)
            {
                CostCenterAccountWithDescription desc = null;
                bERPHtmlTableRow row = new bERPHtmlTableRow();
                bERPHtmlTableCell cell;
                row.cells.Add(new bERPHtmlTableCell(AccountBase.FormatTime(t.execTime)));

                bERPHtmlTableCell cellQuantity = null, cellBalanceQuantity = null;
                bERPHtmlTableCell cellUnitCost = null, cellUnitCostQuantity=null;
                bERPHtmlTableCell cellCost = null, cellBalanceCost = null;

                if (Account.AmountGreater(q, 0))
                {

                    row.cells.Add(cellQuantity = new bERPHtmlTableCell(AccountBase.FormatQuantity(q)));
                    if (!_pars.qauntityOnly)
                    {
                        row.cells.Add(cellUnitCost = new bERPHtmlTableCell(AccountBase.FormatAmount(unitPrice)));
                        row.cells.Add(cellCost = new bERPHtmlTableCell(AccountBase.FormatAmount(q * unitPrice)));
                    }
                    row.cells.Add(new bERPHtmlTableCell(""));
                    if (!_pars.qauntityOnly)
                    {
                        row.cells.Add(new bERPHtmlTableCell(""));
                        row.cells.Add(new bERPHtmlTableCell(""));
                    }
                }
                else if (Account.AmountLess(q, 0))
                {
                    row.cells.Add(new bERPHtmlTableCell(""));
                    if (!_pars.qauntityOnly)
                    {
                        row.cells.Add(new bERPHtmlTableCell(""));
                        row.cells.Add(new bERPHtmlTableCell(""));
                    }
                    row.cells.Add(cellQuantity = new bERPHtmlTableCell(AccountBase.FormatQuantity(-q)));
                    if (!_pars.qauntityOnly)
                    {
                        row.cells.Add(cellUnitCost = new bERPHtmlTableCell(AccountBase.FormatAmount(unitPrice)));
                        row.cells.Add(cellCost = new bERPHtmlTableCell(AccountBase.FormatAmount(-q * unitPrice)));
                    }
                }
                else
                {
                    row.cells.Add(cellQuantity = new bERPHtmlTableCell(""));
                    if (!_pars.qauntityOnly)
                    {
                        row.cells.Add(cellUnitCost = new bERPHtmlTableCell(""));
                        row.cells.Add(cellCost = new bERPHtmlTableCell(""));
                    }
                    row.cells.Add(new bERPHtmlTableCell(""));
                    if (!_pars.qauntityOnly)
                    {
                        row.cells.Add(new bERPHtmlTableCell(""));
                        row.cells.Add(new bERPHtmlTableCell(""));
                    }
                }


                if (cellQuantity != null)
                    cellQuantity.css = "currencyCell";
                if (cellUnitCost != null)
                    cellUnitCost.css = "currencyCell";
                if (cellCost != null)
                    cellCost.css = "currencyCell";
                row.css = index % 2 == 0 ? "Even_row" : "Odd_row";
                if (creditAcccount)
                {
                    qbalance = prevQBalance - q;
                    cbalance = prevCBalance - q * unitPrice;
                }
                else
                {
                    qbalance = prevQBalance + q;
                    cbalance = prevCBalance + q * unitPrice;
                }

                row.cells.Add(cellBalanceQuantity = new bERPHtmlTableCell(AccountBase.FormatQuantity(qbalance)));
                if (!_pars.qauntityOnly)
                {
                    row.cells.Add(cellUnitCostQuantity = new bERPHtmlTableCell(AccountBase.FormatUnitPrice(qbalance, cbalance)));
                    row.cells.Add(cellBalanceCost = new bERPHtmlTableCell(AccountBase.FormatAmount(cbalance)));
                }
                
                if(cellBalanceQuantity!=null) cellBalanceQuantity.css = "currencyCell";
                if (cellUnitCostQuantity != null) cellUnitCostQuantity.css = "currencyCell";
                if (cellBalanceCost != null) cellBalanceCost.css = "currencyCell";

                AccountDocument doc = _parent.Accounting.GetAccountDocument(t.documentID, false);
                //cellQuantity.innerHtml = "<a href='" + SummaryInformation.LINK_OPEN_DOC + "?docID=" + t.documentID + "&docTypeID=" + doc.DocumentTypeID + "'>" + cellQuantity.innerHtml + "</a>";

                string references = null;
                foreach (DocumentTypedReference r in _parent.m_accounting.getAllDocumentReferences(doc.AccountDocumentID))
                {
                    string refhtml = HttpUtility.HtmlEncode(_parent.m_accounting.getDocumentSerialType(r.typeID).prefix + ":" + r.reference);
                    if (references == null)
                        references = refhtml;
                    else
                        references += "<br/>" + refhtml;
                }
                row.cells.Add(cell = new bERPHtmlTableCell(""));
                cell.innerHtml = references;

                DocumentType dt = _parent.Accounting.GetDocumentTypeByID(doc.DocumentTypeID);
                Type docType = dt.GetTypeObject();
                string remark = dt.name;
                if (!string.IsNullOrEmpty(doc.PaperRef))
                {
                    if (string.IsNullOrEmpty(remark))
                        remark = "Ref:" + doc.PaperRef;
                    else
                        remark += "\nRef:" + doc.PaperRef;
                }
                if (!string.IsNullOrEmpty(doc.ShortDescription))
                {
                    if (string.IsNullOrEmpty(remark))
                        remark = doc.ShortDescription;
                    else
                        remark += "\n" + doc.ShortDescription;
                }
                if (!string.IsNullOrEmpty(t.Note))
                {
                    if (string.IsNullOrEmpty(remark))
                        remark = t.Note;
                    else
                        remark += "\n" + t.Note;
                }
                if (docType.GetInterface("IStandardAttributeDocument") != null)
                {
                    doc = _parent.Accounting.GetAccountDocument(t.documentID, true);
                    IStandardAttributeDocument atr = doc as IStandardAttributeDocument;
                    if (atr.hasInstrument)
                    {
                        if (string.IsNullOrEmpty(remark))
                            remark = doc.ShortDescription;
                        else
                            remark += "\n" + atr.getInstrument(true);
                    }
                }

                row.cells.Add(cell = new bERPHtmlTableCell(remark));
                if ((_pars.remarkDetail & LedgerParameters.LedgerRemarkDetail.Link) == LedgerParameters.LedgerRemarkDetail.Link)
                    cell.innerHtml += string.Format("<br/><a href='{2}?docID={0}&docTypeID={1}'>Edit</a><span style='width:10px'></span><a href='{3}?docID={0}'>Delete</a>", doc.AccountDocumentID, doc.DocumentTypeID, SummaryInformation.LINK_OPEN_DOC, SummaryInformation.LINK_DELETE_DOC);
                if (accountColumn)
                {
                    desc = _parent.Accounting.GetCostCenterAccountWithDescription(t.AccountID);
                    row.cells.Add(new bERPHtmlTableCell(desc.NameCode));
                }

                return row;
            }
        }
        string[] generateAccountCodeByPattern(INTAPS.RDBMS.SQLHelper reader, string code)
        {
            return reader.GetColumnArray<String>(string.Format("Select code from {0}.dbo.Account where code like '{1}' order by code", this.Accounting.DBName, code.Replace("'","''")));
        }
        string[] generateAccountCodeByRange(INTAPS.RDBMS.SQLHelper reader, string from,string to)
        {
            if (string.IsNullOrEmpty(to))
                return generateAccountCodeByPattern(reader, from);
            return reader.GetColumnArray<String>(string.Format("Select code from {0}.dbo.Account where code>='{1}' and code<='{2}' order by code",this.Accounting.DBName, INTAPS.RDBMS.SQLHelper.EscapeString(from), INTAPS.RDBMS.SQLHelper.EscapeString(to)));
        }
        internal int[]  expandAccounts(INTAPS.RDBMS.SQLHelper reader, int[] roots)
        {
            HashSet<int> accounts = new HashSet<int>();
            foreach(int r in roots)
            {
                int N;
                foreach (Account ch in Accounting.GetChildAcccount<Account>(r, 0, -1, out N))
                {
                    if (!accounts.Contains(ch.id))
                        accounts.Add(ch.id);
                }
            }
            accounts.RemoveWhere(x => INTAPS.ArrayExtension.contains(roots, x));
            int [] ret=new int[accounts.Count];
            accounts.CopyTo(ret);
            return ret;
        }
        internal int[] generateAccounts(INTAPS.RDBMS.SQLHelper reader, string includeFrom, string includeTo, string excludeFrom, string excludeTo)
        {
            List<string> codes = new List<string>();
            codes.AddRange(generateAccountCodeByRange(reader,includeFrom, includeTo));
            if (!string.IsNullOrEmpty(excludeFrom))
            {
                String[] excl=generateAccountCodeByRange(reader,excludeFrom,excludeTo);
                codes.RemoveAll(x => INTAPS.ArrayExtension.contains(excl, x));
            }
            int[] ids = new int[codes.Count];
            for (int i = 0; i < ids.Length; i++)
                ids[i] = Accounting.GetAccountID<Account>(codes[i]);

            List<int> minimum = new List<int>();
            for (int i = 0; i < ids.Length ; i++)
            {
                bool isControl = false;
                for (int j = 0; j < ids.Length; j++)
                {
                    if (i == j)
                        continue;
                    if (Accounting.IsControlOf<Account>(ids[i], ids[j]))
                    {
                        isControl = true;
                        break;
                    }
                }
                if (isControl)
                    continue;
                minimum.Add(ids[i]);
            }
            return minimum.ToArray(); 
        }
        public string getTradeRelationTransactions(string relationCode,long ticksFrom, long ticksTo,int[] docTypes)
        {
            return new TradeRelationLedgerGenerator().getTradeRelationTransactions(this, relationCode, ticksFrom, ticksTo, null);    
        }
        
    }
}
