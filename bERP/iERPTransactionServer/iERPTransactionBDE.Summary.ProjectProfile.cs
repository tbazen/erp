﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        class CostCenterProfileGenerator
        {
            DateTime _time1;
            DateTime _time2;
            iERPTransactionBDE _parent;
            CostCenter _costCenter;
            string _html;

            public string Html
            {
                get { return _html; }
                set { _html = value; }
            }
            StringBuilder _builder;
            public CostCenterProfileGenerator(iERPTransactionBDE parent, int costCenterID, DateTime time1, DateTime time2)
            {
                _parent = parent;
                _time1 = time1;
                _time2 = time2;
                _costCenter = parent.Accounting.GetAccount<CostCenter>(costCenterID);
                if (_costCenter == null)
                {
                    _html = "<h1>Select Accounting Center</h1>";
                    return;
                }
                _builder = new StringBuilder();
                buildCashAndStore();
                _html = _builder.ToString();
            }

            private void buildCashAndStore()
            {
                CashAccount[] cashAccounts = _parent.GetCashAccountsByCostCenter(_costCenter.id, true);
                BankAccountInfo[] bankAccounts = _parent.GetBankAccountByCostCenter(_costCenter.id);
                StoreInfo[] stores = _parent.GetStoresByCostCenterID(_costCenter.id);
                double totalCash = 0;
                double totalInBank = 0;
                double totalInventory = 0;
                bERPHtmlTable cashTable = new bERPHtmlTable();
                bERPHtmlTableRowGroup cashBody = cashTable.createBodyGroup();

                bERPHtmlTable bankTable = new bERPHtmlTable();
                bERPHtmlTableRowGroup bankBody = bankTable.createBodyGroup();


                bERPHtmlTable storeTable = new bERPHtmlTable();
                bERPHtmlTableRowGroup storeBody = storeTable.createBodyGroup();

                if (cashAccounts.Length > 0)
                {
                    foreach (CashAccount ca in cashAccounts)
                    {
                        double bal = _parent.Accounting.GetNetBalanceAsOf(ca.csAccountID, TransactionItem.DEFAULT_CURRENCY, _time2);
                        if (AccountBase.AmountEqual(bal, 0) && ca.isStaffExpenseAdvance)
                            continue;
                        cashBody.htmlAddRow(new bERPHtmlTableCell(ca.nameCode)
                        , new bERPHtmlTableCell(AccountBase.FormatBalance(bal), "CurrencyCell")
                        ).css = cashBody.rows.Count % 2 == 0 ? "Even_Row" : "Odd_row";
                        totalCash += bal;
                    }
                    if (cashBody.rows.Count != 0)
                    {
                        cashBody.htmlAddRow(0, new bERPHtmlTableCell("Cash on Hand", 2)).css = "Section_Header_1";
                        if (cashBody.rows.Count > 2)
                        {
                            cashBody.htmlAddRow(new bERPHtmlTableCell("Total in Cash on Hand", "CurrencyCell")
                            , new bERPHtmlTableCell(AccountBase.FormatBalance(totalCash), "CurrencyCell")
                                ).css = "Section_Header_1";
                        }
                    }
                }
                if (bankAccounts.Length > 0)
                {

                    foreach (BankAccountInfo ba in bankAccounts)
                    {
                        double bal = _parent.Accounting.GetNetBalanceAsOf(ba.mainCsAccount, TransactionItem.DEFAULT_CURRENCY, _time2);
                        bankBody.htmlAddRow(new bERPHtmlTableCell(ba.ToString())
                        , new bERPHtmlTableCell(AccountBase.FormatBalance(bal), "CurrencyCell")
                            ).css = bankBody.rows.Count % 2 == 0 ? "Even_Row" : "Odd_row";
                        totalInBank += bal;
                    }
                    if (bankBody.rows.Count != 0)
                    {
                        bankBody.htmlAddRow(0, new bERPHtmlTableCell("Cash in Bank", 2)).css = "Section_Header_1"; ;
                        if (bankBody.rows.Count > 2)
                        {
                            bankBody.htmlAddRow(new bERPHtmlTableCell("Total in in Bank", "CurrencyCell")
                            , new bERPHtmlTableCell(AccountBase.FormatBalance(totalInBank), "CurrencyCell")
                                ).css = "Section_Header_1";
                        }
                    }
                }
                if (stores.Length > 0)
                {

                    foreach (StoreInfo st in stores)
                    {
                        double bal = _parent.Accounting.GetNetCostCenterAccountBalanceAsOf(st.costCenterID
                            , _parent.SysPars.assetAccountID
                            , TransactionItem.DEFAULT_CURRENCY, _time2);
                        storeBody.htmlAddRow(new bERPHtmlTableCell(st.nameCode)
                        , new bERPHtmlTableCell(AccountBase.FormatBalance(bal), "CurrencyCell")
                            ).css = storeBody.rows.Count % 2 == 0 ? "Even_Row" : "Odd_row";
                        totalInventory += bal;
                    }

                    if (storeBody.rows.Count != 0)
                    {
                        storeBody.htmlAddRow(0, new bERPHtmlTableCell("Value of Inventory in Stores", 2)).css = "Section_Header_1"; ;
                        if (storeBody.rows.Count > 2)
                        {
                            storeBody.htmlAddRow(new bERPHtmlTableCell("Total Value of Inventory in Stores", "CurrencyCell")
                            , new bERPHtmlTableCell(AccountBase.FormatBalance(totalInventory), "CurrencyCell")
                                ).css = "Section_Header_1";
                        }
                    }
                }

                List<bERPHtmlTable> tables = new List<bERPHtmlTable>();
                foreach (bERPHtmlTable t in new bERPHtmlTable[] { cashTable, bankTable, storeTable })
                    if (t.groups[0].rows.Count > 0)
                        tables.Add(t);
                if (tables.Count == 0)
                    return;
                _builder.Append("<table style='Width:" + (30*tables.Count) + "%'><tr>");
                foreach (bERPHtmlTable t in tables)
                {
                    _builder.Append("<td>");
                    t.build(_builder);
                    t.styles.Add("Width:30%");
                    _builder.Append("</td>");
                }
                _builder.Append("</tr></table>");
            }

        }
    }
}