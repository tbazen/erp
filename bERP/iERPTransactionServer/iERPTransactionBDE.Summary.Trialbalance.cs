﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using System.Web;
using INTAPS.Accounting.BDE;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        // BDE exposed
        public string getTrialBalance<AgregateType, DetailType>(List<int> expandedAccount,List<int>rootAccounts, AgregateType agregateTo, DateTime date, bool twoColumns)
            where AgregateType : AccountBase, new()
            where DetailType : AccountBase, new()
        {
            TrailBalanceGenerator<AgregateType,DetailType> gen = new TrailBalanceGenerator<AgregateType,DetailType>(m_accounting, this,expandedAccount,rootAccounts, agregateTo,date,twoColumns);
            return gen.GenerateTrialBalanceLinear();
        }

        public class TrailBalanceGenerator<AgregateType, DetailType>
            where AgregateType : AccountBase, new()
            where DetailType : AccountBase, new()
        {
            AccountingBDE bdeAccounting;
            iERPTransactionBDE bde;
            DateTime date;
            List<int> expanded;
            List<int> rootAccounts;
            AgregateType agregateTo;
            bool twoColumns = true;
            public TrailBalanceGenerator(AccountingBDE bdeAccounting,
            iERPTransactionBDE bde,
                List<int>   expanded,
                List<int>   rootAccounts,
                AgregateType agreagetTo,
                DateTime date,
                bool twoColumns)
            {
                this.bdeAccounting = bdeAccounting;
                this.bde = bde;
                this.date = date;
                this.expanded = expanded;
                this.rootAccounts = rootAccounts;
                this.agregateTo = agreagetTo;
            }
            const string BIRR_FORMAT = "#,#0.00";
            const string DATE_FORMAT = "dd/MM/yyyy hh:mm";
            CostCenterAccount getCostCenterAccount(AgregateType aval, DetailType dval)                
            {
                if (typeof(AgregateType) == typeof(Account))
                {
                    return bdeAccounting.GetCostCenterAccount((dval as CostCenter).id, (aval as Account).id);
                }
                return bdeAccounting.GetCostCenterAccount((aval as CostCenter).id, (dval as Account).id);
            }

            public string GenerateTrialBalanceLinear()
            {
                if (agregateTo == null)
                {
                    throw new ServerUserMessage("Please select " + AccountBase.GetAccountTypeDescription<AgregateType>(false));
                }
                StringBuilder builder = new StringBuilder();
                bERPHtmlTable table = buildLinearTrialBalance();
                table.build(builder);
                return builder.ToString();
            }

           


            private bERPHtmlTable buildLinearTrialBalance()
            {
                bERPHtmlTable table = new bERPHtmlTable();
                bERPHtmlTableRowGroup header = new bERPHtmlTableRowGroup(TableRowGroupType.header);
                table.groups.Add(header);
                if (twoColumns)
                {
                    bERPHtmlBuilder.htmlAddRow(header,
                        new bERPHtmlTableCell(TDType.ColHeader, "Code")
                        , new bERPHtmlTableCell(TDType.ColHeader, "Name")
                        , new bERPHtmlTableCell(TDType.ColHeader, "Debit")
                        , new bERPHtmlTableCell(TDType.ColHeader, "Credit")
                    );
                }
                else
                    bERPHtmlBuilder.htmlAddRow(header,
                        new bERPHtmlTableCell(TDType.ColHeader, "Code")
                        , new bERPHtmlTableCell(TDType.ColHeader, "Name")
                        , new bERPHtmlTableCell(TDType.ColHeader, "Balance")
                    );

                List<int> levels = new List<int>();
                List<bool> leaf = new List<bool>();
                bERPHtmlTableRowGroup body = new bERPHtmlTableRowGroup(TableRowGroupType.body);
                table.groups.Add(body);
                double total1=0, total2=0;
                    CreateAccountRows(body, -1, agregateTo
                        , twoColumns, 0, levels, leaf, 0, 0, out total1, out total2);
                int maxLevel = 0;
                foreach (int l in levels)
                    if (l > maxLevel)
                        maxLevel = l;
                if (maxLevel > 0)
                {
                    header.rows[0].cells[1].colSpan = maxLevel + 1;
                    int i = 0;
                    foreach (bERPHtmlTableRow row in body.rows)
                    {
                        if (leaf[i])
                            row.cells[1].colSpan = maxLevel - levels[i] + 1;
                        else if (twoColumns)
                            row.cells[1].colSpan = maxLevel - levels[i] + 3;
                        else
                            row.cells[1].colSpan = maxLevel - levels[i] + 2;
                        for (int k = 0; k < levels[i]; k++)
                        {
                            row.cells.Insert(1, new bERPHtmlTableCell("", "IndentCell"));
                        }
                        i++;
                    }
                }
                bERPHtmlTableRowGroup footer = new bERPHtmlTableRowGroup(TableRowGroupType.footer);
                table.groups.Add(footer);
                bERPHtmlTableCell cellTotal1, cellTotal2;
                if (twoColumns)
                {
                    bERPHtmlBuilder.htmlAddRow(footer,
                        new bERPHtmlTableCell(TDType.ColHeader, "Total", 2 + maxLevel)
                        , cellTotal1 = new bERPHtmlTableCell(TDType.ColHeader, total1.ToString(BIRR_FORMAT))
                        , cellTotal2 = new bERPHtmlTableCell(TDType.ColHeader, total2.ToString(BIRR_FORMAT))
                    );
                    cellTotal2.styles.Add("text-align:right");
                }
                else
                    bERPHtmlBuilder.htmlAddRow(footer,
                        cellTotal1 = new bERPHtmlTableCell(TDType.ColHeader, "Total", 2 + maxLevel)
                        , new bERPHtmlTableCell(TDType.ColHeader, total1.ToString(BIRR_FORMAT))
                    );
                cellTotal1.styles.Add("text-align:right");
                return table;
            }

            void CreateAccountRows(bERPHtmlTableRowGroup rows, int PID, AgregateType agregateTo
                , bool twoColumns, int level, List<int> levels, List<bool> leaf
                , double total1In, double total2In, out double total1out, out double total2out
                )
                
            {
                
                int rowIndex = 0;
                total1out = total1In;
                total2out = total2In;
                int N;
               
                    foreach (DetailType item in bdeAccounting.GetChildAcccount<DetailType>(PID, 0, -1, out N))
                    {
                        if(PID==-1 && !rootAccounts.Contains(item.id)) continue;
                        CostCenterAccount csa = getCostCenterAccount(agregateTo, item);
                        if (csa == null)
                            continue;
                    AccountBalance bal = bdeAccounting.GetBalanceAsOf(csa.id, 0, date);
                    //AccountBalance bal = bdeAccounting.GetBalanceAsOfH(csa.id, 0, date);
                    if (bal.IsZero)
                            continue;

                        levels.Add(level);
                        bERPHtmlTableCell cell;
                        if (item.childCount == 0 || !expanded.Contains(item.id))
                        {

                            bERPHtmlTableRow row = bERPHtmlBuilder.htmlAddRow(rows
                                , new bERPHtmlTableCell(item.Code)
                                , new bERPHtmlTableCell(item.Name)
                                );
                            leaf.Add(true);
                            if (twoColumns)
                            {
                                if (csa.creditAccount)
                                {
                                    total2out += bal.GetBalance(true);
                                    row.cells.Add(new bERPHtmlTableCell(""));
                                    row.cells.Add(cell = new bERPHtmlTableCell(bal.GetBalance(true).ToString(BIRR_FORMAT)));
                                }
                                else
                                {
                                    total1out += bal.GetBalance(false);
                                    row.cells.Add(cell = new bERPHtmlTableCell(bal.GetBalance(false).ToString(BIRR_FORMAT)));
                                    row.cells.Add(new bERPHtmlTableCell(""));
                                }


                            }
                            else
                            {
                                total1out += bal.GetBalance(csa.creditAccount);
                                row.cells.Add(cell = new bERPHtmlTableCell(bal.GetBalance(csa.creditAccount).ToString(BIRR_FORMAT)));
                            }
                            cell.styles.Add("text-align:right");
                            row.css = rowIndex % 2 == 0 ? "Even_row" : "Odd_row";
                            rowIndex++;
                        }
                        else
                        {
                            leaf.Add(false);
                            bERPHtmlTableRow row = bERPHtmlBuilder.htmlAddRow(rows
                                , new bERPHtmlTableCell(item.Code)
                                , new bERPHtmlTableCell(item.Name)
                                );
                            row.css = "Section_Header_1";
                            CreateAccountRows(rows, item.id, agregateTo, twoColumns, level + 1, levels, leaf
                                , total1out, total2out, out total1out, out total2out
                                );
                        }
                    }
                
            }

            private bool _isITEMRootAccount(int accountID)
            {
                int summarizationItemAccount = bde.SysPars.summerizationContraAccountID;
                Account rootITEMAccount = bdeAccounting.GetAccount<Account>(summarizationItemAccount);
                if (rootITEMAccount != null)
                {
                    if (accountID == rootITEMAccount.PID)
                        return true;
                }
                return false;
            }
        }
    }
}
