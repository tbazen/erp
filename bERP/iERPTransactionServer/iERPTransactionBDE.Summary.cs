using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using System.Data.SqlClient;
using System.Data;
using BIZNET.iERP.TypedDataSets;
using INTAPS.Evaluator;
using System.Web;
using INTAPS;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE : INTAPS.ClientServer.BDEBase
    {
        class iEPRDBReadAdapter
        {
            iERPTransactionBDE berp;
            INTAPS.RDBMS.SQLHelper _berpReader = null;
            INTAPS.RDBMS.SQLHelper _accountingReader = null;
            CachedObject<int, Account> accountCache = null;
            CachedObject<int, CostCenter> costCenterCache= null;
            CachedObject<int, CostCenterAccount> costCenterAccountCache= null;
            public iEPRDBReadAdapter(iERPTransactionBDE berp)
            {
                this.berp = berp;
            }
            public INTAPS.RDBMS.SQLHelper accountingReader
            {
                get
                {
                    if (_accountingReader == null)
                        _accountingReader = berp.Accounting.GetReaderHelper();
                    return _accountingReader;
                }
            }
            public INTAPS.RDBMS.SQLHelper berpReader
            {
                get
                {
                    if (_berpReader == null)
                        _berpReader = berp.GetReaderHelper();
                    return _berpReader;
                }
            }
            public void releaseReaders()
            {
                if (_accountingReader != null)
                    berp.Accounting.ReleaseHelper(_accountingReader);
                if (_berpReader != null)
                    berp.ReleaseHelper(_berpReader);
            }

        }
        public delegate AccountBalance EvaluteSummeryItemDelegate(int costCenterID, int accountID, int itemID, DateTime time1, DateTime time2, bool flow);
        public delegate AccountBalance EvaluteSummeryItemByItemDelegate(SummaryItemInfo tag);

        public string renderCostCenterProfile(int costCenterID, DateTime time1,DateTime time2)
        {
            return new CostCenterProfileGenerator(this, costCenterID, time1,time2).Html;
        }
        public string renderInventoryTable(InventoryGeneratorParameters pars)
        {
            return new InventorySheetGenerator(this, pars).GenertedHTML;
        }

        //public string renderInventoryTable(int costCenterID, DateTime to,bool separateOrder,bool includeTitle)
        //{
        //    return new InventorySheetGenerator(this, costCenterID, to, separateOrder,includeTitle).GenertedHTML;
        //}
        //public string renderInventoryTable(int costCenterID, DateTime to, bool separateOrder)
        //{
        //    return new InventorySheetGenerator(this, costCenterID, to, separateOrder, true).GenertedHTML;
        //}
        
        public string renderLedgerTable(LedgerParameters pars)
        {
            if (pars.itemID == TransactionItem.MATERIAL_QUANTITY)
            {
                if (pars.accountIDs == null || pars.accountIDs.Length != 1)
                    throw new ServerUserMessage("Only single account expected for stock card");
                return new StockCardGenerator(this, pars).generate();
            }
            else
            {
                if (pars.merged)
                {
                    if (pars.accountIDs == null)
                    {
                        
                        INTAPS.RDBMS.SQLHelper reader = this.GetReaderHelper();
                        try
                        {
                            if (pars.expandAccounts == null)
                                pars.accountIDs = this.generateAccounts(reader, pars.includeAccountFrom, pars.includAccountTo, pars.excludeAccountFrom, pars.excludeAccountTo);
                            else
                            {
                                int[] accounts = this.expandAccounts(reader, pars.expandAccounts);
                                pars.accountIDs = new int[accounts.Length];
                                for (int i = 0; i < accounts.Length; i++)
                                    pars.accountIDs[i] = Accounting.GetCostCenterAccountID(pars.costCenterID, accounts[i]);
                            }
                        }
                        finally
                        {
                            this.ReleaseHelper(reader);
                        }
                    }
                    if (pars.accountIDs.Length == 1)
                    {
                        Account ac = this.Accounting.GetAccount<Account>(pars.accountIDs[0]);
                        pars.asCreditAccount = ac.CreditAccount;
                    }
                    bool hasLedger, hasBalance;
                    return new LedgerGenerator(this, pars).generate(out hasBalance,out hasLedger);
                }
                else
                {
                    StringBuilder ret = new StringBuilder();
                    int[] accounts;
                    if (pars.accountIDs == null)
                    {
                        INTAPS.RDBMS.SQLHelper reader = this.GetReaderHelper();
                        try
                        {
                            if (pars.expandAccounts == null)
                                accounts = this.generateAccounts(reader, pars.includeAccountFrom, pars.includAccountTo, pars.excludeAccountFrom, pars.excludeAccountTo);
                            else
                            {
                                accounts = this.expandAccounts(reader, pars.expandAccounts);
                            }
                        }
                        finally
                        {
                            this.ReleaseHelper(reader);
                        }
                    }
                    else
                    {
                        accounts = pars.accountIDs;
                    }
                    foreach(int ac in accounts)
                    {
                        Account account = this.Accounting.GetAccount<Account>(ac);
                        pars.asCreditAccount = account.CreditAccount;
                        pars.accountIDs = new int[] { ac };
                        bool hasLedger, hasBalance;
                        string table = new LedgerGenerator(this, pars).generate(out hasBalance, out hasLedger);
                        if ((pars.excludeEmptyBalance && !hasBalance) || (pars.excludeEmptyLedger && !hasLedger))
                            continue;
                        ret.Append(string.Format("<br/><br/><span class='SubTitle'><b>{0}</b><u>{1}</u></span><br/>", HttpUtility.HtmlEncode("Account: "), HttpUtility.HtmlEncode(
                            account.CodeName
                            )));
                        ret.Append(table);
                    }
                    return ret.ToString();
                }
            }
        }
        
        //public string renderLedgerTable(bool asCreditAccount, int[] csAccountID, int itemID, DateTime time1, DateTime time2, string groupByPeriod, bool hideAccountColumnIfUniformAccount, bool hideItemColumnIfUniformItem, bool twoColumns)
        //{
        //    if (itemID == TransactionItem.MATERIAL_QUANTITY)
        //        return "<h1>Not Supported</h1>";
        //    return new LedgerGenerator(this, asCreditAccount, csAccountID, itemID, time1, time2, groupByPeriod, hideAccountColumnIfUniformAccount, hideItemColumnIfUniformItem, twoColumns).generate();
        //}
        
        public string renderSummaryTable(string summarySettingField, int costCenterID,DateTime time1, DateTime time2, out int maxLevel)
        {
            try
            {
                SummaryInformation editedSummaryInformation = GetSystemParameters(new string[] { summarySettingField })[0] as SummaryInformation;
                return renderSummaryTable(editedSummaryInformation,costCenterID, time1, time2, out maxLevel);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException("Error generating summary", ex);
                throw ex;
            }

        }
        public SummaryValueSection evaluateSummaryTable(SummaryInformation summaryInfo, int costCenterID, DateTime time1, DateTime time2, bool flow)
        {
            return evaluateSummarySection(summaryInfo.rootSection, costCenterID, time1, time2, flow
                , new EvaluteSummeryItemDelegate(delegate(int costCenerID, int accountID, int itemID, DateTime t1, DateTime t2, bool f)
                    {
                        AccountBalance bal = f ? m_accounting.GetBalanceAsOf(costCenterID, accountID, itemID, t2)
                        : m_accounting.GetFlow(costCenterID, accountID, itemID, t1, t2);
                        return bal;
                    }
                    )
                );
        }
        public SummaryValueSection evaluateSummaryTableByItem(SummaryInformation summaryInfo, EvaluteSummeryItemByItemDelegate evalDelegate)
        {
            return evaluateSummarySectionByItem(summaryInfo.rootSection, evalDelegate);
        }
        SummaryValueSection evaluateSummarySectionByItem(SummarySection section, EvaluteSummeryItemByItemDelegate evaluateDelegate)
        {
            SummaryValueSection ret = new SummaryValueSection();
            ret.label = section.label;

            AccountBalance childTotoal = new AccountBalance();
            foreach (SummaryItemBase item in section.summaryItem)
            {
                if (item is SummarySection)
                {
                    SummaryValueSection sv = evaluateSummarySectionByItem((SummarySection)item, evaluateDelegate);
                    if (AccountBase.AmountEqual(sv.accountBalance.DebitBalance, 0))
                        if (item.removeIfZero)
                        {
                            continue;
                        }
                    ret.childs.Add(sv);
                    childTotoal.AddBalance(sv.accountBalance);
                }
                else
                {
                    SummaryItemInfo valItem = (SummaryItemInfo)item;
                    AccountBalance itemTotal = evaluateDelegate(valItem);
                    if (AccountBase.AmountEqual(itemTotal.DebitBalance, 0))
                        if (valItem.removeIfZero)
                        {
                            continue;
                        }
                    childTotoal.AddBalance(itemTotal);
                    SummaryValueItem val = new SummaryValueItem();
                    val.accountBalance = itemTotal;
                    val.displayStyle = valItem.format;
                    val.label = valItem.label;
                    val.tag = valItem.tag;
                    val.creditBalance = valItem.credit;

                    ret.childs.Add(val);
                }
            }
            ret.accountBalance = childTotoal;
            ret.displayStyle = section.format;
            ret.label = section.label;
            ret.tag = section.tag;
            ret.creditBalance = section.credit;
            return ret;
        }
        class SummaryEvaluator : ISymbolProvider
        {
            public int costCenterID;
            public DateTime time1, time2;
            public bool flow;
            public EvaluteSummeryItemDelegate evaluateDelegate;
            public SummaryInformation sumInfo;


            Dictionary<SummaryItemBase, SummaryValueItem> values = new Dictionary<SummaryItemBase, SummaryValueItem>();
            public SummaryValueItem getValue(SummaryItemBase sItem)
            {
                return getValue(new HashSet<SummaryItemBase>(), sItem);
            }
            public SummaryValueItem getValue(HashSet<SummaryItemBase> stack, SummaryItemBase sItem)
            {
                if (stack.Contains(sItem))
                    throw new ServerUserMessage("Circular reference: {0}", sItem.tag);

                SummaryValueItem test;
                if (values.TryGetValue(sItem, out test))
                    return test;
                stack.Add(sItem);
                try
                {
                    if (sItem is SummarySection)
                    {
                        SummarySection section = (SummarySection)sItem;
                        SummaryValueSection ret = new SummaryValueSection();
                        AccountBalance childTotoal = new AccountBalance();
                        foreach (SummaryItemBase item in section.summaryItem)
                        {
                            SummaryValueItem valItem = getValue(stack, item);
                            ret.childs.Add(valItem);
                            childTotoal.AddBalance(valItem.accountBalance);
                        }
                        
                        ret.accountBalance = childTotoal;
                        ret.displayStyle = section.format;
                        ret.label = section.label;
                        ret.tag = section.tag;
                        ret.creditBalance = section.credit;
                        values.Add(sItem, ret);
                        return ret;
                    }
                    else
                    {
                        SummaryItemInfo valItem = (SummaryItemInfo)sItem;
                        AccountBalance itemTotal = new AccountBalance();
                        for (int i = 0; i < valItem.accounts.Count; i++)
                        {
                            if (valItem.accounts[i] == -1)
                            {
                                INTAPS.Evaluator.Symbolic s = new Symbolic();
                                s.SetSymbolProvider(this);
                                s.Expression = valItem.formulae[i];
                                EData edDebit, edCredit;
                                _debitSide = true;
                                edDebit = s.Evaluate();
                                if (edDebit.Value is FSError)
                                {
                                    throw new ServerUserMessage(((FSError)edDebit.Value).Message);
                                }

                                _debitSide = false;
                                edCredit = s.Evaluate();
                                if (edCredit.Value is FSError)
                                {
                                    throw new ServerUserMessage(((FSError)edCredit.Value).Message);
                                }
                                double debit, credit;
                                if (!double.TryParse(edDebit.ToString(), out debit))
                                    debit = 0;
                                if (!double.TryParse(edCredit.ToString(), out credit))
                                    credit = 0;

                                AccountBalance bal = new AccountBalance(-1, -1, debit, credit);
                                bal.multiplyBy(valItem.factors[i]);
                                itemTotal.AddBalance(bal);
                            }
                            else
                            {
                                AccountBalance bal = evaluateDelegate(costCenterID, valItem.accounts[i], 0, time1, time2, flow);
                                bal.multiplyBy(valItem.factors[i]);
                                itemTotal.AddBalance(bal);
                            }
                        }

                        SummaryValueItem val = new SummaryValueItem();
                        val.accountBalance = itemTotal;
                        val.displayStyle = valItem.format;
                        val.label = valItem.label;
                        val.tag = valItem.tag;
                        val.creditBalance = valItem.credit;
                        values.Add(sItem, val);
                        return val;
                    }
                }
                finally
                {
                    stack.Remove(sItem);
                }
            }

            public FunctionDocumentation[] GetAvialableFunctions()
            {
                return new FunctionDocumentation[0];
            }

            public EData GetData(URLIden iden)
            {
                return GetData(iden.Element.ToUpper());
            }
            bool _debitSide = true;

            public EData GetData(string symbol)
            {
                SummaryItemBase item = sumInfo.getItemByTag(symbol);
                if (item == null)
                    throw new ServerUserMessage("Undefined tag {0}", symbol);
                SummaryValueItem val = getValue(new HashSet<SummaryItemBase>(), item);
                if (_debitSide)
                    return new EData(val.dbValue);
                else
                    return new EData(val.crValue);
            }

            public FunctionDocumentation GetDocumentation(IFunction f)
            {
                return null;
            }

            public FunctionDocumentation GetDocumentation(URLIden iden)
            {
                return null;
            }

            public FunctionDocumentation GetDocumentation(string Symbol)
            {
                return null;
            }

            public IFunction GetFunction(URLIden iden)
            {
                return GetFunction(iden.Element);
            }

            public IFunction GetFunction(string symbol)
            {
                if (CalcGlobal.Functions.Contains(symbol.ToUpper()))
                    return (IFunction)CalcGlobal.Functions[symbol.ToUpper()];
                return null;

            }

            public bool SymbolDefined(string Name)
            {
                return sumInfo.getItemByTag(Name) != null;
            }
        }
                
    
        public SummaryValueSection evaluateSummaryTable(SummaryInformation summaryInfo, int costCenterID, DateTime time1, DateTime time2,bool flow,EvaluteSummeryItemDelegate evalDelegate)
        {
            SummaryEvaluator seval = new SummaryEvaluator()
            {
                sumInfo = summaryInfo,
                costCenterID = costCenterID,
                time1 = time1,
                time2 = time2,
                flow = flow,
                evaluateDelegate = evalDelegate
            };
            return seval.getValue(new HashSet<SummaryItemBase>(),summaryInfo.rootSection) as SummaryValueSection;
            //return evaluateSummarySection(summaryInfo.rootSection,costCenterID,time1,time2,flow,evalDelegate);
        }
        SummaryValueSection evaluateSummarySection(SummarySection section, int costCenterID, DateTime time1, DateTime time2,bool flow,EvaluteSummeryItemDelegate evaluateDelegate)
        {
            SummaryValueSection ret = new SummaryValueSection();
            ret.label = section.label;
            
            AccountBalance childTotoal=new AccountBalance();
            foreach (SummaryItemBase item in section.summaryItem)
            {
                if (item is SummarySection)
                {
                    SummaryValueSection sv = evaluateSummarySection((SummarySection)item, costCenterID, time1, time2,flow,evaluateDelegate);
                    if(AccountBase.AmountEqual(sv.accountBalance.DebitBalance,0))
                        if(item.removeIfZero)
                        {
                            continue;
                        }
                    ret.childs.Add(sv);
                    childTotoal.AddBalance(sv.accountBalance);
                }
                else
                {
                    SummaryItemInfo valItem=(SummaryItemInfo)item;
                    AccountBalance itemTotal=new AccountBalance();
                    for(int i=0;i<valItem.accounts.Count;i++)
                    {
                        if (valItem.accounts[i] == -1)
                        {

                        }
                        else
                        {
                            AccountBalance bal = evaluateDelegate(costCenterID, valItem.accounts[i], 0, time1, time2, flow);
                            bal.multiplyBy(valItem.factors[i]);
                            itemTotal.AddBalance(bal);
                        }
                    }
                    
                    if(AccountBase.AmountEqual( itemTotal.DebitBalance,0))
                        if(valItem.removeIfZero)
                        {
                            continue;
                        }
                    childTotoal.AddBalance(itemTotal);
                    SummaryValueItem val=new SummaryValueItem();
                    val.accountBalance=itemTotal;
                    val.displayStyle=valItem.format;
                    val.label=valItem.label;
                    val.tag=valItem.tag;
                    val.creditBalance = valItem.credit;

                    ret.childs.Add(val);
                }
            }
            ret.accountBalance=childTotoal;
            ret.displayStyle=section.format;
            ret.label=section.label;
            ret.tag=section.tag;
            ret.creditBalance = section.credit;
            return ret;
        }


        public string renderSummaryTable(SummaryInformation summaryInfo, int costCenterID, DateTime time1, DateTime time2, out int maxLevel)
        {
            bERPHtmlTable table = new bERPHtmlTable();

            bERPHtmlTableRowGroup body = new bERPHtmlTableRowGroup(TableRowGroupType.body);
            table.groups.Add(body);
            maxLevel = 0;
            
            if (summaryInfo != null)
            {
                AccountBalance total;
                List<int> levels = new List<int>();
                List<SummaryItemBase> leaf = new List<SummaryItemBase>();
                SummaryEvaluator eval = new SummaryEvaluator()
                {
                    costCenterID = costCenterID,
                    evaluateDelegate = delegate(int ev_costCenterID, int ev_accountID, int ev_itemID, DateTime ev_time1, DateTime ev_time2, bool ev_flow)
                    {

                        AccountBalance bal = ev_flow ? m_accounting.GetFlow(ev_costCenterID, ev_accountID, ev_itemID, ev_time1, ev_time2)
                            : m_accounting.GetBalanceAsOf(ev_costCenterID, ev_accountID, ev_itemID, ev_time2);
                        return bal;
                    },
                    flow=summaryInfo.flow,
                    sumInfo=summaryInfo,
                    time1=time1,
                    time2=time2,
                };
                total=addChildRows(body, summaryInfo, summaryInfo.rootSection, eval, 0, levels, leaf);
                maxLevel = 0;
                foreach (int l in levels)
                    if (l > maxLevel)
                        maxLevel = l;

                int i = 0;

                foreach (bERPHtmlTableRow row in body.rows)
                {
                    if (leaf[i] is SummarySection)
                    {
                        SummarySection ss = (SummarySection)leaf[i];
                        if (ss.showTotal)
                            row.cells[0].colSpan = maxLevel - levels[i] + 1;
                        else if (summaryInfo.twoColumns)
                            row.cells[0].colSpan = maxLevel - levels[i] + 3;
                        else
                            row.cells[0].colSpan = maxLevel - levels[i] + 2;
                    }
                    else
                        row.cells[0].colSpan = maxLevel - levels[i] + 1;
                    for (int k = 0; k < levels[i]; k++)
                    {
                        row.cells.Insert(0, new bERPHtmlTableCell("", "IndentCell"));
                    }
                    i++;
                }
                if (summaryInfo.headerRow)
                {
                    bERPHtmlTableRowGroup header = new bERPHtmlTableRowGroup(TableRowGroupType.header);
                    table.groups.Insert(0,header);
                    if (summaryInfo.twoColumns)
                    {
                        bERPHtmlBuilder.htmlAddRow(header,
                            new bERPHtmlTableCell(TDType.ColHeader, summaryInfo.itemHeader,maxLevel+1)
                            , new bERPHtmlTableCell(TDType.ColHeader, summaryInfo.debitHeader)
                            , new bERPHtmlTableCell(TDType.ColHeader, summaryInfo.creditHeader));
                    }
                    else
                        bERPHtmlBuilder.htmlAddRow(header,
                            new bERPHtmlTableCell(TDType.ColHeader, summaryInfo.itemHeader, maxLevel + 1)
                            , new bERPHtmlTableCell(TDType.ColHeader, summaryInfo.debitHeader));
                }
                if (summaryInfo.grandTotal)
                {
                    bERPHtmlTableRowGroup footer = new bERPHtmlTableRowGroup(TableRowGroupType.footer);
                    table.groups.Add(footer);
                    if (summaryInfo.twoColumns)
                    {
                        bERPHtmlBuilder.htmlAddRow(footer,
                            new bERPHtmlTableCell(TDType.ColHeader, "Total", maxLevel + 1)
                            , new bERPHtmlTableCell(TDType.ColHeader, total.TotalDebit.ToString("#,#0.00"))
                            , new bERPHtmlTableCell(TDType.ColHeader, total.TotalCredit.ToString("#,#0.00")));
                    }
                    else
                        bERPHtmlBuilder.htmlAddRow(footer,
                            new bERPHtmlTableCell(TDType.ColHeader, "Total", maxLevel + 1)
                            , new bERPHtmlTableCell(TDType.ColHeader, total.GetBalance(summaryInfo.rootSection.credit).ToString("#,#0.00")));
                }
            }

            

            StringBuilder builder = new StringBuilder();
            table.build(builder);
            return builder.ToString();
        }
        private AccountBalance addChildRows(bERPHtmlTableRowGroup body, SummaryInformation info, SummarySection section,SummaryEvaluator eval
                        , int level, List<int> levels, List<SummaryItemBase> leaf
    )
        {
            int i = 0;
            AccountBalance ret = new AccountBalance();
            foreach (SummaryItemBase si in section.summaryItem)
            {
                AccountBalance balance;

                if (si is SummaryItemInfo)
                {

                    bERPHtmlTableRow row = createSectionToolRow(info, si, eval, null, out balance);
                    if (!si.removeIfZero || !balance.IsZero)
                    {
                        body.rows.Add(row);
                        leaf.Add(si);
                        levels.Add(level);
                    }
                    ret.AddBalance(balance);
                }
                else if (si is SummaryItemListInfo)
                {
                    //SummaryItemListInfo list=si as SummaryItemListInfo;
                    //Symbolic s = new Symbolic();
                    //s.SetSymbolProvider(_summaryGeneatorSymbolProvider);
                    //s.Expression = list.accountFormula;
                    //EData ret = s.Evaluate();
                    //if (ret.Value is ListData)
                    //{
                    //    foreach (EData val in (ListData)ret.Value)
                    //    {

                    //    }
                    //}
                }
                else
                {
                    int rowIndex = body.rows.Count;
                    AccountBalance childTotal = addChildRows(body, info, (SummarySection)si, eval, level + 1, levels, leaf);
                    if (!si.removeIfZero || !childTotal.IsZero)
                    {
                        bERPHtmlTableRow row = createSectionToolRow(info, si, eval, childTotal, out balance);
                        body.rows.Insert(rowIndex, row);
                        leaf.Insert(rowIndex, si);
                        levels.Insert(rowIndex, level);
                    }
                    ret.AddBalance(childTotal);
                }
                i++;
            }
            return ret;
        }

        //private AccountBalance addChildRows(bERPHtmlTableRowGroup body, SummaryInformation info, SummarySection section, int costCenterID
        //                        , DateTime time1, DateTime time2
        //                        , int level, List<int> levels, List<SummaryItemBase> leaf
        //    )
        //{
        //    int i = 0;
        //    AccountBalance ret = new AccountBalance();
        //    foreach (SummaryItemBase si in section.summaryItem)
        //    {
        //        AccountBalance balance;
                
        //        if (si is SummaryItemInfo)
        //        {
                    
        //            bERPHtmlTableRow row = createSectionToolRow(info, si, costCenterID, time1, time2, null, out balance);
        //            if (!si.removeIfZero || !balance.IsZero)
        //            {
        //                body.rows.Add(row);
        //                leaf.Add(si);
        //                levels.Add(level);
        //                ret.AddBalance(balance);
        //            }
        //        }
        //        else if (si is SummaryItemListInfo)
        //        {
        //            //SummaryItemListInfo list=si as SummaryItemListInfo;
        //            //Symbolic s = new Symbolic();
        //            //s.SetSymbolProvider(_summaryGeneatorSymbolProvider);
        //            //s.Expression = list.accountFormula;
        //            //EData ret = s.Evaluate();
        //            //if (ret.Value is ListData)
        //            //{
        //            //    foreach (EData val in (ListData)ret.Value)
        //            //    {

        //            //    }
        //            //}
        //        }
        //        else
        //        {
        //            int rowIndex = body.rows.Count;
        //            AccountBalance childTotal = addChildRows(body, info, (SummarySection)si, costCenterID, time1, time2, level + 1, levels, leaf);
        //            if (!si.removeIfZero || !childTotal.IsZero)
        //            {
        //                bERPHtmlTableRow row = createSectionToolRow(info, si, costCenterID, time1, time2, childTotal, out balance);
        //                body.rows.Insert(rowIndex, row);
        //                leaf.Insert(rowIndex, si);
        //                levels.Insert(rowIndex, level);
        //                ret.AddBalance(balance);
        //            }
        //        }                
        //        i++;
        //    }
        //    return ret;
        //}
        internal AccountBalance evaluateSummaryItemInfo(SummaryItemInfo item, SummaryInformation info,int costCenterID, DateTime time1, DateTime time2)
        {
            AccountBalance amount=new AccountBalance();
            for (int i = 0; i < item.accounts.Count; i++)
                {
                    CostCenterAccount csa=m_accounting.GetCostCenterAccount(costCenterID,item.accounts[i]);
                    if (csa != null)
                    {
                        AccountBalance bal = info.flow ? m_accounting.GetFlow(csa.id, info.itemID, time1, time2)
                            : m_accounting.GetBalanceAsOf(csa.id, info.itemID, time2);
                        bal.multiplyBy(item.factors[i]);
                        amount.AddBalance(bal);
                    }
                }
            return amount;
        }
        bERPHtmlTableRow createSectionToolRow(SummaryInformation info, SummaryItemBase si, SummaryEvaluator eval, AccountBalance childTotal, out AccountBalance amount)
        {
            bERPHtmlTableRow ret;
            bERPHtmlTableCell cell;
            ret = new bERPHtmlTableRow();
            ret.cells.Add(new bERPHtmlTableCell(si.label));
            amount = new AccountBalance();
            if (si is SummarySection)
            {
                SummarySection section = (SummarySection)si;

                if (section.showTotal)
                {
                    if (info.twoColumns)
                    {
                        if (section.credit)
                        {
                            ret.cells.Add(new bERPHtmlTableCell(""));
                            ret.cells.Add(cell = new bERPHtmlTableCell(childTotal.GetBalance(true).ToString("#,#0.00")));
                        }
                        else
                        {
                            ret.cells.Add(cell = new bERPHtmlTableCell(childTotal.GetBalance(false).ToString("#,#0.00")));
                            ret.cells.Add(new bERPHtmlTableCell(""));
                        }
                    }
                    else
                        ret.cells.Add(cell = new bERPHtmlTableCell(childTotal.GetBalance(section.credit).ToString("#,#0.00")));
                    cell.styles.Add("text-align:right");
                }

            }
            else
            {

                SummaryItemInfo item = (SummaryItemInfo)si;
                //amount.AddBalance(evaluateSummaryItemInfo(item, info, costCenterID, time1, time2));
                amount.AddBalance(eval.getValue(item).accountBalance);
                if (info.twoColumns)
                {
                    if (item.credit)
                    {
                        ret.cells.Add(new bERPHtmlTableCell(""));
                        ret.cells.Add(cell=new bERPHtmlTableCell(amount.GetBalance(true).ToString("#,#0.00")));
                        
                    }
                    else
                    {
                        ret.cells.Add(cell=new bERPHtmlTableCell(amount.GetBalance(false).ToString("#,#0.00")));
                        ret.cells.Add(new bERPHtmlTableCell(""));
                    }

                }
                else
                    ret.cells.Add(cell=new bERPHtmlTableCell(amount.GetBalance(item.credit).ToString("#,#0.00")));
                cell.styles.Add("text-align:right");
            }
            ret.css = si.format;
            return ret;
        }
        public class ChildOfSummaryFunction : IFunction
        {
            iERPTransactionBDE parent;
            public ChildOfSummaryFunction(iERPTransactionBDE parent)
            {
                this.parent = parent;
            }
            public EData Evaluate(EData[] Pars)
            {
                string accountCode = Pars[0].Value as string;
                int accountID = parent.Accounting.GetAccountID<Account>(accountCode);
                if (accountID == -1)
                    return new FSError("Invalid account code: " + accountCode).ToEData();
                int N;
                Account[] childs=parent.Accounting.GetChildAcccount<Account>(accountID, 0, -1, out N);
                ListData ld = new ListData(childs.Length);
                for (int i = 0; i < childs.Length; i++)
                {
                    ld.elements[i] = new EData(childs[i].id);
                }
                return new EData(DataType.ListData, ld);
            }

            public string Name
            {
                get { return "Statement Account List Function: List of Child Accounts"; }
            }

            public int ParCount
            {
                get { return 1; }
            }

            public string Symbol
            {
                get { return "ST_Childs"; }
            }

            public FunctionType Type
            {
                get { return FunctionType.PreFix; }
            }
        }
        class SummaryGeneratorSymbolProvider:INTAPS.Evaluator.ISymbolProvider
        {
            static iERPTransactionBDE parent;
            static Dictionary<string, IFunction> m_funcs;
            static Dictionary<string, FunctionDocumentation> m_docs;
            static void AddFunction(IFunction f)
            {
                m_funcs.Add(f.Symbol.ToUpper(), f);
                m_docs.Add(f.Symbol.ToUpper(), new FunctionDocumentation(f));
            }
            static void initFunctions(iERPTransactionBDE parent)
            {
                m_funcs = new Dictionary<string, IFunction>();
                m_docs = new Dictionary<string, FunctionDocumentation>();
                AddFunction(new ChildOfSummaryFunction(parent));
            }
            
            public SummaryGeneratorSymbolProvider()
            {
                
                
            }
            #region ISymbolProvider Members

            public IFunction GetFunction(string symbol)
            {
                if (m_funcs.ContainsKey(symbol))
                    return m_funcs[symbol];
                if (CalcGlobal.Functions.Contains(symbol))
                    return (IFunction)CalcGlobal.Functions[symbol];
                throw new ServerUserMessage("Unknown function " + symbol);
            }
            List<string> refs = new List<string>();
            public EData GetData(string symbol)
            {
                if(symbol.IndexOf("V")==0)
                {

                }
                throw new ServerUserMessage("Undefind variable " + symbol);
            }

            public IFunction GetFunction(URLIden iden)
            {
                return GetFunction(iden.Element);
            }

            public EData GetData(URLIden iden)
            {
                return GetData(iden.Element);
            }

            public FunctionDocumentation[] GetAvialableFunctions()
            {
                FunctionDocumentation[] ret = new FunctionDocumentation[m_docs.Count
                    /*+CalcGlobal.DocumentedFunctions.Count*/];
                m_docs.Values.CopyTo(ret, 0);
                //CalcGlobal.DocumentedFunctions.Values.CopyTo(ret, m_docs.Count);

                return ret;
            }

            public FunctionDocumentation GetDocumentation(string Symbol)
            {
                if (m_docs.ContainsKey(Symbol))
                    return m_docs[Symbol];
                if (CalcGlobal.DocumentedFunctions.ContainsKey(Symbol))
                    return (FunctionDocumentation)CalcGlobal.DocumentedFunctions[Symbol];
                throw new ServerUserMessage("Undefined function " + Symbol);
            }

            public FunctionDocumentation GetDocumentation(URLIden iden)
            {
                return GetDocumentation(iden.Element);
            }

            public FunctionDocumentation GetDocumentation(IFunction f)
            {
                return GetDocumentation(f.Symbol);
            }

            public bool SymbolDefined(string Name)
            {
                return false;
            }

            #endregion
        }
    }
}