using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        public string RegisterSupplier(int AID, TradeRelation supplier)
        {
            throw new Exception("Depeciated");
        }
        public TradeRelation GetSupplier(string code)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                TradeRelation[] ret = dspReader.GetSTRArrayByFilter<TradeRelation>("Code='" + code + "'  and IsSupplier<>0");
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public TradeRelation[] GetAllSuppliers()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<TradeRelation>("IsSupplier<>0");
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public TradeRelation[] SearchSuppliers(int index, int pageSize, object[] criteria, string[] column, out int NoOfRecords)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            string filter = "";
            try
            {
                if (column != null)
                {
                    for (int i = 0; i < column.Length; i++)
                    {
                        if (i > 0 && i <= column.Length - 1)
                            filter += " AND ";
                        filter += String.Format("{0} LIKE '%{1}%'", column[i], criteria[i]);
                    }
                }
                if (string.IsNullOrEmpty(filter))
                    filter = "IsSupplier<>0";
                else
                    filter += " and IsSupplier<>0";
                return dspReader.GetSTRArrayByFilter<TradeRelation>(filter, index, pageSize, out NoOfRecords);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public void DeleteSupplier(int AID, string[] code)
        {
            throw new Exception("Deprecated");
        }

        public void ActivateSupplier(int AID, string supplierCode)
        {
            throw new Exception("Depreciated");
        }

        public void DeactivateSupplier(int AID, string supplierCode)
        {
            throw new Exception("Depreciated");

        }
        #region Private Methods
        class SupplierPurchasedDocumentsFilter : INTAPS.RDBMS.IObjectFilter<AccountDocument>
        {

            INTAPS.Accounting.BDE.AccountingBDE finance;
            List<Purchase2Document> filtered;
            string supplierCode;
            public SupplierPurchasedDocumentsFilter(INTAPS.Accounting.BDE.AccountingBDE finance, List<Purchase2Document> filtered, string code)
            {
                this.finance = finance;
                this.filtered = filtered;
                supplierCode = code;
            }
            #region IObjectFilter<BondPaymentDocument> Members

            public bool Include(AccountDocument obj, object[] additional)
            {
                Purchase2Document purchasedDocument = (Purchase2Document)finance.GetAccountDocument(obj.AccountDocumentID, true);
                if (!purchasedDocument.relationCode.Equals(supplierCode))
                    return false;
                filtered.Add(purchasedDocument);
                return true;
            }

            #endregion
        }
        class TradeRelationTransactionFilter
        {
            string _code;
            public TradeRelationTransactionFilter(string code)
            {
                _code = code;
            }
            public bool FindTradeTransaction<tt>(AccountDocument doc) where tt : TradeTransaction
            {
                tt ttdoc = doc as tt;
                if (ttdoc == null)
                    return false;
                return ttdoc.relationCode.Equals(_code);
            }
        }
        private bool HasCompanyPurchasedAnyItems(string supplierCode)
        {
            INTAPS.RDBMS.SQLHelper dspReader = m_accounting.GetReaderHelper();
            try
            {
                List<Purchase2Document> filtered = new List<Purchase2Document>();
                SupplierPurchasedDocumentsFilter documentFilter = new SupplierPurchasedDocumentsFilter(m_accounting, filtered, supplierCode);
                TradeRelationTransactionFilter f = new TradeRelationTransactionFilter(supplierCode);
                return m_accounting.FindFirstDocument(null, m_accounting.GetDocumentTypeByType(typeof(Purchase2Document)).id, new INTAPS.Accounting.BDE.AccountDocumentFilterDelegate(f.FindTradeTransaction<Purchase2Document>), true)!=null;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        #endregion
    }
}

