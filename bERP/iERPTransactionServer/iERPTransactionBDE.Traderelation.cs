using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        // BDE exposed
        public TradeRelation[] searchTradeRelation(string categoryCode, TradeRelationType[] types, int index, int pageSize, object[] criteria, string[] column, out int NoOfRecords)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            string filter = "";
            try
            {
                if (column != null)
                {
                    for (int i = 0; i < column.Length; i++)
                    {
                        if (i > 0 && i <= column.Length - 1)
                            filter += " OR ";
                        filter += String.Format("{0} LIKE '%{1}%'", column[i], criteria[i]);
                    }
                }
                
                if (!string.IsNullOrEmpty(categoryCode))
                {
                    validateTradeRelationCategoryCode(categoryCode);
                    if (string.IsNullOrEmpty(filter))
                        filter = "pcode='"+categoryCode+"'";
                    else
                        filter += "and pcode='"+categoryCode+"'";
                }

                if (types != null && types.Length > 0)
                {
                    string typeFilter=null;
                    foreach (TradeRelationType t in types)
                    {
                        string cr;
                        switch (t)
                        {
                            case TradeRelationType.Supplier:
                                cr = "IsSupplier<>0";
                                break;
                            case TradeRelationType.Customer:
                                cr = "IsCustomer<>0";
                                break;
                            case TradeRelationType.Financer:
                                cr = "IsFinancer<>0";
                                break;
                            case TradeRelationType.Financee:
                                cr = "IsFinancee<>0";
                                break;
                            default:
                                cr=null;
                                break;
                        }
                        if (string.IsNullOrEmpty(typeFilter))
                            typeFilter = cr;
                        else
                            typeFilter += " or " + cr;
                    }
                    if (string.IsNullOrEmpty(filter))
                        filter = "(" + typeFilter + ")";
                    else
                        filter += "and (" + typeFilter + ")";
                }
                return dspReader.GetSTRArrayByFilter<TradeRelation>(filter, index, pageSize, out NoOfRecords);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        #region DM Methods

        public void DeleteRelation(int AID, string code)
        {
            if (HasCompanyPurchasedAnyItems(code)
            || HasCompanySoldAnyItems(code))
                throw new ServerUserMessage("You can't delete this trade relation because it is used in transactions.");
            lock (dspWriter)
            {
                try
                {
                    dspWriter.BeginTransaction();
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<RelationsAccountChangeFilter>().allowAction(AID);
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<RelationsCostCenterChangeFilter>().allowAction(AID);
                    TradeRelation relation = GetRelation(code);
                    if (relation == null)
                        throw new ServerUserMessage("Invalid relation code:" + code);
                    foreach (int accountID in relation.allAccountFields)
                    {
                        if (accountID == -1)
                            continue;
                        if(m_accounting.GetAccount<Account>(accountID)!=null)
                            m_accounting.DeleteAccount<Account>(AID, accountID, true);
                    }
                    foreach (int costCenterID in relation.allCostCenterFields)
                    {
                        if (costCenterID == -1)
                            continue;
                        if (m_accounting.GetAccount<CostCenter>(costCenterID) != null)
                            m_accounting.DeleteAccount<CostCenter>(AID, costCenterID, true);
                    }
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.TradeRelation", "code='" + code + "'");

                    dspWriter.DeleteSingleTableRecrod<TradeRelation>(this.DBName, code);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<RelationsAccountChangeFilter>().resetAllowAction();
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<RelationsCostCenterChangeFilter>().resetAllowAction();
                }
            }
        }

        public TradeRelation GetRelation(string code)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                TradeRelation[] ret = reader.GetSTRArrayByFilter<TradeRelation>("code='" + code + "'");
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        public Country[] GetCountries()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                Country[] ret = reader.GetSTRArrayByFilter<Country>(null);
                return ret;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        public Country GetCountryById(short id)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                Country[] ret = reader.GetSTRArrayByFilter<Country>("id="+id);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public TradeRelation GetRelationOldVersion(string code,int versionID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                TradeRelation[] ret = reader.GetSTRArrayBySQL<TradeRelation>(@"SELECT [Code]
      ,[pcode]
      ,[Name]
      ,[TIN]
      ,[VATRegNumber]
      ,[Region]
      ,[Zone]
      ,[Woreda]
      ,[Kebele]
      ,[HouseNumber]
      ,[Telephone]
      ,[Email]
      ,[Website]
      ,[Description]
      ,[TaxRegistrationType]
      ,[ReceivableAccountID]
      ,[PayableAccountID]
      ,[PaidBondAccountID]
      ,[ReceivedBondAccountID]
      ,[RetentionReceivableAccountID]
      ,[RetentionPayableAccountID]
      ,[Activated]
      ,[Logo]
      ,[Withhold]
      ,[IsWithholdingAgent]
      ,[IsVatAgent]
      ,[IsSupplier]
      ,[IsCustomer]
      ,[IsFinancer]
      ,[ISFinancee]
      ,[onCustomerOrderCostCenterID]
      ,[onSupplierOrderCostCenterID]
      ,[Address] from Main_2006.dbo.TradeRelation_Deleted where Code='" + code + "' and __AID=" + versionID);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public TradeRelationCategory getTradeRelationCategory(string code)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                TradeRelationCategory[] ret = dspReader.GetSTRArrayByFilter<TradeRelationCategory>("code='" + code + "'");
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        
        public TradeRelation getRelationByTIN(string TIN)
        {
            if(string.IsNullOrEmpty(TIN))
                return null;
            INTAPS.RDBMS.SQLHelper reader = GetReaderHelper();
            
            try
            {
                TradeRelation[] ret= reader.GetSTRArrayByFilter<TradeRelation>("TIN='" + TIN + "'");
                if (ret.Length==0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public string RegisterRelation(int AID, TradeRelation relation)
        {
            lock (dspWriter)
            {
                try
                {
                    dspWriter.BeginTransaction();
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<RelationsAccountChangeFilter>().allowAction(AID);
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<RelationsCostCenterChangeFilter>().allowAction(AID);
                    TradeRelation oldRelation = string.IsNullOrEmpty(relation.Code) ? null : GetRelation(relation.Code);
                    TradeRelationCategory cat = getTradeRelationCategory(relation.pcode);
                    if (cat == null)
                        throw new ServerUserMessage("Invalid trade relation category " + relation.Code);
                    bool newRelation = string.IsNullOrEmpty(relation.Code);
                    if (newRelation)
                        relation.Code = cat.code + TSConstants.FormatRelationCode(AutoIncrement.GetKey("bERP.RelationCode"));
                    addCategorizedItemToCOA<TradeRelationCategory, TradeRelation>(AID, cat, relation);

                    if (newRelation) //INSERT
                    {
                        TradeRelation test = getRelationByTIN(relation.TIN);
                        if (test != null)
                            throw new ServerUserMessage(string.Format("The TIN number {0} is already used by {1}",test.TIN, test.Name));
                        dspWriter.InsertSingleTableRecord<TradeRelation>(this.DBName, relation, new string[] { "__AID" }, new object[] { AID });
                    }
                    else //UPDATE
                    {
                        TradeRelation test = getRelationByTIN(relation.TIN);
                        if (test != null && !test.Code.Equals(relation.Code))
                            throw new ServerUserMessage(string.Format("The TIN number {0} is already used by {1}", test.TIN, test.Name));

                        if (!relation.pcode.Equals(oldRelation.pcode))
                            throw new ServerUserMessage("It is not allowed to change the category of a trade relation once it is created");
                        dspWriter.logDeletedData(AID, this.DBName + ".dbo.TradeRelation", "code='" + relation.Code + "'");
                        dspWriter.UpdateSingleTableRecord<TradeRelation>(this.DBName, relation, new string[] { "__AID" }, new object[] { AID });
                    }
                    
                    dspWriter.CommitTransaction();
                    return relation.Code;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<RelationsAccountChangeFilter>().resetAllowAction();
                    INTAPS.ClientServer.ObjectFilters.GetFilterInstance<RelationsCostCenterChangeFilter>().resetAllowAction();
                }
            }
        }
        #endregion

        
        private bool RelationTINIsDuplicate(string TIN, string code)
        {
            INTAPS.RDBMS.SQLHelper dspReader = m_accounting.GetReaderHelper();
            try
            {
                if (!string.IsNullOrEmpty(TIN))
                {
                    if (code == null)
                    {
                        if ((int)dspReader.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.{1} where TIN='{2}'", m_DBName, typeof(TradeRelation).Name, TIN)) > 0)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if ((int)dspReader.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.{1} where TIN='{2}' AND Code <> '{3}'", m_DBName, typeof(TradeRelation).Name, TIN, code)) > 0)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public void ActivateRelation(int AID, string code)
        {
            lock (dspWriter)
            {
                TradeRelation relation = GetRelation(code);
                try
                {
                    dspWriter.BeginTransaction();
                    foreach (int ac in relation.allAccountFields)
                        m_accounting.ActivateAcount<Account>(AID, ac, DateTime.Now.Date);
                    dspWriter.ExecuteScalar(String.Format("Update TradeRelation Set Activated='True' where Code='{0}'", code));
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public void DeactivateRelation(int AID, string code)
        {
            lock (dspWriter)
            {
                TradeRelation relation = GetRelation(code);
                try
                {
                    dspWriter.BeginTransaction();
                    foreach (int ac in relation.allAccountFields)
                        m_accounting.DeactivateAccount<Account>(AID, ac, DateTime.Now.Date);
                    dspWriter.ExecuteScalar(String.Format("Update TradeRelation Set Activated='False' where Code='{0}'", code));
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        internal TransactionOfBatch[] getAssetLiabilityPairTransaction(DateTime date, CostCenterAccount assetCsAccount, CostCenterAccount liabilityCsAccount, double amount, string note)
        {
            double assetBalance = m_accounting.GetNetBalanceAsOf(assetCsAccount.id, 0, date);
            double liabilityBalance = m_accounting.GetNetBalanceAsOf(liabilityCsAccount.id, 0, date);
            double net = assetBalance - liabilityBalance + amount;

            List<TransactionOfBatch> ret = new List<TransactionOfBatch>();
            if (!AccountBase.AmountLess(net, 0))
            {
                if (!AccountBase.AmountEqual(liabilityBalance, 0))
                    ret.Add(new TransactionOfBatch(liabilityCsAccount.id, liabilityBalance, note));
                ret.Add(new TransactionOfBatch(assetCsAccount.id, net - assetBalance, note));
            }
            if (!AccountBase.AmountGreater(net, 0))
            {
                if (!AccountBase.AmountEqual(assetBalance, 0))
                    ret.Add(new TransactionOfBatch(assetCsAccount.id, -assetBalance, note));
                ret.Add(new TransactionOfBatch(liabilityCsAccount.id, net + liabilityBalance, note));
            }
            return ret.ToArray();
        }

        // BDE exposed
        public TradeRelationCategory[] getCategories(string parentCategoryCode)
        {
             INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<TradeRelationCategory>("pcode='" + parentCategoryCode + "'");
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        
        // BDE exposed
        public TradeRelation[] getTradeRelationsByCategory(string parentCategoryCode)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<TradeRelation>("pcode='" + parentCategoryCode + "'");
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        void validateTradeRelationCategoryCode(string code)
        {
            System.Text.RegularExpressions.Match m = System.Text.RegularExpressions.Regex.Match(code, @"[\d\w-_]{1,}", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            if (m.Value == null || m.Value.Length != code.Length)
                throw new ServerUserMessage("Invalid code " + code);

        }

        // BDE exposed
        public void createTradeRelationCategory(int AID, TradeRelationCategory cat)
        {
            validateTradeRelationCategoryCode(cat.code);
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    if (getTradeRelationCategory(cat.code) != null)
                        throw new ServerUserMessage("Code " + cat.code + " is already used");
                    if(!cat.pcode.Equals(""))
                        if(getTradeRelationCategory(cat.pcode) == null)
                            throw new ServerUserMessage("Parent code " + cat.pcode + " doesn't exist");
                    dspWriter.InsertSingleTableRecord(this.DBName, cat, new string[] { "__AID" }, new object[] { AID });
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        // BDE exposed
        public void updateTradeRelationCategory(int AID, TradeRelationCategory cat)
        {
            validateTradeRelationCategoryCode(cat.code);
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    TradeRelationCategory oldCat;
                    if ((oldCat = getTradeRelationCategory(cat.code)) == null)
                        throw new ServerUserMessage("Code " + cat.code + " doesn't exist");
                    if (!oldCat.pcode.Equals(cat.pcode))
                        throw new ServerUserMessage("It is not allwoed to change parent of a trade relation category once it is created");
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.TradeRelationCategory", "code='" + cat.code + "'");
                    dspWriter.UpdateSingleTableRecord(this.DBName, cat,new string[]{"__AID"},new object[]{AID});
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        
        // BDE exposed
        public void deleteTradeRelationCategory(int AID, string tradeRelationCategoryCode)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    TradeRelationCategory oldCat;
                    if ((oldCat = getTradeRelationCategory(tradeRelationCategoryCode)) == null)
                        throw new ServerUserMessage("Code " + tradeRelationCategoryCode + " doesn't exist");
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.TradeRelationCategory", "code='" + tradeRelationCategoryCode + "'");
                    dspWriter.DeleteSingleTableRecrod<TradeRelationCategory>(this.DBName, tradeRelationCategoryCode);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
    }
}
