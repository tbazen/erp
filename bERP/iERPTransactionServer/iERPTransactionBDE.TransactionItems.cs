using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS;
using INTAPS.ClientServer;
namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        // BDE exposed
        public void setConvesionFactors(int AID, string itemScope, int categoryScope, UnitCoversionFactor[] factors)
        {
            string cr;
            if (!string.IsNullOrEmpty(itemScope))
            {
                cr = "itemScope='{0}' and categoryScope=-1".format(itemScope);
                categoryScope = -1;
            }
            else if (categoryScope != -1)
            {
                cr = "itemScope='' and  categoryScope={0}".format(categoryScope);
                itemScope = "";
            }
            else
            {
                cr = "itemScope='' and categoryScope=-1";
                itemScope = "";
                categoryScope = -1;
            }
            dspWriter.logDeletedData(AID, "{0}.dbo.{1}".format(this.DBName, typeof(UnitCoversionFactor).Name), cr);
            dspWriter.Delete(this.DBName,typeof(UnitCoversionFactor).Name, cr);
            foreach (UnitCoversionFactor f in factors)
            {
                f.itemScope = itemScope;
                f.categoryScope = categoryScope;
                dspWriter.InsertSingleTableRecord(this.DBName, f, new string[] { "__AID" }, new object[] { AID });
            }
        }
        
        public UnitCoversionFactor[] getConversionFactors(INTAPS.RDBMS.SQLHelper reader, string itemScope, int categoryScope)
        {
            string cr;
            if (!string.IsNullOrEmpty(itemScope))
            {
                cr = "itemScope='{0}' and categoryScope=-1".format(itemScope);
            }
            else if (categoryScope != -1)
                cr = "itemScope='' and  categoryScope={0}".format(categoryScope);
            else
                cr = "itemScope='' and categoryScope=-1";
            return reader.GetSTRArrayByFilter<UnitCoversionFactor>(cr);
        }

        // BDE exposed
        public UnitCoversionFactor[] getConversionFactors(string itemScope, int categoryScope)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return getConversionFactors(reader, itemScope, categoryScope);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public UnitConvertor getItemUnitConvertor(string itemCode)
        {

            TransactionItems item = GetTransactionItems(itemCode);
            if (item == null)
                throw new ServerUserMessage("Invlaid item Code {0}", itemCode);
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                UnitConvertor ret = new UnitConvertor();
                foreach (UnitCoversionFactor f in getConversionFactors("", -1))
                    ret.addFactor(f.baseUnitID, f.conversionUnitID, f.conversionFactor);
                List<int> categories = new List<int>();
                int categoryID = item.categoryID;
                while(categoryID!=-1)
                {
                    categories.Insert(0, categoryID);
                    categoryID = GetItemCategory(categoryID).PID;
                }
                foreach(int catID in categories)
                {
                    foreach (UnitCoversionFactor f in getConversionFactors("", catID))
                        ret.addFactor(f.baseUnitID, f.conversionUnitID, f.conversionFactor);
                }
                foreach (UnitCoversionFactor f in getConversionFactors(itemCode, -1))
                    ret.addFactor(f.baseUnitID, f.conversionUnitID, f.conversionFactor);
                return ret;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

    }
}