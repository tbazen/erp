﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP.Server
{
    class CheckNoFilter
    {
        string searchNo;
        public CheckNoFilter(string n)
        {
            this.searchNo = n;
        }
        public bool CheckNoFilterDeligate(AccountDocument doc)
        {
            System.Reflection.FieldInfo fi = doc.GetType().GetField("checkNumber");
            if (fi == null)
                return false;
            object val = fi.GetValue(doc);
            if (val is string)
                return fi.GetValue(doc).Equals(searchNo);
            else
                return false;
        }

    }
    public partial class iERPTransactionBDE
    {
        public static void CheckCheckNo(INTAPS.Accounting.BDE.AccountingBDE bde, INTAPS.RDBMS.SQLHelper reader, string no)
        {
            int[] checkDocTypes = new int[]{
            bde.GetDocumentTypeByType(typeof(BankWithdrawalDocument)).id
            ,bde.GetDocumentTypeByType(typeof(SupplierReceivableDocument)).id
                , bde.GetDocumentTypeByType(typeof(SupplierPayableDocument)).id
                ,bde.GetDocumentTypeByType(typeof(CustomerAdvanceReturnDocument)).id
                ,bde.GetDocumentTypeByType(typeof(ExpenseAdvanceDocument)).id
                ,bde.GetDocumentTypeByType(typeof(ShortTermLoanDocument)).id
                ,bde.GetDocumentTypeByType(typeof(LongTermLoanDocument)).id
                ,bde.GetDocumentTypeByType(typeof(UnclaimedSalaryDocument)).id};

            CheckNoFilter ch = new CheckNoFilter(no);
            foreach (int type in checkDocTypes)
            {
                AccountDocument doc=bde.FindFirstDocument(no, type, false, DateTime.Now, DateTime.Now,
                    ch.CheckNoFilterDeligate, true);
                if (doc != null)
                {
                    throw new INTAPS.ClientServer.ServerUserMessage(String.Format("The check no is already used.\n Document Type :{0}\n Document Date:{1}", bde.GetDocumentTypeByID(doc.DocumentTypeID).name, doc.DocumentDate.ToShortDateString()));
                }
            }
        }
        public TaxRates getTaxRates()
        {
            TaxRates ret = new TaxRates();

            ret.CommonVATRate = SysPars.CommonVATRate;
            ret.GoodsTaxableRate = SysPars.GoodsTaxableRate;
            ret.ServicesTaxable = SysPars.ServicesTaxable;
            ret.WHGoodsNonTaxablePriceLimit = SysPars.WHGoodsNonTaxablePriceLimit;
            ret.WHGoodsTaxableRate = SysPars.WHGoodsTaxableRate;
            ret.WHServicesNonTaxablePriceLimit = SysPars.WHServicesNonTaxablePriceLimit;
            ret.WHServicesTaxableRate = SysPars.WHServicesTaxableRate;
            ret.UnknownTINWithHoldingRate = SysPars.UnknownTINWithHoldingRate;
            ret.VATWithholdingNonTaxablePriceLimit = SysPars.VATWithholdingNonTaxablePriceLimit;
            
            return ret;
        }
        public static T[] addElement<T>(T[] array, params T[] e)
        {
            T[] ret = new T[array.Length + e.Length];
            Array.Copy(array, ret, array.Length);
            Array.Copy(e, 0, ret, array.Length, e.Length);
            return ret;
        }
    }
}