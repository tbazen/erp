using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using System.Data.SqlClient;
using System.Data;
using INTAPS.Payroll.BDE;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE
    {
        public ZReportDocument[] GetZReport(AccountingPeriod period)
        {
            int N;
            ZReportDocument[] zDocs = null;
            if (period != null)
            {
                AccountDocument[] docs = m_accounting.GetAccountDocuments(null, m_accounting.GetDocumentTypeByType(typeof(ZReportDocument)).id,
                    true, period.fromDate, period.toDate, 0, -1, out N);
                INTAPS.RDBMS.SQLHelper helper = GetReaderHelper();
                try
                {
                    zDocs = new ZReportDocument[docs.Length];
                    int i = 0;
                    foreach (AccountDocument doc in docs)
                    {
                        ZReportDocument zDoc = m_accounting.GetAccountDocument(doc.AccountDocumentID, true) as ZReportDocument;
                        zDocs[i] = zDoc;
                        i++;
                    }

                }
                finally
                {
                    ReleaseHelper(helper);
                }
            }
            return zDocs;
        }

        public double GetSalesTaxableAndNonTaxable(DateTime date, out double nonTaxableAmount, out double tax)
        {
            int N;
            AccountDocument[] docs = m_accounting.GetAccountDocuments(null, m_accounting.GetDocumentTypeByType(typeof(Sell2Document)).id,
                true, date, date.AddDays(1), 0, -1, out N);
            INTAPS.RDBMS.SQLHelper helper = GetReaderHelper();
            try
            {
                double taxable = 0;
                nonTaxableAmount = 0;
                tax=0;
                if (m_sysPars.companyProfile.TaxRegistrationType != TaxRegisrationType.NonTaxPayer)
                {
                    foreach (AccountDocument doc in docs)
                    {
                        Sell2Document saleDoc = m_accounting.GetAccountDocument(doc.AccountDocumentID, true) as Sell2Document;

                        double taxRate = m_sysPars.companyProfile.TaxRegistrationType == TaxRegisrationType.VAT ? saleDoc.taxRates.CommonVATRate : saleDoc.taxRates.GoodsTaxableRate;
                        foreach (TransactionDocumentItem saleItem in saleDoc.items)
                        {
                            TransactionItems item = GetTransactionItems(saleItem.code);
                            if (item.TaxStatus == ItemTaxStatus.NonTaxable || saleItem.remitted)
                            {
                                nonTaxableAmount += saleItem.quantity * saleItem.unitPrice;
                            }
                            else
                            {
                                taxable += saleItem.quantity * saleItem.unitPrice;
                                tax += saleItem.quantity * saleItem.unitPrice * taxRate;
                            }
                        }
                    }

                }
                return taxable;

            }
            finally
            {
                ReleaseHelper(helper);
            }

        }
    }
}
