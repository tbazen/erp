using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using System.Data.SqlClient;
using System.Data;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
using INTAPS;
namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionBDE : INTAPS.ClientServer.BDEBase
    {

        iERPSystemParameters m_sysPars;
        private INTAPS.Accounting.BDE.AccountingBDE m_accounting;
        public INTAPS.Accounting.BDE.AccountingBDE Accounting
        {
            get { return m_accounting; }

        }
        private PayrollBDE m_Payroll;

        public PayrollBDE Payroll
        {
            get { return m_Payroll; }
        }

        public iERPTransactionBDE(string bdeName, string dbName
        , INTAPS.RDBMS.SQLHelper writer, string readerConStr)
            : base(bdeName, dbName, writer, readerConStr)
        {
            m_accounting = (INTAPS.Accounting.BDE.AccountingBDE)ApplicationServer.GetBDE("Accounting");
            m_Payroll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
            DocumentType dt = m_accounting.GetDocumentTypeByTypeNoException(typeof(BookClosingDocument));
            if (dt != null)
                m_accounting.RegisterClosingDocumentType(dt.id);
            LoadSystemParameters();
            loadFixedAssetRules();
            InitializeReporting();
            initProperty();
        }
        public iERPSystemParameters SysPars
        {
            get
            {
                return m_sysPars;
            }
        }
        void LoadSystemParameters()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                m_sysPars = dspReader.LoadSystemParameters<iERPSystemParameters>();
            }
            catch (Exception e)
            {
                INTAPS.ClientServer.ApplicationServer.EventLoger.LogException("Error loading system parameters.", e);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public object[] GetSystemParameters(string[] fields)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSystemParameters<iERPSystemParameters>(m_sysPars, fields);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public void SetSystemParameters(int AID, string[] fields, object[] vals)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.SetSystemParameters<iERPSystemParameters>(m_DBName, m_sysPars, fields, vals, true, AID);
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        private string GetNextSerialCode(string tableName, string columnName, INTAPS.RDBMS.SQLHelper dspWriter)
        {
            string sql = String.Format("select {0} from {1}.dbo.{2}", columnName, this.DBName, tableName);
            DataTable table = dspWriter.GetDataTable(sql);
            if (table != null)
            {
                if (table.Rows.Count > 0)
                {
                    string code = (string)table.Rows[table.Rows.Count - 1][0];
                    int nextCode = int.Parse(code) + 1;
                    return TSConstants.FormatSerialCode(nextCode);
                }
                else
                    return TSConstants.FormatSerialCode(1);
            }
            else
            {
                return TSConstants.FormatSerialCode(1);
            }
        }
        private string GetNextItemSerialCode(string tableName, string columnName, int categID, string categoryCode, INTAPS.RDBMS.SQLHelper dspWriter)
        {
            try
            {
                string sql = String.Format("select {0} from {3}.dbo.{1} where Code LIKE '{2}%'", columnName, tableName, categoryCode, this.DBName);
                DataTable table = dspWriter.GetDataTable(sql);
                if (table != null)
                {
                    if (table.Rows.Count > 0)
                    {
                        string code = (string)table.Rows[table.Rows.Count - 1][0];
                        string itemCode = code.Substring(categoryCode.Length);
                        //Betsu: Code inserted below to validate item code incase property item registeration has been started under the category
                        int nextCode = 0;
                        if (itemCode.Contains("-"))
                            nextCode = int.Parse(itemCode.Split('-')[0]) + 1;
                        else
                            nextCode = int.Parse(itemCode) + 1;
                        return TSConstants.FormatItemSerialCode(categoryCode, nextCode);
                    }
                    return TSConstants.FormatItemSerialCode(categoryCode.Trim(), 1);
                }
                else
                {
                    return TSConstants.FormatItemSerialCode(categoryCode.Trim(), 1);
                }
            }
            catch
            {
                throw;
            }
        }
        private string GetNextStoreSerialCode(string tableName, string columnName, INTAPS.RDBMS.SQLHelper dspWriter)
        {
            string sql = String.Format("select {0} from {2}.dbo.{1}", columnName, tableName, this.DBName);
            DataTable table = dspWriter.GetDataTable(sql);
            if (table != null)
            {
                if (table.Rows.Count > 0)
                {
                    string code = (string)table.Rows[table.Rows.Count - 1][0];
                    int nextCode = int.Parse(code) + 1;
                    return TSConstants.FormatStoreSerialCode(nextCode);
                }
                return TSConstants.FormatStoreSerialCode(1);
            }
            else
            {
                return TSConstants.FormatStoreSerialCode(1);
            }
        }
        public BIZNET.iERP.TypedDataSets.VATDeclarationData GetVATDeclaration(int periodID)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                throw new INTAPS.ClientServer.ServerUserMessage("BDE Note implemented");
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public bool IsDocumentDeclared(int docID)
        {
            bool declared = false;
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                int count = (int)helper.ExecuteScalar("select count(*) From " + this.DBName + ".dbo.DeclaredDocument where documentID=" + docID);
                if (count > 0)
                    declared = true;
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
            return declared;
        }
        public TransactionItems GetTransactionItems(string code)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                TransactionItems[] items = dspReader.GetSTRArrayByFilter<TransactionItems>("Code='" + code + "'");
                if (items.Length == 0)
                    return null;
                return items[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public TransactionItems GetTransactionItems(int accountID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                TransactionItems[] items = dspReader.GetSTRArrayByFilter<TransactionItems>(
                    string.Format(@"[expenseAccountID]={0} or
      [prePaidExpenseAccountID] = {0} or
      [salesAccountID] = {0} or
      [unearnedRevenueAccountID] = {0} or
      [finishedWorkAccountID] = {0} or
      [inventoryAccountID] = {0} or
      [originalFixedAssetAccountID] = {0} or
      [depreciationAccountID] = {0} or
      [accumulatedDepreciationAccountID] = {0}", accountID));
                if (items.Length == 0)
                    return null;
                return items[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public TransactionItems[] getAllFixedAssetItems()
        {
            int N;
            return SearchTransactionItems(0, -1, new object[] { true }, new string[] { "IsFixedAssetItem" }, out N);
        }
        public TransactionItems[] SearchTransactionItems(int index, int pageSize, object[] criteria, string[] column, out int NoOfRecords)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            string filter = "";
            try
            {
                if (column != null)
                {
                    for (int i = 0; i < column.Length; i++)
                    {
                        if (i > 0 && i <= column.Length - 1)
                            filter += " AND ";
                        if (column[i].Equals("IsDirectCost") || column[i].Equals("IsExpenseItem")
                            || column[i].Equals("IsSalesItem") || column[i].Equals("IsFixedAssetItem"))
                            filter += String.Format("{0}='{1}'", column[i], criteria[i]);
                        else if (column[i].Equals("DepreciationType"))
                            filter += String.Format("{0}={1}", column[i], criteria[i]);
                        else
                            filter += String.Format("{0} LIKE '%{1}%'", column[i], criteria[i]);
                    }
                }
                return dspReader.GetSTRArrayByFilter<TransactionItems>(filter, index, pageSize, out NoOfRecords);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public MeasureUnit[] GetMeasureUnits()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                MeasureUnit[] units = dspReader.GetSTRArrayByFilter<MeasureUnit>(null);
                return units;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public MeasureUnit GetMeasureUnit(int ID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                MeasureUnit[] units = dspReader.GetSTRArrayByFilter<MeasureUnit>("ID=" + ID);
                if (units.Length == 0)
                    return null;
                else
                    return units[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public MeasureUnit GetMeasureUnit(string name)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                MeasureUnit[] units = dspReader.GetSTRArrayByFilter<MeasureUnit>("Name='{0}'".format(name));
                if (units.Length == 0)
                    return null;
                else
                    return units[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public double convertUnit(string itemCode, double quantity, int fromUnit, int toUnit)
        {
            if (fromUnit == toUnit)
                return quantity;
            UnitConvertor ucon=getItemUnitConvertor(itemCode);
            if (ucon == null)
                throw new ServerUserMessage("Couldn't find unit conversion settings for item {0}", itemCode);
            return ucon.convert(fromUnit, toUnit,quantity);
        }
        public int RegisterMeasureUnit(int AID, MeasureUnit measureUnit)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();

                try
                {
                    int measureID = -1;
                    if (measureUnit.ID == -1) //INSERT
                    {
                        if ((int)dspWriter.ExecuteScalar("Select count(*) From MeasureUnit where Name LIKE '" + measureUnit.Name + "'") > 0)
                            throw new ServerUserMessage("Measure Unit already exists! Please provide different measuring unit");
                        int unitID = INTAPS.ClientServer.AutoIncrement.GetKey("iERP.MeasureUnitID");
                        measureUnit.ID = unitID;
                        dspWriter.InsertSingleTableRecord(m_DBName, measureUnit, new string[] { "__AID" }, new object[] { AID });
                        measureID = measureUnit.ID;
                    }
                    else //UPDATE
                    {

                        logExistingData(AID, ".dbo.MeasureUnit", "ID=" + measureUnit.ID);
                        dspWriter.UpdateSingleTableRecord(base.DBName, measureUnit, new string[] { "__AID" }, new object[] { AID });
                        measureID = measureUnit.ID;
                    }
                    dspWriter.CommitTransaction();
                    return measureID;
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public string RegisterTransactionItem(int AID,TransactionItems item, int[] costCenterID)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    string ret = null;
                    ItemCategory categ = GetItemCategory(item.categoryID);
                    string categCode = categ.Code;
                    List<string> af = categ.getApplicableAcountFields(item);
                    foreach (string f in getAllCategoryAccountField<ItemCategory>())
                    {
                        if (!af.Contains(f))
                            typeof(ItemCategory).GetField(f).SetValue(categ, -1);
                    }

                    bool newItem;
                    if (item.Code == null)
                    {
                        if (SysPars.manualItemCode)
                            throw new ServerUserMessage("The system is configured for manual item code, please provide item code");
                        else
                            item.Code = GetNextItemSerialCode("TransactionItems", "Code", categ.ID, categCode.Trim(), dspWriter).Trim();
                        newItem = true;
                    }
                    else
                    {
                        if (SysPars.manualItemCode)
                            newItem = ((int)dspWriter.ExecuteScalar("Select count(*) from {0}.dbo.TransactionItems where code='{1}'".format(this.DBName, item.Code))) == 0;
                        else
                            newItem = false;
                    }

                    if (newItem) //INSERT
                    {
                        
                        addCategorizedItemToCOA<ItemCategory, TransactionItems>(AID, categ, item);
                        dspWriter.InsertSingleTableRecord(m_DBName, item, new string[] { "__AID" }, new object[] { AID });
                        ret = item.Code;
                    }
                    else //UPDATE
                    {
                        logExistingData(AID, ".dbo.TransactionItems", "Code='" + item.Code + "'");
                        addCategorizedItemToCOA<ItemCategory, TransactionItems>(AID, categ, item);
                        dspWriter.UpdateSingleTableRecord(base.DBName, item, new string[] { "__AID" }, new object[] { AID });
                        ret = item.Code;
                    }


                    dspWriter.CommitTransaction();
                    return ret;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }

            }
        }
        public void DeleteTransactionItem(int AID, string code)
        {
            lock (dspWriter)
            {
                TransactionItems item = GetTransactionItems(code);

                try
                {
                    dspWriter.BeginTransaction();
                    logExistingData(AID, ".dbo.TransactionItems", "Code='" + code + "'");

                    DeleteItemAccounts(AID, item);
                    Project[] projects = GetAllProjects();

                    foreach (Project project in projects)
                    {
                        Dictionary<string, double> projectItems = new Dictionary<string, double>();
                        if (project.projectData != null && project.projectData.items != null)
                        {
                            for (int i = 0; i < project.projectData.items.Length; i++)
                            {
                                projectItems.Add(project.projectData.items[i], project.projectData.unitPrices[i]);
                            }
                        }
                        if (projectItems.ContainsKey(code))
                        {
                            projectItems.Remove(code);
                            string[] items = new string[projectItems.Keys.Count];
                            double[] unitPrices = new double[projectItems.Values.Count];
                            projectItems.Keys.CopyTo(items, 0);
                            projectItems.Values.CopyTo(unitPrices, 0);
                            project.projectData.items = items;
                            project.projectData.unitPrices = unitPrices;
                            RegisterProject(AID, project, true);
                        }
                    }
                    if ((int)dspWriter.ExecuteScalar(String.Format("select count(*) from {1}.dbo.TransactionItems where Code='{0}'", code, this.DBName)) > 0)
                    {
                        dspWriter.DeleteSingleTableRecrod<TransactionItems>(base.DBName, code);
                    }
                    else
                        throw new INTAPS.ClientServer.ServerUserMessage(String.Format("Error:Item with Code '{0}' not found!", code));
                    
                    //delete associated properties
                    int N;
                    foreach (Property p in getPropertiesByParentItemCode(code, 0, -1, out N))
                    {
                        deleteProperty(AID, p.id);
                    }
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public void ActivateTransactionItem(int AID, string itemCode)
        {
            lock (dspWriter)
            {
                TransactionItems item = GetTransactionItems(itemCode);

                try
                {
                    dspWriter.BeginTransaction();
                    logExistingData(AID, ".dbo.TransactionItems", "Code='" + itemCode + "'");
                    ActivateItemAccounts(AID, item);

                    dspWriter.ExecuteScalar(String.Format("Update TransactionItems Set Activated='True' where Code='{0}'", itemCode));
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        public void DeactivateTransactionItem(int AID, string itemCode)
        {
            lock (dspWriter)
            {
                TransactionItems item = GetTransactionItems(itemCode);

                try
                {
                    dspWriter.BeginTransaction();
                    logExistingData(AID, ".dbo.TransactionItems", "Code='" + itemCode + "'");
                    DeactivateItemAccounts(AID, item);
                    dspWriter.ExecuteScalar(String.Format("Update TransactionItems Set Activated='False' where Code='{0}'", itemCode));
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }
        private int GetItemCategoryParentAccountID(int categoryID, string column, INTAPS.RDBMS.SQLHelper dspWriter)
        {
            string sql = String.Format("select {0} From {2}.dbo.ItemCategory where ID={1}", column, categoryID, this.DBName);
            DataTable table = dspWriter.GetDataTable(sql);
            int accountID = -1;
            if (table != null)
            {
                if (table.Rows.Count > 0)
                {
                    accountID = (int)table.Rows[0][0];
                }
            }
            return accountID;
        }
        private void DeleteItemAccounts(int AID, TransactionItems item)
        {
            foreach (int accountID in item.getPrivateAccounts())
            {
                if (accountID != -1)
                    deleteAccountIFExists<Account>(AID, accountID);
            }
        }
        private void ActivateItemAccounts(int AID, TransactionItems item)
        {
            if (item.expenseAccountID != -1)
            {
                m_accounting.ActivateAcount<Account>(AID, item.expenseAccountID, DateTime.Now.Date);
            }
            if (item.prePaidExpenseAccountID != -1)
            {
                m_accounting.ActivateAcount<Account>(AID, item.prePaidExpenseAccountID, DateTime.Now.Date);
            }
            if (item.salesAccountID != -1)
            {
                m_accounting.ActivateAcount<Account>(AID, item.salesAccountID, DateTime.Now.Date);
            }
            if (item.unearnedRevenueAccountID != -1)
            {
                m_accounting.ActivateAcount<Account>(AID, item.unearnedRevenueAccountID, DateTime.Now.Date);
            }
            if (item.inventoryAccountID != -1)
            {
                m_accounting.ActivateAcount<Account>(AID, item.inventoryAccountID, DateTime.Now.Date);
            }
            if (item.finishedWorkAccountID != -1)
            {
                m_accounting.ActivateAcount<Account>(AID, item.finishedWorkAccountID, DateTime.Now.Date);
            }
            if (item.originalFixedAssetAccountID != -1)
            {
                m_accounting.ActivateAcount<Account>(AID, item.originalFixedAssetAccountID, DateTime.Now.Date);
            }
            if (item.depreciationAccountID != -1)
            {
                m_accounting.ActivateAcount<Account>(AID, item.depreciationAccountID, DateTime.Now.Date);
            }
            if (item.accumulatedDepreciationAccountID != -1)
            {
                m_accounting.ActivateAcount<Account>(AID, item.accumulatedDepreciationAccountID, DateTime.Now.Date);
            }
            if (item.pendingOrderAccountID != -1)
            {
                m_accounting.ActivateAcount<Account>(AID, item.pendingOrderAccountID, DateTime.Now.Date);
            }
            if (item.pendingDeliveryAcountID != -1)
            {
                m_accounting.ActivateAcount<Account>(AID, item.pendingDeliveryAcountID, DateTime.Now.Date);
            }
        }
        private void DeactivateItemAccounts(int AID, TransactionItems item)
        {
            if (item.expenseAccountID != -1)
            {
                m_accounting.DeactivateAccount<Account>(AID, item.expenseAccountID, DateTime.Now.Date);
            }
            if (item.prePaidExpenseAccountID != -1)
            {
                m_accounting.DeactivateAccount<Account>(AID, item.prePaidExpenseAccountID, DateTime.Now.Date);
            }
            if (item.salesAccountID != -1)
            {
                m_accounting.DeactivateAccount<Account>(AID, item.salesAccountID, DateTime.Now.Date);
            }
            if (item.unearnedRevenueAccountID != -1)
            {
                m_accounting.DeactivateAccount<Account>(AID, item.unearnedRevenueAccountID, DateTime.Now.Date);
            }
            if (item.inventoryAccountID != -1)
            {
                m_accounting.DeactivateAccount<Account>(AID, item.inventoryAccountID, DateTime.Now.Date);
            }
            if (item.finishedWorkAccountID != -1)
            {
                m_accounting.DeactivateAccount<Account>(AID, item.finishedWorkAccountID, DateTime.Now.Date);
            }
            if (item.originalFixedAssetAccountID != -1)
            {
                m_accounting.DeactivateAccount<Account>(AID, item.originalFixedAssetAccountID, DateTime.Now.Date);
            }
            if (item.depreciationAccountID != -1)
            {
                m_accounting.DeactivateAccount<Account>(AID, item.depreciationAccountID, DateTime.Now.Date);
            }
            if (item.accumulatedDepreciationAccountID != -1)
            {
                m_accounting.DeactivateAccount<Account>(AID, item.accumulatedDepreciationAccountID, DateTime.Now.Date);
            }
            if (item.pendingOrderAccountID != -1)
            {
                m_accounting.DeactivateAccount<Account>(AID, item.pendingOrderAccountID, DateTime.Now.Date);
            }
            if (item.pendingDeliveryAcountID != -1)
            {
                m_accounting.DeactivateAccount<Account>(AID, item.pendingDeliveryAcountID, DateTime.Now.Date);
            }
        }
        private void logExistingData(int AID, string qualifiedTableName, string criteria)
        {
            dspWriter.logDeletedData(AID, this.DBName + qualifiedTableName, criteria);
        }
        #region Permission Privilege
        internal bool CashierPermissionGranted(UserSessionData session)
        {
            if (session.Permissions.IsPermited("root/iERP/Cashier"))
                return true;
            else
                return false;
        }
        internal bool ConfigurationPermissionGranted(UserSessionData session)
        {
            if (session.Permissions.IsPermited("root/iERP/Configuration"))
                return true;
            else
                return false;
        }
        internal bool StandardViewPermissionGranted(UserSessionData session)
        {
            if (session.Permissions.IsPermited("root/iERP/StandardView"))
                return true;
            else
                return false;
        }
        internal bool OwnerViewPermissionGranted(UserSessionData session)
        {
            if (session.Permissions.IsPermited("root/iERP/OwnerView"))
                return true;
            else
                return false;
        }
        #endregion
        // BDE exposed
        public bool isBERPClientFileUpdated(string fileName, byte[] clientMD5)
        {
            string clientUpdateFolder = System.Configuration.ConfigurationManager.AppSettings["ClientUpdateFolder"];
            if (System.IO.Path.DirectorySeparatorChar != '\\')
                fileName=fileName.Replace('\\', System.IO.Path.DirectorySeparatorChar);
            byte[] fileBytes = System.IO.File.ReadAllBytes(clientUpdateFolder + fileName);

            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] serverMD5 = md5.ComputeHash(fileBytes);
            for (int i = 0; i < clientMD5.Length; i++)
                if (clientMD5[i] != serverMD5[i])
                    return true;
            return false;
        }
        // BDE exposed
        public byte[] getERPClientLatestFile(string fileName)
        {
            string clientUpdateFolder = System.Configuration.ConfigurationManager.AppSettings["ClientUpdateFolder"];
            return System.IO.File.ReadAllBytes(clientUpdateFolder + fileName);
        }
        // BDE exposed
        public string[] getAllClientFiles()
        {
            string clientUpdateFolder = System.Configuration.ConfigurationManager.AppSettings["ClientUpdateFolder"];
            string[] pathNames = System.IO.Directory.GetFiles(clientUpdateFolder, "*.*", System.IO.SearchOption.AllDirectories);
            for (int i = 0; i < pathNames.Length; i++)
                pathNames[i] = pathNames[i].Substring(clientUpdateFolder.Length);
            return pathNames;
        }
        Budget getBudgetInternal(INTAPS.RDBMS.SQLHelper reader, int budgetID)
        {
            Budget[] ret;
            ret = reader.GetSTRArrayByFilter<Budget>("id=" + budgetID);
            if (ret.Length == 0)
                return null;
            return ret[0];
        }
        Budget getBudgetInternal(INTAPS.RDBMS.SQLHelper reader, int costCenterID, long ticks, bool exact)
        {
            string sql;
            if (!exact)
            {
                sql = "Select min(ticksFrom) from {0}.dbo.Budget where costCenterID={1} and ticksFrom<={2} and (ticksTo=-1 or {2}<ticksTo)";
                object date = reader.ExecuteScalar(string.Format(sql, this.DBName, costCenterID, ticks));
                if (date is long)
                {
                    ticks = (long)date;
                }
                else
                    return null;
            }
            Budget[] ret = reader.GetSTRArrayByFilter<Budget>(string.Format("costCenterID={0} and ticksFrom={1}", costCenterID, ticks));
            if (ret.Length == 0)
                return null;
            return ret[0];

        }
        // BDE exposed
        public Budget getBudget(int budgetID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                Budget ret = getBudgetInternal(dspReader, budgetID);
                if (ret == null)
                    return null;
                ret.totalBudget = getBudgetTotalInternal(dspReader, ret.id);
                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        double getBudgetTotalInternal(INTAPS.RDBMS.SQLHelper reader, int budgetID)
        {
            object ret = reader.ExecuteScalar(string.Format("Select sum(amount) from {0}.dbo.BudgetEntry where budgetID={1}", this.DBName, budgetID));
            if (ret is double)
                return (double)ret;
            return 0;
        }
        // BDE exposed
        public Budget getBudget(int costCenterID, DateTime target, bool exact)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                Budget ret = getBudgetInternal(dspReader, costCenterID, target.Ticks, exact);
                if (ret == null)
                    return null;
                ret.totalBudget = getBudgetTotalInternal(dspReader, ret.id);
                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public Budget[] getAllBudgets(int costCenterID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                Budget[] ret = dspReader.GetSTRArrayByFilter<Budget>("costCenterID=" + costCenterID);
                foreach (Budget b in ret)
                    b.totalBudget = getBudgetTotalInternal(dspReader, b.id);
                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public void updateBudget(int AID, Budget budget, BudgetEntry[] entries)
        {
            validateBudgetEntries(budget, entries);

            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    Budget b = getBudgetInternal(dspWriter, budget.id);
                    if (b == null)
                        throw new ServerUserMessage("Budget doesn't exist in database");

                    //if (b.tagetDate > budget.tagetDate)
                    //    throw new ServerUserMessage("The cost center is already budgeted until " + b.tagetDate.ToString("MMM dd,yyyy"));
                    budget.id = b.id;
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.BudgetEntry", "budgetID=" + b.id);
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.Budget", "id=" + b.id);
                    dspWriter.UpdateSingleTableRecord<Budget>(this.DBName, budget, new string[] { "__AID" }, new object[] { AID });
                    dspWriter.Delete(this.DBName, "BudgetEntry", "budgetID=" + budget.id);
                    foreach (BudgetEntry e in entries)
                    {
                        e.budgetID = budget.id;
                        dspWriter.InsertSingleTableRecord<BudgetEntry>(this.DBName, e, new string[] { "__AID" }, new object[] { AID });
                    }
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }

        }
        // BDE exposed
        public void createBudget(int AID, Budget budget, BudgetEntry[] entries)
        {
            validateBudgetEntries(budget, entries);

            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    Budget b = getBudgetInternal(dspWriter, budget.costCenterID, budget.ticksFrom, false);
                    if (b != null)
                        throw new ServerUserMessage("The cost center is already budgeted for the given date range");
                    budget.id = AutoIncrement.GetKey("Accounting.budgetID");
                    dspWriter.InsertSingleTableRecord<Budget>(this.DBName, budget, new string[] { "__AID" }, new object[] { AID });
                    foreach (BudgetEntry e in entries)
                    {
                        e.budgetID = budget.id;
                        dspWriter.InsertSingleTableRecord<BudgetEntry>(this.DBName, e, new string[] { "__AID" }, new object[] { AID });
                    }
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }

        }
        // BDE exposed
        public void deleteBudget(int AID, int budgetID)
        {

            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    Budget b = getBudgetInternal(dspWriter, budgetID);
                    if (b == null)
                        throw new ServerUserMessage("Budget doesn't exist in database");

                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.BudgetEntry", "budgetID=" + budgetID);
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.Budget", "id=" + budgetID);

                    dspWriter.Delete(this.DBName, "BudgetEntry", "budgetID=" + budgetID);
                    dspWriter.DeleteSingleTableRecrod<Budget>(this.DBName, budgetID);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }

        }

        private void validateBudgetEntries(Budget budget, BudgetEntry[] entries)
        {
            for (int i = 0; i < entries.Length; i++)
            {
                //if (m_accounting.GetCostCenterAccount(budget.costCenterID, entries[i].accountID) == null)
                //throw new ServerUserMessage("Invalid account at entry " + i);
                for (int j = i + 1; j < entries.Length; j++)
                {
                    if (Accounting.isInSameHeirarchy<Account>(entries[i].accountID, entries[j].accountID))
                        throw new ServerUserMessage("Entry " + i + " conflicts with entry " + j);
                }
            }
        }
        // BDE exposed
        public BudgetEntry[] getBudgetEntries(int budgetID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<BudgetEntry>("budgetID=" + budgetID);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public bool isDocumentBudgetSet(int documentID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                var b = reader.ExecuteScalar("Select budgetDocumentID from {0}.dbo.BudgetedTransaction where documentID={1}".format(this.DBName, documentID));
                return (b is int) && ((int)b!=-1);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        internal void clearBudget(int AID, int transactionDocumentID)
        {
            dspWriter.Delete(this.DBName, "BudgetedTransaction", "documentID=" + transactionDocumentID);
        }
        public void setBudgetDocument(int AID, int transactionDocumentID, int budgetDocumentID)
        {
            dspWriter.Delete(this.DBName, "BudgetedTransaction", "documentID=" + transactionDocumentID);
            dspWriter.Insert(this.DBName, "BudgetedTransaction"
                , new string[] { "budgetDocumentID", "documentID" }
                , new object[] { budgetDocumentID, transactionDocumentID }
                );
        }
        // BDE exposed
        public AccountDocument[] getUnbudgetedDocuments(DocumentSearchPar pars, int index, int pageSize, out int N)
        {
            if (pars == null)
            {
                INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
                try
                {
                    string sql = @"SELECT    [Document].ID
                        FROM         {1}.dbo.[Document] LEFT OUTER JOIN
                      {0}.dbo.BudgetedTransaction ON [Document].ID = BudgetedTransaction.documentID
                      where BudgetedTransaction.budgetDocumentID is null
                     order by tranTicks desc";
                    int[] docID = helper.GetColumnArray<int>(sql.format(this.DBName, Accounting.DBName), 0, index, pageSize, out N);
                    AccountDocument[] ret = new AccountDocument[docID.Length];
                    for (int i = 0; i < docID.Length; i++)
                    {
                        ret[i] = Accounting.GetAccountDocument(docID[i], true);
                    }
                    return ret;
                }
                finally
                {
                    base.ReleaseHelper(helper);
                }
            }
            else
            {
                int[] budgetedDocumentID;
                INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
                try
                {
                    budgetedDocumentID = helper.GetColumnArray<int>("Select documentID from {0}.dbo.BudgetedTransaction order by documentID".format(this.DBName));
                    return Accounting.GetAccountDocuments(pars, delegate(AccountDocument doc) { return Array.BinarySearch<int>(budgetedDocumentID, doc.AccountDocumentID) <0; }, index, pageSize, out N);
                }
                finally
                {
                    base.ReleaseHelper(helper);
                }
            }

        }
        // BDE exposed
        public ClosedRange[] getBookClosings()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                ClosedRange[] ret = dspReader.GetSTRArrayByFilter<ClosedRange>("");
                foreach (ClosedRange r in ret)
                {
                    AuditInfo ai = ApplicationServer.SecurityBDE.getAuditInfo(r.AID);
                    if (ai != null)
                    {
                        r.actionDate = ai.dateTime;
                        Employee emp = m_Payroll.GetEmployeeByLoginName(ai.userName);
                        if (emp == null)
                            r.agentName = ai.userName;
                        else
                            r.agentName = emp.employeeName;
                    }
                }
                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public DateTime getTransactionsClosedUpto()
        {
            INTAPS.RDBMS.SQLHelper reader = this.GetReaderHelper();
            try
            {

                object max = reader.ExecuteScalar(string.Format("Select max(toDate) from {0}.dbo.ClosedRange", this.DBName));
                if (max is DateTime)
                    return (DateTime)max;
                return DateTime.MinValue;
            }
            finally
            {
                this.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public void closeBookForTransaction(int AID, ClosedRange range)
        {
            range.toDate = range.toDate.Date;
            DateTime time = getTransactionsClosedUpto();
            if (range.toDate <= time)
                throw new ServerUserMessage("Book closed for transactions upto " + range.toDate);
            lock (dspWriter)
            {
                range.AID = AID;
                dspWriter.InsertSingleTableRecord<ClosedRange>(this.DBName, range);
            }
        }
        // BDE exposed
        public void updateBookClosingForTransaction(int AID, ClosedRange range)
        {
            range.toDate = range.toDate.Date;
            range.AID = AID;
            DateTime time = getTransactionsClosedUpto();
            if (time == DateTime.MinValue)
                throw new ServerUserMessage("The book is not closed");

            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    dspWriter.DeleteSingleTableRecrod<ClosedRange>(this.DBName, time);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                }
            }
        }
        // BDE exposed
        public void openBookForTransaction(int AID)
        {
            DateTime time = getTransactionsClosedUpto();
            if (time == DateTime.MinValue)
                throw new ServerUserMessage("The book is not closed");
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    dspWriter.DeleteSingleTableRecrod<ClosedRange>(this.DBName, time);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                }
            }
        }

        public bool AllowNegativeTransactionPost()
        {
            string tran = SysPars.AllowNegativeTransaction;
            if (tran != null && tran.ToUpper().Equals("TRUE"))
            {
                return true;
            }
            else
                return false;
        }

        public void SaveStockLevelConfiguration(int AID, StockLevelConfiguration[] config)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    StockLevelConfiguration[] all = getStockLevelConfiguration();
                    foreach (StockLevelConfiguration conf in all)
                    {
                        dspWriter.logDeletedData(AID, string.Format("{0}.dbo.StockLevelConfiguration", this.DBName), "itemCode='" + conf.itemCode + "'");
                    }
                    dspWriter.ExecuteScalar(string.Format("Delete From {0}.dbo.StockLevelConfiguration", this.DBName));

                    foreach (StockLevelConfiguration conf in config)
                    {
                        dspWriter.InsertSingleTableRecord<StockLevelConfiguration>(this.DBName, conf, new string[] { "__AID" }, new object[] { AID });
                    }
                    dspWriter.CommitTransaction();
                }

                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
            }
        }

        public StockLevelConfiguration[] getStockLevelConfiguration()
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                var config = helper.GetSTRArrayByFilter<StockLevelConfiguration>(null);
                return config;
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }

       
    }
}