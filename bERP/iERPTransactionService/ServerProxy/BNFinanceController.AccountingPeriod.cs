using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using Microsoft.AspNetCore.Mvc;

namespace BIZNET.iERP.Server
{
    partial class BNFinanceController
    {
        #region GenerateAccountingPeriodSeries
        public class GenerateAccountingPeriodSeriesPar
        {
            public string sessionID; public Type generator; public string type; public DateTime from; public DateTime to;
        }

        [HttpPost]
        public ActionResult GenerateAccountingPeriodSeries(GenerateAccountingPeriodSeriesPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).GenerateAccountingPeriodSeries(par.generator, par.type, par.from, par.to);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAccountingPeriod
        public class GetAccountingPeriodPar
        {
            public string sessionID; public string periodType; public int id;
        }

        [HttpPost]
        public ActionResult GetAccountingPeriod(GetAccountingPeriodPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetAccountingPeriod(par.periodType, par.id));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAccountingPeriod2
        public class GetAccountingPeriod2Par
        {
            public string sessionID; public string periodType; public DateTime date;
        }

        [HttpPost]
        public ActionResult GetAccountingPeriod2(GetAccountingPeriod2Par par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetAccountingPeriod(par.periodType, par.date));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAccountingPeriodsInFiscalYear
        public class GetAccountingPeriodsInFiscalYearPar
        {
            public string sessionID; public string periodType; public int year;
        }

        [HttpPost]
        public ActionResult GetAccountingPeriodsInFiscalYear(GetAccountingPeriodsInFiscalYearPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetAccountingPeriodsInFiscalYear(par.periodType, par.year));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetTouchedPeriods
        public class GetTouchedPeriodsPar
        {
            public string sessionID; public string periodType; public DateTime fromDate; public DateTime toDate;
        }

        [HttpPost]
        public ActionResult GetTouchedPeriods(GetTouchedPeriodsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetTouchedPeriods(par.periodType, par.fromDate, par.toDate));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetNextAccountingPeriod
        public class GetNextAccountingPeriodPar
        {
            public string sessionID; public string periodType; public int id;
        }

        [HttpPost]
        public ActionResult GetNextAccountingPeriod(GetNextAccountingPeriodPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetNextAccountingPeriod(par.periodType, par.id));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPreviousAccountingPeriod
        public class GetPreviousAccountingPeriodPar
        {
            public string sessionID; public string periodType; public int id;
        }

        [HttpPost]
        public ActionResult GetPreviousAccountingPeriod(GetPreviousAccountingPeriodPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetPreviousAccountingPeriod(par.periodType, par.id));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
    }
}
