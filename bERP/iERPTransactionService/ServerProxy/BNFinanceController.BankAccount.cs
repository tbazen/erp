using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Server
{
    public partial class BNFinanceController
    {
        #region GetBankAccount
        public class GetBankAccountPar
        {
            public string sessionID; public int mainAccountID;
        }

        [HttpPost]
        public ActionResult GetBankAccount(GetBankAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetBankAccount(par.mainAccountID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllBankAccounts

        public class GetAllBankAccountsPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult GetAllBankAccounts(GetAllBankAccountsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetAllBankAccounts());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteBankAccount
        public class DeleteBankAccountPar
        {
            public string sessionID; public int accountID;
        }

        [HttpPost]
        public ActionResult DeleteBankAccount(DeleteBankAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).DeleteBankAccount(par.accountID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CreateBankAccount
        public class CreateBankAccountPar
        {
            public string sessionID; public BankAccountInfo account; public int costCenterID;
        }

        [HttpPost]
        public ActionResult CreateBankAccount(CreateBankAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).CreateBankAccount(par.account, par.costCenterID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ActivateBankAccount
        public class ActivateBankAccountPar
        {
            public string sessionID; public int mainCsAccount;
        }

        [HttpPost]
        public ActionResult ActivateBankAccount(ActivateBankAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).ActivateBankAccount(par.mainCsAccount);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeactivateBankAccount
        public class DeactivateBankAccountPar
        {
            public string sessionID; public int mainCsAccount;
        }

        [HttpPost]
        public ActionResult DeactivateBankAccount(DeactivateBankAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).DeactivateBankAccount(par.mainCsAccount);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
    }
}
