using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Server
{
    public partial class BNFinanceController
    {
        #region GetCashAccount
        public class GetCashAccountPar
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult GetCashAccount(GetCashAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetCashAccount(par.code));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCashAccount2
        public class GetCashAccount2Par
        {
            public string sessionID; public int csAccountID;
        }

        [HttpPost]
        public ActionResult GetCashAccount2(GetCashAccount2Par par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetCashAccount(par.csAccountID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllCashAccounts
        public class GetAllCashAccountsPar
        {
            public string sessionID; public bool includeExpenseAccount;
        }

        [HttpPost]
        public ActionResult GetAllCashAccounts(GetAllCashAccountsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetAllCashAccounts(par.includeExpenseAccount));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteCashAccount
        public class DeleteCashAccountPar
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult DeleteCashAccount(DeleteCashAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).DeleteCashAccount(par.code);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CreateCashAccount
        public class CreateCashAccountPar
        {
            public string sessionID; public CashAccount account; public int costCenterID;
        }

        [HttpPost]
        public ActionResult CreateCashAccount(CreateCashAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).CreateCashAccount(par.account, par.costCenterID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ActivateCashAccount
        public class ActivateCashAccountPar
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult ActivateCashAccount(ActivateCashAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).ActivateCashAccount(par.code);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeactivateCashAccount
        public class DeactivateCashAccountPar
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult DeactivateCashAccount(DeactivateCashAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).DeactivateCashAccount(par.code);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
    }
}
