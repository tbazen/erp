using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Server
{
    public partial class BNFinanceController
    {
        #region RegisterCustomer
        public class RegisterCustomerPar
        {
            public string sessionID; public TradeRelation customer;
        }

        [HttpPost]
        public ActionResult RegisterCustomer(RegisterCustomerPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).RegisterCustomer(par.customer));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCustomer
        public class GetCustomerPar
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult GetCustomer(GetCustomerPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetCustomer(par.code));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllCustomers
        public class GetAllCustomersPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult GetAllCustomers(GetAllCustomersPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetAllCustomers());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SearchCustomers
        public class SearchCustomersPar
        {
            public string sessionID; public int index; public int NoOfPages; public object[] criteria; public string[] column;
        }
        public class SearchCustomersOut
        {
            public int NoOfRecords;
            public TradeRelation[] _ret;
        }

        [HttpPost]
        public ActionResult SearchCustomers(SearchCustomersPar par)
        {
            try
            {
                var _ret = new SearchCustomersOut();
                SetSessionObject(par.sessionID);
                _ret._ret = ((iERPTransactionService)SessionObject).SearchCustomers(par.index, par.NoOfPages, par.criteria, par.column, out _ret.NoOfRecords);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteCustomer
        public class DeleteCustomerPar
        {
            public string sessionID; public string[] code;
        }

        [HttpPost]
        public ActionResult DeleteCustomer(DeleteCustomerPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).DeleteCustomer(par.code);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region IsCustomerInvolvedInTransactions
        public class IsCustomerInvolvedInTransactionsPar
        {
            public string sessionID; public string customerCode;
        }

        [HttpPost]
        public ActionResult IsCustomerInvolvedInTransactions(IsCustomerInvolvedInTransactionsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).IsCustomerInvolvedInTransactions(par.customerCode));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ActivateCustomer
        public class ActivateCustomerPar
        {
            public string sessionID; public string customerCode;
        }

        [HttpPost]
        public ActionResult ActivateCustomer(ActivateCustomerPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).ActivateCustomer(par.customerCode);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeactivateCustomer
        public class DeactivateCustomerPar
        {
            public string sessionID; public string customerCode;
        }

        [HttpPost]
        public ActionResult DeactivateCustomer(DeactivateCustomerPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).DeactivateCustomer(par.customerCode);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllUnreturnedBonds
        public class GetAllUnreturnedBondsPar
        {
            public string sessionID; public string query;
        }

        [HttpPost]
        public ActionResult GetAllUnreturnedBonds(GetAllUnreturnedBondsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetAllUnreturnedBonds(par.query));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion

    }
}
