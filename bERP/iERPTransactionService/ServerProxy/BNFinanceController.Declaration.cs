using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Data;
using Microsoft.AspNetCore.Mvc;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    partial class BNFinanceController
    {
        #region GetCandidatesForDeclaration
        public class GetCandidatesForDeclarationPar
        {
            public string sessionID; public int declarationType; public DateTime declarationDate; public int periodID;
        }
        public class GetCandidatesForDeclarationOut
        {
            public int[] taxCenterID; public double[] taxAmount;
            public int[] _ret;
        }

        [HttpPost]
        public ActionResult GetCandidatesForDeclaration(GetCandidatesForDeclarationPar par)
        {
            try
            {
                var _ret = new GetCandidatesForDeclarationOut();
                SetSessionObject(par.sessionID);
                _ret._ret = ((iERPTransactionService)SessionObject).GetCandidatesForDeclaration(par.declarationType, par.declarationDate, par.periodID, out _ret.taxCenterID, out _ret.taxAmount);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region PreviewDeclaration
        public class PreviewDeclarationPar
        {
            public string sessionID; public int declarationType; public DateTime declarationDate; public int periodID; public int taxCenterID; public int[] documents; public int[] rejectedDocuments; public bool newDeclaration;
        }
        public class PreviewDeclarationOut
        {
            public double total;
            public BinObject _ret;
        }

        [HttpPost]
        public ActionResult PreviewDeclaration(PreviewDeclarationPar par)
        {
            try
            {
                var _ret = new PreviewDeclarationOut();
                SetSessionObject(par.sessionID);
                _ret._ret = new BinObject(((iERPTransactionService)SessionObject).PreviewDeclaration(par.declarationType, par.declarationDate, par.periodID, par.taxCenterID, par.documents, par.rejectedDocuments, out _ret.total, par.newDeclaration));
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SaveTaxDeclaration
        public class SaveTaxDeclarationPar
        {
            public string sessionID; public int declarationType; public DateTime declarationDate; public int periodID; public int[] documents; public int taxCenterID;
        }

        [HttpPost]
        public ActionResult SaveTaxDeclaration(SaveTaxDeclarationPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).SaveTaxDeclaration(par.declarationType, par.declarationDate, par.periodID, par.documents, par.taxCenterID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetTaxDeclarations
        public class GetTaxDeclarationsPar
        {
            public string sessionID; public int declarationType; public int period1; public int period2;
        }

        [HttpPost]
        public ActionResult GetTaxDeclarations(GetTaxDeclarationsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(new BinObject(((iERPTransactionService)SessionObject).GetTaxDeclarations(par.declarationType, par.period1, par.period2)));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetTaxDeclaration
        public class GetTaxDeclarationPar
        {
            public string sessionID; public int declatationID;
        }

        [HttpPost]
        public ActionResult GetTaxDeclaration(GetTaxDeclarationPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(new BinObject(((iERPTransactionService)SessionObject).GetTaxDeclaration(par.declatationID)));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetTaxDeclaration2
        public class GetTaxDeclaration2Par
        {
            public string sessionID; public int declarationType; public int periodID;
        }

        [HttpPost]
        public ActionResult GetTaxDeclaration2(GetTaxDeclaration2Par par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetTaxDeclaration(par.declarationType, par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region HasTaxDeclarationBegan
        public class HasTaxDeclarationBeganPar
        {
            public string sessionID; public int declarationType;
        }

        [HttpPost]
        public ActionResult HasTaxDeclarationBegan(HasTaxDeclarationBeganPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).HasTaxDeclarationBegan(par.declarationType));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteDeclaration
        public class DeleteDeclarationPar
        {
            public string sessionID; public int declarationID;
        }

        [HttpPost]
        public ActionResult DeleteDeclaration(DeleteDeclarationPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).DeleteDeclaration(par.declarationID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateDeclaration
        public class UpdateDeclarationPar
        {
            public string sessionID; public TaxDeclaration decl;
        }

        [HttpPost]
        public ActionResult UpdateDeclaration(UpdateDeclarationPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).UpdateDeclaration(par.decl);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CountPaidTaxDeclarations
        public class CountPaidTaxDeclarationsPar
        {
            public string sessionID; public int declarationType;
        }

        [HttpPost]
        public ActionResult CountPaidTaxDeclarations(CountPaidTaxDeclarationsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).CountPaidTaxDeclarations(par.declarationType));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region IsPreviousDeclarationPeriodConfirmed
        public class IsPreviousDeclarationPeriodConfirmedPar
        {
            public string sessionID; public int declarationType; public int currPeriodID;
        }

        [HttpPost]
        public ActionResult IsPreviousDeclarationPeriodConfirmed(IsPreviousDeclarationPeriodConfirmedPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).IsPreviousDeclarationPeriodConfirmed(par.declarationType, par.currPeriodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetTaxCenters
        public class GetTaxCentersPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult GetTaxCenters(GetTaxCentersPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetTaxCenters());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetTaxCenter
        public class GetTaxCenterPar
        {
            public string sessionID; public int id;
        }

        [HttpPost]
        public ActionResult GetTaxCenter(GetTaxCenterPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetTaxCenter(par.id));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
    }
}