using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Payroll;
using INTAPS.Accounting;
using Microsoft.AspNetCore.Mvc;

namespace BIZNET.iERP.Server
{
    public partial class BNFinanceController
    {
        #region RegisterEmployee
        public class RegisterEmployeePar
        {
            public string sessionID; public Employee employee; public byte[] picture; public bool updateWorkingData; public int[] otherAccounts; public int periodID; public Data_RegularTime regular; public OverTimeData overtime; public SingleValueData[] othersData; public int[] singleValueFormulaID;
        }

        [HttpPost]
        public ActionResult RegisterEmployee(RegisterEmployeePar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).RegisterEmployee(par.employee, par.picture, par.updateWorkingData, par.otherAccounts, par.periodID, par.regular, par.overtime, par.othersData, par.singleValueFormulaID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteEmployee
        public class DeleteEmployeePar
        {
            public string sessionID; public int ID;
        }

        [HttpPost]
        public ActionResult DeleteEmployee(DeleteEmployeePar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).DeleteEmployee(par.ID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteEmployeeAccount
        public class DeleteEmployeeAccountPar
        {
            public string sessionID; public int empID; public int accountID;
        }

        [HttpPost]
        public ActionResult DeleteEmployeeAccount(DeleteEmployeeAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).DeleteEmployeeAccount(par.empID, par.accountID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region FireEmployee
        public class FireEmployeePar
        {
            public string sessionID; public int ID; public DateTime dismissalDate;
        }

        [HttpPost]
        public ActionResult FireEmployee(FireEmployeePar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).FireEmployee(par.ID, par.dismissalDate);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region EnrollEmployee
        public class EnrollEmployeePar
        {
            public string sessionID; public int ID; public DateTime enrollDate;
        }

        [HttpPost]
        public ActionResult EnrollEmployee(EnrollEmployeePar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).EnrollEmployee(par.ID, par.enrollDate);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllEmployeesExcept
        public class GetAllEmployeesExceptPar
        {
            public string sessionID; public int[] employeeID;
        }

        [HttpPost]
        public ActionResult GetAllEmployeesExcept(GetAllEmployeesExceptPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetAllEmployeesExcept(par.employeeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPaidSalaryDocs
        public class GetPaidSalaryDocsPar
        {
            public string sessionID; public int empID; public PayrollPeriod period;
        }

        [HttpPost]
        public ActionResult GetPaidSalaryDocs(GetPaidSalaryDocsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetPaidSalaryDocs(par.empID, par.period));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetUnclaimedSalaryBalance
        public class GetUnclaimedSalaryBalancePar
        {
            public string sessionID; public int empID; public PayrollPeriod period;
        }

        [HttpPost]
        public ActionResult GetUnclaimedSalaryBalance(GetUnclaimedSalaryBalancePar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetUnclaimedSalaryBalance(par.empID, par.period));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetEmployeePaidSalaryTotal
        public class GetEmployeePaidSalaryTotalPar
        {
            public string sessionID; public int empID; public int periodID;
        }

        [HttpPost]
        public ActionResult GetEmployeePaidSalaryTotal(GetEmployeePaidSalaryTotalPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetEmployeePaidSalaryTotal(par.empID, par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetEmployeePaidSalaries
        public class GetEmployeePaidSalariesPar
        {
            public string sessionID; public int empID; public int periodID;
        }

        [HttpPost]
        public ActionResult GetEmployeePaidSalaries(GetEmployeePaidSalariesPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetEmployeePaidSalaries(par.empID, par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetGroupPayrollUnclaimed
        public class GetGroupPayrollUnclaimedPar
        {
            public string sessionID; public int periodID; public int taxCenter;
        }

        [HttpPost]
        public ActionResult GetGroupPayrollUnclaimed(GetGroupPayrollUnclaimedPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetGroupPayrollUnclaimed(par.periodID, par.taxCenter));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GenerateAndPostPayroll
        public class GenerateAndPostPayrollPar
        {
            public string sessionID; public int[] employees; public int periodID; public DateTime date;
        }

        [HttpPost]
        public ActionResult GenerateAndPostPayroll(GenerateAndPostPayrollPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GenerateAndPostPayroll(par.employees, par.periodID, par.date));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region EmployeeExists
        public class EmployeeExistsPar
        {
            public string sessionID; public int id;
        }

        [HttpPost]
        public ActionResult EmployeeExists(EmployeeExistsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).EmployeeExists(par.id));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SetWorkingData
        public class SetWorkingDataPar
        {
            public string sessionID; public int employeeID; public int periodID; public Data_RegularTime regular; public OverTimeData overtime; public SingleValueData[] othersData; public int[] singleValueFormulaID;
        }

        [HttpPost]
        public ActionResult SetWorkingData(SetWorkingDataPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).SetWorkingData(par.employeeID, par.periodID, par.regular, par.overtime, par.othersData, par.singleValueFormulaID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region setPermanentWorkingData
        public class setPermanentWorkingDataPar
        {
            public string sessionID; public int employeeID; public int periodID; public int formulaID; public PayrollComponentData singleValueData; public INTAPS.ClientServer.BinObject additionalData;
        }

        [HttpPost]
        public ActionResult setPermanentWorkingData(setPermanentWorkingDataPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).setPermanentWorkingData(par.employeeID, par.periodID, par.formulaID, par.singleValueData, par.additionalData.Deserialized());

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetGroupPayrollTotalSalaryPaid
        public class GetGroupPayrollTotalSalaryPaidPar
        {
            public string sessionID; public int periodID; public int taxCenter;
        }

        [HttpPost]
        public ActionResult GetGroupPayrollTotalSalaryPaid(GetGroupPayrollTotalSalaryPaidPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetGroupPayrollTotalSalaryPaid(par.periodID, par.taxCenter));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region closeLoan
        public class closeLoanPar
        {
            public string sessionID; public int loanDocumentID;
        }

        [HttpPost]
        public ActionResult closeLoan(closeLoanPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).closeLoan(par.loanDocumentID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
    }
}
