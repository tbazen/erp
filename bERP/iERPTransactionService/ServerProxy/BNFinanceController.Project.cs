using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Payroll;
using Microsoft.AspNetCore.Mvc;

namespace BIZNET.iERP.Server
{
    public partial class BNFinanceController
    {
        #region RegisterProject
        public class RegisterProjectPar
        {
            public string sessionID;public Project project;public bool logData;
        }
        
[HttpPost]
public ActionResult RegisterProject(RegisterProjectPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
return Json(((iERPTransactionService)SessionObject).RegisterProject(par.project, par.logData));
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region GetProjectInfo 
        public class GetProjectInfoPar
        {
            public string sessionID;public string code;
        }
        
[HttpPost]
public ActionResult GetProjectInfo(GetProjectInfoPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
return Json(((iERPTransactionService)SessionObject).GetProjectInfo(par.code));
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region GetAllProjects
        public class GetAllProjectsPar
        {
            public string sessionID;
        }
        
[HttpPost]
public ActionResult GetAllProjects(GetAllProjectsPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
return Json(((iERPTransactionService)SessionObject).GetAllProjects());
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region DeleteProject
        public class DeleteProjectPar
        {
            public string sessionID;public string code;
        }
        
[HttpPost]
public ActionResult DeleteProject(DeleteProjectPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
            ((iERPTransactionService)SessionObject).DeleteProject(par.code);

return Ok();
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region ActivateProject
        public class ActivateProjectPar
        {
            public string sessionID;public string code;
        }
        
[HttpPost]
public ActionResult ActivateProject(ActivateProjectPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
            ((iERPTransactionService)SessionObject).ActivateProject(par.code);

return Ok();
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region DeactivateProject
        public class DeactivateProjectPar
        {
            public string sessionID;public string code;
        }
        
[HttpPost]
public ActionResult DeactivateProject(DeactivateProjectPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
            ((iERPTransactionService)SessionObject).DeactivateProject(par.code);

return Ok();
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region CreateProjectDivision
        public class CreateProjectDivisionPar
        {
            public string sessionID;public int projectCostCenterID;public string divisionName;
        }
        
[HttpPost]
public ActionResult CreateProjectDivision(CreateProjectDivisionPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
return Json(((iERPTransactionService)SessionObject).CreateProjectDivision(par.projectCostCenterID, par.divisionName));
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region CreateProjectMachinaryAndVehicle
        public class CreateProjectMachinaryAndVehiclePar
        {
            public string sessionID;public int projectCostCenterID;public string name;
        }
        
[HttpPost]
public ActionResult CreateProjectMachinaryAndVehicle(CreateProjectMachinaryAndVehiclePar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
return Json(((iERPTransactionService)SessionObject).CreateProjectMachinaryAndVehicle(par.projectCostCenterID, par.name));
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region AddItemAccountsToProject
        public class AddItemAccountsToProjectPar
        {
            public string sessionID;public TransactionItems[] items;public string projectCode;
        }
        
[HttpPost]
public ActionResult AddItemAccountsToProject(AddItemAccountsToProjectPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
            ((iERPTransactionService)SessionObject).AddItemAccountsToProject(par.items, par.projectCode);

return Ok();
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region AddEmployeeAccountsToProject
        public class AddEmployeeAccountsToProjectPar
        {
            public string sessionID;public Employee[] employees;public string projectCode;public int divisionCostCenterID;
        }
        
[HttpPost]
public ActionResult AddEmployeeAccountsToProject(AddEmployeeAccountsToProjectPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
            ((iERPTransactionService)SessionObject).AddEmployeeAccountsToProject(par.employees, par.projectCode, par.divisionCostCenterID);

return Ok();
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region AddDivisionToProject
        public class AddDivisionToProjectPar
        {
            public string sessionID;public string divisionName;public string projectCode;
        }
        
[HttpPost]
public ActionResult AddDivisionToProject(AddDivisionToProjectPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
            ((iERPTransactionService)SessionObject).AddDivisionToProject(par.divisionName, par.projectCode);

return Ok();
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region AddMachinaryAndVehicleToProject
        public class AddMachinaryAndVehicleToProjectPar
        {
            public string sessionID;public string name;public string projectCode;
        }
        
[HttpPost]
public ActionResult AddMachinaryAndVehicleToProject(AddMachinaryAndVehicleToProjectPar par)
{
    try
    {
                     SetSessionObject(par.sessionID);
             ((iERPTransactionService)SessionObject).AddMachinaryAndVehicleToProject(par.name, par.projectCode);

return Ok();
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region AddStoreToProject
        public class AddStoreToProjectPar
        {
            public string sessionID;public StoreInfo store;public string projectCode;
        }
        
[HttpPost]
public ActionResult AddStoreToProject(AddStoreToProjectPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
return Json(((iERPTransactionService)SessionObject).AddStoreToProject(par.store, par.projectCode));
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region AddCashAccountToProject
        public class AddCashAccountToProjectPar
        {
            public string sessionID;public CashAccount cashAccount;public string projectCode;public int divisionCostCenterID;
        }
        
[HttpPost]
public ActionResult AddCashAccountToProject(AddCashAccountToProjectPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
return Json(((iERPTransactionService)SessionObject).AddCashAccountToProject(par.cashAccount, par.projectCode,par.divisionCostCenterID));
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region AddBankAccountToProject
        public class AddBankAccountToProjectPar
        {
            public string sessionID;public BankAccountInfo bankAccount;public string projectCode;public int divisionCostCenterID;
        }
        
[HttpPost]
public ActionResult AddBankAccountToProject(AddBankAccountToProjectPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
return Json(((iERPTransactionService)SessionObject).AddBankAccountToProject(par.bankAccount, par.projectCode,par.divisionCostCenterID));
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region RemoveEmployeeFromProject
        public class RemoveEmployeeFromProjectPar
        {
            public string sessionID;public int employeeID;public string projectCode;
        }
        
[HttpPost]
public ActionResult RemoveEmployeeFromProject(RemoveEmployeeFromProjectPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
            ((iERPTransactionService)SessionObject).RemoveEmployeeFromProject(par.employeeID,par.projectCode);

return Ok();
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region RemoveDivisionFromProject
        public class RemoveDivisionFromProjectPar
        {
            public string sessionID;public int divisionCostCenterID;public string projectCode;
        }
        
[HttpPost]
public ActionResult RemoveDivisionFromProject(RemoveDivisionFromProjectPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
            ((iERPTransactionService)SessionObject).RemoveDivisionFromProject(par.divisionCostCenterID, par.projectCode);

return Ok();
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region RemoveMachinaryAndVehicleFromProject
        public class RemoveMachinaryAndVehicleFromProjectPar
        {
            public string sessionID;public int mvCostCenterID;public string projectCode;
        }
        
[HttpPost]
public ActionResult RemoveMachinaryAndVehicleFromProject(RemoveMachinaryAndVehicleFromProjectPar par)
{
    try
    {
                     SetSessionObject(par.sessionID);
             ((iERPTransactionService)SessionObject).RemoveMachinaryAndVehicleFromProject(par.mvCostCenterID, par.projectCode);

return Ok();
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region RemoveStoreFromProject
        public class RemoveStoreFromProjectPar
        {
            public string sessionID;public int storeCostCenterID;public string storeCode;public string projectCode;
        }
        
[HttpPost]
public ActionResult RemoveStoreFromProject(RemoveStoreFromProjectPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
            ((iERPTransactionService)SessionObject).RemoveStoreFromProject(par.storeCostCenterID, par.storeCode, par. projectCode);

return Ok();
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region RemoveItemFromProject
        public class RemoveItemFromProjectPar
        {
            public string sessionID;public string itemCode;public string projectCode;
        }
        
[HttpPost]
public ActionResult RemoveItemFromProject(RemoveItemFromProjectPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
            ((iERPTransactionService)SessionObject).RemoveItemFromProject(par.itemCode, par.projectCode);

return Ok();
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region RemoveCashAccountFromProject
        public class RemoveCashAccountFromProjectPar
        {
            public string sessionID;public int csAccountID;public string code;public string projectCode;
        }
        
[HttpPost]
public ActionResult RemoveCashAccountFromProject(RemoveCashAccountFromProjectPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
            ((iERPTransactionService)SessionObject).RemoveCashAccountFromProject(par.csAccountID,par.code,par.projectCode);

return Ok();
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region GetProjectInfo2
        public class GetProjectInfo2Par
        {
            public string sessionID;public int projectCostCenterID;
        }
        
[HttpPost]
public ActionResult GetProjectInfo2(GetProjectInfo2Par par)
{
    try
    {
                    SetSessionObject(par.sessionID);
return Json(((iERPTransactionService)SessionObject).GetProjectInfo(par.projectCostCenterID));
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region RemoveBankAccountFromProject
        public class RemoveBankAccountFromProjectPar
        {
            public string sessionID;public int csAccountID;public string projectCode;
        }
        
[HttpPost]
public ActionResult RemoveBankAccountFromProject(RemoveBankAccountFromProjectPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
            ((iERPTransactionService)SessionObject).RemoveBankAccountFromProject(par.csAccountID, par.projectCode);

return Ok();
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
    }
}
        