using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Server
{
    public partial class BNFinanceController
    {
        #region RegisterStore
        public class RegisterStorePar
        {
            public string sessionID; public StoreInfo store; public int costCenterParentID;
        }

        [HttpPost]
        public ActionResult RegisterStore(RegisterStorePar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).RegisterStore(par.store, par.costCenterParentID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetStoreInfo
        public class GetStoreInfoPar
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult GetStoreInfo(GetStoreInfoPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetStoreInfo(par.code));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllStores
        public class GetAllStoresPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult GetAllStores(GetAllStoresPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetAllStores());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteStore
        public class DeleteStorePar
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult DeleteStore(DeleteStorePar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).DeleteStore(par.code);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ActivateStore
        public class ActivateStorePar
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult ActivateStore(ActivateStorePar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).ActivateStore(par.code);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeactivateStore
        public class DeactivateStorePar
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult DeactivateStore(DeactivateStorePar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).DeactivateStore(par.code);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
    }
}
