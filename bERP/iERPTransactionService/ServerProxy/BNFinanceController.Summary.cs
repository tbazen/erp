using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Data;
using Microsoft.AspNetCore.Mvc;

namespace BIZNET.iERP.Server
{
    partial class BNFinanceController
    {
        #region renderSummaryTable
        public class renderSummaryTablePar
        {
            public string sessionID; public string summarySettingField; public int costCenterID; public DateTime time1; public DateTime time2;
        }
        public class renderSummaryTableOut
        {
            public int maxLevel;
            public string _ret;
        }

        [HttpPost]
        public ActionResult renderSummaryTable(renderSummaryTablePar par)
        {
            try
            {
                var _ret = new renderSummaryTableOut();
                SetSessionObject(par.sessionID);
                _ret._ret = ((iERPTransactionService)SessionObject).renderSummaryTable(par.summarySettingField, par.costCenterID, par.time1, par.time2, out _ret.maxLevel);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetRelation
        public class GetRelationPar
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult GetRelation(GetRelationPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetRelation(par.code));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCountries
        public class GetCountriesPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult GetCountries(GetCountriesPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetCountries());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCountryById
        public class GetCountryByIdPar
        {
            public string sessionID; public short id;
        }

        [HttpPost]
        public ActionResult GetCountryById(GetCountryByIdPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetCountryById(par.id));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region RegisterRelation
        public class RegisterRelationPar
        {
            public string sessionID; public TradeRelation relation;
        }

        [HttpPost]
        public ActionResult RegisterRelation(RegisterRelationPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).RegisterRelation(par.relation));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDepreciation
        public class GetDepreciationPar
        {
            public string sessionID; public int costCenteriD; public string itemCode; public DateTime dateTime;
        }

        [HttpPost]
        public ActionResult GetDepreciation(GetDepreciationPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetDepreciation(par.costCenteriD, par.itemCode, par.dateTime));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDepreciation2
        public class GetDepreciation2Par
        {
            public string sessionID; public int costCenteriD; public string itemCode; public DateTime dateTime;
        }
        public class GetDepreciation2Out
        {
            public double rate; public double year;
            public double _ret;
        }

        [HttpPost]
        public ActionResult GetDepreciation2(GetDepreciation2Par par)
        {
            try
            {
                var _ret = new GetDepreciation2Out();
                SetSessionObject(par.sessionID);
                _ret._ret = ((iERPTransactionService)SessionObject).GetDepreciation(par.costCenteriD, par.itemCode, par.dateTime, out _ret.rate, out _ret.year);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region PreviewDepreciation
        public class PreviewDepreciationPar
        {
            public string sessionID; public DateTime time;
        }

        [HttpPost]
        public ActionResult previewDepreciation(PreviewDepreciationPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).previewDepreciation(par.time));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDepreciationDocument
        public class GetDepreciationDocumentPar
        {
            public string sessionID; public DateTime time;
        }

        [HttpPost]
        public ActionResult getDepreciationDocument(GetDepreciationDocumentPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).getDepreciationDocument(par.time));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetTaxRates
        public class GetTaxRatesPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getTaxRates(GetTaxRatesPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).getTaxRates());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetTransactionItems2
        public class GetTransactionItems2Par
        {
            public string sessionID; public int accountID;
        }

        [HttpPost]
        public ActionResult GetTransactionItems2(GetTransactionItems2Par par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetTransactionItems(par.accountID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetLastBankReconciliation
        public class GetLastBankReconciliationPar
        {
            public string sessionID; public int bankID; public DateTime date;
        }

        [HttpPost]
        public ActionResult getLastBankReconciliation(GetLastBankReconciliationPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).getLastBankReconciliation(par.bankID, par.date));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ActivateRelation
        public class ActivateRelationPar
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult ActivateRelation(ActivateRelationPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).ActivateRelation(par.code);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeactivateRelation
        public class DeactivateRelationPar
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult DeactivateRelation(DeactivateRelationPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).DeactivateRelation(par.code);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteRelation
        public class DeleteRelationPar
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult DeleteRelation(DeleteRelationPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).DeleteRelation(par.code);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region RenderInventoryTable
        public class RenderInventoryTablePar
        {
            public string sessionID; public InventoryGeneratorParameters pars;
        }

        [HttpPost]
        public ActionResult renderInventoryTable(RenderInventoryTablePar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).renderInventoryTable(par.pars));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region RenderCostCenterProfile
        public class RenderCostCenterProfilePar
        {
            public string sessionID; public int costCenterID; public DateTime time1; public DateTime time2;
        }

        [HttpPost]
        public ActionResult renderCostCenterProfile(RenderCostCenterProfilePar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).renderCostCenterProfile(par.costCenterID, par.time1, par.time2));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
    }
}