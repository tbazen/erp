using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Server
{
    public partial class BNFinanceController
    {
        #region RegisterSupplier
        public class RegisterSupplierPar
        {
            public string sessionID;public TradeRelation supplier;
        }
        
[HttpPost]
public ActionResult RegisterSupplier(RegisterSupplierPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
return Json(((iERPTransactionService)SessionObject).RegisterSupplier(par.supplier));
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region GetSupplier
        public class GetSupplierPar
        {
            public string sessionID;public string code;
        }
        
[HttpPost]
public ActionResult GetSupplier(GetSupplierPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
return Json(((iERPTransactionService)SessionObject).GetSupplier(par.code));
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region GetAllSuppliers
        public class GetAllSuppliersPar
        {
            public string sessionID;
        }
        
[HttpPost]
public ActionResult GetAllSuppliers(GetAllSuppliersPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
return Json(((iERPTransactionService)SessionObject).GetAllSuppliers());
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region SearchSuppliers
        public class SearchSuppliersPar
        {
            public string sessionID;public int index;public int NoOfPages;public object[] criteria;public string[] column; 
        }
        public class SearchSuppliersOut
        {
            public int NoOfRecords;
            public TradeRelation[] _ret;
        }
        
[HttpPost]
public ActionResult SearchSuppliers(SearchSuppliersPar par)
{
    try
    {
                    var _ret = new SearchSuppliersOut();
            SetSessionObject(par.sessionID);
            _ret._ret = ((iERPTransactionService)SessionObject).SearchSuppliers(par.index, par.NoOfPages, par.criteria, par.column, out _ret.NoOfRecords);
return Json(_ret);
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region DeleteSupplier 
        public class DeleteSupplierPar
        {
            public string sessionID;public string[] code;
        }
        
[HttpPost]
public ActionResult DeleteSupplier(DeleteSupplierPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
            ((iERPTransactionService)SessionObject).DeleteSupplier(par.code);

return Ok();
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region ActivateSupplier
        public class ActivateSupplierPar
        {
            public string sessionID;public string supplierCode;
        }
        
[HttpPost]
public ActionResult ActivateSupplier(ActivateSupplierPar par)
{
    try
    {
                    SetSessionObject(par.sessionID);
            ((iERPTransactionService)SessionObject).ActivateSupplier(par.supplierCode);

return Ok();
    }
    catch(Exception ex)
    {
        return base.Exception(ex);
    }
}
        #endregion
        #region DeactivateSupplier
        public class DeactivateSupplierPar
        {
            public string sessionID;public string supplierCode;
        }
        public void DeactivateSupplier(DeactivateSupplierPar par)
        {
            SetSessionObject(par.sessionID);
            ((iERPTransactionService)SessionObject).DeactivateSupplier(par. supplierCode);

        }
        #endregion
    }
}
