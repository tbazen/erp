using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using Microsoft.AspNetCore.Mvc;

namespace BIZNET.iERP.Server
{
    public partial class BNFinanceController
    {
        #region GetZReport
        public class GetZReportPar
        {
            public string sessionID; public AccountingPeriod period;
        }

        [HttpPost]
        public ActionResult GetZReport(GetZReportPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetZReport(par.period));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSalesTaxableAndNonTaxable
        public class GetSalesTaxableAndNonTaxablePar
        {
            public string sessionID; public DateTime date;
        }
        public class GetSalesTaxableAndNonTaxableOut
        {
            public double nonTaxableAmount; public double tax;
            public double _ret;
        }

        [HttpPost]
        public ActionResult GetSalesTaxableAndNonTaxable(GetSalesTaxableAndNonTaxablePar par)
        {
            try
            {
                var _ret = new GetSalesTaxableAndNonTaxableOut();
                SetSessionObject(par.sessionID);
                _ret._ret = ((iERPTransactionService)SessionObject).GetSalesTaxableAndNonTaxable(par.date, out _ret.nonTaxableAmount, out _ret.tax);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
    }
}
