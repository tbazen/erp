using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.ClientServer.Server;
using Microsoft.AspNetCore.Mvc;

namespace BIZNET.iERP.Server
{
    [Route("api/erp/[controller]/[action]")]
    [ApiController]
    public partial class BNFinanceController : RESTServerControllerBase<iERPTransactionService>
    {
        #region GetSystemParamters
        public class GetSystemParamtersPar
        {
            public string sessionID; public string[] names;
        }

        [HttpPost]
        public ActionResult GetSystemParameters(GetSystemParamtersPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                var res = ((iERPTransactionService)SessionObject).GetSystemParamters(par.names);
                return Json(new BinObject(res));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSystemParametrsWeb
        [HttpPost]
        public ActionResult GetSystemParametersWeb(GetSystemParamtersPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                var res = ((iERPTransactionService)SessionObject).GetSystemParamters(par.names);
                return Json(res);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion




        #region SetSystemParameters
        public class SetSystemParametersPar
        {
            public string sessionID; public string[] fields; public BinObject vals;
        }

        [HttpPost]
        public ActionResult SetSystemParameters(SetSystemParametersPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).SetSystemParameters(par.fields, (object[])par.vals.Deserialized());

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CountReferedDocuments
        public class CountReferedDocumentsPar
        {
            public string sessionID; public DocumentReferenceType type; public string refNumber;
        }

        [HttpPost]
        public ActionResult CountReferedDocuments(CountReferedDocumentsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).CountReferedDocuments(par.type, par.refNumber));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetReferedDocuments 
        public class GetReferedDocumentsPar
        {
            public string sessionID; public DocumentReferenceType type; public string refNumber;
        }

        [HttpPost]
        public ActionResult GetReferedDocuments(GetReferedDocumentsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetReferedDocuments(par.type, par.refNumber));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetVATDeclaration
        public class GetVATDeclarationPar
        {
            public string sessionID; public int periodID;
        }

        [HttpPost]
        public ActionResult GetVATDeclaration(GetVATDeclarationPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetVATDeclaration(par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GenerateYearAccountingPeriod
        public class GenerateYearAccountingPeriodPar
        {
            public string sessionID; public int year;
        }

        [HttpPost]
        public ActionResult GenerateYearAccountingPeriod(GenerateYearAccountingPeriodPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).GenerateYearAccountingPeriod(par.year);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetTransactionItems
        public class GetTransactionItemsPar
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult GetTransactionItems(GetTransactionItemsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetTransactionItems(par.code));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SearchTransactionItems
        public class SearchTransactionItemsPar
        {
            public string sessionID; public int index; public int pageSize; public object[] criteria; public string[] column;
        }
        public class SearchTransactionItemsOut
        {
            public int NoOfRecords;
            public TransactionItems[] _ret;
        }

        [HttpPost]
        public ActionResult SearchTransactionItems(SearchTransactionItemsPar par)
        {
            try
            {
                var _ret = new SearchTransactionItemsOut();
                SetSessionObject(par.sessionID);
                _ret._ret = ((iERPTransactionService)SessionObject).SearchTransactionItems(par.index, par.pageSize, par.criteria, par.column, out _ret.NoOfRecords);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetMeasureUnits 
        public class GetMeasureUnitsPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult GetMeasureUnits(GetMeasureUnitsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetMeasureUnits());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetMeasureUnit
        public class GetMeasureUnitPar
        {
            public string sessionID; public int ID;
        }

        [HttpPost]
        public ActionResult GetMeasureUnit(GetMeasureUnitPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetMeasureUnit(par.ID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region RegisterMeasureUnit
        public class RegisterMeasureUnitPar
        {
            public string sessionID; public MeasureUnit measureUnit;
        }

        [HttpPost]
        public ActionResult RegisterMeasureUnit(RegisterMeasureUnitPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).RegisterMeasureUnit(par.measureUnit));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region IsSupplierInvolvedInTransactions
        public class IsSupplierInvolvedInTransactionsPar
        {
            public string sessionID; public string supplierCode;
        }

        [HttpPost]
        public ActionResult IsSupplierInvolvedInTransactions(IsSupplierInvolvedInTransactionsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).IsSupplierInvolvedInTransactions(par.supplierCode));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region RegisterTransactionItem
        public class RegisterTransactionItemPar
        {
            public string sessionID; public TransactionItems item; public int[] costCenterID;
        }

        [HttpPost]
        public ActionResult RegisterTransactionItem(RegisterTransactionItemPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).RegisterTransactionItem(par.item, par.costCenterID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteTransactionItem
        public class DeleteTransactionItemPar
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult DeleteTransactionItem(DeleteTransactionItemPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).DeleteTransactionItem(par.code);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ActivateTransactionItem
        public class ActivateTransactionItemPar
        {
            public string sessionID; public string itemCode;
        }

        [HttpPost]
        public ActionResult ActivateTransactionItem(ActivateTransactionItemPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).ActivateTransactionItem(par.itemCode);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeactivateTransactionItem
        public class DeactivateTransactionItemPar
        {
            public string sessionID; public string itemCode;
        }

        [HttpPost]
        public ActionResult DeactivateTransactionItem(DeactivateTransactionItemPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).DeactivateTransactionItem(par.itemCode);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region IsBERPClientFileUpdated
        public class IsBERPClientFileUpdatedPar
        {
            public string sessionID; public string fileName; public byte[] clientMD5;
        }

        [HttpPost]
        public ActionResult isBERPClientFileUpdated(IsBERPClientFileUpdatedPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).isBERPClientFileUpdated(par.fileName, par.clientMD5));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetERPClientLatestFile
        public class GetERPClientLatestFilePar
        {
            public string sessionID; public string fileName;
        }

        [HttpPost]
        public ActionResult getERPClientLatestFile(GetERPClientLatestFilePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getERPClientLatestFile(par.fileName));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllClientFiles
        public class GetAllClientFilesPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getAllClientFiles(GetAllClientFilesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getAllClientFiles());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetEmployeeAccount
        public class GetEmployeeAccountPar
        {
            public string sessionID; public int parentAccountID; public int employeeID;
        }

        [HttpPost]
        public ActionResult GetEmployeeAccount(GetEmployeeAccountPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).GetEmployeeAccount(par.parentAccountID, par.employeeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPrimaryJournal
        public class GetPrimaryJournalPar
        {
            public string sessionID; public JournalFilter filter; public int pageIndex; public int pageSize;
        }
        public class GetPrimaryJournalOut
        {
            public int NRecord;
            public string _ret;
        }

        [HttpPost]
        public ActionResult getPrimaryJournal(GetPrimaryJournalPar par)
        {
            try
            {
                var _ret = new GetPrimaryJournalOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((iERPTransactionService)base.SessionObject).getPrimaryJournal(par.filter, par.pageIndex, par.pageSize, out _ret.NRecord);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDocumentListHtmlByTypedReference
        public class GetDocumentListHtmlByTypedReferencePar
        {
            public string sessionID; public int typeID; public string reference;
        }
        public ActionResult GetDocumentListHtmlByTypedReference(GetDocumentListHtmlByTypedReferencePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
            return Json(((iERPTransactionService)base.SessionObject).getDocumentListHtmlByTypedReference(par.typeID, par.reference));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDocumentEntriesHTML
        public class GetDocumentEntriesHTMLPar
        {
            public string sessionID; public int docID;
        }

        [HttpPost]
        public ActionResult getDocumentEntriesHTML(GetDocumentEntriesHTMLPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getDocumentEntriesHTML(par.docID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetTypedReferenceEntriesHTML
        public class GetTypedReferenceEntriesHTMLPar
        {
            public string sessionID; public INTAPS.Accounting.DocumentTypedReference tref;
        }

        [HttpPost]
        public ActionResult getTypedReferenceEntriesHTML(GetTypedReferenceEntriesHTMLPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getTypedReferenceEntriesHTML(par.tref));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBudget
        public class GetBudgetPar
        {
            public string sessionID; public int budgetID;
        }

        [HttpPost]
        public ActionResult getBudget(GetBudgetPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getBudget(par.budgetID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBudget2
        public class GetBudget2par
        {
            public string sessionID; public int costCenterID; public System.DateTime target; public bool exact;
        }

        [HttpPost]
        public ActionResult getBudget2(GetBudget2par par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getBudget(par.costCenterID, par.target, par.exact));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllBudgets
        public class GetAllBudgetsPar
        {
            public string sessionID; public int costCenterID;
        }

        [HttpPost]
        public ActionResult getAllBudgets(GetAllBudgetsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getAllBudgets(par.costCenterID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateBudget
        public class UpdateBudgetPar
        {
            public string sessionID; public BIZNET.iERP.Budget budget; public BIZNET.iERP.BudgetEntry[] entries;
        }

        [HttpPost]
        public ActionResult updateBudget(UpdateBudgetPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).updateBudget(par.budget, par.entries);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CreateBudget
        public class CreateBudgetPar
        {
            public string sessionID; public BIZNET.iERP.Budget budget; public BIZNET.iERP.BudgetEntry[] entries;
        }

        [HttpPost]
        public ActionResult createBudget(CreateBudgetPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).createBudget(par.budget, par.entries);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteBudget
        public class DeleteBudgetPar
        {
            public string sessionID; public int budgetID;
        }

        [HttpPost]
        public ActionResult deleteBudget(DeleteBudgetPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).deleteBudget(par.budgetID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBudgetEntries
        public class GetBudgetEntriesPar
        {
            public string sessionID; public int budgetID;
        }

        [HttpPost]
        public ActionResult getBudgetEntries(GetBudgetEntriesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getBudgetEntries(par.budgetID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBudgetReport
        public class GetBudgetReportPar
        {
            public string sessionID; public BIZNET.iERP.BudgetFilter filter;
        }

        [HttpPost]
        public ActionResult getBudgetReport(GetBudgetReportPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getBudgetReport(par.filter));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBookClosings
        public class GetBookClosingsPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getBookClosings(GetBookClosingsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getBookClosings());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CloseBookForTransaction
        public class CloseBookForTransactionPar
        {
            public string sessionID; public BIZNET.iERP.ClosedRange range;
        }

        [HttpPost]
        public ActionResult closeBookForTransaction(CloseBookForTransactionPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).closeBookForTransaction(par.range);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateBookClosingForTransaction
        public class UpdateBookClosingForTransactionPar
        {
            public string sessionID; public BIZNET.iERP.ClosedRange range;
        }

        [HttpPost]
        public ActionResult updateBookClosingForTransaction(UpdateBookClosingForTransactionPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).updateBookClosingForTransaction(par.range);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region OpenBookForTransaction
        public class OpenBookForTransactionpar
        {
            public string sessionID;

        }

        [HttpPost]
        public ActionResult openBookForTransaction(OpenBookForTransactionpar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).openBookForTransaction();

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetTransactionsClosedUpto
        public class GetTransactionsClosedUptoPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getTransactionsClosedUpto(GetTransactionsClosedUptoPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getTransactionsClosedUpto());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region AllowNegativeTransactionPost
        public class AllowNegativeTransactionPostPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult AllowNegativeTransactionPost(AllowNegativeTransactionPostPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).AllowNegativeTransactionPost());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CreateTradeRelationCategory
        public class CreateTradeRelationCategoryPar
        {
            public string sessionID; public BIZNET.iERP.TradeRelationCategory cat;
        }

        [HttpPost]
        public ActionResult createTradeRelationCategory(CreateTradeRelationCategoryPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).createTradeRelationCategory(par.cat);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateTradeRelationCategory
        public class UpdateTradeRelationCategoryPar
        {
            public string sessionID; public BIZNET.iERP.TradeRelationCategory cat;
        }

        [HttpPost]
        public ActionResult updateTradeRelationCategory(UpdateTradeRelationCategoryPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).updateTradeRelationCategory(par.cat);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteTradeRelationCategory
        public class DeleteTradeRelationCategoryPar
        {
            public string sessionID; public string tradeRelationCategoryCode;
        }

        [HttpPost]
        public ActionResult deleteTradeRelationCategory(DeleteTradeRelationCategoryPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).deleteTradeRelationCategory(par.tradeRelationCategoryCode);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetTradeRelationsByCategory
        public class GetTradeRelationsByCategoryPar
        {
            public string sessionID; public string parentCategoryCode;
        }

        [HttpPost]
        public ActionResult getTradeRelationsByCategory(GetTradeRelationsByCategoryPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getTradeRelationsByCategory(par.parentCategoryCode));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCategories
        public class GetCategoriesPar
        {
            public string sessionID; public string parentCategoryCode;
        }

        [HttpPost]
        public ActionResult getCategories(GetCategoriesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getCategories(par.parentCategoryCode));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SearchTradeRelation
        public class SearchTradeRelationPar
        {
            public string sessionID; public string categoryCode; public BIZNET.iERP.TradeRelationType[] types; public int index; public int pageSize; public object[] criteria; public string[] column;
        }
        public class SearchTradeRelationOut
        {
            public int NoOfRecords;
            public BIZNET.iERP.TradeRelation[] _ret;
        }

        [HttpPost]
        public ActionResult searchTradeRelation(SearchTradeRelationPar par)
        {
            try
            {
                var _ret = new SearchTradeRelationOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((iERPTransactionService)base.SessionObject).searchTradeRelation(par.categoryCode, par.types, par.index, par.pageSize, par.criteria, par.column, out _ret.NoOfRecords);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetTradeRelationCategory
        public class GetTradeRelationCategoryPar
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult getTradeRelationCategory(GetTradeRelationCategoryPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getTradeRelationCategory(par.code));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }

        #endregion
        #region GetItemCategory 
        public class GetItemCategoryPar
        {
            public string sessionID; public string Code;
        }

        [HttpPost]
        public ActionResult GetItemCategory(GetItemCategoryPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).GetItemCategory(par.Code));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GenerateDetaileCashFlowReport
        public class GenerateDetaileCashFlowReport
        {
            public string sessionID; public System.DateTime date1; public System.DateTime date2; public BIZNET.iERP.DetailedCashFlowReportOption options;
        }

        [HttpPost]
        public ActionResult generateDetaileCashFlowReport(GenerateDetaileCashFlowReport par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).generateDetaileCashFlowReport(par.date1, par.date2, par.options));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetTrialBalance
        public class GetTrialBalancePar
        {
            public string sessionID; public System.Collections.Generic.List<int> expandedAccount; public List<int> rootAccounts; public BinObject agregateTo; public System.DateTime date; public bool twoColumns;
            public String AgregateType; public String DetailType;
        }

        [HttpPost]
        public ActionResult getTrialBalance(GetTrialBalancePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                //  return ((iERPTransactionService)base.SessionObject).getTrialBalance<AgregateType, DetailType>(par.expandedAccount, par.rootAccounts, par.agregateTo, par.date, par.twoColumns);

                Type agregateType = "Account".Equals(par.AgregateType) ? typeof(Account) : typeof(CostCenter);
                Type detailType = "Account".Equals(par.DetailType) ? typeof(Account) : typeof(CostCenter);
                var genericmethod = typeof(iERPTransactionService).GetMethod("getTrialBalance");
                var method = genericmethod.MakeGenericMethod(agregateType, detailType);
                return Json((String)method.Invoke(base.SessionObject, new object[] { par.expandedAccount, par.rootAccounts, par.agregateTo.Deserialized(), par.date, par.twoColumns }));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllPaymentInstrumentTypes
        public class GetAllPaymentInstrumentTypesPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getAllPaymentInstrumentTypes(GetAllPaymentInstrumentTypesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getAllPaymentInstrumentTypes());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPaymentInstrumentType
        public class GetPaymentInstrumentTypePar
        {
            public string sessionID; public string itemCode;
        }

        [HttpPost]
        public ActionResult getPaymentInstrumentType(GetPaymentInstrumentTypePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getPaymentInstrumentType(par.itemCode));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBankInfo
        public class GetBankInfoPar
        {
            public string sessionID; public int bankID;
        }

        [HttpPost]
        public ActionResult getBankInfo(GetBankInfoPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getBankInfo(par.bankID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBankBranchInfo
        public class GetBankBranchInfoPar
        {
            public string sessionID; public int branchID;
        }

        [HttpPost]
        public ActionResult getBankBranchInfo(GetBankBranchInfoPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getBankBranchInfo(par.branchID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllBanks
        public class GetAllBanksPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getAllBanks(GetAllBanksPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getAllBanks());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllBranchsOfBank
        public class GetAllBranchsOfBankPar
        {
            public string sessionID; public int bankID;
        }

        [HttpPost]
        public ActionResult getAllBranchsOfBank(GetAllBranchsOfBankPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getAllBranchsOfBank(par.bankID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SavePaymentInstrumentType
        public class SavePaymentInstrumentTypePar
        {
            public string sessionID; public BIZNET.iERP.PaymentInstrumentType type;
        }
        public void savePaymentInstrumentType(SavePaymentInstrumentTypePar par)
        {
            this.SetSessionObject(par.sessionID);
            ((iERPTransactionService)base.SessionObject).savePaymentInstrumentType(par.type);
        }
        #endregion
        #region DeletePaymentInstrumentType
        public class DeletePaymentInstrumentTypePar
        {
            public string sessionID; public string typeID;
        }

        [HttpPost]
        public ActionResult deletePaymentInstrumentType(DeletePaymentInstrumentTypePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).deletePaymentInstrumentType(par.typeID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SaveBankInfo
        public class SaveBankInfoPar
        {
            public string sessionID; public BIZNET.iERP.BankInfo bank;
        }

        [HttpPost]
        public ActionResult saveBankInfo(SaveBankInfoPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).saveBankInfo(par.bank));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SaveBankBranchInfo
        public class SaveBankBranchInfoPar
        {
            public string sessionID; public BIZNET.iERP.BankBranchInfo branch;
        }

        [HttpPost]
        public ActionResult saveBankBranchInfo(SaveBankBranchInfoPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).saveBankBranchInfo(par.branch));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteBankInfo
        public class DeleteBankInfoPar
        {
            public string sessionID; public int bankID;
        }

        [HttpPost]
        public ActionResult deleteBankInfo(DeleteBankInfoPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).deleteBankInfo(par.bankID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteBankBranchInfo
        public class DeleteBankBranchInfoPar
        {
            public string sessionID; public int branchID;
        }

        [HttpPost]
        public ActionResult deleteBankBranchInfo(DeleteBankBranchInfoPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).deleteBankBranchInfo(par.branchID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetFixedAssetRuleAttribute
        public class GetFixedAssetRuleAttributePar
        {
            public string sessionID; public int typeID;
        }

        [HttpPost]
        public ActionResult getFixedAssetRuleAttribute(GetFixedAssetRuleAttributePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getFixedAssetRuleAttribute(par.typeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllFixedAssetRuleAttributes
        public class GetAllFixedAssetRuleAttributesPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getAllFixedAssetRuleAttributes(GetAllFixedAssetRuleAttributesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getAllFixedAssetRuleAttributes());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SaveFixedAssetRuleSetting
        public class SaveFixedAssetRuleSettingPar
        {
            public string sessionID; public int typeID; public object data;
        }

        [HttpPost]
        public ActionResult saveFixedAssetRuleSetting(SaveFixedAssetRuleSettingPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).saveFixedAssetRuleSetting(par.typeID, par.data);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetChildCashCategories
        public class GetChildCashCategoriesPar
        {
            public string sessionID; public int categoryID;
        }

        [HttpPost]
        public ActionResult getChildCashCategories(GetChildCashCategoriesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getChildCashCategories(par.categoryID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCashCategory
        public class GetCashCategoryPar
        {
            public string sessionID; public int categoryID;
        }

        [HttpPost]
        public ActionResult getCashCategory(GetCashCategoryPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getCashCategory(par.categoryID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CreateCashCategory
        public class CreateCashCategoryPar
        {
            public string sessionID; public BIZNET.iERP.CashAccountCategory cat;
        }

        [HttpPost]
        public ActionResult createCashCategory(CreateCashCategoryPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).createCashCategory(par.cat));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCashAccountsByCategory
        public class GetCashAccountsByCategoryPar
        {
            public string sessionID; public int categoryID;
        }

        [HttpPost]
        public ActionResult getCashAccountsByCategory(GetCashAccountsByCategoryPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getCashAccountsByCategory(par.categoryID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateCashCategory
        public class UpdateCashCategoryPar
        {
            public string sessionID; public BIZNET.iERP.CashAccountCategory cat;
        }

        [HttpPost]
        public ActionResult updateCashCategory(UpdateCashCategoryPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).updateCashCategory(par.cat);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteCashCategory
        public class DeleteCashCategoryPar
        {
            public string sessionID; public int catID;
        }

        [HttpPost]
        public ActionResult deleteCashCategory(DeleteCashCategoryPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).deleteCashCategory(par.catID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region RenderLedgerTable
        public class RenderLedgerTablePar
        {
            public string sessionID; public LedgerParameters pars;
        }

        [HttpPost]
        public ActionResult renderLedgerTable(RenderLedgerTablePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).renderLedgerTable(par.pars));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region RecalculateInventoryWeightedAverage
        public class RecalculateInventoryWeightedAveragePar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult recalculateInventoryWeightedAverage(RecalculateInventoryWeightedAveragePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).recalculateInventoryWeightedAverage();

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SaveStockLevelConfiguration
        public class SaveStockLevelConfigurationPar
        {
            public string sessionID; public StockLevelConfiguration[] config;
        }

        [HttpPost]
        public ActionResult SaveStockLevelConfiguration(SaveStockLevelConfigurationPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).SaveStockLevelConfiguration(par.config);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetStockLevelConfiguration
        public class GetStockLevelConfigurationPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getStockLevelConfiguration(GetStockLevelConfigurationPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getStockLevelConfiguration());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SetConvesionFactors
        public class SetConvesionFactorsPar
        {
            public string sessionID; public string itemScope; public int categoryScope; public BIZNET.iERP.UnitCoversionFactor[] factors;
        }

        [HttpPost]
        public ActionResult setConvesionFactors(SetConvesionFactorsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).setConvesionFactors(par.itemScope, par.categoryScope, par.factors);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetConversionFactors
        public class GetConversionFactorsPar
        {
            public string sessionID; public string itemScope; public int categoryScope;
        }

        [HttpPost]
        public ActionResult getConversionFactors(GetConversionFactorsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getConversionFactors(par.itemScope, par.categoryScope));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetItemUnitConvertor
        public class GetItemUnitConvertorPar
        {
            public string sessionID; public string itemCode;
        }

        [HttpPost]
        public ActionResult getItemUnitConvertor(GetItemUnitConvertorPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getItemUnitConvertor(par.itemCode));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region IsDocumentBudgetSet
        public class IsDocumentBudgetSetPar
        {
            public string sessionID; public int documentID;
        }

        [HttpPost]
        public ActionResult isDocumentBudgetSet(IsDocumentBudgetSetPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).isDocumentBudgetSet(par.documentID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetUnbudgetedDocuments
        public class GetUnbudgetedDocumentsPar
        {
            public string sessionID; public INTAPS.Accounting.DocumentSearchPar pars; public int index; public int pageSize;
        }
        public class GetUnbudgetedDocumentsOut
        {
            public int N;
            public INTAPS.Accounting.AccountDocument[] _ret;
        }

        [HttpPost]
        public ActionResult getUnbudgetedDocuments(GetUnbudgetedDocumentsPar par)
        {
            try
            {
                var _ret = new GetUnbudgetedDocumentsOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((iERPTransactionService)base.SessionObject).getUnbudgetedDocuments(par.pars, par.index, par.pageSize, out _ret.N);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllPropertTypes
        public class GetAllPropertTypesPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getAllPropertTypes(GetAllPropertTypesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getAllPropertTypes());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPropertyByCode
        public class GetPropertyByCodePar
        {
            public string sessionID; public string propertyCode;
        }
        public class GetPropertyByCodeOut
        {
            public object extraData;
            public BIZNET.iERP.Property _ret;
        }

        [HttpPost]
        public ActionResult getPropertyByCode(GetPropertyByCodePar par)
        {
            try
            {
                var _ret = new GetPropertyByCodeOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((iERPTransactionService)base.SessionObject).getPropertyByCode(par.propertyCode, out _ret.extraData);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetProperty
        public class GetPropertyPar
        {
            public string sessionID; public int propertyID;
        }
        public class GetPropertyOut
        {
            public object extraData;
            public BIZNET.iERP.Property _ret;
        }

        [HttpPost]
        public ActionResult getProperty(GetPropertyPar par)
        {
            try
            {
                var _ret = new GetPropertyOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((iERPTransactionService)base.SessionObject).getProperty(par.propertyID, out _ret.extraData);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CreateProperty
        public class CreatePropertyPar
        {
            public string sessionID; public BIZNET.iERP.Property prop; public object extraData;
        }

        [HttpPost]
        public ActionResult createProperty(CreatePropertyPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).createProperty(par.prop, par.extraData));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateProperty
        public class UpdatePropertyPar
        {
            public string sessionID; public BIZNET.iERP.Property prop; public object extraData;
        }

        [HttpPost]
        public ActionResult updateProperty(UpdatePropertyPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).updateProperty(par.prop, par.extraData);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPropertyItemCategory2
        public class GetPropertyItemCategory2Par
        {
            public string sessionID; public int parentCategoryID; public int pageIndex; public int pageSize;
        }
        public class GetPropertyItemCategory2Out
        {
            public int N;
            public BIZNET.iERP.PropertyItemCategory[] _ret;
        }

        [HttpPost]
        public ActionResult getPropertyItemCategory2(GetPropertyItemCategory2Par par)
        {
            try
            {
                var _ret = new GetPropertyItemCategory2Out();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((iERPTransactionService)base.SessionObject).getPropertyItemCategory(par.parentCategoryID, par.pageIndex, par.pageSize, out _ret.N);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPropertyByItemCode
        public class GetPropertyByItemCodePar
        {
            public string sessionID; public string propertyCode;
        }
        public class GetPropertyByItemCodeOut
        {
            public object extraData;
            public BIZNET.iERP.Property _ret;
        }

        [HttpPost]
        public ActionResult getPropertyByItemCode(GetPropertyByItemCodePar par)
        {
            try
            {
                var _ret = new GetPropertyByItemCodeOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((iERPTransactionService)base.SessionObject).getPropertyByItemCode(par.propertyCode, out _ret.extraData);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteProperty
        public class DeletePropertyPar
        {
            public string sessionID; public int propertyID;
        }

        [HttpPost]
        public ActionResult deleteProperty(DeletePropertyPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).deleteProperty(par.propertyID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPropertyItemCategory
        public class GetPropertyItemCategoryPar
        {
            public string sessionID; public string itemCode;
        }

        [HttpPost]
        public ActionResult getPropertyItemCategory(GetPropertyItemCategoryPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getPropertyItemCategory(par.itemCode));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPropertiesByParentItemCode
        public class GetPropertiesByParentItemCodePar
        {
            public string sessionID; public string parentItemCode; public int pageIndex; public int pageSize;
        }
        public class GetPropertiesByParentItemCodeOut
        {
            public int N;
            public BIZNET.iERP.Property[] _ret;
        }

        [HttpPost]
        public ActionResult getPropertiesByParentItemCode(GetPropertiesByParentItemCodePar par)
        {
            try
            {
                var _ret = new GetPropertiesByParentItemCodeOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((iERPTransactionService)base.SessionObject).getPropertiesByParentItemCode(par.parentItemCode, par.pageIndex, par.pageSize, out _ret.N);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetTradeRelationTransactions
        public class GetTradeRelationTransactionsPar
        {
            public string sessionID; public string relationCode; public long ticksFrom; public long ticksTo; public int[] docTypes;
        }

        [HttpPost]
        public ActionResult getTradeRelationTransactions(GetTradeRelationTransactionsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getTradeRelationTransactions(par.relationCode, par.ticksFrom, par.ticksTo, par.docTypes));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetMeasureUnit2
        public class GetMeasureUnit2Par
        {
            public string sessionID; public string name;
        }

        [HttpPost]
        public ActionResult GetMeasureUnit2(GetMeasureUnit2Par par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).GetMeasureUnit(par.name));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ConvertUnit
        public class ConvertUnitPar
        {
            public string sessionID; public string itemCode; public double quantity; public int fromUnit; public int toUnit;
        }

        [HttpPost]
        public ActionResult convertUnit(ConvertUnitPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).convertUnit(par.itemCode, par.quantity, par.fromUnit, par.toUnit));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPropertyType
        public class GetPropertyTypePar
        {
            public string sessionID; public int id;
        }

        [HttpPost]
        public ActionResult getPropertyType(GetPropertyTypePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getPropertyType(par.id));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetMaterialBalance
        public class GetMaterialBalancePar
        {
            public string sessionID; public System.DateTime time; public string itemCode; public int[] itemIDs;
        }

        [HttpPost]
        public ActionResult getMaterialBalance(GetMaterialBalancePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getMaterialBalance(par.time, par.itemCode, par.itemIDs));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        //attachment methods
        #region AttachFile
        public class AttachFilePar
        {
            public string sessionID; public int documentID; public AttachedFileGroup fileGroup; public IEnumerable<AttachedFileItem> items;
        }

        [HttpPost]
        public ActionResult attachFile(AttachFilePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).attachFile(par.documentID, par.fileGroup, par.items));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region AttachMultipeFile
        public class AttachMultipeFilePar
        {
            public string sessionID; public int documentID; public IEnumerable<AttachedFileGroup> fileGroup; public IEnumerable<IEnumerable<AttachedFileItem>> items;
        }

        [HttpPost]
        public ActionResult attachMultipeFile(AttachMultipeFilePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).attachMultipeFile(par.documentID, par.fileGroup, par.items));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region RearrangeAttachedFileItems
        public class RearrangeAttachedFileItemsPar
        {
            public string sessionID; public int fileGroupID; public int[] oldOrdern; public int[] newOrdern;
        }

        [HttpPost]
        public ActionResult rearrangeAttachedFileItems(RearrangeAttachedFileItemsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).rearrangeAttachedFileItems(par.fileGroupID, par.oldOrdern, par.newOrdern);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ReplaceAttachmentFileItem
        public class ReplaceAttachmentFileItemPar
        {
            public string sessionID; public AttachedFileItem item;
        }

        [HttpPost]
        public ActionResult replaceAttachmentFileItem(ReplaceAttachmentFileItemPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).replaceAttachmentFileItem(par.item));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteAttachmentFileItem
        public class DeleteAttachmentFileItemPar
        {
            public string sessionID; public int attachmentFileID;
        }

        [HttpPost]
        public ActionResult deleteAttachmentFileItem(DeleteAttachmentFileItemPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).deleteAttachmentFileItem(par.attachmentFileID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DetachAttachmentFileGroup
        public class DetachAttachmentFileGroupPar
        {
            public string sessionID; public int attachmentFileGroupID; public int documentID;
        }

        [HttpPost]
        public ActionResult detachAttachmentFileGroup(DetachAttachmentFileGroupPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).detachAttachmentFileGroup(par.attachmentFileGroupID, par.documentID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteAttachmentFileGroup
        public class DeleteAttachmentFileGroupPar
        {
            public string sessionID; public int attachmentFileGroupID;
        }

        [HttpPost]
        public ActionResult deleteAttachmentFileGroup(DeleteAttachmentFileGroupPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((iERPTransactionService)base.SessionObject).deleteAttachmentFileGroup(par.attachmentFileGroupID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAttachedFileGroupForDocument
        public class GetAttachedFileGroupForDocumentPar
        {
            public string sessionID; public int documentID;
        }

        [HttpPost]
        public ActionResult getAttachedFileGroupForDocument(GetAttachedFileGroupForDocumentPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getAttachedFileGroupForDocument(par.documentID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAttachedFileItems
        public class GetAttachedFileItemsPar
        {
            public string sessionID; public int fileGroupID;
        }

        [HttpPost]
        public ActionResult getAttachedFileItems(GetAttachedFileItemsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getAttachedFileItems(par.fileGroupID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAttachedFileItem
        public class GetAttachedFileItemPar
        {
            public string sessionID; public int attachedFileItemID;
        }

        [HttpPost]
        public ActionResult getAttachedFileItem(GetAttachedFileItemPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getAttachedFileItem(par.attachedFileItemID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAttachedFileItemImageInfo
        public class GetAttachedFileItemImageInfoPar
        {
            public string sessionID; public int attachedFileItemID; public AttachmentPaperFace face;
        }

        [HttpPost]
        public ActionResult getAttachedFileItemImageInfo(GetAttachedFileItemImageInfoPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getAttachedFileItemImageInfo(par.attachedFileItemID, par.face));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAttachedFileItemImageData
        public class GetAttachedFileItemImageDataPar
        {
            public string sessionID; public int attachmentFileID; public AttachmentPaperFace face;
        }

        [HttpPost]
        public ActionResult getAttachedFileItemImageData(GetAttachedFileItemImageDataPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getAttachedFileItemImageData(par.attachmentFileID, par.face));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAttachedFileItemImageResized
        public class GetAttachedFileItemImageResizedPar
        {
            public string sessionID; public int attachmentFileID; public AttachmentPaperFace face; public int width; public int height;
        }

        [HttpPost]
        public ActionResult getAttachedFileItemImageResized(GetAttachedFileItemImageResizedPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getAttachedFileItemImageResized(par.attachmentFileID, par.face, par.width, par.height));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllFileAttachmentTypes
        public class GetAllFileAttachmentTypesPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getAllFileAttachmentTypes(GetAllFileAttachmentTypesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getAllFileAttachmentTypes());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAttachedFileGroup
        public class GetAttachedFileGroupPar
        {
            public string sessionID; public int fileGroupID;
        }

        [HttpPost]
        public ActionResult getAttachedFileGroup(GetAttachedFileGroupPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).getAttachedFileGroup(par.fileGroupID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetStaffExpenseAdvanceCashAccount
        public class GetStaffExpenseAdvanceCashAccountPar
        {
            public string sessionID; public int employeeID;
        }

        [HttpPost]
        public ActionResult GetStaffExpenseAdvanceCashAccount(GetStaffExpenseAdvanceCashAccountPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)base.SessionObject).GetStaffExpenseAdvanceCashAccount(par.employeeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
    }
}

