using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP.Server
{
    public partial class BNFinanceController
    {
        #region GetItemCategory2
        public class GetItemCategory2Par
        {
            public string sessionID; public int ID;
        }

        [HttpPost]
        public ActionResult GetItemCategory2(GetItemCategory2Par par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetItemCategory(par.ID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetItemCategories
        public class GetItemCategoriesPar
        {
            public string sessionID; public int PID; public ItemType itemType;
        }

        [HttpPost]
        public ActionResult GetItemCategories(GetItemCategoriesPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetItemCategories(par.PID, par.itemType));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetItemCategories2
        public class GetItemCategories2Par
        {
            public string sessionID; public int PID;
        }

        [HttpPost]
        public ActionResult GetItemCategories2(GetItemCategories2Par par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetItemCategories(par.PID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetItemsInCategory
        public class GetItemsInCategoryPar
        {
            public string sessionID; public int categID;
        }

        [HttpPost]
        public ActionResult GetItemsInCategory(GetItemsInCategoryPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetItemsInCategory(par.categID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllItemCategories
        public class GetAllItemCategoriesPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult GetAllItemCategories(GetAllItemCategoriesPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).GetAllItemCategories());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteItemCategory
        public class DeleteItemCategoryPar
        {
            public string sessionID; public int ID;
        }

        [HttpPost]
        public ActionResult DeleteItemCategory(DeleteItemCategoryPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((iERPTransactionService)SessionObject).DeleteItemCategory(par.ID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region RegisterItemCategory
        public class RegisterItemCategoryPar
        {
            public string sessionID; public ItemCategory category;
        }

        [HttpPost]
        public ActionResult RegisterItemCategory(RegisterItemCategoryPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((iERPTransactionService)SessionObject).RegisterItemCategory(par.category));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
    }
}
