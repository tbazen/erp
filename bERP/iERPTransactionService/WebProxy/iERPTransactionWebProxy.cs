﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INTAPS.ClientServer;
using INTAPS.ClientServer.RemottingServer;

namespace BIZNET.iERP.Server
{
    [HttpPageHandler(page = "webproxy.ph")]
    public class iERPTransactionWebProxy : INTAPS.ClientServer.RemottingServer.WebServiceProxyBase<iERPTransactionService>
    {
        [WebProxyMethod]
        public INTAPS.Payroll.PayrollPeriod getNextPayrollPeriod(iERPTransactionService service, int periodID)
        {
            
            INTAPS.Payroll.Service.PayrollService  payroll=
                (INTAPS.Payroll.Service.PayrollService) ApplicationServer.GetSessionObject(service.sessionID,"PayrollService");

            return payroll.getNextPeriod(periodID);
        }
        [WebProxyMethod]
        public INTAPS.Payroll.PayrollPeriod getPrevPayrollPeriod(iERPTransactionService service, int periodID)
        {

            INTAPS.Payroll.Service.PayrollService payroll =
                (INTAPS.Payroll.Service.PayrollService)ApplicationServer.GetSessionObject(service.sessionID, "PayrollService");

            return payroll.getPreviosPeriod(periodID);
        }
    }
}
