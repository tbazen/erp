using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionService
    {
        public void GenerateAccountingPeriodSeries(Type generator, string type, DateTime from, DateTime to)
        {
            this.bdeiERP.GenerateAccountingPeriodSeries(generator, type, from, to);
        }
        public AccountingPeriod GetAccountingPeriod(string periodType, int id)
        {
            return bdeiERP.GetAccountingPeriod(periodType, id);
        }

        public AccountingPeriod GetAccountingPeriod(string periodType, DateTime date)
        {
            return bdeiERP.GetAccountingPeriod(periodType, date);
        }

        public AccountingPeriod[] GetAccountingPeriodsInFiscalYear(string periodType, int year)
        {
            return _bde.GetAccountingPeriodsInFiscalYear(periodType, year);
        }
        public AccountingPeriod[] GetTouchedPeriods(string periodType, DateTime fromDate, DateTime toDate)
        {
            return _bde.GetTouchedPeriods(periodType, fromDate, toDate);
        }
        public AccountingPeriod GetNextAccountingPeriod(string periodType, int id)
        {
            return _bde.GetNextAccountingPeriod(periodType, id);
        }

        public AccountingPeriod GetPreviousAccountingPeriod(string periodType, int id)
        {
            return _bde.GetPreviousAccountingPeriod(periodType, id);
        }


        internal bool IsCustomerInvolvedInTransactions(string customerCode)
        {
            return _bde.IsCustomerInvolvedInTransactions(customerCode);
        }

        internal bool IsSupplierInvolvedInTransactions(string supplierCode)
        {
            return _bde.IsSupplierInvolvedInTransactions(supplierCode);
        }

        
    }

}

