using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionService
    {

        public BankAccountInfo GetBankAccount(int mainAccountID)
        {
            return _bde.GetBankAccount(mainAccountID);
        }

        public BankAccountInfo[] GetAllBankAccounts()
        {
            return _bde.GetAllBankAccounts();
        }

        public void DeleteBankAccount(int accountID)
        {
            CheckConfigurationPermision();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteMethodCallAudit("DeleteBankAccount Attempt", accountID.ToString(), -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.DeleteBankAccount(AID, accountID);
                    base.WriteExecuteAudit("DeleteBankAccount Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeleteBankAccount Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public int CreateBankAccount(BankAccountInfo account, int costCenterID)
        {
            CheckConfigurationPermision();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteObjectAudit("CreateBankAccount Attempt", account.mainCsAccount.ToString(), account, -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    int mainCsAccount = _bde.CreateBankAccount(AID, account, costCenterID);
                    base.WriteExecuteAudit("CreateBankAccount Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return mainCsAccount;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("CreateBankAccount Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void ActivateBankAccount(int mainCsAccount)
        {
            CheckConfigurationPermision();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteMethodCallAudit("ActivateBankAccount Attempt", mainCsAccount.ToString(), -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.ActivateBankAccount(AID, mainCsAccount);
                    base.WriteExecuteAudit("ActivateBankAccount Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("ActivateBankAccount Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void DeactivateBankAccount(int mainCsAccount)
        {
            CheckConfigurationPermision();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteMethodCallAudit("DeactivateBankAccount Attempt", mainCsAccount.ToString(), -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.DeactivateBankAccount(AID, mainCsAccount);
                    base.WriteExecuteAudit("DeactivateBankAccount Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeactivateBankAccount Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
    }
}
