using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionService
    {
        public CashAccount GetCashAccount(string code)
        {
            return _bde.GetCashAccount(code);
        }
        public CashAccount GetCashAccount(int csAccountID)
        {
            return _bde.GetCashAccount(csAccountID);
        }
        public CashAccount[] GetAllCashAccounts(bool includeExpnseAccounts)
        {
            return _bde.GetAllCashAccounts(includeExpnseAccounts);
        }
        public void DeleteCashAccount(string code)
        {
            CheckConfigurationPermision();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteMethodCallAudit("DeleteCashAccount Attempt", code, -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.DeleteCashAccount(AID, code);
                    base.WriteExecuteAudit("DeleteCashAccount Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeleteCashAccount Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public string CreateCashAccount(CashAccount account, int costCenterID)
        {
            CheckConfigurationPermision();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteObjectAudit("CreateCashAccount Attempt", account.csAccountID.ToString(), account, -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    string code = _bde.CreateCashAccount(AID, account, costCenterID);
                    base.WriteExecuteAudit("CreateCashAccount Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return code;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("CreateCashAccount Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void ActivateCashAccount(string code)
        {
            CheckConfigurationPermision();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteMethodCallAudit("ActivateCashAccount Attempt",code, -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.ActivateCashAccount(AID,code);
                    base.WriteExecuteAudit("ActivateCashAccount Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("ActivateCashAccount Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void DeactivateCashAccount(string code)
        {
            CheckConfigurationPermision();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteMethodCallAudit("DeactivateCashAccount Attempt", code, -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.DeactivateCashAccount(AID, code);
                    base.WriteExecuteAudit("DeactivateCashAccount Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeactivateCashAccount Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
    }
}
