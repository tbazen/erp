using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionService
    {

        #region TradeRelation Methods
        public string RegisterCustomer(TradeRelation customer)
        {
            CheckCasheirPermission();
              lock (bdeiERP.WriterHelper)
              {
                  int AID = base.WriteAddAudit("Register TradeRelation Attempt", customer.Code, -1);
                  bdeiERP.WriterHelper.BeginTransaction();
                  try
                  {
                      string customerCode = bdeiERP.RegisterCustomer(AID,customer);
                      base.WriteAddAudit("Register TradeRelation Success", customer.Code, -1);
                      bdeiERP.WriterHelper.CommitTransaction();
                      return customerCode;
                  }
                  catch (Exception ex)
                  {
                      bdeiERP.WriterHelper.RollBackTransaction();
                      base.WriteAudit("Register TradeRelation Failure", customer.Code, ex.Message + "\n" + ex.StackTrace, AID);
                      ApplicationServer.EventLoger.LogException(ex.Message, ex);
                      throw ex;
                  }
                  finally
                  {
                      bdeiERP.WriterHelper.CheckTransactionIntegrity();

                  }
              }
        }
        public TradeRelation GetCustomer(string code)
        {
            return bdeiERP.GetCustomer(code);
        }
        public TradeRelation[] GetAllCustomers()
        {
            return bdeiERP.GetAllCustomers();
        }
        public TradeRelation[] SearchCustomers(int index, int NoOfPages, object[] criteria, string[] column, out int NoOfRecords)
        {
            CheckStandardViewPermsion();
            return bdeiERP.SearchCustomers(index, NoOfPages, criteria, column, out NoOfRecords);
        }
        public void DeleteCustomer(string[] code)
        {
            CheckConfigurationPermision();
              lock (bdeiERP.WriterHelper)
              {
                  int AID = base.WriteAddAudit("Delete TradeRelation Attempt", code[0], -1);
                  bdeiERP.WriterHelper.BeginTransaction();
                  try
                  {
                      bdeiERP.DeleteCustomer(AID,code);
                      base.WriteAddAudit("Delete TradeRelation Success", code[0], -1);
                      bdeiERP.WriterHelper.CommitTransaction();
                  }
                  catch (Exception ex)
                  {
                      bdeiERP.WriterHelper.RollBackTransaction();
                      base.WriteAudit("Delete TradeRelation Failure", code[0], ex.Message + "\n" + ex.StackTrace, AID);
                      ApplicationServer.EventLoger.LogException(ex.Message, ex);
                      throw ex;
                  }
                  finally
                  {
                      bdeiERP.WriterHelper.CheckTransactionIntegrity();
                  }
              }
        }
        public void ActivateCustomer(string customerCode)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Activate TradeRelation Attempt", customerCode, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.ActivateCustomer(AID,customerCode);
                    base.WriteAddAudit("Activate TradeRelation Success", customerCode, -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Activate TradeRelation Failure", customerCode, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();

                }
            }
        }
        public void DeactivateCustomer(string customerCode)
        {
            CheckConfigurationPermision();
             lock (bdeiERP.WriterHelper)
             {
                 int AID = base.WriteAddAudit("Deactivate TradeRelation Attempt", customerCode, -1);
                 bdeiERP.WriterHelper.BeginTransaction();
                 try
                 {
                     bdeiERP.DeactivateCustomer(AID,customerCode);
                     base.WriteAddAudit("Deactivate TradeRelation Success", customerCode, -1);
                     bdeiERP.WriterHelper.CommitTransaction();
                 }
                  catch (Exception ex)
                  {
                      bdeiERP.WriterHelper.RollBackTransaction();
                      base.WriteAudit("Deactivate TradeRelation Failure", customerCode, ex.Message + "\n" + ex.StackTrace, AID);
                      ApplicationServer.EventLoger.LogException(ex.Message, ex);
                      throw ex;
                  }
                  finally
                  {
                      bdeiERP.WriterHelper.CheckTransactionIntegrity();
                  }
             }
        }
        public BondPaymentDocument[] GetAllUnreturnedBonds(string query)
        {
            return bdeiERP.GetAllUnreturnedBonds(query);
        }
        #endregion

    }
}
