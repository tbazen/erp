using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using System.Data;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionService : INTAPS.ClientServer.SessionObjectBase<iERPTransactionBDE>
    {

        public int[] GetCandidatesForDeclaration(int declarationType, DateTime declarationDate, int periodID, out int [] taxCenterID, out double[] taxAmount)
        {
            CheckStandardViewPermsion();
            return _bde.GetCandidatesForDeclaration(declarationType, declarationDate, periodID,out taxCenterID, out taxAmount);
        }

        public DataSet PreviewDeclaration(int declarationType, DateTime declarationDate, int periodID, int taxCenterID, int[] documents, int[] rejectedDocuments, out double total, bool newDeclaration)
        {
            CheckStandardViewPermsion();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("PreviewDeclaration Attempt",-1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    DataSet ret = _bde.PreviewDeclaration(AID, declarationType, declarationDate, periodID,taxCenterID, documents, rejectedDocuments, out total, newDeclaration);
                    base.WriteExecuteAudit("PreviewDeclaration Success", -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("PreviewDeclaration",ex.Message ,ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.CheckTransactionIntegrity();
                }
            }
            
        }

        public int SaveTaxDeclaration(int declarationType, DateTime declarationDate, int periodID, int[] documents, int taxCenterID)
        {
            CheckCasheirPermission();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("SaveTaxDeclaration Attempt", -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.SaveTaxDeclaration(AID,declarationType, declarationDate, periodID, documents, taxCenterID);
                    base.WriteExecuteAudit("SaveTaxDeclaration Success", -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("SaveTaxDeclaration", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.CheckTransactionIntegrity();
                }
            }
        }

        public TaxDeclaration[] GetTaxDeclarations(int declarationType, int period1, int period2)
        {
            return _bde.GetTaxDeclarations(declarationType, period1, period2);
        }

        public TaxDeclaration GetTaxDeclaration(int declatationID)
        {
            return _bde.GetTaxDeclaration(declatationID);
        }
        public TaxDeclaration GetTaxDeclaration(int declarationType, int periodID)
        {
            return _bde.GetTaxDeclaration(declarationType, periodID);
        }
        public bool HasTaxDeclarationBegan(int declarationType)
        {
            return _bde.HasTaxDeclarationBegan(declarationType);
        }
        public void DeleteDeclaration(int declarationID)
        {
            CheckCasheirPermission();
            _bde.DeleteDeclaration(declarationID);
        }

        public void UpdateDeclaration(TaxDeclaration decl)
        {
            CheckCasheirPermission();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("UpdateDeclaration Attempt", -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    _bde.UpdateDeclaration(AID,decl);
                    base.WriteExecuteAudit("UpdateDeclaration Success", -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("UpdateDeclaration", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.CheckTransactionIntegrity();
                }
            }
            
        }
        public int CountPaidTaxDeclarations(int declarationType)
        {
          return  _bde.CountPaidTaxDeclarations(declarationType);
        }
        public bool IsPreviousDeclarationPeriodConfirmed(int declarationType, int currPeriodID)
        {
            return _bde.IsPreviousDeclarationPeriodConfirmed(declarationType, currPeriodID);
        }
        public TaxCenter[] GetTaxCenters()
        {
            return _bde.GetTaxCenters();
        }
        public TaxCenter GetTaxCenter(int id)
        {
            return _bde.GetTaxCenter(id);
        }
    }
}
