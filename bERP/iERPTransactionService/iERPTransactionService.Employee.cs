using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Payroll;
using INTAPS.Accounting;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionService
    {
        public int RegisterEmployee(Employee employee, byte[] picture, bool updateWorkingData, int[] otherAccounts, int periodID, Data_RegularTime regular, OverTimeData overtime, SingleValueData[] othersData, int[] singleValueFormulaID)
        {
            CheckCasheirPermission();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Register Employee Attempt", employee.employeeID, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    int empID = bdeiERP.RegisterEmployee(AID, employee, picture, updateWorkingData,otherAccounts, periodID, regular, overtime, othersData, singleValueFormulaID);
                    base.WriteAddAudit("Register Employee Success", employee.employeeID, -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                    return empID;
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Register Employee Failure", employee.employeeID, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    bdeiERP.CheckTransactionIntegrity();
                }
            }
        }

        public void DeleteEmployee(int ID)
        {
            CheckConfigurationPermision();
             lock (bdeiERP.WriterHelper)
             {
                 int AID = base.WriteAddAudit("Delete Employee Attempt", ID.ToString(), -1);
                 bdeiERP.WriterHelper.BeginTransaction();
                 try
                 {
                     bdeiERP.DeleteEmployee(AID,ID);
                     base.WriteAddAudit("Delete Employee Success", ID.ToString(), -1);
                     bdeiERP.WriterHelper.CommitTransaction();
                 }
                 catch (Exception ex)
                 {
                     bdeiERP.WriterHelper.RollBackTransaction();
                     base.WriteAudit("Delete Employee Failure", ID.ToString(), ex.Message + "\n" + ex.StackTrace, AID);
                     ApplicationServer.EventLoger.LogException(ex.Message, ex);
                     throw ex;
                 }
                 finally
                 {
                     bdeiERP.WriterHelper.CheckTransactionIntegrity();
                 }
             }
        }
        public void DeleteEmployeeAccount(int empID, int accountID)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Delete Employee Account Attempt", accountID.ToString(), -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.DeleteEmployeeAccount(AID,empID,accountID);
                    base.WriteAddAudit("Delete Employee Account Success", accountID.ToString(), -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Delete Employee Account Failure", accountID.ToString(), ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        public void FireEmployee(int ID, DateTime dismissalDate)
        {
            CheckConfigurationPermision();
             lock (bdeiERP.WriterHelper)
             {
                 int AID = base.WriteAddAudit("Dismiss Employee Attempt", ID.ToString(), -1);
                 bdeiERP.WriterHelper.BeginTransaction();
                 try
                 {
                     bdeiERP.FireEmployee(AID,ID, dismissalDate);
                     base.WriteAddAudit("Dismiss Employee Success", ID.ToString(), -1);
                     bdeiERP.WriterHelper.CommitTransaction();
                 }
                 catch (Exception ex)
                 {
                     bdeiERP.WriterHelper.RollBackTransaction();
                     base.WriteAudit("Dismiss Employee Failure",ID.ToString(), ex.Message + "\n" + ex.StackTrace, AID);
                     ApplicationServer.EventLoger.LogException(ex.Message, ex);
                     throw ex;
                 }
                 finally
                 {
                     bdeiERP.WriterHelper.CheckTransactionIntegrity();
                 }
             }
        }
        public void EnrollEmployee(int ID, DateTime enrollDate)
        {
            CheckConfigurationPermision();

            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Enroll Employee Attempt", ID.ToString(), -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.EnrollEmployee(AID, ID, enrollDate);
                    base.WriteAddAudit("Enroll Employee Success", ID.ToString(), -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Enroll Employee Failure", ID.ToString(), ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        public Employee[] GetAllEmployeesExcept(int[] employeeID)
        {
           return bdeiERP.GetAllEmployeesExcept(employeeID);
        }
        public int[] GetPaidSalaryDocs(int empID, PayrollPeriod period)
        {
            return bdeiERP.GetPaidSalaryDocs(empID, period);
        }
        public double GetUnclaimedSalaryBalance(int empID, PayrollPeriod period)
        {
            return bdeiERP.GetUnclaimedSalaryBalance(empID, period);
        }
        public double GetEmployeePaidSalaryTotal(int empID, int periodID)
        {
            return bdeiERP.GetEmployeePaidSalaryTotal(empID, periodID);
        }
        public UnclaimedSalaryDocument[] GetEmployeePaidSalaries(int empID, int periodID)
        {
            return bdeiERP.GetEmployeePaidSalaries(empID, periodID);
        }
        public BatchError GenerateAndPostPayroll(int[] employees, int periodID, DateTime date)
        {
            CheckCasheirPermission();
             lock (bdeiERP.WriterHelper)
             {
                 int AID = base.WriteAddAudit("Generate and Post Payroll Attempt", periodID.ToString(), -1);
                 bdeiERP.WriterHelper.BeginTransaction();
                 try
                 {
                     BatchError error = bdeiERP.GenerateAndPostPayroll(AID,employees, periodID, date);
                     base.WriteAddAudit("Generate and Post Payroll Success", periodID.ToString(), -1);
                     bdeiERP.WriterHelper.CommitTransaction();
                     return error;
                 }
                  catch (Exception ex)
                  {
                      bdeiERP.WriterHelper.RollBackTransaction();
                      base.WriteAudit("Generate and Post Payroll Failure", periodID.ToString(), ex.Message + "\n" + ex.StackTrace, AID);
                      ApplicationServer.EventLoger.LogException(ex.Message, ex);
                      throw ex;
                  }
                  finally
                  {
                      bdeiERP.WriterHelper.CheckTransactionIntegrity();
                  }
             }
        }
        public bool EmployeeExists(int id)
        {
            return bdeiERP.EmployeeExists(id);
        }
        private void CheckPayrollEmployeeDataPermission()
        {
            if (!m_Session.Permissions.IsPermited("root/Payroll/EmployeeData"))
                throw new AccessDeniedException("You are not authroized to edit employee data system");
        }
        public void SetWorkingData(int employeeID, int periodID, Data_RegularTime regular, OverTimeData overtime, SingleValueData[] othersData, int[] singleValueFormulaID)
        {
            CheckPayrollEmployeeDataPermission();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("Set Working Data Attempt", -1);
                try
                {
                    bdeiERP.WriterHelper.BeginTransaction();
                    bdeiERP.SetWorkingData(AID, employeeID, periodID, regular, overtime, othersData, singleValueFormulaID);
                    base.WriteExecuteAudit("Set Working Data Success", -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Set Working Data Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
            
        }
        public void setPermanentWorkingData(int employeeID, int periodID, int formulaID, PayrollComponentData singleValueData, object additionalData)
        {
            CheckPayrollEmployeeDataPermission();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("setPermanentWorkingData Attempt", -1);
                try
                {
                    bdeiERP.WriterHelper.BeginTransaction();
                    bdeiERP.setPermanentWorkingData(AID, employeeID, periodID, formulaID, singleValueData, additionalData);
                    base.WriteExecuteAudit("setPermanentWorkingData Success", -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("setPermanentWorkingData Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }

        }
        public double GetGroupPayrollUnclaimed(int periodID, int taxCenter)
        {
            return bdeiERP.GetGroupPayrollUnclaimed(periodID, taxCenter);
        }
        public double GetGroupPayrollTotalSalaryPaid(int periodID, int taxCenter)
        {
            return bdeiERP.GetGroupPayrollTotalSalaryPaid(periodID, taxCenter);
        }
        public void closeLoan(int loanDocumentID)
        {
            CheckPayrollEmployeeDataPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("closeLoan Attempt",-1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    bdeiERP.closeLoan(AID,loanDocumentID);
                    base.WriteExecuteAudit("closeLoan Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("closeLoan Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
            
        }
    }
}
