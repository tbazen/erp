using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionService
    {

        public ItemCategory GetItemCategory(int ID)
        {
            return _bde.GetItemCategory(ID);
        }

        public ItemCategory[] GetItemCategories(int PID, ItemType itemType)
        {
            return _bde.GetItemCategories(PID,itemType);
        }
        public ItemCategory[] GetItemCategories(int PID)
        {
            return _bde.GetItemCategories(PID);
        }
        public TransactionItems[] GetItemsInCategory(int categID)
        {
            return _bde.GetItemsInCategory(categID);
        }
        public ItemCategory[] GetAllItemCategories()
        {
            return _bde.GetAllItemCategories();
        }

        public void DeleteItemCategory(int ID)
        {
            CheckConfigurationPermision();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteMethodCallAudit("DeleteItemCategory Attempt", ID.ToString(), -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction(); 
                    _bde.DeleteItemCategory(AID, ID);
                    base.WriteExecuteAudit("DeleteItemCategory Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeleteItemCategory Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public int RegisterItemCategory(ItemCategory category)
        {
            CheckConfigurationPermision();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteObjectAudit("RegisterItemCategory Attempt", category.ID.ToString(), category, -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    int ID = _bde.RegisterItemCategory(AID, category);
                    base.WriteExecuteAudit("RegisterItemCategory Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ID;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("RegisterItemCategory Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
    }
}
