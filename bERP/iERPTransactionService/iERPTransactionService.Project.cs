using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Payroll;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionService
    {

        #region Project Methods
        public string RegisterProject(Project project, bool logData)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Register Project Attempt", project.code, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    string code = bdeiERP.RegisterProject(AID, project,logData);
                    base.WriteAddAudit("Register Project Success", project.code, -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                    return code;
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Register Project Failure", project.code, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        public Project GetProjectInfo(string code)
        {
            return bdeiERP.GetProjectInfo(code);
        }
        public Project GetProjectInfo(int projectCostCenterID)
        {
            return bdeiERP.GetProjectInfo(projectCostCenterID);
        }
        public Project[] GetAllProjects()
        {
            return bdeiERP.GetAllProjects();
        }
        public void DeleteProject(string code)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Delete Project Attempt", code, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.DeleteProject(AID, code);
                    base.WriteAddAudit("Delete Project Success", code, -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Delete Project Failure", code, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();

                }
            }

        }
        public void ActivateProject(string code)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Activate Project Attempt", code, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.ActivateProject(AID, code);
                    base.WriteAddAudit("Activate Project Success", code, -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Activate Project Failure", code, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        public void DeactivateProject(string code)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Deactivate Project Attempt", code, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.DeactivateProject(AID, code);
                    base.WriteAddAudit("Deactivate Project Success", code, -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Deactivate Project Failure", code, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public int CreateProjectDivision(int projectCostCenterID, string divisionName)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Create Project Division Attempt", projectCostCenterID.ToString(), -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    int costCenterID = bdeiERP.CreateProjectDivision(AID, projectCostCenterID, divisionName);
                    base.WriteAddAudit("Create Project Division Success", costCenterID.ToString(), -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                    return costCenterID;
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Create Project Division Failure", projectCostCenterID.ToString(), ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        public int CreateProjectMachinaryAndVehicle(int projectCostCenterID, string name)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Create Project Machinary/Vehicle Attempt", projectCostCenterID.ToString(), -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    int costCenterID = bdeiERP.CreateProjectMachinaryAndVehicle(AID, projectCostCenterID, name);
                    base.WriteAddAudit("Create Project Machinary/Vehicle Success", costCenterID.ToString(), -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                    return costCenterID;
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Create Project Machinary/Vehicle Failure", projectCostCenterID.ToString(), ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        public void AddItemAccountsToProject(TransactionItems[] items, string projectCode)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Add Item Accounts To Project Attempt", projectCode, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.AddItemAccountsToProject(AID, items, projectCode);
                    base.WriteAddAudit("Add Item Accounts To Project Success", projectCode, -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Add Item Accounts To Project Failure", projectCode, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        public void AddEmployeeAccountsToProject(Employee[] employees, string projectCode, int divisionCostCenterID)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Add Employees To Project Attempt",projectCode, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.AddEmployeeAccountsToProject(AID, employees, projectCode,divisionCostCenterID);
                    base.WriteAddAudit("Add Employees To Project Success",projectCode, -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Add Employees To Project Failure", projectCode, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        public void AddDivisionToProject(string divisionName, string projectCode)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Add Division To Project Attempt", projectCode, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.AddDivisionToProject(AID, divisionName, projectCode);
                    base.WriteAddAudit("Add Division To Project Success", projectCode, -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Add Division To Project Failure", projectCode, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }

        }
        public void AddMachinaryAndVehicleToProject(string name, string projectCode)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Add Machinary/Vehicle To Project Attempt", projectCode, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.AddMachinaryAndVehicleToProject(AID, name, projectCode);
                    base.WriteAddAudit("Add Machinary/Vehicle To Project Success", projectCode, -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Add Machinary/Vehicle To Project Failure", projectCode, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        public string AddStoreToProject(StoreInfo store, string projectCode)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Add Store To Project Attempt", projectCode, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    string code = bdeiERP.AddStoreToProject(AID, store, projectCode);
                    base.WriteAddAudit("Add Store To Project Success", code, -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                    return code;
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Add Store To Project Failure", projectCode, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }

        }
        public string AddCashAccountToProject(CashAccount cashAccount, string projectCode, int divisionCostCenterID)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Add cash account To Project Attempt", projectCode, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    string code = bdeiERP.AddCashAccountToProject(AID, cashAccount, projectCode,divisionCostCenterID);
                    base.WriteAddAudit("Add cash account To Project Success", code, -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                    return code;
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Add cash account To Project Failure", projectCode, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }


        }
        public int AddBankAccountToProject(BankAccountInfo bankAccount, string projectCode, int divisionCostCenterID)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Add bank account To Project Attempt", projectCode, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    int mainCsID = bdeiERP.AddBankAccountToProject(AID, bankAccount, projectCode,divisionCostCenterID);
                    base.WriteAddAudit("Add bank account To Project Success", mainCsID.ToString(), -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                    return mainCsID;
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Add bank account To Project Failure",projectCode, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        public void RemoveEmployeeFromProject(int employeeID, string projectCode)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Remove employee from Project Attempt", "Employee:" + employeeID + ",Project:" + projectCode, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.RemoveEmployeeFromProject(AID, employeeID, projectCode);
                    base.WriteAddAudit("Remove employee from Project Success", "Employee:" + employeeID + ",Project:" + projectCode, -1);
                    bdeiERP.WriterHelper.CommitTransaction();

                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Remove employee from Project Failure", "Employee:" + employeeID + ",Project:" + projectCode, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        public void RemoveDivisionFromProject(int divisionCostCenterID, string projectCode)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Remove division from Project Attempt", "Division:" + divisionCostCenterID + ",Project:" + projectCode, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.RemoveDivisionFromProject(AID, divisionCostCenterID, projectCode);
                    base.WriteAddAudit("Remove division from Project Success", "Division:" + divisionCostCenterID + ",Project:" +projectCode, -1);
                    bdeiERP.WriterHelper.CommitTransaction();

                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Remove division from Project Failure", "Division:" + divisionCostCenterID + ",Project:" +projectCode, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        public void RemoveMachinaryAndVehicleFromProject(int mvCostCenterID, string projectCode)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Remove Machinary/Vehicle from Project Attempt", "Machinary/Vehicle:" + mvCostCenterID + ",Project:" + projectCode, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.RemoveMachinaryAndVehicleFromProject(AID, mvCostCenterID, projectCode);
                    base.WriteAddAudit("Remove Machinary/Vehicle from Project Success", "Machinary/Vehicle:" + mvCostCenterID + ",Project:" + projectCode, -1);
                    bdeiERP.WriterHelper.CommitTransaction();

                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Remove Machinary/Vehicle from Project Failure", "Machinary/Vehicle:" + mvCostCenterID + ",Project:" + projectCode, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        public void RemoveStoreFromProject(int storeCostCenterID, string storeCode, string projectCode)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Remove store from Project Attempt", "Store:" + storeCostCenterID + ",Project:" + projectCode, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.RemoveStoreFromProject(AID, storeCostCenterID, storeCode,projectCode);
                    base.WriteAddAudit("Remove store from Project Success", "Store:" + storeCostCenterID + ",Project:" +projectCode, -1);
                    bdeiERP.WriterHelper.CommitTransaction();

                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Remove store from Project Failure", "Store:" + storeCostCenterID + ",Project:" + projectCode, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        public void RemoveItemFromProject(string itemCode, string projectCode)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Remove item from Project Attempt", "item:" + itemCode + ",Project:" + projectCode, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.RemoveItemFromProject(AID, itemCode, projectCode);
                    base.WriteAddAudit("Remove item from Project Success", "Item:" + itemCode + ",Project:" + projectCode, -1);
                    bdeiERP.WriterHelper.CommitTransaction();

                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Remove item from Project Failure", "Item:" + itemCode + ",Project:" +projectCode, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        public void RemoveCashAccountFromProject(int csAccountID, string code, string projectCode)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Remove cash account from Project Attempt", "CsAccount ID:" + csAccountID + ",Project:" +projectCode, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.RemoveCashAccountFromProject(AID, csAccountID, code, projectCode);
                    base.WriteAddAudit("Remove cash account from Project Success", "CsAccount ID:" + csAccountID + ",Project:" + projectCode, -1);
                    bdeiERP.WriterHelper.CommitTransaction();

                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Remove cash account from Project Failure", "CsAccount ID::" + csAccountID + ",Project:" + projectCode, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        public void RemoveBankAccountFromProject(int csAccountID, string projectCode)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Remove bank account from Project Attempt", "CsAccount ID:" + csAccountID + ",Project:" + projectCode, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.RemoveBankAccountFromProject(AID, csAccountID, projectCode);
                    base.WriteAddAudit("Remove bank account from Project Success", "CsAccount ID:" + csAccountID + ",Project:" +projectCode, -1);
                    bdeiERP.WriterHelper.CommitTransaction();

                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Remove bank account from Project Failure", "CsAccount ID::" + csAccountID + ",Project:" +projectCode, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        #endregion
    }
}
