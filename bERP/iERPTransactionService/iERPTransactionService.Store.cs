using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionService
    {

        #region Store Methods
        public string RegisterStore(StoreInfo store, int costCenterParentID)
        {
            CheckCasheirPermission();
               lock (bdeiERP.WriterHelper)
               {
                   int AID = base.WriteAddAudit("Register Store Attempt", store.code, -1);
                   bdeiERP.WriterHelper.BeginTransaction();
                   try
                   {
                       string code = bdeiERP.RegisterStore(AID, store,costCenterParentID);
                       base.WriteAddAudit("Register Store Success", store.code, -1);
                       bdeiERP.WriterHelper.CommitTransaction();
                       return code;
                   }
                   catch (Exception ex)
                   {
                       bdeiERP.WriterHelper.RollBackTransaction();
                       base.WriteAudit("Register Store Failure", store.code, ex.Message + "\n" + ex.StackTrace, AID);
                       ApplicationServer.EventLoger.LogException(ex.Message, ex);
                       throw ex;
                   }
                   finally
                   {
                       bdeiERP.WriterHelper.CheckTransactionIntegrity();
                   }
               }
        }
        public StoreInfo GetStoreInfo(string code)
        {
            return bdeiERP.GetStoreInfo(code);
        }
        public StoreInfo[] GetAllStores()
        {
            return bdeiERP.GetAllStores();
        }
        public void DeleteStore(string code)
        {
            CheckConfigurationPermision();
             lock (bdeiERP.WriterHelper)
             {
                 int AID = base.WriteAddAudit("Delete Store Attempt", code, -1);
                 bdeiERP.WriterHelper.BeginTransaction();
                 try
                 {
                     bdeiERP.DeleteStore(AID, code);
                     base.WriteAddAudit("Delete Store Success", code, -1);
                     bdeiERP.WriterHelper.CommitTransaction();
                 }
                 catch (Exception ex)
                 {
                     bdeiERP.WriterHelper.RollBackTransaction();
                     base.WriteAudit("Delete Store Failure", code, ex.Message + "\n" + ex.StackTrace, AID);
                     ApplicationServer.EventLoger.LogException(ex.Message, ex);
                     throw ex;
                 }
                 finally
                 {
                     bdeiERP.WriterHelper.CheckTransactionIntegrity();

                 }
             }

        }
        public void ActivateStore(string code)
        {
            CheckConfigurationPermision();
             lock (bdeiERP.WriterHelper)
             {
                 int AID = base.WriteAddAudit("Activate Store Attempt", code, -1);
                 bdeiERP.WriterHelper.BeginTransaction();
                 try
                 {
                     bdeiERP.ActivateStore(AID, code);
                     base.WriteAddAudit("Activate Store Success", code, -1);
                     bdeiERP.WriterHelper.CommitTransaction();
                 }
                 catch (Exception ex)
                 {
                     bdeiERP.WriterHelper.RollBackTransaction();
                     base.WriteAudit("Activate Store Failure", code, ex.Message + "\n" + ex.StackTrace, AID);
                     ApplicationServer.EventLoger.LogException(ex.Message, ex);
                     throw ex;
                 }
                 finally
                 {
                     bdeiERP.WriterHelper.CheckTransactionIntegrity();
                 }
             }
        }
        public void DeactivateStore(string code)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Deactivate Store Attempt", code, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.DeactivateStore(AID, code);
                    base.WriteAddAudit("Deactivate Store Success", code, -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Deactivate Store Failure", code, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        #endregion

    }
}
