using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using System.Data;
using INTAPS.Accounting;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionService : INTAPS.ClientServer.SessionObjectBase<iERPTransactionBDE>
    {






        public string renderSummaryTable(string summarySettingField, int costCenterID, DateTime time1, DateTime time2, out int maxLevel)
        {
            return _bde.renderSummaryTable(summarySettingField, costCenterID, time1, time2, out maxLevel);
        }



        public TradeRelation GetRelation(string code)
        {
            return _bde.GetRelation(code);
        }

        public Country[] GetCountries()
        {
            return _bde.GetCountries();
        }

        public Country GetCountryById(short id)
        {
            return _bde.GetCountryById(id);
        }
        public string RegisterRelation(TradeRelation relation)
        {
            CheckCasheirPermission();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteAddAudit("Register TradeRelation Attempt", relation.Code, -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    string supplierCode = bdeiERP.RegisterRelation(AID, relation);
                    base.WriteAddAudit("Register TradeRelation Success", relation.Code, -1);
                    bdeiERP.WriterHelper.CommitTransaction();
                    return supplierCode;
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Register TradeRelation Failure", relation.Code, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public double GetDepreciation(int costCenteriD, string itemCode, DateTime dateTime)
        {
            return _bde.GetDepreciation(costCenteriD, itemCode, dateTime);
        }
        public double GetDepreciation(int costCenteriD, string itemCode, DateTime dateTime, out double rate, out double year)
        {
            return _bde.GetDepreciation(costCenteriD, itemCode, dateTime, out rate, out year);
        }

        public FixedAssetDepreciationDocument previewDepreciation(DateTime time)
        {
            return _bde.previewDepreciation(time);
        }

        public int getDepreciationDocument(DateTime time)
        {
            return _bde.getDepreciationDocument(time);
        }

        public TaxRates getTaxRates()
        {
            return _bde.getTaxRates();
        }

        public TransactionItems GetTransactionItems(int accountID)
        {
            return _bde.GetTransactionItems(accountID);
        }

        public BankReconciliationDocument getLastBankReconciliation(int bankID, DateTime date)
        {
            return _bde.getLastBankReconciliation(bankID, date);
        }

        public void ActivateRelation(string code)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("Activate Relation Attempt", -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.ActivateRelation(AID, code);
                    base.WriteExecuteAudit("Activate Relation Successs", AID);
                    bdeiERP.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Activate Relation Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public void DeactivateRelation(string code)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("Deactivate Relation Attempt", -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.DeactivateRelation(AID, code);
                    base.WriteExecuteAudit("Deactivate Relation Successs", AID);
                    bdeiERP.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Deactivate Relation Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public void DeleteRelation(string code)
        {
            CheckConfigurationPermision();
            lock (bdeiERP.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("Delete Relation Attempt", -1);
                bdeiERP.WriterHelper.BeginTransaction();
                try
                {
                    bdeiERP.DeleteRelation(AID, code);
                    base.WriteExecuteAudit("Delete Relation Successs", AID);
                    bdeiERP.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeiERP.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Delete Relation Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    bdeiERP.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        //public string renderInventoryTableWithoutTitle(InventoryGeneratorParameters pars)
        //{
        //    return _bde.renderInventoryTable(InventoryGeneratorParameters pars);
        //}
        public string renderInventoryTable(InventoryGeneratorParameters pars)
        {
            return _bde.renderInventoryTable(pars);
        }

        public string renderCostCenterProfile(int costCenterID, DateTime time1, DateTime time2)
        {
            return _bde.renderCostCenterProfile(costCenterID, time1, time2);
        }
    }
}
