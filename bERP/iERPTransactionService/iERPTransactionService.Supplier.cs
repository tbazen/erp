using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionService
    {

        #region TradeRelation Methods
        public string RegisterSupplier(TradeRelation supplier)
        {
            CheckCasheirPermission();
             lock (bdeiERP.WriterHelper)
             {
                 int AID = base.WriteAddAudit("Register TradeRelation Attempt", supplier.Code, -1);
                 bdeiERP.WriterHelper.BeginTransaction();
                 try
                 {
                     string supplierCode = bdeiERP.RegisterSupplier(AID,supplier);
                     base.WriteAddAudit("Register TradeRelation Success", supplier.Code, -1);
                     bdeiERP.WriterHelper.CommitTransaction();
                     return supplierCode;
                 }
                 catch (Exception ex)
                 {
                     bdeiERP.WriterHelper.RollBackTransaction();
                     base.WriteAudit("Register TradeRelation Failure", supplier.Code, ex.Message + "\n" + ex.StackTrace, AID);
                     ApplicationServer.EventLoger.LogException(ex.Message, ex);
                     throw ex;
                 }
                 finally
                 {
                     bdeiERP.WriterHelper.CheckTransactionIntegrity();
                 }
             }
        }
        public TradeRelation GetSupplier(string code)
        {
            return bdeiERP.GetSupplier(code);
        }
        public TradeRelation[] GetAllSuppliers()
        {
            return bdeiERP.GetAllSuppliers();
        }
        public TradeRelation[] SearchSuppliers(int index, int NoOfPages, object[] criteria, string[] column, out int NoOfRecords)
        {
            CheckStandardViewPermsion();
            return bdeiERP.SearchSuppliers(index, NoOfPages, criteria, column, out NoOfRecords);
        }
        public void DeleteSupplier(string[] code)
        {
            CheckConfigurationPermision();
             lock (bdeiERP.WriterHelper)
             {
                 int AID = base.WriteAddAudit("Delete TradeRelation Attempt", code[0], -1);
                 bdeiERP.WriterHelper.BeginTransaction();
                 try
                 {
                     bdeiERP.DeleteSupplier(AID,code);
                     base.WriteAddAudit("Delete TradeRelation Success", code[0], -1);
                     bdeiERP.WriterHelper.CommitTransaction();
                 }
                 catch (Exception ex)
                 {
                     bdeiERP.WriterHelper.RollBackTransaction();
                     base.WriteAudit("Delete TradeRelation Failure", code[0], ex.Message + "\n" + ex.StackTrace, AID);
                     ApplicationServer.EventLoger.LogException(ex.Message, ex);
                     throw ex;
                 }
                 finally
                 {
                     bdeiERP.WriterHelper.CheckTransactionIntegrity();

                 }
             }
        }
        public void ActivateSupplier(string supplierCode)
        {
            CheckConfigurationPermision();
             lock (bdeiERP.WriterHelper)
             {
                 int AID = base.WriteAddAudit("Activate TradeRelation Attempt", supplierCode, -1);
                 bdeiERP.WriterHelper.BeginTransaction();
                 try
                 {
                     bdeiERP.ActivateSupplier(AID,supplierCode);
                     base.WriteAddAudit("Activate TradeRelation Success", supplierCode, -1);
                     bdeiERP.WriterHelper.CommitTransaction();
                 }
                 catch (Exception ex)
                 {
                     bdeiERP.WriterHelper.RollBackTransaction();
                     base.WriteAudit("Activate TradeRelation Failure", supplierCode, ex.Message + "\n" + ex.StackTrace, AID);
                     ApplicationServer.EventLoger.LogException(ex.Message, ex);
                     throw ex;
                 }
                 finally
                 {
                     bdeiERP.WriterHelper.CheckTransactionIntegrity();
                 }
             }
        }
        public void DeactivateSupplier(string supplierCode)
        {
            CheckConfigurationPermision();
             lock (bdeiERP.WriterHelper)
             {
                 int AID = base.WriteAddAudit("Deactivate TradeRelation Attempt", supplierCode, -1);
                 bdeiERP.WriterHelper.BeginTransaction();
                 try
                 {
                     bdeiERP.DeactivateSupplier(AID,supplierCode);
                     base.WriteAddAudit("Deactivate TradeRelation Success", supplierCode, -1);
                     bdeiERP.WriterHelper.CommitTransaction();
                 }
                 catch (Exception ex)
                 {
                     bdeiERP.WriterHelper.RollBackTransaction();
                     base.WriteAudit("Deactivate TradeRelation Failure", supplierCode, ex.Message + "\n" + ex.StackTrace, AID);
                     ApplicationServer.EventLoger.LogException(ex.Message, ex);
                     throw ex;
                 }
                 finally
                 {
                     bdeiERP.WriterHelper.CheckTransactionIntegrity();
                 }
             }

        }
        #endregion

    }
}
