using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionService
    {
        public ZReportDocument[] GetZReport(AccountingPeriod period)
        {
            CheckStandardViewPermsion();
            ZReportDocument[] docs = bdeiERP.GetZReport(period);
            return docs;
        }
       public double GetSalesTaxableAndNonTaxable(DateTime date,out double nonTaxableAmount,out double tax)
       {
           double taxable = bdeiERP.GetSalesTaxableAndNonTaxable(date, out nonTaxableAmount,out tax);
           return taxable;
       }

        internal CashAccount GetStaffExpenseAdvanceCashAccount(int employeeID)
        {
            return bdeiERP.GetStaffExpenseAdvanceCashAccount(employeeID);
        }
    }
}
