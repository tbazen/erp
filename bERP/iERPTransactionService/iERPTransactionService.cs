using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;

namespace BIZNET.iERP.Server
{
    public partial class iERPTransactionService : INTAPS.ClientServer.SessionObjectBase<iERPTransactionBDE>
    {


        iERPTransactionBDE bdeiERP;
        public iERPTransactionService(UserSessionData session)
            : base(session, (iERPTransactionBDE)ApplicationServer.GetBDE("iERP"))
        {
            bdeiERP = (iERPTransactionBDE)ApplicationServer.GetBDE("iERP");
        }

        void CheckConfigurationPermision()
        {
            if (!base.m_Session.Permissions.IsPermited("root/iERP/Configuration"))
                throw new Exception("You are not allowed to change sysetm configurations");
        }

        void CheckCasheirPermission()
        {
            if (!base.m_Session.Permissions.IsPermited("root/iERP/Cashier"))
                throw new Exception("You are not allowed to access cashier specific permissions");

        }

        void CheckStandardViewPermsion()
        {
            if (!base.m_Session.Permissions.IsPermited("root/iERP/StandardView"))
                throw new Exception("You are not allowed to access standard view permissions");
        }

        void CheckOwnerViewPermission()
        {
            if (!base.m_Session.Permissions.IsPermited("root/iERP/OwnerView"))
                throw new Exception("You are not allowed to access owner's permissions");
        }

        public object[] GetSystemParamters(string[] names)
        {
            //security check
            //atempt audit log
            //call bde
            //success/failure log
            //check transaction integrity
            return bdeiERP.GetSystemParameters(names);
        }

        public void SetSystemParameters(string[] fields, object[] vals)
        {
            CheckConfigurationPermision();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("Set System Parameters Attempt", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    bdeiERP.SetSystemParameters(AID, fields, vals);
                    base.WriteExecuteAudit("Set System Parameters Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Set System Parameters Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public int CountReferedDocuments(DocumentReferenceType type, string refNumber)
        {
            return bdeiERP.CountReferedDocuments(type, refNumber);
        }

        public bERPDocumentReference[] GetReferedDocuments(DocumentReferenceType type, string refNumber)
        {
            return bdeiERP.GetReferedDocuments(type, refNumber);
        }

        public BIZNET.iERP.TypedDataSets.VATDeclarationData GetVATDeclaration(int periodID)
        {
            return _bde.GetVATDeclaration(periodID);
        }
        public void GenerateYearAccountingPeriod(int year)
        {
            _bde.GenerateYearAccountingPeriod(year);
        }

        public TransactionItems GetTransactionItems(string code)
        {
            return bdeiERP.GetTransactionItems(code);
        }
        public TransactionItems[] SearchTransactionItems(int index, int pageSize, object[] criteria, string[] column, out int NoOfRecords)
        {
            return bdeiERP.SearchTransactionItems(index, pageSize, criteria, column, out NoOfRecords);
        }
        public MeasureUnit[] GetMeasureUnits()
        {
            return bdeiERP.GetMeasureUnits();
        }
        public MeasureUnit GetMeasureUnit(int ID)
        {
            return bdeiERP.GetMeasureUnit(ID);
        }
        public int RegisterMeasureUnit(MeasureUnit measureUnit)
        {
            CheckConfigurationPermision();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteObjectAudit("RegisterMeasureUnit Attempt", measureUnit.ID.ToString(), measureUnit, -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    int id = _bde.RegisterMeasureUnit(AID, measureUnit);
                    base.WriteExecuteAudit("RegisterMeasureUnit Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return id;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("RegisterMeasureUnit Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public string RegisterTransactionItem(TransactionItems item, int[] costCenterID)
        {
            CheckConfigurationPermision();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteObjectAudit("RegisterTransactionItem Attempt", item.Code, item, -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    string code = _bde.RegisterTransactionItem(AID, item, costCenterID);
                    base.WriteExecuteAudit("RegisterTransactionItem Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return code;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("RegisterTransactionItem Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void DeleteTransactionItem(string code)
        {
            CheckConfigurationPermision();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteMethodCallAudit("DeleteTransactionItem Attempt", code, -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.DeleteTransactionItem(AID, code);
                    base.WriteExecuteAudit("DeleteTransactionItem Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeleteTransactionItem Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void ActivateTransactionItem(string itemCode)
        {
            CheckConfigurationPermision();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteMethodCallAudit("ActivateTransactionItem Attempt", itemCode, -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.ActivateTransactionItem(AID, itemCode);
                    base.WriteExecuteAudit("ActivateTransactionItem Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("ActivateTransactionItem Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void DeactivateTransactionItem(string itemCode)
        {
            CheckConfigurationPermision();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteMethodCallAudit("DeactivateTransactionItem Attempt", itemCode, -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.DeactivateTransactionItem(AID, itemCode);
                    base.WriteExecuteAudit("DeactivateTransactionItem Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeactivateTransactionItem Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public bool isBERPClientFileUpdated(string fileName, byte[] clientMD5)
        {
            return _bde.isBERPClientFileUpdated(fileName, clientMD5);
        }

        public byte[] getERPClientLatestFile(string fileName)
        {
            return _bde.getERPClientLatestFile(fileName);
        }

        public string[] getAllClientFiles()
        {
            return _bde.getAllClientFiles();
        }

        public INTAPS.Accounting.Account GetEmployeeAccount(int parentAccountID, int employeeID)
        {
            return _bde.GetEmployeeAccount(parentAccountID, employeeID);
        }

        public string getPrimaryJournal(JournalFilter filter, int pageIndex, int pageSize, out int NRecord)
        {
            return _bde.getPrimaryJournal(filter, pageIndex, pageSize, out NRecord);
        }

        public string getDocumentListHtmlByTypedReference(int typeID, string reference)
        {
            return _bde.getDocumentListHtmlByTypedReference(typeID, reference);
        }

        public string getDocumentEntriesHTML(int docID)
        {
            return _bde.Accounting.getDocumentEntriesHTML(docID, _bde.SysPars.summerizeItemTransactions);
        }

        public string getTypedReferenceEntriesHTML(INTAPS.Accounting.DocumentTypedReference tref)
        {
            return _bde.getTypedReferenceEntriesHTML(tref);
        }

        public BIZNET.iERP.Budget getBudget(int budgetID)
        {
            return _bde.getBudget(budgetID);
        }

        public BIZNET.iERP.Budget getBudget(int costCenterID, System.DateTime target, bool exact)
        {
            return _bde.getBudget(costCenterID, target, exact);
        }

        public BIZNET.iERP.Budget[] getAllBudgets(int costCenterID)
        {
            return _bde.getAllBudgets(costCenterID);
        }

        public void updateBudget(BIZNET.iERP.Budget budget, BIZNET.iERP.BudgetEntry[] entries)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("updateBudget Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.updateBudget(AID, budget, entries);
                    base.WriteExecuteAudit("updateBudget Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("updateBudget Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void createBudget(BIZNET.iERP.Budget budget, BIZNET.iERP.BudgetEntry[] entries)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("createBudget Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.createBudget(AID, budget, entries);
                    base.WriteExecuteAudit("createBudget Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("createBudget Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void deleteBudget(int budgetID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteBudget Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteBudget(AID, budgetID);
                    base.WriteExecuteAudit("deleteBudget Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteBudget Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public BIZNET.iERP.BudgetEntry[] getBudgetEntries(int budgetID)
        {
            return _bde.getBudgetEntries(budgetID);
        }

        public string getBudgetReport(BIZNET.iERP.BudgetFilter filter)
        {
            return _bde.getBudgetReport(filter);
        }

        public BIZNET.iERP.ClosedRange[] getBookClosings()
        {
            return _bde.getBookClosings();
        }

        public void closeBookForTransaction(BIZNET.iERP.ClosedRange range)
        {
            CheckOwnerViewPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("closeBookForTransaction Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.closeBookForTransaction(AID, range);
                    base.WriteExecuteAudit("closeBookForTransaction Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("closeBookForTransaction Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void updateBookClosingForTransaction(BIZNET.iERP.ClosedRange range)
        {
            CheckOwnerViewPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("updateBookClosingForTransaction Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.updateBookClosingForTransaction(AID, range);
                    base.WriteExecuteAudit("updateBookClosingForTransaction Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("updateBookClosingForTransaction Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void openBookForTransaction()
        {
            CheckOwnerViewPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("openBookForTransaction Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.openBookForTransaction(AID);
                    base.WriteExecuteAudit("openBookForTransaction Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("openBookForTransaction Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public System.DateTime getTransactionsClosedUpto()
        {
            return _bde.getTransactionsClosedUpto();
        }

        public bool AllowNegativeTransactionPost()
        {
            return _bde.AllowNegativeTransactionPost();
        }

        public void createTradeRelationCategory(BIZNET.iERP.TradeRelationCategory cat)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("createTradeRelationCategory Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.createTradeRelationCategory(AID, cat);
                    base.WriteExecuteAudit("createTradeRelationCategory Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("createTradeRelationCategory Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void updateTradeRelationCategory(BIZNET.iERP.TradeRelationCategory cat)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("updateTradeRelationCategory Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.updateTradeRelationCategory(AID, cat);
                    base.WriteExecuteAudit("updateTradeRelationCategory Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("updateTradeRelationCategory Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void deleteTradeRelationCategory(string tradeRelationCategoryCode)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteTradeRelationCategory Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteTradeRelationCategory(AID, tradeRelationCategoryCode);
                    base.WriteExecuteAudit("deleteTradeRelationCategory Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteTradeRelationCategory Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public BIZNET.iERP.TradeRelation[] getTradeRelationsByCategory(string parentCategoryCode)
        {
            return _bde.getTradeRelationsByCategory(parentCategoryCode);
        }

        public BIZNET.iERP.TradeRelationCategory[] getCategories(string parentCategoryCode)
        {
            return _bde.getCategories(parentCategoryCode);
        }

        public BIZNET.iERP.TradeRelation[] searchTradeRelation(string categoryCode, BIZNET.iERP.TradeRelationType[] types, int index, int pageSize, object[] criteria, string[] column, out int NoOfRecords)
        {
            return _bde.searchTradeRelation(categoryCode, types, index, pageSize, criteria, column, out NoOfRecords);
        }

        public BIZNET.iERP.TradeRelationCategory getTradeRelationCategory(string code)
        {
            return _bde.getTradeRelationCategory(code);
        }

        public BIZNET.iERP.ItemCategory GetItemCategory(string Code)
        {
            return _bde.GetItemCategory(Code);
        }

        public string generateDetaileCashFlowReport(System.DateTime date1, System.DateTime date2, BIZNET.iERP.DetailedCashFlowReportOption options)
        {
            return _bde.generateDetaileCashFlowReport(date1, date2, options);
        }

        public string getTrialBalance<AgregateType, DetailType>(System.Collections.Generic.List<int> expandedAccount, List<int> rootAccounts, AgregateType agregateTo, System.DateTime date, bool twoColumns)
            where AgregateType : AccountBase, new()
            where DetailType : AccountBase, new()
        {
            return _bde.getTrialBalance<AgregateType, DetailType>(expandedAccount, rootAccounts, agregateTo, date, twoColumns);
        }

        public BIZNET.iERP.PaymentInstrumentType[] getAllPaymentInstrumentTypes()
        {
            return _bde.getAllPaymentInstrumentTypes();
        }

        public BIZNET.iERP.PaymentInstrumentType getPaymentInstrumentType(string itemCode)
        {
            return _bde.getPaymentInstrumentType(itemCode);
        }

        public BIZNET.iERP.BankInfo getBankInfo(int bankID)
        {
            return _bde.getBankInfo(bankID);
        }

        public BIZNET.iERP.BankBranchInfo getBankBranchInfo(int branchID)
        {
            return _bde.getBankBranchInfo(branchID);
        }

        public BIZNET.iERP.BankInfo[] getAllBanks()
        {
            return _bde.getAllBanks();
        }

        public BIZNET.iERP.BankBranchInfo[] getAllBranchsOfBank(int bankID)
        {
            return _bde.getAllBranchsOfBank(bankID);
        }

        public void savePaymentInstrumentType(BIZNET.iERP.PaymentInstrumentType type)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("savePaymentInstrumentType Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.savePaymentInstrumentType(AID, type);
                    base.WriteExecuteAudit("savePaymentInstrumentType Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("savePaymentInstrumentType Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void deletePaymentInstrumentType(string typeID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deletePaymentInstrumentType Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deletePaymentInstrumentType(AID, typeID);
                    base.WriteExecuteAudit("deletePaymentInstrumentType Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deletePaymentInstrumentType Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public int saveBankInfo(BIZNET.iERP.BankInfo bank)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("saveBankInfo Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.saveBankInfo(AID, bank);
                    base.WriteExecuteAudit("saveBankInfo Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("saveBankInfo Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public int saveBankBranchInfo(BIZNET.iERP.BankBranchInfo branch)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("saveBankBranchInfo Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.saveBankBranchInfo(AID, branch);
                    base.WriteExecuteAudit("saveBankBranchInfo Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("saveBankBranchInfo Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void deleteBankInfo(int bankID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteBankInfo Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteBankInfo(AID, bankID);
                    base.WriteExecuteAudit("deleteBankInfo Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteBankInfo Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void deleteBankBranchInfo(int branchID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteBankBranchInfo Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteBankBranchInfo(AID, branchID);
                    base.WriteExecuteAudit("deleteBankBranchInfo Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteBankBranchInfo Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public BIZNET.iERP.FixedAssetRuleAttribute getFixedAssetRuleAttribute(int typeID)
        {
            return _bde.getFixedAssetRuleAttribute(typeID);
        }

        public BIZNET.iERP.FixedAssetRuleAttribute[] getAllFixedAssetRuleAttributes()
        {
            return _bde.getAllFixedAssetRuleAttributes();
        }

        public void saveFixedAssetRuleSetting(int typeID, object data)
        {
            _bde.saveFixedAssetRuleSetting(typeID, data);
        }

        public BIZNET.iERP.CashAccountCategory[] getChildCashCategories(int categoryID)
        {
            return _bde.getChildCashCategories(categoryID);
        }

        public BIZNET.iERP.CashAccountCategory getCashCategory(int categoryID)
        {
            return _bde.getCashCategory(categoryID);
        }

        public int createCashCategory(BIZNET.iERP.CashAccountCategory cat)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("createCashCategory Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.createCashCategory(AID, cat);
                    base.WriteExecuteAudit("createCashCategory Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("createCashCategory Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public BIZNET.iERP.CashAccount[] getCashAccountsByCategory(int categoryID)
        {
            return _bde.getCashAccountsByCategory(categoryID);
        }

        public void updateCashCategory(BIZNET.iERP.CashAccountCategory cat)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("updateCashCategory Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.updateCashCategory(AID, cat);
                    base.WriteExecuteAudit("updateCashCategory Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("updateCashCategory Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void deleteCashCategory(int catID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteCashCategory Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteCashCategory(AID, catID);
                    base.WriteExecuteAudit("deleteCashCategory Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteCashCategory Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public string renderLedgerTable(LedgerParameters pars)
        {
            return _bde.renderLedgerTable(pars);
        }

        public void recalculateInventoryWeightedAverage()
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("recalculateInventoryWeightedAverage Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.recalculateInventoryWeightedAverage(AID);
                    base.WriteExecuteAudit("recalculateInventoryWeightedAverage Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("recalculateInventoryWeightedAverage Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void SaveStockLevelConfiguration(StockLevelConfiguration[] config)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("SaveStockLevelConfiguration Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.SaveStockLevelConfiguration(AID, config);
                    base.WriteExecuteAudit("SaveStockLevelConfiguration Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("SaveStockLevelConfiguration Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public StockLevelConfiguration[] getStockLevelConfiguration()
        {
            return _bde.getStockLevelConfiguration();
        }

        public void setConvesionFactors(string itemScope, int categoryScope, BIZNET.iERP.UnitCoversionFactor[] factors)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("setConvesionFactors Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.setConvesionFactors(AID, itemScope, categoryScope, factors);
                    base.WriteExecuteAudit("setConvesionFactors Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("setConvesionFactors Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public BIZNET.iERP.UnitCoversionFactor[] getConversionFactors(string itemScope, int categoryScope)
        {
            return _bde.getConversionFactors(itemScope, categoryScope);
        }

        public BIZNET.iERP.UnitConvertor getItemUnitConvertor(string itemCode)
        {
            return _bde.getItemUnitConvertor(itemCode);
        }

        public bool isDocumentBudgetSet(int documentID)
        {
            return _bde.isDocumentBudgetSet(documentID);
        }

        public INTAPS.Accounting.AccountDocument[] getUnbudgetedDocuments(INTAPS.Accounting.DocumentSearchPar pars, int index, int pageSize, out int N)
        {
            return _bde.getUnbudgetedDocuments(pars, index, pageSize, out N);
        }

        public BIZNET.iERP.PropertyType[] getAllPropertTypes()
        {
            return _bde.getAllPropertTypes();
        }

        public BIZNET.iERP.Property getPropertyByCode(string propertyCode, out object extraData)
        {
            return _bde.getPropertyByCode(propertyCode, out extraData);
        }

        public BIZNET.iERP.Property getProperty(int propertyID, out object extraData)
        {
            return _bde.getProperty(propertyID, out extraData);
        }

        public int createProperty(BIZNET.iERP.Property prop, object extraData)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("createProperty Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.createProperty(AID, prop, extraData);
                    base.WriteExecuteAudit("createProperty Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("createProperty Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void updateProperty(BIZNET.iERP.Property prop, object extraData)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("updateProperty Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.updateProperty(AID, prop, extraData);
                    base.WriteExecuteAudit("updateProperty Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("updateProperty Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public BIZNET.iERP.PropertyItemCategory[] getPropertyItemCategory(int parentCategoryID, int pageIndex, int pageSize, out int N)
        {
            return _bde.getPropertyItemCategory(parentCategoryID, pageIndex, pageSize, out N);
        }

        public BIZNET.iERP.Property getPropertyByItemCode(string propertyCode, out object extraData)
        {
            return _bde.getPropertyByItemCode(propertyCode, out extraData);
        }

        public void deleteProperty(int propertyID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteProperty Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteProperty(AID, propertyID);
                    base.WriteExecuteAudit("deleteProperty Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteProperty Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public BIZNET.iERP.PropertyItemCategory getPropertyItemCategory(string itemCode)
        {
            return _bde.getPropertyItemCategory(itemCode);
        }

        public BIZNET.iERP.Property[] getPropertiesByParentItemCode(string parentItemCode, int pageIndex, int pageSize, out int N)
        {
            return _bde.getPropertiesByParentItemCode(parentItemCode, pageIndex, pageSize, out N);
        }

        public string getTradeRelationTransactions(string relationCode, long ticksFrom, long ticksTo, int[] docTypes)
        {
            return _bde.getTradeRelationTransactions(relationCode, ticksFrom, ticksTo, docTypes);
        }

        public BIZNET.iERP.MeasureUnit GetMeasureUnit(string name)
        {
            return _bde.GetMeasureUnit(name);
        }

        public double convertUnit(string itemCode, double quantity, int fromUnit, int toUnit)
        {
            return _bde.convertUnit(itemCode, quantity, fromUnit, toUnit);
        }

        public BIZNET.iERP.PropertyType getPropertyType(int id)
        {
            return _bde.getPropertyType(id);
        }

        public INTAPS.Accounting.AccountBalance[] getMaterialBalance(System.DateTime time, string itemCode, int[] itemIDs)
        {
            return _bde.getMaterialBalance(time, itemCode, itemIDs);
        }


        //attachment related methods

        public void rearrangeAttachedFileItems(int fileGroupID, int[] oldOrdern, int[] newOrdern)
        {
            if (!base.m_Session.Permissions.IsPermited("Root/iERP/attachment/attach"))
                throw new Exception("You are not allowed to rearrange attached files");
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("rearrangeAttachedFileItems Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.rearrangeAttachedFileItems(AID, fileGroupID, oldOrdern, newOrdern);
                    base.WriteExecuteAudit("rearrangeAttachedFileItems Success", AID);
                    _bde.WriterHelper.CommitTransaction();

                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("rearrangeAttachedFileItems Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public int replaceAttachmentFileItem(AttachedFileItem item)
        {
            if (!base.m_Session.Permissions.IsPermited("Root/iERP/attachment/attach"))
                throw new Exception("You are not allowed to replace attachment file documents");
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("replaceAttachmentFileItem Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.replaceAttachmentFileItem(AID, item);
                    base.WriteExecuteAudit("replaceAttachmentFileItem Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("replaceAttachmentFileItem Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public int attachFile(int documentID, AttachedFileGroup fileGroup, IEnumerable<AttachedFileItem> items)
        {
            if (!base.m_Session.Permissions.IsPermited("Root/iERP/attachment/attach"))
                throw new Exception("You are not allowed to attach files to transactions");
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("attachFile Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.attachFile(AID, documentID, fileGroup, items);
                    base.WriteExecuteAudit("attachFile Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("attachFile Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public List<int> attachMultipeFile(int documentID, IEnumerable<AttachedFileGroup> fileGroup, IEnumerable<IEnumerable<AttachedFileItem>> items)
        {
            if (!base.m_Session.Permissions.IsPermited("Root/iERP/attachment/attach"))
                throw new Exception("You are not allowed to attach files to transactions");
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("attachMultipeFile Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    List<int> ret = _bde.attachMultipeFile(AID, documentID, fileGroup, items);
                    base.WriteExecuteAudit("attachFile Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("attachMultipeFile Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void deleteAttachmentFileItem(int attachmentFileID)
        {
            if (!base.m_Session.Permissions.IsPermited("Root/iERP/attachment/attach"))
                throw new Exception("You are not allowed to delete scanned documents");
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteAttachmentFileItem Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteAttachmentFileItem(AID, attachmentFileID);
                    base.WriteExecuteAudit("deleteAttachmentFileItem Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteAttachmentFileItem Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void detachAttachmentFileGroup(int attachmentFileGroupID, int documentID)
        {
            if (!base.m_Session.Permissions.IsPermited("Root/iERP/attachment/attach"))
                throw new Exception("You are not allowed to detach scanned documents");
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("detachAttachmentFileGroup Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.detachAttachmentFileGroup(AID, attachmentFileGroupID, documentID);
                    base.WriteExecuteAudit("detachAttachmentFileGroup Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("detachAttachmentFileGroup Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void deleteAttachmentFileGroup(int attachmentFileGroupID)
        {
            if (!base.m_Session.Permissions.IsPermited("Root/iERP/attachment/attach"))
                throw new Exception("You are not allowed to detach scanned documents");
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteAttachmentFileGroup Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteAttachmentFileGroup(AID, attachmentFileGroupID);
                    base.WriteExecuteAudit("deleteAttachmentFileGroup Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteAttachmentFileGroup Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public AttachedFileGroup[] getAttachedFileGroupForDocument(int documentID)
        {

            return _bde.getAttachedFileGroupForDocument(documentID);
        }
        public AttachedFileItem[] getAttachedFileItems(int fileGroupID)
        {

            return _bde.getAttachedFileItems(fileGroupID);
        }
        public AttachedFileItem getAttachedFileItem(int attachedFileItemID)
        {

            return _bde.getAttachedFileItem(attachedFileItemID);
        }
        public AttachedFileItemImage getAttachedFileItemImageInfo(int attachedFileItemID, AttachmentPaperFace face)
        {

            return _bde.getAttachedFileItemImageInfo(attachedFileItemID, face);
        }
        public byte[] getAttachedFileItemImageData(int attachmentFileID, AttachmentPaperFace face)
        {

            return _bde.getAttachedFileItemImageData(attachmentFileID, face);
        }
        public byte[] getAttachedFileItemImageResized(int attachmentFileID, AttachmentPaperFace face, int width, int height)
        {

            return _bde.getAttachedFileItemImageResized(attachmentFileID, face, width, height);
        }
        public AttachmentType[] getAllFileAttachmentTypes()
        {
            return _bde.getAllFileAttachmentTypes();
        }
        internal AttachedFileGroup getAttachedFileGroup(int fileGroupID)
        {
            return _bde.getAttachedFileGroup(fileGroupID);
        }

    }// end classs
}//end name spaces