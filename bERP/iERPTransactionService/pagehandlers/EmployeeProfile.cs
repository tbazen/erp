﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using INTAPS.ClientServer.RemottingServer;
using INTAPS.Payroll.Service;
using INTAPS.ClientServer;
using RazorEngine;
using RazorEngine.Templating;
using INTAPS.Accounting;
using INTAPS.Payroll;
using INTAPS.Accounting.Service;
namespace BIZNET.iERP.Server.pagehandlers
{

    [HttpPageHandler(page = "employee_profile.html")]
    partial class EmployeeProfile : HttpPageHandlerBase, IHttpPageHandler
    {
        public override bool handleSessionGet(System.Net.HttpListenerContext context, object sessionObject, QueryDictionary queries)
        {
            iERPTransactionService berp = sessionObject as iERPTransactionService;
            PayrollService payroll = ApplicationServer.GetSessionObject(berp.sessionID, "PayrollService") as PayrollService;
            AccountingService accounting = ApplicationServer.GetSessionObject(berp.sessionID, "AccountingService") as AccountingService;
            int employeeId = int.Parse(queries["id"]);
            string strVID = queries["vid"];
            string strPeriodID= queries["periodID"];
            string panelId = queries["panelId"];

            object model = null;
            string template="";
            if (string.IsNullOrEmpty(panelId))
            {
                if (string.IsNullOrEmpty(strVID) && string.IsNullOrEmpty(strPeriodID)) //layout
                {
                    template = System.IO.File.ReadAllText(RazorWrapper.getTemplateFileFullPath("ierpservice\\employee_profile.cshtml"));
                    long[] versionIDs = payroll.getEmployeVersions(employeeId);
                    object[] versions = new object[versionIDs.Length];
                    for (int i = 0; i < versionIDs.Length; i++)
                        versions[i] = new
                        {
                            label = AccountBase.FormatDate(new DateTime(versionIDs[i])),
                            vid = versionIDs[i].ToString()
                        };
                    IEnumerable<object> reverseVersion = versions.Reverse().ToArray();
                    model = new
                    {
                        empID = employeeId,
                        sid = berp.sessionID,
                        versions = reverseVersion,
                        curPeriod = payroll.GetPayPeriod(DateTime.Now),

                    };
                }
                else
                    throw new ServerUserMessage("Invalid request");
            }

            
            switch (panelId)
            {
                case "account_panel"://Account Information
                    Employee emp = null;
                    template = System.IO.File.ReadAllText(RazorWrapper.getTemplateFileFullPath("ierpservice\\employee_profile_accinfo.cshtml"));
                    if (accounting != null)
                    {
                        if (payroll != null) emp = payroll.GetEmployee(employeeId);
                        if (emp != null)
                        {
                            if (queries["accountId"] != null)
                            {
                                int accountId = Convert.ToInt32(queries["accountId"]);
                                payroll.addEmployeeParentAccount(employeeId, accountId);
                            }
                            TaxCenter[] taxCenters = berp.GetTaxCenters();
                            TaxCenter taxCenter = taxCenters.FirstOrDefault(x => emp != null && x.ID == emp.TaxCenter);
                            var expenseAccount = accounting.GetAccount<Account>(emp.expenseParentAccountID);
                            var expenseParentAccountId = expenseAccount==null?"":expenseAccount.Name;
                            var costCenter = accounting.GetAccount<CostCenter>(emp.costCenterID).Name;
                            DataTable accountInformationViewModel = getAccountInformationViewModel(berp, payroll, accounting, employeeId);
                            model = new
                            {
                                empID = employeeId, emp.ticksFrom, taxCenter, expenseParentAccountID = expenseParentAccountId, costCenter,
                                sid = berp.sessionID,
                                table = accountInformationViewModel
                            };
                        }
                    }

                    break;

                case "workdata_panel":
                    int periodId = int.Parse(strPeriodID);
                    PayrollPeriod payrollPeriod = null;
                    if (payroll != null)
                    {
                        payrollPeriod = payroll.GetPayPeriod(periodId);
                    }
                    template = System.IO.File.ReadAllText(RazorWrapper.getTemplateFileFullPath("ierpservice\\employee_profile_workdata.cshtml"));
                    model = new
                    {
                        sid = berp.sessionID,
                        empID = employeeId,
                        period = payrollPeriod,
                        sections = getWorkDataViewModel(berp,payroll,employeeId,periodId),
                    };
                    break;
                case "bio_panel":
                    if (payroll == null)
                        break;
                    template = System.IO.File.ReadAllText(RazorWrapper.getTemplateFileFullPath("ierpservice\\employee_profile_bio.cshtml"));
                    emp = payroll.GetEmployee(employeeId,long.Parse(strVID));
                    #region Photo
                    if (queries.ContainsKey("img"))
                    {
                        if (accounting != null)
                        {
                            ImageItem[] image = accounting.GetImageItems(emp.picture);
                            if (image != null)
                            {
                                if (image.Length > 0)
                                {
                                    byte[] bytes = accounting.LoadImage(image[0].imageID, image[0].itemIndex, true, -1, -1).imageData;
                                    context.Response.ContentType = "image/png";
                                    context.Response.OutputStream.Write(bytes, 0, bytes.Length);
                                    context.Response.Close();
                                    return true;
                                }
                            }
                        }

                        context.Response.Redirect("/ierpservice/images/user-thumb.png");
                        context.Response.Close();
                        return true;
                    }
                    #endregion

                    string costSharingLable = "";
                    switch (emp.costSharingStatus)
                    {
                        case CostSharingStatus.EvidenceProvided:
                            costSharingLable = "Cost-sharing debt evidence provided";
                            break;
                        case CostSharingStatus.EvidenceNotProvided:
                            costSharingLable = "Cost-sharing debt evidence not provided";
                            break;
                    }

                    model = new
                    {
                        empID = employeeId,
                        sid = berp.sessionID,
                        version = AccountBase.FormatDate(new DateTime(emp.ticksFrom)).ToUpper(),
                        emp = new
                        {
                            emp.id,
                            emp.ticksFrom,
                            name = emp.employeeName,
                            idstr = emp.employeeID,
                            unit = payroll.GetOrgUnit(emp.orgUnitID),
                            emp.title,
                            tin = emp.TIN,
                            emp.telephone,
                            emp.address,
                            bankAccount = emp.BankAccountNo,
                            birthdate = AccountBase.FormatDate(emp.birthDate),
                            sex = emp.sex.ToString(),
                            salarytype = emp.salaryKind.ToString(),
                            grosssalary = emp.grossSalary.ToString("N"),
                            enrollmentdate = emp.enrollmentDate.ToString("d"),
                            employementystatus = emp.employmentType.ToString(),
                            transportallowance = emp.transportAllowance.ToString("N"),
                            costsharing = costSharingLable
                        }
                    };
                    
                    break;
            }

            string html = Engine.Razor.RunCompile(template, template.GetHashCode().ToString(), null, model);
            BDEWebServer.writeHtmlReponse(context.Response, html);
            return true;
        }
    }
}

