﻿using System;
using System.Collections.Generic;
using System.Data;
using INTAPS.Accounting;
using INTAPS.Accounting.Service;
using INTAPS.ClientServer.RemottingServer;
using INTAPS.Payroll.Service;

namespace BIZNET.iERP.Server.pagehandlers
{
    partial class EmployeeProfile : HttpPageHandlerBase, IHttpPageHandler
    {
        private DataTable accountInfoTable;
        AccountBase[] m_Accounts = null;
        DataTable getAccountInformationViewModel(iERPTransactionService berp, PayrollService payroll, AccountingService accounting, int employeeID)
        {
            accountInfoTable = new DataTable();
            m_Accounts = payroll.GetParentAcccountsByEmployee(employeeID);
            SortedDictionary<string, CostCenter> costCenters = new SortedDictionary<string, CostCenter>();
            Dictionary<string, double>[] balances = new Dictionary<string, double>[m_Accounts.Length];
            for (int rowIndex = 0; rowIndex < m_Accounts.Length; rowIndex++)
            {
                Dictionary<string, double> row = new Dictionary<string, double>();
                balances[rowIndex] = row;
                if (m_Accounts[rowIndex] == null)
                    continue;
                bool zeroRow = true;
                foreach (CostCenterAccount csa in accounting.GetCostCenterAccountsOf<Account>(m_Accounts[rowIndex].id))
                {

                    if (csa.isControlAccount)
                        continue;
                    double bal = accounting.GetNetBalanceAsOf(csa.id, TransactionItem.DEFAULT_CURRENCY, DateTime.Now);
                    zeroRow = false;
                    CostCenter cs = null;
                    foreach (KeyValuePair<string, CostCenter> kv in costCenters)
                    {
                        if (kv.Value.id == csa.costCenterID)
                        {
                            cs = kv.Value;
                            break;
                        }
                    }
                    if (cs == null)
                    {
                        cs = accounting.GetAccount<CostCenter>(csa.costCenterID);
                        costCenters.Add(cs.Code, cs);
                    }
                    row.Add(cs.Code, bal);
                }
                if (zeroRow)
                    m_Accounts[rowIndex] = null;
            }

            accountInfoTable.Columns.Add("Account ID", typeof(string));
            accountInfoTable.Columns.Add("Account Code", typeof(string));
            accountInfoTable.Columns.Add("Account Name", typeof(string));
            foreach (KeyValuePair<string, CostCenter> kv in costCenters)
            {
                accountInfoTable.Columns.Add(kv.Value.NameCode, typeof(double));
            }
            accountInfoTable.Columns.Add("Total", typeof(double));
            double[] colTotal = new double[costCenters.Count + 1];
            for (int i = 0; i < m_Accounts.Length; i++)
            {

                if (m_Accounts[i] == null)
                    continue;
                DataRow dataRow = accountInfoTable.NewRow();

                dataRow[0] = m_Accounts[i].id;
                dataRow[1] = m_Accounts[i].Code;
                dataRow[2] = m_Accounts[i].Name;
                int j = 3;
                Dictionary<string, double> balanceRow = balances[i];
                double total = 0;

                foreach (KeyValuePair<string, CostCenter> kv in costCenters)
                {
                    double bal = balanceRow.ContainsKey(kv.Value.Code) ? Math.Round(balanceRow[kv.Value.Code],2) : 0;
                    total += bal;
                    colTotal[j - 3] += bal;
                    dataRow[j] = bal;
                    j++;
                }
                dataRow[j] = total;
                colTotal[j - 3] += total;
                accountInfoTable.Rows.Add(dataRow);
            }
            {
                DataRow dataRow = accountInfoTable.NewRow();
                dataRow[0] = "";
                dataRow[1] = "Total";
                dataRow[2] = "";
                int j = 3;
                foreach (KeyValuePair<string, CostCenter> kv in costCenters)
                {
                    dataRow[j] = colTotal[j - 3];
                    j++;
                }
                dataRow[j] = colTotal[j - 3];
                accountInfoTable.Rows.Add(dataRow);
            }
            return accountInfoTable;

        }

    }
}
