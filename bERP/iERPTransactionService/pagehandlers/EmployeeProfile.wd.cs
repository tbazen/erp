﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.ClientServer.RemottingServer;
using INTAPS.Payroll.Service;
using INTAPS.ClientServer;
using RazorEngine;
using RazorEngine.Templating;
using INTAPS.Accounting;
using INTAPS.Payroll;
namespace BIZNET.iERP.Server.pagehandlers
{

    partial class EmployeeProfile : HttpPageHandlerBase, IHttpPageHandler
    {
        public Data_RegularTime GetRegularTimeData(iERPTransactionService berp,PayrollService payroll, int empID,int periodID, out int formulaID)
        {
            formulaID = -1;
            int pcdID = (int)berp.GetSystemParamters(new string[]{"RegularTimePayrollFormulaID"})[0];
            PayrollComponentFormula regularFormula = payroll.GetPCFormula(pcdID);
            if (regularFormula == null)
                return null;
            PayrollComponentData[] ret = payroll.GetPCData(pcdID, regularFormula.ID, AppliesToObjectType.Employee, empID, periodID, false);
            formulaID = regularFormula.ID;
            if (empID != -1)
            {
                if (ret.Length > 0)
                {
                    object regularTimeData = payroll.GetPCAdditionalData(ret[0].ID);
                    if (regularTimeData != null)
                    {
                        Data_RegularTime rt = (Data_RegularTime)regularTimeData;
                        if (rt.isZero())
                            return null;
                        return (Data_RegularTime)regularTimeData;
                    }
                }
            }
            return null;
        }
        private OverTimeData GetOverTimeData(iERPTransactionService berp, PayrollService payroll, int empID, int periodID, out int formulaID)
        {
            formulaID = -1;
            int pcdID = (int)berp.GetSystemParamters(new string[]{"OverTimePayrollFormulaID"})[0];

            PayrollComponentFormula overTimeFormula = payroll.GetPCFormula(pcdID);
            if (overTimeFormula == null)
                return null;

            PayrollComponentData[] ret = payroll.GetPCData(pcdID, overTimeFormula.ID, AppliesToObjectType.Employee, empID, periodID, false);
            formulaID = overTimeFormula.ID;
            if (empID != -1)
            {
                if (ret.Length > 0)
                {
                    object overTimeData = payroll.GetPCAdditionalData(ret[0].ID);
                    if (overTimeData != null)
                    {
                        if (((OverTimeData)overTimeData).isZero())
                            return null;
                        return (OverTimeData)overTimeData;
                    }

                }
            }
            return null;
        }
        private double[] GetGeneralData(iERPTransactionService berp, PayrollService payroll, int empID, int periodID, out string[] formulas, out int[] formulaID)
        {
            List<double> amounts = new List<double>();
            List<string> names = new List<string>();
            List<int> ids = new List<int>();
            int pcdID = (int)berp.GetSystemParamters(new string[]{"SingleValuePayrollComponentID"})[0];
            if (pcdID != 0)
            {
                PayrollComponentFormula[] singleValueFormula = payroll.GetPCFormulae(pcdID);
                if (singleValueFormula.Length == 0)
                {
                    formulas = null;
                    formulaID = null;
                    return null;
                }

                int i = 0;
                foreach (PayrollComponentFormula formula in singleValueFormula)
                {
                    if (empID != -1)
                    {
                        PayrollComponentData[] ret = payroll.GetPCData(pcdID, singleValueFormula[i].ID, AppliesToObjectType.Employee, empID, periodID, false);
                        if (ret.Length > 0)
                        {
                            object singleValueData = payroll.GetPCAdditionalData(ret[0].ID);
                            if (singleValueData != null)
                            {
                                SingleValueData data = (SingleValueData)singleValueData;
                                if (!AccountBase.AmountEqual(data.value, 0))
                                {
                                    amounts.Add(data.value);
                                    names.Add(formula.Name);
                                    ids.Add(formula.ID);
                                }
                            }
                        }
                    }
                    i++;
                }

            }
            formulas = names.ToArray();
            formulaID = ids.ToArray();
            return amounts.ToArray();
        }
        private double[] GetPermanentBenefitData(iERPTransactionService berp, PayrollService payroll, int empID, int periodID, out string[] formulas, out int[] formulaID, out string[] remark)
        {
            List<double> amounts = new List<double>();
            List<string> names = new List<string>();
            List<int> ids = new List<int>();
            List<string> _remark = new List<string>();
            int pcdID = (int)berp.GetSystemParamters(new string[]{"permanentBenefitComponentID"})[0];
            if (pcdID != 0)
            {
                PayrollComponentFormula[] permanentBenefitFormula = payroll.GetPCFormulae(pcdID);
                if (permanentBenefitFormula.Length == 0)
                {
                    formulas = null;
                    formulaID = null;
                    remark = null;
                    return null;
                }

                int i = 0;
                foreach (PayrollComponentFormula formula in permanentBenefitFormula)
                {
                    if (empID != -1)
                    {
                        PayrollComponentData[] ret = payroll.GetPCData(pcdID, permanentBenefitFormula[i].ID, AppliesToObjectType.Employee, empID, periodID, false);
                        if (ret.Length > 0)
                        {
                            object singleValueData = payroll.GetPCAdditionalData(ret[0].ID);
                            if (singleValueData != null)
                            {
                                SingleValueData data = (SingleValueData)singleValueData;
                                if (!AccountBase.AmountEqual(data.value, 0))
                                {
                                    amounts.Add(data.value);
                                    names.Add(formula.Name);
                                    ids.Add(formula.ID);
                                    PayPeriodRange range = ret[0].periodRange;
                                    if (range.FiniteRange)
                                        _remark.Add(payroll.GetPayPeriod(ret[0].periodRange.PeriodFrom).name + " to " + payroll.GetPayPeriod(ret[0].periodRange.PeriodTo).name);
                                    else
                                        _remark.Add("Since " + payroll.GetPayPeriod(ret[0].periodRange.PeriodFrom).name);
                                }
                            }
                        }
                    }
                    i++;
                }

            }
            formulas = names.ToArray();
            formulaID = ids.ToArray();
            remark = _remark.ToArray();
            return amounts.ToArray();
        }

        IEnumerable<object> getWorkDataViewModel(iERPTransactionService berp, PayrollService payroll, int employeeID, int periodID)
        {

            int regularFormulaID;
            List<object> ret = new List<object>();
            
            Data_RegularTime regularTimeData =GetRegularTimeData(berp,payroll,employeeID,periodID, out regularFormulaID);
            if (regularTimeData != null)
            {
                object[] regTime
                    = new object[] 
                        {
                            new {label="Working Days",value=AccountBase.FormatAmount((double)regularTimeData.WorkingDays),remark=""},
                            new {label="Working Hours",value=AccountBase.FormatAmount((double)regularTimeData.WorkingHours),remark=""},
                            new {label="Working Partial Working Hours",value=AccountBase.FormatAmount((double)regularTimeData.PartialWorkingHour),remark=""}
                        };

                ret.Add(new {title="Regular Time",data=regTime});
            }

            int overTimeFormulaID;
            OverTimeData overTimeData = GetOverTimeData(berp, payroll, employeeID, periodID, out overTimeFormulaID);
            if (overTimeData != null)
            {
                object overTime = new object[]
                {
                    new {label="Off Hours",value=((double)overTimeData.offHours).ToString("0.0"),remark=""},
                    new {label="Late Hours",value=((double)overTimeData.lateHours).ToString("0.0"),remark=""},
                    new {label="Weekend Hours",value=((double)overTimeData.weekendHours).ToString("0.0"),remark=""},
                    new {label="Holiday Hours",value=((double)overTimeData.holidayHours).ToString("0.0"),remark=""},
                };
                ret.Add(new { title = "Over Time", data = overTime });
            }
            
            string[] formulas;
            int[] formulaID;
            double[] values = GetGeneralData(berp,payroll, employeeID, periodID, out formulas, out formulaID);
            if (values != null)
            {
                int j = 0;
                List<object> others = new List<object>();
                foreach (string name in formulas)
                {
                    others.Add(new { label = name, value = ((double)values[j]).ToString("0.0"), remark = "" });
                    j++;
                }
                if(others.Count>0)
                    ret.Add(new { title = "Other Deductions/Benfits", data = others});
            }
            string[] remark;
            values = GetPermanentBenefitData(berp, payroll, employeeID,periodID, out formulas, out formulaID, out remark);
            if (values != null)
            {
                List<object> permanent = new List<object>();
                int j = 0;
                foreach (string name in formulas)
                {
                    permanent.Add(new { label = name, value = ((double)values[j]).ToString("0.0"), remark = remark[j] });
                    j++;
                }
                if(permanent.Count>0)
                    ret.Add(new { title = "Permanent Deductions/Benfits", data = permanent});
            }
            return ret;
        }
    }


    
}

