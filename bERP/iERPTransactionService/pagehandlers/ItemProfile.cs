﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.ClientServer.RemottingServer;

namespace BIZNET.iERP.Server.pagehandlers
{

    [HttpPageHandler(page="item_profile.cshtml")]
    class ItemProfile:HttpPageHandlerBase,IHttpPageHandler
    {
        public override bool handleSessionGet(System.Net.HttpListenerContext context, object sessionObject, QueryDictionary queries)
        {
            iERPTransactionService service = sessionObject as iERPTransactionService;
            return base.handleSessionGet(context, sessionObject, queries);
        }
    }
}
