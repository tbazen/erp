﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIZNET.iERP.Server;
using INTAPS.ClientServer;
using INTAPS.ClientServer.RemottingServer;

namespace BIZNET.iERPMan.Server.pagehandlers
{
    [HttpPageHandler(page = "login.cshtml")]
    public class LoginPage :HttpPageHandlerBase,  IHttpPageHandler
    {
        public override bool handleSessionGet(System.Net.HttpListenerContext context, object sessionObject, QueryDictionary queries)
        {
            string sid = queries["sid"];
            if (!string.IsNullOrEmpty(sid))
                try
                {
                    ApplicationServer.CloseSession(sid);
                }
                catch
                {

                }

            return this.handleStaticGet(context, queries);
        }
        public override bool handleStaticGet(System.Net.HttpListenerContext context, QueryDictionary queries)
        {
           
            StringBuilder sb = new StringBuilder();
            INTAPS.ClientServer.RazorWrapper.executeTemplateFile(sb, "ierpmanservice\\login.html", null);
            INTAPS.ClientServer.RemottingServer.BDEWebServer.writeHtmlReponse(context.Response, sb.ToString());
            return true;
        }
        public override void handlePostForm(object sessionObject,System.Net.HttpListenerContext context, FormDataDictionary formData)
        {
            try
            {
                string userName = formData ["username"].textValue;
                string password = formData["password"].textValue;
                string sessionID = ApplicationServer.CreateUserSession(userName, password, "web");
                ApplicationServer.GetSessionObject(sessionID, ApplicationServer.getSessionShortNameByType(typeof(iERPTransactionService)));
                context.Response.Redirect("mainpage.cshtml?sid=" + sessionID);
                context.Response.Close();
            }
            catch(Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                INTAPS.ClientServer.RazorWrapper.executeTemplateFile(sb, "ierpmanservice\\login.html", new {error=ex.Message});
                INTAPS.ClientServer.RemottingServer.BDEWebServer.writeHtmlReponse(context.Response, sb.ToString());
            }
        }
    }
}
