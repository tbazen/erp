﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using INTAPS.Accounting;
using INTAPS.Accounting.Service;
using INTAPS.ClientServer;
using INTAPS.ClientServer.RemottingServer;
using INTAPS.Payroll;
using INTAPS.Payroll.Service;
using RazorEngine;
using RazorEngine.Templating;

namespace BIZNET.iERP.Server.pagehandlers
{
    [HttpPageHandler(page = "trade_profile.html")]
    partial class TradeRelationProfile : HttpPageHandlerBase, IHttpPageHandler
    {
        public override bool handleSessionGet(System.Net.HttpListenerContext context, object sessionObject, QueryDictionary queries)
        {
            iERPTransactionService berp = sessionObject as iERPTransactionService;
            string code = queries["code"];
            string panelId = queries["panelId"];
            object model = null;
            string template = "";
            if (berp != null)
            {
                TradeRelation tradeRelation = berp.GetRelation(code);
                if (string.IsNullOrEmpty(panelId))
                {
                    if (tradeRelation != null)
                    {
                        template = System.IO.File.ReadAllText(RazorWrapper.getTemplateFileFullPath("ierpservice\\trade_profile.cshtml"));
                        model = new
                        {
                            sid = berp.sessionID,
                            relation = new
                            {
                                code = tradeRelation.Code,
                                isinternational =tradeRelation.IsInternational
                            }
                        };
                    }
                
                }
                switch (panelId)
                {
                    case "account_panel"://Account Information
                        if (tradeRelation != null)
                        {
                            template = System.IO.File.ReadAllText(RazorWrapper.getTemplateFileFullPath("ierpservice\\trade_profile_accinfo.cshtml"));
                            AccountingService accounting = ApplicationServer.GetSessionObject(berp.sessionID, "AccountingService") as AccountingService;
                            DataTable accountInformationViewModel = getAccountInformationViewModel(berp, accounting, tradeRelation.Code);
                            model = new
                            {
                                code = tradeRelation.Code,
                                sid = berp.sessionID,
                                table = accountInformationViewModel
                            };
                        }
                        break;
                    case "relationPanel":
                        if (tradeRelation == null)
                            break;
                        template = System.IO.File.ReadAllText(RazorWrapper.getTemplateFileFullPath("ierpservice\\trade_profile_basic.cshtml"));
                        #region Photo
                        if (queries.ContainsKey("img"))
                        {
                            if (tradeRelation.Logo.Length > 0)
                            {
                                byte[] bytes = tradeRelation.Logo;
                                context.Response.ContentType = "image/png";
                                context.Response.OutputStream.Write(bytes, 0, bytes.Length);
                                context.Response.Close();
                                return true;
                            }
                            context.Response.Redirect("/ierpservice/images/user-thumb.png");
                            context.Response.Close();
                            return true;
                        }
                        #endregion

                        model = new
                        {
                            sid = berp.sessionID,
                            relation = new
                            {
                                code = tradeRelation.Code,
                                name = tradeRelation.NameCode,
                                tin = tradeRelation.TIN,
                                vatno = tradeRelation.VATRegNumber,
                                tradetype = tradeRelation.TradeType,
                                taxregistrationtype = tradeRelation.TaxRegistrationTypeName,
                                phoneno = tradeRelation.Telephone,
                                isinternational = tradeRelation.IsInternational,
                                isinternationalvalue = tradeRelation.IsInternationalValue,
                                withhold = tradeRelation.Withhold,
                                iswithholdingagent = tradeRelation.IsWithholdingAgent,
                                isvatagent = tradeRelation.IsVatAgent

                            }
                        };

                        break;
                }
            }

            string html = Engine.Razor.RunCompile(template, template.GetHashCode().ToString(), null, model);
            BDEWebServer.writeHtmlReponse(context.Response, html);
            return true;
        }
    }
}