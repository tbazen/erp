using System;
using System.Collections.Generic;
using System.Data;
using INTAPS.Accounting;
using INTAPS.Accounting.Service;
using INTAPS.ClientServer.RemottingServer;
using INTAPS.Payroll.Service;

namespace BIZNET.iERP.Server.pagehandlers
{
    partial class TradeRelationProfile : HttpPageHandlerBase, IHttpPageHandler
    {
        private DataTable accountInfoTable;
        AccountBase[] m_Accounts = null;
        DataTable getAccountInformationViewModel(iERPTransactionService berp,  AccountingService accounting, string code)
        {
            accountInfoTable = new DataTable();
            TradeRelation tradeRelation = berp.GetRelation(code);
            object[] paramters = berp.GetSystemParamters(new[] { "mainCostCenterID" });
            int costcenterid = Convert.ToInt32(paramters[0]);
            accountInfoTable.Columns.Add("Account ID", typeof(string));
            accountInfoTable.Columns.Add("Account Code", typeof(string));
            accountInfoTable.Columns.Add("Account Type", typeof(string));
            accountInfoTable.Columns.Add("Balance", typeof(double));
            accountInfoTable.Columns.Add("Status", typeof(double));

            foreach (System.Reflection.FieldInfo fi in typeof(TradeRelation).GetFields())
            {
                object[] atr = fi.GetCustomAttributes(typeof(AIChildOfAttribute), true);
                if (atr == null || atr.Length == 0)
                    continue;
                AIChildOfAttribute a = (AIChildOfAttribute)atr[0];
                AccountBase account = a.isCostCenter 
                    ? (AccountBase)accounting.GetAccount<CostCenter>((int)fi.GetValue(tradeRelation)) 
                    : accounting.GetAccount<Account>((int)fi.GetValue(tradeRelation));
                if (account == null)
                    continue;
                DataRow accountRow = accountInfoTable.NewRow();
                accountRow[0] = account.id;
                accountRow[1] = account.Code;
                accountRow[2] = a.displayName;
                if (a.isCostCenter)
                    accountRow[3] = "0";
                else
                    accountRow[3] = accounting.GetNetCostCenterAccountBalanceAsOf(costcenterid, account.id, TransactionItem.DEFAULT_CURRENCY, DateTime.Now).ToString("N");
                
                accountRow[4] = account.Status == AccountStatus.Activated;
                accountInfoTable.Rows.Add(accountRow);
                
            }
            return accountInfoTable;

        }
    }
}