using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP
{
    public class AccountDocumentAttribute:Attribute 
    {
        public string documentName;
        public AccountDocumentAttribute(string documentName)
        {
            this.documentName = documentName;
        }
    }
}
