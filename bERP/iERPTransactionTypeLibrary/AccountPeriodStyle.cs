﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP
{
    public enum AccountPeriodStyle
    {
        EthiopianFiscalYearEthiopianMonth=1,
        EthiopianFiscalYearGergorianMonth=2,
        GregJul1FiscalYearGregorianMonth=3,
    }
}
