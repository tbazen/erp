﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP
{
    public interface IAccountingItem
    {
        string coaName { get; }
        string coaCode { get; }
    }
    public class AIChildOfAttribute:Attribute
    {
        public string[] categoryField;
        public string displayName;
        public bool isCostCenter;
        public AIChildOfAttribute(string categoryField,string dn,bool isCostCenter)
        {
            this.categoryField = new string[]{ categoryField};
            this.displayName = dn;
            this.isCostCenter = isCostCenter;
        }
        public AIChildOfAttribute(string[] categoryField, string dn, bool isCostCenter)
        {
            this.categoryField = categoryField;
            this.displayName = dn;
            this.isCostCenter = isCostCenter;
        }
    }
    public class AIParentAccountFieldAttribute : Attribute,IComparable<AIParentAccountFieldAttribute>
    {
        public bool isCostCenter = false;
        public bool createChildAccount = true;
        public string[] groups;
        public string name;
        public string displayOrder = "";
        public bool isSummary = false;
        public AIParentAccountFieldAttribute(bool isCostCenter,params string[] groups)
            : this(isCostCenter, true,groups)
        {
        }
        public AIParentAccountFieldAttribute(bool isCostCenter,bool createChildAccount,params string[] groups)
        {
            this.isCostCenter = isCostCenter;
            this.createChildAccount = createChildAccount;
            this.groups = groups;
        }
        public AIParentAccountFieldAttribute(params string[] groups)
            : this(false,true,groups)
        {
        }


        public int CompareTo(AIParentAccountFieldAttribute other)
        {
            return this.displayOrder.CompareTo(other.displayOrder);
        }
    }
}
