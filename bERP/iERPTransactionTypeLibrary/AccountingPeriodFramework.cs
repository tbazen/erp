using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    public abstract class PeriodSeries
    {
        public abstract string Name { get;}
        public abstract AccountingPeriod GetPeriodOfn(int []parentPeriod, int n);
        public abstract int GetMin(int []parentPeriod);
        public abstract int GetMax(int []parentPeriod);
        public abstract string FormatPeriod(string formatString,int []parentPeriod, int n);
        public abstract void GetPeriodForDate(DateTime date, out int []Parent, out int n);
        public virtual AccountingPeriod GetPeriodForDate(DateTime date)
        {
            int []pp;
            int n;
            GetPeriodForDate(date, out pp, out n);
            return GetPeriodOfn(pp, n);
        }
    }

    public class EthiopianYearPeriodSeries:PeriodSeries
    {
        public override AccountingPeriod GetPeriodOfn(int []parentPeriod, int n)
        {
            AccountingPeriod p = new AccountingPeriod();
            p.fromDate=INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, 1, n)).GridDate;
            p.toDate= INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, 1, n+1)).GridDate;
            p.name = n + "(EC)";
            return p;
        }

        public override int GetMin(int []parentPeriod)
        {
            return 1900;
        }

        public override int GetMax(int []parentPeriod)
        {
            return 2100;
        }

        public override string FormatPeriod(string formatString, int []parentPeriod, int n)
        {
            return n.ToString();
        }

        public override void GetPeriodForDate(DateTime date, out int []Parent, out int n)
        {
            Parent = new int[0];
            n = INTAPS.Ethiopic.EtGrDate.ToEth(date).Year;
        }

        public override string Name
        {
            get { return "Ethiopian Years"; }
        }
    }

    public class EthiopianFiscalYearPeriodSeries : PeriodSeries
    {
        
        public override AccountingPeriod GetPeriodOfn(int []parentPeriod, int n)
        {
            AccountingPeriod p = new AccountingPeriod();
            p.fromDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, 11, n-1)).GridDate;
            p.toDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, 11, n)).GridDate;
            p.name = n + "(EC)";
            return p;
        }

        public override int GetMin(int []parentPeriod)
        {
            return 1900;
        }

        public override int GetMax(int []parentPeriod)
        {
            return 2100;
        }

        public override string FormatPeriod(string formatString, int []parentPeriod, int n)
        {
            return n.ToString() + " (E.C. Fiscal)";            
        }

        public override void GetPeriodForDate(DateTime date, out int []Parent, out int n)
        {
            Parent = null;
            INTAPS.Ethiopic.EtGrDate grDate = INTAPS.Ethiopic.EtGrDate.ToEth(date);
            if (grDate.Month < 11)
                n = grDate.Year;
            else
                n = grDate.Year + 1;
        }
        public override string Name
        {
            get { return "Ethiopian Fiscal Years"; }
        }
    }
    
    public class EthiopianMonthPeriodSeries:PeriodSeries
    {
        public override AccountingPeriod GetPeriodOfn(int []parentPeriod, int n)
        {
            AccountingPeriod p = new AccountingPeriod();
            p.fromDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, n, parentPeriod[0])).GridDate;
            p.toDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, n==12?1:n+1, n==12?parentPeriod[0]+1:parentPeriod[0])).GridDate;
            p.name = INTAPS.Ethiopic.EtGrDate.GetEtMonthNameEng(n)+" "+parentPeriod[0];
            return p;
        }
        public override int GetMin(int []parentPeriod)
        {
            return 1;
        }
        public override int GetMax(int []parentPeriod)
        {
            return 12;
        }
        public override string FormatPeriod(string formatString, int []parentPeriod, int n)
        {
            if(string.IsNullOrEmpty(formatString))
                formatString="mmm yyyy (E.C.)";
            return formatString.Replace("mmm",INTAPS.Ethiopic.EtGrDate.GetEtMonthNameEng(n))
            .Replace("mm",n.ToString("00"))
            .Replace("yyyy",parentPeriod[0].ToString())
            .Replace("yy",parentPeriod[0].ToString().Substring(2));
        }
        public override void GetPeriodForDate(DateTime date, out int []Parent, out int n)
        {
            INTAPS.Ethiopic.EtGrDate etg = new INTAPS.Ethiopic.EtGrDate(date);
            Parent = new int[] { etg.Year };
            n = etg.Month;
        }
        public override string Name
        {
            get { return "Ethiopian Months"; }
        }
    }
    
    public class EthiopianFiscalMonthPeriodSeries : PeriodSeries
    {
        int FiscalMonthToMonth(int month)
        {
            return month>2?month-2:month+10;
        }
        int FiscalYeartToYear(int year, int month)
        {
            return month < 3 ? year - 1 : year;
        }
        public override AccountingPeriod GetPeriodOfn(int []parentPeriod, int n)
        {
            AccountingPeriod p = new AccountingPeriod();
            int month1 = n;
            int month2 = n == 12 ? 1 : n + 1;
            int year1 = parentPeriod[0];
            int year2 = n == 12 ? year1 + 1 : year1;
            p.fromDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, FiscalMonthToMonth(month1), FiscalYeartToYear(year1,month1))).GridDate;
            p.toDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, FiscalMonthToMonth(month2), FiscalYeartToYear(year2, month2))).GridDate;
            p.name = INTAPS.Ethiopic.EtGrDate.GetEtMonthNameEng(FiscalMonthToMonth(month1)) + " " + parentPeriod[0]+"(Fiscal)";
            return p;
        }

        public override int GetMin(int []parentPeriod)
        {
            return 1;
        }

        public override int GetMax(int []parentPeriod)
        {
            return 12;
        }

        public override string FormatPeriod(string formatString, int []parentPeriod, int n)
        {
            if (string.IsNullOrEmpty(formatString))
                formatString = "mmm yyyy (E.C. Fiscal)";
            return formatString.Replace("mmm", INTAPS.Ethiopic.EtGrDate.GetEtMonthNameEng(n))
            .Replace("mm", n.ToString("00"))
            .Replace("yyyy", parentPeriod[0].ToString())
            .Replace("yy", parentPeriod[0].ToString().Substring(2));
        }

        public override void GetPeriodForDate(DateTime date, out int []Parent, out int n)
        {
            INTAPS.Ethiopic.EtGrDate etg = INTAPS.Ethiopic.EtGrDate.ToEth( new INTAPS.Ethiopic.EtGrDate(date));
            if (etg.Month < 11)
            {
                n = etg.Month+2;
                Parent = new int[] { etg.Year };
            }
            else
            {
                n = (etg.Month + 12 - 10) % 12;
                Parent = new int[] { etg.Year + 1 };
            }
                
        }
        public override string Name
        {
            get { return "Ethiopian Fiscal Year Months"; }
        }
    }    
}