﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP
{
    public enum TimeRangeFormat
    {
        Time,Day,Month,Year,EtDay,EtMonth,EtYear,DayRange
    }
    public class TimeRange
    {
        public TimeRangeFormat format=TimeRangeFormat.Time; 
        public DateTime time1;
        public DateTime time2;
    }
    public enum ActityStatus
    {
        New,Done,Closed
    }
    public class ActivtyAssignment
    {
        public int entityID;
        public TimeRangeFormat[] time;
    }
    public class bERPActivity
    {
        public int id;
        public int pid;
        public int priorityNumber;
        public DateTime creationDate;
        public string shortName;
        public string htmlText;
        public string text;
        public DateTime assignDate;
        public int assignedEntity;
        public DateTime scheduleStart;
        public DateTime scheduleEnd;
        public ActityStatus status;
        public DateTime statusDate;
        public int aid;
    }
    [Flags]
    public enum ActivityUpdateType
    {
        Delete=1,
        DataCorrection=2,
        Reschedule=4,
        Reassignment=8,
        Comment=16,
        Attachment=32
    }
    public class bERPActivityAttachment
    {
        public int activityID;
        public int aid;
        public int docNumber;
        public byte[] attachment;
        public string attachmentType;
    }
    public class bERPActivityUpdate
    {
        public int id;
        public int pid;
        public int priorityNumber;
        public DateTime creationDate;
        public string shortName;
        public string htmlText;
        public string text;
        public DateTime assignDate;
        public int assignedEntity;
        public DateTime scheduleStart;
        public DateTime scheduleEnd;
        public ActityStatus status;
        public DateTime statusDate;
        public int aid;
        public int uaid;
        public ActivityUpdateType updateType;
        public string comment;

        public void copy(bERPActivity activity)
        {
            Type updateType = typeof(bERPActivityUpdate);
            foreach (System.Reflection.FieldInfo fi in typeof(bERPActivity).GetFields())
            {
                updateType.GetField(fi.Name).SetValue(this, fi.GetValue(activity));
            }
        }
    }
    public class ActivityEntityInfo
    {
        public int id;
        public string name;
    }
    public interface IbERPAtivityManagerService
    {
    }
}
