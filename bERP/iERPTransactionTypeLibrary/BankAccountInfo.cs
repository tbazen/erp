﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;

namespace BIZNET.iERP
{
    [SingleTableObject]
    [Serializable]
    public class BankAccountInfo
    {
        [IDField]
        public int mainCsAccount;
        [DataField]
        public int bankServiceChargeCsAccountID;
        [DataField]
        public string bankName;
        [DataField]
        public string branchName;
        [DataField]
        public string bankAccountNumber;
        [DataField]
        public string bankAccountName;
        public override string ToString()
        {
            return BankBranchAccount;
        }
        public string BankBranchAccount
        {
            get
            {
                return bankName + " " + branchName + " - " + bankAccountNumber;
            }
        }
    }
}
