﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    [Serializable]
    [INTAPS.RDBMS.SingleTableObject]
    public class BudgetEntry
    {
        [INTAPS.RDBMS.IDField]
        public int budgetID;
        [INTAPS.RDBMS.IDField]
        public int accountID;
        [INTAPS.RDBMS.DataField]
        public double amount = 0;
    }

    public enum BudgetOperation
    {
        New,
        Adjustment,
        Transfer
    }

    [Serializable]
    [INTAPS.RDBMS.SingleTableObject(orderBy = "costCenterID,ticksFrom desc")]
    public class Budget
    {
        [INTAPS.RDBMS.IDField]
        public int id;
        [INTAPS.RDBMS.DataField]
        public int costCenterID;
        
        [INTAPS.RDBMS.DataField]
        public long ticksFrom;
        [INTAPS.RDBMS.DataField]
        public long ticksTo;
        
        public double totalBudget;

        
    }
    [Serializable]
    public class BudgetFilter
    {
        public int costCenterID;
        public DateTime targetDate;

        public BudgetFilter(int csID, DateTime t)
        {
            this.costCenterID = csID;
            this.targetDate = t;
        }
    }
}
