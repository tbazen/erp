﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;
using System.ComponentModel;

namespace BIZNET.iERP
{
    [Serializable, SingleTableObject]
    public class CashAccountCategory
    {
        [IDField]
        public int id=-1;
        [DataField]
        public int categoryID = -1;
        [DataField]
        public string name;
        [DataField]
        public int accountID;

    }
    [Serializable]
    [SingleTableObject]
    public class CashAccount
    {
        [DataField]
        public int csAccountID;
        [DataField]
        public string name;
        [IDField]
        public string code;
        [DataField]
        public int employeeID=-1;
        [DataField]
        public bool pettyCash = false;
        [DataField]
        public int categoryID = -1;
        public bool isStaffExpenseAdvance = false;

        public string nameCode { get { return name + "-" + code; } }
        public override string ToString()
        {
            return nameCode;
        }
    }
}
