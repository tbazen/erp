using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using System.Web;
using INTAPS.Payroll;
namespace BIZNET.iERP
{
    public enum CashFlowDirection
    {
        Source,
        Sink
    }
    public enum CashFlowItemType
    {
        Cash,
        IncomeExpense
    }
    [Serializable]
    public class CashFlowItem
    {
        public BizNetPaymentMethod paymentMethod=BizNetPaymentMethod.None;
        public string instrumentReference=null;
        public int cashAccountID=-1;
        public int purposeCostCenterAccountID=-1;
        public CashFlowItemType type=CashFlowItemType.Cash;
        public string html;
        public double amount;

        public static CashFlowItem createByText(string text, double amount)
        {
            return new CashFlowItem(HttpUtility.HtmlEncode(text), amount);
        }
        public static CashFlowItem createByText(int cashAccountID,int purposeCostCenterAccountID,string text, double amount)
        {
            CashFlowItem ret= new CashFlowItem(HttpUtility.HtmlEncode(text), amount);
            ret.cashAccountID = cashAccountID;
            ret.purposeCostCenterAccountID = purposeCostCenterAccountID;
            return ret;
        }
        public CashFlowItem(int cashAccountID, int purposeCostCenterAccountID, string html, double amount)
        {
            this.html = html;
            this.amount = amount;
            this.cashAccountID = cashAccountID;
            this.purposeCostCenterAccountID = purposeCostCenterAccountID;
        }
        public CashFlowItem(string html, double amount)
        {
            this.html = html;
            this.amount = amount;
        }
    }
}
