using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP
{
    public enum BusinessEntity
    {
        SoleProprietorship = 0,
        Partnership = 1,
        GeneralPartnership = 2,
        LimitedPartnership = 3,
        ShareCompany = 4,
        PrivateLimitedCompany = 5,
        JointVenture = 6,
        GovernmentalOrganization=7
    }
    public enum BusinessCategory
    {
        NotListed = 0,
        Importer = 1,
        Exporter = 2
    }
    public enum TaxRegisrationType
    {
        VAT = 0,
        TOT = 1,
        NonTaxPayer = 2
    }
    [Serializable]
    [INTAPS.RDBMS.LoadFromSetting]
    public class CompanyProfile
    {
        public string Name;
        public string AmharicName;
        public string Region;
        public string Zone;
        public string Woreda;
        public string Kebele;
        public string HouseNumber;
        public string Telephone;
        public string MobilePhone;
        public string Fax;
        //public string TaxCenter;
        public int WithholdingTaxCenter;
        public int VATTaxCenter;
        public int PensionTaxCenter;
        public string WebSite;
        public string Email;
        public BusinessEntity BusinessEntity;
        public BusinessCategory BusinessCategory;
        public string Occupation;
        public string TIN;
        public TaxRegisrationType TaxRegistrationType;
        public DateTime TaxRegistrationDate;
        public string VATNumber;
        public string MachineNumber;
        public byte[] Logo;
        public bool isVATAgent = false;
        public bool isWitholdingAgent
        {
            get
            {
                return BusinessEntity == BIZNET.iERP.BusinessEntity.PrivateLimitedCompany
                    || BusinessEntity == BIZNET.iERP.BusinessEntity.ShareCompany
                    || BusinessEntity == BIZNET.iERP.BusinessEntity.GovernmentalOrganization;
            }
        }
    }
}
