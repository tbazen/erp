﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.RDBMS;

namespace BIZNET.iERP
{
    public class ImageIndex : IComparable
    {
        public int DocID;
        public int ImageID;
        public uint MaxDocOrder;
        public uint order;

        public ImageIndex(uint o, int did, int iid)
        {
            this.order = o;
            this.DocID = did;
            this.MaxDocOrder = 0;
            this.ImageID = iid;
        }

        public int CompareTo(object obj)
        {
            ImageIndex index = obj as ImageIndex;
            if (index == null)
            {
                return -1;
            }
            int num = this.MaxDocOrder.CompareTo(index.MaxDocOrder);
            if (num != 0)
            {
                return num;
            }
            return index.order.CompareTo(this.order);
        }
    }
    public enum AttachmentPaperFace
    {
        Digital,
        Paper_Front,
        Paper_Back
    }
    [Serializable]
    [SingleTableObject(orderBy="ordern")]
    public class AttachedFileItem
    {
        [IDField]
        public int id;
        [DataField]
        public int attachmentGroupID;
        [DataField]
        public int ordern;
        public AttachedFileItemImage[] images;
    }
    [Serializable]
    [SingleTableObject(orderBy = "fileItemID,face")]
    public class AttachedFileItemImage
    {
        [IDField]
        public int fileItemID;
        [IDField]
        public AttachmentPaperFace face = AttachmentPaperFace.Digital;
        [DataField]
        public string fileName;
        public byte[] fileImage;
    }
    [Serializable]
    [SingleTableObject(orderBy="id")]
    public class AttachmentType
    {
        [IDField]
        public int id;
        [DataField]
        public string name;
        public override string ToString()
        {
            return name;
        }
    }
    [Serializable]
    [SingleTableObject(orderBy = "ordern")]
    public class AttachedFileGroup
    {
        [IDField]
        public int id;
        [DataField]
        public int parentID=-1;
        [DataField]
        public int attachmentTypeID;
        [DataField]
        public string locationID;
        [DataField]
        public string locationText;
        [DataField]
        public string paperRef;
        [DataField]
        public long documentTicks;
        [DataField]
        public string title;
        [DataField]
        public int ordern;
        public DateTime documentDate
        {
            get
            {
                return new DateTime(documentTicks);
            }
            set
            {
                this.documentTicks = value.Ticks;
            }
        }

    }
    [Serializable]
    [SingleTableObject]
    public class AttachedFileGroupDocument
    {
        [IDField]
        public int documentID;
        [IDField]
        public int attachedFileGroupID;
        [DataField]
        public int ordern;
    }
}
