using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;

namespace BIZNET.iERP
{
    public enum DocumentReferenceType
    {
        PaymentVoucher=1,
        Check=2,
        Receipt=3
    }

    [Serializable]
    [SingleTableObject]
    public class bERPDocumentReference
    {
        [IDField]
        public string refType;
        [IDField]
        public string refNumber;
        [IDField]
        public int documentID;
        public bERPDocumentReference()
        {
        }
        public bERPDocumentReference(string type, string no, int docID)
        {
            this.refType = type;
            this.refNumber = no;
            this.documentID = docID;
        }
    }
}
