using System;
using System.Collections.Generic;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    [Serializable]
    public class BookClosingDocument : AccountDocument
    {
        [DocumentTypedReferenceField("JV")]
        public DocumentTypedReference voucher;
        public int closingAccount;
    }
    [Serializable]
    public class AdjustmentDocument : AccountDocument
    {
        [DocumentTypedReferenceField("JV", "CHKPV", "CPV", "PCPV", "PV", "CH", "CRV", "FSN", "CSIV", "CRSIV", "GEN", "CSINV2", "CRSINV2")]
        public DocumentTypedReference voucher;
        public bool openingBalances = false;
        public AdjustmentAccount[] adjustmentAccounts;
    }
    [Serializable]
    public class AdjustmentAccount
    {
        public string accountCode;
        public int accountID=-1;
        public int costCenterID=-1;
        public double debitAmount;
        public double creditAmount;
        public string description;
        public string employeeName;
        public string employeeTIN;
        public TradeRelation relation=null;
        public GoodOrService goodOrService=GoodOrService.Unknown;
        public string receiptNo = null;
    }
}
