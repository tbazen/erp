using System;
using System.Collections.Generic;
using INTAPS.Accounting;
using System.Xml.Serialization;
namespace BIZNET.iERP
{
    [Serializable]
    [XmlInclude(typeof(Property_General))]
    [XmlInclude(typeof(Property_Vehicle))]
    public class AssignToCustodianDocument : AccountDocument
    {
        [Serializable]
        public class AssignmentItem
        {
            public int existingPropertyID;
            public int newPropertyCostCenterID;
            public object newPropertyData;
            public Property newProperty;
        }
        public int custodianID = -1;
        public AssignmentItem[] items = new AssignmentItem[0];

    }
    [Serializable]
    [XmlInclude(typeof(Property_General))]
    [XmlInclude(typeof(Property_Vehicle))]
    public class ConvertFixedAssetToPropertyDocument : AccountDocument
    {
        [Serializable]
        public class ConvertFixedAssetToPropertyItem
        {
            public int costCenterID = -1;
            public Property property;
            public object propertyData;
        }
        public ConvertFixedAssetToPropertyItem[] items=new ConvertFixedAssetToPropertyItem[0];
    }
}
