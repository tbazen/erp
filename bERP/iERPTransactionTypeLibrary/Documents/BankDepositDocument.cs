using System;
using System.Collections.Generic;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    [Serializable]
    public class BankDepositDocument : AccountDocument,IStandardAttributeDocument
    {
        [DocumentTypedReferenceFieldAttribute("CRV", "JV", "CHKPV", "CPV", "PCPV", "PV")]
        public DocumentTypedReference voucher = null;        

        public int casherAccountID=-1;
        public int bankAccountID=-1;
        public double amount;

        public bool ammountApplies
        {
            get { return true; }
        }

        public double documentAmount
        {
            get { return amount; }
        }

        public bool hasInstrument
        {
            get { return true; }
        }

        public string getInstrument(bool includeInstrucmentType)
        {
            return PaperRef;
        }
    }
}
