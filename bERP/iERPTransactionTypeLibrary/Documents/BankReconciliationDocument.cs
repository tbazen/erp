﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP
{

    [Serializable]
    public class BankReconciliationDocument:AccountDocument
    {
        public int previosReconciationDocument;
        public int bankID;
        public int[] outstandingTransactions;
        public double bankBalance;
    }
}