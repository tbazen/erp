using System;
using System.Collections.Generic;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    [Serializable]
    public class BankToBankDocument : AccountDocument
    {
        [DocumentTypedReferenceFieldAttribute("CHKPV", "CPV", "PCPV", "PV", "JV", "CRV")]
        public DocumentTypedReference voucher = null;        
        public int toBankAccountID=-1;
        public int fromBankAccountID=-1;
        public double amount;
    }
}
