using System;
using System.Collections.Generic;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    [Serializable]
    public class BankWithdrawalDocument : AccountDocument,IStandardAttributeDocument,IPaymentDocument
    {
        [DocumentTypedReferenceFieldAttribute("CHKPV", "CPV", "PV", "PCPV", "JV","GEN")]
        public DocumentTypedReference voucher = null;        
        public string chekNumber;
        public int casherAccountID=-1;
        public int bankAccountID=-1;
        public double amount;

        public bool ammountApplies
        {
            get { return true; }
        }

        public double documentAmount
        {
            get { return amount; }
        }

        public bool hasInstrument
        {
            get { return true; }
        }

        public string paymentMethodReference { get => chekNumber; set => chekNumber=value; }
        public BizNetPaymentMethod paymentMethod
        {
            get => BizNetPaymentMethod.Check; set
            {
                if (value != BizNetPaymentMethod.Check) throw new ArgumentOutOfRangeException("Only check payment method is supported by bank deposit");
            }
        }
        public double serviceChargeAmount { get => 0; set
            {
                if (value != 0)
                    throw new ArgumentOutOfRangeException("Service charge is not supported for bank withdrawal");
            }
        }
        public int assetAccountID { get => casherAccountID; set => casherAccountID=value; }
        public ServiceChargePayer serviceChargePayer { get => ServiceChargePayer.Company; set { } }

        public double netPayment { get => amount; set => amount = value; }
        public DocumentTypedReference voucherReference { get => this.voucher; set => this.voucher = value; }

        public string getInstrument(bool includeInstrucmentType)
        {
            if (includeInstrucmentType)
                return "CHK: " + chekNumber;
            return chekNumber;
        }
    }
}
