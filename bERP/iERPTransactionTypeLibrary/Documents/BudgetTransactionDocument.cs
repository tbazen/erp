using System;
using System.Collections.Generic;
using INTAPS.Accounting;
using INTAPS.RDBMS;

namespace BIZNET.iERP
{
    [Serializable]
    public class BudgetTransaction
    {
        public int costCenterID;
        public int accountID;
        public double debitAmount;
        public double creditAmount;
        public string description;
    }
    [Serializable]
    public class BudgetTransactionDocument : AccountDocument
    {
        public bool transferProperties = true;
        public List<int> referedDocs=null;
        public int referedDocumentID = -1;
        public BudgetTransaction[] transactions;
    }
    [Serializable]
    [SingleTableObject]
    public class BudgetTransactionReference
    {
        [IDField]
        public int bugetTransaction;
        [DataField]
        public int referedTransaction;
    }

    public class NoBudgetAttribute:Attribute
    {

    }
    
}
