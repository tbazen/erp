using System;
using System.Collections.Generic;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    [Serializable]
    public class CashierToCashierDocument : AccountDocument
    {
        [DocumentTypedReferenceFieldAttribute("CHKPV", "CPV", "PCPV","PV", "JV", "CRV", "GEN")]
        public DocumentTypedReference voucher = null;        
        public int fromCashierAccountID=-1;
        public int toCashierAccountID=-1;
        public double amount;
    }
}
