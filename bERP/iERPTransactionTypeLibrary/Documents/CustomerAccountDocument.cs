using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Xml.Serialization;

namespace BIZNET.iERP
{
    [Serializable]
    public class CustomerAccountDocument : AccountDocument
    {
        public BizNetPaymentMethod paymentMethod;
        public int assetAccountID = -1;
        public double serviceChargeAmount;
        public ServiceChargePayer serviceChargePayer = ServiceChargePayer.None;
        public double amount;
        public string customerCode;
        public string checkNumber;
        public string slipReferenceNo;
        public string cpoNumber;

        public string paymentMethodReference
        {
            get
            {
                switch (paymentMethod)
                {
                    case BizNetPaymentMethod.Cash:
                        return "";
                    case BizNetPaymentMethod.Check:
                        return checkNumber;
                    case BizNetPaymentMethod.CPOFromCash:
                        return cpoNumber;
                    case BizNetPaymentMethod.CPOFromBankAccount:
                        return cpoNumber;
                    case BizNetPaymentMethod.BankTransferFromCash:
                        return slipReferenceNo;
                    case BizNetPaymentMethod.BankTransferFromBankAccount:
                        return slipReferenceNo;
                    case BizNetPaymentMethod.Credit:
                        return "";
                    case BizNetPaymentMethod.None:
                        return "";
                    default:
                        return "";
                }
            }
        }
        #region IStandardAttributeDocument
        public bool ammountApplies
        {
            get { return true; }
        }

        public double documentAmount
        {
            get { return this.amount; }
        }

        public bool hasInstrument
        {
            get { return PaymentMethodHelper.IsPaymentMethodBank(this.paymentMethod); }
        }

        public string getInstrument(bool includeInstrucmentType)
        {
            if (includeInstrucmentType)
                return PaymentMethodHelper.GetPaymentTypeReferenceName(this.paymentMethod) + ": " + this.paymentMethodReference;
            return this.paymentMethodReference;
        }
        #endregion
    }
    /// <summary>
    /// TradeRelation Advance Payment Document
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(Sell2Document))]
    [XmlInclude(typeof(CustomerAdvanceReturnDocument))]
    public class CustomerPayableDocument : CustomerAccountDocument
    {
        //CRSINV2 - Credit Sales Invoice /Hand to Hand Credit Sales/
        //CSINV2 - Cash sales Invoice Additional
        [DocumentTypedReferenceFieldAttribute("CRV", "FSN", "CSINV", "CRSINV", "GEN", "CRSINV2", "CSINV2")]
        public DocumentTypedReference voucher = null;   
        public AccountDocument[] scheduledSettlement = null;
        public bool calculateVAT; //calculate VAT from customer advance payment amount
        public bool calculateWithholding;
        public TaxRates taxRates;
        public double totalCash; //total cash calculated after taxes
    }
    [Serializable]
    public class CustomerReceivableDocument:CustomerAccountDocument
    {
        [DocumentTypedReferenceFieldAttribute("CRV", "FSN", "CSINV", "CRSINV", "GEN", "CSINV2", "CRSINV2")]
        public DocumentTypedReference voucher = null;
        public bool retention = false;

    }
    [Serializable]
    public class CustomerAdvanceReturnDocument : CustomerAccountDocument, IStandardAttributeDocument
    {
        [DocumentTypedReferenceFieldAttribute("CHKPV", "CPV", "PCPV", "PV", "JV", "GEN")]
        public DocumentTypedReference voucher = null;
    }
    [Serializable]
    public class BondPaymentDocument : CustomerAccountDocument, IStandardAttributeDocument
    {
        [DocumentTypedReferenceFieldAttribute("CHKPV", "CPV", "PCPV", "PV", "JV", "GEN")]
        public DocumentTypedReference voucher = null;   
        public bool returned = false;
    }
    [Serializable]
    public class BondReturnDocument : CustomerAccountDocument
    {
        [DocumentTypedReferenceFieldAttribute("CRV","GEN")]
        public DocumentTypedReference voucher = null;   
        public int bondPaymentDocumentID = -1;
        public bool returnBond = true;
    }
}
