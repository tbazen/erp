using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    [Serializable]
    [TaxDeclarationDocument("AP_Tax_Declaration", "Income Tax Declaration")]
    public class IncomeTaxDeclarationDocument : TaxDeclarationDocumentBase, IStandardAttributeDocument
    {

    }
}
