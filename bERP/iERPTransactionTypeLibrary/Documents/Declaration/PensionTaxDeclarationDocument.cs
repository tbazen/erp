using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    [Serializable]
    [TaxDeclarationDocument("AP_Tax_Declaration", "Pension Tax Declaration")]
    public class PensionTaxDeclarationDocument : TaxDeclarationDocumentBase, IStandardAttributeDocument
    {

    }
}
