using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    public class TaxDeclarationDocumentAttribute : Attribute
    {
        public string periodType;
        public string name;
        public TaxDeclarationDocumentAttribute(string periodType, string name)
        {
            this.periodType = periodType;
            this.name = name;
        }
    }
    [Serializable]
    public class TaxDeclarationDocumentBase:AccountDocument
    {
        public int declarationID;
        public int declarationPeriodID;
        public int[] declaredDocuments;

        [DocumentTypedReferenceFieldAttribute("CHKPV", "CPV", "PCPV", "PV", "JV", "GEN")]
        public DocumentTypedReference voucher = null;
        public BizNetPaymentMethod paymentMethod;
        public int assetAccountID = -1;
        public double serviceChargeAmount;
        public ServiceChargePayer serviceChargePayer = ServiceChargePayer.None;
        public string cpoNumber;
        public string checkNumber;
        public string receiptNumber; //Receipt number of the paid declaration receipt
        public double declaredAmount;
        public double penality;
        public double interest;
        public double unaccountedTax;
        public string otherAdditionNote;
        public double deduction;
        public string deductionNote;
        public double accumulatedVATReceivable;
        public double NetPay
        {
            get
            {
                return declaredAmount + penality + interest + unaccountedTax - deduction;
            }
        }
        public static TaxDeclarationDocumentAttribute GetAttribute(Type t)
        {
            object [] atrs=t.GetCustomAttributes(typeof(TaxDeclarationDocumentAttribute),false);
            if (atrs == null || atrs.Length != 1)
                throw new Exception("Tax declaration type attribute not properly set for " + t);
            return (TaxDeclarationDocumentAttribute)atrs[0];
        }

        public string paymentMethodReference
        {
            get
            {
                switch (paymentMethod)
                {
                    case BizNetPaymentMethod.Cash:
                        return "";
                    case BizNetPaymentMethod.Check:
                        return checkNumber;
                    case BizNetPaymentMethod.CPOFromCash:
                        return cpoNumber;
                    case BizNetPaymentMethod.CPOFromBankAccount:
                        return cpoNumber;
                    case BizNetPaymentMethod.BankTransferFromCash:
                        return this.checkNumber;
                    case BizNetPaymentMethod.BankTransferFromBankAccount:
                        return this.checkNumber;
                    case BizNetPaymentMethod.Credit:
                        return "";
                    case BizNetPaymentMethod.None:
                        return "";
                    default:
                        return "";
                }
            }
        }
        #region IStandardAttributeDocument
        public bool ammountApplies
        {
            get { return true; }
        }

        public double documentAmount
        {
            get { return this.NetPay; }
        }

        public bool hasInstrument
        {
            get { return PaymentMethodHelper.IsPaymentMethodBank(this.paymentMethod); }
        }

        public string getInstrument(bool includeInstrucmentType)
        {
            if (includeInstrucmentType)
                return PaymentMethodHelper.GetPaymentTypeReferenceName(this.paymentMethod) + ": " + this.paymentMethodReference;
            return this.paymentMethodReference;
        }
        #endregion
    }
}
