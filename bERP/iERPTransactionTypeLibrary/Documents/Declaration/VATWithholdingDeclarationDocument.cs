using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    [Serializable]
    [TaxDeclarationDocument("AP_Tax_Declaration", "VAT Withholding Declaration")]
    public class VATWithholdingDeclarationDocument : TaxDeclarationDocumentBase, IStandardAttributeDocument
    {

    }
}
