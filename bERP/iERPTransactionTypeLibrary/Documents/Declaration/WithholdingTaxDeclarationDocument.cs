using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    [Serializable]
    [TaxDeclarationDocument("AP_Tax_Declaration", "Withholding Tax Declaration")]
    public class WithholdingTaxDeclarationDocument : TaxDeclarationDocumentBase, IStandardAttributeDocument
    {

    }
}
