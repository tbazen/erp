﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    [Serializable]
    public class FixedAssetDepreciationDocument:AccountDocument
    {
        public TransactionDocumentItem[] items;
        [XmlIgnore]
        public bool verifyClientDocument = false;
    }

    [Serializable]
    public class FixedAssetDisposalDocument : AccountDocument
    {

        [DocumentTypedReferenceField("JV","GEN")]
        public DocumentTypedReference voucher;
        public TransactionDocumentItem[] items;

    }
}
