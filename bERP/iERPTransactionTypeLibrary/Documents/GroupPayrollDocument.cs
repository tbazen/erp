using System;
using System.Collections.Generic;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    [Serializable]
    public class GroupPayrollDocument : AccountDocument,IStandardAttributeDocument
    {
        [DocumentTypedReferenceFieldAttribute("CHKPV", "CPV", "PCPV", "PV", "JV", "GEN")]
        public DocumentTypedReference voucher = null;        
        public string description;
        public double grossSalaryTotalAmount;
        public double taxableGrossSalaryTotalAmount;
        public double incomeTaxTotalAmount;
        public double employeesTotalPensionAmount;
        public double employerTotalPensionAmount;
        public double overTimeTotalAmount;
        public double totalSalary;
        public double unclaimedSalary;
        public int costCenterID = 1; //default root cost center
        public int taxCenter;
        public bool accountAsCost;
        public int periodID;
        public bool declareTax = false;
        public bool fullUnclaimed = false;
        public BizNetPaymentMethod paymentMethod;
        public int assetAccountID = -1; //cash payment type ///assetAccountID
        public double serviceChargeAmount;
        public ServiceChargePayer serviceChargePayer = ServiceChargePayer.None;
        public string checkNumber;
        public string slipReferenceNo;
        public string cpoNumber;

        public string paymentMethodReference
        {
            get
            {
                switch (paymentMethod)
                {
                    case BizNetPaymentMethod.Cash:
                        return "";
                    case BizNetPaymentMethod.Check:
                        return checkNumber;
                    case BizNetPaymentMethod.CPOFromCash:
                        return cpoNumber;
                    case BizNetPaymentMethod.CPOFromBankAccount:
                        return cpoNumber;
                    case BizNetPaymentMethod.BankTransferFromCash:
                        return slipReferenceNo;
                    case BizNetPaymentMethod.BankTransferFromBankAccount:
                        return slipReferenceNo;
                    case BizNetPaymentMethod.Credit:
                        return "";
                    case BizNetPaymentMethod.None:
                        return "";
                    default:
                        return "";
                }
            }
        }
        public bool ammountApplies
        {
            get { return true; }
        }

        public double documentAmount
        {
            get { return this.totalSalary; }
        }

        public bool hasInstrument
        {
            get { return PaymentMethodHelper.IsPaymentMethodBank(this.paymentMethod); }
        }

        public string getInstrument(bool includeInstrucmentType)
        {
            if (includeInstrucmentType)
                return PaymentMethodHelper.GetPaymentTypeReferenceName(this.paymentMethod) + ": " +  this.paymentMethodReference;
            return this.paymentMethodReference;
        }
    }
   
}
