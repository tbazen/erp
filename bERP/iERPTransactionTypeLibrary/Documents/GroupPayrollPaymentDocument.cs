using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Xml.Serialization;

namespace BIZNET.iERP
{
    [Serializable]
    public class GroupPayrollPaymentDocument : AccountDocument, IStandardAttributeDocument
    {
        [DocumentTypedReferenceFieldAttribute("CHKPV", "CPV", "PCPV","PV" , "JV", "GEN")]
        public DocumentTypedReference voucher = null;        
        public BizNetPaymentMethod paymentMethod;
        public double amount;
        public int periodID;
        public int taxCenter;
        public int assetAccountID = -1;
        public int costCenterID;
        public double serviceChargeAmount;
        public ServiceChargePayer serviceChargePayer = ServiceChargePayer.None;
        public string checkNumber;
        public string cpoNumber;
        public string slipReferenceNo;

        public string paymentMethodReference
        {
            get
            {
                switch (paymentMethod)
                {
                    case BizNetPaymentMethod.Cash:
                        return "";
                    case BizNetPaymentMethod.Check:
                        return checkNumber;
                    case BizNetPaymentMethod.CPOFromCash:
                        return cpoNumber;
                    case BizNetPaymentMethod.CPOFromBankAccount:
                        return cpoNumber;
                    case BizNetPaymentMethod.BankTransferFromCash:
                        return slipReferenceNo;
                    case BizNetPaymentMethod.BankTransferFromBankAccount:
                        return slipReferenceNo;
                    case BizNetPaymentMethod.Credit:
                        return "";
                    case BizNetPaymentMethod.None:
                        return "";
                    default:
                        return "";
                }
            }
        }
        #region IStandardAttributeDocument
        public bool ammountApplies
        {
            get { return true; }
        }

        public double documentAmount
        {
            get { return this.amount; }
        }

        public bool hasInstrument
        {
            get { return PaymentMethodHelper.IsPaymentMethodBank(this.paymentMethod); }
        }

        public string getInstrument(bool includeInstrucmentType)
        {
            if (includeInstrucmentType)
                return PaymentMethodHelper.GetPaymentTypeReferenceName(this.paymentMethod) + ": " + this.paymentMethodReference;
            return this.paymentMethodReference;
        }
        #endregion
        
    }
}
