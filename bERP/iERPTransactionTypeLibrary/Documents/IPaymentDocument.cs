﻿using INTAPS.Accounting;

namespace BIZNET.iERP
{
    public  interface IPaymentDocument
    {
        string paymentMethodReference { get; set;}
        BizNetPaymentMethod paymentMethod { get; set;}
        double serviceChargeAmount { get; set; }
        int assetAccountID { get; set;}
        ServiceChargePayer serviceChargePayer { get; set;}
        double netPayment{ get; set;}
        DocumentTypedReference voucherReference { get; set; }
    }
}