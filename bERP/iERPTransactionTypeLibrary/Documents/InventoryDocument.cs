﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    [Serializable]
    public class TransactionDocumentItem
    {
        public int costCenterID=-1;
        public string code;
        public string name;
        public double quantity;
        public double unitPrice;
        public double rate;
        public double price;
        public bool directExpense=true;
        public bool remitted=false;
        public bool prepaid=false;
        public int unitID=-1;

        public TransactionDocumentItem(int costCenterID,TransactionItems item, double quantity, double unitPrice)
        {
            this.costCenterID = costCenterID;
            this.code = item.Code;
            this.name = item.Name;
            this.quantity = quantity;
            this.unitPrice = unitPrice;
            this.price = quantity * unitPrice;
        }

        public TransactionDocumentItem()
        {

        }
        public TransactionDocumentItem clone()
        {
            return this.MemberwiseClone() as TransactionDocumentItem;
        }

        public static TransactionDocumentItem sumByItem(IEnumerable<TransactionDocumentItem> items, string itemCode,int costCenterID)
        {
            TransactionDocumentItem ret = new TransactionDocumentItem();
            ret.code = itemCode;
            ret.costCenterID = costCenterID;
            ret.quantity = 0;
            ret.price = 0;
            if (items != null)
            {
                foreach (TransactionDocumentItem item in items)
                {
                    if (item.code.Equals(itemCode, StringComparison.OrdinalIgnoreCase) && (costCenterID == -1 || costCenterID == item.costCenterID))
                    {
                        ret.quantity += item.quantity;
                        ret.price += item.price;
                    }
                }
            }
            return ret;
        }
        public static bool allHasUnitPrice(IEnumerable<TransactionDocumentItem> items)
        {
            foreach (TransactionDocumentItem item in items)
                if (AccountBase.AmountEqual(item.unitPrice, 0))
                    return false;
            return true;
        }
        public static double totalAmount(IEnumerable<TransactionDocumentItem> items)
        {
            if (items == null)
                return 0;
            double ret = 0;
            foreach (TransactionDocumentItem item in items)
            {
                ret += item.price;
            }
            return ret;
        }
        public static double totalQuantity(IEnumerable<TransactionDocumentItem> items)
        {
            if (items == null)
                return 0;
            double ret = 0;
            
            foreach (TransactionDocumentItem item in items)
            {
                ret += item.quantity>0?item.quantity:1;
            }
            return ret;
        }
        public static TransactionDocumentItem[] uniquefy(TransactionDocumentItem[] transactionDocumentItem)
        {
            Dictionary<string, TransactionDocumentItem> ret = new Dictionary<string, TransactionDocumentItem>();
            foreach (TransactionDocumentItem ditem in transactionDocumentItem)
            {
                if (ret.ContainsKey(ditem.code.ToUpper()))
                {
                    TransactionDocumentItem old = ret[ditem.code.ToUpper()];
                    double q = old.quantity + ditem.quantity;
                    double p = old.price + ditem.price;
                    old.price = p;
                    old.quantity = q;
                    if (AccountBase.AmountEqual(q, 0))
                        old.unitPrice = 0;
                    else
                        old.unitPrice = p / q;
                }
                else
                    ret.Add(ditem.code.ToUpper(), ditem);
            }
            TransactionDocumentItem[] arr = new TransactionDocumentItem[ret.Values.Count];
            ret.Values.CopyTo(arr, 0);
            return arr;
        }
        public static bool isUnique(IEnumerable<TransactionDocumentItem> items)
        {
            if (items == null)
                return true;
            if (items is TransactionDocumentItem[])
            {
                TransactionDocumentItem[] arr = (TransactionDocumentItem[])items;
                for (int i = 0; i < arr.Length - 1; i++)
                {
                    for (int j = i + 1; j < arr.Length; j++)
                    {
                        if (arr[i].code.Equals(arr[j].code))
                            return false;
                    }
                }
                return true;
            }
            foreach(TransactionDocumentItem i1 in items)
                foreach(TransactionDocumentItem i2 in items)
                {
                    if(i1==i2)
                        continue;
                    if (i1.code.Equals(i2.code))
                        return false;
                }
            return true;
        }
        public static bool isSubset(IEnumerable<TransactionDocumentItem> domain, IEnumerable<TransactionDocumentItem> set)
        {
            if(!isUnique(domain) || !isUnique(set))
                throw new InvalidOperationException("Unique set of items expected");
            foreach (TransactionDocumentItem s in set)
            {
                TransactionDocumentItem requested = null;
                foreach (TransactionDocumentItem d in domain)
                {
                    if (s.code.Equals(d.code) && s.costCenterID == d.costCenterID)
                    {
                        requested = d;
                        break;
                    }
                }
                if (requested == null)
                    return false;
                if (AccountBase.AmountLess(requested.quantity, s.quantity))
                    return false;
            }
            return true;
        }
        public static List<TransactionDocumentItem> subTract(IEnumerable<TransactionDocumentItem> items1, IEnumerable<TransactionDocumentItem> items2)
        {
            bool p;
            return subTract(items1, items2, out p);
        }
        public static List<TransactionDocumentItem> subTract(IEnumerable<TransactionDocumentItem> items1, IEnumerable<TransactionDocumentItem> items2,out bool hasNoNegative)
        {
            if (items1 == null && items2 == null)
            {
                hasNoNegative = true;
                return null;
            }
            if (!isUnique(items1) || !isUnique(items2))
                throw new InvalidOperationException("Unique set of items expected");
            
            hasNoNegative = true;
            List<TransactionDocumentItem> ret = new List<TransactionDocumentItem>();
            if (items2 == null)
            {
                foreach (TransactionDocumentItem s in items1)
                {
                    TransactionDocumentItem diff = s.clone();
                    ret.Add(diff);
                }
            }
            else
            {
                if (items1 != null)
                {
                    foreach (TransactionDocumentItem d in items1)
                        ret.Add(d.clone());
                }

                foreach (TransactionDocumentItem s in items2)
                {
                    TransactionDocumentItem requested = null;
                    if (items1 != null)
                    {
                        foreach (TransactionDocumentItem d in ret)
                        {
                            if (s.code.Equals(d.code) /*&& s.costCenterID == d.costCenterID*/)
                            {
                                if (requested == null)
                                {
                                    requested = d;
                                    break;
                                }
                            }
                        }
                    }
                    if (requested == null)
                    {
                        TransactionDocumentItem itm = s.clone();
                        itm.quantity = -s.quantity;
                        ret.Add(itm);
                        hasNoNegative = false;
                        continue;
                    }
                    if (AccountBase.AmountEqual(requested.quantity, s.quantity))
                    {
                        ret.Remove(requested);
                        continue;
                    }
                    if (AccountBase.AmountLess(requested.quantity, s.quantity))
                        hasNoNegative = false;
                    requested.quantity-=s.quantity;
                }
            }
            return ret;
        }
        public static List<TransactionDocumentItem> add(IEnumerable< TransactionDocumentItem> items1, IEnumerable< TransactionDocumentItem> items2)
        {
            if (items1 == null && items2 == null)
                return null;
            if (!isUnique(items1) || !isUnique(items2))
                throw new InvalidOperationException("Unique set of items expected");
            List<TransactionDocumentItem> ret = new List<TransactionDocumentItem>();
            if (items2 == null)
            {
                if (items1 == null)
                    return null;
                foreach (TransactionDocumentItem d in items1)
                {
                    ret.Add(d.clone());
                }
            }
            else
            {
                if (items1 != null)
                {
                    foreach (TransactionDocumentItem d in items1)
                        ret.Add(d.clone());
                }
                foreach (TransactionDocumentItem s in items2)
                {
                    TransactionDocumentItem leftSide = null;
                    if (items1 != null)
                    {
                        foreach (TransactionDocumentItem d in ret)
                        {
                            if (s.code.Equals(d.code) && s.costCenterID == d.costCenterID)
                            {
                                if (leftSide == null)
                                {
                                    leftSide = d;
                                    break;
                                }
                            }
                        }
                    }
                    if (leftSide == null)
                    {
                        TransactionDocumentItem itm = s.clone();
                        itm.quantity = s.quantity;
                        ret.Add(itm);
                        continue;
                    }
                    leftSide.quantity += s.quantity;
                }
            }
            return ret;
        }
        public static bool equalQuantityPrice(IList<TransactionDocumentItem> set1, IList<TransactionDocumentItem> set2)
        {
            if (set1 == null && set2 == null)
                return true;
            if (set2 == null)
                return false;
            if (set1 == null)
                return false;
            
            if (!isUnique(set1) || !isUnique(set2))
                throw new InvalidOperationException("Unique set of items expected");

            if (set1.Count != set2.Count)
                return false;
            foreach (TransactionDocumentItem i1 in set1)
            {
                bool found = false;
                foreach (TransactionDocumentItem i2 in set2)
                {
                    if (AccountBase.AmountEqual(i1.quantity, i2.quantity))
                    {
                        if (AccountBase.AmountEqual(i1.unitPrice, i2.unitPrice))
                        {
                            found = true;
                            break;
                        }
                        else
                            return false;
                    }
                }
                if(!found)
                    return false;
            }
            return true;
        }
    }
    public static class TransactionDocumentItemArrayExtensions
    {
        public static double totalAmount(this TransactionDocumentItem[] items)
        {
            return TransactionDocumentItem.totalAmount(items);
        }
    }
    [Serializable]
    public class InventoryDocument:AccountDocument
    {
        [DocumentTypedReferenceFieldAttribute("JV", "GEN")]
        public DocumentTypedReference voucher = null;        
        public int storeID;
        public bool fixedAsset = false;
        public TransactionDocumentItem[] items;
    }
    [Serializable]
    public class FixedAssetInventoryItem
    {
        public string itemCode;
        public double quantity;
        public double orginalValue;
        public double accumulatedDepreciation;
    }
    [Serializable]
    public class FixedAssetInventoryDocument:AccountDocument
    {
        [DocumentTypedReferenceFieldAttribute("JV", "GEN")]
        public DocumentTypedReference voucher = null;
        public int costCenterID;
        public FixedAssetInventoryItem[] items;
    }

    [Serializable]
    public class ReturnToStoreDocument: AccountDocument
    {
        [DocumentTypedReferenceFieldAttribute("GRV", "JV", "GEN")]
        public DocumentTypedReference voucher = null;
        public int sourceCostCenterID;
        public int storeID;
        public TransactionDocumentItem[] items;
    }
    [Serializable]
    public class StoreIssueDocument : AccountDocument
    {
        [DocumentTypedReferenceFieldAttribute("SIV", "DO","GEN","JOB")]
        public DocumentTypedReference voucher = null;        
        public int storeID;
        public TransactionDocumentItem[] items;
    }
    [Serializable]
    public class StoreFinishedGoodsDocument : AccountDocument
    {
        [DocumentTypedReferenceFieldAttribute("SRV", "GEN", "JOB")]
        public DocumentTypedReference voucher = null;
        public int costCenterID;
        public TransactionDocumentItem[] items;
    }
    [Serializable]
    public class StoreTransferDocument: AccountDocument
    {
        [DocumentTypedReferenceFieldAttribute("STV","SIV","SRV", "JOB", "GEN")]
        public DocumentTypedReference voucher = null;        
        public int sourceStoreID;
        public int destinationStoreID;
        public TransactionDocumentItem[] items;
    }
    [Serializable]
    public class InternalServiceDocument : AccountDocument
    {
        [DocumentTypedReferenceFieldAttribute("JV", "GEN")]
        public DocumentTypedReference voucher = null;        
        public int sourceCostCenterID;
        public int destinationCostCenterID;
        public TransactionDocumentItem[] items;
    }
}
