using System;
using System.Collections.Generic;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    [Serializable]
    public class LaborPaymentDocument : AccountDocument
    {
        [DocumentTypedReferenceFieldAttribute("CHKPV", "CPV", "PCPV", "PV", "PRN", "JV", "PON", "GEN")]
        public DocumentTypedReference voucher = null;
        public BizNetPaymentMethod paymentMethod;
        public double serviceChargeAmount=0;
        public ServiceChargePayer serviceChargePayer = ServiceChargePayer.None;
        public int assetAccountID=-1;
        public double amount;
        public string checkNumber;
        public string slipReferenceNo;
        public string paidTo;
        public int costCenterID = 1;
        public int taxCenter = 1;
        public string paymentMethodReference
        {
            get
            {
                switch (paymentMethod)
                {
                    case BizNetPaymentMethod.Cash:
                        return "";
                    case BizNetPaymentMethod.Check:
                        return checkNumber;
                    case BizNetPaymentMethod.CPOFromCash:
                        return checkNumber;
                    case BizNetPaymentMethod.CPOFromBankAccount:
                        return checkNumber;
                    case BizNetPaymentMethod.BankTransferFromCash:
                        return slipReferenceNo;
                    case BizNetPaymentMethod.BankTransferFromBankAccount:
                        return slipReferenceNo;
                    case BizNetPaymentMethod.Credit:
                        return "";
                    case BizNetPaymentMethod.None:
                        return "";
                    default:
                        return "";
                }

            }
            set
            {
                switch (paymentMethod)
                {
                    case BizNetPaymentMethod.Check:
                        this.checkNumber = value;
                        break;
                    case BizNetPaymentMethod.CPOFromCash:
                        this.checkNumber = value;
                        break;
                    case BizNetPaymentMethod.CPOFromBankAccount:
                        this.checkNumber = value;
                        break;
                    case BizNetPaymentMethod.BankTransferFromCash:
                        this.slipReferenceNo = value;
                        break;
                    case BizNetPaymentMethod.BankTransferFromBankAccount:
                        this.slipReferenceNo = value;
                        break;
                }
            }
        }
    }
   
}
