using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Xml.Serialization;

namespace BIZNET.iERP
{
    [Serializable]
    [XmlInclude(typeof(AccountDocument))]
    public class OpeningTransactionDocument : AccountDocument
    {
        public int[] capitalAccountID;
        public double[] capitals; //Balance amount

        public double accumulatedVATBalance;
 
        public int[] bankAccounts;
        public double[] bankBalances;

        public int[] cashOnHandsAccountID;
        public double[] cashOnHands; //Balance amount

        public int[] staffLoanAccountID;
        public double[] staffLoans; //Balance amount

        public int costCenterID;
    }
}
