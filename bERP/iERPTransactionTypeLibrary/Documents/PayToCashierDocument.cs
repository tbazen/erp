using System;
using System.Collections.Generic;
using INTAPS.Accounting;
using System.Xml.Serialization;

namespace BIZNET.iERP
{
    [Serializable]
    public class PayToCashierDocument : AccountDocument,IStandardAttributeDocument
    {
        [DocumentTypedReferenceFieldAttribute("CHKPV", "CPV", "PCPV", "PV", "JV", "CRV", "FSN", "CSINV", "CRSINV", "GEN")]
        public DocumentTypedReference voucher = null;        

        public BizNetPaymentMethod paymentMethod;
        public int assetAccountID = -1; //cash payment type ///assetAccountID
        public int destinationCashAccID=-1;
        public double serviceChargeAmount;
        public ServiceChargePayer serviceChargePayer = ServiceChargePayer.None;
        public int employeeID;// int employeeID
        public double amount;
        public string checkNumber;
        public string slipReferenceNo;
        public string cpoNumber;

        public string paymentMethodReference
        {
            get
            {
                switch (paymentMethod)
                {
                    case BizNetPaymentMethod.Cash:
                        return "";
                    case BizNetPaymentMethod.Check:
                        return checkNumber;
                    case BizNetPaymentMethod.CPOFromCash:
                        return cpoNumber;
                    case BizNetPaymentMethod.CPOFromBankAccount:
                        return cpoNumber;
                    case BizNetPaymentMethod.BankTransferFromCash:
                        return slipReferenceNo;
                    case BizNetPaymentMethod.BankTransferFromBankAccount:
                        return slipReferenceNo;
                    case BizNetPaymentMethod.Credit:
                        return "";
                    case BizNetPaymentMethod.None:
                        return "";
                    default:
                        return "";
                }
                
            }
            set
            {
                switch (paymentMethod)
                {
                    case BizNetPaymentMethod.Check:
                        this.checkNumber = value;
                        break;
                    case BizNetPaymentMethod.CPOFromCash:
                        this.cpoNumber = value;
                        break;
                    case BizNetPaymentMethod.CPOFromBankAccount:
                        this.cpoNumber = value;
                        break;
                    case BizNetPaymentMethod.BankTransferFromCash:
                        this.slipReferenceNo = value;
                        break;
                    case BizNetPaymentMethod.BankTransferFromBankAccount:
                        this.slipReferenceNo = value;
                        break;
                }
            }
        }
        #region IStandardAttributeDocument
        public bool ammountApplies
        {
            get { return true; }
        }
        
        public double documentAmount
        {
            get { return this.amount; }
        }

        public bool hasInstrument
        {
            get { return PaymentMethodHelper.IsPaymentMethodBank(this.paymentMethod); }
        }

        public string getInstrument(bool includeInstrucmentType)
        {
            if (includeInstrucmentType)
                return PaymentMethodHelper.GetPaymentTypeReferenceName(this.paymentMethod) + ": " + this.paymentMethodReference;
            return this.paymentMethodReference;
        }
        #endregion
    }
    
}
