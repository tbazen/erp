using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Payroll;
using System.Xml.Serialization;
namespace BIZNET.iERP
{
    [Serializable]
    public class PayrollPaymentSetDocument : AccountDocument
    {
        public BizNetPaymentMethod paymentMethod;
        public string checkNumber;
        public string cpoNumber;
        public string transferReference;
        public int assetAccountID = -1;
        public double serviceChargeAmount;
        public ServiceChargePayer serviceChargePayer = ServiceChargePayer.None;

        [DocumentTypedReferenceFieldAttribute("CHKPV", "CPV", "PCPV", "PV","JV", "GEN")]
        public DocumentTypedReference voucher = null;
        public PayrollPeriod payPeriod;
        [XmlIgnore]
        public int[] employeeID;
        [XmlIgnore]
        public int periodID;

        public Employee[] employee;
        public double[] amount;
        public int payrollDocumentID;
        public PayrollPayment getPayment(int index)
        {
            PayrollPayment payment = new PayrollPayment();
            payment.accountDocumentID = this.AccountDocumentID;
            payment.amount = amount[index];
            payment.employeeID = employee[index].id;
            payment.periodID = payPeriod.id;
            return payment;
        }
        public string paymentMethodReference
        {
            get
            {
                switch (paymentMethod)
                {
                    case BizNetPaymentMethod.Cash:
                        return "";
                    case BizNetPaymentMethod.Check:
                        return checkNumber;
                    case BizNetPaymentMethod.CPOFromCash:
                        return checkNumber;
                    case BizNetPaymentMethod.CPOFromBankAccount:
                        return checkNumber;
                    case BizNetPaymentMethod.BankTransferFromCash:
                        return transferReference;
                    case BizNetPaymentMethod.BankTransferFromBankAccount:
                        return transferReference;
                    case BizNetPaymentMethod.Credit:
                        return "";
                    case BizNetPaymentMethod.None:
                        return "";
                    default:
                        return "";
                }

            }
            set
            {
                switch (paymentMethod)
                {
                    case BizNetPaymentMethod.Check:
                        this.checkNumber = value;
                        break;
                    case BizNetPaymentMethod.CPOFromCash:
                        this.checkNumber = value;
                        break;
                    case BizNetPaymentMethod.CPOFromBankAccount:
                        this.checkNumber = value;
                        break;
                    case BizNetPaymentMethod.BankTransferFromCash:
                        this.transferReference = value;
                        break;
                    case BizNetPaymentMethod.BankTransferFromBankAccount:
                        this.transferReference = value;
                        break;
                }
            }
        }
        public double totalAmount
        {
            get
            {
                double net = 0;
                foreach (double a in amount)
                    net += a;
                return net;
            }
        }
    }
}
