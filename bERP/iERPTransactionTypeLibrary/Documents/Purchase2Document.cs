﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Xml.Serialization;

namespace BIZNET.iERP
{
    [Serializable]
    public class ItemDeliveryDocument : AccountDocument
    {
        public string relationCode;
        public TransactionDocumentItem[] items;
    }
    [Serializable]
    public class PurchasedItemDeliveryDocument : ItemDeliveryDocument
    {
        [DocumentTypedReferenceFieldAttribute("SRV","SC", "PRN", "PO", "GEN")]
        public DocumentTypedReference voucher = null;
        public int purchaseDocumentID = -1;
        public TransactionDocumentItem[] orderItems = null;
    }
    [Serializable]
    public class SoldItemDeliveryDocument : ItemDeliveryDocument
    {
        [DocumentTypedReferenceFieldAttribute("DO","SIV", "JV", "GEN")]
        public DocumentTypedReference voucher = null;
        public int salesDocumentID= -1;

    }

    [Serializable]
    public class PrePaymentDocument:ItemDeliveryDocument
    {
    }
    [Serializable]
    public class PurchasePrePaymentDocument : PrePaymentDocument
    {
        [DocumentTypedReferenceFieldAttribute("PO","CHKPV","CPV" ,"PV" ,"GEN")]
        public DocumentTypedReference voucher = null;
        public int purchaseDocumentID = -1;

    }
    [Serializable]
    public class SellPrePaymentDocument : PrePaymentDocument
    {
        [DocumentTypedReferenceFieldAttribute("FSN", "CSINV", "CRSINV" ,"CRV","GEN")]
        public DocumentTypedReference voucher = null;
        public int sellsAccountDocumentID;
    }
    [Serializable]
    public class TradeTransaction : AccountDocument
    {
        //payment detail
        private BizNetPaymentMethod _paymentMethod;
        public string checkNumber;
        public string cpoNumber;
        public string transferReference;
        private int _assetAccountID = -1;
        private double _serviceChargeAmount;
        private ServiceChargePayer _serviceChargePayer = ServiceChargePayer.None;
        public double settleAdvancePayment;
        public double supplierCredit;
        public double paidAmount;
        public double retention = 0;
        
        public string relationCode;
        public TradeRelation relationData=null;
        //transaction detail
        public TransactionDocumentItem[] items;
        public double amount;
        public int[] creditSettlementIDs = new int[0];
        public int[] deliverieIDs = new int[0];
        public int[] prepaymentsIDs = new int[0];

        
        //taxes
        public string withholdingReceiptNo;
        public string vatWithholdingReceiptNo;
        public TaxImposed[] taxImposed;
        public double[] costWithTax=null;
        public TaxRates taxRates;

        public string paymentMethodReference
        {
            get
            {
                switch (paymentMethod)
                {
                    case BizNetPaymentMethod.Cash:
                        return "";
                    case BizNetPaymentMethod.Check:
                        return checkNumber;
                    case BizNetPaymentMethod.CPOFromCash:
                        return cpoNumber;
                    case BizNetPaymentMethod.CPOFromBankAccount:
                        return cpoNumber;
                    case BizNetPaymentMethod.BankTransferFromCash:
                        return transferReference;
                    case BizNetPaymentMethod.BankTransferFromBankAccount:
                        return transferReference;
                    case BizNetPaymentMethod.Credit:
                        return "";
                    case BizNetPaymentMethod.None:
                        return "";
                    default:
                        return "";
                }

            }
            set
            {
                switch (paymentMethod)
                {
                    case BizNetPaymentMethod.Check:
                        this.checkNumber = value;
                        break;
                    case BizNetPaymentMethod.CPOFromCash:
                        this.cpoNumber = value;
                        break;
                    case BizNetPaymentMethod.CPOFromBankAccount:
                        this.cpoNumber = value;
                        break;
                    case BizNetPaymentMethod.BankTransferFromCash:
                        this.transferReference = value;
                        break;
                    case BizNetPaymentMethod.BankTransferFromBankAccount:
                        this.transferReference = value;
                        break;
                }
            }
        }
        public static double GetTOTCalculationRate(GoodOrService goodOrService, ServiceType serviceType, TaxRates rates)
        {
            double totRate = 0;

            if (goodOrService == GoodOrService.Service)
            {
                switch (serviceType)
                {
                    case ServiceType.None:
                        totRate = rates.ServicesTaxable.OtherServicesTaxableRate;
                        break;
                    case ServiceType.Contractors:
                        totRate = rates.ServicesTaxable.ContractorsTaxableRate;
                        break;
                    case ServiceType.MillServices:
                        totRate = rates.ServicesTaxable.MillServicesTaxableRate;
                        break;
                    case ServiceType.TractorsAndCombinedHarvesters:
                        totRate = rates.ServicesTaxable.TractorsAndHarvTaxableRate;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                totRate = rates.GoodsTaxableRate;
            }
            return totRate;
        }
        public static double GetWithHoldingCalculationRate(bool tinNumberKnown, double itemTotalPrice, GoodOrService goodOrService, bool mixedType, TaxRates taxRates)
        {
            double nonTaxablePriceLimit, withholdingRate;
            nonTaxablePriceLimit = withholdingRate = 0;

            if (mixedType)
            {
                nonTaxablePriceLimit = taxRates.WHServicesNonTaxablePriceLimit;
                withholdingRate = tinNumberKnown ? taxRates.WHServicesTaxableRate : taxRates.UnknownTINWithHoldingRate;
            }
            else if (!mixedType && goodOrService == GoodOrService.Service)
            {
                nonTaxablePriceLimit = taxRates.WHServicesNonTaxablePriceLimit;
                withholdingRate = tinNumberKnown ? taxRates.WHServicesTaxableRate : taxRates.UnknownTINWithHoldingRate;
            }
            else if (!mixedType && goodOrService == GoodOrService.Good)
            {
                nonTaxablePriceLimit = taxRates.WHGoodsNonTaxablePriceLimit;
                withholdingRate = tinNumberKnown ? taxRates.WHGoodsTaxableRate : taxRates.UnknownTINWithHoldingRate;
            }

            if (itemTotalPrice < nonTaxablePriceLimit)
                return 0;
            else
                return withholdingRate;

        }
        public static double GetVATWithholdingRate(double itemTotalPrice, TaxRates taxRates)
        {
            double nonTaxablePriceLimit, vatRate;
            nonTaxablePriceLimit = vatRate = 0;
            nonTaxablePriceLimit = taxRates.VATWithholdingNonTaxablePriceLimit;
            vatRate = taxRates.CommonVATRate;

            if (itemTotalPrice < nonTaxablePriceLimit)
                return 0;
            else
                return vatRate;

        }

        public static void addTax(Dictionary<TaxType, TaxImposed> taxList,TaxImposed tax)
        {
            if (AccountBase.AmountEqual(tax.TaxValue, 0))
                return;
            if (taxList.ContainsKey(tax.TaxType))
            {
                taxList[tax.TaxType].TaxBaseValue += tax.TaxBaseValue;
                taxList[tax.TaxType].TaxValue += tax.TaxValue;
            }
            else
            {
                taxList.Add(tax.TaxType, tax);
            }
        }
        public static void setTax(Dictionary<TaxType, TaxImposed> taxList, TaxImposed tax)
        {
            if (taxList.ContainsKey(tax.TaxType))
            {
                if (AccountBase.AmountEqual(tax.TaxValue, 0))
                    taxList.Remove(tax.TaxType);
                else
                {
                    taxList[tax.TaxType].TaxBaseValue= tax.TaxBaseValue;
                    taxList[tax.TaxType].TaxValue= tax.TaxValue;
                }
            }
            else
            {
                taxList.Add(tax.TaxType, tax);
            }
        }
        

        public double totalBeforeTax
        {
            get
            {
                double ret = 0;
                foreach (TransactionDocumentItem ditem in items)
                    ret += ditem.quantity*ditem.unitPrice;
                return ret;
            }
        }
        public double totalTax
        {
            get
            {
                double ret = 0;
                foreach (TaxImposed taxItem in taxImposed)
                    ret += taxItem.TaxValue;
                return ret;
            }
        }
        public double totalAfterTax
        {
            get
            {
                return totalBeforeTax + totalTax;
            }
        }

        public double serviceChargeAmount { get => _serviceChargeAmount; set => _serviceChargeAmount = value; }
        public int assetAccountID { get => _assetAccountID; set => _assetAccountID = value; }
        public ServiceChargePayer serviceChargePayer { get => _serviceChargePayer; set => _serviceChargePayer = value; }
        public BizNetPaymentMethod paymentMethod { get => _paymentMethod; set => _paymentMethod = value; }

        public double getTax(TaxType type)
        {
            double ret = 0;
            foreach (TaxImposed taxItem in taxImposed)
                if (taxItem.TaxType == type)
                    ret += taxItem.TaxValue;
            return ret;
        }
        [XmlIgnore]
        public bool replaceAttachments = false;
    }    

    [Serializable]
    public class Purchase2Document : TradeTransaction,IStandardAttributeDocument,IPaymentDocument
    {
        [DocumentTypedReferenceFieldAttribute("CHKPV", "CPV", "PCPV", "PV", "JV","PO","GEN")]
        public DocumentTypedReference paymentVoucher = null;
        public double manualWithholdBase = -1;
        public double manualVATBase = -1;
        public bool invoiced = true;
        public bool postTransactions = true;
        public int[] invoiceIDs = new int[0];
        [XmlIgnore]
        public SupplierCreditSettlement2Document[] creditSettlements = new SupplierCreditSettlement2Document[0];
        [XmlIgnore]
        public PurchasedItemDeliveryDocument[] deliveries=new PurchasedItemDeliveryDocument[0];
        [XmlIgnore]
        public PurchasePrePaymentDocument[] prepayments = new PurchasePrePaymentDocument[0];
        [XmlIgnore]
        public PurchaseInvoiceDocument[] invoices = new PurchaseInvoiceDocument[0];
       
        
        public static TaxImposed[] getTaxes(TradeRelation supplier
            , CompanyProfile profile
            ,TransactionDocumentItem[] docItems
            ,TransactionItems[] items
            ,TaxRates rates
            ,bool isVATBaseCustom, double customSalesTaxBase
            ,bool isWHBaseCustom, double customWHBase
            ,out double totalPurchase,out double totalTaxable
            ,out bool vatApplies,out bool whApplies
            ,out double [] itemPriceWithTax
            )
        {
            itemPriceWithTax = new double[docItems.Length];
            vatApplies = whApplies = false;
            Dictionary<TaxType, TaxImposed> taxList = new Dictionary<TaxType, TaxImposed>();
            totalPurchase = 0;
            totalTaxable = 0;
            int i = 0;
            GoodOrService goodOrService = GoodOrService.Good;
            bool mixedGoodAndService = false;
            //totals
            bool[] sellsTaxable = new bool[items.Length];
            foreach (TransactionItems item in items)
            {
                if (i == 0)
                    goodOrService = item.GoodOrService;
                else
                    if (!mixedGoodAndService && goodOrService != item.GoodOrService)
                        mixedGoodAndService = true;
                totalPurchase += docItems[i].price;
                sellsTaxable[i] = item.TaxStatus == ItemTaxStatus.Taxable && !docItems[i].remitted;
                if (sellsTaxable[i])
                    totalTaxable += docItems[i].price;
                i++;
            }
            //taxes
            if (!AccountBase.AmountEqual(totalTaxable, 0) && supplier != null && supplier.TaxRegistrationType != TaxRegisrationType.NonTaxPayer)
            {

                if (supplier.TaxRegistrationType == TaxRegisrationType.TOT)
                {
                    i = 0;
                    foreach (TransactionDocumentItem ditem in docItems)
                    {
                        TransactionItems item = items[i];

                        double totRate = GetTOTCalculationRate(item.GoodOrService, item.ServiceType, rates);
                        addTax(taxList, new TaxImposed(TaxType.TOT, docItems[i].price, totRate * docItems[i].price));
                        itemPriceWithTax[i] = docItems[i].price * (1 + totRate);
                        i++;
                    }
                    if (isVATBaseCustom)
                    {
                        if (taxList.ContainsKey(TaxType.TOT))
                        {
                            TaxImposed tax = taxList[TaxType.TOT];
                            tax.TaxValue = customSalesTaxBase / tax.TaxBaseValue * tax.TaxValue;
                            tax.TaxBaseValue = customSalesTaxBase;
                            setTax(taxList, tax);
                        }
                    }
                }
                else if (vatApplies = (supplier.TaxRegistrationType == TaxRegisrationType.VAT))
                {
                    double vatBase = isVATBaseCustom ? customSalesTaxBase : totalTaxable;
                    addTax(taxList, new TaxImposed(TaxType.VAT, vatBase, vatBase * rates.CommonVATRate));
                    if(profile.isVATAgent)
                    {
                        double vatWithholdingRate = GetVATWithholdingRate(vatBase, rates);
                        if (vatWithholdingRate != 0)
                            addTax(taxList, new TaxImposed(TaxType.VATWitholding, vatBase, -vatBase * vatWithholdingRate));
                    }
                    i = 0;
                    foreach (TransactionDocumentItem ditem in docItems)
                    {
                        TransactionItems item = items[i];
                        if (supplier.TaxRegistrationType == TaxRegisrationType.VAT)
                        {
                            if (AccountBase.AmountEqual(totalTaxable, 0) || !sellsTaxable[i])
                                itemPriceWithTax[i] = docItems[i].price;
                            else
                                itemPriceWithTax[i] = docItems[i].price * (1 + rates.CommonVATRate * vatBase / totalTaxable);
                        }
                        i++;
                    }
                }
            }
            else
            {
                i = 0;

                foreach (TransactionDocumentItem ditem in docItems)
                {
                    TransactionItems item = items[i];
                    itemPriceWithTax[i] = docItems[i].price;
                    i++;
                }
            }

            if (supplier != null)
            {
                if (whApplies = (profile.isWitholdingAgent && supplier.Withhold)) //if company is withholding agent and supplier should be withheld tax
                {

                    double whBase = isWHBaseCustom ? customWHBase : totalPurchase;
                    double whRate = GetWithHoldingCalculationRate(supplier.hasTINNumber, whBase, goodOrService, mixedGoodAndService, rates);
                    whApplies=whApplies&& !AccountBase.AmountEqual(whRate,0);
                    if (whApplies)
                    {
                        addTax(taxList, new TaxImposed(TaxType.WithHoldingTax, whBase,
                            -whBase * whRate));
                    }
                }
            }
            TaxImposed[] ret=new TaxImposed[taxList.Count];
            taxList.Values.CopyTo(ret,0);
            return ret;
        }

        public static bool TaxAuthorityUnclaimableItemExists(TransactionItems item)
        {
            if (item.ExpenseType == ExpenseType.TaxAuthorityUnclaimable)
                return true;
            return false;
        }

        public double netPayment
        {
            get
            {
                return this.paidAmount;
            }
            set
            {
                this.paidAmount = value;
            }
        }
        public double overPayment
        {
            get
            {
                double np = netPayment;
                if (AccountBase.AmountGreater(paidAmount, np))
                    return paidAmount - np;
                return 0;
            }
        }
        public double underPayment
        {
            get
            {
                double np = netPayment;
                if (AccountBase.AmountLess(paidAmount, np))
                    return np - paidAmount;
                return 0;
            }
        }

        public List<int> getAttachmentArray()
        {
            List<int> ret = new List<int>();
            int[][] docIDs = new int[][] { this.deliverieIDs, this.prepaymentsIDs, this.creditSettlementIDs };
            foreach (int[] ar in docIDs)
                ret.AddRange(ar);
            return ret;
        }



        public bool isSalesTaxAddedToCost(CompanyProfile company, TradeRelation supplier)
        {
           //taxes
            foreach (TaxImposed tax in this.taxImposed)
            {
                switch (tax.TaxType)
                {
                    case TaxType.VAT:
                        if (company.TaxRegistrationType == TaxRegisrationType.VAT)
                        {
                            return  false;
                        }
                        break;
                }
            }
            return true; ;
        }
        #region IStandardAttributeDocument
        public bool ammountApplies
        {
            get { return true; }
        }

        public double documentAmount
        {
            get { return this.totalAfterTax; }
        }

        public bool hasInstrument
        {
            get { return PaymentMethodHelper.IsPaymentMethodBank(this.paymentMethod); }
        }

        public DocumentTypedReference voucherReference { get => this.paymentVoucher; set => this.paymentVoucher=value; }

        public string getInstrument(bool includeInstrucmentType)
        {
            if (includeInstrucmentType)
                return PaymentMethodHelper.GetPaymentTypeReferenceName(this.paymentMethod) + ": " + this.paymentMethodReference;
            return this.paymentMethodReference;
        }
        #endregion
    }
}
