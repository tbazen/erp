using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Xml.Serialization;
namespace BIZNET.iERP
{
    public class PurchaseTaxInfo
    {
        public string PaperRef;
        public long tranTicks; 
        public TaxImposed[] taxImposed;//
        public TaxRates taxRates;//
        public double manualVATBase;//
        public double manualWithholdBase;//
        public string withholdingReceiptNo;//
        public string vatWithholdingReceiptNo;//
        public TransactionDocumentItem[] items;
        public PurchaseTaxInfo(object purchase)
        {
            if(purchase==null)
                return;
            Type t=purchase.GetType();

            foreach(System.Reflection.FieldInfo fi in typeof(PurchaseTaxInfo).GetFields())
            {
                fi.SetValue( this,t.GetField(fi.Name).GetValue(purchase));
            }
        }
    }
    [Serializable]
    public class PurchaseInvoiceDocument : AccountDocument
    {
        public int purchaseTransactionDocumentID;
        public TaxImposed[] taxImposed;
        public TaxRates taxRates;
        public double manualVATBase;
        public double manualWithholdBase;
        public string withholdingReceiptNo;
        public string vatWithholdingReceiptNo;
        public TransactionDocumentItem[] items;

        public double totalBeforeTax
        {
            get
            {
                double ret = 0;
                foreach (TransactionDocumentItem ditem in items)
                    ret += ditem.quantity * ditem.unitPrice;
                return ret;
            }
        }
    }
    public class SellsTaxInfo
    {
        public string PaperRef;
        public long tranTicks;
        public TaxImposed[] taxImposed;//
        public TaxRates taxRates;//
        public bool doWithheldVAT;//
        public double manualVATBase;//
        public double manualWithholdBase;//
        public string withholdingReceiptNo;//
        public string withHeldVatReceiptNo;//
        public TransactionDocumentItem[] items;
        public double amount;
        public SellsTaxInfo(object sells)
        {
            if (sells == null)
                return;
            Type t = sells.GetType();

            foreach (System.Reflection.FieldInfo fi in typeof(SellsTaxInfo).GetFields())
            {
                fi.SetValue(this, t.GetField(fi.Name).GetValue(sells));
            }
        }
    }
    [Serializable]
    public class SellsInvoiceDocument : AccountDocument
    {
        public int sellsTransactionDocumentID;
        public TaxImposed[] taxImposed;
        public TaxRates taxRates;
        public bool doWithheldVAT;
        public double manualVATBase;
        public double manualWithholdBase;
        public string withholdingReceiptNo;
        public string withHeldVatReceiptNo = "";
        public TransactionDocumentItem[] items;
        

        public double totalBeforeTax
        {
            get
            {
                double ret = 0;
                foreach (TransactionDocumentItem ditem in items)
                    ret += ditem.quantity * ditem.unitPrice;
                return ret;
            }
        }
    }

}
