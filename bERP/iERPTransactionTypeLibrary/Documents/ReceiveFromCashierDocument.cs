using System;
using System.Collections.Generic;
using INTAPS.Accounting;
using System.Xml.Serialization;

namespace BIZNET.iERP
{
    [Serializable]
    public class ReceiveFromCashierDocument : AccountDocument
    {
        [DocumentTypedReferenceFieldAttribute("CHKPV", "CPV", "PCPV", "PV", "JV", "CRV", "FSN", "CSINV", "CRSINV", "GEN")]
        public DocumentTypedReference voucher = null;        

        public BizNetPaymentMethod paymentMethod;
        public int assetAccountID = -1; //cash payment type ///assetAccountID
        public int destinationBankAccID=-1;
        public double serviceChargeAmount;
        public ServiceChargePayer serviceChargePayer = ServiceChargePayer.None;
        public int employeeID;// int employeeID
        public double amount;
        public string checkNumber;
        public string slipReferenceNo;
        public string cpoNumber;
    }
    
}
