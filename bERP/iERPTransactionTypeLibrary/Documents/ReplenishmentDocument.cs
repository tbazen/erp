using System;
using System.Collections.Generic;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    [Serializable]
    public class ReplenishmentDocument : AccountDocument
    {
        [DocumentTypedReferenceFieldAttribute("CRV", "CHKPV", "CPV", "PCPV","PV", "JV", "GEN")]
        public DocumentTypedReference voucher = null;   
        public BizNetPaymentMethod paymentMethod;
        public int assetAccountID=-1; //cash payment type ///assetAccountID
        public double serviceChargeAmount;
        public ServiceChargePayer serviceChargePayer = ServiceChargePayer.None;
        public double amount;
        public string checkNumber;
        public string slipReferenceNo;
        public string cpoNumber;
    }
}
