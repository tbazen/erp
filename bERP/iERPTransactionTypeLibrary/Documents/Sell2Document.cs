﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Xml.Serialization;

namespace BIZNET.iERP
{
    [Serializable]
    public class Sell2Document:TradeTransaction
    {
        [DocumentTypedReferenceFieldAttribute("FSN", "CSINV", "CRSINV", "CSINV2", "CRSINV2", "CRV", "GEN", "JV")]
        public DocumentTypedReference salesVoucher= null;
        public bool postFromParent = false;

        public int attachmentNumber = -1;
        public bool doWithheldVAT;
        public string withHeldVatReceiptNo = "";
        public double manualWithholdBase = -1;
        public double manualVATBase = -1;
        public bool invoiced = true;
        public int[] invoiceIDs;
        [XmlIgnore]
        public CustomerReceivableDocument[] creditSettlements = new CustomerReceivableDocument[0];
        [XmlIgnore]
        public SoldItemDeliveryDocument[] deliveries=new SoldItemDeliveryDocument[0];
        [XmlIgnore]
        public SellPrePaymentDocument[] prepayments = new SellPrePaymentDocument[0];
        [XmlIgnore]
        public SellsInvoiceDocument[] invoices = new SellsInvoiceDocument[0];

        public static TaxImposed[] getTaxes(
            CompanyProfile profile
            , TradeRelation customer
            , TransactionDocumentItem[] docItems
            , TransactionItems[] items
            , TaxRates rates
            , bool isSaleTaxBaseCustom, double customSaleTaxBase
            , bool isWHBaseCustom, double customWHBase
            , out double totalSale, out double totalTaxable
            , out bool salesTaxApplies, out bool whApplies
            )
        {

            Dictionary<TaxType, TaxImposed> taxList = new Dictionary<TaxType, TaxImposed>();
            totalSale = 0;
            totalTaxable = 0;
            int i = 0;
            GoodOrService goodOrService = GoodOrService.Unknown;
            bool mixedGoodAndService = false;
            salesTaxApplies = false;
            whApplies = false;
            //totals
            foreach (TransactionItems item in items)
            {
                TransactionDocumentItem ditem = docItems[i];
                totalSale += ditem.price;

                if (item.TaxStatus == ItemTaxStatus.Taxable && !ditem.remitted)
                {
                    if (goodOrService == GoodOrService.Unknown)
                        goodOrService = item.GoodOrService;
                    else
                        if (!mixedGoodAndService && goodOrService != item.GoodOrService)
                        {
                            mixedGoodAndService = true;
                            if (isSaleTaxBaseCustom)
                                throw new INTAPS.ClientServer.ServerUserMessage("Good and service items can't be mixed when sales tax is provided manually");
                        }
                    totalTaxable += ditem.price;


                    switch (profile.TaxRegistrationType)
                    {

                        case TaxRegisrationType.TOT:
                        case TaxRegisrationType.VAT:
                            TaxType tt;
                            double salesTaxRate;
                            if (profile.TaxRegistrationType == TaxRegisrationType.TOT)
                            {
                                tt = TaxType.TOT;
                                salesTaxRate = GetTOTCalculationRate(item.GoodOrService, item.ServiceType, rates);
                            }
                            else
                            {
                                tt = TaxType.VAT;
                                salesTaxRate = rates.CommonVATRate;
                            }
                            if (isSaleTaxBaseCustom)
                            {
                                if (taxList.ContainsKey(tt))
                                    if (!AccountBase.AmountEqual(taxList[tt].rate, salesTaxRate))
                                    {
                                        throw new INTAPS.ClientServer.ServerUserMessage("Mixed sales tax rate items can't be used with manually set sales tax base");
                                    }
                            }
                            addTax(taxList, new TaxImposed(tt, ditem.price, salesTaxRate * ditem.price));
                            if (tt == TaxType.VAT)
                            {
                                if (customer.IsVatAgent)
                                    addTax(taxList, new TaxImposed(TaxType.VATWitholding, ditem.price, -salesTaxRate * ditem.price));
                            }
                            break;
                        case TaxRegisrationType.NonTaxPayer:
                            break;
                        default:
                            throw new INTAPS.ClientServer.ServerUserMessage("Unsupported tax registration type:" + profile.TaxRegistrationType);
                    }
                }
                i++;
            }

            //taxes
            if (profile.TaxRegistrationType != TaxRegisrationType.NonTaxPayer)
            {
                TaxType tt;
                if (profile.TaxRegistrationType == TaxRegisrationType.TOT)
                    tt = TaxType.TOT;
                else if (profile.TaxRegistrationType == TaxRegisrationType.VAT)
                    tt = TaxType.VAT;
                else
                    throw new INTAPS.ClientServer.ServerUserMessage("Unsupported tax registration type:" + profile.TaxRegistrationType);
                salesTaxApplies = taxList.ContainsKey(tt);
                if (salesTaxApplies && isSaleTaxBaseCustom)
                {
                    TaxImposed ti = taxList[tt];
                    if (AccountBase.AmountGreater(customSaleTaxBase, totalTaxable))
                    {
                        throw new INTAPS.ClientServer.ServerUserMessage("Manually provided sales tax base can't be greater than the total value of items invoiced");
                    }
                    ti.TaxValue = ti.rate * customSaleTaxBase;
                    ti.TaxBaseValue = customSaleTaxBase;
                    setTax(taxList, ti);

                    if (tt == TaxType.VAT && customer.IsVatAgent)
                    {
                        ti = taxList[TaxType.VATWitholding];
                        ti.TaxValue = ti.rate * customSaleTaxBase;
                        ti.TaxBaseValue = customSaleTaxBase;
                        setTax(taxList, ti);
                    }
                }

                if (customer.IsWithholdingAgent)
                {
                    double whRate = GetWithHoldingCalculationRate(true, totalTaxable, goodOrService, mixedGoodAndService, rates);
                    whApplies = !AccountBase.AmountEqual(whRate, 0);

                    if (whApplies)
                    {
                        double withholdingBase = isWHBaseCustom ? customWHBase : totalTaxable;
                        if (!AccountBase.AmountEqual(withholdingBase, 0))
                        {
                            whRate = GetWithHoldingCalculationRate(true, withholdingBase, goodOrService, mixedGoodAndService, rates);
                            addTax(taxList, new TaxImposed(TaxType.WithHoldingTax, withholdingBase,
                                 -withholdingBase * GetWithHoldingCalculationRate(true, withholdingBase, goodOrService, mixedGoodAndService, rates)));
                        }
                    }
                }
            }
            TaxImposed[] ret = new TaxImposed[taxList.Count];
            taxList.Values.CopyTo(ret, 0);
            return ret;
        }

        public List<int> getAttachmentArray()
        {
            List<int> ret = new List<int>();
            int[][] docIDs = new int[][] { this.deliverieIDs, this.prepaymentsIDs, this.creditSettlementIDs };
            foreach (int[] ar in docIDs)
                ret.AddRange(ar);
            return ret;
        }
        public double netPayment
        {
            get
            {
                return totalBeforeTax+ totalTax - settleAdvancePayment - supplierCredit - retention;
            }
        }
    }
}
