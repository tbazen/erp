using System;
using System.Collections.Generic;
using INTAPS.Accounting;
using System.Xml.Serialization;

namespace BIZNET.iERP
{
    [Serializable]
    public class StaffTransactionDocument : AccountDocument
    {

        private BizNetPaymentMethod _paymentMethod;
        private int _assetAccountID = -1; //cash payment type ///assetAccountID
        private double _serviceChargeAmount;
        private ServiceChargePayer _serviceChargePayer = ServiceChargePayer.None;
        public int employeeID;// int employeeID
        public double amount;
        public string checkNumber;
        public string slipReferenceNo;
        public string cpoNumber;


        public string paymentMethodReference
        {
            get
            {
                switch (paymentMethod)
                {
                    case BizNetPaymentMethod.Cash:
                        return "";
                    case BizNetPaymentMethod.Check:
                        return checkNumber;
                    case BizNetPaymentMethod.CPOFromCash:
                        return cpoNumber;
                    case BizNetPaymentMethod.CPOFromBankAccount:
                        return cpoNumber;
                    case BizNetPaymentMethod.BankTransferFromCash:
                        return slipReferenceNo;
                    case BizNetPaymentMethod.BankTransferFromBankAccount:
                        return slipReferenceNo;
                    case BizNetPaymentMethod.Credit:
                        return "";
                    case BizNetPaymentMethod.None:
                        return "";
                    default:
                        return "";
                }
                
            }
            set
            {
                switch (paymentMethod)
                {
                    case BizNetPaymentMethod.Check:
                          this.checkNumber=value;
                        break;
                    case BizNetPaymentMethod.CPOFromCash:
                        this.cpoNumber=value;
                        break;
                    case BizNetPaymentMethod.CPOFromBankAccount:
                        this.cpoNumber = value;
                        break;
                    case BizNetPaymentMethod.BankTransferFromCash:
                        this.slipReferenceNo = value;
                        break;
                    case BizNetPaymentMethod.BankTransferFromBankAccount:
                        this.slipReferenceNo = value;
                        break;
                }
            }
        }
        #region IStandardAttributeDocument
        public bool ammountApplies
        {
            get { return true; }
        }

        public double documentAmount
        {
            get { return this.amount; }
        }

        public bool hasInstrument
        {
            get { return PaymentMethodHelper.IsPaymentMethodBank(this.paymentMethod); }
        }

        public BizNetPaymentMethod paymentMethod { get => _paymentMethod; set => _paymentMethod = value; }
        public int assetAccountID { get => _assetAccountID; set => _assetAccountID = value; }
        public double serviceChargeAmount { get => _serviceChargeAmount; set => _serviceChargeAmount = value; }
        public ServiceChargePayer serviceChargePayer { get => _serviceChargePayer; set => _serviceChargePayer = value; }
        public double netPayment { get => this.amount; set => this.amount=value; }

        public string getInstrument(bool includeInstrucmentType)
        {
            if (includeInstrucmentType)
                return PaymentMethodHelper.GetPaymentTypeReferenceName(this.paymentMethod) + ": " + this.paymentMethodReference;
            return this.paymentMethodReference;
        }
        #endregion
    }
    [Serializable]
    public class StaffReceivableAccountDocument : StaffTransactionDocument,IStandardAttributeDocument
    {
        [DocumentTypedReferenceFieldAttribute("CHKPV", "CPV", "PCPV", "PV", "JV","GEN")]
        public DocumentTypedReference voucher = null;        
        public int periodID;
        public double monthlyReturn;
    }
    [Serializable]
    [XmlInclude(typeof(StaffPerDiemDocument))]
    [XmlInclude(typeof(Purchase2Document))]
    [XmlInclude(typeof(ExpenseAdvanceReturnDocument))]
    public class ExpenseAdvanceDocument : StaffReceivableAccountDocument, IStandardAttributeDocument,IPaymentDocument
    {
        public AccountDocument[] schedules;
        public DocumentTypedReference voucherReference { get => this.voucher; set => this.voucher=value; }

    }

    [Serializable]
    public class StaffLoandDocumentBase : StaffReceivableAccountDocument
    {
        [Serializable]
        public class LoanReturnItem
        {
            public int periodID;
            public double returnAmount;
        }
        public delegate int getNextPeriodIDelegate(int periodID);
        
        public INTAPS.Payroll.PayrollComponentData[] data;
        public LoanReturnItem[] returnExceptions = new LoanReturnItem[0];

        public int[] directReturnDocumentID=new int[0];
        public bool closed = false;
        public LoanReturnItem[] getReturnSchedule(getNextPeriodIDelegate getNextPeriodID,double directReturn)
        {
            List<LoanReturnItem> ret = new List<LoanReturnItem>();
            double totalLoan = this.amount-directReturn;
            int returnPeriodID = this.periodID;
            while (AccountBase.AmountGreater(totalLoan, 0))
            {
                double returnAmount=0;
                bool regular=true;
                foreach (LoanReturnItem item in returnExceptions)
                {
                    if (item.periodID == returnPeriodID)
                    {
                        returnAmount = item.returnAmount;
                        regular = false;
                        break;
                    }
                }
                if (regular)
                {
                    returnAmount = this.monthlyReturn;
                }
                if (AccountBase.AmountGreater(returnAmount, totalLoan))
                    returnAmount = totalLoan;
                LoanReturnItem ri = new LoanReturnItem();
                ri.periodID = returnPeriodID;
                ri.returnAmount = returnAmount;
                ret.Add(ri);
                totalLoan -= returnAmount;
                returnPeriodID = getNextPeriodID(returnPeriodID);
                if (returnPeriodID == -1)
                    break;
            }
            return ret.ToArray();
        }
    }
    [Serializable]
    public class ShortTermLoanDocument : StaffLoandDocumentBase, IStandardAttributeDocument
    {

    }
    [Serializable]
    public class LongTermLoanDocument : StaffLoandDocumentBase, IStandardAttributeDocument
    {

    }
    [Serializable]
    public class UnclaimedSalaryDocument : StaffReceivableAccountDocument
    {
       // public int periodID; //period of payroll payment
    }
    [Serializable]
    public class StaffLoanPaymentDocument : StaffReceivableAccountDocument, IStandardAttributeDocument
    {

    }
    [Serializable]
    public class StaffPerDiemDocument : StaffReceivableAccountDocument,IStandardAttributeDocument
    {
        public DateTime leaveDate;
        public DateTime returnDate;
        public double dayAdjustment;
        public double unitPerdiem;
        public string placeOfDestination;
        public int costCenterID;

    }
    [Serializable]
    public class ShareHoldersLoanDocument:StaffReceivableAccountDocument
    {
        
    }

}
