using System;
using System.Collections.Generic;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    [Serializable]
    public class StaffReturnDocument : StaffTransactionDocument
    {
        [DocumentTypedReferenceFieldAttribute("CRV","JV", "GEN")]
        public DocumentTypedReference voucher = null;
        public int loanDocumentID = -1;
    }
    [Serializable]
    public class ExpenseAdvanceReturnDocument : StaffReturnDocument
    {
      
    }
    [Serializable]
    public class StaffLoanReturnDocument : StaffReturnDocument
    {
        public int loadDocumentID = -1;
    }
    [Serializable]
    public class ShortTermLoanReturnDocument : StaffLoanReturnDocument
    {

    }
    [Serializable]
    public class LongTermLoanReturnDocument : StaffLoanReturnDocument
    {

    }
    [Serializable]
    public class LoanFromStaffDocument : StaffReturnDocument
    {

    }
    [Serializable]
    public class ShareHoldersLoanReturnDocument:StaffReturnDocument,IStandardAttributeDocument
    {
        
    }

}
