using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Xml.Serialization;

namespace BIZNET.iERP
{
    [Serializable]
    public class SupplierAccountDocument:AccountDocument
    {
        [DocumentTypedReferenceFieldAttribute("CHKPV", "CPV", "PCPV", "PV", "JV", "CRV", "GEN")]
        public DocumentTypedReference voucher = null;
        public BizNetPaymentMethod paymentMethod;
        public int assetAccountID = -1; //cash payment type ///assetAccountID
        public double serviceChargeAmount;
        public ServiceChargePayer serviceChargePayer = ServiceChargePayer.None;
        public double amount;
        public string supplierCode;
        public string checkNumber;
        public string slipReferenceNo;
        public string cpoNumber;

        public string paymentMethodReference
        {
            get
            {
                switch (paymentMethod)
                {
                    case BizNetPaymentMethod.Cash:
                        return "";
                    case BizNetPaymentMethod.Check:
                        return checkNumber;
                    case BizNetPaymentMethod.CPOFromCash:
                        return cpoNumber;
                    case BizNetPaymentMethod.CPOFromBankAccount:
                        return cpoNumber;
                    case BizNetPaymentMethod.BankTransferFromCash:
                        return slipReferenceNo;
                    case BizNetPaymentMethod.BankTransferFromBankAccount:
                        return slipReferenceNo;
                    case BizNetPaymentMethod.Credit:
                        return "";
                    case BizNetPaymentMethod.None:
                        return "";
                    default:
                        return "";
                }
            }
        }
        #region IStandardAttributeDocument
        public bool ammountApplies
        {
            get { return true; }
        }

        public double documentAmount
        {
            get { return this.amount; }
        }

        public bool hasInstrument
        {
            get { return PaymentMethodHelper.IsPaymentMethodBank(this.paymentMethod); }
        }

        public string getInstrument(bool includeInstrucmentType)
        {
            if (includeInstrucmentType)
                return PaymentMethodHelper.GetPaymentTypeReferenceName(this.paymentMethod) + ": " + this.paymentMethodReference;
            return this.paymentMethodReference;
        }
        #endregion
    }
    /// <summary>
    /// TradeRelation Advance Payment Document
    /// </summary>
    [Serializable]
    public class SupplierReceivableDocument : SupplierAccountDocument,IStandardAttributeDocument
    {
     
    }
    [Serializable]
    public class SupplierPayableDocument : SupplierAccountDocument
    {
        public bool retention=false;
    }
    [Serializable]
    public class SupplierAdvanceReturnDocument:SupplierAccountDocument
    {
        
    }
}
