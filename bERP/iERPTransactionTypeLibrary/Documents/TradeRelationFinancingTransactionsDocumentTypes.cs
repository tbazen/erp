﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    [Serializable]
    public class SupplierAdvancePayment2Document:Purchase2Document
    {
    }
    [Serializable]
    public class SupplierRetentionSettlementDocument : Purchase2Document,IStandardAttributeDocument
    {
    }
    [Serializable]
    public class SupplierCreditSettlement2Document : Purchase2Document, IStandardAttributeDocument
    {
    }
    [Serializable]
    public class SupplierAdvanceReturn2Document : Sell2Document
    {
    }

    [Serializable]
    public class CustomerAdvancePayment2Document : Sell2Document
    {
    }
    [Serializable]
    public class CustomerRetentionSettlementDocument : Sell2Document
    {

    }
    [Serializable]
    public class CustomerCreditSettlement2Document : Sell2Document
    {
    }
    [Serializable]
    public class CustomerAdvanceReturn2Document : Purchase2Document
    {
    }
}
