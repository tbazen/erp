using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Xml.Serialization;


namespace BIZNET.iERP
{
    [Serializable]
    public class ZReportDocument : AccountDocument
    {
        [DocumentTypedReferenceFieldAttribute("SZR","SFRR","JV")] // Session Z Report, Summary Fiscal Reading Report 
        public DocumentTypedReference voucher = null;        

        public BizNetPaymentMethod paymentMethod;
        public int assetAccountID = -1;
        public double serviceChargeAmount;
        public ServiceChargePayer serviceChargePayer = ServiceChargePayer.None;

        public double grandTotal; //Grand Total
        public TaxImposed taxImposed; //various taxes imposed on the sales document
        public double nonTaxableAmount;

        public double salesTaxableAmount;
        public double salesTax;
        public double salesNonTaxableAmount;

        public string checkNumber;
        public string cpoNumber;
        public string slipReferenceNo;

        public double NetTaxableAmount
        {
            get
            {
                if (AccountBase.AmountGreater(salesTaxableAmount - taxImposed.TaxBaseValue, 0))
                    return salesTaxableAmount - taxImposed.TaxBaseValue;
                else
                    return -1 * (salesTaxableAmount - taxImposed.TaxBaseValue);
            }
        }
        public double NetNonTaxableAmount
        {
            get
            {
                if (AccountBase.AmountGreater(salesNonTaxableAmount - nonTaxableAmount, 0))
                    return salesNonTaxableAmount - nonTaxableAmount;
                else
                    return -1 * (salesNonTaxableAmount - nonTaxableAmount);
            }
        }
        public double NetTaxAmount
        {
            get
            {
                if (AccountBase.AmountGreater(salesTax - taxImposed.TaxValue, 0))
                    return salesTax - taxImposed.TaxValue;
                else
                    return -1 * (salesTax - taxImposed.TaxValue);
            }
        }
    }
}
