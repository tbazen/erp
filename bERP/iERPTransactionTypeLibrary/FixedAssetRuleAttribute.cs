﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;

namespace BIZNET.iERP
{
    [Serializable]   
    public class FixedAssetRuleAttribute:Attribute
    {
        public int id;
        public string name;
        public bool hasConfiguration;
        public FixedAssetRuleAttribute(int id, string name,bool hasConfiguration)
        {
            this.id = id;
            this.name = name;
            this.hasConfiguration = hasConfiguration;
        }
        public override string ToString()
        {
            return name;
        }
    }

    [Serializable]
    [SingleTableObject]
    public class Property_Vehicle
    {
        [IDField]
        public int propertyID;
        [DataField]
        public string make;
        [DataField]
        public string model;
        [DataField]
        public string plateNo;
        [DataField]
        public string chasisNo;
        [DataField]
        public string engineNo;
    }

    [Serializable]
    [SingleTableObject]
    public class Property_General
    {
        [IDField]
        public int propertyID;
        [DataField]
        public string make;
        [DataField]
        public string model;
        [DataField]
        public string serialNo;
    }
}
