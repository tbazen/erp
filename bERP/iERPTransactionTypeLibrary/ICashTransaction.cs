﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    public interface ICashTransaction
    {
        List<CashFlowItem> getCashFlowItems(AccountDocument doc, DetailedCashFlowReportOption option,AccountTransaction[] docTrans);
        string getInstrumentNo(AccountDocument doc,out string typeName);
        
    }
}
