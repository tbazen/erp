﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIZNET.iERP
{
    [Serializable]
    public class InventoryGeneratorParameters
    {
        public int costCenterID;
        public DateTime to;
        public bool seprateOrder=true;
        public bool includeTitle=true;
        public bool showBelowMinStock = false;
        public bool showAboveMaxStock = false;
        public bool filterAbnormalStockLevel= false;
        public bool showInventoryRunTime = false;
        public double inventoryRunTimeMultiplier = 1;
        public string inventoryRunTimeUnit = "day";
        public int nCols
        {
            get
            {
                int ret=3;
                if(showNormalLevelColumn)
                    ret++;
                if (showInventoryRunTime)
                    ret++;
                return ret;
            }
        }
        public bool showNormalLevelColumn
        {
            get
            {
                return showAboveMaxStock || showBelowMinStock;
            }
        }

    }
}
