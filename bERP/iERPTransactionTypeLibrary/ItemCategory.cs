using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;
using INTAPS.Accounting;
namespace BIZNET.iERP
{
    [Serializable]
    [SingleTableObject(orderBy = "Code")]
    public class ItemCategory
    {
        [IDField]
        public int ID;
        [DataField]
        public string Code;
        [DataField]
        public int PID = -1;
        [DataField]
        public string description;
        
        [AIParentAccountField(name="Expense Account",groups=new string[]{"EXP"},displayOrder="000")]
        [DataField]
        public int expenseAccountPID = -1;

        [AIParentAccountField(name = "Direct Cost Account", groups = new string[] { "EXP" }, displayOrder = "010")]
        [DataField]
        public int directCostAccountPID = -1;

        [AIParentAccountField(name = "Prepaid Expense Account", groups = new string[] { "EXP" }, displayOrder = "020")]
        [DataField]
        public int prePaidExpenseAccountPID = -1;

        [AIParentAccountField(name = "Income Account", groups = new string[] { "SLS" }, displayOrder = "030")]
        [DataField]
        public int salesAccountPID = -1;

        [AIParentAccountField(name = "Unearned Revenue Account", groups = new string[] { "SLS" }, displayOrder = "040")]
        [DataField]
        public int unearnedRevenueAccountPID = -1;

        [AIParentAccountField(name = "Finished Work Account", groups = new string[] { "SLS" }, displayOrder = "050")]
        [DataField]
        public int finishedWorkAccountPID = -1; //finished services

        [AIParentAccountField(name = "Inventory Account", groups = new string[] { "INV", "FAS" }, displayOrder = "060")]
        [DataField]
        public int generalInventoryAccountPID = -1;

        [AIParentAccountField(name = "Finished Good Account", groups = new string[] { "INV", "FAS" }, displayOrder = "070")]
        [DataField]
        public int finishedGoodsAccountPID = -1;


        [AIParentAccountField(name = "Original Fixed Asset Value Account", groups = new string[] { "FAS" }, displayOrder = "080")]
        [DataField]
        public int originalFixedAssetAccountPID = -1;

        [AIParentAccountField(name = "Depreciation Account", groups = new string[] { "FAS" }, displayOrder = "090")]
        [DataField]
        public int depreciationAccountPID = -1; //Expense account

        [AIParentAccountField(name = "Accumulated Depreciation Account", groups = new string[] { "FAS" }, displayOrder = "100")]
        [DataField]
        public int accumulatedDepreciationAccountPID = -1; //Asset account



        [AIParentAccountField(name = "Pending Order Account", groups = new string[] { "INV", "FAS" }, displayOrder = "110")]
        [DataField]
        public int pendingOrderAccountPID = -1;

        [AIParentAccountField(name = "Pending Delivery Account", groups = new string[] { "INV", "FAS" }, displayOrder = "120")]
        [DataField]
        public int pendingDeliveryAcountPID = -1;
        
        //summary accounts follow
        [AIParentAccountField(false, false, name = "Summerized Expense Account", groups = new string[] { "EXP" }, displayOrder = "001", isSummary = true)]
        [DataField]
        public int expenseSummaryAccountID = -1;

        [AIParentAccountField(false, false, name = "Summerized Direct Cost Account", groups = new string[] { "EXP" }, displayOrder = "011", isSummary = true)]
        [DataField]
        public int directCostSummaryAccountID = -1;

        [AIParentAccountField(false, false, name = "Summerized Prepad Expense Account", groups = new string[] { "EXP" }, displayOrder = "021", isSummary = true)]
        [DataField]
        public int prePaidExpenseSummaryAccountID = -1;

        [AIParentAccountField(false, false, name = "Summerized Income Account", groups = new string[] { "SLS" }, displayOrder = "031", isSummary = true)]
        [DataField]
        public int salesSummaryAccountID = -1;

        [AIParentAccountField(false, false, name = "Summerized Unearned Reveue Account", groups = new string[] { "SLS" }, displayOrder = "041", isSummary = true)]
        [DataField]
        public int unearnedSummaryAccountID = -1;

        [AIParentAccountField(false, false, name = "Summerized Finished Work Account", groups = new string[] { "SLS" }, displayOrder = "051", isSummary = true)]
        [DataField]
        public int finishedWorkSummaryAccountID = -1; //finished services

        [AIParentAccountField(false, false, name = "Summerized Inventory Account", groups = new string[] { "INV", "FAS" }, displayOrder = "061",isSummary=true)]
        [DataField]
        public int generalInventorySummaryAccountID = -1;

        [AIParentAccountField(false, false, name = "Summerized Finished Good Account", groups = new string[] { "INV", "FAS" }, displayOrder = "071", isSummary = true)]
        [DataField]
        public int finishedGoodsSummaryAccountID = -1;

        [AIParentAccountField(false, false, name = "Summerized Original Fixed Asset Account", groups = new string[] { "FAS" }, displayOrder = "081", isSummary = true)]
        [DataField]
        public int originalFixedAssetSummaryAccountID = -1;

        [AIParentAccountField(false, false, name = "Summerized Fixed Asset Depreciation Account", groups = new string[] { "FAS" }, displayOrder = "091", isSummary = true)]
        [DataField]
        public int depreciationSummaryAccountID = -1; //Expense account

        [AIParentAccountField(false, false, name = "Summerized Fixed Asset Accumulated Depreciation Account", groups = new string[] { "FAS" }, displayOrder = "101", isSummary = true)]
        [DataField]
        public int accumulatedDepreciationSummaryAccountID = -1; //Asset account


        [AIParentAccountField(false, false, name = "Summerized Pending Order Account", groups = new string[] { "INV", "FAS" }, displayOrder = "111", isSummary = true)]
        [DataField]
        public int pendingOrderSummaryAccountID = -1;

        [AIParentAccountField(false, false, name = "Summerized Pending Delivery Account", groups = new string[] { "INV", "FAS" }, displayOrder = "121", isSummary = true)]
        [DataField]
        public int pendingDeliverySummaryAccountID = -1;



        public List<string> getApplicableAcountFields(TransactionItems item)
        {
            List<string> ret = new List<string>();
            if (item.IsExpenseItem)
            {
                if (item.IsDirectCost)
                {
                    ret.Add("directCostAccountPID");
                    ret.Add("directCostSummaryAccountID");
                }
                else
                {
                    ret.Add("expenseAccountPID");
                    ret.Add("expenseSummaryAccountID");
                }
                ret.Add("prePaidExpenseAccountPID");
                ret.Add("prePaidExpenseSummaryAccountID");
            }
            if (item.IsSalesItem)
            {
                ret.Add("salesAccountPID");
                ret.Add("unearnedRevenueAccountPID");
                ret.Add("salesSummaryAccountID");
                ret.Add("unearnedRevenueSummaryAccountID");
                if (item.GoodOrService == GoodOrService.Service)
                {
                    ret.Add("finishedWorkAccountPID");
                    ret.Add("finishedWorkSummaryAccountID");
                }
            }
            if (item.IsFixedAssetItem)
            {
                ret.Add("originalFixedAssetAccountPID");
                ret.Add("accumulatedDepreciationAccountPID");
                ret.Add("depreciationAccountPID");

                ret.Add("pendingOrderAccountPID");
                ret.Add("pendingDeliveryAcountPID");

                ret.Add("originalFixedAssetSummaryAccountID");
                ret.Add("accumulatedDepreciationSummaryAccountID");
                ret.Add("depreciationSummaryAccountID");
                ret.Add("pendingOrderSummaryAccountID");
                ret.Add("pendingDeliverySummaryAccountID");
                if (item.inventoryType == InventoryType.GeneralInventory)
                {
                    ret.Add("generalInventoryAccountPID");
                    ret.Add("generalInventorySummaryAccountID");
                }
                else
                {
                    ret.Add("finishedGoodsAccountPID");
                    ret.Add("finishedGoodsSummaryAccountID");
                }
            }

            if (item.IsInventoryItem)
            {
                if (item.inventoryType == InventoryType.GeneralInventory)
                {
                    ret.Add("generalInventoryAccountPID");
                    ret.Add("generalInventorySummaryAccountID");
                }
                else
                {
                    ret.Add("finishedGoodsAccountPID");
                    ret.Add("finishedGoodsSummaryAccountID");
                }
                ret.Add("pendingOrderAccountPID");
                ret.Add("pendingOrderSummaryAccountID");
                ret.Add("pendingDeliveryAcountPID");
                ret.Add("pendingDeliverySummaryAccountID");
            }
            return ret;
        }

        public string NameCode { get { return description + " - " + Code; } }
    }
}
