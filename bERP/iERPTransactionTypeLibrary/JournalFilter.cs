﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    
    [Serializable]
    public class JournalFilter
    {
        public TransactionJournalField sortBy = TransactionJournalField.Date;
        public DateTime time1;
        public DateTime time2;
        public DocumentTypedReference[] expand = null;
        public int[] refTypes = null;
        public int[] docTypes = null;
        public DocumentTypedReference[] specificReferences = null;
        public string textFilter = null;
        public JournalFilter()
        {

        }
        public JournalFilter(DateTime time1,
                             DateTime time2,
                             DocumentTypedReference[] expand,
                             int[] refTypes,
                             int[] docTypes,
            string textFilter,
            TransactionJournalField sortBy)
        {
            this.sortBy = sortBy;
            this.time1 = time1;
            this.time2 = time2;
            this.expand = expand;
            this.refTypes = refTypes;
            this.docTypes = docTypes;
            this.specificReferences = null;
            this.textFilter = textFilter;
        }
        public JournalFilter(DocumentTypedReference[] specificReferences, DocumentTypedReference[] expand)
        {
            this.specificReferences = specificReferences;
            this.expand = expand;
        }
    }
}
