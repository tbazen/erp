using System;
using System.Collections.Generic;
using INTAPS.RDBMS;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    public enum ItemTaxStatus
    {
        Taxable = 1, //VAT Taxable or TOT Taxable
        NonTaxable = 2 //VAT Non-Taxable or TOT Non-Taxable
    }
    public enum GoodOrService
    {
        Good = 1,
        Service = 2,
        Unknown=3
    }
    public enum TaxCategory
    {
        VAT,
        TOT
    }
    public enum ServiceType
    {
        None = 0, //If service is selected 10% TOT (Other services)
        Contractors = 1, //2% TOT
        MillServices = 2, //2% TOT
        TractorsAndCombinedHarvesters = 3     //2% TOT
    }
    public enum GoodType
    {
        None = 0,
        RawHide = 1 //For Export-> 150% VAT
    }
    public enum PurchaseType
    {
        LocalPurchase,
        InternationalPurchase
    }
    public enum SalesType
    {
        Export,
        LocalSales
    }
    public struct PurchaseAndSalesData
    {
        public PurchaseType purchaseType;
        public string agentCode;
        public BizNetPaymentMethod paymentMethod;
        public int assetAccountID;
        public ItemTransactionType itemTransactionType;
        public int ReceivableAccountID; //TradeRelation advance payment
        public int PayableAccountID; //TradeRelation advance payment
        public SalesType SalesType;
    }
    [Serializable]
    public class TaxImposed:IEquatable<TaxImposed>
    {
        public TaxType TaxType;
        public double TaxValue;
        public double TaxBaseValue;
        public TaxImposed()
        {
        }

        public TaxImposed(iERP.TaxType taxType, double baseValue, double tax)
        {
            this.TaxType = taxType;
            this.TaxBaseValue = baseValue;
            this.TaxValue = tax;
        }
        
        public static string TaxTypeName(TaxType type)
        {
            switch (type)
            {
                case TaxType.VAT:
                    return "VAT";
                case TaxType.TOT:
                    return "TOT";
                case TaxType.WithHoldingTax:
                    return "Withholding Tax";
                case TaxType.CustomDutyTax:
                    return "Customs Duty";
                case TaxType.ExciseTax:
                    return "Excise Tax";
                case TaxType.SurTax:
                    return "Sur Tax";
                case TaxType.VATWitholding:
                    return "Witheld VAT";
                default:
                    return "Unknown";
            }
        }

        public bool Equals(TaxImposed other)
        {
            return other.TaxType == this.TaxType && INTAPS.Accounting.AccountBase.AmountEqual( other.TaxBaseValue, this.TaxBaseValue)
                && INTAPS.Accounting.AccountBase.AmountEqual(other.TaxValue ,this.TaxValue);
        }

        public double rate
        {
            get
            {
                if (INTAPS.Accounting.AccountBase.AmountEqual(TaxBaseValue, 0))
                    throw new INTAPS.ClientServer.ServerUserMessage("Undefined tax rate");
                return TaxValue / TaxBaseValue;
            }
        }

        public static bool equal(TaxImposed[] x, TaxImposed[] y)
        {
            if (x == null && y != null)
                return false;
            if (x != null && y == null)
                return false;
            if (x == null && y == null)
                return true;
            if (x.Length != y.Length)
                return false;
            foreach (TaxImposed xx in x)
            {
                bool found = false;
                foreach (TaxImposed yy in y)
                {
                    if (xx.TaxType == yy.TaxType)
                    {
                        if (!AccountBase.AmountEqual(xx.TaxValue, yy.TaxValue))
                            return false;
                        found = true;
                        break;
                    }
                }
                if (!found)
                    return false;
            }
            return true;
        }
    }
    public enum TaxType
    {
        VAT,
        TOT,
        WithHoldingTax,
        CustomDutyTax,
        ExciseTax,
        SurTax,
        VATWitholding
    }
    public enum ItemTransactionType
    {
        Purchase,
        Sales
    }
    public enum StaffPaymentListType
    {
        ExpenseAdvanceList,
        SalaryAdvanceList,
        LongTermLoanList,
        LongTermLoanWithServiceChargeList,
        UnclaimedSalaryList,
        StaffLoanPayment,
        PerDiemPayment,
        ShareHoldersLoanList
    }
    public enum StaffReturnListType
    {
        ExpenseAdvanceReturnList,
        ShortTermLoanReturnList,
        LongTermLoanReturnList,
        LoanFromStaff,
        ShareHoldersLoanReturnList
    }
    public enum SupplierListType
    {
        AdvanceList,
        PayableList,
        AdvanceReturnList,
    }
    public enum CustomerListType
    {
        PayableList,
        CreditList,
        AdvanceReturnList,
        BondPaymentList,
        BondReturnList
    }
    public enum AccountType
    {
        Cash,
        Bank,
        None
    }
    [Serializable]
    public class TaxRates
    {
        public double CommonVATRate;
        public double VATWithholdingNonTaxablePriceLimit;

        public double GoodsTaxableRate;
        public ServicesTaxable ServicesTaxable;

        public double WHGoodsNonTaxablePriceLimit;
        public double WHGoodsTaxableRate;

        public double WHServicesNonTaxablePriceLimit;
        public double WHServicesTaxableRate;

        public double UnknownTINWithHoldingRate;
    }
    public enum EmployeeWorkValidationKind
    {
        DayHour,
        MonthHour,
        Day
    }

    [Serializable]
    [SingleTableObject]
    public class TaxCenter
    {
        [IDField]
        public int ID;
        [DataField]
        public string Name;

        public override string ToString()
        {
            return Name;
        }
    }

    public enum ItemType
    {
        Expense,
        Sales,
        FixedAsset
    }
}
