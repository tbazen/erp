using System;
using System.Collections.Generic;

namespace BIZNET.iERP
{
    public enum BizNetPaymentMethod
    {
        Cash,
        Check,
        CPOFromCash,
        CPOFromBankAccount,
        BankTransferFromCash,
        BankTransferFromBankAccount,
        Credit,
        None
    }
    public enum ServiceChargePayer
    {
        Company,
        Employee,
        None
    }
    public static class PaymentMethodHelper
    {
        public static string GetPaymentTypeText(BizNetPaymentMethod method)
        {
            switch (method)
            {
                case BizNetPaymentMethod.Cash:
                    return "Cash";
                case BizNetPaymentMethod.Check:
                    return "Check";
                case BizNetPaymentMethod.CPOFromCash:
                    return "CPO from cash";
                case BizNetPaymentMethod.CPOFromBankAccount:
                    return "CPO from bank account";
                case BizNetPaymentMethod.BankTransferFromCash:
                    return "Bank transfer from cash";
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    return "Bank transfer from bank account";
                case BizNetPaymentMethod.Credit:
                    return "Credit";
                default:
                    return "Unknown";
            }
        }
        public static bool IsPaymentMethodCash(BizNetPaymentMethod method)
        {
            switch (method)
            {
                case BizNetPaymentMethod.Cash:
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.BankTransferFromCash:
                    return true;
            }
            return false;
        }
        public static bool IsPaymentMethodCredit(BizNetPaymentMethod method)
        {
            switch (method)
            {
                case BizNetPaymentMethod.Credit:
                    return true;
            }
            return false;
        }
        public static bool IsPaymentMethodWithReference(BizNetPaymentMethod method)
        {
            switch (method)
            {
                case BizNetPaymentMethod.Credit:
                case BizNetPaymentMethod.Cash:
                case BizNetPaymentMethod.None:
                    return false;
            }
            return true;
        }
        public static bool IsPaymentMethodBank(BizNetPaymentMethod method)
        {
            switch (method)
            {
                case BizNetPaymentMethod.Check:
                case BizNetPaymentMethod.CPOFromBankAccount:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    return true;
            }
            return false;
        }
        public static bool IsPaymentMethodCPO(BizNetPaymentMethod method)
        {
            switch (method)
            {
                case BizNetPaymentMethod.CPOFromBankAccount:
                case BizNetPaymentMethod.CPOFromCash:
                    return true;
            }
            return false;
        }
        public static bool IsPaymentMethodTransfer(BizNetPaymentMethod method)
        {
            switch (method)
            {
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                case BizNetPaymentMethod.BankTransferFromCash:
                    return true;
            }
            return false;
        }
        public static bool IsPaymentMethodWithServiceCharge(BizNetPaymentMethod method)
        {
            switch (method)
            {
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.CPOFromBankAccount:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                case BizNetPaymentMethod.BankTransferFromCash:
                    return true;
            }
            return false;
        }
        public static string GetPaymentTypeReferenceName(BizNetPaymentMethod method)
        {
            switch (method)
            {
                case BizNetPaymentMethod.Check:
                    return "Check Number";
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.CPOFromBankAccount:
                    return "CPO Number";
                case BizNetPaymentMethod.BankTransferFromCash:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    return "Transfer Reference";
                default:
                    return "Not Applicable";
            }
        }
        public static string selectReference(BizNetPaymentMethod method, string check, string cpo, string slip)
        {
            switch (method)
            {
                case BizNetPaymentMethod.Check:
                    return check;
                case BizNetPaymentMethod.CPOFromCash:
                case BizNetPaymentMethod.CPOFromBankAccount:
                    return cpo;
                case BizNetPaymentMethod.BankTransferFromCash:
                case BizNetPaymentMethod.BankTransferFromBankAccount:
                    return slip;
                default:
                    return null;
            }
        }
    }
    
}
