﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;
using INTAPS.ClientServer;

namespace BIZNET.iERP
{
    [Serializable]
    [SingleTableObject(orderBy = "itemCode")]
    [TypeID(3003)]
    public class PaymentInstrumentType
    {
        [IDField]
        public string itemCode;
        [DataField]
        public string name;
        [DataField]
        public bool isBankDocument;
        [DataField]
        public bool isCurreny;
        [DataField]
        public bool isToken;
        [DataField]
        public bool isTransferFromAccount;
        
        public override string ToString()
        {
            return name;
        }
    }

    [Serializable]
    [SingleTableObject]
    public class PaymentInstrumentItem
    {
        [DataField]
        public int despositToBankAccountID=-1;
        [DataField]
        public string instrumentTypeItemCode;//ITEM CODE REF
        [DataField]
        public int bankBranchID;
        [DataField]
        public string accountNumber;
        [DataField]
        public string documentReference;
        [DataField]
        public DateTime documentDate;
        [DataField]
        public string remark;
        [DataField]
        public double amount;
        public PaymentInstrumentItem()
        {
        }
        public PaymentInstrumentItem(string instrumentCode, double amount)
        {
            this.instrumentTypeItemCode = instrumentCode;
            this.amount = amount;
        }
    }


    [Serializable]
    [SingleTableObject]
    [TypeID(3001)]
    public class BankInfo
    {
        [IDField]
        public int id;
        [DataField]
        public string name;
        public override string ToString()
        {
            return name;
        }
    }
    
    [Serializable]
    [SingleTableObject]
    [TypeID(3002)]
    public class BankBranchInfo
    {
        [IDField]
        public int id;
        [DataField]
        public int bankID;
        [DataField]
        public string name;
        public override string ToString()
        {
            return name;
        }
    }
}
