﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP
{
    public class ItemOrCategory
    {
        public bool isCategory;
        public int categoryID;
        public string itemCode;
    }
    public class FixedAssetSettingItem
    {
        public string description;
        public ItemOrCategory[] categories;
        public double rate;
    }
    [Serializable]
    public class FixedAssetSetting
    {
        public int typeID;
        public FixedAssetSettingItem[] items;
    }

}
