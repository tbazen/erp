﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;
using System.Xml.Serialization;

namespace BIZNET.iERP
{
    [SingleTableObject]
    [XmlInclude(typeof(ProjectData))]
    [Serializable]
    public class Project
    {
        [DataField]
        public int costCenterID; //project cost center ID
        [IDField]
        public string code;
        [DataField]
        public string Name;
        [DataField]
        public DateTime startDate;
        [DataField]
        public int employeeID; //payroll manager
        [DataField]
        public double contractPrice;
        [DataField]
        public string description;
        [XMLField]
        public ProjectData projectData;
        [DataField]
        public ProjectType projectType;

        public Project()
        {
            projectData = new ProjectData();
        }
        public override string ToString()
        {
            return Name;
        }
    }
    [Serializable]
    public class ProjectData
    {
        public List<int> employees; //employee ids
        public List<string> stores; //store codes
        public List<string> cashAccounts; //cash codes
        public List<int> bankAccounts; //bank main account ids
       // public Dictionary<string, double> items; //item code key and unit price value
        public string[] items;
        public double[] unitPrices;
        public List<int> projectDivisions; //cost center ids under project cost center
        public List<int> machinaryAndVehicles;
        public ProjectData()
        {
            employees = new List<int>();
            stores = new List<string>();
            cashAccounts = new List<string>();
            bankAccounts = new List<int>();
           // items = new Dictionary<string, double>();
            projectDivisions = new List<int>();
            machinaryAndVehicles = new List<int>();
        }
    }

    public enum ProjectType
    {
        Project = 1,
        Branch = 2
    }
}
