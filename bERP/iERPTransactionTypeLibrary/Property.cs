﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.RDBMS;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    [Serializable]
    [SingleTableObject]
    public class PropertyType
    {
        [IDField]
        public int ID;
        [DataField]
        public string name;
        [DataField]
        public string extraDataTypeName;
        public Type extraDataType
        {
            get
            {
                return Type.GetType(extraDataTypeName);
            }
        }

        public override string ToString()
        {
            return name;
        }
    }

    [Serializable]
    [SingleTableObject]
    public class Property
    {
        [IDField]
        public int id;
        [DataField]
        public int propertyTypeID;
        [DataField]
        public string parentItemCode;
        [DataField]
        public string propertyCode;
        [DataField]
        public string name;
        [DataField]
        public string itemCode;
        [DataField]
        public DateTime acquireDate=DateTime.Now;
        [DataField]
        public string description;
        [DataField]
        public bool directCost = false;
        public Property clone()
        {
            return (Property)this.MemberwiseClone();
        }
        public static bool verifyPropertyQuantityBalance(Property p,AccountBalance[] matBal,bool throwException)
        {
            if (matBal.Length != 1 || !AccountBase.AmountEqual(matBal[0].DebitBalance, 1))
            {
                if (!throwException)
                    return false;
                throw new INTAPS.ClientServer.ServerUserMessage("Property {0} has invalid balance. There are {1} balances", p.propertyCode,matBal.Length);
            }
            return true;
        }
    }

    [Serializable]
    [SingleTableObject]
    public class PropertyItemCategory
    {
        [IDField]
        public string itemCode;
        [IDField]
        public int categorID;
    }
    //public class PropertyTypeAttribute:Attribute
    //{
    //    public int id;
    //}

    //[Serializable]
    //[SingleTableObject]
    //[PropertyType(id=2)]
    //public class VehiclePropertyData
    //{
    //    public string plateNo;
    //    public string sourceCountry;
    //    public string make;
    //    public string model;
    //    public int year;
    //}

}
