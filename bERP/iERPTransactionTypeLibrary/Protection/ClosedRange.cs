﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.RDBMS;

namespace BIZNET.iERP
{
    [Serializable]
    [SingleTableObject(orderBy="toDate desc")]
    public class ClosedRange
    {
        [IDField]
        public DateTime toDate;
        [DataField]
        public string comment;
        [DataField]
        public int AID;

        public string agentName="<Unknown>";
        public DateTime actionDate=DateTime.Now;
    }
}
    