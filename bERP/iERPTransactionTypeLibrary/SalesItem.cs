using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;

namespace BIZNET.iERP
{
    [Serializable]
    [SingleTableObject]
    public class SalesItem
    {
        [IDField]
        public string Code;
        [DataField]
        public string Name;
        [DataField]
        public string Description;
        [DataField]
        public double FixedUnitPrice;
        [DataField]
        public int SalesAccountID; //Income account
        [DataField]
        public ItemTaxStatus TaxStatus;
        [DataField]
        public GoodOrService GoodOrService;
        [DataField]
        public ServiceType ServiceType;
        [DataField]
        public GoodType GoodType;
        [DataField]
        public bool Activated = true;

    }
    
}
