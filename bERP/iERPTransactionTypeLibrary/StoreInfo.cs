﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.RDBMS;

namespace BIZNET.iERP
{
    [SingleTableObject]
    [Serializable]
    public class StoreInfo
    {
        [DataField]
        public int costCenterID;
        [IDField]
        public string code;
        [DataField]
        public string description;

        public string nameCode { get { return description + "-" + code; } }
        public override string ToString()
        {
            return nameCode;
        }
    }
   public enum StockLevelType
   {
       Normal,
       BelowMinimum,
       AboveMaximum
   }
    [Serializable]
    [SingleTableObject]
    public class StockLevelConfiguration
    {
        [IDField]
        public string itemCode;
        [DataField]
        public double minimum;
        [DataField]
        public double maximum;
        [DataField]
        public double dailyUse=0;
        public StockLevelType getStockLevelType(double q, bool considerMinStock, bool considerMaxStock)
        {
            if (considerMinStock && AccountBase.AmountGreater(minimum,0) && AccountBase.AmountLess(q, minimum))
                return StockLevelType.BelowMinimum;
            if (considerMaxStock && AccountBase.AmountGreater(maximum, 0) && AccountBase.AmountGreater(q, maximum))
                return StockLevelType.AboveMaximum;
            return StockLevelType.Normal;
        }
        public override string ToString()
        {
            if (AccountBase.AmountEqual(minimum, 0) && AccountBase.AmountEqual(maximum, 0))
                return "";
            if (AccountBase.AmountEqual(minimum, 0))
                return "Not more than " + MeasureUnit.FormatQuantity(maximum, null);
            if (AccountBase.AmountEqual(maximum, 0))
                return "Not less than" + MeasureUnit.FormatQuantity(minimum, null);
            return MeasureUnit.FormatQuantity(minimum, null) + " to " + MeasureUnit.FormatQuantity(maximum, null);

        }
    }
}
