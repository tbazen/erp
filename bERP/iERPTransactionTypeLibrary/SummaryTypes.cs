using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    public enum CashFlowReportDepth
    {
        Basic,
        Detailed,
    }
    [Serializable]
    public class DetailedCashFlowReportOption
    {
        public CashFlowReportDepth depth = CashFlowReportDepth.Basic;
        public int costCenterFilter=-1;
        public List<int> costCenters = null;

        public DetailedCashFlowReportOption(int costCenterID)
        {
            this.costCenterFilter = costCenterID;
        }
    }
    public enum AccountSummaryTypes
    {
        BalanceSheet,
        TrialBalance,
        IncomeStatement
    }
    public enum AccountSummaryDepthLevel
    {
        Full,
        Extended,
        Standard,
        Compact,
    }
    public enum AccountSummaryItemType
    {
        Debit,
        Credit,
        Balance,
    }
    [INTAPS.RDBMS.LoadFromSetting]
    [Serializable]
    public class SimpleAccountSummaryInfo
    {
        public int[][] addAccounts;
        public AccountSummaryItemType[] summaryType;
        public string[] description;
    }
    [INTAPS.RDBMS.LoadFromSetting]
    [Serializable]
    public class BalanceSheetFormat
    {
        public string[] assetSectionTitle;
        public SimpleAccountSummaryInfo[] assetSections;
        public string[] liabilitySectionTitle;
        public SimpleAccountSummaryInfo[] liabilitySections;
    }
    [Serializable]
    public class SummaryValueItem 
    {
        public string tag;
        public string label;
        public string displayStyle;
        public double dbValue;
        public double crValue;
        public bool creditBalance;
        public AccountBalance accountBalance
        {
            get
            {
                return new AccountBalance(-1, 0, dbValue, crValue);
            }
            set
            {
                this.dbValue = value.TotalDebit;
                this.crValue = value.TotalCredit;
            }
        }

        public double netBalance
        {
            get
            {
                return accountBalance.GetBalance(creditBalance);
            }
        }


        public virtual SummaryValueItem Clone()
        {
            return (SummaryValueItem)this.MemberwiseClone();
        }
    }
    [Serializable]
    public class SummaryValueSection : SummaryValueItem
    {
        public override SummaryValueItem Clone()
        {
            SummaryValueSection clone = new SummaryValueSection();
            foreach(SummaryValueItem c in childs)
            {
                clone.childs.Add(c.Clone());
            }
            return clone;
        }
        public List<SummaryValueItem> childs = new List<SummaryValueItem>();
        public SummaryValueItem getValueByTag(string tag)
        {
            return getValueByTag(this, tag);
        }
        SummaryValueItem getValueByTag(SummaryValueSection section,string tag)
        {
            foreach (SummaryValueItem item in section.childs)
            {
                if (tag.Equals(item.tag,StringComparison.CurrentCultureIgnoreCase))
                {
                    return item;
                }
                if (item is SummaryValueSection)
                {
                    SummaryValueItem ret = getValueByTag((SummaryValueSection)item, tag);
                    if (ret != null)
                        return ret;
                }
            }
            return null;
        }

        static void addToSectionByTag(SummaryValueSection a, SummaryValueSection b)
        {
            Dictionary<string, SummaryValueItem> taggedItems = new Dictionary<string, SummaryValueItem>();
            foreach (SummaryValueItem item in a.childs)
            {
                if (!string.IsNullOrEmpty(item.tag))
                    taggedItems.Add(item.tag, item);
            }
            foreach (SummaryValueItem item in b.childs)
            {
                if (string.IsNullOrEmpty(item.tag))
                {
                    a.childs.Add(item);
                }
                else
                {
                    if (taggedItems.ContainsKey(item.tag))
                    {
                        SummaryValueItem taggedItem = taggedItems[item.tag];
                        if (!item.label.Equals(taggedItem.label))
                            taggedItem.label = "[Mixed]";
                        taggedItem.dbValue += item.dbValue;
                        taggedItem.crValue += item.crValue;

                        if (taggedItem is SummaryValueSection)
                        {
                            if (!(item is SummaryValueSection))
                                throw new INTAPS.ClientServer.ServerUserMessage("Incompatible summaries for addition operation. The problem is at item with tag {0}");
                            addToSectionByTag((SummaryValueSection)taggedItem, (SummaryValueSection)item);
                        }                        
                    }
                    else
                        a.childs.Add(item);
                }
            }
        }
        public static SummaryValueSection addByTag(SummaryValueSection a, SummaryValueSection b)
        {
            SummaryValueSection c = a.Clone() as SummaryValueSection;
            addToSectionByTag(c, b);
            return c;
        }
    }

    [Serializable]
    public class SummaryItemBase
    {
        public string tag="";
        public bool credit;
        public string label;
        public string format;
        public bool removeIfZero;
    }
    [Serializable]
    public class SummaryItemInfo : SummaryItemBase
    {
        public List<int> accounts=new List<int>();
        public List<double> factors=new List<double>();
        public List<string> formulae = new List<string>();
    }
    [Serializable]
    public class SummaryItemListInfo : SummaryItemBase
    {
        public string accountFormula;
        public string factorFormula;
        public string tagFormula;
    }
    [Serializable]
    [XmlInclude(typeof(SummaryItemInfo))]
    [XmlInclude(typeof(SummarySection))]
    [INTAPS.RDBMS.LoadFromSetting]
    public class SummarySection : SummaryItemBase
    {
        public bool showTotal;
        public List<SummaryItemBase> summaryItem=new List<SummaryItemBase>();
    }
    [Serializable]
    [XmlInclude(typeof(SummaryItemInfo))]
    [XmlInclude(typeof(SummarySection))]
    [INTAPS.RDBMS.LoadFromSetting]
    public class SummaryInformation
    {
        public static string formatDate2(DateTime date2Value)
        {
            if (date2Value.Hour == 0 && date2Value.Minute == 0 && date2Value.Second == 0 && date2Value.Millisecond == 0)
            {
                return AccountBase.FormatDate(date2Value.AddDays(-1));
            }
            return date2Value.ToString("MMM dd,yyy hh:mm tt");
        }

        public const string LINK_OPEN_DOC = "doc";
        public const string LINK_DELETE_DOC = "deldoc";
        public const string LINK_VIEW_ENTRIES = "docEntries";
        public bool twoColumns=true;
        public bool flow = false;
        public bool headerRow = false;
        public bool grandTotal = false;
        public int itemID = TransactionItem.DEFAULT_CURRENCY;
        
        public string itemHeader="Particular";
        public string debitHeader="";
        public string creditHeader="";
        
        public SummarySection rootSection = new SummarySection(){label="Root Section",tag="root"};
        public bool isTagUnique(bool ignoreEmpty)
        {
            return isTagUnique(rootSection, new List<string>(), ignoreEmpty);
        }
        bool isTagUnique(SummarySection rootSection, List<string> tags, bool ignoreEmpty)
        {
            foreach (SummaryItemBase item in rootSection.summaryItem)
            {
                if (string.IsNullOrEmpty(item.tag))
                {
                    if (!ignoreEmpty)
                        return false;
                    continue;
                }
                string tag = item.tag.ToUpper();
                if (tags.Contains(tag))
                    return false;
                tags.Add(tag);
                if (item is SummarySection)
                    if (!isTagUnique((SummarySection)item, tags, ignoreEmpty))
                        return false;
            }
            return true;
        }
        public SummaryItemBase getItemByTag(string tag)
        {
            SummarySection par;
            return getItemByTag(rootSection, tag,out par);
        }
        public SummaryItemBase getItemByTag(SummarySection section, string tag, out SummarySection parent)
        {
            foreach(SummaryItemBase item in section.summaryItem)
            {
                if (tag.Equals(item.tag, StringComparison.CurrentCultureIgnoreCase))
                {
                    parent = section;
                    return item;
                }
                if (item is SummarySection)
                {
                    SummaryItemBase b = getItemByTag((SummarySection)item, tag,out parent);
                    if (b != null)
                        return b;
                }
            }
            parent = null;
            return null;
        }
        public SummarySection getItemParent(SummaryItemBase theItem)
        {
            return getItemParent(rootSection, theItem);
        }
        public SummarySection getItemParent(SummarySection section, SummaryItemBase theItem)
        {
            foreach (SummaryItemBase item in section.summaryItem)
            {
                if (theItem==item)
                {
                    return section;
                }
                if (item is SummarySection)
                {
                    SummarySection b = getItemParent((SummarySection)item, theItem);
                    if (b != null)
                        return b;
                }
            }
            return null;
        }
        bool isChildParent(string child, SummaryItemBase parent)
        {
            if (child.Equals(parent.tag, StringComparison.CurrentCultureIgnoreCase))
                return false;
            SummarySection parentItem = parent as SummarySection;
            while (parentItem!=null)
            {
                foreach (SummaryItemBase item in parentItem.summaryItem)
                {
                    if (child.Equals(item.tag, StringComparison.CurrentCultureIgnoreCase))
                        return true;
                    bool test = isChildParent(child, item);
                    if (test)
                        return true;
                }
            }
            return false;
        }
        public bool isInSameHeirarchy(string tag1, string tag2)
        {
            if (tag1.Equals(tag2, StringComparison.CurrentCultureIgnoreCase))
                return true;
            SummaryItemBase item1 = getItemByTag(tag1);
            SummaryItemBase item2 = getItemByTag(tag2);
            if (isChildParent(tag1, item2))
                return true;
            if (isChildParent(tag2, item1))
                return true;
            return false;
        }

        public SummarySection getItemParent(string item)
        {
            SummarySection ret;
            getItemByTag(rootSection, item, out ret);
            return ret;
        }

        public void merge(SummaryInformation s)
        {
            foreach(SummaryItemInfo i in  s.rootSection.summaryItem)
            {
                //if(string.IsNullOrEmpty(i.tag) || getItemByTag(i.tag)==null)
                {
                    this.rootSection.summaryItem.Add(i);
                }
            }
        }

        public bool isHierarchicalParent(SummarySection parentSection, SummaryItemBase testChild)
        {
            foreach(SummaryItemBase item in parentSection.summaryItem)
            {
                if (item == testChild)
                    return true;
                if(item is SummarySection)
                {
                    if (isHierarchicalParent((SummarySection)item, testChild))
                        return true;
                }
            }
            return false;
        }
        
    }
    
    [Serializable]
    public class LedgerParameters
    {
        [Flags]
        public enum LedgerRemarkDetail
        {
            Concise=0,
            Link=1,
            PaperRef=2,
            AllRef=4,
            Detail=Link|PaperRef|AllRef,
        }

        public int costCenterID;
        
        public string includeAccountFrom=null;
        public string includAccountTo=null;
        public string excludeAccountFrom = null;
        public string excludeAccountTo = null;
        
        public int[] expandAccounts = null;
        
        public int [] accountIDs=null;
        public int itemID;
        public DateTime time1;
        public DateTime time2;
        public string groupByPeriod=null;
        public bool hideAccountColumnIfUniformAccount=true;
        public bool hideItemColumnIfUniformItem=true;
        public bool twoColumns=true;
        public bool asCreditAccount;
        public bool merged = false;
        public bool excludeEmptyLedger=false;
        public bool excludeEmptyBalance=false;
        public LedgerRemarkDetail remarkDetail = LedgerRemarkDetail.Detail;
        public bool printMode = false;
        public bool qauntityOnly = false;
        
    }
}
    