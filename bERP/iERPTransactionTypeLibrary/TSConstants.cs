using System;
using System.Collections.Generic;
using System.Text;

namespace BIZNET.iERP
{
    public class TSConstants
    {
        private const string BIRR_FORMAT = "#,0.00";
        private const string ACCOUNT_CODE_SUFFIX_FORMAT = "000";
        private const string STORE_CODE_SUFFIX_FORMAT = "00";
        private const string BANK_CODE_SUFFIX_FORMAT = "0000";
        private const string CASH_CODE_SUFFIX_FORMAT = "0000";
        public const string ITEM_ACCOUNTCODE_SUFFIX_FORMAT = "000";
        private const string SERIAL_CODE_FORMAT = "000";
        private const string COSTCENTER_SERIAL_CODE_FORMAT = "00";
        private const string RELATION_CODE_FORMAT = "0000";
        public static string FormatBirr(double amount)
        {
            return amount.ToString(BIRR_FORMAT);
        }

        public static string FormatAccountCode(string code, int codeSuffix)
        {
            string suffix = codeSuffix.ToString(ACCOUNT_CODE_SUFFIX_FORMAT);
            return String.Format("{0}-{1}", code, suffix);
        }
        public static string FormatAccountName(string parentAccountName, string name)
        {
            return String.Format("{0}-{1}", parentAccountName, name);
        }
        public static string FormatStoreCode(string code, int codeSuffix)
        {
            string suffix = codeSuffix.ToString(STORE_CODE_SUFFIX_FORMAT);
            return String.Format("{0}-S{1}", code, suffix);
        }
       
        public static string FormatItemAccountCode(string code, string suffix)
        {
            return String.Format("{0}-{1}", code, suffix);
        }
        public static string FormatBankCode(string code, int codeSuffix, string format)
        {
            string suffix = codeSuffix.ToString(format);
            return String.Format("{0}-{1}", code, suffix);
        }
        public static string FormatCashCode(string code, int codeSuffix, string format)
        {
            string suffix = codeSuffix.ToString(format);
            return String.Format("{0}-{1}", code, suffix);
        }
        public static string FormatProjectDivisionCode(string code, int codeSuffix)
        {
            string suffix = codeSuffix.ToString(COSTCENTER_SERIAL_CODE_FORMAT);
            return String.Format("{0}-D{1}", code, suffix);
        }
        public static string FormatMachinaryAndVehicleCode(string code, int codeSuffix)
        {
            string suffix = codeSuffix.ToString(COSTCENTER_SERIAL_CODE_FORMAT);
            return String.Format("{0}-M{1}", code, suffix);
        }
        public static string FormatSerialCode(int code)
        {
            return code.ToString(SERIAL_CODE_FORMAT);
        }
        public static string FormatRelationCode(int code)
        {
            return code.ToString(RELATION_CODE_FORMAT);
        }
        public static string FormatItemSerialCode(string categoryCode,int code)
        {
            string c = code.ToString(SERIAL_CODE_FORMAT);
            return string.Concat(categoryCode, c);
        }
        public static string FormatStoreSerialCode(int code)
        {
            return code.ToString(COSTCENTER_SERIAL_CODE_FORMAT);
        }
        public static void addCompanyNameLine(CompanyProfile profile, StringBuilder builder)
        {
            string companyName = string.Format("<h1>{0}</h1>", profile == null ? "Unknown company" : profile.Name);
            builder.Append(companyName);
        }
        
    }
}
