using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using INTAPS.RDBMS;
using BIZNET.iERP.TypedDataSets;

namespace BIZNET.iERP
{
    [SingleTableObject]
    public class DeclaredDocument
    {
        [IDField]
        public int documentID=-1;
        [IDField]
        public int rejectedDocumentID = -1;
        [IDField]
        public int declarationType=-1;
        [DataField]
        public int declarationID=-1;
        [DataField]
        public int postDocumentID=-1;
        [DataField]
        public int taxCenterID = 1;
    }
    [Serializable]
    [SingleTableObject]
    public class TaxDeclaration
    {
        [IDField]
        public int id;
        [DataField]
        public int declarationType;
        [DataField]
        public int periodID;
        [XMLField]
        public DataSet report;
        [XMLField]
        public int[] declaredDocuments;
        [XMLField]
        public int[] rejectedDocuments;
        [DataField]
        public DateTime logDate;
        [DataField]
        public int payDocumentID;
        [DataField]
        public DateTime payDate;
        [DataField]
        public double paidAmount;
        [DataField]
        public DateTime declarationDate;
        [DataField]
        public int TaxCenter;
        public TaxDeclaration()
        {
            id = -1;
            declarationType = -1;
            periodID = -1;
            report = null;
            declaredDocuments = new int[0];
            logDate = DateTime.Now;
            payDocumentID = -1;
            payDate = DateTime.Now;
            paidAmount = 0;
            declarationDate = DateTime.Now;
        }
    }
}
