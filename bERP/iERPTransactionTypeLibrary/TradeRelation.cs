using System;
using System.Collections.Generic;
using INTAPS.RDBMS;

namespace BIZNET.iERP
{
    public enum TradeRelationType
    {
        Supplier,
        Customer,
        Financer,
        Financee,
        All
    }

    [Serializable]
    [SingleTableObject(orderBy="code")]
    public class TradeRelationCategory
    {
        [IDField]
        public string code;
        [DataField]
        public string pcode;
        [DataField]
        public string name;

        [AIParentAccountField]
        [DataField]
        public int ReceivableParentAccountID = -1;
        [AIParentAccountField]
        [DataField]
        public int PayableParentAccountID = -1;
        [AIParentAccountField]
        [DataField]
        public int PaidBondParentAccountID = -1;
        [AIParentAccountField]
        [DataField]
        public int ReceivedBondParentAccountID = -1;
        [AIParentAccountField(true)]
        [DataField]
        public int onCustomerOrderParentCostCenterID = -1;
        
        [AIParentAccountField(true)]
        [DataField]
        public int onSupplierOrderParentCostCenterID = -1;
        
        [DataField]
        [AIParentAccountField(false)]
        public int RetentionReceivableParentAccountID = -1;
        
        [DataField]
        [AIParentAccountField(false)]
        public int RetentionPayableParentAccountID = -1;

        public string nameCode
        {
            get
            {
                return name + "(" + code+")";
            }
        }
        public override string ToString()
        {
            return nameCode;
        }
    }
    [Serializable]
    [SingleTableObject]
    public class TradeRelation : IAccountingItem
    {
        [IDField]
        public string Code;
        [DataField]
        public string pcode;
        [DataField]
        public string Name;
        [DataField]
        public string TIN;
        [DataField]
        public string VATRegNumber;
        [DataField] 
        public bool IsInternational;
        [DataField] 
        public short Country;
        [DataField] 
        public string City;
        [DataField]
        public string Region;
        [DataField]
        public string Zone;
        [DataField]
        public string Woreda;
        [DataField]
        public string Kebele;
        [DataField]
        public string HouseNumber;
        [DataField]
        public string Telephone;
        [DataField]
        public string Email;
        [DataField]
        public string Website;
        [DataField]
        public string Description;
        [DataField]
        public TaxRegisrationType TaxRegistrationType;
        [DataField]
        [AIChildOf("ReceivableParentAccountID", "Receivable Account", false)]
        public int ReceivableAccountID = -1;
        [DataField]
        [AIChildOf("PayableParentAccountID", "Payable Account", false)]
        public int PayableAccountID = -1;
        [DataField]
        [AIChildOf("PaidBondParentAccountID", "Paid Bond Account", false)]
        public int PaidBondAccountID = -1;
        [DataField]
        [AIChildOf("ReceivedBondParentAccountID", "Received Bond Account", false)]
        public int ReceivedBondAccountID = -1;
        [DataField]
        [AIChildOf("RetentionReceivableParentAccountID", "Retention Receivable Account", false)]
        public int RetentionReceivableAccountID = -1;
        [DataField]
        [AIChildOf("RetentionPayableParentAccountID", "Retaionn Payable Account", false)]
        public int RetentionPayableAccountID = -1;
        [DataField]
        public bool Activated = true;
        [DataField]
        public byte[] Logo;
        [DataField]
        public bool Withhold = true;
        [DataField]
        public bool IsWithholdingAgent = true;
        [DataField]
        public bool IsVatAgent = false;
        [DataField]
        public bool IsSupplier;
        [DataField]
        public bool IsCustomer;
        [DataField]
        public bool IsFinancer;
        [DataField]
        public bool IsFinancee;
        [DataField]
        [AIChildOf("onCustomerOrderParentCostCenterID", "Items Ordered By Customer Cost Center", true)]
        public int onCustomerOrderCostCenterID = -1;
        [DataField]
        [AIChildOf("onSupplierOrderParentCostCenterID", "Items Ordered From Supplier Cost Center", true)]
        public int onSupplierOrderCostCenterID = -1;
        [DataField]
        public string Address;

        public bool IsTradeRelation
        {
            get
            {
                return IsSupplier || IsCustomer;
            }
        }
        public bool IsFinancingRelation
        {
            get
            {
                return !IsSupplier && !IsCustomer && (IsFinancee || IsFinancee);
            }
        }

        public int[] allAccountFields
        {
            get
            {
                return new int[] { ReceivableAccountID, PayableAccountID, PaidBondAccountID, ReceivedBondAccountID };
            }
        }
        public int[] allCostCenterFields
        {
            get
            {
                return new int[] { onSupplierOrderCostCenterID, onCustomerOrderCostCenterID };
            }
        }
        public bool hasTINNumber
        {
            get
            {
                return !string.IsNullOrEmpty(TIN);
            }
        }


        public string TypedName
        {
            get
            {
                if (IsCustomer && IsSupplier)
                    return " trader " + Name;
                if (IsCustomer)
                    return " customer " + Name;
                if (IsSupplier)
                    return " supplier " + Name;
                if (IsFinancer)
                    return " financer " + Name;
                if (IsFinancee)
                    return " financee " + Name;
                return Name;
            }
        }

        public string TaxRegistrationTypeName
        {
            get
            {
                if (TaxRegistrationType == TaxRegisrationType.VAT)
                    return " VAT (Value Added Tax) ";
                if (TaxRegistrationType == TaxRegisrationType.TOT)
                    return " TOT (Turn Over Tax) " ;
                if (TaxRegistrationType == TaxRegisrationType.NonTaxPayer)
                    return " Non-VAT & TOT Tax Payer " ;
                return "";
            }
        }
        public string TradeType
        {
            get
            {
                string ret = null;
                if (IsSupplier)
                    ret = "Supplier";
                if (IsCustomer)
                    if (ret == null)
                        ret = "Customer";
                    else
                        ret += ", Customer";
                if (IsFinancer)
                    if (ret == null)
                        ret = "Creditor";
                    else
                        ret += ", Creditor";
                if (IsFinancee)
                    if (ret == null)
                        ret = "Debitor";
                    else
                        ret += ", Debitor";
                return ret;
            }
        }
        public string coaName
        {
            get { return Name; }
        }
        public string coaCode
        {
            get { return Code; }
        }
        public string NameCode
        {
            get
            {
                return Name + "(" + Code + ")";
            }
        }

        public byte IsInternationalValue
        {
            get { return Convert.ToByte(IsInternational); }
        }
    }

    [Serializable]
    [SingleTableObject(orderBy = "Name")]
    public class Country
    {
        [IDField]
        public short Id;
        [DataField]
        public string Name;

        public override string ToString()
        {
            return Name;
        }
    }

}
