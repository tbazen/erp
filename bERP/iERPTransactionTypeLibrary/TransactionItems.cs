using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;
using INTAPS.Accounting;
using INTAPS;
namespace BIZNET.iERP
{

    [Serializable]
    [SingleTableObject]
    public class TransactionItems:IAccountingItem
    {
        [IDField]
        public string Code;
        [DataField]
        public string Name;
        [DataField]
        public string Description;
        [DataField]
        public double FixedUnitPrice;
        [DataField]
        public bool IsDirectCost;
        [DataField]
        public ItemTaxStatus TaxStatus;
        [DataField]
        public GoodOrService GoodOrService;
        [DataField]
        public ServiceType ServiceType;
        [DataField]
        public ExpenseType ExpenseType = ExpenseType.GeneralExpense;
        [DataField]
        public InventoryType inventoryType;
        [DataField]
        public int DepreciationType;
        [DataField]
        public GoodType GoodType;
        [DataField]
        public int MeasureUnitID = 1; //1 is the default measure unit which is by 'piece'
        [DataField]
        public bool Activated = true;
        [DataField]
        public bool IsInventoryItem = false;
        [DataField]
        public bool IsExpenseItem = false;
        [DataField]
        public bool IsSalesItem = false;
        [DataField]
        public bool IsFixedAssetItem = false;
        [DataField]
        [AIChildOf(new string[]{"expenseAccountPID", "directCostAccountPID"}, "Expense Account", false)]
        public int expenseAccountID=-1; //either director cost or expense account
        [DataField]
        [AIChildOf("prePaidExpenseAccountPID", "Prepaid Expense Account", false)]
        public int prePaidExpenseAccountID = -1;
        [DataField]
        [AIChildOf("salesAccountPID", "Sales Account", false)]
        public int salesAccountID = -1;
        [DataField]
        [AIChildOf("unearnedRevenueAccountPID", "Uneared Revenue Account", false)]
        public int unearnedRevenueAccountID = -1;
        [DataField]
        [AIChildOf("generalInventoryAccountPID", "General Inventory Account", false)]
        public int inventoryAccountID = -1; //either general or finished goods inventory type
        [DataField]
        [AIChildOf("finishedGoodsAccountPID", "Finished Good Account", false)]
        public int finishedGoodAccountID = -1;
        
        [DataField]
        [AIChildOf("finishedWorkAccountPID", "Finished Work Account", false)]
        public int finishedWorkAccountID = -1; //finished services
        
        [DataField]
        [AIChildOf("originalFixedAssetAccountPID", "Fixed Asset Original Value Account", false)]
        public int originalFixedAssetAccountID = -1;
        [DataField]
        [AIChildOf("depreciationAccountPID", "Fixed Asset Depreciation Expense Account", false)]
        public int depreciationAccountID = -1; //Expense account
        [DataField]
        [AIChildOf("accumulatedDepreciationAccountPID", "Fixed Asset Accumulated Depreciation Account", false)]
        public int accumulatedDepreciationAccountID = -1; //Asset account
        [DataField]
        [AIChildOf("pendingOrderAccountPID", "Pending Order Account", false)]
        public int pendingOrderAccountID = -1;
        [DataField]
        [AIChildOf("pendingDeliveryAcountPID", "Pending Delivery Account", false)]
        public int pendingDeliveryAcountID = -1;

        //summary accounts follow
        [AIChildOf(new string[] { "expenseSummaryAccountID", "directCostSummaryAccountID" }, "Expense Summary Account", false)]
        [DataField]
        public int expenseSummaryAccountID = -1;
        
        [AIChildOf("prePaidExpenseSummaryAccountID", "Prepaid Expense Summary Account", false)]
        [DataField]
        public int prePaidExpenseSummaryAccountID = -1;

        [AIChildOf("salesSummaryAccountID", "Sales Summary Account", false)]
        [DataField]
        public int salesSummaryAccountID = -1;

        [AIChildOf("unearnedSummaryAccountID", "Uneared Revenue Summary Account", false)]
        [DataField]
        public int unearnedSummaryAccountID = -1;

        [AIChildOf("finishedWorkSummaryAccountID", "Finished Work Summary Account", false)]
        [DataField]
        public int finishedWorkSummaryAccountID = -1; //finished services

        [AIChildOf("originalFixedAssetSummaryAccountID", "Fixed Asset Original Value Summary Account", false)]
        [DataField]
        public int originalFixedAssetSummaryAccountID = -1;

        [AIChildOf("depreciationSummaryAccountID", "Fixed Asset Depreciation Expense Summary Account", false)]
        [DataField]
        public int depreciationSummaryAccountID = -1; //Expense account

        [AIChildOf("accumulatedDepreciationSummaryAccountID", "Fixed Asset Accumulated Depreciation Summary Account", false)]
        [DataField]
        public int accumulatedDepreciationSummaryAccountID = -1; //Asset account

        [AIChildOf("generalInventorySummaryAccountID", "General Inventory Summary Account", false)]
        [DataField]
        public int generalInventorySummaryAccountID = -1;

        [AIChildOf("finishedGoodsSummaryAccountID", "Finished Good Inventory Summary Account", false)]
        [DataField]
        public int finishedGoodsSummaryAccountID = -1;


        [AIChildOf("pendingOrderSummaryAccountID", "Pending Order Summary Account", false)]
        [DataField]
        public int pendingOrderSummaryAccountID = -1;

        [AIChildOf("pendingDeliverySummaryAccountID", "Pending Delivery Summary Account", false)]
        [DataField]
        public int pendingDeliverySummaryAccountID = -1;
        
        
        
        
        
        
        
        [DataField]
        public int categoryID;

        public static int compareCode(string code1, string code2)
        {
            return code1.ToUpper().CompareTo(code2.ToUpper());
        }

        public static bool codeEqual(string code1, string code2)
        {
            return code1.ToUpper().Equals(code2.ToUpper());
        }
        public override string ToString()
        {
            return Name;
        }

        public int materialAssetAccountID(bool inStore)
        {
            if (inStore && inventoryAccountID!=-1)
                return inventoryAccountID;

            if (IsInventoryItem)
                if(this.inventoryType==InventoryType.GeneralInventory)
                    return inventoryAccountID;
                else
                    return finishedGoodAccountID;

            if (IsFixedAssetItem)
                return originalFixedAssetAccountID;
            return -1;
        }
        
        public int materialAssetSummaryAccountID(bool inStore)
        {
            if (inStore && generalInventorySummaryAccountID!=-1)
                return generalInventorySummaryAccountID;

            if (IsInventoryItem)
                if (this.inventoryType == InventoryType.GeneralInventory)
                    return generalInventorySummaryAccountID;
                else
                    return finishedGoodsSummaryAccountID;
            if (IsFixedAssetItem)
                return originalFixedAssetSummaryAccountID;
            return -1;
        }

        public int materialAssetReturnAccountID
        {
            get
            {
                if (IsInventoryItem)
                    return expenseAccountID;
                if (IsFixedAssetItem)
                    return originalFixedAssetAccountID;
                return -1;

            }
        }

        public string coaName
        {
            get { return Name; }
        }

        public string coaCode
        {
            get { return Code; }
        }

        public string NameCode { get { return Name + " - " + Code; } }

        public void clearAccounts()
        {
            foreach(System.Reflection.FieldInfo fi in typeof(TransactionItems).GetFields())
            {
                if (fi.GetCustomAttributes(typeof(AIChildOfAttribute),false).Length > 0)
                    fi.SetValue(this, -1);
            }
        }
        //public List<int> getAllAccounts()
        //{
        //    List<int> ret = new List<int>();
        //    foreach (System.Reflection.FieldInfo fi in typeof(TransactionItems).GetFields())
        //    {
        //        if (fi.GetCustomAttributes(typeof(AIChildOfAttribute), false).Length > 0)
        //        {
        //            int aid = (int)fi.GetValue(this);
        //            if(aid!=-1)
        //                ret.Add(aid);
        //        }
        //    }
        //    return ret;
        //}
        public List<int> getPrivateAccounts()
        {
            List<int> ret = new List<int>();
            foreach (System.Reflection.FieldInfo fi in typeof(TransactionItems).GetFields())
            {
                object[] atr = fi.GetCustomAttributes(typeof(AIChildOfAttribute), false);
                if (atr.Length > 0)
                {
                    AIChildOfAttribute aiAtr = (AIChildOfAttribute)atr[0];
                    atr = typeof(ItemCategory).GetField(aiAtr.categoryField[0]).GetCustomAttributes(typeof(AIParentAccountFieldAttribute), false);
                    if (atr.Length > 0)
                    {
                        AIParentAccountFieldAttribute aipAtr = (AIParentAccountFieldAttribute)atr[0];
                        if (aipAtr.createChildAccount)
                        {
                            int aid = (int)fi.GetValue(this);
                            if (aid != -1)
                                ret.Add(aid);
                        }
                    }
                }
            }
            return ret;
        }
    }

    

    [Serializable]
    [SingleTableObject]
    public class MeasureUnit
    {
        [IDField]
        public int ID;
        [DataField]
        public string Name;

        public static string FormatQuantity(double quantity, MeasureUnit unit)
        {
            double absquantity = Math.Abs(quantity);
            string unitText;
            bool intValue = AccountBase.AmountEqual(absquantity - Math.Floor(absquantity), 0);
            if (unit == null)
                unitText = "";
            else
            {
                if (AccountBase.AmountGreater(absquantity, 1) && intValue)
                    unitText = " "+unit.plural;
                else
                    unitText = " " + unit.Name;
            }
                string format = intValue ? "#,#0" : "#,#0.00";
            if (AccountBase.AmountLess(quantity, 0))

                return "(" + (-quantity).ToString(format) + ") " + unitText;
            if (AccountBase.AmountEqual(quantity, 0))
                return "-";
            return quantity.ToString(format) + unitText;
        }

        public override string ToString()
        {
            return Name;
        }
        public string plural
        {
            get
            {
                return Name + "s";
            }
        }
        public override bool Equals(object obj)
        {
            if (!(obj is MeasureUnit))
                return false;
            return this.ID == ((MeasureUnit)obj).ID;
        }
    }
    [Serializable]
    [SingleTableObject]
    public class UnitCoversionFactor
    {
        [IDField]
        public string itemScope="";
        [IDField]
        public int categoryScope=-1;
        [IDField]
        public int baseUnitID;
        [IDField]
        public int conversionUnitID;
        [DataField]
        public double conversionFactor;
    }
    
    [Serializable]
    public class UnitConvertor
    {
        Dictionary<int, Dictionary<int, double>> factors = new Dictionary<int, Dictionary<int, double>>();
        public void addFactor(int unit1,int unit2,double factor)
        {
            addOneDirectionFactor(unit1, unit2, factor);
            if (AccountBase.AmountEqual(factor, 0))
                return;
            addOneDirectionFactor(unit2, unit1, 1 / factor);
        }
        void addOneDirectionFactor(int unit1, int unit2, double factor)
        {
            addToConversionDictionary(unit1, unit2, factor);
            List<KeyValuePair<int, double>> conversionsToUnit1 = new List<KeyValuePair<int, double>>();
            foreach(KeyValuePair<int,Dictionary<int,double>> f in factors)
            {
                foreach(KeyValuePair<int,double> ff in f.Value)
                {
                    if(ff.Key==unit1)
                    {
                        conversionsToUnit1.Add(new KeyValuePair<int,double>(f.Key,ff.Value));
                    }
                }
            }
            foreach(KeyValuePair<int,double> ff in conversionsToUnit1)
            {
                addToConversionDictionary(ff.Key, unit2, factor * ff.Value);
            }
        }
        private void addToConversionDictionary(int unit1, int unit2, double factor)
        {
            if (!factors.ContainsKey(unit1))
                factors.Add(unit1, new Dictionary<int, double>());
            factors[unit1][unit2] = factor;
        }

        public bool canConvert(int unit1, int unit2)
        {
            Dictionary<int,double> conv;
            return factors.TryGetValue(unit1, out conv) && conv.ContainsKey(unit2);
        }
        public double convert(int unit1,int unit2, double value)
        {
            Dictionary<int, double> conv;
            double factor;
            if (!factors.TryGetValue(unit1, out conv) || !conv.TryGetValue(unit2, out factor))
            {
                if (!factors.TryGetValue(unit2, out conv) || !conv.TryGetValue(unit1, out factor))
                    throw new InvalidOperationException("Unit conversion from {0} to {1} not supprted".format(unit1, unit2));
                factor = 1 / factor;
            }
            return value * factor;
        }

    }
    public enum ExpenseType
    {
        PurchaseExpense = 1,
        GeneralExpense = 2,
        UnclaimedExpense = 3,
        TaxAuthorityUnclaimable=4
    }
    public enum InventoryType
    {
        GeneralInventory=0,
        FinishedGoods=1
    }
    public enum DepreciationType
    {
        Building = 0,
        IntangibleAssets = 1,
        ComputersAndSoftwareProductsRelated = 2,
        OtherBusinessAssets = 3,
        None = 4
    }
}
