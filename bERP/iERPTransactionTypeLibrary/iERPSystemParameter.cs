using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;
using INTAPS.Accounting;

namespace BIZNET.iERP
{
    public class AccountReferenceAttribute: Attribute
    {
        public string description;
        public string category;
        public AccountReferenceAttribute(string cat, string desc)
        {
            description = desc;
            category = cat;
        }
    }
    public class CostCenterReferenceAttribute : AccountReferenceAttribute
    {
        public CostCenterReferenceAttribute(string cat, string desc)
            : base(cat, desc)
        {
        }
    }


    public class AccountDesignationAttribute : Attribute
    {
        public string name;
        public string parent;
        public AccountDesignationAttribute(string name,string parent)
        {
            this.name = name;
            this.parent = parent;
        }
        public AccountDesignationAttribute(string name)
        {
            this.name = name;
            this.parent = null;
        }

    }
    [Serializable]
    public class iERPSystemParameters
    {
        [AccountReference("Root Accounts", "Asset Account")]
        public int assetAccountID;
        
        [AccountReference("Root Accounts", "Liabiliy Account")]
        public int assetLiabilityAccountID;
        
        [AccountReference("Root Accounts", "Capital Account")]
        public int assetCapitalAccountID;
        
        [AccountReference("Root Accounts", "Income Account")]
        public int assetIncomeAccountID;

        [AccountReference("Root Accounts", "Cost of Sells Account")]
        public int costOfSalesAccountID;

        [AccountReference("Root Accounts", "General Expense Account")]
        public int expenseAccountID;

        [AccountReference("Common Expense Accounts", "Vehicle Fuel Expense")]
        public int vehicleFuelExpenseAccountID;
        [AccountReference("Common Expense Accounts", "Vehicle Spare Part Expense")]
        public int vehicleSparePartExpenseAccountID;
        [AccountReference("Common Expense Accounts", "Vehicle Maintenance Expense")]
        public int vehicleMaintainanceExpenseAccountID;
        
        [AccountReference("Staff Related Accounts", "Loan From Staff Liability")]
        public int LoanFromStaffAccountID;
        [AccountReference("Staff Related Accounts", "Labor Expense")]
        public int laborExpenseAccountID;
        [AccountReference("Staff Related Accounts", "Labor Income Tax Payable")]
        public int laborIncomeTaxAccountID;

        [AccountReference("Trade and Finance Relations Account", "Trade Receivable")]
        public int tradeReceivablesAccountID;
        [AccountReference("Trade and Finance Relations Account", "Trade Payable")]
        public int tradePayablesAccountID;

        [AccountReference("Trade and Finance Relations", "Financee Receivable")]
        public int financeReceivablesAccountID;
        [AccountReference("Trade and Finance Relations", "Financer Payable")]
        public int financePayablesAccountID;

        [CostCenterReference("Trade and Finance Relations", "Pending Orders")]
        public int onSupplierOrderCostCenterID;
        [CostCenterReference("Trade and Finance Relations", "Pending Deliveries")]
        public int onCustomerOrderCostCenterID;
        
        
        [AccountReference("Fixed Asset", "Original Fixed Asset Value")]
        public int OriginalFixedAssetAccountID;
        [AccountReference("Fixed Asset", "Fixed Asset Deperciation")]
        public int FixedAssetDepreciationAccountID;
        [AccountReference("Fixed Asset", "Accumulated Depreciation")]
        public int FixedAssetAccumulatedDepreciationAccountID;
        public bool depreciateBookValue = false;

        public int AttachmentNumber; //Attachment reference number
        public string AttachmentNumberFormat; //E.g '000000'
        public string BankAccountSuffixCodeFormat;
        public string CashAccountSuffixCodeFormat;

        public string AllowNegativeTransaction;

        public CompanyProfile companyProfile;

        public double CommonVATRate;
        public double ExportVATRate;
        public double RawHideExportVATRate;
        public double VATWithholdingNonTaxablePriceLimit;

        public double GoodsTaxableRate;
        public ServicesTaxable ServicesTaxable;

        public double WHGoodsNonTaxablePriceLimit;
        public double WHGoodsTaxableRate;

        public double WHServicesNonTaxablePriceLimit;
        public double WHServicesTaxableRate;

        public double UnknownTINWithHoldingRate;

        [AccountReference("Tax", "Input VAT")]
        public int inputVATAccountID;
        [AccountReference("Tax", "Accumulated VAT Receivable")]
        public int accumulatedReceivableVATAccountID;
        [AccountReference("Tax", "Output VAT")]
        public int outPutVATAccountID;
        [AccountReference("Tax", "Withheld Output VAT")]
        public int outputVATWithHeldAccountID;
        [AccountReference("Tax", "Paid Witholding Tax")]
        public int paidWithHoldingTaxAccountID;
        [AccountReference("Tax", "Withholding Tax as Expense")]
        public int withholdingExpenseAccountID;
        [AccountReference("Tax", "Collected Witholding Tax")]
        public int collectedWithHoldingTaxAccountID;
        [AccountReference("Tax", "Paid TOT")]
        public int TOTExpenseAccountID;
        [AccountReference("Tax", "Collected TOT")]
        public int collectedTOTAccountID;
        [AccountReference("Tax", "Paid Custom Duty")]
        public int customDutyTaxExpenseAccountID;
        [AccountReference("Tax", "Excise Tax")]
        public int exciseTaxExpenseAccountID;
        [AccountReference("Tax", "Sur Tax")]
        public int surTaxExpenseAccountID;
        [AccountReference("Tax", "Tax Penality Account")]
        public int taxPenalityAcountID;
        [AccountReference("Tax", "Tax Interest Account")]
        public int taxInterestAccountID;
        [AccountReference("Tax", "Tax Exemption")]
        public int taxExemptionsAccountID;

        [AccountReference("Tax", "Input VAT(Not Invoiced)")]
        public int inputVATAccountIDNotInvoiced;
        [AccountReference("Tax", "Output VAT (Not Invoiced)")]
        public int outPutVATNotInoicedAccountID;
        [AccountReference("Tax", "Withheld Output VAT (Not Invoiced)")]
        public int outputVATWithHeldNotInvoicedAccountID;
        [AccountReference("Tax", "Paid Witholding Tax(Not Invoiced)")]
        public int paidWithHoldingTaxNotInvoicedAccountID;
        [AccountReference("Tax", "Collected Witholding Tax (Not Invoiced)")]
        public int collectedWithHoldingTaxNotInvoicedAccountID;




        public string taxDeclarationPeriods;

        [AccountReference("Z-Report", "Taxable Account")]
        public int zReportTaxableAccountID;
        [AccountReference("Z-Report", "None Taxable")]
        public int zReportNonTaxableAccountID;

        [AccountReference("Owners", "Sole Proprioter Withdrawal")]
        public int WithdrawalAccountID;
        public int otherIncomeAccountID;

        //System required formula and components
        public int GrossSalaryPayrollComponentID;
        public int GrossSalaryPayrollFormulaID;

        public int TransportAllowancePayrollComponentID;
        public int TransportAllowancePayrollFormulaID;

        public int TaxableTransportAllowancePayrollComponentID;
        public int TaxableTransportAllowancePayrollFormulaID;

        public string otherBenfitsFormulaIDs;
        public int IncomeTaxPayrollComponentID;
        public int IncomeTaxPayrollFormulaID;

        public int EmployeePensionPayrollComponentID;
        public int EmployeePensionPayrollFormulaID;

        public int EmployerPensionPayrollComponentID;
        public int EmployerPensionPayrollFormulaID;

        public int CostSharingPayrollComponentID;
        public int CostSharingPayrollFormulaID;

        public int OverTimePayrollComponentID;
        public int OverTimePayrollFormulaID;

        public int SingleValuePayrollComponentID;
        public int SingleValuePayrollFormulaID;

        public int OtherBenefitsPayrollComponentID;
        public int OtherBenefitsPayrollFormulaID;
        
        public int RegularTimePayrollComponentID;
        public int RegularTimePayrollFormulaID;
        public int permanentBenefitComponentID;
        
           

        public SummaryInformation reportBalanceSheet1;
        public SummaryInformation reportBalanceSheet2;
        public SummaryInformation reportBalanceSheet3;
        public SummaryInformation reportBalanceSheet4;
        public SummaryInformation reportIncomeStatement1;
        public SummaryInformation reportIncomeStatement2;
        public SummaryInformation reportIncomeStatement3;
        public SummaryInformation reportIncomeStatement4;
        public SummaryInformation reportCashFlow1;
        public SummaryInformation reportCashFlow2;
        public SummaryInformation reportCashFlow3;
        public SummaryInformation reportCashFlow4;
        public SummaryInformation reportDashBoard;

        public SummaryInformation budgetItems;

        

        [AccountReference("Cash and Bank", "Bank Accounts")]
        public int mainBankAccountID;
        [AccountReference("Cash and Bank", "Bank Account Service Charge Accounts")]
        public int bankServiceChargeAccountID;
        [AccountReference("Cash and Bank", "Bank Service Charge with Cash")]
        public int bankServiceChargeByCash;
        [AccountReference("Cash and Bank", "Cash Accounts")]
        public int cashAccountID;

        public AccountPeriodStyle accountingPeriodStyle;

        
        [AccountReference("Inventory", "Income from inventory count")]
        public int inventoryIncomeAccount = -1;
        [AccountReference("Inventory", "Loss from inventory count")]
        public int inventoryExpenseAccount = -1;
        [AccountReference("Inventory", "Combined Material Inflow")]
        public int materialInFlowAccountID;
        [AccountReference("Inventory", "Receivable from Internal Services")]
        public int internalServiceReceivableAccountID = -1;
        [AccountReference("Inventory", "Payable for Internal Services")]
        public int internalServicePayableAccountID = -1;


        [CostCenterReference("Accounting Center", "Main Accounting Center")]
        public int mainCostCenterID;//used as default account, headquarter cost center account must be this one    }

        public string cashInstrumentCode;
        public string bankDepositInstrumentCode="401002";
        public bool summerizeItemTransactions = false;
        [AccountReference("Summerization", "Summerization Contra")]
        public int summerizationContraAccountID= -1;


        public string[] summaryRoots = new string[] { "ITEM", "CUST","BUDGET" };
        
        [AccountReference("Book Closing", "Closing Transaction Account")]
        public int closingAccountID = -1;

        public bool manualItemCode = false;
        public bool manualPropertyCode = false;

        public string propertyCodeFormat="000000";

        [CostCenterReference("Accounting Center", "Employee Costcenter")]
        public int employeeCostCenterID;

        
    }
    [Serializable]
    [LoadFromSetting]
    public class ServicesTaxable
    {
        public double ContractorsTaxableRate;
        public double MillServicesTaxableRate;
        public double TractorsAndHarvTaxableRate;
        public double OtherServicesTaxableRate;
    }
}
