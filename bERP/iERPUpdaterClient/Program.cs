﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace iERPUpdaterClient
{
    static class Program
    {
        
        static void Main(string[] args)
        {
            bool killed;
            do
            {
                killed=false;
                foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcesses())
                {
                    if (p.ProcessName.ToUpper() == "ICASHERFRONTEND.EXE"
                        || p.ProcessName.ToUpper() == "ICASHERFRONTEND.EXE*32"
                        || p.ProcessName.ToUpper() == "ICASHERFRONTEND"

                        || p.ProcessName.ToUpper()=="BILLINGCLIENT.EXE"
                        || p.ProcessName.ToUpper() == "BILLINGCLIENT.EXE*32"
                        || p.ProcessName.ToUpper()=="BILLINGCLIENT"

                        || p.ProcessName.ToUpper() == "CUSTOMERSERVICES.EXE"
                        || p.ProcessName.ToUpper() == "CUSTOMERSERVICES.EXE*32"
                        || p.ProcessName.ToUpper() == "CUSTOMERSERVICES"

                        || p.ProcessName.ToUpper() == "PAYMENT.EXE"
                        || p.ProcessName.ToUpper() == "PAYMENT.EXE*32"
                        || p.ProcessName.ToUpper() == "PAYMENT"

                        || p.ProcessName.ToUpper() == "WFWINCLIENT.EXE"
                        || p.ProcessName.ToUpper() == "WFWINCLIENT.EXE*32"
                        || p.ProcessName.ToUpper() == "WFWINCLIENT"

                        || p.ProcessName.ToUpper() == "ADMINISTRATOR.EXE"
                        || p.ProcessName.ToUpper() == "ADMINISTRATOR.EXE*32"
                        || p.ProcessName.ToUpper() == "ADMINISTRATOR"

                        || p.ProcessName.ToUpper() == "SUBSCRIBERMANAGMENTCLIENT.EXE"
                        || p.ProcessName.ToUpper() == "SUBSCRIBERMANAGMENTCLIENT.EXE*32"
                        || p.ProcessName.ToUpper() == "SUBSCRIBERMANAGMENTCLIENT"
                        )
                    {
                        Console.WriteLine("Killing bERP Processes");
                        p.Kill();
                        killed = true;
                    }
                }
                if (killed)
                {
                    System.Threading.Thread.Sleep(200);
                }
            }
            while (killed);
            Console.WriteLine("Copying updated files");
            try
            {
                string clientFolder= Application.StartupPath+ "\\" + System.Configuration.ConfigurationManager.AppSettings["clientFolder"];
                foreach (string f in System.IO.Directory.GetFiles(Application.StartupPath,"*.*",System.IO.SearchOption.AllDirectories))
                {
                    string fn=f.Substring(Application.StartupPath.Length+1);
                    Console.WriteLine("Copying " + fn);
                    
                    string pathPart = "";
                    string[] path = fn.Split('\\');
                    for (int j = 0; j < path.Length - 1; j++)
                    {
                        pathPart += path[j] + "\\";
                        if (!System.IO.Directory.Exists(clientFolder + pathPart))
                            System.IO.Directory.CreateDirectory(clientFolder + pathPart);
                    }

                    System.IO.File.Copy(f,fn = clientFolder + fn,true);
                }
                Console.WriteLine("Starting bERP Client");
                if(args.Length==0)
                    System.Diagnostics.Process.Start(clientFolder + "iCasherFrontend.exe");
                else
                    System.Diagnostics.Process.Start(args[0]);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Failed to apply updates after they are download. Your bERP might be incorrect version.\n"+ex.Message);
            }
            
        }
    }
}
