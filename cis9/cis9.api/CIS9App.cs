﻿using intaps.cis.data;
using intaps.cis.domain.Admin;
using intaps.cis.domain.Archive;
using intaps.cis.domain.Extensions;
using intaps.cis.domain.Infrastructure;
using intaps.cis.domain.Infrastructure.Database;
using intaps.cis.domain.Workflows;
using intaps.cis.domain.Workflows.CaseWorkflow;
using intaps.cis.domain.Workflows.DamsWorkflow;
using intaps.cis.domain.Workflows.FileRequest;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace intaps.cis.api
{
    public class CIS9App
    {
        public static void InitWebApp(IServiceCollection services, IConfiguration Configuration)
        {
            //Add Npgsql as provider
            var connString = Configuration.GetConnectionString("cis_context");
            services.Configure<DamsOptions>(Configuration.GetSection("DamsOptions"));
            services.Configure<HostConfiguration>(Configuration.GetSection("HostConfiguration"));
            services.AddEntityFrameworkNpgsql().AddDbContext<CisContext>(options => { options.UseNpgsql(connString); });

            //Dependency Injection
            services.AddTransient<ICisConnection, CisConnection>();
            services.AddTransient<IUserFacade, UserFacade>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IWorkflowService, WorkflowService>();
            services.AddTransient<IArchiveService, ArchiveService>();
            services.AddTransient<IDamsWorkflowService, DamsWorkflowService>();
            services.AddTransient<ILookupService, LookupService>();
            services.AddTransient<ICaseWorkflowService, CaseWorkflowService>();
            services.AddTransient<IFileRequestService, FileRequestService>();
        }
    }
}
