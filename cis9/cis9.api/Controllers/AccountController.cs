﻿using System;
using System.Threading.Tasks;
using intaps.cis.data.Entities;
using intaps.cis.domain.Admin;
using intaps.cis.domain.Extensions;
using intaps.cis.domain.Infrastructure;
using intaps.cis.Extensions;
using intaps.cis.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;

namespace intaps.cis.Controllers
{
    [Route("api/dams/[controller]/[action]")]
    [ApiController]
    public class AccountController : BaseController
    {
        private IUserFacade _userFacade;
        private string CityName;
        private string OrganizationName;
        private HostConfiguration Config;

        public AccountController(IUserFacade facade, IOptions<HostConfiguration> config)
        {
            _userFacade = facade;
            Config = config.Value;
        }

        [HttpGet]
        public HostConfiguration PageLoad()
        {
            return Config;
        }
        [HttpPost]
        public IActionResult Login([FromBody]LoginViewModel loginViewModel)
        {
            if (!ModelState.IsValid) return Json(false);

            try
            {
                    var session = new UserSession()
                    {
                        UserName = loginViewModel.Username
                    };
                
                    _userFacade.LoginUser(session, loginViewModel);
                    HttpContext.Session.SetSession("sessionInfo", new UserSession()
                    {
                        UserName = loginViewModel.Username,
                        CreatedTime = DateTime.Now,
                        LastSeen = DateTime.Now,
                        Role = loginViewModel.Role
                    });
                
       

                return Json(new {message = "success"});
            }

            catch (Exception e)
            {
                return StatusCode(401, new {message = e.Message});
            }
        }
        [HttpGet]
        public IActionResult GetSubscriber([FromQuery] string contractNo)
        {
            try
            {
                var session = GetSession();
                var customerName =
                    _userFacade.GetSubscription(Config.WSISHost, session.WSISSessionId, contractNo);
                return Json(customerName);
            }
            catch (Exception e)
            {
                return StatusCode(500, new {message = e.Message});
            }
        }
        [HttpGet]
        public IActionResult GetEmployee([FromQuery] string username)
        {
            try
            {
                var fullname = _userFacade.GetEmployee(username);
                return Json(fullname);
            }
            catch (Exception e)
            {
                return StatusCode(500, new {message = e.Message});
            }
        }
        [HttpGet]
        public IActionResult GetWSISEmployees()
        {
            try
            {
                var session = GetSession();
                var employees =
                    _userFacade.GetWSISEmployees(session,Config.WSISHost);
                return Json(employees);
            }
            catch (Exception e)
            {
                return StatusCode(500, new {message = e.Message});
            }
        }
        [HttpPost]
        public IActionResult LoginWSIS([FromBody]LoginViewModel loginViewModel)
        {
            if (!ModelState.IsValid) return Json(false);

            try
            {
                var session = new UserSession()
                {
                    UserName = loginViewModel.Username
                };
                    var responseModel=_userFacade.LoginWSISUser(session, loginViewModel, Config.WSISHost);
                HttpContext.Session.SetSession("sessionInfo", new UserSession()
                {
                    UserName = loginViewModel.Username,
                    CreatedTime = DateTime.Now,
                    LastSeen = DateTime.Now,
                    Role = loginViewModel.Role,
                    WSISSessionId = responseModel.SessionID,
                    UserId = responseModel.UserID
                });

                
              

                return Json(new {message = "success"});
            }

            catch (Exception e)
            {
                return StatusCode(401, new {message = e.Message});
            }
        }
        [HttpGet]
        public IActionResult GetWSISUsers()
        {
            try
            {
                var session = GetSession();
                var users = _userFacade.GetWSISUsers(session, Config.WSISHost);

                return Json(users);
            }
            catch (Exception e)
            {
                return StatusCode(500, new {Message = e.Message});
            }
            
        }
        [HttpGet]
        [Roles]
        public IActionResult GetRoles()
        {    
            try
            {
                var session = GetSession();
                var roles = _userFacade.GetRoles(session, Config.ApplicationFor);
                return Json(roles);

            }            
            catch(Exception e)
            {
                return StatusCode(500, new {message = e.Message});
            } 
        }

        [HttpPost]
        [Roles]
        public IActionResult SetRole([FromBody] JObject userRole)
        {
            var role = (int) userRole["role"];
            var session = GetSession();
            
            HttpContext.Session.SetSession("sessionInfo", new UserSession()
            {
                UserName = session.UserName,
                CreatedTime = DateTime.Now,
                LastSeen = DateTime.Now,
                Role = role,
                WSISSessionId = session.WSISSessionId,
                UserId = session.UserId
            });

            return Json(new {message = "success"});
        }
            
        [Roles(UserRoles.Admin)]
        [HttpPost]    
        public IActionResult Register([FromBody]RegisterViewModel userModel)
        {
            if (!ModelState.IsValid) return Json(false);

            try
            {
                var session = GetSession();
                _userFacade.RegisterUser(session, userModel);
                return StatusCode(200, new {message = true});

            }            
            catch(Exception e)
            {
                return StatusCode(500, new {message = e.Message});
            }
           

        }


        [Roles]
        public IActionResult ChangePassword([FromBody] ChangePasswordViewModel changePassVm)
        {
            if (!ModelState.IsValid) return Json(false);

            try
            {
                var session = GetSession();
                _userFacade.ChangePassword(session, session.UserName, changePassVm.OldPassword, changePassVm.NewPassword);
                return Json(new {message = "success"});

            }            
            catch(Exception e)
            {
                return Json(new {errorCode = e.GetType().Name, message = e.Message}); 
            } 
        }

        [Roles(UserRoles.Admin)]
        public IActionResult ResetPassword([FromBody] ResetPasswordViewModel resetVm)
        {
            if (!ModelState.IsValid) return Json(false);

            try
            {
                _userFacade.ResetPassword(GetSession(),resetVm.UserName, resetVm.NewPassword);
                return Json(new {message = "success"});

            }            
            catch(Exception e)
            {
                return Json(new {errorCode = e.GetType().Name, message = e.Message}); 
            }  
        }

        [HttpPost]
        public IActionResult UpdateRole([FromBody] WsisUserViewModel roleVm)
        {
            if (!ModelState.IsValid) return Json(false);
            try
            {
                _userFacade.UpdateRole(GetSession(), roleVm);
                return Json(new {message = "success"});
            }
            catch (Exception e)
            {
                return Json(new {errorCode = e.GetType().Name, message = e.Message}); 
            }
        }
        [HttpPost]
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();

            return Json(true);
        }
    }
}