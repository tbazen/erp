﻿using System;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using intaps.cis.data;
using intaps.cis.data.Entities;
using intaps.cis.domain.Admin;
using intaps.cis.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sieve.Models;
using Sieve.Services;

namespace intaps.cis.Controllers
{
    [Route("api/dams/[controller]/[action]")]
    [ApiController]
    [Roles(UserRoles.Admin)]
    public class AuditController : BaseController
    {
        private IUserFacade _userFacade;

        private ISieveProcessor _sieveProcessor;

        private CisContext _cisContext;

        public AuditController(IUserFacade facade, ISieveProcessor sieveProcessor, CisContext context)
        {
            _userFacade = facade;
            _sieveProcessor = sieveProcessor;
            _cisContext = context;
        }

        [HttpGet]
        public IActionResult GetAudit([FromQuery]SieveModel sieveModel)
        {
            try
            {
                if(!ModelState.IsValid) throw new InvalidOperationException("Model not valid");

                var actions = _userFacade.GetAllActions(GetSession(), sieveModel.Page.Value, sieveModel.PageSize.Value).ToList();

                var count = _cisContext.UserAction.Count().ToString();
                Request.HttpContext.Response.Headers.Add("X-Total-Count", count);
                Request.HttpContext.Response.Headers.Add("Access-Control-Expose-Headers", "X-Total-Count");
                   
                return Json(actions);
            }
            catch (Exception e)
            {
                return StatusCode(500, new {message = e.Message});
            } 
        }


        [HttpGet]
        public IActionResult SearchAudits([FromQuery]ActionSearch actionSearch)
        {
            try
            {
                if(!ModelState.IsValid) throw new InvalidOperationException("Model not valid");

                var actions = _userFacade.SearchActions(GetSession(),actionSearch);

                var actionTypes = _cisContext.ActionType.AsNoTracking();
                
                var count = actions.Count().ToString();
                Request.HttpContext.Response.Headers.Add("X-Total-Count", count);
                Request.HttpContext.Response.Headers.Add("Access-Control-Expose-Headers", "X-Total-Count");

                var paginated = actions.Skip((actionSearch.Page - 1) * actionSearch.PageSize).Take(actionSearch.PageSize);

                var result  = paginated.Select(r => new
                {
                    UserName = r.Username,
                    Action = actionTypes.First(at => at.Id == r.ActionType),
                    ActionTime = new DateTime(r.Timestamp).ToShortDateString()
                });
                
                return Json(result.ToList());
            }
            catch (Exception e)
            {
                return StatusCode(500, new {message = e.Message});
            }
        }
        
        
        
    }
}