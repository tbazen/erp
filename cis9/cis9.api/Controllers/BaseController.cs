﻿using intaps.cis.domain.Infrastructure;
using intaps.cis.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace intaps.cis.Controllers
{
    public class BaseController : Controller
    {
        
        protected UserSession GetSession()
        {
            return HttpContext.Session.GetSession<UserSession>("sessionInfo");
        }

        public IActionResult Index()
        {
            
            return View("Index");
        }
    }
}