﻿using System;
using System.Linq;
using intaps.cis.data;
using intaps.cis.domain.Archive;
using intaps.cis.domain.Extensions;
using intaps.cis.domain.Workflows;
using intaps.cis.domain.Workflows.CaseWorkflow;
using intaps.cis.domain.Workflows.DamsWorkflow;
using intaps.cis.domain.Workflows.Models;
using intaps.cis.Filters;
using intaps.cis.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace intaps.cis.Controllers
{
    [Roles]
    [Route("api/dams/[controller]/[action]")]
    [ApiController]
    public class CaseController : BaseController
    {
        private ICaseWorkflowService _caseWorkflowService;
        private IWorkflowService _workflowService;
        private CisContext _cisContext;
        private IArchiveService _archiveService;
        private HostConfiguration Config;

        public CaseController(IWorkflowService service, ICaseWorkflowService caseService, CisContext context, IArchiveService archiveService,  IOptions<HostConfiguration> config)
        {
            _cisContext = context;
            
            _workflowService = service;
            _caseWorkflowService = caseService;
            _archiveService = archiveService;
            
            _workflowService.SetContext(_cisContext);
            _caseWorkflowService.SetContext(_cisContext);
            _archiveService.SetContext(_cisContext);
            Config = config.Value;
        }


        [HttpGet]
        public IActionResult GetCase([FromQuery] string id)
        {
            var caseModel = _caseWorkflowService.GetCase(id);
            var notes = _workflowService.GetNotes(caseModel.Workflow);

            return Json(new CaseVm {CaseModel = caseModel, Notes = notes});
        }

        [HttpGet]
        public IActionResult GetCaseByDoc([FromQuery] string docId)
        {
            var caseModel = _caseWorkflowService.GetCasebyDoc(docId);

            return Json(caseModel);
        }

        [HttpPost]
        public IActionResult OpenCase([FromBody] CaseDetail caseVm)
        {
            
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid) throw new InvalidOperationException("Case Malformed");


                    var session = GetSession();
                    DocumentModel caseDoc = null; 
                    if(caseVm.Document!=null)
                        caseDoc=_archiveService.GetDocument(caseVm.Document);
                    FileModel caseFile = null;
                    if(caseDoc!=null)
                       caseFile= _archiveService.GetDocFile(caseDoc.FileId);
                    
                    var caseFileOwner="";
                    var caseFileUpin = "";
                    if (caseFile != null)
                    {
                        caseFileOwner = caseFile.FileOwner;
                        caseFileUpin = caseFile.Upin;
                    }
                    var workflowModel = new WorkflowModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Title = $"{caseVm.Title} ({caseFileOwner}, {caseFileUpin})",
                        Type = _cisContext.WorkflowType.First(wt => wt.Id == 2)
                    };

                    var workItem = new WorkItemModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        AssignedRole = -1,
                        AssignedUser = session.UserName,
                        Workflow = workflowModel.Id,
                        DataType = typeof(CaseDetail).FullName
                    };
                
                    
                    //set current_workitem
                    workflowModel.CurrentWorkItem = workItem.Id;
                
                    caseVm.Id = Guid.NewGuid().ToString();
                    caseVm.Workflow = workflowModel.Id;
                    caseVm.CaseNumber = _caseWorkflowService.GetCaseNumber();
                    caseVm.Date = DateTime.Now.ToShortDateString();
                    caseVm.CreatedBy = GetSession().UserName;

                    workItem.Data = JsonConvert.SerializeObject(caseVm);

                    var caseWorkflow = new CaseWorkflow(_caseWorkflowService, (int) State.Initial, GetSession(), Config.ApplicationFor);
                    caseWorkflow.OpenCase(workflowModel, workItem);
                
                    transaction.Commit();

                    return Json(new {message = "Success"});
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
   
        }

        [HttpPost]
        public IActionResult ForwardCase([FromBody] ForwardCaseVm model)
        {
            
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid) throw new InvalidOperationException("Case model malformed");

                    var workflowModel = _workflowService.GetWorkflow(model.CaseWorkflow);
                    var workItem = _workflowService.GetWorkItems(workflowModel.Id)[0];

                    CaseWorkflow caseWorkflow =
                        new CaseWorkflow(_caseWorkflowService, workflowModel.CurrentState, GetSession(), Config.ApplicationFor);

                    workItem.AssignedUser = model.Assignee;

                    var workItemNote = new WorkItemNote()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Date = DateTime.Now.ToLongDateString(),
                        Note = model.Comment,
                        Workitem = workItem.Id,
                        Username = GetSession().UserName,
                        ForwardTo = model.Assignee
                    };

                    caseWorkflow.ForwardCase(workItem, workItemNote);
                
                    transaction.Commit();

                    return Json(new {message = "Case Forwarded"});
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
   
        }

        [HttpPost]
        public IActionResult CloseCase([FromBody] JObject caseModel)
        {
            
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid) throw new InvalidOperationException("Case model malformed");

                    var id = (string) caseModel["id"];

                    int state = _workflowService.GetWorkflow(id).CurrentState;

                    CaseWorkflow caseWorkflow = new CaseWorkflow(_caseWorkflowService, state, GetSession(), Config.ApplicationFor);

                    caseWorkflow.CloseCase(id, (int) State.Closed);
                
                    transaction.Commit();

                    return Json(new {message = "Case Closed"});
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }

        }

        [HttpPost]
        public IActionResult CancelCase([FromBody] JObject caseModel)
        {
            
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid) throw new InvalidOperationException("Case model malformed");

                    var id = (string) caseModel["id"];

                    int state = _workflowService.GetWorkflow(id).CurrentState;

                    CaseWorkflow caseWorkflow = new CaseWorkflow(_caseWorkflowService, state, GetSession(), Config.ApplicationFor);

                    caseWorkflow.CloseCase(id, (int) State.Closed);
                
                    transaction.Commit();

                    return Json(new {message = "Case Cancelled"});
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
            
      
        }
    }
}