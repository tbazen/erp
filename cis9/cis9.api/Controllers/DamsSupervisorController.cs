﻿using System;
using intaps.cis.data;
using intaps.cis.domain.Admin;
using intaps.cis.domain.Extensions;
using intaps.cis.domain.Workflows;
using intaps.cis.domain.Workflows.DamsWorkflow;
using intaps.cis.domain.Workflows.Models;
using intaps.cis.Filters;
using intaps.cis.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;

namespace intaps.cis.Controllers
{
    [Roles(UserRoles.DAMSSupervisor)]
    [Route("api/dams/[controller]/[action]")]
    [ApiController]
    public class DamsSupervisorController : BaseController
    {
        private readonly IDamsWorkflowService _damsWorkflowService;
        private readonly IWorkflowService _workflowService;

        private CisContext _cisContext;
        private HostConfiguration Config;
 
        public DamsSupervisorController(IWorkflowService service, IDamsWorkflowService damsWorkflowService, CisContext context, IOptions<HostConfiguration> config)
        {
            _workflowService = service;
            _damsWorkflowService = damsWorkflowService;

            _cisContext = context;
            
            _workflowService.SetContext(_cisContext);
            _damsWorkflowService.SetContext(_cisContext);
            Config = config.Value;
        }
        
       
        [HttpPost]
        public IActionResult ApproveRequest([FromBody] JObject workflowId)
        {
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    if(!ModelState.IsValid) throw new InvalidOperationException("Workflow is malformed");
                    
                    string id = (string) workflowId["id"];
                
                    var workflow = _workflowService.GetWorkflow(id);
                
                    var damsWorkflow = new DamsWorkflow(_damsWorkflowService, workflow.CurrentState, GetSession(), Config.ApplicationFor);
                
                
                    damsWorkflow.ApproveRequest(workflow);
                
                    transaction.Commit();
                
                    return Json(new {message = "Approved!"});
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
            
        }

        [HttpPost]
        public IActionResult RejectFileRequest([FromBody] RejectRequest rejectRequest)
        {
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    if(!ModelState.IsValid) throw new InvalidOperationException("Workflow is malformed");
                    
                    var workflow = _workflowService.GetWorkflow(rejectRequest.Workflow);
                
                    var damsWorkflow = new DamsWorkflow(_damsWorkflowService, workflow.CurrentState, GetSession(), Config.ApplicationFor);
                
                    var workItems = _workflowService.GetWorkItems(workflow.Id);
                
                    foreach (var workItem in workItems)
                    {
                        workItem.AssignedRole = UserRoles.Digitizer;
                    }
                
                    var workItemNote = new WorkItemNote()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Note = rejectRequest.Note,
                        Username = GetSession().UserName,
                        Workitem = rejectRequest.WorkItem
                    };
                
                    var rejectWorkItem = new WorkItemModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        AssignedRole = UserRoles.Digitizer,
                        Type = (int)DamsTriggers.RejectFileApproval,
                        Workflow = workflow.Id
                    };
                
                    _workflowService.SetSession(GetSession());
                    _workflowService.AddWorkItem(rejectWorkItem);
                    damsWorkflow.RejectFileApproveRequest(workflow.Id, workItems,workItemNote);

                    transaction.Commit();
                
                    return Json(new {message = "File Rejected!"});
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
   
        }
        
        [HttpPost]
        public IActionResult RejectDocRequest([FromBody] RejectRequest rejectRequest)
        {
            
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    if(!ModelState.IsValid) throw new InvalidOperationException("Workflow is malformed");
                    
                    var workflow = _workflowService.GetWorkflow(rejectRequest.Workflow);
                
                    var damsWorkflow = new DamsWorkflow(_damsWorkflowService, workflow.CurrentState, GetSession(), Config.ApplicationFor);
                
                    var workItems = _workflowService.GetWorkItems(workflow.Id);
                
                    foreach (var workItem in workItems)
                    {
                        workItem.AssignedRole = UserRoles.Digitizer;
                    }
                
                    var workItemNote = new WorkItemNote()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Note = rejectRequest.Note,
                        Username = GetSession().UserName,
                        Workitem = rejectRequest.WorkItem
                    };
                
                    var rejectWorkItem = new WorkItemModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        AssignedRole = UserRoles.Digitizer,
                        Type = (int)DamsTriggers.RejectDocApproval,
                        Workflow = workflow.Id
                    };
                
                    _workflowService.SetSession(GetSession());
                    _workflowService.AddWorkItem(rejectWorkItem);
                    damsWorkflow.RejectDocRequest(workflow.Id, workItems,workItemNote);
                
                    transaction.Commit();

                    return Json(new {message = "File Rejected!"});
                    
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
        
        }

        [HttpPost]
        public IActionResult CancelRequest([FromBody] JObject workflowId)
        {
            
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    if(!ModelState.IsValid) throw new InvalidOperationException("Workflow is malformed");
                    
                    string id = (string) workflowId["id"];
                
                    var workflow = _workflowService.GetWorkflow(id);
                
                    var damsWorkflow = new DamsWorkflow(_damsWorkflowService, workflow.CurrentState, GetSession(), Config.ApplicationFor);
                
                
                    damsWorkflow.Cancel(workflow.Id);
                
                    transaction.Commit();

                    return Json(new {message = "Cancelled!"});
                    
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
            
        }
    }
}