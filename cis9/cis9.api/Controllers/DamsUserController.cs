﻿using System;
using System.Collections.Generic;
using System.Linq;
using intaps.cis.data;
using intaps.cis.domain.Admin;
using intaps.cis.domain.Extensions;
using intaps.cis.domain.Workflows;
using intaps.cis.domain.Workflows.DamsWorkflow;
using intaps.cis.domain.Workflows.Models;
using intaps.cis.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace intaps.cis.Controllers
{
    [Roles(UserRoles.DAMSUser)]
    [Route("api/dams/[controller]/[action]")]
    [ApiController]
    public class DamsUserController : BaseController
    {
        private IDamsWorkflowService _damsWorkflowService;
        private IWorkflowService _workflowService;
        private CisContext _cisContext;
        private HostConfiguration Config;

        public DamsUserController(IDamsWorkflowService workflowSerivce, IWorkflowService service, CisContext cisContext, IOptions<HostConfiguration> config)
        {
           _damsWorkflowService = workflowSerivce;
            _workflowService = service;

            _cisContext = cisContext;
            
            _damsWorkflowService.SetContext(_cisContext);
            _workflowService.SetContext(_cisContext);
            Config = config.Value;
        }

        [HttpPost]
        public IActionResult AddFile([FromBody] FileModel file)
        {
     
                using (var transaction = _cisContext.Database.BeginTransaction())
                {
                    try
                    {
                        var workflowModel = new WorkflowModel()
                        {
                            Id = Guid.NewGuid().ToString(),
                            Title = $"{file.FileOwner}({file.Upin})",
                            Type = _cisContext.WorkflowType.First(wt => wt.Id == 1),
                            CurrentState = (int)State.DigitizingFile
                        };
            
                        var workItem = new WorkItemModel()
                        {
                            Id = Guid.NewGuid().ToString(),
                            AssignedRole = UserRoles.Digitizer,
                            AssignedUser = "",
                            DataType = typeof(FileModel).FullName,
                            Type = (int) DamsTriggers.AddFile,
                            Workflow = workflowModel.Id
                        };
                
                        //set current workitem
                        workflowModel.CurrentWorkItem = workItem.Id;

                        file.WorkItem = workItem.Id;
                        file.Id = Guid.NewGuid().ToString();

                        workItem.Data = JsonConvert.SerializeObject(file);
            
                        var damsWorklfow = new DamsWorkflow(_damsWorkflowService, (int)State.Initial, GetSession(), Config.ApplicationFor);
                        damsWorklfow.AddFile(workflowModel,workItem);
  
                        transaction.Commit();
                        
                        return Json(new {message = "Success"});
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        return StatusCode(500, new {mesage = e.Message});
                    }
                }

        }

        [HttpPost]
        public IActionResult EditFile([FromBody] JObject model)
        {

            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    var workflowId = (string) model["workflow"];
                    var file = model["file"].ToObject<FileModel>();
                
                    file.Documents = new List<DocumentModel>(); //DamsUser can't update documents

                    var state = _workflowService.GetWorkflowState(workflowId);
                
                    var damsWorkflow = new DamsWorkflow(_damsWorkflowService, state, GetSession(), Config.ApplicationFor);
                
                    var workItemModel = new WorkItemModel()
                    {
                        Id = file.WorkItem,
                        Data = JsonConvert.SerializeObject(file),
                    };
                
                    var workItems = _workflowService.GetWorkItems(workflowId);
                
                    foreach (var workItem in workItems)
                    {
                        if (workItem.Id.Equals(workItemModel.Id))
                        {
                            workItem.Data = workItemModel.Data;
                        }
                    
                        workItem.AssignedRole = UserRoles.Digitizer;
                    
                    }

                    damsWorkflow.EditFile(workflowId, workItems);
                    
                    transaction.Commit();
                    
                    return Json(new {message = "Success"});
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
            
        }
        
        [HttpPost]
        public IActionResult UpdateFile([FromBody]FileModel file)
        {
            
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    var workflowModel = new WorkflowModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Title = $"{file.FileOwner}({file.Upin})",
                        Type = _cisContext.WorkflowType.First(wt => wt.Id == 1),
                        CurrentState = (int)State.DigitizingFile
                    };
            
                    var workItem = new WorkItemModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        AssignedRole = UserRoles.Digitizer,
                        AssignedUser = "",
                        DataType = typeof(FileModel).FullName,
                        Type = (int) DamsTriggers.UpdateFile,
                        Workflow = workflowModel.Id
                    };
                
                    //set current workitem
                    workflowModel.CurrentWorkItem = workItem.Id;

                    file.WorkItem = workItem.Id;
           
                    workItem.Data = JsonConvert.SerializeObject(file);
            
                    var damsWorklfow = new DamsWorkflow(_damsWorkflowService, (int)State.Initial, GetSession(), Config.ApplicationFor);
                
                    damsWorklfow.UpdateFile(workflowModel,workItem);
                    
                    transaction.Commit();
                    
                    return Json(new {message = "Success"});
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
        }

        [HttpPost]
        public IActionResult UpdateFileInfo([FromBody] FileModel file)
        {
            
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    var workflowModel = new WorkflowModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Title = $"{file.FileOwner}({file.Upin})",
                        Type = _cisContext.WorkflowType.First(wt => wt.Id == 1),
                        CurrentState = (int)State.DigitizingFile
                    };
            
                    var workItem = new WorkItemModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        AssignedRole = UserRoles.DAMSSupervisor,
                        AssignedUser = "",
                        DataType = typeof(FileModel).FullName,
                        Type = (int) DamsTriggers.UpdateFileInfo,
                        Workflow = workflowModel.Id
                    };
                
                    //set current workitem
                    workflowModel.CurrentWorkItem = workItem.Id;

                    file.WorkItem = workItem.Id;
                
                    workItem.Data = JsonConvert.SerializeObject(file);
            
                    var damsWorklfow = new DamsWorkflow(_damsWorkflowService, (int)State.Initial, GetSession(), Config.ApplicationFor);
                
                    damsWorklfow.UpdateFileInfo(workflowModel,workItem);
                    
                    transaction.Commit();
                    
                    return Json(new {message = "Success"});
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
            
        }
        
        [HttpPost]
        public IActionResult DeleteFile([FromBody]FileModel file)
        {
            
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    var workflowModel = new WorkflowModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Title = $"{file.FileOwner}({file.Upin})",
                        Type = _cisContext.WorkflowType.First(wt => wt.Id == 4),
                        CurrentState = (int)State.Initial
                    };
            
                    var workItem = new WorkItemModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        AssignedRole = UserRoles.DAMSSupervisor,
                        AssignedUser = "",
                        DataType = typeof(FileModel).FullName,
                        Type = (int) DamsTriggers.RemoveFile,
                        Workflow = workflowModel.Id
                    };
                
                    //set current workitem
                    workflowModel.CurrentWorkItem = workItem.Id;
                
                    workItem.Data = JsonConvert.SerializeObject(file);
            
                    var damsWorklfow = new DamsWorkflow(_damsWorkflowService, (int)State.Initial, GetSession(), Config.ApplicationFor);
                
                    damsWorklfow.RemoveFile(workflowModel,workItem);
                
                
                    transaction.Commit();
                
                    return Json(new {message = "Success"});
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
        }
    }
}