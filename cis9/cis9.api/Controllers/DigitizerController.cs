﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using intaps.cis.data;
using intaps.cis.domain.Admin;
using intaps.cis.domain.Extensions;
using intaps.cis.domain.Workflows;
using intaps.cis.domain.Workflows.DamsWorkflow;
using intaps.cis.domain.Workflows.Models;
using intaps.cis.Filters;
using intaps.cis.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace intaps.cis.Controllers
{
    [Roles(UserRoles.Digitizer)]
    [Route("api/dams/[controller]/[action]")]
    [ApiController]
    public class DigitizerController : BaseController
    {
        private IDamsWorkflowService _damsWorkflowService;
        private IWorkflowService _workflowService;
        private Regex mimeTypeRegx = new Regex("(.+)/(.+)");
        private string fileLocation;

        private CisContext _cisContext;
        private HostConfiguration Config;

        public DigitizerController(IWorkflowService _service, IDamsWorkflowService damsWorkflowService, IOptions<DamsOptions> imagePath, CisContext context, IOptions<HostConfiguration> config)
        {
            _damsWorkflowService = damsWorkflowService;
            _workflowService = _service;
            fileLocation = imagePath.Value.ImagePath;

            _cisContext = context;
            
            _damsWorkflowService.SetContext(_cisContext);
            _workflowService.SetContext(_cisContext);
            Config = config.Value;
        }


        [HttpPost]
        public IActionResult FileUpdateDoc([FromBody] DocumentModel docModel)
        {
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid) throw new ArgumentNullException("Document workflow malformed");

                    var workflowModel = new WorkflowModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Title = docModel.ReferenceNo,
                        Type = _cisContext.WorkflowType.First(wt => wt.Id == 3),
                        CurrentState = (int) State.DigitizingDocument
                    };
                
                    var damsWorkflow = new DamsWorkflow(_damsWorkflowService, (int) State.Initial, GetSession(), Config.ApplicationFor);

                    var images = docModel.Images;
                    docModel.Images = new List<ImageModel>();

                    var workItem = new WorkItemModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        AssignedRole = UserRoles.DAMSSupervisor,
                        DataType = typeof(DocumentModel).FullName,
                        Type = (int) DamsTriggers.FileUpdateDoc,
                        Workflow = workflowModel.Id
                    };
                
                    //set current workitem
                    workflowModel.CurrentWorkItem = workItem.Id;
                
                    docModel.WorkItem = workItem.Id;

                    workItem.Data = JsonConvert.SerializeObject(docModel);
                
                    damsWorkflow.FileUpdateDoc(workflowModel, workItem);

                    if (images.Count > 0)
                    {
                        foreach (var image in images)
                        {
                            var imageData = image.Data;
                            image.Data = "";
                            var imageWorkItem = new WorkItemModel()
                            {
                                Id = image.WorkItem,
                                Data = JsonConvert.SerializeObject(image),
                                DataType = image.GetType().FullName,
                                Type = (int) DamsTriggers.AddImage,
                                AssignedRole = UserRoles.DAMSSupervisor,
                                Workflow = workflowModel.Id
                            };
                        
                            damsWorkflow.AddImage(imageWorkItem);
                        
                            SaveImage(image.Id, image.Mime, imageData);
                        }
                    }

                    transaction.Commit();
                
                    return Json(new {id = workflowModel.Id});
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
          
        }

        [HttpPost]
        public IActionResult AddDocument([FromBody] JObject model)
        {
            
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid) throw new ArgumentNullException("Document workflow malformed");

                    var workflowId = (string) model["workflow"];
                    var document = model["document"].ToObject<DocumentModel>();
                    

                    int state = _workflowService.GetWorkflow(workflowId).CurrentState;

                    var damsWorkflow = new DamsWorkflow(_damsWorkflowService, state, GetSession(), Config.ApplicationFor);

                    var images = document.Images;

                    document.Id = Guid.NewGuid().ToString();
                    document.TimeStamp = DateTime.Now.Ticks;
                    document.WorkItem = Guid.NewGuid().ToString();
                    document.Date = domain.Extensions.EtGrDate.ToGrig(domain.Extensions.EtGrDate.Parse(document.Date)).GridDate.ToString();
                    document.Images = new List<ImageModel>();

                    blockInvalidDocumentData(document);

                    var workItemModel = new WorkItemModel()
                    {
                        Id = document.WorkItem,
                        Data = JsonConvert.SerializeObject(document),
                        DataType = document.GetType().FullName,
                        Type = (int) DamsTriggers.AddDocument,
                        AssignedRole = UserRoles.Digitizer,
                        Workflow = workflowId
                    };

                    damsWorkflow.AddDocument(workItemModel);

                    if (images.Count != 0)
                    {
                        foreach (var image in images)
                        {
                            image.Id = Guid.NewGuid().ToString();
                            image.DocumentId = document.Id;
                            image.WorkItem = Guid.NewGuid().ToString();
                            var imageData = image.Data;
                            image.Data = "";

                            var imageWorkItem = new WorkItemModel()
                            {
                                Id = image.WorkItem,
                                Data = JsonConvert.SerializeObject(image),
                                DataType = image.GetType().FullName,
                                Type = (int) DamsTriggers.AddImage,
                                AssignedRole = UserRoles.Digitizer,
                                Workflow = workflowId
                            };
                        
                            damsWorkflow.AddImage(imageWorkItem);
                        
                            SaveImage(image.Id, image.Mime, imageData);
                        }

                        document.Images = images;
                    }
                
                    transaction.Commit();
                
                    return Json(document);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
        }

        private void blockInvalidDocumentData(DocumentModel document)
        {
            Guid guid;
            if (!Guid.TryParse(document.Id, out guid))
                throw new Exception("Invalid document guid:" + document.Id);

            if (!Guid.TryParse(document.FileId, out guid))
                throw new Exception("Invalid file guid:" + document.FileId);

            if(document.Type==null || document.Type.Id==0)
                throw new Exception("Invalid document type");
        }

        [HttpPost]
        public IActionResult FileUpdateAddDoc([FromBody] FileModel file)
        {
            
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid) throw new ArgumentNullException("File Model malformed");
                    
                    var workflowModel = new WorkflowModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Title = file.FileOwner,
                        Type = _cisContext.WorkflowType.First(wt => wt.Id == 1),
                        CurrentState = (int) State.DigitizingFile
                    };

                    var workItem = new WorkItemModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        AssignedRole = UserRoles.Digitizer,
                        AssignedUser = "",
                        DataType = typeof(FileModel).FullName,
                        Type = (int) DamsTriggers.FileUpdateAddDoc,
                        Workflow = workflowModel.Id
                    };
                
                    //set current workitem
                    workflowModel.CurrentWorkItem = workItem.Id;

                    file.WorkItem = workItem.Id;

                    workItem.Data = JsonConvert.SerializeObject(file);

                    var damsWorklfow = new DamsWorkflow(_damsWorkflowService, (int) State.Initial, GetSession(), Config.ApplicationFor);

                    damsWorklfow.FileUpdateAddDoc(workflowModel, workItem);
                
                
                    transaction.Commit();
                
                    return Json(workflowModel.Id);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
           
        }

        [HttpPost]
        public IActionResult UpdateDocument([FromBody] JObject model)
        {
            
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid) throw new ArgumentNullException("Document Model malformed");
                    
                    var workflowId = (string) model["workflow"];
                    var document = model["document"].ToObject<DocumentModel>();
                    int state = _workflowService.GetWorkflowState(workflowId);

                    var damsWorkflow = new DamsWorkflow(_damsWorkflowService, state, GetSession(), Config.ApplicationFor);

                    var images = document.Images;

                    document.TimeStamp = DateTime.Now.Ticks;
                    document.Images = new List<ImageModel>();
                    document.Date = domain.Extensions.EtGrDate.ToGrig(domain.Extensions.EtGrDate.Parse(document.Date)).GridDate.ToString();
                    var workItem = new WorkItemModel()
                    {
                        Id = document.WorkItem,
                        Data = JsonConvert.SerializeObject(document),
                        DataType = document.GetType().FullName,
                        Type = (int) DamsTriggers.UpdateDocument,
                        AssignedRole = UserRoles.Digitizer,
                        Workflow = workflowId
                    };


                    damsWorkflow.UpdateDocument(workItem);

                    if (images.Count != 0)
                    {
                        foreach (var image in images)
                        {
                            image.Id = Guid.NewGuid().ToString();
                            image.DocumentId = document.Id;
                            image.WorkItem = Guid.NewGuid().ToString();
                            var imageData = image.Data;
                            image.Data = "";

                            var imageWorkItem = new WorkItemModel()
                            {
                                Id = image.WorkItem,
                                Data = JsonConvert.SerializeObject(image),
                                DataType = image.GetType().FullName,
                                Type = (int) DamsTriggers.AddImage,
                                AssignedRole = UserRoles.Digitizer,
                                Workflow = workflowId
                            };

                            damsWorkflow.AddImage(imageWorkItem);
                            SaveImage(image.Id, image.Mime, imageData);
                        }
                    }
                
                    transaction.Commit();

                    return Json(new {message = "Succes"});
                    
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
       
        }

        [HttpPost]
        public IActionResult RemoveDocument([FromBody] JObject model)
        {
            
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {

                    if (!ModelState.IsValid) throw new InvalidOperationException("Document Model malformed");

                    string workflowId = (string) model["workflow"];
                    var document = model["document"].ToObject<DocumentModel>();

                    int state = _workflowService.GetWorkflowState(workflowId);

                    var damsWorkflow = new DamsWorkflow(_damsWorkflowService, state, GetSession(), Config.ApplicationFor);

                    damsWorkflow.RemoveDocument(document.WorkItem);

                    if (document.Images.Count != 0)
                    {
                        foreach (var image in document.Images)
                        {
                            damsWorkflow.RemoveImage(image.WorkItem);
                        
                            DeleteImage(image.Id, image.Mime);
                        }
                    }
                
                    transaction.Commit();
                
                    return Json( new {message = "Success"});
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
            
        }

        [HttpPost]
        public IActionResult UpdateImage([FromBody] JObject model)
        {
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid) throw new ArgumentNullException("Image Model malformed");
                    var workflowId = (string) model["workflow"];
                    var images = model["image"].ToObject<IList<ImageModel>>();
                    int state = _workflowService.GetWorkflowState(workflowId);
                    var damsWorkflow=new DamsWorkflow(_damsWorkflowService, state, GetSession(), Config.ApplicationFor);
                    foreach (var image in images)
                    {
                        image.Data = "";
                        var imageWorkItem = new WorkItemModel()
                        {
                            Id = image.WorkItem,
                            Data = JsonConvert.SerializeObject(image),
                            DataType = image.GetType().FullName,
                            Type = (int) DamsTriggers.AddImage,
                            AssignedRole = UserRoles.Digitizer,
                            Workflow = workflowId
                        };
                        damsWorkflow.UpdateImage(imageWorkItem);
                    }
                    
                    transaction.Commit();
                
                    return Json(new {message = "Success"});
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {message = e.Message});
                }
            }
        }
        [HttpPost]
        public IActionResult RemoveImage([FromBody] JObject model)
        {
            
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid) throw new InvalidOperationException("Image Model malformed");
                    
                    string workflowId = (string) model["workflow"];
                    var image = model["image"].ToObject<ImageModel>();

                    int state = _workflowService.GetWorkflowState(workflowId);


                    var damsWorkflow = new DamsWorkflow(_damsWorkflowService, state, GetSession(), Config.ApplicationFor);

                    damsWorkflow.RemoveImage(image.WorkItem);
                
                    DeleteImage(image.Id, image.Mime);
                
                    transaction.Commit();
                
                    return Json(new {message = "Success"});
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
   
        }

        [HttpPost]
        public IActionResult RequestDocApproval([FromBody] JObject workflowId)
        {

            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid) throw new InvalidOperationException("Workflow is malformed");
                    
                    string id = (string) workflowId["id"];

                    int state = _workflowService.GetWorkflow(id).CurrentState;

                    var workItems = _workflowService.GetWorkItems(id);

                    var damsWorkflow = new DamsWorkflow(_damsWorkflowService, state, GetSession(), Config.ApplicationFor);

                    foreach (var workItem in workItems)
                    {
                        workItem.AssignedRole = UserRoles.DAMSSupervisor;
                    }

                    var requestFile = new WorkItemModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        AssignedRole = UserRoles.DAMSSupervisor,
                        Type = (int) DamsTriggers.RequestDocAprroval,
                        Workflow = id
                    };
                    _workflowService.SetSession(GetSession());
                    _workflowService.AddWorkItem(requestFile);

                    damsWorkflow.SendDocApprovalRequest(id, workItems);
                
                    transaction.Commit();
                
                    return Json(new {message = "Success"});               
                    
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }

        }
        [HttpPost]
        public IActionResult RequestFileApproval([FromBody] JObject workflowId)
        {
            
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid) throw new InvalidOperationException("Workflow is malformed");
                    
                    string id = (string) workflowId["id"];

                    int state = _workflowService.GetWorkflow(id).CurrentState;

                    var workItems = _workflowService.GetWorkItems(id);

                    var damsWorkflow = new DamsWorkflow(_damsWorkflowService, state, GetSession(), Config.ApplicationFor);

                    foreach (var workItem in workItems)
                    {
                        workItem.AssignedRole = UserRoles.DAMSSupervisor;
                    }

                    var requestFile = new WorkItemModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        AssignedRole = UserRoles.DAMSUser,
                        Type = (int) DamsTriggers.RequestFileApproval,
                        Workflow = id
                    };
                    _workflowService.SetSession(GetSession());
                    _workflowService.AddWorkItem(requestFile);

                    damsWorkflow.SendFileApproveRequest(id, workItems);
                
                    transaction.Commit();
                
                    return Json(new {message = "Success"});
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
        }

        [HttpPost]
        public IActionResult RejectFile([FromBody] RejectRequest rejectRequest)
        {
            
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid) throw new InvalidOperationException("Workflow is malformed");
                    
                    var workflow = _workflowService.GetWorkflow(rejectRequest.Workflow);

                    var damsWorkflow = new DamsWorkflow(_damsWorkflowService, workflow.CurrentState, GetSession(), Config.ApplicationFor);

                    var workItems = _workflowService.GetWorkItems(workflow.Id);

                    foreach (var workItem in workItems)
                    {
                        workItem.AssignedRole = UserRoles.DAMSUser;
                    }

                    var workItemNote = new WorkItemNote()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Note = rejectRequest.Note,
                        Username = GetSession().UserName,
                        Workitem = rejectRequest.WorkItem
                    };

                    var rejectWorkItem = new WorkItemModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        AssignedRole = UserRoles.DAMSUser,
                        Type = (int) DamsTriggers.RejectFile,
                        Workflow = workflow.Id
                    };

                    _workflowService.SetSession(GetSession());

                    _workflowService.AddWorkItem(rejectWorkItem);

                    damsWorkflow.RejectFile(workflow.Id, workItems, workItemNote);
                
                    transaction.Commit();
                
                    return Json(new {message = "File Rejected!"});
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
   
        }
        
        private void SaveImage(string fileName, string mime, string encodedStr)
        {
            if (mimeTypeRegx.IsMatch(mime))
            {
                var imgEx = mimeTypeRegx.Match(mime).Groups[2].Value;
                var path = Path.Combine(fileLocation, $"{fileName}.{imgEx}");
                var byteArr = Convert.FromBase64String(encodedStr);

                BinaryWriter binaryWriter = null;

                try
                {
                    binaryWriter = new BinaryWriter(System.IO.File.OpenWrite(path));

                    binaryWriter.Write(byteArr);
                    binaryWriter.Flush();
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    binaryWriter?.Close();
                }
            }
            else
            {
                throw new InvalidOperationException("Mime type malformed");
            }
        }

        private void DeleteImage(string fileName, string mime)
        {
            if (mimeTypeRegx.IsMatch(mime))
            {
                var imgEx = mimeTypeRegx.Match(mime).Groups[2].Value;
                var path = Path.Combine(fileLocation, $"{fileName}.{imgEx}");
                
                System.IO.File.Delete(path);
            }
            else
            {
                throw new InvalidOperationException("Mime type malformed");
            }
        }
    }
}