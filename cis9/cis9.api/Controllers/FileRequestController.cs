﻿using System;
using System.Linq;
using intaps.cis.data;
using intaps.cis.domain.Archive;
using intaps.cis.domain.Extensions;
using intaps.cis.domain.Workflows;
using intaps.cis.domain.Workflows.DamsWorkflow;
using intaps.cis.domain.Workflows.FileRequest;
using intaps.cis.domain.Workflows.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace intaps.cis.Controllers
{
    [Route("api/dams/[controller]/[action]")]
    [ApiController]
    public class FileRequestController : BaseController
    {
        private IFileRequestService _fileRequestService;
        private IWorkflowService _workflowService;
        private CisContext _cisContext;
        private IArchiveService _archiveService;
        private HostConfiguration Config;

        public FileRequestController(IWorkflowService workflowService, IFileRequestService requestService,
            CisContext context, IArchiveService archiveService, IOptions<HostConfiguration> config)
        {
            _cisContext = context;
            _workflowService = workflowService;
            _fileRequestService = requestService;
            _archiveService = archiveService;
            
            _workflowService.SetContext(context);
            _fileRequestService.SetContext(context);
            _archiveService.SetContext(context);
            Config = config.Value;
        }


        [HttpGet]
        public IActionResult GetFileRequest([FromQuery] string id)
        {
            var fileRequest = _fileRequestService.GetRequestWorkflow(id);

            return Json(fileRequest);
        }


        [HttpPost]
        public IActionResult RequestFile([FromBody] FileRequestModel fileModel)
        {
            using (var transaction = _cisContext.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid) throw new InvalidOperationException("Request model malformed");

                    var session = GetSession();
                    string  fileUpin = "";
                    if (fileModel.FileId != "")
                    {
                        var phyFile = _archiveService.GetFile(fileModel.FileId);
                        fileUpin = phyFile.Upin;
                    }
                    var workflowModel = new WorkflowModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        CurrentState = (int) State.Initial,
                        Type = _cisContext.WorkflowType.First(wt => wt.Id == 5),
                        Title = $"{fileUpin} (Requested By: {session.UserName})"
                    };

                    fileModel.Id = Guid.NewGuid().ToString();
                    fileModel.WorkflowId = workflowModel.Id;
                    
                    var workItem = new WorkItemModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        AssignedRole = 2,
                        Workflow = workflowModel.Id,
                        DataType = typeof(FileRequestModel).FullName,
                        Type = (int) FileRequestTrigger.RequestFile
                    };
                    
                    //set current workitem
                    workflowModel.CurrentWorkItem = workItem.Id;

                    fileModel.UserName = session.UserName;

                    workItem.Data = JsonConvert.SerializeObject(fileModel);
                    
                    var fileRequestWorkflow = new FileRequestWorkflow(_fileRequestService, (int)State.Initial, session, Config.ApplicationFor);                   
                    fileRequestWorkflow.RequestFile(workflowModel, workItem);
                    
                    transaction.Commit();

                    return Json(new {message = "Success"});

                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    
                    return StatusCode(500, new {mesage = e.Message});
                }
            }
        }

        [HttpPost]
        public IActionResult RejectFileRequest([FromBody] JObject workflowModel)
        {
            
            using(var transaction = _cisContext.Database.BeginTransaction()) {
                
                try{
                    if (!ModelState.IsValid) throw new InvalidOperationException("Request model malformed");

                    var workflowId = (string) workflowModel["id"];

                    int state = _workflowService.GetWorkflow(workflowId).CurrentState;
                    
                    
                    
                    var workItem = new WorkItemModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        AssignedRole = 2,
                        Workflow = workflowId,
                        Type = (int)FileRequestTrigger.RejectFile                     
                    };
                    
                    var fileRequestWorkflow = new FileRequestWorkflow(_fileRequestService, state, GetSession(), Config.ApplicationFor);
                    fileRequestWorkflow.RejectFileRequest(workflowId, workItem);
                    
                    transaction.Commit();

                    return Json(new {message = "Success"});
                }
                catch(Exception e) {
                    transaction.Rollback();
                    
                    return StatusCode(500, new {mesage = e.Message});
                }
                
            }
            
        }
        
        
        [HttpPost]
        public IActionResult AcceptFileRequest([FromBody] JObject workflowModel)
        {
            
            using(var transaction = _cisContext.Database.BeginTransaction()) {
                
                try
                {
                    if (!ModelState.IsValid) throw new InvalidOperationException("Request model malformed");

                    var workflowId = (string) workflowModel["id"];

                    int state = _workflowService.GetWorkflow(workflowId).CurrentState;

                    var session = GetSession();
                        
                    var workItem = new WorkItemModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        AssignedRole = -1,
                        AssignedUser = session.UserName,
                        Workflow = workflowId,
                        Type = (int)FileRequestTrigger.AcceptFileRequest                     
                    };             
                    
                    var fileRequestWorkflow = new FileRequestWorkflow(_fileRequestService, state, GetSession(), Config.ApplicationFor);
                    fileRequestWorkflow.AcceptFileRequest(workflowId, workItem);
                    
                    transaction.Commit();

                    return Json(new {message = "Success"});
                }
                catch(Exception e) {
                    transaction.Rollback();
                    
                    return StatusCode(500, new {mesage = e.Message});
                }
                
            }
            
        }
        
        [HttpPost]
        public IActionResult FailFileRequest([FromBody] JObject workflowModel)
        {
            
            using(var transaction = _cisContext.Database.BeginTransaction()) {
                
                try{
                    if (!ModelState.IsValid) throw new InvalidOperationException("Request model malformed");

                    var workflowId = (string) workflowModel["id"];

                    int state = _workflowService.GetWorkflow(workflowId).CurrentState;
                    
                        
                    var workItem = new WorkItemModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        AssignedRole = 2,
                        Workflow = workflowId,
                        Type = (int)FileRequestTrigger.FileRequestFail                     
                    };
                    
                    var fileRequestWorkflow = new FileRequestWorkflow(_fileRequestService, state, GetSession(), Config.ApplicationFor);
                    fileRequestWorkflow.FileFail(workflowId, workItem);
                    
                    transaction.Commit();

                    return Json(new {message = "Success"});
                }
                catch(Exception e) {
                    transaction.Rollback();
                    
                    return StatusCode(500, new {mesage = e.Message});
                }
                
            }
            
        }
        
        
        [HttpPost]
        public IActionResult CheckOutFileRequest([FromBody] JObject workflowModel)
        {
            
            using(var transaction = _cisContext.Database.BeginTransaction()) {
                
                try{
                    if (!ModelState.IsValid) throw new InvalidOperationException("Request model malformed");

                    var workflowId = (string) workflowModel["id"];

                    int state = _workflowService.GetWorkflow(workflowId).CurrentState;
                    
                        
                    var workItem = new WorkItemModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        AssignedRole = 2,
                        Workflow = workflowId,
                        Type = (int)FileRequestTrigger.CheckoutFile                     
                    };   
                    var fileRequestWorkflow = new FileRequestWorkflow(_fileRequestService, state, GetSession(), Config.ApplicationFor);
                    fileRequestWorkflow.CheckoutFile(workflowId, workItem);
                    
                    transaction.Commit();

                    return Json(new {message = "Success"});
                }
                catch(Exception e) {
                    transaction.Rollback();
                    
                    return StatusCode(500, new {mesage = e.Message});
                }
                
            }
            
        }
        
        [HttpPost]
        public IActionResult CheckInFileRequest([FromBody] JObject workflowModel)
        {
            
            using(var transaction = _cisContext.Database.BeginTransaction()) {
                
                try{
                    if (!ModelState.IsValid) throw new InvalidOperationException("Request model malformed");

                    var workflowId = (string) workflowModel["id"];

                    int state = _workflowService.GetWorkflow(workflowId).CurrentState;
                    
                        
                    var workItem = new WorkItemModel()
                    {
                        Id = Guid.NewGuid().ToString(),
                        AssignedRole = 2,
                        Workflow = workflowId,
                        Type = (int)FileRequestTrigger.CheckinFile                     
                    };
                    
                    var fileRequestWorkflow = new FileRequestWorkflow(_fileRequestService, state, GetSession(),Config.ApplicationFor);
                    fileRequestWorkflow.CheckinFile(workflowId, workItem);
                    
                    transaction.Commit();

                    return Json(new {message = "Success"});
                }
                catch(Exception e) {
                    transaction.Rollback();
                    
                    return StatusCode(500, new {mesage = e.Message});
                }
                
            }
            
        }
       
    }
}