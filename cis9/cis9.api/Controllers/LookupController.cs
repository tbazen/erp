﻿using intaps.cis.domain.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace intaps.cis.Controllers
{
    [Route("api/dams/[controller]/[action]")]
    [ApiController]
    public class LookupController : BaseController
    {
        private ILookupService _lookupService;

        public LookupController(ILookupService service)
        {
            _lookupService = service;
        }

        [HttpGet]
        public IActionResult DocumentType()
        {
            return Json(_lookupService.GetDocumentTypes());
        }

        [HttpGet]
        public IActionResult Role()
        {
            return Json(_lookupService.GetRoles());
        }

        [HttpGet]
        public IActionResult GetActionTypes()
        {
            return Json(_lookupService.GetActionTypes());
        }

        [HttpGet]
        public IActionResult CaseType()
        {
            return Json(_lookupService.GetCaseTypes());
        }
        
    }
}