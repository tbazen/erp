﻿using System;
using System.Linq;
using intaps.cis.domain.Admin;
using intaps.cis.domain.Infrastructure;
using intaps.cis.Filters;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Sieve.Models;
using Sieve.Services;

namespace intaps.cis.Controllers
{
    [Route("api/dams/[controller]/[action]")]
    [ApiController]
    public class UserController : BaseController
    {
        private IUserFacade _userFacade;

        private ISieveProcessor _sieveProcessor;
        
        public UserController(IUserFacade facade, ISieveProcessor sieveProcessor)
        {
            _userFacade = facade;
            _sieveProcessor = sieveProcessor;
        }
        
        [Roles(UserRoles.Admin)]
        [HttpGet]
        public IActionResult GetUsers([FromQuery]SieveModel sieveModel)
        {
            try
            {
                if (!ModelState.IsValid) throw new InvalidOperationException("Model invalid");
                var users = _userFacade.GetAllUsers(GetSession());
                
                Request.HttpContext.Response.Headers.Add("X-Total-Count", users.Count().ToString());
                Request.HttpContext.Response.Headers.Add("Access-Control-Expose-Headers", "X-Total-Count");
                
                users = _sieveProcessor.Apply(sieveModel, users);

                return Json(users.ToList());
            }
            catch (Exception e)
            {
                return Json(new {errorCode = 500, message = e.Message}); 
            }
        }
        
        [Roles]
        [HttpGet]
        public IActionResult GetNonAdminUsers()
        {
            try
            {
                return Json(_userFacade.GetNonAdminUsers(GetSession()));
            }
            catch (Exception e)
            {
                return Json(new {errorCode = 500, message = e.Message}); 
            }
        }

        [Roles(UserRoles.Admin)]
        [HttpGet]
        public IActionResult CheckUser(string username)
        {
            try
            {
                return Json(_userFacade.CheckUser(GetSession(), username));
            }
            catch (Exception e)
            {
                return Json(false); 
            } 
        }
        
        [HttpPost]
        [Roles(UserRoles.Admin)]
        public IActionResult Update([FromBody] UserViewModel userVm)
        {
            if (!ModelState.IsValid) return Json(false);

            try
            {
                _userFacade.UpdateUser(GetSession(), userVm);
                return new JsonResult(new {message = "Success"}){StatusCode = 200};
            }
            catch (Exception e)
            {
                return StatusCode(500, new {message = "Internal server error occurred"});
            }
        }
        
        [Roles(UserRoles.Admin)]
        [HttpPost]
        public IActionResult AddRole([FromBody] UserRoleViewModel roleVm)
        {
            if (!ModelState.IsValid) return Json(false);

            try
            {
                _userFacade.AddUserRole(GetSession(), roleVm.UserName, roleVm.Roles);
                return Json(true);
            }
            catch (Exception e)
            {
                return StatusCode(500, new {message = "Internal server error occurred"});
            }
        }

        [HttpPost]
        public IActionResult RevokeRole([FromBody] UserRoleViewModel roleVm)
        {
            if (!ModelState.IsValid) return Json(false);

            try
            {
                _userFacade.RevokeUserRole(GetSession(), roleVm.UserName, roleVm.Roles);
                return Json(true);
            }
            catch (Exception e)
            {
                return StatusCode(500, new {message = "Internal server error occurred"});
            } 
        }

        [Roles(UserRoles.Admin)]
        [HttpPost]
        public IActionResult Deactivate([FromBody] JObject user)
        {
            if (!ModelState.IsValid) return Json(false);

            try
            {
                var username = (string)user["username"];
                _userFacade.DeactivateUser(GetSession(), username);
                return Json(true);
            }
            catch (Exception e)
            {
                return StatusCode(500, new {message = "Internal server error occurred"});
            }  
        }
        
        [Roles(UserRoles.Admin)]
        [HttpPost]
        public IActionResult Activate([FromBody] JObject user)
        {
            if (!ModelState.IsValid) return Json(false);

            try
            {
                var username = (string)user["username"];
                _userFacade.ActivateUser(GetSession(), username);
                return Json(true);
            }
            catch (Exception e)
            {
                return StatusCode(500, new {message = "Internal server error occurred"});
            }  
        }
        

        [Roles]
        [HttpGet]
        public IActionResult Search(string query)
        {
            try
            {
                if (string.IsNullOrEmpty(query))
                {
                    return Json(_userFacade.GetAllUsers(GetSession()));
                }
                
                return Json(_userFacade.GetUsers(GetSession(), query));
                
            }
            catch (Exception e)
            {
                return StatusCode(500, new {message = "Internal server error occurred"}); 
            } 
        }
    }
}