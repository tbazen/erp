﻿using System;
using System.Linq;
using intaps.cis.data;
using intaps.cis.domain.Archive;
using intaps.cis.domain.Workflows;
using intaps.cis.domain.Workflows.DamsWorkflow;
using intaps.cis.Filters;
using Microsoft.AspNetCore.Mvc;

namespace intaps.cis.Controllers
{
    [Roles]
    [Route("api/dams/[controller]/[action]")]
    [ApiController]
    public class WorkflowController : BaseController
    {
        private readonly IDamsWorkflowService _damsWorkflowService;
        private readonly IWorkflowService _workflowService;
        private readonly IArchiveService _archiveService;

        private CisContext _cisContext;

        public WorkflowController(IWorkflowService service,  IDamsWorkflowService damsService, IArchiveService archiveService, CisContext context)
        {
            _workflowService = service;
            _damsWorkflowService = damsService;
            _archiveService = archiveService;

            _cisContext = context;
            
            _workflowService.SetContext(_cisContext);
            _damsWorkflowService.SetContext(_cisContext);
            _archiveService.SetContext(_cisContext);
        }

        [HttpGet]
        public IActionResult GetId([FromQuery] string id)
        {
            try
            {
                var workflowId = _workflowService.GetWorkflowId(id);
                
                return new JsonResult(workflowId){StatusCode = 200};
            }
            catch (Exception e)
            {
                return StatusCode(500, new {mesage = e.Message});
            } 
        }
        
        
        [HttpGet]
        public IActionResult GetFile([FromQuery]string workItem, [FromQuery]string fileId)
        {
            try
            {
                var file = _archiveService.GetFile(workItem, fileId);
                
                return new JsonResult(file){StatusCode = 200};
            }
            catch (Exception e)
            {
                return StatusCode(500, new {mesage = e.Message});
            }
        }

        [HttpGet]
        public IActionResult GetDocument([FromQuery] string id)
        {
            try
            {
                var document = _archiveService.GetDocument(id);
                
                return new JsonResult(document){StatusCode = 200};
            }
            catch (Exception e)
            {
                return StatusCode(500, new {mesage = e.Message});
            }
        }


        [HttpGet]
        public IActionResult GetFileWorkflow([FromQuery] string id)
        {
            try
            {
                var file = _damsWorkflowService.GetFileWorkflow(id);
                
                return new JsonResult(file){StatusCode = 200};
            }
            catch (Exception e)
            {
                return StatusCode(500, new {mesage = e.Message});
            }
        }

        [HttpGet]
        public IActionResult GetDocumentWorkflow([FromQuery] string id)
        {
            try
            {
                var document = _damsWorkflowService.GetDocumentWorklfow(id);
                
                return new JsonResult(document){StatusCode = 200};
            }
            catch (Exception e)
            {
                return StatusCode(500, new {mesage = e.Message});
            }
        }
        
        [HttpGet]
        public IActionResult GetFileById([FromQuery]string fileId)
        {
            try
            {
                var file = _archiveService.GetFile(fileId);
                
                return new JsonResult(file){StatusCode = 200};
            }
            catch (Exception e)
            {
                return StatusCode(500, new {mesage = e.Message});
            }
        }

        [HttpGet]
        public IActionResult GetDocFile([FromQuery] string fileId)
        {
            try
            {
                var file = _archiveService.GetDocFile(fileId);
                return new JsonResult(file){StatusCode = 200};
            }
            catch (Exception e)
            {
                return StatusCode(500, new {mesage = e.Message});
            }
        }

        [HttpGet]
        public IActionResult GetDocImages([FromQuery] string workflow, [FromQuery] string docId)
        {
            try
            {
                var images = _damsWorkflowService.GetDocumentImages(workflow,docId);
                
                return new JsonResult(images){StatusCode = 200};
            }
            catch (Exception e)
            {
                return StatusCode(500, new {mesage = e.Message});
            }
        }

        [HttpGet]
        public IActionResult GetArchiveStat()
        {
            try
            {
                var stats = _archiveService.ArchiveStat();
                
                return new JsonResult(stats){StatusCode = 200};
            }
            catch (Exception e)
            {
                return StatusCode(500, new {mesage = e.Message});
            }
        }

        [HttpGet]
        public IActionResult GetFileStat([FromQuery] string id, [FromQuery] string workflow)
        {
            try
            {
                FileStat stat = workflow == "" ? _archiveService.GetFileStat(id) : _archiveService.GetFileStat(id, workflow);

                return Json(stat);
            }
            catch (Exception e)
            {
                 return StatusCode(500, new {mesage = e.Message});
            }
        }

        [HttpGet]
        public IActionResult GetWorkflows()
        {
            try
            {
                var workflows = _workflowService.GetWorkflow(GetSession());  
                return Ok(workflows);
            }
            catch (Exception e)
            {
                return new JsonResult(new {message = e.Message}){StatusCode = 200};
            }
        }


        [HttpGet]
        public IActionResult GetWorkflowNotes([FromQuery] string id)
        {
            try
            {
                var notes = _workflowService.GetNotes(id);  
                return Ok(notes);
            }
            catch (Exception e)
            {
                return new JsonResult(new {message = e.Message}){StatusCode = 200};
            }
        }

        [HttpGet]
        public IActionResult GetWorkflowState([FromQuery] string workflow)
        {
            try
            {
                
                return Ok(_workflowService.GetWorkflowState(workflow));
            }
            catch (Exception e)
            {
                return new JsonResult(new {message = e.Message}){StatusCode = 200};
            }
        }

        [HttpGet]
        public IActionResult GetWorkflowType([FromQuery] string workflow)
        {
            try
            {
                
                return Ok(_workflowService.GetWorkflowType(workflow));
            }
            catch (Exception e)
            {
                return new JsonResult(new {message = e.Message}){StatusCode = 200};
            }
        }

        [HttpGet]
        public IActionResult GetWorkItemType([FromQuery] string workItem)
        {
            try
            {
                
                return Ok(_workflowService.GetWorkItemType(workItem));
            }
            catch (Exception e)
            {
                return new JsonResult(new {message = e.Message}){StatusCode = 200};
            }
        }

        [HttpGet]
        public IActionResult CheckFile([FromQuery] string id)
        {
            return Json(_archiveService.CheckFile(id));
        }
        
        [HttpPost]
        public IActionResult SearchFile([FromBody] FileSearch fileSearch)
        {
            try
            {
                var files = _archiveService.SearchFiles(fileSearch);
                Request.HttpContext.Response.Headers.Add("X-Total-Count", files.Count().ToString());
                Request.HttpContext.Response.Headers.Add("Access-Control-Expose-Headers", "X-Total-Count");
                
                files = files.Skip((fileSearch.Page - 1) * fileSearch.PageSize).Take(fileSearch.PageSize);
                
                return Json(files.ToList());
            }
            catch (Exception e)
            {
                return new JsonResult(new {message = e.Message}){StatusCode = 200};
            }
        }
        
        [HttpPost]
        public IActionResult SearchDocument([FromBody] DocumentSearch docSearch)
        {
            try
            {
                var docs = _archiveService.SearchDocuments(docSearch);
                Request.HttpContext.Response.Headers.Add("X-Total-Count", docs.Count().ToString());
                Request.HttpContext.Response.Headers.Add("Access-Control-Expose-Headers", "X-Total-Count");

                docs = docs.Skip((docSearch.Page - 1) * docSearch.PageSize).Take(docSearch.PageSize);
                
                return Json(docs.ToList());
            }
            catch (Exception e)
            {
                return new JsonResult(new {message = e.Message}){StatusCode = 200};
            }
        }

        [HttpGet]
        public IActionResult ShowImage([FromQuery] string workflowId, string docId)
        {
            try
            {
                var images = _damsWorkflowService.GetDocumentImages(workflowId,docId);
                var document = images[0];
                var data = Convert.FromBase64String(document.Data);
                return File(data, document.Mime, null);

            }
            catch (Exception e)
            {
                return new JsonResult(new {message = e.Message}){StatusCode = 200};
            }
        }
        [HttpGet]
        public IActionResult ShowImageByDocId([FromQuery] string docId)
        {
            try
            {
                var doc = _archiveService.GetDocument(docId);
                var images = doc.Images;
                var document = images[0];
                var data = Convert.FromBase64String(document.Data);
                return File(data, document.Mime, null);

            }
            catch (Exception e)
            {
                return new JsonResult(new {message = e.Message}){StatusCode = 200};
            }
        }

        [HttpPost]
        public IActionResult SearchCase([FromBody] CaseSearch caseSearch)
        {
            try
            {
                var cases = _archiveService.SearchCaseDetails(caseSearch);
                return Json(cases.ToList());
            }
            catch (Exception e)
            {
                return new JsonResult(new {message = e.Message}){StatusCode = 200};
            }
        }
    }
}