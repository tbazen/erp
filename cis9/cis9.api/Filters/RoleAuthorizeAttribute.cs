﻿using System;
using System.Linq;
using intaps.cis.domain.Infrastructure;
using intaps.cis.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace intaps.cis.Filters
{
        
    public class Roles  : ActionFilterAttribute
    {
        public int[] AllowedUsers { get; set; }


        public Roles(params int[] values)
        {
            AllowedUsers = values;
        }
        
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var session = context.HttpContext.Session;

            try
            {
                var userSession = session.GetSession<UserSession>("sessionInfo"); 
                userSession.LastSeen = DateTime.Now;
                session.SetSession("sessionInfo", userSession);
                
                if (AllowedUsers != null && AllowedUsers.Length > 0)
                {
                    
                    if (!AllowedUsers.Contains(userSession.Role))
                    {
                        context.Result = new JsonResult(new {status = 403, message = "Forbidden"}){ StatusCode = 403};
                    }  
                }

            }

            catch (ArgumentNullException e)
            {
                context.Result = new BadRequestObjectResult(new {status = 403, message = "Forbidden"});
            }
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            
        }
    }
}