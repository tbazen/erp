﻿using intaps.cis.domain.Workflows.DamsWorkflow;
using Microsoft.AspNetCore.Http;

namespace intaps.cis.Models
{
    public class ImageViewModel : ImageModel
    {
        public IFormFile File { get; set; }
    }
}