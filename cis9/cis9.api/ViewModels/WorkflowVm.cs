﻿using System.Collections.Generic;
using intaps.cis.domain.Workflows.CaseWorkflow;
using intaps.cis.domain.Workflows.Models;

namespace intaps.cis.ViewModels
{
    public class RejectRequest
    {
        public string Workflow { get; set; }
        public string WorkItem { get; set; }
        public string Note { get; set; }
    }

    /*public class OpenCaseVm
    {
        public CaseDetail Case { get; set; }
        public string Assignee { get; set; }
    }*/

    public class ForwardCaseVm
    {
        public string CaseWorkflow { get; set; }
        public string Assignee { get; set; }
        public string Comment { get; set; }
    }

    public class CaseVm
    {
        public CaseModel CaseModel { get; set; }
        public IList<WorkItemNote> Notes { get; set; }
    }
}