﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class ActionType
    {
        public ActionType()
        {
            UserAction = new HashSet<UserAction>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<UserAction> UserAction { get; set; }
    }
}
