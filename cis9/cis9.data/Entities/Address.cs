﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class Address
    {
        public Address()
        {
            Party = new HashSet<Party>();
        }

        public long Id { get; set; }
        public string Region { get; set; }
        public string Zone { get; set; }
        public string City { get; set; }
        public int? HouseNo { get; set; }
        public string PhoneNo { get; set; }
        public string SpecialName { get; set; }
        public int? Woreda { get; set; }
        public int? Kebele { get; set; }

        public Kebele KebeleNavigation { get; set; }
        public Woreda WoredaNavigation { get; set; }
        public ICollection<Party> Party { get; set; }
    }
}
