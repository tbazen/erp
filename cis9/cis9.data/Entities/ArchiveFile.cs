﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class ArchiveFile
    {
        public Guid Id { get; set; }
        public string Shelf { get; set; }
        public string Location { get; set; }
        public string Folder { get; set; }
        public string Remark { get; set; }
        public string FileCode { get; set; }
        public string FileOwner { get; set; }
        public string MobileNumber { get; set; }
        public string Upin { get; set; }
        public Guid WorkItem { get; set; }
    }
}
