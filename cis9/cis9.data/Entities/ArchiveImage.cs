﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class ArchiveImage
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public Guid DocumentId { get; set; }
        public string Mime { get; set; }
        public Guid Workitem { get; set; }
        public int Page { get; set; }
    }
}
