﻿using System;
using System.Collections.Generic;
using NpgsqlTypes;

namespace intaps.cis.data.Entities
{
    public partial class Building
    {
        public Building()
        {
            House = new HashSet<House>();
        }

        public Guid Id { get; set; }
       
        public int Nfloors { get; set; }
        public double FloorArea { get; set; }
        public Guid ParcelId { get; set; }
        public string BuildingNo { get; set; }
        public Guid Workflow { get; set; }
        public Guid PropertyId { get; set; }

        public Parcel Parcel { get; set; }
        public Property Property { get; set; }
        public ICollection<House> House { get; set; }
    }
}
