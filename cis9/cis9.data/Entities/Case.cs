﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class Case
    {
        public Case()
        {
            CaseDocument = new HashSet<CaseDocument>();
        }

        public Guid Id { get; set; }
        public long CaseNumber { get; set; }
        public int CaseType { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public Guid Workflow { get; set; }
        public string Title { get; set; }
        public long Date { get; set; }

        public CaseType CaseTypeNavigation { get; set; }
        public ICollection<CaseDocument> CaseDocument { get; set; }
    }
}
