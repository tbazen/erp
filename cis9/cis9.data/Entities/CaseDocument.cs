﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class CaseDocument
    {
        public Guid CaseId { get; set; }
        public Guid DocumentId { get; set; }
        public Guid Id { get; set; }

        public Case Case { get; set; }
        public Document Document { get; set; }
    }
}
