﻿using System;

namespace intaps.cis.data.Entities
{
    public partial class CaseNote
    {
        public Guid Id { get; set; }
        public string Note { get; set; }
        public Guid CaseId { get; set; }
        public long TimeStamp { get; set; }

        public Case Case { get; set; }
    }
}
