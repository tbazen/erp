﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class CaseNumber
    {
        public long Id { get; set; }
        public long CurrentCase { get; set; }
    }
}
