using System;
using System.Collections.Generic;
namespace intaps.cis.data.Entities
{
    public partial class CaseSearchResult
    {
        public Guid Id { get; set; }
        public String Title  { get; set; }
        public String CaseType{ get; set; }
        public String CaseNumber{ get; set; }
        public Guid Workflow{ get; set; }
        public String Date{ get; set; }
        public int Status{ get; set; }
        public String CreatedBy { get; set; }
    }
}