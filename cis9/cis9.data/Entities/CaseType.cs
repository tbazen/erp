﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class CaseType
    {
        public CaseType()
        {
            Case = new HashSet<Case>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Case> Case { get; set; }
    }
}
