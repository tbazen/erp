﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class Document
    {
        public Document()
        {
            CaseDocument = new HashSet<CaseDocument>();
        }

        public Guid Id { get; set; }
        public int Sheets { get; set; }
        public string ReferenceNo { get; set; }
        public Guid FileId { get; set; }
        public int Type { get; set; }
        public long Timestamp { get; set; }
        public long Date { get; set; }
        public Guid Workitem { get; set; }

        public DocumentType TypeNavigation { get; set; }
        public ICollection<CaseDocument> CaseDocument { get; set; }
    }
}
