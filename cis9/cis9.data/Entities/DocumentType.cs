﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class DocumentType
    {
        public DocumentType()
        {
            Document = new HashSet<Document>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Document> Document { get; set; }
    }
}
