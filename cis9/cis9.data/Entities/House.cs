﻿using System;
using System.Collections.Generic;
using NpgsqlTypes;

namespace intaps.cis.data.Entities
{
    public partial class House
    {
        public Guid Id { get; set; }
        public int FloorNo { get; set; }
        public string HouseNo { get; set; }
        
        public Guid BuildingId { get; set; }
        public Guid Workflow { get; set; }
        public Guid PropertyId { get; set; }

        public Building Building { get; set; }
        public Property Property { get; set; }
    }
}
