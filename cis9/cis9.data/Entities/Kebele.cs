﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class Kebele
    {
        public Kebele()
        {
            Address = new HashSet<Address>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Address> Address { get; set; }
    }
}
