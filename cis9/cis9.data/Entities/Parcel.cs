﻿using System;
using System.Collections.Generic;
using NpgsqlTypes;

namespace intaps.cis.data.Entities
{
    public partial class Parcel
    {
        public Parcel()
        {
            Building = new HashSet<Building>();
            ParcelId = new HashSet<ParcelId>();
        }

        public Guid Id { get; set; }
        //public PostgisGeometry Geom { get; set; }
        public Guid Workflow { get; set; }
        public Guid? PropertyId { get; set; }

        public Property Property { get; set; }
        public ICollection<Building> Building { get; set; }
        public ICollection<ParcelId> ParcelId { get; set; }
    }
}
