﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class ParcelId
    {
        public string Id { get; set; }
        public int SchemeId { get; set; }
        public Guid? ParcelId1 { get; set; }

        public Parcel ParcelId1Navigation { get; set; }
        public ParcelIdScheme Scheme { get; set; }
    }
}
