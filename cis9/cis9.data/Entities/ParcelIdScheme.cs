﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class ParcelIdScheme
    {
        public ParcelIdScheme()
        {
            ParcelId = new HashSet<ParcelId>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<ParcelId> ParcelId { get; set; }
    }
}
