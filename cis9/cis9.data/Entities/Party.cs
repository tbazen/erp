﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class Party
    {
        public Party()
        {
            PartyMemberGroupPartyNavigation = new HashSet<PartyMember>();
            PartyMemberMemberPartyNavigation = new HashSet<PartyMember>();
            PropertyParty = new HashSet<PropertyParty>();
        }

        public Guid Id { get; set; }
        public string FullName { get; set; }
        public long? BirthDate { get; set; }
        public long Address { get; set; }
        public bool? Sex { get; set; }
        public int PartySubtype { get; set; }
        public Guid Workflow { get; set; }

        public Address AddressNavigation { get; set; }
        public PartySubtype PartySubtypeNavigation { get; set; }
        public ICollection<PartyMember> PartyMemberGroupPartyNavigation { get; set; }
        public ICollection<PartyMember> PartyMemberMemberPartyNavigation { get; set; }
        public ICollection<PropertyParty> PropertyParty { get; set; }
    }
}
