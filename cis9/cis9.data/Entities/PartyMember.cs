﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class PartyMember
    {
        public Guid GroupParty { get; set; }
        public Guid MemberParty { get; set; }
        public long? Share { get; set; }

        public Party GroupPartyNavigation { get; set; }
        public Party MemberPartyNavigation { get; set; }
    }
}
