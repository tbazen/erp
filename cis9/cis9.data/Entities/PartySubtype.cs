﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class PartySubtype
    {
        public PartySubtype()
        {
            PartyNavigation = new HashSet<Party>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? Party { get; set; }

        public PartyType Party1 { get; set; }
        public ICollection<Party> PartyNavigation { get; set; }
    }
}
