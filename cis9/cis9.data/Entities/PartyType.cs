﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class PartyType
    {
        public PartyType()
        {
            PartySubtype = new HashSet<PartySubtype>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? Party { get; set; }

        public ICollection<PartySubtype> PartySubtype { get; set; }
    }
}
