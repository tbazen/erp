﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class PhysicalFileRequest
    {
        public Guid Id { get; set; }
        public Guid? FileId { get; set; }
        public string Description { get; set; }
        public string UserName { get; set; }
    }
}
