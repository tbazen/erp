﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class Property
    {
        public Property()
        {
            Building = new HashSet<Building>();
            House = new HashSet<House>();
            Parcel = new HashSet<Parcel>();
            PropertyParty = new HashSet<PropertyParty>();
        }

        public Guid Id { get; set; }
        public string Note { get; set; }
        public int Type { get; set; }
        public Guid Workflow { get; set; }
        public Guid File { get; set; }

        public PropertyType TypeNavigation { get; set; }
        public ICollection<Building> Building { get; set; }
        public ICollection<House> House { get; set; }
        public ICollection<Parcel> Parcel { get; set; }
        public ICollection<PropertyParty> PropertyParty { get; set; }
    }
}
