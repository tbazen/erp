﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class PropertyParty
    {
        public long Id { get; set; }
        public double? Share { get; set; }
        public long? StartDate { get; set; }
        public long? EndDate { get; set; }
        public int PropertyPartyType { get; set; }
        public Guid Property { get; set; }
        public Guid Party { get; set; }
        public Guid Workflow { get; set; }

        public Party PartyNavigation { get; set; }
        public Property PropertyNavigation { get; set; }
        public PropertyPartytype PropertyPartyTypeNavigation { get; set; }
    }
}
