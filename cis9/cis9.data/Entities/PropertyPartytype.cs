﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class PropertyPartytype
    {
        public PropertyPartytype()
        {
            PropertyParty = new HashSet<PropertyParty>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<PropertyParty> PropertyParty { get; set; }
    }
}
