﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class PropertyType
    {
        public PropertyType()
        {
            Property = new HashSet<Property>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Property> Property { get; set; }
    }
}
