﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class TransactionType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
