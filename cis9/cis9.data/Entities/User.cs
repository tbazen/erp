﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class User
    {
        public User()
        {
            UserRole = new HashSet<UserRole>();
        }

        public long Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string PhoneNo { get; set; }
        public int Status { get; set; }

        public ICollection<UserRole> UserRole { get; set; }
    }
}
