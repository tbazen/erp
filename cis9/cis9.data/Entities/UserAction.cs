﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class UserAction
    {
        public UserAction()
        {
            AuditLog = new HashSet<AuditLog>();
            Workflow = new HashSet<Workflow>();
        }

        public long Timestamp { get; set; }
        public int ActionType { get; set; }
        public string Username { get; set; }
        public long Id { get; set; }

        public ActionType ActionTypeNavigation { get; set; }
        public ICollection<AuditLog> AuditLog { get; set; }
        public ICollection<Workflow> Workflow { get; set; }
    }
}
