﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class UserRole
    {
        public long Userid { get; set; }
        public long Roleid { get; set; }

        public Role Role { get; set; }
        public User User { get; set; }
    }
}
