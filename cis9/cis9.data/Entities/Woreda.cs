﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class Woreda
    {
        public Woreda()
        {
            Address = new HashSet<Address>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Address> Address { get; set; }
    }
}
