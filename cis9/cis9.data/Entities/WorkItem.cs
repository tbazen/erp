﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class WorkItem
    {
        public WorkItem()
        {
            WorkitemNote = new HashSet<WorkitemNote>();
        }

        public Guid Id { get; set; }
        public long SeqNo { get; set; }
        public Guid Workflow { get; set; }
        public string Data { get; set; }
        public string DataType { get; set; }
        public int Type { get; set; }
        public string AssignedUser { get; set; }
        public int AssignedRole { get; set; }

        public Workflow WorkflowNavigation { get; set; }
        public ICollection<WorkitemNote> WorkitemNote { get; set; }
    }
}
