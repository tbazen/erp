﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class Workflow
    {
        public Workflow()
        {
            WorkItem = new HashSet<WorkItem>();
        }

        public Guid Id { get; set; }
        public int CurrentState { get; set; }
        public int OldState { get; set; }
        public int Type { get; set; }
        public long Timestamp { get; set; }
        public string Title { get; set; }
        public long Aid { get; set; }
        public Guid CurrentWorkItem { get; set; }

        public UserAction A { get; set; }
        public WorkflowType TypeNavigation { get; set; }
        public ICollection<WorkItem> WorkItem { get; set; }
    }
}
