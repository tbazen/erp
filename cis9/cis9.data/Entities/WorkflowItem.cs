﻿using System;
using System.Collections.Generic;

namespace intaps.cis.data.Entities
{
    public partial class WorkflowItem
    {
        public WorkflowItem()
        {
            WorkItem = new HashSet<WorkItem>();
        }

        public Guid Id { get; set; }
        public long Type { get; set; }

        public WorkflowType TypeNavigation { get; set; }
        public ICollection<WorkItem> WorkItem { get; set; }
    }
}
