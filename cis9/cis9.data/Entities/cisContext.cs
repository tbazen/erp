﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace intaps.cis.data.Entities
{
    public partial class cisContext : DbContext
    {
        public virtual DbSet<ActionType> ActionType { get; set; }
        public virtual DbSet<Address> Address { get; set; }
        public virtual DbSet<ArchiveFile> ArchiveFile { get; set; }
        public virtual DbSet<ArchiveImage> ArchiveImage { get; set; }
        public virtual DbSet<AuditLog> AuditLog { get; set; }
        public virtual DbSet<Building> Building { get; set; }
        public virtual DbSet<Case> Case { get; set; }
        public virtual DbSet<CaseDocument> CaseDocument { get; set; }
        public virtual DbSet<CaseNumber> CaseNumber { get; set; }
        public virtual DbSet<CaseSearchResult> CaseSearchResult { get; set; }
        public virtual DbSet<CaseType> CaseType { get; set; }
        public virtual DbSet<Document> Document { get; set; }
        public virtual DbSet<DocumentType> DocumentType { get; set; }
        public virtual DbSet<House> House { get; set; }
        public virtual DbSet<Kebele> Kebele { get; set; }
        public virtual DbSet<Parcel> Parcel { get; set; }
        public virtual DbSet<ParcelId> ParcelId { get; set; }
        public virtual DbSet<ParcelIdScheme> ParcelIdScheme { get; set; }
        public virtual DbSet<Party> Party { get; set; }
        public virtual DbSet<PartyMember> PartyMember { get; set; }
        public virtual DbSet<PartySubtype> PartySubtype { get; set; }
        public virtual DbSet<PartyType> PartyType { get; set; }
        public virtual DbSet<PhysicalFileRequest> PhysicalFileRequest { get; set; }
        public virtual DbSet<Property> Property { get; set; }
        public virtual DbSet<PropertyParty> PropertyParty { get; set; }
        public virtual DbSet<PropertyPartytype> PropertyPartytype { get; set; }
        public virtual DbSet<PropertyType> PropertyType { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<SpatialRefSys> SpatialRefSys { get; set; }
        public virtual DbSet<TransactionType> TransactionType { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserAction> UserAction { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }
        public virtual DbSet<Woreda> Woreda { get; set; }
        public virtual DbSet<Workflow> Workflow { get; set; }
        public virtual DbSet<WorkflowType> WorkflowType { get; set; }
        public virtual DbSet<WorkItem> WorkItem { get; set; }
        public virtual DbSet<WorkitemNote> WorkitemNote { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql(@"Host=localhost;Database=cis;Username=postgres;Password=toor");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.HasPostgresExtension("postgis");

            modelBuilder.Entity<ActionType>(entity =>
            {
                entity.ToTable("action_type", "sys");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name).HasColumnName("name");
            });

            modelBuilder.Entity<Address>(entity =>
            {
                entity.ToTable("address", "legal");

                entity.HasIndex(e => e.Kebele)
                    .HasName("ixfk_address_kebele");

                entity.HasIndex(e => e.Woreda)
                    .HasName("ixfk_address_woreda");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.City).HasColumnName("city");

                entity.Property(e => e.HouseNo).HasColumnName("house_no");

                entity.Property(e => e.Kebele).HasColumnName("kebele");

                entity.Property(e => e.PhoneNo).HasColumnName("phone_no");

                entity.Property(e => e.Region).HasColumnName("region");

                entity.Property(e => e.SpecialName).HasColumnName("special_name");

                entity.Property(e => e.Woreda).HasColumnName("woreda");

                entity.Property(e => e.Zone).HasColumnName("zone");

                entity.HasOne(d => d.KebeleNavigation)
                    .WithMany(p => p.Address)
                    .HasForeignKey(d => d.Kebele)
                    .HasConstraintName("fk_address_kebele");

                entity.HasOne(d => d.WoredaNavigation)
                    .WithMany(p => p.Address)
                    .HasForeignKey(d => d.Woreda)
                    .HasConstraintName("fk_address_woreda");
            });

            modelBuilder.Entity<ArchiveFile>(entity =>
            {
                entity.ToTable("archive_file", "wf");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.FileCode)
                    .IsRequired()
                    .HasColumnName("file_code")
                    .HasColumnType("varchar");

                entity.Property(e => e.FileOwner)
                    .IsRequired()
                    .HasColumnName("file_owner")
                    .HasColumnType("varchar");

                entity.Property(e => e.Folder)
                    .IsRequired()
                    .HasColumnName("folder");

                entity.Property(e => e.Location)
                    .IsRequired()
                    .HasColumnName("location");

                entity.Property(e => e.MobileNumber)
                    .HasColumnName("mobile_number")
                    .HasColumnType("varchar");

                entity.Property(e => e.Remark).HasColumnName("remark");

                entity.Property(e => e.Shelf)
                    .IsRequired()
                    .HasColumnName("shelf");

                entity.Property(e => e.Upin)
                    .IsRequired()
                    .HasColumnName("upin")
                    .HasColumnType("varchar");

                entity.Property(e => e.WorkItem).HasColumnName("work_item");
            });

            modelBuilder.Entity<ArchiveImage>(entity =>
            {
                entity.ToTable("archive_image", "wf");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.DocumentId).HasColumnName("document_id");

                entity.Property(e => e.Mime)
                    .IsRequired()
                    .HasColumnName("mime")
                    .HasColumnType("varchar");

                entity.Property(e => e.Page).HasColumnName("page");

                entity.Property(e => e.Workitem).HasColumnName("workitem");
            });

            modelBuilder.Entity<AuditLog>(entity =>
            {
                entity.ToTable("audit_log", "sys");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval(('sys.\"audit_seq\"'::text)::regclass)");

                entity.Property(e => e.KeyValues).HasColumnName("key_values");

                entity.Property(e => e.NewValues).HasColumnName("new_values");

                entity.Property(e => e.OldValues).HasColumnName("old_values");

                entity.Property(e => e.TableName)
                    .IsRequired()
                    .HasColumnName("table_name");

                entity.Property(e => e.TimeStamp).HasColumnName("time_stamp");

                entity.Property(e => e.UserAction).HasColumnName("user_action");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnName("user_name");

                entity.HasOne(d => d.UserActionNavigation)
                    .WithMany(p => p.AuditLog)
                    .HasForeignKey(d => d.UserAction)
                    .HasConstraintName("audit_log_action_fk");
            });

            modelBuilder.Entity<Building>(entity =>
            {
                entity.ToTable("building", "legal");

                entity.HasIndex(e => e.ParcelId)
                    .HasName("ixfk_building_parcel");

                entity.HasIndex(e => e.PropertyId)
                    .HasName("ixfk_building_property");

                entity.HasIndex(e => e.Workflow)
                    .HasName("ixfk_building_workflow");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.BuildingNo).HasColumnName("building_no");

                entity.Property(e => e.FloorArea).HasColumnName("floor_area");

                //entity.Property(e => e.Geom).HasColumnName("geom");

                entity.Property(e => e.Nfloors).HasColumnName("nfloors");

                entity.Property(e => e.ParcelId).HasColumnName("parcel_id");

                entity.Property(e => e.PropertyId).HasColumnName("property_id");

                entity.Property(e => e.Workflow).HasColumnName("workflow");

                entity.HasOne(d => d.Parcel)
                    .WithMany(p => p.Building)
                    .HasForeignKey(d => d.ParcelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_building_parcel");

                entity.HasOne(d => d.Property)
                    .WithMany(p => p.Building)
                    .HasForeignKey(d => d.PropertyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_building_property");
            });

            modelBuilder.Entity<Case>(entity =>
            {
                entity.ToTable("case", "wf");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CaseNumber)
                    .HasColumnName("case_number")
                    .HasDefaultValueSql("nextval('wf.case_case_number_seq'::regclass)");

                entity.Property(e => e.CaseType).HasColumnName("case_type");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("created_by")
                    .HasColumnType("varchar");

                entity.Property(e => e.Date).HasColumnName("date");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title");

                entity.Property(e => e.Workflow).HasColumnName("workflow");

                entity.HasOne(d => d.CaseTypeNavigation)
                    .WithMany(p => p.Case)
                    .HasForeignKey(d => d.CaseType)
                    .HasConstraintName("case_type_fk");
            });

            modelBuilder.Entity<CaseDocument>(entity =>
            {
                entity.ToTable("case_document", "wf");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CaseId).HasColumnName("case_id");

                entity.Property(e => e.DocumentId).HasColumnName("document_id");

                entity.HasOne(d => d.Case)
                    .WithMany(p => p.CaseDocument)
                    .HasForeignKey(d => d.CaseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("case_document_case_fk");

                entity.HasOne(d => d.Document)
                    .WithMany(p => p.CaseDocument)
                    .HasForeignKey(d => d.DocumentId)
                    .HasConstraintName("case_document_document_fk");
            });

            modelBuilder.Entity<CaseNumber>(entity =>
            {
                entity.ToTable("case_number", "wf");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CurrentCase).HasColumnName("current_case");
            });

            modelBuilder.Entity<CaseSearchResult>(entity =>
            {
                entity.ToTable("case_search_result", "wf");

                entity.Property(e => e.Id)
                    .HasColumnName("id");
                entity.Property(e => e.Title)
                    .HasColumnName("title");
                entity.Property(e => e.CaseType)
                    .HasColumnName("casetype");
                entity.Property(e => e.CaseNumber)
                    .HasColumnName("casenumber");
                entity.Property(e => e.Workflow)
                    .HasColumnName("workflow");
                entity.Property(e => e.Date)
                    .HasColumnName("date");
                entity.Property(e => e.Status)
                    .HasColumnName("status");
                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdby");

            });
            
            modelBuilder.Entity<CaseType>(entity =>
            {
                entity.ToTable("case_type", "wf");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Document>(entity =>
            {
                entity.ToTable("document", "wf");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Date).HasColumnName("date");

                entity.Property(e => e.FileId).HasColumnName("file_id");

                entity.Property(e => e.ReferenceNo)
                    .IsRequired()
                    .HasColumnName("reference_no");

                entity.Property(e => e.Sheets).HasColumnName("sheets");

                entity.Property(e => e.Timestamp).HasColumnName("timestamp");

                entity.Property(e => e.Type).HasColumnName("type");

                entity.Property(e => e.Workitem).HasColumnName("workitem");

                entity.HasOne(d => d.TypeNavigation)
                    .WithMany(p => p.Document)
                    .HasForeignKey(d => d.Type)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("archive_document_type_fk");
            });

            modelBuilder.Entity<DocumentType>(entity =>
            {
                entity.ToTable("document_type", "wf");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('wf.document_type_id_seq'::regclass)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");
            });

            modelBuilder.Entity<House>(entity =>
            {
                entity.ToTable("house", "legal");

                entity.HasIndex(e => e.BuildingId)
                    .HasName("ixfk_house_building");

                entity.HasIndex(e => e.PropertyId)
                    .HasName("ixfk_house_property");

                entity.HasIndex(e => e.Workflow)
                    .HasName("ixfk_house_transaction");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.BuildingId).HasColumnName("building_id");

                entity.Property(e => e.FloorNo).HasColumnName("floor_no");

                //entity.Property(e => e.Geom).HasColumnName("geom");

                entity.Property(e => e.HouseNo)
                    .IsRequired()
                    .HasColumnName("house_no");

                entity.Property(e => e.PropertyId).HasColumnName("property_id");

                entity.Property(e => e.Workflow).HasColumnName("workflow");

                entity.HasOne(d => d.Building)
                    .WithMany(p => p.House)
                    .HasForeignKey(d => d.BuildingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_house_building");

                entity.HasOne(d => d.Property)
                    .WithMany(p => p.House)
                    .HasForeignKey(d => d.PropertyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_house_property");
            });

            modelBuilder.Entity<Kebele>(entity =>
            {
                entity.ToTable("kebele", "sys");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name).HasColumnName("name");
            });

            modelBuilder.Entity<Parcel>(entity =>
            {
                entity.ToTable("parcel", "legal");

                entity.HasIndex(e => e.PropertyId)
                    .HasName("ixfk_parcel_property");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                //entity.Property(e => e.Geom).HasColumnName("geom");

                entity.Property(e => e.PropertyId).HasColumnName("property_id");

                entity.Property(e => e.Workflow).HasColumnName("workflow");

                entity.HasOne(d => d.Property)
                    .WithMany(p => p.Parcel)
                    .HasForeignKey(d => d.PropertyId)
                    .HasConstraintName("fk_parcel_property");
            });

            modelBuilder.Entity<ParcelId>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.SchemeId });

                entity.ToTable("parcel_id", "legal");

                entity.HasIndex(e => e.ParcelId1)
                    .HasName("ixfk_parcel_id_parcel");

                entity.HasIndex(e => e.SchemeId)
                    .HasName("ixfk_parcel_id_parcel_id_scheme");

                entity.HasIndex(e => new { e.SchemeId, e.ParcelId1 })
                    .HasName("unique_scheme")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.SchemeId).HasColumnName("scheme_id");

                entity.Property(e => e.ParcelId1).HasColumnName("parcel_id");

                entity.HasOne(d => d.ParcelId1Navigation)
                    .WithMany(p => p.ParcelId)
                    .HasForeignKey(d => d.ParcelId1)
                    .HasConstraintName("fk_parcel_id_parcel");

                entity.HasOne(d => d.Scheme)
                    .WithMany(p => p.ParcelId)
                    .HasForeignKey(d => d.SchemeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_parcel_id_parcel_id_scheme");
            });

            modelBuilder.Entity<ParcelIdScheme>(entity =>
            {
                entity.ToTable("parcel_id_scheme", "sys");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name).HasColumnName("name");
            });

            modelBuilder.Entity<Party>(entity =>
            {
                entity.ToTable("party", "legal");

                entity.HasIndex(e => e.Address)
                    .HasName("ixfk_party_address");

                entity.HasIndex(e => e.PartySubtype)
                    .HasName("ixfk_party_party_subtype");

                entity.HasIndex(e => e.Workflow)
                    .HasName("ixfk_party_transaction");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Address).HasColumnName("address");

                entity.Property(e => e.BirthDate).HasColumnName("birth_date");

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasColumnName("full_name");

                entity.Property(e => e.PartySubtype).HasColumnName("party_subtype");

                entity.Property(e => e.Sex).HasColumnName("sex");

                entity.Property(e => e.Workflow).HasColumnName("workflow");

                entity.HasOne(d => d.AddressNavigation)
                    .WithMany(p => p.Party)
                    .HasForeignKey(d => d.Address)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_party_address");

                entity.HasOne(d => d.PartySubtypeNavigation)
                    .WithMany(p => p.PartyNavigation)
                    .HasForeignKey(d => d.PartySubtype)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_party_party_subtype");
            });

            modelBuilder.Entity<PartyMember>(entity =>
            {
                entity.HasKey(e => new { e.GroupParty, e.MemberParty });

                entity.ToTable("party_member");

                entity.HasIndex(e => e.GroupParty)
                    .HasName("ixfk_party_member_party");

                entity.HasIndex(e => e.MemberParty)
                    .HasName("ixfk_party_member_party_02");

                entity.Property(e => e.GroupParty).HasColumnName("group_party");

                entity.Property(e => e.MemberParty).HasColumnName("member_party");

                entity.Property(e => e.Share).HasColumnName("share");

                entity.HasOne(d => d.GroupPartyNavigation)
                    .WithMany(p => p.PartyMemberGroupPartyNavigation)
                    .HasForeignKey(d => d.GroupParty)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_party_member_party");

                entity.HasOne(d => d.MemberPartyNavigation)
                    .WithMany(p => p.PartyMemberMemberPartyNavigation)
                    .HasForeignKey(d => d.MemberParty)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_party_member_party_02");
            });

            modelBuilder.Entity<PartySubtype>(entity =>
            {
                entity.ToTable("party_subtype", "sys");

                entity.HasIndex(e => e.Party)
                    .HasName("ixfk_party_subtype_party_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name).HasColumnName("name");

                entity.Property(e => e.Party).HasColumnName("party");

                entity.HasOne(d => d.Party1)
                    .WithMany(p => p.PartySubtype)
                    .HasForeignKey(d => d.Party)
                    .HasConstraintName("fk_party_subtype_party_type");
            });

            modelBuilder.Entity<PartyType>(entity =>
            {
                entity.ToTable("party_type", "sys");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name).HasColumnName("name");

                entity.Property(e => e.Party).HasColumnName("party");
            });

            modelBuilder.Entity<PhysicalFileRequest>(entity =>
            {
                entity.ToTable("physical_file_request", "wf");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.FileId).HasColumnName("file_id");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnName("user_name");
            });

            modelBuilder.Entity<Property>(entity =>
            {
                entity.ToTable("property", "legal");

                entity.HasIndex(e => e.Type)
                    .HasName("ixfk_property_propertytype");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.File).HasColumnName("file");

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.Type).HasColumnName("type");

                entity.Property(e => e.Workflow).HasColumnName("workflow");

                entity.HasOne(d => d.TypeNavigation)
                    .WithMany(p => p.Property)
                    .HasForeignKey(d => d.Type)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_property_propertytype");
            });

            modelBuilder.Entity<PropertyParty>(entity =>
            {
                entity.ToTable("property_party", "legal");

                entity.HasIndex(e => e.Party)
                    .HasName("ixfk_property_party_party");

                entity.HasIndex(e => e.Property)
                    .HasName("ixfk_property_party_property");

                entity.HasIndex(e => e.PropertyPartyType)
                    .HasName("ixfk_property_party_property_partytype");

                entity.HasIndex(e => e.Workflow)
                    .HasName("ixfk_property_party_transaction");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval(('legal.\"property_party_id_seq\"'::text)::regclass)");

                entity.Property(e => e.EndDate).HasColumnName("end_date");

                entity.Property(e => e.Party).HasColumnName("party");

                entity.Property(e => e.Property).HasColumnName("property");

                entity.Property(e => e.PropertyPartyType).HasColumnName("property_party_type");

                entity.Property(e => e.Share)
                    .HasColumnName("share")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.StartDate).HasColumnName("start_date");

                entity.Property(e => e.Workflow).HasColumnName("workflow");

                entity.HasOne(d => d.PartyNavigation)
                    .WithMany(p => p.PropertyParty)
                    .HasForeignKey(d => d.Party)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_property_party_party");

                entity.HasOne(d => d.PropertyNavigation)
                    .WithMany(p => p.PropertyParty)
                    .HasForeignKey(d => d.Property)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_property_party_property");

                entity.HasOne(d => d.PropertyPartyTypeNavigation)
                    .WithMany(p => p.PropertyParty)
                    .HasForeignKey(d => d.PropertyPartyType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_property_party_property_partytype");
            });

            modelBuilder.Entity<PropertyPartytype>(entity =>
            {
                entity.ToTable("property_partytype", "legal");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");
            });

            modelBuilder.Entity<PropertyType>(entity =>
            {
                entity.ToTable("property_type", "legal");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval(('legal.\"property_type_id_seq\"'::text)::regclass)");

                entity.Property(e => e.Name).HasColumnName("name");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("role", "sys");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval(('sys.\"role_id_seq\"'::text)::regclass)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");
            });

            modelBuilder.Entity<SpatialRefSys>(entity =>
            {
                entity.HasKey(e => e.Srid);

                entity.ToTable("spatial_ref_sys");

                entity.Property(e => e.Srid)
                    .HasColumnName("srid")
                    .ValueGeneratedNever();

                entity.Property(e => e.AuthName).HasColumnName("auth_name");

                entity.Property(e => e.AuthSrid).HasColumnName("auth_srid");

                entity.Property(e => e.Proj4text).HasColumnName("proj4text");

                entity.Property(e => e.Srtext).HasColumnName("srtext");
            });

            modelBuilder.Entity<TransactionType>(entity =>
            {
                entity.ToTable("transaction_type", "sys");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name).HasColumnName("name");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user", "sys");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval(('sys.\"user_id_seq\"'::text)::regclass)");

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasColumnName("full_name");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password");

                entity.Property(e => e.PhoneNo).HasColumnName("phone_no");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username");
            });

            modelBuilder.Entity<UserAction>(entity =>
            {
                entity.ToTable("user_action", "sys");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.ActionType).HasColumnName("action_type");

                entity.Property(e => e.Timestamp).HasColumnName("timestamp");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username");

                entity.HasOne(d => d.ActionTypeNavigation)
                    .WithMany(p => p.UserAction)
                    .HasForeignKey(d => d.ActionType)
                    .HasConstraintName("user_action_type_fk");
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasKey(e => new { e.Userid, e.Roleid });

                entity.ToTable("user_role", "sys");

                entity.HasIndex(e => e.Roleid)
                    .HasName("ixfk_userrole_role");

                entity.HasIndex(e => e.Userid)
                    .HasName("ixfk_userrole_user");

                entity.Property(e => e.Userid).HasColumnName("userid");

                entity.Property(e => e.Roleid).HasColumnName("roleid");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserRole)
                    .HasForeignKey(d => d.Roleid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_userrole_role");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserRole)
                    .HasForeignKey(d => d.Userid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_userrole_user");
            });

            modelBuilder.Entity<Woreda>(entity =>
            {
                entity.ToTable("woreda", "sys");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name).HasColumnName("name");
            });

            modelBuilder.Entity<Workflow>(entity =>
            {
                entity.ToTable("workflow", "wf");

                entity.ForNpgsqlHasComment("This table represents a state machine");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Aid).HasColumnName("aid");

                entity.Property(e => e.CurrentState).HasColumnName("current_state");

                entity.Property(e => e.CurrentWorkItem).HasColumnName("current_work_item");

                entity.Property(e => e.OldState)
                    .HasColumnName("old_state")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Timestamp).HasColumnName("timestamp");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title");

                entity.Property(e => e.Type).HasColumnName("type");

                entity.HasOne(d => d.A)
                    .WithMany(p => p.Workflow)
                    .HasForeignKey(d => d.Aid)
                    .HasConstraintName("workflow_action_fk");

                entity.HasOne(d => d.TypeNavigation)
                    .WithMany(p => p.Workflow)
                    .HasForeignKey(d => d.Type)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("workflow_type_fk");
            });

            modelBuilder.Entity<WorkflowType>(entity =>
            {
                entity.ToTable("workflow_type", "wf");

                entity.ForNpgsqlHasComment("This table enumerates different types of workflows");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('wf.workflow_type_id_seq'::regclass)");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");
            });

            modelBuilder.Entity<WorkItem>(entity =>
            {
                entity.ToTable("work_item", "wf");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.AssignedRole).HasColumnName("assigned_role");

                entity.Property(e => e.AssignedUser).HasColumnName("assigned_user");

                entity.Property(e => e.Data)
                    .HasColumnName("data")
                    .HasColumnType("json");

                entity.Property(e => e.DataType).HasColumnName("data_type");

                entity.Property(e => e.SeqNo)
                    .HasColumnName("seq_no")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Type).HasColumnName("type");

                entity.Property(e => e.Workflow).HasColumnName("workflow");

                entity.HasOne(d => d.WorkflowNavigation)
                    .WithMany(p => p.WorkItem)
                    .HasForeignKey(d => d.Workflow)
                    .HasConstraintName("work_item_workflow_fk");
            });

            modelBuilder.Entity<WorkitemNote>(entity =>
            {
                entity.ToTable("workitem_note", "wf");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Date).HasColumnName("date");
                entity.Property(e => e.ForwardTo).HasColumnName("forward_to");
                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasColumnName("note");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username");

                entity.Property(e => e.Workitem).HasColumnName("workitem");

                entity.HasOne(d => d.WorkitemNavigation)
                    .WithMany(p => p.WorkitemNote)
                    .HasForeignKey(d => d.Workitem)
                    .HasConstraintName("workflow_note_workitem_fk");
            });

            modelBuilder.HasSequence("property_party_id_seq");

            modelBuilder.HasSequence("property_type_id_seq");

            modelBuilder.HasSequence("audit_seq");

            modelBuilder.HasSequence("role_id_seq");

            modelBuilder.HasSequence("user_action_id_seq");

            modelBuilder.HasSequence("user_id_seq");

            modelBuilder.HasSequence("case_case_number_seq");

            modelBuilder.HasSequence("document_seq");

            modelBuilder.HasSequence("document_type_id_seq");

            modelBuilder.HasSequence("document_type_seq");

            modelBuilder.HasSequence("file_seq");

            modelBuilder.HasSequence("image_seq");

            modelBuilder.HasSequence("workflow_type_id_seq");
        }
    }
}
