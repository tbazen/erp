﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using intaps.cis.data.Entities;
using intaps.cis.domain.Infrastructure;

namespace intaps.cis.domain.Admin
{
    public interface IUserFacade
    {
        WSISResponseModel LoginWSISUser(UserSession session, LoginViewModel loginView, string WSISUrl);
        List<WsisUserViewModel> GetWSISUsers(UserSession userSession, string url);
        void LoginUser(UserSession session, LoginViewModel loginView);

        void RegisterUser(UserSession session, RegisterViewModel viewModel);

        void ChangePassword(UserSession session, string username, string oldPassword, string newPassword);

        IQueryable<UserViewModel> GetAllUsers(UserSession session);

        void AddUserRole(UserSession session, string username, int[] roles);

        void RevokeUserRole(UserSession session, string username, int[] roles);

        void DeactivateUser(UserSession session, string userame);
        
        void ActivateUser(UserSession session, string username);

        void UpdateUser(UserSession userSession, UserViewModel userVm);

        bool CheckUser(UserSession userSession, string username);
        void ResetPassword(UserSession getSession, string resetVmUserName, string resetVmNewPassword);
        IQueryable<UserActionViewModel> GetAllActions(UserSession getSession, int page, int pageSize);
        IQueryable<UserViewModel> GetUsers(UserSession userSession, string query);

        IList<UserViewModel> GetNonAdminUsers(UserSession session);

        IQueryable<UserAction> SearchActions(UserSession userSession, ActionSearch actionSearch);

        object GetRoles(UserSession session, string applicationFor);
        void UpdateRole(UserSession session, WsisUserViewModel roleVm);
        string GetSubscription(string url, string sessionId, string contractNo);
        List<UserWithName> GetWSISEmployees(UserSession userSession, string url);
        string GetEmployee(string username);
    }
    
    
    
    
    public class UserFacade : IUserFacade
    {
        private IUserService _userService;
        
        public UserFacade( IUserService service)
        {
            _userService = service;

        }

        public string GetSubscription(string url, string sessionId, string contractNo)
        {
            var customerName=_userService.GetSubscription(url, sessionId, contractNo);
            return customerName;
        }
        public void LoginUser(UserSession userSession, LoginViewModel loginViewModel)
        {    
 
            _userService.SetSession(userSession);
            _userService.LoginUser(loginViewModel);
        }
        public WSISResponseModel LoginWSISUser(UserSession userSession, LoginViewModel loginViewModel, string WSISUrl)
        {
            _userService.SetSession(userSession);
            var responseModel = _userService.LoginWSISUser(loginViewModel, WSISUrl);
            return responseModel;
        
           
        }

        public List<WsisUserViewModel> GetWSISUsers(UserSession userSession, string url)
        {
            _userService.SetSession(userSession);
            var users = _userService.GetWSISUsers(url);
            return users;
        }

        public List<UserWithName> GetWSISEmployees(UserSession userSession,string url)
        {
            _userService.SetSession(userSession);
            var employees = _userService.GetWSISEmployees(userSession.WSISSessionId, url);
            return employees;
        }
        public void RegisterUser(UserSession userSession, RegisterViewModel userVm)
        {
            _userService.SetSession(userSession);
            _userService.RegisterUser(userVm);
        }

        public void ChangePassword(UserSession userSession, string username, string oldPassword, string newPassword)
        {
            _userService.SetSession(userSession);
            _userService.ChangePassword(username, oldPassword, newPassword);
        }

        public void ResetPassword(UserSession userSession, string username, string newPassword)
        {
            _userService.SetSession(userSession);
            _userService.ResetPassword(username, newPassword);

        }

        public IQueryable<UserViewModel> GetUsers(UserSession userSession, string query)
        {
          
            _userService.SetSession(userSession);

            return _userService.GetUsers(query);
        }

        public IList<UserViewModel> GetNonAdminUsers(UserSession session)
        {
            _userService.SetSession(session);

            return _userService.GetNonAdminUsers();
        }

        public object GetRoles(UserSession session, string applicationFor)
        {
            _userService.SetSession(session);

            return _userService.GetRoles(applicationFor);
        }

        public IQueryable<UserViewModel> GetAllUsers(UserSession userSession)
        {
         
           _userService.SetSession(userSession);

            return _userService.GetAllUsers();
        }

        public IQueryable<UserActionViewModel> GetAllActions(UserSession userSession, int page, int pageSize)
        {
          
            _userService.SetSession(userSession);
            return _userService.GetAllActions(page, pageSize);
        }

        public IQueryable<UserAction> SearchActions(UserSession userSession, ActionSearch actionSearch)
        {
            _userService.SetSession(userSession);
            return _userService.SearchActions(actionSearch);
        }

        public void AddUserRole(UserSession userSession, string username, int[] roles)
        {
            _userService.SetSession(userSession);
            _userService.AddUserRole(username, roles);
        }

        public void RevokeUserRole(UserSession userSession, string username, int[] roles)
        {
            _userService.SetSession(userSession);
            _userService.RevokeUserRole(username, roles);
        }

        public void DeactivateUser(UserSession userSession, string username)
        {
            _userService.SetSession(userSession);
            _userService.DeactivateUser(username);
        }

        public void ActivateUser(UserSession userSession, string username)
        {
            _userService.SetSession(userSession);
            _userService.ActivateUser(username);
        }

        public void UpdateUser(UserSession userSession, UserViewModel userVm)
        {
            _userService.SetSession(userSession);
            _userService.UpdateUser(userVm);
        }

        public bool CheckUser(UserSession userSession, string username)
        {
           
            _userService.SetSession(userSession);

            return _userService.CheckUser(username);
        }

        public void UpdateRole(UserSession session, WsisUserViewModel roleVm)
        {
            _userService.SetSession(session);
            _userService.UpdateRole(roleVm);
        }

        public string GetEmployee(string username)
        {
           return _userService.GetWSISEmployee(null, null, username);
        }
    }
}