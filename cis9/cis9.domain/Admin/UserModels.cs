﻿using System;
using intaps.cis.data.Entities;
using Sieve.Attributes;

namespace intaps.cis.domain.Admin
{
    public enum UserActionType
    {
        REGISTER = 1,
        LOGIN = 2,
        LOGOUT = 3,
        CHANGE_PASSWORD = 4,
        RESET_PASSWORD = 5,
        DEACTIVATE_USER = 6,
        ADD_ROLE = 7,
        REVOKE_ROLE = 8,
        ACTIVATE_USER = 9,
        ADD_WORKITEM = 10,
        ADD_WORKFLOW = 11,
        UPDATE_WORKFLOW = 12,
        REMOVE_WORKITEM = 13,
        UPDATE_WORKITEM = 14,
        UPDATE_USER = 15,
        ADD_WORKITEM_NOTE = 16
    }

    public static class UserRoles
    {
        public const int Admin = 1;
        public const int Clerk = 2;
        public const int BillManager = 3;
        public const int Digitizer = 4;
        public const int DAMSUser = 5;
        public const int DAMSSupervisor = 6;
    }


    public class UserViewModel
    {
        [Sieve(CanFilter = true, CanSort = true)]
        public string UserName { get; set; }
        
        [Sieve(CanFilter = true, CanSort = true)]
        public string FullName { get; set; }
        
        public string PhoneNumber { get; set; }
        
        [Sieve(CanFilter = true, CanSort = false)]
        public int Status { get; set; }
        
        public string[] Roles { get; set; }
    }

    public class UserActionViewModel
    {
        [Sieve(CanFilter = true, CanSort = true)]
        public string UserName { get; set; }
        
        [Sieve(CanFilter = true, CanSort = true)]
        public ActionType Action { get; set; }
        
        [Sieve(CanFilter = true, CanSort = true)]
        public DateTime ActionTime { get; set; }
    }

    public class ActionSearch
    {
        public string UserName { get; set; }
        public int ActionType { get; set; }
        public string StartDate { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }


    public class LoginViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public int Role { get; set; }
    }

    public class RegisterViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public int[] Roles { get; set; }
    }


    public class ChangePasswordViewModel
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        public string UserName { get; set; }
        public string NewPassword { get; set; }
    }

    public class UserRoleViewModel
    {
        public string UserName { get; set; }
        public int[] Roles { get; set; }
    }
}