﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

using System.Threading.Tasks;
using intaps.cis.data;
using intaps.cis.data.Entities;
using intaps.cis.domain.Exceptions;
using intaps.cis.domain.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace intaps.cis.domain.Admin
{
     
    public interface IUserService
    {
        
        void SetSession(UserSession session);
        
        void LoginUser(LoginViewModel loginView);
        
        WSISResponseModel WSISLogin(string url, string username, string password);

        WSISResponseModel LoginWSISUser(LoginViewModel loginView,string WSISUrl);
        
        List<WsisUserViewModel> GetWSISUsers(string url);

        void RegisterUser(RegisterViewModel user);

        IQueryable<UserViewModel> GetAllUsers();

        bool CheckUser(string username);
        void UpdateUser(UserViewModel userVm);
        void ActivateUser(string username);
        void DeactivateUser(string username);
        void RevokeUserRole(string username, int[] roles);
        void AddUserRole(string username, int[] roles);
        IQueryable<UserActionViewModel> GetAllActions(int page, int pageSize);
        IQueryable<UserViewModel> GetUsers(string query);
        User GetUser(string username);

        bool HasRole(string username, int role);
        
        void ResetPassword(string username, string newPassword);
        void ChangePassword(string username, string oldPassword, string newPassword);
        object GetRoles(string applicationFor);
        IList<UserViewModel> GetNonAdminUsers();

        IQueryable<UserAction> SearchActions(ActionSearch actionSearch);
        void UpdateRole(WsisUserViewModel userRole);
        string GetSubscription(string url, string sessionId, string contractNo);
        List<UserWithName> GetWSISEmployees(string sessionId, string url);
        string GetWSISEmployee(string sessionId, string url, string username);
    }
    
    
    
    
    public class UserService : IUserService
    {
        private CisContext _context;
        private UserSession _userSession;

        public UserService(CisContext context)
        {
            _context = context;
        }

        public void SetSession(UserSession session)
        {
            _userSession = session;
        }

        public string GetSubscription(string url, string sessionId, string contractNo)
        {
            var subsc = INTAPS.ClientServer.ApplicationServer.GetBDE<INTAPS.SubscriberManagment.BDE.SubscriberManagmentBDE>().GetSubscription(contractNo, DateTime.Now.Ticks);
            return subsc.subscriber.name;
        }

        public WSISResponseModel WSISLogin(string url, string username, string password)
        {
            var wsissession = INTAPS.ClientServer.ApplicationServer.CreateUserSession(username, password, "DAMS Web");
            var sinfo = INTAPS.ClientServer.ApplicationServer.getSessionInfo(wsissession);
            return new WSISResponseModel()
            {
                SessionID = wsissession,
                UserID = sinfo.UserID,

            };
        }

        public string GetWSISEmployee(string sessionId, string url, string username)
        {

            var e=INTAPS.ClientServer.ApplicationServer.GetBDE<INTAPS.Payroll.BDE.PayrollBDE>().GetEmployeeByLoginName(username);
            if (e == null)
                return username;
            return e.employeeName;
        }
        public List<UserWithName> GetWSISEmployees(string sessionId, string url)
        {

            var users = INTAPS.ClientServer.ApplicationServer.SecurityBDE.GetAllUsers();
            var employees = new List<UserWithName>();
            foreach (var u in users)
            {
                  var emp = GetWSISEmployee(sessionId, url, u);
                    var empVms = new UserWithName()
                    {
                        Id = INTAPS.ClientServer.ApplicationServer.SecurityBDE.GetUIDForName(u), UserName = u, FullName = emp
                    };
                    employees.Add(empVms);
            }
            return employees;
            
        }

        public List<WsisUserViewModel> GetWSISUsers(string url)
        {

            var users = INTAPS.ClientServer.ApplicationServer.SecurityBDE.GetAllUsers();

            var userVms = new List<WsisUserViewModel>();

            foreach (var user in users)
            {
                var userid = INTAPS.ClientServer.ApplicationServer.SecurityBDE.GetUIDForName(user);
                var userVm = new WsisUserViewModel
                {
                    UserName = user,
                    UserId = userid,
                    Role = _context.UserRole.Where(u => u.Userid == userid).Select(ur => ur.Role.Name).ToArray(),
                    Fullname = GetWSISEmployee(null,null,user)
                };


                userVms.Add(userVm);

            }

            return userVms;
            
            
          
        }
        public bool WSISAPILogout(string sessionID,string url)
        {
         
            try
            {
                INTAPS.ClientServer.ApplicationServer.CloseSession(sessionID);    
                return true;
            }

            catch (Exception)
            {
                return true;
            }
        }
        public WSISResponseModel LoginWSISUser(LoginViewModel loginView,string url)
        {
            
            
            try
            {

                var sessionID = INTAPS.ClientServer.ApplicationServer.CreateUserSession(loginView.Username, loginView.Password,"DAMS Web");
                var sessionInfo = INTAPS.ClientServer.ApplicationServer.getSessionInfo(sessionID);
                if (sessionID == null) throw new AccessDeniedException("Invalid Username/Password or System Error");
                
                return new WSISResponseModel()
                {
                    SessionID = sessionInfo.SessionID,
                    UserID = sessionInfo.UserID
                };
            }
            catch (InvalidOperationException e)
            {
                //return null;
                throw new AccessDeniedException("Invalid Username, Please Try Again");
               
            }
        }
        public void LoginUser(LoginViewModel loginView)
        {
            var hashPassword = loginView.Password.Hash();

            try
            {
                User dbUser = _context.User.First(u => u.Username == loginView.Username && u.Password == hashPassword);

                if (dbUser.Status == 0)
                {
                    throw new AccessDeniedException("User Deactivated");
                }

             
                _context.SaveChanges(_userSession.UserName, (int)UserActionType.LOGIN);


            }
            
            catch(InvalidOperationException e)
            {
                throw new AccessDeniedException("Username, Password or Role Incorrect");
            }
        }

        public void RegisterUser(RegisterViewModel userVm)
        {
            
            User user = new User
            {
                Username = userVm.Username,
                FullName = userVm.FullName,
                Password = userVm.Password.Hash(),
                PhoneNo = userVm.PhoneNumber
            };

       
            _context.User.Add(user);

            foreach (var ur in userVm.Roles)
            {
                var role = GetRole(ur);

                UserRole userRole = new UserRole
                {
                    User = user,
                    Role = role
                };        
                _context.UserRole.Add(userRole);
            }
            
            _context.SaveChanges(_userSession.UserName, (int)UserActionType.REGISTER);
        }
        
        public void ChangePassword(string username, string oldPassword, string newPassword)
        {
            var hashPassword = oldPassword.Hash();

            User user = GetUser(username);
            
         

            if (!user.Password.Equals(hashPassword)) throw new AccessDeniedException("Incorrect Password");
            user.Password = newPassword.Hash();

            _context.User.Update(user);        
            _context.SaveChanges(username, (int)UserActionType.CHANGE_PASSWORD);
        }

        public object GetRoles(string applicationFor)
        {
            object roles=null;
            if (applicationFor.Equals("cis"))
            {
                roles = _context.UserRole.Where(ur => ur.User.Username.Equals(_userSession.UserName))
                    .Select(ur => new {id = ur.Roleid, name = ur.Role.Name}).ToList();
            }
            else if (applicationFor.Equals("wsis"))
            {
                roles = _context.UserRole.Where(ur => ur.Userid==_userSession.UserId)
                    .Select(ur => new {id = ur.Roleid, name = ur.Role.Name}).ToList();
            }

            return roles;

        }

        public IList<UserViewModel> GetNonAdminUsers()
        {
            var userIds = _context.UserRole.Where(ur => ur.Roleid != 1).Select(ur => ur.Userid);
            
            var userVms = new List<UserViewModel>();

            foreach (var userId in userIds)
            {
                var user = _context.User.First(u => u.Id == userId);
                var userVm = new UserViewModel {UserName =  user.Username, FullName = user.FullName, PhoneNumber = user.PhoneNo, Status = user.Status};

                userVm.Roles = _context.UserRole.Where(u => u.Userid == user.Id).Select(ur => ur.Role.Name).ToArray();

                
                userVms.Add(userVm);

            }

            return userVms;
        }

        public void ResetPassword(string username, string newPassword)
        {
            var user = GetUser(username);

            
            user.Password = newPassword.Hash();
    
            _context.SaveChanges(_userSession.UserName, (int)UserActionType.RESET_PASSWORD);
        }
        
        public void AddUserRole(string username, int[] roles)
        {
            var user = GetUser(username);

            var userRoles = _context.UserRole.Where(u => u.Userid == user.Id).Select(ur => ur.Role.Id);

            foreach (var role in roles)
            {
                if(userRoles.Contains(role)) continue;
                
                var userRole = new UserRole { User = user, Role = GetRole(role)};

                _context.UserRole.Add(userRole);
            }

            _context.SaveChanges(_userSession.UserName, (int)UserActionType.ADD_ROLE);
        }

        public void RevokeUserRole(string username, int[] roles)
        {
            var user = GetUser(username);
            
          
            foreach (var role in roles)
            {
                var userRole = _context.UserRole.First(ur => ur.Userid == user.Id && ur.Role.Id == role);

                _context.UserRole.Remove(userRole);
            }
            
            _context.SaveChanges(_userSession.UserName, (int)UserActionType.REVOKE_ROLE);
        }

        public void DeactivateUser(string username)
        {
            var user = GetUser(username);
            
            
            user.Status = 0;

            _context.User.Update(user);
          
            _context.SaveChanges(_userSession.UserName, (int)UserActionType.DEACTIVATE_USER);

        }
        
        public void ActivateUser(string username)
        {
            var user = GetUser(username);
            
            
            user.Status = 1;

            _context.User.Update(user);
            
            _context.SaveChanges(_userSession.UserName, (int)UserActionType.ACTIVATE_USER);

        }

        public IQueryable<UserViewModel> GetUsers(string filter)
        {
            var users = _context.User.Where(u => u.Username.Contains(filter) || u.FullName.Contains(filter));
            
            var userVms = new List<UserViewModel>();

            foreach (var user in users)
            {
                var userVm = new UserViewModel {UserName =  user.Username, FullName = user.FullName, PhoneNumber = user.PhoneNo, Status = user.Status};

                userVm.Roles = _context.UserRole.Where(u => u.Userid == user.Id).Select(ur => ur.Role.Name).ToArray();

                
                userVms.Add(userVm);

            }

            return userVms.AsQueryable();
        }
        
        public IQueryable<UserViewModel> GetAllUsers()
        {
            var users = _context.User.AsNoTracking().ToList();
            
            var userVms = new List<UserViewModel>();

            foreach (var user in users)
            {
                var userVm = new UserViewModel
                {
                    UserName = user.Username,
                    FullName = user.FullName,
                    PhoneNumber = user.PhoneNo,
                    Status = user.Status,
                    Roles = _context.UserRole.Where(u => u.Userid == user.Id).Select(ur => ur.Role.Name).ToArray()
                };


                userVms.Add(userVm);

            }

            return userVms.AsQueryable();
        }


        public IQueryable<UserActionViewModel> GetAllActions(int page, int pageSize)
        {
            var actions = page == 1 ? _context.UserAction.Take(pageSize).ToList() : _context.UserAction.Skip((page -1 ) * pageSize).Take(pageSize).ToList();
            
            var actionVms = new List<UserActionViewModel>();
            
            foreach (var userAction in actions)
            {
              
                var actionType = GetActionType(userAction.ActionType);
                
                actionVms.Add(new UserActionViewModel
                {
                    UserName = userAction.Username,
                    Action = new ActionType(){Id = actionType.Id, Name = actionType.Name},
                    ActionTime = new DateTime(userAction.Timestamp)
                });
                
            }

            return actionVms.AsQueryable();
        }
        
        public IQueryable<UserAction> SearchActions(ActionSearch actionSearch)
        {
            actionSearch.UserName = actionSearch.UserName ?? "";
            actionSearch.StartDate = actionSearch.StartDate ?? DateTime.MinValue.ToLongDateString();


            var actions = from action in _context.UserAction
                where (string.IsNullOrEmpty(actionSearch.UserName) ||
                       action.Username.ToLower().Equals(actionSearch.UserName)) &&
                      (string.IsNullOrEmpty(actionSearch.StartDate) || DateTime.Parse(actionSearch.StartDate).Ticks < action.Timestamp) 
                      && (action.ActionType == actionSearch.ActionType)
                select action;
            
            return actions;
        }
        
        public void UpdateUser(UserViewModel userVm)
        {
            var user = GetUser(userVm.UserName);

            user.FullName = userVm.FullName;
            user.PhoneNo = userVm.PhoneNumber;
            
            var userRoles = _context.UserRole.Where(u => u.Userid == user.Id);
            
            //Remove Previous roles
            foreach (var role in userRoles)
            {
                /* var oldRole = _context.Role.First(r => r.Id == role.Roleid).Name;
                 if(userVm.Roles.Contains(oldRole)) continue;*/
                _context.UserRole.Remove(role);
            }

            _context.SaveChanges();
           
            
            foreach (var role in userVm.Roles)
            {
                var ur = _context.Role.First(r => r.Name.Equals(role));
     
                var userRole = new UserRole { User = user, Role = ur};

                _context.UserRole.Add(userRole);
            }

            _context.User.Update(user);

            _context.SaveChanges(_userSession.UserName, (int)UserActionType.UPDATE_USER);
        }

        public User GetUser(string username)
        {
            return _context.User.First(u => u.Username == username);
        }

        public bool HasRole(string username, int role)
        {
            var userId = GetUser(username).Id;
            var roleId = GetRole(role).Id;

            return _context.UserRole.Any(ur => ur.Roleid == roleId && ur.Userid == userId);

        }

        private User GetUser(long id)
        {
            return _context.User.First(u => u.Id == id);
        }
        
        
        private Role GetRole(int role)
        {
            return _context.Role.First(r => r.Id == role);
        }

    

        private ActionType GetActionType(int id)
        {
            return _context.ActionType.First(at => at.Id == id);
        }

        public bool CheckUser(string username)
        {
            return _context.User.Any(u => u.Username == username);
        }

        public void UpdateRole(WsisUserViewModel userRole)
        {
            var ur= _context.UserRole.Where(u => u.Userid == userRole.UserId);
            foreach (var role in ur)
            {
                _context.UserRole.Remove(role);
            }
            
            foreach (var role in userRole.Role)
            {
                var urs = _context.Role.First(r => r.Name.Equals(role));
     
                var uRole = new UserRole { Userid = userRole.UserId, Role = urs};

                _context.UserRole.Add(uRole);
            }
            _context.SaveChanges(_userSession.UserName, (int)UserActionType.UPDATE_USER);
            
        }
        
    }
}