﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using intaps.cis.data;
using intaps.cis.data.Entities;
using intaps.cis.domain.Extensions;
using intaps.cis.domain.Workflows.CaseWorkflow;
using intaps.cis.domain.Workflows.DamsWorkflow;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;


namespace intaps.cis.domain.Archive
{
    public class FileSearch
    {
        public string FileOwner { get; set; }
        public string FileCode { get; set; }
        public string Folder { get; set; }
        public string Upin { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }

    public class DocumentSearch
    {
        public string ReferenceNo { get; set; }
        public int Type { get; set; }
        public string Date { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
}

    public class CaseSearch
    {
        public string CaseNo { get; set; }
        public int CaseStatus { get; set;}
        public string Date { get; set; }
    }

    public interface IArchiveService
    {

        void SetContext(CisContext context);
        
        IQueryable<FileModel> SearchFiles(FileSearch fileSearch);

        IQueryable<DocumentModel> SearchDocuments(DocumentSearch docSearch);
        FileModel GetFile(string workflow, string fileId);
        FileModel GetFile(string fileId);
        DocumentModel GetDocument(string docId);
        object ArchiveStat();
        bool CheckFile(string fileId);
        FileModel GetDocFile(string fileId);
        FileStat GetFileStat(string fileId, string workflow = null);

        IList<CaseModel> SearchCaseDetails(CaseSearch caseSearch);
        
    }

    public class ArchiveService : IArchiveService
    {
        private CisContext _cisContext;
        private string fileLocation;
        private Regex mimeTypeRegx = new Regex("(.+)/(.+)");

        public ArchiveService(IOptions<DamsOptions> options)
        {
            fileLocation = options.Value.ImagePath;
        }


        public void SetContext(CisContext context)
        {
            _cisContext = context;
        }
        
        public FileModel GetFile(string wid, string fileid)
        {
            var workItemId = Guid.Parse(wid);

            var id = Guid.Parse(fileid);

            var fileModel = new FileModel();

            if (!_cisContext.ArchiveFile.Any(file => file.Id == id))
            {
                var workflowId = _cisContext.WorkItem.First(wi => wi.Id == workItemId).Workflow;
                var workItems = _cisContext.WorkItem.Where(wi => wi.Workflow == workflowId).OrderBy(wi => wi.SeqNo)
                    .ToList();

                var fileType = typeof(FileModel).FullName;
                var documentType = typeof(DocumentModel).FullName;
                var imageType = typeof(ImageModel).FullName;

                foreach (var workItem in workItems)
                {
                    if (string.IsNullOrEmpty(workItem.DataType)) continue;
                    if (workItem.DataType.Equals(fileType))
                    {
                        var file = JsonConvert.DeserializeObject<FileModel>(workItem.Data);
                        if (file.Id.Equals(fileid))
                        {
                            fileModel.Id = file.Id;
                            fileModel.Folder = file.Folder;
                            fileModel.Shelf = file.Shelf;
                            fileModel.Location = file.Location;
                            fileModel.Remark = file.Remark;
                            fileModel.FileCode = file.FileCode;
                            fileModel.FileOwner = file.FileOwner;
                            fileModel.MobileNumber = file.MobileNumber;
                            fileModel.Upin = file.Upin;
                            fileModel.WorkItem = file.WorkItem;
                        }
                    }
                    else if (workItem.DataType.Equals(documentType))
                    {
                        var document = JsonConvert.DeserializeObject<DocumentModel>(workItem.Data);
                        if (document.FileId.Equals(fileid))
                        {
                            var docModel = new DocumentModel
                            {
                                Id = document.Id,
                                FileId = document.FileId,
                                ReferenceNo = document.ReferenceNo,
                                Sheets = document.Sheets,
                                Type = document.Type,
                                TimeStamp = document.TimeStamp,
                                WorkItem = document.WorkItem,
                                Date = document.Date
                            };

                            if (fileModel.Documents.Any(doc => doc.Id == document.Id))
                            {
                                fileModel.Documents.Remove(docModel);
                                fileModel.Documents.Add(docModel);
                            }

                            else
                            {
                                fileModel.Documents.Add(docModel);
                            }
                        }
                    }

                    else if (workItem.DataType.Equals(imageType))
                    {
                        var image = JsonConvert.DeserializeObject<ImageModel>(workItem.Data);
                        var document = fileModel.Documents.First(img => img.Id == image.DocumentId);
                        var imgModel = new ImageModel
                        {
                            Id = image.Id,
                            Description = image.Description,
                            DocumentId = image.DocumentId,
                            Data = image.Data,
                            WorkItem = image.WorkItem,
                            Page = image.Page,
                            Mime = image.Mime
                        };
                        if (document.Images.Any(img => img.Id == image.Id))
                        {
                            document.Images.Remove(image);
                            document.Images.Add(imgModel);
                        }
                        else
                        {
                            document.Images.Add(imgModel);
                        }
                    }
                }
            }
            else
            {
                var file = _cisContext.ArchiveFile.First(f => f.Id.Equals(id));

                fileModel.Id = file.Id.ToString();
                fileModel.Folder = file.Folder;
                fileModel.Shelf = file.Shelf;
                fileModel.Location = file.Location;
                fileModel.Remark = file.Remark;
                fileModel.FileCode = file.FileCode;
                fileModel.FileOwner = file.FileOwner;
                fileModel.MobileNumber = file.MobileNumber;
                fileModel.Upin = file.Upin;
                fileModel.WorkItem = file.WorkItem.ToString();

                var documents = _cisContext.Document.Where(doc => doc.FileId.Equals(id));

                foreach (var document in documents)
                {
                    var docModel = new DocumentModel
                    {
                        Id = document.Id.ToString(),
                        FileId = document.FileId.ToString(),
                        ReferenceNo = document.ReferenceNo,
                        Sheets = document.Sheets,
                        Type =_cisContext.DocumentType.First(dt => dt.Id == document.Type),
                        TimeStamp = document.Timestamp,
                        WorkItem = document.Workitem.ToString()
                    };
                    fileModel.Documents.Add(docModel);

                    var images = _cisContext.ArchiveImage.Where(img => img.DocumentId.Equals(document.Id));

                    foreach (var image in images)
                    {
                        var imgModel = new ImageModel
                        {
                            Id = image.Id.ToString(),
                            Description = image.Description,
                            DocumentId = image.DocumentId.ToString(),
                            WorkItem = image.Workitem.ToString(),
                            Page = image.Page.ToString(),
                            Mime = image.Mime
                        };
                        
                        var imgEx = mimeTypeRegx.Match(imgModel.Mime).Groups[2].Value;
                        var path = $"{fileLocation}{imgModel.Id}.{imgEx}";
                        var byteArr = File.ReadAllBytes(path);

                        imgModel.Data = Convert.ToBase64String(byteArr);

                        docModel.Images.Add(imgModel);
                        
                        docModel.Images.Add(imgModel);
                    }
                }
            }

            return fileModel;
        }

        public FileModel GetFile(string fileId)
        {
            var id = Guid.Parse(fileId);

            var fileModel = new FileModel();
            
            var file = _cisContext.ArchiveFile.First(f => f.Id.Equals(id));

            fileModel.Id = file.Id.ToString();
            fileModel.Folder = file.Folder;
            fileModel.Shelf = file.Shelf;
            fileModel.Location = file.Location;
            fileModel.Remark = file.Remark;
            fileModel.FileCode = file.FileCode;
            fileModel.FileOwner = file.FileOwner;
            fileModel.MobileNumber = file.MobileNumber;
            fileModel.Upin = file.Upin;
            fileModel.WorkItem = file.WorkItem.ToString();

            var documents = _cisContext.Document.Where(doc => doc.FileId.Equals(id));

            foreach (var document in documents)
            {
                var docModel = new DocumentModel
                {
                    Id = document.Id.ToString(),
                    FileId = document.FileId.ToString(),
                    ReferenceNo = document.ReferenceNo,
                    Sheets = document.Sheets,
                    Type =_cisContext.DocumentType.First(dt => dt.Id == document.Type),
                    TimeStamp = document.Timestamp,
                    Date = EtGrDate.ToEthDate(new DateTime(document.Date).ToShortDateString()),
                    WorkItem = document.Workitem.ToString()
                };
                fileModel.Documents.Add(docModel);

                var images = _cisContext.ArchiveImage.Where(img => img.DocumentId.Equals(document.Id));

                foreach (var image in images)
                {
                    var imgModel = new ImageModel
                    {
                        Id = image.Id.ToString(),
                        Description = image.Description,
                        DocumentId = image.DocumentId.ToString(),
                        WorkItem = image.Workitem.ToString(),
                        Page = image.Page.ToString(),
                        Mime = image.Mime
                    };
                        
                    try
                    {
                        var imgEx = mimeTypeRegx.Match(imgModel.Mime).Groups[2].Value;
                        var path = Path.Combine(fileLocation, $"{imgModel.Id}.{imgEx}");
                        var byteArr = File.ReadAllBytes(path);

                        imgModel.Data = Convert.ToBase64String(byteArr);
                    }
                    catch (FileNotFoundException e)
                    {
                        imgModel.Data = string.Empty;
                    }

                    docModel.Images.Add(imgModel);
                }
            }

            return fileModel;
        }

        public bool CheckFile(string fileId)
        {
            return _cisContext.ArchiveFile.Any(file => file.Id.Equals(fileId.ToGuid()));
        }

        public FileStat GetFileStat(string fileId, string workflow=null)
        {
            var fileStat = new FileStat();
            if (workflow == null)
            {
                fileStat.NumDocuments = _cisContext.Document.Count(doc => doc.FileId == fileId.ToGuid());
                fileStat.NumSheets = _cisContext.Document.Where(doc => doc.FileId == fileId.ToGuid())
                    .Select(doc => doc.Sheets).Sum();

                return fileStat;
            }

            var workItems = _cisContext.WorkItem.Where(wi => wi.Workflow == workflow.ToGuid() && wi.DataType.Equals("intaps.cis.domain.Workflows.DamsWorkflow.DocumentModel"));

            foreach (var item in workItems)
            {
                var doc = JsonConvert.DeserializeObject<DocumentModel>(item.Data);

                if (doc.FileId.Equals(fileId))
                {
                    fileStat.NumDocuments++;
                    fileStat.NumSheets += doc.Sheets;
                }
            }

            return fileStat;

        }
        
        public FileModel GetDocFile(string fileId)
        {
            var id = Guid.Parse(fileId);

            var fileModel = new FileModel();
            
            var file = _cisContext.ArchiveFile.First(f => f.Id.Equals(id));

            fileModel.Id = file.Id.ToString();
            fileModel.Folder = file.Folder;
            fileModel.Shelf = file.Shelf;
            fileModel.Location = file.Location;
            fileModel.Remark = file.Remark;
            fileModel.FileCode = file.FileCode;
            fileModel.FileOwner = file.FileOwner;
            fileModel.MobileNumber = file.MobileNumber;
            fileModel.Upin = file.Upin;
            fileModel.WorkItem = file.WorkItem.ToString();

            return fileModel;
        }

        public DocumentModel GetDocument(string docId)
        {
            var document = _cisContext.Document.First(doc => doc.Id == docId.ToGuid());
            
            var docModel = new DocumentModel()
            {
                Id = document.Id.ToString(),
                FileId = document.FileId.ToString(),
                ReferenceNo = document.ReferenceNo,
                Sheets = document.Sheets,
                Type =_cisContext.DocumentType.First(dt => dt.Id == document.Type),
                Date = EtGrDate.ToEthDate(new DateTime(document.Date).ToShortDateString()),
                WorkItem = document.Workitem.ToString()
            };
            
            var images = _cisContext.ArchiveImage.Where(img => img.DocumentId.Equals(document.Id));

            foreach (var image in images)
            {
                var imgModel = new ImageModel
                {
                    Id = image.Id.ToString(),
                    Description = image.Description,
                    DocumentId = image.DocumentId.ToString(),
                    WorkItem = image.Workitem.ToString(),
                    Page = image.Page.ToString(),
                    Mime = image.Mime
                };

                try
                {
                    var imgEx = mimeTypeRegx.Match(imgModel.Mime).Groups[2].Value;
                    var path = Path.Combine(fileLocation, $"{imgModel.Id}.{imgEx}");
                    var byteArr = File.ReadAllBytes(path);

                    imgModel.Data = Convert.ToBase64String(byteArr);
                }
                catch (FileNotFoundException e)
                {
                    imgModel.Data = null;
                }
                

                docModel.Images.Add(imgModel);
            }

            return docModel;
        }

        public object ArchiveStat()
        {
            var fileNums = _cisContext.ArchiveFile.Count();
            var docNums = _cisContext.Document.Count();

            return new {files = fileNums, docs = docNums};
        }
      
        public IQueryable<FileModel> SearchFiles(FileSearch fileSearch)
        {
            var fileVms = new List<FileModel>();

            fileSearch.Upin = fileSearch.Upin ?? "";
            fileSearch.FileCode = fileSearch.FileCode ?? "";
            fileSearch.FileOwner = fileSearch.FileOwner ?? "";
            fileSearch.Folder = fileSearch.Folder ?? "";

            var files = _cisContext.ArchiveFile.Where(f =>
                (String.IsNullOrEmpty(fileSearch.FileCode) || f.FileCode.StartsWith(fileSearch.FileCode))
                && (String.IsNullOrEmpty(fileSearch.FileOwner) || f.FileOwner.Contains(fileSearch.FileOwner))
                && (String.IsNullOrEmpty(fileSearch.Folder) || f.Folder.StartsWith(fileSearch.Folder))
                && (String.IsNullOrEmpty(fileSearch.Upin) || f.Upin.StartsWith(fileSearch.Upin))).AsNoTracking();


            foreach (var file in files)
            {
                var fileModel = new FileModel
                {
                    Id = file.Id.ToString(),
                    Folder = file.Folder,
                    Shelf = file.Shelf,
                    Location = file.Location,
                    Remark = file.Remark,
                    FileCode = file.FileCode,
                    FileOwner = file.FileOwner,
                    MobileNumber = file.MobileNumber,
                    Upin = file.Upin,
                    WorkItem = file.WorkItem.ToString()
                };

                fileVms.Add(fileModel);
            }


            return fileVms.AsQueryable();
        }

        public IQueryable<DocumentModel> SearchDocuments(DocumentSearch docSearch)
        {
            var docVms = new List<DocumentModel>();

            IQueryable<Document> documents = null;

            documents = _cisContext.Document.Where(doc =>
                (!String.IsNullOrEmpty(docSearch.Date) && doc.Date == DateTime.Parse(docSearch.Date).Ticks) ||
                (!String.IsNullOrEmpty(docSearch.ReferenceNo) && doc.ReferenceNo.Contains(docSearch.ReferenceNo)) ||
                doc.Type == docSearch.Type);
            
            foreach (var document in documents)
            {
                
               
                var docModel = new DocumentModel
                {
                    Id = document.Id.ToString(),
                    ReferenceNo = document.ReferenceNo,
                    Sheets = document.Sheets,
                    Type =_cisContext.DocumentType.First(docType => docType.Id == document.Type),
                    TimeStamp = document.Timestamp,
                    Date = new DateTime(document.Date).ToShortDateString(),
                    FileId = document.FileId.ToString()
                };
                
                docVms.Add(docModel);
            }


            return docVms.AsQueryable();
        }

        public IList<CaseModel> SearchCaseDetails(CaseSearch caseSearch)
        {
            var caseVms=new List<CaseModel>();

            var cases= _cisContext.CaseSearchResult.FromSql("SElect * from wf.case_search_result where status="+caseSearch.CaseStatus+" or casenumber='"+caseSearch.CaseNo+"' or date='"+caseSearch.Date+"'");
            

            foreach (var c in cases)
            {
                var caseModel = new CaseModel
                {
                    Id = c.Id.ToString(),
                    Title = c.Title,
                    CaseType = c.CaseType,
                    CaseNumber = int.Parse(c.CaseNumber),
                    Workflow = c.Workflow.ToString(),
                    Date = c.Date,
                    Status = c.Status,
                    CreatedBy = c.CreatedBy
                };
               caseVms.Add(caseModel);
            }

            return caseVms;
        }
    }
}