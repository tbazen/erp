﻿using System;

namespace intaps.cis.domain.Exceptions
{
    public class AccessDeniedException : Exception
    {
         public AccessDeniedException(string message): base(message){}
    }
}