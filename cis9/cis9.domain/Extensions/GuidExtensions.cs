﻿using System;

namespace intaps.cis.domain.Extensions
{
    public static class GuidExtensions
    {
        public static Guid ToGuid(this string s)
        {
            return Guid.Parse(s);
        }       
    }
}