namespace intaps.cis.domain.Extensions
{
    public class HostConfiguration
    {
        public string WSISHost { get; set; }
        public string CityName { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationLogo { get; set; }
        public string ApplicationFor { get; set; }
        public string Language { get; set; }
    }
}