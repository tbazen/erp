﻿using System.Data;
using System.Data.Common;
using Npgsql;

namespace intaps.cis.domain.Infrastructure.Database
{

    public interface ICisConnection
    {

        DbConnection GetWriteConnection();

        void CloseConnection();
    }
    
    public class CisConnection : ICisConnection
    {
        private static DbConnection _dbConnection;

        public DbConnection GetWriteConnection()
        {
            if (_dbConnection == null || _dbConnection.State != ConnectionState.Open)
            {
                _dbConnection = new NpgsqlConnection(@"Host=localhost;Database=cis;Username=postgres;Password=toor");
                _dbConnection.Open();
                
            }          
            return _dbConnection;
        }

        public void CloseConnection()
        {
            _dbConnection.Close();
            _dbConnection.Dispose();
        }
    }
}