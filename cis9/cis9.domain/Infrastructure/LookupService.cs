﻿using System.Collections.Generic;
using System.Linq;
using intaps.cis.data;
using intaps.cis.data.Entities;

namespace intaps.cis.domain.Infrastructure
{
    public interface ILookupService
    {
        IList<DocumentType> GetDocumentTypes();
        object GetRoles();
        object GetCaseTypes();
        IList<ActionType> GetActionTypes();
    }

    public class LookupService : ILookupService
    {
        private CisContext _cisContext;

        public LookupService(CisContext context)
        {
            _cisContext = context;
        }
        
        public IList<DocumentType> GetDocumentTypes()
        {  
           return _cisContext.DocumentType.ToList();
            
        }

        public IList<ActionType> GetActionTypes()
        {
            return _cisContext.ActionType.ToList();
        }

        public object GetRoles()
        {
            return _cisContext.Role.Select(role => new {id = role.Id, name = role.Name}).ToList();
        }

        public object GetCaseTypes()
        {
            return _cisContext.CaseType.Select(casetype => new {id = casetype.Id, name = casetype.Name}).ToList();
        }
    }
}