﻿using System;
using System.Collections.Generic;

namespace intaps.cis.domain.Infrastructure
{
    public class UserSession
    {
        public string UserName { get; set; }
        public int Role { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime LastSeen { get; set; }
        public string WSISSessionId { get; set; }
        public int UserId { get; set; }
        public Dictionary<string, object> Content;
    }
}