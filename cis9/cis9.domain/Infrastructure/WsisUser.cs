namespace intaps.cis.domain.Infrastructure
{
    public class WsisUser
    {
        public string UserName { get; set; }
        public int Id { get; set; }
        public int ParentId { get; set;}
    }

    public class WsisUserViewModel
    {
        public string UserName { get; set;}
        public int UserId { get; set; }
        public string[] Role { get; set; }
        public string Fullname { get; set; }
    }

    public class WSISResponseModel
    {
        public string SessionID { get; set; }
        public int UserID { get; set; }
    }
    
    public class UserWithName
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
    }
}