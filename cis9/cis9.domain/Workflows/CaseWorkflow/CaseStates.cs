﻿using intaps.cis.domain.Workflows.DamsWorkflow;
using intaps.cis.domain.Workflows.Models;
using Stateless;

namespace intaps.cis.domain.Workflows.CaseWorkflow{
    


    public enum CaseTrigger
    {
        Open = 1,
        Forward = 2,
        Close = 3,
        Cancel = 4
    }

    public static class CaseTriggerParameters
    {
        public static StateMachine<State, CaseTrigger>.TriggerWithParameters<WorkflowModel, WorkItemModel> OpenCaseTrigger;
        public static StateMachine<State, CaseTrigger>.TriggerWithParameters<WorkItemModel, WorkItemNote> ForwardCaseTrigger;
        public static StateMachine<State, CaseTrigger>.TriggerWithParameters<string, int> CloseCaseTrigger;
        public static StateMachine<State, CaseTrigger>.TriggerWithParameters<string, int> CancelCaseTrigger;

        public static void ConfigureTriggers(StateMachine<State, CaseTrigger> caseMachine)
        {
            OpenCaseTrigger = caseMachine.SetTriggerParameters<WorkflowModel, WorkItemModel>(CaseTrigger.Open);
            ForwardCaseTrigger = caseMachine.SetTriggerParameters<WorkItemModel, WorkItemNote>(CaseTrigger.Forward);
            CloseCaseTrigger = caseMachine.SetTriggerParameters<string, int>(CaseTrigger.Close);
            CancelCaseTrigger = caseMachine.SetTriggerParameters<string, int>(CaseTrigger.Cancel);
        }
        
    } 
}