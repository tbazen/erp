﻿using System;
using intaps.cis.domain.Infrastructure;
using intaps.cis.domain.Workflows.DamsWorkflow;
using intaps.cis.domain.Workflows.Models;
using Stateless;

namespace intaps.cis.domain.Workflows.CaseWorkflow
{
    public class CaseWorkflow
    {
        private StateMachine<State, CaseTrigger> _caseStateMachine;

        private ICaseWorkflowService _caseWorkflowService;

        private State _state;

        public CaseWorkflow(ICaseWorkflowService caseWorkflowService, int intitalState, UserSession session, string applicationFor)
        {
            if(applicationFor.Equals("wsis"))
                INTAPS.ClientServer.ApplicationServer.RenewSession(session.WSISSessionId);
            _caseWorkflowService = caseWorkflowService;
            _caseWorkflowService.SetSession(session);
            _state = (State)Enum.Parse(typeof(State),intitalState.ToString());
            
            _caseStateMachine = new StateMachine<State, CaseTrigger>(_state);

            ConfigureStateMachine();
        }

        private void ConfigureStateMachine()
        {
            CaseTriggerParameters.ConfigureTriggers(_caseStateMachine);

            _caseStateMachine.Configure(State.Initial)
                .Permit(CaseTrigger.Open, State.Opened);

            //Open case
            _caseStateMachine.Configure(State.Opened)
                .Permit(CaseTrigger.Close, State.Closed)
                .Permit(CaseTrigger.Cancel, State.Cancelled)
                .PermitReentry(CaseTrigger.Forward)
                .OnEntryFrom(CaseTriggerParameters.OpenCaseTrigger, OnCaseOpened)
                .OnEntryFrom(CaseTriggerParameters.ForwardCaseTrigger, OnCaseForwarded);
            
            //Close Case
            _caseStateMachine.Configure(State.Closed)
                .OnEntryFrom(CaseTriggerParameters.CloseCaseTrigger, OnCaseClosed);

            _caseStateMachine.Configure(State.Cancelled)
                .OnEntryFrom(CaseTriggerParameters.CancelCaseTrigger, OnCaseCancelled);

        }


        public void OpenCase(WorkflowModel model, WorkItemModel item)
        {
            _caseStateMachine.Fire(CaseTriggerParameters.OpenCaseTrigger, model, item);
        }


        public void ForwardCase(WorkItemModel model, WorkItemNote note)
        {
            _caseStateMachine.Fire(CaseTriggerParameters.ForwardCaseTrigger, model, note);
        }

        public void CloseCase(string id, int state)
        {
            _caseStateMachine.Fire(CaseTriggerParameters.CloseCaseTrigger, id, state);
        }

        public void CancelCase(string id, int state)
        {
            _caseStateMachine.Fire(CaseTriggerParameters.CancelCaseTrigger, id, state);
        }

        private void OnCaseOpened(WorkflowModel model, WorkItemModel item)
        {
            _caseWorkflowService.OpenCase(model, item);
        }

        private void OnCaseForwarded(WorkItemModel model, WorkItemNote note)
        {
           _caseWorkflowService.ForwardCase(model, note);
        }

        private void OnCaseClosed(string id, int state)
        {
            _caseWorkflowService.CloseCase(id, state);
        }

        private void OnCaseCancelled(string id, int state)
        {
            _caseWorkflowService.CancelCase(id, state);
        }
        
    }
}