﻿using intaps.cis.domain.Workflows.DamsWorkflow;

namespace intaps.cis.domain.Workflows.CaseWorkflow
{
    public class CaseModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string CaseType { get; set; }
        public long CaseNumber { get; set; }
        public string Workflow { get; set; }
        public string Description { get; set; }
        public string Date { get; set; }
        public DocumentModel Document { get; set; }
        public string CreatedBy { get; set; }
        public int Status { get; set; }
        public string ForwardTo { get; set; }
    }

    public class CaseDetail
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string CaseType { get; set; }
        public long CaseNumber { get; set; }
        public string Date { get; set; }
        public string Workflow { get; set; }
        public string Description { get; set; }
        public string Document { get; set; }
        public string CreatedBy { get; set; }
        public string ForwardTo { get; set; }
    }
}