﻿using System;
using System.Collections.Generic;
using System.Linq;
using intaps.cis.data;
using intaps.cis.data.Entities;
using intaps.cis.domain.Archive;
using intaps.cis.domain.Extensions;
using intaps.cis.domain.Infrastructure;
using intaps.cis.domain.Workflows.DamsWorkflow;
using intaps.cis.domain.Workflows.Models;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
namespace intaps.cis.domain.Workflows.CaseWorkflow
{
    public interface ICaseWorkflowService
    {
        void SetSession(UserSession session);
        void OpenCase(WorkflowModel workflowCase, WorkItemModel item);
        void ForwardCase(WorkItemModel workItem, WorkItemNote note);
        void CloseCase(string id, int state);
        void CancelCase(string id, int state);
        CaseModel GetCase(string id);
        IList<CaseModel> GetCasebyDoc(string docId);
        long GetCaseNumber();
        void SetContext(CisContext cisContext);
    }

    public class CaseWorkflowService : ICaseWorkflowService
    {
        private IWorkflowService _workflowService;
        private CisContext _cisContext;
        private IArchiveService _archiveService;

        public CaseWorkflowService(IWorkflowService workflowService, IArchiveService archiveService)
        {
            _workflowService = workflowService;
            _archiveService = archiveService;
        }

        public void SetSession(UserSession session)
        {
            _workflowService.SetSession(session);
        }


        public void SetContext(CisContext context)
        {
            _cisContext = context;
            
            _archiveService.SetContext(context);
            _workflowService.SetContext(context);
        }

        public void OpenCase(WorkflowModel workflowCase, WorkItemModel item)
        {
            _workflowService.CreateWorkflow(workflowCase);
            _workflowService.UpdateWorkflow(workflowCase.Id, (int)State.Opened);
            _workflowService.AddWorkItem(item);
        }

        public void ForwardCase(WorkItemModel workItem, WorkItemNote note)
        {
            _workflowService.UpdateWorkItem(workItem);
            _workflowService.AddWorkItemNote(note);
        }

        public void CloseCase(string id, int state)
        {
            _workflowService.UpdateWorkflow(id, state);
//            AddEntities(id);
        }

        public void CancelCase(string id, int state)
        {
            _workflowService.UpdateWorkflow(id, state);
        }

        public IList<CaseModel> GetCasebyDoc(string docId)
        {
           // var caseDocs = _cisContext.CaseDocument.Where(cd => cd.DocumentId == docId.ToGuid());
            IList<CaseModel> caseModels = new List<CaseModel>();
            var sql = $@"select * from wf.work_item where seq_no=1 and workflow 
in (Select id from wf.workflow where type=2) and data->>'Document'='{docId}'
";
            Console.WriteLine($"Executing getCaseByDoc\nDocID:{docId}\nsql:{sql}");
            var workItems = _cisContext.WorkItem.FromSql(sql);

            //var workflow = _cisContext.Workflow.Where(wr => wr.Type == 2);
            //foreach (var wr in workflow)
            //{
            //var workitem = _workflowService.GetWorkItems(wr.Id.ToString())[0];
            foreach(var workitem in workItems)
            {
                var caseData = JsonConvert.DeserializeObject<CaseDetail>(workitem.Data);
                if (caseData.Document.Equals(docId))
                {
                    var caseModel = new CaseModel()
                    {
                        Id = caseData.Id,
                        Workflow = caseData.Workflow,
                        Title = caseData.Title,
                        Description = caseData.Description,
                        CaseNumber = caseData.CaseNumber,
                        CaseType = caseData.CaseType,
                        Date = EtGrDate.ToEthDate(caseData.Date),
                        Document = string.IsNullOrEmpty(caseData.Document) ? null : _archiveService.GetDocument(caseData.Document),
                        CreatedBy = caseData.CreatedBy
                    };
                    caseModels.Add(caseModel);
                }
            }
  
            return caseModels;
        }

        public long GetCaseNumber()
        {
            var currentCaseNo = _cisContext.CaseNumber.First(cn => cn.Id == 1);

            currentCaseNo.CurrentCase += 1;

            _cisContext.CaseNumber.Update(currentCaseNo);

            _cisContext.SaveChanges();

            return currentCaseNo.CurrentCase;
        }

        public CaseModel GetCase(string id)
        {
            var caseWorkItem = _workflowService.GetWorkItems(id)[0];

            var caseDetail = JsonConvert.DeserializeObject<CaseDetail>(caseWorkItem.Data);
            
            var caseModel = new CaseModel()
            {
                Id = caseDetail.Id,
                Workflow = caseDetail.Workflow,
                Title = caseDetail.Title,
                Description = caseDetail.Description,
                CaseNumber = caseDetail.CaseNumber,
                CaseType = caseDetail.CaseType,
                Date = EtGrDate.ToEthDate(caseDetail.Date),
                Document = string.IsNullOrEmpty(caseDetail.Document) ? null : _archiveService.GetDocument(caseDetail.Document),
                CreatedBy = caseDetail.CreatedBy
                };
            
            return caseModel;
        }

        private CaseDetail GetCaseDetail(string id)
        {
            var caseModel = _cisContext.Case.First(c => c.Id == id.ToGuid());
            
            var caseDetail = new CaseDetail()
            {
                Id = caseModel.Id.ToString(),
                Workflow = caseModel.Workflow.ToString(),
                Title = caseModel.Title,
                Description = caseModel.Description,
                CaseNumber = caseModel.CaseNumber,
                CaseType = _cisContext.CaseType.First(ct => ct.Id == caseModel.CaseType).Name,
                Date = EtGrDate.ToEthDate(new DateTime(caseModel.Date).ToShortDateString()),
            };

            return caseDetail;
        }

        private void AddEntities(string id)
        {
            var workItems = _workflowService.GetWorkItems(id);

            foreach (var item in workItems)
            {
                var caseDetail = JsonConvert.DeserializeObject<CaseDetail>(item.Data);

                var caseModel = new Case()
                {
                    Id = caseDetail.Id.ToGuid(),
                    Workflow = caseDetail.Workflow.ToGuid(),
                    Title = caseDetail.Title,
                    Description = caseDetail.Description,
                    CreatedBy = caseDetail.CreatedBy,
                    CaseNumber = caseDetail.CaseNumber,
                    Date = DateTime.Parse(caseDetail.Date).Ticks,
                    CaseType = _cisContext.CaseType.First(c => c.Name.Equals(caseDetail.CaseType)).Id
                };

                if (!string.IsNullOrEmpty(caseDetail.Document))
                {
                    var caseDoc = new CaseDocument()
                    {
                        Id = Guid.NewGuid(),
                        CaseId = caseModel.Id,
                        DocumentId = caseDetail.Document.ToGuid()
                    };
                    
                    _cisContext.CaseDocument.Add(caseDoc);
                }
 
                _cisContext.Case.Add(caseModel);

                _cisContext.SaveChanges();
            }
        }
    }
}