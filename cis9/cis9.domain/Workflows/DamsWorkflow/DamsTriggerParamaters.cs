﻿using System.Collections.Generic;
using intaps.cis.domain.Workflows.Models;
using Stateless;

namespace intaps.cis.domain.Workflows.DamsWorkflow
{
    public static class DamsTriggerParamaters
    {
        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<WorkItemModel> AddDocumentTrigger;
        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<string> RemoveDocumentTrigger;   
        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<WorkItemModel> UpdateDocumentTrigger;


        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<WorkflowModel, WorkItemModel>
            FileUpdateAddDocTrigger;

        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<WorkflowModel, WorkItemModel>
            FileUpdateDocTrigger;
        
        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<WorkItemModel> AddImageTrigger;
        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<string> RemoveImageTrigger;
        
        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<WorkItemModel> UpdateImageTrigger;
        
        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<WorkflowModel, WorkItemModel> AddFileTrigger;
        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<WorkflowModel, WorkItemModel> RemoveFileTrigger;
        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<WorkflowModel, WorkItemModel> UpdateFileTrigger;
        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<WorkflowModel, WorkItemModel> UpdateFileInfoTrigger;
        
        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<string, IList<WorkItemModel>> RequestDocApprovalTrigger;
        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<string, IList<WorkItemModel>> RequestFileApprovalTrigger;
        
        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<string, IList<WorkItemModel>, WorkItemNote> RejectDocApprovalTrigger;
        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<string, IList<WorkItemModel>, WorkItemNote> RejectFileApprovalTrigger;

        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<string, IList<WorkItemModel>,
            WorkItemNote> RejectFileTrigger;
        
        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<WorkflowModel> ApproveRequestTrigger;
      
        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<string, IList<WorkItemModel>> EditFileTrigger;
        public static StateMachine<State, DamsTriggers>.TriggerWithParameters<string> CancelTrigger;

        public static void ConifugreTriggers(StateMachine<State, DamsTriggers> damStateMachine)
        {
            AddDocumentTrigger = damStateMachine.SetTriggerParameters<WorkItemModel>(DamsTriggers.AddDocument);
            RemoveDocumentTrigger = damStateMachine.SetTriggerParameters<string>(DamsTriggers.RemoveDocument);
            UpdateDocumentTrigger = damStateMachine.SetTriggerParameters<WorkItemModel>(DamsTriggers.UpdateDocument);


            FileUpdateAddDocTrigger =
                damStateMachine.SetTriggerParameters<WorkflowModel, WorkItemModel>(DamsTriggers.FileUpdateAddDoc);
            
            FileUpdateDocTrigger = damStateMachine.SetTriggerParameters<WorkflowModel, WorkItemModel>(DamsTriggers.FileUpdateDoc);

            AddImageTrigger = damStateMachine.SetTriggerParameters<WorkItemModel>(DamsTriggers.AddImage);
            RemoveImageTrigger = damStateMachine.SetTriggerParameters<string>(DamsTriggers.RemoveImage);
            UpdateImageTrigger = damStateMachine.SetTriggerParameters<WorkItemModel>(DamsTriggers.UpdateImage);
     
            AddFileTrigger = damStateMachine.SetTriggerParameters<WorkflowModel, WorkItemModel>(DamsTriggers.AddFile);
            RemoveFileTrigger = damStateMachine.SetTriggerParameters<WorkflowModel, WorkItemModel>(DamsTriggers.RemoveFile);
            UpdateFileTrigger = damStateMachine.SetTriggerParameters<WorkflowModel, WorkItemModel>(DamsTriggers.UpdateFile);
            UpdateFileInfoTrigger = damStateMachine.SetTriggerParameters<WorkflowModel, WorkItemModel>(DamsTriggers.UpdateFileInfo);

            RequestDocApprovalTrigger = damStateMachine.SetTriggerParameters<string, IList<WorkItemModel>>(DamsTriggers.RequestDocAprroval);
            RequestFileApprovalTrigger = damStateMachine.SetTriggerParameters<string, IList<WorkItemModel>>(DamsTriggers.RequestFileApproval);

            RejectDocApprovalTrigger =
                damStateMachine.SetTriggerParameters<string, IList<WorkItemModel>, WorkItemNote>(DamsTriggers.RejectDocApproval);
            RejectFileApprovalTrigger =
                damStateMachine.SetTriggerParameters<string, IList<WorkItemModel>, WorkItemNote>(DamsTriggers.RejectFileApproval);

            RejectFileTrigger =
                damStateMachine.SetTriggerParameters<string, IList<WorkItemModel>, WorkItemNote>(
                    DamsTriggers.RejectFile);

            ApproveRequestTrigger = damStateMachine.SetTriggerParameters<WorkflowModel>(DamsTriggers.ApproveRequest);
            EditFileTrigger = damStateMachine.SetTriggerParameters<string, IList<WorkItemModel>>(DamsTriggers.EditFile);
            CancelTrigger = damStateMachine.SetTriggerParameters<string>(DamsTriggers.Cancel);
        }
        
        
        
    }
}