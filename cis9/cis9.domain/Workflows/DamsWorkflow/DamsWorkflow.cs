﻿using System;
using System.Collections.Generic;
using intaps.cis.domain.Infrastructure;
using intaps.cis.domain.Workflows.Models;
using Stateless;

namespace intaps.cis.domain.Workflows.DamsWorkflow
{
    public class DamsWorkflow
    {
        private StateMachine<State, DamsTriggers> _damsStateMachine;

        private IDamsWorkflowService _damsWorkflowService;
        
        
        private State _state;

        public DamsWorkflow(IDamsWorkflowService service, int currentState, UserSession session, string applicationFor)
        {
            if(applicationFor.Equals("wsis"))
                INTAPS.ClientServer.ApplicationServer.RenewSession(session.WSISSessionId);
            _state = (State)Enum.Parse(typeof(State),currentState.ToString());
            _damsWorkflowService = service;
            
            _damsWorkflowService.SetSession(session);
            
            _damsStateMachine = new StateMachine<State, DamsTriggers>(_state);

            ConfigureStateMachine();
            
           
        }

        private void ConfigureStateMachine()
        {
            DamsTriggerParamaters.ConifugreTriggers(_damsStateMachine);
            
            //Happy Path
            
            //Initial State
            _damsStateMachine.Configure(State.Initial)
                .Permit(DamsTriggers.FileUpdateAddDoc, State.DigitizingFile)
                .Permit(DamsTriggers.FileUpdateDoc, State.DigitizingDocument)
                .Permit(DamsTriggers.AddFile, State.DigitizingFile)
                .Permit(DamsTriggers.UpdateFile, State.DigitizingFile)
                .Permit(DamsTriggers.RemoveFile, State.FileDeleteRequested)
                .Permit(DamsTriggers.EditFile, State.DigitizingFile)
                .Permit(DamsTriggers.UpdateFileInfo, State.FileApprovalRequested)
                .OnEntryFrom(DamsTriggerParamaters.RejectFileTrigger, OnFileRejected);
            

            
            //Digitizing File
            _damsStateMachine.Configure(State.DigitizingFile)
                .OnEntryFrom(DamsTriggerParamaters.AddDocumentTrigger, OnDocumentAdded)
                .OnEntryFrom(DamsTriggerParamaters.UpdateDocumentTrigger, OnDocumentUpdated)
                .OnEntryFrom(DamsTriggerParamaters.UpdateImageTrigger, OnImageUpdated)
                .OnEntryFrom(DamsTriggerParamaters.RemoveDocumentTrigger, OnDocumentRemoved)
                .OnEntryFrom(DamsTriggerParamaters.AddImageTrigger, OnImageAdded)
                .OnEntryFrom(DamsTriggerParamaters.RemoveImageTrigger, OnImageRemoved)
                .OnEntryFrom(DamsTriggerParamaters.AddFileTrigger, OnFileAdded)
                .OnEntryFrom(DamsTriggerParamaters.UpdateFileTrigger, OnFileUpdated)
                .OnEntryFrom(DamsTriggerParamaters.RejectFileApprovalTrigger, OnFileRequestRejected)
                .OnEntryFrom(DamsTriggerParamaters.EditFileTrigger, OnFileEdited)
                .OnEntryFrom(DamsTriggerParamaters.FileUpdateAddDocTrigger, OnFileUpdateDoc)
                .PermitReentry(DamsTriggers.AddImage)
                .PermitReentry(DamsTriggers.RemoveImage)
                .PermitReentry(DamsTriggers.UpdateImage)
                .PermitReentry(DamsTriggers.AddDocument)
                .PermitReentry(DamsTriggers.UpdateDocument)
                .PermitReentry(DamsTriggers.RemoveDocument)
                .PermitReentry(DamsTriggers.RejectFileApproval)
                .Permit(DamsTriggers.RequestFileApproval, State.FileApprovalRequested)
                .Permit(DamsTriggers.RejectFile, State.Initial);
            
            //Digitizing Document
            _damsStateMachine.Configure(State.DigitizingDocument)
                .OnEntryFrom(DamsTriggerParamaters.FileUpdateDocTrigger, OnFileDocUpdated)
                .OnEntryFrom(DamsTriggerParamaters.RejectDocApprovalTrigger, OnDocRequestRejected)
                .PermitReentry(DamsTriggers.AddImage)
                .Permit(DamsTriggers.RequestDocAprroval, State.DocumentApprovalRequested);
             
                
            
            //File Approval

            _damsStateMachine.Configure(State.FileApprovalRequested)
                .OnEntryFrom(DamsTriggerParamaters.UpdateFileInfoTrigger, OnFileInfoUpdated)
                .OnEntryFrom(DamsTriggerParamaters.RequestFileApprovalTrigger, OnFileApprovalRequested)
                .Permit(DamsTriggers.ApproveRequest, State.Closed)
                .Permit(DamsTriggers.RejectFileApproval, State.DigitizingFile)
                .Permit(DamsTriggers.Cancel, State.Cancelled);
            
            
            //Document Approval
            _damsStateMachine.Configure(State.DocumentApprovalRequested)
                .OnEntryFrom(DamsTriggerParamaters.RequestDocApprovalTrigger, OnDocApprovalRequest)
                .Permit(DamsTriggers.RejectDocApproval, State.DigitizingDocument)
                .Permit(DamsTriggers.ApproveRequest, State.Closed)
                .Permit(DamsTriggers.Cancel, State.Cancelled);
             
            //File Deletion Requested
            _damsStateMachine.Configure(State.FileDeleteRequested)
                .OnEntryFrom(DamsTriggerParamaters.RemoveFileTrigger, OnFileRemoved)
                .Permit(DamsTriggers.ApproveRequest, State.Closed)
                .Permit(DamsTriggers.Cancel, State.Cancelled);
                
                
            _damsStateMachine.Configure(State.Closed)
                .OnEntryFrom(DamsTriggerParamaters.ApproveRequestTrigger ,OnClose);


            _damsStateMachine.Configure(State.Cancelled)
                .OnEntryFrom(DamsTriggerParamaters.CancelTrigger, OnCancelled);


            //_damsStateMachine.OnUnhandledTrigger(OnUnHandledTrigger);
        }

    
        public void AddDocument(WorkItemModel doc)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.AddDocumentTrigger, doc);
        }
        
        public void UpdateDocument(WorkItemModel doc)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.UpdateDocumentTrigger, doc);
        }
        
        public void RemoveDocument(string workItem)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.RemoveDocumentTrigger, workItem);
        }
        

        public void AddImage(WorkItemModel img)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.AddImageTrigger, img);
        }
        
        public void RemoveImage(string img)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.RemoveImageTrigger, img);
        }

        public void UpdateImage(WorkItemModel img)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.UpdateImageTrigger, img);
        }
        public void AddFile(WorkflowModel model, WorkItemModel item)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.AddFileTrigger, model, item);
        }
        
        public void UpdateFile(WorkflowModel model, WorkItemModel item)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.UpdateFileTrigger, model, item);
        }

        public void UpdateFileInfo(WorkflowModel model, WorkItemModel item)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.UpdateFileInfoTrigger, model, item);
        }

        public void FileUpdateAddDoc(WorkflowModel model, WorkItemModel item)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.FileUpdateAddDocTrigger, model, item);
        }

        public void FileUpdateDoc(WorkflowModel model, WorkItemModel item)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.FileUpdateDocTrigger, model, item);
        }

        public void EditFile(string workflowId, IList<WorkItemModel> item)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.EditFileTrigger, workflowId, item);
        }
        
        public void RemoveFile(WorkflowModel model, WorkItemModel item)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.RemoveFileTrigger, model, item);
        }

        public void RejectFile(string workflow, IList<WorkItemModel> workItems, WorkItemNote note)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.RejectFileTrigger, workflow, workItems, note);
        }

        
        public void SendFileApproveRequest(string workflowId, IList<WorkItemModel> workItems)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.RequestFileApprovalTrigger, workflowId, workItems);
        }

        public void SendDocApprovalRequest(string workflowId, IList<WorkItemModel> workitems)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.RequestDocApprovalTrigger, workflowId, workitems);
        }
        
        public void RejectFileApproveRequest(string workflowId, IList<WorkItemModel> workItems,WorkItemNote note)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.RejectFileApprovalTrigger, workflowId, workItems, note);
        }

        public void RejectDocRequest(string workflowId, IList<WorkItemModel> workItems, WorkItemNote note)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.RejectDocApprovalTrigger, workflowId, workItems, note);
        }
        
        public void ApproveRequest(WorkflowModel model)
        {
           
            _damsStateMachine.Fire(DamsTriggerParamaters.ApproveRequestTrigger, model);
        }

      
        public void Cancel(string id)
        {
            _damsStateMachine.Fire(DamsTriggerParamaters.CancelTrigger, id);
        }

        private void OnFileRejected(string workflowId, IList<WorkItemModel> workItems, WorkItemNote note)
        {
    
            _damsWorkflowService.UpdateWorkflow(workflowId, (int)State.Initial);
            _damsWorkflowService.AddWorkItemNote(note);
            
            foreach (var workItem in workItems)
            {
                _damsWorkflowService.UpdateWorkItem(workItem);
            }
    
        }
        
        private void OnFileUpdated(WorkflowModel model, WorkItemModel file)
        {
            _damsWorkflowService.CreateWorkflow(model);
            _damsWorkflowService.AddWorkItem(file);
            
        }
        
        private void OnFileInfoUpdated(WorkflowModel model, WorkItemModel item)
        {
            _damsWorkflowService.CreateWorkflow(model);
            _damsWorkflowService.AddWorkItem(item);
            _damsWorkflowService.UpdateWorkflow(model.Id, (int)State.FileApprovalRequested);
        }

        private void OnFileUpdateDoc(WorkflowModel model, WorkItemModel item)
        {
            _damsWorkflowService.CreateWorkflow(model);
            _damsWorkflowService.AddWorkItem(item);
            _damsWorkflowService.UpdateWorkflow(model.Id, (int)State.DigitizingFile);
        }

        private void OnFileDocUpdated(WorkflowModel model, WorkItemModel item)
        {
            _damsWorkflowService.CreateWorkflow(model);
            _damsWorkflowService.AddWorkItem(item);
            _damsWorkflowService.UpdateWorkflow(model.Id, (int)State.DigitizingDocument);
        }
        
        private void OnFileEdited(string model, IList<WorkItemModel> workItems)
        {
            _damsWorkflowService.UpdateWorkflow(model, (int)State.DigitizingFile);
            foreach (var workItem in workItems)
            {
                _damsWorkflowService.UpdateWorkItem(workItem);
            }
        }


        private void OnFileAdded(WorkflowModel model, WorkItemModel file)
        {
            _damsWorkflowService.CreateWorkflow(model);
            _damsWorkflowService.AddWorkItem(file);

        }

        private void OnFileApprovalRequested(string workflowId, IList<WorkItemModel> workItems)
        {
            _damsWorkflowService.UpdateWorkflow(workflowId, (int)State.FileApprovalRequested);
            
            foreach (var workItem in workItems)
            {
                _damsWorkflowService.UpdateWorkItem(workItem);
            }
            
        }

        private void OnDocApprovalRequest(string workflowId, IList<WorkItemModel> workItems)
        {
            _damsWorkflowService.UpdateWorkflow(workflowId, (int)State.DocumentApprovalRequested);
            
            foreach (var workItem in workItems)
            {
                _damsWorkflowService.UpdateWorkItem(workItem);
            }
        }
        
        
        private void OnDocumentUpdated(WorkItemModel item)
        {
            _damsWorkflowService.UpdateWorkItem(item);
        }

        private void OnImageUpdated(WorkItemModel item)
        {
            _damsWorkflowService.UpdateWorkItem(item);
        }
        void OnDocumentAdded(WorkItemModel item)
        {
            _damsWorkflowService.AddWorkItem(item);
  
        }
        private void OnDocumentRemoved(string id)
        {
            _damsWorkflowService.RemoveWorkItem(id);
        }
        
        private void OnFileRemoved(WorkflowModel model, WorkItemModel item)
        {
            _damsWorkflowService.CreateWorkflow(model);
            _damsWorkflowService.AddWorkItem(item);
            _damsWorkflowService.UpdateWorkflow(model.Id, (int)State.FileApprovalRequested);
        }
        
        void OnImageAdded(WorkItemModel model)
        {
            _damsWorkflowService.AddWorkItem(model);
        }
        
        private void OnImageRemoved(string id)
        {
            _damsWorkflowService.RemoveWorkItem(id);
        }

        void OnFileRequestRejected(string workflowId, IList<WorkItemModel> workItems,  WorkItemNote note)
        {
            _damsWorkflowService.UpdateWorkflow(workflowId, (int)State.DigitizingFile);
            _damsWorkflowService.AddWorkItemNote(note);
            
            foreach (var workItem in workItems)
            {
                _damsWorkflowService.UpdateWorkItem(workItem);
            }
        }
        
        private void OnDocRequestRejected(string workflowId, IList<WorkItemModel> workItems, WorkItemNote note)
        {
            _damsWorkflowService.UpdateWorkflow(workflowId, (int)State.DigitizingDocument);
            _damsWorkflowService.AddWorkItemNote(note);
            
            foreach (var workItem in workItems)
            {
                _damsWorkflowService.UpdateWorkItem(workItem);
            }
        }

        void OnClose(WorkflowModel model)
        {
            _damsWorkflowService.Aprrove(model);
        }

        void OnCancelled(string workflowId)
        {
            _damsWorkflowService.UpdateWorkflow(workflowId, (int)State.Cancelled);
        }
        
        void OnUnHandledTrigger(State state, DamsTriggers trigger)
        {
            Console.WriteLine($"You cannot enter State {state} with trigger {trigger}");
        }

      
    }
}