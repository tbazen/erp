﻿using System.Collections.Generic;
using intaps.cis.data.Entities;
using intaps.cis.domain.Workflows.Models;

namespace intaps.cis.domain.Workflows.DamsWorkflow
{
    public class FileWorkflow : WorkflowModel
    {
        public FileModel File { get; set; }
    }

    public class DamsOptions
    {
        public string ImagePath { get; set; }
    }


    public class FileModel
    {
        public string Id { get; set; }
        public string WorkItem {get; set;}
        public string Shelf { get; set; }
        public string Location { get; set; }
        public string Folder { get; set; }
        public string Remark { get; set; }
        public string FileOwner { get; set; }
        public string FileCode { get; set; }
        public string MobileNumber { get; set; }   
        public string Upin { get; set; }
        public IList<DocumentModel> Documents = new List<DocumentModel>();
    }

    public class FileStat
    {
        public int NumDocuments { get; set; }
        public int NumSheets { get; set; }
    }

    public class DocumentModel
    {
        public string Id { get; set; }
        public string WorkItem {get; set;}
        public int Sheets { get; set; }
        public string ReferenceNo { get; set; }
        public string FileId { get; set; }
        public DocumentType Type { get; set; }
        public long TimeStamp { get; set; }
        public string Date { get; set; }
        public IList<ImageModel> Images = new List<ImageModel>();
    }

    public class ImageModel
    {
        public string Id { get; set; }
        public string WorkItem {get; set;}
        public string Description { get; set; }
        public string DocumentId { get; set; }
        public string Page { get; set; }
        public string Data { get; set; }
        public string Mime { get; set; }
    }

    
}