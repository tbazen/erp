﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using intaps.cis.data;
using intaps.cis.data.Entities;
using intaps.cis.domain.Extensions;
using intaps.cis.domain.Infrastructure;
using intaps.cis.domain.Workflows.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Options;

namespace intaps.cis.domain.Workflows.DamsWorkflow
{
    public interface IDamsWorkflowService
    {
        void SetSession(UserSession session);
        void SetContext(CisContext context);
        void CreateWorkflow(WorkflowModel model);
        void UpdateWorkflow(string workflow, int state);
        void Aprrove(WorkflowModel model);
        FileModel GetFileWorkflow(string id);
        DocumentModel GetDocumentWorklfow(string id);
        int GetWorkflowState(string workItem);
        IList<ImageModel> GetDocumentImages(string workflowId, string docId);

        void AddWorkItem(WorkItemModel model);
        void UpdateWorkItem(WorkItemModel model);
        void RemoveWorkItem(string id);

        void AddWorkItemNote(WorkItemNote note);
    }

    public class DamsWorkflowService : IDamsWorkflowService
    {
        private CisContext _cisContext;
        private UserSession _userSession;
        private IWorkflowService _workflowService;
        private string fileLocation;
        private Regex mimeTypeRegx = new Regex("(.+)/(.+)");


        public DamsWorkflowService(IWorkflowService service, IOptions<DamsOptions> imagePath)
        {
            _workflowService = service;
            fileLocation = imagePath.Value.ImagePath;
        }


        public void SetSession(UserSession session)
        {
            _workflowService.SetSession(session);
        }

        public void SetContext(CisContext cisContext)
        {
            _cisContext = cisContext;
            _workflowService.SetContext(cisContext);
        }


        public void CreateWorkflow(WorkflowModel model)
        {
            _workflowService.CreateWorkflow(model);
        }

        public void UpdateWorkflow(string workflow, int state)
        {
            _workflowService.UpdateWorkflow(workflow, state);
        }

        public void AddWorkItem(WorkItemModel model)
        {
            _workflowService.AddWorkItem(model);
        }

        public void UpdateWorkItem(WorkItemModel model)
        {
            _workflowService.UpdateWorkItem(model);
        }

        public void RemoveWorkItem(string id)
        {
            _workflowService.RemoveWorkItem(id.ToGuid());
        }

        public void AddWorkItemNote(WorkItemNote note)
        {
            _workflowService.AddWorkItemNote(note);
        }

        public void Aprrove(WorkflowModel model)
        {
            AddEntities(Guid.Parse(model.Id));

            _workflowService.UpdateWorkflow(model.Id, (int) State.Closed);
        }


        public FileModel GetFileWorkflow(string id)
        {
            var workItems = _workflowService.GetWorkItems(id).OrderBy(wi => wi.SeqNo);
            FileModel fileModel = null;
            foreach (var workItem in workItems)
            {
                if (string.IsNullOrEmpty(workItem.DataType))
                    continue;
                var type = Type.GetType($"{workItem.DataType}, intaps.cis.domain");

                if (type.Name.Equals(typeof(FileModel).Name))
                {
                    var file = JsonConvert.DeserializeObject<FileModel>(workItem.Data);
                    fileModel = new FileModel
                    {
                        Id = file.Id,
                        Folder = file.Folder,
                        Shelf = file.Shelf,
                        Location = file.Location,
                        Remark = file.Remark,
                        FileCode = file.FileCode,
                        FileOwner = file.FileOwner,
                        MobileNumber = file.MobileNumber,
                        Upin = file.Upin,
                        WorkItem = file.WorkItem
                    };
                }

                else if (type.Name.Equals(typeof(DocumentModel).Name))
                {
                    var document = JsonConvert.DeserializeObject<DocumentModel>(workItem.Data);
                    if (fileModel.Documents.Any(doc => doc.Id == document.Id))
                    {
                        var docModel = new DocumentModel
                        {
                            Id = document.Id,
                            FileId = document.FileId,
                            ReferenceNo = document.ReferenceNo,
                            Sheets = document.Sheets,
                            Type = document.Type,
                            TimeStamp = document.TimeStamp,
                            Date = EtGrDate.ToEthDate(document.Date),
                            WorkItem = document.WorkItem
                        };

                        fileModel.Documents.Remove(document);
                        fileModel.Documents.Add(docModel);
                    }

                    else
                    {
                        fileModel.Documents.Add(new DocumentModel
                        {
                            Id = document.Id,
                            FileId = document.FileId,
                            ReferenceNo = document.ReferenceNo,
                            Sheets = document.Sheets,
                            Type = document.Type,
                            TimeStamp = document.TimeStamp,
                            Date = EtGrDate.ToEthDate(document.Date),
                            WorkItem = document.WorkItem
                        });
                    }
                }
            }

            return fileModel;
        }

        public DocumentModel GetDocumentWorklfow(string id)
        {
            var workItems = _cisContext.WorkItem.Where(wi => wi.Workflow.Equals(id.ToGuid()));

            DocumentModel document = null;

            foreach (var workItem in workItems)
            {
                if (string.IsNullOrEmpty(workItem.DataType))
                    continue;
                var type = Type.GetType($"{workItem.DataType}, intaps.cis.domain");

                if (type.Name.Equals(typeof(DocumentModel).Name))
                {
                    document = JsonConvert.DeserializeObject<DocumentModel>(workItem.Data);

                    document.Date = EtGrDate.ToEthDate(document.Date);

                    var file = _cisContext.ArchiveFile.First(f => f.Id.Equals(document.FileId.ToGuid()));
                }

                else if (type.Name.Equals(typeof(ImageModel).Name))
                {
                    var image = JsonConvert.DeserializeObject<ImageModel>(workItem.Data);
                    if (!(document.Id == image.DocumentId)) continue;
                    var imgModel = new ImageModel
                    {
                        Id = image.Id,
                        Description = image.Description,
                        DocumentId = image.DocumentId,
                        Mime = image.Mime,
                        Page = image.Page,
                        WorkItem = image.WorkItem
                    };

                    imgModel.Data = ReadImage(image.Id, image.Mime, out var data) ? data : image.Data;

                    if (document.Images.Any(img => img.Id == image.Id))
                    {
                        document.Images.Remove(image);
                        document.Images.Add(imgModel);
                    }
                    else
                    {
                        document.Images.Add(imgModel);
                    }
                }
            }

            return document;
        }

        public IList<ImageModel> GetDocumentImages(string workflowId, string docId)
        {
            var workItems = _cisContext.WorkItem.Where(wi =>
                wi.Workflow.Equals(workflowId.ToGuid()) &&
                wi.DataType.Equals("intaps.cis.domain.Workflows.DamsWorkflow.ImageModel"));

            var imageModels = new List<ImageModel>();

            foreach (var workItem in workItems)
            {
                var image = JsonConvert.DeserializeObject<ImageModel>(workItem.Data);

                if (!image.DocumentId.Equals(docId)) continue;


                image.Data = ReadImage(image.Id, image.Mime, out var data) ? data : image.Data;

                imageModels.Add(image);
            }

            return imageModels;
        }

        public IList<WorkflowModel> GetWorkflowModel(UserSession session)
        {
            return _workflowService.GetWorkflow(session);
        }

        public int GetWorkflowState(string workItem)
        {
            return _workflowService.GetWorkflowState(workItem);
        }


        private void AddEntities(Guid workflowId)
        {
            var workItems = _cisContext.WorkItem.Where(wi => wi.Workflow == workflowId).OrderBy(wi => wi.SeqNo);
            try
            {
                foreach (var workItem in workItems)
                {
                    if (!string.IsNullOrEmpty(workItem.DataType))
                    {
                        var type = Type.GetType($"{workItem.DataType}, intaps.cis.domain");

                        if (type.Name.Equals(typeof(FileModel).Name))
                        {
                            var file = JsonConvert.DeserializeObject<FileModel>(workItem.Data);
                            HandleFile(workItem.Type, file);
                        }

                        else if (type.Name.Equals(typeof(DocumentModel).Name))
                        {
                            var document = JsonConvert.DeserializeObject<DocumentModel>(workItem.Data);
                            HandleDocument(document, workItem.Type);
                        }
                        else if (type.Name.Equals(typeof(ImageModel).Name))
                        {
                            var image = JsonConvert.DeserializeObject<ImageModel>(workItem.Data);
                            HandleImage(image);
                        }
                    }
                }

                _cisContext.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void HandleFile(int workItemType, FileModel filevm)
        {
            var trigger = (DamsTriggers)Enum.Parse(typeof(DamsTriggers),workItemType.ToString());
            var file = new ArchiveFile
            {
                Id = filevm.Id.ToGuid(),
                FileCode = filevm.FileCode,
                FileOwner = filevm.FileOwner,
                Folder = filevm.Folder,
                Location = filevm.Location,
                Remark = filevm.Remark,
                Shelf = filevm.Shelf,
                WorkItem = filevm.WorkItem.ToGuid(),
                MobileNumber = filevm.MobileNumber,
                Upin = filevm.Upin
            };

            switch (trigger)
            {
                case DamsTriggers.AddFile:

                    _cisContext.ArchiveFile.Add(file);
                    break;


                case DamsTriggers.UpdateFileInfo:
                    if (_cisContext.ArchiveFile.Any(f => f.Id == file.Id))
                    {
                        _cisContext.ArchiveFile.Update(file);
                    }
                    else
                    {
                        var entry = _cisContext.Set<ArchiveFile>().Local.First(af => af.Id == file.Id);
                        entry = file;
                        _cisContext.Entry(entry).State = EntityState.Modified;
                    }

                    break;

                case DamsTriggers.UpdateFile:
                    if (_cisContext.ArchiveFile.Any(f => f.Id == file.Id))
                    {
                        _cisContext.ArchiveFile.Update(file);
                    }
                    else
                    {
                        throw new InvalidOperationException(
                            "File Not Found. It must have been deleted by other user first");
                    }

                    break;
                case DamsTriggers.RemoveFile:
                    if (_cisContext.ArchiveFile.Any(f => f.Id == file.Id))
                    {
                        RemoveFile(filevm.Id);
                    }
                    else
                    {
                        throw new InvalidOperationException(
                            "File Not Found. It must have been deleted by other user first");
                    }

                    break;
            }
        }

        private void RemoveFile(string fileId)
        {
            var documents = _cisContext.Document.Where(doc => doc.FileId == fileId.ToGuid());

            foreach (var document in documents)
            {
                var docImages = _cisContext.ArchiveImage.Where(img => img.DocumentId == document.Id);

                foreach (var image in docImages)
                {
                    _cisContext.ArchiveImage.Remove(image);

                    DeleteImage(image.Id.ToString(), image.Mime);
                }

                _cisContext.Document.Remove(document);
            }

            var file = _cisContext.ArchiveFile.First(af => af.Id == fileId.ToGuid());

            _cisContext.ArchiveFile.Remove(file);
        }

        private void HandleDocument(DocumentModel documentVm, int workItemType)
        {
            var trigger = (DamsTriggers)Enum.Parse(typeof(DamsTriggers),workItemType.ToString());

            DateTime date;
            if(!DateTime.TryParse(documentVm.Date,out date))
            {
                Console.WriteLine("Invalid date found: " + date);
                date = DateTime.Now;
            }
            Guid guid;
            if(!Guid.TryParse( documentVm.Id,out  guid))
            {
                throw new Exception("Invalid document guid: " + documentVm.Id);
            }
            if (!Guid.TryParse(documentVm.FileId, out guid))
            {
                throw new Exception("Invalid document guid: " + documentVm.FileId);
            }
            var document = new Document
            {
                Id = documentVm.Id.ToGuid(),
                FileId = documentVm.FileId.ToGuid(),
                ReferenceNo = documentVm.ReferenceNo.Trim(),
                Sheets = documentVm.Sheets,
                Timestamp = documentVm.TimeStamp,
                Workitem = documentVm.WorkItem.ToGuid(),
                Type = documentVm.Type.Id,
                Date = date.Ticks
            };

            switch (trigger)
            {
                case DamsTriggers.AddDocument:

                    _cisContext.Document.Add(document);
                    break;

                case DamsTriggers.FileUpdateDoc:
                    _cisContext.Document.Update(document);

                    if (documentVm.Images.Any())
                    {
                        foreach (var imageModel in documentVm.Images)
                        {
                            HandleImage(imageModel);
                        }
                    }

                    break;
            }
        }

        private void HandleImage(ImageModel imageVm)
        {
            var image = new ArchiveImage
            {
                Id = imageVm.Id.ToGuid(),
                Description = imageVm.Description,
                DocumentId = imageVm.DocumentId.ToGuid(),
                Mime = imageVm.Mime,
                Workitem = imageVm.WorkItem.ToGuid(),
                Page = int.Parse(imageVm.Page)
            };
            _cisContext.ArchiveImage.Add(image);
            SaveImage(image.Id.ToString(), image.Mime, imageVm.Data);
        }


        private void SaveImage(string fileName, string mime, string encodedStr)
        {
            if (mimeTypeRegx.IsMatch(mime))
            {
                var imgEx = mimeTypeRegx.Match(mime).Groups[2].Value;
                var path = Path.Combine(fileLocation, $"{fileName}.{imgEx}");
                if (File.Exists(path))
                    return;
                var byteArr = Convert.FromBase64String(encodedStr);

                BinaryWriter binaryWriter = null;

                try
                {
                    binaryWriter = new BinaryWriter(File.OpenWrite(path));

                    binaryWriter.Write(byteArr);
                    binaryWriter.Flush();
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    binaryWriter?.Close();
                }
            }
            else
            {
                throw new InvalidOperationException("Mime type malformed");
            }
        }

        private void DeleteImage(string fileName, string mime)
        {
            if (mimeTypeRegx.IsMatch(mime))
            {
                var imgEx = mimeTypeRegx.Match(mime).Groups[2].Value;
                var path = Path.Combine(fileLocation, $"{fileName}.{imgEx}");

                File.Delete(path);
            }
        }

        private bool ReadImage(string fileName, string mime, out string data)
        {
            if (mimeTypeRegx.IsMatch(mime))
            {
                var imgEx = mimeTypeRegx.Match(mime).Groups[2].Value;
                var path = Path.Combine(fileLocation, $"{fileName}.{imgEx}");


                try
                {
                    var imgBytes = File.ReadAllBytes(path);

                    data = Convert.ToBase64String(imgBytes);
                    return true;
                }
                catch (Exception e)
                {
                    data = "";
                    return false;
                }
            }

            throw new InvalidOperationException("Mime type malformed");
        }
    }
}