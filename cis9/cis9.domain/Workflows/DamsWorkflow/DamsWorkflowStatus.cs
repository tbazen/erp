﻿namespace intaps.cis.domain.Workflows.DamsWorkflow
{
    public enum State
    {
        Initial = 0,
        DigitizingDocument = 1,
        DocumentApprovalRequested = 2,
        
        DigitizingFile = 3,
        FileApprovalRequested = 4,
            
        FileDeleteRequested = 5,
        
        Closed = 6, 
        Cancelled = 7,
        Opened = 8,
        
        FileRequested = 9,
        FileRequestAccepted = 10,
        CheckedOut = 11   
    }

    public enum DamsTriggers
    {
        AddDocument = 0,
        RemoveDocument = 1,
        UpdateDocument = 2,
        
        AddImage = 3,
        RemoveImage = 4,
        UpdateImage = 5,
        
        AddFile = 6,
        RemoveFile = 7,
        UpdateFile = 8,
       
        RequestDocAprroval = 9,
        RequestFileApproval = 10,
        
        RejectDocApproval = 11,
        RejectFileApproval = 12,
    
        ApproveRequest = 13,
        RejectDocument = 14,
        RejectFile = 15,
        Cancel = 16,
        
        EditFile = 17,
        
        UpdateFileInfo = 18,
        
        FileUpdateAddDoc = 19,
        
        FileUpdateDoc = 20
    }
}