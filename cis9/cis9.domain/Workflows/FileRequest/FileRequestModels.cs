﻿using intaps.cis.domain.Workflows.DamsWorkflow;

namespace intaps.cis.domain.Workflows.FileRequest
{
    public class FileRequestModel
    {
        public string Id { get; set; }
        public string FileId { get; set; }
        public string WorkflowId { get; set; }
        public string UserName { get; set; }
        public string Description { get; set; }
    }

    public class FileRequestViewModel : FileRequestModel
    {
        public FileModel FileModel { get; set;}
    }
}