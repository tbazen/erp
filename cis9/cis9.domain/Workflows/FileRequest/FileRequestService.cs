﻿using System;
using System.Linq;
using intaps.cis.data;
using intaps.cis.data.Entities;
using intaps.cis.domain.Archive;
using intaps.cis.domain.Extensions;
using intaps.cis.domain.Infrastructure;
using intaps.cis.domain.Workflows.DamsWorkflow;
using intaps.cis.domain.Workflows.Models;
using Newtonsoft.Json;

namespace intaps.cis.domain.Workflows.FileRequest
{


    public interface IFileRequestService
    {
        void SetSession(UserSession session);
        void SetContext(CisContext cisContext);

        void RequestFile(WorkflowModel workflowModel, WorkItemModel workItem);
        void RejectFileRequest(string id, WorkItemModel workItem);
        void AcceptFileRequest(string id, WorkItemModel workItem);
        void FileRequestFailed(string id, WorkItemModel workItem);
        void CheckOutFile(string id, WorkItemModel workItem);
        void CheckInFile(string id, WorkItemModel workItem);

        FileRequestModel GetRequestWorkflow(string id);
    }

    public class FileRequestService : IFileRequestService
    {
        private CisContext _cisContext;
        private UserSession _userSession;
        
        private IWorkflowService _workflowService;
        private IArchiveService _archiveService;

        public FileRequestService(IWorkflowService workflowService, IArchiveService archiveService)
        {
            _workflowService = workflowService;
            _archiveService = archiveService;
        }

        public void SetSession(UserSession session)
        {
            _userSession = session;
            _workflowService.SetSession(session);
          
        }

        public void SetContext(CisContext context)
        {
            _cisContext = context;
            _workflowService.SetContext(context);
            _archiveService.SetContext(context);
        }

        public void RequestFile(WorkflowModel workflowModel, WorkItemModel workItem)
        {
            _workflowService.CreateWorkflow(workflowModel);
            _workflowService.UpdateWorkflow(workflowModel.Id, (int)State.FileRequested);
            _workflowService.AddWorkItem(workItem);
        }

        public void RejectFileRequest(string id, WorkItemModel workItem)
        {
            _workflowService.UpdateWorkflow(id, (int)State.Closed);
            _workflowService.AddWorkItem(workItem);
        }

        public void AcceptFileRequest(string id, WorkItemModel workItem)
        {
            _workflowService.UpdateWorkflow(id, (int)State.FileRequestAccepted);
            
            var workItems = _workflowService.GetWorkItems(id);
            
            var userName =
                JsonConvert.DeserializeObject<FileRequestModel>(workItems.OrderBy(wi => wi.SeqNo).ToList()[0].Data).UserName;

            foreach (var item in workItems)
            {
                item.AssignedRole = -1;
                item.AssignedUser = userName;
                        
                _workflowService.UpdateWorkItem(item);
            }
            
            _workflowService.AddWorkItem(workItem);
        }

        public void FileRequestFailed(string id, WorkItemModel workItem)
        {
            _workflowService.UpdateWorkflow(id, (int)State.Closed);
            _workflowService.AddWorkItem(workItem);
        }

        public void CheckOutFile(string id, WorkItemModel workItem)
        {
            _workflowService.UpdateWorkflow(id, (int)State.CheckedOut);
            
            var workItems = _workflowService.GetWorkItems(id);
            
            foreach (var item in workItems)
            {
                item.AssignedRole = 2;
                item.AssignedUser = null;
                        
                _workflowService.UpdateWorkItem(item);
            }
            
            _workflowService.AddWorkItem(workItem);
        }

        public void CheckInFile(string id, WorkItemModel workItem)
        {
            _workflowService.UpdateWorkflow(id, (int)State.Closed);
            _workflowService.AddWorkItem(workItem);
            AddEntities(id);
        }

        public FileRequestModel GetRequestWorkflow(string id)
        {
            var workItem = _workflowService.GetWorkItems(id).Where(wi => !string.IsNullOrEmpty(wi.Data)).ToList()[0];

            var fileRequestModel = JsonConvert.DeserializeObject<FileRequestModel>(workItem.Data);

            if (string.IsNullOrEmpty(fileRequestModel.FileId)) return fileRequestModel;

            var fileModel = _archiveService.GetDocFile(fileRequestModel.FileId);
            
            var requestModelVm = new FileRequestViewModel()
            {
                Id =  fileRequestModel.Id,
                FileId = fileRequestModel.FileId,
                UserName = fileRequestModel.UserName,
                Description = fileRequestModel.Description,
                FileModel = fileModel,
                WorkflowId = fileRequestModel.WorkflowId
            };

            return requestModelVm;

        }


        private void AddEntities(string workflowId)
        {
            var workItems = _workflowService.GetWorkItems(workflowId).Where(wi => !string.IsNullOrEmpty(wi.Data));

            foreach (var item in workItems)
            {
                var fileRequest = JsonConvert.DeserializeObject<FileRequestModel>(item.Data);
                
                var requestModel = new PhysicalFileRequest()
                {
                    Id = Guid.NewGuid(),
                    FileId = string.IsNullOrEmpty(fileRequest.FileId) ? Guid.Empty : fileRequest.FileId.ToGuid(),
                    Description = fileRequest.Description,
                    UserName = fileRequest.UserName
                };

                _cisContext.PhysicalFileRequest.Add(requestModel);

                _cisContext.SaveChanges();
            }
        }
    }

}