﻿using intaps.cis.domain.Workflows.DamsWorkflow;
using intaps.cis.domain.Workflows.Models;
using Stateless;

namespace intaps.cis.domain.Workflows.FileRequest
{
    public enum FileRequestTrigger
    {
        RequestFile = 1,
        RejectFile = 2,
        
        AcceptFileRequest = 3,
        FileRequestFail = 4,
        
        CheckoutFile = 5,
        CheckinFile = 6
    }


    public static class FileRequestTriggerParams
    {
        public static StateMachine<State, FileRequestTrigger>.TriggerWithParameters<WorkflowModel, WorkItemModel>
            RequestFileTrigger;

        public static StateMachine<State, FileRequestTrigger>.TriggerWithParameters<string, WorkItemModel>
            RejectFileRequestTrigger;

        public static StateMachine<State, FileRequestTrigger>.TriggerWithParameters<string, WorkItemModel>
            AcceptFileRequestTrigger;
        
        public static StateMachine<State, FileRequestTrigger>.TriggerWithParameters<string, WorkItemModel>
            FileRequestFailTrigger;
        
        public static StateMachine<State, FileRequestTrigger>.TriggerWithParameters<string, WorkItemModel>
            CheckoutFileTrigger;
        
        public static StateMachine<State, FileRequestTrigger>.TriggerWithParameters<string, WorkItemModel>
            CheckinFileTrigger;

        public static void ConfigureTriggers(StateMachine<State, FileRequestTrigger> stateMachine)
        {
            RequestFileTrigger =
                stateMachine.SetTriggerParameters<WorkflowModel, WorkItemModel>(FileRequestTrigger.RequestFile);

            RejectFileRequestTrigger =
                stateMachine.SetTriggerParameters<string, WorkItemModel>(FileRequestTrigger.RejectFile);
            
            AcceptFileRequestTrigger = stateMachine.SetTriggerParameters<string, WorkItemModel>(FileRequestTrigger.AcceptFileRequest);
            
            FileRequestFailTrigger = stateMachine.SetTriggerParameters<string, WorkItemModel>(FileRequestTrigger.FileRequestFail);
            
            CheckoutFileTrigger = stateMachine.SetTriggerParameters<string, WorkItemModel>(FileRequestTrigger.CheckoutFile);
          
            CheckinFileTrigger = stateMachine.SetTriggerParameters<string, WorkItemModel>(FileRequestTrigger.CheckinFile);
        }
    }
}