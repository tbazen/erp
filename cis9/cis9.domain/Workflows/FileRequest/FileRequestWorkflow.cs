﻿using System;
using intaps.cis.data.Entities;
using intaps.cis.domain.Infrastructure;
using intaps.cis.domain.Workflows.DamsWorkflow;
using intaps.cis.domain.Workflows.Models;
using Stateless;

namespace intaps.cis.domain.Workflows.FileRequest
{
    public class FileRequestWorkflow
    {
        private StateMachine<State, FileRequestTrigger> _stateMachine;

        private IFileRequestService _fileRequestService;

        private State _state;

        public FileRequestWorkflow(IFileRequestService fileRequestService, int initialState, UserSession session, string applicationFor)
        {
            if (applicationFor.Equals("wsis"))
                INTAPS.ClientServer.ApplicationServer.RenewSession(session.WSISSessionId);
           
            _fileRequestService = fileRequestService;
            _fileRequestService.SetSession(session);

            _state = (State)Enum.Parse(typeof(State),initialState.ToString());
            
            _stateMachine = new StateMachine<State, FileRequestTrigger>(_state);

            ConfigureStateMachine();
        }

        private void ConfigureStateMachine()
        {
            FileRequestTriggerParams.ConfigureTriggers(_stateMachine);
            
            //Initial State
            _stateMachine.Configure(State.Initial)
                .Permit(FileRequestTrigger.RequestFile, State.FileRequested);


            _stateMachine.Configure(State.FileRequested)
                .OnEntryFrom(FileRequestTriggerParams.RequestFileTrigger, OnFileRequested)
                .Permit(FileRequestTrigger.AcceptFileRequest, State.FileRequestAccepted)
                .Permit(FileRequestTrigger.RejectFile, State.Closed)
                .Permit(FileRequestTrigger.FileRequestFail, State.Closed);

            _stateMachine.Configure(State.FileRequestAccepted)
                .OnEntryFrom(FileRequestTriggerParams.AcceptFileRequestTrigger, OnFileRequestAccept)
                .Permit(FileRequestTrigger.CheckoutFile, State.CheckedOut);

            _stateMachine.Configure(State.CheckedOut)
                .OnEntryFrom(FileRequestTriggerParams.CheckoutFileTrigger, OnFileCheckedOut)
                .Permit(FileRequestTrigger.CheckinFile, State.Closed);

            _stateMachine.Configure(State.Closed)
                .OnEntryFrom(FileRequestTriggerParams.RejectFileRequestTrigger, OnFileRejected)
                .OnEntryFrom(FileRequestTriggerParams.FileRequestFailTrigger, OnFileFailed)
                .OnEntryFrom(FileRequestTriggerParams.CheckinFileTrigger, OnFileCheckedIn);
        }

        public void RequestFile(WorkflowModel workflow, WorkItemModel workItemModel)
        {
            _stateMachine.Fire(FileRequestTriggerParams.RequestFileTrigger, workflow, workItemModel);
        }

        public void AcceptFileRequest(string workflowId, WorkItemModel workItemModel)
        {
            _stateMachine.Fire(FileRequestTriggerParams.AcceptFileRequestTrigger, workflowId, workItemModel);
        }

        public void RejectFileRequest(string workflowId, WorkItemModel workItemModel)
        {
            _stateMachine.Fire(FileRequestTriggerParams.RejectFileRequestTrigger, workflowId, workItemModel);
        }

        public void CheckoutFile(string workflowId, WorkItemModel workItemModel)
        {
            _stateMachine.Fire(FileRequestTriggerParams.CheckoutFileTrigger, workflowId, workItemModel);
        }

        public void FileFail(string workflowId, WorkItemModel workItemModel)
        {
            _stateMachine.Fire(FileRequestTriggerParams.FileRequestFailTrigger, workflowId, workItemModel);
        }

        public void CheckinFile(string workflowId, WorkItemModel workItemModel)
        {
            _stateMachine.Fire(FileRequestTriggerParams.CheckinFileTrigger, workflowId, workItemModel);
        }


        private void OnFileRequested(WorkflowModel workflowModel, WorkItemModel workItemModel)
        {
            _fileRequestService.RequestFile(workflowModel, workItemModel);
        }

        private void OnFileRequestAccept(string workflowId, WorkItemModel workItemModel)
        {
            _fileRequestService.AcceptFileRequest(workflowId, workItemModel);
        }

        private void OnFileCheckedOut(string workflowId, WorkItemModel workItemModel)
        {
            _fileRequestService.CheckOutFile(workflowId, workItemModel);
        }

        private void OnFileCheckedIn(string workflowId, WorkItemModel workflowItem)
        {
            _fileRequestService.CheckInFile(workflowId, workflowItem);
        }

        private void OnFileFailed(string workflowId, WorkItemModel workflowItemModel)
        {
            _fileRequestService.FileRequestFailed(workflowId, workflowItemModel);
        }

        private void OnFileRejected(string workflowId, WorkItemModel workItemModel)
        {
            _fileRequestService.RejectFileRequest(workflowId, workItemModel);
        }
    }
}