﻿using System.Collections.Generic;
using intaps.cis.data.Entities;

namespace intaps.cis.domain.Workflows.Models
{
    public enum WorkflowTypes
    {
        FileScanning = 1,
        DocumentScanning = 2,
        PropertyRegistration = 3,
        Case = 4,
        FileUpdate = 5
    }


    public class WorkflowModel
    {
        public string Id { get; set; }
        public int CurrentState { get; set; }
        public WorkflowType Type { get; set; }
        public string TimeStamp { get; set; }
        public string Title { get; set; }
        public string CurrentWorkItem { get; set; }
    }


    public class WorkItemModel
    {
        public string Id { get; set; }
        public string Workflow { get; set; }
        public int SeqNo { get; set; }
        public string Data { get; set; }
        public string DataType { get; set; }
        public int Type { get; set; }
        public string AssignedUser { get; set; }
        public int AssignedRole { get; set; }
        public IList<WorkItemNote> Notes = new List<WorkItemNote>();
    }

    public class WorkItemNote
    {
        public string Id { get; set; }
        public string Workitem { get; set; }
        public string Note { get; set; }
        public string Date { get; set; }
        public string Username { get; set; }
        public string ForwardTo { get; set; }
    }

}