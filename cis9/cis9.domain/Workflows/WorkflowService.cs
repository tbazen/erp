﻿using System;
using System.Collections.Generic;
using System.Linq;
using intaps.cis.data;
using intaps.cis.data.Entities;
using intaps.cis.domain.Admin;
using intaps.cis.domain.Extensions;
using intaps.cis.domain.Infrastructure;
using intaps.cis.domain.Workflows.Models;
using Microsoft.EntityFrameworkCore.Metadata.Conventions.Internal;

namespace intaps.cis.domain.Workflows
{

    public interface IWorkflowService
    {
       
        void SetSession(UserSession session);
        void SetContext(CisContext context);
        void CreateWorkflow(WorkflowModel model);
        void UpdateWorkflow(string id, int state);
        
        void AddWorkItem(WorkItemModel model);
        void UpdateWorkItem(WorkItemModel model);
        void RemoveWorkItem(Guid id);
        
        IList<WorkflowModel> GetWorkflow(UserSession session);
        WorkflowModel GetWorkflow(string id);
        
        
        IList<WorkItemModel> GetWorkItems(string workflowId);
        void AddWorkItemNote(WorkItemNote note);
        IList<WorkItemNote> GetNotes(string id);

        int GetWorkflowState(string workItem);
        string GetWorkflowId(string id);
        int GetWorkflowType(string workflow);
        int GetWorkItemType(string workItem);
    }
    
    public class WorkflowService : IWorkflowService
    {
        private CisContext _cisContext;
        private UserSession _userSession;


        public void SetSession(UserSession session)
        {
            _userSession = session;
        }

        public void SetContext(CisContext context)
        {
            _cisContext = context;
        }

        public void CreateWorkflow(WorkflowModel model)
        {
            Workflow workflow = new Workflow
            {
                Id = Guid.Parse(model.Id),
                Type = model.Type.Id,
                OldState = 0,
                CurrentState = model.CurrentState,
                Timestamp = DateTime.Now.Ticks,
                Title = model.Title,
                CurrentWorkItem = model.CurrentWorkItem.ToGuid()
            };
            
            var userAction = new UserAction()
            {
                ActionType = (int)UserActionType.ADD_WORKFLOW,
                Username = _userSession.UserName,
                Timestamp = DateTime.Now.Ticks
            };
            workflow.A = userAction;
            
            _cisContext.Workflow.Add(workflow);            
            _cisContext.SaveChanges(_userSession.UserName, userAction);
        }

        public void UpdateWorkflow(string id, int state)
        {
            var oldWorkflow = _cisContext.Workflow.First(wf => wf.Id == id.ToGuid());

            oldWorkflow.OldState = oldWorkflow.CurrentState;
            oldWorkflow.CurrentState = state;

            _cisContext.Workflow.Update(oldWorkflow);
            _cisContext.SaveChanges(_userSession.UserName, (int)UserActionType.UPDATE_WORKFLOW);
        }

        public void AddWorkItem(WorkItemModel model)
        {
            var workItem = new WorkItem()
            {
                Id = model.Id.ToGuid(),
                Workflow = model.Workflow.ToGuid(),
                Data = model.Data,
                DataType = model.DataType,
                SeqNo = GetMaxSeq(model.Workflow.ToGuid()) + 1,
                AssignedRole = model.AssignedRole,
                AssignedUser = model.AssignedUser,
                Type = model.Type
            };
            
            _cisContext.WorkItem.Add(workItem);
            
            if (model.Notes.Count > 0)
            {
                foreach (var note in model.Notes)
                {
                  
                    var workflowNote = new WorkitemNote()
                    {
                        Id = note.Id.ToGuid(),
                        Username = note.Username,
                        Workitem = note.Workitem.ToGuid(),
                        Note = note.Note,
                        Date = DateTime.Now.Ticks
                    };
                    
                    _cisContext.WorkitemNote.Add(workflowNote);

                }              
            }

            _cisContext.SaveChanges(_userSession.UserName, (int)UserActionType.ADD_WORKITEM);

        }

        public void UpdateWorkItem(WorkItemModel model)
        {
            var oldWorkItem = _cisContext.WorkItem.First(wi => wi.Id == model.Id.ToGuid());

            oldWorkItem.Data = model.Data;
            oldWorkItem.DataType = model.DataType;
            oldWorkItem.AssignedUser = model.AssignedUser;
            oldWorkItem.AssignedRole = model.AssignedRole;
            oldWorkItem.Workflow = model.Workflow.ToGuid();
           
            _cisContext.WorkItem.Update(oldWorkItem);
            
            _cisContext.SaveChanges(_userSession.UserName, (int)UserActionType.UPDATE_WORKITEM);

        }

        public void AddWorkItemNote(WorkItemNote note)
        {
            var workflowNote = new WorkitemNote()
            {
                Id = note.Id.ToGuid(),
                Username = note.Username,
                Workitem = note.Workitem.ToGuid(),
                Note = note.Note,
                Date = DateTime.Now.Ticks,
                ForwardTo = note.ForwardTo
            };
                    
            _cisContext.WorkitemNote.Add(workflowNote);

            _cisContext.SaveChanges(_userSession.UserName, (int)UserActionType.ADD_WORKITEM_NOTE);

        }

        public void RemoveWorkItem(Guid id)
        {
            var workItem = _cisContext.WorkItem.First(wi => wi.Id == id);

            _cisContext.WorkItem.Remove(workItem);

            _cisContext.SaveChanges(_userSession.UserName, (int)UserActionType.REMOVE_WORKITEM);

        }

        public IList<WorkflowModel> GetWorkflow(UserSession session)
        {
     
            var workflows = from wf in _cisContext.Workflow
                join wi in _cisContext.WorkItem on wf.CurrentWorkItem equals wi.Id
                where (wf.CurrentState != 6 && wf.CurrentState != 7) &&
                      (wi.AssignedRole == session.Role || wi.AssignedUser == session.UserName)
                orderby wf.Type
                select wf;

            var workflowModels = new List<WorkflowModel>();
            
            //A way of caching the workflow type
            WorkflowType wfType = null;
            
            
            foreach (var workflow in workflows)
            {
                var model = new WorkflowModel()
                {
                    Id = workflow.Id.ToString(),
                    CurrentState = workflow.CurrentState,
                    TimeStamp = new DateTime(workflow.Timestamp).ToShortDateString(),
                    Title = workflow.Title
                };
              
                if (wfType != null && wfType.Id == workflow.Type) model.Type = wfType;

                else
                {
                    wfType = _cisContext.WorkflowType.First(wt => wt.Id == workflow.Type);
                    model.Type = wfType;
                }
                
                workflowModels.Add(model);
            }

            return workflowModels;
        }

        public WorkflowModel GetWorkflow(string id)
        {
            var workflow = _cisContext.Workflow.First(wf => wf.Id == id.ToGuid());
            
            return new WorkflowModel()
            {
                Id = workflow.Id.ToString(),
                Type = _cisContext.WorkflowType.First(wt => wt.Id == workflow.Type),
                CurrentState = workflow.CurrentState,
                TimeStamp = new DateTime(workflow.Timestamp).ToShortDateString(),
            };
        }

        public IList<WorkItemNote> GetNotes(string id)
        {
            var workItems = _cisContext.WorkItem.Where(wi => wi.Workflow == id.ToGuid());
            
            var notes = new List<WorkItemNote>();

            foreach (var workItem in workItems)
            {
                var workItemNotes = _cisContext.WorkitemNote.Where(wn => wn.Workitem == workItem.Id).OrderBy(wn => wn.Date);

                foreach (var workitemNote in workItemNotes)
                {
                   
                    notes.Add(new WorkItemNote()
                    {
                        Id = workitemNote.Id.ToString(),
                        Date = new DateTime(workitemNote.Date).ToLongDateString() ,
                        Note = workitemNote.Note,
                        Username = workitemNote.Username,
                        Workitem = workitemNote.Workitem.ToString(),
                        ForwardTo = workitemNote.ForwardTo
                    });
                }
            }

            return notes;
        }
  

        public IList<WorkItemModel> GetWorkItems(string workflowId)
        {
            var workItems = _cisContext.WorkItem.Where(wi => wi.Workflow == workflowId.ToGuid()).OrderBy(wi => wi.SeqNo);
            
            var workItemModels = new List<WorkItemModel>();

            foreach (var item in workItems)
            {
                var model = new WorkItemModel()
                {
                    Id = item.Id.ToString(),
                    AssignedRole = item.AssignedRole,
                    AssignedUser = item.AssignedUser,
                    Data = item.Data,
                    DataType = item.DataType,
                    Workflow = item.Workflow.ToString(),
                    Notes = new List<WorkItemNote>()
                };

                foreach (var note in _cisContext.WorkitemNote.Where(wn => wn.Workitem == item.Id))
                {
                   
                    model.Notes.Add(new WorkItemNote()
                    {
                        Id = note.Id.ToString(),
                        Date = new DateTime(note.Date).ToShortDateString(),
                        Note = note.Note,
                        Username = note.Username,
                        Workitem = note.Workitem.ToString(),
                        
                    });
                }
                
                workItemModels.Add(model);
                
            }

            return workItemModels;
        }

        public int GetWorkflowState(string workflow)
        {         
            var state = _cisContext.Workflow.First(wf => wf.Id == workflow.ToGuid()).CurrentState;
            return state;
        }

        public string GetWorkflowId(string id)
        {
            return _cisContext.WorkItem.First(wi => wi.Id == id.ToGuid()).Workflow.ToString();
        }

        public int GetWorkflowType(string workflow)
        {
            return _cisContext.Workflow.First(wf => wf.Id == workflow.ToGuid()).Type;
        }

        public int GetWorkItemType(string workItem)
        {
            return _cisContext.WorkItem.First(wi => wi.Id == workItem.ToGuid()).Type;
        }

        private long GetMaxSeq(Guid id)
        {
            var seqs =  _cisContext.WorkItem.Where(wi => wi.Workflow == id);

            return !seqs.Any() ? 0 : seqs.Select(wi => wi.SeqNo).Max();
        }
    }
}