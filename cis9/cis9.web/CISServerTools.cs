﻿using intaps.cis.domain.Workflows.DamsWorkflow;
using Npgsql;
using System;
using System.Collections.Generic;
using System.IO;
using intaps.cis.domain.Extensions;

namespace intaps.cis
{
    class ServerToolMethodAttribute : Attribute
    {
        public String name;
        public String pass = null;
    }
    public class CISServerTools
    {
        public class ConnectionStrings
        {
            public String cis_context;
        }
        class ConfigObject
        {
            public ConnectionStrings ConnectionStrings;
            public DamsOptions DamsOptions;
            public HostConfiguration HostConfiguration;
        }
        
        [ServerToolMethod(name = "Write unsaved images from workitems",pass ="spatni")]
        public static void fixNotSavedFiles()
        {
            ConfigObject conf=readConfiguration();
            Console.WriteLine("Unsaved images verifier.\nDo you want to try to save too?(Y/N)");
            bool fix = Console.ReadLine().Equals("Y");

            using (var conn = new NpgsqlConnection(conf.ConnectionStrings.cis_context))
            {
                conn.Open();
                Console.WriteLine("Loading alls files ids");
                List<String> fileIDs = new List<string>();
                using (var fileReader = new NpgsqlCommand("Select id from wf.archive_file", conn).ExecuteReader())
                {
                    while (fileReader.Read())
                    {
                        fileIDs.Add(fileReader[0].ToString());
                    }
                }
                int c = fileIDs.Count;
                int nimage = 0;
                Console.WriteLine($"{c} files found");
                int nTotal = 0;
                int nError = 0;
                foreach (String fileID in fileIDs)
                {
                    Console.CursorTop--;
                    Console.WriteLine($"{c}                         {nimage}\\{nTotal}      unfixable:{nError}");
                    c--;
                    String sql = $@"SELECT id,workitem
	                    FROM wf.archive_image where document_id in (Select id from wf.document where file_id='{fileID}'::uuid)";
                    Dictionary<String, String> toSave = new Dictionary<String, String>();
                    using (var imageReader = new NpgsqlCommand(sql, conn).ExecuteReader())
                    {
                        while (imageReader.Read())
                        {
                            nTotal++;
                            var imageID = imageReader[0].ToString();
                            if (!System.IO.File.Exists(conf.DamsOptions.ImagePath + imageID + ".jpeg"))
                            {
                                toSave.Add(imageReader[0].ToString(), imageReader[1].ToString());
                                nimage++;
                            }
                        }
                    }
                    foreach (var wi in toSave)
                    {
                        sql = $"Select data from wf.work_item where id='{wi.Value}'::uuid";
                        String json = new NpgsqlCommand(sql, conn).ExecuteScalar().ToString();
                        var imginfo = Newtonsoft.Json.JsonConvert.DeserializeObject<intaps.cis.domain.Workflows.DamsWorkflow.ImageModel>(json);
                        BinaryWriter binaryWriter = null;
                        try
                        {
                            if (String.IsNullOrEmpty(imginfo.Data))
                                throw new Exception("Data not saved");
                            var byteArr = Convert.FromBase64String(imginfo.Data);
                            if (fix)
                            {
                                binaryWriter = new BinaryWriter(File.OpenWrite(conf.DamsOptions.ImagePath + wi.Key + ".jpeg"));
                                binaryWriter.Write(byteArr);
                                binaryWriter.Flush();
                            }
                        }
                        catch (Exception e)
                        {
                            nError++;
                            Console.WriteLine($"Error saving imageid:{wi.Key} wi:{wi.Value}");
                            Console.WriteLine(e.Message);
                            if (fix)
                                Console.ReadLine();
                        }
                        finally
                        {
                            binaryWriter?.Close();
                        }
                    }
                }
                Console.WriteLine($"" +
                    $"" +
                    $"{nimage}\\{nTotal}      unfixable:{nError}");

            }
        }

        private static ConfigObject readConfiguration()
        {
            ConfigObject conf;
            String configJson = System.IO.File.ReadAllText("appsettings.json");
            conf = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigObject>(configJson);
            return conf;
        }
    }
}
