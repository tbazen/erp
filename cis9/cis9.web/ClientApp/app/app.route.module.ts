﻿import {Routes} from "@angular/router";
import {MainComponent} from "./components/main/main.component";
import {DashboardComponent} from "./components/admin/dashboard/dashboard.component";
import {UsersComponent} from "./components/admin/users/users.component";
import {AuditComponent} from "./components/admin/audit/audit.component";
import {LoginComponent} from "./components/admin/users/login/login.component";
import {Dashboard2Component} from "./components/dashboard2/dashboard2.component";
import {DashboardPanelComponent} from "./components/dashboard2/mainBody/dashboardPanel/dashboardPanel.component";
import {PropertyRegistarationComponent} from "./components/propertyRegistration/propertyRegistration.component";
import {NoAuthGuard} from "./services/auth.guard";
import {DamsdashboardComponent} from "./components/dams/dashboard/damsdashboard.component";
import {UpdatepasswordComponent} from "./components/dams/updatepassword/updatepassword.component";

export const appRoutes: Routes = [
    {path: 'login', component: LoginComponent},

    {
        path: 'changepassword', component:UpdatepasswordComponent,
        canActivate: [NoAuthGuard]
    },
    
    {
        path: '',
        component: MainComponent,
        canActivate: [NoAuthGuard]
    },
  /*  {
        path: '',
        component: MainComponent,
        children: [
            {
                path: 'dashboard', component: Dashboard2Component,
                children: [
                    {path: 'pendingTask', component: DashboardPanelComponent},
                    {path: 'propertyRegistration', component: PropertyRegistarationComponent}
                ]
            }
        ],
        canActivate: [NoAuthGuard]
    },*/
    // {path: '**', redirectTo: 'login'},
];