import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import { ToastrModule } from 'ngx-toastr';
    

import { AppComponent } from './components/app/app.component';
import {ApiService} from "./services/api.service";
import {appRoutes} from "./app.route.module";
import {MainModule} from "./components/main/main.module";
import {DashboardModule} from "./components/admin/dashboard/dashboard.module";
import {UsersModule} from "./components/admin/users/users.module";
import {AuditModule} from "./components/admin/audit/audit.module";
import {AuthModule} from "./services/auth/auth.module";
import {PagerService} from "./services/pager.service";
import {Dashboard2Module} from "./components/dashboard2/dashboard2.module";
import {MainBodyModule} from "./components/dashboard2/mainBody/mainBody.module";
import {DamsheaderModule} from "./components/dams/header/damsheader.module";
import {DamsdashboardModule} from "./components/dams/dashboard/damsdashboard.module";
import {WorkflowResolve as FileResolve} from "./components/dams/dashboard/damsdashboard.resolve";
import {DamssearchModule} from "./components/dams/search/damssearch.module";
import {AdminGuard, DamsGuard, NoAuthGuard} from "./services/auth.guard";
import {BrowserModule} from "@angular/platform-browser";


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(),
        AuthModule,
        MainModule,
        DashboardModule,
        UsersModule,
        AuditModule,
        Dashboard2Module,
        MainBodyModule,
        DamsheaderModule,
        DamsdashboardModule,
        DamssearchModule,
        RouterModule.forRoot(appRoutes, {useHash: true})
        
    ],
    providers : [
        ApiService, PagerService, FileResolve, NoAuthGuard, DamsGuard, AdminGuard
        ]
})
export class AppModuleShared {
}
