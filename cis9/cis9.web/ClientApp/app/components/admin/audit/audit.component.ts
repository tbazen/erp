﻿import {Component, OnInit} from "@angular/core";
import {ActionType, AuditModel, AuditQuery} from "./models/AuditModel";
import {AuditService} from "./audit.service";
import {NgxSpinnerService} from "ngx-spinner";
import * as _ from "lodash";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";

@Component({
    selector: 'app-audit',
    templateUrl: './audit.component.html'
})
export class AuditComponent implements OnInit{
    
    public auditModels: AuditModel[];
    
    public actionTypes: ActionType[];
    
    public auditQuery: AuditQuery;
    
    public pageSize = 100;
    
    public totalCount: number;
    public pages = [];
    public currentPage: number = 1;
    public totalPages:number;
    public startPage:number;
    public endPage:number;
    public startIndex:number;
    public endIndex:number;
    public auditForm: FormGroup;
    
    public seaching: boolean = false;
    
    constructor(public auditService: AuditService, public loadingService: NgxSpinnerService, public fb: FormBuilder, public toastrService: ToastrService){
        
        this.auditForm = this.fb.group({
            userName: ['', Validators.required],
            actionType: ['', Validators.required],
            startDate: ['', Validators.required]
        })
    }
    
     ngOnInit(){
        
        this.auditQuery = {
            page:1,
            pageSize:100,
            userName: '',
            action: 1,
            startDate: ''
        };
        
        console.log(this.auditQuery);
        
        this.getActions(1);
        this.getActionTypes();
    }
    
    public getActions(pageNum: number) {
        if (!this.seaching){
            this.loadingService.show();
            this.auditService.getAudits({page: pageNum, pageSize:this.pageSize}).subscribe(res =>{
                this.auditModels = res.json();
                this.loadingService.hide();
                this.totalCount = res.headers.get("x-total-count");
                this.totalPages = Math.ceil(this.totalCount / this.pageSize);
                this.currentPage = pageNum;
                if(this.totalPages<=10){
                    this.startPage=1;
                    this.endPage=this.totalPages;
                }
                else {
                    if (this.currentPage <= 6) {
                        this.startPage = 1;
                        this.endPage = 10;
                    } else if (this.currentPage + 4 >= this.totalPages) {
                        this.startPage = this.totalPages - 9;
                        this.endPage = this.totalPages;
                    } else {
                        this.startPage = this.currentPage - 5;
                        this.endPage = this.currentPage + 4;
                    }
                }
                 this.startIndex = (this.currentPage - 1) * this.pageSize;
                this.endIndex = Math.min(this.startIndex + this.pageSize - 1, this.totalCount - 1);
                this.pages = Array.from(Array((this.endPage + 1) - this.startPage).keys()).map(i => this.startPage + i);
            });   
        }
        else{
            this.auditQuery.page = pageNum;
            this.currentPage = pageNum; 
            this.searchActions();
        }
    }
    
    public getActionTypes() {
        this.auditService.getActionTypes().subscribe(res => {this.actionTypes = res});
    }
    
    public searchActions() {
        this.seaching = true;
        this.loadingService.show();
        this.auditService.searchActions(this.auditQuery).subscribe(res => {
            this.auditModels = res.json();
            this.loadingService.hide();
            this.totalCount = res.headers.get("x-total-count");
            this.totalPages = Math.ceil(this.totalCount / this.pageSize);
            
            if(this.totalPages<=10){
                this.startPage=1;
                this.endPage=this.totalPages;
            }
            else {
                if (this.currentPage <= 6) {
                    this.startPage = 1;
                    this.endPage = 10;
                } else if (this.currentPage + 4 >= this.totalPages) {
                    this.startPage = this.totalPages - 9;
                    this.endPage = this.totalPages;
                } else {
                    this.startPage = this.currentPage - 5;
                    this.endPage = this.currentPage + 4;
                }
            }
            this.startIndex = (this.currentPage - 1) * this.pageSize;
            this.endIndex = Math.min(this.startIndex + this.pageSize - 1, this.totalCount - 1);
            this.pages = Array.from(Array((this.endPage + 1) - this.startPage).keys()).map(i => this.startPage + i);
        }, error => {
            this.loadingService.hide();
            });
    }
    
    
}