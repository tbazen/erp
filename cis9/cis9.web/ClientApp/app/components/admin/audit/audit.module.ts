﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {AuditComponent} from "./audit.component";
import {AuditService} from "./audit.service";
import {RouterModule} from "@angular/router";
import {NgxSpinnerModule} from "ngx-spinner";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        NgxSpinnerModule
        ],
    
    declarations: [
        AuditComponent
        ],
    
    exports: [AuditComponent],
    providers: [AuditService]
})
export class AuditModule{
    
}