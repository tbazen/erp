﻿import {Injectable} from "@angular/core";
import {ApiService} from "../../../services/api.service";
import {AuditQuery} from "./models/AuditModel";

@Injectable()
export class AuditService {
    
    constructor(public apiService: ApiService){}
    
    public getAudits(query: AuditQuery){
        return this.apiService.getWithHeaders(`audit/getaudit?page=${query.page}&pageSize=${query.pageSize}`);
    }

    getActionTypes() {
        return this.apiService.get("lookup/getactiontypes");
    }
    
    public searchActions(query: AuditQuery) {
        return this.apiService.getWithHeaders(`audit/searchaudits?userName=${query.userName}&actionType=${query.action}
                                    &startDate=${query.startDate}&endDate=${query.endDate}&page=${query.page}&pageSize=${query.pageSize}`)
    }
}