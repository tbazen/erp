﻿export interface ActionType {
    id:number;
    name:string;
    userAction: any
}

export interface AuditModel{
    userName: string;
    action: ActionType;
    actionTime: string;
}


export interface AuditQuery {
    page: number;
    pageSize: number;
    userName?:string;
    action?: number;
    startDate?: string;
    endDate?:string;
}