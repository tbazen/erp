import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
declare var $:any;
@Component({
    selector: 'app-dash',
    templateUrl: './dashboard.component.html'
})

export class DashboardComponent implements AfterViewInit, OnInit{
    
    constructor(public router: Router){}
    
    
    ngAfterViewInit(): void {
        this.router.navigate(["admin/dashboard/users"])
    }
    ngOnInit(): void {

        this.tabStyle();
    }
    public tabStyle():void{
        $(".tab-header").find('a').on('click', function(event:any) {
            console.log('clicked - sidebar_menu');
            let target=event.currentTarget;
            let $li = $(target).parent();

            if ($li.is('.active')) {
                $li.removeClass('active');
            }
            else{
                $(".tab-header").find( "li" ).removeClass( "active" );
            }
                $li.addClass('active');
        });
    }
    
    
}
