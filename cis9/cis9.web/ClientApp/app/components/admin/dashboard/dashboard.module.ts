﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {DashboardComponent} from "./dashboard.component";
import {DashboardRouteModule} from "./dashboard.route.module";
import {UsersModule} from "../users/users.module";
import {AuditModule} from "../audit/audit.module";

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        DashboardRouteModule,
        UsersModule,
        AuditModule
        ],
    
    declarations: [
        DashboardComponent
        ],
    
    exports: [DashboardComponent]
})

export class DashboardModule{}