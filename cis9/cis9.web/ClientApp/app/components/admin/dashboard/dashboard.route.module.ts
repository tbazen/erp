﻿import {RouterModule, Routes} from "@angular/router";
import {DashboardComponent} from "./dashboard.component";
import {NgModule} from "@angular/core";
import {UsersComponent} from "../users/users.component";
import {AuditComponent} from "../audit/audit.component";
import {AdminGuard, NoAuthGuard} from "../../../services/auth.guard";

const dashboardRoutes: Routes = [
    {
        path: 'dashboard', 
        component: DashboardComponent,
        children: [
            {
                path: 'users',
                component: UsersComponent,
                
            },
            {
                path: 'audit',
                component: AuditComponent,

            },
            {
                path: '',
                component: UsersComponent,

            },
            ],
        
        canActivate: [NoAuthGuard,AdminGuard]
        
    }

];

@NgModule({
    imports: [
        RouterModule.forChild(dashboardRoutes)
        ],
    exports: [
        RouterModule
        ]
    }
)

export class DashboardRouteModule {

}