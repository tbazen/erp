﻿import {Component, EventEmitter, Input, Output} from "@angular/core";
import {UserModel} from "../models/UserModel";
declare var $:any;
@Component({
    selector: 'app-user-details',
    templateUrl: './user.detail.component.html'
})
export class UserDetailComponent{
    @Input() public user: UserModel;
    @Output() formClose = new EventEmitter();
    public closeForm() {
        this.formClose.next();
    }
}

