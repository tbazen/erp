import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {FormBuilder, FormControl, FormGroup, Validator, Validators} from "@angular/forms";
import {WsisUser} from "../../models/UserModel";
import {UsersService} from "../../users.service";
import {ToastrService} from "ngx-toastr";
import {AuthService} from "../../../../../services/auth/auth.service";
import { window } from "rxjs/operators";
import { Router } from "@angular/router";

declare var $:any;
@Component({
    selector:'app-edit-role',
    templateUrl:'./edit-role.component.html'
})

export class EditRoleComponent implements OnInit{
    @Input() wsisUser:WsisUser;
    public editRoleForm:FormGroup;
    public roleModel:WsisUser;
    public ROLES = [];
   
    @Output() closeUpdateRoleForm = new EventEmitter();

    constructor(public userService: UsersService, public fb: FormBuilder, public toastr: ToastrService, public authService: AuthService, public router: Router) {
        this.editRoleForm=this.fb.group({
            username:new FormControl({value:'', disabled:true}, Validators.required),
            role:['', Validators.required],
            userid:['']
        })
    }
    ngOnInit():void{
        this.authService.getRoles().subscribe(res=>{
            this.ROLES=res;
        });
        
        this.roleModel={
            userId:this.wsisUser.userId,
            userName:this.wsisUser.userName,
            role:this.wsisUser.role
        };
        $('#edit_user_role').val(this.wsisUser.role);
    }
    
    public updateRole(){
        this.userService.updateRole(this.roleModel).subscribe(resizeBy=>{
            this.toastr.success('You have successfully edit user role');
            this.closeForm();
            this.router.navigateByUrl("dams/dashboard/users");
        })
    }
    public closeForm() {
        this.closeUpdateRoleForm.next();
        
    }
}