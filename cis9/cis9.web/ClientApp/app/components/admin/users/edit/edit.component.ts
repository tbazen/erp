﻿import {Component, EventEmitter, Input, Output, OnInit} from "@angular/core";
import {UserModel} from "../models/UserModel";
import {UsersService} from "../users.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {UsersComponent} from "../users.component";
import {ToastrService} from "ngx-toastr";
import {AuthService} from "../../../../services/auth/auth.service";
import {DISABLED} from "@angular/forms/src/model";
declare var $:any;
@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html'
})
export class EditComponent implements OnInit{
    
    @Input() public userModel: UserModel;
    public editForm: FormGroup;
    public editModel:UserModel;
    public ROLES = [];
    @Output() closeEditForm = new EventEmitter();
    
    constructor(public userService: UsersService, public fb: FormBuilder, public toastr: ToastrService, public authService:AuthService){
        this.editForm = fb.group({
            fullname: ['', Validators.required],
            phoneno: ['', Validators.required],
            role: ['', Validators.required],
            username:new FormControl({value:'',disabled:true}, Validators.required)
        })
    }
    ngOnInit():void{

        this.authService.getRoles().subscribe(res => {
            this.ROLES = res;
        });
        console.log(this.userModel);
        this.editModel={
            userName:this.userModel.userName,
            fullName:this.userModel.fullName,
            phoneNumber:this.userModel.phoneNumber,
            status: this.userModel.status,
            roles: this.userModel.roles
            
        }
        $('#edit_user_role').val(this.editModel.roles);
}
    public editUser(){
        this.userService.editUser(this.editModel).subscribe(resizeBy => {
            this.toastr.success("User Edited Successfully", "")
            this.closeForm();
        });
    }
    
    public closeForm() {
       this.closeEditForm.next();
    }

    
    
}