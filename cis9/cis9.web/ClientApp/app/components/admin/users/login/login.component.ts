﻿import {Router} from "@angular/router";
import {Component, OnInit} from "@angular/core";
import { ToastrService } from 'ngx-toastr';
import {FormBuilder, FormGroup, Validators, ReactiveFormsModule} from "@angular/forms";
import {AuthService} from "../../../../services/auth/auth.service";
import {LoginUser} from "../../../../services/auth/auth.model";
import {TileStyler} from "@angular/material/typings/grid-list/tile-styler";
import { forEach } from "@angular/router/src/utils/collection";
import {UsersService} from "../users.service";

declare var $ :any;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit{
    
    public loggedIn: boolean = false;
    public user: LoginUser;
    public loginForm: FormGroup;
    public selectedRole: number;
    public config:any;
    

    public ROLES = [];
    
    constructor(public router: Router, public authService: AuthService,public userService:UsersService, public formBuilder: FormBuilder, public toastr: ToastrService){
        this.loginForm = formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    ngOnInit(): void {
        this.userService.loadPage().subscribe(res=>{
            this.config=res;
        });
        this.user = {
            username : '',
            password : '',
        }
    
    }
    
    public login(): void{
        this.user.username = this.user.username;
        if(this.config.applicationFor=='cis'){
            this.authService.login(this.user).subscribe(res => {
                localStorage.setItem("username", this.user.username);
                this.authService.getUserRoles().subscribe(res => {
                    this.ROLES = res;

                    if (this.ROLES.length == 1){ //Only one role, log the user with it
                        this.selectedRole = this.ROLES[0].id;
                        this.proceed();
                    }
                    else{
                        $("#roleModal").show().addClass('in');
                    }


                });
            });
        }
        else if(this.config.applicationFor=='wsis'){
            this.authService.loginWsis(this.user).subscribe(res => {
                localStorage.setItem("username", this.user.username);
                this.authService.getUserRoles().subscribe(res => {
                    this.ROLES = res;

                    if (this.ROLES.length == 1){ //Only one role, log the user with it
                        this.selectedRole = this.ROLES[0].id;
                        this.proceed();
                    }
                    else{
                        $("#roleModal").show().addClass('in');
                    }


                });
            });
        }
       
    }
    
    public proceed():void{
        this.authService.setUserRole({role: this.selectedRole}).subscribe(res => {
            if (typeof this.selectedRole === "string") {
                this.selectedRole = +this.selectedRole;
            }
            localStorage.setItem("role", JSON.stringify(this.selectedRole));
            for (let r of this.ROLES) {
                if (r.id === this.selectedRole) {
                    localStorage.setItem("role_name", r.name);
                    break;
                }
            }
            if (this.selectedRole == 1) {
                this.router.navigate(['/admin/dashboard/users'])
            }
            else if (this.selectedRole == 2 || this.selectedRole == 4 || this.selectedRole == 5 || this.selectedRole == 6){
                this.router.navigate(['/dams/dashboard'])
            }
        });
        
    }
    
    public closeForm(roleID:number):void{
        this.selectedRole=roleID;
        $("#roleModal").hide();
        this.proceed();
    }
    
}