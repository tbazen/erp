﻿
export interface UserModel {
    userName : string;
    fullName: string;
    phoneNumber: string;
    status: number;
    roles: string[];
    
}

export interface UserQuery {
    page : number;
    pageSize: number;
    filters?: string;
    sorts?:string
}

export interface WsisUser {
    userId:number;
    userName:string;
    role:string[];
}

