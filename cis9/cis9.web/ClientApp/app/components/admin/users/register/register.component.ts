﻿import {Component, EventEmitter, OnInit, Output} from "@angular/core";
import {UsersComponent} from "../users.component";
import {RegisterUser} from "../../../../services/auth/auth.model";
import {AuthService} from "../../../../services/auth/auth.service";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {UsersService} from "../users.service";
import {Router} from "@angular/router";

declare var $:any;

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit{
    
    public userModel: RegisterUser;
    public registrationForm: FormGroup;

    @Output() closeEditForm = new EventEmitter();
    
    public ROLES = [];
    
    constructor(public authService: AuthService, public fb: FormBuilder, public toastr : ToastrService, public userComponent:UsersComponent, public router:Router){
        this.registrationForm = fb.group({
            fullname: ['', Validators.required],
            phoneno: ['', Validators.required],
            role: ['', Validators.required],
            username: ['', [Validators.required, Validators.min(6)]],
            password: ['', Validators.required],
            confirmPassword: ['',[ Validators.required, RegisterComponent.matchValidator]]
        })
    }
    
    public ngOnInit():void {
        this.authService.getRoles().subscribe(res => {
            this.ROLES = res;
        });
        this.userModel = {
            username: '',
            password: '',
            fullname: '',
            roles: [],
            phonenumber: ''
        }
    }
    
    
    public register(){
        console.log(this.userModel);
       this.authService.register(this.userModel).subscribe(res => {
           if(res.errorCode!=null){
               this.toastr.error(res.message, "User Registration")
           }
           else {
               this.closeForm();
               this.toastr.success("User Registered Successfully", "User Registration")
           }
       });
    }

    public closeForm() {
        this.closeEditForm.next();
        $('#registrationForm').trigger('reset');
    }
    
    
    static matchValidator(abs: AbstractControl){
        
        const control = abs.parent;
        if (control){
            const passwordCtrl = control.get('password');
            const confirmPassCtrl = control.get('confirmPassword');
            
            if(passwordCtrl && confirmPassCtrl){
                const pass = passwordCtrl.value;
                const confirmPass = confirmPassCtrl.value;

                if(pass != confirmPass){
                   return {matchPassword : true};
                }
                else {
                    return null;
                }
            }

             
        }
        
        else{
            return null;
        }
    }
    
}