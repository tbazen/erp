﻿import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ResetPassowrd} from "../../../../services/auth/auth.model";
import {UsersService} from "../users.service";
import {ToastrService} from "ngx-toastr";
import {UserModel} from "../models/UserModel";

declare var $:any;

@Component({
    selector:'app-reset-password',
    templateUrl:'./resetpassword.component.html'
})
export class ResetpasswordComponent implements OnInit{
    @Input() public userModel: UserModel;

    @Output() closeResetPassForm = new EventEmitter();
    public newPassConfirm:string;
    public updatePassForm:FormGroup;
    public passwordModel:ResetPassowrd;
    
    public valid:boolean |true;
    
    constructor(public fb:FormBuilder, public userService: UsersService, public toastr: ToastrService,){
        this.updatePassForm=fb.group({
            newPass:['', Validators.required],
            confirmPass:['', [Validators.required, ResetpasswordComponent.matchValidator]]
        });
    }
    
    ngOnInit():void{
        this.passwordModel={
            UserName:this.userModel.userName,
            NewPassword:''
        }
    }

    public updatePass():void{
        console.log(this.passwordModel);
        this.userService.resetPass(this.passwordModel).subscribe(res=>{
            if (res.errorCode != null) {
                this.toastr.error(res.message, 'Error');
            }else {
                this.closePassForm();
                this.toastr.success("Successfully Reset the password", "Password Update");
            }
        })

    }
    public closePassForm():void{
        this.closeResetPassForm.next();
    }
    public checkDiff(){

        if ($('#confirmPass').val() != $('#newPass').val()){
            this.newPassConfirm='New and Confirm Password Missmatch';
            this.valid=false;
        }
        else {
            this.newPassConfirm='';
            this.valid=true;
        }
        return this.valid;
    }
    static matchValidator(abs: AbstractControl){

        const control = abs.parent;
        if (control){
            const passwordCtrl = control.get('newPass');
            const confirmPassCtrl = control.get('confirmPass');

            if(passwordCtrl && confirmPassCtrl){
                const pass = passwordCtrl.value;
                const confirmPass = confirmPassCtrl.value;

                if(pass != confirmPass){
                    return {matchPassword : true};
                }
                else {
                    return null;
                }
            }
        }

        else{
            return null;
        }
    }
}