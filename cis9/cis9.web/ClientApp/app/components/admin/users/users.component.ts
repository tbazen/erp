﻿import {AfterViewInit, Component, OnInit} from "@angular/core";
import {UserModel, WsisUser} from "./models/UserModel";
import {UsersService} from "./users.service";
import {ToastrService} from "ngx-toastr";
import {PagerService} from "../../../services/pager.service";
import {Router} from "@angular/router";
import {NgxSpinnerService} from "ngx-spinner";
import * as _ from "lodash"

declare var $: any;

@Component(
    {
        selector: "app-users",
        templateUrl: "./users.component.html"
    }
)

export class UsersComponent implements OnInit,AfterViewInit{

    public isRegisterShowed : boolean = false;
    public users: UserModel[];
    public selectedUser = null;
    public selectedWSISUser = null;
    public query: string;
    
    public pager: any = {};
    pagedItems : any[];
    
    public pageSize = 30;
    public currentPage: number = 1;
    public totalCount: number = 0;
    public pages = [];
   
    public isEdit: boolean = false;
    public wsisUser:any;
    public config:any;
    public totalPages:number;
    public startPage:number;
    public endPage:number;
    public startIndex:number;
    public endIndex:number;
    
    
    constructor( public userService: UsersService, public toastr: ToastrService, public pagerSerice: PagerService, public router:Router, public loadingService: NgxSpinnerService){}

    ngOnInit(): void {
       this.loadingService.show();
        
        this.userService.loadPage().subscribe(res=>{
           this.config=res;
            if(this.config.applicationFor == 'wsis'){
                this.getAllWSISUsers();
            }else{
                this.getAllUsers(1);
            }
        });
        
        
        
    }

    ngAfterViewInit(): void {
       
    }
    
    public getAllWSISUsers(){
        this.userService.getWSISUsers().subscribe(res=>{
            this.wsisUser=res;
        })
    }
    public filterUsers(search: string){
        this.userService.getUsers({page:1, pageSize:30, filters:search}).subscribe(res => {
            this.users = res.json();
            this.totalCount = res.headers.get("x-total-count");
            this.totalPages = Math.ceil(this.totalCount / this.pageSize);
            this.currentPage = 1;
            if(this.totalPages<=10){
                this.startPage=1;
                this.endPage=this.totalPages;
            }
            else {
                if (this.currentPage <= 6) {
                    this.startPage = 1;
                    this.endPage = 10;
                } else if (this.currentPage + 4 >= this.totalPages) {
                    this.startPage = this.totalPages - 9;
                    this.endPage = this.totalPages;
                } else {
                    this.startPage = this.currentPage - 5;
                    this.endPage = this.currentPage + 4;
                }
            }
            this.startIndex = (this.currentPage - 1) * this.pageSize;
            this.endIndex = Math.min(this.startIndex + this.pageSize - 1, this.totalCount - 1);
            this.pages = Array.from(Array((this.endPage + 1) - this.startPage).keys()).map(i => this.startPage + i);
        });
        
    }
    
    public getAllUsers(pageNum: number){
        this.userService.getUsers({page:pageNum, pageSize: this.pageSize}).subscribe(res => {
            this.users = res.json();
            this.totalCount = res.headers.get("x-total-count");
            this.loadingService.hide();

            this.totalPages = Math.ceil(this.totalCount / this.pageSize);
            this.currentPage = pageNum;
            if(this.totalPages<=10){
                this.startPage=1;
                this.endPage=this.totalPages;
            }
            else {
                if (this.currentPage <= 6) {
                    this.startPage = 1;
                    this.endPage = 10;
                } else if (this.currentPage + 4 >= this.totalPages) {
                    this.startPage = this.totalPages - 9;
                    this.endPage = this.totalPages;
                } else {
                    this.startPage = this.currentPage - 5;
                    this.endPage = this.currentPage + 4;
                }
            }
            this.startIndex = (this.currentPage - 1) * this.pageSize;
            this.endIndex = Math.min(this.startIndex + this.pageSize - 1, this.totalCount - 1);
            this.pages = Array.from(Array((this.endPage + 1) - this.startPage).keys()).map(i => this.startPage + i);
        }); 
    }
    
    
    public resetPass(usr:UserModel){
        this.selectedUser=usr;
        $('#updatePass').addClass('in');
        $('#updatePass').show();
    }
    public closePassForm():void{
        $('#updatePass').removeClass('in');
        $('#updatePass').hide();
        $('#updatePassForm').trigger('reset');
    }
    public closeRoleForm():void{
        $('#updateRole').removeClass('in');
        $('#updateRole').hide();
        this.selectedWSISUser=null;
        this.getAllWSISUsers();
    }
    public addBtnClick(): void{
        this.isRegisterShowed = true;
        this.isEdit = false;
        this.showRegistrationForm();
    }
    
    public showRegistrationForm(): void {
        $(".user_page").attr("editor","show").removeClass("col-md-12").addClass("col-md-8");
        $(".edit-user").show();
        $("#add_user_btn").attr("disabled","disabled");
    }
    public showUpdateForm():void{
       
        $(".user_page").attr("editor","show").removeClass("col-md-12").addClass("col-md-8");
        $(".edit-user").show();
        $("#add_user_btn").attr("disabled","disabled");
    }
    public closeForm(evt: any){
        this.isRegisterShowed = false;
        this.isEdit = false;
        this.getAllUsers(1);
        $(".user_page").attr("editor","hide").removeClass("col-md-8").addClass("col-md-12");
        $(".edit-user").hide();
        $("#add_user_btn").removeAttr("disabled");
    }
    
    public closeDetailForm(evt:any){
        $('#user_detail').removeClass('in');
        $('#user_detail').hide();
    }
    
    
    public static navigateEditorOnAdd(): void{
        
    }
    
    public viewUserDetails(usr: UserModel):void{
        this.selectedUser = usr;
        $('#user_detail').addClass('in');
        $('#user_detail').show();
    }
    
    public showEditForm(usr: UserModel): void {
        this.selectedUser = usr;
        this.isRegisterShowed=false;
        this.isEdit=true;
        this.showUpdateForm();
    }
    
    public showEditRole(usr:WsisUser){
        this.selectedWSISUser=usr;
        $('#updateRole').addClass('in');
        $('#updateRole').show();
    }
    public deactivateUser(usr:UserModel):void{
        console.log(usr);
        this.userService.deactivateUser({username: usr.userName}).subscribe(res=>{
            if (res.errorCode!=null){
                this.toastr.error(res.message, 'Error');
            }
            else{
                this.toastr.success('Successfully, Deactiveted user ('+usr.fullName+')', 'User Deactivation');
                this.getAllUsers(1);
            }
        })
    }
    
    public activateUser(usr:UserModel):void{
        console.log(usr);
        this.userService.activateUser({username: usr.userName}).subscribe(res=>{
            if (res.errorCode!=null){
                this.toastr.error(res.message, 'Error');
            }
            else{
                this.toastr.success('Successfully, Activated User ('+usr.fullName+')', 'User Activation');
                this.getAllUsers(1);
            }
        })
    }
    
}