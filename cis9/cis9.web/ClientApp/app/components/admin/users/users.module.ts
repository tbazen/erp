﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {UsersComponent} from "./users.component";
import {UserDetailComponent} from "./details/user.detail.component";
import {RegisterComponent} from "./register/register.component";
import {UsersService} from "./users.service";
import {LoginComponent} from "./login/login.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {EditComponent} from "./edit/edit.component";
import {ResetpasswordComponent} from "./resetPassword/resetpassword.component";
import {NgxSpinnerModule} from "ngx-spinner";
import {EditRoleComponent} from "./edit/edit-role/edit-role.component";


@NgModule({
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        NgxSpinnerModule
        ],
    
    declarations: [
        UsersComponent,
        UserDetailComponent,
        RegisterComponent,
        LoginComponent,
        EditComponent,
        ResetpasswordComponent,
        EditRoleComponent
        ],
    providers: [UsersService],
    exports: [UsersComponent]
})
export class UsersModule{
    
}