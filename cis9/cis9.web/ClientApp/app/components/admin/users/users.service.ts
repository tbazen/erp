﻿import {ApiService} from "../../../services/api.service";
import {UserModel, UserQuery, WsisUser} from "./models/UserModel";
import {Injectable} from "@angular/core";
import {ResetPassowrd} from "../../../services/auth/auth.model";

@Injectable()
export class UsersService {
    
    public static ROLES = ["admin","clerk","Billing Supervisor","LA Expert","GIS Expert","LA Supervisor","TX User","Billing Clerk","Billing User","Data Encoder","PV Supervisor","PV User","Digitizer","DAMS Supervisor","DAMS User"];
    
    constructor(public apiService: ApiService){
        
    }
    public loadPage(){
        return this.apiService.get("account/pageload");
    }
    public getUsers(query: UserQuery){
        if (query.filters != null){
            return this.apiService.getWithHeaders(`user/getusers?filters=UserName|FullName@=*${query.filters}&page=1&pageSize=30`);
        } 
        else {
            return this.apiService.getWithHeaders(`user/getusers?page=${query.page}&pageSize=${query.pageSize}`)
        }
    }
    
    public getNonAdminUsers(){
        return this.apiService.get("user/getnonadminusers")
    }
    
    public editUser(user: UserModel){
        return this.apiService.post("user/update", user);
    }
    
   public deactivateUser(username:any){
        return this.apiService.post('user/deactivate', username);
   }
   public activateUser(username:any){
       return this.apiService.post('user/activate', username);
   }
   public resetPass(user:ResetPassowrd){
        return this.apiService.post('account/resetpassword', user);
   }
   
   public getWSISUsers(){
        return this.apiService.get("account/getwsisusers");
   }
   
   public updateRole(wsisUser:WsisUser){
        return this.apiService.post('account/updaterole',wsisUser);
   }
   public getCustomer(contractNo:any){
        return this.apiService.get(`account/getsubscriber?contractNo=${contractNo}`);
   }
   public getWSISEmployees(){
        return this.apiService.get("account/getwsisemployees");
   }
   public getEmployee(username:string){
        return this.apiService.get(`account/getemployee?username=${username}`);
   }
}