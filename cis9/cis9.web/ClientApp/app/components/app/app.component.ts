import { Component } from '@angular/core';
import {ToastrService} from "ngx-toastr";
import {ApiService, ErrorMsg} from "../../services/api.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
   
    public errData : {msg: '', title: ''};
    
    constructor(public toastr: ToastrService, public apiService: ApiService, public router: Router){
        ApiService.apiEvent.subscribe((errMsg: ErrorMsg) => {
            console.log(errMsg);
           this.toastr.error(errMsg.msg, errMsg.title);
           
           if (errMsg.status === 400){ //session expired head to login  page
               this.router.navigate(["/login"])
               
           }
        })
    }
}
