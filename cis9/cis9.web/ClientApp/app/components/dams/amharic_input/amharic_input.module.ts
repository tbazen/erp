﻿import {NgModule} from "@angular/core";
import {DamsheaderModule} from "../header/damsheader.module";
import {ReactiveFormsModule,FormsModule} from "@angular/forms";
import { CommonModule } from '@angular/common';  
import { ImageViewerModule } from 'ngx-image-viewer';
import {NgAutoCompleteModule} from "ng-auto-complete";
import {RouterModule} from "@angular/router";

import { AmharicInputComponent } from './amharic_input.component';


@NgModule({
    imports: [
        FormsModule,
        CommonModule
        ],
    
    declarations: [AmharicInputComponent],
    exports: [AmharicInputComponent],
    providers: []
})
export class AmharicInputModule{}