﻿import {Component, EventEmitter, OnInit, Output} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {DamsdashboardService} from "../dashboard/damsdashboard.service";
import {UsersService} from "../../admin/users/users.service";
import {ForwardCaseModel} from "./case.model";
import {UserModel} from "../../admin/users/models/UserModel";
import {ActivatedRoute, Router} from "@angular/router";
import {ViewfileComponent} from "../viewfile/viewfile.component";
import {CreateNewAutocompleteGroup, SelectedAutocompleteItem} from "ng-auto-complete";
import {File} from "../dashboard/damsdashboard.model";
import BaseComponent from "../../../base.component";
import {configs} from "../../../app.config";

declare var $:any;
declare var EthiopianDate:any;
declare var Image_Viewer:any;
declare var full_screen:any;
@Component({
    selector:"app-case-detail",
    templateUrl:'./case.component.html'
})
export class CaseComponent  extends BaseComponent implements OnInit{
    
    public docSelected:boolean = false;
    public selectedCase: any;
    public caseId:string;
    public forwardCaseForm:FormGroup;
    public forwardCaseModel:ForwardCaseModel;
    public images: string[];
    public ethDate: any;
    public showAutoComplete = false;
    public loggedinUser:string;
    public loggedInEmp:string;
    public assignedUser:string;

    public selectedFile: File;

    public users:UserModel[];
    public group: any;
    public workflowState: number;

    public config:any;
    public loadedIndex:number=0;
    public isImage:boolean=true;
    public isPDF:boolean=false;
    public isOther:boolean=false;
    public page:number=0;
    public wsisUser:any[];
    public noImage:boolean=false;
    public svg:any;
    public imagePage:number;
    public rotationAmount:number=0;

    constructor(public fb:FormBuilder, public toastr : ToastrService, public userService:UsersService, public ddService:DamsdashboardService, public activatedRoute: ActivatedRoute, public router: Router) {
        super();
        
        this.forwardCaseForm = fb.group({
            Description: ['', Validators.required]
        });
        
        this.activatedRoute.data.subscribe(res => {
            console.log("Selected Case");
            console.log(res);
            this.selectedCase = res.data;
            this.assignedUser=this.selectedCase.notes[this.selectedCase.notes.length-1].forwardTo;
        });
        
        this.ddService.getWorkflowState(this.selectedCase.caseModel.workflow).subscribe(res => {
            this.workflowState = res;
        })
    }
    
    ngOnInit():void{
        this.userService.loadPage().subscribe(res=>{
            this.config=res;
            this.getUsers();
        });
        
        this.loggedinUser=localStorage.getItem("username");
        this.caseId = this.activatedRoute.snapshot.params["id"];
        new Image_Viewer(document.getElementById('imageViewer'), document.getElementById('img_viewer_img'));
        if (this.selectedCase.caseModel.document !== null) {
            this.docSelected = true;
            this.ddService.getDocFile(this.selectedCase.caseModel.document.fileId).subscribe(file => {
                this.ddService.selectedFile.next(file);
            });
            this.images = ViewfileComponent.toBase64Str(this.selectedCase.caseModel.document.images);
            this.ethDate = ViewfileComponent.toEthDate(this.selectedCase.caseModel.document.date);
            $("#img_viewer_img").attr('href', " ");
            this.loadedIndex=0;
            this.isPDF=false;
            this.isOther=false;
            if(this.images.length > 0){
                let mimeType=this.images[0].split(';',1).pop();
                if(mimeType=="data:image/jpg" || mimeType=="data:image/png" || mimeType=="data:image/jpeg"){

                    this.isImage=true;
                    this.isPDF=false;
                    this.isOther=false;
                    this.noImage=false;
                    $(".pdf-viewer").hide();
                    $(".other-viewer").hide();
                    $(".no-image").hide();
                    this.svgPosition();
                    if(this.isImage==true){
                       
                        $("#img_viewer_img").attr('href', this.images[0]);
                        this.imagePage=parseInt(this.selectedCase.caseModel.document.images[0].page+"");
                    }

                }
                else if(mimeType=="data:application/pdf"){
                    this.isPDF=true;
                    this.isImage=false;
                    this.isOther=false;
                    this.noImage=false;
                    $(".pdf-viewer").show();
                    $(".other-viewer").hide();
                    $(".no-image").hide();
                    if(this.images.length > 0){
                        let overDuePath=`${configs.url}Workflow/ShowImageByDocId?docId=${this.selectedCase.caseModel.document.id}`;
                        $("#pdf-viewer").attr('src',overDuePath);
                      
                    }
                }
                else{
                    this.isOther=true;
                    this.isImage=false;
                    this.isPDF=false;
                    this.noImage=false;
                    $(".pdf-viewer").hide();
                    $(".other-viewer").show();
                    $(".no-image").hide()
                }

            }
            else {
                this.isImage=false;
                this.isPDF=false;
                this.isOther=false;
                this.noImage=true;
                $(".pdf-viewer").hide();
                $(".other-viewer").hide();
                $(".no-image").show();
            }
        }
        
        else{
            this.images = [];
            this.ethDate = "";
        }
        
        console.log("Component initialized");
        
        console.log(this.selectedCase);
        
        this.forwardCaseModel={
            caseWorkflow:this.selectedCase.caseModel.workflow,
            assignee:'',
            comment:'',
        };


        this.ddService.selectedFile$.takeUntil(this.destroyed$).subscribe(file => {
            this.selectedFile = file;
        });
        
        
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();

        this.ddService.selectedDocument.next(this.ddService.defaultDoc);
        this.ddService.selectedFile.next(this.ddService.defaultFile);
    }

    public getUsers():void{
        if(this.config.applicationFor=="cis") {
            this.userService.getNonAdminUsers().subscribe(res => {
                this.users = res;

                let items = [];
                for (let user of this.users) {
                    items.push({title: user.fullName, id: user.userName})
                }

                this.group = [CreateNewAutocompleteGroup('Search User to Assign', 'completer', items, {
                    titleKey: 'title',
                    childrenKey: null
                })];

                this.showAutoComplete = true;
            });
        }
        else if(this.config.applicationFor=="wsis"){
            this.userService.getWSISEmployees().subscribe(res => {
                this.wsisUser = res;

                let items = [];
                for (let user of this.wsisUser) {
                    items.push({title: user.fullName, id: user.userName})
                }

                this.group = [CreateNewAutocompleteGroup('Search User to Assign', 'completer', items, {
                    titleKey: 'title',
                    childrenKey: null
                })];

                this.showAutoComplete = true;
            });
        }
    }
    public userSelected(event: SelectedAutocompleteItem){
        this.forwardCaseModel.assignee = <string>event.item.id
    }
    
    public forwardCase(){
        this.ddService.forwardCase(this.forwardCaseModel).subscribe(res => {
            this.toastr.success(`Case Forwarded to ${this.forwardCaseModel.assignee}`, "Case Forward")
            
            this.router.navigate(['/dams/dashboard']);
        })
    }
    
    public closeCase(){
        this.ddService.closeCase({id: this.selectedCase.caseModel.workflow}).subscribe(res => {
            this.toastr.success(`Case Closed`, "");

            this.router.navigate(['/dams/dashboard']);
        })
    }

    public openFile():void {
        this.ddService.getFileById(this.selectedFile.id).subscribe(file => {
            this.ddService.selectedFile.next(file);
            this.router.navigate(['/dams/file/', this.selectedFile.id]);
        });

    }

    public downloadPdf(){
        const overRidePath=`${configs.url}Workflow/ShowImageByDocId?docId=${this.selectedCase.caseModel.document.id}`;
        $("#otherDocument").attr('src',overRidePath);
    }
    public next(): void{
        if(this.loadedIndex < this.images.length -1){
            this.loadedIndex++;
            this.page=this.loadedIndex;
            $("#img_viewer_img").attr('href', this.images[this.loadedIndex]);
            this.imagePage=parseInt(this.selectedCase.caseModel.document.images[this.loadedIndex].page+"");
        }
    }

    public previous(): void{
        if(this.loadedIndex > 0){
            this.loadedIndex--;
            this.page=this.loadedIndex;
            $("#img_viewer_img").attr('href', this.images[this.loadedIndex]);
            this.imagePage=parseInt(this.selectedCase.caseModel.document.images[this.loadedIndex].page+"");
        }

    }

    public svgPosition(){
        this.svg=$('svg')[0].getBoundingClientRect();
        $("#img_viewer_img").attr('x', "0");
        $("#img_viewer_img").attr('y', "0");
        $("#img_viewer_img").attr('height', this.svg.height.toString());
        $("#img_viewer_img").attr('width', this.svg.width.toString());
        this.rotationAmount=0;
        $("#img_viewer_img").css('transform','rotate('+this.rotationAmount+'deg)');
    }

    public fullScreen(){
        this.makeFull();
        setTimeout(()=>this.svgPosition(),2000);

    }
    public makeFull(){
        new full_screen();
    }
    
    public print(){
        const printContent=document.getElementById("printable").innerHTML;
        const originalContent=document.body.innerHTML;
        document.body.innerHTML = printContent;
        window.print();
        document.body.innerHTML = originalContent;
    }
    public getEmployee(){
        this.userService.getEmployee(this.loggedinUser).subscribe(res=>{
            this.loggedInEmp=res;
        })
    }
    public rotate(degree:any){

        let imgContainer=$('svg')[0].getBoundingClientRect();
        let image=$("#img_viewer_img");
        if(this.rotationAmount == 360 ||this.rotationAmount == -360)
            this.rotationAmount=0;
        this.rotationAmount +=parseInt(degree);
        image.css('transform','rotate('+this.rotationAmount+'deg)');
        image.attr('degree', this.rotationAmount);
        image.css('transform','translate('+imgContainer.width/2 +','+ imgContainer.height/2+')');
        image.css('transform-origin-x', imgContainer.width/2);
        image.css('transform-origin-y', imgContainer.height/2);
    }
}