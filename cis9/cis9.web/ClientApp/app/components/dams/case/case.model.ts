﻿import {Document, WorkItemNote} from "../dashboard/damsdashboard.model";

export interface WorkflowModel {
    id: string;
    type: number;
    currentState:number;
    workItems: WorkItem[];
}

export interface WorkItem{
    id: string;
    seqNo:number;
    workflow:string;
    data: any;
    assignedRole: number;
    type: number;
}


export interface CaseModel{
    Id:string;
    Title:string;
    CaseType:string;
    Workflow:string;
    Description:string;
    CaseNumber:number;
    Date:string;
    document: Document,
    CreatedBy:string;
}

export interface CaseDetail{
    Id:string;
    Title:string;
    CaseType:string;
    Workflow:string;
    Description:string;
    CaseNumber:number;
    Date:string;
    Document:string;
    CreatedBy:string;
}


export interface CaseType{
    id:number;
    name:string;
}

export class CaseVm {
    caseModel: CaseModel;
    notes: WorkItemNote[]
}

export interface OpenCaseModel {
    Case: CaseDetail;
    assignee: string;
}

export interface ForwardCaseModel {
   caseWorkflow:string;
   assignee:string;
   comment:string;
}
export interface caseForward{
    id:string;
    forwardTo:string;
    note:string;
}

export interface SearchCaseModel{
    caseNo:string;
    caseStatus:number;
    caseDate:string;
}
export interface SearchCaseResult {
    id:string;
    title:string;
    caseType:string;
    caseNumber:number;
    workflow:string;
    date:string;
    createdBy:string;
}