﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {DamsheaderModule} from "../header/damsheader.module";
import {ReactiveFormsModule} from "@angular/forms";
import {CaseRouteModule} from "./case.route.module";
import {CaseComponent} from "./case.component";
import {CreatecaseComponent} from "./createcase/createcase.component";

import { ImageViewerModule } from 'ngx-image-viewer';
import {NgAutoCompleteModule} from "ng-auto-complete";
import {CaseResolve} from "./case.resolve";
import { RouterModule } from "@angular/router";
import { AmharicInputModule } from "../amharic_input/amharic_input.module"
import {SearchCaseComponent} from "./searchCase/searchCase.component";

const config = {
    btnClass: 'default', // The CSS class(es) that will apply to the buttons
    zoomFactor: 0.1, // The amount that the scale will be increased by
    containerBackgroundColor: '#f9f6ff', // The color to use for the background. This can provided in hex, or rgb(a).
    wheelZoom: true, // If true, the mouse wheel can be used to zoom in
    allowFullscreen: true, // If true, the fullscreen button will be shown, allowing the user to entr fullscreen mode
    // allowKeyboardNavigation: true, // If true, the left / right arrow keys can be used for navigation
    btnIcons: { // The icon classes that will apply to the buttons. By default, font-awesome is used.
        zoomIn: 'fa fa-plus',
        zoomOut: 'fa fa-minus',
        rotateClockwise: 'fa fa-repeat',
        rotateCounterClockwise: 'fa fa-undo',
        next: 'fa fa-arrow-right',
        prev: 'fa fa-arrow-left',
        fullscreen: 'fa fa-arrows-alt',
    },
    btnShow: {
        // zoomIn: true,
        // zoomOut: true,
        rotateClockwise: true,
        rotateCounterClockwise: true,
        next: true,
        prev: true
    }
};

@NgModule({
    imports: [
        CommonModule,
        CaseRouteModule,
        ImageViewerModule.forRoot(config),
        ReactiveFormsModule,
        NgAutoCompleteModule,
        AmharicInputModule
        ],
    
    declarations: [CaseComponent, CreatecaseComponent],
    exports: [CaseComponent, CreatecaseComponent],
    
    providers: [
        CaseResolve
        ]
})
export class CaseModule{}