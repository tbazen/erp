﻿import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {CaseModel, CaseVm} from "./case.model";
import {DamsdashboardService} from "../dashboard/damsdashboard.service";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";

@Injectable()
export class CaseResolve implements Resolve<CaseVm>{

    constructor(public ddSerivce: DamsdashboardService){

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CaseVm>|Promise<CaseVm>|CaseVm{
        return this.ddSerivce.getCase(<string>route.paramMap.get("id"));
    }

}