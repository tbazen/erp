﻿import {Route, RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {CaseComponent} from "./case.component";
import {CaseResolve} from "./case.resolve";
import {DamsdashboardComponent} from "../dashboard/damsdashboard.component";
import {DamsheaderComponent} from "../header/damsheader.component";
import {DamssearchComponent} from "../search/damssearch.component";
import {ViewfileComponent} from "../viewfile/viewfile.component";


const caseRoute: Routes = [
    {
        path: '',
        component: DamsheaderComponent,
        children: [
            {path: 'case/:id', component: CaseComponent, resolve: {data: CaseResolve}}
        ]
    }

];

@NgModule({
    imports: [
        RouterModule.forChild(caseRoute)
    ],
    exports: [
        RouterModule
    ]
})
export class CaseRouteModule {
}