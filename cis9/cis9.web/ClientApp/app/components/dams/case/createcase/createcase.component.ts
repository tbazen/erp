﻿import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {Document} from "../../dashboard/damsdashboard.model";
import {CaseDetail, CaseModel, CaseType, OpenCaseModel} from "../case.model";
import {UserModel} from "../../../admin/users/models/UserModel";
import {UsersService} from "../../../admin/users/users.service";
import {DamsdashboardService} from "../../dashboard/damsdashboard.service";
import {CreateNewAutocompleteGroup, NgAutocompleteComponent, SelectedAutocompleteItem} from "ng-auto-complete";

declare var $:any;
@Component({
    selector:'app-create-case',
    templateUrl:'./createcase.component.html',
    styleUrls: ['./createcase.component.css']
})
export class CreatecaseComponent implements OnInit{

    @Output() public formClosed = new EventEmitter();
    @Input() public selDoc:Document | string;
    public createCaseForm:FormGroup;
    public docSelected:boolean=false;
    public caseModel:CaseDetail;
    public users:UserModel[];
    public caseTypes:CaseType[];
    public group : any;
    public config:any;
    
    
    constructor(public fb:FormBuilder, public toastr : ToastrService, public userService:UsersService, public ddService:DamsdashboardService){
        this.createCaseForm=fb.group({
            Title: ['', Validators.required],
            CaseType:['',Validators.required],
            Description:['']
        });

        this.ddService.getCaseTyps().subscribe(res =>{
            this.caseTypes=res;
        });
    }
    ngOnInit():void{
        this.userService.loadPage().subscribe(res=>{
            this.config=res;
        });
        /*this.ddService.selectedDocument.subscribe(doc=>{
            this.selDoc=doc;
           
        });*/
        
        this.caseModel = {
                Id:'',
                Title: '',
                CaseType: '',
                Workflow:'',
                Description:'',
                CaseNumber: 0,
                Date: '',
                Document:'',
                CreatedBy: ''
            };
        
    }
    public docAppend(){
        this.caseModel.Document = (typeof this.selDoc) === "string" ? <string>this.selDoc : (<Document>this.selDoc).id;
    }
    public closeForm(){
        this.formClosed.next();
    }
    public createCase(){
        this.docAppend();
        console.log(this.caseModel);
        this.ddService.openCase(this.caseModel).subscribe(res=>{
            this.closeForm();
            this.toastr.success("Successfully Create Case", "");
        } );
    }

    /*public getUsers():void{
        this.userService.getNonAdminUsers().subscribe(res =>{
            this.users=res;
            
            let items = [];
            for (let user of this.users){
                items.push({title: user.userName, id: user.userName})
            } 
            
            this.group = [
                CreateNewAutocompleteGroup(
                    'Search User to Assign',
                    'completer',
                    items,
                    {titleKey: 'title', childrenKey: null}
                )
            ];
            
            this.showAutoComplete = true;
        });
    }
    
    public userSelected(event: SelectedAutocompleteItem){
        this.caseModel.assignee = <string>event.item.id
    }*/
}