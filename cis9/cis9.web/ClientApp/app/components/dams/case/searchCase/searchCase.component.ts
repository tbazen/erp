import {Component, OnInit} from "@angular/core";
import {SearchCaseModel, SearchCaseResult} from "../case.model";
import {FormBuilder, FormGroup} from "@angular/forms";
import {DamsdashboardService} from "../../dashboard/damsdashboard.service";
import {Router} from "@angular/router";

@Component({
    selector:'app-search-case',
    templateUrl:'./searchCase.component.html'
})
export class SearchCaseComponent implements OnInit{
    public searchCaseModel:SearchCaseModel;
    public searchCaseForm:FormGroup;
    public searchCaseResult:SearchCaseResult[];
    
    constructor(public ddService:DamsdashboardService, public fb:FormBuilder, public router:Router){
        this.searchCaseForm=this.fb.group({
            caseNo:[""],
            caseStatus:[""],
            caseDate:[""]
        })
    }
    ngOnInit(){
        this.searchCaseModel={
           caseDate:'',
            caseStatus:0,
            caseNo:''
        }
    }
    public onSearchClicked(){
        this.ddService.searchCase(this.searchCaseModel).subscribe(res=>{
            this.searchCaseResult=res;
        })
    }
    
    public showCase(caseModel:any){
        this.router.navigate(['/case/', caseModel.workflow]);
    }
}