﻿import { AfterViewInit, Component, Input, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validator, Validators } from "@angular/forms";
import { File } from "../dashboard/damsdashboard.model";
import { DamsdashboardService } from "../dashboard/damsdashboard.service";
import { ToastrService } from "ngx-toastr";
import { ISubscription } from "rxjs/Subscription";
import {UsersService} from "../../admin/users/users.service";

declare var $: any;
@Component({
    selector: 'app-registor-file',
    templateUrl: './damsCreateFile.component.html'
})
export class DamsCreateFileComponent implements OnInit, OnDestroy {

    public workflowId: string;

    public fileRegForm: FormGroup;
    public fileModel: File;
    public subscription: ISubscription;
    public fileExists: boolean = false;
    public config:any;
    public upiText: string;
    public isCIS:boolean;
    public isWSIS:boolean;
    public customerLoaded:boolean=false;
    public validCustomerCode:any;
    public customerName:string;
    constructor(public fb: FormBuilder, public ddService: DamsdashboardService, public toastr: ToastrService, public userService:UsersService) {
        this.fileRegForm = fb.group({
            fileCode: ['', Validators.required],
            FileOwner: ['', Validators.required],
            Shelf: ['', Validators.required],
            Location: ['', Validators.required],
            Folder: ['', Validators.required],
            upin: ['', Validators.required],
            MobileNumber: [''],
            Remark: ['']
        })

    }

    public ngOnInit(): void {
            this.fileModel = this.ddService.defaultFile;
            this.userService.loadPage().subscribe(res=>{
                this.config=res;
            });
    
            this.subscription = this.ddService.selectedFile$.subscribe(res => {
                this.fileModel = res;
            });
    
            this.ddService.selectedWorkflow$.subscribe(res => {
                this.workflowId = res;
            });
    }

    public ngOnDestroy(): void {

        this.ddService.selectedFile.next(this.ddService.defaultFile);
    }

    public checkCustomer(customerCode:any){
        this.customerLoaded=true;
        this.userService.getCustomer(customerCode).subscribe(res=>{
            if(res==""){
               
                this.validCustomerCode=false;
                this.customerName="There is no customer with this customer code"
                this.fileRegForm.controls["FileOwner"].setValue("");
            }
            else{
                this.validCustomerCode=true;
                this.fileRegForm.controls["FileOwner"].setValue(res);
                this.customerName="";
            }
            this.customerLoaded=false;
        })
    }
    public registerFile(): void {
        console.log(this.fileModel);

        if (this.fileModel.workItem == "") {
            this.ddService.addArchiveFile(this.fileModel).subscribe(res => {
                if (res.errorCode != null) {
                    this.toastr.error(res.message, 'Error');
                }
                else {
                    this.toastr.success('Successfully Registered a New File (' + this.fileModel.fileCode + ')', "File Registration");
                    this.fileRegForm.reset();
                    this.closeForm();
                }
            });
        }
        else {

            this.ddService.checkFile(this.fileModel.id).subscribe(res => {
                if (res) {
                    this.ddService.updateFileInfo(this.fileModel).subscribe(res => {
                        if (res.errorCode != null) {
                            this.toastr.error(res.message, 'Error');
                        }
                        else {
                            this.toastr.success('Successfully Updated File Info (' + this.fileModel.fileCode + ')', "File Update");
                            this.closeForm();
                        }
                    })
                }
                else {
                    this.ddService.editArchiveFile({ workflow: this.workflowId, file: this.fileModel }).subscribe(res => {
                        if (res.errorCode != null) {
                            this.toastr.error(res.message, 'Error');
                        }
                        else {
                            this.toastr.success('Successfully Edited File (' + this.fileModel.fileCode + ')', "File Edit");
                            this.closeForm();
                            this.ddService.dashboardChanged.next(null);
                        }
                    });
                }
            });

        }
    }
    public validFileForm(): void {
        let valide = true;
        let requiredFilde;
        if(this.config.language=='am'){
            requiredFilde= ['fileCode', 'shelfNo', 'inshelfLocation', 'folderCode', 'upin', 'fileOwner input'];
        }
        else if (this.config.language=='en'){
            requiredFilde= ['fileCode', 'shelfNo', 'inshelfLocation', 'folderCode', 'upin', 'fileOwner'];
        }
       
        for (let i = 0; i < requiredFilde.length; i++) {
            let arrayList = $('#' + requiredFilde[i]).val();
            if (arrayList == "" || arrayList == null) {
                $('#' + requiredFilde[i]).removeClass('form-control');
                $('#' + requiredFilde[i]).addClass('form-error');
                valide = false;
            }
            else {
                $('#' + requiredFilde[i]).removeClass('form-error');
                $('#' + requiredFilde[i]).addClass('form-control');
            }
        }
        if(this.config.applicationFor=='cis'){
            if ($('#upin').val().length == 20 || $('#upin').val().length == 14) {
                $('#upin').removeClass('form-error');
                $('#upin').addClass('form-control');
                this.upiText = "";
            }
            else {
                $('#upin').removeClass('form-control');
                $('#upin').addClass('form-error');
                this.upiText = "The UPIN should be 14 or 20 digits long";
                valide = false;
            }
        }
        
        if (valide) {
            this.registerFile();
        }
    }
    public closeForm(): void {
        $('#regFile').removeClass('in');
        $('#regFile').hide();
        this.ngOnDestroy();
    }



}