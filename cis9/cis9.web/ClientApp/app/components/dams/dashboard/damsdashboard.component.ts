﻿import {Component, OnInit, ViewChild} from "@angular/core";
import {Router} from "@angular/router";
import {DamsdashboardService} from "./damsdashboard.service";
import {ToastrService} from "ngx-toastr";
import {
    ArchiveStat, File, RejectRequest, WorkflowModel, WorkflowModel as PendingTasks,
    WorkItemNote
} from "./damsdashboard.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {searchFile} from "../search/damssearch.model";
import {NgxSmartModalService} from "ngx-smart-modal";
import {FileRequest, FileRequestVm} from "../filerequest/filerequest.model";
import {DamsSearchService} from "../search/damssearch.service";
import { NgxSpinnerService } from 'ngx-spinner';
import {UsersService} from "../../admin/users/users.service";

declare var $: any;

@Component({
    templateUrl: './damsdashboard.component.html'

})
export class DamsdashboardComponent implements OnInit {

    public dashSearchForm: FormGroup;

    public userrole: number | null;
    public damsUser:boolean=false;
    public damsDigitizer:boolean=false;
    public damsSupervisor:boolean=false;
    public damsClerk:boolean=false;

    public archiveStat: ArchiveStat;
    public selectedWorkflow: WorkflowModel;
    public workflows: WorkflowModel[];
    public notes: WorkItemNote[] = [];
    public showNote: boolean = false;
    public workflowState: number;
    
    public showFile:boolean=false;
    public selectedFile:File;
    public fileEdit: boolean = false;
    public fileRejectForm:FormGroup;
    public rejectModel:RejectRequest;
    public workflowId:string;
    
    public fileRequestModel: FileRequestVm;
    public searchFileModel: searchFile;
    public config:any;
    
    constructor(public ddService: DamsdashboardService, public toastr: ToastrService, public router: Router, public fb: FormBuilder, public modalService: NgxSmartModalService, public searchService: DamsSearchService, public loadingService: NgxSpinnerService, public userService:UsersService) {
        this.dashSearchForm = fb.group({
            FileCode: ['', Validators],
            FileOwner: ['', Validators],
            upin: ['', Validators]
        });
        this.fileRejectForm=fb.group({
            note:['',Validators.required]
        });
       

    }
    ngOnInit() {

        this.userrole = JSON.parse(<string>localStorage.getItem("role"));
        this.userrole = +this.userrole;
        this.userService.loadPage().subscribe(res=>{
            this.config=res;
        });
        switch (this.userrole) {
            
            case 2:
                this.damsClerk = true;
                break;
            
            case 4:
                this.damsDigitizer = true;
                break;
            case 6:
                this.damsSupervisor = true;
                break;
            case 5:
                this.damsUser = true;
                break;
        }

        this.loadingService.show();
        
        this.ddService.getPendingTasks().subscribe(res => {
            this.workflows = res;
            this.loadingService.hide();
        });
        this.ddService.archiveStats().subscribe(res => {
            this.archiveStat = res;
        });
        
        this.ddService.dashboardChanged$.subscribe(res => {
            this.loadingService.show();
            this.ddService.getPendingTasks().subscribe(res => {
                this.workflows = res;
                this.loadingService.hide();
            });
        });
        
        this.rejectModel = {
            workItem:'',
            note: '',
            workflow: ''
        };

        this.archiveStat = {files: 0, docs: 0};

        this.fileRequestModel = {
            id: '',
            fileId: '',
            userName: '',
            description: '',
            workflowId: '',
            fileModel: null
        };

        this.searchFileModel = this.searchService.defaultFileSearchModel;
        
    }

    public onViewWorkflow(workflow: WorkflowModel): void {
        this.selectedWorkflow = workflow;
        //Checks if the workflow has a note
        
        
        if (this.selectedWorkflow.type.id == 2){
            this.router.navigate(['/case/', this.selectedWorkflow.id])
        }
        else if (workflow.type.id == 5){

            this.ddService.getFileRequestWorkflow(workflow.id).subscribe(res => {
                this.fileRequestModel = res;
                this.ddService.getWorkflowState(workflow.id).subscribe(state => {
                    this.workflowState = state;
                    this.modalService.getModal('fileRequest').open(true)
                });
            });
        }
        else {
            this.ddService.getWorklowNotes(workflow.id).subscribe(res => {
                if (res.length == 0){
                    this.viewWorkflow(workflow);
                }

                else{
                    this.notes = res;
                    this.showNote = true;
                    $('#workflowNote').show().addClass('in');
                }
            }); 
        }
    }
    
    public viewWorkflow(workflow: WorkflowModel):void{
        this.closeWorkNote();
        
        if (this.damsUser){
            
            this.ddService.getFile(workflow.id).subscribe(res => {
                this.ddService.selectedFile.next(res);
                this.ddService.selectedWorkflow.next(this.selectedWorkflow.id);
                $("#regFile").show();
                $('#regFile').addClass('in');
            })
        }
        
        else if (this.damsDigitizer || this.damsSupervisor){
            this.loadingService.show();
            if (workflow.type.id == 1) {
                this.ddService.getFile(workflow.id).subscribe(res => {
                    this.ddService.selectedFile.next(res);
                    this.loadingService.hide();
                    this.router.navigate(['/dams/workflow/file/', workflow.id]);
                })
            }
            else if (workflow.type.id == 3){
                this.ddService.getDocument(workflow.id).subscribe(res => {
                    this.ddService.selectedDocument.next(res);
                    this.router.navigate(['/dams/workflow/doc/', workflow.id])
                })
            }
            
            else if (workflow.type.id == 4){
                this.loadingService.show();
                this.ddService.getFile(workflow.id).subscribe(res => {
                    this.ddService.selectedFile.next(res);
                    this.loadingService.hide();
                    this.router.navigate(['/dams/workflow/file/', workflow.id]);
                });
            } 
        }
    }


    public dashSearch(): void {
        this.searchService.fileSearch.next(this.searchFileModel);
        this.router.navigate(['/dams/search/']);
    }
    
    public closeWorkNote():void{
        this.showNote = true;
        $('#workflowNote').hide().removeClass('in');
    }

    public showRejectNoteForm():void{
        $("#rejectNote").show().addClass('in');
    }

    public rejectChange(){
        if (this.damsSupervisor){
            this.ddService.rejectFileRequest(this.rejectModel).subscribe(res => {
                if (res.errorCode!=""){
                    this.toastr.error(res.messages, "Error");
                }
                else{
                    this.toastr.info("Change Rejected", "Info");
                    this.closeFileForm(Event);
                    this.closeForm(Event);
                    this.router.navigate(["/dams/dashboard"]);
                }
                
            });
        }
    }
    public approveChange() {
        this.ddService.approveRequest({id: this.workflowId}).subscribe(res => {
            if (res.errorCode!=""){
                this.toastr.error(res.messages, "Error");
            }
            else{
                this.toastr.success("Change Approved", "Success");
                this.closeFileForm(Event);
                this.router.navigate(["dams/dashboard"]);
            }
            
        })
    }

    /**
     * File Request action methods
     */
    public acceptRequestFile(frModel: FileRequest) {
        this.ddService.acceptFileRequest({id: frModel.workflowId}).subscribe(res => {
            this.toastr.success("Request Accept", "Physical File Request");
            this.modalService.getModal("fileRequest").close();
            this.workflows = this.workflows.filter(function (el) {return el.id !== frModel.workflowId});
        });
    }
    
    public failRequestFile(frModel: FileRequest) {
        this.ddService.failFileRequest({id: frModel.workflowId}).subscribe(res => {
            this.toastr.warning("Request Failed", "Physical File Request");
            this.modalService.getModal("fileRequest").close();
            this.workflows = this.workflows.filter(function (el) {return el.id !== frModel.workflowId});
        });
    }
    
    public rejectRequestFile(frModel: FileRequest) {
        this.ddService.rejectPhysicalFileRequest({id: frModel.workflowId}).subscribe(res => {
            this.toastr.warning("Request Rejected", "Physical File Request");
            this.modalService.getModal("fileRequest").close();
            this.workflows = this.workflows.filter(function (el) {return el.id !== frModel.workflowId});
        });
    }
    
    public checkoutFile(frModel: FileRequest) {
        this.ddService.checkoutFile({id: frModel.workflowId}).subscribe(res => {
            this.toastr.success("File Checked Out!", "Physical File Request");
            this.modalService.getModal("fileRequest").close();
            this.workflows = this.workflows.filter(function (el) {return el.id !== frModel.workflowId});
        });
    }

    public checkinFile(frModel: FileRequest) {
        this.ddService.checkinFile({id: frModel.workflowId}).subscribe(res => {
            this.toastr.success("File Checked In!", "Physical File Request");
            this.modalService.getModal("fileRequest").close();
            this.workflows = this.workflows.filter(function (el) {return el.id !== frModel.workflowId});
        });
    }
    
    public closeForm(evt:any):void{
        $("#rejectNote").removeClass("in");
        $("#rejectNote").hide();
    }
    public closeFileForm(evt:any):void{
        $("#fileInfo").removeClass("in");
        $("#fileInfo").hide();
    }
}