﻿export interface WorkflowModel {
    id: string;
    type: WorkflowType;
    currentState:number;
    title: string;
    timeStamp: string;
}

export interface WorkflowType {
    id: number,
    name:string,
    description: string
}

export interface WorkItem{
    id: string;
    seqNo:number 
    workflow:string;
    data: any;
    assignedRole: number;
    type: number;
}

export interface WorkItemNote{
    id: string;
    workitem:string;
    note:string;
    date:string;
    username:string;
}


export interface DocumentType{
    id: number;
    name:string;
}

export interface FileStat {
    numDocuments: number;
    numSheets: number;
}


export interface ArchiveStat{
    files: number,
    docs: number
}



export interface File {
    id: string;
    shelf: string;
    location: string;
    workItem: string;
    folder: string;
    fileCode: string;
    mobileNumber: string;
    upin:string;
    remark:string;
    fileOwner:string;
    documents: Document[];
}
export interface Document{
    id: string;
    sheets: number;
    referenceNo : string;
    fileId : string;
    workItem: string;
    type: DocumentType;
    timeStamp: number;
    date:Date;
    images: Image[];
}

export interface Image{
    id:string;
    data:string;
    workItem:string;
    description:string;
    documentId:string;
    page:number;
    mime: string;
}
export interface WorkflowNoteModel
{
    Id:string;
    Workflow:string;
    Username:string;
    Note:string;
    Date:string;
}

export interface RejectRequest{
    workItem:string;
    note:string;
    workflow:string;
}

