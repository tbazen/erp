﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {DamsdashboardComponent} from "./damsdashboard.component";
import {DamsdashboardService} from "./damsdashboard.service";
import {DamsdashboardRoute} from "./damsdashboard.route.module";
import {ReactiveFormsModule} from "@angular/forms";
import {ApplicationPipesModule} from "../../../pipes/pipes.module";
import {WorkflowResolve} from "./damsdashboard.resolve";
import {NgxSmartModalModule, NgxSmartModalService} from "ngx-smart-modal";
import {DamsheaderModule} from "../header/damsheader.module";
import {CaseModule} from "../case/case.module";
import {NgxSpinnerModule} from "ngx-spinner";
import { AmharicInputModule } from "../amharic_input/amharic_input.module"


@NgModule({
    imports: [
        CommonModule,
        DamsdashboardRoute,
        NgxSmartModalModule.forChild(),
        NgxSpinnerModule,
        ReactiveFormsModule,
        ApplicationPipesModule,
        DamsheaderModule,
        CaseModule,
        AmharicInputModule
    ],
    declarations:[DamsdashboardComponent],
    exports:[
        DamsdashboardComponent,
    ],
    providers: [
        DamsdashboardService,
        WorkflowResolve,
        NgxSmartModalService
    ]
})
export class DamsdashboardModule{
    
}