﻿import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {File} from "./damsdashboard.model";
import {Observable} from "rxjs/Observable";
import {DamsdashboardService} from "./damsdashboard.service";
import {Injectable} from "@angular/core";
import {CaseVm} from "../case/case.model";

@Injectable()
export class WorkflowResolve implements Resolve<File>{
    
    constructor(public ddSerivce: DamsdashboardService){
        
    }
    
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        return this.ddSerivce.getWorkflowState(<string>route.paramMap.get("workflow"));
    }
    fileResolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        return this.ddSerivce.getFileById(<string>route.paramMap.get("fileId"));
    }
    documentResolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        return this.ddSerivce.getWorklfowId(<string>route.paramMap.get("docId"));
    }
    
    viewDocResolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.ddSerivce.getFileById(<string>route.paramMap.get("docId"));
    }
}