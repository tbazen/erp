﻿import {RouterModule, Routes} from "@angular/router";
import {DamsdashboardComponent} from "./damsdashboard.component";
import {ViewfileComponent} from "../viewfile/viewfile.component";
import {DamsheaderComponent} from "../header/damsheader.component";
import {DamssearchComponent} from "../search/damssearch.component";
import {NgModule} from "@angular/core";
import {DamsGuard, NoAuthGuard} from "../../../services/auth.guard";
import {CaseComponent} from "../case/case.component";
import {CaseResolve} from "../case/case.resolve";
import {DamsCreateFileComponent} from "../createFile/damsCreateFile.component";
import {SingledocumenteditComponent} from "../singleDocumentEdit/singledocumentedit.component";
import {ReadonlyfileComponent} from "../viewfile/readonly/readonlyfile.component";
import {ReadonlydocumentComponent} from "../viewdocument/readonly/readonlydocument.component";
import {DocWorkflowComponent} from "../doc/doc.workflow.component";
import {SearchCaseComponent} from "../case/searchCase/searchCase.component";


const damsdashboardroute : Routes = [
    {
        path:'dams',
        component: DamsheaderComponent,
        children:[
            {path:'dashboard', component:DamsdashboardComponent},
            {path:'search', component:DamssearchComponent},
            {path: 'createFile', component: DamsCreateFileComponent},   
            {path:'workflow/file/:workflow',component:ViewfileComponent},
            {path:'workflow/doc/:workflow',component:DocWorkflowComponent},
            {path:'editDoc/:id',component:SingledocumenteditComponent},
            {path:'file/:fileId', component:ReadonlyfileComponent},
            {path:'document/:docId', component:ReadonlydocumentComponent},
            {path:'searchCase', component:SearchCaseComponent},
            {path:'', pathMatch: 'full', redirectTo: 'dashboard'}
           
        ],
        
        canActivate: [NoAuthGuard]
    }
    
    ];

@NgModule({
    imports: [
        RouterModule.forChild(damsdashboardroute)
    ],
    exports: [
        RouterModule
    ]
})
export class DamsdashboardRoute {

}