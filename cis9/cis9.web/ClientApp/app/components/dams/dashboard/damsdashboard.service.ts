﻿import {Injectable} from "@angular/core";
import {ApiService} from "../../../services/api.service";
import {Document, File, Image, RejectRequest} from "./damsdashboard.model";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {CaseDetail, ForwardCaseModel} from "../case/case.model";
import {UpdatePassowrd} from "../../../services/auth/auth.model";
import {FileRequest} from "../filerequest/filerequest.model";

@Injectable()
export class DamsdashboardService{

    
    //default values for file, image and doc
    public defaultFile = {
        id:'',
        shelf: '',
        location: '',
        workItem: '',
        folder: '',
        fileCode: '',
        mobileNumber: '',
        upin:'',
        remark:'',
        fileOwner:'',
        documents: []
    };
    public defaultDoc = { id: '',
        sheets: 0,
        referenceNo: '',
        workItem: '',
        fileId: '',
        type: {
            id: 0,
            name: ''
        },
        timeStamp: 0,
        date: null,
        images: []};
    public defaultImg = { id: '',
        data: '',
        description: '',
        documentId: '',
        workItem: '',
        page: -1,
        mime: ''};
    
    
    public workflowState = new BehaviorSubject<number>(0);
    public workflowState$ = this.workflowState.asObservable();
    
    public selectedWorkflow = new BehaviorSubject<string>('');
    public selectedWorkflow$ = this.selectedWorkflow.asObservable();
    
    public dashboardChanged = new BehaviorSubject<any>(null);
    public dashboardChanged$ = this.dashboardChanged.asObservable();
    
    public selectedFile = new BehaviorSubject<File>(this.defaultFile);
    public selectedFile$ = this.selectedFile.asObservable();
    public selectedDocument=new BehaviorSubject<Document>(this.defaultDoc);
    public selectedDocument$ = this.selectedDocument.asObservable();
    public selectedImage = new BehaviorSubject<string[]>([]);
    public selectedImage$ = this.selectedImage.asObservable();
    
    public docSelected = new BehaviorSubject<boolean>(false);
    public docSelected$ = this.docSelected.asObservable();

    public selectedEditDoc=new BehaviorSubject<Document>(this.defaultDoc);
    public selectedEditDoc$=this.selectedEditDoc.asObservable();
    
    public selectedEditImg=new BehaviorSubject<Image>(this.defaultImg);
    public selectedEditImg$=this.selectedEditImg.asObservable();
   
    
    
    constructor(public apiService: ApiService){}
    
    public archiveStats(){
        return this.apiService.get("workflow/getarchivestat")
    }

    public addArchiveFile(archivefile:File){
        return this.apiService.post("damsuser/addfile",archivefile);
    }

    public checkFile(fileId:string){
        return this.apiService.get(`workflow/checkfile?id=${fileId}`);
    }

    // public getFilebyWorkItem(workItem:string,  fileId: string){
    //     return this.apiService.get(`workflow/getfile?workItem=${workItem}&fileId=${fileId}`);
    // }
    public getFile(id:string){
        return this.apiService.get(`workflow/getfileworkflow?id=${id}`);
    }

    public getDocument(id: string) {
        return this.apiService.get(`workflow/getdocumentworkflow?id=${id}`)
    }

    public getFileById(fileId: string){
        return this.apiService.get(`workflow/getfilebyid?fileId=${fileId}`);
    }
    
    public getDocumentById(docId: string){
        return this.apiService.get(`workflow/getdocument?id=${docId}`)
    }


    public getPendingTasks(){
        return this.apiService.get('workflow/getworkflows');
    }
    
    public getDocFile(fileId: string) {
        return this.apiService.get(`workflow/getdocfile?fileId=${fileId}`);
    }
    
    public getDocImages(workflow:string , docId:string){
        return this.apiService.get(`workflow/getdocimages?workflow=${workflow}&docId=${docId}`);
    }
    
    public getWorklowNotes(id:string){
        return this.apiService.get(`workflow/getworkflowNotes?id=${id}`)
    }
    
    public getWorkflowState(workflow: string){
        return this.apiService.get(`workflow/getworkflowstate?workflow=${workflow}`)
    }
    
    public getWorkflowType(workflow: string){
        return this.apiService.get(`workflow/getworkflowtype?workflow=${workflow}`)
    }
    
    public getWorkItemType(id:string){
        return this.apiService.get(`workflow/getworkitemtype?workitem=${id}`)
    }
    
    public getDocumentTypes(){
        return this.apiService.get('lookup/documenttype');
    }

    public getCaseTyps(){
        return this.apiService.get('lookup/casetype');
    }
    public addDocument(doc: any){
        return this.apiService.post('digitizer/adddocument', doc);
    }
    
    public editDocument(doc:any){
        return this.apiService.post('digitizer/updatedocument', doc);
    }
    public deleteDoc(doc: any){
        return this.apiService.post('digitizer/removedocument', doc);
    }

    public editImage(img:any){
        return this.apiService.post('digitizer/updateimage', img);
    }
    public deleteImg(img: any){
        return this.apiService.post('digitizer/removeimage', img);
    }
    public getPendingCase(){
        return this.apiService.get('case/getcases');
    }
    
    public getCasebyDoc(docId: string){
        return this.apiService.get(`case/getcasebydoc?docId=${docId}`)
    }
    
    public forwardCase(model: ForwardCaseModel){
        return this.apiService.post('case/forwardcase', model)
    }

    public openCase(cases: CaseDetail){
        return this.apiService.post('case/opencase', cases);
    }

    public closeCase(model: any) {
        return this.apiService.post('case/closecase', model);
    }
    
    
    public fileUpdateDoc(doc: Document){
        return this.apiService.post('digitizer/fileupdatedoc', doc);
    }
    
    public requestFileApproval(workflow: any){
        return this.apiService.post('digitizer/requestfileapproval', workflow);
    }
    
    public requestDocApproval(workflow: any){
        return this.apiService.post('digitizer/requestdocapproval', workflow)
    }
    
    public approveRequest(workflow: any){
        return this.apiService.post('damssupervisor/approverequest', workflow);
    }
    
    public rejectFileRequest(workflow: any){
        return this.apiService.post('damssupervisor/rejectfilerequest', workflow);
    }
    
    public rejectDocRequest(workflow:any){
        return this.apiService.post('digitizer/rejectdocrequest', workflow)
    }
    
    public deleteFile(file: File){
        return this.apiService.post('damsuser/deletefile',file);
    }
    public getCase(id:string){
        return this.apiService.get(`case/getcase?id=${id}`);
    }
    public changePass(changeP: UpdatePassowrd){
        return this.apiService.post('account/changepassword',changeP);
    }

    public getWorklfowId(workItem: string) {
        return this.apiService.get(`workflow/getId?id=${workItem}`)
    }

    public rejectFile(rejectModel: RejectRequest) {
        return this.apiService.post(`digitizer/rejectfile`, rejectModel);
    }

    public editArchiveFile(model: any) {
        return this.apiService.post(`damsuser/editfile`,model);
    }


    public updateFileInfo(fileModel: File) {
        return this.apiService.post(`damsuser/updatefileinfo`, fileModel);
    }
    
    public fileUpdateAddDoc(fileModel: File){
        return this.apiService.post('digitizer/fileUpdateAddDoc', fileModel);
    }
    
    public cancelChange(workflow: any){
        return this.apiService.post('damssupervisor/cancelrequest', workflow);
    }

    /**
     * 
     * @param {FileRequest} fileRequestModel
     * @returns {Observable<any>}
     */

    public requestFile(fileRequestModel: FileRequest) {
        return this.apiService.post('filerequest/requestfile', fileRequestModel);
    }

    /**
     * 
     * @param {string} id
     * @returns {Observable<any>}
     */
    
    public getFileRequestWorkflow(id:string) {
        return this.apiService.get(`filerequest/getfilerequest?id=${id}`)
    }
    
    public acceptFileRequest(workflow: any) {
        return this.apiService.post('filerequest/acceptfilerequest', workflow)
    }
    
    public rejectPhysicalFileRequest(workflow:any) {
        return this.apiService.post('filerequest/rejectfilerequest', workflow)
    }

    public failFileRequest(workflow:any) {
        return this.apiService.post('filerequest/failfilerequest', workflow)
    }

    public checkoutFile(workflow:any) {
        return this.apiService.post('filerequest/checkoutfilerequest', workflow)
    }
    
    public checkinFile(workflow:any){
        return this.apiService.post('filerequest/checkinfilerequest', workflow)
    }

    getFileStat(id: string, workflow: string) {
        return this.apiService.get(`workflow/getfilestat?id=${id}&workflow=${workflow}`);
    }
    
    public searchCase(searchModel:any){
        return this.apiService.post('workflow/searchcase', searchModel);
    }
   
}