﻿import {Component, OnInit, ViewChild} from "@angular/core";
import {CustomEvent, ImageViewerComponent} from "ngx-image-viewer";
import {Document, Image} from "../dashboard/damsdashboard.model";
import {ActivatedRoute, Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {DamsdashboardService} from "../dashboard/damsdashboard.service";
import { ReadonlydocumentComponent } from "../viewdocument/readonly/readonlydocument.component";
import { configs } from "../../../app.config";

declare var $: any;
declare var EthiopianDate: any;
declare var Image_Viewer: any;
declare var full_screen: any;

@Component({
    selector: 'app-doc-workflow',
    templateUrl: './doc.workflow.component.html'
})
export class DocWorkflowComponent implements OnInit {
    public docSelected: boolean = false;
    public selectedDoc: Document;
    public selectedImage: string[] = [];
    public page: number = 0;
    public totalPages: number;
    public selectedEtDate: any;
    public workflowId: string;
    public fileId: string;
    public workflowState: number;
    public config: any;
    public loadedIndex: number = 0;
    public isImage: boolean = true;
    public isPDF: boolean = false;
    public isOther: boolean = false;
    public noImage: boolean = false;
    public svg: any;
    public rotationAmount:number=0;


    @ViewChild(ImageViewerComponent) imageViewer: ImageViewerComponent;

    constructor(public ddService: DamsdashboardService, public activatedRoute: ActivatedRoute, public toastr: ToastrService, public router: Router) {

        this.selectedDoc = this.ddService.defaultDoc;
        this.selectedImage = [];
        this.docSelected = false;

    }

    ngOnInit(): void {
        new Image_Viewer(document.getElementById('imageViewer'), document.getElementById('img_viewer_img'));
        this.docSelected = false;

        this.workflowId = this.activatedRoute.snapshot.params["workflow"];

        this.ddService.getWorkflowState(this.workflowId).subscribe(res => {
            this.ddService.workflowState.next(res);
        });


        this.ddService.selectedDocument$.subscribe(doc => {
            this.selectedDoc = doc;
            this.page = 1;
            this.totalPages = this.selectedDoc.images.length;
            this.selectedImage = ReadonlydocumentComponent.toBase64Str(this.selectedDoc.images)

        });

        this.ddService.selectedImage.subscribe(img => {
            this.selectedImage = img
            $("#img_viewer_img").attr('href', " ");

            this.loadedIndex = 0;
            this.isPDF = false;
            this.isOther = false;
            this.isImage = false;
            this.noImage = false;
            this.page = 0;
            if (this.selectedImage.length > 0) {

                let mimeType = this.selectedImage[0].split(';', 1).pop();
                if (mimeType == "data:image/jpg" || mimeType == "data:image/png" || mimeType == "data:image/jpeg") {

                    this.isImage = true;
                    this.isPDF = false;
                    this.isOther = false;
                    this.noImage = false;
                    $(".pdf-viewer").hide();
                    $(".other-viewer").hide();
                    $(".no-image").hide();
                    this.svgPosition();
                    $("#img_viewer_img").attr('href', this.selectedImage[0]);


                }
                else if (mimeType == "data:application/pdf") {
                    this.isPDF = true;
                    this.isImage = false;
                    this.isOther = false;
                    this.noImage = false;
                    $(".pdf-viewer").show();
                    $(".other-viewer").hide();
                    $(".no-image").hide();
                    if (this.selectedDoc.images.length > 0) {
                        let overDuePath = `${configs.url}Workflow/ShowImageByDocId?docId=${this.selectedDoc.id}`;
                        $("#pdf-viewer").attr('src', overDuePath);
                    }
                }
                else {
                    this.isOther = true;
                    this.isImage = false;
                    this.isPDF = false;
                    this.noImage = false;
                    $(".pdf-viewer").hide();
                    $(".other-viewer").show();
                    $(".no-image").hide()
                }

            }
            else {
                this.isImage = false;
                this.isPDF = false;
                this.isOther = false;
                this.noImage = true;
                $(".pdf-viewer").hide();
                $(".other-viewer").hide();
                $(".no-image").show();
            }
        });

        this.ddService.workflowState$.subscribe(ws => {
            this.workflowState = ws
        });


        if (this.selectedDoc.workItem == "") { //a page reload occurred!
            this.ddService.getDocument(this.workflowId).subscribe(res => {
                this.ddService.selectedDocument.next(<Document>res);
            });
        }
    }

    public nextPage() {
        if (this.imageViewer != null) {
            this.imageViewer.index = this.page;
            this.page++;
        }
    }


    public prevPage() {
        if (this.page > 0) {
            this.page--;
            if (this.imageViewer != null) {
                this.imageViewer.index = this.page;
            }
        }
    }

    public static toEthDate(d: any): any {
        let date = new Date(d);
        let selectedEtDate = EthiopianDate.prototype.dateToEthiopian(date);
        return selectedEtDate.day + '/' + (selectedEtDate.month + 1) + '/' + selectedEtDate.year;
    }

    public handleEvent(event: CustomEvent) {
        if (event.name == 'delete') {

            let img = this.selectedDoc.images[event.imageIndex];
            this.ddService.deleteImg({workflow: this.workflowId, image: img}).subscribe(res => {
                let index = this.selectedDoc.images.findIndex(i => i.id == img.id);
                this.selectedDoc.images.splice(index, 1);
                let images = this.ddService.selectedImage.getValue();
                images.splice(event.imageIndex, 1);
                this.imageViewer.prevImage(event);
                this.page = this.imageViewer.index + 1;
                this.ddService.selectedImage.next(images);
                this.toastr.warning('Successfully Deleted the image');
            });
        }

    }

    public approveDoc() {
        this.ddService.approveRequest({id: this.workflowId}).subscribe(res => {
            if (res.errorCode != null) {
                this.toastr.error(res.message, 'Update Document');
            } else {
                this.toastr.success('Successfully Updated Document', 'Update Document');
                this.router.navigate(['/dams/dashboard'])
            }
        });
    }
    
    public cancelDoc(){
        this.ddService.cancelChange({id: this.workflowId}).subscribe(res => {
            if (res.errorCode != null) {
                this.toastr.error(res.message, 'Update Document');
            } else {
                this.toastr.success('Successfully Update Document', 'Update Document');
                this.router.navigate(['/dams/dashboard'])
            }
        });
    }
    

    public indexChanged() {
        console.log("Current Page: " + this.page);
    }
    public downloadPdf() {
        const overRidePath = `${configs.url}Workflow/ShowImage?workflowId=${this.workflowId}&docId=${this.selectedDoc.id}`;
        $("#otherDocument").attr('src', overRidePath);
    }

    public svgPosition() {
        this.svg = $('svg')[0].getBoundingClientRect();
        $("#img_viewer_img").attr('x', "0");
        $("#img_viewer_img").attr('y', "0");
        $("#img_viewer_img").attr('height', this.svg.height.toString());
        $("#img_viewer_img").attr('width', this.svg.width.toString());
        this.rotationAmount=0;
        $("#img_viewer_img").css('transform','rotate('+this.rotationAmount+'deg)');
    }
    public fullScreen() {
        this.makeFull();
        setTimeout(() => this.svgPosition(), 2000);

    }
    public makeFull() {
        new full_screen();
    }

    public next(): void {
        if (this.loadedIndex < this.selectedImage.length - 1) {
            this.loadedIndex++;
            this.page = this.loadedIndex;
            this.svgPosition();
            $("#img_viewer_img").attr('href', this.selectedImage[this.loadedIndex]);
        }
    }

    public previous(): void {
        if (this.loadedIndex > 0) {
            this.loadedIndex--;
            this.page = this.loadedIndex;
            this.svgPosition();
            $("#img_viewer_img").attr('href', this.selectedImage[this.loadedIndex]);
        }

    }
    public rotate(degree:any){

        let imgContainer=$('svg')[0].getBoundingClientRect();
        let image=$("#img_viewer_img");
        if(this.rotationAmount == 360 ||this.rotationAmount == -360)
            this.rotationAmount=0;
        this.rotationAmount +=parseInt(degree);
        image.css('transform','rotate('+this.rotationAmount+'deg)');
        image.attr('degree', this.rotationAmount);
        image.css('transform','translate('+imgContainer.width/2 +','+ imgContainer.height/2+')');
        image.css('transform-origin-x', imgContainer.width/2);
        image.css('transform-origin-y', imgContainer.height/2);
    }


}