import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {DamsdashboardService} from "../../dashboard/damsdashboard.service";
import {Document, File, Image} from "../../dashboard/damsdashboard.model";
import {ToastrService} from "ngx-toastr";
import {ViewfileComponent} from "../../viewfile/viewfile.component";
declare var $: any;
declare var Image_Viewer:any;
@Component({
    selector:'app-image-reorder',
    templateUrl:'./image_reordering.component.html'
})
export class Image_reorderingComponent implements OnInit{
    public selectedImage: string[]=[];
    public selectedEditDoc:Document;
    public images:any[]=[];
    public reorderImages:any[]=[];
    public image:any;
    public loadedIndex:number=0;
    public page:number=0;
    public svg:any;
    @Output() formClose = new EventEmitter();
    @Input()
    public workflow: string;
    @Input()
    public file: File;
    public docModel:Document;
    
    constructor(public ddService:DamsdashboardService, public toast:ToastrService){
        this.selectedEditDoc=this.ddService.defaultDoc;
    }
    ngOnInit(){
        this.image={
            id: '',
            data: '',
            description: '',
            documentId: '',
            workItem: '',
            page: 1,
            mime: '',
            img:'',
            page_label:''
        };
        new Image_Viewer(document.getElementById('imageViewer2'), document.getElementById('img_viewer_img2'));
        this.ddService.selectedEditDoc$.subscribe(doc => {
            this.selectedEditDoc = doc;
            for(let img of doc.images){
                this.image.img='data:' + img.mime + ';base64,' + img.data;
                this.image.id=img.id;
                this.image.mime=img.mime;
                this.image.data=img.data;
                this.image.description=img.description;
                this.image.documentId=img.documentId;
                this.image.workItem=img.workItem;
                this.image.page=img.page;
                let pn=parseInt(img.page+"");
                this.image.page_label=pn%2==0?pn/2+ " Back":((pn+1)/2)+" Front";
                
                this.images.push(this.image);
                this.image={
                    id: '',
                    data: '',
                    description: '',
                    documentId: '',
                    workItem: '',
                    page: 1,
                    mime: '',
                    img:'',
                    page_label:''
                };
            }
            this.ddService.selectedDocument.next(doc)
        });
        this.ddService.selectedImage$.subscribe(img => {
            this.selectedImage = img;
            this.svgPosition();
            $("#img_viewer_img2").attr('href', this.selectedImage[this.loadedIndex]);
        });
        
       this.docModel={
           id: this.selectedEditDoc.id,
           sheets:this.selectedEditDoc.sheets,
           referenceNo: this.selectedEditDoc.referenceNo,
           workItem: this.selectedEditDoc.workItem,
           fileId: this.file.id,
           type: this.selectedEditDoc.type,
           timeStamp: 0,
           date:this.selectedEditDoc.date,
           images: this.selectedEditDoc.images
       }
    }

    public svgPosition(){
        this.svg=$('#imageViewer2')[0].getBoundingClientRect();
        $("#img_viewer_img2").attr('x', "0");
        $("#img_viewer_img2").attr('y', "0");
        $("#img_viewer_img2").attr('height', this.svg.height.toString());
        $("#img_viewer_img2").attr('width', this.svg.width.toString());
    }

    public next(): void{
        if(this.loadedIndex < this.selectedImage.length -1){
            this.loadedIndex++;
            this.page=this.loadedIndex;
            this.svgPosition();
            $("#img_viewer_img2").attr('href', this.images[this.loadedIndex].img);
        }
    }

    public previous(): void{
        if(this.loadedIndex > 0){
            this.loadedIndex--;
            this.page=this.loadedIndex;
            this.svgPosition();
            $("#img_viewer_img2").attr('href', this.images[this.loadedIndex].img);
        }

    }
    
    public closeReorderForm():void{
        this.formClose.next();
    }
    
    public selectImage(index:number){
        this.loadedIndex=index;
        this.page=index;
        this.svgPosition();
        $("#img_viewer_img2").attr('href', this.images[this.loadedIndex].img);
    }
    public up(index:number){
        if(index > 0){
            let upImage=this.images[index].page;
            let downImage=this.images[index-1].page;
            this.images[index].page=downImage;
            this.images[index -1].page=upImage;
            
        this.ddService.editImage({workflow: this.workflow, image:[this.images[index],this.images[index-1]]}).subscribe(res=>{
            if(res.errorCode!=null){
                this.toast.error(res.message, "Update Image")
            }else{
              this.ddService.getDocImages(this.workflow, this.selectedEditDoc.id).subscribe(res=>{
                  this.images=[];
                  this.docModel.images=res;
                  this.ddService.selectedEditDoc.next(this.docModel);
                  this.loadedIndex=index-1;
                  this.page=index-1;
                  this.ddService.selectedImage.next(ViewfileComponent.toBase64Str(this.docModel.images));
                 
              })
            }
        })
        }
    }
    
    public down(index:number){
        if(index < this.images.length-1){
        
            let upImage=this.images[index+1].page;
            let downImage=this.images[index].page;
            this.images[index].page=upImage;
            this.images[index+1].page=downImage;
            this.ddService.editImage({workflow: this.workflow, image:[this.images[index],this.images[index+1]]}).subscribe(res=>{
                if(res.errorCode!=null){
                    this.toast.error(res.message, "Update Image")
                }else{
                    this.ddService.getDocImages(this.workflow, this.selectedEditDoc.id).subscribe(res=>{
                        this.images=[];
                        this.docModel.images=res;
                        this.ddService.selectedEditDoc.next(this.docModel);
                        this.loadedIndex=index+1;
                        this.page=index+1;
                        this.ddService.selectedImage.next(ViewfileComponent.toBase64Str(this.docModel.images));

                    })
                }
            })
     }
    }
    
    public flip(index:number){
        const imgPage=parseInt(this.images[index].page);
        let isExist=true;
        let pageNo=0;
        
        if(imgPage%2==1){
            pageNo=imgPage+1;
        }
        else{
            pageNo=imgPage-1;
        }
        
        for (let img of this.images){
            if(img.page== pageNo){
                isExist=false;
            }
        }
        if(isExist==true){
            this.images[index].page=pageNo;
            this.ddService.editImage({workflow: this.workflow, image:[this.images[index]]}).subscribe(res=>{
                if(res.errorCode!=null){
                    this.toast.error(res.message, "Update Image")
                }else{
                    this.ddService.getDocImages(this.workflow, this.selectedEditDoc.id).subscribe(res=>{
                        this.images=[];
                        this.docModel.images=res;
                        this.ddService.selectedEditDoc.next(this.docModel);
                        this.loadedIndex=index;
                        this.page=index;
                        this.ddService.selectedImage.next(ViewfileComponent.toBase64Str(this.docModel.images));

                    })
                }
            })
        }
        else{
            this.toast.error("This is the page exist to flip", "Flip Image Error");
        }
       
    }
    
}