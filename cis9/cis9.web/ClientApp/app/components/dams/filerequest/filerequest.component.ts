﻿import {Component, EventEmitter, OnDestroy, OnInit, Output} from "@angular/core";
import {FileRequest} from "./filerequest.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {DamsdashboardService} from "../dashboard/damsdashboard.service";


@Component({
    selector: 'app-file-request',
    templateUrl: './filerequest.component.html'
})
export class FileRequestComponent{
    
    @Output() public formClosed = new EventEmitter();
    
    public fileRequestModel: FileRequest;
    public fileRequestForm:FormGroup;
    
    
    constructor(public fb: FormBuilder, public toastr: ToastrService, public ddService: DamsdashboardService) {
        
        this.fileRequestForm = this.fb.group({
            Description: ['', Validators.required]
        });
        
        this.fileRequestModel = {
            id: '',
            fileId: '',
            userName: '',
            workflowId: '',
            description: ''
        }
        
    }
    
    public requestFile() {
        this.ddService.requestFile(this.fileRequestModel).subscribe(res => {
            this.toastr.success("File Request Sent");
            this.formClosed.next()
        })
    }


}
