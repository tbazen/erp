﻿import {File} from "../dashboard/damsdashboard.model";

export interface FileRequest {
    id: string,
    fileId: string,
    userName:string, 
    workflowId:string;
    description:string
}


export interface FileRequestVm extends FileRequest {
    fileModel: File
}