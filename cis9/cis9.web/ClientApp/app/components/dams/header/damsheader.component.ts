﻿import {Component, OnInit, AfterViewInit, Input} from "@angular/core";
import {AuthService} from "../../../services/auth/auth.service";
import {Router} from "@angular/router";
import {File} from "../dashboard/damsdashboard.model";
import {DamsdashboardService} from "../dashboard/damsdashboard.service";
import {ISubscription} from "rxjs/Subscription";
import {NgxSmartModalService} from "ngx-smart-modal";
import {UsersService} from "../../admin/users/users.service";

declare var $:any;
@Component({
    selector:'app-main',
    templateUrl:'./damsheader.component.html'
})

export class DamsheaderComponent implements OnInit, AfterViewInit {
    public storage: WindowLocalStorage;

    @Input() public file:File;
    
    public username: string | null;
    public menuShowed: boolean = false;
    public userrole: number | null;
    public userrole_name: string | null;
    public damsUser:boolean=false;
    public damsDigitizer:boolean=false;
    public damsSupervisor:boolean=false;
    
    public showCase:boolean = false;
    public config:any;
    
    constructor(public authService: AuthService,  public userService:UsersService, public router: Router, public ddService: DamsdashboardService, public modalService:NgxSmartModalService) {
    }
    
    ngOnInit(): void {
        this.username = localStorage.getItem("username");
        this.userrole=JSON.parse(<string>localStorage.getItem("role"));
        if (this.userrole != null) this.userrole = +this.userrole;
        this.userrole_name = <string>localStorage.getItem("role_name");
        this.userService.loadPage().subscribe(res=>{
            this.config=res;
        })

        this.sideMenuNav();
        switch (this.userrole) {
            case 4:
                this.damsDigitizer = true;
                break;
            case 6:
                this.damsSupervisor = true;
                break;
            case 5:
                this.damsUser = true;
                break;
        }
        
        
    }
    ngAfterViewInit():void{
        
    }
    public logOut(): void {
        this.authService.logout();
    }

    public showdropDown(){
        this.menuShowed = !this.menuShowed;
        if(this.menuShowed){
            $(".header-menu").show()
        }
        else{
            $(".header-menu").hide()
        }
    }
    public sideMenuNav():void{
        $(".dams-side-menu").find('a').on('click', function(ev:any) {
            console.log('clicked - sidebar_menu');
            let target=ev.currentTarget;
            let $li = $(target).parent();
                $(".dams-side-menu").find('li').removeClass('active');
                $li.addClass('active');
        });
    }
    
    
    public openForm(){
        $("#regFile").show();
        $('#regFile').addClass('in');
    }
    
   public changePassword(){
       $("#updatePass").show();
       $('#updatePass').addClass('in');
   }
   
   public openCaseForm() {
        this.showCase = true;
        this.modalService.getModal("caseModal").open();
   }

   public openRequestForm() {
       this.modalService.getModal("requestModal").open();
   }
   
    public closeCaseModal() {
        this.modalService.getModal("caseModal").close();
    }
    
    public closeRequestModal() {
        this.modalService.getModal("requestModal").close();
    }
}