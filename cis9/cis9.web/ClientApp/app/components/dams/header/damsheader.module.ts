﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {DamsheaderComponent} from "./damsheader.component";
import {DamsdashboardModule} from "../dashboard/damsdashboard.module";
import {DamsdashboardComponent} from "../dashboard/damsdashboard.component";
import {DamsCreateFileComponent} from "../createFile/damsCreateFile.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ViewfileComponent} from "../viewfile/viewfile.component";
import {EditdocumentComponent} from "../editdocument/editdocument.component";
import {ImageviewerComponent} from "../imageViewer/imageviewer.component";
import {ViewdocumentComponent} from "../viewdocument/viewdocument.component";
import {DamssearchModule} from "../search/damssearch.module";
import {UpdatepasswordComponent} from "../updatepassword/updatepassword.component";
import { ImageViewerModule } from 'ngx-image-viewer';
import {NgAutoCompleteModule} from "ng-auto-complete";
import {CaseModule} from "../case/case.module";
import {SingledocumenteditComponent} from "../singleDocumentEdit/singledocumentedit.component";
import {ReadonlyfileComponent} from "../viewfile/readonly/readonlyfile.component";
import {ReadonlydocumentComponent} from "../viewdocument/readonly/readonlydocument.component";
import {EditDocComponent} from "../singleDocumentEdit/edit.doc.component";
import {DocWorkflowComponent} from "../doc/doc.workflow.component";
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import {FileRequestComponent} from "../filerequest/filerequest.component";
import {NgxSmartModalModule, NgxSmartModalService} from "ngx-smart-modal";
import {NgxSpinnerModule} from "ngx-spinner";
import { AmharicInputModule } from "../amharic_input/amharic_input.module"
import {Image_reorderingComponent} from "../editdocument/image_reordering/image_reordering.component";
import {SearchCaseComponent} from "../case/searchCase/searchCase.component";
const PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};

const config = {
    btnClass: 'default', // The CSS class(es) that will apply to the buttons
    zoomFactor: 0.1, // The amount that the scale will be increased by
    containerBackgroundColor: '#f9f6ff', // The color to use for the background. This can provided in hex, or rgb(a).
    wheelZoom: true, // If true, the mouse wheel can be used to zoom in
    allowFullscreen: true, // If true, the fullscreen button will be shown, allowing the user to entr fullscreen mode
    // allowKeyboardNavigation: true, // If true, the left / right arrow keys can be used for navigation
    btnIcons: { // The icon classes that will apply to the buttons. By default, font-awesome is used.
        zoomIn: 'fa fa-plus',
        zoomOut: 'fa fa-minus',
        rotateClockwise: 'fa fa-repeat',
        rotateCounterClockwise: 'fa fa-undo',
        next: 'fa fa-arrow-right',
        prev: 'fa fa-arrow-left',
        fullscreen: 'fa fa-arrows-alt',
    },
    btnShow: {
        // zoomIn: true,
        // zoomOut: true,
        rotateClockwise: true,
        rotateCounterClockwise: true,
        next: true,
        prev: true
    }
};

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        NgxSmartModalModule.forChild(),
        ImageViewerModule.forRoot(config),
        PerfectScrollbarModule.forChild(),
        ReactiveFormsModule,
        NgAutoCompleteModule,
        NgxSpinnerModule,
        DamssearchModule,
        CaseModule,
        AmharicInputModule
    ],
    declarations: [
        DamsheaderComponent,
        DamsCreateFileComponent,
        ViewfileComponent,
        EditdocumentComponent,
        ImageviewerComponent,
        ViewdocumentComponent,
        UpdatepasswordComponent,
        SingledocumenteditComponent,
        DocWorkflowComponent,
        EditDocComponent,
        ReadonlyfileComponent,
        ReadonlydocumentComponent,
        FileRequestComponent,
        Image_reorderingComponent,
        SearchCaseComponent
        
    ],
    exports: [DamsheaderComponent, DamsCreateFileComponent],
    providers: [NgxSmartModalService]
})
export class DamsheaderModule{

}