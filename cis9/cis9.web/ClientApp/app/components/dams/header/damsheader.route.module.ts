﻿import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {DamsdashboardComponent} from "../dashboard/damsdashboard.component";
import {CaseComponent} from "../case/case.component";
import {CaseResolve} from "../case/case.resolve";

const damsMain:Routes=[
    {path: '', component: DamsdashboardComponent}
];
@NgModule({
    imports: [
        RouterModule.forChild(damsMain)
    ],

    exports: [
        RouterModule
    ]
})
export  class DamsheaderRouteModule {
    
}