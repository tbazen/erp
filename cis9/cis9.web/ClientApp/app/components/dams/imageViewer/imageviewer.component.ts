﻿import {AfterViewInit, Component, Input, OnInit} from "@angular/core";
import {Image} from "../dashboard/damsdashboard.model";
import {DamsdashboardService} from "../dashboard/damsdashboard.service";
import {DomSanitizer} from "@angular/platform-browser";
import BaseComponent from "../../../base.component";


declare var $:any;
declare var Image_Viewer:any;
@Component({
    selector:'app-image-viewer',
    templateUrl:'./imageviewer.component.html'
})
export class ImageviewerComponent implements OnInit{
    
  
   
    constructor(public ddService:DamsdashboardService){
       
    }
    ngOnInit(){
        new Image_Viewer(document.getElementById('imageViewer'));
    }
    
  
        
       
    
    
}