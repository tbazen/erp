﻿import {AfterViewInit, Component, OnInit} from "@angular/core";
import {PagerService} from "../../../services/pager.service";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {searchDocument, searchFile} from "./damssearch.model";
import {ActivatedRoute, Router} from "@angular/router";
import {DamsdashboardService} from "../dashboard/damsdashboard.service";
import {Document, File} from "../dashboard/damsdashboard.model";
import {ToastrService} from "ngx-toastr";
import {DamsSearchService as DamssearchService} from "./damssearch.service";
import {NgxSpinnerService} from "ngx-spinner";
import * as _ from "lodash";
import {UsersService} from "../../admin/users/users.service";

declare var EthiopianDualCalendar: any;
declare var $: any;

@Component({
    templateUrl: './damssearch.component.html'
})
export class DamssearchComponent implements OnInit, AfterViewInit {

    public storage: WindowLocalStorage;
    public searchForm: FormGroup;
    public searchDocumentModel: searchDocument;
    public searchFileModel: searchFile;
    public searchType: number = 1;
    public fileResult: File[];
    public docResult: Document[];
    public searchItems: any[];
    public documentTypes: DocumentType[];
    public filecode: string;
    public fileowner: string;
    public upin: string;
    public searchError: string;
    public docForm: boolean;
    public fileForm: boolean;

    public userrole: number | null;
    public damsUser: boolean = false;
    public damsDigitizer: boolean = false;
    public damsSupervisor: boolean = false;
    public ethiopianDateTxt: any;

    public fileTotalCount: number;
    public filePages = [];
    public filePageSize = 30;
    public fileCurrentPage: number = 1;
    public totalPages:number;
    public startPage:number;
    public endPage:number;
    public startIndex:number;
    public endIndex:number;

    public docTotalCount: number;
    public docPages = [];
    public docPageSize = 30;
    public docCurrentPage: number = 1;
    public config:any;

    constructor(public fb: FormBuilder, public pagerService: PagerService, private activeRoute: ActivatedRoute, public ddService: DamsdashboardService, public router: Router, public toastr: ToastrService, public searchService: DamssearchService, public loadingService: NgxSpinnerService, public userService:UsersService) {
        this.ddService.getDocumentTypes().subscribe(res => {
            this.documentTypes = res;
        });
        this.searchForm = fb.group({
            searchType: ['1', Validators],
            FileCode: ['', Validators],
            FileOwner: ['', Validators],
            Folder: ['', Validators],
            upin: ['', Validators],
            referenceNo: ['', Validators],
            type: ['', Validators],
            date: ['', Validators]
        });

    }

    ngAfterViewInit(): void {

    }

    ngOnInit(): void {
        this.userService.loadPage().subscribe(res=>{
            this.config=res;
        })
        
        $("#docSearchContent").hide();
        $("#docSearchContent").hide();
        this.userrole = JSON.parse(<string>localStorage.getItem("role"));
        this.userrole = +this.userrole;
        
        switch (this.userrole) {
            case 4:
                this.damsDigitizer = true;
                break;
            case 6:
                this.damsSupervisor = true;
                break;
            case 5:
                this.damsUser = true;
                break;
        }
        
       

        this.filecode = this.activeRoute.snapshot.queryParamMap.get('file');
        this.fileowner = this.activeRoute.snapshot.queryParamMap.get('owner');
        this.upin = this.activeRoute.snapshot.queryParamMap.get('fileUPIN');
        
        this.searchService.fileSearch$.subscribe(fs => {
            this.searchFileModel = fs;
            this.search();
        });
        
        this.searchService.docSearch$.subscribe(ds => {
            this.searchDocumentModel = ds;
            this.search();
        });
        
        // this.fileResult = this.searchService.fileResult.getValue();
        // this.docResult = this.searchService.docResult.getValue();
        
        this.searchService.fileResult$.subscribe(fr => {
            this.fileResult = fr;
            this.loadingService.hide();
        });
        
        this.searchService.docResult$.subscribe(dr => {
            this.docResult = dr;
            this.loadingService.hide();
        });
        
        this.fileForm = true;
    }
    
    public getResult(pageNum : number) {
        
        if (this.searchType == 1){
            if (this.fileCurrentPage === pageNum) return;
            this.searchFileModel.page = pageNum;
            this.fileCurrentPage = pageNum;
            this.search();
          
}
        
        else {
           if (this.docCurrentPage === pageNum) return;
           this.searchDocumentModel.page = pageNum;
           this.docCurrentPage = pageNum;
           this.search();
        }
    }
    
    public onSearchClicked() {
        this.docCurrentPage = 1;
        this.fileCurrentPage = 1;
        this.search();
    }
    
    public search(): void {
        this.loadingService.show();
        
        if (this.searchType == 1) {
            if (this.searchFileModel.upin == '' && this.searchFileModel.FileOwner == '' && this.searchFileModel.FileCode == '' && this.searchFileModel.Folder == '') {
                $('.no-parameter').show();
                this.searchError = 'Please. Enter At Least One Search Parameter !'
            }
            else {
                $('.no-parameter').hide();
                this.searchService.searchFile(this.searchFileModel).subscribe(res => {
                  
                    this.searchService.fileResult.next(res.json());

                    this.fileTotalCount = res.headers.get("x-total-count");
                    this.totalPages = Math.ceil(this.fileTotalCount / this.filePageSize);
                   
                    if(this.totalPages<=10){
                        this.startPage=1;
                        this.endPage=this.totalPages;
                    }
                    else {
                        if (this.fileCurrentPage <= 6) {
                            this.startPage = 1;
                            this.endPage = 10;
                        } else if (this.fileCurrentPage + 4 >= this.totalPages) {
                            this.startPage = this.totalPages - 9;
                            this.endPage = this.totalPages;
                        } else {
                            this.startPage = this.fileCurrentPage - 5;
                            this.endPage = this.fileCurrentPage + 4;
                        }
                    }
                    this.startIndex = (this.fileCurrentPage - 1) * this.filePageSize;
                    this.endIndex = Math.min(this.startIndex + this.filePageSize - 1, this.fileTotalCount - 1);
                    this.filePages = Array.from(Array((this.endPage + 1) - this.startPage).keys()).map(i => this.startPage + i);

                    if (res.errorCode != null) {
                        $('.no-parameter').show();
                        this.searchError = res.message;
                    }
                    else {
                        if (this.fileResult.length == 0) {
                            $('.no-parameter').show();
                            this.searchError = "I can't find the result, Please Try Again";
                        }

                    }
                })
            }
        }
        else if (this.searchType == 2) {
            if (this.searchDocumentModel.type == 0 && this.searchDocumentModel.referenceNo == '' && (this.searchDocumentModel.date.toString() == '' || this.searchDocumentModel.date == 0)) {
                $('.no-parameter').show();
                this.searchError = 'Please. Enter At Least One Search Parameter !'
            }
            else {
                $('.no-parameter').hide();
                this.ethiopianDateTxt = $('#date-txt').attr('dateval');
                this.searchDocumentModel.date = this.ethiopianDateTxt;
                this.searchService.searchDoc(this.searchDocumentModel).subscribe(res => {
        
                    this.searchService.docResult.next(res.json());
                    this.docTotalCount = res.headers.get("x-total-count");
                    this.totalPages = Math.ceil(this.docTotalCount / this.docPageSize);

                    if(this.totalPages<=10){
                        this.startPage=1;
                        this.endPage=this.totalPages;
                    }
                    else {
                        if (this.docCurrentPage <= 6) {
                            this.startPage = 1;
                            this.endPage = 10;
                        } else if (this.docCurrentPage + 4 >= this.totalPages) {
                            this.startPage = this.totalPages - 9;
                            this.endPage = this.totalPages;
                        } else {
                            this.startPage = this.docCurrentPage - 5;
                            this.endPage = this.docCurrentPage + 4;
                        }
                    }
                    this.startIndex = (this.docCurrentPage - 1) * this.docPageSize;
                    this.endIndex = Math.min(this.startIndex + this.docPageSize - 1, this.docTotalCount - 1);
                    this.docPages = Array.from(Array((this.endPage + 1) - this.startPage).keys()).map(i => this.startPage + i);


                    if (res.errorCode != null) {
                        $('.no-parameter').show();
                        this.searchError = res.message;
                    }
                    else {
                        if (this.docResult.length == 0) {
                            $('.no-parameter').show();
                            this.searchError = "I can't find the result, Please Try Again";
                        }
                    }
                })
            }
        }

    }

    public navSearchForm(): void {
        if ($("input[name='searchType']:checked").val() == 1) {
            $("#docSearchContent").hide();
            $("#fileSearchContent").show();
            this.fileForm = true;
            this.docForm = false;
        }
        else if ($("input[name='searchType']:checked").val() == 2) {
            $("#fileSearchContent").hide();
            $("#docSearchContent").show();
            let datetxt = new EthiopianDualCalendar("#date-txt");
            this.docForm = true;

            this.fileForm = false;

        }
    }

    public viewFile(file: File): void {

        this.ddService.getFileById(file.id).subscribe(res => {
            this.ddService.selectedFile.next(res);
            this.router.navigate(['/dams/file/', file.id]);
        });
    }

    public viewDoc(doc: Document): void {
        console.log(doc);
        this.ddService.getDocumentById(doc.id).subscribe(res => {
            this.ddService.selectedDocument.next(res);
            this.ddService.getDocFile(doc.fileId).subscribe(file => {
                this.ddService.selectedFile.next(file);
                this.router.navigate(['/dams/document/', res.id, {edit: 'fasle'}]);
            });
            
        });
    }

    public editDoc(doc: Document): void {
        this.ddService.getDocumentById(doc.id).subscribe(res => {
            this.ddService.selectedDocument.next(res);
            this.ddService.selectedEditDoc.next(res);
            this.router.navigate(['/dams/editDoc/', res.id], {queryParams: {edit: false}})
        })
    }

    public deleteFile(file: File): void {
        this.ddService.deleteFile(file).subscribe(res => {
            this.toastr.warning("Sent Request For File Delete!", 'Delete');
        })
    }

    public updateFile(file: File): void {
        $('#regFile').addClass('in');
        $("#regFile").show();
        this.ddService.selectedFile.next(file);

        //this.router.navigate(['/dams/createFile']);
    }

    public addDocument(file: File): void {
        this.ddService.fileUpdateAddDoc(file).subscribe(res => {
            file.documents = [];
            this.ddService.selectedFile.next(file);
            this.router.navigate(['/dams/workflow/file/', res])
        });
    }

    public actionNav(evt: any): void {
        console.log('clicked - action');
        let target = evt.currentTarget;
        let $btn = $(target).parent();

        if ($btn.is('.open')) {
            $btn.removeClass('open');
        }
        else {
            $('.search_action div').removeClass('open');
            $btn.addClass('open');
        }

    }

}