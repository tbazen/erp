﻿export interface searchFile{
    FileCode:any;
    FileOwner:any;
    Folder:any;
    upin:any;
    page: number;
    pageSize: number;
}

export interface searchDocument{
    referenceNo : string;
    type: number;
    date:number;
    page: number;
    pageSize: number;
}
export interface DocumentType {
    id: number;
    name: string;
}
export interface DocumentSearchResult{
    
}

export interface FileSearchResult{
    
}