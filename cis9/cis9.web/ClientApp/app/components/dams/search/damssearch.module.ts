﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {DamssearchComponent} from "./damssearch.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DamsSearchService} from "./damssearch.service";
import {NgxSpinnerModule} from "ngx-spinner";

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        NgxSpinnerModule
    ],
    declarations: [
        DamssearchComponent
    ],
    exports: [
        DamssearchComponent,
        FormsModule,
        ReactiveFormsModule
    ],

    providers: [DamsSearchService]
})
export class DamssearchModule {

}