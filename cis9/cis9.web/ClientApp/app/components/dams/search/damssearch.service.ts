﻿import {Injectable} from "@angular/core";
import {ApiService} from "../../../services/api.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {searchDocument, searchFile} from "./damssearch.model";
import {Document, File} from "../dashboard/damsdashboard.model";

@Injectable()
export class DamsSearchService {
    
    public defaultFileSearchModel: searchFile =  {
        FileCode:  "" ,
        FileOwner: "",
        Folder: "",
        upin: "",
        page:1,
        pageSize: 30
    };
    
    constructor(public apiService: ApiService) {};
    
    public defaultDocSearchModel: searchDocument = {
        referenceNo: "",
        type: 0,
        date: 0,
        page:1,
        pageSize: 30
    };
    
    
    public fileSearch = new BehaviorSubject<searchFile>(this.defaultFileSearchModel);
    public fileSearch$ = this.fileSearch.asObservable();
    
    public docSearch = new BehaviorSubject<searchDocument>(this.defaultDocSearchModel);
    public docSearch$ = this.docSearch.asObservable();
    
    public fileResult = new BehaviorSubject<File[]>([]);
    public fileResult$ = this.fileResult.asObservable();
    
    public docResult = new BehaviorSubject<Document[]>([]);
    public docResult$ = this.docResult.asObservable();

    public searchFile(file: searchFile){
        return this.apiService.postWithHeaders('workflow/searchfile', file);
    }
    
    public searchDoc(doc: searchDocument){
        return this.apiService.postWithHeaders('workflow/searchdocument', doc);
    }
    
}