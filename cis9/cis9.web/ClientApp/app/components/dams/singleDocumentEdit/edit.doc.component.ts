﻿import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {FormBuilder, FormArray, FormGroup, Validators, ValidatorFn, AbstractControl} from "@angular/forms";
import {Document, Image, DocumentType} from "../dashboard/damsdashboard.model";
import {ActivatedRoute, Router} from "@angular/router";
import {DamsdashboardService} from "../dashboard/damsdashboard.service";
import {ToastrService} from "ngx-toastr";
import {Subscription} from "rxjs/Subscription";
import {ViewfileComponent} from "../viewfile/viewfile.component";
import Swal from "sweetalert2";

declare var $: any;
declare var EthiopianDualCalendar:any;
declare var EthiopianDate: any;
@Component({
    selector: 'app-edit-single-doc',
    templateUrl: './edit.doc.component.html'
})

export class EditDocComponent implements OnInit {

    @Output() showViewForm = new EventEmitter();
    @Output() nextImage = new EventEmitter();
    public docImageForm: FormGroup;
    public docAttributeForm: FormGroup;
    public documentModel: Document;
    public imageModel: Image;
    public selectedImage: string[]=[];
    public subscription : Subscription;
    public totalPages = 0;
    public selectedEditDoc:Document;
    @Input()
    public workflow: string;

    public documentTypes: DocumentType[];
    public docEthiopianDate:any;
    public images:Image[]=[];
    public isImages:boolean=true;
    public isOthers:boolean=false;
    public selectedEditImg:any;
    public config:any;
    public uploadedOtherDocument:Image;
    public onEditing:boolean=false;

    constructor(public fb: FormBuilder, public activedRoute: ActivatedRoute, public ddService: DamsdashboardService, public toastr: ToastrService, public router: Router) {

        this.selectedEditDoc=this.ddService.defaultDoc;

        this.ddService.getDocumentTypes().subscribe(res => {
            this.documentTypes = res;
        });

        this.docAttributeForm = fb.group({
            referenceNo: ['', Validators.required],
            sheets: ['', Validators.required],
            type: ['', Validators.required],
            date: ['', this.etDateValidator()]
        });
        this.docImageForm = fb.group({
            fileName: ['', Validators.required],
            page: ['', Validators.required]
        });

    }

    initImageItemRows() {
        return this.fb.group({});
    }

    get imageItemRows(): FormArray {
        return this.docImageForm.get('images') as FormArray;
    }
    etDateValidator(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } | null => {
            var val: String = control.value.toString();
            var invalid: boolean;
            if (!val || val.length == 0)
                invalid = true;
            else
            {

                invalid = EthiopianDate.prototype.parseEthiopianDateString(val) == null;

            }
            return invalid ? { 'etDate': { value: control.value } } : null;
        };
    }
    ngOnInit(): void {
        let docDate=new EthiopianDualCalendar('#doc_date');

        this.addBackImgTrigger();
        this.addFrontImgTrigger();

        this.ddService.selectedEditDoc$.subscribe(doc => {
            this.selectedEditDoc = doc;
            this.ddService.selectedImage.next(EditDocComponent.toBase64Str(this.selectedEditDoc.images));
        });
        this.ddService.selectedImage$.subscribe(img => {this.selectedImage = img});

        this.imageModel = {
            id: '',
            data: '',
            description: '',
            documentId: this.selectedEditDoc.id,
            workItem: '',
            page: 1,
            mime: ''
        };
        this.uploadedOtherDocument={
            id: '',
            data: '',
            description: '',
            documentId: '',
            workItem: '',
            page: 1,
            mime: ''
        };
        this.documentModel = {
            id: this.selectedEditDoc.id,
            sheets:this.selectedEditDoc.sheets,
            referenceNo: this.selectedEditDoc.referenceNo,
            workItem: this.selectedEditDoc.workItem,
            fileId: this.selectedEditDoc.fileId,
            type: this.selectedEditDoc.type,
            timeStamp: 0,
            date: this.selectedEditDoc.date,
            images: this.selectedEditDoc.images
        };

        if(this.documentModel.images.length > 0){
            this.onEditing=true;
            const mimeType=this.selectedImage[0].split('/',1).pop();
            if(mimeType=='data:application') {
                this.isOthers = true;
                this.isImages = false;
                $("#other").prop('checked', true);
                $("#images").prop('checked', false);
            }
            else{
                this.isImages=true;
                this.isOthers=false;
                $("#images").prop('checked', true);
                $("#other").prop('checked', false);
            }
        }
        //docDate.setDate(this.selectedEditDoc.date);
        this.totalPages=this.selectedEditDoc.images.length;
    }

    public addFrontImgTrigger():void{
        $("#uploadFrontImg").click();

    }

    public addBackImgTrigger():void{
        $("#uploadBackImg").click();
    }
    public addOtherDocTrigger():void{
        $("#uploadOtherdoc").click();
    }
    public saveImage(): void {
        console.log("Document Form is: " + this.docAttributeForm.valid);
        console.log("Document Model: " + this.documentModel);
        if(this.imageModel.data == ""){ //User trying to add empty image
            this.toastr.warning("Please select an image file", "Empty Image");
            return;

        }
        if (this.totalPages < (this.documentModel.sheets * 2)){
            if ($('#uploadFrontImg').val()!=""){ //Front Page
                this.imageModel.page = (2 * this.totalPages) + 1;
            }
            else if ($('#uploadBackImg').val()!=""){ //Back Page
                this.imageModel.page = 2 * this.totalPages;
            }

            this.totalPages++;
            console.log(this.totalPages);
            this.documentModel.images.push(this.imageModel);
            this.ddService.selectedImage.next(EditDocComponent.toBase64Str(this.documentModel.images));
            this.nextImage.emit();
            this.images.push(this.imageModel);
            //this.ddService.selectedEditDoc.next(this.documentModel);
            this.imageModel = {
                id: '',
                data: '',
                description: '',
                documentId: '',
                workItem: '',
                page: -1,
                mime: ''
            };

            $('#uploadBackImg').val("");
            $('#uploadFrontImg').val("");
        }
        else{
            this.toastr.info("Number of images exceeds document sheet number")
        }

    }

    public deleteImage(index: number){
        this.documentModel.images.splice(index, 1);
        this.totalPages--;
        this.imageModel = this.ddService.defaultImg;
    }

    public registerDocument(): void {
        console.log(this.documentModel);

        this.docEthiopianDate=$('#doc_date').attr('dateval');
        this.documentModel.date=this.docEthiopianDate;

        if (this.documentModel.date == null){
            this.toastr.warning("Please Enter document date", "No Document Date")
            return;
        }
        this.documentModel.images=this.images;
        this.ddService.fileUpdateDoc(this.documentModel).subscribe(res => {
            console.log(res);
            
            this.ddService.requestDocApproval({id: res.id}).subscribe(response => {
                if (response.errorCode!=null){
                    this.toastr.error(response.message, 'Add Document');
                }else
                {
                    this.toastr.success('Successfully Edited Document', 'Edit Document');
                    this.showDocView();
                } 
            });

           
        });
        

    }

    public handleFileUpload(evt: any): void {
        let files = evt.target.files;

        let file = files[0];


        if (files && file) {

            let reader = new FileReader();

            this.imageModel.mime = file.type;
            reader.onload = this.handleFileOnLoad.bind(this);

            reader.readAsBinaryString(file);
        }
    }

    private handleFileOnLoad(readerEvt: any): void {
        let binaryString = readerEvt.target.result;

        console.log("File Base64 Encoded");
        this.imageModel.data = btoa(binaryString);
        this.saveImage();
    }

    public showDocView(): void {
        this.router.navigate(['/dams/dashboard']);
    }

    public cancelEdit():void {
        this.router.navigate(['/dams/dashboard']);
    }

    public handleOtherFileUpload(evt:any):void{
        let files = evt.target.files;
        let fileName = $('#uploadOtherdoc').val().replace(/\\/gi, '/').split('/').pop();
        let file = files[0];


        if (files && file) {

            let reader = new FileReader();

            this.imageModel.mime = file.type;
            this.imageModel.description=fileName;
            reader.onload = this.handleOtherFileOnLoad.bind(this);

            reader.readAsBinaryString(file);
        }
    }

    private handleOtherFileOnLoad(readerEvt: any): void {
        let binaryString = readerEvt.target.result;

        console.log("File Base64 Encoded");
        this.imageModel.data = btoa(binaryString);
        this.saveOther();
    }

    public saveOther():void{
        if(this.imageModel.data == ""){ //User trying to add empty image
            this.toastr.warning("Please select an image file", "Empty Image");
            return;

        }

        if (this.documentModel.type.name === ''){
            let my = this;
            var dt = this.documentTypes.find(function (el) {
                return el.id == my.documentModel.type.id
            });
            if(dt)
                this.documentModel.type.name = dt.name;
        }

        this.imageModel.page=1;
        this.documentModel.images.push(this.imageModel);
        this.uploadedOtherDocument=this.imageModel;
        this.ddService.selectedImage.next(ViewfileComponent.toBase64Str(this.documentModel.images));
        //this.ddService.selectedEditDoc.next(this.documentModel);
        this.images.push(this.imageModel);
        const imageData=JSON.stringify(this.uploadedOtherDocument);
        const overduepath='data:' + this.uploadedOtherDocument.mime + ',' + encodeURI(this.uploadedOtherDocument.data);
        $('#pdf-viewer').attr('src',overduepath);


        this.imageModel = {
            id: '',
            data: '',
            description: '',
            documentId: '',
            workItem: '',
            page: -1,
            mime: ''
        };


    }

    public changeDoc():void{
        this.uploadedOtherDocument=null;
        this.documentModel.images.splice(0,1);
        this.selectedImage.splice(0,1);
        this.addOtherDocTrigger();
    }

    public navFormatForm():void{
        if(this.documentModel.images.length > 0){
            let mimeType=this.selectedImage[0].split('/',1).pop();
            if($("input[name='docFormat']:checked").val() == 1 && mimeType=="data:application"){
                Swal({
                    title: 'Are you sure?',
                    text: "There have a other document format if you click yes you lose the data permanently",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Lose it!'
                }).then((result) => {
                    if (result.value) {
                        this.ddService.deleteImg({workflow: this.workflow, image: this.selectedEditDoc.images[0]}).subscribe(res=>{
                            this.toastr.warning("Successfully clear the document data");
                            this.ddService.selectedEditDoc$.subscribe(doc => {
                                this.selectedEditDoc = doc;
                                this.ddService.selectedDocument.next(doc)
                            });
                            this.isImages=true;
                            this.isOthers=false;
                            this.documentModel.images=[];
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        $("#other").prop('checked', true);
                        $("#images").prop('checked', false);
                    }
                });
            }
            if($("input[name='docFormat']:checked").val() == 2 && mimeType=="data:image"){
                Swal({
                    title: 'Are you sure?',
                    text: "There have a image format if you click yes you lose the data permanently",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Lose it!'
                }).then((result) => {
                    if (result.value) {
                        for(let img of this.selectedEditDoc.images){
                            this.ddService.deleteImg({workflow: this.workflow, image: img}).subscribe(res=>{
                                this.ddService.selectedEditDoc$.subscribe(doc => {
                                    this.selectedEditDoc = doc;
                                    this.ddService.selectedDocument.next(doc)
                                });

                            });
                        }
                        this.isImages=false;
                        this.isOthers=true;
                        this.documentModel.images=[];

                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        $("#other").prop('checked', false);
                        $("#images").prop('checked', true);
                    }
                });
            }
        }else{
            if ($("input[name='docFormat']:checked").val() == 1) {
                this.isImages=true;
                this.isOthers=false;
            }
            else if($("input[name='docFormat']:checked").val() == 2){
                this.isImages=false;
                this.isOthers=true;
            }
        }
    }

    public static toBase64Str(imageModels: Image[]): string[] {
        let images = [];
        for (let model of imageModels) {
            images.push('data:' + model.mime + ';base64,' + model.data)
        }
        return images;
    }
    
}