﻿import {Component, OnInit, ViewChild} from "@angular/core";
import {Document, File, Image} from "../dashboard/damsdashboard.model";
import {CustomEvent, ImageViewerComponent} from "ngx-image-viewer";
import {EditdocumentComponent} from "../editdocument/editdocument.component";
import {DamsdashboardService} from "../dashboard/damsdashboard.service";
import {ActivatedRoute, Router} from "@angular/router";
import * as _ from "lodash";
import {ToastrService} from "ngx-toastr";
import {throwMdDuplicatedSidenavError} from "@angular/material";
import {EditDocComponent} from "./edit.doc.component";
import {configs} from "../../../app.config";

declare var $:any;
declare var Image_Viewer:any;
declare var full_screen:any;
declare var exit_fullScreen:any;
@Component({
    selector: 'app-single-doc-edit',
    templateUrl: './singledocumentedit.component.html'
})
export class SingledocumenteditComponent implements OnInit {

    public editDoc: boolean = false;
    public selectedDoc: Document;
    public docSelected: boolean = false;
    public selectedImage: string[] = [];
    public page: number = 0;
    public totalPages: number;
    public workItem: string;
    public selectedEditDoc: Document;
    public workflowId: string;

    @ViewChild(ImageViewerComponent) imageViewer: ImageViewerComponent;
    @ViewChild(EditDocComponent) editDocument: EditdocumentComponent;

    public config:any;
    public loadedIndex:number=0;
    public isImage:boolean=true;
    public isPDF:boolean=false;
    public isOther:boolean=false;
    public noImage:boolean=false;
    public svg:any;
    public indexOf:number;
    public rotationAmount:number=0;

    constructor(public activatedRoute: ActivatedRoute, public ddService: DamsdashboardService, public router: Router, public toastr: ToastrService) {

    }

    ngOnInit(): void {
        new Image_Viewer(document.getElementById('imageViewer'), document.getElementById('img_viewer_img'));
        this.docSelected = false;
        this.editDoc = true;
        this.ddService.getWorkflowState(this.workflowId).subscribe(res => {
            this.ddService.workflowState.next(res);
        });
        this.ddService.selectedDocument$.subscribe(doc => {
            this.selectedDoc = doc;
            this.page = 1;
            this.totalPages = this.selectedDoc.images.length;
            this.ddService.selectedImage.next(SingledocumenteditComponent.toBase64Str(this.selectedDoc.images));
        });
        this.ddService.docSelected$.subscribe(value => {
            this.docSelected = value;
        });

        this.ddService.selectedImage$.subscribe(res => {
            this.selectedImage = res;

            $("#img_viewer_img").attr('href', " ");

            this.loadedIndex=0;
            this.isPDF=false;
            this.isOther=false;
            this.isImage=false;
            this.noImage=false;
            this.page=0;

            if(this.selectedImage.length > 0){
                let mimeType=this.selectedImage[0].split(';',1).pop();
                if(mimeType=="data:image/jpg" || mimeType=="data:image/png" || mimeType=="data:image/jpeg"){

                    this.isImage=true;
                    this.isPDF=false;
                    this.isOther=false;
                    this.noImage=false;
                    $(".pdf-viewer").hide();
                    $(".other-viewer").hide();
                    $(".no-image").hide();
                    this.svgPosition();
                    //new Image_Viewer(document.getElementById('imageViewer'));
                    $("#img_viewer_img").attr('href', this.selectedImage[0]);



                }
                else if(mimeType=="data:application/pdf"){
                    this.isPDF=true;
                    this.isImage=false;
                    this.isOther=false;
                    this.noImage=false;
                    $(".pdf-viewer").show();
                    $(".other-viewer").hide();
                    $(".no-image").hide();
                    if(this.selectedDoc.images.length > 0){
                        let overDuePath=`${configs.url}Workflow/ShowImage?workflowId=${this.workflowId}&docId=${this.selectedDoc.id}`;
                        $("#pdf-viewer").attr('src',overDuePath);
                    }
                }
                else{
                    this.isOther=true;
                    this.isImage=false;
                    this.isPDF=false;
                    this.noImage=false;
                    $(".pdf-viewer").hide();
                    $(".other-viewer").show();
                    $(".no-image").hide();
                }

            }
            else {
                this.isImage=false;
                this.isPDF=false;
                this.isOther=false;
                this.noImage=true;
                $(".pdf-viewer").hide();
                $(".other-viewer").hide();
                $(".no-image").show();
            }
        })

    }

    public handleEvent(event: CustomEvent) {
        if (event.name == 'delete') {
            let images = this.ddService.selectedImage.getValue();
            images.splice(event.imageIndex, 1);
            this.imageViewer.prevImage(event);
            this.page = this.imageViewer.index + 1;
            this.ddService.selectedImage.next(images);
            this.editDocument.deleteImage(event.imageIndex);

        }
    }

    public nextPage() {
        this.indexOf=this.selectedImage.length - 1;
        this.loadedIndex=this.indexOf;
        this.page=this.loadedIndex;
        this.svgPosition();
        $("#img_viewer_img").attr('href', this.selectedImage[this.loadedIndex]);
    }

    public static toBase64Str(imageModels: Image[]): string[] {
        let images = [];
        for (let model of imageModels) {
            images.push('data:' + model.mime + ';base64,' + model.data)
        }
        return images;
    }

    public svgPosition(){
        this.svg=$('svg')[0].getBoundingClientRect();
        $("#img_viewer_img").attr('x', "0");
        $("#img_viewer_img").attr('y', "0");
        $("#img_viewer_img").attr('height', this.svg.height.toString());
        $("#img_viewer_img").attr('width', this.svg.width.toString());
        this.rotationAmount=0;
        $("#img_viewer_img").css('transform','rotate('+this.rotationAmount+'deg)');
    }
    public fullScreen(){
        this.makeFull();
        setTimeout(()=>this.svgPosition(),2000);

    }
    public makeFull(){
        new full_screen();
    }
    public exit_fullScreen(){
        new exit_fullScreen();
        setTimeout(()=>this.svgPosition(), 2000);
    }

    public next(): void{
        if(this.loadedIndex < this.selectedImage.length -1){
            this.loadedIndex++;
            this.page=this.loadedIndex;
            this.svgPosition();
            $("#img_viewer_img").attr('href', this.selectedImage[this.loadedIndex]);
        }
    }

    public previous(): void{
        if(this.loadedIndex > 0){
            this.loadedIndex--;
            this.page=this.loadedIndex;
            this.svgPosition();
            $("#img_viewer_img").attr('href', this.selectedImage[this.loadedIndex]);
        }

    }
    public downloadPdf() {
        const overRidePath = `${configs.url}Workflow/ShowImage?workflowId=${this.workflowId}&docId=${this.selectedDoc.id}`;
        $("#otherDocument").attr('src', overRidePath);
    }
    public rotate(degree:any){

        let imgContainer=$('svg')[0].getBoundingClientRect();
        let image=$("#img_viewer_img");
        if(this.rotationAmount == 360 ||this.rotationAmount == -360)
            this.rotationAmount=0;
        this.rotationAmount +=parseInt(degree);
        image.css('transform','rotate('+this.rotationAmount+'deg)');
        image.attr('degree', this.rotationAmount);
        image.css('transform','translate('+imgContainer.width/2 +','+ imgContainer.height/2+')');
        image.css('transform-origin-x', imgContainer.width/2);
        image.css('transform-origin-y', imgContainer.height/2);
    }
}