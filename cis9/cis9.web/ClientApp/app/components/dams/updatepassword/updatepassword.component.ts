﻿import {Component, OnInit} from "@angular/core";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UpdatePassowrd} from "../../../services/auth/auth.model";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {DamsdashboardService} from "../dashboard/damsdashboard.service";
import {AuthService} from "../../../services/auth/auth.service";

declare var $:any;

@Component({
    selector:'app-update-pass',
    templateUrl:'./updatepassword.component.html'
})
export class UpdatepasswordComponent implements OnInit{
    public oldNewCheck:string;
    public newPassConfirm:string;
    public updatePassForm:FormGroup;
    public passwordModel:UpdatePassowrd;
    public valid:boolean |true;
    
    
    
    constructor(public fb:FormBuilder, public toastr : ToastrService, public router:Router, public ddService:DamsdashboardService, public  authService:AuthService){
        this.updatePassForm=fb.group({
            oldPass:['', Validators.required],
            newPass:['', [Validators.required, UpdatepasswordComponent.misMatchValidator]],
            confirmPass:['', [Validators.required, UpdatepasswordComponent.matchValidator]]
            } )
    }
    
    ngOnInit():void{
        this.passwordModel={
            OldPassword:'',
            NewPassword:''
        }
    }

    public updatePass():void{
        console.log(this.passwordModel);
        this.ddService.changePass(this.passwordModel).subscribe(res=>{
            if (res.errorCode != null) {
                this.toastr.error(res.message, 'Error');
            }else {
                this.closeForm();
                this.toastr.success("Successfully change your password, Please Login Again", "Password Update");
                this.authService.logout();
                this.router.navigate(["/login"]);
            }
        })

    }
    
    public checkSim(){
        if ($('#oldPass').val() == $('#newPass').val()){
            this.oldNewCheck='The Old and New Password Must be Different';
            this.valid=false;
        }
        else{
            this.oldNewCheck='';
        }
        return this.valid;
    }
    
    public checkDiff(){

        if ($('#confirmPass').val() != $('#newPass').val()){
            this.newPassConfirm='New and Confirm Password Missmatch';
            this.valid=false;
        }
        else {
            this.newPassConfirm='';
            this.valid=true;
        }
        return this.valid;
    }
    public closeForm(){
        $('#updatePass').removeClass('in');
        $('#updatePass').hide();
        $('#updatePassForm').trigger('reset');
    }
    
   static matchValidator(abs: AbstractControl){

        const control = abs.parent;
        if (control){
            const passwordCtrl = control.get('newPass');
            const confirmPassCtrl = control.get('confirmPass');

            if(passwordCtrl && confirmPassCtrl){
                const pass = passwordCtrl.value;
                const confirmPass = confirmPassCtrl.value;

                if(pass != confirmPass){
                    return {matchPassword : true};
                }
                else {
                    return null;
                }
            }
        }

        else{
            return null;
        }
    }

    static misMatchValidator(abs: AbstractControl){

        const control = abs.parent;
        if (control){
            const passwordCtrl = control.get('newPass');
            const oldPassCtrl = control.get('oldPass');

            if(passwordCtrl && oldPassCtrl){
                const pass = passwordCtrl.value;
                const oldPass = oldPassCtrl.value;

                if(pass == oldPass){
                    return {matchPassword : true};
                }
                else {
                    return null;
                }
            }
        }

        else{
            return null;
        }
    }
}