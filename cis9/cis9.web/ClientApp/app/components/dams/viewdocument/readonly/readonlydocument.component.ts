﻿import {Component, OnInit, ViewChild} from "@angular/core";
import {CustomEvent, ImageViewerComponent} from "ngx-image-viewer";
import {Document, File, Image} from "../../dashboard/damsdashboard.model";
import {ActivatedRoute, Router} from "@angular/router";
import {DamsdashboardService} from "../../dashboard/damsdashboard.service";
import {ToastrService} from "ngx-toastr";
import {CaseDetail} from "../../case/case.model";
import {ISubscription} from "rxjs/Subscription";
import BaseComponent from "../../../../base.component";
import {takeUntil} from "rxjs/operators";
import {NgxSmartModalService} from "ngx-smart-modal";
import {configs} from "../../../../app.config";
import {UsersService} from "../../../admin/users/users.service";

declare var $:any;
declare var EthiopianDate:any;
declare var Image_Viewer:any;
declare var full_screen:any;
@Component({
    selector:'app-view-document',
    templateUrl:'./readonlydocument.component.html'
})
export class ReadonlydocumentComponent extends BaseComponent implements OnInit{
    public docSelected:boolean=false;
    public selectedDoc:Document;
    public selectedFile: File;
    public selectedImage:string[];
    
    public page:number=0;
    public totalPages:number;
    public docId:string;
    
    public selectedEtDate:any;
    public workflowId:string;
    public ethToDate:any;
    
    public fileId:string;
    public workflowState:number;
    public images :string[];
    public showCase:boolean = false;

    public userrole: number | null;
    public damsUser:boolean=false;
    public damsDigitizer:boolean=false;
    public damsSupervisor:boolean=false;

    public docSubscription : ISubscription;
    public imgSubscription : ISubscription;
    
    public caseModels: CaseDetail[] = [];
    
    @ViewChild(ImageViewerComponent) imageViewer: ImageViewerComponent;

    public config:any;
    public loadedIndex:number=0;
    public isImage:boolean=true;
    public isPDF:boolean=false;
    public isOther:boolean=false;
    public noImage:boolean=false;
    public svg:any;
    public imagePage:number;

    constructor( public ddService:DamsdashboardService, public activatedRoute:ActivatedRoute, public toastr:ToastrService, public router: Router, public modalService: NgxSmartModalService, public userService:UsersService){

        super();

    }
    
    ngOnInit():void{
        this.docSelected=false;
        this.userService.loadPage().subscribe(res=>{
            this.config=res;
        });
        new Image_Viewer(document.getElementById('imageViewer'), document.getElementById('img_viewer_img'));
        this.docId = this.activatedRoute.snapshot.params["docId"];
        
        this.ddService.getWorkflowState(this.workflowId).takeUntil(this.destroyed$).subscribe(res => {
            this.ddService.workflowState.next(res);
        });
        
        this.ddService.getCasebyDoc(this.docId).takeUntil(this.destroyed$).subscribe(res => {
            this.caseModels = res;
        });

        this.ddService.selectedFile$.takeUntil(this.destroyed$).subscribe(file => {
            this.selectedFile = file;
        });
        
        this.ddService.selectedDocument$.takeUntil(this.destroyed$).subscribe(doc => {
            this.selectedDoc = doc;
            this.totalPages = this.selectedDoc.images.length;
            this.ddService.selectedImage.next(ReadonlydocumentComponent.toBase64Str(this.selectedDoc.images));
            

        });
       this.ddService.selectedImage.takeUntil(this.destroyed$).subscribe(img => {
            this.selectedImage = img;
           $("#img_viewer_img").attr('href', " ");

           this.loadedIndex=0;
           this.isPDF=false;
           this.isOther=false;
           this.isImage=false;
           this.noImage=false;
           this.page=0;

           if(this.selectedImage.length > 0){
               let mimeType=this.selectedImage[0].split(';',1).pop();
               if(mimeType=="data:image/jpg" || mimeType=="data:image/png" || mimeType=="data:image/jpeg"){

                   this.isImage=true;
                   this.isPDF=false;
                   this.isOther=false;
                   this.noImage=false;
                   $(".pdf-viewer").hide();
                   $(".other-viewer").hide();
                   $(".no-image").hide();
                   this.svgPosition();
                   $("#img_viewer_img").attr('href', this.selectedImage[0]);
                   this.imagePage=parseInt(this.selectedDoc.images[0].page+"");
               }
               else if(mimeType=="data:application/pdf"){
                   this.isPDF=true;
                   this.isImage=false;
                   this.isOther=false;
                   this.noImage=false;
                   $(".pdf-viewer").show();
                   $(".other-viewer").hide();
                   $(".no-image").hide();
                   if(this.selectedDoc.images.length > 0){
                       let overDuePath=`${configs.url}Workflow/ShowImageByDocId?docId=${this.selectedDoc.id}`;
                       $("#pdf-viewer").attr('src',overDuePath);
                   }
               }
               else{
                   this.isOther=true;
                   this.isImage=false;
                   this.isPDF=false;
                   this.noImage=false;
                   $(".pdf-viewer").hide();
                   $(".other-viewer").show();
                   $(".no-image").hide()
               }


           }
           else {
               this.isImage=false;
               this.isPDF=false;
               this.isOther=false;
               this.noImage=true;
               $(".pdf-viewer").hide();
               $(".other-viewer").hide();
               $(".no-image").show();
           }

       });

        this.ddService.workflowState$.takeUntil(this.destroyed$).subscribe(ws => {this.workflowState = ws});


        this.ddService.docSelected$.takeUntil(this.destroyed$).subscribe(value => {
            this.docSelected = value;
        });
       

        if (this.selectedDoc.workItem == ""){ //a page reload occurred!
            this.ddService.getDocumentById(this.docId).takeUntil(this.destroyed$).subscribe(res => {
                this.ddService.selectedDocument.next(<Document>res);
                this.ddService.getDocFile(res.fileId).subscribe(file => {
                    this.ddService.selectedFile.next(file);
                })
            });
        }

        this.userrole = JSON.parse(<string>localStorage.getItem("role"));
        this.userrole = +this.userrole;
        switch (this.userrole) {
            case 4:
                this.damsDigitizer = true;
                break;
            case 6:
                this.damsSupervisor = true;
                break;
            case 5:
                this.damsUser = true;
                break;
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        
        this.ddService.selectedDocument.next(this.ddService.defaultDoc);
        this.ddService.selectedFile.next(this.ddService.defaultFile);
    }

    public svgPosition(){
        this.svg=$('svg')[0].getBoundingClientRect();
        $("#img_viewer_img").attr('x', "0");
        $("#img_viewer_img").attr('y', "0");
        $("#img_viewer_img").attr('height', this.svg.height.toString());
        $("#img_viewer_img").attr('width', this.svg.width.toString());
    }

    public fullScreen(){
        this.makeFull();
        setTimeout(()=>this.svgPosition(),2000);

    }
    public makeFull(){
        new full_screen();
    }

    public openFile():void {
        this.ddService.getFileById(this.selectedFile.id).subscribe(file => {
            this.ddService.selectedFile.next(file);
            this.router.navigate(['/dams/file/', this.selectedFile.id]);
        });
        
    }

    public static toBase64Str(imageModels: Image[]):string[]{
        let images = [];
        for (let model of imageModels){
            images.push('data:'+ model.mime + ';base64,' + model.data)
        }
        return images;
    }

    public static toEthDate(d: any):any{
        let date = new Date(d);
        let selectedEtDate=EthiopianDate.prototype.dateToEthiopian(date);
        return selectedEtDate.day +'/'+(selectedEtDate.month + 1)+'/'+ selectedEtDate.year;
    }

    public handleEvent(event: CustomEvent){
        if (event.name == 'delete'){
            
                let img = this.selectedDoc.images[event.imageIndex];
                this.ddService.deleteImg({workflow: this.workflowId, image: img}).subscribe(res=>{
                    let index = this.selectedDoc.images.findIndex(i => i.id == img.id);
                    this.selectedDoc.images.splice(index, 1);
                    let images = this.ddService.selectedImage.getValue();
                    images.splice(event.imageIndex, 1);
                    this.imageViewer.prevImage(event);
                    this.page = this.imageViewer.index + 1;
                    this.ddService.selectedImage.next(images);
                    this.toastr.warning('Successfully Deleted the image');
                });
            }
        
    }

    public indexChanged(){
        console.log("Current Page: " + this.page);
    }
    public showCreateCaseDoc(doc: Document){
        this.showCase = true;
        this.modalService.getModal("docCaseModal").open();
    }

    public closeModal() {
        this.modalService.getModal("docCaseModal").close();
    }

    public downloadPdf(){
        const overRidePath=`${configs.url}Workflow/ShowImageByDocId?docId=${this.selectedDoc.id}`;
        $("#otherDocument").attr('src',overRidePath);
    }
    public next(): void{
        if(this.loadedIndex < this.selectedImage.length -1){
            this.loadedIndex++;
            this.page=this.loadedIndex;
            this.svgPosition();
            $("#img_viewer_img").attr('href', this.selectedImage[this.loadedIndex]);
            this.imagePage=parseInt(this.selectedDoc.images[this.loadedIndex].page+"");
        }
    }

    public previous(): void{
        if(this.loadedIndex > 0){
            this.loadedIndex--;
            this.page=this.loadedIndex;
            this.svgPosition();
            $("#img_viewer_img").attr('href', this.selectedImage[this.loadedIndex]);
            this.imagePage=parseInt(this.selectedDoc.images[this.loadedIndex].page+"");
        }

    }
    
}