﻿import {Component, EventEmitter, Input, OnInit, Output, AfterViewInit, OnDestroy} from "@angular/core";
import {Document,File} from "../dashboard/damsdashboard.model";
import {ViewfileComponent} from "../viewfile/viewfile.component";
import {DamsdashboardService} from "../dashboard/damsdashboard.service";
import {ToastrService} from "ngx-toastr";

declare var $:any;



@Component({
    selector:'app-view-doc',
    templateUrl:'./viewdocument.component.html'
    
})

export class ViewdocumentComponent implements OnInit, AfterViewInit, OnDestroy{
    
    @Output() showEditForm = new EventEmitter();

    @Input()  public docs : any;
    @Input()  public workflowState: number = 0;
   
    public selectedDoc:Document;
    public emptyDoc:Document;
    public userrole: number | null;
    public damsUser:boolean=false;
    public damsDigitizer:boolean=false;
    public damsSupervisor:boolean=false;

    constructor(public ddservice:DamsdashboardService, public toastr:ToastrService){}
    
    ngOnInit():void{
        
        this.userrole=JSON.parse(<string>localStorage.getItem("role"));
        this.userrole = +this.userrole;
        
        switch (this.userrole) {
            case 4:
                this.damsDigitizer = true;
                break;
            case 6:
                this.damsSupervisor = true;
                break;
            case 5:
                this.damsUser = true;
                break;
        }
        console.log("Workflow State: ");
        console.log(this.workflowState);
        this.ddservice.workflowState$.subscribe(res => {this.workflowState = res});
    }
    
    ngAfterViewInit(): void {
     
    }
    
    ngOnDestroy(): void {
      
    }
    
    public showImg(doc:Document):void{
        this.ddservice.selectedDocument.next(doc);
        //this.ddservice.selectedImage.next([]);
        this.ddservice.docSelected.next(true);
    }
    public showDocEdit(){
        this.showEditForm.next();
        this.ddservice.selectedEditDoc.next(this.ddservice.defaultDoc);
    }
    public documentNav(evt:any):void{
            console.log('clicked - sidebar_menu');
            let target=evt.currentTarget;
            let $li = $(target).parent();

            if ($li.is('.active')) {
                $li.removeClass('active');
                $li.find('.fa-folder-open-o').addClass('fa-folder-o').removeClass('fa-folder-open-o');
                $('ul:first', $li).slideUp();
            } else {
                // prevent closing menu if we are on child menu
                
                    $(".list_of_documets").find('.parent').removeClass('active');
                    $(".list_of_documets").find('.fa-folder-open-o').addClass('fa-folder-o').removeClass('fa-folder-open-o');
                    $(".list_of_documets").find('.parent ul').slideUp();
                
                $li.addClass('active');
                $li.find('.fa-folder-o').addClass('fa-folder-open-o').removeClass('fa-folder-o');
                $('ul:first', $li).slideDown();
            }
        
    }
    public activeDoc(evt:any){
        console.log('clicked - sidebar_menu');
        let target=evt.currentTarget;
        let $li = $(target).parent();

        if ($li.is('.active')) {
            $('.child_menu').find('li').removeClass('active');
            $li.addClass('active');
        }
        else{
            $('.child_menu').find('li').removeClass('active');
            $li.addClass('active');
        }
    }
}