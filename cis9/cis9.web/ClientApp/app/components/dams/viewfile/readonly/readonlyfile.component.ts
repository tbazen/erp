﻿import {Component, OnInit, OnDestroy, ViewChild} from "@angular/core";
import {Document, File, FileStat, Image} from "../../dashboard/damsdashboard.model";
import {DamsdashboardService} from "../../dashboard/damsdashboard.service";
import * as _ from "lodash";
import {ActivatedRoute, Router} from "@angular/router";
import {ImageViewerComponent} from "ngx-image-viewer";
import BaseComponent from "../../../../base.component";

import {takeUntil} from "rxjs/operator/takeUntil";
import {FileRequest} from "../../filerequest/filerequest.model";
import {ToastrService} from "ngx-toastr";
import { NgxSmartModalService } from "ngx-smart-modal";
import {UsersService} from "../../../admin/users/users.service";
import {configs} from "../../../../app.config";
import {CaseComponent} from "../../case/case.component";

declare var $:any;
declare var EthiopianDate:any;
declare var Image_Viewer:any;
declare var full_screen:any;

@Component ({
    selector:'app-readonly-file',
    templateUrl:'./readonlyfile.component.html'
})

export class ReadonlyfileComponent extends BaseComponent implements OnInit{

    public showCase: boolean = false;

    public selectedDoc: Document;
    public selectedFile:File;
    public selectedImage:string[];
    
    public page:number=0;
    public totalPages:number;
    
    public workItem:string;
    public docGroup:any;
    public selectedEtDate:any;
    
    public docSelected:boolean=false;
    public ethToDate:any;
    public fileId:string;

    public requestedFileModel: FileRequest;
    public fileRequested: boolean = false;

    public fileStat: FileStat;

    public userrole: number | null;
    public damsUser: boolean = false;
    public damsDigitizer: boolean = false;
    public damsSupervisor: boolean = false;

    public imageCompLoaded:boolean = false;
    public config:any;
    public loadedIndex:number=0;
    public isImage:boolean=true;
    public isPDF:boolean=false;
    public isOther:boolean=false;
    public noImage:boolean=false;
    public svg:any;
    public imagePage:number;
    public cases:any;
    public rotationAmount:number=0;

    @ViewChild(ImageViewerComponent) imageViewer: ImageViewerComponent;

    constructor(public ddService: DamsdashboardService,
        public activatedRoute: ActivatedRoute, 
        public toastr: ToastrService,
        public modalService: NgxSmartModalService, public userService:UsersService, public router:Router) {
        super();
    
    }
    ngOnInit():void{
        this.userService.loadPage().subscribe(res=>{
            this.config=res;
        });
        new Image_Viewer(document.getElementById('imageViewer'), document.getElementById('img_viewer_img'));
        this.docSelected=false;
        this.selectedImage = [];
        this.fileId=this.activatedRoute.snapshot.params['fileId'];

        this.fileStat = {numDocuments: 0, numSheets: 0};
        
        this.ddService.selectedFile$.takeUntil(this.destroyed$).subscribe(file =>{
            this.selectedFile=file;
            this.docGroup = _(this.selectedFile.documents).groupBy(function (d) {return d.type.name;})
                .map(function(v, i) {return {type: i, docs:v}}).value();
            
            if (file.id != ''){
                this.ddService.getFileStat(file.id,"").subscribe(stat => {
                    this.fileStat = stat;
                });
            } 
            
        });
        
        this.ddService.selectedDocument$.takeUntil(this.destroyed$).subscribe(doc=>{
            this.selectedDoc=doc;
            //this.page=1;
            this.totalPages=this.selectedDoc.images.length;
            if (this.selectedDoc.date!=null){
                this.ethToDate= ReadonlyfileComponent.toEthDate(this.selectedDoc.date);
            }
            
            this.ddService.selectedImage.next(ReadonlyfileComponent.toBase64Str(doc.images));
            this.ddService.getCasebyDoc(doc.id).subscribe(docCase=>{
                this.cases=docCase;
            });
        });
        this.ddService.selectedImage$.takeUntil(this.destroyed$).subscribe(img => {
            this.selectedImage = img;
            $("#img_viewer_img").attr('href', " ");

            this.loadedIndex=0;
            this.isPDF=false;
            this.isOther=false;
            this.isImage=false;
            this.noImage=false;
            this.page=0;
            if(this.selectedImage.length > 0){
                
                let mimeType=this.selectedImage[0].split(';',1).pop();
                if(mimeType=="data:image/jpg" || mimeType=="data:image/png" || mimeType=="data:image/jpeg"){

                    this.isImage=true;
                    this.isPDF=false;
                    this.isOther=false;
                    this.noImage=false;
                    $(".pdf-viewer").hide();
                    $(".other-viewer").hide();
                    $(".no-image").hide();
                    this.svgPosition();
                    $("#img_viewer_img").attr('href', this.selectedImage[0]);
                    this.imagePage=parseInt(this.selectedDoc.images[0].page+"");

                }
                else if(mimeType=="data:application/pdf"){
                    this.isPDF=true;
                    this.isImage=false; 
                    this.isOther=false;
                    this.noImage=false;
                    $(".pdf-viewer").show();
                    $(".other-viewer").hide();
                    $(".no-image").hide();
                    if(this.selectedDoc.images.length > 0){
                        let overDuePath=`${configs.url}Workflow/ShowImageByDocId?docId=${this.selectedDoc.id}`;
                        $("#pdf-viewer").attr('src',overDuePath);
                    }
                }
                else{
                    this.isOther=true;
                    this.isImage=false;
                    this.isPDF=false;
                    this.noImage=false;
                    $(".pdf-viewer").hide();
                    $(".other-viewer").show();
                    $(".no-image").hide()
                }

            }
            else {
                this.isImage=false;
                this.isPDF=false;
                this.isOther=false;
                this.noImage=true;
                $(".pdf-viewer").hide();
                $(".other-viewer").hide();
                $(".no-image").show();
            }
        });
        this.ddService.docSelected$.takeUntil(this.destroyed$).subscribe(value => {
            this.docSelected=value;
        });
        
        if (this.selectedFile.workItem == ''){
            this.ddService.getFileById(this.fileId).takeUntil(this.destroyed$).subscribe(file=>{
                this.ddService.selectedFile.next(<File>file);
            });
        }

        this.requestedFileModel = {
            id: '',
            fileId: this.selectedFile.id,
            description: '',
            userName: '',
            workflowId: ''
        };
        this.userrole = JSON.parse(<string>localStorage.getItem("role"));
        this.userrole = +this.userrole;
        switch (this.userrole) {
            case 4:
                this.damsDigitizer = true;
                break;
            case 6:
                this.damsSupervisor = true;
                break;
            case 5:
                this.damsUser = true;
                break;
        }
        
    }
    
    ngOnDestroy(){
        this.selectedImage = [];
        this.ddService.selectedDocument.next(this.ddService.defaultDoc);
        this.ddService.selectedImage.next([]);
        this.ddService.docSelected.next(false);
        super.ngOnDestroy();
    }
    public svgPosition(){
        this.svg=$('svg')[0].getBoundingClientRect();
        $("#img_viewer_img").attr('x', "0");
        $("#img_viewer_img").attr('y', "0");
        $("#img_viewer_img").attr('height', this.svg.height.toString());
        $("#img_viewer_img").attr('width', this.svg.width.toString());
        this.rotationAmount=0;
        $("#img_viewer_img").css('transform','rotate('+this.rotationAmount+'deg)');
    }

    public fullScreen(){
        this.makeFull();
        setTimeout(()=>this.svgPosition(),2000);

    }
    public makeFull(){
        new full_screen();
    }

    public static toBase64Str(imageModels: Image[]):string[]{
        let images = [];
        for (let model of imageModels){
            images.push('data:'+ model.mime + ';base64,' + model.data)
        }
        return images;
    }

    public static toEthDate(d: any):any{
        let date = new Date(d);
        let selectedEtDate=EthiopianDate.prototype.dateToEthiopian(date);
        return selectedEtDate.day +'/'+(selectedEtDate.month + 1)+'/'+ selectedEtDate.year;
    }

    public next(): void{
        if(this.loadedIndex < this.selectedImage.length -1){
            this.loadedIndex++;
            this.page=this.loadedIndex;
            this.svgPosition();
            $("#img_viewer_img").attr('href', this.selectedImage[this.loadedIndex]);
            this.imagePage=parseInt(this.selectedDoc.images[this.loadedIndex].page+"");
        }
    }

    public previous(): void{
        if(this.loadedIndex > 0){
            this.loadedIndex--;
            this.page=this.loadedIndex;
            this.svgPosition();
            $("#img_viewer_img").attr('href', this.selectedImage[this.loadedIndex]);
            this.imagePage=parseInt(this.selectedDoc.images[this.loadedIndex].page+"");
        }

    }

    public documentNav(evt:any):void{
        console.log('clicked - sidebar_menu');
        let target=evt.currentTarget;
        let $li = $(target).parent();

        if ($li.is('.active')) {
            $li.removeClass('active');
            $li.find('.fa-folder-open-o').addClass('fa-folder-o').removeClass('fa-folder-open-o');
            $('ul:first', $li).slideUp();
        } else {
            // prevent closing menu if we are on child menu
            if (!$li.parent().is('.child_menu')) {
                $(".list_of_documets").find('.parent').removeClass('active');
                $(".list_of_documets").find('.fa-folder-open-o').addClass('fa-folder-o').removeClass('fa-folder-open-o');
                $(".list_of_documets").find('.parent ul').slideUp();
            }
            $li.addClass('active');
            $li.find('.fa-folder-o').addClass('fa-folder-open-o').removeClass('fa-folder-o');
            $('ul:first', $li).slideDown();
        }

    }
    public activeDoc(evt:any){
        console.log('clicked - sidebar_menu');
        let target=evt.currentTarget;
        let $li = $(target).parent();

        if ($li.is('.active')) {
            $('.child_menu').find('li').removeClass('active');
            $li.addClass('active');
        }
        else{
            $('.child_menu').find('li').removeClass('active');
            $li.addClass('active');
        }
    }
    public showImg(doc:Document):void{
        this.ddService.selectedDocument.next(doc);
        //this.ddservice.selectedImage.next([]);
        this.ddService.docSelected.next(true);
    }
    public showCreateCaseDoc(doc: Document) {
        this.showCase = true;
        this.modalService.getModal("docCaseModal").open();
    }
    public closeModal() {
        this.modalService.getModal("docCaseModal").close();
    }
    public requestPhysicalFile() {
        if (this.fileRequested){
            this.toastr.warning("You have already requested this file", "Physical File Request");
        }

        else{
            this.ddService.requestFile(this.requestedFileModel).subscribe(res => {
                this.toastr.success("File Request Sent", "Physical File Request");
                this.fileRequested = true;
            })

        }
        
    }
    public downloadPdf(){
        const overRidePath=`${configs.url}Workflow/ShowImageByDocId?docId=${this.selectedDoc.id}`;
        $("#otherDocument").attr('src',overRidePath);
    }
    public openCaseDetails(caseModel:any){
        this.router.navigate(['/case/', caseModel.workflow]);
    }
    public rotate(degree:any){

        let imgContainer=$('svg')[0].getBoundingClientRect();
        let image=$("#img_viewer_img");
        if(this.rotationAmount == 360 ||this.rotationAmount == -360)
            this.rotationAmount=0;
        this.rotationAmount +=parseInt(degree);
        image.css('transform','rotate('+this.rotationAmount+'deg)');
        image.attr('degree', this.rotationAmount);
        image.css('transform','translate('+imgContainer.width/2 +','+ imgContainer.height/2+')');
        image.css('transform-origin-x', imgContainer.width/2);
        image.css('transform-origin-y', imgContainer.height/2);
    }
}