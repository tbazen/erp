﻿import {Component, AfterViewInit, OnInit, ViewChild, OnDestroy} from "@angular/core";
import {
    File, Document, Image, RejectRequest, FileStat
} from "../dashboard/damsdashboard.model";
import {DamsdashboardService} from "../dashboard/damsdashboard.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import "rxjs/add/operator/takeUntil";
import "rxjs/add/operator/do";
import * as _ from "lodash";
import {CustomEvent, ImageViewerComponent} from "ngx-image-viewer";
import {EditdocumentComponent} from "../editdocument/editdocument.component";
import BaseComponent from "../../../base.component";
import {NgxSmartModalService} from "ngx-smart-modal";
import {FileRequest} from "../filerequest/filerequest.model";
import {NgxSpinnerService} from "ngx-spinner";
import {UsersService} from "../../admin/users/users.service";
import {configs} from "../../../app.config";
import Swal from 'sweetalert2';
import {delayWhen} from "rxjs/operators";
import {Observable} from "rxjs";
import {delay} from "rxjs/operator/delay";
import {timeout} from "rxjs/operator/timeout";
declare var $: any;
declare var EthiopianDate: any;
declare var Image_Viewer:any;
declare var full_screen:any;
declare var exit_fullScreen:any;


@Component({
    selector: 'app-fileviewer',
    templateUrl: './viewfile.component.html'
})

export class ViewfileComponent extends BaseComponent implements OnInit{

    public fileRejectForm: FormGroup;
    public rejectModel: RejectRequest;
    public editDoc: boolean = false;
    public viewDoc: boolean = true;

    public selectedDoc: Document;
    public selectedFile: File;
    public docSelected: boolean = false;
    public selectedImage: string[];
    public page: number = 0;
    public totalPages: number;
    public indexOf:number;
    public workItem: string;

    public userrole: number | null;
    public damsUser: boolean = false;
    private _damsDigitizer: boolean = false;
    public get damsDigitizer(): boolean {
        return this._damsDigitizer;
    }
    public set damsDigitizer(val: boolean) {
        this._damsDigitizer = val;
    }
    public damsSupervisor: boolean = false;

    public docGroup: any;

    public requestFileForm: FormGroup;

    public selectedEditDoc: Document;
    public ethToDate: any;
    public selectedEtDate: any;

    private _workflowState: number;
    public get workflowState(): number {
        return this._workflowState;
    }
    public set workflowState(val: number) {
        this._workflowState=val;
    }
    public workflowId: string;
    public workItemType: number;

    public fileStat: FileStat;
    public imageCompLoaded:boolean = false;
    public svg:any;

    private _iv: ImageViewerComponent;
    @ViewChild(ImageViewerComponent)
    set imageViewer(val: ImageViewerComponent) {
        this._iv = val;
    }
    get imageViewer(): ImageViewerComponent {
        return this._iv;
    }
    @ViewChild(EditdocumentComponent) editDocument: EditdocumentComponent;

    public config:any;
    public loadedIndex:number=0;
    public isImage:boolean=true;
    public isPDF:boolean=false;
    public isOther:boolean=false;
    public noImage:boolean=false;
    public reordering:boolean=false;
    public imagePage:number;
    public rotationAmount:number=0;

    constructor(public activatedRoute: ActivatedRoute, public ddService: DamsdashboardService, public router: Router, public fb: FormBuilder, public toastr: ToastrService, public modalService: NgxSmartModalService, public loadingService: NgxSpinnerService, public userService:UsersService) {

        super();
        this.requestFileForm = fb.group({
            description: ['', Validators.required]
        });
        this.fileRejectForm = fb.group({
            note: ['', Validators.required]
        });
        this.selectedFile = this.ddService.defaultFile;
        this.selectedDoc = this.ddService.defaultDoc;
        this.selectedImage = [];

        this.docSelected = false;


    }

    ngOnInit(): void {
        this.userService.loadPage().subscribe(res=>{
            this.config=res;
        });
        new Image_Viewer(document.getElementById('imageViewer'), document.getElementById('img_viewer_img'));
        this.docSelected = false;

        this.workflowId = this.activatedRoute.snapshot.params['workflow'];

        this.fileStat = {numDocuments: 0, numSheets: 0};

        this.ddService.selectedFile$.takeUntil(this.destroyed$).subscribe(file => {
            this.selectedFile = file;

            this.docGroup = _(this.selectedFile.documents).groupBy(function (d) {return d.type.name;})
                .map(function(v, i) {return {type: i, docs:v}}).value();

            this.ddService.getWorkItemType(file.workItem).takeUntil(this.destroyed$).subscribe(res => this.workItemType = res);

            this.ddService.getFileStat(file.id, this.workflowId).subscribe(stat => {
                this.fileStat = stat;
            });

            this.rejectModel = {
                workItem: this.selectedFile.workItem,
                note: '',
                workflow: this.workflowId
            };
        });

        this.ddService.getWorkflowState(this.workflowId).takeUntil(this.destroyed$).subscribe(res => {
            this.ddService.workflowState.next(res);
        });

        this.ddService.selectedDocument$.takeUntil(this.destroyed$).subscribe(doc => {
            this.selectedDoc = doc;
            if (this.selectedDoc.id != '') {
                this.page = 0;

                this.selectedImage = [];

                this.imageCompLoaded = false;

                if (this.imageViewer !== undefined) this.imageViewer.index =  0;

                this.totalPages = this.selectedDoc.images.length;
                if (this.selectedDoc.date != null) {

                    this.ethToDate = ViewfileComponent.toEthDate(this.selectedDoc.date);
                }


                this.ddService.getDocImages(this.workflowId ,this.selectedDoc.id).subscribe(res => {
                    this.selectedDoc.images=res;

                    this.ddService.selectedImage.next(ViewfileComponent.toBase64Str(res));
                }, error1 => {this.loadingService.hide()});

            }
            else{
                this.selectedDoc.images=[];
            }

        });

        this.ddService.selectedImage$.takeUntil(this.destroyed$).subscribe(img => {
            this.selectedImage = img;
          
            
            $("#img_viewer_img").attr('href', " ");
            
            this.loadedIndex=0;
            this.isPDF=false;
            this.isOther=false;
            this.isImage=false;
            this.noImage=false;
            this.page=0;
            
            if(this.selectedImage.length > 0){
                let mimeType=this.selectedImage[0].split(';',1).pop();
                if(mimeType=="data:image/jpg" || mimeType=="data:image/png" || mimeType=="data:image/jpeg"){

                    this.isImage=true;
                    this.isPDF=false;
                    this.isOther=false;
                    this.noImage=false;
                    $(".pdf-viewer").hide();
                    $(".other-viewer").hide();
                    $(".no-image").hide();
                    this.svgPosition();
                    //new Image_Viewer(document.getElementById('imageViewer'));
                    $("#img_viewer_img").attr('href', this.selectedImage[0]);
                    this.imagePage=parseInt(this.selectedDoc.images[0].page+"");
                }
                else if(mimeType=="data:application/pdf"){
                    this.isPDF=true;
                    this.isImage=false;
                    this.isOther=false;
                    this.noImage=false;
                    $(".pdf-viewer").show();
                    $(".other-viewer").hide();
                    $(".no-image").hide();
                    if(this.selectedDoc.images.length > 0 && this.selectedDoc.images[0].workItem!=null){
                        let overDuePath=`${configs.url}Workflow/ShowImage?workflowId=${this.workflowId}&docId=${this.selectedDoc.id}`;
                        $("#pdf-viewer").attr('src',overDuePath);
                    }
                }
                else{
                    this.isOther=true;
                    this.isImage=false;
                    this.isPDF=false;
                    this.noImage=false;
                    $(".pdf-viewer").hide();
                    $(".other-viewer").show();
                    $(".no-image").hide();
                }

            }
            else {
                this.isImage=false;
                this.isPDF=false;
                this.isOther=false;
                this.noImage=true;
                $(".pdf-viewer").hide();
                $(".other-viewer").hide();
                $(".no-image").show();
            }


        });


        this.ddService.workflowState$.takeUntil(this.destroyed$).subscribe(ws => {
            this.workflowState = ws
        });


        this.ddService.docSelected$.takeUntil(this.destroyed$).subscribe(value => {
            this.docSelected = value;
        });

        console.log(this.selectedFile);

        this.userrole = JSON.parse(<string>localStorage.getItem("role"));
        this.userrole = +this.userrole;
        switch (this.userrole) {
            case 4:
                this.damsDigitizer = true;
                break;
            case 6:
                this.damsSupervisor = true;
                break;
            case 5:
                this.damsUser = true;
                break;
        }

        console.log("Selected Image");
        console.log(this.selectedImage);

        this.workItem = this.activatedRoute.snapshot.params["workflow"];

        if (this.selectedFile.workItem == "") { //a page reload occurred!
            this.ddService.getFile(this.workflowId).subscribe(file => {
                this.ddService.selectedFile.next(<File>file);
            });
        }

    }



    ngOnDestroy(): void {
        this.selectedImage = [];
        this.ddService.selectedDocument.next(this.ddService.defaultDoc);
        this.ddService.selectedImage.next([]);
        this.ddService.docSelected.next(false);
        super.ngOnDestroy();

    }
    
    public svgPosition(){
        this.svg=$('svg')[0].getBoundingClientRect();
        $("#img_viewer_img").attr('x', "0");
        $("#img_viewer_img").attr('y', "0");
        $("#img_viewer_img").attr('height', this.svg.height.toString());
        $("#img_viewer_img").attr('width', this.svg.width.toString());
        this.rotationAmount=0;
        $("#img_viewer_img").css('transform','rotate('+this.rotationAmount+'deg)');
    }
    
    public fullScreen(){
       this.makeFull();
       setTimeout(()=>this.svgPosition(),2000);
        
    }
    public makeFull(){
        new full_screen();
    }
    public exit_fullScreen(){
        new exit_fullScreen();
        setTimeout(()=>this.svgPosition(), 2000);
    }

    public static toBase64Str(imageModels: Image[]): string[] {
        let images = [];
        for (let model of imageModels) {
            images.push('data:' + model.mime + ';base64,' + model.data)
        }
        return images;
    }

    public static toEthDate(d: any): any {
        let date = new Date(d);
        let selectedEtDate = EthiopianDate.prototype.dateToEthiopian(date);
        return selectedEtDate.day + '/' + (selectedEtDate.month + 1) + '/' + selectedEtDate.year;
    }

    public viewDocument(): void {
        this.editDoc = false;
        this.viewDoc = true;
        this.ddService.getFileStat(this.selectedFile.id, this.workflowId).subscribe(stat => {
            this.fileStat = stat;
        });
    }

    public nextPage() {
        this.indexOf=this.selectedImage.length - 1;
        this.loadedIndex=this.indexOf;
        this.page=this.loadedIndex;
        this.svgPosition();
        $("#img_viewer_img").attr('href', this.selectedImage[this.loadedIndex]);
        this.imagePage=parseInt(this.selectedDoc.images[this.loadedIndex].page+"");
    }

    public next(): void{
        if(this.loadedIndex < this.selectedImage.length -1){
            this.loadedIndex++;
            this.page=this.loadedIndex;
            this.svgPosition();
            $("#img_viewer_img").attr('href', this.selectedImage[this.loadedIndex]);
            this.imagePage=parseInt(this.selectedDoc.images[this.loadedIndex].page+"");
        }
    }

    public previous(): void{
        if(this.loadedIndex > 0){
            this.loadedIndex--;
            this.page=this.loadedIndex;
            this.svgPosition();
            $("#img_viewer_img").attr('href', this.selectedImage[this.loadedIndex]);
            this.imagePage=parseInt(this.selectedDoc.images[this.loadedIndex].page+"");
        }

    }

    public requestApproval(): void {
        let reqObj = {id: this.workflowId};
        Swal({
            title: 'Are you sure?',
            text: "You want to send the file to approval",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Request Approval!'
        }).then((result) => {
            if (result.value) {
                this.ddService.requestFileApproval(reqObj).subscribe(res => {
                        this.toastr.success('Successfully Request File Approval');
                        this.router.navigate(["dams/dashboard"])
                    }
                );
            }
        })
        
    }

    public deleteDoc(doc: Document) {
        Swal({
            title: 'Are you sure?',
            text: "You want delete this document",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Delete!'
        }).then((result) => {
            if (result.value) {
                console.log('document=' + this.selectedDoc);
                this.ddService.deleteDoc({workflow: this.workflowId, document: doc}).subscribe(res => {
                    this.toastr.warning("Successfully, Deleted The Document");
                    let index = this.selectedFile.documents.findIndex(i => i.id == doc.id);
                    this.selectedFile.documents.splice(index, 1);
                    this.ddService.selectedImage.next([]);
                    this.ddService.selectedFile.next(this.selectedFile);
                });
            }
        });

    }

    public cancelRequest() {
        console.log(this.workflowId);
        this.modalService.getModal('cancelModal').close();
        this.ddService.cancelChange({id: this.workflowId}).subscribe(res => {
            this.toastr.warning("Workflow Cancelled", "Cancel");
            this.router.navigate(['/dams/dashboard'])
        });
    }

    public showCancelDialog() {
        this.modalService.getModal('cancelModal').open();
    }

    public handleEvent(index:number) {
        Swal({
            title: 'Are you sure?',
            text: "You want delete this image",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Delete!'
        }).then((result) => {
            if (result.value) {
                if (this.editDoc) { //in edit mode, just remove it from the array
                    let images = this.ddService.selectedImage.getValue();
                    this.page=this.loadedIndex;
                    images.splice(index, 1);
                    this.ddService.selectedImage.next(images);
                    this.editDocument.deleteImage(index);
                }
                else {
                    let img = this.selectedDoc.images[index];
                    this.ddService.deleteImg({workflow: this.workflowId, image: img}).subscribe(res => {
                        let index = this.selectedDoc.images.findIndex(i => i.id == img.id);
                        this.selectedDoc.images.splice(index, 1);
                        let images = this.ddService.selectedImage.getValue();
                        images.splice(index, 1);
                        this.page=this.loadedIndex;
                        this.ddService.selectedImage.next(images);
                        this.toastr.warning('Successfully Deleted the image');
                    });
                }
            }
        });
    }

    public deleteImg(img: Image) {

        console.log("Image to be deleted");
        console.log(img);

    }

    public approveChange() {
        Swal({
            title: 'Are you sure?',
            text: "You want to approve the change",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Approve It!'
        }).then((result) => {
            if (result.value) {
                this.ddService.approveRequest({id: this.workflowId}).subscribe(res => {
                    this.toastr.success("Change Approved", "Success");
                    this.router.navigate(["dams/dashboard"])
                });
            }
        })
    }

    public showRejectNoteForm(): void {
        this.modalService.getModal('rejectModal').open();
    }

    public rejectChange() {

        this.modalService.getModal('rejectModal').close();

        if (this.damsDigitizer) {
            this.ddService.rejectFile(this.rejectModel).subscribe(res => {
                this.toastr.info("Change Rejected", "Info");
                this.router.navigate(["/dams/dashboard"]);
            });
        }

        else if (this.damsSupervisor) {
            this.ddService.rejectFileRequest(this.rejectModel).subscribe(res => {
                this.toastr.info("Change Rejected", "Info");
                this.router.navigate(["/dams/dashboard"]);
            });
        }
    }

    public addDocument(): void {
        this.ddService.selectedImage.next([]);
        this.ddService.selectedEditDoc.next(this.ddService.defaultDoc);
        this.viewDoc = false;
        this.editDoc = true;
        this.docSelected = false;
    }

    public editDocumnet(doc: Document): void {
        console.log(doc);
        //doc.images = [];
        //this.selectedImage = [];
        //this.ddService.selectedImage.next([]);
        this.ddService.selectedEditDoc.next(doc);
        this.viewDoc = false;
        this.docSelected = false;
        this.editDoc = true;

    }

    public indexChanged() {
        console.log("Current Page: " + this.page);
    }

    public closeForm(): void {
        $("#rejectNote").hide();
    }
    public downloadPdf(){
        const overRidePath=`${configs.url}Workflow/ShowImage?workflowId=${this.workflowId}&docId=${this.selectedDoc.id}`;
        $("#otherDocument").attr('src',overRidePath);
    }
    
    public reorderImage(doc:Document){
        this.reordering=true;
        this.ddService.selectedEditDoc.next(doc);
        $('#image_reordering').addClass('in');
        $('#image_reordering').show();
    }
    public closeReorderingPage(evt:any){
        this.reordering=false;
        $('#image_reordering').removeClass('in');
        $('#image_reordering').hide();
    }
    public rotate(degree:any){
        
        let imgContainer=$('svg')[0].getBoundingClientRect();
        let image=$("#img_viewer_img");
        if(this.rotationAmount == 360 ||this.rotationAmount == -360)
            this.rotationAmount=0;
        this.rotationAmount +=parseInt(degree);
        image.css('transform','rotate('+this.rotationAmount+'deg)');
        image.attr('degree', this.rotationAmount);
        image.css('transform','translate('+imgContainer.width/2 +','+ imgContainer.height/2+')');
        image.css('transform-origin-x', imgContainer.width/2);
        image.css('transform-origin-y', imgContainer.height/2);
    }
}