﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {Dashboard2Component} from "./dashboard2.component";
import {SideMenuComponent} from "./sideMenu/sideMenu.component";
import {MainComponent} from "../main/main.component";
import {MainBodyModule} from "./mainBody/mainBody.module";
import {MainBodyComponent} from "./mainBody/mainBody.component";
import { SideMenuModule } from "./sideMenu/sideMenu.module";

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        SideMenuModule,
        MainBodyModule
    ],

    declarations: [
        Dashboard2Component 
    ],

    exports: [Dashboard2Component]
})
export class Dashboard2Module {
    
}