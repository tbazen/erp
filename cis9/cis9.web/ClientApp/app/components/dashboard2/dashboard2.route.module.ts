﻿import {NgModule} from "@angular/core";
import {Dashboard2Component} from "./dashboard2.component";
import {Routes, RouterModule} from "@angular/router";

const dashboardRoute2:Routes=[
    {path: '', component: Dashboard2Component}
    ];

@NgModule({
    imports: [
        RouterModule.forChild(dashboardRoute2)
    ],

    exports: [
        RouterModule
    ]
})
export class Dashboard2RouteModule {
    
}