﻿import {NgModule} from "@angular/core";
import {DashboardPanelComponent} from "./dashboardPanel.component";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";

@NgModule({
    imports:[
        CommonModule,
        RouterModule
    ],
    declarations:[
        DashboardPanelComponent
    ],
    exports: [DashboardPanelComponent]
})
export class  DashboardPanelModule{

}