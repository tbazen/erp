﻿import {Component, AfterViewInit} from "@angular/core";
import {Router} from "@angular/router";
declare var $:any;
@Component({
    selector:'app-main-body',
    templateUrl:'./mainBody.component.html'
})

export class  MainBodyComponent implements AfterViewInit{

    constructor(public router: Router){}


    ngAfterViewInit(): void {
        this.router.navigate(["/dashboard/pendingTask"])
    }
    
    public showMap():void{
        if ($('.dashboard-map-sm').hasClass('col-md-1')){
            $('.dashboard-panel').removeClass('col-md-11');
            $('.dashboard-panel').addClass('col-md-6');
            $('.dashboard-panel').find('.fa-toggle-left').addClass('fa-toggle-right').removeClass('fa-toggle-left');
            $('.dashboard-map').show();
            $('.dashboard-map-sm').hide();
        }
    }
}