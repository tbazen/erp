﻿import {NgModule} from "@angular/core";
import {MainBodyComponent} from "./mainBody.component";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {Dashboard2Component} from "../dashboard2.component";
import {PropertyRegistarationComponent} from "../../propertyRegistration/propertyRegistration.component";
import { DashboardPanelModule } from "./dashboardPanel/dashboardPanel.module";
import { PropertyRegistrationModule } from "../../propertyRegistration/propertyRegistration.module";

@NgModule({
    imports:[
        CommonModule,
        RouterModule,
        DashboardPanelModule,
        PropertyRegistrationModule
    ],
    declarations:[
        MainBodyComponent
    ],
    exports: [MainBodyComponent]
})
export class  MainBodyModule{

}