﻿import {NgModule} from "@angular/core";
import {MainBodyComponent} from "./mainBody.component";
import {RouterModule, Routes} from "@angular/router";

const mainBodyRoute:Routes=[{
    path: '',
    component: MainBodyComponent,
    children: [
        {
            path: 'pendingTask',
            loadChildren: './dashboardPanel/dashboardPanel.module#DashboardPanelModule',

        },
        {
            path: 'propertyRegistration',
            loadChildren: '../propertyRegistration/propertyRegistration.module#PropertyRegistrationModule',

        }
    ]

}];
@NgModule({
    imports: [
        RouterModule.forChild(mainBodyRoute)
    ],

    exports: [
        RouterModule
    ]
})
export class  MainBodyRouteModule{
    
}