﻿import {Component, OnInit} from "@angular/core";

declare var $:any;
@Component({
    selector:'app-side-menu',
    templateUrl:'./sideMenu.component.html'
})
export class SideMenuComponent implements OnInit{
    public collapseMenu():void{
        if($('#dashboard').hasClass('dashboard-md')){
            $('#sideMenu').find('a span.a-tag').addClass('a-tag-sm').removeClass('a-tag');
            $('.side-menu').find('li.active ul').hide();
            $('.side-menu').find('li.active').addClass('active-sm').removeClass('active');
            $('#sideMenu').addClass('sideMenu-sm').removeClass('sideMenu-md');
            $('#dashboard-main-body').addClass('dashboard-main-body-sm').removeClass('dashboard-main-body-md');
        }
        else{
            $('#sideMenu').find('a span.a-tag-sm').addClass('a-tag').removeClass('a-tag-sm');
            $('.side-menu').find('li.active-sm ul').show();
            $('.side-menu').find('li.active-sm').addClass('active').removeClass('active-sm');
            $('#sideMenu').addClass('sideMenu-md').removeClass('sideMenu-sm');
            $('#dashboard-main-body').addClass('dashboard-main-body-md').removeClass('dashboard-main-body-sm');
        }
        $('#dashboard').toggleClass('dashboard-md dashboard-sm');
    }
    ngOnInit(): void {

        this.showChildMenu();
    }
    public showChildMenu():void{
        $(".side-menu").find('a').on('click', function(ev:any) {
            console.log('clicked - sidebar_menu');
            let target=ev.currentTarget;
            let $li = $(target).parent();

            if ($li.is('.active')) {
                $li.removeClass('active active-sm');
                $('ul:first', $li).slideUp();
            } else {
                // prevent closing menu if we are on child menu
                if (!$li.parent().is('.child_menu')) {
                    $(".side-menu").find('li').removeClass('active active-sm');
                    $(".side-menu").find('li ul').slideUp();
                }else{
                    if ( $('#dashboard').is( ".dashboard-sm" ) )
                    {
                        $(".side-menu").find( "li" ).removeClass( "active active-sm" );
                        $(".side-menu").find( "li ul" ).slideUp();
                    }
                }
                $li.addClass('active');

                $('ul:first', $li).slideDown();
            }
        });
    }
    
}