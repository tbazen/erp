﻿import {Component, OnInit} from "@angular/core";
import {AuthService} from "../../services/auth/auth.service";
import {Router} from "@angular/router";
import {UsersService} from "../admin/users/users.service";

declare var $: any;

@Component({
    selector: "app-main",
    templateUrl: "main.component.html"
})
export class MainComponent implements OnInit{
   

    public storage: WindowLocalStorage;
    public username: string | null;
    public menuShowed: boolean = false;
    public config:any;
    
    constructor(public authService: AuthService, public router: Router, public userService:UsersService){}

    ngOnInit(): void {
        
        this.username = localStorage.getItem("username");
        this.userService.loadPage().subscribe(res=>{
            this.config=res;
        })
    }
    
    public logOut():void{   
        this.authService.logout();
        this.router.navigate(["/login"]);
    }
    
    public showdropDown(){
        this.menuShowed = !this.menuShowed;
        if(this.menuShowed){
            $(".dropdown-menu").show()
        }
        else{
            $(".dropdown-menu").hide()
        }
        
        
}
}