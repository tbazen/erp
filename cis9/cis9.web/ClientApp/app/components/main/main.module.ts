﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {MainComponent} from "./main.component";
import {MainRouteModule} from "./main.route";

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        MainRouteModule
        ],
    declarations: [
        MainComponent
        ],
    exports: [MainComponent]
})
export class MainModule{
    
}