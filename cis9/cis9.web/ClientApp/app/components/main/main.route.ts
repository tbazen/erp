﻿import {Router, RouterModule, Routes} from "@angular/router";
import {MainComponent} from "./main.component";
import {NgModule} from "@angular/core";
import {DashboardComponent} from "../admin/dashboard/dashboard.component";
import {NoAuthGuard} from "../../services/auth.guard";
import {AuditComponent} from "../admin/audit/audit.component";
import {UsersComponent} from "../admin/users/users.component";
import {LoginComponent} from "../admin/users/login/login.component";

const mainRoutes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'admin',
        component: MainComponent,
        children: [
            {
                path: '', loadChildren: "../admin/dashboard/dashboard.module#DashboardModule"
            }
        ]

    },

    {
        path: 'dams',
        component: MainComponent,
        children: [
            {path: '', loadChildren: '../dams/dashboard/damsdashboard.module#DamsdashboardModule'}
        ]
    },
    {
        path: '', pathMatch: 'full', redirectTo: 'login'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(mainRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class MainRouteModule {

}