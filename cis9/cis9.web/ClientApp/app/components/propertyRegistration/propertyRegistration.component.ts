﻿
import{Component} from "@angular/core";
declare var $:any;
@Component({
    selector:'app-propertyReg',
    templateUrl:'./propertyRegistration.component.html'
})
export class PropertyRegistarationComponent {
    
    public hideMap():void{
        if ($('.dashboard-panel').hasClass('col-md-6')){
            $('.dashboard-panel').removeClass('col-md-6');
            $('.dashboard-panel').addClass('col-md-11');
            $('.dashboard-panel').find('.fa-toggle-right').addClass('fa-toggle-left').removeClass('fa-toggle-right');
            $('.dashboard-map').hide();
            $('.dashboard-map-sm').show();
        }
        else{
            $('.dashboard-panel').removeClass('col-md-11');
            $('.dashboard-panel').addClass('col-md-6');
            $('.dashboard-panel').find('.fa-toggle-left').addClass('fa-toggle-right').removeClass('fa-toggle-left');
            $('.dashboard-map').show();
            $('.dashboard-map-sm').hide();
        }
    }
}