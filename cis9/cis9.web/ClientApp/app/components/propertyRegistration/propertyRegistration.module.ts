﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {PropertyRegistarationComponent} from "./propertyRegistration.component";


@NgModule({
    imports: [
        CommonModule,
        RouterModule
    ],

    declarations: [
        PropertyRegistarationComponent
    ],

    exports: [PropertyRegistarationComponent]
})

export class PropertyRegistrationModule{}