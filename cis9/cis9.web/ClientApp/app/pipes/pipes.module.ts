﻿import {SafeHtml} from "./safehtml.pipe";
import {NgModule} from "@angular/core";

@NgModule({
    imports: [
        // dep modules
    ],
    declarations: [
        SafeHtml
    ],
    exports: [
        SafeHtml
    ]
})
export class ApplicationPipesModule {}