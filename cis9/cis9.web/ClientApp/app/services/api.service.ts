﻿import {Headers, Http, Response} from "@angular/http";
import {configs} from "../app.config";
import {EventEmitter, Injectable} from "@angular/core";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {Observable} from "rxjs/Observable";
import {ReplaySubject} from "rxjs/ReplaySubject";
import {Router} from "@angular/router";
import {ParseLocation, UrlResolver} from "@angular/compiler";


export class ErrorMsg{
    status: number;
    msg: string;
    title: string;
}

@Injectable()
export class ApiService {
    
    public cookie: string;
    public base_url=configs.url;
    public KEY_NAME = '.ASPNetCoreSession';
    
    public headers= new Headers({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin':'*'
    });
    
    public  static apiEvent = new ReplaySubject(1);



    constructor(public _http: Http, public router: Router) {

    }

    public get(path: string): Observable<any> {
        return this._http.get(`${this.base_url}${path}`, {withCredentials: true, headers: this.headers})
            .map((res: Response) => res.json())
            .catch(ApiService.handleError);

    }
    
    public getWithHeaders(path:string):Observable<any> {
        return this._http.get(`${this.base_url}${path}`, {withCredentials: true, headers: this.headers})
            .catch(ApiService.handleError);
    }

    public post(path: string, body: any): Observable<any> {
        return this._http.post(`${this.base_url}${path}`, body, {withCredentials: true, headers: this.headers})
            .map((res: Response) => res.json())
            .catch(ApiService.handleError);


    }
    
    public postWithHeaders(path:string,  body: any): Observable<any>{
        return this._http.post(`${this.base_url}${path}`, body, {withCredentials: true, headers: this.headers})
            .catch(ApiService.handleError);
    }

    public put(path: string, body: any): Observable<any> {
        return this._http.put(`${this.base_url}${path}`, body, {withCredentials: true, headers: this.headers})  
            .map((res: Response) => res.json())
            .catch(ApiService.handleError);

    }
    

    public delete(path: string): Observable<any> {
        return this._http.delete(`${this.base_url}${path}`, {withCredentials: true, headers: this.headers})
            .map((res: Response) => res.json())
            .catch(ApiService.handleError);

    }

    public patch(path: string, body: any): Observable<any> {
        return this._http.patch(`${this.base_url}${path}`, body, {withCredentials: true, headers: this.headers})
            .map((res: Response) => res.json())
            .catch(ApiService.handleError);
    }

    public postFormData(path: string, form_data: FormData): Observable<any> {
        const formDataHeaders = new Headers();
        formDataHeaders.set('enctype', 'multipart\/form-data');
        formDataHeaders.set('Accept', 'application/json');
        formDataHeaders.set('Authorization', 'Bearer ' + window.localStorage.getItem(this.KEY_NAME));
        formDataHeaders.set('Access-Control-Allow-Headers', '*');

        return this._http.post(`${this.base_url}${path}`, form_data, {headers: formDataHeaders})
            .map((res: Response) => res.json())
            .catch(ApiService.handleError);
    }

    public static handleError(response: any) {

        const err = {status: 0, message: ''};
        console.log(response);
        err.status = response.status ? response.status : 0;
        err.message = JSON.parse(response._body).message ;
        
        let errMsg = new ErrorMsg();
        
        if (response.ok) {
            return response;

        } else if (response.status === 0) {
            errMsg = {status: response.status,msg: response.message, title: 'Network Error'};
            ApiService.apiEvent.next(errMsg);
        } else if (response.status === 422) {
            //  ApiService.apiEvent.next({msg:"Uprocesseable Entity!", title:"Validation Error"});
            // let the component handle the error
        } else if (response.status === 400) {
            errMsg = {status: response.status,msg: 'Your session expired, Please login again', title: 'Session Expired'};
            ApiService.apiEvent.next(errMsg);
        } else if (response.status === 401) {
             errMsg = {status: response.status,msg: err.message, title: 'Unauthorized'};
            ApiService.apiEvent.next(errMsg);
        } else if (response.status === 404) {
             errMsg = {status: response.status,msg: 'File Not found!', title: ' Not Found'};
            ApiService.apiEvent.next(errMsg);
        } else if (response.status === 500) {
             errMsg = {status: response.status,msg: 'The Server is down!', title: 'Server Error'};
            ApiService.apiEvent.next(errMsg);
        }
        
        else if (response.status == 403){
            errMsg = {status: response.status,msg: 'You are unathorized for this action', title: 'Forbidden'};
            ApiService.apiEvent.next(errMsg);
        }
        
        return Observable.throw(err);
    }
    



}