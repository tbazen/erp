﻿import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";


export class NoAuthGuard implements CanActivate{
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
            return localStorage.getItem("username") != null;
    }
}

export class DamsGuard implements CanActivate {
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        let role = localStorage.getItem("role");
        if (role == null) return false;
        
        let damsUser = +<string>role;
        
        return damsUser == 4 || damsUser == 5 || damsUser == 6 || damsUser == 2
    }
}

export class AdminGuard implements CanActivate{
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        let role  = localStorage.getItem("role");
        
        if (role == null) return false;
        
        let admin = +<string>role;
        
        return admin == 1;
    }
}