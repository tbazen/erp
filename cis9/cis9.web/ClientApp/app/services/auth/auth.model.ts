﻿export interface LoginUser {
    username: string,
    password: string
}

export interface RegisterUser{
    username: string,
    password: string,
    roles: number[],
    fullname: string,
    phonenumber: string
}

export interface UpdatePassowrd{
    OldPassword:string;
    NewPassword:string;
}
export interface ResetPassowrd{
    UserName:string;
    NewPassword:string;
}