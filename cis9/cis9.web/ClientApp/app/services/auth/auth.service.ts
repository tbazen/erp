﻿import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {ApiService} from "../api.service";
import {LoginUser, RegisterUser} from "./auth.model";
import {Injectable} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {UpdatePassowrd} from "./auth.model";


@Injectable()
export class AuthService{
    
    public user: LoginUser;
    public formFb: FormBuilder;
    
    constructor(public router: Router, public apiService: ApiService){
        
    }
    
    public login(user: LoginUser){
        return this.apiService.post("account/login", user);
    }
    public loginWsis(user:LoginUser){
        return this.apiService.post("account/LoginWSIS", user);
    }
    public getUserRoles(){
        return this.apiService.get("account/getroles");
    }
    
    public setUserRole(role: any){
        return this.apiService.post("account/setrole", role);
    }
    
    public register(user: RegisterUser){
        return this.apiService.post("account/register", user);
    }
    
    public getRoles(){
        return this.apiService.get("lookup/role");
    }
    
    public logout(){
        return this.apiService.post("account/logout", null).subscribe(res => {
           localStorage.removeItem("username");
           this.router.navigate(["login"])
        });
    }
    public updatePassword(user:UpdatePassowrd){
        return this.apiService.post("/user/changepassword",user);
    }
    
}