﻿using intaps.cis.domain.Archive;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace intaps.cis.Controllers
{
    [Route("api/dams/[controller]/[action]")]
    [ApiController]
    public class SessionLinkController : Microsoft.AspNetCore.Mvc.Controller
    {
        private readonly IArchiveService _archiveService;
        public SessionLinkController(IArchiveService archiveService)
        {
            _archiveService = archiveService;
        }
        [HttpGet]
        public IActionResult viewfile([FromQuery] String sid, [FromQuery]String file_id)
        {
            base.Response.Cookies.Append(".ASPNetCoreSession", sid);
            return Redirect("/#/dams/file/" + file_id);
        }
        [HttpGet]
        public IActionResult addDoc([FromQuery] String sid, [FromQuery]String file_id)
        {
            base.Response.Cookies.Append(".ASPNetCoreSession", sid);
            return Redirect("/#/dams/file/" + file_id);
        }
    }
}
