using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.FileProviders;

namespace intaps.cis
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length>0 && args[0].Equals("--tools=true"))
            {
                Console.WriteLine("Starting in tools mode...");
                RunToolsMode();
                Console.WriteLine("Tools mode executed ... press enter to exit.");
                Console.ReadLine();
                return;
            }
            IWebHost host = BuildWebHost(args);
            host.Run();
        }
        class ToolsModeHostingEnv : IHostingEnvironment
        {
            public string EnvironmentName {
                get => "Tools";
                set => throw new NotImplementedException();
            }
            public string ApplicationName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public string WebRootPath { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public IFileProvider WebRootFileProvider { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public string ContentRootPath {
                get {
                    return Environment.CurrentDirectory;
                    }
                set => throw new NotImplementedException(); }
            public IFileProvider ContentRootFileProvider { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        }
        static void RunToolsMode()
        {
            int i = 1;
            Dictionary<String, System.Reflection.MethodInfo> methods = new Dictionary<string, System.Reflection.MethodInfo>();
            Dictionary<System.Reflection.MethodInfo, ServerToolMethodAttribute> methodAttributes
                = new Dictionary<System.Reflection.MethodInfo, ServerToolMethodAttribute>();
            foreach (var method in typeof(CISServerTools).GetMethods())
            {
                var atrs = method.GetCustomAttributes(typeof(ServerToolMethodAttribute), false);
                if (atrs.Any())
                {
                    Console.WriteLine($"{i}:{((ServerToolMethodAttribute)atrs[0]).name}");
                    methodAttributes.Add(method, (ServerToolMethodAttribute)atrs[0]);
                    methods.Add(i.ToString(), method);
                    i++;

                }
            }
            Console.WriteLine("Q: Exit");
            do
            {
                Console.Write("Enter tool:");
                string selection = Console.ReadLine();
                if (selection.Equals("Q"))
                    return;

                if (methods.ContainsKey(selection))
                {
                    try
                    {
                        var atr = methodAttributes[methods[selection]];
                        if(atr.pass!=null)
                        {
                            Console.Write("Type pass code:");
                            String typedPass = "";
                            while (true)
                            {
                                ConsoleKeyInfo key = Console.ReadKey(true);
                                if (key.Key == ConsoleKey.Enter)
                                    break;
                                typedPass += key.KeyChar;
                            }
                            if (!typedPass.Equals(atr.pass))
                            {
                                Console.WriteLine("Invalid pass code");
                                continue;
                            }
                        }
                        methods[selection].Invoke(null, new object[] { });
                    }
                    catch (Exception ex)
                    {
                        while (ex != null)
                        {
                            Console.WriteLine(ex.Message);
                            Console.WriteLine(ex.StackTrace);
                            ex = ex.InnerException;
                        }
                    }
                    return;
                }
                else
                    Console.WriteLine("Please enter valid selection");

            }
            while (true);
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseUrls("http://0.0.0.0:5001")
                .UseContentRoot(Directory.GetCurrentDirectory())
                .Build();
        
       /*     .UseHttpSys(options =>
        {
            // The following options are set to default values.
            options.Authentication.Schemes = AuthenticationSchemes.None;
            options.Authentication.AllowAnonymous = true;
            options.MaxConnections = null;
            options.MaxRequestBodySize = null;
            options.UrlPrefixes.Add("http://0.0.0.0:5000");
        })*/
    }
}
