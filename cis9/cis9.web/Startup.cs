using System;
using System.IO;
using intaps.cis.data;
using intaps.cis.domain.Admin;
using intaps.cis.domain.Archive;
using intaps.cis.domain.Extensions;
using intaps.cis.domain.Infrastructure;
using intaps.cis.domain.Infrastructure.Database;
using intaps.cis.domain.Workflows;
using intaps.cis.domain.Workflows.CaseWorkflow;
using intaps.cis.domain.Workflows.DamsWorkflow;
using intaps.cis.domain.Workflows.FileRequest;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Sieve.Services;

namespace intaps.cis
{
    public class Startup
    {
/*        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;    
        }*/

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            this.Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Add Distributed Mem cache
            services.AddDistributedMemoryCache();
            services.AddDataProtection();
            //Add session service
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(30);
                options.CookieName = ".ASPNetCoreSession";
                options.CookiePath = "/";
            });

            services.AddAntiforgery(opts =>
            {
                opts.CookieName = ".ASPNetCoreSession";
                opts.CookiePath = "/";
            });
            services.AddMvc().AddJsonOptions(opts =>
            {
                opts.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Include;
                opts.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });


            

           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }


            app.UseSession();

            DefaultFilesOptions options = new DefaultFilesOptions();
            options.DefaultFileNames.Clear();
            options.DefaultFileNames.Add("index.html");

            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value))
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });

            app.UseCors("AllowAll");
            
            app.UseDefaultFiles(options);
            app.UseStaticFiles();
            app.UseMvc();
          
        }
    }
}