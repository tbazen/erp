function navigateEditorOnAdd(){
    
   if($(".user_page").attr("editor")=="hide"){
       $(".user_page").attr("editor","show");
       $(".user_page").removeClass("col-md-12");
       $(".user_page").addClass("col-md-8");
       $(".edit-user").show();
       $("#add_user_btn").attr("disabled","disabled");
   }
   else
   {
       $(".user_page").attr("editor","hide");
       $(".user_page").removeClass("col-md-8");
       $(".user_page").addClass("col-md-12");
       $(".edit-user").hide();
       $("#add_user_btn").removeAttr("disabled");
   }
}
function navigateEditorOnEdit(){
       $(".user_page").attr("editor","show");
       $(".user_page").removeClass("col-md-12");
       $(".user_page").addClass("col-md-8");
       $(".edit-user").show();
       $("#add_user_btn").attr("disabled","disabled");
}
 $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        
function viewUser(){
    $("#user_detail").modal("show");
}