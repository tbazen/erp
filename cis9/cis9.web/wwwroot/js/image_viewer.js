var Image_Viewer = (function () {
    function Image_Viewer(svg, image) {
        var _this = this;
        this.step = 0.1;
        this.svg = svg;
        this.img = image;
        this.svg.addEventListener("mousewheel", function (event) {
            _this.handleMouseWheel(event);
        }, false);
        this.svg.onmousedown = function (event) {
            _this.handleMouseDown(event);
        };
        this.svg.onmouseup = function (event) {
            _this.handleMouseUp(event);
        };
        this.svg.onmousemove = function (event) {
            _this.handleMoseMove(event);
        };
     
        var r = this.svg.getBoundingClientRect();
        this.img.setAttribute("width", r.width.toString());
        this.img.setAttribute("height", r.height.toString());
        this.dwn = false;
        
    }
    Image_Viewer.prototype.handleMouseDown = function (event) {
        this.dwn = true;
        this.dwn_x = event.x;
        this.dwn_y = event.y;
        this.dwn_img_x = this.img.x.baseVal.value;
        this.dwn_img_y = this.img.y.baseVal.value;
    };
    Image_Viewer.prototype.handleMouseUp = function (event) {
        this.dwn = false;
    };
    Image_Viewer.prototype.handleMoseMove = function (event) {
        var degree=parseFloat(this.img.getAttribute("degree"));
        if (this.dwn == true) {
            if(degree == 90 || degree== -270){
                this.img.setAttribute("y", (-1*(this.dwn_img_x + (event.x - this.dwn_x))).toString());
                this.img.setAttribute("x", (this.dwn_img_y+ (event.y - this.dwn_y)).toString());
            }
            else if(degree==180 || degree== -180){
                this.img.setAttribute("x", (-1*(this.dwn_img_x + (event.x - this.dwn_x))).toString());
                this.img.setAttribute("y", (-1*(this.dwn_img_y + (event.y - this.dwn_y))).toString());
            }
            else if(degree==270 || degree==-90){
                this.img.setAttribute("y", (this.dwn_img_x + (event.x - this.dwn_x)).toString());
                this.img.setAttribute("x", (-1*(this.dwn_img_y+ (event.y - this.dwn_y))).toString());
            }
            else{
                this.img.setAttribute("x", (this.dwn_img_x + (event.x - this.dwn_x)).toString());
                this.img.setAttribute("y", (this.dwn_img_y + (event.y - this.dwn_y)).toString());
            }
            
        }
    };
    Image_Viewer.prototype.parseMeasurement = function (val) {
        return parseFloat(val.substr(0, val.length - 1));
    };
    Image_Viewer.prototype.handleMouseWheel = function (event) {
        var w = parseFloat(this.img.getAttribute("width"));
        var h = parseInt(this.img.getAttribute("height"));
        var x = this.img.x.baseVal.value;
        var y = this.img.y.baseVal.value;
        var r = this.svg.getBoundingClientRect();
        var a = event.x - r.left - x;
        var b = event.y - r.top - y;
        var k = 1;
        var normalizedWheel = -event.wheelDelta / 120;
        k = 1;
        for (var i = 0; i < Math.abs(normalizedWheel); i++) {
            if (normalizedWheel > 0)
                k = k * (1 - this.step);
            else
                k = k * (1 + this.step);
        }
        this.img.setAttribute("width", (w * k).toString());
        this.img.setAttribute("height", (h * k).toString());
        this.img.setAttribute("x", (x - a * (k - 1)).toString());
        this.img.setAttribute("y", (y - b * (k - 1)).toString());
        event.preventDefault();
    };
  
    return Image_Viewer;
})();
//# sourceMappingURL=image_viewer.js.map

var full_screen=(function () {
    function full_screen() {
        var elem = document.getElementById("imageViewer");
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.mozRequestFullScreen) { /* Firefox */
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
            elem.webkitRequestFullscreen();
        } else if (elem.msRequestFullscreen) { /* IE/Edge */
            elem.msRequestFullscreen();
        }
    }
    return full_screen;
})();

var exit_fullScreen=(function () {
    function exit_fullScreen() {
        

            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.mozCancelFullScreen) { /* Firefox */
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
                document.webkitExitFullscreen();
            } else if (document.msExitFullscreen) { /* IE/Edge */
                document.msExitFullscreen();
            }

        }
    
    return exit_fullScreen;
});
