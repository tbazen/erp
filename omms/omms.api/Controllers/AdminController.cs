﻿using Microsoft.AspNetCore.Mvc;
using omms.api.Helper;
using omms.domain.TaskServices;
using System;

namespace omms.api.Controllers.Admin
{
    [Route("api/omms/[controller]")]
    [ApiController]
    public class AdminController : BaseController
    {
        readonly ITaskFacade _facade;
        public AdminController(ITaskFacade facade) => _facade = facade;

        #region Task management


        [HttpGet]
        [Route("gettaskbyid/{taskId}")]
        public IActionResult GetTaskById(Guid taskId)
        {
            try
            {
                return Ok(new { success = true, task = _facade.GetTaskById(taskId) });
            }
            catch (Exception e)
            {
                return StatusCode(501, new { success = false, error = e.Message });

            }
        }

        [HttpGet]
        [Route("getalltasks")]
        public IActionResult GetAllTasks()
        {
            try
            {
                return Ok(new { success = true, tasks = _facade.GetAllTasks() });
            }
            catch (Exception e)
            {


                return StatusCode(501, new { success = false, error = e.Message });
            }
        }

        #endregion

        #region Team management Api



        [Route("remove-team-member")]
        [HttpPost]
        public IActionResult RemoveTeamMember([FromBody] long teamId, string memberId)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.RemoveTeamMember(teamId, memberId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }
        #endregion

        [Route("tasks-sorted-by-date")]
        [HttpGet]
        public IActionResult GetTaskSortedByDate()
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetTaskSortedByDate());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }


        #region task notification
        //[Route("set-notification")]
        //[HttpPost]
        //public IActionResult SetNotification([FromBody]NotificationModel notification)
        //{
        //    try
        //    {
        //        return Ok(_facade.SetNotification(notification));
        //    }
        //    catch (Exception e)
        //    {
        //        return StatusCode(501, new { message = e.Message });
        //    }
        //}


        //[Route("get-notifications/{empId}")]
        //[HttpGet]
        //public IActionResult GetNotification()
        //{
        //    try
        //    {
        //        return Ok(_facade.GetNotification());
        //    }
        //    catch (Exception e)
        //    {
        //        return StatusCode(501, new { message = e.Message });
        //    }
        //}


        //[Route("get-new-notification/{empId}")]
        //[HttpGet]
        //public IActionResult GetNewNotification()
        //{
        //    try
        //    {
        //        return Ok(_facade.GetNewNotifications());
        //    }
        //    catch (Exception e)
        //    {
        //        return StatusCode(501, new { message = e.Message });
        //    }
        //}
        #endregion

        #region Asset managemnet
        [Route("get-asset-byid/{assetId}")]
        [HttpGet]
        public IActionResult GetAssetById(int assetId)
        {
            try
            {
                return Ok(_facade.GetAssetById(assetId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        #endregion 
    }
}