﻿using Microsoft.AspNetCore.Mvc;
using omms.api.Helper;
using omms.domain.TaskServices;
using omms.types.Models;
using System;

namespace omms.api.Controllers
{
    [Route("api/omms/[controller]")]
    [ApiController]
    public class AssetController : BaseController
    {
        readonly ITaskFacade _facade;
        public AssetController(ITaskFacade facade) => _facade = facade;
        [HttpGet]
        [Route("get-asset-form-schema")]
        public IActionResult GetAssetFormSchema()
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetAssetFormSchema());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [HttpGet]
        [Route("get-asset-reading-schema/{assetType}")]
        public IActionResult GetAssetReadingSchema(int assetType)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetReadingSchema(assetType));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }


        [HttpPost]
        [Route("register-asset")]
        public IActionResult RequestAssetRegister([FromBody] AssetRequest asset)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.RequestAssetRegistration(asset, asset.Wfid));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [HttpGet]
        [Route("get-pending-assets")]
        public IActionResult GetPendingAssets()
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetPendingAssets());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }


        //[HttpGet]
        //[Route("get-workitem/{wfid}")]
        //public IActionResult GetWorkItem(Guid wfid)
        //{
        //    try
        //    {
        //        return Ok(_facade.GetWorkItem(wfid));
        //    }
        //    catch (Exception e)
        //    {
        //        return StatusCode(500, new { message = e.Message });
        //    }
        //}

        [HttpGet]
        [Route("approve-asset/{wfid}")]
        public IActionResult ApproveAssetregistration(Guid wfid)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.ApproveAssetRegistration(wfid));

            }
            catch (Exception e)
            {
                return StatusCode(500, new { message = e.Message });
            }
        }

        [HttpGet]
        [Route("get-asset-byid/{assetId}")]
        public IActionResult GetAssetById(int assetId)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetAssetById(assetId));

            }
            catch (Exception e)
            {
                return StatusCode(500, new { message = e.Message });
            }
        }

        [Route("reject-asset-request")]
        [HttpPost]
        public IActionResult RejectApprovalrequest([FromBody]WFRequestModel request)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.RejectAssetRegistration(request));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [HttpGet]
        [Route("get-rejected-assets")]
        public IActionResult GetRejectedAssets()
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetRejectedAssets());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [HttpGet]
        [Route("get-asset-by-location/{lid}")]
        public IActionResult GetAssetByLocation(string lid)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetAssetByLocation(lid));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [Route("add-asset-reading")]
        [HttpPost]
        public IActionResult AddAssetReading([FromBody] AssetReading reading)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.AddAssetReading(reading));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [Route("get-asset-readings/{assetId}")]
        [HttpGet]
        public IActionResult GetAssetReadings(int assetId)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetAllAssetReadings(assetId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [Route("get-asset-byPUKID")]
        [HttpPost]
        public IActionResult GetAssetInfoByPUKID([FromBody]AssetSearchParams searchParams)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetAssetInfoByPUKID(searchParams));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }


        [Route("asset-report")]
        [HttpGet]
        public IActionResult GetAssetReport()
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetAssetReport());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [Route("asset-report-file/{docType}/{filterParams?}")]
        [HttpGet]
        public IActionResult GetTeamReportDocument(string docType, string filterParams)
        {
            try
            {
                byte[] document = _facade.GetAssetReportDocument(docType, filterParams);
                if (docType.ToLower() == RPDocumentType.EXCEL)
                    return File(document, "application/vnd.ms-excel", "asset report.xls");
                return File(document, "application/pdf", "asset report.pdf");
            }
            catch (Exception e)
            {
                return StatusCode(500, new { message = e.Message });
            }
        }
    }
}
