﻿using Microsoft.AspNetCore.Mvc;
using omms.api.Helper;
using omms.domain.TaskServices;
using omms.types.Models;
using omms.types.Models.Thread;
using System;

namespace omms.api.Controllers.Employee
{
    [Route("api/omms/[controller]")]
    [ApiController]
    public class EmployeeController : BaseController
    {


        readonly ITaskFacade _facade;


        public EmployeeController(ITaskFacade facade) => _facade = facade;

        #region Employee specific API
        [Route("employeetask")]
        [HttpPost]
        public IActionResult GetEmplyeeTask()
        {
            try
            {
                return Ok(_facade.GetEmployeeTask());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }


        #endregion

        #region Team API

        [Route("get-team-task")]
        [HttpPost]
        public IActionResult GetTeamTask([FromBody] long teamId)
        {
            try
            {
                return Ok(_facade.GetTeamTask(teamId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }


        //chatroom
        [Route("get-messages/{threadId}")]
        [HttpGet]
        public IActionResult GetMessages(Guid threadId)
        {
            try
            {
                return Ok(_facade.GetMessages(threadId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }



        [Route("send-message")]
        [HttpPost]
        public IActionResult SendMessage([FromBody]MessageRequest message)
        {
            try
            {
                return Ok(_facade.SendMessage(message));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [Route("employee-thread/{employeeId}")]
        [HttpGet]
        public IActionResult GetEmployeeThreads(Guid employeeId)
        {
            try
            {
                return Ok(_facade.GetEmployeeThreads(employeeId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        #endregion

        #region Shared Services API
        [Route("search-task-byid")]
        [HttpPost]
        public IActionResult SearchTaskById(Guid taskId)
        {
            try
            {
                return Ok(_facade.GetTaskById(taskId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [Route("get-team")]
        [HttpPost]
        public IActionResult GetTeam([FromBody] long teamId)
        {
            try
            {
                return Ok(_facade.GetTeam(teamId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        //status management service

        [Route("updatetaskstatus")]
        [HttpPost]
        public IActionResult UpdateTaskStatus([FromBody]TaskStatusModel newStatus)
        {
            try
            {
                return Ok(value: _facade.UpdateTaskStatus(newStatus));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [Route("task-status-history")]
        [HttpPost]
        public IActionResult GetTaskStatusHistory([FromBody]Guid taskId)
        {
            try
            {
                return Ok(_facade.GetTaskStatusHistory(taskId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }
        public IActionResult GetStatusById(int statusId)
        {
            try
            {
                return Ok(_facade.GetStatusById(statusId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        public IActionResult ChangeTaskStatus(TaskStatusModel newStatus)
        {
            try
            {
                return Ok(_facade.ChangeTaskStatus(newStatus));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }


        //failure repor
        [Route("report-failure")]
        [HttpPost]
        public IActionResult ReportFailure([FromBody]FailureModel failure)
        {
            try
            {
                return Ok(_facade.ReportFailure(failure));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [Route("get-status-failure")]
        [HttpPost]
        public IActionResult GetStatusFailureReport([FromBody]int statusId)
        {
            try
            {
                return Ok(_facade.GetStatusFailureReport(statusId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [Route("add-maintenance-procedure")]
        [HttpPost]
        public IActionResult AddMaintenanceProcedure([FromBody]MaintenanceProc procedure)
        {
            try
            {
                return Ok(_facade.AddMaintenanceProcedure(procedure));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [Route("maintenance-procedires")]
        [HttpGet]
        public IActionResult GetMaintenanceProcedures()
        {
            try
            {
                return Ok(_facade.GetMaintenanceProcedures());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }



        [Route("latest-task-status")]
        [HttpPost]
        public IActionResult GetLatestStatus([FromBody]Guid taskId)
        {
            try
            {
                return Ok(_facade.GetLatestStatus(taskId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }
        [Route("team-members/{teamId}")]
        [HttpGet]
        public IActionResult GetTeamMembers(long teamId)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetTeamMembers(teamId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [Route("download/{documentId}")]
        [HttpGet]
        public IActionResult GetFileById(Guid documentId)
        {
            try
            {
                var document = _facade.GetDocumentById(documentId);
                if (document == null)
                    return NotFound();
                return File(document.File, document.Mimetype, null);
            }
            catch (Exception e)
            {
                return StatusCode(500, new { message = e.Message });
            }
        }


        [Route("get-all-asset")]
        [HttpGet]
        public IActionResult GetAllAssets()
        {
            try
            {
                return Ok(_facade.GetAllAssets());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [Route("get-reading-byid/{readingId}")]
        [HttpGet]
        public IActionResult GetAssetReadingById(int readingId)
        {
            try
            {
                //return Ok(_facade.GetAssetReadingById(readingId));
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }
        #endregion


        [Route("get-task-by-type/{type}")]
        [HttpGet]
        public IActionResult GetTaskByType(TaskType type)
        {
            try
            {
                return Ok(_facade.GetTaskByType(type));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }


        [Route("get-all-employees")]
        [HttpGet]
        public IActionResult GetAllEmployees()
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetAllEmployees());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [Route("get-pending-items/{wfType}")]
        [HttpGet]
        public IActionResult GetPendingItems(int wfType)
        {
            try
            {
                return Ok(_facade.GetPendingItems(wfType));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }

        }
    }
}