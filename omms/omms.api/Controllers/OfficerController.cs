﻿using Microsoft.AspNetCore.Mvc;
using omms.api.Helper;
using omms.domain.TaskServices;
using omms.types.Models;
using System;

namespace omms.api.Controllers
{

    //Todo change the api name 
    [Route("api/omms/officer")]
    [ApiController]
    public class OfficerController : BaseController
    {
        readonly ITaskFacade _facade;
        public OfficerController(ITaskFacade facade) => _facade = facade;

        #region Task Registration
        [Route("createtask")]
        [HttpPost]
        public IActionResult CreateTask([FromBody] TaskRequest model)
        {
            try
            {
                return Ok(_facade.CreateTask(model));
            }
            catch (Exception e)
            {
                return StatusCode(500, new { success = false, message = e.Message });
            }
        }


        [Route("updatetask")]
        [HttpPost]
        public IActionResult UpdateTask([FromBody] TaskRequest task)
        {
            try
            {
                return Ok(new { success = true, task = _facade.UpdateTask(task) });
            }
            catch (Exception e)
            {
                return StatusCode(501, new { succcess = false, error = e.Message });
            }
        }


        [Route("assign-task")]
        [HttpPost]
        public IActionResult AssignTask([FromBody]Assignee assignee)
        {
            try
            {
                return Ok(_facade.AssignTask(assignee));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }
        #endregion



        [HttpGet]
        [Route("cancel-registration/{wfid}")]
        public IActionResult CancelRegistrationRequest(Guid wfid)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.CancelRegistrationRequest(wfid));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

    }
}
