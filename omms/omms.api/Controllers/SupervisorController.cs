﻿using Microsoft.AspNetCore.Mvc;
using omms.domain.TaskServices;
using System;

namespace omms.api.Controllers
{

    [Route("api/omms/admin")]
    [ApiController]
    //[Authorize(Policy = "Technical Supervisor")]
    public class SupervisorController : Controller
    {
        readonly ITaskFacade _facade;
        #region Task Approval Api

        public SupervisorController(ITaskFacade facade) => _facade = facade;


        [Route("deletetask/{taskId}")]
        [HttpGet]
        public IActionResult DeleteTask(Guid taskId)
        {
            try
            {
                return Json(new { success = true, response = _facade.DeleteTask(taskId) });
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e);
                return StatusCode(500, new { success = false, message = e.Message });
            }
        }



        #endregion
    }
}
