﻿using Microsoft.AspNetCore.Mvc;
using omms.api.Helper;
using omms.domain.TaskServices;
using omms.types.Models;
using System;

namespace omms.api.Controllers
{
    [Route("api/omms/[controller]")]
    [ApiController]
    public class TaskController : BaseController
    {
        readonly ITaskFacade _facade;
        public TaskController(ITaskFacade facade) => _facade = facade;

        [HttpGet]
        [Route("asset-tasks/{assetId}")]
        public IActionResult GetAssetsTasks(int assetId)
        {
            try
            {
                //_facade.SetSession(GetSession());
                return Ok(_facade.GetAssetTasks(assetId));
            }
            catch (Exception e)
            {
                return StatusCode(500, new { message = e.Message });
            }
        }


        [HttpPost]
        [Route("request-task-registration")]
        public IActionResult RequestTaskRegister([FromBody] TaskRequest task)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.RequestTaskRegistration(task, task.Wfid));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [HttpGet]
        [Route("get-pending-tasks")]
        public IActionResult GetPendingTasks()
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetPendingTasks());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [HttpGet]
        [Route("approve-task/{wfid}")]
        public IActionResult ApproveTaskRegistration(Guid wfid)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.ApproveTaskRegistration(wfid));

            }
            catch (Exception e)
            {
                return StatusCode(500, new { message = e.Message });
            }
        }

        [Route("reject-task-request")]
        [HttpPost]
        public IActionResult RejectApprovalRequest(WFRequestModel request)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.RejectTaskRegistration(request));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [HttpGet]
        [Route("get-rejected-tasks")]
        public IActionResult GetRejectedTasks()
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetRejectedTasks());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }


        [HttpGet]
        [Route("get-task-notes/{taskId}")]
        public IActionResult GettaskNote(Guid taskId)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetTaskNotes(taskId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }




        [HttpGet]
        [Route("get-latest-note/{taskId}")]
        public IActionResult GetLatestTaskNote(Guid taskId)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetLatestTaskNote(taskId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }


        [HttpPost]
        [Route("add-note")]
        public IActionResult AddNote([FromBody] NoteRequest request)
        {
            try
            {

                _facade.SetSession(GetSession());
                return Ok(_facade.AddNote(request));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }


        [HttpPost]
        [Route("rqst-cancel")]
        public IActionResult RequestCancellation(WFRequestModel request)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.RequestTaskCancellation(request));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }



        [HttpPost]
        [Route("rqst-terminate")]
        public IActionResult RequestTermination(WFRequestModel request)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.RequestTaskTermination(request));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [HttpPost]
        [Route("rqst-complete")]
        public IActionResult RequestCompletion(WFRequestModel request)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.RequestTaskCompletion(request));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [HttpGet]
        [Route("approve-state/{wfid}")]
        public IActionResult ApproveState(Guid wfid)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.ApproveTaskState(wfid));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }


        [HttpPost]
        [Route("reject-state")]
        public IActionResult RejectState([FromBody]WFRequestModel request)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.RejectTaskState(request));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [HttpGet]
        [Route("get-task-by-state/{state}")]
        public IActionResult GetTaskByState(int state)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetTaskByState(state));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }


        [HttpGet]
        [Route("get-individual-tasks")]
        public IActionResult GetIndevidualTasks()
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetIndividualTasks());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [HttpGet]
        [Route("get-team-tasks")]
        public IActionResult GetTeamTasks()
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetTeamTasks());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [HttpGet]
        [Route("get-tasks-by-tid/{teamId}")]
        public IActionResult GetTaskByTeamId(int teamId)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GettasksByTeamId(teamId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }



        [Route("get-task-dependency/{taskId}")]
        [HttpGet]
        public IActionResult GetTaskDependency(Guid taskId)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetTaskDependency(taskId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [Route("get-task-byID/{taskId}")]
        [HttpGet]
        public IActionResult GetTaskById(Guid taskId)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetTaskById(taskId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }


        [Route("task-metadata")]
        [HttpGet]
        public IActionResult GetTaskMetaData()
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetTaskMetaData());

            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }


        [Route("update-task-assignee")]
        [HttpPost]
        public IActionResult UpdateTaskAssignee([FromBody]AssigneeViewModel assignee)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.UpdateTaskAssignee(assignee));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [Route("employee-report")]
        [HttpGet]
        public IActionResult GetEmployeeReport()
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetEmployeeReport());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }


        [Route("task-report")]
        [HttpGet]
        public IActionResult GetTaskReport()
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetTaskReport());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }


        [Route("employee-report-file/{docType}/{filterParams?}")]
        [HttpGet]
        public IActionResult GetEmployeeReportDocument(string docType, string filterParams)
        {
            try
            {
                var document = _facade.GetEmployeeReportDocument(docType, filterParams);
                if (docType.ToLower() == RPDocumentType.EXCEL)
                    return File(document, "application/vnd.ms-excel", "employee report.xls");
                return File(document, "application/pdf", "employee report.pdf");
            }
            catch (Exception e)
            {
                return StatusCode(500, new { message = e.Message });
            }
        }

        [Route("task-report-file/{docType}/{filterParams?}")]
        [HttpGet]
        public IActionResult GetTaskReportDocument(string docType, string filterParams)
        {
            try
            {
                byte[] document = _facade.GetTaskReportDocument(docType, filterParams);
                if (docType.ToLower() == RPDocumentType.EXCEL)
                    return File(document, "application/vnd.ms-excel", "task report.xls");
                return File(document, "application/pdf", "task report.pdf");
            }
            catch (Exception e)
            {
                return StatusCode(500, new { message = e.Message });
            }
        }



    }
}