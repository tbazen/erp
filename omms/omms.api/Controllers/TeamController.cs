﻿using Microsoft.AspNetCore.Mvc;
using omms.api.Helper;
using omms.domain.TaskServices;
using omms.types.Models;
using System;

namespace omms.api.Controllers
{
    [Route("api/omms/[controller]")]
    [ApiController]
    public class TeamController : BaseController
    {
        ITaskFacade _facade;
        public TeamController(ITaskFacade facade) => _facade = facade;
        [Route("request-team-registration")]
        [HttpPost]
        public IActionResult RequestTeamRegistration([FromBody] TeamModel model)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.RequestTeamRegistration(model, model.Wfid));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { success = false, message = e.Message });
            }
        }

        [HttpGet]
        [Route("get-pending-teams")]
        public IActionResult GetPendingTeams()
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetPendingTeams());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [HttpGet]
        [Route("approve-team/{wfid}")]
        public IActionResult ApproveTeamRegistration(Guid wfid)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.ApproveTeamRegistration(wfid));

            }
            catch (Exception e)
            {
                return StatusCode(500, new { message = e.Message });
            }
        }

        [HttpGet]
        [Route("get-team-byid/{teamId}")]
        public IActionResult GetTeamBiId(long teamId)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetTeamById(teamId));
            }
            catch (Exception e)
            {
                return StatusCode(500, new { message = e.Message });
            }
        }

        [Route("reject-team-request")]
        [HttpPost]
        public IActionResult RejectApprovalrequest([FromBody]WFRequestModel request)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.RejectTeamRegistration(request));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [HttpGet]
        [Route("get-rejected-teams")]
        public IActionResult GetRejectedTeams()
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetRejectedTeams());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [Route("teamlist/{employeeId?}")]
        [HttpGet]
        public IActionResult GetTeamList(string employeeId = null)
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetTeamList(employeeId));
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [Route("team-report")]
        [HttpGet]
        public IActionResult GetTeamReport()
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetTeamReport());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [Route("team-report-file/{docType}/{filterParams?}")]
        [HttpGet]
        public IActionResult GetTeamReportDocument(string docType, string filterParams)
        {
            try
            {
                byte[] document = _facade.GetTeamReportDocument(docType, filterParams);
                if (docType.ToLower() == RPDocumentType.EXCEL)
                    return File(document, "application/vnd.ms-excel", "team report.xls");
                return File(document, "application/pdf", "team report.pdf");
            }
            catch (Exception e)
            {
                return StatusCode(500, new { message = e.Message });
            }
        }

    }
}
