﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using omms.api.Helper;
using omms.domain.User;
using System;

namespace omms.api.Controllers
{
    [Route("api/omms/[controller]")]
    [ApiController]
    public class UserController : BaseController
    {
        readonly IUserService _userService;
        readonly IConfiguration Configuration;
        public UserController(IConfiguration _configuration, IUserService userService)
        {
            Configuration = _configuration;
            _userService = userService;
        }

        [Route("login")]
        [HttpPost]
        public IActionResult Login([FromBody]LoginViewModel loginModel)
        {
            try
            {
                string token;
                var session = _userService.ValidateCredential(loginModel.UserName, loginModel.Password, out token);
                bool hasRole = false;
                foreach (var role in session.Roles)
                {
                    if (role == loginModel.Role)
                    {
                        session.Role = role;
                        hasRole = true;
                        break;
                    }
                }
                if (!hasRole)
                    throw new AccessViolationException("User role is not valid");

                session.Role = loginModel.Role;
                var serializedSession = JsonConvert.SerializeObject(session);

                HttpContext.Session.SetSession("omms-session", session);
                return Ok(new
                {
                    status = true,
                    message = "Successful",
                    response = session,
                });
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }



        [HttpGet]
        [Route("profile")]
        public IActionResult Profile()
        {
            var session = GetSession();
            return Ok(new
            {
                Id = session.EmployeeID,
                UserName = session.Username,
                Role = session.Role
            });
        }


        [Route("logout")]
        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return Json(true);
        }

        [Route("checkuser")]
        [HttpGet]
        public IActionResult CheckUser()
        {
            try
            {
                GetSession();
                return Ok(true);
            }
            catch (Exception e)
            {
                return StatusCode(500, new { message = e.Message });
            }
        }

    }
}