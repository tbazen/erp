﻿using Microsoft.AspNetCore.Mvc;
using omms.api.Filters;
using omms.api.Helper;
using omms.domain.TaskServices;
using System;

namespace omms.api.Controllers
{
    [Roles]
    [Route("api/omms/[controller]")]
    [ApiController]
    public class UtilController : BaseController
    {
        ITaskFacade _facade;
        public UtilController(ITaskFacade facade) => _facade = facade;

        [HttpGet]
        [Route("get-metadata")]
        public IActionResult GetMetaData()
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.GetMetaData());
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        [HttpGet]
        [Route("seen-inv-tasks")]
        public IActionResult MarkIndvTasksAsSeen(){
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.MarkIndvTasksAsSeen());
            }
            catch(Exception e)
            {
                return StatusCode(501,new {message = e.Message});
            }
        }

        [HttpGet]
        [Route("seen-team-tasks")]
        public IActionResult MarkTeamTasksAsSeen()
        {
            try
            {
                _facade.SetSession(GetSession());
                return Ok(_facade.MarkTeamTasksAsSeen());
            }   
            catch(Exception e)
            {
                return StatusCode(501,new {message = e.Message});                
            }         
        }
    }
}
