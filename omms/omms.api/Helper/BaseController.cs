﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using omms.types.Models;
using System;

namespace omms.api.Helper
{
    public class BaseController : Controller
    {
        public UserSession GetSession()
        {

            var session = HttpContext.Session.GetString("omms-session");
            if (session == null)
            {
                throw new Exception("Not Authenticated");
            }
            return JsonConvert.DeserializeObject<UserSession>(session);
        }

    }
}
