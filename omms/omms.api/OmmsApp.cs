﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using omms.data.Entities;
using omms.data.GisEntities;
using omms.domain.TaskServices;
using omms.domain.TaskServices.AdminServices;
using omms.domain.TaskServices.EmployeeServices;
using omms.domain.TaskServices.SharedServices;
using omms.domain.User;
using omms.domain.Workflow.services;

namespace omms.api
{
    public class OmmsApp
    {
        public static void InitWebApp(IServiceCollection services, IConfiguration Configuration)
        {

            //Taken Code
            var _connString = Configuration.GetConnectionString("omms-connection");
            var _gisConnString = Configuration.GetConnectionString("omms-gis-connection");


            services.AddDbContext<OmmsContext>(
                options => options.UseSqlServer(_connString));
            services.AddDbContext<OmmsGisContext>(
                option => option.UseNpgsql(_gisConnString));

            //inject services
            InjectService(services);
        }


        static void InjectService(IServiceCollection services)
        {
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IEmployeeService, EmployeeService>();
            services.AddTransient<IAdminService, AdminService>();
            services.AddTransient<ITeamService, TeamService>();
            services.AddTransient<ISharedService, SharedService>();
            services.AddTransient<ITaskFacade, TaskFacade>();
            services.AddTransient<IWorkflowService, WorkflowService>();
        }
    }

}
