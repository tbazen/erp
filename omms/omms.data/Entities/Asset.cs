﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class Asset
    {
        public Asset()
        {
            AssetImages = new HashSet<AssetImages>();
            AssetRecord = new HashSet<AssetRecord>();
            AssetTask = new HashSet<AssetTask>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string GeoLocation { get; set; }
        public int TypeId { get; set; }
        public long Pkuid { get; set; }

        public virtual AssetType Type { get; set; }
        public virtual ICollection<AssetImages> AssetImages { get; set; }
        public virtual ICollection<AssetRecord> AssetRecord { get; set; }
        public virtual ICollection<AssetTask> AssetTask { get; set; }
    }
}
