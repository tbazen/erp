﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class AssetImages
    {
        public int Id { get; set; }
        public int AssetId { get; set; }
        public Guid DocumentId { get; set; }

        public virtual Asset Asset { get; set; }
        public virtual Document Document { get; set; }
    }
}
