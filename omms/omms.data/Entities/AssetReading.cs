﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class AssetReading
    {
        public int Id { get; set; }
        public int AssetId { get; set; }
        public DateTime ReadingDate { get; set; }
        public Guid ReadingBy { get; set; }
        public string Reading { get; set; }

        public virtual Asset Asset { get; set; }
        public virtual Employee ReadingByNavigation { get; set; }
    }
}
