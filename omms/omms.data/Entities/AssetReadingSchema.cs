﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class AssetReadingSchema
    {
        public int Id { get; set; }
        public int AssetType { get; set; }
        public string Name { get; set; }
        public string ValueType { get; set; }
        public int Priority { get; set; }

        public virtual AssetType AssetTypeNavigation { get; set; }
    }
}
