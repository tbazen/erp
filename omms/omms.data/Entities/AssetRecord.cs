﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class AssetRecord
    {
        public int Id { get; set; }
        public int AssetId { get; set; }
        public DateTime ReadingDate { get; set; }
        public string Reading { get; set; }
        public string ReadingBy { get; set; }

        public virtual Asset Asset { get; set; }
    }
}
