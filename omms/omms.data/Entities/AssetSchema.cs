﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class AssetSchema
    {
        public int Id { get; set; }
        public int TypeId { get; set; }
        public string AttributeName { get; set; }
        public string ValueType { get; set; }
        public int Priority { get; set; }

        public virtual AssetType Type { get; set; }
    }
}
