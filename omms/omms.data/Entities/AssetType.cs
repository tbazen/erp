﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class AssetType
    {
        public AssetType()
        {
            Asset = new HashSet<Asset>();
            AssetReadingSchema = new HashSet<AssetReadingSchema>();
            AssetSchema = new HashSet<AssetSchema>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Asset> Asset { get; set; }
        public virtual ICollection<AssetReadingSchema> AssetReadingSchema { get; set; }
        public virtual ICollection<AssetSchema> AssetSchema { get; set; }
    }
}
