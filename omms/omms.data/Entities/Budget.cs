﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class Budget
    {
        public Budget()
        {
            TaskBudget = new HashSet<TaskBudget>();
        }

        public int Id { get; set; }
        public decimal EstimatedCost { get; set; }
        public decimal? ActualCost { get; set; }

        public virtual ICollection<TaskBudget> TaskBudget { get; set; }
    }
}
