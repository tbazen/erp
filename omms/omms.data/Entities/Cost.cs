﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class Cost
    {
        public int Id { get; set; }
        public decimal Expense { get; set; }
    }
}
