﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class Document
    {
        public Document()
        {
            AssetImages = new HashSet<AssetImages>();
            IncidentTaskImages = new HashSet<IncidentTaskImages>();
            MaintenanceProcedure = new HashSet<MaintenanceProcedure>();
            Message = new HashSet<Message>();
            NoteImages = new HashSet<NoteImages>();
            TaskWarranty = new HashSet<TaskWarranty>();
        }

        public Guid Id { get; set; }
        public long Date { get; set; }
        public string Ref { get; set; }
        public string Note { get; set; }
        public string Mimetype { get; set; }
        public int? Type { get; set; }
        public long? Aid { get; set; }
        public string Filename { get; set; }
        public byte[] File { get; set; }
        public string OverrideFilePath { get; set; }

        public virtual DocumentType TypeNavigation { get; set; }
        public virtual ICollection<AssetImages> AssetImages { get; set; }
        public virtual ICollection<IncidentTaskImages> IncidentTaskImages { get; set; }
        public virtual ICollection<MaintenanceProcedure> MaintenanceProcedure { get; set; }
        public virtual ICollection<Message> Message { get; set; }
        public virtual ICollection<NoteImages> NoteImages { get; set; }
        public virtual ICollection<TaskWarranty> TaskWarranty { get; set; }
    }
}
