﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class Employee
    {
        public Employee()
        {
            EmployeeTask = new HashSet<EmployeeTask>();
            Task = new HashSet<Task>();
            TaskStatus = new HashSet<TaskStatus>();
            TeamMembers = new HashSet<TeamMembers>();
            ThreadMessage = new HashSet<ThreadMessage>();
        }

        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }

        public virtual RoleType Role { get; set; }
        public virtual ICollection<EmployeeTask> EmployeeTask { get; set; }
        public virtual ICollection<Task> Task { get; set; }
        public virtual ICollection<TaskStatus> TaskStatus { get; set; }
        public virtual ICollection<TeamMembers> TeamMembers { get; set; }
        public virtual ICollection<ThreadMessage> ThreadMessage { get; set; }
    }
}
