﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class EmployeeTask
    {
        public Guid TaskId { get; set; }
        public DateTime DateAssigned { get; set; }
        public string EmployeeId { get; set; }

        public virtual Task Task { get; set; }
    }
}
