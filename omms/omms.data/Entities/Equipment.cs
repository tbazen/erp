﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class Equipment
    {
        public Equipment()
        {
            TaskEquipment = new HashSet<TaskEquipment>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<TaskEquipment> TaskEquipment { get; set; }
    }
}
