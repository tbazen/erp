﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class Failure
    {
        public Failure()
        {
            StatusMaintenanceProc = new HashSet<StatusMaintenanceProc>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public Guid Id { get; set; }

        public virtual ICollection<StatusMaintenanceProc> StatusMaintenanceProc { get; set; }
    }
}
