﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class IncidentTask
    {
        public Guid TaskId { get; set; }
        public string GeoLocation { get; set; }
        public string Incident { get; set; }
        public string Description { get; set; }
        public string ReportedBy { get; set; }

        public virtual Task Task { get; set; }
    }
}
