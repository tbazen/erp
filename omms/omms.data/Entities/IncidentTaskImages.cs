﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class IncidentTaskImages
    {
        public Guid ImageId { get; set; }
        public Guid TaskId { get; set; }

        public virtual Document Image { get; set; }
        public virtual Task Task { get; set; }
    }
}
