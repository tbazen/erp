﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class MailSchedule
    {
        public int MailId { get; set; }
        public DateTime Schedule { get; set; }
        public int StateId { get; set; }

        public virtual MailStore Mail { get; set; }
        public virtual MailState State { get; set; }
    }
}
