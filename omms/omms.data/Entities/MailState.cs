﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class MailState
    {
        public MailState()
        {
            MailSchedule = new HashSet<MailSchedule>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<MailSchedule> MailSchedule { get; set; }
    }
}
