﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class MailStore
    {
        public MailStore()
        {
            MailSchedule = new HashSet<MailSchedule>();
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public Guid TaskId { get; set; }

        public virtual Task Task { get; set; }
        public virtual ICollection<MailSchedule> MailSchedule { get; set; }
    }
}
