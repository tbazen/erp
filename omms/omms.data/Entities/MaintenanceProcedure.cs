﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class MaintenanceProcedure
    {
        public MaintenanceProcedure()
        {
            StatusMaintenanceProc = new HashSet<StatusMaintenanceProc>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? Document { get; set; }

        public virtual Document DocumentNavigation { get; set; }
        public virtual ICollection<StatusMaintenanceProc> StatusMaintenanceProc { get; set; }
    }
}
