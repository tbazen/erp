﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class Message
    {
        public Message()
        {
            ThreadMessage = new HashSet<ThreadMessage>();
        }

        public string TextMsg { get; set; }
        public Guid? Document { get; set; }
        public DateTime Date { get; set; }
        public Guid Id { get; set; }

        public virtual Document DocumentNavigation { get; set; }
        public virtual ICollection<ThreadMessage> ThreadMessage { get; set; }
    }
}
