﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class NoteImages
    {
        public int Id { get; set; }
        public int NoteId { get; set; }
        public Guid DocumentId { get; set; }

        public virtual Document Document { get; set; }
        public virtual TaskNote Note { get; set; }
    }
}
