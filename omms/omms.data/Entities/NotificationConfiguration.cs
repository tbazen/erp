﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class NotificationConfiguration
    {
        public Guid TaskId { get; set; }
        public int? DaysBeforeStart { get; set; }
        public int? DaysBeforeEnd { get; set; }
        public string NotificationMessage { get; set; }
        public int? OnState { get; set; }
        public int Id { get; set; }
        public bool Seen { get; set; }

        public virtual StatusType OnStateNavigation { get; set; }
        public virtual Task Task { get; set; }
    }
}
