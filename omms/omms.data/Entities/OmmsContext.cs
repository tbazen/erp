﻿using Microsoft.EntityFrameworkCore;

namespace omms.data.Entities
{
    public partial class OmmsContext : DbContext
    {
        public OmmsContext()
        {
        }

        public OmmsContext(DbContextOptions<OmmsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Asset> Asset { get; set; }
        public virtual DbSet<AssetImages> AssetImages { get; set; }
        public virtual DbSet<AssetReadingSchema> AssetReadingSchema { get; set; }
        public virtual DbSet<AssetRecord> AssetRecord { get; set; }
        public virtual DbSet<AssetSchema> AssetSchema { get; set; }
        public virtual DbSet<AssetTask> AssetTask { get; set; }
        public virtual DbSet<AssetType> AssetType { get; set; }
        public virtual DbSet<Budget> Budget { get; set; }
        public virtual DbSet<Cost> Cost { get; set; }
        public virtual DbSet<Document> Document { get; set; }
        public virtual DbSet<DocumentType> DocumentType { get; set; }
        public virtual DbSet<EmployeeTask> EmployeeTask { get; set; }
        public virtual DbSet<Equipment> Equipment { get; set; }
        public virtual DbSet<Failure> Failure { get; set; }
        public virtual DbSet<IncidentTask> IncidentTask { get; set; }
        public virtual DbSet<IncidentTaskImages> IncidentTaskImages { get; set; }
        public virtual DbSet<MailSchedule> MailSchedule { get; set; }
        public virtual DbSet<MailState> MailState { get; set; }
        public virtual DbSet<MailStore> MailStore { get; set; }
        public virtual DbSet<MaintenanceProcedure> MaintenanceProcedure { get; set; }
        public virtual DbSet<MaintenanceType> MaintenanceType { get; set; }
        public virtual DbSet<Message> Message { get; set; }
        public virtual DbSet<NoteImages> NoteImages { get; set; }
        public virtual DbSet<NotificationConfiguration> NotificationConfiguration { get; set; }
        public virtual DbSet<PeriodicTask> PeriodicTask { get; set; }
        public virtual DbSet<PriorityType> PriorityType { get; set; }
        public virtual DbSet<StatusMaintenanceProc> StatusMaintenanceProc { get; set; }
        public virtual DbSet<StatusType> StatusType { get; set; }
        public virtual DbSet<Task> Task { get; set; }
        public virtual DbSet<TaskAsigneeType> TaskAsigneeType { get; set; }
        public virtual DbSet<TaskAssignee> TaskAssignee { get; set; }
        public virtual DbSet<TaskBudget> TaskBudget { get; set; }
        public virtual DbSet<TaskDependency> TaskDependency { get; set; }
        public virtual DbSet<TaskEquipment> TaskEquipment { get; set; }
        public virtual DbSet<TaskNote> TaskNote { get; set; }
        public virtual DbSet<TaskNotification> TaskNotification { get; set; }
        public virtual DbSet<TaskPriority> TaskPriority { get; set; }
        public virtual DbSet<TaskStatus> TaskStatus { get; set; }
        public virtual DbSet<TaskThread> TaskThread { get; set; }
        public virtual DbSet<TaskType> TaskType { get; set; }
        public virtual DbSet<TaskWarranty> TaskWarranty { get; set; }
        public virtual DbSet<Team> Team { get; set; }
        public virtual DbSet<TeamMembers> TeamMembers { get; set; }
        public virtual DbSet<TeamTask> TeamTask { get; set; }
        public virtual DbSet<Thread> Thread { get; set; }
        public virtual DbSet<ThreadMessage> ThreadMessage { get; set; }
        public virtual DbSet<ThreadStateType> ThreadStateType { get; set; }
        public virtual DbSet<Workflow> Workflow { get; set; }
        public virtual DbSet<WorkflowItem> WorkflowItem { get; set; }
        public virtual DbSet<WorkflowType> WorkflowType { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.2-servicing-10034");

            modelBuilder.Entity<Asset>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("asset_pk")
                    .ForSqlServerIsClustered(false);

                entity.ToTable("asset", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.GeoLocation)
                    .IsRequired()
                    .HasColumnName("geo_location")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Pkuid).HasColumnName("pkuid");

                entity.Property(e => e.TypeId).HasColumnName("type_id");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.Asset)
                    .HasForeignKey(d => d.TypeId)
                    .HasConstraintName("asset_asset_type_id_fk");
            });

            modelBuilder.Entity<AssetImages>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("asset_images_pk")
                    .ForSqlServerIsClustered(false);

                entity.ToTable("asset_images", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AssetId).HasColumnName("asset_id");

                entity.Property(e => e.DocumentId).HasColumnName("document_id");

                entity.HasOne(d => d.Asset)
                    .WithMany(p => p.AssetImages)
                    .HasForeignKey(d => d.AssetId)
                    .HasConstraintName("asset_images_asset_id_fk");

                entity.HasOne(d => d.Document)
                    .WithMany(p => p.AssetImages)
                    .HasForeignKey(d => d.DocumentId)
                    .HasConstraintName("asset_images_document_id_fk");
            });

            modelBuilder.Entity<AssetReadingSchema>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("asset_reading_schema_pk")
                    .ForSqlServerIsClustered(false);

                entity.ToTable("asset_reading_schema", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AssetType).HasColumnName("asset_type");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Priority).HasColumnName("priority");

                entity.Property(e => e.ValueType)
                    .IsRequired()
                    .HasColumnName("value_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.AssetTypeNavigation)
                    .WithMany(p => p.AssetReadingSchema)
                    .HasForeignKey(d => d.AssetType)
                    .HasConstraintName("asset_reading_schema_asset_type_id_fk");
            });

            modelBuilder.Entity<AssetRecord>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("asset_reading_pk")
                    .ForSqlServerIsClustered(false);

                entity.ToTable("asset_record", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AssetId).HasColumnName("asset_id");

                entity.Property(e => e.Reading)
                    .IsRequired()
                    .HasColumnName("reading")
                    .HasColumnType("text");

                entity.Property(e => e.ReadingBy)
                    .IsRequired()
                    .HasColumnName("reading_by")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ReadingDate)
                    .HasColumnName("reading_date")
                    .HasColumnType("date");

                entity.HasOne(d => d.Asset)
                    .WithMany(p => p.AssetRecord)
                    .HasForeignKey(d => d.AssetId)
                    .HasConstraintName("asset_reading_asset_id_fk");
            });

            modelBuilder.Entity<AssetSchema>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("asset_schema_pk")
                    .ForSqlServerIsClustered(false);

                entity.ToTable("asset_schema", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AttributeName)
                    .IsRequired()
                    .HasColumnName("attribute_name")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Priority).HasColumnName("priority");

                entity.Property(e => e.TypeId).HasColumnName("type_id");

                entity.Property(e => e.ValueType)
                    .IsRequired()
                    .HasColumnName("value_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.AssetSchema)
                    .HasForeignKey(d => d.TypeId)
                    .HasConstraintName("asset_schema_asset_type_id_fk");
            });

            modelBuilder.Entity<AssetTask>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("asset_task_pk")
                    .ForSqlServerIsClustered(false);

                entity.ToTable("asset_task", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AssetId).HasColumnName("asset_id");

                entity.Property(e => e.TaskId).HasColumnName("task_id");

                entity.HasOne(d => d.Asset)
                    .WithMany(p => p.AssetTask)
                    .HasForeignKey(d => d.AssetId)
                    .HasConstraintName("asset_task_asset_id_fk");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.AssetTask)
                    .HasForeignKey(d => d.TaskId)
                    .HasConstraintName("asset_task_task_id_fk");
            });

            modelBuilder.Entity<AssetType>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("asset_type_pk")
                    .ForSqlServerIsClustered(false);

                entity.ToTable("asset_type", "lb");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Budget>(entity =>
            {
                entity.ToTable("budget", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ActualCost)
                    .HasColumnName("actual_cost")
                    .HasColumnType("decimal(30, 10)");

                entity.Property(e => e.EstimatedCost)
                    .HasColumnName("estimated_cost")
                    .HasColumnType("decimal(30, 10)");
            });

            modelBuilder.Entity<Cost>(entity =>
            {
                entity.ToTable("cost", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Expense)
                    .HasColumnName("expense")
                    .HasColumnType("decimal(30, 10)");
            });

            modelBuilder.Entity<Document>(entity =>
            {
                entity.ToTable("document", "doc");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Aid).HasColumnName("aid");

                entity.Property(e => e.Date).HasColumnName("date");

                entity.Property(e => e.File)
                    .IsRequired()
                    .HasColumnName("file");

                entity.Property(e => e.Filename)
                    .HasColumnName("filename")
                    .HasMaxLength(100);

                entity.Property(e => e.Mimetype)
                    .IsRequired()
                    .HasColumnName("mimetype")
                    .HasMaxLength(250);

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasMaxLength(100);

                entity.Property(e => e.OverrideFilePath)
                    .HasColumnName("override_file_path")
                    .HasMaxLength(512);

                entity.Property(e => e.Ref)
                    .IsRequired()
                    .HasColumnName("ref")
                    .HasMaxLength(250);

                entity.Property(e => e.Type).HasColumnName("type");

                entity.HasOne(d => d.TypeNavigation)
                    .WithMany(p => p.Document)
                    .HasForeignKey(d => d.Type)
                    .HasConstraintName("fk_document_document_type");
            });

            modelBuilder.Entity<DocumentType>(entity =>
            {
                entity.ToTable("document_type", "doc");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EmployeeTask>(entity =>
            {
                entity.HasKey(e => e.TaskId)
                    .HasName("employee_task_pk");

                entity.ToTable("employee_task", "lb");

                entity.Property(e => e.TaskId)
                    .HasColumnName("task_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.DateAssigned)
                    .HasColumnName("date_assigned")
                    .HasColumnType("date");

                entity.Property(e => e.EmployeeId)
                    .IsRequired()
                    .HasColumnName("employee_id")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Task)
                    .WithOne(p => p.EmployeeTask)
                    .HasForeignKey<EmployeeTask>(d => d.TaskId)
                    .HasConstraintName("employee_task_task_id_fk");
            });

            modelBuilder.Entity<Equipment>(entity =>
            {
                entity.ToTable("equipment", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<Failure>(entity =>
            {
                entity.ToTable("failure", "lb");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<IncidentTask>(entity =>
            {
                entity.HasKey(e => e.TaskId)
                    .HasName("incident_task_pk");

                entity.ToTable("incident_task", "lb");

                entity.Property(e => e.TaskId)
                    .HasColumnName("task_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("ntext");

                entity.Property(e => e.GeoLocation)
                    .HasColumnName("geo_location")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Incident)
                    .IsRequired()
                    .HasColumnName("incident")
                    .HasMaxLength(500);

                entity.Property(e => e.ReportedBy)
                    .IsRequired()
                    .HasColumnName("reported_by")
                    .HasMaxLength(100);

                entity.HasOne(d => d.Task)
                    .WithOne(p => p.IncidentTask)
                    .HasForeignKey<IncidentTask>(d => d.TaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("incident_task_task_id_fk");
            });

            modelBuilder.Entity<IncidentTaskImages>(entity =>
            {
                entity.HasKey(e => new { e.ImageId, e.TaskId })
                    .HasName("incident_task_images_pk");

                entity.ToTable("incident_task_images", "lb");

                entity.Property(e => e.ImageId).HasColumnName("image_id");

                entity.Property(e => e.TaskId).HasColumnName("task_id");

                entity.HasOne(d => d.Image)
                    .WithMany(p => p.IncidentTaskImages)
                    .HasForeignKey(d => d.ImageId)
                    .HasConstraintName("incident_task_images_document_id_fk");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.IncidentTaskImages)
                    .HasForeignKey(d => d.TaskId)
                    .HasConstraintName("incident_task_images_task_id_fk");
            });

            modelBuilder.Entity<MailSchedule>(entity =>
            {
                entity.HasKey(e => new { e.MailId, e.Schedule })
                    .HasName("mail_schedule_pk");

                entity.ToTable("mail_schedule", "ml");

                entity.Property(e => e.MailId).HasColumnName("mail_id");

                entity.Property(e => e.Schedule)
                    .HasColumnName("schedule")
                    .HasColumnType("date");

                entity.Property(e => e.StateId).HasColumnName("state_id");

                entity.HasOne(d => d.Mail)
                    .WithMany(p => p.MailSchedule)
                    .HasForeignKey(d => d.MailId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("mail_schedule_mail_store_id_fk");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.MailSchedule)
                    .HasForeignKey(d => d.StateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("mail_schedule_mail_state_id_fk");
            });

            modelBuilder.Entity<MailState>(entity =>
            {
                entity.ToTable("mail_state", "ml");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<MailStore>(entity =>
            {
                entity.ToTable("mail_store", "ml");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasColumnType("ntext");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(100);

                entity.Property(e => e.TaskId).HasColumnName("task_id");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.MailStore)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("mail_store_task_id_fk");
            });

            modelBuilder.Entity<MaintenanceProcedure>(entity =>
            {
                entity.ToTable("maintenance_procedure", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasColumnType("ntext");

                entity.Property(e => e.Document).HasColumnName("document");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(250);

                entity.HasOne(d => d.DocumentNavigation)
                    .WithMany(p => p.MaintenanceProcedure)
                    .HasForeignKey(d => d.Document)
                    .HasConstraintName("maintenance_procedure_document_id_fk");
            });

            modelBuilder.Entity<MaintenanceType>(entity =>
            {
                entity.ToTable("maintenance_type", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<Message>(entity =>
            {
                entity.ToTable("message", "lb");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");

                entity.Property(e => e.Document).HasColumnName("document");

                entity.Property(e => e.TextMsg)
                    .HasColumnName("text_msg")
                    .HasColumnType("ntext");

                entity.HasOne(d => d.DocumentNavigation)
                    .WithMany(p => p.Message)
                    .HasForeignKey(d => d.Document)
                    .HasConstraintName("thread_message_document_id_fk");
            });

            modelBuilder.Entity<NoteImages>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("note_images_pk")
                    .ForSqlServerIsClustered(false);

                entity.ToTable("note_images", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DocumentId).HasColumnName("document_id");

                entity.Property(e => e.NoteId).HasColumnName("note_id");

                entity.HasOne(d => d.Document)
                    .WithMany(p => p.NoteImages)
                    .HasForeignKey(d => d.DocumentId)
                    .HasConstraintName("note_images_document_id_fk");

                entity.HasOne(d => d.Note)
                    .WithMany(p => p.NoteImages)
                    .HasForeignKey(d => d.NoteId)
                    .HasConstraintName("note_images_task_note_id_fk");
            });

            modelBuilder.Entity<NotificationConfiguration>(entity =>
            {
                entity.ToTable("notification_configuration", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DaysBeforeEnd).HasColumnName("days_before_end");

                entity.Property(e => e.DaysBeforeStart).HasColumnName("days_before_start");

                entity.Property(e => e.NotificationMessage)
                    .IsRequired()
                    .HasColumnName("notification_message")
                    .HasColumnType("ntext");

                entity.Property(e => e.OnState).HasColumnName("on_state");

                entity.Property(e => e.Seen).HasColumnName("seen");

                entity.Property(e => e.TaskId).HasColumnName("task_id");

                entity.HasOne(d => d.OnStateNavigation)
                    .WithMany(p => p.NotificationConfiguration)
                    .HasForeignKey(d => d.OnState)
                    .HasConstraintName("notification_configuration_status_type_id_fk");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.NotificationConfiguration)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("notification_configuration_task_id_fk");
            });

            modelBuilder.Entity<PeriodicTask>(entity =>
            {
                entity.HasKey(e => e.TaskId)
                    .HasName("periodic_task_pk");

                entity.ToTable("periodic_task", "lb");

                entity.Property(e => e.TaskId)
                    .HasColumnName("task_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Interval).HasColumnName("interval");

                entity.HasOne(d => d.Task)
                    .WithOne(p => p.PeriodicTask)
                    .HasForeignKey<PeriodicTask>(d => d.TaskId)
                    .HasConstraintName("periodic_task_task_id_fk");
            });

            modelBuilder.Entity<PriorityType>(entity =>
            {
                entity.ToTable("priority_type", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<StatusMaintenanceProc>(entity =>
            {
                entity.ToTable("status_maintenance_proc", "lb");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.DateOccured)
                    .HasColumnName("date_occured")
                    .HasColumnType("date");

                entity.Property(e => e.Failure).HasColumnName("failure");

                entity.Property(e => e.ProcedureId).HasColumnName("procedure_id");

                entity.Property(e => e.StatusId).HasColumnName("status_id");

                entity.HasOne(d => d.FailureNavigation)
                    .WithMany(p => p.StatusMaintenanceProc)
                    .HasForeignKey(d => d.Failure)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("status_maintenance_proc_failure_id_fk");

                entity.HasOne(d => d.Procedure)
                    .WithMany(p => p.StatusMaintenanceProc)
                    .HasForeignKey(d => d.ProcedureId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("status_maintenance_proc_maintenance_procedure_id_fk");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.StatusMaintenanceProc)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("status_maintenance_proc_task_status_id_fk");
            });

            modelBuilder.Entity<StatusType>(entity =>
            {
                entity.ToTable("status_type", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<Task>(entity =>
            {
                entity.ToTable("task", "lb");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("created_by")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("ntext");

                entity.Property(e => e.EndDateActual)
                    .HasColumnName("end_date_actual")
                    .HasColumnType("date");

                entity.Property(e => e.EndDateExpected)
                    .HasColumnName("end_date_expected")
                    .HasColumnType("date");

                entity.Property(e => e.MaintenanceType).HasColumnName("maintenance_type");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(250);

                entity.Property(e => e.StartDateActual)
                    .HasColumnName("start_date_actual")
                    .HasColumnType("date");

                entity.Property(e => e.StartDateExpected)
                    .HasColumnName("start_date_expected")
                    .HasColumnType("date");

                entity.Property(e => e.Type).HasColumnName("type");

                entity.Property(e => e.Wfid).HasColumnName("wfid");

                entity.HasOne(d => d.MaintenanceTypeNavigation)
                    .WithMany(p => p.Task)
                    .HasForeignKey(d => d.MaintenanceType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("task_maintenance_type_id_fk");

                entity.HasOne(d => d.TypeNavigation)
                    .WithMany(p => p.Task)
                    .HasForeignKey(d => d.Type)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("task_task_type_id_fk");

                entity.HasOne(d => d.Wf)
                    .WithMany(p => p.Task)
                    .HasForeignKey(d => d.Wfid)
                    .HasConstraintName("task_workflow_id_fk");
            });

            modelBuilder.Entity<TaskAsigneeType>(entity =>
            {
                entity.ToTable("task_asignee_type", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<TaskAssignee>(entity =>
            {
                entity.HasKey(e => e.TaskId)
                    .HasName("task_assigned_pk");

                entity.ToTable("task_assignee", "lb");

                entity.Property(e => e.TaskId)
                    .HasColumnName("task_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Asignee).HasColumnName("asignee");

                entity.HasOne(d => d.AsigneeNavigation)
                    .WithMany(p => p.TaskAssignee)
                    .HasForeignKey(d => d.Asignee)
                    .HasConstraintName("task_assignee_task_asignee_type_id_fk");

                entity.HasOne(d => d.Task)
                    .WithOne(p => p.TaskAssignee)
                    .HasForeignKey<TaskAssignee>(d => d.TaskId)
                    .HasConstraintName("task_assigned_task_id_fk");
            });

            modelBuilder.Entity<TaskBudget>(entity =>
            {
                entity.HasKey(e => new { e.TaskId, e.BudgetId })
                    .HasName("task_budget_pk");

                entity.ToTable("task_budget", "lb");

                entity.Property(e => e.TaskId).HasColumnName("task_id");

                entity.Property(e => e.BudgetId).HasColumnName("budget_id");

                entity.HasOne(d => d.Budget)
                    .WithMany(p => p.TaskBudget)
                    .HasForeignKey(d => d.BudgetId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("task_budget_budget_id_fk");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.TaskBudget)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("task_budget_task_id_fk");
            });

            modelBuilder.Entity<TaskDependency>(entity =>
            {
                entity.HasKey(e => new { e.Dependent, e.DependUpon })
                    .HasName("task_dependency_pk");

                entity.ToTable("task_dependency", "lb");

                entity.Property(e => e.Dependent).HasColumnName("dependent");

                entity.Property(e => e.DependUpon).HasColumnName("depend_upon");

                entity.HasOne(d => d.DependUponNavigation)
                    .WithMany(p => p.TaskDependencyDependUponNavigation)
                    .HasForeignKey(d => d.DependUpon)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("task_dependency_task_id_fk_2");

                entity.HasOne(d => d.DependentNavigation)
                    .WithMany(p => p.TaskDependencyDependentNavigation)
                    .HasForeignKey(d => d.Dependent)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("task_dependency_task_id_fk");
            });

            modelBuilder.Entity<TaskEquipment>(entity =>
            {
                entity.HasKey(e => new { e.TaskId, e.EquipmentId })
                    .HasName("task_equipment_pk");

                entity.ToTable("task_equipment", "lb");

                entity.Property(e => e.TaskId).HasColumnName("task_id");

                entity.Property(e => e.EquipmentId).HasColumnName("equipment_id");

                entity.Property(e => e.AllocatedDate)
                    .HasColumnName("allocated_date")
                    .HasColumnType("date");

                entity.Property(e => e.CostId).HasColumnName("cost_id");

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.HasOne(d => d.Equipment)
                    .WithMany(p => p.TaskEquipment)
                    .HasForeignKey(d => d.EquipmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("task_equipment_equipment_id_fk");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.TaskEquipment)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("task_equipment_task_id_fk");
            });

            modelBuilder.Entity<TaskNote>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("task_note_pk")
                    .ForSqlServerIsClustered(false);

                entity.ToTable("task_note", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Header)
                    .IsRequired()
                    .HasColumnName("header")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasColumnType("text");

                entity.Property(e => e.NoteBy)
                    .IsRequired()
                    .HasColumnName("note_by")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TaskId).HasColumnName("task_id");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.TaskNote)
                    .HasForeignKey(d => d.TaskId)
                    .HasConstraintName("task_note_task_id_fk");
            });

            modelBuilder.Entity<TaskNotification>(entity =>
            {
                entity.HasKey(e => new { e.TaskId, e.EmployeeId })
                    .HasName("task_notification_pk")
                    .ForSqlServerIsClustered(false);

                entity.ToTable("task_notification", "lb");

                entity.Property(e => e.TaskId).HasColumnName("task_id");

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("employee_id")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Seen).HasColumnName("seen");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.TaskNotification)
                    .HasForeignKey(d => d.TaskId)
                    .HasConstraintName("task_notification_task_id_fk");
            });

            modelBuilder.Entity<TaskPriority>(entity =>
            {
                entity.HasKey(e => new { e.TaskId, e.PriorityId })
                    .HasName("task_priority_pk");

                entity.ToTable("task_priority", "lb");

                entity.Property(e => e.TaskId).HasColumnName("task_id");

                entity.Property(e => e.PriorityId).HasColumnName("priority_id");

                entity.HasOne(d => d.Priority)
                    .WithMany(p => p.TaskPriority)
                    .HasForeignKey(d => d.PriorityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("task_priority_priority_type_id_fk");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.TaskPriority)
                    .HasForeignKey(d => d.TaskId)
                    .HasConstraintName("task_priority_task_id_fk");
            });

            modelBuilder.Entity<TaskStatus>(entity =>
            {
                entity.ToTable("task_status", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("ntext");

                entity.Property(e => e.EmployeeId).HasColumnName("employee_id");

                entity.Property(e => e.EndDateActual)
                    .HasColumnName("end_date_actual")
                    .HasColumnType("date");

                entity.Property(e => e.EndDateExpected)
                    .HasColumnName("end_date_expected")
                    .HasColumnType("date");

                entity.Property(e => e.Progress)
                    .HasColumnName("progress")
                    .HasColumnType("decimal(30, 10)");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("date");

                entity.Property(e => e.StatusId).HasColumnName("status_id");

                entity.Property(e => e.TaskId).HasColumnName("task_id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.TaskStatus)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("task_status_status_type_id_fk");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.TaskStatus)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("task_status_task_id_fk");
            });

            modelBuilder.Entity<TaskThread>(entity =>
            {
                entity.HasKey(e => new { e.TaskId, e.ThreadId, e.TeamId })
                    .HasName("team_thread_pk");

                entity.ToTable("task_thread", "lb");

                entity.Property(e => e.TaskId).HasColumnName("task_id");

                entity.Property(e => e.ThreadId).HasColumnName("thread_id");

                entity.Property(e => e.TeamId).HasColumnName("team_id");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.TaskThread)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("team_thread_team_id_fk");

                entity.HasOne(d => d.Thread)
                    .WithMany(p => p.TaskThread)
                    .HasForeignKey(d => d.ThreadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("team_thread_thread_id_fk");
            });

            modelBuilder.Entity<TaskType>(entity =>
            {
                entity.ToTable("task_type", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<TaskWarranty>(entity =>
            {
                entity.HasKey(e => new { e.TaskId, e.WarrantyDocument })
                    .HasName("task_warranty_pk");

                entity.ToTable("task_warranty", "lb");

                entity.Property(e => e.TaskId).HasColumnName("task_id");

                entity.Property(e => e.WarrantyDocument).HasColumnName("warranty_document");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.TaskWarranty)
                    .HasForeignKey(d => d.TaskId)
                    .HasConstraintName("task_warranty_task_id_fk");

                entity.HasOne(d => d.WarrantyDocumentNavigation)
                    .WithMany(p => p.TaskWarranty)
                    .HasForeignKey(d => d.WarrantyDocument)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("task_warranty_document_id_fk");
            });

            modelBuilder.Entity<Team>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("team_pk")
                    .ForSqlServerIsClustered(false);

                entity.ToTable("team", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Wfid).HasColumnName("wfid");

                entity.HasOne(d => d.Wf)
                    .WithMany(p => p.Team)
                    .HasForeignKey(d => d.Wfid)
                    .HasConstraintName("team_workflow_id_fk");
            });

            modelBuilder.Entity<TeamMembers>(entity =>
            {
                entity.HasKey(e => new { e.MemberId, e.TeamId })
                    .HasName("team_members_pk")
                    .ForSqlServerIsClustered(false);

                entity.ToTable("team_members", "lb");

                entity.Property(e => e.MemberId)
                    .HasColumnName("member_id")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TeamId).HasColumnName("team_id");

                entity.HasOne(d => d.Team)
                    .WithMany(p => p.TeamMembers)
                    .HasForeignKey(d => d.TeamId)
                    .HasConstraintName("team_members_team_id_fk");
            });

            modelBuilder.Entity<TeamTask>(entity =>
            {
                entity.HasKey(e => e.TaskId)
                    .HasName("team_task_pk");

                entity.ToTable("team_task", "lb");

                entity.Property(e => e.TaskId)
                    .HasColumnName("task_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.DateAssigned)
                    .HasColumnName("date_assigned")
                    .HasColumnType("date");

                entity.Property(e => e.TeamId).HasColumnName("team_id");

                entity.HasOne(d => d.Task)
                    .WithOne(p => p.TeamTask)
                    .HasForeignKey<TeamTask>(d => d.TaskId)
                    .HasConstraintName("team_task_task_id_fk");

                entity.HasOne(d => d.Team)
                    .WithMany(p => p.TeamTask)
                    .HasForeignKey(d => d.TeamId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("team_task_team_id_fk");
            });

            modelBuilder.Entity<Thread>(entity =>
            {
                entity.ToTable("thread", "lb");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(250);

                entity.Property(e => e.State).HasColumnName("state");

                entity.HasOne(d => d.StateNavigation)
                    .WithMany(p => p.Thread)
                    .HasForeignKey(d => d.State)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("thread_thread_state_type_id_fk");
            });

            modelBuilder.Entity<ThreadMessage>(entity =>
            {
                entity.HasKey(e => new { e.Thread, e.Message })
                    .HasName("thread_message_pk_2");

                entity.ToTable("thread_message", "lb");

                entity.Property(e => e.Thread).HasColumnName("thread");

                entity.Property(e => e.Message).HasColumnName("message");

                entity.Property(e => e.MessageBy).HasColumnName("message_by");

                entity.HasOne(d => d.MessageNavigation)
                    .WithMany(p => p.ThreadMessage)
                    .HasForeignKey(d => d.Message)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("thread_message_message_id_fk");

                entity.HasOne(d => d.ThreadNavigation)
                    .WithMany(p => p.ThreadMessage)
                    .HasForeignKey(d => d.Thread)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("thread_message_thread_id_fk");
            });

            modelBuilder.Entity<ThreadStateType>(entity =>
            {
                entity.ToTable("thread_state_type", "lb");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<Workflow>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("workflow_pk")
                    .ForSqlServerIsClustered(false);

                entity.ToTable("workflow", "wf");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CurrentState).HasColumnName("current_state");

                entity.Property(e => e.Description)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.StateDate)
                    .HasColumnName("state_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.TypeId).HasColumnName("type_id");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.Workflow)
                    .HasForeignKey(d => d.TypeId)
                    .HasConstraintName("workflow_workflow_type_id_fk");
            });

            modelBuilder.Entity<WorkflowItem>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("workflow_item_pk")
                    .ForSqlServerIsClustered(false);

                entity.ToTable("workflow_item", "wf");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.ActivityDate)
                    .HasColumnName("activity_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Data)
                    .HasColumnName("data")
                    .HasColumnType("text");

                entity.Property(e => e.EmployeeId)
                    .IsRequired()
                    .HasColumnName("employee_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FromState).HasColumnName("from_state");

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasColumnType("text");

                entity.Property(e => e.Role).HasColumnName("role");

                entity.Property(e => e.SeqenceNo).HasColumnName("seqence_no");

                entity.Property(e => e.ToState).HasColumnName("to_state");

                entity.Property(e => e.Trigger).HasColumnName("trigger");

                entity.Property(e => e.WorkflowId).HasColumnName("workflow_id");

                entity.HasOne(d => d.Workflow)
                    .WithMany(p => p.WorkflowItem)
                    .HasForeignKey(d => d.WorkflowId)
                    .HasConstraintName("workflow_item_workflow_id_fk");
            });

            modelBuilder.Entity<WorkflowType>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("workflow_type_pk")
                    .ForSqlServerIsClustered(false);

                entity.ToTable("workflow_type", "wf");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
        }
    }
}
