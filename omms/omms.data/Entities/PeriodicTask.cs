﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class PeriodicTask
    {
        public Guid TaskId { get; set; }
        public int Interval { get; set; }

        public virtual Task Task { get; set; }
    }
}
