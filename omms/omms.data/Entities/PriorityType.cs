﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class PriorityType
    {
        public PriorityType()
        {
            TaskPriority = new HashSet<TaskPriority>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<TaskPriority> TaskPriority { get; set; }
    }
}
