﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class RoleType
    {
        public RoleType()
        {
            Employee = new HashSet<Employee>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Employee> Employee { get; set; }
    }
}
