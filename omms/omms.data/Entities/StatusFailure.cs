﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class StatusFailure
    {
        public int Id { get; set; }
        public int StatusId { get; set; }
        public int FailureId { get; set; }

        public virtual Failure Failure { get; set; }
        public virtual TaskStatus Status { get; set; }
    }
}
