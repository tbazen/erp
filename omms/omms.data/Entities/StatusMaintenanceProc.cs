﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class StatusMaintenanceProc
    {
        public int StatusId { get; set; }
        public int ProcedureId { get; set; }
        public DateTime DateOccured { get; set; }
        public Guid Id { get; set; }
        public Guid Failure { get; set; }

        public virtual Failure FailureNavigation { get; set; }
        public virtual MaintenanceProcedure Procedure { get; set; }
        public virtual TaskStatus Status { get; set; }
    }
}
