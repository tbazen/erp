﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class StatusType
    {
        public StatusType()
        {
            NotificationConfiguration = new HashSet<NotificationConfiguration>();
            TaskStatus = new HashSet<TaskStatus>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<NotificationConfiguration> NotificationConfiguration { get; set; }
        public virtual ICollection<TaskStatus> TaskStatus { get; set; }
    }
}
