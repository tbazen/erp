﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class Task
    {
        public Task()
        {
            AssetTask = new HashSet<AssetTask>();
            IncidentTaskImages = new HashSet<IncidentTaskImages>();
            MailStore = new HashSet<MailStore>();
            NotificationConfiguration = new HashSet<NotificationConfiguration>();
            TaskBudget = new HashSet<TaskBudget>();
            TaskDependencyDependUponNavigation = new HashSet<TaskDependency>();
            TaskDependencyDependentNavigation = new HashSet<TaskDependency>();
            TaskEquipment = new HashSet<TaskEquipment>();
            TaskNote = new HashSet<TaskNote>();
            TaskNotification = new HashSet<TaskNotification>();
            TaskPriority = new HashSet<TaskPriority>();
            TaskStatus = new HashSet<TaskStatus>();
            TaskThread = new HashSet<TaskThread>();
            TaskWarranty = new HashSet<TaskWarranty>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime? StartDateExpected { get; set; }
        public DateTime? EndDateExpected { get; set; }
        public DateTime? StartDateActual { get; set; }
        public DateTime? EndDateActual { get; set; }
        public string Description { get; set; }
        public int Type { get; set; }
        public int MaintenanceType { get; set; }
        public string CreatedBy { get; set; }
        public Guid Wfid { get; set; }

        public virtual MaintenanceType MaintenanceTypeNavigation { get; set; }
        public virtual TaskType TypeNavigation { get; set; }
        public virtual Workflow Wf { get; set; }
        public virtual EmployeeTask EmployeeTask { get; set; }
        public virtual IncidentTask IncidentTask { get; set; }
        public virtual PeriodicTask PeriodicTask { get; set; }
        public virtual TaskAssignee TaskAssignee { get; set; }
        public virtual TeamTask TeamTask { get; set; }
        public virtual ICollection<AssetTask> AssetTask { get; set; }
        public virtual ICollection<IncidentTaskImages> IncidentTaskImages { get; set; }
        public virtual ICollection<MailStore> MailStore { get; set; }
        public virtual ICollection<NotificationConfiguration> NotificationConfiguration { get; set; }
        public virtual ICollection<TaskBudget> TaskBudget { get; set; }
        public virtual ICollection<TaskDependency> TaskDependencyDependUponNavigation { get; set; }
        public virtual ICollection<TaskDependency> TaskDependencyDependentNavigation { get; set; }
        public virtual ICollection<TaskEquipment> TaskEquipment { get; set; }
        public virtual ICollection<TaskNote> TaskNote { get; set; }
        public virtual ICollection<TaskNotification> TaskNotification { get; set; }
        public virtual ICollection<TaskPriority> TaskPriority { get; set; }
        public virtual ICollection<TaskStatus> TaskStatus { get; set; }
        public virtual ICollection<TaskThread> TaskThread { get; set; }
        public virtual ICollection<TaskWarranty> TaskWarranty { get; set; }
    }
}
