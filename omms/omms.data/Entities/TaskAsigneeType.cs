﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class TaskAsigneeType
    {
        public TaskAsigneeType()
        {
            TaskAssignee = new HashSet<TaskAssignee>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<TaskAssignee> TaskAssignee { get; set; }
    }
}
