﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class TaskAsset
    {
        public Guid TaskId { get; set; }
        public int AssetId { get; set; }
        public int Id { get; set; }

        public virtual Asset Asset { get; set; }
        public virtual Task Task { get; set; }
    }
}
