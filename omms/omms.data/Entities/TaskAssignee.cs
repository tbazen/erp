﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class TaskAssignee
    {
        public int Asignee { get; set; }
        public Guid TaskId { get; set; }

        public virtual TaskAsigneeType AsigneeNavigation { get; set; }
        public virtual Task Task { get; set; }
    }
}
