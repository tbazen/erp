﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class TaskBudget
    {
        public Guid TaskId { get; set; }
        public int BudgetId { get; set; }

        public virtual Budget Budget { get; set; }
        public virtual Task Task { get; set; }
    }
}
