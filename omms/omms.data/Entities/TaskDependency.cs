﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class TaskDependency
    {
        public Guid Dependent { get; set; }
        public Guid DependUpon { get; set; }

        public virtual Task DependUponNavigation { get; set; }
        public virtual Task DependentNavigation { get; set; }
    }
}
