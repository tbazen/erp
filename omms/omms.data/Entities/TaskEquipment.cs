﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class TaskEquipment
    {
        public Guid TaskId { get; set; }
        public int EquipmentId { get; set; }
        public int Quantity { get; set; }
        public int CostId { get; set; }
        public DateTime AllocatedDate { get; set; }

        public virtual Equipment Equipment { get; set; }
        public virtual Task Task { get; set; }
    }
}
