﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class TaskNote
    {
        public TaskNote()
        {
            NoteImages = new HashSet<NoteImages>();
        }

        public int Id { get; set; }
        public Guid TaskId { get; set; }
        public string Header { get; set; }
        public string Note { get; set; }
        public DateTime Date { get; set; }
        public string NoteBy { get; set; }

        public virtual Task Task { get; set; }
        public virtual ICollection<NoteImages> NoteImages { get; set; }
    }
}
