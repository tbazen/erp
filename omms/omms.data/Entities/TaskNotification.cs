﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class TaskNotification
    {
        public Guid TaskId { get; set; }
        public string EmployeeId { get; set; }
        public bool Seen { get; set; }

        public virtual Task Task { get; set; }
    }
}
