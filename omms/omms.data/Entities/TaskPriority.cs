﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class TaskPriority
    {
        public int PriorityId { get; set; }
        public Guid TaskId { get; set; }

        public virtual PriorityType Priority { get; set; }
        public virtual Task Task { get; set; }
    }
}
