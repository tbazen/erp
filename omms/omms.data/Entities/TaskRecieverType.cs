﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class TaskRecieverType
    {
        public TaskRecieverType()
        {
            TaskAssigned = new HashSet<TaskAssigned>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<TaskAssigned> TaskAssigned { get; set; }
    }
}
