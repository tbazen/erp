﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class TaskStatus
    {
        public TaskStatus()
        {
            StatusMaintenanceProc = new HashSet<StatusMaintenanceProc>();
        }

        public int Id { get; set; }
        public int StatusId { get; set; }
        public Guid TaskId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDateExpected { get; set; }
        public string Description { get; set; }
        public decimal Progress { get; set; }
        public DateTime? EndDateActual { get; set; }
        public long EmployeeId { get; set; }

        public virtual StatusType Status { get; set; }
        public virtual Task Task { get; set; }
        public virtual ICollection<StatusMaintenanceProc> StatusMaintenanceProc { get; set; }
    }
}
