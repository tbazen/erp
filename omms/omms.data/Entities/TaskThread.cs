﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class TaskThread
    {
        public Guid TaskId { get; set; }
        public Guid ThreadId { get; set; }
        public Guid TeamId { get; set; }

        public virtual Task Task { get; set; }
        public virtual Thread Thread { get; set; }
    }
}
