﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class TaskWarranty
    {
        public Guid TaskId { get; set; }
        public Guid WarrantyDocument { get; set; }

        public virtual Task Task { get; set; }
        public virtual Document WarrantyDocumentNavigation { get; set; }
    }
}
