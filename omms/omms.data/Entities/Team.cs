﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class Team
    {
        public Team()
        {
            TeamMembers = new HashSet<TeamMembers>();
            TeamTask = new HashSet<TeamTask>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid Wfid { get; set; }

        public virtual Workflow Wf { get; set; }
        public virtual ICollection<TeamMembers> TeamMembers { get; set; }
        public virtual ICollection<TeamTask> TeamTask { get; set; }
    }
}
