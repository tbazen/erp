﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class TeamMembers
    {
        public string MemberId { get; set; }
        public long TeamId { get; set; }

        public virtual Team Team { get; set; }
    }
}
