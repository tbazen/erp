﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class TeamTask
    {
        public Guid TaskId { get; set; }
        public DateTime DateAssigned { get; set; }
        public long TeamId { get; set; }

        public virtual Task Task { get; set; }
        public virtual Team Team { get; set; }
    }
}
