﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class TeamThread
    {
        public Guid Team { get; set; }
        public Guid Thread { get; set; }

        public virtual Team TeamNavigation { get; set; }
        public virtual Thread ThreadNavigation { get; set; }
    }
}
