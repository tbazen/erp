﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class Thread
    {
        public Thread()
        {
            TaskThread = new HashSet<TaskThread>();
            ThreadMessage = new HashSet<ThreadMessage>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int State { get; set; }

        public virtual ThreadStateType StateNavigation { get; set; }
        public virtual ICollection<TaskThread> TaskThread { get; set; }
        public virtual ICollection<ThreadMessage> ThreadMessage { get; set; }
    }
}
