﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class ThreadMessage
    {
        public Guid Thread { get; set; }
        public Guid Message { get; set; }
        public long MessageBy { get; set; }

        public virtual Message MessageNavigation { get; set; }
        public virtual Thread ThreadNavigation { get; set; }
    }
}
