﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class ThreadStateType
    {
        public ThreadStateType()
        {
            Thread = new HashSet<Thread>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Thread> Thread { get; set; }
    }
}
