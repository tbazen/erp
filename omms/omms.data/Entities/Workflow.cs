﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class Workflow
    {
        public Workflow()
        {
            Task = new HashSet<Task>();
            Team = new HashSet<Team>();
            WorkflowItem = new HashSet<WorkflowItem>();
        }

        public Guid Id { get; set; }
        public int CurrentState { get; set; }
        public int TypeId { get; set; }
        public string Description { get; set; }
        public DateTime StateDate { get; set; }

        public virtual WorkflowType Type { get; set; }
        public virtual ICollection<Task> Task { get; set; }
        public virtual ICollection<Team> Team { get; set; }
        public virtual ICollection<WorkflowItem> WorkflowItem { get; set; }
    }
}
