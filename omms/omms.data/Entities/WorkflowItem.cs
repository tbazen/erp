﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class WorkflowItem
    {
        public Guid Id { get; set; }
        public Guid WorkflowId { get; set; }
        public int FromState { get; set; }
        public int ToState { get; set; }
        public int Trigger { get; set; }
        public int SeqenceNo { get; set; }
        public string Data { get; set; }
        public string Note { get; set; }
        public DateTime ActivityDate { get; set; }
        public long Role { get; set; }
        public string EmployeeId { get; set; }

        public virtual Workflow Workflow { get; set; }
    }
}
