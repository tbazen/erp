﻿using System;
using System.Collections.Generic;

namespace omms.data.Entities
{
    public partial class WorkflowType
    {
        public WorkflowType()
        {
            Workflow = new HashSet<Workflow>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Workflow> Workflow { get; set; }
    }
}
