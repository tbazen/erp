﻿using System;
using System.Collections.Generic;

namespace omms.data.GisEntities
{
    public partial class Boreholes
    {
        public int Id { get; set; }
        public long? Pkuid { get; set; }
        public string Network { get; set; }
        public string TypeMeter { get; set; }
        public string Name { get; set; }
    }
}
