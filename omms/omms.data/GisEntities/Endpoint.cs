﻿using System;
using System.Collections.Generic;

namespace omms.data.GisEntities
{
    public partial class Endpoint
    {
        public int Id { get; set; }
        public long? Pkuid { get; set; }
        public string Network { get; set; }
        public string Material { get; set; }
        public string Diameter { get; set; }
        public string ZoneName { get; set; }
        public string KebelName { get; set; }
        public string SpecificArea { get; set; }
        public string Name { get; set; }
    }
}
