﻿using System;
using System.Collections.Generic;

namespace omms.data.GisEntities
{
    public partial class HouseConnections
    {
        public int Id { get; set; }
        public long? Pkuid { get; set; }
        public string CustomerType { get; set; }
        public string WatermeterCapacity { get; set; }
        public string Network { get; set; }
        public string Material { get; set; }
        public string Diameter { get; set; }
        public string Status { get; set; }
        public string ZoneName { get; set; }
        public string KebeleName { get; set; }
        public string SpecificArea { get; set; }
    }
}
