﻿using Microsoft.EntityFrameworkCore;

namespace omms.data.GisEntities
{
    public partial class OmmsGisContext : DbContext
    {
        public OmmsGisContext()
        {
        }

        public OmmsGisContext(DbContextOptions<OmmsGisContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Boreholes> Boreholes { get; set; }
        public virtual DbSet<Endpoint> Endpoint { get; set; }
        public virtual DbSet<HouseConnections> HouseConnections { get; set; }
        public virtual DbSet<Hydrants> Hydrants { get; set; }
        public virtual DbSet<Pipes> Pipes { get; set; }
        public virtual DbSet<Reduce> Reduce { get; set; }
        public virtual DbSet<Reservoirs> Reservoirs { get; set; }
        public virtual DbSet<SmallerPipes> SmallerPipes { get; set; }
        public virtual DbSet<Valves> Valves { get; set; }
        public virtual DbSet<Watermeters> Watermeters { get; set; }
        public virtual DbSet<Watermeters1> Watermeters1 { get; set; }
        public virtual DbSet<Waypoint01> Waypoint01 { get; set; }
        public virtual DbSet<Waypoint02> Waypoint02 { get; set; }
        public virtual DbSet<Waypoint03> Waypoint03 { get; set; }
        public virtual DbSet<Waypoint04> Waypoint04 { get; set; }
        public virtual DbSet<Waypoint05> Waypoint05 { get; set; }
        public virtual DbSet<Waypoint06> Waypoint06 { get; set; }
        public virtual DbSet<Waypoint07> Waypoint07 { get; set; }
        public virtual DbSet<Waypoint08> Waypoint08 { get; set; }
        public virtual DbSet<Waypoint09> Waypoint09 { get; set; }
        public virtual DbSet<Waypoint10> Waypoint10 { get; set; }
        public virtual DbSet<Waypoint11> Waypoint11 { get; set; }
        public virtual DbSet<Waypoint12> Waypoint12 { get; set; }
        public virtual DbSet<Waypoint13> Waypoint13 { get; set; }
        public virtual DbSet<Waypoint14> Waypoint14 { get; set; }
        public virtual DbSet<Waypoint15> Waypoint15 { get; set; }
        public virtual DbSet<Waypoint16> Waypoint16 { get; set; }
        public virtual DbSet<Waypoint17> Waypoint17 { get; set; }
        public virtual DbSet<Waypoint18> Waypoint18 { get; set; }
        public virtual DbSet<Waypoint19> Waypoint19 { get; set; }
        public virtual DbSet<Waypoint20> Waypoint20 { get; set; }
        public virtual DbSet<Waypoint21> Waypoint21 { get; set; }
        public virtual DbSet<Waypoint22> Waypoint22 { get; set; }
        public virtual DbSet<Waypoint23> Waypoint23 { get; set; }
        public virtual DbSet<Waypoint24> Waypoint24 { get; set; }
        public virtual DbSet<Waypoint25> Waypoint25 { get; set; }
        public virtual DbSet<Waypoint26> Waypoint26 { get; set; }
        public virtual DbSet<Waypoint27> Waypoint27 { get; set; }
        public virtual DbSet<Waypoint28> Waypoint28 { get; set; }
        public virtual DbSet<Waypoint29> Waypoint29 { get; set; }
        public virtual DbSet<Waypoint30> Waypoint30 { get; set; }
        public virtual DbSet<Waypoint31> Waypoint31 { get; set; }
        public virtual DbSet<Waypoint32> Waypoint32 { get; set; }
        public virtual DbSet<Waypoint33> Waypoint33 { get; set; }
        public virtual DbSet<Waypoint34> Waypoint34 { get; set; }
        public virtual DbSet<Waypoint35> Waypoint35 { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("postgis")
                .HasAnnotation("ProductVersion", "2.2.2-servicing-10034");

            modelBuilder.Entity<Boreholes>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Network).HasColumnType("character varying");

                entity.Property(e => e.Pkuid).HasColumnName("pkuid");

                entity.Property(e => e.TypeMeter)
                    .HasColumnName("Type meter")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Endpoint>(entity =>
            {
                entity.ToTable("endpoint");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Diameter).HasColumnType("character varying");

                entity.Property(e => e.KebelName)
                    .HasColumnName("kebel name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Material).HasColumnType("character varying");

                entity.Property(e => e.Name).HasColumnType("character varying");

                entity.Property(e => e.Network).HasColumnType("character varying");

                entity.Property(e => e.Pkuid).HasColumnName("pkuid");

                entity.Property(e => e.SpecificArea)
                    .HasColumnName("Specific area")
                    .HasColumnType("character varying");

                entity.Property(e => e.ZoneName)
                    .HasColumnName("Zone name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<HouseConnections>(entity =>
            {
                entity.ToTable("house connections");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CustomerType)
                    .HasColumnName("Customer type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Diameter).HasColumnType("character varying");

                entity.Property(e => e.KebeleName)
                    .HasColumnName("Kebele name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Material).HasColumnType("character varying");

                entity.Property(e => e.Network).HasColumnType("character varying");

                entity.Property(e => e.Pkuid).HasColumnName("pkuid");

                entity.Property(e => e.SpecificArea)
                    .HasColumnName("Specific area")
                    .HasColumnType("character varying");

                entity.Property(e => e.Status).HasColumnType("character varying");

                entity.Property(e => e.WatermeterCapacity)
                    .HasColumnName("Watermeter capacity")
                    .HasColumnType("character varying");

                entity.Property(e => e.ZoneName)
                    .HasColumnName("Zone name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Hydrants>(entity =>
            {
                entity.ToTable("hydrants");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Diameter).HasColumnType("character varying");

                entity.Property(e => e.KebeleName)
                    .HasColumnName("Kebele name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Material).HasColumnType("character varying");

                entity.Property(e => e.Name).HasColumnType("character varying");

                entity.Property(e => e.Network).HasColumnType("character varying");

                entity.Property(e => e.Pkuid).HasColumnName("pkuid");

                entity.Property(e => e.SpecificArea)
                    .HasColumnName("Specific area")
                    .HasColumnType("character varying");

                entity.Property(e => e.Status).HasColumnType("character varying");

                entity.Property(e => e.ZoneName)
                    .HasColumnName("Zone name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Pipes>(entity =>
            {
                entity.ToTable("pipes");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Diameter).HasColumnType("character varying");

                entity.Property(e => e.KebeleName)
                    .HasColumnName("kebele name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Material).HasColumnType("character varying");

                entity.Property(e => e.Network).HasColumnType("character varying");

                entity.Property(e => e.Pkuid).HasColumnName("pkuid");

                entity.Property(e => e.SpecificArea)
                    .HasColumnName("Specific area")
                    .HasColumnType("character varying");

                entity.Property(e => e.ZoneName)
                    .HasColumnName("Zone name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Reduce>(entity =>
            {
                entity.ToTable("reduce");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Diameter1)
                    .HasColumnName("Diameter_1")
                    .HasColumnType("character varying");

                entity.Property(e => e.Diameter2)
                    .HasColumnName("Diameter_2")
                    .HasColumnType("character varying");

                entity.Property(e => e.KebeleName)
                    .HasColumnName("Kebele name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Material).HasColumnType("character varying");

                entity.Property(e => e.Network).HasColumnType("character varying");

                entity.Property(e => e.Pkuid).HasColumnName("pkuid");

                entity.Property(e => e.SpecificArea)
                    .HasColumnName("Specific area")
                    .HasColumnType("character varying");

                entity.Property(e => e.ZoneName)
                    .HasColumnName("Zone name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Reservoirs>(entity =>
            {
                entity.ToTable("reservoirs");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.M3)
                    .HasColumnName("m3")
                    .HasColumnType("character varying");

                entity.Property(e => e.Material).HasColumnType("character varying");

                entity.Property(e => e.Pkuid).HasColumnName("pkuid");

                entity.Property(e => e.SpesficArea)
                    .HasColumnName("Spesfic area")
                    .HasColumnType("character varying");

                entity.Property(e => e.TypeOfReservoir)
                    .HasColumnName("Type of reservoir")
                    .HasColumnType("character varying");

                entity.Property(e => e.Valve)
                    .HasColumnName("valve")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<SmallerPipes>(entity =>
            {
                entity.ToTable("smaller pipes ");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Diameters)
                    .HasColumnName("diameters")
                    .HasColumnType("character varying");

                entity.Property(e => e.Kebele)
                    .HasColumnName("kebele")
                    .HasColumnType("character varying");

                entity.Property(e => e.Length)
                    .HasColumnName("length")
                    .HasColumnType("character varying");

                entity.Property(e => e.Materials)
                    .HasColumnName("materials")
                    .HasColumnType("character varying");

                entity.Property(e => e.Network)
                    .HasColumnName("network")
                    .HasColumnType("character varying");

                entity.Property(e => e.OgcFid).HasColumnName("ogc_fid");

                entity.Property(e => e.Pkuid).HasColumnName("pkuid");

                entity.Property(e => e.SpecificArea)
                    .HasColumnName("specific area")
                    .HasColumnType("character varying");

                entity.Property(e => e.ZoneName)
                    .HasColumnName("zone name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Valves>(entity =>
            {
                entity.ToTable("valves");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Diameter).HasColumnType("character varying");

                entity.Property(e => e.KebeleName)
                    .HasColumnName("Kebele name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Material).HasColumnType("character varying");

                entity.Property(e => e.Network).HasColumnType("character varying");

                entity.Property(e => e.Pkuid).HasColumnName("pkuid");

                entity.Property(e => e.SpecificArea)
                    .HasColumnName("Specific area")
                    .HasColumnType("character varying");

                entity.Property(e => e.Status).HasColumnType("character varying");

                entity.Property(e => e.ValveFunction)
                    .HasColumnName("Valve function")
                    .HasColumnType("character varying");

                entity.Property(e => e.ValveTurningDir)
                    .HasColumnName("Valve turning dir")
                    .HasColumnType("character varying");

                entity.Property(e => e.ValveType)
                    .HasColumnName("Valve type")
                    .HasColumnType("character varying");

                entity.Property(e => e.X)
                    .HasColumnName("x")
                    .HasColumnType("character varying");

                entity.Property(e => e.Y)
                    .HasColumnName("y")
                    .HasColumnType("character varying");

                entity.Property(e => e.ZoneName)
                    .HasColumnName("Zone name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Watermeters>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CustomerCode).HasColumnName("Customer code");

                entity.Property(e => e.Diameter).HasColumnType("character varying");

                entity.Property(e => e.HouseNo).HasColumnName("House no");

                entity.Property(e => e.KebelName)
                    .HasColumnName("Kebel name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Material).HasColumnType("character varying");

                entity.Property(e => e.MeterNo).HasColumnName("Meter No");

                entity.Property(e => e.Network).HasColumnType("character varying");

                entity.Property(e => e.Pkuid).HasColumnName("pkuid");

                entity.Property(e => e.SpecificArea)
                    .HasColumnName("Specific area")
                    .HasColumnType("character varying");

                entity.Property(e => e.Status).HasColumnType("character varying");

                entity.Property(e => e.ZoneName)
                    .HasColumnName("Zone name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Watermeters1>(entity =>
            {
                entity.ToTable("watermeters");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CustomerCode).HasColumnName("Customer code");

                entity.Property(e => e.Diameter).HasColumnType("character varying");

                entity.Property(e => e.HouseNo).HasColumnName("House no");

                entity.Property(e => e.KebelName)
                    .HasColumnName("Kebel name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Material).HasColumnType("character varying");

                entity.Property(e => e.MeterNo).HasColumnName("Meter No");

                entity.Property(e => e.Network).HasColumnType("character varying");

                entity.Property(e => e.Pkuid).HasColumnName("pkuid");

                entity.Property(e => e.SpecificArea)
                    .HasColumnName("Specific area")
                    .HasColumnType("character varying");

                entity.Property(e => e.Status).HasColumnType("character varying");

                entity.Property(e => e.ZoneName)
                    .HasColumnName("Zone name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Waypoint01>(entity =>
            {
                entity.ToTable("waypoint_01");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('waypoints_01_id_seq'::regclass)");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint02>(entity =>
            {
                entity.ToTable("waypoint_02");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint03>(entity =>
            {
                entity.ToTable("waypoint_03");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.X00)
                    .HasColumnName("x:(0,0)")
                    .HasMaxLength(255);

                entity.Property(e => e.Y00)
                    .HasColumnName("y:(0,0)")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Waypoint04>(entity =>
            {
                entity.ToTable("waypoint_04");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint05>(entity =>
            {
                entity.ToTable("waypoint_05");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint06>(entity =>
            {
                entity.ToTable("waypoint_06");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint07>(entity =>
            {
                entity.ToTable("waypoint_07");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint08>(entity =>
            {
                entity.ToTable("waypoint_08");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint09>(entity =>
            {
                entity.ToTable("waypoint_09");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint10>(entity =>
            {
                entity.ToTable("waypoint_10");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint11>(entity =>
            {
                entity.ToTable("waypoint_11");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint12>(entity =>
            {
                entity.ToTable("waypoint_12");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint13>(entity =>
            {
                entity.ToTable("waypoint_13");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint14>(entity =>
            {
                entity.ToTable("waypoint_14");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint15>(entity =>
            {
                entity.ToTable("waypoint_15");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint16>(entity =>
            {
                entity.ToTable("waypoint_16");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint17>(entity =>
            {
                entity.ToTable("waypoint_17");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint18>(entity =>
            {
                entity.ToTable("waypoint_18");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint19>(entity =>
            {
                entity.ToTable("waypoint_19");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint20>(entity =>
            {
                entity.ToTable("waypoint_20");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint21>(entity =>
            {
                entity.ToTable("waypoint_21");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");

                entity.Property(e => e.Wptx1WaypointExtension).HasColumnName("wptx1_WaypointExtension");
            });

            modelBuilder.Entity<Waypoint22>(entity =>
            {
                entity.ToTable("waypoint_22");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint23>(entity =>
            {
                entity.ToTable("waypoint_23");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint24>(entity =>
            {
                entity.ToTable("waypoint_24");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint25>(entity =>
            {
                entity.ToTable("waypoint_25");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasColumnType("character varying");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("character varying");

                entity.Property(e => e.Elevation).HasColumnName("elevation");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Source)
                    .HasColumnName("source")
                    .HasColumnType("character varying");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasColumnType("character varying");

                entity.Property(e => e.UrlName)
                    .HasColumnName("url name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Waypoint26>(entity =>
            {
                entity.ToTable("waypoint_26");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasColumnType("character varying");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("character varying");

                entity.Property(e => e.Elevation).HasColumnName("elevation");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Source)
                    .HasColumnName("source")
                    .HasColumnType("character varying");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasColumnType("character varying");

                entity.Property(e => e.UrlName)
                    .HasColumnName("url name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Waypoint27>(entity =>
            {
                entity.ToTable("waypoint_27");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasColumnType("character varying");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("character varying");

                entity.Property(e => e.Elevation).HasColumnName("elevation");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Source)
                    .HasColumnName("source")
                    .HasColumnType("character varying");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasColumnType("character varying");

                entity.Property(e => e.UrlName)
                    .HasColumnName("url name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Waypoint28>(entity =>
            {
                entity.ToTable("waypoint_28");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasColumnType("character varying");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("character varying");

                entity.Property(e => e.Elevation).HasColumnName("elevation");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Source)
                    .HasColumnName("source")
                    .HasColumnType("character varying");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasColumnType("character varying");

                entity.Property(e => e.UrlName)
                    .HasColumnName("url name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Waypoint29>(entity =>
            {
                entity.ToTable("waypoint_29");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasColumnType("character varying");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("character varying");

                entity.Property(e => e.Elevation).HasColumnName("elevation");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Source)
                    .HasColumnName("source")
                    .HasColumnType("character varying");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasColumnType("character varying");

                entity.Property(e => e.UrlName)
                    .HasColumnName("url name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Waypoint30>(entity =>
            {
                entity.ToTable("waypoint_30");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasColumnType("character varying");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("character varying");

                entity.Property(e => e.Elevation).HasColumnName("elevation");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Source)
                    .HasColumnName("source")
                    .HasColumnType("character varying");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasColumnType("character varying");

                entity.Property(e => e.UrlName)
                    .HasColumnName("url name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Waypoint31>(entity =>
            {
                entity.ToTable("waypoint_31");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasColumnType("character varying");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("character varying");

                entity.Property(e => e.Elevation).HasColumnName("elevation");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Source)
                    .HasColumnName("source")
                    .HasColumnType("character varying");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasColumnType("character varying");

                entity.Property(e => e.UrlName)
                    .HasColumnName("url name")
                    .HasColumnType("character varying");
            });

            modelBuilder.Entity<Waypoint32>(entity =>
            {
                entity.ToTable("waypoint_32");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint33>(entity =>
            {
                entity.ToTable("waypoint_33");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint34>(entity =>
            {
                entity.ToTable("waypoint_34");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.Entity<Waypoint35>(entity =>
            {
                entity.ToTable("waypoint_35");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ageofdgpsdata).HasColumnName("ageofdgpsdata");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .HasColumnType("character varying");

                entity.Property(e => e.Desc)
                    .HasColumnName("desc")
                    .HasColumnType("character varying");

                entity.Property(e => e.Dgpsid).HasColumnName("dgpsid");

                entity.Property(e => e.Ele).HasColumnName("ele");

                entity.Property(e => e.Fix)
                    .HasColumnName("fix")
                    .HasColumnType("character varying");

                entity.Property(e => e.Geoidheight).HasColumnName("geoidheight");

                entity.Property(e => e.Hdop).HasColumnName("hdop");

                entity.Property(e => e.Link1Href)
                    .HasColumnName("link1_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Text)
                    .HasColumnName("link1_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link1Type)
                    .HasColumnName("link1_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Href)
                    .HasColumnName("link2_href")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Text)
                    .HasColumnName("link2_text")
                    .HasColumnType("character varying");

                entity.Property(e => e.Link2Type)
                    .HasColumnName("link2_type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Magvar).HasColumnName("magvar");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Pdop).HasColumnName("pdop");

                entity.Property(e => e.Sat).HasColumnName("sat");

                entity.Property(e => e.Src)
                    .HasColumnName("src")
                    .HasColumnType("character varying");

                entity.Property(e => e.Sym)
                    .HasColumnName("sym")
                    .HasColumnType("character varying");

                entity.Property(e => e.Time).HasColumnName("time");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("character varying");

                entity.Property(e => e.Vdop).HasColumnName("vdop");
            });

            modelBuilder.HasSequence<int>("waypoints_01_id_seq");
        }
    }
}
