﻿using System;
using System.Collections.Generic;

namespace omms.data.GisEntities
{
    public partial class Reservoirs
    {
        public int Id { get; set; }
        public long? Pkuid { get; set; }
        public string Material { get; set; }
        public string TypeOfReservoir { get; set; }
        public string SpesficArea { get; set; }
        public string M3 { get; set; }
        public string Valve { get; set; }
    }
}
