﻿using System;
using System.Collections.Generic;

namespace omms.data.GisEntities
{
    public partial class SmallerPipes
    {
        public int Id { get; set; }
        public long? OgcFid { get; set; }
        public int? Pkuid { get; set; }
        public string Network { get; set; }
        public string Materials { get; set; }
        public string Diameters { get; set; }
        public string ZoneName { get; set; }
        public string Kebele { get; set; }
        public string SpecificArea { get; set; }
        public string Length { get; set; }
    }
}
