﻿using System;
using System.Collections.Generic;

namespace omms.data.GisEntities
{
    public partial class Valves
    {
        public int Id { get; set; }
        public long? Pkuid { get; set; }
        public string ValveType { get; set; }
        public string ValveFunction { get; set; }
        public string ValveTurningDir { get; set; }
        public string Network { get; set; }
        public string Material { get; set; }
        public string Diameter { get; set; }
        public string Status { get; set; }
        public string ZoneName { get; set; }
        public string KebeleName { get; set; }
        public string SpecificArea { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
    }
}
