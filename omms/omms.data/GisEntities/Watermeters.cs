﻿using System;
using System.Collections.Generic;

namespace omms.data.GisEntities
{
    public partial class Watermeters
    {
        public int Id { get; set; }
        public long? Pkuid { get; set; }
        public string Network { get; set; }
        public string Material { get; set; }
        public string Diameter { get; set; }
        public string Status { get; set; }
        public string ZoneName { get; set; }
        public string KebelName { get; set; }
        public string SpecificArea { get; set; }
        public long? MeterNo { get; set; }
        public double? CustomerCode { get; set; }
        public double? HouseNo { get; set; }
    }
}
