﻿using System;
using System.Collections.Generic;

namespace omms.data.GisEntities
{
    public partial class Waypoint03
    {
        public int Id { get; set; }
        public string X00 { get; set; }
        public string Y00 { get; set; }
    }
}
