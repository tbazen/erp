﻿using System;
using System.Collections.Generic;

namespace omms.data.GisEntities
{
    public partial class Waypoint08
    {
        public int Id { get; set; }
        public double? Ele { get; set; }
        public DateTime? Time { get; set; }
        public double? Magvar { get; set; }
        public double? Geoidheight { get; set; }
        public string Name { get; set; }
        public string Cmt { get; set; }
        public string Desc { get; set; }
        public string Src { get; set; }
        public string Link1Href { get; set; }
        public string Link1Text { get; set; }
        public string Link1Type { get; set; }
        public string Link2Href { get; set; }
        public string Link2Text { get; set; }
        public string Link2Type { get; set; }
        public string Sym { get; set; }
        public string Type { get; set; }
        public string Fix { get; set; }
        public int? Sat { get; set; }
        public double? Hdop { get; set; }
        public double? Vdop { get; set; }
        public double? Pdop { get; set; }
        public double? Ageofdgpsdata { get; set; }
        public int? Dgpsid { get; set; }
    }
}
