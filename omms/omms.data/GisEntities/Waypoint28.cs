﻿using System;
using System.Collections.Generic;

namespace omms.data.GisEntities
{
    public partial class Waypoint28
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double? Elevation { get; set; }
        public string Comment { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string Url { get; set; }
        public string UrlName { get; set; }
    }
}
