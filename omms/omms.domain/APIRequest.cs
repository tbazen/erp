﻿namespace omms.domain
{
    public class OmmsConfiguration
    {
        public string SecretKey { get; set; }
        public string Audience { get; set; }
        public string Issuer { get; set; }
    }
}
