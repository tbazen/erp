﻿using omms.data.Entities;
using omms.types.Models.Document;
using System;
using System.Collections.Generic;
using System.Linq;

namespace omms.domain.Documents
{
    public interface IDocumentService
    {

        List<DocumentType> GetAllDocumentTypes();
        List<DocumentResponse> GetDocumentResponsesByRef(string Ref);

        DocumentResponse GetDocumentResponse(Guid id);
        Document GetDocument(Guid? id);

        Document CreateDocument(DocumentRequest data);
        Document UpdateDocument(Guid id, DocumentRequest data);
        Document DeleteDocument(Guid id);
    }
    public class DocumentService : IDocumentService
    {
        OmmsContext Context;
        public void SetContext(OmmsContext context) => Context = context;

        public List<DocumentType> GetAllDocumentTypes()
        {
            var docs = Context.DocumentType.ToList();
            return docs;
        }

        public List<DocumentResponse> GetDocumentResponsesByRef(string Ref)
        {
            var docs = Context.Document.Where(m => m.Ref == Ref).ToList();
            return docs.Select(ParseDocumentResponse).ToList();
        }

        public Document GetDocument(Guid? id)
        {
            return id == null ? null : Context.Document.Find(id);
        }

        public DocumentResponse GetDocumentResponse(Guid id)
        {
            var doc = Context.Document.Find(id);
            return ParseDocumentResponse(doc);
        }


        public Document CreateDocument(DocumentRequest data)
        {
            var doc = ParseDocument(data);
            Context.Document.Add(doc);
            Context.SaveChanges();

            return doc;
        }

        public Document UpdateDocument(Guid id, DocumentRequest data)
        {
            var doc = GetDocument(id);

            doc.Date = data.Date;
            doc.Ref = data.Ref;
            doc.Note = data.Note;
            doc.Mimetype = data.Mimetype;
            doc.Type = data.Type;
            doc.Filename = data.Filename;
            if (data.File != null) doc.File = Convert.FromBase64String(data.File);
            doc.OverrideFilePath = data.OverrideFilePath;

            Context.Document.Update(doc);
            doc.Aid = Context.SaveChanges();
            Context.Update(doc);

            return doc;
        }

        public Document DeleteDocument(Guid id)
        {
            var doc = GetDocument(id);

            Context.Document.Remove(doc);
            doc.Aid = Context.SaveChanges();

            return doc;
        }

        public static DocumentResponse ParseDocumentResponse(Document doc)
        {
            return doc == null
                ? null
                : new DocumentResponse
                {
                    Id = doc.Id,
                    Date = doc.Date,
                    Filename = doc.Filename,
                    Mimetype = doc.Mimetype,
                    Note = doc.Note,
                    Ref = doc.Ref,
                    Type = doc.Type,
                    OverrideFilePath = doc.OverrideFilePath
                };
        }

        public static Document ParseDocument(DocumentRequest data)
        {
            if (data == null)
                return null;
            try
            {
                Document doc = new Document
                {
                    Id = Guid.NewGuid(),
                    Date = data.Date,
                    Ref = data.Ref,
                    Note = data.Note,
                    Mimetype = data.Mimetype,
                    Type = data.Type,
                    Filename = data.Filename,
                    File = Convert.FromBase64String(data.File),
                    OverrideFilePath = data.OverrideFilePath
                };
                doc.OverrideFilePath += (doc.Id.ToString() + doc.Filename);
                data.OverrideFilePath = doc.OverrideFilePath;
                return doc;
            }
            catch (Exception e)
            {
                Console.WriteLine($"FAILED TO PARSE {e.Message}");
                return null;
            }

        }

    }
}
