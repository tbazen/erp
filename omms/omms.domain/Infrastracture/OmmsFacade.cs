﻿using omms.data.Entities;
using omms.data.GisEntities;

namespace omms.domain.Infrastracture
{
    public interface IOmmsFacade
    {
        void PassContext(IOmmsService service, OmmsContext context);
        void PassGisContext(IOmmsService service, OmmsGisContext context);
    }
    public abstract class OmmsFacade : IOmmsFacade
    {
        public virtual void PassContext(IOmmsService service, OmmsContext context)
        {
            service.SetContext(context);
        }

        public virtual void PassGisContext(IOmmsService service, OmmsGisContext context)
        {
            service.SetGisContext(context);
        }
    }
}
