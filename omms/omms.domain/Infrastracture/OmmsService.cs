﻿using omms.data.Entities;
using omms.data.GisEntities;
using omms.types.Models;

namespace omms.domain.Infrastracture
{
    public interface IOmmsService
    {
        void SetContext(OmmsContext context);
        OmmsContext GetContext();
        void SetSession(UserSession session);

        OmmsGisContext GetGisContext();
        void SetGisContext(OmmsGisContext _gisContext);
    }

    public class OmmsService : IOmmsService
    {
        protected UserSession _session;
        protected OmmsContext Context;
        protected OmmsGisContext GisContext;

        public OmmsContext GetContext() => Context;
        public void SetContext(OmmsContext context) => Context = context;

        public void SetSession(UserSession session)
        {
            _session = session;
        }
        public UserSession GetSession() => _session;

        public OmmsGisContext GetGisContext() => GisContext;

        public void SetGisContext(OmmsGisContext _gisContext) => GisContext = _gisContext;
    }
}
