﻿using omms.data.Entities;
using omms.domain.Infrastracture;
using omms.domain.TaskServices.Helper;
using omms.domain.TaskServices.SharedServices;
using omms.domain.Workflow.services;
using omms.types.Models;
using omms.types.Models.Thread;
using omms.types.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;

namespace omms.domain.TaskServices.AdminServices
{
    public class AdminService : OmmsService, IAdminService
    {
        readonly SharedService sharedService;
        public AdminService()
        {
            sharedService = new SharedService();
        }

        public TaskResponse CreateTask(TaskRequest model)
        {
            var taskId = SaveTaskData(model);
            sharedService.SetContext(Context);
            TaskResponse tsk = sharedService.PopulateTaskData(taskId);
            return tsk;
        }

        private Guid SaveTaskData(TaskRequest model, Task task = null)
        {
            #region Adding New Task
            if (task == null)
            {
                task = new Task
                {
                    Id = Guid.NewGuid(),
                    Name = model.Name,
                    Description = model.Description,
                    StartDateExpected = model.StartDateExpected,
                    StartDateActual = model.StartDateActual,
                    EndDateExpected = model.EndDateExpected,
                    EndDateActual = model.EndDateActual,
                    Type = (int)model.TaskType,
                    MaintenanceType = (int)model.MaintenanceType,
                    CreatedBy = model.CreatedBy
                };
                Context.Task.Add(task);
                Context.SaveChanges();

                switch (model.TaskType)
                {
                    case types.Models.TaskType.Periodic:
                        Context.PeriodicTask.Add(new PeriodicTask
                        {
                            TaskId = task.Id,
                            Interval = (int)model.IntervalDays
                        });
                        Context.SaveChanges();
                        break;

                    case types.Models.TaskType.Incident:
                        Context.IncidentTask.Add(new IncidentTask
                        {
                            TaskId = task.Id,
                            GeoLocation = model.IncidentInfo.GeoLocation,
                            Incident = model.IncidentInfo.Incident,
                            Description = model.IncidentInfo.Description,
                            ReportedBy = model.IncidentInfo.ReportedBy
                        });
                        Context.SaveChanges();

                        Documents.DocumentService documentService = new Documents.DocumentService();
                        documentService.SetContext(Context);
                        model.IncidentInfo.Images.ForEach(
                            img =>
                            {
                                Document doc = documentService.CreateDocument(img);
                                Context.IncidentTaskImages.Add(
                                    new IncidentTaskImages
                                    {
                                        ImageId = doc.Id,
                                        TaskId = task.Id
                                    });
                                Context.SaveChanges();
                            });
                        break;
                    default:
                        break;
                }

            }
            #endregion

            #region Adding Task dependency
            model.DependUpon?.ForEach(
                du =>
                {
                    TaskDependency dp = new TaskDependency
                    {
                        Dependent = task.Id,
                        DependUpon = du
                    };
                    Context.TaskDependency.Add(dp);
                    Context.SaveChanges();
                });
            #endregion

            #region Setting TaskPriority
            TaskPriority priority = new TaskPriority
            {
                PriorityId = (int)model.Priority,
                TaskId = task.Id
            };
            Context.TaskPriority.Add(priority);
            Context.SaveChanges();
            #endregion

            #region Setting Assigned type  Employee and Team
            if (model.TaskAssignee != null)
                AssignTaskHelper(model.TaskAssignee, task.Id);

            #endregion

            #region Setting Task Budget
            //data.Entities.Budget budget = new data.Entities.Budget
            //{
            //    EstimatedCost = model.Budget.EstimatedCost,
            //    ActualCost = model.Budget.ActualCost
            //};
            //Context.Budget.Add(budget);
            //Context.SaveChanges();
            #endregion

            #region Setting Allocated Equipment

            //model.Equipment?.ForEach(
            //    eq =>
            //    {
            //        TaskEquipment eqpment = new TaskEquipment
            //        {
            //            TaskId = task.Id,
            //            Quantity = eq.Quantity,
            //            CostId = eq.CostId,
            //            EquipmentId = eq.Id,
            //            AllocatedDate = eq.AllocatedDate == null ? DateTime.Now : eq.AllocatedDate
            //        };
            //        Context.TaskEquipment.Add(eqpment);
            //        Context.SaveChanges();
            //    });


            #endregion

            #region saving warranty document
            if (model.WarrantyDocument != null)
            {
                Documents.DocumentService documentService = new Documents.DocumentService();
                documentService.SetContext(Context);
                Document doc = documentService.CreateDocument(model.WarrantyDocument);
                task.TaskWarranty.Add(new TaskWarranty
                {
                    TaskId = task.Id,
                    WarrantyDocument = doc.Id
                });
                Context.SaveChanges();
            }
            #endregion

            #region Setting task Status
            //if (task != null)
            //{
            //    if (model.TaskStatus != null)
            //    {
            //        model.TaskStatus.TaskId = task.Id;
            //        sharedService.SetContext(Context);
            //        sharedService.UpdateTaskStatus(model.TaskStatus);
            //    }
            //}
            //else
            //{
            //    if (model.TaskStatus != null)
            //    {
            //        model.TaskStatus.TaskId = task.Id;
            //        sharedService.SetContext(Context);
            //        sharedService.UpdateTaskStatus(model.TaskStatus);
            //    }
            //}

            #endregion
            return task.Id;
        }

        public TaskResponse SearchTaskById(Guid taskId)
        {
            var task = Context.Task.FirstOrDefault(t => t.Id == taskId);
            sharedService.SetContext(Context);
            return task == null ?
                 null : sharedService.PopulateTaskData(taskId);
        }

        public List<TaskResponse> GetAllTasks()
        {
            var taskList = Context.Task.ToList();
            List<TaskResponse> taskModelList = new List<TaskResponse>();
            sharedService.SetContext(Context);
            taskList?.ForEach(
                tsk =>
                {
                    var wf = Context.Workflow.Find(tsk.Wfid);
                    switch ((TaskState)wf.CurrentState)
                    {
                        case TaskState.Cancelled:
                        case TaskState.Completed:
                        case TaskState.RegistrationApprived:
                        case TaskState.Terminated:
                            taskModelList.Add(sharedService.PopulateTaskData(tsk.Id));
                            break;
                    }
                });

            return taskModelList;
        }
        public bool DeleteTask(Guid taskId)
        {
            var tsk = Context.Task.FirstOrDefault(t => t.Id == taskId);
            if (tsk == null)
                return false;
            RemoveTaskData(tsk);
            Context.Remove(tsk);
            Context.SaveChanges();
            return true;
        }
        private void RemoveTaskData(Task task)
        {

            var priority = Context.TaskPriority.Where(p => p.TaskId == task.Id).ToList();
            Context.TaskPriority.RemoveRange(priority);
            Context.SaveChanges();

            var bdget = Context.TaskBudget.Where(b => b.TaskId == task.Id).ToList();
            Context.TaskBudget.RemoveRange(bdget);
            Context.SaveChanges();

            var depend = Context.TaskDependency.Where(d => d.Dependent == task.Id).ToList();
            Context.TaskDependency.RemoveRange(task.TaskDependencyDependentNavigation);
            Context.SaveChanges();

            var tEqp = Context.TaskEquipment.Where(eq => eq.TaskId == task.Id).ToList();
            Context.TaskEquipment.RemoveRange(tEqp);
            Context.SaveChanges();

            var empTask = Context.EmployeeTask.Where(et => et.TaskId == task.Id).ToList();
            Context.EmployeeTask.RemoveRange(empTask);
            Context.SaveChanges();

            var teamTask = Context.TeamTask.Where(tm => tm.TaskId == task.Id).ToList();
            Context.TeamTask.RemoveRange(teamTask);
            Context.SaveChanges();

            var tskWar = Context.TaskWarranty.Where(tw => tw.TaskId == task.Id);
            Context.TaskWarranty.RemoveRange(tskWar);
            Context.SaveChanges();

            var tskStat = Context.TaskStatus.Where(st => st.TaskId == task.Id).ToList();
            Context.TaskStatus.RemoveRange(tskStat);
            Context.SaveChanges();

            var asgnee = Context.TaskAssignee.Where(ta => ta.TaskId == task.Id).ToList();
            Context.TaskAssignee.RemoveRange(asgnee);
            Context.SaveChanges();

        }
        public TaskResponse UpdateTask(TaskRequest newTask)
        {
            var tsk = Context.Task.FirstOrDefault(t => t.Id == newTask.Id);
            if (tsk == null)
                return null;

            RemoveTaskData(tsk);
            SaveTaskData(newTask, tsk);

            tsk.Name = newTask.Name;
            tsk.StartDateExpected = newTask.StartDateExpected;
            tsk.StartDateActual = newTask.StartDateActual;
            tsk.EndDateExpected = newTask.EndDateExpected;
            tsk.EndDateActual = newTask.EndDateActual;
            tsk.Description = newTask.Description;
            tsk.Type = (int)newTask.TaskType;
            tsk.MaintenanceType = (int)newTask.MaintenanceType;

            Context.Task.Update(tsk);
            Context.SaveChanges();

            sharedService.SetContext(Context);
            var mdl = sharedService.PopulateTaskData(tsk.Id);
            return mdl;
        }
        private TaskResponse AssignTaskHelper(Assignee assignee, Guid taskId)
        {

            sharedService.SetContext(Context);
            var assigneeType = Context.TaskAssignee.FirstOrDefault(ts => ts.TaskId == taskId);
            if (assigneeType == null)
            {
                Context.TaskAssignee.Add(new TaskAssignee
                {
                    TaskId = taskId,
                    Asignee = (int)assignee.AssigneeType
                });
                Context.SaveChanges();
            }
            else
            {
                assigneeType.Asignee = (int)assignee.AssigneeType;
                Context.TaskAssignee.Update(assigneeType);
                Context.SaveChanges();
            }

            switch (assignee.AssigneeType)
            {
                case AssigneeType.None:
                    return sharedService.GetTaskById(taskId);
                case AssigneeType.Employee:
                    EmployeeTask et = new EmployeeTask
                    {
                        TaskId = taskId,
                        EmployeeId = assignee.AssigneeModel.AssigneeId,
                        DateAssigned = DateTime.Now
                    };
                    Context.EmployeeTask.Add(et);
                    Context.SaveChanges();
                    return sharedService.GetTaskById(taskId);
                case AssigneeType.Team:

                    TeamTask ts = new TeamTask
                    {
                        TaskId = taskId,
                        TeamId = Convert.ToInt64(assignee.AssigneeModel.AssigneeId),
                        DateAssigned = DateTime.Now
                    };
                    Context.TeamTask.Add(ts);
                    Context.SaveChanges();
                    //opening thread for team 
                    sharedService.SetContext(Context);


                    //long teamId = Convert.ToInt64(assignee.AssigneeModel.AssigneeId);
                    //var team = sharedService.GetTeam(teamId);
                    //var task = sharedService.GetTaskById(assignee.AssigneeModel.TaskId);
                    //CreateTeamThread(team, task);

                    return sharedService.GetTaskById(taskId);
                default:
                    return sharedService.GetTaskById(taskId);
            }

        }
        public TaskResponse AssignTask(Assignee assignee)
        {
            var tsk = Context.Task.FirstOrDefault(t => t.Id == assignee.AssigneeModel.TaskId);
            if (tsk == null)
                return null;
            return AssignTaskHelper(assignee, tsk.Id);
        }
        private ThreadModel CreateTeamThread(TeamModel team, TaskRequest task)
        {
            //creating thread
            Thread thread = new Thread
            {
                Id = Guid.NewGuid(),
                Name = task.Name,
                Description = task.Description,
                State = (int)ThreadState.Open
            };
            Context.Thread.Add(thread);
            Context.SaveChanges();

            //assigning team to the thread

            //TaskThread teamThread = new TaskThread
            //{
            //    TaskId = (Guid)task.Id,
            //    ThreadId = thread.Id,
            //    TeamId = team.Id
            //};
            //Context.TaskThread.Add(teamThread);
            //Context.SaveChanges();

            sharedService.SetContext(Context);
            return sharedService.GetThreadById(thread.Id);
        }
        public List<TaskResponse> GetTaskSortedByDate()
        {
            var tskList = Context.Task.OrderBy(t => t.StartDateExpected).ToList();
            var tskmdlList = GetTaskModelList(tskList).ToList();
            return tskmdlList;
        }


        private IEnumerable<TaskResponse> GetTaskModelList(List<Task> tskList)
        {
            foreach (var ts in tskList)
            {
                sharedService.SetContext(Context);
                yield return sharedService.PopulateTaskData(ts.Id);
            }
        }


        #region Team Management task
        public TeamModel CreateTeam(TeamModel team)
        {
            Team t = new Team
            {
                Name = team.Name,
                Description = team.Description
            };
            Context.Team.Add(t);
            Context.SaveChanges();


            //Adding team members
            team.Members.ForEach(
                m =>
                {
                    TeamMembers mem = new TeamMembers
                    {
                        TeamId = t.Id,
                        MemberId = m.Id
                    };
                    Context.TeamMembers.Add(mem);
                    Context.SaveChanges();
                });

            //getting team info from database
            sharedService.SetContext(Context);
            return sharedService.GetTeam(t.Id);
        }



        private void RemoveTeamMembers(long teamId)
        {

            var members = Context.TeamMembers.Where(m => m.TeamId == teamId);
            Context.TeamMembers.RemoveRange(members);
            Context.SaveChanges();
        }
        public bool DeleteTeam(long teamId)
        {
            var team = Context.Team.FirstOrDefault(t => t.Id == teamId);
            if (team == null)
                return false;
            RemoveTeamMembers(team.Id);
            Context.Team.Remove(team);
            Context.SaveChanges();
            return true;
        }
        public bool RemoveTeamMember(long teamId, string memberId)
        {
            var team = Context.Team.FirstOrDefault(t => t.Id == teamId);
            if (team == null)
                return false;

            var member = team.TeamMembers.FirstOrDefault(m => m.MemberId == memberId);
            Context.TeamMembers.Remove(member);
            Context.SaveChanges();
            return true;
        }


        #endregion

        #region notification
        public List<NotificationModel> GetNotification()
        {
            string empId = EmployeeHelper.GetEmployeeByLoginName(GetSession().Username).Id;
            var empTsk = Context.Task.Where(t => t.CreatedBy == empId).ToList();
            var tskIdlist = new List<Guid>();
            List<NotificationModel> notificationModels = new List<NotificationModel>();
            //empTsk?.ForEach(et => tskIdlist.Add(et.Id));

            //var notifications = Context.NotificationConfiguration
            //    .Where(n => tskIdlist.Contains(n.TaskId) && n.Seen == false)
            //    .ToList();


            //var notificationByStatus = notifications.Where(n => n.OnState != null).ToList();
            //var notificationByDate = notifications.Where(n => n.DaysBeforeStart != null || n.DaysBeforeEnd != null).ToList();


            //sharedService.SetContext(Context);
            //notificationByStatus?.ForEach(
            //    ntf =>
            //    {
            //        if (ntf.OnState == (int)sharedService.GetLatestStatus(ntf.TaskId)?.Status)
            //        {
            //            notificationModels.Add(new NotificationModel
            //            {
            //                Task = sharedService.GetTaskById(ntf.TaskId),
            //                NotificationMessage = ntf.NotificationMessage
            //            });
            //            ntf.Seen = true;
            //        }
            //    });


            //notificationByDate?.ForEach(
            //    ntf =>
            //    {
            //        if (ntf.DaysBeforeStart != null)
            //        {
            //            if (DateTime.Now.AddDays((int)ntf.DaysBeforeStart) >= sharedService.PopulateTaskData(ntf.TaskId).StartDateExpected)
            //            {
            //                notificationModels.Add(new NotificationModel
            //                {
            //                    Task = sharedService.GetTaskById(ntf.TaskId),
            //                    NotificationMessage = ntf.NotificationMessage
            //                });
            //                ntf.Seen = true;
            //            }
            //        }
            //        if (ntf.DaysBeforeEnd != null)
            //        {
            //            if (DateTime.Now.AddDays((int)ntf.DaysBeforeEnd) >= sharedService.PopulateTaskData(ntf.TaskId).EndDateExpected)
            //            {
            //                notificationModels.Add(new NotificationModel
            //                {
            //                    Task = sharedService.GetTaskById(ntf.TaskId),
            //                    NotificationMessage = ntf.NotificationMessage
            //                });
            //                ntf.Seen = true;
            //            }
            //        }
            //    });


            ////setting notifications as seen
            //Context.UpdateRange(notificationByStatus);
            //Context.UpdateRange(notificationByDate);
            //Context.SaveChanges();
            return notificationModels;
        }

        public bool SetNotification(NotificationModel notification)
        {
            try
            {
                Context.NotificationConfiguration.Add(new NotificationConfiguration
                {
                    DaysBeforeStart = (int)notification.DaysBeforeStart,
                    OnState = (int)notification.OnStatus,
                    NotificationMessage = notification.NotificationMessage,
                    TaskId = (Guid)notification.Task.Id,
                    DaysBeforeEnd = (int)notification.DaysBeforeEnd
                });
                Context.SaveChanges();

                if (notification.NotificationEmail != null)
                {
                    MailStore mail = new MailStore
                    {
                        Email = notification.NotificationEmail,
                        Description = notification.NotificationMessage,
                        TaskId = (Guid)notification.Task.Id
                    };
                    Context.MailStore.Add(mail);
                    Context.SaveChanges();

                    var tempTask = SearchTaskById((Guid)notification.Task.Id);
                    if (notification.DaysBeforeStart != null)
                    {
                        Context.MailSchedule.Add(new MailSchedule
                        {
                            MailId = mail.Id,
                            Schedule = (DateTime)tempTask.StartDateExpected,
                            StateId = (int)types.Models.MailState.NotSent
                        });
                        Context.SaveChanges();
                    }
                    if (notification.DaysBeforeEnd != null)
                    {
                        Context.MailSchedule.Add(new MailSchedule
                        {
                            MailId = mail.Id,
                            Schedule = (DateTime)tempTask.EndDateExpected,
                            StateId = (int)types.Models.MailState.NotSent
                        });
                        Context.SaveChanges();
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<NotificationModel> GetNewNotifications()
        {
            string empId = EmployeeHelper.GetEmployeeByLoginName(GetSession().Username).Id;
            var empTsk = Context.Task.Where(t => t.CreatedBy == empId).ToList();
            if (empTsk == null)
                return null;

            var tskIdlist = new List<Guid>();
            List<NotificationModel> notificationModels = new List<NotificationModel>();
            //empTsk?.ForEach(et => tskIdlist.Add(et.Id));

            //var notifications = Context.NotificationConfiguration
            //    .Where(n => tskIdlist.Contains(n.TaskId) && n.Seen == false)
            //    .ToList();


            //var notificationByStatus = notifications.Where(n => n.OnState != null).ToList();
            //var notificationByDate = notifications.Where(n => n.DaysBeforeStart != null || n.DaysBeforeEnd != null).ToList();

            //sharedService.SetContext(Context);

            //notificationByStatus?.ForEach(
            //    ntf =>
            //    {
            //        if (ntf.OnState == (int)sharedService.GetLatestStatus(ntf.TaskId)?.Status)
            //        {
            //            notificationModels.Add(new NotificationModel
            //            {
            //                Task = sharedService.GetTaskById(ntf.TaskId),
            //                NotificationMessage = ntf.NotificationMessage
            //            });
            //        }
            //    });


            //notificationByDate?.ForEach(
            //    ntf =>
            //    {
            //        if (ntf.DaysBeforeStart != null)
            //        {
            //            if (DateTime.Now.AddDays((int)ntf.DaysBeforeStart) >= sharedService.PopulateTaskData(ntf.TaskId).StartDateExpected)
            //            {
            //                notificationModels.Add(new NotificationModel
            //                {
            //                    Task = sharedService.GetTaskById(ntf.TaskId),
            //                    NotificationMessage = ntf.NotificationMessage
            //                });
            //            }
            //        }
            //        if (ntf.DaysBeforeEnd != null)
            //        {
            //            if (DateTime.Now.AddDays((int)ntf.DaysBeforeEnd) >= sharedService.PopulateTaskData(ntf.TaskId).EndDateExpected)
            //            {
            //                notificationModels.Add(new NotificationModel
            //                {
            //                    Task = sharedService.GetTaskById(ntf.TaskId),
            //                    NotificationMessage = ntf.NotificationMessage
            //                });
            //            }
            //        }
            //    });

            return notificationModels;
        }

        public List<UserModel> GetAllEmployees() => EmployeeHelper.GetAllEmployees();

        #endregion

        public List<TaskResponse> GetAssetTasks(int assetId)
        {
            List<TaskResponse> tasks = new List<TaskResponse>();
            var tas = Context.AssetTask.Where(a => a.AssetId == assetId).ToList();
            tas.ForEach(
                t =>
                {
                    var task = Context.Task.Find(t.TaskId);
                    var wf = Context.Workflow.Find(task.Wfid);
                    if (wf.CurrentState == (int)TaskState.RegistrationApprived)
                    {
                        sharedService.SetContext(Context);
                        tasks.Add(sharedService.GetTaskById(t.TaskId));
                    }
                });
            return tasks;
        }
    }
}
