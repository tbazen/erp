﻿using omms.types.Models;
using omms.types.Models.User;
using System;
using System.Collections.Generic;

namespace omms.domain.TaskServices.AdminServices
{
    public interface IAdminService
    {
        TaskResponse CreateTask(TaskRequest model);
        bool DeleteTask(Guid taskId);
        TaskResponse UpdateTask(TaskRequest newTask);
        TaskResponse AssignTask(Assignee assignee);

        TaskResponse SearchTaskById(Guid taskId);
        List<TaskResponse> GetAllTasks();



        List<UserModel> GetAllEmployees();

        //Team management
        TeamModel CreateTeam(TeamModel team);
        bool DeleteTeam(long teamId);
        bool RemoveTeamMember(long teamId, string memberId);

        List<TaskResponse> GetTaskSortedByDate();


        List<TaskResponse> GetAssetTasks(int assetId);


        //configuration mail
        bool SetNotification(NotificationModel notification);
        List<NotificationModel> GetNotification();
        List<NotificationModel> GetNewNotifications();

    }
}
