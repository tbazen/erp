﻿using OfficeOpenXml;
using omms.domain.Documents;
using omms.domain.Infrastracture;
using omms.domain.TaskServices.Helper;
using omms.domain.Workflow.services;
using omms.types.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace omms.domain.TaskServices
{
    public class AssetService : OmmsService, IAssetService
    {
        AssetWorkflow _assetWorkFlow;
        DocumentService _documentService;
        public AssetService()
        {
            _assetWorkFlow = new AssetWorkflow(this);
            _documentService = new DocumentService();
        }

        public AssetFormSchema GetAssetFormSchema()
        {

            AssetFormSchema form = new AssetFormSchema();
            var types = Context.AssetType.ToList();
            types.ForEach(
                t =>
                {
                    var schema = Context.AssetSchema.Where(s => s.TypeId == t.Id).ToList();
                    form.AssetTypes.Add(new omms.types.Models.AssetType
                    {
                        Id = t.Id,
                        Name = t.Name,
                        Attributes = GetSchema(t.Id)
                    });
                });
            return form;
        }

        public List<Schema> GetSchema(int assetType)
        {
            List<Schema> attributes = new List<Schema>();
            var schema = Context.AssetSchema.Where(s => s.TypeId == assetType).ToList();
            schema.ForEach(
                sm =>
                {
                    attributes.Add(new Schema
                    {
                        Id = sm.Id,
                        AttributeName = sm.AttributeName,
                        Priority = sm.Priority,
                        ValueType = sm.ValueType
                    });
                });
            return attributes;
        }

        public types.Models.AssetReadingSchema GetAssetReadingSchema(int assetType)
        {
            types.Models.AssetReadingSchema readingSchema = new types.Models.AssetReadingSchema();
            var type = Context.AssetType.Find(assetType);
            var schema = Context.AssetReadingSchema.Where(a => a.AssetType == assetType).ToList();

            readingSchema.Reading.Id = type.Id;
            readingSchema.Reading.Name = type.Name;

            schema.ForEach(
                s =>
                {
                    readingSchema.Reading.Attributes.Add(new Schema
                    {
                        Id = s.Id,
                        AttributeName = s.Name,
                        Priority = s.Priority,
                        ValueType = s.ValueType
                    });
                });

            return readingSchema;
        }

        public Guid RequestAssetRegistration(AssetRequest request, Guid? wfid)
        {
            _assetWorkFlow.SetContext(Context);
            _documentService.SetContext(Context);
            request.Images?.ForEach(img =>
            {
                var doc = _documentService.CreateDocument(img);
                img.Id = doc.Id;
                img.File = null;
                img.Filename = null;
                img.Mimetype = null;
                img.Note = null;
            });
            _assetWorkFlow.SetSession(GetSession());
            return _assetWorkFlow.RequestAssetRegistration(request, wfid);
        }

        internal void RegisterAsset(AssetRequest request)
        {
            data.Entities.Asset asset = new data.Entities.Asset
            {
                Name = request.Name,
                Description = request.Description,
                GeoLocation = request.GeoLocation,
                TypeId = request.TypeId,
                Pkuid = request.PkuId
            };
            Context.Asset.Add(asset);
            Context.SaveChanges();

            request.Images.ForEach(
                img =>
                {
                    Context.AssetImages.Add(new data.Entities.AssetImages
                    {
                        AssetId = asset.Id,
                        DocumentId = (Guid)img.Id
                    });
                    Context.SaveChanges();
                });
        }

        public List<AssetResponse> GetPendingAssets()
        {
            _assetWorkFlow.SetContext(Context);
            _assetWorkFlow.SetGisContext(GisContext);
            _assetWorkFlow.SetSession(GetSession());
            return _assetWorkFlow.GetPendingAssets();
        }

        public Guid ApproveAssetRegistration(Guid wfid)
        {
            _assetWorkFlow.SetContext(Context);
            _assetWorkFlow.SetSession(GetSession());
            return _assetWorkFlow.ApproveAssetRegistration(wfid);
        }

        public Guid RejectAssetRegistration(WFRequestModel request)
        {
            _assetWorkFlow.SetContext(Context);
            _assetWorkFlow.SetSession(GetSession());
            return _assetWorkFlow.RejectAssetRegistration(request);
        }

        public AssetResponse GetAssetById(int id)
        {
            var asset = Context.Asset.FirstOrDefault(ast => ast.Id == id);
            if (asset == null)
                return null;
            DocumentService documentService = new DocumentService();
            AssetResponse response = new AssetResponse
            {
                Id = asset.Id,
                Name = asset.Name,
                Description = asset.Description,
                GeoLocation = asset.GeoLocation,
                PkuId = asset.Pkuid,
                Wfid = null
            };
            response.AssetType = GetAssetInfoByPUKID(
                new AssetSearchParams
                {
                    AssetType = Context.AssetType.Find(asset.TypeId).Name,
                    PkuId = asset.Pkuid
                });
            documentService.SetContext(Context);
            var imgs = Context.AssetImages.Where(im => im.AssetId == asset.Id).ToList();
            imgs.ForEach(i =>
            {
                response.Images.Add(documentService.GetDocumentResponse(i.DocumentId));
            });

            return response;
        }

        public AssetResponse UpdateAsset(AssetRequest asset)
        {
            var ast = Context.Asset.FirstOrDefault(a => a.Id == asset.Id);
            if (ast == null)
                return null;
            ast.Name = asset.Name;
            ast.Description = asset.Description;
            ast.GeoLocation = asset.GeoLocation;
            ast.TypeId = asset.TypeId;
            ast.Pkuid = asset.PkuId;
            Context.Asset.Update(ast);
            Context.SaveChanges();
            return GetAssetById(ast.Id);
        }

        public bool DeleteAsset(int assetId)
        {
            var ast = Context.Asset.FirstOrDefault(a => a.Id == assetId);
            if (ast == null)
                return false;
            Context.Asset.Remove(ast);
            Context.SaveChanges();
            return true;
        }

        internal List<AssetResponse> GetRejectedAssets()
        {
            _assetWorkFlow.SetContext(Context);
            _assetWorkFlow.SetSession(GetSession());
            return _assetWorkFlow.GetRejectedAssets();
        }

        public AssetResponse GetAssetByLocation(string lid)
        {
            var asset = Context.Asset.FirstOrDefault(a => a.GeoLocation == lid);
            if (asset == null)
                return null;
            return GetAssetById(asset.Id);
        }

        public int AddAssetReading(AssetReading reading)
        {
            var employeeId = Helper.EmployeeHelper.GetEmployeeByLoginName(GetSession().Username).Id;
            data.Entities.AssetRecord record = new data.Entities.AssetRecord
            {
                AssetId = (int)reading.Asset.Id,
                ReadingDate = DateTime.Now,
                Reading = Newtonsoft.Json.JsonConvert.SerializeObject(reading.Readings),
                ReadingBy = employeeId
            };
            Context.AssetRecord.Add(record);
            Context.SaveChanges();
            return record.Id;
        }

        public List<AssetReading> GetAssetReading(int assetId)
        {
            List<AssetReading> readings = new List<AssetReading>();
            var rid = Context.AssetRecord.Where(a => a.AssetId == assetId).ToList();
            rid.ForEach(
                record =>
                {
                    AssetReading reading = new AssetReading
                    {
                        Id = record.Id,
                        DateRecorded = record.ReadingDate,
                        Readings = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Schema>>(record.Reading),
                        Asset = GetAssetById(record.AssetId),
                        ReadingBy = Helper.EmployeeHelper.GetEmployeeByLoginName(GetSession().Username)
                    };
                    readings.Add(reading);
                });
            return readings;
        }

        internal types.Models.AssetType GetAssetInfoByPUKID(AssetSearchParams searchParams)
        {
            AssetHelper _helper = new AssetHelper();
            _helper.SetContext(Context);
            _helper.SetGisContext(GisContext);
            return _helper.GetAdditionalAssetInformationByPUKID(searchParams);
        }

        internal List<AssetReportModel> GetAssetReport()
        {
            List<AssetReportModel> report = new List<AssetReportModel>();
            var assets = Context.Asset.ToList();
            assets.ForEach(
                asset =>
                {
                    AssetReportModel model = new AssetReportModel
                    {
                        Id = asset.Id.ToString(),
                        Name = asset.Name,
                        AssetType = Context.AssetType.Find(asset.TypeId).Name,
                        CancelledTasks = GetTaskByStateAndAsset(asset.Id, TaskState.Cancelled),
                        CompletedTasks = GetTaskByStateAndAsset(asset.Id, TaskState.Completed),
                        OnGoingTasks = GetTaskByStateAndAsset(asset.Id, TaskState.RegistrationApprived),
                        TerminatedTasks = GetTaskByStateAndAsset(asset.Id, TaskState.Terminated)
                    };
                    model.TotalTasks = model.CancelledTasks +
                                       model.CompletedTasks +
                                       model.TerminatedTasks +
                                       model.OnGoingTasks;

                    report.Add(model);
                });
            return report;
        }


        internal byte[] GetAssetReportDocument(string docType, string filterParams)
        {
            using (ExcelPackage package = new ExcelPackage())
            {

                string workSheetName = "asset report";
                package.Workbook.Properties.Title = AssetReport.Metadata.Title;
                package.Workbook.Properties.Author = ReportConstants.Author;
                package.Workbook.Properties.Subject = ReportConstants.Subject;
                package.Workbook.Properties.Keywords = ReportConstants.key;
                package.Workbook.Properties.Created = DateTime.Now;

                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(workSheetName);

                worksheet.Cells[1, 1].Value = AssetReport.Id;
                worksheet.Cells[1, 2].Value = AssetReport.Name;
                worksheet.Cells[1, 3].Value = AssetReport.AssetType;

                worksheet.Cells[1, 4].Value = AssetReport.OnGoingTasks;
                worksheet.Cells[1, 5].Value = AssetReport.CompletedTasks;
                worksheet.Cells[1, 6].Value = AssetReport.CancelledTasks;
                worksheet.Cells[1, 7].Value = AssetReport.TerminatedTasks;
                worksheet.Cells[1, 8].Value = AssetReport.TotalTasks;


                var assetReport = GetAssetReport();

                List<AssetReportModel> filteredReport = new List<AssetReportModel>();
                if (String.IsNullOrWhiteSpace(filterParams))
                    filteredReport = assetReport;
                else
                    filteredReport = FilterAssetReport(assetReport, filterParams.Split(';'));


                int row = 2;
                filteredReport.ForEach(
                    report =>
                    {
                        worksheet.Cells[row, 1].Value = report.Id.ToString();
                        worksheet.Cells[row, 2].Value = report.Name;
                        worksheet.Cells[row, 3].Value = report.AssetType;

                        worksheet.Cells[row, 4].Value = report.OnGoingTasks;
                        worksheet.Cells[row, 5].Value = report.CompletedTasks;
                        worksheet.Cells[row, 6].Value = report.CancelledTasks;
                        worksheet.Cells[row, 7].Value = report.TerminatedTasks;
                        worksheet.Cells[row, 8].Value = report.TotalTasks;
                        ++row;
                    });

                PDFConverter pdfConverter = new PDFConverter();
                if (docType.ToLower() == RPDocumentType.EXCEL)
                    return package.GetAsByteArray();
                return pdfConverter.ConvertExcelToPdfReport(package, workSheetName, AssetReport.Metadata);
            }
        }


        private List<AssetReportModel> FilterAssetReport(List<AssetReportModel> assetReport, string[] filterParams)
        {
            List<AssetReportModel> filteredReport = new List<AssetReportModel>();
            var filterParamsList = filterParams.ToList();
            assetReport.ForEach(
                empRprt =>
                {
                    bool match = false;
                    filterParamsList.ForEach(
                        fp =>
                        {
                            switch ((ERDFilterParams)(Convert.ToInt32(fp)))
                            {
                                case ERDFilterParams.ASSIGNED_EMPLYEES:
                                    if (empRprt.TotalTasks > 0)
                                        match = true;
                                    break;

                                case ERDFilterParams.UNASSIGNED_EMPLOYEES:
                                    if (empRprt.TotalTasks == 0)
                                        match = true;
                                    break;

                                case ERDFilterParams.WITH_CANCELLED_TASKS:
                                    if (empRprt.CancelledTasks > 0)
                                        match = true;
                                    break;
                                case ERDFilterParams.WITH_COMPLETED_TASK:
                                    if (empRprt.CompletedTasks > 0)
                                        match = true;
                                    break;

                                case ERDFilterParams.WITH_ONGOING_TASK:
                                    if (empRprt.OnGoingTasks > 0)
                                        match = true;
                                    break;

                                case ERDFilterParams.WITH_TERMINATED_TASKS:
                                    if (empRprt.TerminatedTasks > 0)
                                        match = true;
                                    break;
                            }
                        });
                    if (match)
                        filteredReport.Add(empRprt);
                });
            return filteredReport;
        }

        private int GetTaskByStateAndAsset(int assetId, TaskState state)
        {
            var assetTasks = Context.AssetTask.Where(at => at.AssetId == assetId).ToList();
            var tskCounter = 0;
            assetTasks.ForEach(
                tsk =>
                {
                    var task = Context.Task.Find(tsk.TaskId);
                    var wf = Context.Workflow.Find(task.Wfid);
                    if (wf.CurrentState == (int)state)
                        tskCounter += 1;
                });
            return tskCounter;
        }
    }
}
