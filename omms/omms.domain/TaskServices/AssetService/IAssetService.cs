﻿using omms.types.Models;
using System;
using System.Collections.Generic;

namespace omms.domain.TaskServices
{
    public interface IAssetService
    {
        AssetFormSchema GetAssetFormSchema();
        List<Schema> GetSchema(int assetType);
        types.Models.AssetReadingSchema GetAssetReadingSchema(int assetType);


        Guid RequestAssetRegistration(AssetRequest request, Guid? wfid);
        Guid ApproveAssetRegistration(Guid wfid);
        Guid RejectAssetRegistration(WFRequestModel wfid);


        AssetResponse GetAssetById(int id);
        AssetResponse GetAssetByLocation(string lid);
        int AddAssetReading(AssetReading reading);
        List<AssetReading> GetAssetReading(int assetId);

    }
}
