﻿using omms.domain.Infrastracture;
using omms.domain.TaskServices.Helper;
using omms.domain.TaskServices.SharedServices;
using omms.types.Models;
using omms.types.Models.Thread;
using omms.types.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;

namespace omms.domain.TaskServices.EmployeeServices
{
    public class EmployeeService : OmmsService, IEmployeeService
    {
        readonly SharedService sharedService;

        public EmployeeService()
        {
            sharedService = new SharedService();
        }

        public List<TaskResponse> GetEmployeeTask()
        {
            string empId = EmployeeHelper.GetEmployeeByLoginName(GetSession().Username).Id;
            var empTask = Context.EmployeeTask.Where(et => et.EmployeeId == empId).ToList();
            var tskList = new List<TaskResponse>();
            sharedService.SetContext(Context);

            empTask.ForEach(
                et => tskList.Add(sharedService.GetTaskById(et.TaskId)));
            return tskList;
        }

        public UserModel GetEmployee(string employeeId) => EmployeeHelper.GetEmployeeById(employeeId);


        public List<UserModel> GetTeamMembers(long teamId)
        {
            List<UserModel> members = new List<UserModel>();
            sharedService.SetContext(Context);
            var team = sharedService.GetTeam(teamId);
            team.Members.ForEach(
                mid =>
                {
                    var employee = GetEmployee(mid.Id);
                    members.Add(employee);
                });
            return members;
        }

        public List<ThreadModel> GetEmployeeThreads(Guid empId)
        {
            List<ThreadModel> threads = new List<ThreadModel>();
            //sharedService.SetContext(Context);
            //var teamList = sharedService.GetTeamList(empId);
            //teamList?.ForEach(
            //    team =>
            //    {
            //        var thread = GetThreadByTeamId(team.Id);
            //        if (thread != null)
            //            threads.Add(thread);
            //    });
            return threads;
        }

        public ThreadModel GetThreadByTeamId(Guid teamId)
        {
            //var thread = Context.TaskThread.FirstOrDefault(th => th.TeamId == teamId);
            //if (thread == null)
            //    return null;

            //var trd = Context.Thread.FirstOrDefault(t => t.Id == thread.ThreadId);

            //ThreadModel threadModel = new ThreadModel
            //{
            //    Id = thread.ThreadId,
            //    Name = trd.Name,
            //    Description = trd.Description,
            //    State = (ThreadState)trd.State,
            //    //  Team = sharedService.GetTeam(thread.TeamId),
            //    Task = sharedService.GetTaskById(thread.TaskId)
            //};
            //return threadModel;
            return new ThreadModel();
        }

    }
}
