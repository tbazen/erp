﻿using omms.types.Models;
using omms.types.Models.Thread;
using omms.types.Models.User;
using System;
using System.Collections.Generic;

namespace omms.domain.TaskServices.EmployeeServices
{
    public interface IEmployeeService
    {
        List<TaskResponse> GetEmployeeTask();
        List<UserModel> GetTeamMembers(long teamId);
        List<ThreadModel> GetEmployeeThreads(Guid empId);

    }
}
