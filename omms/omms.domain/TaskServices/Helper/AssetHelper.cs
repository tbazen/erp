﻿using omms.domain.Infrastracture;
using omms.types.Models;
using System;
using System.Linq;

namespace omms.domain.TaskServices.Helper
{

    class AssetTypes
    {
        public const string Pipeline = "Pipe";
        public const string Valve = "Valve";
        public const string Boreholes = "";

        public const string Endpoints = "EndPoints";
        public const string Reservior = "Reservior";
        public const string Hydrants = "Hydrant";

        public const string Reduce = "Reduce";
        public const string HouseConnections = "HouseConnection";
        public const string WaterMeter = "Water meter";
    }

    public class AssetHelper : OmmsService
    {

        public AssetType GetAdditionalAssetInformationByPUKID(AssetSearchParams searchParams)
        {
            switch (searchParams.AssetType)
            {
                case AssetTypes.Pipeline:
                    return GetPipeInformation(searchParams.PkuId);
                case AssetTypes.Valve:
                    return GetValveInformation(searchParams.PkuId);
                case AssetTypes.Boreholes:
                    return GetBoreholeInformation(searchParams.PkuId);



                case AssetTypes.Endpoints:
                    return GetEndPointsInformation(searchParams.PkuId);
                case AssetTypes.Reservior:
                    return GetReserviorInformation(searchParams.PkuId);
                case AssetTypes.Hydrants:
                    return GetHydrantsInformation(searchParams.PkuId);




                case AssetTypes.Reduce:
                    return GetReduceInformation(searchParams.PkuId);

                case AssetTypes.HouseConnections:
                    return GetHouseConnectionInformation(searchParams.PkuId);
                case AssetTypes.WaterMeter:
                    return GetWaterMeterInformation(searchParams.PkuId);

                default:
                    throw new Exception("Asset not found!");

            }


        }

        internal AssetType GetPipeInformation(long PUKID)
        {
            AssetType typeInfo = new AssetType
            {
                Id = 1,
                Name = "Pipeline"
            };
            var pipe = GisContext.Pipes.Where(p => p.Pkuid == PUKID).First();
            var properties = pipe.GetType().GetProperties().ToList();
            properties.ForEach(prop =>
            {
                var values = prop.GetValue(pipe);
                typeInfo.Attributes.Add(new Schema
                {
                    Id = pipe.Id,
                    AttributeName = prop.Name,
                    Priority = 5,
                    Value = values == null ? "Not Added" : values.ToString(),
                    ValueType = values?.GetType() == typeof(long) ||
                                values?.GetType() == typeof(int) ||
                                values?.GetType() == typeof(decimal) ||
                                values?.GetType() == typeof(double) ? "number" : "text"
                }); ;
            });

            return typeInfo;
        }
        internal AssetType GetValveInformation(long PUKID)
        {
            AssetType typeInfo = new AssetType
            {
                Id = 1,
                Name = "Pipeline"
            };
            var pipe = GisContext.Valves.Where(p => p.Pkuid == PUKID).First();
            var properties = pipe.GetType().GetProperties().ToList();
            properties.ForEach(prop =>
            {
                var values = prop.GetValue(pipe);
                typeInfo.Attributes.Add(new Schema
                {
                    Id = pipe.Id,
                    AttributeName = prop.Name,
                    Priority = 5,
                    Value = values == null ? "Not Added" : values.ToString(),
                    ValueType = values?.GetType() == typeof(long) ||
                                values?.GetType() == typeof(int) ||
                                values?.GetType() == typeof(decimal) ||
                                values?.GetType() == typeof(double) ? "number" : "text"
                }); ;
            });

            return typeInfo;
        }
        internal AssetType GetBoreholeInformation(long PUKID)
        {
            AssetType typeInfo = new AssetType
            {
                Id = 1,
                Name = "Pipeline"
            };
            var pipe = GisContext.Boreholes.Where(p => p.Pkuid == PUKID).First();
            var properties = pipe.GetType().GetProperties().ToList();
            properties.ForEach(prop =>
            {
                var values = prop.GetValue(pipe);
                typeInfo.Attributes.Add(new Schema
                {
                    Id = pipe.Id,
                    AttributeName = prop.Name,
                    Priority = 5,
                    Value = values == null ? "Not Added" : values.ToString(),
                    ValueType = values?.GetType() == typeof(long) ||
                                values?.GetType() == typeof(int) ||
                                values?.GetType() == typeof(decimal) ||
                                values?.GetType() == typeof(double) ? "number" : "text"
                }); ;
            });

            return typeInfo;
        }


        internal AssetType GetEndPointsInformation(long PUKID)
        {
            AssetType typeInfo = new AssetType
            {
                Id = 1,
                Name = "Pipeline"
            };
            var pipe = GisContext.Endpoint.Where(p => p.Pkuid == PUKID).First();
            var properties = pipe.GetType().GetProperties().ToList();
            properties.ForEach(prop =>
            {
                var values = prop.GetValue(pipe);
                typeInfo.Attributes.Add(new Schema
                {
                    Id = pipe.Id,
                    AttributeName = prop.Name,
                    Priority = 5,
                    Value = values == null ? "Not Added" : values.ToString(),
                    ValueType = values?.GetType() == typeof(long) ||
                                values?.GetType() == typeof(int) ||
                                values?.GetType() == typeof(decimal) ||
                                values?.GetType() == typeof(double) ? "number" : "text"
                }); ;
            });

            return typeInfo;
        }
        internal AssetType GetReserviorInformation(long PUKID)
        {
            AssetType typeInfo = new AssetType
            {
                Id = 1,
                Name = "Pipeline"
            };
            var pipe = GisContext.Reservoirs.Where(p => p.Pkuid == PUKID).First();
            var properties = pipe.GetType().GetProperties().ToList();
            properties.ForEach(prop =>
            {
                var values = prop.GetValue(pipe);
                typeInfo.Attributes.Add(new Schema
                {
                    Id = pipe.Id,
                    AttributeName = prop.Name,
                    Priority = 5,
                    Value = values == null ? "Not Added" : values.ToString(),
                    ValueType = values?.GetType() == typeof(long) ||
                                values?.GetType() == typeof(int) ||
                                values?.GetType() == typeof(decimal) ||
                                values?.GetType() == typeof(double) ? "number" : "text"
                }); ;
            });

            return typeInfo;
        }
        internal AssetType GetHydrantsInformation(long PUKID)
        {
            AssetType typeInfo = new AssetType
            {
                Id = 1,
                Name = "Pipeline"
            };
            var pipe = GisContext.Hydrants.Where(p => p.Pkuid == PUKID).First();
            var properties = pipe.GetType().GetProperties().ToList();
            properties.ForEach(prop =>
            {
                var values = prop.GetValue(pipe);
                typeInfo.Attributes.Add(new Schema
                {
                    Id = pipe.Id,
                    AttributeName = prop.Name,
                    Priority = 5,
                    Value = values == null ? "Not Added" : values.ToString(),
                    ValueType = values?.GetType() == typeof(long) ||
                                values?.GetType() == typeof(int) ||
                                values?.GetType() == typeof(decimal) ||
                                values?.GetType() == typeof(double) ? "number" : "text"
                }); ;
            });

            return typeInfo;

        }


        internal AssetType GetReduceInformation(long PUKID)
        {
            AssetType typeInfo = new AssetType
            {
                Id = 1,
                Name = "Pipeline"
            };
            var pipe = GisContext.Reduce.Where(p => p.Pkuid == PUKID).First();
            var properties = pipe.GetType().GetProperties().ToList();
            properties.ForEach(prop =>
            {
                var values = prop.GetValue(pipe);
                typeInfo.Attributes.Add(new Schema
                {
                    Id = pipe.Id,
                    AttributeName = prop.Name,
                    Priority = 5,
                    Value = values == null ? "Not Added" : values.ToString(),
                    ValueType = values?.GetType() == typeof(long) ||
                                values?.GetType() == typeof(int) ||
                                values?.GetType() == typeof(decimal) ||
                                values?.GetType() == typeof(double) ? "number" : "text"
                }); ;
            });

            return typeInfo;
        }
        internal AssetType GetHouseConnectionInformation(long PUKID)
        {
            AssetType typeInfo = new AssetType
            {
                Id = 1,
                Name = "Pipeline"
            };
            var pipe = GisContext.HouseConnections.Where(p => p.Pkuid == PUKID).First();
            var properties = pipe.GetType().GetProperties().ToList();
            properties.ForEach(prop =>
            {
                var values = prop.GetValue(pipe);
                typeInfo.Attributes.Add(new Schema
                {
                    Id = pipe.Id,
                    AttributeName = prop.Name,
                    Priority = 5,
                    Value = values == null ? "Not Added" : values.ToString(),
                    ValueType = values?.GetType() == typeof(long) ||
                                values?.GetType() == typeof(int) ||
                                values?.GetType() == typeof(decimal) ||
                                values?.GetType() == typeof(double) ? "number" : "text"
                }); ;
            });

            return typeInfo;

        }
        internal AssetType GetWaterMeterInformation(long PUKID)
        {
            AssetType typeInfo = new AssetType
            {
                Id = 1,
                Name = "Pipeline"
            };
            var pipe = GisContext.Watermeters.Where(p => p.Pkuid == PUKID).First();
            var properties = pipe.GetType().GetProperties().ToList();
            properties.ForEach(prop =>
            {
                var values = prop.GetValue(pipe);
                typeInfo.Attributes.Add(new Schema
                {
                    Id = pipe.Id,
                    AttributeName = prop.Name,
                    Priority = 5,
                    Value = values == null ? "Not Added" : values.ToString(),
                    ValueType = values?.GetType() == typeof(long) ||
                                values?.GetType() == typeof(int) ||
                                values?.GetType() == typeof(decimal) ||
                                values?.GetType() == typeof(double) ? "number" : "text"
                }); ;
            });
            return typeInfo;
        }
    }
}
