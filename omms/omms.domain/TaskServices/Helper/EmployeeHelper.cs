﻿using INTAPS.Payroll.BDE;
using omms.types.Models.User;
using System.Collections.Generic;
using System.Linq;

namespace omms.domain.TaskServices.Helper
{
    public static class EmployeeHelper
    {
        private static PayrollBDE payrollService;
        static EmployeeHelper() => payrollService = INTAPS.ClientServer.ApplicationServer.GetBDE<PayrollBDE>();
        public static UserModel GetEmployeeByLoginName(string name)
        {
            var employee = payrollService.GetEmployeeByLoginName(name);
            UserModel user = new UserModel
            {
                Id = employee.employeeID,
                UserName = employee.employeeName
            };
            return user;
        }

        public static List<UserModel> GetAllEmployees()
        {
            var payrollService = INTAPS.ClientServer.ApplicationServer.GetBDE<PayrollBDE>();
            var allEmployees = payrollService.GetEmployees(-1, true).ToList();

            List<UserModel> employees = new List<UserModel>();
            allEmployees
                .ForEach(
                emp =>
                {
                    employees.Add(new UserModel
                    {
                        Id = emp.employeeID,
                        UserName = emp.employeeName
                    });
                });
            return employees;
        }

        internal static UserModel GetEmployeeById(string employeeId)
        {
            var empl = payrollService.GetEmployeeByID(employeeId);
            UserModel employee = new UserModel
            {
                Id = empl.employeeID,
                UserName = empl.employeeName
            };
            return employee;
        }
    }
}
