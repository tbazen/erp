﻿using omms.domain.TaskServices.AdminServices;
using omms.domain.TaskServices.EmployeeServices;
using omms.domain.TaskServices.SharedServices;
using omms.domain.Workflow.Models;
using omms.types.Models;
using System;
using System.Collections.Generic;

namespace omms.domain.TaskServices
{
    public interface ITaskFacade : ITeamService, IEmployeeService, IAdminService, ISharedService
    {

        List<WorkFlowResponse> GetPendingItems(int type);
        void SetSession(UserSession session);

        AssetFormSchema GetAssetFormSchema();
        omms.types.Models.AssetReadingSchema GetReadingSchema(int assetType);
        Guid RequestAssetRegistration(AssetRequest asset, Guid? wfid);
        List<AssetResponse> GetPendingAssets();

        Guid ApproveAssetRegistration(Guid wfid);
        AssetResponse GetAssetById(int assetId);
        bool CancelRegistrationRequest(Guid wfid);
        Guid RejectAssetRegistration(WFRequestModel wfid);
        List<TaskResponse> MarkIndvTasksAsSeen();
        List<AssetResponse> GetRejectedAssets();
        List<TeamModel> GetPendingTeams();
        AssetResponse GetAssetByLocation(string lid);
        int AddAssetReading(AssetReading reading);
        List<AssetReading> GetAllAssetReadings(int assetId);
        Guid RequestTaskRegistration(TaskRequest task, Guid? wfid);
        List<TaskResponse> MarkTeamTasksAsSeen();
        List<TeamModel> GetRejectedTeams();
        new TeamModel GetTeamById(long teamId);
        List<TaskResponse> GetPendingTasks();
        Guid ApproveTaskRegistration(Guid wfid);
        Guid RejectTaskRegistration(WFRequestModel request);
        List<TaskResponse> GetRejectedTasks();

        NoteResponse AddNote(NoteRequest request);
        List<NoteResponse> GetTaskNotes(Guid taskId);



        //Task work flow

        Guid RequestTaskCancellation(WFRequestModel request);
        Guid RequestTaskTermination(WFRequestModel request);
        Guid RequestTaskCompletion(WFRequestModel request);
        Guid ApproveTaskState(Guid wfid);
        Guid RejectTaskState(WFRequestModel request);
        List<TaskResponse> GetTaskByState(int state);
        List<TaskResponse> GetTeamTasks();
        List<TaskResponse> GetIndividualTasks();
        List<TaskResponse> GettasksByTeamId(int teamId);

        MetaData GetMetaData();
        NoteResponse GetLatestTaskNote(Guid taskId);

        Tree<TaskResponse> GetTaskDependency(Guid taskId);
        AssetType GetAssetInfoByPUKID(AssetSearchParams searchParams);
        TaskMetaDataModel GetTaskMetaData();
        List<TeamReportModel> GetTeamReport();
        List<EmployeeReportModel> GetEmployeeReport();
        List<AssetReportModel> GetAssetReport();
        TaskReportViewModel GetTaskReport();
        byte[] GetEmployeeReportDocument(string docType, string filterParams);
        byte[] GetTaskReportDocument(string docType, string filterParams);
        byte[] GetTeamReportDocument(string docType, string filterParams);
        byte[] GetAssetReportDocument(string docType, string filterParams);
        Guid UpdateTaskAssignee(AssigneeViewModel assignee);
    }
}
