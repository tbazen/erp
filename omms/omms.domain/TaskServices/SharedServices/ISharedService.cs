﻿using omms.data.Entities;
using omms.types.Models;
using omms.types.Models.Thread;
using System;
using System.Collections.Generic;

namespace omms.domain.TaskServices.SharedServices
{
    public interface ISharedService
    {
        TaskResponse GetTaskById(Guid guid);
        List<TaskResponse> GetTaskByType(omms.types.Models.TaskType type);


        TeamModel GetTeam(long teamId);

        // status managemnt services
        TaskStatusModel UpdateTaskStatus(TaskStatusModel newStatus);
        List<TaskStatusModel> GetTaskStatusHistory(Guid taskId);
        TaskStatusModel GetStatusById(int statusId);
        TaskStatusModel ChangeTaskStatus(TaskStatusModel newStatus);


        //failure repor
        FailureModel ReportFailure(FailureModel failure);
        List<FailureModel> GetStatusFailureReport(int statusId);
        bool AddMaintenanceProcedure(MaintenanceProc procedure);
        List<MaintenanceProc> GetMaintenanceProcedures();

        //task depeendency
        // Tree<TaskResponse> GetTaskDependency(Guid taskId);

        TaskStatusModel GetLatestStatus(Guid taskId);

        //team
        ThreadModel GetThreadById(Guid threadId);
        Document GetDocumentById(Guid documentId);

        //assets
        List<AssetResponse> GetAllAssets();

    }
}
