﻿using omms.data.Entities;
using omms.domain.Documents;
using omms.domain.Infrastracture;
using omms.domain.TaskServices.Helper;
using omms.domain.Workflow.services;
using omms.types.Models;
using omms.types.Models.Thread;
using System;
using System.Collections.Generic;
using System.Linq;

namespace omms.domain.TaskServices.SharedServices
{
    public class SharedService : OmmsService, ISharedService
    {
        Tree<TaskResponse> dependencyTree;
        public SharedService()
        {
            dependencyTree = new Tree<TaskResponse>();
        }
        public TaskResponse GetTaskById(Guid taskId)
        {
            var task = Context.Task.FirstOrDefault(t => t.Id == taskId);
            if (task == null)
                return null;
            TaskResponse model = PopulateTaskData(task.Id);
            return model;
        }
        public TaskResponse PopulateTaskData(Guid taskId)
        {
            TaskResponse taskModel = new TaskResponse();
            var tsk = Context.Task.FirstOrDefault(t => t.Id == taskId);
            if (tsk == null)
                return null;

            #region Task Data
            taskModel.Id = tsk.Id;
            taskModel.Wfid = tsk.Wfid;
            taskModel.Name = tsk.Name;
            taskModel.Description = tsk.Description;
            taskModel.StartDateExpected = tsk.StartDateExpected;
            taskModel.StartDateActual = tsk.StartDateActual;
            taskModel.EndDateExpected = tsk.EndDateExpected;
            taskModel.EndDateActual = tsk.EndDateActual;
            taskModel.TaskType = (types.Models.TaskType)tsk.Type;
            taskModel.MaintenanceType = (types.Models.MaintenanceType)tsk.MaintenanceType;
            taskModel.CreatedBy = EmployeeHelper.GetEmployeeById(tsk.CreatedBy);
            taskModel.AssetId = Context.AssetTask.FirstOrDefault(asTsk => asTsk.TaskId == tsk.Id)?.AssetId;

            var wf = Context.Workflow.Find(taskModel.Wfid);

            TaskState state = (TaskState)wf?.CurrentState;
            switch (state)
            {
                case TaskState.CancelRejected:
                    taskModel.State = "Cancel Request Rejected";
                    break;

                case TaskState.Cancelled:
                    taskModel.State = "Cancelled";
                    break;


                case TaskState.CancelRequested:
                    taskModel.State = "Waiting cancel approval";
                    break;


                case TaskState.Completed:
                    taskModel.State = "Completed";
                    break;

                case TaskState.CompletedRejected:
                    taskModel.State = "Completed request rejected";
                    break;

                case TaskState.CompleteRequested:
                    taskModel.State = "Waiting Completion approval";
                    break;


                case TaskState.RegistrationApprived:
                    taskModel.State = "On going task";
                    break;


                case TaskState.Terminated:
                    taskModel.State = "Terminated";
                    break;
                case TaskState.TerminateRejected:
                    taskModel.State = "Termination request rejected";
                    break;
                case TaskState.TerminateRequsted:
                    taskModel.State = "Waiting Termination Approval";
                    break;

                case TaskState.AssigneeModificationRejected:
                    taskModel.State = "Task Assignee Modification Rejected";
                    break;
                case TaskState.AssigneeModificationRequested:
                    taskModel.State = "Waiting Assignee Modification Approval";
                    break;

            }

            switch (taskModel.TaskType)
            {
                case types.Models.TaskType.Periodic:
                    taskModel.IntervalDays = Context.PeriodicTask.FirstOrDefault(pt => pt.TaskId == taskModel.Id).Interval;
                    break;
                case types.Models.TaskType.Incident:
                    var incident = Context.IncidentTask.FirstOrDefault(it => it.TaskId == taskModel.Id);
                    var incidentImgs = Context.IncidentTaskImages.Where(it => it.TaskId == taskModel.Id).ToList();
                    IncidentResponse incidentInfo = new IncidentResponse
                    {
                        Incident = incident.Incident,
                        Description = incident.Description,
                        GeoLocation = incident.GeoLocation,
                        ReportedBy = incident.ReportedBy,
                    };
                    DocumentService documentService = new DocumentService();
                    documentService.SetContext(Context);
                    incidentImgs.ForEach(
                        img =>
                        {
                            var dRes = documentService.GetDocumentResponse(img.ImageId);
                            taskModel.IncidentInfo.Images.Add(dRes);
                        });

                    taskModel.IncidentInfo = incidentInfo;
                    break;
            }
            #endregion

            #region Task dependency
            //var dep = tsk.TaskDependencyDependUponNavigation.ToList();
            var dep = Context.TaskDependency.Where(td => td.Dependent == taskId).ToList();
            dep?.ForEach(
                d =>
                {
                    taskModel.DependUpon.Add(d.DependUpon);
                });
            #endregion

            #region TaskPriority
            var prty = tsk.TaskPriority.FirstOrDefault();
            if (prty != null)
                taskModel.Priority = (types.Models.PriorityType)prty.PriorityId;
            #endregion

            #region Getting Assigned type 

            //var asgn = tsk.TaskAssignee.FirstOrDefault();
            var asgn = Context.TaskAssignee.FirstOrDefault(tk => tk.TaskId == taskId);
            if (asgn != null)
                taskModel.TaskAssignee.AssigneeType = (AssigneeType)asgn.Asignee;
            #endregion

            #region Getting Assigned employees and Team

            switch (taskModel.TaskAssignee.AssigneeType)
            {
                case AssigneeType.None:
                    break;
                case AssigneeType.Employee:
                    //var empl = tsk.EmployeeTask;
                    var empl = Context.EmployeeTask.FirstOrDefault(emp => emp.TaskId == taskId);
                    if (empl != null)
                    {
                        AssigneeModel employee = new AssigneeModel
                        {
                            TaskId = empl.TaskId,
                            AssigneeId = empl.EmployeeId,
                            DateAssigned = empl.DateAssigned
                        };
                        taskModel.TaskAssignee.AssigneeModel = employee;
                    }
                    break;
                case AssigneeType.Team:
                    //var tm = tsk.TeamTask;
                    var tm = Context.TeamTask.FirstOrDefault(teamTsk => teamTsk.TaskId == taskId);
                    if (tm != null)
                    {

                        AssigneeModel team = new AssigneeModel
                        {
                            TaskId = tm.TaskId,
                            AssigneeId = tm.TeamId.ToString(),
                            DateAssigned = tm.DateAssigned
                        };
                        taskModel.TaskAssignee.AssigneeModel = team;
                    }
                    break;
            }

            #endregion

            #region Getting task warranty document


            //var docRef = tsk.TaskWarranty.FirstOrDefault();
            var docRef = Context.TaskWarranty.FirstOrDefault(tw => tw.TaskId == taskId);

            if (docRef != null)
            {
                DocumentService documentService = new DocumentService();
                documentService.SetContext(Context);
                taskModel.WarrantyDocument = documentService.GetDocumentResponse(docRef.WarrantyDocument);
            }
            #endregion

            return taskModel;
        }

        public TaskStatusModel GetLatestStatus(Guid taskId)
        {
            var state = Context.TaskStatus.OrderBy(s => s.StartDate).FirstOrDefault();
            return state == null
            ? null :
            GetStatusById(state.Id);
        }

        public TeamModel GetTeam(long teamId)
        {
            var team = Context.Team.FirstOrDefault(t => t.Id == teamId);
            if (team == null)
                return null;
            TeamModel model = new TeamModel
            {
                Id = team.Id,
                Name = team.Name,
                Description = team.Description,
                Wfid = team.Wfid
            };

            //getting team members
            var memb = Context.TeamMembers.Where(tMemb => tMemb.TeamId == model.Id).ToList();
            memb?.ForEach(m => model.Members.Add(EmployeeHelper.GetEmployeeById(m.MemberId)));
            //getting team tasks
            var tsks = Context.TeamTask.Where(tt => tt.TeamId == teamId).ToList();
            tsks?.ForEach(tskId => model.Tasks.Add(tskId.TaskId));
            return model;
        }
        #region services for status management
        public TaskStatusModel UpdateTaskStatus(TaskStatusModel newStatus)
        {
            var stat = Context.TaskStatus.FirstOrDefault(s => s.Id == newStatus.Id);
            if (stat == null)
                return null;
            stat.StatusId = (int)newStatus.Status;
            stat.TaskId = newStatus.TaskId;
            stat.StartDate = newStatus.StartDate;
            stat.EndDateExpected = newStatus.ExpectedEndDate;
            stat.EndDateActual = newStatus.ActualEndDate;
            stat.Description = newStatus.Description;
            stat.Progress = newStatus.Progress;
            stat.EmployeeId = newStatus.EmployeeId;
            Context.TaskStatus.Update(stat);
            Context.SaveChanges();

            return GetStatusById(stat.Id);
        }

        public List<TaskStatusModel> GetTaskStatusHistory(Guid taskId)
        {
            var task = Context.Task.FirstOrDefault(t => t.Id == taskId);
            if (task == null)
                return null;
            List<TaskStatusModel> statusList = new List<TaskStatusModel>();
            task.TaskStatus.ToList().ForEach(
                s =>
                {
                    statusList.Add(new TaskStatusModel
                    {
                        Id = s.Id,
                        TaskId = s.TaskId,
                        Status = (types.Models.TaskStatus)s.StatusId,
                        StartDate = s.StartDate,
                        ExpectedEndDate = s.EndDateExpected,
                        ActualEndDate = s.EndDateActual,
                        Description = s.Description,
                        EmployeeId = s.EmployeeId,
                        Progress = s.Progress
                    });
                });
            return statusList;
        }

        public TaskStatusModel GetStatusById(int statusId)
        {
            var stat = Context.TaskStatus.FirstOrDefault(s => s.Id == statusId);
            return stat == null ?
                null :
                new TaskStatusModel
                {
                    Id = stat.Id,
                    Status = (types.Models.TaskStatus)stat.StatusId,
                    TaskId = stat.TaskId,
                    StartDate = stat.StartDate,
                    ExpectedEndDate = stat.EndDateExpected,
                    ActualEndDate = stat.EndDateActual,
                    Description = stat.Description,
                    Progress = stat.Progress,
                    EmployeeId = stat.EmployeeId
                };
        }

        public TaskStatusModel ChangeTaskStatus(TaskStatusModel newStatus)
        {
            var task = Context.Task.FirstOrDefault(t => t.Id == newStatus.TaskId);
            if (task == null)
                return null;
            data.Entities.TaskStatus taskStatus = new data.Entities.TaskStatus
            {
                TaskId = newStatus.TaskId,
                StatusId = (int)newStatus.Status,
                EmployeeId = newStatus.EmployeeId,
                StartDate = newStatus.StartDate,
                EndDateExpected = newStatus.ExpectedEndDate,
                EndDateActual = newStatus.ActualEndDate,
                Description = newStatus.Description,
                Progress = newStatus.Progress

            };
            Context.TaskStatus.Add(taskStatus);
            Context.SaveChanges();
            //
            var id = Context.Entry(taskStatus).GetDatabaseValues().GetValue<int>("Id");
            return GetStatusById(id);
        }

        #endregion

        #region Failure report
        public FailureModel ReportFailure(FailureModel failure)
        {
            Failure flr = new Failure
            {
                Id = Guid.NewGuid(),
                Name = failure.Name,
                Description = failure.FailureDescription
            };
            Context.Failure.Add(flr);
            Context.SaveChanges();

            StatusMaintenanceProc maintenanceProc = new StatusMaintenanceProc
            {
                Id = Guid.NewGuid(),
                ProcedureId = failure.MaintenanceProc.Id,
                Failure = flr.Id,
                DateOccured = failure.DateOccured
            };
            Context.StatusMaintenanceProc.Add(maintenanceProc);
            Context.SaveChanges();

            return GetMaintenanceProcedure(maintenanceProc.Id);
        }

        private FailureModel GetMaintenanceProcedure(Guid maintenanceProcId)
        {
            var maint = Context.StatusMaintenanceProc.FirstOrDefault(mt => mt.Id == maintenanceProcId);
            if (maint == null)
                return null;
            DocumentService service = new DocumentService();
            service.SetContext(Context);

            FailureModel failure = new FailureModel
            {
                StatusId = maint.StatusId,
                FailureId = maint.Failure,
                DateOccured = maint.DateOccured,
                MaintenanceProc = new MaintenanceProc
                {
                    Id = maint.Procedure.Id,
                    Name = maint.Procedure.Name,
                    Description = maint.Procedure.Description,
                    ProcedureDocument = maint.Procedure.Document == null ? null : service.GetDocumentResponse((Guid)maint.Procedure.Document)
                }
            };
            var flr = Context.Failure.FirstOrDefault(f => f.Id == failure.FailureId);

            failure.Name = flr?.Name;
            failure.FailureDescription = flr?.Description;

            return failure;

        }
        public List<FailureModel> GetStatusFailureReport(int statusId)
        {
            var flrList = Context.StatusMaintenanceProc.Where(smp => smp.StatusId == statusId).ToList();
            if (flrList == null)
                return null;
            List<FailureModel> failures = new List<FailureModel>();
            flrList.ForEach(
                f =>
                {
                    failures.Add(GetMaintenanceProcedure(f.Id));
                });
            return failures;
        }

        public bool AddMaintenanceProcedure(MaintenanceProc proc)
        {
            DocumentService service = new DocumentService();
            service.SetContext(Context);
            MaintenanceProcedure procedure = new MaintenanceProcedure
            {
                Name = proc.Name,
                Description = proc.Description,
                Document = service.CreateDocument(proc.ProcedureDocumentReq).Id
            };
            Context.MaintenanceProcedure.Add(procedure);
            Context.SaveChanges();
            return true;
        }

        public List<MaintenanceProc> GetMaintenanceProcedures()
        {
            var maintProc = Context.MaintenanceProcedure.ToList();
            if (maintProc == null)
                return null;
            List<MaintenanceProc> maintenances = new List<MaintenanceProc>();

            DocumentService document = new DocumentService();
            document.SetContext(Context);
            maintProc?.ForEach(
                m =>
                {
                    maintenances.Add(new MaintenanceProc
                    {
                        Id = m.Id,
                        Name = m.Name,
                        Description = m.Description,
                        ProcedureDocument = m.Document == null ? null : document.GetDocumentResponse((Guid)m.Document)
                    });
                });
            return maintenances;
        }

        #endregion


        public ThreadModel GetThreadById(Guid threadId)
        {
            //var thread = Context.TaskThread.FirstOrDefault(th => th.ThreadId == threadId);
            //if (thread == null)
            //    return null;
            //ThreadModel threadModel = new ThreadModel
            //{
            //    Id = thread.ThreadId,
            //    Name = thread.Thread.Name,
            //    Description = thread.Thread.Description,
            //    State = (ThreadState)thread.Thread.State,
            //    //Team = GetTeam(thread.TeamId),
            //    Task = GetTaskById(thread.TaskId)
            //};
            return new ThreadModel();
        }

        public Document GetDocumentById(Guid documentId)
        {
            var doc = Context.Document.FirstOrDefault(d => d.Id == documentId);
            return doc;
        }

        public List<AssetResponse> GetAllAssets()
        {
            var assets = Context.Asset.ToList();
            List<AssetResponse> assetModels = new List<AssetResponse>();
            assets.ForEach(
                asset =>
                assetModels.Add(GetAssetById(asset.Id)));

            return assetModels;
        }

        public AssetResponse GetAssetById(int assetId)
        {
            var asset = Context.Asset.FirstOrDefault(a => a.Id == assetId);
            if (asset == null)
                return null;

            DocumentService documentService = new DocumentService();
            documentService.SetContext(Context);

            AssetResponse response = new AssetResponse
            {
                Id = asset.Id,
                Name = asset.Name,
                Description = asset.Description,
                GeoLocation = asset.GeoLocation,
                Wfid = null
            };
            var imgs = Context.AssetImages.Where(ai => ai.AssetId == asset.Id).ToList();
            var type = Context.AssetType.Find(asset.TypeId);
            response.AssetType = new types.Models.AssetType
            {
                Id = type.Id,
                Name = type.Name
            };
            imgs?.ForEach(
                img =>
                {
                    var doc = documentService.GetDocumentResponse(img.DocumentId);
                    response.Images.Add(doc);
                });
            return response;
        }

        public List<TaskResponse> GetTaskByType(types.Models.TaskType type)
        {
            var tsks = Context.Task.Where(t => t.Type == (int)type).ToList();
            if (tsks == null)
                return null;
            List<TaskResponse> tasks = new List<TaskResponse>();
            tsks.ForEach(t => tasks.Add(GetTaskById(t.Id)));
            return tasks;
        }
    }
}
