﻿using omms.data.Entities;
using omms.data.GisEntities;
using omms.domain.Infrastracture;
using omms.domain.TaskServices.AdminServices;
using omms.domain.TaskServices.EmployeeServices;
using omms.domain.TaskServices.SharedServices;
using omms.domain.Workflow.Models;
using omms.domain.Workflow.services;
using omms.types.Models;
using omms.types.Models.Thread;
using omms.types.Models.User;
using System;
using System.Collections.Generic;

namespace omms.domain.TaskServices
{
    public class TaskFacade : OmmsFacade, ITaskFacade
    {
        private readonly OmmsContext _context;
        private readonly OmmsGisContext _gisContext;
        readonly AdminService _admin;
        readonly EmployeeService _employee;
        readonly SharedService _shared;

        readonly WorkflowService _workflow;

        readonly TeamService _team;
        readonly AssetService _asset;
        readonly TaskService _task;

        UserSession _session;
        public TaskFacade(OmmsContext context, OmmsGisContext gisContext, IWorkflowService wfService)
        {
            _context = context;
            _gisContext = gisContext;

            _admin = new AdminService();
            _employee = new EmployeeService();
            _shared = new SharedService();
            _team = new TeamService();
            _asset = new AssetService();
            _workflow = new WorkflowService();
            _task = new TaskService();
        }

        public void SetSession(UserSession session)
        {
            _session = session;
            _admin.SetSession(_session);
            _team.SetSession(_session);
            _shared.SetSession(_session);
            _employee.SetSession(_session);
            _workflow.SetSession(_session);
            _asset.SetSession(_session);
            _task.SetSession(_session);
        }

        #region Admin services facade
        //task CRUD
        public TaskResponse CreateTask(TaskRequest model)
        {
            PassContext(_admin, _context);
            return _admin.CreateTask(model);
        }
        public TaskResponse AssignTask(Assignee assignee)
        {
            PassContext(_admin, _context);
            return _admin.AssignTask(assignee);
        }

        public List<UserModel> GetAllEmployees()
        {
            PassContext(_admin, _context);
            return _admin.GetAllEmployees();
        }
        public TaskResponse UpdateTask(TaskRequest newTask)
        {
            PassContext(_admin, _context);
            return _admin.UpdateTask(newTask);
        }
        public bool DeleteTask(Guid taskId)
        {
            PassContext(_admin, _context);
            return _admin.DeleteTask(taskId);
        }


        public TaskResponse SearchTaskById(Guid taskId)
        {
            PassContext(_admin, _context);
            return _admin.SearchTaskById(taskId);
        }
        public List<TaskResponse> GetAllTasks()
        {
            PassContext(_admin, _context);
            return _admin.GetAllTasks();
        }

        //team management
        public TeamModel CreateTeam(TeamModel team)
        {
            PassContext(_admin, _context);
            return _admin.CreateTeam(team);
        }

        public bool DeleteTeam(long teamId)
        {
            PassContext(_admin, _context);
            return _admin.DeleteTeam(teamId);
        }
        public bool RemoveTeamMember(long teamId, string memberId)
        {
            PassContext(_admin, _context);
            return _admin.RemoveTeamMember(teamId, memberId);
        }


        //retriving tasks

        public List<TaskResponse> GetTaskSortedByDate()
        {
            PassContext(_admin, _context);
            return _admin.GetTaskSortedByDate();
        }


        //approval


        //notificatio 
        public List<NotificationModel> GetNotification()
        {
            PassContext(_admin, _context);
            return _admin.GetNotification();
        }
        public bool SetNotification(NotificationModel notification)
        {
            PassContext(_admin, _context);
            return _admin.SetNotification(notification);
        }

        public List<NotificationModel> GetNewNotifications()
        {
            PassContext(_admin, _context);
            return _admin.GetNewNotifications();
        }


        #endregion

        #region Employee task facade
        public List<TaskResponse> GetEmployeeTask()
        {
            PassContext(_employee, _context);
            return _employee.GetEmployeeTask();
        }

        public List<UserModel> GetTeamMembers(long teamId)
        {
            PassContext(_employee, _context);
            return _employee.GetTeamMembers(teamId);
        }
        public List<ThreadModel> GetEmployeeThreads(Guid empId)
        {
            PassContext(_employee, _context);
            return _employee.GetEmployeeThreads(empId);
        }

        #endregion

        #region Team Services facade


        public List<TaskResponse> GetTeamTask(long teamId)
        {
            PassContext(_team, _context);
            return _team.GetTeamTask(teamId);
        }

        public MessageResponse SendMessage(MessageRequest message)
        {
            PassContext(_team, _context);
            return _team.SendMessage(message);
        }

        public List<MessageResponse> GetMessages(Guid threadId)
        {
            PassContext(_team, _context);
            return _team.GetMessages(threadId);
        }

        #endregion

        #region SharedServices Facade



        public TaskResponse GetTaskById(Guid guid)
        {
            PassContext(_shared, _context);
            return _shared.GetTaskById(guid);
        }

        public TeamModel GetTeam(long teamId)
        {
            PassContext(_shared, _context);
            return _shared.GetTeam(teamId);
        }
        public TaskStatusModel UpdateTaskStatus(TaskStatusModel newStatus)
        {
            PassContext(_shared, _context);
            return _shared.UpdateTaskStatus(newStatus);
        }

        public List<TaskStatusModel> GetTaskStatusHistory(Guid taskId)
        {
            PassContext(_shared, _context);
            return _shared.GetTaskStatusHistory(taskId);
        }
        public TaskStatusModel GetStatusById(int statusId)
        {
            PassContext(_shared, _context);
            return _shared.GetStatusById(statusId);
        }

        public TaskStatusModel ChangeTaskStatus(TaskStatusModel newStatus)
        {
            PassContext(_shared, _context);
            return _shared.ChangeTaskStatus(newStatus);
        }

        //failure
        public FailureModel ReportFailure(FailureModel failure)
        {
            PassContext(_shared, _context);
            return _shared.ReportFailure(failure);
        }

        public List<FailureModel> GetStatusFailureReport(int statusId)
        {
            PassContext(_shared, _context);
            return _shared.GetStatusFailureReport(statusId);
        }

        public bool AddMaintenanceProcedure(MaintenanceProc procedure)
        {
            PassContext(_shared, _context);
            return _shared.AddMaintenanceProcedure(procedure);
        }

        public List<MaintenanceProc> GetMaintenanceProcedures()
        {
            PassContext(_shared, _context);
            return _shared.GetMaintenanceProcedures();
        }



        public TaskStatusModel GetLatestStatus(Guid taskId)
        {
            PassContext(_shared, _context);
            return _shared.GetLatestStatus(taskId);
        }


        public List<TeamModel> GetTeamList(string memberId)
        {
            PassContext(_team, _context);
            return _team.GetTeamList(memberId);
        }


        public ThreadModel GetThreadById(Guid threadId)
        {
            PassContext(_shared, _context);
            return _shared.GetThreadById(threadId);
        }


        public Document GetDocumentById(Guid documentId)
        {
            PassContext(_shared, _context);
            return _shared.GetDocumentById(documentId);
        }


        public List<AssetResponse> GetAllAssets()
        {
            PassContext(_shared, _context);
            return _shared.GetAllAssets();
        }
        #endregion


        public Tree<TaskResponse> GetTaskDependency(Guid taskId)
        {
            PassContext(_task, _context);
            return _task.GetTaskDependency(taskId);
        }

        public List<TaskResponse> GetTaskByType(types.Models.TaskType type)
        {
            PassContext(_shared, _context);
            return _shared.GetTaskByType(type);
        }

        #region Team WorkFlow
        public Guid RequestTeamRegistration(TeamModel team, Guid? wfid)
        {
            PassContext(_team, _context);
            return _team.RequestTeamRegistration(team, wfid);
        }
        public Guid ApproveTeamRegistration(Guid wfid)
        {
            PassContext(_team, _context);
            return _team.ApproveTeamRegistration(wfid);
        }
        public Guid RejectTeamRegistration(WFRequestModel request)
        {
            PassContext(_team, _context);
            return _team.RejectTeamRegistration(request);
        }

        public List<WorkFlowResponse> GetPendingItems(int type)
        {
            PassContext(_workflow, _context);
            return _workflow.GetPendingItems(type);
        }

        #endregion

        #region Asset Workflow
        public AssetFormSchema GetAssetFormSchema()
        {
            PassContext(_asset, _context);
            return _asset.GetAssetFormSchema();
        }

        public omms.types.Models.AssetReadingSchema GetReadingSchema(int assetType)
        {
            PassContext(_asset, _context);
            return _asset.GetAssetReadingSchema(assetType);
        }

        public Guid RequestAssetRegistration(AssetRequest asset, Guid? wfid)
        {
            PassContext(_asset, _context);
            return _asset.RequestAssetRegistration(asset, wfid);
        }

        public List<AssetResponse> GetPendingAssets()
        {
            PassContext(_asset, _context);
            PassGisContext(_asset, _gisContext);
            return _asset.GetPendingAssets();
        }

        public Guid ApproveAssetRegistration(Guid wfid)
        {
            PassContext(_asset, _context);
            return _asset.ApproveAssetRegistration(wfid);
        }


        public AssetResponse GetAssetById(int id)
        {
            PassContext(_asset, _context);
            PassGisContext(_asset, _gisContext);
            return _asset.GetAssetById(id);
        }

        public AssetResponse UpdateAsset(AssetRequest asset)
        {
            PassContext(_asset, _context);
            return _asset.UpdateAsset(asset);
        }

        public bool DeleteAsset(int assetId)
        {
            PassContext(_asset, _context);
            return _asset.DeleteAsset(assetId);
        }

        public bool CancelRegistrationRequest(Guid wfid)
        {
            PassContext(_team, _context);
            if (_team.IsTeamModificationCancelled(wfid))
                return true;
            PassContext(_workflow, _context);
            return _workflow.CancelRegistrationRequest(wfid);
        }

        public Guid RejectAssetRegistration(WFRequestModel request)
        {
            PassContext(_asset, _context);
            return _asset.RejectAssetRegistration(request);
        }

        public List<AssetResponse> GetRejectedAssets()
        {

            PassContext(_asset, _context);
            PassGisContext(_asset, _gisContext);
            return _asset.GetRejectedAssets();
        }

        public List<TaskResponse> GetAssetTasks(int assetId)
        {
            PassContext(_admin, _context);
            return _admin.GetAssetTasks(assetId);
        }

        public AssetResponse GetAssetByLocation(string lid)
        {
            PassContext(_asset, _context);
            return _asset.GetAssetByLocation(lid);
        }

        public int AddAssetReading(types.Models.AssetReading reading)
        {
            PassContext(_asset, _context);
            return _asset.AddAssetReading(reading);
        }

        public List<types.Models.AssetReading> GetAllAssetReadings(int assetId)
        {
            PassContext(_asset, _context);
            return _asset.GetAssetReading(assetId);
        }

        public List<TeamModel> GetPendingTeams()
        {
            PassContext(_team, _context);
            return _team.GetPendingTeams();
        }


        public List<TeamModel> GetRejectedTeams()
        {
            PassContext(_team, _context);
            return _team.GetRejectedTeams();
        }


        public TeamModel GetTeamById(long teamId)
        {
            PassContext(_team, _context);
            return _team.GetTeamById(teamId);
        }

        #endregion

        #region Task workflow

        public Guid RequestTaskRegistration(TaskRequest task, Guid? wfid)
        {
            PassContext(_task, _context);
            return _task.RequestTaskRegistration(task, wfid);
        }

        public List<TaskResponse> GetPendingTasks()
        {
            PassContext(_task, _context);
            return _task.GetPengingTasks();
        }

        public Guid ApproveTaskRegistration(Guid wfid)
        {
            PassContext(_task, _context);
            return _task.ApproveTaskRegistration(wfid);
        }

        public Guid RejectTaskRegistration(WFRequestModel request)
        {
            PassContext(_task, _context);
            return _task.RejectTaskRegistration(request);
        }

        public List<TaskResponse> GetRejectedTasks()
        {
            PassContext(_task, _context);
            return _task.GetRejectedTasks();
        }
        #endregion


        public NoteResponse AddNote(NoteRequest request)
        {
            PassContext(_task, _context);
            return _task.AddNote(request);
        }

        public List<NoteResponse> GetTaskNotes(Guid taskId)
        {
            PassContext(_task, _context);
            return _task.GetTaskNotes(taskId);
        }



        #region Task status Workflow 

        public Guid RequestTaskCancellation(WFRequestModel request)
        {
            PassContext(_task, _context);
            return _task.RequestTaskCancellation(request);
        }

        public Guid RequestTaskTermination(WFRequestModel request)
        {
            PassContext(_task, _context);
            return _task.RequestTaskTermination(request);
        }

        public Guid RequestTaskCompletion(WFRequestModel request)
        {
            PassContext(_task, _context);
            return _task.RequestTaskCompletion(request);
        }

        public Guid ApproveTaskState(Guid wfid)
        {
            PassContext(_task, _context);
            return _task.ApproveTaskState(wfid);
        }

        public Guid RejectTaskState(WFRequestModel request)
        {
            PassContext(_task, _context);
            return _task.RejectTaskState(request);
        }

        public List<TaskResponse> GetTaskByState(int state)
        {
            PassContext(_task, _context);
            return _task.GetTasksByState(state);
        }

        #endregion

        public List<TaskResponse> GetTeamTasks()
        {
            PassContext(_task, _context);
            return _task.GetTeamTasks();
        }

        public List<TaskResponse> GetIndividualTasks()
        {
            PassContext(_task, _context);
            return _task.GetIndividualTasks();
        }

        public List<TaskResponse> GettasksByTeamId(int teamId)
        {
            PassContext(_task, _context);
            return _task.GetTasksByTeamId(teamId);
        }

        public MetaData GetMetaData()
        {
            PassContext(_workflow, _context);
            PassContext(_task, _context);
            MetaData meta = new MetaData
            {
                PendingItems = _workflow.NumberOfPendingItems(),
                RejectedItems = _workflow.NumberOfRejectedItems(),
                NewIndividualTasks = _task.NumberOfNewIndeviduialTasks(),
                NewTeamTasks = _task.NumberOfNewTeamTasks()
            };
            return meta;
        }

        public NoteResponse GetLatestTaskNote(Guid taskId)
        {
            PassContext(_task, _context);
            return _task.GetLatestTaskNote(taskId);
        }

        public types.Models.AssetType GetAssetInfoByPUKID(AssetSearchParams searchParams)
        {
            PassGisContext(_asset, _gisContext);
            PassContext(_asset, _context);
            return _asset.GetAssetInfoByPUKID(searchParams);
        }

        public List<TaskResponse> MarkIndvTasksAsSeen()
        {
            PassContext(_task, _context);
            return _task.MarkIndvTasksAsSeen();
        }

        public List<TaskResponse> MarkTeamTasksAsSeen()
        {
            PassContext(_task, _context);
            return _task.MarkTeamTasksAsSeen();
        }

        public TaskMetaDataModel GetTaskMetaData()
        {
            PassContext(_task, _context);
            return _task.GetTaskMetaData();
        }

        public List<TeamReportModel> GetTeamReport()
        {
            PassContext(_team, _context);
            return _team.GetTeamReport();
        }

        public List<EmployeeReportModel> GetEmployeeReport()
        {
            PassContext(_task, _context);
            return _task.GetEmployeeReport();
        }

        public List<AssetReportModel> GetAssetReport()
        {
            PassContext(_asset, _context);
            return _asset.GetAssetReport();
        }

        public TaskReportViewModel GetTaskReport()
        {
            PassContext(_task, _context);
            return _task.GetTaskReport();
        }

        public byte[] GetEmployeeReportDocument(string docType, string filterParams)
        {
            PassContext(_task, _context);
            return _task.GetEmployeeReportDocument(docType, filterParams);
        }

        public byte[] GetTaskReportDocument(string docType, string filterParams)
        {
            PassContext(_task, _context);
            return _task.GetTaskReportDocument(docType, filterParams);
        }

        public byte[] GetTeamReportDocument(string docType, string filterParams)
        {
            PassContext(_team, _context);
            return _team.GetTeamReportDocument(docType, filterParams);
        }

        public byte[] GetAssetReportDocument(string docType, string filterParams)
        {
            PassContext(_asset, _context);
            return _asset.GetAssetReportDocument(docType, filterParams);
        }

        public Guid UpdateTaskAssignee(AssigneeViewModel assignee)
        {
            PassContext(_task, _context);
            return _task.UpdateTaskAssignee(assignee);
        }
    }
}
