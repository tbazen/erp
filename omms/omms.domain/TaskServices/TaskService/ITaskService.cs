﻿using omms.types.Models;
using System;
using System.Collections.Generic;

namespace omms.domain.TaskServices
{
    public interface ITaskService
    {
        Guid RequestTaskRegistration(TaskRequest request, Guid? wfid);
        Guid ApproveTaskRegistration(Guid wfid);
        Guid RejectTaskRegistration(WFRequestModel request);

        TaskResponse GetTaskById(Guid tastId);

        List<TaskResponse> GetPengingTasks();
        List<TaskResponse> GetRejectedTasks();





        Guid RequestTaskCancellation(WFRequestModel request);
        Guid RequestTaskTermination(WFRequestModel request);
        Guid RequestTaskCompletion(WFRequestModel request);
        Guid ApproveTaskState(Guid wfid);
        Guid RejectTaskState(WFRequestModel request);


        List<TaskResponse> GetTasksByState(int state);



        NoteResponse AddNote(NoteRequest request);
        List<NoteResponse> GetTaskNotes(Guid taskId);

    }
}
