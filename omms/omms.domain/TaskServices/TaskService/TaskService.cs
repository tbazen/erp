﻿using OfficeOpenXml;
using omms.data.Entities;
using omms.domain.Documents;
using omms.domain.Infrastracture;
using omms.domain.TaskServices.Helper;
using omms.domain.Workflow.services;
using omms.types.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace omms.domain.TaskServices
{
    public class TaskService : OmmsService, ITaskService
    {
        readonly TaskWorkflow _taskWorkFlow;
        readonly DocumentService _documentService;
        readonly WorkflowService _workflowService;

        Tree<TaskResponse> dependencyTree;

        public TaskService()
        {
            _taskWorkFlow = new TaskWorkflow(this);
            _workflowService = new WorkflowService();
            _documentService = new DocumentService();

            dependencyTree = new Tree<TaskResponse>();
        }

        public Guid ApproveTaskRegistration(Guid wfid)
        {
            _taskWorkFlow.SetContext(Context);
            _taskWorkFlow.SetSession(GetSession());
            return _taskWorkFlow.ApproverTaskRegistration(wfid);
        }

        public TaskResponse GetTaskById(Guid taskId)
        {
            //_sharedService.SetContext(Context);
            //return _sharedService.GetTaskById(taskId);
            var task = Context.Task.FirstOrDefault(t => t.Id == taskId);
            if (task == null)
                return null;
            TaskResponse model = PopulateTaskData(task);
            return model;
        }




        public TaskResponse PopulateTaskData(Task tsk)
        {
            TaskResponse taskModel = new TaskResponse();

            #region Task Data
            taskModel.Id = tsk.Id;
            taskModel.Wfid = tsk.Wfid;
            taskModel.Name = tsk.Name;
            taskModel.Description = tsk.Description;
            taskModel.StartDateExpected = tsk.StartDateExpected;
            taskModel.StartDateActual = tsk.StartDateActual;
            taskModel.EndDateExpected = tsk.EndDateExpected;
            taskModel.EndDateActual = tsk.EndDateActual;
            taskModel.TaskType = (types.Models.TaskType)tsk.Type;
            taskModel.MaintenanceType = (types.Models.MaintenanceType)tsk.MaintenanceType;
            taskModel.CreatedBy = EmployeeHelper.GetEmployeeById(tsk.CreatedBy);
            taskModel.AssetId = Context.AssetTask.FirstOrDefault(asTsk => asTsk.TaskId == tsk.Id)?.AssetId;


            var wf = Context.Workflow.Find(taskModel.Wfid);
            TaskState state = (TaskState)wf?.CurrentState;

            _workflowService.SetContext(Context);
            if (state != TaskState.RegistrationApprived ||
                GetSession()?.Role == (int)types.Models.User.RoleType.TechnicalOfficer)
                taskModel.Note = _workflowService.GetLatestWorkItemObject((Guid)taskModel.Wfid)?.Note ?? "";


            switch (state)
            {
                case TaskState.CancelRejected:
                    taskModel.State = "Cancel Request Rejected";
                    break;

                case TaskState.Cancelled:
                    taskModel.State = "Cancelled";
                    break;


                case TaskState.CancelRequested:
                    taskModel.State = "Waiting cancel approval";
                    break;


                case TaskState.Completed:
                    taskModel.State = "Completed";
                    break;

                case TaskState.CompletedRejected:
                    taskModel.State = "Completed request rejected";
                    break;

                case TaskState.CompleteRequested:
                    taskModel.State = "Waiting Completion approval";
                    break;


                case TaskState.RegistrationApprived:
                    taskModel.State = "On going task";
                    break;


                case TaskState.Terminated:
                    taskModel.State = "Terminated";
                    break;
                case TaskState.TerminateRejected:
                    taskModel.State = "Termination request rejected";
                    break;
                case TaskState.TerminateRequsted:
                    taskModel.State = "Waiting Termination Approval";
                    break;

                case TaskState.AssigneeModificationRejected:
                    taskModel.State = "Task Assignee Modification Rejected";
                    break;
                case TaskState.AssigneeModificationRequested:
                    taskModel.State = "Waiting Assignee Modification Approval";
                    break;

            }

            switch (taskModel.TaskType)
            {
                case types.Models.TaskType.Periodic:
                    taskModel.IntervalDays = Context.PeriodicTask.FirstOrDefault(pt => pt.TaskId == taskModel.Id).Interval;
                    break;
                case types.Models.TaskType.Incident:
                    var incident = Context.IncidentTask.FirstOrDefault(it => it.TaskId == taskModel.Id);
                    var incidentImgs = Context.IncidentTaskImages.Where(it => it.TaskId == taskModel.Id).ToList();
                    IncidentResponse incidentInfo = new IncidentResponse
                    {
                        Incident = incident.Incident,
                        Description = incident.Description,
                        GeoLocation = incident.GeoLocation,
                        ReportedBy = incident.ReportedBy,
                    };
                    DocumentService documentService = new DocumentService();
                    documentService.SetContext(Context);
                    incidentImgs.ForEach(
                        img =>
                        {
                            var dRes = documentService.GetDocumentResponse(img.ImageId);
                            taskModel.IncidentInfo.Images.Add(dRes);
                        });

                    taskModel.IncidentInfo = incidentInfo;
                    break;
            }
            #endregion

            #region Task dependency
            //var dep = tsk.TaskDependencyDependUponNavigation.ToList();
            var dep = Context.TaskDependency.Where(td => td.Dependent == tsk.Id).ToList();
            dep?.ForEach(
                d =>
                {
                    taskModel.DependUpon.Add(d.DependUpon);
                });
            #endregion

            #region TaskPriority
            var prrty = Context.TaskPriority.FirstOrDefault(tpr => tpr.TaskId == tsk.Id);
            if (prrty != null)
                taskModel.Priority = (types.Models.PriorityType)prrty.PriorityId;
            #endregion

            #region Getting Assigned type 

            //var asgn = tsk.TaskAssignee.FirstOrDefault();
            var asgn = Context.TaskAssignee.FirstOrDefault(tk => tk.TaskId == tsk.Id);
            if (asgn != null)
                taskModel.TaskAssignee.AssigneeType = (AssigneeType)asgn.Asignee;
            #endregion

            #region Getting Assigned employees and Team

            switch (taskModel.TaskAssignee.AssigneeType)
            {
                case AssigneeType.None:
                    break;
                case AssigneeType.Employee:
                    //var empl = tsk.EmployeeTask;
                    var empl = Context.EmployeeTask.FirstOrDefault(emp => emp.TaskId == tsk.Id);
                    if (empl != null)
                    {
                        AssigneeModel employee = new AssigneeModel
                        {
                            TaskId = empl.TaskId,
                            AssigneeId = empl.EmployeeId,
                            DateAssigned = empl.DateAssigned
                        };
                        taskModel.TaskAssignee.AssigneeModel = employee;
                    }
                    break;
                case AssigneeType.Team:
                    //var tm = tsk.TeamTask;
                    var tm = Context.TeamTask.FirstOrDefault(teamTsk => teamTsk.TaskId == tsk.Id);
                    if (tm != null)
                    {

                        AssigneeModel team = new AssigneeModel
                        {
                            TaskId = tm.TaskId,
                            AssigneeId = tm.TeamId.ToString(),
                            DateAssigned = tm.DateAssigned
                        };
                        taskModel.TaskAssignee.AssigneeModel = team;
                    }
                    break;
            }

            #endregion

            #region Getting task warranty document


            //var docRef = tsk.TaskWarranty.FirstOrDefault();
            var docRef = Context.TaskWarranty.FirstOrDefault(tw => tw.TaskId == tsk.Id);

            if (docRef != null)
            {
                DocumentService documentService = new DocumentService();
                documentService.SetContext(Context);
                taskModel.WarrantyDocument = documentService.GetDocumentResponse(docRef.WarrantyDocument);
            }
            #endregion

            return taskModel;
        }



        public Guid RejectTaskRegistration(WFRequestModel request)
        {
            _taskWorkFlow.SetContext(Context);
            _taskWorkFlow.SetSession(GetSession());
            return _taskWorkFlow.RejectTaskRegistration(request);
        }

        public Guid RequestTaskRegistration(TaskRequest request, Guid? wfid)
        {
            _taskWorkFlow.SetContext(Context);
            _taskWorkFlow.SetSession(GetSession());
            _documentService.SetContext(Context);

            if (request.WarrantyDocument != null)
            {
                var doc = _documentService.CreateDocument(request.WarrantyDocument);
                request.WarrantyDocument.Id = doc.Id;
                request.WarrantyDocument.Filename = null;
                request.WarrantyDocument.Mimetype = null;
                request.WarrantyDocument.Note = null;
                request.WarrantyDocument.OverrideFilePath = null;
                request.WarrantyDocument.Ref = null;
                request.WarrantyDocument.File = null;
            }

            request.IncidentInfo?.Images.ForEach(
                dr =>
                {
                    var doc = _documentService.CreateDocument(dr);
                    dr.Id = doc.Id;
                    dr.Filename = null;
                    dr.Mimetype = null;
                    dr.Note = null;
                    dr.OverrideFilePath = null;
                    dr.Ref = null;
                    dr.File = null;
                });
            return _taskWorkFlow.RequestTaskRegistration(request, wfid);
        }

        public List<TaskResponse> GetPengingTasks()
        {
            _taskWorkFlow.SetContext(Context);
            _taskWorkFlow.SetSession(GetSession());
            return _taskWorkFlow.GetPendingTasks();
        }

        public List<TaskResponse> GetRejectedTasks()
        {
            _taskWorkFlow.SetContext(Context);
            _taskWorkFlow.SetSession(GetSession());
            return _taskWorkFlow.GetRejectedTasks();
        }

        public void CreateTask(TaskRequest model)
        {
            var taskId = SaveTaskData(model);
            //return tsk;
            return;
        }
        private Guid SaveTaskData(TaskRequest model, Task task = null)
        {
            #region Adding New Task
            if (task == null)
            {
                task = new Task
                {
                    Id = Guid.NewGuid(),
                    Wfid = (Guid)model.Wfid,
                    Name = model.Name,
                    Description = model.Description,
                    StartDateExpected = model.StartDateExpected,
                    StartDateActual = model.StartDateExpected,
                    EndDateExpected = model.EndDateExpected,
                    EndDateActual = model.EndDateExpected,
                    Type = (int)model.TaskType,
                    MaintenanceType = (int)model.MaintenanceType,
                    CreatedBy = model.CreatedBy
                };
                Context.Task.Add(task);
                Context.SaveChanges();

                if (model.AssetId != null)
                {
                    Context.AssetTask.Add(new AssetTask
                    {
                        AssetId = (int)model.AssetId,
                        TaskId = task.Id
                    });
                    Context.SaveChanges();
                }
                switch (model.TaskType)
                {
                    case types.Models.TaskType.Periodic:
                        Context.PeriodicTask.Add(new PeriodicTask
                        {
                            TaskId = task.Id,
                            Interval = (int)model.IntervalDays
                        });
                        Context.SaveChanges();
                        break;

                    case types.Models.TaskType.Incident:
                        Context.IncidentTask.Add(new IncidentTask
                        {
                            TaskId = task.Id,
                            GeoLocation = model.IncidentInfo.GeoLocation,
                            Incident = model.IncidentInfo.Incident,
                            Description = model.IncidentInfo.Description,
                            ReportedBy = model.IncidentInfo.ReportedBy
                        });
                        Context.SaveChanges();
                        model.IncidentInfo.Images.ForEach(
                            img =>
                            {
                                Context.IncidentTaskImages.Add(
                                    new IncidentTaskImages
                                    {
                                        ImageId = (Guid)img.Id,
                                        TaskId = task.Id
                                    });
                                Context.SaveChanges();
                            });
                        break;
                    default:
                        break;
                }

            }
            #endregion

            #region Adding Task dependency
            model.DependUpon?.ForEach(
                du =>
                {
                    TaskDependency dp = new TaskDependency
                    {
                        Dependent = task.Id,
                        DependUpon = du
                    };
                    Context.TaskDependency.Add(dp);
                    Context.SaveChanges();
                });
            #endregion

            #region Setting TaskPriority
            TaskPriority priority = new TaskPriority
            {
                PriorityId = (int)model.Priority,
                TaskId = task.Id
            };
            Context.TaskPriority.Add(priority);
            Context.SaveChanges();
            #endregion

            #region Setting Assigned type  Employee and Team
            if (model.TaskAssignee != null)
                AssignTaskHelper(model.TaskAssignee, task.Id);

            #endregion

            #region saving warranty document
            if (model.WarrantyDocument != null)
            {
                task.TaskWarranty.Add(new TaskWarranty
                {
                    TaskId = task.Id,
                    WarrantyDocument = (Guid)model.WarrantyDocument.Id
                });
                Context.SaveChanges();
            }
            #endregion
            return task.Id;
        }

        internal void RestartPeriodicTask(Guid id)
        {
            UpdateEndDate(id);
            var newTask = GetTaskRequestById(id);
            RequestTaskRegistration(newTask, null);
        }

        private TaskRequest GetTaskRequestById(Guid taskId)
        {
            var response = GetTaskById(taskId);
            _documentService.SetContext(Context);
            int endDate = (int)(response?.EndDateActual?.Subtract((DateTime)response.StartDateActual).TotalDays) + response.IntervalDays ?? 0;
            TaskRequest request = new TaskRequest
            {
                Id = null,
                Wfid = null,
                Name = response.Name,
                IncidentInfo = null,
                Priority = response.Priority,
                TaskAssignee = response.TaskAssignee,
                TaskType = response.TaskType,
                AssetId = response.AssetId,
                CreatedBy = response.CreatedBy.Id,
                Description = response.Description,
                DependUpon = response.DependUpon,
                StartDateExpected = DateTime.Now.AddDays((int)response.IntervalDays),
                EndDateExpected = DateTime.Now.AddDays(endDate),
                MaintenanceType = response.MaintenanceType,
                IntervalDays = (int)response.IntervalDays,
                StartDateActual = DateTime.Now,
                EndDateActual = null
            };

            var doc = _documentService.GetDocument(response.WarrantyDocument.Id);
            if (doc != null)
            {
                types.Models.Document.DocumentRequest reqDoc = new omms.types.Models.Document.DocumentRequest
                {
                    Id = null,
                    File = doc.File.ToString(),
                    Filename = doc.Filename,
                    Mimetype = doc.Mimetype,
                    OverrideFilePath = doc.OverrideFilePath,
                    Note = doc.Note,
                    Type = doc.Type,
                    Date = doc.Date,
                    Ref = doc.Ref

                };
                request.WarrantyDocument = reqDoc;
            }

            return request;
        }

        internal void UpdateEndDate(Guid taskId)
        {
            var task = Context.Task.Find(taskId);
            task.EndDateActual = DateTime.Now;
            Context.Task.Update(task);
            Context.SaveChanges();
        }

        internal Tree<TaskResponse> GetTaskDependency(Guid taskId)
        {
            var task = GetTaskById(taskId);
            if (task == null)
                return null;
            dependencyTree = new Tree<TaskResponse>(task);
            TaskDependencyHelper(dependencyTree.Root);
            return dependencyTree;

        }


        void TaskDependencyHelper(Node<TaskResponse> node)
        {
            if (node == null)
                return;
            var dependUponTasks = Context.TaskDependency.Where(t => t.Dependent == node.Data.Id).ToList();
            dependUponTasks?.ForEach(
                td =>
                {
                    TaskResponse tm = GetTaskById(td.DependUpon);
                    if (!TaskExists(tm))
                        node.Children.Add(new Node<TaskResponse>(tm));
                });
            node.Children.ForEach(n => TaskDependencyHelper(n));
        }


        Boolean TaskExists(TaskResponse tsk)
        {
            if (dependencyTree.Root == null)
                return false;
            Boolean exists = false;
            TaskExistHelper(dependencyTree.Root, tsk, ref exists);
            return exists;
        }

        void TaskExistHelper(Node<TaskResponse> node, TaskResponse tsk, ref Boolean exists)
        {
            if (tsk.Id == node.Data?.Id)
            {
                exists = true;
                return;
            }
            foreach (var child in node.Children)
                TaskExistHelper(child, tsk, ref exists);
        }


        internal void CheckDependency(Guid wfid)
        {
            var task = Context.Task.FirstOrDefault(tsk => tsk.Wfid == wfid);
            if (task == null)
                throw new Exception("Task not found");
            var dependUpon = Context.TaskDependency.Where(ts => ts.Dependent == task.Id).ToList();
            dependUpon.ForEach(dep =>
            {
                var tsk = Context.Task.Find(dep.DependUpon);
                var depWf = Context.Workflow.Find(tsk.Wfid);
                if (depWf.CurrentState == (int)TaskState.RegistrationApprived)
                    throw new Exception("Task can not be approved, There are uncompleted dependency");
            });
        }

        internal TaskResponse UpdateTaskAssignee(Assignee assignee, Guid wfid)
        {
            var task = Context.Task.FirstOrDefault(tsk => tsk.Wfid == wfid);
            if (task == null)
                return null;
            return AssignTaskHelper(assignee, task.Id);
        }
        private TaskResponse AssignTaskHelper(Assignee assignee, Guid taskId)
        {
            var assigneeType = Context.TaskAssignee.FirstOrDefault(ts => ts.TaskId == taskId);
            if (assigneeType == null)
            {
                Context.TaskAssignee.Add(new TaskAssignee
                {
                    TaskId = taskId,
                    Asignee = (int)assignee.AssigneeType
                });
                Context.SaveChanges();
            }
            else
            {
                assigneeType.Asignee = (int)assignee.AssigneeType;
                Context.TaskAssignee.Update(assigneeType);
                Context.SaveChanges();
            }

            switch (assignee.AssigneeType)
            {
                case AssigneeType.None:
                    return GetTaskById(taskId);
                case AssigneeType.Employee:
                    EmployeeTask et = new EmployeeTask
                    {
                        TaskId = taskId,
                        EmployeeId = assignee.AssigneeModel.AssigneeId,
                        DateAssigned = DateTime.Now
                    };
                    Context.EmployeeTask.Add(et);
                    Context.SaveChanges();
                    return GetTaskById(taskId);
                case AssigneeType.Team:

                    long assigneeId = Convert.ToInt64(assignee.AssigneeModel.AssigneeId);
                    TeamTask ts = new TeamTask
                    {
                        TaskId = taskId,
                        TeamId = assigneeId,
                        DateAssigned = DateTime.Now
                    };
                    Context.TeamTask.Add(ts);
                    Context.SaveChanges();

                    return GetTaskById(taskId);
                default:
                    return GetTaskById(taskId);
            }

        }

        internal IEnumerable<TaskResponse> GetWaitingStateTasks()
        {
            List<TaskResponse> response = new List<TaskResponse>();
            string employeeId = EmployeeHelper.GetEmployeeByLoginName(GetSession().Username).Id;
            var tasks = Context.Task.ToList();
            tasks.ForEach(
                tsk =>
                {
                    var wf = Context.Workflow.Find(tsk.Wfid);

                    if (wf.CurrentState == (int)TaskState.CancelRequested ||
                        wf.CurrentState == (int)TaskState.TerminateRequsted ||
                        wf.CurrentState == (int)TaskState.CompleteRequested ||
                        wf.CurrentState == (int)TaskState.AssigneeModificationRequested
                        )
                    {
                        if (GetSession().Role != (int)types.Models.User.RoleType.TecnnicalSupervisor)
                        {
                            if (tsk.CreatedBy == employeeId)
                                response.Add(PopulateTaskData(tsk));
                        }
                        else
                            response.Add(PopulateTaskData(tsk));
                    }
                });


            //var wfList = Context.Workflow.Where(
            //    wf =>
            //     wf.CurrentState == (int)TaskState.CancelRequested ||
            //     wf.CurrentState == (int)TaskState.TerminateRequsted ||
            //     wf.CurrentState == (int)TaskState.CompleteRequested ||
            //     wf.CurrentState == (int)TaskState.AssigneeModificationRequested).ToList();

            //wfList.ForEach(
            //    wf =>
            //    {
            //        var task = Context.Task.FirstOrDefault(t => t.Wfid == wf.Id);
            //        if (task != null)
            //            if (GetSession().Role != (int)types.Models.User.RoleType.TecnnicalSupervisor)
            //            {
            //                if (task.CreatedBy == employeeId)
            //                    response.Add(GetTaskById(task.Id));
            //            }
            //            else
            //                response.Add(GetTaskById(task.Id));

            //    });
            return response;
        }

        public NoteResponse AddNote(NoteRequest request)
        {
            _documentService.SetContext(Context);
            var employeeId = EmployeeHelper.GetEmployeeByLoginName(GetSession().Username).Id;
            TaskNote note = new TaskNote
            {
                Date = DateTime.Now,
                Header = request.Header,
                Note = request.Note,
                NoteBy = employeeId,
                TaskId = request.TaskId,
            };
            Context.TaskNote.Add(note);
            Context.SaveChanges();

            request
                .Images?
                .ForEach(
                    img =>
                    {
                        var doc = _documentService.CreateDocument(img);
                        Context.NoteImages.Add(new NoteImages
                        {
                            DocumentId = doc.Id,
                            NoteId = note.Id
                        });
                        Context.SaveChanges();
                    });


            return GetNoteById(note.Id);
        }

        public List<NoteResponse> GetTaskNotes(Guid taskId)
        {
            List<NoteResponse> Notes = new List<NoteResponse>();
            var notes = Context.TaskNote
                .Where(n => n.TaskId == taskId)
                .OrderByDescending(nt => nt.Date)
                .ToList();
            notes.ForEach(
                note =>
                {
                    Notes.Add(GetNoteById(note.Id));
                });
            return Notes;
        }

        internal int NumberOfNewIndeviduialTasks()
        {
            var individualTasks = GetIndividualTasks();
            int ivTasksCounter = 0;
            var employeeId = EmployeeHelper.GetEmployeeByLoginName(GetSession().Username).Id;
            individualTasks.ForEach(task =>
            {
                var notification = Context.TaskNotification.FirstOrDefault(tn => tn.TaskId == task.Id && tn.EmployeeId == employeeId);
                if (notification == null)
                {
                    CreateNewNotification((Guid)task.Id, employeeId);
                    ++ivTasksCounter;
                }
                else
                {
                    if (!notification.Seen)
                        ++ivTasksCounter;
                }
            });
            return ivTasksCounter;
        }

        internal TaskMetaDataModel GetTaskMetaData()
        {
            TaskMetaDataModel report = new TaskMetaDataModel();

            var tasks = Context.Task.ToList();
            tasks.ForEach(tsk =>
            {
                switch ((types.Models.TaskType)tsk.Type)
                {
                    case types.Models.TaskType.Incident:
                        report.IncidentTask = UpdateReportModel(tsk, report.IncidentTask);
                        break;
                    case types.Models.TaskType.Periodic:
                        report.PeriodicTask = UpdateReportModel(tsk, report.PeriodicTask);
                        break;

                    case types.Models.TaskType.Unscheduled:
                        report.UnscheduledTask = UpdateReportModel(tsk, report.UnscheduledTask);
                        break;

                    case types.Models.TaskType.Scheduled:
                        report.ScheduledTask = UpdateReportModel(tsk, report.ScheduledTask);
                        break;
                }
            });
            return report;
        }

        internal Guid UpdateTaskAssignee(AssigneeViewModel assignee)
        {
            _taskWorkFlow.SetContext(Context);
            _taskWorkFlow.SetSession(GetSession());
            return _taskWorkFlow.RequestAssegneeModification(assignee);
        }

        private MDModel UpdateReportModel(Task tsk, MDModel report)
        {
            MDModel model = report;
            var wf = Context.Workflow.Find(tsk.Wfid);
            switch ((TaskState)wf.CurrentState)
            {
                case TaskState.CancelRequested:
                    ++model.WaitingForCancellation;
                    break;
                case TaskState.CompleteRequested:
                    ++model.WaitingForCompletion;
                    break;
                case TaskState.TerminateRequsted:
                    ++model.WaitingForTermination;
                    break;

                case TaskState.Cancelled:
                    ++model.Cancelled;
                    break;

                case TaskState.Terminated:
                    ++model.Terminated;
                    break;

                case TaskState.Completed:
                    ++model.Completed;
                    break;
            }
            return model;
        }

        internal List<TaskResponse> MarkTeamTasksAsSeen()
        {
            List<TaskResponse> response = new List<TaskResponse>();
            var indvTasks = GetTeamTasks();
            string employeeId = EmployeeHelper.GetEmployeeByLoginName(GetSession().Username).Id;
            indvTasks.ForEach(tsk =>
            {
                var notification = Context.TaskNotification.FirstOrDefault(tn => tn.EmployeeId == employeeId && tn.TaskId == tsk.Id);
                if (notification == null)
                {
                    CreateNewNotification((Guid)tsk.Id, employeeId);
                    notification = Context.TaskNotification.FirstOrDefault(tn => tn.EmployeeId == employeeId && tn.TaskId == tsk.Id);
                }
                if (!notification.Seen)
                    response.Add(tsk);
                notification.Seen = NotificationState.SEEN;
                Context.TaskNotification.Update(notification);
                Context.SaveChanges();
            });
            return response;
        }

        internal List<TaskResponse> MarkIndvTasksAsSeen()
        {
            List<TaskResponse> response = new List<TaskResponse>();
            var teamTasks = GetIndividualTasks();
            string employeeId = EmployeeHelper.GetEmployeeByLoginName(GetSession().Username).Id;
            teamTasks.ForEach(tsk =>
            {
                var notification = Context.TaskNotification.FirstOrDefault(tn => tn.EmployeeId == employeeId && tn.TaskId == tsk.Id);
                if (notification == null)
                {
                    CreateNewNotification((Guid)tsk.Id, employeeId);
                    notification = Context.TaskNotification.FirstOrDefault(tn => tn.EmployeeId == employeeId && tn.TaskId == tsk.Id);
                }
                if (!notification.Seen)
                    response.Add(tsk);
                notification.Seen = NotificationState.SEEN;
                Context.TaskNotification.Update(notification);
                Context.SaveChanges();
            });
            return response;
        }

        internal int NumberOfNewTeamTasks()
        {
            var teamTasks = GetTeamTasks();
            var employeeId = EmployeeHelper.GetEmployeeByLoginName(GetSession().Username).Id;
            int teamTaskCounter = 0;

            teamTasks.ForEach(task =>
            {
                var notification = Context.TaskNotification.FirstOrDefault(tn => tn.TaskId == task.Id && tn.EmployeeId == employeeId);
                if (notification == null)
                {
                    CreateNewNotification((Guid)task.Id, employeeId);
                    ++teamTaskCounter;
                }
                else
                {
                    if (!notification.Seen)
                        ++teamTaskCounter;
                }
            });
            return teamTaskCounter;
        }

        void CreateNewNotification(Guid taskId, string employeeId)
        {
            Context.TaskNotification.Add(new TaskNotification
            {
                EmployeeId = employeeId,
                TaskId = taskId,
                Seen = NotificationState.NOT_SEEN
            });
            Context.SaveChanges();
        }

        public NoteResponse GetNoteById(int noteId)
        {

            var note = Context.TaskNote.Find(noteId);
            if (note == null)
                return null;
            _documentService.SetContext(Context);
            NoteResponse response = new NoteResponse
            {
                Id = note.Id,
                Date = note.Date,
                Header = note.Header,
                Note = note.Note,
                TaskId = note.TaskId
            };
            response.NoteBy = EmployeeHelper.GetEmployeeById(note.NoteBy);

            var imgs = Context.NoteImages.Where(im => im.NoteId == noteId).ToList();
            imgs.ForEach(
                img =>
                {
                    var i = _documentService.GetDocumentResponse(img.DocumentId);
                    response.Images.Add(i);
                });

            return response;
        }

        #region task status workflow

        public Guid RequestTaskCancellation(WFRequestModel request)
        {
            _taskWorkFlow.SetContext(Context);
            _taskWorkFlow.SetSession(GetSession());
            return _taskWorkFlow.RequestTaskCancellation(request);
        }

        public Guid RequestTaskTermination(WFRequestModel request)
        {
            _taskWorkFlow.SetContext(Context);
            _taskWorkFlow.SetSession(GetSession());
            return _taskWorkFlow.RequestTaskTermination(request);
        }

        public Guid RequestTaskCompletion(WFRequestModel request)
        {
            _taskWorkFlow.SetContext(Context);
            _taskWorkFlow.SetSession(GetSession());
            return _taskWorkFlow.RequestTaskCompletion(request);
        }

        public Guid ApproveTaskState(Guid wfid)
        {
            _taskWorkFlow.SetContext(Context);
            _taskWorkFlow.SetSession(GetSession());
            return _taskWorkFlow.ApproveTaskState(wfid);
        }

        public Guid RejectTaskState(WFRequestModel request)
        {
            _taskWorkFlow.SetContext(Context);
            _taskWorkFlow.SetSession(GetSession());
            return _taskWorkFlow.RejectTaskState(request);
        }


        #endregion

        #region Getting Tasks by Status
        public List<TaskResponse> GetTasksByState(int state)
        {
            List<TaskResponse> tasks = new List<TaskResponse>();
            var tsks = Context.Task.ToList();
            tsks.ForEach(
                t =>
                {
                    var wfs = Context.Workflow.Find(t.Wfid);
                    if (wfs.CurrentState == state)
                        tasks.Add(GetTaskById(t.Id));
                });
            return tasks;
        }

        internal List<TaskResponse> GetIndividualTasks()
        {
            List<TaskResponse> response = new List<TaskResponse>();
            var employeeId = EmployeeHelper.GetEmployeeByLoginName(GetSession().Username).Id;
            var idList = Context.EmployeeTask.Where(emt => emt.EmployeeId == employeeId).ToList();
            SetContext(Context);

            idList.ForEach(
                id =>
                {
                    var task = Context.Task.Find(id.TaskId);
                    var wf = Context.Workflow.Find(task.Wfid);
                    if (wf.CurrentState == (int)TaskState.RegistrationApprived)
                        response.Add(GetTaskById(task.Id));
                });
            return response;
        }

        internal List<TaskResponse> GetTeamTasks()
        {
            List<TaskResponse> response = new List<TaskResponse>();

            var employeeId = EmployeeHelper.GetEmployeeByLoginName(GetSession().Username).Id;
            var emplTeams = Context.TeamMembers.Where(tm => tm.MemberId == employeeId).ToList();

            emplTeams.ForEach(
                tm =>
                {
                    var team = Context.Team.Find(tm.TeamId);
                    var teamWorkflow = Context.Workflow.Find(team.Wfid);
                    if (teamWorkflow.CurrentState == (int)TeamState.RegistrationApprived)
                    {
                        var idList = Context.TeamTask.Where(emt => emt.TeamId == tm.TeamId).ToList();
                        idList.ForEach(
                            id =>
                            {
                                var task = Context.Task.Find(id.TaskId);
                                var wf = Context.Workflow.Find(task.Wfid);
                                if (wf.CurrentState == (int)TaskState.RegistrationApprived)
                                    response.Add(GetTaskById(task.Id));
                            });
                    }
                });
            return response;
        }

        internal List<TaskResponse> GetTasksByTeamId(int teamId)
        {
            List<TaskResponse> response = new List<TaskResponse>();
            var tsId = Context.TeamTask.Where(tt => tt.TeamId == teamId).ToList();
            tsId.ForEach(
                t =>
                {
                    response.Add(GetTaskById(t.TaskId));
                });
            return response;
        }

        internal NoteResponse GetLatestTaskNote(Guid taskId)
        {
            var note = Context.TaskNote.Where(n => n.TaskId == taskId).OrderByDescending(w => w.Date).FirstOrDefault();
            return note == null
                ?
                null
                :
                GetNoteById(note.Id);
        }

        #endregion

        internal List<EmployeeReportModel> GetEmployeeReport()
        {
            List<EmployeeReportModel> report = new List<EmployeeReportModel>();
            var employees = EmployeeHelper.GetAllEmployees();
            employees.ForEach(
                empl =>
                {
                    EmployeeReportModel model = new EmployeeReportModel
                    {
                        EmployeeId = empl.Id,
                        Name = empl.UserName,
                        OnGoingTasks = GetTaskByStatusAndEmployee(empl.Id, TaskState.RegistrationApprived),
                        CancelledTasks = GetTaskByStatusAndEmployee(empl.Id, TaskState.Cancelled),
                        CompletedTasks = GetTaskByStatusAndEmployee(empl.Id, TaskState.Completed),
                        TerminatedTasks = GetTaskByStatusAndEmployee(empl.Id, TaskState.Terminated)
                    };
                    model.TotalTasks = model.OnGoingTasks +
                                       model.CancelledTasks +
                                       model.TerminatedTasks +
                                       model.CompletedTasks;
                    report.Add(model);
                });
            return report;
        }

        private int GetTaskByStatusAndEmployee(string employeeId, TaskState state)
        {
            var tasks = Context.EmployeeTask.Where(et => et.EmployeeId == employeeId).ToList();
            if (tasks == null || tasks.Count == 0)
                return 0;
            else
            {
                int counter = 0;
                tasks.ForEach(
                    tsk =>
                    {
                        var task = Context.Task.Find(tsk.TaskId);
                        var wf = Context.Workflow.Find(task.Wfid);
                        if (wf.CurrentState == (int)state)
                            ++counter;
                    });
                return counter;
            }
        }

        internal TaskReportViewModel GetTaskReport()
        {

            List<TaskReportModel> report = new List<TaskReportModel>();
            var tasks = Context.Task.ToList();
            tasks.ForEach(
                tsk =>
                {
                    var task = GetTaskById(tsk.Id);
                    TaskReportModel model = new TaskReportModel
                    {
                        Id = task.Id.ToString(),
                        Name = task.Name,
                        Type = task.TaskType.ToString(),
                        Status = task.State,
                        AssigneeType = task.TaskAssignee.AssigneeType.ToString(),
                        AssigneeName = task.TaskAssignee.AssigneeType == AssigneeType.Team ?
                                       Context.Team.Find(Convert.ToInt64(task.TaskAssignee.AssigneeModel.AssigneeId)).Name
                                       :
                                       task.TaskAssignee.AssigneeType == AssigneeType.Employee ?
                                       EmployeeHelper.GetEmployeeById(task.TaskAssignee.AssigneeModel.AssigneeId).UserName
                                       :
                                       "",

                        EndDate = task.EndDateExpected == null ? "" : ((DateTime)task.EndDateExpected).ToString("MM/dd/yyyy"),

                        StartDate = task.StartDateExpected == null ? "" : ((DateTime)task.StartDateActual).ToString("MM/dd/yyyy"),

                        //DelayDays = task.TaskType != types.Models.TaskType.Unscheduled ?
                        //             (int)((DateTime)task.EndDateActual - (DateTime)task.EndDateExpected).TotalDays
                        //             :
                        //             0
                    };


                    report.Add(model);
                });


            TaskReportViewModel responseModel = new TaskReportViewModel
            {
                Reports = report,
                IncidentTasks = new TRMetadata
                {
                    Cancelled = GetNoOfTasks(types.Models.TaskType.Incident, TaskState.Cancelled),
                    Completed = GetNoOfTasks(types.Models.TaskType.Incident, TaskState.Completed),
                    Terminated = GetNoOfTasks(types.Models.TaskType.Incident, TaskState.Terminated),
                    Ongoing = GetNoOfTasks(types.Models.TaskType.Incident, TaskState.RegistrationApprived),
                },
                PeriodicTasks = new TRMetadata
                {
                    Cancelled = GetNoOfTasks(types.Models.TaskType.Periodic, TaskState.Cancelled),
                    Completed = GetNoOfTasks(types.Models.TaskType.Periodic, TaskState.Completed),
                    Terminated = GetNoOfTasks(types.Models.TaskType.Periodic, TaskState.Terminated),
                    Ongoing = GetNoOfTasks(types.Models.TaskType.Periodic, TaskState.RegistrationApprived),
                },
                ScheduledTasks = new TRMetadata
                {
                    Cancelled = GetNoOfTasks(types.Models.TaskType.Scheduled, TaskState.Cancelled),
                    Completed = GetNoOfTasks(types.Models.TaskType.Scheduled, TaskState.Completed),
                    Terminated = GetNoOfTasks(types.Models.TaskType.Scheduled, TaskState.Terminated),
                    Ongoing = GetNoOfTasks(types.Models.TaskType.Scheduled, TaskState.RegistrationApprived),
                },
                UnscheduledTasks = new TRMetadata
                {
                    Cancelled = GetNoOfTasks(types.Models.TaskType.Unscheduled, TaskState.Cancelled),
                    Completed = GetNoOfTasks(types.Models.TaskType.Unscheduled, TaskState.Completed),
                    Terminated = GetNoOfTasks(types.Models.TaskType.Unscheduled, TaskState.Terminated),
                    Ongoing = GetNoOfTasks(types.Models.TaskType.Unscheduled, TaskState.RegistrationApprived),
                },
            };
            return responseModel;
        }

        private int GetNoOfTasks(types.Models.TaskType type, TaskState state) => Context.Task.Count(t => t.Type == (int)type && t.Wf.CurrentState == (int)state);

        internal byte[] GetEmployeeReportDocument(string docType, string filterParams)
        {
            using (ExcelPackage package = new ExcelPackage())
            {

                string workSheetName = "employee report";
                package.Workbook.Properties.Title = "Employee Report";
                package.Workbook.Properties.Author = "Omms";
                package.Workbook.Properties.Subject = "Employee Report";
                package.Workbook.Properties.Keywords = "Employee";
                package.Workbook.Properties.Created = DateTime.Now;

                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(workSheetName);

                worksheet.Cells[1, 1].Value = EmployeeReport.EmployeeId;
                worksheet.Cells[1, 2].Value = EmployeeReport.Name;
                worksheet.Cells[1, 3].Value = EmployeeReport.OnGoingTasks;
                worksheet.Cells[1, 4].Value = EmployeeReport.CompletedTasks;
                worksheet.Cells[1, 5].Value = EmployeeReport.CancelledTasks;
                worksheet.Cells[1, 6].Value = EmployeeReport.TerminatedTasks;
                worksheet.Cells[1, 7].Value = EmployeeReport.TotalTasks;


                var employeereport = GetEmployeeReport();
                List<EmployeeReportModel> filteredReport = new List<EmployeeReportModel>();
                if (String.IsNullOrWhiteSpace(filterParams))
                    filteredReport = employeereport;
                else
                    filteredReport = FilterEmployeeReport(employeereport, filterParams.Split(';'));
                int row = 2;
                filteredReport.ForEach(
                    report =>
                    {
                        worksheet.Cells[row, 1].Value = report.EmployeeId.ToString();
                        worksheet.Cells[row, 2].Value = report.Name;
                        worksheet.Cells[row, 3].Value = report.OnGoingTasks.ToString();
                        worksheet.Cells[row, 4].Value = report.CompletedTasks.ToString();
                        worksheet.Cells[row, 5].Value = report.CancelledTasks.ToString();
                        worksheet.Cells[row, 6].Value = report.TerminatedTasks.ToString();
                        worksheet.Cells[row, 7].Value = report.TotalTasks.ToString();
                        ++row;
                    });

                PDFConverter pdfConverter = new PDFConverter();
                if (docType.ToLower() == RPDocumentType.EXCEL)
                    return package.GetAsByteArray();
                return pdfConverter.ConvertExcelToPdfReport(package, workSheetName, EmployeeReport.Metadata);
            }
        }

        private List<EmployeeReportModel> FilterEmployeeReport(List<EmployeeReportModel> employeereport, string[] filterParams)
        {
            List<EmployeeReportModel> filteredReport = new List<EmployeeReportModel>();
            var filterParamsList = filterParams.ToList();
            employeereport.ForEach(
                empRprt =>
                {
                    bool match = false;
                    filterParamsList.ForEach(
                        fp =>
                        {
                            switch ((ERDFilterParams)(Convert.ToInt32(fp)))
                            {
                                case ERDFilterParams.ASSIGNED_EMPLYEES:
                                    if (empRprt.TotalTasks > 0)
                                        match = true;
                                    break;

                                case ERDFilterParams.UNASSIGNED_EMPLOYEES:
                                    if (empRprt.TotalTasks == 0)
                                        match = true;
                                    break;

                                case ERDFilterParams.WITH_CANCELLED_TASKS:
                                    if (empRprt.CancelledTasks > 0)
                                        match = true;
                                    break;
                                case ERDFilterParams.WITH_COMPLETED_TASK:
                                    if (empRprt.CompletedTasks > 0)
                                        match = true;
                                    break;

                                case ERDFilterParams.WITH_ONGOING_TASK:
                                    if (empRprt.OnGoingTasks > 0)
                                        match = true;
                                    break;

                                case ERDFilterParams.WITH_TERMINATED_TASKS:
                                    if (empRprt.TerminatedTasks > 0)
                                        match = true;
                                    break;
                            }
                        });
                    if (match)
                        filteredReport.Add(empRprt);
                });
            return filteredReport;
        }

        internal byte[] GetTaskReportDocument(string docType, string filterParams)
        {
            using (ExcelPackage package = new ExcelPackage())
            {

                string workSheetName = "task report";
                package.Workbook.Properties.Title = TaskReport.Metadata.Title;
                package.Workbook.Properties.Author = ReportConstants.Author;
                package.Workbook.Properties.Subject = ReportConstants.Subject;
                package.Workbook.Properties.Keywords = ReportConstants.key;
                package.Workbook.Properties.Created = DateTime.Now;

                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(workSheetName);

                worksheet.Cells[1, 1].Value = TaskReport.Name;
                worksheet.Cells[1, 2].Value = TaskReport.TaskType;
                worksheet.Cells[1, 3].Value = TaskReport.Status;
                worksheet.Cells[1, 4].Value = TaskReport.AssigneeType;
                worksheet.Cells[1, 5].Value = TaskReport.AssigneeName;

                worksheet.Cells[1, 6].Value = TaskReport.StartDate;
                worksheet.Cells[1, 7].Value = TaskReport.EndDate;
                //worksheet.Cells[1, 8].Value = TaskReport.DelayIndays;


                var taskReport = GetTaskReport().Reports;

                List<TaskReportModel> filteredReport = new List<TaskReportModel>();
                if (String.IsNullOrWhiteSpace(filterParams))
                    filteredReport = taskReport;
                else
                    filteredReport = FilterTaskReport(taskReport, filterParams.Split(';'));
                int row = 2;
                filteredReport.ForEach(
                    report =>
                    {
                        //worksheet.Cells[row, 1].Value = report.Id.ToString();
                        worksheet.Cells[row, 1].Value = report.Name;
                        worksheet.Cells[row, 2].Value = report.Type;
                        worksheet.Cells[row, 3].Value = report.Status;
                        worksheet.Cells[row, 4].Value = report.AssigneeType;
                        worksheet.Cells[row, 5].Value = report.AssigneeName;

                        worksheet.Cells[row, 6].Value = report.StartDate;
                        worksheet.Cells[row, 7].Value = report.EndDate;
                  //      worksheet.Cells[row, 8].Value = report.DelayDays;

                        ++row;
                    });

                PDFConverter pdfConverter = new PDFConverter();
                if (docType.ToLower() == RPDocumentType.EXCEL)
                    return package.GetAsByteArray();

                return pdfConverter.ConvertExcelToPdfReport(package, workSheetName, TaskReport.Metadata);
            }
        }

        private List<TaskReportModel> FilterTaskReport(List<TaskReportModel> employeereport, string[] filterParams)
        {
            List<TaskReportModel> filteredReport = new List<TaskReportModel>();
            var filterParamsList = filterParams.ToList();
            employeereport.ForEach(
                empRprt =>
                {
                    bool match = false;
                    filterParamsList.ForEach(
                        fp =>
                        {
                            switch ((TRDFilterParams)(Convert.ToInt32(fp)))
                            {
                                case TRDFilterParams.EMPLOYEE:
                                    if (empRprt.AssigneeType.ToLower() == "employee")
                                        match = true;
                                    break;

                                case TRDFilterParams.TEAM:
                                    if (empRprt.AssigneeType.ToLower() == "team")
                                        match = true;
                                    break;

                                case TRDFilterParams.ONGOING_TASKS:
                                    if (empRprt.Status.ToLower() == "on going task")
                                        match = true;
                                    break;
                                case TRDFilterParams.CANCELLED_TASKS:
                                    if (empRprt.Status.ToLower() == "cancelled task")
                                        match = true;
                                    break;

                                case TRDFilterParams.COMPLETED_TASTKS:
                                    if (empRprt.Status.ToLower() == "completed task")
                                        match = true;
                                    break;

                                case TRDFilterParams.TERMINATED_TASKS:
                                    if (empRprt.Status.ToLower() == "terminated task")
                                        match = true;
                                    break;



                                case TRDFilterParams.UNSCHEDULED_TASKS:
                                    if (empRprt.Type.ToLower() == "unscheduled")
                                        match = true;
                                    break;
                                case TRDFilterParams.SCHEDULED_TASKS:
                                    if (empRprt.Type.ToLower() == "scheduled")
                                        match = true;
                                    break;
                                case TRDFilterParams.PERIODIC_TASKS:
                                    if (empRprt.Type.ToLower() == "periodic")
                                        match = true;
                                    break;
                                case TRDFilterParams.INCIDENT_TASKS:
                                    if (empRprt.Type.ToLower() == "incident")
                                        match = true;
                                    break;

                            }
                        });
                    if (match)
                        filteredReport.Add(empRprt);
                });
            return filteredReport;
        }


    }
}
