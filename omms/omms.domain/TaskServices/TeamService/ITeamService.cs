﻿using omms.types.Models;
using omms.types.Models.Thread;
using System;
using System.Collections.Generic;

namespace omms.domain.TaskServices
{
    public interface ITeamService
    {

        List<TaskResponse> GetTeamTask(long teamId);

        //chatroom service
        List<MessageResponse> GetMessages(Guid threadId);
        MessageResponse SendMessage(MessageRequest message);



        Guid RequestTeamRegistration(TeamModel team, Guid? wfid);
        Guid ApproveTeamRegistration(Guid wfid);
        Guid RejectTeamRegistration(WFRequestModel request);
        TeamModel CreateTeam(TeamModel team);
        bool DeleteTeam(long teamId);
        TeamModel GetTeamById(long teamId);


        List<TeamModel> GetTeamList(string memberId);
    }
}
