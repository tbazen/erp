﻿using OfficeOpenXml;
using omms.data.Entities;
using omms.domain.Documents;
using omms.domain.Infrastracture;
using omms.domain.TaskServices.Helper;
using omms.domain.TaskServices.SharedServices;
using omms.domain.Workflow.Models;
using omms.domain.Workflow.services;
using omms.types.Models;
using omms.types.Models.Thread;
using System;
using System.Collections.Generic;
using System.Linq;

namespace omms.domain.TaskServices
{
    public class TeamService : OmmsService, ITeamService
    {
        readonly SharedService sharedService;
        readonly TeamWorkflow _teamWorkflow;
        readonly WorkflowService _workflowService;


        public TeamService()
        {
            sharedService = new SharedService();
            _teamWorkflow = new TeamWorkflow(this);
            _workflowService = new WorkflowService();
        }

        public List<TaskResponse> GetTeamTask(long teamId)
        {
            var team = Context.Team.FirstOrDefault(t => t.Id == teamId);
            if (team == null)
                return null;
            var taskList = team.TeamTask.ToList();
            List<TaskResponse> taskModels = new List<TaskResponse>();

            taskList.ForEach(
                t =>
                {
                    sharedService.SetContext(Context);
                    var tsk = sharedService.GetTaskById(t.TaskId);
                    if (tsk != null)
                        taskModels.Add(tsk);
                });

            return taskModels;
        }


        #region Chatroom services
        public List<MessageResponse> GetMessages(Guid threadId)
        {
            var trd = Context.Thread.FirstOrDefault(t => t.Id == threadId);
            if (trd == null)
                return null;
            List<MessageResponse> messages = new List<MessageResponse>();
            var msgs = Context.ThreadMessage.Where(tm => tm.Thread == trd.Id).ToList();
            msgs?.ForEach(
                msg =>
                {
                    messages.Add(GetMessageById(msg.Message));
                });
            return messages;
        }
        public MessageResponse SendMessage(MessageRequest message)
        {
            var thread = Context.Thread.FirstOrDefault(t => t.Id == message.ThreadId);
            if (thread == null || thread.State == (int)ThreadState.Lock)
                return null;
            DocumentService documentService = new DocumentService();
            documentService.SetContext(Context);


            data.Entities.Message msg = new data.Entities.Message
            {
                Id = Guid.NewGuid(),
                TextMsg = message.TextMessage,
                Date = DateTime.Now,
            };
            if (message.Document != null)
            {
                msg.Document = documentService.CreateDocument(message.Document).Id;

            }


            Context.Message.Add(msg);
            Context.SaveChanges();


            Context.ThreadMessage.Add(new data.Entities.ThreadMessage
            {
                Message = msg.Id,
                MessageBy = message.MessageBy,
                Thread = message.ThreadId
            });
            Context.SaveChanges();
            return GetMessageById(msg.Id);
        }
        private MessageResponse GetMessageById(Guid msgId)
        {

            #region Getting Message by id
            //var msgThr = Context.ThreadMessage.FirstOrDefault(m => m.Message == msgId);
            //if (msgThr == null)
            //    return null;

            //DocumentService service = new DocumentService();
            //service.SetContext(Context);

            //var msg = Context.Message.FirstOrDefault(m => m.Id == msgThr.Message);

            //var payrollService = INTAPS.ClientServer.ApplicationServer.GetBDE<PayrollBDE>();
            //var msgBy = payrollService.GetEmployeeByID(msgThr.MessageBy.ToString());

            //MessageResponse response = new MessageResponse
            //{
            //    Id = msg.Id,
            //    Date = msg.Date,
            //    TextMessage = msg.TextMsg,
            //    MessageBy = new UserModel
            //    {
            //        Id = msgBy.id,
            //        UserName = msgBy.employeeName
            //    }
            //};
            //if (msg.Document == null)
            //{
            //    response.Document = null;
            //}
            //else
            //{
            //    response.Document = service.GetDocumentResponse((Guid)msg.Document);
            //}
            //return response;
            #endregion
            return new MessageResponse();
        }

        internal IEnumerable<TeamModel> GetTeamByState(TeamState state)
        {
            List<TeamModel> teams = new List<TeamModel>();
            var tms = Context.Team.ToList();
            tms.ForEach(tm =>
            {
                var wf = Context.Workflow.Find(tm.Wfid);
                if (wf.CurrentState == (int)state)
                    teams.Add(GetTeamById(tm.Id));
            });
            return teams;
        }
        #endregion


        public Guid RequestTeamRegistration(TeamModel team, Guid? wfid)
        {
            _teamWorkflow.SetContext(Context);
            _teamWorkflow.SetSession(GetSession());
            return _teamWorkflow.RequestTeamRegistration(team, wfid);
        }


        public Guid ApproveTeamRegistration(Guid wfid)
        {
            _teamWorkflow.SetContext(Context);
            _teamWorkflow.SetSession(GetSession());
            return _teamWorkflow.ApproverTeamRegistration(wfid);
        }

        public Guid RejectTeamRegistration(WFRequestModel request)
        {
            _teamWorkflow.SetContext(Context);
            _teamWorkflow.SetSession(GetSession());
            return _teamWorkflow.RejectTeamRegistration(request);
        }

        public TeamModel CreateTeam(TeamModel teamModel)
        {
            Team team = new Team
            {
                Name = teamModel.Name,
                Description = teamModel.Description,
                Wfid = (Guid)teamModel.Wfid
            };
            if (teamModel.Id == null)
                Context.Team.Add(team);
            else
            {
                var tm = Context.Team.Find(teamModel.Id);
                tm.Name = team.Name;
                tm.Description = team.Description;
                team.Id = tm.Id;
                Context.Team.Update(tm);
            }
            Context.SaveChanges();


            //clearing team members from database
            var members = Context.TeamMembers.Where(memb => memb.TeamId == team.Id);
            Context.TeamMembers.RemoveRange(members);
            Context.SaveChanges();


            //Adding team members
            teamModel.Members.ForEach(
                m =>
                {
                    TeamMembers mem = new TeamMembers
                    {
                        TeamId = team.Id,
                        MemberId = m.Id
                    };
                    Context.TeamMembers.Add(mem);
                    Context.SaveChanges();
                });

            //getting team info from database
            sharedService.SetContext(Context);
            return sharedService.GetTeam(team.Id);
        }

        public bool DeleteTeam(long teamId)
        {
            throw new NotImplementedException();
        }

        internal List<TeamModel> GetPendingTeams()
        {
            _teamWorkflow.SetContext(Context);
            return _teamWorkflow.GetPendingTeams();
        }

        internal List<TeamModel> GetRejectedTeams()
        {
            _teamWorkflow.SetContext(Context);
            return _teamWorkflow.GetRejectedTeams();
        }

        public TeamModel GetTeamById(long teamId)
        {
            var team = Context.Team.Find(teamId);
            if (team == null)
                return null;
            TeamModel teamResponse = new TeamModel
            {
                Id = team.Id,
                Name = team.Name,
                Description = team.Description,
                Wfid = team.Wfid
            };
            _workflowService.SetContext(Context);
            string note = _workflowService.GetLatestWorkItem<TeamModel>(team.Wfid)?.Note ?? "";
            teamResponse.Note = note;

            var empIds = Context.TeamMembers.Where(t => t.TeamId == teamId).ToList();
            empIds.ForEach(
                emp =>
                {
                    var employee = EmployeeHelper.GetEmployeeById(emp.MemberId);
                    teamResponse.Members.Add(employee);
                });
            return teamResponse;
        }

        public List<TeamModel> GetTeamList(string memberId)
        {
            return GetTeamByState(TeamState.RegistrationApprived).ToList();
        }

        internal List<TeamReportModel> GetTeamReport()
        {
            List<TeamReportModel> report = new List<TeamReportModel>();
            var teams = Context.Team.ToList();
            teams.ForEach(
                tm =>
                {
                    TeamReportModel model = new TeamReportModel
                    {
                        Id = tm.Id.ToString(),
                        Name = tm.Name,
                        DateCreated = GetCreationDate(tm.Wfid).ToString("MM/dd/yyyy"),
                        NoOfMembers = GetTeamById(tm.Id).Members.Count,
                        OnGoingTasks = GetTaskByStatusAndTeam(tm.Id, TaskState.RegistrationApprived),
                        CompletedTasks = GetTaskByStatusAndTeam(tm.Id, TaskState.Completed),
                        CancelledTasks = GetTaskByStatusAndTeam(tm.Id, TaskState.Cancelled),
                        TerminatedTasks = GetTaskByStatusAndTeam(tm.Id, TaskState.Terminated)
                    };
                    model.TotalTasks = model.OnGoingTasks +
                                       model.CompletedTasks +
                                       model.CancelledTasks +
                                       model.TerminatedTasks;
                    report.Add(model);
                });
            return report;
        }

        private int GetTaskByStatusAndTeam(long teamId, TaskState state)
        {
            var tasks = Context.TeamTask.Where(tt => tt.TeamId == teamId).ToList();
            if (tasks == null || tasks.Count == 0)
                return 0;
            else
            {
                int counter = 0;
                tasks.ForEach(
                    tsk =>
                    {
                        var task = Context.Task.Find(tsk.TaskId);
                        var wf = Context.Workflow.Find(task.Wfid);
                        if (wf.CurrentState == (int)state)
                            ++counter;
                    });
                return counter;
            }
        }

        private DateTime GetCreationDate(Guid wfid)
        {
            var workItems = Context.WorkflowItem.OrderBy(wfi => wfi.ActivityDate);
            var workItem = workItems.First(
                wi => wi.ToState == (int)TeamState.RegistrationApprived &&
                      wi.WorkflowId == wfid
            );
            return workItem.ActivityDate;
        }

        internal bool IsTeamModificationCancelled(Guid wfid)
        {
            var workflow = Context.Workflow.Find(wfid);
            if (workflow.TypeId == (int)WorkflowTypes.Team)
            {
                _teamWorkflow.SetContext(Context);
                _teamWorkflow.SetSession(GetSession());
                return _teamWorkflow.CancelTeamModification(wfid);
            }
            return false;
        }

        internal byte[] GetTeamReportDocument(string docType, string filterParams)
        {
            using (ExcelPackage package = new ExcelPackage())
            {

                string workSheetName = "team report";
                package.Workbook.Properties.Title = TeamReport.Metadata.Title;
                package.Workbook.Properties.Author = ReportConstants.Author;
                package.Workbook.Properties.Subject = ReportConstants.Subject;
                package.Workbook.Properties.Keywords = ReportConstants.key;
                package.Workbook.Properties.Created = DateTime.Now;

                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(workSheetName);

                //worksheet.Cells[1, 1].Value = TeamReport.Id;
                worksheet.Cells[1, 1].Value = TeamReport.Name;
                worksheet.Cells[1, 2].Value = TeamReport.DateCreated;
                worksheet.Cells[1, 3].Value = TeamReport.NoOfMemberes;


                worksheet.Cells[1, 4].Value = TeamReport.OnGoingTasks;
                worksheet.Cells[1, 5].Value = TeamReport.CompletedTasks;
                worksheet.Cells[1, 6].Value = TeamReport.CancelledTasks;
                worksheet.Cells[1, 7].Value = TeamReport.TerminatedTasks;
                worksheet.Cells[1, 8].Value = TeamReport.TotalTasks;


                var teamReport = GetTeamReport();


                List<TeamReportModel> filteredReport = new List<TeamReportModel>();
                if (String.IsNullOrWhiteSpace(filterParams))
                    filteredReport = teamReport;
                else
                    filteredReport = FilterTeamReport(teamReport, filterParams.Split(';'));





                int row = 2;
                filteredReport.ForEach(
                    report =>
                    {
                        //worksheet.Cells[row, 1].Value = report.Id.ToString();
                        worksheet.Cells[row, 1].Value = report.Name;
                        worksheet.Cells[row, 2].Value = report.DateCreated;
                        worksheet.Cells[row, 3].Value = report.NoOfMembers;

                        worksheet.Cells[row, 4].Value = report.OnGoingTasks;
                        worksheet.Cells[row, 5].Value = report.CompletedTasks;
                        worksheet.Cells[row, 6].Value = report.CancelledTasks;
                        worksheet.Cells[row, 7].Value = report.TerminatedTasks;
                        worksheet.Cells[row, 8].Value = report.TotalTasks;
                        ++row;
                    });

                PDFConverter pdfConverter = new PDFConverter();
                if (docType.ToLower() == RPDocumentType.EXCEL)
                    return package.GetAsByteArray();
                return pdfConverter.ConvertExcelToPdfReport(package, workSheetName, TeamReport.Metadata);
            }
        }

        private List<TeamReportModel> FilterTeamReport(List<TeamReportModel> teamReport, string[] filterParams)
        {
            List<TeamReportModel> filteredReport = new List<TeamReportModel>();
            var filterParamsList = filterParams.ToList();
            teamReport.ForEach(
                empRprt =>
                {
                    bool match = false;
                    filterParamsList.ForEach(
                        fp =>
                        {
                            switch ((TMRDFilterParams)(Convert.ToInt32(fp)))
                            {
                                case TMRDFilterParams.ASSIGNED_TEAMS:
                                    if (empRprt.TotalTasks > 0)
                                        match = true;
                                    break;

                                case TMRDFilterParams.UNASSIGNED_TEAMS:
                                    if (empRprt.TotalTasks == 0)
                                        match = true;
                                    break;

                                case TMRDFilterParams.WITH_CANCELLED_TASKS:
                                    if (empRprt.CancelledTasks > 0)
                                        match = true;
                                    break;
                                case TMRDFilterParams.WITH_COMPLETED_TASK:
                                    if (empRprt.CompletedTasks > 0)
                                        match = true;
                                    break;

                                case TMRDFilterParams.WITH_ONGOING_TASK:
                                    if (empRprt.OnGoingTasks > 0)
                                        match = true;
                                    break;

                                case TMRDFilterParams.WITH_TERMINATED_TASKS:
                                    if (empRprt.TerminatedTasks > 0)
                                        match = true;
                                    break;
                            }
                        });
                    if (match)
                        filteredReport.Add(empRprt);
                });
            return filteredReport;
        }
    }
}
