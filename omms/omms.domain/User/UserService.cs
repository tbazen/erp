﻿using omms.types.Models;
using omms.types.Models.User;
using System;
using System.Collections.Generic;

namespace omms.domain.User
{
    public interface IUserService
    {
        UserSession ValidateCredential(string username, string passwordHash, out string token);
    }
    public class UserService : IUserService
    {
        private readonly data.Entities.OmmsContext _context;
        public UserService(data.Entities.OmmsContext context) => _context = context;

        public UserSession ValidateCredential(string username, string password, out string token)
        {
            token = INTAPS.ClientServer.ApplicationServer.CreateUserSession(username, password, "OMMS");
            var permissions = INTAPS.ClientServer.ApplicationServer.SecurityBDE.GetUserPermissions(INTAPS.ClientServer.ApplicationServer.SecurityBDE.GetUIDForName(username));

            var userID = permissions.UID;

            List<long> roles = new List<long>();

            foreach (RoleType role in Enum.GetValues(typeof(RoleType)))
            {
                if (permissions.IsPermited("root/omms/" + role.ToString()))
                    roles.Add((int)role);
            }

            if (roles.Count <= 0)
                throw new AccessViolationException("User has no OMMS role");

            return new UserSession()
            {
                EmployeeID = userID,
                Username = username,
                CreatedTime = DateTime.Now,
                LastSeen = DateTime.Now,
                Roles = roles.ToArray(),
            };
        }
    }
    public class LoginViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public long Role { get; set; }
    }

}
