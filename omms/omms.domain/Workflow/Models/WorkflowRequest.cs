﻿using System;

namespace omms.domain.Workflow.Models
{
    public enum WorkflowTypes
    {
        Team = 1,
        Asset = 2,
        Task = 3
    }
    public class WorkflowRequest
    {
        public Guid Id { get; set; }
        public int CurrentState { get; set; }
        public WorkflowTypes Type { get; set; }
        public string Description { get; set; }
    }
}
