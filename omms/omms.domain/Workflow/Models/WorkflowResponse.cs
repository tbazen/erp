﻿using System;

namespace omms.domain.Workflow.Models
{
    public class WorkFlowResponse
    {
        public WorkFlowResponse()
        {
            WorkItem = new WorkItemModel();
        }
        public Guid Id { get; set; }
        public int CurrentState { get; set; }
        public string Description { get; set; }
        public WorkflowTypes Type { get; set; }
        public WorkItemModel WorkItem { get; set; }
    }
    public class WorkItemModel
    {
        public Guid? Id { get; set; }
        public Guid WFId { get; set; }
        public int FromState { get; set; }
        public int ToState { get; set; }
        public int Trigger { get; set; }
        public string Data { get; set; }
        public int SeqNo { get; set; }
        public string Note { get; set; }
    }
}
