﻿using omms.data.Entities;
using omms.domain.Infrastracture;
using omms.domain.TaskServices.Helper;
using omms.domain.Workflow.services;
using omms.types.Models;
using Stateless;
using System;

namespace omms.domain.Workflow
{
    public class StatelessWorkflowBase<StateType, TriggerType> : OmmsService
    {
        protected StateMachine<StateType, TriggerType> _machine;
        protected WorkflowService _workflowService;

        public StatelessWorkflowBase()
        {
            _workflowService = new WorkflowService();
        }

        protected WorkflowItem fireAction<T>(TriggerType trigger, Guid wfid, T data, UserSession user, string requestNote = "")
        {
            var prevState = _machine.State;
            _machine.Fire(trigger);
            var employee = EmployeeHelper.GetEmployeeByLoginName(user.Username);

            string note = "";

            note = typeof(T) == typeof(TeamModel) ? (data as TeamModel)?.Note ?? "" : note;
            note = typeof(T) == typeof(AssigneeViewModel) ? (data as AssigneeViewModel)?.Note ?? "" : note;
            note = typeof(T) == typeof(TaskRequest) ? (data as TaskRequest)?.Note ?? "" : note;

            note = String.IsNullOrWhiteSpace(requestNote) ? note : requestNote;

            string wfiData = null;
            wfiData = data == null ? wfiData : Newtonsoft.Json.JsonConvert.SerializeObject(data);
            wfiData = typeof(T) == typeof(AssigneeViewModel)
                      ?
                      Newtonsoft.Json.JsonConvert.SerializeObject((data as AssigneeViewModel).Assignee)
                      :
                      wfiData;
            WorkflowItem item = new WorkflowItem
            {
                Id = Guid.NewGuid(),
                WorkflowId = wfid,
                Data = wfiData,
                FromState = Convert.ToInt32(prevState),
                ToState = Convert.ToInt32(_machine.State),
                Trigger = Convert.ToInt32(trigger),
                Note = note,
                ActivityDate = DateTime.Now,
                EmployeeId = employee.Id,
                Role = user.Role
            };
            return _workflowService.CreateWorkItemStateChange(item);
        }
    }
}
