﻿using omms.domain.Documents;
using omms.domain.TaskServices;
using omms.domain.Workflow.Models;
using omms.types.Models;
using System;
using System.Collections.Generic;

namespace omms.domain.Workflow.services
{
    public enum AssetState
    {
        Initial = 1,
        RegistrationRequested = 2,
        RegistrationApprived = 3,
        RegistrationRejected = 4
    }
    public enum AssetTrigger
    {
        RegistrationRequest = 1,
        UpdateRequest = 2,
        Approve = 3,
        Reject = 4
    }
    public class AssetWorkflow : StatelessWorkflowBase<AssetState, AssetTrigger>
    {
        omms.data.Entities.Workflow _workflow;
        AssetService _assetService;
        public AssetWorkflow(AssetService service)
        {
            _assetService = service;
        }
        void ConfigureMachine()
        {
            _machine = new Stateless.StateMachine<AssetState, AssetTrigger>((AssetState)_workflow.CurrentState);
            DefineMachine();
        }
        void DefineMachine()
        {
            _machine.Configure(AssetState.Initial)
                .Permit(AssetTrigger.RegistrationRequest, AssetState.RegistrationRequested);

            _machine.Configure(AssetState.RegistrationRequested)
                .PermitReentry(AssetTrigger.UpdateRequest)
                .Permit(AssetTrigger.Approve, AssetState.RegistrationApprived)
                .Permit(AssetTrigger.Reject, AssetState.RegistrationRejected);

            _machine.Configure(AssetState.RegistrationRejected)
                .Permit(AssetTrigger.UpdateRequest, AssetState.RegistrationRequested);
        }
        public Guid RequestAssetRegistration(AssetRequest asset, Guid? wfid)
        {
            AssetTrigger trigger;
            _workflowService.SetContext(Context);
            if (wfid == null)
            {
                _workflow = _workflowService.CreateWorkFlow(new Models.WorkflowRequest
                {
                    Type = Models.WorkflowTypes.Asset,
                    CurrentState = (int)TeamState.Initial,
                    Description = "Initial Asset registration Request"
                });
                asset.Note = String.IsNullOrWhiteSpace(asset.Note) ? "Initial asset registration request " : asset.Note;
                trigger = AssetTrigger.RegistrationRequest;
            }
            else
            {
                _workflow = _workflowService.GetWorkFlow((Guid)wfid);
                _workflow.Description = "Updated asset registration request";
                Context.Update(_workflow);
                Context.SaveChanges();
                trigger = AssetTrigger.UpdateRequest;
            }
            asset.Wfid = _workflow.Id;
            ConfigureMachine();
            return fireAction<AssetRequest>(trigger, _workflow.Id, asset, GetSession()).Id;
        }
        public Guid ApproveAssetRegistration(Guid wfid)
        {
            _workflowService.SetContext(Context);
            _workflow = _workflowService.GetWorkFlow(wfid);

            ConfigureMachine();
            var asset = _workflowService.GetLatestWorkItem<AssetRequest>(wfid);
            if (asset != null)
                _assetService.RegisterAsset(asset);
            return fireAction<AssetRequest>(AssetTrigger.Approve, wfid, null, GetSession()).Id;
        }
        public Guid RejectAssetRegistration(WFRequestModel request)
        {
            _workflowService.SetContext(Context);
            _workflow = _workflowService.GetWorkFlow(request.Wfid);
            ConfigureMachine();
            var asset = _workflowService.GetLatestWorkItem<AssetRequest>(request.Wfid);
            asset.Note = request.Note;
            return fireAction<AssetRequest>(AssetTrigger.Reject, request.Wfid, asset, GetSession()).Id;
        }
        public List<AssetResponse> GetPendingAssets()
        {
            List<AssetResponse> assets = new List<AssetResponse>();
            _workflowService.SetContext(Context);
            DocumentService documentService = new DocumentService();
            documentService.SetContext(Context);
            var assetItems = _workflowService.GetPendingItems((int)WorkflowTypes.Asset);
            assetItems.ForEach(item =>
            {
                if (item.WorkItem?.Data != null)
                {
                    AssetRequest req = Newtonsoft.Json.JsonConvert.DeserializeObject<AssetRequest>(item?.WorkItem.Data);
                    AssetResponse response = new AssetResponse
                    {
                        Wfid = item.Id,
                        Id = req.Id,
                        AssetType = _assetService.GetAssetInfoByPUKID(
                            new AssetSearchParams
                            {
                                PkuId = req.PkuId,
                                AssetType = Context.AssetType.Find(req.TypeId).Name
                            }),
                        GeoLocation = req.GeoLocation,
                        PkuId = req.PkuId,
                        Description = req.Description,
                        Name = req.Name,
                        Note = req.Note
                    };
                    req.Images?.ForEach(img =>
                    {
                        var image = documentService.GetDocumentResponse((Guid)img.Id);
                        response.Images.Add(image);
                    });
                    assets.Add(response);
                }
            });
            return assets;
        }
        public List<AssetResponse> GetRejectedAssets()
        {
            List<AssetResponse> assets = new List<AssetResponse>();
            _workflowService.SetContext(Context);
            DocumentService documentService = new DocumentService();
            documentService.SetContext(Context);
            var assetItems = _workflowService.GetRejectedItems((int)WorkflowTypes.Asset);
            assetItems.ForEach(item =>
            {
                if (item.WorkItem?.Data != null)
                {
                    AssetRequest req = Newtonsoft.Json.JsonConvert.DeserializeObject<AssetRequest>(item?.WorkItem.Data);
                    AssetResponse response = new AssetResponse
                    {
                        Wfid = item.Id,
                        Id = req.Id,
                        AssetType = _assetService.GetAssetInfoByPUKID(
                            new AssetSearchParams
                            {
                                PkuId = req.PkuId,
                                AssetType = Context.AssetType.Find(req.TypeId).Name
                            }),
                        GeoLocation = req.GeoLocation,
                        Description = req.Description,
                        PkuId = req.PkuId,
                        Name = req.Name,
                        Note = req.Note
                    };
                    req.Images?.ForEach(img =>
                    {
                        var image = documentService.GetDocumentResponse((Guid)img.Id);
                        response.Images.Add(image);
                    });
                    assets.Add(response);
                }
            });
            return assets;
        }
    }
}