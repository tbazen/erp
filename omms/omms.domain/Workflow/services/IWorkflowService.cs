﻿using omms.data.Entities;
using omms.domain.Workflow.Models;
using System;

namespace omms.domain.Workflow.services
{
    public interface IWorkflowService
    {
        WorkflowItem CreateWorkItemStateChange(WorkflowItem request);
        omms.data.Entities.Workflow CreateWorkFlow(WorkflowRequest request);
        omms.data.Entities.Workflow GetWorkFlow(Guid wfid);
        T GetLatestWorkItem<T>(Guid wfid) where T : class;

        // List<WorkFlowResponse<>> GetPendingItems(int type);

        int NumberOfPendingItems();
        int NumberOfRejectedItems();

    }
}
