﻿using omms.domain.Documents;
using omms.domain.TaskServices;
using omms.domain.TaskServices.Helper;
using omms.domain.Workflow.Models;
using omms.types.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace omms.domain.Workflow.services
{
    public enum TaskState
    {
        Initial = 1,
        RegistrationRequested = 2,
        RegistrationApprived = 3,
        RegistrationRejected = 4,


        CancelRequested = 5,
        TerminateRequsted = 6,
        CompleteRequested = 7,

        CancelRejected = 8,
        TerminateRejected = 9,
        CompletedRejected = 10,

        Cancelled = 11,
        Terminated = 12,
        Completed = 13,

        AssigneeModificationRequested = 14,
        AssigneeModificationRejected = 15
    }
    public enum TaskTrigger
    {
        RegistrationRequest = 1,
        UpdateRequest = 2,
        Approve = 3,
        Reject = 4,

        RequestCancel = 5,
        RequestTerminate = 6,
        RequestComplete = 7,
        Cancel = 8,

        RequestAssigneeModification = 9

    }
    public class TaskWorkflow : StatelessWorkflowBase<TaskState, TaskTrigger>
    {
        data.Entities.Workflow _workflow;
        TaskService _taskService;
        public TaskWorkflow(TaskService service)
        {
            _taskService = service;
        }
        void ConfigureMachine()
        {
            _machine = new Stateless.StateMachine<TaskState, TaskTrigger>((TaskState)_workflow.CurrentState);
            DefineMachine();
        }
        void DefineMachine()
        {
            _machine.Configure(TaskState.Initial)
                .Permit(TaskTrigger.RegistrationRequest, TaskState.RegistrationRequested);

            _machine.Configure(TaskState.RegistrationRequested)
                .PermitReentry(TaskTrigger.UpdateRequest)
                .Permit(TaskTrigger.Approve, TaskState.RegistrationApprived)
                .Permit(TaskTrigger.Reject, TaskState.RegistrationRejected);

            _machine.Configure(TaskState.RegistrationRejected)
                .Permit(TaskTrigger.UpdateRequest, TaskState.RegistrationRequested)
                .Permit(TaskTrigger.RequestAssigneeModification, TaskState.AssigneeModificationRequested);


            _machine.Configure(TaskState.RegistrationApprived)
                .Permit(TaskTrigger.RequestAssigneeModification, TaskState.AssigneeModificationRequested)
                .Permit(TaskTrigger.RequestCancel, TaskState.CancelRequested)
                .Permit(TaskTrigger.RequestComplete, TaskState.CompleteRequested)
                .Permit(TaskTrigger.RequestTerminate, TaskState.TerminateRequsted);



            _machine.Configure(TaskState.CancelRequested)
                .Permit(TaskTrigger.Reject, TaskState.RegistrationApprived);
            _machine.Configure(TaskState.CompleteRequested)
                .Permit(TaskTrigger.Reject, TaskState.RegistrationApprived);
            _machine.Configure(TaskState.TerminateRequsted)
                            .Permit(TaskTrigger.Reject, TaskState.RegistrationApprived);



            _machine.Configure(TaskState.CancelRequested)
                .Permit(TaskTrigger.Approve, TaskState.Cancelled);
            _machine.Configure(TaskState.TerminateRequsted)
                .Permit(TaskTrigger.Approve, TaskState.Terminated);
            _machine.Configure(TaskState.CompleteRequested)
                            .Permit(TaskTrigger.Approve, TaskState.Completed);


            _machine.Configure(TaskState.AssigneeModificationRequested)
                .PermitReentry(TaskTrigger.RequestAssigneeModification)
                .Permit(TaskTrigger.Approve, TaskState.RegistrationApprived)
                .Permit(TaskTrigger.Reject, TaskState.RegistrationApprived);

            _machine.Configure(TaskState.AssigneeModificationRejected)
                .Permit(TaskTrigger.UpdateRequest, TaskState.AssigneeModificationRequested)
                .Permit(TaskTrigger.Cancel, TaskState.RegistrationApprived);
        }
        public Guid RequestTaskRegistration(TaskRequest task, Guid? wfid)
        {
            TaskTrigger trigger;
            string loginName = _taskService.GetSession()?.Username;
            string creatorId = EmployeeHelper.GetEmployeeByLoginName(loginName).Id;
            task.CreatedBy = creatorId;
            _workflowService.SetContext(Context);
            if (wfid == null)
            {
                _workflow = _workflowService.CreateWorkFlow(new Models.WorkflowRequest
                {
                    Type = Models.WorkflowTypes.Task,
                    CurrentState = (int)TeamState.Initial,
                    Description = "Initial Task Registration Request"
                });
                trigger = TaskTrigger.RegistrationRequest;
                task.Note = "Initail Task Registratiion";
            }
            else
            {
                _workflow = _workflowService.GetWorkFlow((Guid)wfid);
                _workflow.Description = "Updated task registration request";
                _workflow.StateDate = DateTime.Now;
                Context.Update(_workflow);
                Context.SaveChanges();
                trigger = TaskTrigger.UpdateRequest;
            }

            ConfigureMachine();
            task.Wfid = _workflow.Id;
            return fireAction<TaskRequest>(trigger, _workflow.Id, task, GetSession()).Id;
        }
        public Guid RequestAssegneeModification(AssigneeViewModel newAssignee)
        {
            TaskTrigger trigger;
            _workflowService.SetContext(Context);
            _workflow = _workflowService.GetWorkFlow(newAssignee.Wfid);
            _workflow.Description = "Updated task assignee";
            _workflow.StateDate = DateTime.Now;
            Context.Update(_workflow);
            Context.SaveChanges();
            trigger = TaskTrigger.RequestAssigneeModification;
            ConfigureMachine();
            return fireAction<AssigneeViewModel>(trigger, _workflow.Id, newAssignee, GetSession()).Id;
        }
        public Guid RequestTaskCancellation(WFRequestModel request)
        {
            TaskTrigger trigger;
            _workflowService.SetContext(Context);
            _workflow = _workflowService.GetWorkFlow(request.Wfid);
            _workflow.Description = "Task cancellation request";
            Context.Update(_workflow);
            Context.SaveChanges();
            trigger = TaskTrigger.RequestCancel;
            ConfigureMachine();
            return fireAction<TaskRequest>(trigger, _workflow.Id, null, GetSession(), request.Note).Id;
        }
        public Guid RequestTaskTermination(WFRequestModel request)
        {
            TaskTrigger trigger;
            _workflowService.SetContext(Context);
            _workflow = _workflowService.GetWorkFlow(request.Wfid);
            _workflow.Description = "Task termination request";
            Context.Update(_workflow);
            Context.SaveChanges();
            trigger = TaskTrigger.RequestTerminate;

            ConfigureMachine();
            return fireAction<TaskRequest>(trigger, _workflow.Id, null, GetSession()).Id;
        }
        public Guid RequestTaskCompletion(WFRequestModel request)
        {
            TaskTrigger trigger;
            _workflowService.SetContext(Context);
            _workflow = _workflowService.GetWorkFlow(request.Wfid);
            _workflow.Description = "Task completion request";
            Context.Update(_workflow);
            Context.SaveChanges();
            trigger = TaskTrigger.RequestComplete;

            ConfigureMachine();
            return fireAction<TaskRequest>(trigger, _workflow.Id, null, GetSession()).Id;
        }
        public Guid ApproveTaskState(Guid wfid)
        {
            _workflowService.SetContext(Context);
            _workflow = _workflowService.GetWorkFlow(wfid);


            if (_workflow.CurrentState == (int)TaskState.AssigneeModificationRequested)
            {
                _workflow.Description = "Update task Assignee";
                var assignee = _workflowService.GetLatestWorkItem<Assignee>(wfid);
                _taskService.UpdateTaskAssignee(assignee, _workflow.Id);

                Context.Workflow.Update(_workflow);
                Context.SaveChanges();
                ConfigureMachine();

                return fireAction<TaskRequest>(TaskTrigger.Approve, wfid, null, GetSession()).Id;
            }

            _taskService.CheckDependency(wfid);
            var task = Context.Task.FirstOrDefault(tsk => tsk.Wfid == wfid);
            if (task == null)
                throw new Exception("Task not found!");
            ConfigureMachine();
            Guid approvedId = fireAction<TaskRequest>(TaskTrigger.Approve, wfid, null, GetSession()).Id;
            if (task.Type == (int)TaskType.Periodic)
                _taskService.RestartPeriodicTask(task.Id);
            if (task.Type == (int)TaskType.Scheduled)
                _taskService.UpdateEndDate(task.Id);
            return approvedId;
        }
        public Guid RejectTaskState(WFRequestModel request)
        {
            _workflowService.SetContext(Context);
            _workflow = _workflowService.GetWorkFlow(request.Wfid);
            ConfigureMachine();
            return fireAction<TaskRequest>(TaskTrigger.Reject, request.Wfid, null, GetSession(), request.Note).Id;
        }
        public Guid ApproverTaskRegistration(Guid wfid)
        {
            _workflowService.SetContext(Context);
            _workflow = _workflowService.GetWorkFlow(wfid);
            _workflow.Description = "Ongoing task";

            if (_workflow.CurrentState == (int)TaskState.AssigneeModificationRequested)
            {
                _workflow.Description = "Update task Assignee";
                var assignee = _workflowService.GetLatestWorkItem<Assignee>(wfid);
                _taskService.UpdateTaskAssignee(assignee, _workflow.Id);

                Context.Workflow.Update(_workflow);
                Context.SaveChanges();
                ConfigureMachine();

                return fireAction<TaskRequest>(TaskTrigger.Approve, wfid, null, GetSession()).Id;
            }
            Context.Workflow.Update(_workflow);
            Context.SaveChanges();
            ConfigureMachine();
            var task = _workflowService.GetLatestWorkItem<TaskRequest>(wfid);
            task.Wfid = wfid;
            if (task != null)
                _taskService.CreateTask(task);
            return fireAction<TaskRequest>(TaskTrigger.Approve, wfid, null, GetSession()).Id;
        }
        public Guid RejectTaskRegistration(WFRequestModel request)
        {
            _workflowService.SetContext(Context);
            _workflow = _workflowService.GetWorkFlow(request.Wfid);
            _workflow.Description = "Registration rejected";
            Context.Workflow.Update(_workflow);
            Context.SaveChanges();

            ConfigureMachine();
            var task = _workflowService.GetLatestWorkItem<TaskRequest>(request.Wfid);
            return fireAction<TaskRequest>(TaskTrigger.Reject, request.Wfid, task, GetSession()).Id;
        }
        public List<TaskResponse> GetPendingTasks()
        {
            List<TaskResponse> tasks = new List<TaskResponse>();
            _workflowService.SetContext(Context);
            DocumentService documentService = new DocumentService();
            documentService.SetContext(Context);
            var taskItems = _workflowService.GetPendingItems((int)WorkflowTypes.Task);
            taskItems.ForEach(item =>
            {
                if (item.WorkItem?.Data != null)
                {
                    TaskRequest request = Newtonsoft.Json.JsonConvert.DeserializeObject<TaskRequest>(item?.WorkItem.Data);
                    if (GetSession().Role != (int)types.Models.User.RoleType.TecnnicalSupervisor &&
                         request.CreatedBy == EmployeeHelper.GetEmployeeByLoginName(GetSession().Username).Id)
                        tasks.Add(TransformRequst(request));
                    else
                        tasks.Add(TransformRequst(request));
                }
            });

            tasks.AddRange(_taskService.GetWaitingStateTasks());
            return tasks;
        }
        internal TaskResponse TransformRequst(TaskRequest request)
        {
            DocumentService documentService = new DocumentService();
            documentService.SetContext(Context);
            TaskResponse response = new TaskResponse
            {
                Id = request.Id,
                AssetId = request.AssetId,
                CreatedBy = EmployeeHelper.GetEmployeeById(request.CreatedBy),
                DependUpon = request.DependUpon,
                Description = request.Description,
                EndDateActual = request.EndDateActual,
                EndDateExpected = request.EndDateExpected,
                IntervalDays = request.IntervalDays,
                MaintenanceType = request.MaintenanceType,
                Name = request.Name,
                Wfid = request.Wfid,
                Priority = request.Priority,
                StartDateActual = request.StartDateActual,
                StartDateExpected = request.StartDateExpected,
                TaskAssignee = request.TaskAssignee,
                TaskType = request.TaskType,
            };

            response.Note = _workflowService.GetLatestWorkItemObject((Guid)response.Wfid)?.Note ?? "";
            TaskState state = (TaskState)Context.Workflow.Find(response.Wfid)?.CurrentState;
            switch (state)
            {
                case TaskState.CancelRejected:
                    response.State = "Cancel Request Rejected";
                    break;
                case TaskState.RegistrationRequested:
                    response.State = "Waiting registration approval";
                    break;
                case TaskState.Cancelled:
                    response.State = "Cancelled";
                    break;


                case TaskState.CancelRequested:
                    response.State = "Waiting cancel approval";
                    break;


                case TaskState.Completed:
                    response.State = "Completed";
                    break;

                case TaskState.CompletedRejected:
                    response.State = "Completed request rejected";
                    break;

                case TaskState.CompleteRequested:
                    response.State = "Waiting Completion approval";
                    break;


                case TaskState.RegistrationApprived:
                    response.State = "On going task";
                    break;


                case TaskState.Terminated:
                    response.State = "Terminated";
                    break;
                case TaskState.TerminateRejected:
                    response.State = "Termination request rejected";
                    break;
                case TaskState.TerminateRequsted:
                    response.State = "Waiting Termination Approval";
                    break;
            }


            if (request.WarrantyDocument != null)
                response.WarrantyDocument = documentService.GetDocumentResponse((Guid)request.WarrantyDocument.Id);
            if (request.TaskType == TaskType.Incident)
            {
                response.IncidentInfo.Incident = request.IncidentInfo.Incident;
                response.IncidentInfo.ReportedBy = request.IncidentInfo.ReportedBy;
                response.IncidentInfo.GeoLocation = request.IncidentInfo.GeoLocation;
                response.IncidentInfo.Description = request.IncidentInfo.Description;

                request.IncidentInfo.Images.ForEach(
                    img =>
                    {
                        response.IncidentInfo.Images.Add(documentService.GetDocumentResponse((Guid)img.Id));
                    });
            }
            return response;
        }
        public List<TaskResponse> GetRejectedTasks()
        {
            List<TaskResponse> tasks = new List<TaskResponse>();
            _workflowService.SetContext(Context);
            DocumentService documentService = new DocumentService();
            documentService.SetContext(Context);
            var taskItems = _workflowService.GetRejectedItems((int)WorkflowTypes.Task);
            taskItems.ForEach(item =>
            {
                if (item.WorkItem?.Data != null)
                {
                    TaskRequest req = Newtonsoft.Json.JsonConvert.DeserializeObject<TaskRequest>(item?.WorkItem.Data);
                    if (GetSession().Role != (int)types.Models.User.RoleType.TecnnicalSupervisor &&
                        req.CreatedBy == EmployeeHelper.GetEmployeeByLoginName(GetSession().Username).Id)
                        tasks.Add(TransformRequst(req));
                    else
                        tasks.Add(TransformRequst(req));
                }
            });
            tasks.AddRange(_taskService.GetTasksByState((int)TaskState.AssigneeModificationRejected));
            return tasks;
        }
    }
}
