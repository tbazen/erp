﻿using omms.domain.TaskServices;
using omms.domain.Workflow.Models;
using omms.types.Models;
using System;
using System.Collections.Generic;

namespace omms.domain.Workflow.services
{
    public enum TeamState
    {
        Initial = 1,
        RegistrationRequested = 2,
        RegistrationApprived = 3,
        RegistrationRejected = 4,
        ModificationRequested = 5,
        ModificationRejected = 6
    }
    public enum Trigger
    {
        RegistrationRequest = 1,
        UpdateRequest = 2,
        Approve = 3,
        Reject = 4,
        CancelRequest = 5
    }
    public class TeamWorkflow : StatelessWorkflowBase<TeamState, Trigger>
    {
        omms.data.Entities.Workflow _workflow;
        readonly TeamService _teamService;


        public TeamWorkflow(TeamService tmSvr)
        {
            _teamService = tmSvr;
        }
        void ConfigureMachine()
        {
            _machine = new Stateless.StateMachine<TeamState, Trigger>((TeamState)_workflow.CurrentState);
            DefineMachine();
        }
        void DefineMachine()
        {
            _machine.Configure(TeamState.Initial)
                .Permit(Trigger.RegistrationRequest, TeamState.RegistrationRequested);

            _machine.Configure(TeamState.RegistrationRequested)
                .PermitReentry(Trigger.UpdateRequest)
                .Permit(Trigger.Approve, TeamState.RegistrationApprived)
                .Permit(Trigger.Reject, TeamState.RegistrationRejected);

            _machine.Configure(TeamState.RegistrationRejected)
                .Permit(Trigger.UpdateRequest, TeamState.RegistrationRequested);

            _machine.Configure(TeamState.RegistrationApprived)
                .Permit(Trigger.UpdateRequest, TeamState.ModificationRequested);

            _machine.Configure(TeamState.ModificationRequested)
                .PermitReentry(Trigger.UpdateRequest)
                .Permit(Trigger.Approve, TeamState.RegistrationApprived)
                .Permit(Trigger.Reject, TeamState.ModificationRejected)
                .Permit(Trigger.CancelRequest, TeamState.RegistrationApprived);

            _machine.Configure(TeamState.ModificationRejected)
                .Permit(Trigger.UpdateRequest, TeamState.ModificationRequested)
                .Permit(Trigger.CancelRequest, TeamState.RegistrationApprived);
        }

        public Guid RequestTeamRegistration(TeamModel team, Guid? wfid)
        {
            _workflowService.SetContext(Context);
            Trigger trigger;

            if (wfid == null)
            {
                _workflow = _workflowService.CreateWorkFlow(new Models.WorkflowRequest
                {
                    Type = WorkflowTypes.Team,
                    CurrentState = (int)TeamState.Initial,
                    Description = "Initial Team registration Request"
                });
                team.Note = "Initial Team Registration";
                team.Wfid = _workflow.Id;
                trigger = Trigger.RegistrationRequest;
            }
            else
            {
                _workflow = _workflowService.GetWorkFlow((Guid)wfid);
                _workflow.Description = "Updated team registration request";
                Context.Update(_workflow);
                Context.SaveChanges();
                trigger = Trigger.UpdateRequest;
            }

            ConfigureMachine();
            return fireAction<TeamModel>(trigger, _workflow.Id, team, GetSession()).Id;
        }
        public Guid ApproverTeamRegistration(Guid wfid)
        {
            _workflowService.SetContext(Context);
            _workflow = _workflowService.GetWorkFlow(wfid);
            ConfigureMachine();
            var team = _workflowService.GetLatestWorkItem<TeamModel>(wfid);
            if (team != null)
                _teamService.CreateTeam(team);
            return fireAction<TeamModel>(Trigger.Approve, wfid, null, GetSession()).Id;
        }
        public Guid RejectTeamRegistration(WFRequestModel request)
        {
            _workflowService.SetContext(Context);
            _workflow = _workflowService.GetWorkFlow(request.Wfid);
            ConfigureMachine();
            var team = _workflowService.GetLatestWorkItem<TeamModel>(request.Wfid);
            team.Note = request.Note;
            return fireAction<TeamModel>(Trigger.Reject, request.Wfid, team, GetSession()).Id;
        }

        public List<TeamModel> GetPendingTeams()
        {
            List<TeamModel> teams = new List<TeamModel>();
            _workflowService.SetContext(Context);
            //waitong for registration .approval

            var teamItems = _workflowService.GetPendingItems((int)WorkflowTypes.Team);
            teamItems.ForEach(item =>
            {
                if (item.WorkItem?.Data != null)
                {
                    TeamModel response = Newtonsoft.Json.JsonConvert.DeserializeObject<TeamModel>(item?.WorkItem.Data);
                    response.Wfid = item.Id;
                    teams.Add(response);
                }
            });

            //waitong for modification approval
            teams.AddRange(_teamService.GetTeamByState(TeamState.ModificationRequested));
            return teams;
        }
        public List<TeamModel> GetRejectedTeams()
        {
            List<TeamModel> teams = new List<TeamModel>();
            _workflowService.SetContext(Context);
            var assetItems = _workflowService.GetRejectedItems((int)WorkflowTypes.Team);
            assetItems.ForEach(item =>
            {
                if (item.WorkItem?.Data != null)
                {
                    TeamModel response = Newtonsoft.Json.JsonConvert.DeserializeObject<TeamModel>(item?.WorkItem.Data);
                    response.Wfid = item.Id;
                    teams.Add(response);
                }
            });

            //team list modification rejected
            teams.AddRange(_teamService.GetTeamByState(TeamState.ModificationRejected));
            return teams;
        }

        internal bool CancelTeamModification(Guid wfid)
        {
            _workflowService.SetContext(Context);
            _workflow = _workflowService.GetWorkFlow(wfid);

            if (_workflow.CurrentState == (int)TeamState.ModificationRejected ||
               _workflow.CurrentState == (int)TeamState.ModificationRequested)
            {
                ConfigureMachine();
                fireAction<TeamModel>(Trigger.CancelRequest, wfid, null, GetSession());
                return true;
            }
            return false;
        }
    }
}
