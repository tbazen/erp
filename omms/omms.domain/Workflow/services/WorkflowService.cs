﻿using omms.data.Entities;
using omms.domain.Infrastracture;
using omms.domain.Workflow.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace omms.domain.Workflow.services
{
    public class WorkflowService : OmmsService, IWorkflowService
    {
        public data.Entities.Workflow CreateWorkFlow(WorkflowRequest request)
        {
            omms.data.Entities.Workflow wf = new data.Entities.Workflow
            {
                Id = Guid.NewGuid(),
                CurrentState = request.CurrentState,
                StateDate = DateTime.Now,
                Description = request.Description,
                TypeId = (int)request.Type
            };
            Context.Workflow.Add(wf);
            Context.SaveChanges();
            return wf;
        }
        public WorkflowItem CreateWorkItemStateChange(WorkflowItem item)
        {
            var wf = Context.Workflow.FirstOrDefault(wfi => wfi.Id == item.WorkflowId);
            item.SeqenceNo = GetSeqNo(item.WorkflowId) + 1;

            Context.WorkflowItem.Add(item);
            Context.SaveChanges();

            wf.CurrentState = item.ToState;
            wf.StateDate = DateTime.Now;
            Context.Workflow.Update(wf);
            Context.SaveChanges();
            return item;
        }

        public int GetSeqNo(Guid wfid)
        {
            var sqnc = Context.WorkflowItem.Where(wi => wi.WorkflowId == wfid);
            return sqnc.Any() ? sqnc.Select(s => s.SeqenceNo).Max() : 0;
        }

        public data.Entities.Workflow GetWorkFlow(Guid wfid)
        {
            return Context.Workflow.Find(wfid);
        }

        public T GetLatestWorkItem<T>(Guid wfid) where T : class
        {
            var seqNo = GetSeqNo(wfid);
            if (seqNo == 0)
                return null;
            var item = Context.WorkflowItem.FirstOrDefault(itm => itm.WorkflowId == wfid && itm.SeqenceNo == seqNo);
            if (item.Data == null)
                return null;
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(item.Data);
        }

        public WorkflowItem GetLatestWorkItemObject(Guid wfid)
        {
            var seqNo = GetSeqNo(wfid);
            if (seqNo == 0)
                return null;
            var item = Context.WorkflowItem.FirstOrDefault(itm => itm.WorkflowId == wfid && itm.SeqenceNo == seqNo);
            return item;
        }

        public WorkItemModel GetWorkItem(Guid wfid)
        {
            var item = Context.WorkflowItem
                .OrderByDescending(wi => wi.SeqenceNo)
                .Where(wi => wi.WorkflowId == wfid)
                .FirstOrDefault();

            return item == null ? null : new WorkItemModel
            {
                Id = item.Id,
                WFId = item.WorkflowId,
                FromState = item.FromState,
                ToState = item.ToState,
                Trigger = item.Trigger,
                Data = item.Data,
                SeqNo = item.SeqenceNo
            };
        }

        public WorkFlowResponse GetWorkFlowResponse(Guid wfid)
        {
            var wf = Context.Workflow.FirstOrDefault(w => w.Id == wfid);
            return wf == null ?
                null
                :
                new WorkFlowResponse
                {
                    Id = wf.Id,
                    CurrentState = wf.CurrentState,
                    Description = wf.Description,
                    Type = (WorkflowTypes)wf.TypeId,
                    WorkItem = GetWorkItem(wf.Id)
                };
        }

        public List<WorkFlowResponse> GetPendingItems(int type)
        {
            //getting waitong for registration
            List<WorkFlowResponse> wfList = new List<WorkFlowResponse>();
            var wfs = Context.Workflow.Where(
                wf => wf.TypeId == type &&
                wf.CurrentState == (int)TeamState.RegistrationRequested)
                .OrderByDescending(wf => wf.StateDate)
                .ToList();
            wfs.ForEach(
                wf => wfList.Add(GetWorkFlowResponse(wf.Id)));

            return wfList;
        }


        public List<WorkFlowResponse> GetRejectedItems(int type)
        {
            if (type == (int)WorkflowTypes.Team)
            {
                List<WorkFlowResponse> wfList = new List<WorkFlowResponse>();
                var wfs = Context.Workflow.Where(
                    wf => wf.TypeId == type &&
                    wf.CurrentState == (int)TeamState.RegistrationRejected
                    )
                    .ToList();
                wfs.ForEach(
                    wf => wfList.Add(GetWorkFlowResponse(wf.Id)));
                return wfList;
            }
            else
            {
                List<WorkFlowResponse> wfList = new List<WorkFlowResponse>();
                var wfs = Context.Workflow.Where(
                    wf => wf.TypeId == type &&
                    wf.CurrentState == (int)TeamState.RegistrationRejected)
                    .ToList();
                wfs.ForEach(
                    wf => wfList.Add(GetWorkFlowResponse(wf.Id)));
                return wfList;
            }
        }

        internal bool CancelRegistrationRequest(Guid wfid)
        {
            var wf = Context.Workflow.Find(wfid);
            Context.Workflow.Remove(wf);
            Context.SaveChanges();
            return true;
        }


        #region metadata services
        public int NumberOfPendingItems()
        {
            var employee = GetSession();
            int pendginTasks = Context.Workflow
                                .Count(
                                wf =>
                                wf.TypeId == (int)WorkflowTypes.Task &&
                                (
                                    wf.CurrentState == (int)TaskState.CancelRequested ||
                                    wf.CurrentState == (int)TaskState.CompleteRequested ||
                                    wf.CurrentState == (int)TaskState.TerminateRequsted ||
                                    wf.CurrentState == (int)TaskState.RegistrationRequested ||
                                    wf.CurrentState == (int)TaskState.AssigneeModificationRequested
                                ));
            if (employee.Role == (int)types.Models.User.RoleType.Employee)
                return pendginTasks;

            int pendginAssets = Context.Workflow
                .Count(
                wf =>
                wf.TypeId == (int)WorkflowTypes.Asset &&
                (
                    wf.CurrentState == (int)AssetState.RegistrationRequested
                ));

            int pendginTeams = Context.Workflow
                .Count(
                wf =>
                wf.TypeId == (int)WorkflowTypes.Team &&
                (
                    wf.CurrentState == (int)TeamState.RegistrationRequested ||
                    wf.CurrentState == (int)TeamState.ModificationRequested
                ));

            int pendingItems = pendginAssets + pendginTasks + pendginTeams;
            return pendingItems;
        }

        public int NumberOfRejectedItems()
        {
            int rejectedTasks = Context.Workflow
                .Count(
                wf =>
                wf.TypeId == (int)WorkflowTypes.Task &&
                (
                    wf.CurrentState == (int)TaskState.RegistrationRejected ||
                    wf.CurrentState == (int)TaskState.AssigneeModificationRejected
                ));

            int rejectedAssets = Context.Workflow
                .Count(
                wf =>
                wf.TypeId == (int)WorkflowTypes.Asset &&
                (
                    wf.CurrentState == (int)AssetState.RegistrationRejected
                ));

            int rejectedTeams = Context.Workflow
                .Count(
                wf =>
                wf.TypeId == (int)WorkflowTypes.Team &&
                (
                    wf.CurrentState == (int)TeamState.RegistrationRejected ||
                    wf.CurrentState == (int)TeamState.ModificationRejected
                ));

            int rejectedItems = rejectedAssets + rejectedTasks + rejectedTeams;
            return rejectedItems;
        }
        #endregion
    }
}
