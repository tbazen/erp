﻿using omms.types.Models.User;
using System;
using System.Collections.Generic;

namespace omms.types.Models
{
    public class AssetModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string GeoLocation { get; set; }
    }

    public class AssetRequest
    {
        public AssetRequest()
        {
            Images = new List<Document.DocumentRequest>();
        }
        public int? Id { get; set; }
        public Guid? Wfid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string GeoLocation { get; set; }
        public int TypeId { get; set; }
        public int PkuId { get; set; }
        public string Note { get; set; }
        public List<Document.DocumentRequest> Images { get; set; }

    }

    public class AssetResponse
    {
        public AssetResponse()
        {
            Images = new List<Document.DocumentResponse>();
        }
        public int? Id { get; set; }
        public Guid? Wfid { get; set; }
        public long PkuId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public string GeoLocation { get; set; }
        public AssetType AssetType { get; set; }
        public string Note { get; set; }
        public List<Document.DocumentResponse> Images { get; set; }
    }
    public class AssetFormSchema
    {
        public AssetFormSchema()
        {
            AssetTypes = new List<AssetType>();
        }
        public List<AssetType> AssetTypes { get; set; }
    }

    public class AssetReadingSchema
    {
        public AssetReadingSchema()
        {
            Reading = new AssetType();
        }
        public AssetType Reading { get; set; }
    }

    public class AssetType
    {
        public AssetType()
        {
            Attributes = new List<Schema>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Schema> Attributes { get; set; }

    }



    public class Schema
    {
        public int Id { get; set; }
        public string AttributeName { get; set; }
        public string ValueType { get; set; }
        public int Priority { get; set; }
        public string Value { get; set; }
    }

    public class AssetReading
    {
        public AssetReading()
        {
            Readings = new List<Schema>();
        }
        public int? Id { get; set; }
        public AssetResponse Asset { get; set; }
        public DateTime? DateRecorded { get; set; }

        public UserModel ReadingBy { get; set; }
        public List<Schema> Readings { get; set; }
    }
}
