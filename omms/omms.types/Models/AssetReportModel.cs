﻿namespace omms.types.Models
{
    public class AssetReportModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string AssetType { get; set; }
        public int OnGoingTasks { get; set; }
        public int CompletedTasks { get; set; }
        public int CancelledTasks { get; set; }
        public int TerminatedTasks { get; set; }
        public int TotalTasks { get; set; }
    }

    public class AssetReport
    {
        public static readonly ReportDocumentMetaData Metadata = new ReportDocumentMetaData(title: "Asset Report", header: "Asset Report");

        public const string Id = "Id";
        public const string Name = "Name";
        public const string AssetType = "Asset Type";

        public const string OnGoingTasks = "Ongoing Tasks";
        public const string CompletedTasks = "Completed Tasks";
        public const string CancelledTasks = "Cancelled Tasks";
        public const string TerminatedTasks = "Terminated Tasks";
        public const string TotalTasks = "Total Tasks";
    }
}
