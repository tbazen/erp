﻿namespace omms.types.Models
{
    public class AssetSearchParams
    {
        public long PkuId { get; set; }
        public string AssetType { get; set; }
    }
}
