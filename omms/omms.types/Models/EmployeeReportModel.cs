﻿namespace omms.types.Models
{
    public class EmployeeReportModel
    {
        public string EmployeeId { get; set; }
        public string Name { get; set; }

        public int OnGoingTasks { get; set; }
        public int CompletedTasks { get; set; }
        public int CancelledTasks { get; set; }
        public int TerminatedTasks { get; set; }
        public int TotalTasks { get; set; }
    }


    public enum ERDFilterParams
    {
        UNASSIGNED_EMPLOYEES = 0,
        ASSIGNED_EMPLYEES = 1,

        WITH_ONGOING_TASK = 2,
        WITH_COMPLETED_TASK = 3,
        WITH_CANCELLED_TASKS = 4,
        WITH_TERMINATED_TASKS = 5
    }

    public class EmployeeReport
    {

        public static readonly ReportDocumentMetaData Metadata = new ReportDocumentMetaData(title: "Employee Report", header: "Employee Report");

        public const string EmployeeId = "Id";
        public const string Name = "Name";
        public const string OnGoingTasks = "Ongoing Tasks";
        public const string CompletedTasks = "Completed Tasks";
        public const string CancelledTasks = "Cancelled Tasks";
        public const string TerminatedTasks = "Terminated Tasks";
        public const string TotalTasks = "Total Tasks";

    }
}
