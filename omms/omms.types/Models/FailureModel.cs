﻿using omms.types.Models.Document;
using System;

namespace omms.types.Models
{
    public class FailureModel
    {
        public Guid? FailureId { get; set; }
        public string Name { get; set; }
        public string FailureDescription { get; set; }
        public int StatusId { get; set; }
        public DateTime DateOccured { get; set; }
        public MaintenanceProc MaintenanceProc { get; set; }
    }

    public class MaintenanceProc
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DocumentResponse ProcedureDocument { get; set; }
        public DocumentRequest ProcedureDocumentReq { get; set; }
    }
}
