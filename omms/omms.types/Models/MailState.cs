﻿namespace omms.types.Models
{
    public enum MailState
    {
        NotSent = 1,
        Sent = 2,
        Failed = 3
    }
}
