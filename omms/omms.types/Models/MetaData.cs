﻿namespace omms.types.Models
{
    public class MetaData
    {
        public int PendingItems { get; set; }
        public int RejectedItems { get; set; }
        public int NewIndividualTasks { get; set; }
        public int NewTeamTasks { get; set; }
    }
}
