﻿using System.Collections.Generic;

namespace omms.types.Models
{
    public class Node<T>
    {
        public Node(T data)
        {
            Data = data;
            Children = new List<Node<T>>();
        }
        public Node(T data, List<Node<T>> children)
        {
            Data = data;
            Children = children;
        }
        public T Data { get; set; }
        public List<Node<T>> Children { get; set; }
    }
}
