﻿using omms.types.Models.User;
using System;
using System.Collections.Generic;

namespace omms.types.Models
{
    public class NoteRequest
    {
        public int? Id { get; set; }
        public Guid TaskId { get; set; }
        public string Header { get; set; }
        public string Note { get; set; }
        public DateTime? Date { get; set; }
        public string NoteBy { get; set; }

        public List<Document.DocumentRequest> Images { get; set; }
    }

    public class NoteResponse
    {
        public NoteResponse()
        {
            Images = new List<Document.DocumentResponse>();
            NoteBy = new UserModel();
        }
        public int Id { get; set; }
        public Guid TaskId { get; set; }
        public string Header { get; set; }
        public string Note { get; set; }
        public DateTime Date { get; set; }
        public UserModel NoteBy { get; set; }

        public List<Document.DocumentResponse> Images { get; set; }
    }
}
