﻿namespace omms.types.Models
{
    public class NotificationModel
    {
        public TaskRequest Task { get; set; }
        public int? DaysBeforeStart { get; set; }
        public int? DaysBeforeEnd { get; set; }
        public string NotificationMessage { get; set; }
        public string NotificationEmail { get; set; }
        public TaskStatus? OnStatus { get; set; }
    }

    public class NotificationState
    {
        public const bool NOT_SEEN = false;
        public const bool SEEN = true;
    }
}
