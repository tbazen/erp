﻿namespace omms.types.Models
{
    public class ReportConstants
    {
        public const string Author = "Omms";
        public const string Application = "Omms Report Generator";
        public const string Subject = "Report";
        public const string key = "Report";
    }

    public class ReportDocumentMetaData
    {
        public readonly string Title;
        public readonly string Header;
        public ReportDocumentMetaData(
            string title,
            string header
            )
        {
            Title = title;
            Header = header;
        }
    }

    public class RPDocumentType
    {
        public const string PDF = "pdf";
        public const string EXCEL = "excel";
    }
}
