﻿namespace omms.types.Models
{
    public class TaskMetaDataModel
    {
        public TaskMetaDataModel()
        {
            PeriodicTask = new MDModel();
            IncidentTask = new MDModel();
            ScheduledTask = new MDModel();
            UnscheduledTask = new MDModel();
        }
        public MDModel PeriodicTask { get; set; }
        public MDModel IncidentTask { get; set; }
        public MDModel ScheduledTask { get; set; }
        public MDModel UnscheduledTask { get; set; }
    }

    public class MDModel
    {
        public int WaitingForCancellation { get; set; }
        public int WaitingForCompletion { get; set; }
        public int WaitingForTermination { get; set; }

        public int Completed { get; set; }
        public int Cancelled { get; set; }
        public int Terminated { get; set; }
    }
}
