﻿using System.Collections.Generic;

namespace omms.types.Models
{

    public class TaskReportViewModel
    {
        public TaskReportViewModel()
        {
            Reports = new List<TaskReportModel>();
            PeriodicTasks = new TRMetadata();
            IncidentTasks = new TRMetadata();
            ScheduledTasks = new TRMetadata();
            UnscheduledTasks = new TRMetadata();
        }

        public List<TaskReportModel> Reports { get; set; }


        public TRMetadata PeriodicTasks { get; set; }
        public TRMetadata IncidentTasks { get; set; }
        public TRMetadata ScheduledTasks { get; set; }
        public TRMetadata UnscheduledTasks { get; set; }
    }

    public class TRMetadata
    {
        public int Ongoing { get; set; }
        public int Completed { get; set; }
        public int Terminated { get; set; }
        public int Cancelled { get; set; }
    }

    public class TaskReportModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string AssigneeType { get; set; }
        public string AssigneeName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int? DelayDays { get; set; }
    }


    public enum TRDFilterParams
    {
        EMPLOYEE = 1,
        TEAM = 2,

        //state filter
        ONGOING_TASKS = 3,
        COMPLETED_TASTKS = 4,
        CANCELLED_TASKS = 5,
        TERMINATED_TASKS = 6,

        //task type
        UNSCHEDULED_TASKS = 7,
        SCHEDULED_TASKS = 8,
        PERIODIC_TASKS = 9,
        INCIDENT_TASKS = 10
    }

    public class TaskReport
    {

        public static readonly ReportDocumentMetaData Metadata = new ReportDocumentMetaData(title: "Task Report", header: "Task Report");

        public const string Id = "Id";
        public const string Name = "Name";
        public const string TaskType = "Task Type";

        public const string Status = "Task State";
        public const string AssigneeType = "Assignee Type";
        public const string AssigneeName = "Assignee Name";
        public const string StartDate = "Start Date";
        public const string EndDate = "End Date";

        public const string DelayIndays = "Delay (In Days)";

    }
}
