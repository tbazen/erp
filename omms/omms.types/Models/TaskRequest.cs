﻿using omms.types.Models.Document;
using System;
using System.Collections.Generic;

namespace omms.types.Models
{
    public enum AssigneeType
    {
        None = 0,
        Employee = 1,
        Team = 2
    };
    public enum MaintenanceType
    {
        Preventive = 1,
        Corrective = 2
    }
    public enum TaskType
    {
        Scheduled = 1,
        Unscheduled = 2,
        Periodic = 3,
        Incident = 4
    }

    public enum PriorityType
    {
        A = 1,
        B = 2,
        C = 3,
        D = 4,
        E = 5
    }

    public class TaskRequest
    {
        public Guid? Id { get; set; }
        public Guid? Wfid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? AssetId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? StartDateExpected { get; set; }
        public DateTime? EndDateExpected { get; set; }

        public DateTime? StartDateActual { get; set; }
        public DateTime? EndDateActual { get; set; }

        public TaskType TaskType { get; set; }
        //for periodic tasks only 
        public int? IntervalDays { get; set; }
        //for incident tasks only
        public IncidentInformation IncidentInfo { get; set; }

        public MaintenanceType MaintenanceType { get; set; }
        public PriorityType Priority { get; set; }
        public Assignee TaskAssignee { get; set; } = new Assignee();

        public DocumentRequest WarrantyDocument { get; set; }
        public List<Guid> DependUpon { get; set; } = new List<Guid>();

        public string Note { get; set; }
    }



    public class Assignee
    {
        public AssigneeType AssigneeType { get; set; }
        public AssigneeModel AssigneeModel { get; set; }
    }

    public class AssigneeModel
    {
        public Guid? TaskId { get; set; }
        public string AssigneeId { get; set; }
        public DateTime? DateAssigned { get; set; }
    }

    public class AssigneeViewModel
    {
        public Assignee Assignee { get; set; }
        public string Note { get; set; }
        public Guid Wfid { get; set; }
    }

    public class IncidentInformation
    {
        public IncidentInformation()
        {
            Images = new List<DocumentRequest>();
        }
        public string GeoLocation { get; set; }
        public string Incident { get; set; }
        public string Description { get; set; }
        public string ReportedBy { get; set; }
        public List<DocumentRequest> Images { get; set; }
    }

}
