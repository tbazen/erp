﻿using omms.types.Models.Document;
using omms.types.Models.User;
using System;
using System.Collections.Generic;

namespace omms.types.Models
{
    public class TaskResponse
    {
        public TaskResponse()
        {
            TaskAssignee = new Assignee();
            WarrantyDocument = new DocumentResponse();
            IncidentInfo = new IncidentResponse();
            CreatedBy = new UserModel();
            DependUpon = new List<Guid>();
        }
        public Guid? Id { get; set; }
        public Guid? Wfid { get; set; }

        public int? AssetId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public UserModel CreatedBy { get; set; }

        public DateTime? StartDateExpected { get; set; }
        public DateTime? EndDateExpected { get; set; }

        public DateTime? StartDateActual { get; set; }
        public DateTime? EndDateActual { get; set; }

        public TaskType TaskType { get; set; }
        //for periodic tasks only 
        public int? IntervalDays { get; set; }
        //for incident tasks only
        public IncidentResponse IncidentInfo { get; set; }
        public MaintenanceType MaintenanceType { get; set; }
        public PriorityType Priority { get; set; }
        public Assignee TaskAssignee { get; set; } = new Assignee();

        public DocumentResponse WarrantyDocument { get; set; }
        public List<Guid> DependUpon { get; set; }
        public string State { get; set; }
        public string Note { get; set; }
    }


    public class IncidentResponse
    {
        public IncidentResponse()
        {
            Images = new List<DocumentResponse>();
        }
        public string GeoLocation { get; set; }
        public string Incident { get; set; }
        public string Description { get; set; }
        public string ReportedBy { get; set; }
        public List<DocumentResponse> Images { get; set; }
    }


}

