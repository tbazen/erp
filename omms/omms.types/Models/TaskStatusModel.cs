﻿using System;

namespace omms.types.Models
{
    public enum TaskStatus
    {
        Pending = 1,
        Deligated = 2,
        RequestApproval = 3,
        Approved = 4,
        Rejected = 5
    }

    public class TaskStatusModel
    {
        //for updating status [ chainging the actual end date ]
        public int? Id { get; set; }
        public Guid TaskId { get; set; }
        public long EmployeeId { get; set; }
        public TaskStatus Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime ExpectedEndDate { get; set; }
        public DateTime? ActualEndDate { get; set; }
        public string Description { get; set; }
        public decimal Progress { get; set; }
    }
}
