﻿using omms.types.Models.User;
using System;
using System.Collections.Generic;

namespace omms.types.Models
{
    public class TeamModel
    {
        public long? Id { get; set; }
        public Guid? Wfid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<UserModel> Members { get; set; } = new List<UserModel>();
        public List<Guid> Tasks { get; set; } = new List<Guid>();

        //For sending and getting workflow note only
        public string Note { get; set; }
    }

}
