﻿namespace omms.types.Models
{
    public class TeamReportModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string DateCreated { get; set; }

        public int NoOfMembers { get; set; }
        public int OnGoingTasks { get; set; }
        public int CompletedTasks { get; set; }
        public int CancelledTasks { get; set; }
        public int TerminatedTasks { get; set; }

        public int TotalTasks { get; set; }
    }

    public enum TMRDFilterParams
    {
        UNASSIGNED_TEAMS = 0,
        ASSIGNED_TEAMS = 1,

        WITH_ONGOING_TASK = 2,
        WITH_COMPLETED_TASK = 3,
        WITH_CANCELLED_TASKS = 4,
        WITH_TERMINATED_TASKS = 5
    }

    public class TeamReport
    {

        public static readonly ReportDocumentMetaData Metadata = new ReportDocumentMetaData(title: "Team Report", header: "Team Report");

        public const string Id = "Id";
        public const string Name = "Name";
        public const string DateCreated = "Date Created";
        public const string NoOfMemberes = "Number Of Members";


        public const string OnGoingTasks = "Ongoing Tasks";
        public const string CompletedTasks = "Completed Tasks";
        public const string CancelledTasks = "Cancelled Tasks";
        public const string TerminatedTasks = "Terminated Tasks";
        public const string TotalTasks = "Total Tasks";

    }
}
