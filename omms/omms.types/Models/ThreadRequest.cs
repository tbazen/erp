﻿using omms.types.Models.Document;
using omms.types.Models.User;
using System;

namespace omms.types.Models.Thread
{

    public enum ThreadState
    {
        Open = 1,
        Lock = 2
    }
    public class ThreadModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ThreadState State { get; set; }
        public TeamModel Team { get; set; }
        public TaskRequest Task { get; set; }
    }

    public class MessageRequest
    {
        public Guid ThreadId { get; set; }
        public string TextMessage { get; set; }
        public DocumentRequest Document { get; set; }
        public long MessageBy { get; set; }
    }

    public class MessageResponse
    {
        public Guid Id { get; set; }
        public string TextMessage { get; set; }
        public DocumentResponse Document { get; set; }
        public DateTime Date { get; set; }
        public UserModel MessageBy { get; set; }
    }

}
