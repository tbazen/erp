﻿namespace omms.types.Models
{
    public class Tree<T>
    {
        public Tree()
        {
            Root = null;
        }
        public Tree(T data)
        {
            Root = new Node<T>(data);
        }
        public Node<T> Root { get; set; }
    }

}
