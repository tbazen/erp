﻿namespace omms.types.Models.User
{
    public enum RoleType
    {
        Employee = 1,
        Admin = 2,
        TechnicalOfficer,
        TecnnicalSupervisor,
        NoRole,
    }
    public class UserModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public long Role { get; set; }
        public long[] Roles { get; set; }
    }
}
