﻿using System;

namespace omms.types.Models
{
    public class UserSession
    {
        public int? EmployeeID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public long Role { get; set; }
        public long[] Roles { get; set; }
        public long Id { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime LastSeen { get; set; }
        public string Token { get; set; }
    }
}
