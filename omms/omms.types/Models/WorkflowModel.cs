﻿using System;

namespace omms.types.Models
{
    public class WFRequestModel
    {
        public Guid Wfid { get; set; }
        public string Note { get; set; }
    }
}
