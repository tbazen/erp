import React, { Component,Fragment } from 'react'
import './App.scss'
import Login from '../modules/shared/login/login'
import IncidentMap from './../modules/shared/map/full-screen';

enum ComponentSwitch{
  LOGIN_PAGE = 'LOGIN PAGE',
  INCIDENT_MAP_PAGE = 'INCIDENT MAP PAGE'
}
interface IState {
  activeComponent:ComponentSwitch
}
export class App extends Component<{},IState> {
  constructor(props:{}){
    super(props)
    this.state={
      activeComponent:ComponentSwitch.LOGIN_PAGE
    }
    this.changeActiveComponent = this.changeActiveComponent.bind(this)
  }

  changeActiveComponent(){
    let {activeComponent} = this.state
    activeComponent = activeComponent == ComponentSwitch.LOGIN_PAGE? ComponentSwitch.INCIDENT_MAP_PAGE : ComponentSwitch.LOGIN_PAGE 
    this.setState({activeComponent})
  }

  public render() {
    let {activeComponent} = this.state
    return (
     <Fragment>
      {
        activeComponent == ComponentSwitch.LOGIN_PAGE &&
        <Login swapComponent = {this.changeActiveComponent}/>
      }
      {
        activeComponent == ComponentSwitch.INCIDENT_MAP_PAGE &&
        <IncidentMap/>
      }
    </Fragment>
    )
  }
}
