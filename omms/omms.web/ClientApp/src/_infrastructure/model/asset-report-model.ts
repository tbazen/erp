export interface AssetReportModel
{
    id: string
    name: string
    assetType: string
    onGoingTasks: number
    completedTasks: number
    cancelledTasks: number
    terminatedTasks: number
    totalTasks: number
}