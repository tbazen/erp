export interface AssetSearhParams
{
    pkuId: number
    assetType: string
}

export const enum AssetTypes
{
    PIPELINE= 'Pipe',
    VALVE= 'Valve',
    BOREHOLE= 'Borehole',
    ENDPOINTS= 'End Points',
    RESERVIOR= 'Reservior',
    HYDRANTS= 'Hydrant',
    REDUCE= 'Reduce',
    HOUSE_CONNECTIONS= 'House Connection',
    WATER_METER = 'Water meter'
}