import { DocumentRequest, DocumentResponse } from './documentModel'
import { IUserInformation } from 'src/_infrastructure/model/userInformationModel';
export interface AssetModel {
  id: number | null
  name: string
  description: string
  geoLocation: number
}

export interface AssetReading {
  id: number | null
  asset: AssetResponse
  dateRecorded: Date | string | null
  readings: Schema[]
  readingBy: IUserInformation
}

export interface AssetRequest
{
  id: number | null;
  wfid: string | null;
  name: string;
  description: string;
  note: string;
  geoLocation: string;
  typeId: number;
  pkuId: number;
  images: DocumentRequest[];
}

export interface AssetResponse {
  id: number | null
  wfid: string | null
  pkuId: number
  name: string
  note: string
  description: string
  geoLocation: string
  assetType: AssetType
  images: DocumentResponse[]
}

export interface AssetFormSchema {
  assetTypes: AssetType[]
}

export interface AssetReadingSchema {
  reading: AssetType
}

export interface AssetType {
  id: number
  name: string
  attributes: Schema[]
}

export interface Schema {
  id: number
  attributeName: string
  valueType: string
  priority: number
  value: string
}
