export interface DocumentRequest {
  Date: number
  Ref: string
  Note: string
  Mimetype: string
  Type: number | null
  Filename: string
  File: string

  // only for viewing documents from a custom url (esp. in work items)
  Id: string | null
  OverrideFilePath: string
}

export interface DocumentResponse {
  id: string
  date: number
  ref: string
  note: string
  mimetype: string
  type: number | null
  filename: string

  // only for viewing documents from a custom url (esp. in work items)
  overrideFilePath: string
}
