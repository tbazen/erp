export interface EmployeeReportModel
{
    employeeId: string;
    name: string;

    onGoingTasks: number;
    completedTasks: number;
    cancelledTasks: number;
    terminatedTasks: number;
    totalTasks: number;
}